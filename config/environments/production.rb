Rails.application.configure do

  config.paperclip_defaults = {
    storage: :s3,
    s3_credentials: {
      bucket: ENV.fetch('S3_BUCKET_NAME'),
      s3_host_name: ENV.fetch('S3_HOST_NAME'),
      access_key_id: ENV.fetch('AWS_ACCESS_KEY_ID'),
      secret_access_key: ENV.fetch('AWS_SECRET_ACCESS_KEY'),
      s3_region: ENV.fetch('AWS_REGION')
    }
  }

  Twilio.configure do |config|
    config.account_sid = ENV['TWILIO_SID']
    config.auth_token = ENV['TWILIO_TOKEN']
  end

  # Settings specified here will take precedence over those in config/application.rb.

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.logger = nil

  config.action_mailer.default_url_options = { :host => ENV['APP_ROOT'] }
  config.action_controller.default_url_options = { host: ENV['APP_ROOT'] }
  # config.action_controller.asset_host = ENV['APP_ROOT']

  # config.action_mailer.smtp_settings = {
  #   address: ENV["SMTP_ADDRESS"], # example: "smtp.sendgrid.net"
  #   authentication: :plain,
  #   domain: ENV["SMTP_DOMAIN"], # example: "heroku.com"
  #   enable_starttls_auto: true,
  #   password: ENV["SMTP_PASSWORD"],
  #   port: "587",
  #   user_name: ENV["SMTP_USERNAME"]
  # }
  # puts 'ENV[SMTP_USERNAME]'

  config.action_mailer.smtp_settings = {
    :user_name => ENV.fetch('SENDGRID_USERNAME'),
    :password => ENV.fetch('SENDGRID_PASSWORD'),
    :domain => ENV.fetch('SMTP_DOMAIN'),
    :address => ENV.fetch('SMTP_ADDRESS'),
    :port => 587,
    :authentication => :plain,
    :enable_starttls_auto => true
  }
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true

  config.action_controller.page_cache_directory = Rails.root.join("public", "cached_pages")

  # Attempt to read encrypted secrets from `config/secrets.yml.enc`.
  # Requires an encryption key in `ENV["RAILS_MASTER_KEY"]` or
  # `config/secrets.yml.key`.
  config.read_encrypted_secrets = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  # config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.year.seconds.to_i}"
}

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = Uglifier.new(harmony: true)
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true
  config.serve_static_files = true
  # Generate digests for assets URLs
  config.assets.digest = true

  config.assets.logger = Logger.new $stdout

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Mount Action Cable outside main process or domain
  # config.action_cable.mount_path = nil
  # config.action_cable.url = "wss://test.quickcard.me/cable"
  # config.action_cable.url = "wss://quickcard-chat.herokuapp.com/cable"
  # config.action_cable.allowed_request_origins = [/http:\/\/*/, /https:\/\/*/, /wss:\/\/*/]
  config.action_cable.allow_same_origin_as_host = true
  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = true
  config.ssl_options = {
    hsts: {subdomains: true, preload: true, expires: 1.year}
  }

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  if ENV['HEROKU_APP'].present? || ENV['RAILS_LOG_TO_STDOUT'].present?
    config.log_level = :debug
  else
    config.log_level = :info
  end
  #Datadog.configure do |c|
  #  c.tracer enabled: Rails.env.production? && ENV["HEROKU_APP"].present?, tags: { env: ENV["STACK"] }
  #  c.use :rails, service_name: 'quickcard-app'
  #end

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]

  # Use a different cache store in production.
  # cache_servers = %W(#{ENV["REDISTOGO_URL"]})
  if ENV['HEROKU_APP'].blank? || (ENV['HEROKU_APP'].present? && ENV['HEROKU_APP'] != true)
    # config.cache_store = :redis_cache_store, { url: cache_servers,
    #   connect_timeout:    30,  # Defaults to 20 seconds
    #   read_timeout:       0.2, # Defaults to 1 second
    #   write_timeout:      0.2, # Defaults to 1 second
    #   reconnect_attempts: 1,   # Defaults to 0

    #   error_handler: -> (method:, returning:, exception:) {
    #     # Report errors to Sentry as warnings
    #     Raven.capture_exception exception, level: 'warning',
    #       tags: { method: method, returning: returning }
    #   }
    # }
    config.cache_store = :redis_store, "#{ENV["REDISTOGO_URL"]}/0", { expires_in: 180.minutes }
  end

  # Use a real queuing backend for Active Job (and separate queues per environment)
  config.active_job.queue_adapter = :sidekiq
  config.active_job.queue_name_prefix = "quickcard"
  config.active_job.queue_name_delimiter = "_"

  config.mailer_sender = 'support@quickcard.me'
  config.action_mailer.perform_deliveries = true
  config.action_mailer.default_options = {from: 'QuickCard <support@quickcard.me>'}

  # config.active_job.queue_name_prefix = "quickcard_#{Rails.env}"
  config.action_mailer.perform_caching = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require 'syslog/logger'
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
end
