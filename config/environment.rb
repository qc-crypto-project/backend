# Load the Rails application.
require_relative 'application'

# Getting latest published tag from GITHUB Repo
QuickCard::APP_VERSION = `git --git-dir=/home/deploy/quickcard/repo/ describe --tags $(git --git-dir=/home/deploy/quickcard/repo/ rev-list --tags --max-count=1)` unless defined? QuickCard::APP_VERSION

# Initialize the Rails application.
Rails.application.initialize!
