require_relative 'boot'
require 'rails/all'
require 'write_xlsx'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
require "active_model_serializers"
require 'slack-ruby-client'
require 'wicked_pdf'
require 'wicked'

module QuickCard
  class Application < Rails::Application
    # GETTING ENV vars from AWS ParameterStore
    if ['development', 'test', 'staging'].include?(Rails.env)
      STACK = Rails.env
    elsif ENV['HEROKU_APP'].blank?
      require_relative '../app/services/ec2'
      ec2 = EC2.new
      STACK = ec2.stack
      if STACK.present?
        ENV['STACK'] = STACK
        KEYSTORE = ec2.parameter_store
        KEYSTORE.params.each { |key, value| ENV[key] = value }
      end
      #ENV = KEYSTORE.params.merge(ENV).with_indifferent_access
    end

    Slack.configure do |config|
      config.token = ENV['SLACK_API_TOKEN']
      # raise 'Missing ENV[SLACK_API_TOKEN]!' unless config.token
    end

    # Initialize configuration defaults for originally generated Rails version.
    Rails.application.config.content_security_policy_nonce_generator = -> request { SecureRandom.base64(16) }
    config.load_defaults 5.1
    config.middleware.use WickedPdf::Middleware
    # config.api_only = true

    config.action_dispatch.default_headers = {
      'Feature-policy' => 'geolocation none',
      'X-Frame-Options' => 'ALLOWALL',
      'X-XSS-Protection' => '1; mode=block',
      'X-Content-Type-Options' => 'nosniff',
      'X-Download-Options' => 'noopen',
      'X-Permitted-Cross-Domain-Policies' => 'none',
      'Referrer-Policy' => 'strict-origin-when-cross-origin'
    }

    # config.middleware.insert_before 0, Rack::Cors do
    #   allow do
    #     origins '*'
    #     resource '*', :headers => :any, :methods => [:get, :post, :options]
    #   end
    # end

    Bundler.require(*Rails.groups)
    Dotenv::Railtie.load
    HOSTNAME = ENV['HOSTNAME']
    APP_ROOT = ENV['APP_ROOT']
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Disable Redis integer warnings in sidekiq logs
    Redis.exists_returns_integer = false

    config.exceptions_app = ->(env) { ExceptionsController.action(:show).call(env) }
    config.active_record.cache_timestamp_format = :nsec
    config.middleware.insert_after ActionDispatch::Static, Rack::Deflater

    config.i18n.available_locales = [:en]

    # Inject Sentry logger breadcrumbs
    # require 'raven/breadcrumbs/logger'

    # config.rails_activesupport_breadcrumbs = true

    # Sentry Exception Logging Turned ON
    Raven.configure do |config|
      config.dsn = ENV['SENTRY_DSN']
      config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
      config.sanitize_credit_cards = true
      config.ssl_verification = true
      config.timeout = 2
      config.excluded_exceptions = ['ActionController::RoutingError']
      config.release = ENV['version_tag']

      config.tags = { server: ENV['APP_ROOT'], release: ENV['version_tag'] }
      config.current_environment = ENV["STACK"]
      config.environments = %w[ production staging beta heroku]

      config.async = lambda { |event|
        if ENV['SENTRY_DSN'].present?
          SentryWorker.perform_later(event)
        end
      }
      # Report is sentry hook fails
      config.transport_failure_callback = lambda { |event|
        SlackService.notify("*Alert!* - SENTRY EXCEPTION LOGGING FAILED!")
      }
    end
  end
end
