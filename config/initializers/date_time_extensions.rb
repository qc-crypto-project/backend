class DateTime
  def get_beginning_of_day(time_zone)
    time = Time.parse("#{self.utc}")
    time = time.in_time_zone(time_zone).beginning_of_day.utc
    return time
  end

  def get_end_of_day(time_zone)
    time = Time.parse("#{self.utc}")
    time = time.in_time_zone(time_zone).end_of_day.utc
    return time
  end

  def set_time(time_zone)
    return self.utc + Time.zone_offset(time_zone)
  end

  def valid_datepicker(time_zone)
    utc_time = Time.strptime(self, "%m/%d/%Y")
    return Time.new("#{utc_time.year}", "#{utc_time.month}", "#{utc_time.day}", "#{utc_time.hour}","#{utc_time.min}",00, time_zone).utc
  end
end