SecureHeaders::Configuration.default do |config|
  config.cookies = {
    secure: true, # mark all cookies as "Secure"
    httponly: SecureHeaders::OPT_OUT, # do not mark any cookies as HttpOnly
    samesite: {
      lax: true # mark all cookies as SameSite=lax
    }
  }
  # Add "; preload" and submit the site to hstspreload.org for best protection.
  config.x_frame_options = "SAMEORIGIN"
  config.x_content_type_options = "nosniff"
  config.x_xss_protection = "1; mode=block"
  config.x_download_options = "noopen"
  config.x_permitted_cross_domain_policies = "none"
  config.referrer_policy = %w(origin-when-cross-origin strict-origin-when-cross-origin)

  config.csp = {
    # "meta" values. these will shape the header, but the values are not included in the header.
    preserve_schemes: true, # default: false. Schemes are removed from host sources to save bytes and discourage mixed content.
    disable_nonce_backwards_compatibility: true, # default: false. If false, `unsafe-inline` will be added automatically when using nonces. If true, it won't. See #403 for why you'd want this.

    # directive values: these values will directly translate into source directives
    default_src: %w('self' http:),
    base_uri: %w('self'),
    block_all_mixed_content: true, # see http://www.w3.org/TR/mixed-content/
    #connect_src: %w(wss:),
    #font_src: %w('self' https://maxcdn.bootstrapcdn.com/* https://code.ionicframework.com/* https://cdnjs.cloudflare.com/* data:),
    font_src: %w('self' https:  data:),
    connect_src: %w('self' wss://*.herokuapp.com wss://*.quickcard.me ws:),
    frame_src: %w('self' https://www.google.com/ ),
    #report_uri: %w( "#{ENV['REPORT_URL']}" ),
    frame_ancestors: %w('none'),
    manifest_src: %w('self' https:),
    object_src: %w('self'),
    img_src: %w('self' https: data:),
    #sandbox: true, # true and [] will set a maximally restrictive setting
    #script_src: %w('self' https:),
    script_src: %w('self' 'unsafe-eval' https:),
    style_src: %w('self' 'unsafe-inline' https:),
    worker_src: %w('self'),
    #upgrade_insecure_requests: true, # see https://www.w3.org/TR/upgrade-insecure-requests/
    #report_uri: %w(https://report-uri.io/example-csp)
  }
end
