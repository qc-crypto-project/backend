FILTER_PARAMS = [:password, :password_confirmation, :card_number, :cvv, :card_cvv, :account_number, :routing_number, :cardNoSafe]
Rails.application.config.filter_parameters << lambda do |param_name, value|
  if param_name.match(/card/)
    # Masking Card Numbers to show First 6 & Last 4 digits
    if value.present? && value.try(:gsub, /\d/).present?
      return value.try(:gsub, /\d/).with_index do |number, index|
        if index >= 6 && index < value.to_s.size - 4
          '*'
        else
          number.to_s
        end
      end
    end
  elsif FILTER_PARAMS.include?(param_name.to_sym)
    value.replace("[FILTERED]") if value.present?
  end
end
