Minfraud.configure do |c|
  c.license_key = ENV['MIN_FRAUD_LICENCE_KEY']
  c.user_id     = ENV['MIN_FRAUD_USER_ID']
end