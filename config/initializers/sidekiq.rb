require "sidekiq/web"
require 'sidekiq/cron/web'
require 'sidekiq-status'
require 'sidekiq-status/web'
require 'autoscaler/sidekiq'
require 'autoscaler/heroku_platform_scaler'

heroku = nil
if ENV['HEROKU_APP'].present? && ENV['HEROKU_APP'] == true
  heroku = Autoscaler::HerokuPlatformScaler.new
end

Sidekiq.configure_server do |config|
  config.redis = { driver: :hiredis, url: "#{ENV['REDISTOGO_URL']}" }
  config.server_middleware do |chain|
    if heroku
      p "[Sidekiq] Running on Heroku, autoscaler is used"
      chain.add(Autoscaler::Sidekiq::Server, heroku, 60) # 60 seconds timeout
    else
      p "[Sidekiq] Running locally, so autoscaler isn't used"
    end
  end
  config.death_handlers << ->(job, ex) do
    # puts "Uh oh, #{job['class']} #{job["jid"]} just died with error #{ex.message}."
    puts "Sidekiq error backtrace", ex.backtrace
    back_trace = ex.backtrace.select { |x| x.match(Rails.application.class.parent_name.downcase)}.first(15)
    SlackService.notify("Uh oh, *#{job['class']} - #{job["jid"]}* just died with error #{ex.message}. \n App: \`\`\` #{{URL: ENV['APP_ROOT'], HOST: ENV['APP_ROOT']}.to_json} \`\`\` Error: \`\`\` #{ex.to_json}  \`\`\` BACKTRACE: \`\`\` #{back_trace.to_json} \`\`\`")
    # SlackService.notify("Uh oh, #{job['class']} #{job["jid"]} just died with error #{ex.message}.")
  end
  # config.error_handlers << Proc.new {|ex, ctx_hash| SlackService.handle_exception(ex, '#quickcard-exceptions', ctx_hash) }

  # Sidekiq Cron Settings
  schedule_file = "config/schedule.yml"
  if File.exist?(schedule_file) && Sidekiq.server?
    Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
  end

  # accepts :expiration (optional)
  Sidekiq::Status.configure_server_middleware config, expiration: 30.minutes
  # accepts :expiration (optional)
  Sidekiq::Status.configure_client_middleware config, expiration: 30.minutes
end

Sidekiq.configure_client do |config|
  config.redis = { driver: :hiredis, url: "#{ENV['REDISTOGO_URL']}" }
  if heroku
    config.client_middleware do |chain|
      chain.add Autoscaler::Sidekiq::Client, 'default' => heroku
    end
  end
  Sidekiq::Status.configure_client_middleware config, expiration: 30.minutes
end
