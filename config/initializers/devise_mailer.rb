class DeviseMailer < Devise::Mailer
  def reset_password_instructions(record, token, opts={})
    mail = super
    mail.subject = "Create password instructions" if record.old_passwords.count == 0 
    mail
  end
  def email_changed(record, opts={})
    return if record.user?
    super
  end
end