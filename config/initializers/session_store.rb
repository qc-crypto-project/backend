Rails.application.config.session_store :cookie_store, :key => '_quickcard_', expire_after: 2.days, secure: true unless ['development'].include?(Rails.env)
