ALGORITHM = 'HS256'

class String

  def initial
    self[0,1]
  end

  def to_currency
    number_to_currency(self)
  end

  def jwt_encode
    JWT.encode self, Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
  end

  def jwt_decode
    JWT.decode self, Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
  end

  def filtered
    uri = URI(self)
    hash = Rack::Utils.parse_query uri.query
    filters = Rails.application.config.filter_parameters
    param_filter = ActionDispatch::Http::ParameterFilter.new filters
    hash = param_filter.filter hash
    uri.query = URI.encode_www_form hash
    return uri.to_s
  end

  def filter_json
    return self if !self.valid_json?
    hash = JSON.parse(self)
    hash.filter_json
  end

  def valid_json?
    return false if self.blank?
    JSON.parse(self)
    return true
  rescue JSON::ParserError => e
    return false
  end
end

class Hash
  def filter_json
    filters = Rails.application.config.filter_parameters
    param_filter = ActionDispatch::Http::ParameterFilter.new filters
    return param_filter.filter self
  end

  def valid_json?
    return false if self.blank?
    self.to_json.valid_json?
  end
end
