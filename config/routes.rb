Rails.application.routes.draw do
  namespace :admins do
    get 'messages/index'
    get 'messages/show'
    resources :emails do
      collection do
        get :export
        get :delete_schedule
        post :resend
        get :resend_email
      end
    end
  end

  require 'sidekiq/web'

  root :to => 'home#index'
  
  # ActionCable mounted for microservice comms
  mount ActionCable.server => '/cable'
  # Sidekiq mounted for background queueing dashboard
  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  get 'contacts/message'
  get 'update_valid/:id', to: 'admins#update_valid', as: :update_valid_app

  resources :checkout, module: 'checkout', only: [:index,:new] do
    collection do
      get 'error'
      post 'get_card_information'
      get 'documentation'
      get 'select_payment_options'
      get 'select_payment_options_post'
      post 'checkout'
      post 'invoice_rtp_request'
      post 'test_webhook'
      get 'invoice_rtp_checkout'
    end
  end


  namespace :customers do
    resources :messages , only: [:index,:show,:new] do
      collection do
        post :save_chat
      end
    end
    resources :orders , only: [:index,:show]
    resources :disputes, only: [:index,:new,:create,:show] do
      collection do
        get 'update_tracker/:id' , to: "disputes#update_tracker" , as: :update_tracker
        get 'mail_generate_label/:id' , to: "disputes#mail_generate_label" , as: :mail_generate_label
        get 'update_notification/:id' , to: "disputes#update_notification" , as: :update_notification
      end
      resources :dispute_requests , only: [:update,:create]
    end
    resources :profiles , only: [:edit,:update] , path: '' do
      collection do
        get 'form_validate' , to: "profiles#form_validate"
      end
    end
  end


  resources :transactions, module: 'checkout'  do
    collection do
      post 'payment'
      post 'rtp_payment'
      get 'rtp_payment1'
      post 'rtp_payment_hook'
      get 'status'
    end
  end

  namespace :admins do
    resources :load_balancers, only: [:index, :new, :create, :edit, :update, :show] do
      collection do
        get 'update_load_balancer'
        post 'update_load_balancer'
      end
    end

    # get 'sales/index' , to: 'sales#index'
    # get 'sales/batch_detail' , to: 'sales#batch_detail'
    # get 'sales/qr_refund' , to: 'sales#qr_refund'
    # get 'sales/edit' , to: 'sales#edit'
    # get 'sales/refund_all' , to: 'sales#refund_all'
    # put 'sales/update' , to: 'sales#update'
    get 'locations/check_location_name'
    get 'mechant_bank_apis/index'
    get 'mechant_bank_apis/boltpay'
    get 'mechant_bank_apis/new'
    post 'mechant_bank_apis/create'
    get 'mechant_bank_apis/show'
    get 'mechant_bank_apis/edit'
    post 'mechant_bank_apis/update'
    get 'merchants/country_data'

    get :customer_disputes, to: "customer_disputes#customer_disputes"
    resources :customer_disputes, only: [:show]
    resources :messages, only: [:index,:show]
    # ---------sub-merchant-Admin Portal---------------
    get 'merchants/new_sub_merchant' , to:  'merchants#new_sub_merchant'
    post 'merchants/create_sub_merchant' , to:  'merchants#create_sub_merchant'
    get 'merchants/edit_sub_merchant' , to: 'merchants#edit_sub_merchant'
    put 'merchants/update_sub_merchant' , to: 'merchants#update_user_sub_merchant'
    get 'merchants/destroy_sub_merchant' , to: 'merchants#destroy_sub_merchant'

    resources :maxmind, only: [:show] do
      collection do
        get :edit
        post :update
      end
    end
    # ---------sub-merchant-Admin Portal---------------

    resources :plugin

    resources :admin_users do
        collection do
            get :get_data
            get :profile
        end
    end
    resources :affiliate_programs
    resources :reports, only: [] do
      collection do
        get :old_reports
        get :run_report
        get :autocomplete_location
        get :autocomplete_location_WR
        get :autocomplete_user
        get :autopopulate_partner
        get :autopopulate_affiliates
        post :generate_report
        get :generate_report
        get :search_user
        kinds = %w|chargeback sale gateway merchant wallet withdrawal terminal_id affiliate_program cbk_detail|
        get "index/:kind" => "reports#index", constraints: {kind: Regexp.new(kinds.join('|'))}
      end
    end

    resources :ach_gateways, only: [:index,:create,:edit,:update,:new] do
      collection do
        get 'archive_ach_gateway/:id', to:  "ach_gateways#archive", :as => :archive_ach_gateway
        get :archived_ach_gateways
        get 'block_ach_gateways/:id', to:  "ach_gateways#block_ach_gateways", :as => :block_ach_gateways
        get 'push_to_card' , to: "ach_gateways#push_to_card" , :as => :push_to_card
        get 'check' , to: "ach_gateways#check" , :as => :check
        get 'archive_push_to_card_gateway/:id', to:  "ach_gateways#archive", :as => :archive_push_to_card_gateway
        get 'archive_check_gateway/:id', to:  "ach_gateways#archive", :as => :archive_check_gateway
        get :archived_push_to_card_gateways
        get :archived_check_gateways
        get :check_gateway_existance
        # get :getverticaltype
        # get :getverticaltypenew
        # post :postverticaltype
      end
    end

    resources :slot_gateways, only: [:index,:create,:edit,:update,:new] do
      collection do
        get 'archive_slot_gateway/:id', to:  "slot_gateways#archive", :as => :archive_slot_gateway
        get :archived_slot_gateways
        get :validate_slot
        get :slot_management
        get :reset_slot_setting
        post :post_slot_management
        get 'block_slot_gateways/:id', to:  "slot_gateways#block_slot_gateways", :as => :block_slot_gateways
      end
    end

    resources :total_funds, only: [:index] do
      collection do
        get 'total_funds_search'
      end
    end


    resources :payment_gateways, only: [:index,:create,:edit,:update,:new] do
      collection do
        get 'archive_gateway/:id', to:  "payment_gateways#archive", :as => :archive_gateway
        get :archived_gateways
        get :unique_descriptor
        get 'block_gateways/:id', to:  "payment_gateways#block_gateways", :as => :block_gateways
        get 'specific_merchant', to:  'payment_gateways#specific_merchant'
        get :getverticaltype
        get :getverticaltypenew
        post :postverticaltype
        get :new_descriptor
        post :archive_descriptor
        post :create_descriptor
        get :old_gateways
      end
    end

    get 'transfers/index'
    get 'transfers/get_location_wallets'
    get 'transfers/get_merchant_location'
    get 'transfers/get_users'
    get 'transfers/get_user_details'
    post 'transfers/transfer_post'
    get 'transfers/get_wallet_name'
    resources :transfers, only: [] do
      get :autocomplete_user_name, on: :collection
    end

    resources :buy_rate, except: [:show] do
      collection do
        get :verify_admin
        get :verify_admin_password
      end
    end

    get 'buy_rate/getAgents'
    get 'buy_rate/getAffiliates'
    get 'buy_rate/getUsers'
    get 'buy_rate/getMerchantLocations'

    # ach
    get 'batch_checks', to: 'checks#batch_checks' , as: :batch_checks
    get 'check_batches', to: 'checks#check_batches' , as: :check_batches
    post 'check_batch_date', to: 'checks#check_batch_date' , as: :check_batch_date
    get :ach_index, :to => 'ach#index'
    get :under_review, :to => 'checks#under_review'
    get 'bulk_status_change', to: 'checks#bulk_status_change' , as: :bulk_status_change

    # post :instant_pay_batch_date, :to=>'instant_pay#instant_pay_batch_date',as: :member

    post :ach_batch_date, :to=>'instant_pay#ach_batch_date',as: :member
    get :ach_approved, :to => 'ach#ach_approved'
    get 'ach/new',  :to => 'ach#new'
    get 'ach/:id/approve', to: 'ach#approve',as: 'ach_approve'
    get 'ach/batches'
    get 'selected_locations', :to => 'ach#selected_locations'
    get 'location_data', :to => 'ach#location_data'
    # post 'create_ach', :to => 'ach#create_ach'

    #instant pay
    get :instant_pay_verify_phone_number, :to => 'instant_pay#verify_phone_number'
    get :instant_pay_verification, :to => 'instant_pay#verification'
    get :instant_pay_index, :to => 'instant_pay#index'
    # post :instant_pay_index, :to => 'instant_pay#index'
    post :instant_pay_batch_date, :to=>'instant_pay#instant_pay_batch_date',as: :instant_member
    get :approve_debit_card_deposit, :to => 'checks#approve_debit_card_deposit'
    get 'instant_pay/new',  :to => 'instant_pay#new'
    get :instant_pay_approved, :to => 'instant_pay#instant_pay_approved'
    get 'instant_pay/:id/approve', to: 'instant_pay#approve',as: 'instant_approve'
    put :update_name, :to => 'instant_pay#update_name'
    put 'instant_pay/update', :to => 'instant_pay#update', as: :instant_pay_update
    get 'instant_pay/batches'
    get 'instant_pay/selected_locations', :to => 'instant_pay#selected_locations'
    get 'instant_pay/location_data', :to => 'instant_pay#location_data'
    # post 'create_instant_pay', :to => 'instant_pay#create_instant_pay'
    get 'instant_pay/bulk_status_change', to: 'instant_pay#bulk_status_change' , as: :instant_pay_bulk_status_change

    #= instant_ach
    get :instant_ach_verify_phone_number, :to => 'instant_ach#verify_phone_number'
    get :instant_ach_verification, :to => 'instant_ach#verification'
    get :instant_ach_index, :to => 'instant_ach#index'
    post :instant_ach_export, :to => 'instant_ach#export'
    post :instant_ach_batch_date, :to=>'instant_ach#instant_ach_batch_date',as: :instant_ach_member
    get :instant_ach_approved, :to => 'instant_ach#instant_ach_approved'
    get :d_date, :to => 'instant_ach#d_date'
    put :update_recipient, :to => 'instant_ach#update_recipient'
    get 'instant_ach/new',  :to => 'instant_ach#new'
    get 'instant_ach/:id/approve', to: 'instant_ach#approve',as: 'instant_ach_approve'
    get 'instant_ach/batches'
    get 'instant_ach/selected_locations', :to => 'instant_ach#selected_locations'
    get 'instant_ach/location_data', :to => 'instant_ach#location_data'
    # post 'create_instant_ach', :to => 'instant_ach#create_instant_ach'
    get 'instant_ach/achs_search'
    get 'instant_ach/batches_search'
    get 'instant_ach/bulk_status_change'
    get :approve_ach_request, to: 'instant_ach#approve_ach_request'
    get :edit_bank_details, to: 'instant_ach#edit_bank_details'
    post :update_bank_details, :to => 'instant_ach#update_bank_details'
    get :approve_request, to: 'instant_ach#approve_request'
    get :image_delete, to: 'instant_ach#image_delete'
    get :disapprove_ach_request,to: 'instant_ach#disapprove_ach_request'

    post :get_bank_by_routing_number_ach, :to => 'ach#get_bank_by_routing_number'
    # resources :ach, :except => [:new]
    get 'invoices/index'
    get 'invoices/invoice_filter'
    get 'checks/refresh_status'
    get 'checks/refreshing_check'
    get 'checks/remaining_check'
    #get 'checks/admin_checks',  to:'checks#admin_checks'
    get 'checks/send_check',  to:'checks#send_check'
    get 'checks/get_fee_sum'
    get 'checks/getUsers'
    # post 'checks/create_check'
    post 'checks/get_bank_by_routing_number'
    post 'checks/export_checks'
    get 'merchants/get_data'
    get 'merchants/next_baked_section'
    #get 'merchants/location_setting'
    #post 'merchants/update_setting'
    #get 'merchants/location_fee'
    #post 'merchants/update_fee'
    #get 'merchants/location_documentation'
    #post 'merchants/update_documentation'
    #get 'merchants/location_iso_agents'
    #post 'merchants/update_iso_agents'
    get 'merchants/document_download'
    get 'merchants/document_delete'

    # merchant profile
    #get 'merchants/profile'
    get 'merchants/update_status'
    get 'merchants/update_two_step'
    get 'merchants/send_unblock_email'
    get 'merchants/resend_welcome_email'
    #get 'merchants/update_location_block'
    #get 'merchants/unlock'
    #post 'merchants/unlock_post'
    #get 'merchants/wallet_detail'
    #get 'merchants/wallet_detail_transactions'
    #get 'merchants/transfer_money'
    #get 'merchants/specfic_transactions'
    #get 'merchants/specfic_decline_transactions'
    #get 'merchants/apis_keys'
    #get 'merchants/location_users'
    #get 'merchants/location_bank_account'
    #post 'merchants/update_location_bank'
    #get 'merchants/location_cbks'
    #get 'merchants/export_location_cbks'
    #get 'merchants/employee'
    #post 'merchants/post_employee'
    #get 'merchants/profile_location'
    #post 'merchants/post_profile_location'
    #get 'merchants/more_owners'
    #get 'merchants/remove_owner'
    #get 'merchants/delete_users'
    get 'merchants/clear_all'
    get 'merchants/edit'
    post 'merchants/update'
    post 'merchants/export'
    post 'transfers/export'
    get 'transfers/search'
    get 'merchants/search' #TODO remove this
    get 'merchants/duplicate_owner'
    get 'merchant_circle', to: 'hold_money#index'
    get 'released_transactions', to: 'hold_money#transactions'
    resources :order_banks, :only => [:index] do
      collection do
        post :export
        post :export_summary
      end
    end

    resources :companies do
      collection do
        get :form_validate
        get :check_phone_format
        get :check_phone_number_format
      end
    end

    resources :activity_logs do
      collection do
        get :un_authorized
        get :not_found
        get :server_error
        get :bad_request
        get :system
        get :more_activity_logs
        get :previous_activity_logs
        get :search_logs
        get :email_track
        get :version
        get :graph_setting
        get :accounting_setting
        get :api_setting
        post :post_version
        get :affiliate_logs
      end
    end

    resources :merchant_apis do
      collection do
        get :vt_api
        post :submit_vt_api
        post :submit_api
      end
    end

    resources :invoices, only: [:index, :show, :edit] do
      collection do
        get 'pay_invoice/:id', to: 'invoices#pay_invoice', as: :pay_invoice
        get :verify_phone_number
      end
    end

    resources :checks, only: [:index, :show, :edit, :update] do
      collection do
        get 'pay_check/:id', to: 'checks#pay_check', as: :pay_check
        get 'approve_debit_card_deposit/:id', to: 'checks#approve_debit_card_deposit' , as: :approve_debit_card_deposit
        get 'approve/:id', to: 'checks#approve' , as: :approve
        # get 'approve_by_ach/:id', to: 'checks#approve_by_ach' , as: :approve_by_ach
        get :verify_phone_number
      end
    end

    resources :audits do
      collection do
        get :users
        get :transactions
        get 'user_reports/:id', to: 'audits#user_report', as: :user_report
        get 'transaction_reports/:id', to: 'audits#transaction_report', as: :transaction_report
      end
    end

    resources :disputes do
      collection do
        post :delete_dispute_notification_notes
        post :submit_dispute_case
        post :dispute_notification_reason
        post :dispute_notification_note
        get :dispute_notification_notes
        get :dispute_notification_reasons
        get :dispute_case
        get :cases
        get :imported_cbk
        get :imported_for_lost
        post :bulk_cbk
        post :bulk_cbk_for_lost
        get :dispute_evidence
        get :get_case_number
      end
    end

    resources :blocked_cards do
      collection do
        get :cards
        get :import_cards
        get :import_cards_to
        post :bulk_cards
        get :export_download
        delete :destroy_multiple_block_cards
        get :export_cards
      end
    end
  end

  namespace :merchant do
    resources :customer_disputes, only: [:index, :show] do
      resources :dispute_requests, only: [:new,:update] do
        get :return_product
        get :generate_label
      end
    end
    resources :messages, only: [:index,:show] do
      collection do
        post :save_chat
      end
    end
    resources :shipment, only: [:index]
    resources :permission
    resources :plugins, only: [:index]

    resources :maxmind, only: [:index,:show,:update]

    get '/pages', to: 'pages#index'
    get 'bank/index'
    post 'bank/create'
    get  'testing_buyrate'
    post  'testing_buyrate_post'
    get 'deposites', to: 'hold_money#index'
    resources :orders, only: [:index] do
      collection do
        get :get_rates
        get :add_carrier_key
        get :carrier_key
        get :carriers
        get :get_warehouse
        get :warehouse
        get :add_warehouse
        post :post_warehouse
        get :add_label
        post :post_add_label
        get 'add_tracker', to: 'orders#new'
        get 'tracker_detail', to: 'orders#show'
        get 'cancel_shipment', to: 'orders#cancel'
        post 'set_tracker', to: 'orders#update'
      end
    end


    get :ach_index, :to => 'ach#index'
    get 'ach/new',  :to => 'ach#new'
    post 'create_ach', :to => 'ach#create_ach'
    get 'location_data', :to => 'ach#location_data'

    get :instant_ach_index, :to => 'instant_ach#index'
    get 'instant_ach/new',  :to => 'instant_ach#new'
    post 'create_instant_ach', :to => 'instant_ach#create_instant_ach'
    post 'request_instant_ach', :to => 'instant_ach#request_instant_ach'
    get 'instant_ach/location_data', :to => 'instant_ach#location_data'

    get 'released_transactions', to: 'hold_money#transactions'
  end

  devise_scope :user do
    get 'reset/password/:id', to: 'users/passwords#edit', as: 'resetemailpassword'
    get 'instant_ach/approve_request/:id', to: 'users/passwords#approve_request', as: 'approve_request'
    get 'check_failure', to: "users/sessions#check_failure" , as: 'check_failure'
  end

  resources :agreements, :only => [] do
    collection do
      get :tos
      get :legal
    end
  end

  devise_for :users, :controllers => { registrations: 'users/registrations', sessions: 'users/sessions' , passwords: 'users/passwords', two_factor_authentication: 'users/two_factor_authentication', unlocks: 'users/unlocks',password_expired: 'users/password_expired'}

  devise_scope :user do
    get '/try_again', to: 'users/sessions#try_again', :as => :try_again
    get '/download_invoice', to: 'users/sessions#download_invoice', :as => :download_invoice
    get '/download_virtual_terminal', to: 'users/sessions#download_virtual_terminal', :as => :download_virtual_terminal
    get '/download_customer_checkout', to: 'users/sessions#download_customer_checkout', :as => :download_customer_checkout
    get '/download_customer_checkout_qcp', to: 'users/sessions#download_customer_checkout_qcp', :as => :download_customer_checkout_qcp
    post :login_verification, to: "users/sessions#login_verification"
    post :test_post, to: "users/sessions#test_post"
    post 'admins/merchant/reset', :to => 'users/passwords#from_admin_reset'
    get '/unlock_account', :to => 'users/passwords#unlock_account'
    get :user_from_email, to: "users/sessions#user_from_email"
    post :send_2factor_code, to: "users/sessions#send_2factor_code"
  end

  resources :oauth, :only => [] do
    collection do
      post :revoke
      post :token
      post 'token/retrieve', to:  "oauth#token"
    end
  end

  namespace :api do

    get 'merchant/accounts', to: "merchant_accounts#accounts"
    resources :checkout, only: [] do
      collection do
        post 'get_product_info'
        post 'get_transaction_info'
        post 'get_rtp_info'
        get 'get_invoice_rtp_info'
        get 'country_data'
        get 'invoice_checkout'
      end
    end

    post :affiliate_program , to: "affiliate_program#affiliate"
    post 'affiliate_program/deposit' , to: "affiliate_program#deposit"

    get :wallet, to: 'wallets#show_wallet'
    post :wallet, to: 'wallets#show_wallet'
    get :check_status, to: 'checks#get_check'
    resources :supports, only: [:create] do
      collection do
        post :update_block, as: "update_seq"
        post :update_block_import
      end
    end
    resources :checks, only: [:create]

    resources :releases, only: [:create]

    resources :accounts, :only => [:create] do
      collection do
        post 'delete'
        post :phone_api
      end
    end
    get :quickcard_status, to: "webhooks#quickcard_status", as: "quickcard_status"
    resources :webhooks, :only => [] do
      collection do
        post 'checkbook'
        post 'sengrid_email'
        post :easypost_hook
        post 'cbk_defense'
      end
    end

    resources :cards, :only => [:create] do
      collection do
        post :list
        post :delete
        post :charge
        post 'elavon/ach/transaction', to: 'cards#elavon_ach_transact'
      end
    end

    # resources :transactions, :only => [] do
    #   collection do
    #     kinds = %w|card account|
    #     post 'do/:kind' => 'transactions#create', constraints: {kind: Regexp.new(kinds.join('|'))}
    #   end
    # end

    resources :giftcards, only: [:create, :scan, :balance] do
      collection do
        post 'scan'
        post 'balance'
      end
    end

    resources :withdrawls, only: [:submit] do
      collection do
        post 'submit'
      end
    end

    namespace 'users' do

      resources :sessions, only: [:create] do
        collection do
          post :forget_password
          post :sign_out
        end
      end
      resources :sales, only: [:create] do
        collection do
          post :qr_scan
          post :search
          post :signature_sale
        end
      end

      resources :accounts, only: [:create, :delete] do
        collection do
          post 'delete'
        end
      end

      resources :transactions, only: [:webhook] do
        collection do
          post 'webhook'
        end
      end

      scope controller: 'tickets' do
        post 'create_ticket'
        post 'tickets'
        post 'ticket'
        post 'create_comment'
      end

      scope controller: 'settings' do
        post 'settings'
        post 'notifier'
        post 'update_settings'
      end

      scope controller: 'contacts' do
        post 'contacts'
        post 'contact_list'
        post 'wallet_details'
        post 'wallet_transactions_pages'
        post 'between_us_transactions_pages'
      end
    end

    resources :sessions, only: [:create, :sign_out, :destroy]

    namespace :v2 do
      resources :transactions, only: [:create] do
        collection do
          post :virtual
        end
      end
    end

    resources :registrations, only: [] do
      collection do
        post :virtual_terminal_transaction
        post :virtual_terminal_transaction_debit
        post :virtual_transaction
        post :virtual_transaction_card
        post :card_tokenization
      end
    end

    resources :auth_token do
      collection do
        post :validate
      end
    end

    resources :wallets, except: [:index, :show, :edit, :new, :update, :destroy ] do
      collection  do
        post :check_transaction_status
        post :transaction_last4
        post :check_transaction_reference
        get :issue, to: 'wallets#issue'
        post :issue, to: 'wallets#issue'
        get 'issue_with_stripe', to: 'wallets#issue_with_stripe'
        post 'issue_from_settings', to: 'wallets#issue_from_settings'
        post :issue_with_payeezy
        get :withdraw , to: 'wallets#withdraw'
        post :withdraw , to: 'wallets#withdraw'
        get :transaction_between, to: 'wallets#transaction'
        post :transaction_between, to: 'wallets#transaction'
        get :qr_generator, to: 'wallets#generator'
        post :qr_generator, to: 'wallets#generator'
        get :qr_scanner, to: 'wallets#scanner'
        post :qr_scanner, to: 'wallets#scanner'
        get :transactions_to , to: 'wallets#scanner_reverse'
        post :transactions_to , to: 'wallets#scanner_reverse'
        get :search

        post :search
        post 'issue_money'
        post 'transfer_amount'
        post 'pos_scanner', to: 'poss#pos_scanner'
        post 'generate_qr', to: "poss#generate_qr"
        post 'generate_qr_debit', to: "poss#generate_qr_debit"
        post :refund_money
        post :refund_payment_gateway
        post :virtual_debit_charge
        post :create_merchant_bank
        post :balance

        scope controller: 'cards' do
          post 'tango/catalogues', to: 'cards#catalogues'
          post 'tango/order/create', to: 'cards#order'
          post 'tango/orders', to: 'cards#orders'
          post 'tango/get_order', to: 'cards#get_order'
          post 'cards_list' , to: 'cards#list'
          post 'cards' , to: 'cards#create'
          post :delete_card , to: 'cards#delete'
          get :delete_card , to: 'cards#delete'
        end

        scope controller: 'requests' do
          get :approved_requests
          get :rejected_requests
          get :reject_request
          post :reject_request
          get :approve_request
          post :approve_request
          get :send_request, to: 'requests#request_money'
          post :send_request, to: 'requests#request_money'
          post :request_code
          post :approve_request_code
          post :approve_request_without_issue
          get :pending_requests
          get :cancel_request
          post :cancel_request
          get :requests
          post :requests
          get :scanner_request
          get :generator_request
        end

        get :show_wallet, to: 'wallets#show_wallet'
        post :show_balance_of_wallet
        get :get_token, to: 'wallets#generate_token'
        get :all_wallets, to: 'wallets#wallets'
      end
    end
    resources :notifications, only: [] do
      collection do
        post :list
        post :mark_as_read
        post :count
        post :archive
      end
    end
  end

  resources :accounts, only: [] do
    collection do
      get :dashboard
      get :deposite
      get :withdrawal
      get :money_transfer
      get :exchange
      get :history
      get :dispute
      get :requests
      get :vouchers
      get :support
      get :user_settings
    end
  end

  resources :admins do
    collection do
      scope module: :admins do
        resources :tickets, only: [:index, :edit, :close_ticket,:mail_support] do
          get :close_ticket
          collection do
            get :change_flag
            get :mail_support
            post :assign_to
          end
          resources :comments, only: [:create]
        end
        resources :locations, except: [:destroy] do
          post :in_valid_location
          get :block_transactions, on: :member
          get :get_gateways, on: :member
          get :new_bank
          post :create_bank
          get :delete_bank
          get :edit_bank
          patch :update_bank
          get :d_account_no

          collection do
            post :duplication
            get :next_baked_section
            get :next_ez_merchant_section
            get :next_profit_section
            post :export
            post :update_gateway
            get :location_add_category
            post :create_category
            get :location_add_ecom_platform
            post :create_ecom_platform
            get 'update_location_token/:id', to: "locations#update_location_token", :as => :update_location
            get 'transactions/:wallet_id', to:  "locations#transactions", :as => :wallet_transactions
            get 'merchant/:user_id', to: "locations#index", :as => :merchant_id
            get :document_delete
          end
        end

        resources :export_accounts do
          collection do
            get :export
            get :get_user_count
          end
        end

        resources :block_transactions, only: [:index] do
          collection do
            get :sync_transactions
            get :sync_status
            get :update_balances
            get :balance_status
          end
        end
        # resources :roles, only: [:edit, :update]
        resources :email_templates, only: [:index, :edit, :update]
        resources :agents, only: [:index, :new, :show, :create, :edit,:update]
        resources :affiliates, only: [:index, :new, :show, :create, :edit,:update]
        resources :isos, only: [:index, :new, :show, :create, :edit,:update] do
          member do
            get :details
          end
          collection do
            get :next_baked
            get :profile
            get :update_status
            get :update_two_step
            get :unlock
            post :unlock_post
            get :profile_wallet_details
            get :transfer_money
            get :specfic_transactions
            get :merchants
            get :agent_affiliates
            get :specfic_decline_transactions
            get :document_delete
            get :bank_account
            post :save_bank_account
            get :setting
            post :setting_post

          end
        end
        resources :partners, except: [:show]
        resources :companies do
          collection do
            get :form_validate
          end
        end
        resources :merchants
        resources :error_message
      end

      resources :images, only: [] do
        collection do
          post :upload_docs
          get :cancel_upload_doc
          delete :delete_image
        end
      end

      # resources :pos_merchants do
      #   collection do
      #     get :archived_pos
      #   end
      #   get :transactions, to: 'pos_merchants#transactions'
      # end

      resources :users, controller: "admins/users" do
        collection do
          get 'edit_archive/:id', to: 'admins/users#edit_archive', as: :edit_archive
          post 'edit_archive', to: 'admins/users#post_edit_archive', as: :post_edit_archive
          get :archived_users
          get :all_users
          get :search
          get :search_wallet_transactions
          get :ticket_details
          get :wallet_details

          # post :wallet_details_post
        end
        put :submit_user_main_information
        put :update_role
        get :all_transactions
        get :transactions
      end

      resources :wallets, controller: "admins/wallets", only: [:show, :retire_wallet, :retire_wallet_form] do
        post :retire_wallet
      end

      resources :app_configs, only: [:create, :show], controller: "admins/app_configs" do
        collection do
          post 'search_gateway', to: "admins/app_configs#merchant_modal"
          post 'update_location_gateways', to: "admins/app_configs#merchant_modal"
          post 'mid_disable', to: "admins/app_configs#mid_disable"
          get :atm_config
          get 'atm_config/:id' => 'admins/app_configs#update_atm_congif', as: :update_atm_config
          get :merchant_configs_logs
          get :merchant_app_configs
          get :load_balancer_configs
          get :merchant_modal
          get :merchant_modal_search
          get :update_payment_gateway
          post :update_payment_gateway
          post :update_graph_setting
          post :update_accounting_setting
          get  'merchantDetails/:id', :to => 'admins/app_configs#show_config_log_details', as: :merchantDetails
          get  'revertdetails/:id', :to => 'admins/app_configs#update_payment_gateway', as: :revertDetails

        end
      end

      resources :orders, only: [:index] do
        collection do
          get 'add_tracker', to: 'admins/orders#new'
          get 'tracker_detail', to: 'admins/orders#show'
          get 'cancel_shipment', to: 'admins/orders#cancel'
          post 'set_tracker', to: 'admins/orders#update'
        end
      end

      resources :transactions, only: [], controller: "admins/transactions" do
        collection do
          get :ach, :to => 'admins/transactions#ach'
          get :generate_export_file
          get :send_email_export_file
          post :local_transaction_details
          post :block_card
          get :export_download
          get :exported_files
          post :users_local_detail
        end
      end

      # post 'tos_approval' , to: 'merchant/locations#tos_approval', as: :tos_approval
      get :transactions, to: 'admins#transactions'
      get :verifications, as: :user_request_verification
      get :refund_check
      get :activity_log
      # get 'approve_status/:id', to:  'admins#approve_status' , as: :approve_status
      get :approve_status
      get 'reject_status/:id' , to: 'admins#reject_status' , as: :reject_status
      get :refund_transaction
      get :refund_failed_transaction
      get :get_partialAmount
    end
  end

  namespace :merchant do
    root :to => "transactions#index"
    post 'transactions', to: "transactions#index"
    post 'next_transactions', to: "transactions#next_transactions", as: :next_transactions
    post 'previous_transactions', to: "transactions#previouss_transactions", as: :previous_dashboard_wallet_transactions
    get :refund_check, :to => "transactions#refund_check"
    resources :base, only: [] do
      collection do
        get :valid_location
        get :verify_user
        get :verifying_user
        get :verifying_user_phone_email
        get :show_wallet_balance
      end
    end

    resources :reports, only: [:index] do
      collection do
        get :export_file
        get :checks_report
        get :send_money_report
        get :export_checks_report
        get :export_send_money_report
        get :close_batch_report
        get :export_close_batch_report
        get :my_reports
        get :gift_cards_report
        get :export_gift_cards_report
        get :get_wallets
      end
    end

    resources :apps do
      post :update_valid
    end

    resources :giftcards, only: [:show] do
      collection do
        get :merchant_orders
        get :giftcard_catalogues
        post :buy_card
      end
    end

    resources :checks do
      collection do
        post :create_check
        get :get_fee_sum
        get  :invoices
        get  :new_invoice
        get :check_form
        post :bulk_checks
        get :create_bulk_checks
        get :load_updated_checks
        get 'check_under_review', :to => 'checks#check_under_review'
        get :all_bulk_checks
        get 'show_bulk/:id' , to: 'checks#show_bulk' , as: :show_bulk
        post :resend_check
        # post :create_invoice
        post :policy
        post :direct_deposit
        get :debit_card_deposit
        get :delete_debit_card
        get :present_card_verification
        get :debit_card_deposit_form
        post :create_debit_card_deposit
        post :update_single_bulk_check
        get :update_check_form
        post :get_bank_by_routing_number
      end
      member do
        get :bulk_check_detail
      end
    end

    resources :disputes do
      collection do
        get :accept_dispute
        get :upload_evidence
        get :export
        post :submit_evidence
        get :delete_doc
      end
    end

    resources :transactions, only: [:index] do
      get :details_modal
      get :local_details_modal
      collection do
        post :transfer_amount
        get :verify_user
        get :verifying_user
        get :transfer
        post :pay_tip
        get :refund
        get :refund_merchant_transaction
        get :get_merchant_amount
        get :refund_check
        get :generate_export_file
        get :exported_files
        get :send_email_export_file
        # get :kill_job
        get :export_download
        get :update_email_status
      end
    end

    resources :invoices, only: [:index, :create, :new] do
      post :update_invoice_status
      get :status_modal
      get :detail_modal
      collection do
        get :inbox
        get :sent
        get :get_balance
      end
    end

    resources :sales, only: [:index] do
      collection do
        get :verify_email_and_phone_number
        get :card_number_verify
        get :country
        post :submit_virtual_terminal
        get :virtual_terminal
        post :close_batch
        post :wallet_details
        get :batch
        get :transactions
        post :scanner_sale
        get :verify_sale_limit
        # get :sale_requests
        # get :qr_codes
        # get :redeem_modal
      end
    end

    resources :locations, only: [:index, :show] do
      get :tip_modal
      collection do
        get :transactions
        post 'next_transactions', to: "locations#next_transactions", as: :next_wallet_transactions
        post 'previous_transactions', to: "locations#previouss_transactions", as: :previous_wallet_transactions
        get :transfers
        get 'show_merchant_balance', to: 'locations#show_merchant_balance', as: :show_merchant_balance
        get 'transfer_delete', to: 'locations#transfer_delete', as: :transfer_delete
        post 'transfer_post', to: 'locations#transfer_post', as: :transfer_post
        post 'transfer_update', to: 'locations#transfer_update', as: :transfer_update
        post 'tos_approval' , to: 'locations#tos_approval', as: :tos_approval
      end
    end

    resources :supports, only: [:index, :show] do
      collection do
        post :create_ticket
        post :comment
        get 'close_ticket/:id' , to: 'supports#close_ticket' , as: :close_ticket
        get 'open_ticket/:id' , to: 'supports#open_ticket' , as: :open_ticket
      end
    end

    resources :settings, except: [:create] do
      collection  do
        post :new_sub_merchant
        get :edit_user
        put :update_user
      end
    end
  end

  namespace :v2 do
    get :get_user, to: "merchant/base#get_user"
    get :welcome, to: "merchant/base#welcome"
    post 'tos_approval' , to: 'merchant/base#tos_approval'
    post 'send_tos' , to: 'merchant/base#send_tos'
    post 'update_password_tos' , to: 'merchant/base#update_password_tos'
    get :generate_export_file, to: 'merchant/base#generate_export_file'
    get 'activity_logs/:action_type', to: 'merchant/base#activity_logs'
    get 'update_notification/:id', to: 'merchant/base#update_notification'
    get 'hide_all_notification', to: 'merchant/base#hide_all_notification'
    get 'load_notifications' , to: "merchant/base#load_notifications"
    namespace :merchant do
      get 'plugins' , to: "plugins#index"
      get 'apps' , to: "apps#index"
      get 'location_fees' , to: "apps#location_fees"
      get 'location_fee' , to: "apps#location_fee"
      post 'update_revealed/:id' , to: "apps#update_revealed"
      resources :accounts, only: [:index, :show] do
        collection do
          get :get_accounts
          get :sales_batch
          get :get_ach
          get :get_p2c
          get :get_check
          get :get_giftcard
          get :graph_data
          get :show_merchant_balance
          post :account_transfer
          post :tip_transfer
          get :get_location_data
          get :get_giftcard_data
          get :reserve_transactions
          get :tip_transactions
          get :account_transactions
          post :create_ach
          get :transaction_detail
          post :maxmind_update
          post :create_check
          post :create_p2c
          get :present_card_verification
          get :tip_reserve_transaction_detail
          get :buy_giftcard
          get :giftcard_show
          post :buy_card
          get :get_help
          get :batch_transactions
          post :delete_p2c_card
          get :get_refund_fee
          post :refund_merchant_transaction
          get :transaction_event
        end
      end
      resources :profiles, except: [:create] do
        collection  do
          put :update_user
          post :update_pin
          post :update_profile_email_number
          get :update_two_step_verification
          get :employee_list
          get :faq
          get :privacy_policy
          get :tos
        end
      end

      resources :chargebacks, only: [:index] do
        collection do
          get :show_details
          get :verify_admin_password
          get :accept_dispute
          get :account_chargeback
          post :submit_evidence
        end
      end
      resources :settings do
        collection do
        end
      end
      resources :base, only: [] do
        collection do
          get :valid_location
          get :verify_user
          get :verifying_user
          get :verifying_user_phone_email
          get :show_wallet_balance
          post :accept_cookies
        end
      end

      resources :sales, only: [:index] do
        collection do
          get :country_data
          post :submit_virtual_terminal
          get :customer_search
          get :customer_address
          get :check_phone_number

        end
      end

      resources :hold_money, only: [:index] do
        collection do
          get :transactions
          get :reserve_money
          get :reserve_transactions
        end
      end
      resources :permission do
        collection do
          get :get_name
        end

      end

      resources :exports, only: [:index] do
        collection do
          get :export_download
        end
      end
      resources :invoices, only: [:index, :create, :new] do
        collection do
          post :update_invoice_status
          get :ach_payment
          get :quickcard_payment
          post :quickcard_payment_post
          get :get_balance
          get :payee
          get :cancel
          get :reopen
          get :edit
          get :view_invoice
          get :approve_invoice
          get :customer_search
          get :customer_address
          get :check_phone_number
          get :recurring_billing
          post :recurring_billing_post
          get :clients
          get :client_details
        end
      end

      resources :push_to_card do
        collection do
          get :debit_card_deposit_form
          get :location_data
          get :present_card_verification
          get :delete_debit_card
        end
      end
      resources :checks do
          collection do
              get :location_data
          end
      end
     resources :instant_ach do
          collection do
              get :location_data
          end
      end

      resources :bulk_checks do
          collection do
              post :import_checks
              get :load_updated_checks
              get :before_send_index
          end
      end
      
      resources :giftcards, only: [:index,:show] do
        collection do
          get :merchant_orders
          get :buy_giftcard
          post :buy_card
        end
      end

    end

    namespace :partner do
        get :welcome, to: "base#welcome_partner"
        post 'send_tos' , to: 'base#send_tos'
        post 'update_password_tos' , to: 'base#update_password_tos'
        post 'update_bank_details' , to: 'base#update_bank_details'
        post 'update_documents' , to: 'base#update_documents'
        post 'tos_approval' , to: 'base#tos_approval'
        get 'update_notification/:id', to: 'base#update_notification'
        get 'load_notifications' , to: "base#load_notifications"
        get 'hide_all_notification', to: 'base#hide_all_notification'
        get 'activity_logs/:action_type', to: 'base#activity_logs'
      resources :base, only: [] do
        collection do
          post :accept_cookies
          get :verify_user
          get :verifying_user
          get :verifying_user_phone_email
        end
      end

        resources :accounts, only: [:index, :show] do
            collection do
                get :get_ach
                get :get_p2c
                get :get_check
                get :get_giftcard
                get :get_help   
                get :sales_batch
                get :get_withdraw_data
                get :buy_giftcard
                get :giftcard_show
                get :batch_transactions
                post :create_ach
                post :create_check
                post :create_p2c
                get :graph_data
                get :merchant_graph_data
                post :buy_card
                get :merchant_list
                get :agent_list
                get :present_card_verification
                post :delete_p2c_card
            end
        end
        get :fee_structure , to: "settings#fee_structure"
        resources :instant_ach do
            collection do
              get :location_data
            end
        end

        resources :checks do
            collection do
                get :location_data
            end
        end

        resources :push_to_card do
            collection do
                get :debit_card_deposit_form
                get :location_data
                get :present_card_verification
                get :delete_debit_card
            end
        end

        resources :giftcards, only: [:index,:show] do
            collection do
                get :merchant_orders
                get :buy_giftcard
                post :buy_card
            end
        end

        resources :transactions , only: [:index , :show]

        resources :exports, only: [:index] do
          collection do
            get :export_download
          end
        end
        resources :invoices, only: [:index, :create, :new] do
            collection do
              post :update_invoice_status
              get :ach_payment
              get :quickcard_payment
              post :quickcard_payment_post
              get :get_balance
              get :payee
              get :cancel
              get :reopen
              get :edit
              get :view_invoice
              get :approve_invoice
              get :customer_search
              get :customer_address
              get :recurring_billing
              post :recurring_billing_post
              get :clients
              get :client_details
              get :check_phone_number
            end
        end

        resources :profiles, except: [:create] do
            collection  do
              put :update_user
              post :update_pin
              post :update_profile_email_number
              get :update_two_step_verification
              get :employee_list
              get :faq
              get :privacy_policy
              get :tos
            end
        end
        resources :sales, only: [:index] do
            collection do
              get :country_data
              post :submit_virtual_terminal
            end
          end
    end
  end

  # kinds = %w|profile logs verification security|
  # get 'settings/:kind' => 'settings#show', constraints: {kind: Regexp.new(kinds.join('|'))}
end
