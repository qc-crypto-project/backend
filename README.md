# Quickcard

## Key Technologies

- [Ruby](https://www.ruby-lang.org/en/)
- [Rails](https://rubyonrails.org/)
- [SideKiq](https://sidekiq.org/)
- [Sequence/Chain](https://chain.com/)
- [Heroku](https://heroku.com/)
- [DB - Postgresql](https://www.postgresql.org/)
- [Redis](https://redis.io/)

### Pre-requisites

- Ruby 2.4.1
- Rails 5.1.4
- Redis 4.0 or newer
- RubyMine IDE
- Bundler 1.16.1 or newer (bundle update)
- Sidekiq 5.1.3

## Running The App
1. set up .env file with environment variables (Get the `.env` file from a developer)
1. Setup your local db host settings in `config/database.yml` file
1. bundle install
To Run the app:
`rails s`

### <a name="stepByStep"></a>Step by step
1. Install RubyMine IDE (https://www.jetbrains.com/ruby/download)
1. Install git & git-gui packages
1. Clone This repo and open in RubyMine
1. Get sequence ledger key from Fahad/Yawar & add them to your `.env` file
1. Install rvm ([link to nvm instal instructions - go to installation](https://rvm.io/rvm/install)
1. Install ruby 2.4.1 using: rvm install 2.4.1
1. Install dependencies: `bundle install`
1. Install Postgresql latest version & Setup postgres root DB user for localhost connectivity
1. Add a database.yml file to `configs/database.yml` get it from a developer
1. Get the .env file from a developer (Make sure all env vars are in place & pointed to your local environment.)
1. Create, migrate & seed app db `rails db:create && rails db:migrate && rails db:seed`
1. `rails s` App will launch if there are any errors OR warnings resolve them before starting forward

- Before writing any actual code just in case refresh coding basics with [guide here](http://www.pragtob.info/rails-beginner-cheatsheet/RailsBeginnerCheatSheetSinglePage.pdf)

### Background app

We are using `sidekiq` as background queue manager in our app & redis is being used as a data store to handle queue operational data.
1. run redis-server on your local (install it if necessary). Point your .env variables to redis localhost

## App Coding Guide

- We should be following pure rails conventions while generating new models, controller, views & routes. See [Rails Conventions HERE](#railsConventions)

### Running in Heroku environment
To run Web app:
1. Install Heroku CLI
1. run from command line
`heroku local web`

To run Worker role:
`heroku local worker`

### Tests

Currently, there are no auto testing environment in place to run tests. You can add them separately for your own tasks & add them to deploy & server start scripts.
 
### Debugging

### Adding New Merchant
1. Go to http://quickcard.herokuapp.com
2. Go to Merchants
3. press New Merchant
4. Input required data and make sure to give it a unique name, number, email & required details
5. Login with Merchant details you got in your email to Merchant Portal

## Code Style Guide

All new code should be written in [Ruby Standard Guidelines](https://github.com/github/rubocop-github/blob/master/STYLEGUIDE.md). The style should generally follow the [Robocop Ruby style guide](https://github.com/rubocop-hq/ruby-style-guide). There may be some differences however. You should familiarise yourself with ruby default conventions.

### General Overview
- Indentation MUST use 2 spaces
- Variables MUST be camelCased
- Functions MUST be camelCased
- Use PascalCase for classes and enums
- Control structures MUST have space after control words and before opening curly braces. In addition, opening curly braces MUST be on the same line as the control structure.

  **Good**

  ```
  if (someCondition)
     ...code...
  elsif (someOtherCondition) {
  	 ...do something
  else
     ...do something
  }

  while (someCondition) {
      ...do stuff
  }

  Array.each{|element| .... }

  ```
  **Bad**

  ```
  if(someCondition)
   ...
   else
   ...
  end
  ```
- Lib should be postfixed with `Lib`. Examples: `PaymentGatewayLib`, `CardLib`, etc...
- Use whole words for symbol names such as variables, functions, classes, interfaces, etc... Avoid using types in variable names.

  **Good names**

  ```
  testImage = nil
  localFile
  listPoints = []
  ```

  **Bad names**

  ```
  workoutImg
  aFile
  graphPts = []
  graphPointsArray = []
  ```
- If a Line Of Code is too long to fit in one line, all properties should be on their own line with the closing brace having the same indentation as the opening brace:

  **Good**

  ```
  MyMethod(
    someProp={123}
    anotherProp={'abc'}
  )
  ```

  **Bad**

  ```
  MyMethod( someProp={123}
    anotherProp={'abc'}
    )
  ```

- Curly Brace Spacing
  - For object literals, there MUST be space between the opening and closing curly braces:

    **Good**

    ```
    objectlIteral = { key: 'value', boolKey: true }
    ```

    **Bad**

    ```
    objectLiteral = {key: 'value', boolKey: true}
    ```
### Adding Dependencies

Dependencies should be added via gem packages & added to the project's `Gemfile` file. Try to only add most active & good rating gem packages. If its needed for some important task discuss with TeamLead for suggestions.

### Using Dependencies

Add dependencies with `require` OR `include` snippets to files where needed, don't add them to be loaded in the whole app.

## JS/CSS Guide

- Styles/JS should be in the css corresponding file of controller OR even possible action.
- Style/JS names should be camelCased and descriptive of what they are for.
- Styles/JS that are reused across many components should be placed in the `layout.scss` file.
- Style/JS names should be abstract and not specific to what's in the style

  **Bad**

  ```
  blackText: {
      color: '#000000'
  },
  width50: {
      width: '50%'
  }
  ```
  **Good**

  ```
  darkText: {
      color: '#000000'
  },
  button: {
      width: '50%'
  }
  ```


# <a name="railsConventions"></a>Rails naming conventions #

## General Ruby conventions ##

Class names are `CamelCase`.

Methods and variables are `snake_case`.

Methods with a `?` suffix will return a boolean.

Methods with a `!` suffix mean one of two things: either the method operates destructively in some fashion, or it will raise and exception instead of failing (such as Rails models' `#save!` vs. `#save`).

In documentation, `::method_name` denotes a *class method*, while `#method_name` denotes a *instance method*.

## Database ##

*Database tables* use `snake_case`. Table names are **plural**.

*Column names* in the database use `snake_case`, but are generally **singular**.

Example:

```
+--------------------------+
| bigfoot_sightings        |
+------------+-------------+
| id         | ID          |
| sighted_at | DATETIME    |
| location   | STRING      |
| profile_id | FOREIGN KEY |
+------------+-------------+

+------------------------------+
| profiles                     |
+---------------------+--------+
| id                  | ID     |
| name                | STRING |
| years_of_experience | INT    |
+---------------------+--------+
```

## Model ##

Model *class names* use `CamelCase`. These are **singular**, and will map automatically to the plural database table name.

Model *attributes* and *methods* use `snake_case` and match the column names in the database.

Model files go in `app/models/#{singular_model_name}.rb`.

Example:

```ruby
# app/models/bigfoot_sighting.rb
class BigfootSighting < ActiveRecord::Base
  # This class will have these attributes: id, sighted_at, location
end
```
```ruby
# app/models/profile.rb
class Profile < ActiveRecord::Base
  # Methods follow the same conventions as attributes
  def veteran_hunter?
    years_of_experience > 2
  end
end
```

### Relations in models ###

Relations use `snake_case` and follow the type of relation, so `has_one` and `belongs_to` are **singular** while `has_many` is **plural**.

Rails expects foreign keys in the database to have an `_id` suffix, and will map relations to those keys automatically if the names line up.

Example:

```ruby
# app/models/bigfoot_sighting.rb
class BigfootSighting < ActiveRecord::Base
  # This knows to use the profile_id field in the database
  belongs_to :profile
end
```
```ruby
# app/models/profile.rb
class Profile < ActiveRecord::Base
  # This knows to look at the BigfootSighting class and find the foreign key in that table
  has_many :bigfoot_sightings
end
```

## Controllers ##

Controller *class names* use `CamelCase` and have `Controller` as a suffix. The `Controller` suffix is always singular. The name of the resource is usually **plural**.

Controller *actions* use `snake_case` and usually match the standard route names Rails defines (`index`, `show`, `new`, `create`, `edit`, `update`, `delete`).

Controller files go in `app/controllers/#{resource_name}_controller.rb`.

Example:

```ruby
# app/controllers/bigfoot_sightings_controller.rb
BigfootSightingsController < ApplicationController
  def index
    # ...
  end
  def show
    # ...
  end
  # etc
end
```

```ruby
# app/controllers/profiles_controller.rb
ProfilesController < ApplicationController
  def show
    # ...
  end
  # etc
end
```

## Routes ##

Route names are `snake_case`, and usually match the controller. Most of the time routes are **plural** and use the plural `resources`.

[Singular routes](http://edgeguides.rubyonrails.org/routing.html#singular-resources) are a special case. These use the singular `resource` and a singular resource name. However, they still map to a plural controller by default!

Example:

```ruby
resources :bigfoot_sightings
# Users can only see their own profiles, so we'll use `/profile` instead
# of putting an id in the URL.
resource :profile
```

## Views ##

View file names, by default, match the controller and action that they are tied to.

Views go in `app/views/#{resource_name}/#{action_name}.html.erb`.

Examples:

 * `app/views/bigfoot_sightings/index.html.erb`
 * `app/views/bigfoot_sightings/show.html.erb`
 * `app/views/profile/show.html.erb`
 
## Deployment ##

This project uses capistrano for auto-deployments, Get updated `deploy.rb` & `Capfile` from your Project Manager to setup for CAP Auto Deployment.

Deployment server username on VM is: `deploy`

Setup your `deploy/#{ENV}.rb` file with server ips have tunnel access through jump server.

Jump server username is: `jumpp` IP aren't fix so get ips from your project manager.

# More resources #

* [ActiveRecord naming and schema conventions (including magic column names)](http://edgeguides.rubyonrails.org/active_record_basics.html#naming-conventions)
* [Mind map of Rails conventions](https://teddicodes.files.wordpress.com/2015/02/railsnamingconventions.pdf)

