namespace :user_profile do
  desc "Update Iso,Agent,Affiliate Profiles"
  task user_profile_update: :environment do
    puts 'Updating Profiles'
    begin
      id = nil
      job = CronJob.new(name: 'user_profile:user_profile_update', description: 'Update User Profile', success: true)
      result = {updates: [],  error: nil}
      @user=User.where(role: ["iso","agent","affiliate"])
      @user.each do |user|
        id=user.id
        if user.present?
          if !user.profile.present?
            if user.company_name.present?
              Profile.create(company_name: user.company_name ,years_in_business: 0,ein: 0000,tax_id: 0000,street_address: 'n/a',city:'n/a',state:'n/a',zip_code:'0',user_id: user.id)
            else
              Profile.create(company_name: 'n/a',years_in_business: 0,ein: 0000,tax_id: 0000,street_address: 'n/a',city:'n/a',state:'n/a',zip_code:'0',user_id: user.id)
            end
            result[:updates].push(id)
            puts "Updated Successfully"
          end
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Profile Update: ', exc
    end
    job.result = result
    job.user_profile_task!
    puts 'User Profiles Updated Successfully!'
  end
  # --------------User Profile Update Task Revert--------------------
  task revert_user_profile_update: :environment do
    puts 'Reverting Updated User Profile Update'
    jobs = CronJob.user_profile_task.where(success: true, name: 'user_profile:user_profile_update')
    jobs.each do |job|
      if job["error"].nil? && job.result["updates"].present?
        begin
          job.result["updates"].each do |user_record|
            user_update=User.find(user_record)
            id=user_record
            if user_update.present?
              if user_update.profile.present?
                user_update.profile.delete
                puts "Reverting User Profile For #{id}"
              end
            end
          end
        rescue StandardError => exc
          puts 'Error occured during User Profile Update: ', exc
        end
      end
    end
  end
end
