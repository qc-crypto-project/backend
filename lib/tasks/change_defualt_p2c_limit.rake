namespace :update_db do
  desc "Change defautl value of p2c limit and value"
  task p2c_limit: :environment do
  #User.iso.system_fee.update_all()
    puts "Updating Iso"
    users = User.iso + User.agent + User.affiliate
    users_updated = users.each do |i|
      system_fee = i.system_fee
      if system_fee.present? && system_fee != "{}"
        system_fee["push_to_card_limit"] = "2000" if system_fee.try(:[],"push_to_card_limit").blank?
        system_fee["push_to_card_limit_type"] = "Day" if system_fee.try(:[],"push_to_card_limit_type").blank?
        i.system_fee = system_fee
      end
    end
    User.import users_updated, on_duplicate_key_update: {conflict_target: [:id], columns: [:system_fee]}
    puts "Updated Successfully"
    end
end
