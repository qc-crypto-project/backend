namespace :update_db do
  desc "Change default value of Check Withdraw limit"
  task check_withdraw_limit: :environment do
    puts "Updating Check Withdraw Limit"
    users = User.iso
    check_withdraw_limit_updated = users.each do |i|
      system_fee = i.system_fee
      if system_fee.present? && system_fee != "{}"
        system_fee["check_withdraw_limit"] = "100" if system_fee.try(:[],"check_withdraw_limit").blank?
        i.system_fee = system_fee
      end
    end
    User.import check_withdraw_limit_updated, on_duplicate_key_update: {conflict_target: [:id], columns: [:system_fee]}
    puts "Updated Successfully"
  end
end
