# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# # include ApplicationHelper

# ADDING DEFAULT ROLES
  p "-------------ADDING DEFAULT ROLES--------------"
  ['admin', 'user', 'atm', 'gift-card', 'merchant','app','qc','iso','partner', 'agent', 'affiliate', 'support'].each do |role|
    if Role.find_by_title(role).nil?
      p "Creating #{role.upcase} role..."
      Role.create!(:title => role, :charge_percentage => "0")
    end
  end
  p "-------------DEFAULT ROLES ADDED--------------"
# END: ADDING DEFAULT ROLES

# ADDING DEFAULT MERCHANT USER PERMISSION
p "-------------ADDING DEFAULT MERCHANT USER PERMISSION--------------"
  if Permission.find_by_permission_type("admin").nil?
    p "Creating #{"admin".upcase} Permission..."
    Permission.create!( name: "admin (Full Access)", permission_type: "admin", wallet: true, refund: true, transfer: true, b2b: true, virtual_terminal: true, dispute_view_only: true, dispute_submit_evidence: true, accept_dispute: true, funding_schedule: true, check: true, push_to_card: true, ach: true, gift_card: true, sales_report: true, checks_report: true, gift_card_report: true, user_view_only: true, user_edit: true, user_add: true, developer_app: true, view_accounts: true, account_transfer: true, tip_transfer: true, withdrawal: true, export_daily_batch: true, view_chargeback_cases: true, fight_chargeback: true, accept_chargeback: true, view_invoice:true, create_invoice: true, cancel_invoice: true, customer_list: true, approve_mr_transaction: true, refund_mr_transaction: true, export_list: true, permission_view_only: true, permission_edit: true, permission_add: true, api_key: true, plugin: true, fee_structure: true, help: true, check_view_only: true, check_add: true, check_void: true, push_to_card_view_only: true, push_to_card_add: true, push_to_card_void: true, ach_view_only: true, ach_add: true,  ach_void: true, gift_card_view_only: true, gift_card_add: true, gift_card_void: true, export_account: true, export_withdrawal: true, export_chargeback: true, export_invoice: true, export_funding_schedule: true, export_business_settings: true, accounts: true, withdrawals: true, e_checks: true, push_to_cards: true, achs: true, giftcards: true, invoices: true, exports: true, business_settings: true, employee_list: true, permission_list: true , risk_analysis: true, chargebacks: true, all: true )
  end
  if Permission.find_by_permission_type("regular").nil?
    p "Creating #{"regular".upcase} Permission..."
    Permission.create!( name: "regular (VT Only)", permission_type: "regular",virtual_terminal: true )
  end
p "-------------DEFAULT MERCHANT USER PERMISSION ADDED--------------"
# END: ADDING DEFAULT ROLES

# ADD APP CONFIGS
  p "-------------ADDING App Configs--------------"
  configs = [
      AppConfig::Key::PaymentProcessor,
      AppConfig::Key::ATMPaymentProcessor,
      AppConfig::Fee::UserPaypalCardFee,
      AppConfig::Fee::UserGiftCardFee,
      AppConfig::Fee::UserVisaCardFee,
      AppConfig::Fee::UserSpendingLimit,
      AppConfig::Fee::DepositCard,
      AppConfig::Fee::UserCheckFee,
      AppConfig::Key::MerchantApiKey,
      AppConfig::Fee::UserDepositLimit,
      AppConfig::Key::GiftCardConfig,
      AppConfig::Key::APIsAuthorization,
      AppConfig::Key::MidDisable,
      AppConfig::Key::InstantDailyLimit,
      AppConfig::Key::InstantPerTxLimit,
      AppConfig::Key::IssueDisputeConfig
  ]
  configs.each do |key|
    if AppConfig.where(:key => key).count <= 0
      AppConfig.create!(:key => key)
    else
      p "#{key} already exists in DB. We are good..."
    end
  end

  app = AppConfig.all
  notpresnt = app.select{|v| v[:key] == "atm_payment_processor"}
  if notpresnt.empty?
    AppConfig.create(key: "atm_payment_processor",stringValue: "stripe", boolValue: true)
  end
  seq_key = app.select{|v| v[:key] == "sequence_down"}
  if seq_key.blank?
    AppConfig.create(key: "sequence_down", boolValue: true)
  end
# END: ADD APP CONFIGS

p ".............Error Codes Creation Start..... "

error_details = I18n.t("quickcard")
error_details.keys.each do |error|
  error = error_details[error]
  if ErrorMessage.where(error_code: error[:error_code]).first.blank?
    p "Creating  #{ error[:error_code] } ..."
    ErrorMessage.create(error_code: error[:error_code], error_message: error[:error_message], error_reason: error[:error_reason])
  else
    p "Error Code  #{ error[:error_code] } is already exist in DB."
  end
end

p ".............Error Codes Successfully Ended..... "

# Configure LEDGER FLAVOR KEY & ASSET
p "-------------CONFIGURING LEDGER DETAILS-------------" # USE ONLY FIRST APP START
  ledger_configured = false
  if ledger_configured == false
    begin
      ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential:  ENV['LEDGER_CREDENTIAL'] )
      currency = 'usd'
      if ledger.present?
        p "LEDGER initialised..."
      end
      flavors = ledger.flavors.list(
          filter: 'tags.type=$1',
          filter_params: ['currency']
      ).map do |flavor|
        flavor.id
      end
      unless flavors.include? currency
        key_1 = ledger.keys.create(id: currency)
        if key_1.present?
          p "FLAVOR KEY created..."
        end
        asset = ledger.flavors.create(id: currency, key_ids: [currency], tags: {"type": "currency"})
        if asset.present?
        end
      end
      ledger_configured = true
    rescue Sequence::APIError => e
      action_error = JSON.parse(e.response.body)
      if action_error['seq_code'] == 'SEQ002'
        p "Invalid Ledger CREDENTIALS OR Name, LEDGER CONFIGS NOT DONE!"
      else
        p "Sequence API Failed with error code: #{action_error['seq_code']} message: #{action_error['message']}"
      end
    end
  end
  if ledger_configured == true
    ledger_configured = true
    p "=== LEDGER CONFIGURATIONS DONE ==="
    # ADD DEFAULT USERS...
      if ledger_configured
        p "-------------ADDING DEFAULT ADMIN USER--------------"
        u = User.where(:email => 'admin@quickcard.me').first
        if u.nil?
          u = User.new(name: "Quickcard Admin", email: "admin@quickcard.me", password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019', postal_address: 'California', phone_number: '090078612', is_block: false)
          u.admin!
        end

        p "-------------ADDING DEFAULT QC Support USER--------------"
        if User.where(:name => 'QC Support', :email => 'support@quickcard.me').first.nil?
          User.create(name: "QC Support", email: "support@quickcard.me",password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019', phone_number: '00078601', is_block: false)
        end

        p "-------------ADDING DEFAULT Escrow Support USER--------------"
        if User.where(:name => 'Escrow Support', :email => 'escrow@quickcard.me').first.nil?
          User.create(name: "Escrow Support", email: "escrow@quickcard.me",password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019', phone_number: '111111', is_block: false)
        end

        p "-------------ADDING DEFAULT GIFTCARD USER--------------"
        if User.where(:name => 'Gift Cards', :email => 'giftcards@quickcard.me').first.nil?
          User.create(name: "Gift Cards", email: "giftcards@quickcard.me",password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019', phone_number: '090078601' ,is_block: false)
        end

        p "-------------CREATING DEFAULT ESCROW WALLET--------------"
        if Wallet.where(:wallet_type => 5, :name => 'Escrow Wallet').first.nil?
          Wallet.create(wallet_type:5, name:"Escrow Wallet")
        end

        p "-------------ADDING DEFAULT CHARGEBACK WALLET & USER--------------"
        if User.where(:name => 'CBK Wallet', :email => 'chargeback@quickcard.me').first.nil?
          user = User.new(name: "CBK Wallet", email: "chargeback@quickcard.me",password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019', phone_number: '12345678910', is_block: false)
          if user.user!
            wallet = user.wallets.new(wallet_type: 6, name:"CBK Wallet")
            wallet.save!
          end
        end

        p "----------Adding default support user"
        if User.where(email: "support@Greenboxpos.com").first.blank?
          user = User.new(name: "greenbox support", email: "support@Greenboxpos.com", phone_number: "09876543210", password: "Qcsupport-2019", password_confirmation:"Qcsupport-2019", is_block: false)
          user.support!
        end
        # NOT USING ADDS for now in the system...
          # if Add.where(:user_id => u.id).first.nil?
          #   p "-------------SETTING UP ADS STRUCTURE--------------"
          #   Add.create(user_id: u.id)
          # end
      else
        p "LEDGER not configured properly first configure that then you can add default users..."
        return
      end
    # END: ADD DEFAULT USERS...

    # Running just as a precaution...

    # UPDATE ref_no for all users
      p "-------------UPDATING REF NO. for all users just in case--------------"
      users = User.all
      users.each do |u|
        id = u.id
        if u.user?
          u.update(ref_no: "C-#{id}")
        elsif u.agent?
          u.update(ref_no: "A-#{id}")
        elsif u.partner?
          u.update(ref_no: "CP-#{id}")
        elsif u.iso?
          u.update(ref_no: "ISO-#{id}")
        elsif u.merchant?
          u.update(ref_no: "M-#{id}")
        end
      end
    # END: UPDATE ref_no for all users
  else
    p "LEDGER not configured properly first configure that then you can add default users..."
    return
  end
# END: Configure LEDGER FLAVOR KEY & ASSET

p "DATABASE SEEDED, You can proceed now..."
# THESE ARE ONE-TIME SCRIPTS DON'T BOTHER...
