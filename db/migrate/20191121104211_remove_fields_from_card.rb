class RemoveFieldsFromCard < ActiveRecord::Migration[5.2]
  def change
    remove_column :cards, :address_id
    remove_column :cards, :default
  end
end
