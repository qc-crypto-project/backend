class AddCountsToBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :iso_count, :integer, default: 0
    add_column :transaction_batches, :agent_count, :integer, default: 0
    add_column :transaction_batches, :affiliate_count, :integer, default: 0
  end
end
