class AddProcessingTypeToLocations < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :processing_type, :string, default: 'retail'
  end
end
