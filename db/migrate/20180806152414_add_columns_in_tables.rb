class AddColumnsInTables < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :years_in_business, :string
    add_column :profiles, :tax_id, :string
    add_column :users, :owner_name, :string
  end
end
