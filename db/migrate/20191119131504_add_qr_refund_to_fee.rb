class AddQrRefundToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :qr_refund, :float
  end
end
