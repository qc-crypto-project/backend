class AddTypeAndSubTypeToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :type, :string
    add_column :transactions, :sub_type, :string
  end
end
