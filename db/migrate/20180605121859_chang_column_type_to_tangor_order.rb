class ChangColumnTypeToTangorOrder < ActiveRecord::Migration[5.1]
  def change
    rename_column :tango_orders , :type , :order_type
  end
end
