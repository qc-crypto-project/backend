class IndexForeignKeysInWallets < ActiveRecord::Migration[5.1]
  def change
    add_index :wallets, :location_id
  end
end
