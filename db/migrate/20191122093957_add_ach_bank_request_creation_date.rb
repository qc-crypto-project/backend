class AddAchBankRequestCreationDate < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :ach_bank_request_date, :datetime
  end
end
