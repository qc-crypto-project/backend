class AddBlockIpToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :block_ip, :boolean, default: false
  end
end
