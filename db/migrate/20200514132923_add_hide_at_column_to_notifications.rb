class AddHideAtColumnToNotifications < ActiveRecord::Migration[5.2]
  def change
  	add_column :notifications , :hide_at , :datetime
  end
end
