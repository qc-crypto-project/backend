class AddIsSlotToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :is_slot, :boolean, default: false
  end
end
