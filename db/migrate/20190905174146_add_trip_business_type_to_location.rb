class AddTripBusinessTypeToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :trip_business_type, :string
    add_column :locations, :ach_type, :string
  end
end
