class AddDateOfBirthToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :date_of_birth, :string
    add_column :users, :issue_date, :string
    add_column :users, :expire_date, :string

  end
end
