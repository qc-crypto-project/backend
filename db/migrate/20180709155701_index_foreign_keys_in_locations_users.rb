class IndexForeignKeysInLocationsUsers < ActiveRecord::Migration[5.1]
  def change
    add_index :locations_users, :location_id
    add_index :locations_users, :user_id
  end
end
