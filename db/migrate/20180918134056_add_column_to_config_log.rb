class AddColumnToConfigLog < ActiveRecord::Migration[5.1]
  def change
    add_column :config_logs, :total_merchants, :integer
  end
end
