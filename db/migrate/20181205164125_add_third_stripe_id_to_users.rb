class AddThirdStripeIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :third_stripe_id, :string
  end
end
