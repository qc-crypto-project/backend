class AddReserveToTransactionBatch < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :reserve, :float, default: 0
  end
end
