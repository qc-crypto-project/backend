class IndexForeignKeysInOauthApps < ActiveRecord::Migration[5.1]
  def change
    add_index :oauth_apps, :location_id
  end
end
