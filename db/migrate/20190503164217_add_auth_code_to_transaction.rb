class AddAuthCodeToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :auth_code, :string
  end
end
