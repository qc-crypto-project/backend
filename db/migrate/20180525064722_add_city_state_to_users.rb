class AddCityStateToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :city ,:string
    add_column :users, :state ,:string
  end
  def down
    remove_column :users, :city ,:string
    remove_column :users, :state ,:string
  end
end
