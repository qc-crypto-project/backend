class AddResultsToCronJob < ActiveRecord::Migration[5.1]
  def up
    add_column :cron_jobs, :result, :jsonb
    add_column :cron_jobs, :success, :boolean
  end

  def down
    remove_column :cron_jobs, :results, :jsonb
  end
end
