class AddRequestIdToAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :request_id, :integer
    add_column :addresses, :suit, :string
  end
end
