class AddAttentionToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :merchant_attention, :boolean, default: false
  end
end
