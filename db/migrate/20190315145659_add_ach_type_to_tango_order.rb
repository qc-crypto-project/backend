class AddAchTypeToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :ach_type, :string
  end
end
