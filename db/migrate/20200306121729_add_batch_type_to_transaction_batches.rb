class AddBatchTypeToTransactionBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :batch_type, :string

  end
end
