class AddCsNumberToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :cs_number, :string
  end
end
