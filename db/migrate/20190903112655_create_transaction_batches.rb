class CreateTransactionBatches < ActiveRecord::Migration[5.1]
  def change
    create_table :transaction_batches do |t|
      t.integer :merchant_id
      t.integer :payment_gateway_id
      t.integer :location_id
      t.integer :merchant_wallet_id

      t.timestamps
    end
    add_index :transaction_batches, :merchant_id
    add_index :transaction_batches, :payment_gateway_id
    add_index :transaction_batches, :location_id
    add_index :transaction_batches, :merchant_wallet_id
  end
end
