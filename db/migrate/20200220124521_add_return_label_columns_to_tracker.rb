class AddReturnLabelColumnsToTracker < ActiveRecord::Migration[5.2]
  def change
  	add_column :trackers , :is_return , :boolean
  	add_column :trackers , :return_tracking_id , :string
  	add_column :trackers , :return_tracking_code , :string
  	add_column :trackers , :return_tracking_details , :jsonb 
  	add_column :trackers , :return_label_url , :string
  	add_column :trackers , :return_carrier , :string
  	add_column :trackers , :return_easypost_ids , :jsonb
  	add_column :trackers , :return_status , :string
  	add_column :trackers , :return_shipment_date , :datetime
  	add_column :trackers , :return_delivered_date , :datetime
  	add_column :trackers , :return_est_delivery_date , :datetime
  end
end