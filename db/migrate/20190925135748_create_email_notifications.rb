class CreateEmailNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :email_notifications do |t|
      t.string :subject
      t.text :body
      t.datetime :scdedule_date
      t.string :sender
      t.string :search_option

      t.timestamps
    end
  end
end
