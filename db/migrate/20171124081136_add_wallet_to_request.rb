class AddWalletToRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :requests, :wallet_id, :string
  end
end
