class AddsTipColumnToQrcards < ActiveRecord::Migration[5.1]
  def up
    add_column :qr_cards , :tip , :float
  end
  def down
    remove_column :qr_cards , :tip , :float
  end
end
