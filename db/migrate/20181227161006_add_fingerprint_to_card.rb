class AddFingerprintToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :fingerprint, :string
  end
end
