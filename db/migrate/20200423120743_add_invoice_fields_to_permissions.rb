class AddInvoiceFieldsToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :view_invoice, :boolean, default: false
    add_column :permissions, :create_invoice, :boolean, default: false
    add_column :permissions, :cancel_invoice, :boolean, default: false
  end
end
