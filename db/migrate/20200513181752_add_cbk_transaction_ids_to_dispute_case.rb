class AddCbkTransactionIdsToDisputeCase < ActiveRecord::Migration[5.2]
  def change
    add_column :dispute_cases,:dispute_result_transaction_id,:integer
  end
end
