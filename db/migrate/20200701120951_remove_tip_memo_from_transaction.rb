class RemoveTipMemoFromTransaction < ActiveRecord::Migration[5.2]
  def change
    remove_column :transactions, :tip_memo if column_exists? :transactions, :tip_memo
  end
end
