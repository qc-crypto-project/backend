class AddRefundLogToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :refund_log, :jsonb
  end
end
