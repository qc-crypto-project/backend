class AddWithdrawalFieldsToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :check_view_only, :boolean, default: false
    add_column :permissions, :check_add, :boolean, default: false
    add_column :permissions, :check_void, :boolean, default: false
    add_column :permissions, :push_to_card_view_only, :boolean, default: false
    add_column :permissions, :push_to_card_add, :boolean, default: false
    add_column :permissions, :push_to_card_void, :boolean, default: false
    add_column :permissions, :ach_view_only, :boolean, default: false
    add_column :permissions, :ach_add, :boolean, default: false
    add_column :permissions, :ach_void, :boolean, default: false
    add_column :permissions, :gift_card_view_only, :boolean, default: false
    add_column :permissions, :gift_card_add, :boolean, default: false
    add_column :permissions, :gift_card_void, :boolean, default: false
  end
end
