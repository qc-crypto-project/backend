class AddColumnsToFeeToFees < ActiveRecord::Migration[5.1]
  def up
    add_column :fees , :buy_qc_card_cash , :float
    add_column :fees , :buy_qc_card_cc , :float
    add_column :fees , :atm_processing_fee , :float
    add_column :fees ,:atm_id , :integer
  end
  def down
    remove_column :fees , :buy_qc_card_cash , :float
    remove_column :fees , :buy_qc_card_cc , :float
    remove_column :fees , :atm_processing_fee , :float
    remove_column :fees , :atm_id , :integer
  end
end
