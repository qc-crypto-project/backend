class CreateEcommPlatforms < ActiveRecord::Migration[5.1]
  def change
    create_table :ecomm_platforms do |t|
      t.string :name

      t.timestamps
    end
  end
end
