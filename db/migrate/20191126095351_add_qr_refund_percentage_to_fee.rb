class AddQrRefundPercentageToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :qr_refund_percentage, :float
  end
end
