class AddLocationIdToAddress < ActiveRecord::Migration[5.1]
  def up
    add_column :addresses , :location_id , :integer
  end
  def down
    remove_column :addresses , :location_id , :integer
  end
end
