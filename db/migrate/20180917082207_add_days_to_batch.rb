class AddDaysToBatch < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :days, :integer, default: 0
  end
end
