class AddCcNumberTransactionAttemptsToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :cc_number_transaction_attempts, :jsonb
  end
end
