class IndexForeignKeysInUsersWallets < ActiveRecord::Migration[5.1]
  def change
    add_index :users_wallets, :user_id
    add_index :users_wallets, :wallet_id
  end
end
