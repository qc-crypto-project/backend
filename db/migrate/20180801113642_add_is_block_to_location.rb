class AddIsBlockToLocation < ActiveRecord::Migration[5.1]
  def up
    add_column :locations , :is_block, :boolean , default: false
  end
  def down
    remove_column :locations , :is_block , :boolean
  end
end
