class AddProductFieldsInTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :billing_id, :integer
    add_column :transactions, :shipping_id, :integer
    add_column :transactions, :delivery_status, :string
    add_column :transactions, :redirect_url, :string
    add_column :block_transactions, :transaction_id, :integer
  end
end
