class ChangeTypesOfEnumInBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    change_column :block_transactions, :main_type, :string
    change_column :block_transactions, :sub_type, :string
    change_column :block_transactions, :api_type, :string
  end
end
