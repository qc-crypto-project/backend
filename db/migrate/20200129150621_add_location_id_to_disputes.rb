class AddLocationIdToDisputes < ActiveRecord::Migration[5.2]
  def change
    add_column :customer_disputes, :location_id, :integer
  end
end
