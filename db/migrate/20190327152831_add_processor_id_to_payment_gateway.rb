class AddProcessorIdToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :processor_id, :text
  end
end
