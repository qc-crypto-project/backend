class CreateMiscFees < ActiveRecord::Migration[5.1]
  def change
    create_table :misc_fees do |t|
    	t.string :name
    	t.float		:dollar_fee
    	t.float		:percentage_fee
    	t.integer		:misc_type
    	t.references :payment_gateway
      t.timestamps
    end
  end
end
