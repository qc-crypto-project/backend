class AddSenderFieldToQrCard < ActiveRecord::Migration[5.1]
  def change
    add_column :qr_cards, :from_id, :integer
  end
end
