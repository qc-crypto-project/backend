class AddGroupTransactionAttemptsToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :group_transaction_attempts, :jsonb
    add_column :cards, :decline_transaction_attempts, :jsonb
    add_column :cards, :blocked_by_load_balancer, :boolean, default: false

  end
end
