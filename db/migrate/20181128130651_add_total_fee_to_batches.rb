class AddTotalFeeToBatches < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :total_fee, :float, default: 0
  end
end
