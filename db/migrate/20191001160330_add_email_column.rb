class AddEmailColumn < ActiveRecord::Migration[5.1]
  def change
    add_column :cron_jobs, :email, :boolean, default: false
  end
end
