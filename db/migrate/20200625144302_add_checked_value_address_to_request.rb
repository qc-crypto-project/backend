class AddCheckedValueAddressToRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :requests, :checked_value_address, :boolean , default: false
  end
end
