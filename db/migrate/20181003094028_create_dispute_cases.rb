class CreateDisputeCases < ActiveRecord::Migration[5.1]
  def change
    create_table :dispute_cases do |t|
      t.string :case_number, :defailt => ""
      t.references :user, foreign_key: true
      t.references :transaction, foreign_key: true

      t.timestamps
    end
  end
end
