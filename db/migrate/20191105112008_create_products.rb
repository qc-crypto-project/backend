class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.float :amount
      t.integer :quantity
      t.integer :transaction_id
      t.has_attached_file :image
      t.timestamps
    end
  end
end
