class AddProfitSplitToLocation < ActiveRecord::Migration[5.1]
  def up
    add_column :locations, :profit_split, :boolean, default: false
  end

  def down
    remove_column :locations, :profit_split, :boolean, default: false
  end
end
