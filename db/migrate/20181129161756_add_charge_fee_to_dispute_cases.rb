class AddChargeFeeToDisputeCases < ActiveRecord::Migration[5.1]
  def change
    add_column :dispute_cases, :charge_fee, :boolean, default: true
  end
end
