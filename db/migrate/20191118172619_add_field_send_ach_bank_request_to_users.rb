class AddFieldSendAchBankRequestToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :send_ach_bank_request, :boolean, :default => false
  end
end
