class ChangeCheckNumberType < ActiveRecord::Migration[5.1]
  def change
    change_column :tango_orders, :number, :string
  end
end
