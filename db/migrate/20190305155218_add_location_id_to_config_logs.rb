class AddLocationIdToConfigLogs < ActiveRecord::Migration[5.1]
  def change
    add_column :config_logs, :location_id, :integer
    add_column :config_log_details, :location_id, :integer
  end
end
