class AddColumnsToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
  	add_column :payment_gateways, :cbk_max_percent, :float
  	add_column :payment_gateways, :cbk_max_count, :integer
  	add_column :payment_gateways, :average_ticket, :float
  	add_column :payment_gateways, :high_ticket, :float
  	add_column :payment_gateways, :daily_limit, :float
  	add_column :payment_gateways, :monthly_limit, :float
  end
end
