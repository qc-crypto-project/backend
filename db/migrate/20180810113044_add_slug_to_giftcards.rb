class AddSlugToGiftcards < ActiveRecord::Migration[5.1]
  def change
    add_column :giftcards, :slug, :string
    add_index :giftcards, :slug, unique: true
  end
end
