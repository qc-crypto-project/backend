class AddTaxColumnToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :tax, :float
  end
end
