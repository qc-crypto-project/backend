class AddSystemFeeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :system_fee, :jsonb, null: false, default: '{}'
  end
end
