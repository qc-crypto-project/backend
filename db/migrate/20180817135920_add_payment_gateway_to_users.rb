class AddPaymentGatewayToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :payment_gateway, :integer
  end
end
