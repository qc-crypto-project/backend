class AddExportColToAchGateways < ActiveRecord::Migration[5.1]
  def change
    add_column :ach_gateways, :export_col, :jsonb
  end
end
