class AddTransactionIdToQrCard < ActiveRecord::Migration[5.1]
  def change
    add_column :qr_cards, :transaction_id, :integer
  end
end
