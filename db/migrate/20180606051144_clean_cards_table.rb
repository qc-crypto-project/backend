class CleanCardsTable < ActiveRecord::Migration[5.1]
  def change
    remove_column :cards, :token, :string
    remove_column :cards, :exp_month, :string
    remove_column :cards, :exp_year, :string
    remove_column :cards, :elavon_token, :string
  end
end
