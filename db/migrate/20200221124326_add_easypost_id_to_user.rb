class AddEasypostIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :easypost_id, :jsonb
  end
end
