class AddWalletIdToChecks < ActiveRecord::Migration[5.1]
  def up
    add_column :checks , :wallet_id , :integer
  end
  def down
    remove_column :checks , :wallet_id , :integer
  end
end
