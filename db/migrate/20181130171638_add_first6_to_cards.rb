class AddFirst6ToCards < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :first6, :integer
  end
end
