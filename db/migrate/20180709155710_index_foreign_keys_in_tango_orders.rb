class IndexForeignKeysInTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_index :tango_orders, :wallet_id
  end
end
