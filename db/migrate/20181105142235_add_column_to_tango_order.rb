class AddColumnToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :card_number, :string
    add_column :tango_orders, :expiry_date, :date
    add_column :tango_orders, :zip_code, :string
    add_column :tango_orders, :cvv, :string
    add_column :tango_orders, :address_line_1, :string
    add_column :tango_orders, :address_line_2, :string
    add_column :tango_orders, :account_type, :string
  end
end
