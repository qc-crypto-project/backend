class AddTwoStepVerficationToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :two_step_verification, :boolean, default: false
  end
end
