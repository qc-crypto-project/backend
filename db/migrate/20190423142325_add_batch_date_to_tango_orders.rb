class AddBatchDateToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :batch_date, :datetime
  end
end
