class AddCardSubTypeToOrderBanks < ActiveRecord::Migration[5.1]
  def change
    add_column :order_banks, :card_sub_type, :string
  end
end
