class AddVirtualTxToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :virtual_tx, :boolean
  end
end
