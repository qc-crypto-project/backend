class AddClerkIdToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :clerk_id, :string
  end
end
