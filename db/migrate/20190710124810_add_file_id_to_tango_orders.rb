class AddFileIdToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :shape_file_id, :integer, default: false unless TangoOrder.column_names.include?('shape_file_id')
  end
end