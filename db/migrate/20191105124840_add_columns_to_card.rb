class AddColumnsToCard < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :address_id, :integer
    add_column :cards, :default, :boolean, default: false
  end
end
