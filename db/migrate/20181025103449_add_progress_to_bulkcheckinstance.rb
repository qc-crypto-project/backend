class AddProgressToBulkcheckinstance < ActiveRecord::Migration[5.1]
  def change
    add_column :bulk_check_instances, :checks_processed, :boolean, default: false
  end
end
