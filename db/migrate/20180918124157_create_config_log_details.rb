class CreateConfigLogDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :config_log_details do |t|
      t.references :user, foreign_key: true
      t.references :config_log, foreign_key: true

      t.timestamps
    end
  end
end
