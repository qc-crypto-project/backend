class AddBatchIdToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :transaction_batch_id, :integer
  end
end
