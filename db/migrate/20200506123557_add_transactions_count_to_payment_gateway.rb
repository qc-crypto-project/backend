class AddTransactionsCountToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :transaction_count, :integer, default: 0
  end
end
