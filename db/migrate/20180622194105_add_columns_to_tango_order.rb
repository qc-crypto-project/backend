class AddColumnsToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :actual_amount, :float
    add_column :tango_orders, :check_fee, :float
    add_column :tango_orders, :fee_perc, :float
  end
end
