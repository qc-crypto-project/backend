class AddAchBuyrateToFees < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :ach_fee, :float
    add_column :fees, :ach_fee_dollar, :float
  end
end
