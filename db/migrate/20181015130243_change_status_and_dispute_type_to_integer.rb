class ChangeStatusAndDisputeTypeToInteger < ActiveRecord::Migration[5.1]
  def up
  	remove_column :dispute_cases, :status
  	remove_column :dispute_cases, :dispute_type
  	add_column :dispute_cases,:status, :integer, :default => 0
  	add_column :dispute_cases,:dispute_type, :integer, :default => 0
  end
  def down
  	remove_column :dispute_cases, :status
  	remove_column :dispute_cases, :dispute_type
  	add_column :dispute_cases,:status, :string, :default => ""
  	add_column :dispute_cases,:dispute_type, :string, :default => ""
  end
end
