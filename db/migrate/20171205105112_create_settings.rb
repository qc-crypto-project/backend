class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.boolean :push_notification, default: false
      t.boolean :touch_id_login, default: false
      t.boolean :touch_id_transaction, default: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
