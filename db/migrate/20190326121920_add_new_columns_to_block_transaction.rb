class AddNewColumnsToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :iso_fee, :float, default: 0
    add_column :block_transactions, :agent_fee, :float, default: 0
    add_column :block_transactions, :affiliate_fee, :float, default: 0
    add_column :block_transactions, :partner_fee, :float, default: 0
    add_column :block_transactions, :gbox_fee, :float, default: 0
  end
end
