class ChangeColumnNamesInTransactionBatch < ActiveRecord::Migration[5.2]
  def change
    rename_column :transaction_batches, :total_fee_in_cents, :total_fee
    rename_column :transaction_batches, :total_main_amount, :total_amount
    rename_column :transaction_batches, :total_net_amount, :total_net
    change_column :transaction_batches, :batch_date, :date
    change_column :transaction_batches, :total_trxs, :integer
  end
end
