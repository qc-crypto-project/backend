class AddVipCardToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :vip_card, :boolean, default: false
    add_column :cards, :is_vip, :boolean, default: false
  end
end
