class AddRequestTypeToRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :requests, :request_type, :string
  end
end
