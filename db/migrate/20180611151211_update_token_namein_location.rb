class UpdateTokenNameinLocation < ActiveRecord::Migration[5.1]
  def change
    rename_column :locations , :merchant_secure_token , :location_secure_token
  end
end
