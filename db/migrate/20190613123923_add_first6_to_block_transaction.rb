class AddFirst6ToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :first6, :string
  end
end
