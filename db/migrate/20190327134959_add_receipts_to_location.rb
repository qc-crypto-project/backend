class AddReceiptsToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :email_receipt, :boolean, default: true
    add_column :locations, :sms_receipt, :boolean, default: true
  end
end
