class AddArchivedToCompanies < ActiveRecord::Migration[5.1]
  def up
    add_column :companies, :archived, :boolean ,default: false
  end
  def down
    add_column :companies, :archived, :boolean
  end
end
