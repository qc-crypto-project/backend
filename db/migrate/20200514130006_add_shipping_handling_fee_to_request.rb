class AddShippingHandlingFeeToRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :requests, :shipping_handling_fee, :float
    add_column :requests, :shipping_handling_method, :string, default: "dollar"
    add_column :requests, :apply_tax, :boolean, default: false
    unless column_exists? :transactions, :shipping_amount
      add_column :transactions, :shipping_amount, :float
    end
  end
end
