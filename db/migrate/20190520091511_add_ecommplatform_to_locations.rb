class AddEcommplatformToLocations < ActiveRecord::Migration[5.1]
  def change
    add_reference :locations, :ecomm_platform, foreign_key: true
  end
end
