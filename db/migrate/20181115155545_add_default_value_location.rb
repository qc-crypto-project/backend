class AddDefaultValueLocation < ActiveRecord::Migration[5.1]
  def change
    change_column :locations, :sales, :boolean, default: false
    change_column :locations, :virtual_terminal, :boolean, default: false
    change_column :locations, :close_batch, :boolean,  default: false
    change_column :locations, :disable_sales_transaction, :boolean, default: false
  end
end
