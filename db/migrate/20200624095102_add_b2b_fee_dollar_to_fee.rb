class AddB2bFeeDollarToFee < ActiveRecord::Migration[5.2]
  def change
    unless column_exists? :fees, :b2b_fee_dollar
      add_column :fees, :b2b_fee_dollar, :float, default: 0.0
    end
  end
end
