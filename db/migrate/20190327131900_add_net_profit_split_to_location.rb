class AddNetProfitSplitToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :net_profit_split, :boolean, default: false
  end
end
