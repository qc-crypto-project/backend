class CreateNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :notes do |t|
      t.string :name
      t.references :notable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
