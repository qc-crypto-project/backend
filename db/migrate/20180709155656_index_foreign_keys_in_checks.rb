class IndexForeignKeysInChecks < ActiveRecord::Migration[5.1]
  def change
    add_index :checks, :user_id
    add_index :checks, :wallet_id
  end
end
