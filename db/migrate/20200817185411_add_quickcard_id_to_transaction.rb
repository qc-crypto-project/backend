class AddQuickcardIdToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :quickcard_id, :string
    add_index :transactions, :quickcard_id, unique: true
  end
end
