class AddColumnsInBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :terminal_id, :string
    add_column :block_transactions, :ref_id, :string
    add_column :block_transactions, :clerk_id, :string
  end
end
