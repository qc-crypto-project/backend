class AddIndexForMainTypeOnBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_index :block_transactions, :main_type
    add_index :block_transactions, :parent_id
    add_index :block_transactions, :sequence_id
  end
end
