class AddAllowDecimalToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :allow_decimal, :boolean, default: false
  end
end
