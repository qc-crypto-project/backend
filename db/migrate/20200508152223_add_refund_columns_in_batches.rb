class AddRefundColumnsInBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :refund_amount, :float, default: 0
    add_column :transaction_batches, :refund_count, :integer, default: 0
  end
end
