class AddDescriptionColumnToRequests < ActiveRecord::Migration[5.1]
  def up
    add_column :requests, :description, :text
  end
  def down
    remove_column :requests, :description, :text
  end
end
