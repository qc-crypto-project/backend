class AddBlockedByLoadBalancerToPaymentGateways < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :blocked_by_load_balancer, :boolean, default: false
  end
end
