class CreateHoldInRears < ActiveRecord::Migration[5.1]
  def change
    create_table :hold_in_rears do |t|
      t.integer :status
      t.date :release_date
      t.float :amount, default: 0.0
      t.integer :location_id

      t.timestamps
    end
    add_index :hold_in_rears, :location_id
  end
end
