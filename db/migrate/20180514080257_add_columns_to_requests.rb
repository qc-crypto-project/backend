class AddColumnsToRequests < ActiveRecord::Migration[5.1]
  def up
    add_column :requests ,:sender_access_token, :string
    add_column :requests, :pin_code, :string
  end

  def down
    remove_column :requests ,:sender_access_token, :string
    remove_column :requests, :pin_code, :string
  end
end
