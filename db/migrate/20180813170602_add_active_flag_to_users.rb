class AddActiveFlagToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :active_flag, :boolean, default: true
  end
end
