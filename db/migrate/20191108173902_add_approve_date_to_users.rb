class AddApproveDateToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :approve_date, :datetime
  end
end
