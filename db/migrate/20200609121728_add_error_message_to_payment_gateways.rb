class AddErrorMessageToPaymentGateways < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :error_message, :jsonb
  end
end
