class AddRefundReasonToTransactions < ActiveRecord::Migration[5.1]
  def change
      add_column :transactions, :refund_reason, :string, :default => ""
  end
end
