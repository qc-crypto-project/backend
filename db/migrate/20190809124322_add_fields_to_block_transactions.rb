class AddFieldsToBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :category_id, :integer
    add_column :block_transactions, :payment_gateway_id, :integer
    add_column :block_transactions, :location_id, :integer
  end
end
