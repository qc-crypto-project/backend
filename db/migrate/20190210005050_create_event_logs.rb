class CreateEventLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :event_logs do |t|
      t.text :message
      t.belongs_to :transaction, index: true
      t.timestamps
    end
  end
end
