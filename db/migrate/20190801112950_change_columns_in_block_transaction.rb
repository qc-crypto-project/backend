class ChangeColumnsInBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :position, :string
    remove_column :block_transactions, :new_type
    remove_column :block_transactions, :refunded
    remove_column :block_transactions, :refund_reason
    remove_column :block_transactions, :refund_log
  end
end
