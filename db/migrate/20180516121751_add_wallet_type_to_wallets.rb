class AddWalletTypeToWallets < ActiveRecord::Migration[5.1]
  def up
    add_column :wallets, :wallet_type, :integer
  end
  def down
    remove_column :wallets, :wallet_type, :integer
  end
end
