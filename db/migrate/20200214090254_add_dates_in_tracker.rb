class AddDatesInTracker < ActiveRecord::Migration[5.2]
  def change
    add_column :trackers, :shipment_date, :datetime
    add_column :trackers, :delivered_date, :datetime
  end
end
