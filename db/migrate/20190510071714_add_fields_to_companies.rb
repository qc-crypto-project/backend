class AddFieldsToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :years_in_business, :string
    add_column :companies, :tax_id, :string
  end
end
