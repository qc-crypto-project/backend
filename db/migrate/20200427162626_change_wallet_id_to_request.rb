class ChangeWalletIdToRequest < ActiveRecord::Migration[5.2]
  def change
    change_column :requests, :wallet_id, 'integer USING CAST(wallet_id AS integer)'
  end
end
