class AddDisableSaleButtonToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :disable_sales_transaction, :BOOLEAN
  end
end
