class AddBatchDateToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    column_exists?(:tango_orders, :batch_date, :datetime)
  end
end
