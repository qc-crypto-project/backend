class AddSplitsToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :splits, :jsonb
  end
end
