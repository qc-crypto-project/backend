class CreateConfigLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :config_logs do |t|
      t.integer :user_id
      t.string :to
      t.string :from

      t.timestamps
    end
  end
end
