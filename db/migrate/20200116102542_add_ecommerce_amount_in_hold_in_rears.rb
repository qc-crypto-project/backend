class AddEcommerceAmountInHoldInRears < ActiveRecord::Migration[5.2]
  def change
    add_column :hold_in_rears, :ecommerce_amount,:float,default: 0.0
  end
end
