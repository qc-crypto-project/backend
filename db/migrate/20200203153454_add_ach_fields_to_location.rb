class AddAchFieldsToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :ach_flag_text, :string
    add_column :locations, :ach_flag, :boolean
  end
end
