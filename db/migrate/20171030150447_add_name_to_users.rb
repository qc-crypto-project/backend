class AddNameToUsers < ActiveRecord::Migration[5.1]
  def change
 	add_column :users, :name, :string
    add_column :users, :postal_address, :string
    add_column :users, :phone_number, :numeric
    add_column :users, :status, :boolean 
  end
end
