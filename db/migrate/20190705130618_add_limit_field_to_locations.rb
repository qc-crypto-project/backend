class AddLimitFieldToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :ach_limit, :integer, :default => 0
    add_column :locations, :push_to_card_limit, :integer, :default => 2000
    add_column :locations, :check_limit, :integer, :default => 0
    add_column :locations, :ach_limit_type, :string
    add_column :locations, :push_to_card_limit_type, :string
    add_column :locations, :check_limit_type, :string

  end
end
