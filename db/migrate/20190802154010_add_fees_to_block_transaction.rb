class AddFeesToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    # add_column :block_transactions, :misc_fee, :float, default: 0
    # add_column :block_transactions, :retrieval_fee, :float, default: 0
    # add_column :block_transactions, :chargeback_fee, :float, default: 0
    # add_column :block_transactions, :misc_fee_dollars, :float, default: 0
    # add_column :block_transactions, :reserve_money_fee, :float, default: 0
    # add_column :block_transactions, :monthly_service_fee, :float, default: 0
    # add_column :block_transactions, :per_transaction_fee, :float, default: 0
    # add_column :block_transactions, :transaction_fee, :float, default: 0
    # add_column :block_transactions, :account_processing_limit, :float, default: 0
    # add_column :block_transactions, :reserve_money_days, :integer, default: 0
    add_column :block_transactions, :seq_block_id, :string
  end
end
