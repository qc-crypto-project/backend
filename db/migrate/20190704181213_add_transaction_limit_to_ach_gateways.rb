class AddTransactionLimitToAchGateways < ActiveRecord::Migration[5.1]
  def change
    add_column :ach_gateways, :transaction_limit, :float , default: 0.0
  end
end
