class AddAdminUserIdToReport < ActiveRecord::Migration[5.1]
  def change
    add_column :reports, :admin_user_id, :string
  end
end
