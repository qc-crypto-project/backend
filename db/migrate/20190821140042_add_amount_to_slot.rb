class AddAmountToSlot < ActiveRecord::Migration[5.1]
  def change
    add_column :slots, :amount, :string
    add_column :slots, :slot_under, :jsonb
    add_column :slots, :slot_over, :jsonb
  end
end
