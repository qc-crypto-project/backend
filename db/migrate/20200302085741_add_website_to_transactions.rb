class AddWebsiteToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :website, :string
  end
end
