class AddOauthAppIdToWallets < ActiveRecord::Migration[5.1]
  def change
    add_reference :wallets, :oauth_app, index: true
  end
end
