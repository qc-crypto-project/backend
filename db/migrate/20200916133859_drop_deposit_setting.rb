class DropDepositSetting < ActiveRecord::Migration[5.2]
  def change
    drop_table(:deposit_settings, if_exists: true)
  end
end
