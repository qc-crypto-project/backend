class AddDescriptionToEvidence < ActiveRecord::Migration[5.1]
  def change
    add_column :evidences, :description, :string
  end
end
