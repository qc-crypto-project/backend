class AddMoneyCheckToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :add_money_check, :string
  end
end
