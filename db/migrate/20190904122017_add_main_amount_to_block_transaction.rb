class AddMainAmountToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :main_amount, :float
  end
end
