class CreateOauthApps < ActiveRecord::Migration[5.1]
  def change
    create_table :oauth_apps do |t|
      t.string :name
      t.text :description
      t.string :website
      t.string :tos
      t.string :privacy_url
      t.string :redirect_uri

      t.boolean :is_block, default: false

      t.string :key
      t.string :secret
      t.string :token
      t.string :code

      t.string :organization_name
      t.string :organization_web

      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
