class AddFieldToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :void_transaction_id, :integer
    add_index :tango_orders, :void_transaction_id
  end
end
