class AddCountsToPaymentGateways < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :cc_number_count, :integer, default: 0
    add_column :payment_gateways, :amount_count, :integer, default: 0
    add_column :payment_gateways, :cc_brand_count, :integer, default: 0
  end
end
