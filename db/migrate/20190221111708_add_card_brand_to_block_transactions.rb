class AddCardBrandToBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :card_brand, :string
    add_column :block_transactions, :card_type, :string
  end
end
