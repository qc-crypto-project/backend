class AddVersionToAssets < ActiveRecord::Migration[5.2]
  def change
    add_column :assets, :version, :string, default: "1.0.0"
  end
end
