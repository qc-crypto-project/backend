class AddIndexesOnColumns < ActiveRecord::Migration[5.2]
  def change
    add_index :transactions, :status
    add_index :transactions, :created_at
    add_index :transactions, [:status, :main_type]
  end
end
