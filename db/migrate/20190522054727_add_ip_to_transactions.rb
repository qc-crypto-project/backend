class AddIpToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :ip, :string
  end
end
