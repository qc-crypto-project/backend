class AddEmailNotificationToImage < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :email_notification_id, :integer
  end
end
