class AddArchivedToNotifications < ActiveRecord::Migration[5.1]
  def up
    add_column :notifications, :archived, :boolean, default: false
  end

  def down
    remove_column :notifications, :archived, :boolean, default: false
  end
end
