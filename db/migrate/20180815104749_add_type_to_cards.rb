class AddTypeToCards < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :type, :string
  end
end
