class AddReadToReport < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :read, :boolean
    add_column :reports, :name, :string
  end
end
