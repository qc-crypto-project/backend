class AddRefIdToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :ref_id, :string
  end
end
