class AddNewColumnsToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :service, :float, default: 0
    add_column :fees, :statement, :float, default: 0
    add_column :fees, :misc, :float, default: 0
  end
end
