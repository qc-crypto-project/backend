class AddAchInternationalToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :ach_international, :boolean, default: false
  end
end
