class AddRiskToFee < ActiveRecord::Migration[5.1]
  def up
    add_column :fees, :risk, :integer
  end

  def down
    remove_column :fees, :risk, :integer
  end
end
