class AddTosAcceptanceDateToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :tos_acceptance_date, :datetime
  end
end
