class AddIndexForEmailOnUsers < ActiveRecord::Migration[5.1]
  def change
    add_index :users, :email
    add_index :users, :authentication_token
    add_index :users, [:phone_number, :archived]
    add_index :users, :ref_no
  end
end
