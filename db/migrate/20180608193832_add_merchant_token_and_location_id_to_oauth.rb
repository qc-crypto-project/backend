class AddMerchantTokenAndLocationIdToOauth < ActiveRecord::Migration[5.1]
  def up
     add_column :oauth_apps , :location_id ,:integer
     add_column :locations , :merchant_secure_token ,:string
  end
  def down
      remove_column :oauth_apps , :location_id ,:integer
      remove_column :locations , :merchant_secure_token ,:string
  end
end
