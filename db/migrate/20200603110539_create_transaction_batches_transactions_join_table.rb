class CreateTransactionBatchesTransactionsJoinTable < ActiveRecord::Migration[5.2]
  def change

    create_table :transaction_batches_transactions do |t|
      t.integer :transaction_batch_id
      t.integer :transaction_id
    end
    add_index :transaction_batches_transactions, :transaction_batch_id
    add_index :transaction_batches_transactions, :transaction_id
    # create_join_table :transaction_batches, :transactions do |t|
    #   t.index :transaction_id
    #   t.index :transaction_batch_id
    # end
  end
end
