class AddFuildPayToUserCard < ActiveRecord::Migration[5.1]
  def up
  	add_column :cards, :fluid_pay, :string, :default => ""
  	add_column :users, :fluid_pay_customer_id, :string, :default => ""
  end
  def down
 	remove_column :cards, :fluid_pay
  	remove_column :users, :fluid_pay_customer_id
   end
end
