class AddDenominationsFirldsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :denominations, :text, default:  "{1 => 0, 2 => 0, 5 => 0, 10 => 0, 20 => 0, 50 => 0, 100 => 0}"
  end
end
