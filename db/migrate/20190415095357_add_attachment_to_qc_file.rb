class AddAttachmentToQcFile < ActiveRecord::Migration[5.1]
  def change
    add_attachment :qc_files, :image
    remove_column :qc_files, :file_name
    remove_column :qc_files, :file_url
  end
end
