class AddRequestIdToQrCard < ActiveRecord::Migration[5.1]
  def change
    add_column :qr_cards, :request_id, :integer
  end
end
