class AddAddImageProcessingToImages < ActiveRecord::Migration[5.2]
  def change
    add_column :images, :add_image_processing, :boolean
  end
end
