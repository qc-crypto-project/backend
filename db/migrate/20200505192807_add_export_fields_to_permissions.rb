class AddExportFieldsToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :export_account, :boolean, default: false
    add_column :permissions, :export_withdrawal, :bool, default: false
    add_column :permissions, :export_chargeback, :boolean, default: false
    add_column :permissions, :export_invoice, :boolean, default: false
    add_column :permissions, :export_funding_schedule, :boolean, default: false
    add_column :permissions, :export_business_settings, :boolean, default: false
  end
end
