class AddUserIdToPermission < ActiveRecord::Migration[5.1]
  def change
    add_column :permissions, :merchant_id, :integer
  end
end
