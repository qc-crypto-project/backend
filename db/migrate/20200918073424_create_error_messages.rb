class CreateErrorMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :error_messages do |t|
      t.integer :error_code,  unique: true
      t.string :error_message
      t.string :error_reason
      t.string :error_description
      t.timestamps
    end
  end
end
