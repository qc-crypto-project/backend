class AddRtpToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :rtp_fee, :float
    add_column :fees, :rtp_fee_perc, :float
  end
end
