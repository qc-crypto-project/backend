class AddAffiliateToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :affiliate_allowed, :boolean, default: false
  end
end
