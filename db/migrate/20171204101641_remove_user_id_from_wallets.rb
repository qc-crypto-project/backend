class RemoveUserIdFromWallets < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :wallets, :users

    add_foreign_key :wallets, :users, on_delete: :cascade
  end
end
