class AddCatalogImageToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :catalog_image, :string
  end
end
