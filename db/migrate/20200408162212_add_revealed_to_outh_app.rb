class AddRevealedToOuthApp < ActiveRecord::Migration[5.2]
  def change
  	add_column :oauth_apps, :revealed_by, :text
  end
end
