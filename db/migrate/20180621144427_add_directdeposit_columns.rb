class AddDirectdepositColumns < ActiveRecord::Migration[5.1]

  def up
    add_column :tango_orders , :account_number ,:integer
    add_column :tango_orders , :routing_number ,:integer
  end

  def down
    remove_column :tango_orders , :account_number ,:integer
    remove_column :tango_orders , :routing_number ,:integer
  end

end
