class CreateOwners < ActiveRecord::Migration[5.1]
  def change
    create_table :owners do |t|
      t.string  :name
      t.string  :ownership
      t.string  :address
      t.string  :city
      t.string  :state
      t.string  :zip_code
      t.string  :phone_number
      t.string  :ssn
      t.string  :date_of_birth
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
