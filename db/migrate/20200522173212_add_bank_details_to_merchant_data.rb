class AddBankDetailsToMerchantData < ActiveRecord::Migration[5.2]
  def change
    add_column :merchant_data, :bank_details, :jsonb, default: {}
  end
end
