class AddChhecksRelatedColumnsToTnagoOrders < ActiveRecord::Migration[5.1]
  def up
    add_column :tango_orders ,:checkId , :string
    add_column :tango_orders ,:description, :text
    add_column :tango_orders ,:direction, :string
    add_column :tango_orders ,:name,:string
    add_column :tango_orders ,:number, :integer
    add_column :tango_orders ,:recipient, :string
    add_column :tango_orders ,:sender,:string
    add_column :tango_orders ,:status,:string
    add_column :tango_orders ,:wallet_id, :integer
  end

  def down
    remove_column :tango_orders ,:checkId , :string
    remove_column :tango_orders ,:description , :text
    remove_column :tango_orders ,:direction, :string
    remove_column :tango_orders ,:name ,:string
    remove_column :tango_orders ,:number, :integer
    remove_column :tango_orders ,:recipient, :string
    remove_column :tango_orders ,:sender ,:string
    remove_column :tango_orders ,:status,:string
    remove_column :tango_orders ,:wallet_id, :integer
  end
end
