class AddImageIdInSupport < ActiveRecord::Migration[5.1]
  def up
    add_column :images , :ticket_id , :integer
  end
  def down
    remove_column :images , :ticket_id , :integer
  end
end
