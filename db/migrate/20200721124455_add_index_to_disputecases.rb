class AddIndexToDisputecases < ActiveRecord::Migration[5.2]
  def change
    add_index :dispute_cases, :case_number
    add_index :dispute_cases, :created_at
  end
end
