class AddMaxTransactionToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :sale_limit, :float
    add_column :locations, :sale_limit_percentage, :float
  end
end
