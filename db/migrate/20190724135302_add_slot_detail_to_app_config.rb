class AddSlotDetailToAppConfig < ActiveRecord::Migration[5.1]
  def change
    add_column :app_configs, :slot_detail, :json
  end
end
