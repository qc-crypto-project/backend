class AddRefundedByToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :refunded_by, :integer unless Transaction.column_names.include?('refunded_by')
  end
end
