class ChangeLimitTypeToFloat < ActiveRecord::Migration[5.1]
  def self.up
   	change_column :locations, :ach_limit, :float, :default => 0
   	change_column :locations, :push_to_card_limit, :float, :default => 2000
   	change_column :locations, :check_limit, :float, :default => 0
  end
  def self.down
   	change_column :locations, :ach_limit, :integer, :default => 0
   	change_column :locations, :push_to_card_limit, :integer, :default => 2000
   	change_column :locations, :check_limit, :integer, :default => 0
  end
end
