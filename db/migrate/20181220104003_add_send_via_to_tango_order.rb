class AddSendViaToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :send_via, :string
  end
end
