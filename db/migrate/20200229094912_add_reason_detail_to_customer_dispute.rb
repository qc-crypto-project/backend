class AddReasonDetailToCustomerDispute < ActiveRecord::Migration[5.2]
  def change
    add_column :customer_disputes, :reason_detail, :string
  end
end
