class AddShippingInformationToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :shipping_first_name, :string
    add_column :users, :shipping_last_name, :string
    add_column :users, :shipping_company, :string
    add_column :users, :shipping_address, :string
    add_column :users, :shipping_city, :string
    add_column :users, :shipping_state, :string
    add_column :users, :shipping_country, :string
    add_column :users, :shipping_zip_code, :string
  end
end
