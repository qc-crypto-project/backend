class IndexForeignKeysInFees < ActiveRecord::Migration[5.1]
  def change
    add_index :fees, :location_id
  end
end
