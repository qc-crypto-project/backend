class AddReadAtDisputeCase < ActiveRecord::Migration[5.1]
  def change
    add_column :dispute_cases, :read_at, :date
  end
end
