class AddReserveDaysToHoldInRears < ActiveRecord::Migration[5.2]
  def change
    add_column :hold_in_rears,:reserve_amount,:float , default: 0.0
    add_column :hold_in_rears,:reserve_release,:datetime

  end
end
