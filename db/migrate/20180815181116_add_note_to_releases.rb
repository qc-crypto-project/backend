class AddNoteToReleases < ActiveRecord::Migration[5.1]
  def change
    add_column :releases, :note, :string
  end
end
