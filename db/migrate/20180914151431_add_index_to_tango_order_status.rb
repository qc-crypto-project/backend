class AddIndexToTangoOrderStatus < ActiveRecord::Migration[5.1]
  def change
    add_index :tango_orders, :status
  end
end
