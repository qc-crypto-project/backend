class AddLocationToTangorder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :location_id, :integer unless TangoOrder.column_names.include?('location_id')
  end
end
