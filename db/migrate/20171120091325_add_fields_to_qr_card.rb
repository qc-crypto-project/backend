class AddFieldsToQrCard < ActiveRecord::Migration[5.1]
  def change
    add_reference :qr_cards, :wallet, foreign_key: true
    add_column :qr_cards, :token, :string
  end
end
