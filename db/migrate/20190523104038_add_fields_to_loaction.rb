class AddFieldsToLoaction < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :low_ticket, :float
    add_column :locations, :average_ticket, :float
    add_column :locations, :high_ticket, :float
    add_column :locations, :monthly_processing_volume, :float
    add_column :locations, :over_high, :float
    add_column :locations, :extra_over, :float
  end
end
