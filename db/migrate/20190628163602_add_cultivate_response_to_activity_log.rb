class AddCultivateResponseToActivityLog < ActiveRecord::Migration[5.1]
  def change
  	add_column :activity_logs, :cultivate_response, :json
  end
end
