class ChangeStatusType < ActiveRecord::Migration[5.1]
  def change
    change_column :qc_files, :status, 'integer USING CAST(status AS integer)'
  end
end
