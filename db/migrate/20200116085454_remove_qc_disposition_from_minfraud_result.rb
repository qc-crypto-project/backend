class RemoveQcDispositionFromMinfraudResult < ActiveRecord::Migration[5.2]
  def change
    remove_column :minfraud_results, :qc_disposition
  end
end
