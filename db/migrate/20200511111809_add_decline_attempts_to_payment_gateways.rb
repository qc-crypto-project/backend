class AddDeclineAttemptsToPaymentGateways < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :decline_attempts, :jsonb
    add_column :payment_gateways, :decline_count, :integer, default: 0
    add_index :payment_gateways, :decline_count
    add_column :payment_gateways, :last_status, :string
  end
end
