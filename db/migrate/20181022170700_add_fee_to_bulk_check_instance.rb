class AddFeeToBulkCheckInstance < ActiveRecord::Migration[5.1]
  def change
    add_column :bulk_check_instances, :fee, :float, default: 0
  end
end
