class AddEnableRefundToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :enable_refund, :boolean, default: true
  end
end
