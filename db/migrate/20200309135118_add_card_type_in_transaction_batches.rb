class AddCardTypeInTransactionBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :card_type, :string
    add_column :transaction_batches, :card_brand, :string
  end
end
