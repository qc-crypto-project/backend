class AddBonusToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :bonus, :float
  end
end
