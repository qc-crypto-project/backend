class AddChangeNameOfGatewayId < ActiveRecord::Migration[5.1]
  def change
    rename_column :dispute_cases, :gateway_id, :payment_gateway_id
  end
end
