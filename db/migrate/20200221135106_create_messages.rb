class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.integer :user_id
      t.integer :chat_id
      t.string :text
      t.datetime :read_at
      t.boolean :seen,default: false

      t.timestamps
    end
  end
end
