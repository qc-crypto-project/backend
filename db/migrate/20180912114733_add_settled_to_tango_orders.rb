class AddSettledToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :settled, :boolean, default: true
  end
end
