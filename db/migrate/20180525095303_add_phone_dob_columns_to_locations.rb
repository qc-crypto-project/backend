class AddPhoneDobColumnsToLocations < ActiveRecord::Migration[5.1]
  def up
    add_column :locations, :phone_number ,:string
  end
  def down
    remove_column :locations, :phone_number ,:string
  end
end
