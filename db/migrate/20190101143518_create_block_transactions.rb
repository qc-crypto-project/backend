class CreateBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :block_transactions do |t|
      t.integer :sender_id
      t.integer :receiver_id
      t.integer :sender_wallet_id
      t.integer :receiver_wallet_id
      t.integer :amount_in_cents
      t.integer :fee_in_cents
      t.integer :action
      t.integer :main_type
      t.integer :sub_type
      t.integer :gateway
      t.integer :net_cents
      t.integer :tip_cents
      t.integer :hold_money_cents
      t.integer :seq_parent_id
      t.jsonb :tags
      t.boolean :refunded
      t.string :refund_reason
      t.jsonb :refund_log
      t.string :sequence_id
      t.datetime :timestamp
      t.timestamps
    end
  end
end