class AddColumnNameFromAsset < ActiveRecord::Migration[5.1]
  def change
    add_column :assets, :name, :string unless Asset.column_names.include?('name')
  end
end
