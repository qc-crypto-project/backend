class AddIsBakedIsoToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :is_baked_iso, :boolean, default: :false
  end
end
