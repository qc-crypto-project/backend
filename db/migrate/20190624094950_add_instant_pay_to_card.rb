class AddInstantPayToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :instant_pay, :boolean, :default => false
  end
end
