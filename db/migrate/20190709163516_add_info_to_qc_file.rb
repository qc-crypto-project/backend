class AddInfoToQcFile < ActiveRecord::Migration[5.1]
  def change
    add_column :qc_files, :info, :string
  end
end
