class AddIssueFeeToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :issue_fee, :boolean, default: true
  end
end
