class AddLocationIdInWallets < ActiveRecord::Migration[5.1]
  def up
    add_column :wallets, :location_id ,:integer
  end
  def down
   remove_column :wallets, :location_id ,:integer
  end
end
