class AddFlagToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :flag, :integer, default: 'no_flag'
  end
end
