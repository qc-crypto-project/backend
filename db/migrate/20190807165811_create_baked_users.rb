class CreateBakedUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :baked_users do |t|

      t.integer :user_id
      t.integer :location_id
      t.integer :wallet_id

      t.string :split_type
      t.string :position

      t.boolean :gbox
      t.boolean :iso_on_file
      t.boolean :attach

      t.float :dollar
      t.float :percent

      t.string :user_role


      t.timestamps
    end
  end
end
