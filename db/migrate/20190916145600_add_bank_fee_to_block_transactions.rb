class AddBankFeeToBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :transaction_fee, :float
    add_column :block_transactions, :per_transaction_fee, :float
    change_column_default :block_transactions, :transaction_fee, 0
    change_column_default :block_transactions, :per_transaction_fee, 0
  end
end
