class AddCompanyIdToGitfcards < ActiveRecord::Migration[5.1]
  def up
    add_column :giftcards, :company_id, :integer
  end
  def down
    remove_column :giftcards, :company_id, :integer
  end
end
