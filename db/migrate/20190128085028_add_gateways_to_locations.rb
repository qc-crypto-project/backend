class AddGatewaysToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :primary_gateway_id, :integer
    add_column :locations, :secondary_gateway_id, :integer
    add_column :locations, :ternary_gateway_id, :integer
  end
end
