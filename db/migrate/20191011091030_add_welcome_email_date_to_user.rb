class AddWelcomeEmailDateToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :welcome_user_date, :datetime
  end
end
