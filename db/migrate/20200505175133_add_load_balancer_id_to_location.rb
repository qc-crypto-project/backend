class AddLoadBalancerIdToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :load_balancer_id, :integer
    add_column :locations, :apply_load_balancer, :boolean, default: false
    add_index :locations, :load_balancer_id

  end
end
