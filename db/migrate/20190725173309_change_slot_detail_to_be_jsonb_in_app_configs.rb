class ChangeSlotDetailToBeJsonbInAppConfigs < ActiveRecord::Migration[5.1]
  def change
    change_column :app_configs, :slot_detail, :jsonb
  end
end
