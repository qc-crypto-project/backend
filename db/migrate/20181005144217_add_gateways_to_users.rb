class AddGatewaysToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :secondary_payment_gateway, :integer
    add_column :users, :third_payment_gateway, :integer
  end
end
