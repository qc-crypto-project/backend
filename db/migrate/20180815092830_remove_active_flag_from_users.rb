class RemoveActiveFlagFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :active_flag, :boolean
  end
end
