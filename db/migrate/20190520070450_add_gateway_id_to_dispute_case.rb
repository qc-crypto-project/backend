class AddGatewayIdToDisputeCase < ActiveRecord::Migration[5.1]
  def change
    add_column :dispute_cases, :gateway_id, :integer
  end
end
