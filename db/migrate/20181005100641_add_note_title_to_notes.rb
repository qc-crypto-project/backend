class AddNoteTitleToNotes < ActiveRecord::Migration[5.1]
  def change
  	remove_column :notes, :name
  	add_column :notes, :title, :string,:default => ""
  	add_column :notes, :name, :string, :default => ""
  end
end
 