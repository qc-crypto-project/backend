class AddOauthCodeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :oauth_code, :string
  end
end
