class CreatePermissions < ActiveRecord::Migration[5.1]
  def change
    create_table :permissions do |t|
      t.string :name
      t.numeric :permission_type
      t.boolean :wallet, default: false
      t.boolean :refund, default: false
      t.boolean :transfer, default: false
      t.boolean :b2b, default: false
      t.boolean :virtual_terminal, default: false
      t.boolean :dispute_view_only, default: false
      t.boolean :dispute_submit_evidence, default: false
      t.boolean :accept_dispute, default: false
      t.boolean :funding_schedule, default: false
      t.boolean :check, default: false
      t.boolean :push_to_card, default: false
      t.boolean :ach, default: false
      t.boolean :gift_card, default: false
      t.boolean :sales_report, default: false
      t.boolean :checks_report, default: false
      t.boolean :gift_card_report, default: false
      t.boolean :user_view_only, default: false
      t.boolean :user_edit, default: false
      t.boolean :user_add, default: false
      t.boolean :developer_app, default: false
      t.timestamps
    end
  end
end
