class AddImageIdInBankDetails < ActiveRecord::Migration[5.1]
  def up
    add_column :images , :bank_detail_id , :integer
  end
  def down
    remove_column :images , :bank_detail_id , :integer
  end
end
