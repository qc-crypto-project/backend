class AddTransactionIdToTransfers < ActiveRecord::Migration[5.1]
  def change
    add_column :transfers, :sequence_id, :string
  end
end
