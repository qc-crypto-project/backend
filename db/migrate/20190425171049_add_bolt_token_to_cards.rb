class AddBoltTokenToCards < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :bolt_token, :string
  end
end
