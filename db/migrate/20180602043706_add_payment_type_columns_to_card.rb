class AddPaymentTypeColumnsToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :stripe, :string
    add_column :cards, :elavon, :string
    add_column :cards, :payeezy, :string
    add_column :cards, :exp_date, :string
  end
end
