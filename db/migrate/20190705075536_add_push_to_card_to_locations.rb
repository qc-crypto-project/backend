class AddPushToCardToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :push_to_card, :boolean , default: false
  end
end
