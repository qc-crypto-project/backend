class RenameEFieldOfQrCard < ActiveRecord::Migration[5.1]
  def change
    rename_column :qr_cards, :type, :category
  end
end
