class AddRefNoToCompany < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :ref_no, :string
  end
end
