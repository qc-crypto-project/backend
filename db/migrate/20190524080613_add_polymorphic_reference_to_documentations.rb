class AddPolymorphicReferenceToDocumentations < ActiveRecord::Migration[5.1]
  def change
    remove_column :documentations, :user_id
    add_reference  :documentations , :imageable, polymorphic: true, index: true
  end
end
