class AddDispositionInMinfraudResult < ActiveRecord::Migration[5.2]
  def change
    add_column :minfraud_results, :mm_disposition, :jsonb
    add_column :minfraud_results, :qc_disposition, :jsonb
  end
end
