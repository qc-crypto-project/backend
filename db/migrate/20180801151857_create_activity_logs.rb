class CreateActivityLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :activity_logs do |t|
      t.integer  :user_id
      t.string   :browser
      t.string   :ip_address
      t.string   :action
      t.jsonb    :params
      t.string   :controller
      t.text     :note
      t.jsonb    :respond_with
      t.string   :browser
      t.string   :host
      t.string   :url
      t.timestamps
    end
  end
end
