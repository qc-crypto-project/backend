class CreateTangoOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :tango_orders do |t|
      t.references :user, foreign_key: true
      t.integer :utid
      t.integer :amount
      t.integer :order_id
      t.string :account_identifier
      t.timestamps
    end
  end
end
