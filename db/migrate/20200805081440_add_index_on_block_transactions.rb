class AddIndexOnBlockTransactions < ActiveRecord::Migration[5.2]
  def change
    add_index :block_transactions, :position
    add_index :block_transactions, :seq_parent_id
    add_index :block_transactions, :timestamp
    add_index :block_transactions, :amount_in_cents
  end
end
