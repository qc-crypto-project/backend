class AddLocationIdToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    column_exists?(:tango_orders, :location_id, :integer)
  end
end
