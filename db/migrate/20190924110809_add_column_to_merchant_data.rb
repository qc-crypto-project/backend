class AddColumnToMerchantData < ActiveRecord::Migration[5.1]
  def change
    add_column :merchant_data, :buyrate, :jsonb, default: '{}'
  end
end
