class AddDetailsToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :details, :jsonb
  end
end
