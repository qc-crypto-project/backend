class AddIsLockedToWallet < ActiveRecord::Migration[5.1]
  def change
    add_column :wallets, :is_locked, :boolean, default: true
    add_column :fees, :pincode, :string
    add_column :fees, :is_locked, :boolean, default: true
    add_column :locations, :pincode, :string
    add_column :locations, :is_locked, :boolean, default: true
  end
end
