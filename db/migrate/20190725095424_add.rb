class Add < ActiveRecord::Migration[5.1]
  def change
    add_column :dispute_cases, :chargeback_transaction_id, :integer
  end
end
