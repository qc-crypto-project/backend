class AddMerchantIdToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :merchant_id, :integer
    change_column :cards, :flag, :string,default: "straight"
  end
end
