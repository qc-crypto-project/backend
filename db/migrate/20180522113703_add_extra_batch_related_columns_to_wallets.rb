class AddExtraBatchRelatedColumnsToWallets < ActiveRecord::Migration[5.1]
  def up
    add_column :wallets , :total_amount , :float
    add_column :wallets , :total_sales , :integer
  end
  def down
    remove_column :wallets , :total_amount , :float
    remove_column :wallets , :total_sales , :integer
  end
end
