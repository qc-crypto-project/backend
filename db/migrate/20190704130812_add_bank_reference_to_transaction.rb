class AddBankReferenceToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :bank_reference, :string
  end
end
