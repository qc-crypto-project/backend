class ChangeColumnsInBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :ip, :string unless BlockTransaction.column_names.include?('ip')
    add_column :block_transactions, :last4, :string unless BlockTransaction.column_names.include?('last4')
    add_column :block_transactions, :card_id, :string unless BlockTransaction.column_names.include?('card_id')
    add_column :block_transactions, :charge_id, :string unless BlockTransaction.column_names.include?('charge_id')
    add_column :block_transactions, :status, :integer unless BlockTransaction.column_names.include?('status')
    add_column :block_transactions, :api_type, :string unless BlockTransaction.column_names.include?('api_type')
    add_column :block_transactions, :parent_id, :integer unless BlockTransaction.column_names.include?('parent_id')
    remove_column :block_transactions, :net_cents, :integer if BlockTransaction.column_names.include?('net_cents')
  end
end
