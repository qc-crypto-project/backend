class AddQrScannedToRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :requests, :qr_scanned, :boolean, default: false
  end
end
