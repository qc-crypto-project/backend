class AddWalletsToTransactionBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :iso_wallet_id, :integer unless column_exists? :transaction_batches, :iso_wallet_id
    add_column :transaction_batches, :agent_wallet_id, :integer unless column_exists? :transaction_batches, :agent_wallet_id
    add_column :transaction_batches, :affiliate_wallet_id, :integer unless column_exists? :transaction_batches, :affiliate_wallet_id
  end
end
