class AddLastDeclineToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :last_decline, :datetime
  end
end
