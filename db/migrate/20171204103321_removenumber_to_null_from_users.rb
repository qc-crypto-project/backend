class RemovenumberToNullFromUsers < ActiveRecord::Migration[5.1]
  def change 
    change_column :users, :phone_number, :string, :null => true
  end
end
