class AddInvoiceDebitCardFeeToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :invoice_debit_card_fee, :float, default: 0
    add_column :fees, :invoice_debit_card_fee_perc, :float, default: 0
  end
end
