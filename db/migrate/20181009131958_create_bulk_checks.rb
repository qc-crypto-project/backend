class CreateBulkChecks < ActiveRecord::Migration[5.1]
  def change
    create_table :bulk_checks do |t|
      t.string :name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :routing_number
      t.string :account_number
      t.string :confirm_account
      t.float :amount
      t.string :status, default: "Ready"
      t.text :response_message
      t.boolean :is_processed
      t.references :bulk_check_instance, foreign_key: true

      t.timestamps
    end
  end
end
