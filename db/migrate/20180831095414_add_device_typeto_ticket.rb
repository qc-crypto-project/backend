class AddDeviceTypetoTicket < ActiveRecord::Migration[5.1]
  def up
    add_column :tickets , :device_type , :string
    add_column :tickets , :time_stamp , :string
  end
  def down
   remove_column :tickets , :device_type , :string
   remove_column :tickets , :time_stamp , :string
  end
end
