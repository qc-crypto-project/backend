class AddLocationIdToImages < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :location_id, :integer
  end
end
