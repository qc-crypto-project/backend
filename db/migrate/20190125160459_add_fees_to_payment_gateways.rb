class AddFeesToPaymentGateways < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :account_processing_limit, :float, default: 0
    add_column :payment_gateways, :transaction_fee, :float, default: 0
    add_column :payment_gateways, :charge_back_fee, :float, default: 0
    add_column :payment_gateways, :retrieval_fee, :float, default: 0
    add_column :payment_gateways, :per_transaction_fee, :float, default: 0
    add_column :payment_gateways, :reserve_money, :float, default: 0
    add_column :payment_gateways, :reserve_money_days, :integer, default: 0
    add_column :payment_gateways, :monthly_service_fee, :float, default: 0
  end
end
