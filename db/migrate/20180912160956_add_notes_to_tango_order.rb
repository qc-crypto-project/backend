class AddNotesToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :notes, :string
  end
end
