class CreateChecks < ActiveRecord::Migration[5.1]
  def change
    create_table :checks do |t|
      t.text   :description
      t.float  :amount
      t.string :direction
      t.string :checkId
      t.string :status
      t.string :recipient
      t.string :sender
      t.string :name
      t.integer :number
      t.string :image_uri
      t.integer :user_id
      t.timestamps
    end
  end
end
