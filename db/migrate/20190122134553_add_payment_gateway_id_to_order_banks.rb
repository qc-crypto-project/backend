class AddPaymentGatewayIdToOrderBanks < ActiveRecord::Migration[5.1]
  def change
    add_column :order_banks, :payment_gateway_id, :integer
    add_index :order_banks, :payment_gateway_id
  end
end
