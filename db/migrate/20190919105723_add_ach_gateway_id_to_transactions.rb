class AddAchGatewayIdToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :ach_gateway_id, :integer
    add_index :transactions, :ach_gateway_id
  end
end
