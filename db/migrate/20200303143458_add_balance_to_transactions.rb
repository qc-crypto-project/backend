class AddBalanceToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :sender_balance, :float
    add_column :transactions, :receiver_balance, :float
  end
end
