class RenameReasonInDisputeRequests < ActiveRecord::Migration[5.2]
  def change
    rename_column(:dispute_requests,:reason,:title)
  end
end
