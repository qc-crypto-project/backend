class AddBlockAchToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :block_ach, :boolean, default: false
  end
end
