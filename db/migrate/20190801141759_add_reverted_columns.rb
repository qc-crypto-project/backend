class AddRevertedColumns < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :new_type, :string
    add_column :block_transactions, :refunded, :boolean
    add_column :block_transactions, :refund_reason, :jsonb
    add_column :block_transactions, :refund_log, :jsonb
  end
end
