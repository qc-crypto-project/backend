class AddMscFeeVerticalTypesToPaymentgateways < ActiveRecord::Migration[5.1]
  def change

    add_column :payment_gateways, :misc_fee, :float, default: 0
    add_column :payment_gateways, :misc_fee_dollars, :float, default: 0
    add_column :payment_gateways, :vertical_types, :text, default: 0
  end
end
