class UpdateTransactionTable < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :type, :string
    add_column :transactions, :sender_id, :integer
  end
end
