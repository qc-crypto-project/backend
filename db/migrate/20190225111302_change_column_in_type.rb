class ChangeColumnInType < ActiveRecord::Migration[5.1]
  def change
    rename_column :transactions, :type, :main_type
  end
end
