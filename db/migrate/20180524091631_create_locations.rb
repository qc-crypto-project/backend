class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.string  :business_name
      t.string  :first_name
      t.string  :last_name
      t.string  :email
      t.string  :web_site
      t.text    :address
      t.string  :business_type
      t.string  :years_in_business
      t.string  :category
      t.string  :city
      t.string  :state
      t.string  :zip
      t.string  :social_security_no
      t.string  :tax_id
      t.integer :user_id
      t.timestamps
    end
  end
end
