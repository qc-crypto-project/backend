class AddAmountToBulkcheck < ActiveRecord::Migration[5.1]
  def change
    add_column :bulk_check_instances, :total_amount, :float
  end
end
