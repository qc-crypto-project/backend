class ChangeSequenceIdInBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    # change_column_null :block_transactions, :sequence_id, false
    change_column :block_transactions, :sequence_id, :string, null: false, unique: true
    execute <<-SQL
      ALTER TABLE block_transactions
        ADD CONSTRAINT sequence_id UNIQUE (sequence_id);
    SQL
  end
end
