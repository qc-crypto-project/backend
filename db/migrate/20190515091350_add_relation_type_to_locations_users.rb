class AddRelationTypeToLocationsUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :locations_users, :relation_type, :string, default: "primary"
  end
end
