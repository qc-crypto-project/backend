class AddMessageIdToAhoyMessages < ActiveRecord::Migration[5.1]
  def change
    add_column :ahoy_messages, :message_id, :string
    add_column :ahoy_messages, :email_notification_id, :integer
    add_column :ahoy_messages, :status, :string
  end
end
