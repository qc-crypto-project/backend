class DebitCardDepositFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :dc_deposit_fee, :float
    add_column :fees, :dc_deposit_fee_dollar, :float
  end
end