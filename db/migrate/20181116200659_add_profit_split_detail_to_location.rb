class AddProfitSplitDetailToLocation < ActiveRecord::Migration[5.1]
  def up
    add_column :locations, :profit_split_detail, :jsonb
  end

  def down
    remove_column :locations, :profit_split_detail, :jsonb
  end
end
