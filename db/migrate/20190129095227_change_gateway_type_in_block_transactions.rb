class ChangeGatewayTypeInBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    change_column :block_transactions, :gateway, :string
  end
end
