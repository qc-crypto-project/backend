class AddActiveToDescriptor < ActiveRecord::Migration[5.2]
  def change
    add_column :descriptors, :active, :boolean, default: true
    add_column :descriptors, :daily_limit, :float, :default => 0
    add_column :descriptors, :monthly_limit, :float, :default => 0
    add_column :descriptors, :cbk_limit, :float, :default => 0
  end
end
