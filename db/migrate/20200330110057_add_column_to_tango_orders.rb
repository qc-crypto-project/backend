class AddColumnToTangoOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :tango_orders, :ach_international, :boolean, default: false
  end
end
