class CreateMerchantData < ActiveRecord::Migration[5.1]
  def change
    create_table :merchant_data do |t|
      t.jsonb :corporate, default: '{}'
      t.jsonb :owners, default: '{}'
      t.jsonb :location, default: '{}'
      t.jsonb :iso_agent, default: '{}'
      t.jsonb :fees, default: '{}'
      t.jsonb :setting, default: '{}'
      t.jsonb :documentation, default: '{}'
    end
  end
end
