class AddColumnsToAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :name, :string
    add_column :addresses, :email, :string
    add_column :addresses, :country, :string
    add_column :addresses, :address_type, :string
    add_column :addresses, :phone_no, :string
  end
end
