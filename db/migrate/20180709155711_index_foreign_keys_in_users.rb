class IndexForeignKeysInUsers < ActiveRecord::Migration[5.1]
  def change
    add_index :users, :company_id
    add_index :users, :location_id
  end
end
