class CreateWalletsUsersJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table :wallets_users do |t|
      t.integer :wallet_id
      t.integer :user_id

      t.timestamps
    end
  end
end
