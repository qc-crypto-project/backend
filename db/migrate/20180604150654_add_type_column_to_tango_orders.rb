class AddTypeColumnToTangoOrders < ActiveRecord::Migration[5.1]
  def up
    add_column :tango_orders ,:type , :integer
  end
  def remove
    remove_column :tango_orders ,:type , :integer
  end
end
