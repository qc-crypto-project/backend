class ChangeColumnNameAchFile < ActiveRecord::Migration[5.1]
  def change
    rename_column :ach_files, :check_id, :file_id
  end
end
