class AddIndexesOnActivityLogs < ActiveRecord::Migration[5.1]
  def change
    add_index :activity_logs, :transaction_id
    add_index :activity_logs, :activity_type
    add_index :activity_logs, :created_at
    add_index :activity_logs, :params, :using => :gin
    execute <<-SQL
      CREATE INDEX index_activity_logs_on_params_amount_key ON activity_logs ((params->>'amount'));
      CREATE INDEX index_activity_logs_on_params_card_number_key ON activity_logs ((params->>'card_number'));
    SQL
    # add_index :activity_logs, :params, :using => :gin, :expression => "(params->'amount')", :name => 'index_activity_logs_on_params_amount_key'
    # add_index :activity_logs, :params, :using => :gin, :expression => "(params->'card_number')", :name => 'index_activity_logs_on_params_card_number_key'
    add_index :activity_logs, [:user_id, :created_at]
  end
end
