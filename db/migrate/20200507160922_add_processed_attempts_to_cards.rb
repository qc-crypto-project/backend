class AddProcessedAttemptsToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :processed_attempts, :integer
    add_column :cards, :last_gateway, :integer
    add_index :cards, :last_gateway
  end
end
