class CreateFees < ActiveRecord::Migration[5.1]
  def change
    create_table :fees do |t|
      t.string :customer_fee
      t.string :b2b_fee
      t.string :redeem_fee
      t.string :send_check
      t.string :gbox
      t.string :partner
      t.string :iso
      t.string :agent
      t.integer :fee_id
      t.integer :fee_status
      t.integer :location_id
      t.timestamps
    end
  end
end
