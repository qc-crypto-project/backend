class CreateGiftcards < ActiveRecord::Migration[5.1]
  def change
    create_table :giftcards do |t|
      t.string :token
      t.string :exp_year
      t.string :exp_month
      t.references :wallet, foreign_key: true
      t.boolean :is_valid, :default => true

      t.timestamps
    end
  end
end
