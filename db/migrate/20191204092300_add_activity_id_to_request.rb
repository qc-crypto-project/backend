class AddActivityIdToRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :requests, :activity_id, :integer
  end
end
