class AddUnderReviewToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :under_review, :boolean, default: false
  end
end
