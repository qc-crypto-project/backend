class AddDispensarySplitToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :dispensary_credit_split, :boolean
    add_column :locations, :dispensary_debit_split, :boolean
  end
end
