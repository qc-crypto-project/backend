class AddGiftCardFeeToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :gift_card_fee, :float
  end
end
