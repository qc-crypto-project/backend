class AddApprovedByToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :approved_by, :integer
  end
end
