class CreateBankDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :bank_details do |t|
      t.string :bank_name
      t.string :account_name
      t.string :account_no
      t.string :routing_no
      t.string :account_digit
      t.string :routing_digit
      t.timestamps
    end
  end
end
