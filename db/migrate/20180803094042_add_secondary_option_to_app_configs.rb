class AddSecondaryOptionToAppConfigs < ActiveRecord::Migration[5.1]
  def change
    add_column :app_configs, :secondary_option, :string
  end
end
