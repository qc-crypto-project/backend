class AddCompanyEmailToCompanies < ActiveRecord::Migration[5.1]
  def up
    add_column :companies, :company_email, :string
  end
  def down
    remove_column :companies, :company_email, :string
  end
end
