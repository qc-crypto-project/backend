class AddWithdrawalToPermission < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :withdrawal, :boolean, default: false unless column_exists? :permissions, :withdrawal
  end
end
