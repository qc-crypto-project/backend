class AddBlockedTillToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :blocked_till, :datetime
  end
end
