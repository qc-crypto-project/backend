class AddRefNoToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :ref_no, :string
  end
end
