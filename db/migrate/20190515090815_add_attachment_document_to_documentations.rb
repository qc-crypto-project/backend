class AddAttachmentDocumentToDocumentations < ActiveRecord::Migration[5.1]
  def change
    create_table :documentations do |t|
      t.integer    :image_type
      t.attachment :document
      t.references :user, foreign_key: true
    end
  end
end
