class AddHoldMoneyDaysAndCheckInLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :hold_in_rear, :boolean, default: false
    add_column :locations, :hold_in_rear_days, :integer, default: 1
  end
end
