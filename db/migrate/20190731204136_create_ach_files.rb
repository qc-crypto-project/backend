class CreateAchFiles < ActiveRecord::Migration[5.1]
  def change
    create_table :ach_files do |t|
      t.integer :check_id
      t.attachment :file

      t.timestamps
    end
  end
end
