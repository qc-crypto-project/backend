class AddApprovedToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :approved, :boolean, default: true
  end
end
