class AddAppFieldsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :client_id, :string
    add_column :users, :client_secret, :string
    add_column :users, :app_id, :integer
    add_column :users, :redirect_url, :string
  end
end
