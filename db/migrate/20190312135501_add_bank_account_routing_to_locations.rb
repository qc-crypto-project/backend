class AddBankAccountRoutingToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :bank_account, :string
    add_column :locations, :bank_routing, :string
  end
end
