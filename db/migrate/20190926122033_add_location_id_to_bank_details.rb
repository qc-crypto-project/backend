class AddLocationIdToBankDetails < ActiveRecord::Migration[5.1]
  def change
    add_column :bank_details, :location_id, :integer
    #add_index :bank_details, :location_id
  end
end