class AddEazeReportingToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :eaze_reporting, :boolean, default: false unless Location.column_names.include?('eaze_reporting')
  end
end
