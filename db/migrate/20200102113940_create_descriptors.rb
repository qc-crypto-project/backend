class CreateDescriptors < ActiveRecord::Migration[5.2]
  def change
    create_table :descriptors do |t|
      t.string :name

      t.timestamps
    end
  end
end
