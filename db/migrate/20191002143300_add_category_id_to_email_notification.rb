class AddCategoryIdToEmailNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :email_notifications, :category_id, :string
  end
end
