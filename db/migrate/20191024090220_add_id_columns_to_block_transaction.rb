class AddIdColumnsToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :iso_id, :integer
    add_column :block_transactions, :agent_id, :integer
    add_column :block_transactions, :affiliate_id, :integer
  end
end
