class AddTotalTransactionCountToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :total_transaction_count, :integer
    add_column :payment_gateways, :total_decline_count, :integer
    add_column :payment_gateways, :total_cbk_count, :integer
  end
end
