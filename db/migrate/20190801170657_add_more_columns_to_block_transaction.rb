class AddMoreColumnsToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :source_name, :string
    add_column :block_transactions, :destination_name, :string
    add_column :block_transactions, :merchant_name, :string
    add_column :block_transactions, :location_dba_name, :string
  end
end
