class AddLastPasswordUpdateToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :last_password_update, :datetime, default: Time.zone.now
  end
end
