class AddLedgerToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :ledger, :string
  end
end
