class AddFeeMethodToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :fee_method, :string  unless column_exists? :transactions, :fee_method
  end
end
