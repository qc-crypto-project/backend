class RemoveLocationIdFromTransactionBatches < ActiveRecord::Migration[5.2]
  def change
    remove_column :transaction_batches, :location_id
  end
end
