class AddStandaloneModeToLocation < ActiveRecord::Migration[5.1]
  def change
  	add_column :locations, :standalone, :boolean, default: false
  end
end
