class IndexForeignKeysInLocations < ActiveRecord::Migration[5.1]
  def change
    add_index :locations, :merchant_id
  end
end
