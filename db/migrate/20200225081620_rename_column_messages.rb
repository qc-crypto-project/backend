class RenameColumnMessages < ActiveRecord::Migration[5.2]
  def change
    rename_column :messages,:user_id,:actor_id
  end
end
