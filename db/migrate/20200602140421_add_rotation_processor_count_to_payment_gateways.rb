class AddRotationProcessorCountToPaymentGateways < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :rotation_count, :integer, default: 0
  end
end
