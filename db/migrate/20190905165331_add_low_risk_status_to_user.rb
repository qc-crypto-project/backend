class AddLowRiskStatusToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :low_risk_status, :string
  end
end
