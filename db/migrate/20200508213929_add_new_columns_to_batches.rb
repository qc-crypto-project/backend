class AddNewColumnsToBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :iso_commission, :float unless column_exists? :transaction_batches, :iso_commission
    add_column :transaction_batches, :agent_commission, :float unless column_exists? :transaction_batches, :agent_commission
    add_column :transaction_batches, :affiliate_commission, :float unless column_exists? :transaction_batches, :affiliate_commission
    add_column :transaction_batches, :iso_expense, :float unless column_exists? :transaction_batches, :iso_expense
    add_column :transaction_batches, :affiliate_expense, :float unless column_exists? :transaction_batches, :affiliate_expense
    add_column :transaction_batches, :agent_expense, :float unless column_exists? :transaction_batches, :agent_expense
    add_column :transaction_batches, :iso_split, :float unless column_exists? :transaction_batches, :iso_split
    add_column :transaction_batches, :agent_split, :float unless column_exists? :transaction_batches, :agent_split
    add_column :transaction_batches, :affiliate_split, :float unless column_exists? :transaction_batches, :affiliate_split
  end
end
