class AddFailedAchFeeToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :failed_ach_fee, :float, default: 0.0
    add_column :fees, :failed_push_to_card_fee, :float, default: 0.0
    add_column :fees, :failed_check_fee, :float, default: 0.0
    add_column :fees, :failed_ach_percent_fee, :float, default: 0.0
    add_column :fees, :failed_push_to_card_percent_fee, :float, default: 0.0
    add_column :fees, :failed_check_percent_fee, :float, default: 0.0
  end
end
