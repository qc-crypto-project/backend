class RenameValidFromQrCards < ActiveRecord::Migration[5.1]
  def change
    rename_column :qr_cards, :vaild, :is_valid
  end
end
