class AddFromAdminToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :from_admin, :boolean, :default => false
  end
end
