class CreateCustomerMerchants < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_merchants do |t|
      t.integer :customer_id
      t.integer :merchant_id
      t.integer :location_id
      t.integer :wallet_id

      t.timestamps
    end
  end
end
