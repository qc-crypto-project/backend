class AddColumnsToImages < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :evidence_id, :integer
    add_column :images, :file_type, :string, default: 'default'
  end
end
