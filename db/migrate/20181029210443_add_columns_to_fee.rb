class AddColumnsToFee < ActiveRecord::Migration[5.1]
  def up
    add_column :fees, :commission, :jsonb
  end

  def down
    remove_column :fees, :commission, :jsonb
  end
end
