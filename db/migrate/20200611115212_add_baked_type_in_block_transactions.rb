class AddBakedTypeInBlockTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :block_transactions, :baked, :string
  end
end
