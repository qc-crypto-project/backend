class AddPaymentGatewayIdToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :payment_gateway_id, :integer
    add_index :transactions, :payment_gateway_id
  end
end
