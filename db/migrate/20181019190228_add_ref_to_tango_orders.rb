class AddRefToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_reference :bulk_checks, :tango_order, foreign_key: true
  end
end
