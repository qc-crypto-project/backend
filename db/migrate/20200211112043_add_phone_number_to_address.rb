class AddPhoneNumberToAddress < ActiveRecord::Migration[5.2]
  def change
    rename_column :addresses, :phone_no, :phone_number
    add_column :addresses, :whom, :string
    add_column :addresses, :tracker_id, :integer
    add_column :addresses, :company, :string
    add_column :addresses, :address_code, :string
    add_column :addresses, :first_name, :string
    add_column :addresses, :last_name, :string
  end
end
