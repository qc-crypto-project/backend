class AddNetColumnsToBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :tx_amount, :float, :default => 0
    add_column :block_transactions, :net_fee, :float, :default => 0
    add_column :block_transactions, :total_net, :float, :default => 0
    add_column :block_transactions, :privacy_fee, :float, :default => 0
  end
end
