class AddSenderWalletIdToDisputeCase < ActiveRecord::Migration[5.1]
  def up
  	remove_column :dispute_cases, :card_number
    add_column :dispute_cases, :merchant_wallet_id, :integer
    add_column :dispute_cases, :user_wallet_id, :integer 
  	add_column :dispute_cases, :card_number, :string, :default => ""
  end
  def down
  	remove_column :dispute_cases, :merchant_wallet_id
  	remove_column :dispute_cases, :card_number
    remove_column :dispute_cases, :user_wallet_id
    add_column :dispute_cases, :card_number, :float, :default => 0.0
  end
end
