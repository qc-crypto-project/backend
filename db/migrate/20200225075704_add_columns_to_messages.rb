class AddColumnsToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages,:transaction_id,:integer
    remove_column :messages,:seen
  end
end
