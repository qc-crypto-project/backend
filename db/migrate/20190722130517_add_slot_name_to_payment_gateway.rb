class AddSlotNameToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :slot_name, :string
  end
end
