class AddBlockGiftCardToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :block_giftcard, :BOOLEAN
  end
end
