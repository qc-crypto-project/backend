class AddProfileStatusToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :is_profile_completed, :boolean, :default => false
  end
end
