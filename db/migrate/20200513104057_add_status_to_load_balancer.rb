class AddStatusToLoadBalancer < ActiveRecord::Migration[5.2]
  def change
    add_column :load_balancers, :active, :boolean, default: true
  end
end
