class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :transactions, :bonus, :discount
  end
end
