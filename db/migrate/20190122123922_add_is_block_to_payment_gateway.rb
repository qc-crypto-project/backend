class AddIsBlockToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :is_block, :boolean, :default => false
  end
end
