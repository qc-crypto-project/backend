class AddNewColumnsInBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :tx_type, :string
    add_column :block_transactions, :tx_sub_type, :string
  end
end
