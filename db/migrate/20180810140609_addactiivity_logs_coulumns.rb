class AddactiivityLogsCoulumns < ActiveRecord::Migration[5.1]
  def up
    add_column :activity_logs , :response_status, :string
    add_column :activity_logs , :activity_type, :integer
  end
  def remove
    remove_column :activity_logs , :response_status, :string
    remove_column :activity_logs , :activity_type, :integer
  end
end
