class ChangeSalesColumnTypeOfWallets < ActiveRecord::Migration[5.1]
  def change
    change_column :wallets, :total_sales, :float
  end
end
