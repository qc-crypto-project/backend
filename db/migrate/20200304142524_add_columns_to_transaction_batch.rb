class AddColumnsToTransactionBatch < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :expenses, :float
    add_column :transaction_batches, :starting_balance, :float
  end
end
