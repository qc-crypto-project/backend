class AddiSoAgentIsTruetoSetting < ActiveRecord::Migration[5.1]
  def up
    add_column :locations, :iso_allowed, :boolean, :default => false
    add_column :locations, :agent_allowed, :boolean, :default => false
  end
  def down
    remove_column :locations, :iso_allowed, :boolean, :default => false
    remove_column :locations, :agent_allowed, :boolean, :default => false
  end
end
