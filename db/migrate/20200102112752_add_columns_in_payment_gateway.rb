class AddColumnsInPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :descriptor_id, :integer
    add_column :payment_gateways, :status, :string, default: "active"
  end
end
