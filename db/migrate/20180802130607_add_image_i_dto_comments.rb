class AddImageIDtoComments < ActiveRecord::Migration[5.1]
  def up
    add_column :images , :comment_id , :integer
  end
  def down
    remove_column :images , :comment_id , :integer
  end
end
