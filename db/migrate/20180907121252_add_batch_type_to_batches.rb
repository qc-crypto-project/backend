class AddBatchTypeToBatches < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :batch_type, :integer
  end
end
