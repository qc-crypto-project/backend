class CreateBlockedatCards < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :is_blocked, :boolean, :default => false
  end
end
