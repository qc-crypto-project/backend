class CreateTemplates < ActiveRecord::Migration[5.2]
  def change
    add_column :images, :name, :string
    add_column :images, :invoice_user_id, :integer
    add_column :requests, :due_days, :integer
    add_column :transactions, :invoice_id, :integer
    add_column :requests, :logo_id, :integer
    add_column :requests, :schedule_date, :datetime
    add_column :requests, :late_fee, :float
    add_column :requests, :late_fee_method, :string
    add_column :requests, :discount_method, :string
    add_column :requests, :fee_method, :string
    add_column :requests, :tax_method, :string
    add_column :requests, :total_amount, :float
    add_column :products, :total_amount, :float
    add_column :products, :request_id, :integer
    add_column :requests, :invoice_number, :string
    add_column :requests, :payment_option, :json, default: {"card"=>false, "ach"=>false, "quickcard"=>false}
    add_column :requests, :discount, :float
    add_column :requests, :tax, :float
    add_column :requests, :fee, :float
    add_column :requests, :due_date, :datetime
  end
end