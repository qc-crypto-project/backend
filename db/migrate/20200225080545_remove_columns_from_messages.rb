class RemoveColumnsFromMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages,:recipient_id,:integer
    remove_column :messages,:chat_id
  end
end
