class AddNewFieldsToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :approve_mr_transaction, :boolean, default: false
    add_column :permissions, :refund_mr_transaction, :boolean, default: false
    add_column :permissions, :export_list, :boolean, default: false
    add_column :permissions, :permission_view_only, :boolean, default: false
    add_column :permissions, :permission_edit, :boolean, default: false
    add_column :permissions, :permission_add, :boolean, default: false
    add_column :permissions, :api_key, :boolean, default: false
    add_column :permissions, :plugin, :boolean, default: false
    add_column :permissions, :fee_structure, :boolean, default: false
    add_column :permissions, :help, :boolean, default: false
  end
end
