class AddRiskToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :risk, :integer
  end
end
