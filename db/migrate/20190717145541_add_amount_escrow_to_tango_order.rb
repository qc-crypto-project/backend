class AddAmountEscrowToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :amount_escrow, :boolean
  end
end
