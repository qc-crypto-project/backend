class AddFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :block_withdrawal, :boolean
    add_column :users, :block_ach,        :boolean
    add_column :users, :push_to_card,     :boolean
    add_column :users, :block_giftcard,   :boolean
  end
end
