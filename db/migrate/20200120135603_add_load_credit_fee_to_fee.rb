class AddLoadCreditFeeToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :load_fee_credit, :float
    add_column :fees, :load_fee_percent_credit, :float
    add_column :fees, :load_fee_debit, :float
    add_column :fees, :load_fee_percent_debit, :float
  end
end
