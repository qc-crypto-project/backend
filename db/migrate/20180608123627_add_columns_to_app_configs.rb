class AddColumnsToAppConfigs < ActiveRecord::Migration[5.1]
  def change
    add_column :app_configs, :merchant_fee, :float
    add_column :app_configs, :user_fee, :float
    add_column :app_configs, :atm_fee, :float
  end
end
