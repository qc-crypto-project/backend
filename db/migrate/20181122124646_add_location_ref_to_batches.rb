class AddLocationRefToBatches < ActiveRecord::Migration[5.1]
  def change
    add_reference :batches, :location, foreign_key: true
  end
end
