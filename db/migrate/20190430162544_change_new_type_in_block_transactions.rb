class ChangeNewTypeInBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    change_column :block_transactions, :new_type, :string
  end
end
