class AddDisputeColumnsInProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products,:customer_dispute_id, :integer
    add_column :products,:refund_count,:integer
    add_index :products, :customer_dispute_id
  end
end
