class AddFieldsToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :charge_back_percent, :float
    add_column :fees, :charge_back_count, :integer
    add_column :fees, :refund_percent, :float
    add_column :fees, :refund_count, :integer
  end
end
