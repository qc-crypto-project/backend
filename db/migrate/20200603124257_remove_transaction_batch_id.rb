class RemoveTransactionBatchId < ActiveRecord::Migration[5.2]
  def up
    remove_column :transactions, :transaction_batch_id
  end

  def down
    add_column :transactions, :transaction_batch_id, :integer
  end
end
