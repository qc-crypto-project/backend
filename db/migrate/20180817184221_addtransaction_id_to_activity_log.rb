class AddtransactionIdToActivityLog < ActiveRecord::Migration[5.1]
  def up
    add_column :activity_logs, :transaction_id, :integer
  end
  def down
    remove_column :activity_logs, :transaction_id, :integer
  end
end
