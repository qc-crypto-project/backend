class AddIndexToWalletType < ActiveRecord::Migration[5.1]
  def change
    add_index :wallets, :wallet_type
    add_index :fees, :fee_status
    add_index :fees, :fee_id
    add_index :order_banks, :user_id
    add_index :order_banks, :merchant_id
    add_index :payment_gateways, :type
    add_index :tango_orders, :order_type
    add_index :transfers, :from_wallet
    add_index :transfers, :to_wallet
  end
end
