class AddTransactionActivityIndex < ActiveRecord::Migration[5.2]
  def change
    add_index :transactions, :ref_id
    add_index :activity_logs, :ref_id
    add_index :activity_logs, :action
  end
end
