class ChangeLimitOfColumn < ActiveRecord::Migration[5.1]
  def change
    change_column :block_transactions, :receiver_id, :integer, limit: 8
    change_column :block_transactions, :sender_id, :integer, limit: 8
    change_column :block_transactions, :sender_wallet_id, :integer, limit: 8
    change_column :block_transactions, :receiver_wallet_id, :integer, limit: 8

  end
end
