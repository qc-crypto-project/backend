class RemoverUserFromLocations < ActiveRecord::Migration[5.1]
  def change
    remove_column :locations, :user_id ,:integer
    add_column :locations,:merchant_id ,:integer
  end
end
