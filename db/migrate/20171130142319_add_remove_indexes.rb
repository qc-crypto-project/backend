class AddRemoveIndexes < ActiveRecord::Migration[5.1]
  def change
    remove_index :users, :email
    change_column :users, :email, :string, :null => true
    change_column :users, :phone_number, :string, :null => false
    add_index :users, :phone_number
  end
end
