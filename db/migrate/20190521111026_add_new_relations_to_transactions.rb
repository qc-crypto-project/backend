class AddNewRelationsToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :receiver_id, :integer
    add_column :transactions, :sender_wallet_id, :integer
    add_column :transactions, :receiver_wallet_id, :integer
  end
end
