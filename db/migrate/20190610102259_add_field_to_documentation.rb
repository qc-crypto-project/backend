class AddFieldToDocumentation < ActiveRecord::Migration[5.1]
  def change
    add_column :documentations, :is_deleted, :boolean, default: false
  end
end
