class AddLoadBalancerIdToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :load_balancer_id, :integer
    add_index :payment_gateways, :load_balancer_id
  end
end
