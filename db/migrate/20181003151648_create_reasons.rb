class CreateReasons < ActiveRecord::Migration[5.1]
  def change
    create_table :reasons do |t|
      t.string :title, :default => ""
      t.string :reason_type, :default => ""

      t.timestamps
    end
  end
end
