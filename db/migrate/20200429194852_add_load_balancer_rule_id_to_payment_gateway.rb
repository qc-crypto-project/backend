class AddLoadBalancerRuleIdToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :load_balancer_rule_id, :integer
    add_index :payment_gateways, :load_balancer_rule_id
  end
end
