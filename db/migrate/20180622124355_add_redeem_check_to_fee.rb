class AddRedeemCheckToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :redeem_check, :string
  end
end
