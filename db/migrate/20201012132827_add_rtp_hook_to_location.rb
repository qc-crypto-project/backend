class AddRtpHookToLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :rtp_hook, :string
  end
end
