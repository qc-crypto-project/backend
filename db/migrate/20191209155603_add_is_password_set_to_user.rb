class AddIsPasswordSetToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_password_set, :boolean, null: false, default: true
  end
end
