class AddColumnToOwnerDlStateIssued < ActiveRecord::Migration[5.1]
  def change
    add_column :owners, :dl_state_issue, :string
  end
end
