class AddNewColumnsToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :gbox_fee, :float, default: 0
    add_column :transactions, :iso_fee, :float, default: 0
    add_column :transactions, :affiliate_fee, :float, default: 0
    add_column :transactions, :agent_fee, :float, default: 0
    add_column :transactions, :total_amount, :float, default: 0
    add_column :transactions, :net_fee, :float, default: 0
    add_column :transactions, :net_amount, :float, default: 0
  end
end
