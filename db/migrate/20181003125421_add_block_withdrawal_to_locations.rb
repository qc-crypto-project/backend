class AddBlockWithdrawalToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :block_withdrawal, :boolean, default: false
  end
end
