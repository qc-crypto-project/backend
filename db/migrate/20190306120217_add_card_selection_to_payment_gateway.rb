class AddCardSelectionToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :card_selection, :json, default: {"visa_credit"=>"on", "visa_debit"=>"on", "mastercard"=>"on", "mastercard_debit"=>"on","amex_credit"=>"on","discover_credit"=>"on","jcb_credit"=>"on"}
  end
end
