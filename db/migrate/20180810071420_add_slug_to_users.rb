class AddSlugToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :slug, :string
    add_column :wallets, :slug, :string
    add_column :tickets, :slug, :string

    add_index :users, :slug, unique: true
    add_index :wallets, :slug, unique: true
    add_index :tickets, :slug, unique: true
  end
end
