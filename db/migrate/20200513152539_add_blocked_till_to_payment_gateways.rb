class AddBlockedTillToPaymentGateways < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :blocked_till, :datetime
  end
end
