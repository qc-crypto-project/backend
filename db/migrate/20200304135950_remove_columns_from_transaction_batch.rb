class RemoveColumnsFromTransactionBatch < ActiveRecord::Migration[5.2]
  def change
    remove_column :transaction_batches, :payment_gateway_id
    remove_column :transaction_batches, :total_amount_in_cents
    remove_column :transaction_batches, :total_iso_fee
    remove_column :transaction_batches, :total_agent_fee
    remove_column :transaction_batches, :total_affiliate_fee
    remove_column :transaction_batches, :total_gbox_fee
    remove_column :transaction_batches, :total_tip
    remove_column :transaction_batches, :total_privacy_fee
    remove_column :transaction_batches, :total_partner_fee
    remove_column :transaction_batches, :total_hold_money
    remove_column :transaction_batches, :total_tx_amount
    remove_column :transaction_batches, :total_net_fee

  end
end
