class CreateOrderBanks < ActiveRecord::Migration[5.1]
  def change
    create_table :order_banks do |t|
      t.integer :user_id
      t.integer :merchant_id
      t.string :bank_type
      t.string :card_type
      t.string :status
      t.float :amount
      t.timestamps
    end
  end
end
