class AddDefaultValueToUser < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :low_risk, :boolean, default: false
  end
end
