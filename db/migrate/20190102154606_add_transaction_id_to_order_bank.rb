class AddTransactionIdToOrderBank < ActiveRecord::Migration[5.1]
  def change
    add_column :order_banks, :transaction_id, :integer
  end
end
