class AddTimeZoneToTransfer < ActiveRecord::Migration[5.1]
  def change
    add_column :transfers, :transfer_timezone, :string
  end
end
