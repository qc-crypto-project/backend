class AddChargebacksFieldsToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :view_chargeback_cases, :boolean, default: false
    add_column :permissions, :fight_chargeback, :boolean, default: false
    add_column :permissions, :accept_chargeback, :boolean, default: false
  end
end
