class AddBlockedEmailToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :blocked_email, :string
  end
end
