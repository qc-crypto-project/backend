class AddPaymentGatwayRefToVerticalTypes < ActiveRecord::Migration[5.1]
  def change
  	add_reference :vertical_types, :payment_gateway, index: true
  end
end
