class AddAmexFeeToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :vmcardrate, :float
    add_column :fees, :distransfee, :float
    add_column :fees, :amexrate, :float
    add_column :fees, :amextransfee, :float
    add_column :fees, :monthlygatewayfee, :float
    add_column :fees, :annualccsalesvol, :float
  end
end
