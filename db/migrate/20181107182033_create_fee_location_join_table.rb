class CreateFeeLocationJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table :fees_locations, id: false do |t|
      t.integer :fee_id
      t.integer :location_id
    end
  end
end
