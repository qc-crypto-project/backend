class AddAuthenticationIdToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :authentication_id, :text
    add_column :payment_gateways, :signature_maker, :text
  end
end
