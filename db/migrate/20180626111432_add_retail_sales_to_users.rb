class AddRetailSalesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :retail_sales, :boolean, default: false
  end
end
