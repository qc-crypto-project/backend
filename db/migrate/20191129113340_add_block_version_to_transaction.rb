class AddBlockVersionToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :block_version, :string,default: 2
    add_column :block_transactions, :block_version, :string,default: 2
    add_column :wallets, :block_version, :string,default: 2
  end
end
