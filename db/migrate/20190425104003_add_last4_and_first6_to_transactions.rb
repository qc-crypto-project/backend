class AddLast4AndFirst6ToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :last4, :string
    add_column :transactions, :first6, :string
  end
end
