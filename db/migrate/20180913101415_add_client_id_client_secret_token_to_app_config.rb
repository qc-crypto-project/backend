class AddClientIdClientSecretTokenToAppConfig < ActiveRecord::Migration[5.1]
  def up
  	add_column :app_configs, :client_id, :string, :default => ""
  	add_column :app_configs, :client_secret, :string, :default => ""
  end
  def down
  	add_column :app_configs, :client_id, :string, :default => ""
  	add_column :app_configs, :client_secret, :string, :default => ""
  end
end
