class AddCoulumRemainingBalanceToGiftcards < ActiveRecord::Migration[5.1]
  def change
    add_column :giftcards, :remaining_balance, :string
  end
end
