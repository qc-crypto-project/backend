class AddIndexToTangoOrders < ActiveRecord::Migration[5.2]
  def change
    add_index :tango_orders, :batch_date
  end
end
