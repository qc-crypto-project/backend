class ChangeColumnsInMaxmindResult < ActiveRecord::Migration[5.2]
  def change
    add_column :minfraud_results, :qc_action, :string
    add_column :minfraud_results, :qc_reason, :string
  end
end
