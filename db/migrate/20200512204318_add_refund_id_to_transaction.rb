class AddRefundIdToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions,:refund_id,:integer
  end
end
