class AddTransactionIdToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :transaction_id, :integer
  end
end
