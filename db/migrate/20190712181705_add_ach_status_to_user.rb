class AddAchStatusToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :merchant_ach_status, :integer, :default => "initial"
    add_column :locations, :bank_account_name, :string
  end
end
