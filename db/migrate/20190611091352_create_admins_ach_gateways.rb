class CreateAdminsAchGateways < ActiveRecord::Migration[5.1]
  def change
    create_table :ach_gateways do |t|
      t.string :bank_name
      t.string :descriptor
      t.integer :status, default: 0
      t.boolean :archive, default: false
      t.float :bank_fee_percent, default: 0.0
      t.float :bank_fee_dollar, default: 0.0

      t.timestamps
    end
  end
end
