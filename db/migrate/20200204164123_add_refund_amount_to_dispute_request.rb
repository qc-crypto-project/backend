class AddRefundAmountToDisputeRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :dispute_requests,:refund_amount,:float
    add_column :customer_disputes,:refund_amount,:float

  end
end
