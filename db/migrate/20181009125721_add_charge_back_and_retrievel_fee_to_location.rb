class AddChargeBackAndRetrievelFeeToLocation < ActiveRecord::Migration[5.1]
  def change
  	add_column :fees, :charge_back_fee, :float, :default => 0.0 
  	add_column :fees, :retrievel_fee, :float, :default => 0.0 
  end
end
