class AddGetattentionToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :get_attention, :boolean, default: true
  end
end
