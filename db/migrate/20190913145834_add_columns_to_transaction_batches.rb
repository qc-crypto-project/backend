class AddColumnsToTransactionBatches < ActiveRecord::Migration[5.1]
  def change
		add_column :transaction_batches, :batch_date, :datetime
		add_column :transaction_batches, :total_amount_in_cents, :float
		add_column :transaction_batches, :total_trxs, :float
		add_column :transaction_batches, :total_fee_in_cents, :float
		add_column :transaction_batches, :total_iso_fee, :float
		add_column :transaction_batches, :total_agent_fee, :float
		add_column :transaction_batches, :total_gbox_fee, :float
		add_column :transaction_batches, :total_main_amount, :float
		add_column :transaction_batches, :total_tip, :float
		add_column :transaction_batches, :total_privacy_fee, :float
		add_column :transaction_batches, :total_hold_money, :float
		add_column :transaction_batches, :total_partner_fee, :float
		add_column :transaction_batches, :total_affiliate_fee, :float
		add_column :transaction_batches, :total_net_amount, :float
		add_column :transaction_batches, :total_tx_amount, :float
		add_column :transaction_batches, :total_net_fee, :float
  end
end
