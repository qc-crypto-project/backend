class AddBatchRiskToBatches < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :batch_risk, :integer
  end
end
