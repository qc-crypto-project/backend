class AddLowRiskToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :low_risk, :boolean
    add_column :users, :low_risk_details, :jsonb
  end
end
