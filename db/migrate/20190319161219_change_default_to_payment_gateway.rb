class ChangeDefaultToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    change_column_default :payment_gateways, :card_selection, {"visa_credit"=>"on", "visa_debit"=>"on", "mastercard_credit"=>"on", "mastercard_debit"=>"on","amex_credit"=>"on","discover_credit"=>"on","jcb_credit"=>"on"}
  end
end
