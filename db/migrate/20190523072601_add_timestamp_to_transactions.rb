class AddTimestampToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :timestamp, :datetime
  end
end
