class CreateQcFile < ActiveRecord::Migration[5.1]
  def change
    create_table :qc_files do |t|
      t.string :name
      t.string :file_name
      t.string :file_url
      t.datetime :start_date
      t.datetime :end_date
      t.string :status
    end
  end
end
