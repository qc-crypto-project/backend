class CreateAdminsPaymentGateways < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_gateways do |t|
      t.integer :type
      t.string :name
      t.text :client_secret
      t.text :client_id
      t.timestamps
    end
  end
end
