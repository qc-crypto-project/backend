class RenameColumnInAsset < ActiveRecord::Migration[5.1]
  def change
    rename_column :assets, :filetype, :file_type
  end
end
