class AddGatewayIdToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :payment_gateway_id, :integer
  end
end
