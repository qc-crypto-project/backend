class AddFieldsToFees < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :service_last_charged, :datetime
    add_column :fees, :statement_last_charged, :datetime
    add_column :fees, :misc_last_charged, :datetime
  end
end
