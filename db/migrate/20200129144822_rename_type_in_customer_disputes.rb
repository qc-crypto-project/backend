class RenameTypeInCustomerDisputes < ActiveRecord::Migration[5.2]
  def change
    rename_column(:customer_disputes,:type,:dispute_type)
  end
end
