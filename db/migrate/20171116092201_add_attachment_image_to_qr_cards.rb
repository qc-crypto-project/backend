class AddAttachmentImageToQrCards < ActiveRecord::Migration[5.1]
  def self.up
    change_table :qr_cards do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :qr_cards, :image
  end
end
