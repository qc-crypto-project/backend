class RemoveColumnsFromTransactionBatches < ActiveRecord::Migration[5.2]
  def change
    remove_column :transaction_batches, :card_brand
    remove_column :transaction_batches, :card_type
  end
end
