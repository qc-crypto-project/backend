class AddWebsiteUrlToMinfraudResults < ActiveRecord::Migration[5.2]
  def change
    add_column :minfraud_results, :website_url, :string
  end
end
