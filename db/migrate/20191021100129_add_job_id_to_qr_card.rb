class AddJobIdToQrCard < ActiveRecord::Migration[5.1]
  def change
    add_column :qr_cards, :job_id, :string
  end
end
