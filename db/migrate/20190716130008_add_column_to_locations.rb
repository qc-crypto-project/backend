class AddColumnToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :standalone_type, :string
  end
end
