class AddVolumeAchivedToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :daily_volume_achived, :float, default: 0
    add_column :payment_gateways, :last_daily_updated, :datetime
    add_column :payment_gateways, :monthly_volume_achived, :float, default: 0
    add_column :payment_gateways, :last_monthly_updated, :datetime
  end
end
