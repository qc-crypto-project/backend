class AddRequestIdtoImage < ActiveRecord::Migration[5.1]
  def up
    add_column :images, :request_id, :integer
  end
  def down
    remove_column :images, :request_id, :integer
  end
end
