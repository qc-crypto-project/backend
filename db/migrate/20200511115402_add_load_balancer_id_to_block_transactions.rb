class AddLoadBalancerIdToBlockTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :block_transactions, :load_balancer_id, :integer
    add_index :block_transactions, :load_balancer_id
    add_column :order_banks, :load_balancer_id, :integer
    add_index :order_banks, :load_balancer_id
  end
end
