class AddFeeAndPlanPasswordToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :fee, :float
    add_column :users, :plain_password_text, :string
  end
end
