class CreateDisputeRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :dispute_requests do |t|
      t.integer :user_id
      t.float :amount
      t.text :message
      t.string :status
      t.timestamps
    end
  end
end
