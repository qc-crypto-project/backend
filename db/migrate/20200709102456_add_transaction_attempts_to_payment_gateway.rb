class AddTransactionAttemptsToPaymentGateway < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_gateways, :transaction_attempts, :jsonb
  end
end
