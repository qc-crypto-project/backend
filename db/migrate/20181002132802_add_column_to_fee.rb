class AddColumnToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :transaction_fee_app, :string
    add_column :fees, :transaction_fee_app_dollar, :string
    add_column :fees, :transaction_fee_dcard, :string
    add_column :fees, :transaction_fee_dcard_dollar, :string
    add_column :fees, :transaction_fee_ccard, :string
    add_column :fees, :transaction_fee_ccard_dollar, :string
  end
end
