class AddColumnBankAccountTypeInBankDetails < ActiveRecord::Migration[5.1]
  def change
    add_column :bank_details,  :bank_account_type , :string
  end
end
