class AddAttachmentFrontCardImageToUsers < ActiveRecord::Migration[5.1]
  def self.up
    change_table :users do |t|
      t.attachment :front_card_image
    end
  end

  def self.down
    remove_attachment :users, :front_card_image
  end
end
