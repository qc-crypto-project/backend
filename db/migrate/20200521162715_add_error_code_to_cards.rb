class AddErrorCodeToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :error_code, :string
  end
end
