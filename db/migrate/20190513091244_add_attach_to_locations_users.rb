class AddAttachToLocationsUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :locations_users, :attach, :boolean, default: false
  end
end
