class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.integer :user_id
      t.jsonb :params
      t.datetime :first_date
      t.datetime :second_date
      t.string :status
      t.jsonb :file_id
      t.text :message
      t.string :report_type
      t.string :offset
      t.string :wallet
      t.string :send_via
      t.string :transaction_type

      t.timestamps
    end
  end
end
