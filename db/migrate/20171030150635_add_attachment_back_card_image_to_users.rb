class AddAttachmentBackCardImageToUsers < ActiveRecord::Migration[5.1]
  def self.up
    change_table :users do |t|
      t.attachment :back_card_image
    end
  end

  def self.down
    remove_attachment :users, :back_card_image
  end
end
