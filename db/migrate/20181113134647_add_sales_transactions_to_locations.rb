class AddSalesTransactionsToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :sales, :BOOLEAN
    add_column :locations, :virtual_terminal, :BOOLEAN
    add_column :locations, :close_batch, :BOOLEAN
  end
end
