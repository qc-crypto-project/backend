class AddReserveAndDaysToFees < ActiveRecord::Migration[5.1]
  def up
    add_column :fees, :reserve_fee, :float, default: 0
    add_column :fees, :days, :integer
  end

  def down
    remove_column :fees, :reserve_fee, :float, default: 0
    remove_column :fees, :days, :integer
  end
end
