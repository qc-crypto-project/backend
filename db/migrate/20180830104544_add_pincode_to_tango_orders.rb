class AddPincodeToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :pincode, :string
  end
end
