class AddStatusToEmailNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :email_notifications, :status, :string
  end
end
