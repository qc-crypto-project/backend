class AddCustomerListToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :customer_list, :boolean, default: false
  end
end
