class CreateCronJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :cron_jobs do |t|
      t.string :name
      t.jsonb :description
      t.integer :log_type
      t.timestamps
    end
  end
end
