class AddStatusToCronJob < ActiveRecord::Migration[5.1]
  def change
    add_column :cron_jobs, :status, :string
  end
end
