class AddDebitUnredeemedFeeToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :debit_unredeemed_fee, :float
    add_column :fees, :credit_unredeemed_fee, :float
  end
end
