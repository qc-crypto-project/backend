class AddFirst6ToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :first6, :string
    add_column :tango_orders, :last4, :string
  end
end
