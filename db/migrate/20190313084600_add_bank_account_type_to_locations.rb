class AddBankAccountTypeToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :bank_account_type, :integer
    add_index :locations, :bank_account_type
  end
end
