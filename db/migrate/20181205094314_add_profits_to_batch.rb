class AddProfitsToBatch < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :iso_profit, :float, default: 0
    add_column :batches, :agent_profit, :float, default: 0
    add_column :batches, :affiliate_profit, :float, default: 0
  end
end
