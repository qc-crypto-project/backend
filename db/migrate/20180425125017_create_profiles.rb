class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string  :company_name
      t.integer :business_type
      t.string  :ein
      t.string  :business_location
      t.string  :street_address
      t.string  :city
      t.string  :state
      t.string  :zip_code
      t.timestamps
    end
  end
end
