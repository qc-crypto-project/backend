class AddRefIdToActivityLog < ActiveRecord::Migration[5.1]
  def change
    add_column :activity_logs, :ref_id, :string
  end
end
