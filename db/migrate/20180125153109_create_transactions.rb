class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.string :to
      t.string :from
      t.string :amount
      t.string :status
      t.string :charge_id
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
