class AddMonthlyFeeDateToFee < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :service_date, :datetime
    add_column :fees, :statement_date, :datetime
    add_column :fees, :misc_date, :datetime
  end
end
