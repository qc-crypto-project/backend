class AddDeclineAttemptsToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :decline_attempts, :integer
  end
end
