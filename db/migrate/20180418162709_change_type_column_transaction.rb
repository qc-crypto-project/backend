class ChangeTypeColumnTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :action, :string
    remove_column :transactions, :type
  end
end
