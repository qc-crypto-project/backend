class AddBalanceCoulumToWallets < ActiveRecord::Migration[5.1]
  def change
    add_column :wallets, :balance, :string, default: '0'
  end
end
