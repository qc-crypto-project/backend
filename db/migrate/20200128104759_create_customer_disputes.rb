class CreateCustomerDisputes < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_disputes do |t|
      t.integer :transaction_id
      t.string :type
      t.text :reason
      t.string :status
      t.text :description
      t.float :amount
      t.integer :merchant_id
      t.integer :customer_id
      t.datetime :close_date
      t.timestamps
    end
  end
end
