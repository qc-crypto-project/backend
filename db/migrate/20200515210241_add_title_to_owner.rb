class AddTitleToOwner < ActiveRecord::Migration[5.2]
  def change
    add_column :owners, :title, :string  unless column_exists? :owners, :title
  end
end
