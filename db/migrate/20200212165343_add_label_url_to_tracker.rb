class AddLabelUrlToTracker < ActiveRecord::Migration[5.2]
  def change
    add_column :trackers, :label_url, :string
    add_column :trackers, :easypost_id, :jsonb
  end
end
