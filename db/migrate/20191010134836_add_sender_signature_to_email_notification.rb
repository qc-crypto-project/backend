class AddSenderSignatureToEmailNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :email_notifications, :sender_signature, :string
  end
end
