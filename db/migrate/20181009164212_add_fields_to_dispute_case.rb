class AddFieldsToDisputeCase < ActiveRecord::Migration[5.1]
  def change
  	add_column :dispute_cases, :dispute_type, :string, :default => ""
    add_column :dispute_cases, :recieved_date, :datetime
    add_column :dispute_cases, :due_date, :datetime
    add_column :dispute_cases, :amount, :float, :default => 0.0
    add_column :dispute_cases, :card_type, :string, :default => ""
    add_column :dispute_cases, :status, :string,:default => ""
    add_column :dispute_cases, :card_number, :float, :default => 0
    add_reference :dispute_cases, :reason
  end
end
