class AddNewTypeToBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :new_type, :integer
  end
end
