class AddSlugToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :slug, :string
    add_index :transactions, :slug, unique: true
  end
end
