class AddPaymentGatewayIdsToLoadBalancerRule < ActiveRecord::Migration[5.2]
  def change
    add_column :load_balancer_rules, :descriptor, :string
  end
end
