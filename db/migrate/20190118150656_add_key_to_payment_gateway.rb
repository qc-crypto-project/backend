class AddKeyToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :key, :string
    add_column :payment_gateways, :is_deleted, :boolean, default: false
  end
end
