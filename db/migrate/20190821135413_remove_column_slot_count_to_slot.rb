class RemoveColumnSlotCountToSlot < ActiveRecord::Migration[5.1]
  def change
    remove_column :slots, :slot_name
    remove_column :slots, :slot_rule
    remove_column :slots, :slot_count
  end
end