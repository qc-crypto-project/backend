class CreateOldPasswords < ActiveRecord::Migration[5.1]
  def change
    create_table :old_passwords do |t|
      t.string :encrypted_password, null: true
      t.string :password_archivable_type, null: true
      t.integer :password_archivable_id, null: true
      t.string :password_salt # Optional. bcrypt stores the salt in the encrypted password field so this column may not be necessary.
      t.datetime :created_at
    end
    add_index :old_passwords, [:password_archivable_type, :password_archivable_id], name: :index_password_archivable
  end
end
