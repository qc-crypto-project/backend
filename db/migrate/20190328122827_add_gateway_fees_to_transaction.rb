class AddGatewayFeesToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :account_processing_limit, :float, default: 0 unless Transaction.column_names.include?('account_processing_limit')
    add_column :transactions, :transaction_fee, :float, default: 0 unless Transaction.column_names.include?('transaction_fee')
    add_column :transactions, :charge_back_fee, :float, default: 0 unless Transaction.column_names.include?('charge_back_fee')
    add_column :transactions, :retrieval_fee, :float, default: 0 unless Transaction.column_names.include?('retrieval_fee')
    add_column :transactions, :per_transaction_fee, :float, default: 0 unless Transaction.column_names.include?('per_transaction_fee')
    add_column :transactions, :reserve_money_fee, :float, default: 0 unless Transaction.column_names.include?('reserve_money_fee')
    add_column :transactions, :reserve_money_days, :float, default: 0 unless Transaction.column_names.include?('reserve_money_days')
    add_column :transactions, :monthly_service_fee, :float, default: 0 unless Transaction.column_names.include?('monthly_service_fee')
    add_column :transactions, :misc_fee, :float, default: 0 unless Transaction.column_names.include?('misc_fee')
    add_column :transactions, :misc_fee_dollars, :float, default: 0 unless Transaction.column_names.include?('misc_fee_dollars')
  end
end
