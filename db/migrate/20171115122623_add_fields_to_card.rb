class AddFieldsToCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :brand, :string
    add_column :cards, :last4, :string
    add_column :cards, :exp_month, :string
    add_column :cards, :exp_year, :string
    add_column :cards, :customer, :string
  end
end
