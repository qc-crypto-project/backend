class AddBonusToBatch < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :total_bonus, :float, default: 0
  end
end
