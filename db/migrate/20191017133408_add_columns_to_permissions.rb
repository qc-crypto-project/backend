class AddColumnsToPermissions < ActiveRecord::Migration[5.1]
  def change
    add_column :permissions, :qr_scan, :boolean,default:false
    add_column :permissions, :qr_view_only, :boolean,default:false
    add_column :permissions, :qr_redeem, :boolean,default:false

  end
end
