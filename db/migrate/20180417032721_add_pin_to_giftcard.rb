class AddPinToGiftcard < ActiveRecord::Migration[5.1]
  def change
    add_column :giftcards, :pincode, :string
  end
end
