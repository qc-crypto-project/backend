class AddCardIdToTransaction < ActiveRecord::Migration[5.1]
  def change
  	add_reference :transactions, :card
  end
end
