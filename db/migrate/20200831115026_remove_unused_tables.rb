class RemoveUnusedTables < ActiveRecord::Migration[5.2]
  def change
    drop_table(:friendships, if_exists: true)
    drop_table(:withdrawls, if_exists: true)
    drop_table(:contacts, if_exists: true)
    drop_table(:accounts, if_exists: true)
    drop_table(:adds, if_exists: true, force: :cascade)
    drop_table(:dispute_notifications, if_exists: true)
    drop_table(:dispute_transactions, if_exists: true)
    drop_table(:videos, if_exists: true)
    drop_table(:escrows, if_exists: true)
  end
end
