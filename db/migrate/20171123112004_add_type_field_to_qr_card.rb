class AddTypeFieldToQrCard < ActiveRecord::Migration[5.1]
  def change
    add_column :qr_cards, :type, :string
  end
end
