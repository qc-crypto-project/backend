class CreateLoadBalancerRules < ActiveRecord::Migration[5.2]
  def change
    create_table :load_balancer_rules do |t|
      t.integer :operator
      t.integer :count
      t.float :amount
      t.integer :no_of_hours
      t.float :percentage
      t.string :country
      t.text :alert_receivers_emails
      t.string :decline_reason
      t.integer :rule_type
      t.integer :identifier
      t.integer :status
      t.integer :load_balancer_id
      t.string :card_brand

      t.timestamps
    end
    add_index :load_balancer_rules, :rule_type
    add_index :load_balancer_rules, :identifier
    add_index :load_balancer_rules, :status
    add_index :load_balancer_rules, :load_balancer_id
  end
end
