class AddUsageIdToImages < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :usage_id, :integer
  end
end
