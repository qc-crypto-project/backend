class AddStatusAndTitleFieldsToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :status, :string,  default:  "untreated"
    add_column :tickets, :title, :string
  end
end
