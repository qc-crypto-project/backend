class AddBlockApiToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :block_api, :boolean, default: false
    add_column :locations, :virtual_transaction_api, :boolean, default: false
    add_column :locations, :virtual_terminal_transaction_api, :boolean, default: false
    add_column :locations, :virtual_debit_api, :boolean, default: false
  end
end
