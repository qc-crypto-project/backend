class AddAssignedLocationFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :assigned_location_detail, :jsonb
    add_column :users, :assign_location, :boolean, default: false
  end
end
