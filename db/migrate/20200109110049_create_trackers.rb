class CreateTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.text :tracker_id
      t.string :tracker_code
      t.string :carrier
      t.string :signed_by
      t.datetime :est_delivery_date
      t.jsonb :tracking_details
      t.string :status
      t.bigint :user_id
      t.bigint :transaction_id

      t.timestamps
    end
    add_index :trackers, :user_id
    add_index :trackers, :transaction_id
  end
end
