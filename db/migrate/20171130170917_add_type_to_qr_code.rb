class AddTypeToQrCode < ActiveRecord::Migration[5.1]
  def change
    add_column :qr_cards, :type_of, :string
  end
end
