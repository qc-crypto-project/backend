class AddSenderReceiverToBlockTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :block_transactions, :sender_name, :string
    add_column :block_transactions, :receiver_name, :string
  end
end
