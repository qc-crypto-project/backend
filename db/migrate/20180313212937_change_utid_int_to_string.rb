class ChangeUtidIntToString < ActiveRecord::Migration[5.1]
  def change
    change_column :tango_orders, :utid, :string
    change_column :tango_orders, :order_id, :string
  end
end
