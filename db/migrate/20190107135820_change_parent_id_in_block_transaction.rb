class ChangeParentIdInBlockTransaction < ActiveRecord::Migration[5.1]
  def change
    change_column :block_transactions, :seq_parent_id, :string
  end
end
