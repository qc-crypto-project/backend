class ChangeColumnNameOfAtmTouser < ActiveRecord::Migration[5.1]
  def change
    rename_column :fees , :atm_id , :user_id
  end
end
