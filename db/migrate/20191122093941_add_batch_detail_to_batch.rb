class AddBatchDetailToBatch < ActiveRecord::Migration[5.2]
  def change
    add_column :batches, :batch_detail, :jsonb
  end
end
