class AddPinCodeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :pin_code, :string
  end
end
