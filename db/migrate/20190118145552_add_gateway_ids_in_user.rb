class AddGatewayIdsInUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :primary_gateway_id, :integer
    add_column :users, :secondary_gateway_id, :integer
    add_column :users, :ternary_gateway_id, :integer
  end
end
