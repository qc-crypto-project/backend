class AddFromWalletIdToRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :requests, :from_wallet_id, :integer
  end
end
