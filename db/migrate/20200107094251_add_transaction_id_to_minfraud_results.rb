class AddTransactionIdToMinfraudResults < ActiveRecord::Migration[5.2]
  def change
    add_column :minfraud_results, :transaction_id, :integer
    add_index :minfraud_results, :transaction_id
  end
end
