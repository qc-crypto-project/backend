class CreateMinfraudResults < ActiveRecord::Migration[5.2]
  def change
    create_table :minfraud_results do |t|
      t.float :risk_score
      t.jsonb :insight_detail

      t.timestamps
    end
  end
end
