class CreateBatches < ActiveRecord::Migration[5.1]
  def change
    create_table :batches do |t|
      t.date :close_date
      t.time :close_time
      t.integer :total_transactions
      t.float :total_sales
      t.boolean :is_closed, default: false

      t.timestamps
    end

    add_reference :batches, :user, index: true
    add_reference :batches, :wallet, index: true
  end
end
