class AddCurrentTierToBatch < ActiveRecord::Migration[5.1]
  def change
    add_column :batches, :current_tier, :integer, default: 1
  end
end
