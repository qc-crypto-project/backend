class CreateQrCards < ActiveRecord::Migration[5.1]
  def change
    create_table :qr_cards do |t|
      t.boolean :vaild, default: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
