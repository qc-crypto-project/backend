class CreateAppConfigs < ActiveRecord::Migration[5.1]
  def change
    create_table :app_configs do |t|
      t.string :key, :null => false
      t.string :stringValue
      t.boolean :boolValue, :default => true
      t.timestamps
    end
  end
end
