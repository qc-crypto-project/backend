class AddCustomerFeeToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :product_fee, :float
    add_column :transactions, :fee_method, :string
    add_column :transactions, :discount_method, :string
  end
end
