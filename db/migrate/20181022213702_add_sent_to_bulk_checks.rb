class AddSentToBulkChecks < ActiveRecord::Migration[5.1]
  def change
    add_column :bulk_checks, :sent, :boolean, default: false
  end
end
