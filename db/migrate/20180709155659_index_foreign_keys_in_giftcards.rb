class IndexForeignKeysInGiftcards < ActiveRecord::Migration[5.1]
  def change
    add_index :giftcards, :company_id
  end
end
