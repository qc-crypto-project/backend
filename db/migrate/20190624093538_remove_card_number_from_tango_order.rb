class RemoveCardNumberFromTangoOrder < ActiveRecord::Migration[5.1]
  def change
    remove_column :tango_orders, :card_number
    remove_column :tango_orders, :expiry_date
    remove_column :tango_orders, :cvv
  end
end
