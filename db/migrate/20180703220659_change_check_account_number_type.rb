class ChangeCheckAccountNumberType < ActiveRecord::Migration[5.1]
  def change
    change_column :tango_orders, :account_number, :string
    change_column :tango_orders, :routing_number, :string
    change_column :tango_orders, :amount, :float
  end
end
