class AddCardJsonToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :card_json, :json
  end
end
