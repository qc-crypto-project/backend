class AddSecondStripeIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :second_stripe_id, :string
  end
end
