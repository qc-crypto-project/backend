class AddBalanceColumnsToBatch < ActiveRecord::Migration[5.2]
  def change
    add_column :transaction_batches, :iso_balance, :float, default: 0
    add_column :transaction_batches, :agent_balance, :float, default: 0
    add_column :transaction_batches, :affiliate_balance, :float, default: 0
  end
end
