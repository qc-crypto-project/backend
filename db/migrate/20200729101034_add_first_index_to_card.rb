class AddFirstIndexToCard < ActiveRecord::Migration[5.2]
  def change
    add_index :cards, [:first6, :last4]
    add_index :cards, :fingerprint
    add_index :cards, [:fingerprint, :merchant_id]
  end
end
