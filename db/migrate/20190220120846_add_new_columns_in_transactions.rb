class AddNewColumnsInTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions,:fee, :float unless Transaction.column_names.include?('fee')
    add_column :transactions, :tip, :float unless Transaction.column_names.include?('tip')
    add_column :transactions, :privacy_fee, :float unless Transaction.column_names.include?('privacy_fee')
    add_column :transactions, :reserve_money, :float unless Transaction.column_names.include?('reserve_money')
  end
end
