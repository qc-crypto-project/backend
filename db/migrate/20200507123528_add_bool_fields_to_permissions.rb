class AddBoolFieldsToPermissions < ActiveRecord::Migration[5.2]
  def change
    add_column :permissions, :accounts, :boolean, default: false
    add_column :permissions, :withdrawals, :boolean, default: false
    add_column :permissions, :e_checks, :boolean, default: false
    add_column :permissions, :push_to_cards, :boolean, default: false
    add_column :permissions, :achs, :boolean, default: false
    add_column :permissions, :giftcards, :boolean, default: false
    add_column :permissions, :invoices, :boolean, default: false
    add_column :permissions, :exports, :boolean, default: false
    add_column :permissions, :business_settings, :boolean, default: false
    add_column :permissions, :employee_list, :boolean, default: false
    add_column :permissions, :permission_list, :boolean, default: false
    add_column :permissions, :risk_analysis, :boolean, default: false
    add_column :permissions, :chargebacks, :boolean, default: false
    add_column :permissions, :all, :boolean, default: false
    add_column :permissions, :view_accounts, :boolean, default: false
    add_column :permissions, :account_transfer, :boolean, default: false
    add_column :permissions, :tip_transfer, :boolean, default: false
    add_column :permissions, :export_daily_batch, :boolean, default: false
  end
end
