class AddConfirmationCodeEncToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :confirmation_code_enc, :text
  end
end
