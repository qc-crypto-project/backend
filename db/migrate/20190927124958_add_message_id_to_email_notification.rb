class AddMessageIdToEmailNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :email_notifications, :message_id, :string
  end
end
