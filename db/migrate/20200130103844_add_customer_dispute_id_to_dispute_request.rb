class AddCustomerDisputeIdToDisputeRequest < ActiveRecord::Migration[5.2]
  def change
    add_column  :dispute_requests,:customer_dispute_id,:integer
  end
end
