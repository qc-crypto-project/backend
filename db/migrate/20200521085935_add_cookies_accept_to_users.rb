class AddCookiesAcceptToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :cookies_accept, :boolean, default: false
  end
end
