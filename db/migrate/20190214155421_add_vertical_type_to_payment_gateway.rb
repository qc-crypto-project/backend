class AddVerticalTypeToPaymentGateway < ActiveRecord::Migration[5.1]
  def change
    add_column :payment_gateways, :vertical_type_id, :integer
  end
end
