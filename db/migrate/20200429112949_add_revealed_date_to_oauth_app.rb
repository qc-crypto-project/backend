class AddRevealedDateToOauthApp < ActiveRecord::Migration[5.2]
  def change
  	add_column :oauth_apps , :revealed_date , :datetime
  end
end
