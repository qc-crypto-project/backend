class CreateSlots < ActiveRecord::Migration[5.1]
  def change
    create_table :slots do |t|
      t.integer :slot_name
      t.text :slot_rule
      t.integer :slot_count, default: 0
      t.integer :location_id

      t.timestamps
    end
  end
end
