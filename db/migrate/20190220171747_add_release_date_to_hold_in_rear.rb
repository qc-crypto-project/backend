class AddReleaseDateToHoldInRear < ActiveRecord::Migration[5.1]
  def change
    add_column :hold_in_rears, :release_time, :datetime
  end
end
