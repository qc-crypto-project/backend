class AddHoldPendingDeliveryAndDaysToLocations < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :on_hold_pending_delivery, :boolean, default: false
    add_column :locations, :on_hold_pending_delivery_days, :integer, default: 1
  end
end
