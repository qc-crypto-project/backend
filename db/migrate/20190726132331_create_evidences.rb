class CreateEvidences < ActiveRecord::Migration[5.1]
  def change
    create_table :evidences do |t|

      t.timestamps
      t.string :billing_address
      t.string :package_carrier
      t.string :tracking_number
      t.string :shipping_address
      t.date :shipping_date
      t.integer :dispute_id
      t.string :name
      t.string :customer_ip
      t.string :email
    end
  end
end
