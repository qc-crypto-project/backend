class AddOrderBankIdToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :order_bank_id, :integer
  end
end
