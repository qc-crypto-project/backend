class AddColumnBakedUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :baked_users, :name, :string
    add_column :baked_users, :active, :boolean
  end
end
