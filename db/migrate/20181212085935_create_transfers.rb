class CreateTransfers < ActiveRecord::Migration[5.1]
  def change
    create_table :transfers do |t|
      t.integer :from_wallet
      t.integer :to_wallet
      t.float :amount, default: 0
      t.date :transfer_date
      t.string :memo
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
