class AddEcomPlatformToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :ecom_platform, :string
  end
end
