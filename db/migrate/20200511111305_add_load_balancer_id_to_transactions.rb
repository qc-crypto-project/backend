class AddLoadBalancerIdToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :load_balancer_id, :integer
    add_index :transactions, :load_balancer_id
  end
end
