class AddLedgerToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :ledger, :string
  end
end
