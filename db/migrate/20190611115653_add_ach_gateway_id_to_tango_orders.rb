class AddAchGatewayIdToTangoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :ach_gateway_id, :integer
    add_index :tango_orders, :ach_gateway_id
  end
end
