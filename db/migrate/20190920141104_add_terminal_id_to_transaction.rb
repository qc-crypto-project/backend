class AddTerminalIdToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :terminal_id, :string
  end
end
