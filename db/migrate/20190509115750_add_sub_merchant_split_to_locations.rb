class AddSubMerchantSplitToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :sub_merchant_split, :boolean, default: false
  end
end
