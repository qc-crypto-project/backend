class AddFieldsToWallet < ActiveRecord::Migration[5.1]
  def change
    add_attachment :wallets, :qr_image, :string
    add_column :wallets, :token, :string
  end
end
