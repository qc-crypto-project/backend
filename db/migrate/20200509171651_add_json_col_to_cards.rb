class AddJsonColToCards < ActiveRecord::Migration[5.2]
  def change
    remove_column :cards, :processed_attempts, :integer
    remove_index :cards, :last_gateway
    remove_column :cards, :last_gateway, :integer
    add_column :cards,:transaction_attempts, :jsonb
  end
end
