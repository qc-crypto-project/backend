class RemoveColumnInAsset < ActiveRecord::Migration[5.1]
  def change
    remove_column :assets, :file_type
  end
end
