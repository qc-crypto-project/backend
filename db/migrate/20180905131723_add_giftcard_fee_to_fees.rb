class AddGiftcardFeeToFees < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :giftcard_fee, :string
  end
end
