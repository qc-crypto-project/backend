class AddDdaToLocation < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :dda_type, :string
  end
end
