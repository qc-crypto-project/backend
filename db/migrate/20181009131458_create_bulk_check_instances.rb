class CreateBulkCheckInstances < ActiveRecord::Migration[5.1]
  def change
    create_table :bulk_check_instances do |t|
      t.string :bulk_type
      t.integer :wallet
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
