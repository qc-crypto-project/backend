class AddStreetColumnToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :street ,:string
  end
  def down
    remove_column :users, :street ,:string
  end
end
