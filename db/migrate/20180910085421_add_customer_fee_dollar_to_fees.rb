class AddCustomerFeeDollarToFees < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :customer_fee_dollar, :string
  end
end
