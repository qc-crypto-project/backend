class AddLocationIdToUsers < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :location_id ,:integer
  end
  def down
    remove_column :users, :location_id ,:integer
  end
end
