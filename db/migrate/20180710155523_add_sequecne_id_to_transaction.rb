class AddSequecneIdToTransaction < ActiveRecord::Migration[5.1]
  def up
    add_column :transactions, :seq_transaction_id, :text
  end
  def down
    remove_column :transactions, :seq_transaction_id, :text
  end
end
