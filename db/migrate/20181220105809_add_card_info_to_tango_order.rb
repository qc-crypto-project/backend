class AddCardInfoToTangoOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :tango_orders, :account_token, :json
  end
end
