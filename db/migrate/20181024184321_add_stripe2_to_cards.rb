class AddStripe2ToCards < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :stripe2, :string
  end
end
