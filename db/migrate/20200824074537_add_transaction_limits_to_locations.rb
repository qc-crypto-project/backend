class AddTransactionLimitsToLocations < ActiveRecord::Migration[5.2]
  def change
  	add_column :locations , :transaction_limit , :boolean, default: false
  	add_column :locations , :transaction_limit_offset , :float , array: true , default: [] 
  end
end
