class AddGenerateLabelFeeToFee < ActiveRecord::Migration[5.2]
  def change
    add_column :fees, :generate_label_fee, :float
    add_column :fees, :tracking_fee, :float
  end
end
