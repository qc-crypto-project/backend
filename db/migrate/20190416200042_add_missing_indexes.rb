class AddMissingIndexes < ActiveRecord::Migration[5.1]
  def change
    add_index :activity_logs, :user_id
    add_index :audits, [:associated_id, :associated_type]
    add_index :audits, [:auditable_id, :auditable_type]
    add_index :block_transactions, :receiver_id
    add_index :block_transactions, :receiver_wallet_id
    add_index :block_transactions, :sender_id
    add_index :block_transactions, :sender_wallet_id
    add_index :config_log_details, :location_id
    add_index :config_logs, :location_id
    add_index :config_logs, :user_id
    add_index :dispute_cases, :merchant_wallet_id
    add_index :dispute_cases, :user_wallet_id
    add_index :fees, :user_id
    add_index :fees_locations, :fee_id
    add_index :fees_locations, :location_id
    add_index :fees_locations, [:fee_id, :location_id]
    add_index :images, :comment_id
    add_index :images, :request_id
    add_index :images, :ticket_id
    add_index :locations, :primary_gateway_id
    add_index :locations, :secondary_gateway_id
    add_index :locations, :ternary_gateway_id
    add_index :locations_users, [:location_id, :user_id]
    add_index :notes, [:notable_id, :notable_type]
    add_index :notifications, :actor_id
    add_index :notifications, :recipient_id
    add_index :notifications, :read_at
    add_index :notifications, :action
    add_index :notifications, [:action, :read_at]
    add_index :notifications, [:notifiable_id, :notifiable_type]
    add_index :requests, :reciever_id
    add_index :requests, :sender_id
    add_index :transactions, :sender_id
    add_index :users, :primary_gateway_id
    add_index :users, :secondary_gateway_id
    add_index :users, :ternary_gateway_id
    add_index :users, :app_id
    add_index :users_wallets, [:user_id, :wallet_id]
  end
end
