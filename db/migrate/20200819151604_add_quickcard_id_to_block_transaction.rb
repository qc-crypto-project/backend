class AddQuickcardIdToBlockTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :block_transactions, :quickcard_id, :string
    add_index :block_transactions, :quickcard_id, unique: true
  end
end
