class AddUserNameToEmailMessage < ActiveRecord::Migration[5.1]
  def change
    add_column :ahoy_messages, :user_name, :string
  end
end
