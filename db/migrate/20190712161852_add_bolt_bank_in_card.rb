class AddBoltBankInCard < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :bolt_bank, :string
  end
end
