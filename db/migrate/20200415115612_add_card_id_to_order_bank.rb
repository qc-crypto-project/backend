class AddCardIdToOrderBank < ActiveRecord::Migration[5.2]
  def change
    add_column :order_banks, :card_id, :integer
  end
end
