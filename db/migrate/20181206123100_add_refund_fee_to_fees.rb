class AddRefundFeeToFees < ActiveRecord::Migration[5.1]
  def change
    add_column :fees, :refund_fee, :float, default: 0
  end
end
