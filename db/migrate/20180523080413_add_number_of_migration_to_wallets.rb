class AddNumberOfMigrationToWallets < ActiveRecord::Migration[5.1]
  def up
    add_column :wallets , :total_transactions , :integer
  end
  def down
    remove_column :wallets , :total_transactions , :integer
  end
end
