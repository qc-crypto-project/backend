class AddSenderReceiverToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :sender_name, :string
    add_column :transactions, :receiver_name, :string
  end
end
