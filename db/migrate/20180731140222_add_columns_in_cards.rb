class AddColumnsInCards < ActiveRecord::Migration[5.1]
  def change
    add_column :cards, :bridge_pay, :string
    add_column :cards, :cvv, :string
  end
end
