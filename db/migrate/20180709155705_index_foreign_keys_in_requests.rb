class IndexForeignKeysInRequests < ActiveRecord::Migration[5.1]
  def change
    add_index :requests, :wallet_id
  end
end
