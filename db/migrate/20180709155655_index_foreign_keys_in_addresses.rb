class IndexForeignKeysInAddresses < ActiveRecord::Migration[5.1]
  def change
    add_index :addresses, :location_id
    add_index :addresses, :user_id
  end
end
