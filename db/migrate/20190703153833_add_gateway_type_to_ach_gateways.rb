class AddGatewayTypeToAchGateways < ActiveRecord::Migration[5.1]
  def change
    add_column :ach_gateways, :gateway_type, :integer
  end
end
