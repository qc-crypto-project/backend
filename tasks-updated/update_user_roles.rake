# namespace :user_roles do
#   desc "update user roles to enums"
#   task update_enum: :environment do
#     puts 'Updating User Role Enums'
#     puts 'ALL USER ROLES ARE FIXED ALREADY WITH THIS TASK...'
#     return
#     begin
#       id = nil
#       job = CronJob.new(name: 'user_roles:update_enum', description: 'Update User Role Enums', success: true)
#       result = {updated_users: [], not_updated_users: [], error: nil}
#       User.all.each do |user|
#         id = user.id
#         if user.role.blank?
#           puts "Updating User Role Enums For USER ID: #{user.id} & EMAIL: #{ user.email}"
#           role = user.roles.first
#           if role.present? && role.title.present?
#             if role.title.downcase == 'admin'
#               user.admin!
#             elsif role.title.downcase == 'user'
#               user.user!
#             elsif role.title.downcase == 'merchant'
#               user.merchant!
#             elsif role.title.downcase == 'atm'
#               user.atm!
#             elsif role.title.downcase == 'qc'
#               user.qc!
#             elsif role.title.downcase == 'app'
#               user.app!
#             elsif role.title.downcase == 'agent'
#               user.agent!
#             elsif role.title.downcase == 'iso'
#               user.iso!
#             elsif role.title.downcase == 'affiliate'
#               user.affiliate!
#             elsif role.title.downcase == 'partner'
#               user.partner!
#             elsif role.title.downcase == 'gift_card'
#               user.gift_card!
#             else
#               result[:not_updated_users] << user.id
#               puts 'NO ROLE FOR USER ID: ', user.id
#               return
#             end
#             result[:updated_users] << user.id if role.title.present?
#           else
#             result[:not_updated_users] << user.id
#             puts 'NO ROLE FOR USER ID: ', user.id
#           end
#         end
#       end
#     rescue StandardError => exc
#       job.success = false
#       result[:error] = "Error: #{exc.message} for #{id}"
#       puts 'Error occured during ENUM Update: ', exc
#     end
#     job.result = result
#     job.rake_task!
#     puts 'Role Enums Updated Successfully!'
#   end
# end
