# namespace :gateway_charge do
#   desc "update Wrong Charge Ids"
#   task fix_charge_id: :environment do
#     puts 'Updating Charge_Ids'
#     begin
#       id = nil
#       job = CronJob.new(name: 'gateway_charge:fix_charge_id', description: 'Update/Fix wrong Charge Ids for transactions', success: true)
#       result = {updates: [], error: nil}
#       charges = []
#       Transaction.all.each do |trans|
#         if trans.charge_id.present? && trans.charge_id.include?('message') && trans.charge_id.include?('response')
#           puts "Updating charge_id for Transaction id: #{trans.id}"
#           id = trans.id
#           st_charge_id = trans.charge_id
#           st_charge_id = st_charge_id.gsub('"', '')
#           st_charge_id = st_charge_id.gsub('{:response=>', '')
#           st_charge_id = st_charge_id.gsub(', :message=>nil}', '')
#           charges.push(st_charge_id)
#           result[:updates].push({id: trans.id, old_value: trans.charge_id, new_value: st_charge_id})
#           trans.update(charge_id: st_charge_id)
#         end
#       end
#     rescue StandardError => exc
#       job.success = false
#       result[:error] = "Error: #{exc.message} for #{id}"
#       puts 'Error occured during Charge_Ids Update: ', exc
#     end
#     job.result = result
#     job.rake_task!
#     puts 'Charge_Ids Updated Successfully!'
#     puts "Total of #{charges.length} Transactions updated..."
#   end
# end
