# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def transaction_email
    location = Location.find(1)
    amount = 45
    gateway = "converge"
    user = User.user.first
    date = Time.now.to_date
    card = Card.last
    card_number = "42424242424242"
    UserMailer.transaction_email(location.id,amount,gateway,user.id,date,card.brand,card_number,"asdasdasdadasdadsada")
  end
  # def welcome_merchant
  #   user = User.find(14)
  #   password = "password"
  #   UserMailer.welcome_merchant(user,password);
  # end

  def refund_email
    location = Location.find(5)
    user = User.find(5)
    amount = 100
    date = Date.today
    card_brand = "Visa"
    card_number = "424242"
    UserMailer.refund_email(location.id,amount, user.id, date,card_brand, card_number, "3ebe8b61cb99ff0efb0f90169ae77235c6222a62d8c35eee81684578c3cc7af7")
  end

  def welcome
    data = [{"wallet_id"=>"100", "dba_name"=>"Ajmal", "bank_name"=>"Adeel User", "bank_account"=>"111111", "bank_routing"=>"111111111", "type"=>"checking"},
            {"wallet_id"=>"12", "dba_name"=>"Think", "bank_name"=>"Adeel User", "bank_account"=>"2222222222", "bank_routing"=>"222222222", "type"=>"savings"},
            {"wallet_id"=>"8", "dba_name"=>"Clipper location", "bank_name"=>"Adeel User", "bank_account"=>"3333333333", "bank_routing"=>"333333333", "type"=>"business"}]
    UserMailer.ach_request(data.to_json,User.find(6).id)
  end

  def ach_status_change_email
    check = TangoOrder.find 59
    UserMailer.ach_status_change_email(check);
  end
  def welcome_user
   @user  = User.find 22
   @message = "abc"
   @password= "abcvf"
   UserMailer.welcome_user(@user, @message, 'agent', @password , @key, @secret , @auth_apps)
  end
  def send_export_notification
    @user = User.last
    UserMailer.send_export_notification(@user)
  end
  def invoice_pdf_email
    UserMailer.invoice_pdf_email("111",Request.last.id)

  end
end
