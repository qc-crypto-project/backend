FactoryBot.define do
  factory :email_notification do
    subject { "MyString" }
    body { "MyText" }
    scdedule_date { "2019-09-25 18:57:48" }
    from_id { 1 }
  end
end
