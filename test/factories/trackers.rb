FactoryBot.define do
  factory :tracker do
    tracker_id { "MyText" }
    tracker_code { "MyString" }
    carrier { 1 }
    signed_by { "MyString" }
    est_delivery_date { "2020-01-09 16:00:49" }
    tracking_details { "" }
    status { 1 }
    user_id { "" }
    transaction_id { "" }
  end
end
