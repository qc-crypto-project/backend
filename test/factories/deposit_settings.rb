FactoryBot.define do
  factory :deposit_setting do
    user_id { 1 }
    balance { 1.5 }
  end
end
