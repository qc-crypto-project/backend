FactoryBot.define do
  factory :load_balancer_rule do
    operator { 1 }
    count { 1 }
    amount { 1.5 }
    no_of_hours { 1 }
    percentage { 1.5 }
    country { "MyString" }
    alert_receivers_emails { "MyText" }
    decline_reason { "MyString" }
    rule_type { 1 }
    identifier { 1 }
    status { 1 }
    load_balancer_id { 1 }
    card_brand { "MyString" }
  end
end
