FactoryBot.define do
  factory :customer_merchant do
    customer_id { 1 }
    merchant_id { 1 }
    location_id { 1 }
  end
end
