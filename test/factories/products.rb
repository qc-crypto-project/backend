FactoryBot.define do
  factory :product do
    name { "MyString" }
    description { "MyString" }
    amount { 1.5 }
    quantity { 1 }
    transaction_id { 1 }
  end
end
