FactoryBot.define do
  factory :minfraud_result do
    risk_score { 1.5 }
    insight_detail { "" }
  end
end
