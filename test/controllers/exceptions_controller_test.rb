require 'test_helper'

class ExceptionsControllerTest < ActionDispatch::IntegrationTest
  test "should get 400" do
    get exceptions_400_url
    assert_response :success
  end

  test "should get 404" do
    get exceptions_404_url
    assert_response :success
  end

  test "should get 500" do
    get exceptions_500_url
    assert_response :success
  end

end
