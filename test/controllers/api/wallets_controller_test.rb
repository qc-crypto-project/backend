require 'test_helper'

class Api::WalletsControllerTest < ActionDispatch::IntegrationTest
  test "the truth" do
    assert true
  end

  test "wallets show" do
    get "/api/wallet", params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742"}, as: :json
    assert_response :success
  end

  test "issue amount" do
    get "/api/wallets/issue", params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742", amount: "300"}, as: :json
    json = JSON.parse(response.body)
    assert_nil json[:message]
  end
 
  test "issue amount with missing params" do
    get "/api/wallets/issue", params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742"}, as: :json
    json = JSON.parse(response.body)
    assert_operator json[:message].length, :>, 1
  end
  
  test "issue amount with negtive value" do
    get "/api/wallets/issue", params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742",amount: -2000}, as: :json
    json = JSON.parse(response.body)
    assert_nil json[:message]
  end

  test "withdraw amount" do
    get "/api/wallets/withdraw",params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742", amount: "300"}, as: :json
    json = JSON.parse(response.body)
    assert_nil json[:message]
  end

  test "withdraw amount negtive value" do
    get "/api/wallets/withdraw",params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742", amount: "-300"}, as: :json
    json = JSON.parse(response.body)
    assert_nil json[:message]
  end

  test "search" do
    get "/api/wallets/search", params: {search: "dgSd"}, as: :json
    assert_response :success
  end

  test "create card for stripe" do
    post "/api/wallets/cards", params: {auth_token: "f6de9d0f0099a3cb93084e5a493422a3e082c01c",token_id: "tok_Blyy8gtOEslHHF&name=adsf"}, as: :json
    assert_response :success
  end

  test "get all cards" do
    get "/api/wallets/cards", params: {auth_token: "f6de9d0f0099a3cb93084e5a493422a3e082c01c"}, as: :json
    assert_response :success
  end

  test "Issue with stripe" do
    get "/api/wallets/issue_with_stripe", params: {auth_token: "f6de9d0f0099a3cb93084e5a493422a3e082c01c", amount: "30000" ,card_token: "card_BlzIGkPSa5lw0y"}, as: :json
    assert_response :success
  end

  test "Issue with stripe negtive" do
    get "/api/wallets/issue_with_stripe", params: {auth_token: "f6de9d0f0099a3cb93084e5a493422a3e082c01c", amount: "-30000" ,card_token: "card_BlzIGkPSa5lw0y"}, as: :json
    assert_response :success
  end

  test "withdraw" do
    get "/api/wallets/withdraw", params: {auth_token: "41f4220ba50b5c980f491e9666f0c9e12cef9742" ,amount: "99971"}, as: :json
    json = JSON.parse(response.body) 
    assert_response :success
  end

  test "transfer" do
    get "/api/wallets/transaction_between", params: {auth_token: "c788d7cae77ed475c206abcdca7820347038a8bd",amount: "99", to_id: "3", from_id: "2",message: "helo"} , as: :json
    assert_response :success
  end 

  test "transfer greater amoiunt" do
    get "/api/wallets/transaction_between", params: {auth_token: "c788d7cae77ed475c206abcdca7820347038a8bd",amount: "9900000000000000000", to_id: "3", from_id: "2",message: "helo"} , as: :json
    assert_response :success
  end

  test "create card" do
    post "/api/wallets/cards", params: {auth_token: "f6de9d0f0099a3cb93084e5a493422a3e082c01c", token_id: "tok_Blyy8gtOEslHHF",name: "adsf"}
    assert_response :success
  end

  test "all cards" do
    get "/api/wallets/cards", params: {auth_token: "f6de9d0f0099a3cb93084e5a493422a3e082c01c"}
    puts response.body
  end
end
