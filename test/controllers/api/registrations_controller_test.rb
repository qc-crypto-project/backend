

require 'test_helper'

class Api::RegistrationsControllerTest < ActionDispatch::IntegrationTest
  test "should post registrations" do
    post '/api/registrations', params: {email: 'shakeel', phone_number: 923300000}, as: :json
    assert_response :success
  end
  
  test "update profile" do
    post "/api/registrations/update", params: {postal_address: "lahore",auth_token: "e3976c540c3e639187cef9285e2b306c1f6095ab", id: 117}
    assert_response :success
  end

  test "check for email" do
    get "/api/registrations/check_email_phone" , params: {email: "shaek@hma.com", phone_number: "9230000"}
    assert_response :success
  end
end
