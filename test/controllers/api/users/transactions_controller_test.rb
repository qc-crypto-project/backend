require 'test_helper'

class Api::Users::TransactionsControllerTest < ActionDispatch::IntegrationTest
  test "should get webhook" do
    get api_users_transactions_webhook_url
    assert_response :success
  end

end
