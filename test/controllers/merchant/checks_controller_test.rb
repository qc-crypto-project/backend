require 'test_helper'

class Merchant::ChecksControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get merchant_checks_index_url
    assert_response :success
  end

  test "should get new" do
    get merchant_checks_new_url
    assert_response :success
  end

  test "should get create" do
    get merchant_checks_create_url
    assert_response :success
  end

  test "should get edit" do
    get merchant_checks_edit_url
    assert_response :success
  end

  test "should get update" do
    get merchant_checks_update_url
    assert_response :success
  end

  test "should get delete" do
    get merchant_checks_delete_url
    assert_response :success
  end

end
