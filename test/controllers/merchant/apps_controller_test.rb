require 'test_helper'

class Merchant::AppsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get merchant_apps_index_url
    assert_response :success
  end

  test "should get create" do
    get merchant_apps_create_url
    assert_response :success
  end

  test "should get new" do
    get merchant_apps_new_url
    assert_response :success
  end

  test "should get edi" do
    get merchant_apps_edi_url
    assert_response :success
  end

  test "should get update" do
    get merchant_apps_update_url
    assert_response :success
  end

end
