require 'test_helper'

class Admins::LoadBalancersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admins_load_balancers_index_url
    assert_response :success
  end

  test "should get new" do
    get admins_load_balancers_new_url
    assert_response :success
  end

end
