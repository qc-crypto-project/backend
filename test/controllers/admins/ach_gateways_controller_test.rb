require 'test_helper'

class Admins::AchGatewaysControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admins_ach_gateway = admins_ach_gateways(:one)
  end

  test "should get index" do
    get admins_ach_gateways_url
    assert_response :success
  end

  test "should get new" do
    get new_admins_ach_gateway_url
    assert_response :success
  end

  test "should create admins_ach_gateway" do
    assert_difference('Admins::AchGateway.count') do
      post admins_ach_gateways_url, params: { admins_ach_gateway: { archive: @admins_ach_gateway.archive, transaction_limit: @admins_ach_gateway.transaction_limit , bank_fee_dollar: @admins_ach_gateway.bank_fee_dollar, bank_fee_percent: @admins_ach_gateway.bank_fee_percent, bank_name: @admins_ach_gateway.bank_name, descriptor: @admins_ach_gateway.descriptor, status: @admins_ach_gateway.status } }
    end


    assert_redirected_to admins_ach_gateway_url(Admins::AchGateway.last)
  end

  test "should show admins_ach_gateway" do
    get admins_ach_gateway_url(@admins_ach_gateway)
    assert_response :success
  end

  test "should get edit" do
    get edit_admins_ach_gateway_url(@admins_ach_gateway)
    assert_response :success
  end

  test "should update admins_ach_gateway" do
    patch admins_ach_gateway_url(@admins_ach_gateway), params: { admins_ach_gateway: { archive: @admins_ach_gateway.archive, transaction_limit: @admins_ach_gateway.transaction_limit,bank_fee_dollar: @admins_ach_gateway.bank_fee_dollar, bank_fee_percent: @admins_ach_gateway.bank_fee_percent, bank_name: @admins_ach_gateway.bank_name, descriptor: @admins_ach_gateway.descriptor, status: @admins_ach_gateway.status } }
    assert_redirected_to admins_ach_gateway_url(@admins_ach_gateway)
  end

  test "should destroy admins_ach_gateway" do
    assert_difference('Admins::AchGateway.count', -1) do
      delete admins_ach_gateway_url(@admins_ach_gateway)
    end

    assert_redirected_to admins_ach_gateways_url
  end
end
