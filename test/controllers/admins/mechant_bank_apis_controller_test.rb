require 'test_helper'

class Admins::MechantBankApisControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admins_mechant_bank_apis_index_url
    assert_response :success
  end

  test "should get create" do
    get admins_mechant_bank_apis_create_url
    assert_response :success
  end

end
