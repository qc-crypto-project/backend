require 'test_helper'

class Admins::RolesControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get admins_roles_edit_url
    assert_response :success
  end

  test "should get update" do
    get admins_roles_update_url
    assert_response :success
  end

end
