# == Schema Information
#
# Table name: users
#
#  id                            :bigint(8)        not null, primary key
#  email                         :string           default("")
#  encrypted_password            :string           default(""), not null
#  reset_password_token          :string
#  reset_password_sent_at        :datetime
#  remember_created_at           :datetime
#  sign_in_count                 :integer          default(0), not null
#  current_sign_in_at            :datetime
#  last_sign_in_at               :datetime
#  current_sign_in_ip            :inet
#  last_sign_in_ip               :inet
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  name                          :string
#  postal_address                :string
#  phone_number                  :string
#  status                        :boolean
#  user_image_file_name          :string
#  user_image_content_type       :string
#  user_image_file_size          :integer
#  user_image_updated_at         :datetime
#  front_card_image_file_name    :string
#  front_card_image_content_type :string
#  front_card_image_file_size    :integer
#  front_card_image_updated_at   :datetime
#  back_card_image_file_name     :string
#  back_card_image_content_type  :string
#  back_card_image_file_size     :integer
#  back_card_image_updated_at    :datetime
#  authentication_token          :string
#  admin                         :boolean          default(FALSE)
#  is_block                      :boolean          default(TRUE)
#  confirmation_code             :string
#  date_of_birth                 :string
#  issue_date                    :string
#  expire_date                   :string
#  stripe_customer_id            :string
#  pin_code                      :string
#  device_token                  :string
#  device_type                   :string
#  archived                      :boolean          default(FALSE)
#  registeration_id              :string
#  username                      :string
#  client_id                     :string
#  client_secret                 :string
#  app_id                        :integer
#  redirect_url                  :string
#  denominations                 :text             default("{1 => 0, 2 => 0, 5 => 0, 10 => 0, 20 => 0, 50 => 0, 100 => 0}")
#  oauth_code                    :string
#  first_name                    :string
#  last_name                     :string
#  card_json                     :json
#  company_id                    :integer
#  role                          :integer
#  fee                           :float
#  plain_password_text           :string
#  zip_code                      :string
#  merchant_id                   :integer
#  address                       :string
#  city                          :string
#  state                         :string
#  street                        :string
#  location_id                   :integer
#  issue_fee                     :boolean          default(TRUE)
#  retail_sales                  :boolean          default(FALSE)
#  owner_name                    :string
#  confirmation_code_enc         :text
#  slug                          :string
#  ref_no                        :string
#  source                        :string
#  payment_gateway               :integer
#  fluid_pay_customer_id         :string           default("")
#  ledger                        :string
#  submerchant_type              :string
#  area_code                     :string           default("")
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
