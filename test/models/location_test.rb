# == Schema Information
#
# Table name: locations
#
#  id                    :bigint(8)        not null, primary key
#  business_name         :string
#  first_name            :string
#  last_name             :string
#  email                 :string
#  web_site              :string
#  address               :text
#  business_type         :string
#  years_in_business     :string
#  category              :string
#  city                  :string
#  state                 :string
#  zip                   :string
#  social_security_no    :string
#  tax_id                :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  phone_number          :string
#  merchant_id           :integer
#  location_secure_token :string
#  iso_allowed           :boolean          default(FALSE)
#  agent_allowed         :boolean          default(FALSE)
#  is_block              :boolean          default(FALSE)
#  slug                  :string
#  sale_limit            :float
#  sale_limit_percentage :float
#  ledger                :string
#  block_withdrawal      :boolean          default(FALSE)
#

require 'test_helper'

class LocationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
