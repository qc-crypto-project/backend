# == Schema Information
#
# Table name: fees
#
#  id                           :bigint(8)        not null, primary key
#  customer_fee                 :string
#  b2b_fee                      :string
#  redeem_fee                   :string
#  send_check                   :string
#  gbox                         :string
#  partner                      :string
#  iso                          :string
#  agent                        :string
#  fee_id                       :integer
#  fee_status                   :integer
#  location_id                  :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  buy_qc_card_cash             :float
#  buy_qc_card_cc               :float
#  atm_processing_fee           :float
#  user_id                      :integer
#  add_money_check              :string
#  redeem_check                 :string
#  giftcard_fee                 :string
#  reserve_fee                  :float            default(0.0)
#  days                         :integer
#  customer_fee_dollar          :string
#  transaction_fee_app          :string
#  transaction_fee_app_dollar   :string
#  transaction_fee_dcard        :string
#  transaction_fee_dcard_dollar :string
#  transaction_fee_ccard        :string
#  transaction_fee_ccard_dollar :string
#  charge_back_fee              :float            default(0.0)
#  retrievel_fee                :float            default(0.0)
#

require 'test_helper'

class FeeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
