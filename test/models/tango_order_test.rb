# == Schema Information
#
# Table name: tango_orders
#
#  id                 :bigint(8)        not null, primary key
#  user_id            :bigint(8)
#  utid               :string
#  amount             :float
#  order_id           :string
#  account_identifier :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  catalog_image      :string
#  checkId            :string
#  description        :text
#  direction          :string
#  name               :string
#  number             :string
#  recipient          :string
#  sender             :string
#  status             :string
#  wallet_id          :integer
#  order_type         :integer
#  account_number     :string
#  routing_number     :string
#  actual_amount      :float
#  check_fee          :float
#  fee_perc           :float
#  gift_card_fee      :float
#  pincode            :string
#  settled            :boolean          default(TRUE)
#  notes              :string
#

require 'test_helper'

class TangoOrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
