class NotifyUserSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :name, :phone_number, :user_image
end
