# == Schema Information
#
# Table name: settings
#
#  id                   :bigint(8)        not null, primary key
#  push_notification    :boolean          default(FALSE)
#  touch_id_login       :boolean          default(FALSE)
#  touch_id_transaction :boolean          default(FALSE)
#  user_id              :bigint(8)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class SettingSerializer < ActiveModel::Serializer
  attributes :id, :push_notification,:touch_id_login,:touch_id_transaction, :updated_at,:user_id
  
  def self.eager_load_relation(relation)
    relation.includes(:user)
  end

  belongs_to :user
end
