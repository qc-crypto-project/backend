# == Schema Information
#
# Table name: videos
#
#  id         :bigint(8)        not null, primary key
#  video_url  :string
#  add_id     :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class VideoSerializer < ActiveModel::Serializer
  attributes :video_url

  # def self.eager_load_relation(relation)
  #   relation.includes(:add)
  # end

  # belongs_to :add
end
