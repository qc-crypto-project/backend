class AtmSerializer < ActiveModel::Serializer
  attributes :email, :name, :postal_address, :phone_number, :authentication_token, :denominations

  def denominations
    eval(object.denominations)
  end
end
