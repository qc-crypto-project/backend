# == Schema Information
#
# Table name: cards
#
#  id         :bigint(8)        not null, primary key
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  brand      :string
#  last4      :string
#  customer   :string
#  name       :string
#  stripe     :string
#  elavon     :string
#  payeezy    :string
#  exp_date   :string
#  bridge_pay :string
#  cvv        :string
#  card_type  :string
#  fluid_pay  :string           default("")
#  archived   :boolean          default(FALSE)
#

class CardSerializer < ActiveModel::Serializer
  attributes :id, :token , :brand ,:last4, :exp_month, :exp_year, :customer, :elavon_token
end
