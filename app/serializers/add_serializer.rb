# == Schema Information
#
# Table name: adds
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(8)
#  add_url    :string
#

class AddSerializer < ActiveModel::Serializer
  attributes :category, :videos, :images

  def self.eager_load_relation(relation)
    relation.includes(:images)
    relation.includes(:videos)
  end

  def videos
    object.videos.pluck(:video_url)
  end

  def images
    object.images.map{|i| i.add_image.url}
  end
end
