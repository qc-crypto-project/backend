class OnlyuserSerializer < ActiveModel::Serializer
  attributes :id, :email, :name, :postal_address, :phone_number, :user_image, :front_card_image, :role, :first_name, :last_name, :card_json

  def role
    object.role if object.role.present?
    return 'No Role'
  end
end
