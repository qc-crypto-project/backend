# == Schema Information
#
# Table name: wallets
#
#  id                    :bigint(8)        not null, primary key
#  name                  :string
#  type                  :string
#  user_id               :bigint(8)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  qr_image_file_name    :string
#  qr_image_content_type :string
#  qr_image_file_size    :integer
#  qr_image_updated_at   :datetime
#  string_file_name      :string
#  string_content_type   :string
#  string_file_size      :integer
#  string_updated_at     :datetime
#  token                 :string
#  balance               :string           default("0")
#  oauth_app_id          :bigint(8)
#  wallet_type           :integer
#  total_amount          :float
#  total_sales           :float
#  total_transactions    :integer
#  location_id           :integer
#  slug                  :string
#

class WalletSerializer < ActiveModel::Serializer
  attributes :id, :name,:type,:user_id,:created_at,:updated_at, :qr_image

  def self.eager_load_relation(relation)
    relation.includes(:users)
    relation.includes(:qr_cards)
    relation.includes(:giftcards)
  end

  has_many :users
  has_many :qr_cards
  has_many :giftcards
end
