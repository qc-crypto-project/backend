# == Schema Information
#
# Table name: comments
#
#  id         :bigint(8)        not null, primary key
#  body       :text
#  user_id    :bigint(8)
#  ticket_id  :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CommentSerializer < ActiveModel::Serializer
  attributes :id,:body,:ticket_id,:created_at,:user_name, :user_id

  def self.eager_load_relation(relation)
    relation.includes(:user)
  end

  def user_name
    object.user.admin? ? "QC Support" :  object.user.name
  end
end
