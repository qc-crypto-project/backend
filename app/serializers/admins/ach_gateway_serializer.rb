class Admins::AchGatewaySerializer < ActiveModel::Serializer
  attributes :id, :bank_name, :descriptor, :status, :archive, :bank_fee_percent, :bank_fee_dollar , :transaction_limit
end
