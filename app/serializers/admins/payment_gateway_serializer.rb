class Admins::PaymentGatewaySerializer < ActiveModel::Serializer
  attributes :id, :type, :name, :client_secret, :client_id, :user_id
end
