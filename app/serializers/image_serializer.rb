# == Schema Information
#
# Table name: images
#
#  id                     :bigint(8)        not null, primary key
#  add_image_file_name    :string
#  add_image_content_type :string
#  add_image_file_size    :integer
#  add_image_updated_at   :datetime
#  add_id                 :bigint(8)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  request_id             :integer
#  ticket_id              :integer
#  comment_id             :integer
#  usage_id               :integer
#

class ImageSerializer < ActiveModel::Serializer
  attributes :id ,:add_image
end
