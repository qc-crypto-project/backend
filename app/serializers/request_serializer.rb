# == Schema Information
#
# Table name: requests
#
#  id                  :bigint(8)        not null, primary key
#  amount              :string
#  status              :string
#  sender_id           :integer
#  reciever_id         :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  wallet_id           :string
#  sender_name         :string
#  reciever_name       :string
#  sender_access_token :string
#  pin_code            :string
#  description         :text
#

class RequestSerializer < ActiveModel::Serializer
  attributes :id, :amount,:status,:sender_id, :reciever_id,:created_at,:updated_at,:wallet_id
end
