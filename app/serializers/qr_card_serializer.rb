# == Schema Information
#
# Table name: qr_cards
#
#  id                 :bigint(8)        not null, primary key
#  is_valid           :boolean          default(TRUE)
#  user_id            :bigint(8)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  price              :string
#  wallet_id          :bigint(8)
#  token              :string
#  message            :string
#  category           :string
#  from_id            :integer
#  type_of            :string
#  tip                :float
#

class QrCardSerializer < ActiveModel::Serializer
  attributes :id,:is_valid,:user_id, :price,:wallet_id,:token,:message,:from_id
end
