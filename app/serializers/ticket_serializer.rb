# == Schema Information
#
# Table name: tickets
#
#  id            :bigint(8)        not null, primary key
#  message       :string
#  user_id       :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  status        :string           default("untreated")
#  title         :string
#  slug          :string
#  device_type   :string
#  time_stamp    :string
#  get_attention :boolean          default(TRUE)
#

class TicketSerializer < ActiveModel::Serializer
  attributes :id

  def self.eager_load_relation(relation)
    relation.includes(:comments)
  end
  has_many :comments
end
