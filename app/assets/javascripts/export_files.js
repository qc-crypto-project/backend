$('#alert-message-top').fadeOut(5000);
$(function() {
    $(".exportReport").on('click', function () {
        var my_var = $(this).data('my-variable');
        // if (my_var === true){
        $("#body_done").hide();
        $("#no_tx").hide();
        $('#export_btn').hide();
        $("#export_close2").show();
        $('#body_1').hide();
        $('#body_2').show();
        $('.export-print').show();
        // $('#send_merchant_email_div').show();
        $('#body_3').hide();
        $('#body_4').hide();
        $('#body_6').hide();
        $("#export_done_btn").hide();
        $("#export_orginal_btn").hide();
        var email = $("#send_email").val();
        // setting_text(email);
        var selected = $(this).data('date');
        var start_date = $(this).data('refund_start_date');
        var end_date = $(this).data('refund_end_date');
        var export_date = $("#exportDate").val();
        var type = $(this).data('type');
        var status = $(this).data('status');
        var wallet = $(this).data('wallet');
        var trans = $(this).data('subtype');
        var wallet_id = $(this).data('wallet_id');
        if (wallet == ""){
            wallet = $("#wallet_id").val();
        }
        var send_via = $(this).data('send_via');
        var offset = moment().local().format('Z');
        var search_query = {
            name:  $(this).data('search-name'),
            email: $(this).data('search-email'),
            phone: $(this).data('search-phone'),
            amount: $(this).data('search-amount'),
            date: $(this).data('search-date'),
            last4: $(this).data('search-last4'),
            wallets_id: $(this).data('search-wallets_id'),
            DBA_name: $(this).data('name'),
            Receiver_name: $(this).data('search-Receiver_name'),
            type: $(this).data('search-type'),
            type1: $(this).data('search-type1'),
            time1: $(this).data('search-time1'),
            time2: $(this).data('search-time2'),
            first6: $(this).data('search-first6'),
            transaction_id: $(this).data('search-transaction-id'),
            export_date: export_date
        };
        $.ajax({
            url: "/merchant/transactions/generate_export_file",
            dataType: "json",
            data: {trans: trans, wallet_id: wallet_id, start_date: start_date, end_date: end_date,date: selected, type: type, status: status, wallet: wallet, send_via: send_via, offset: offset, search_query: search_query,email: email}
        }).done(function(response, status, ajaxOpts) {
            if (status === "success" && response && response.status == "completed") {
                $('#export_btn').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                $("#body_6").hide();
                if(response.file_id == "" || response.file_id == null || response.file_id == "null"){
                    $("#no_tx").show();
                }else{
                    $("#body_4").show();
                    $('#download_link').attr('href', "/merchant/transactions/export_download?id="+response.file_id)
                }
            }else if (status === "success" && response && (response.status == "pending" || response.status == "crashed")) {
                if (response.status == "crashed") {
                    $('#export_btn').hide();
                    $("#transaction_heading_2").show();
                    $("#transaction_heading").hide();
                    // $("#send_merchant_email_div").hide();
                    $("#body_2").show();
                    $(".export-print").hide();
                    $('#send_merchant_email_div').hide();
                    // $("#body_2").hide();

                    // $('#body_message > .export-status').text('');
                    // $('.export-status').hide();
                    $("#body_message").hide();
                    $(".export-heading").text('').text(response.heading);
                    $('.export-status').text('').text(response.message);
                    $("#body_1").hide();
                    $("#body_6").hide();
                    $("#message_h5").hide();
                    $("#message_p").hide();
                    $("#body_message").hide();
                }
                else{
                    $('#export_btn').hide();
                    $("#transaction_heading_2").show();
                    $("#transaction_heading").hide();
                    $("#body_2").hide();
                    $("#body_1").hide();
                    $("#body_6").hide();
                    $("#body_message").show();
                    $("#message_h5").text('').text('Alert!')
                    $("#message_p").text('').text(response.message);
                    $("#printcount").text('');
                }
            }else{
                $('#export_btn').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $(".export-heading").text('');
                $('.export-status').text('');
                $("#body_2").show();
                $("#email_user_1").show();
                global.jobId = response.jid;
                global.intervalName = "job_" + jobId;
            }
        }).fail(function(error) {
            console.log(error);
        });
        // }
        // else {
        //     $("#body_1").hide();
        //     $("#body_6").show();
        //     $("#dummy_export_btn").hide();
        //     $("#export_orginal_btn").show();
        // }

    });
    $("#send_merchant_email").on('click', function(){
        $('#body_message').hide();
        $('#body_done').hide();
    });
    $("#export_close1").on('click', function(){
        $('#body_1').show();
        $('#export_btn').show();
        $('#body_2').hide();
        $('#send_merchant_email_div').hide()
        $('#body_3').hide();
        $("#body_done").hide();
        $("#no_tx").hide();
    });

    $("#export_btn").on("click", function(e) {
        $("#body_done").hide();
        $("#no_tx").hide();
        $("#export_close2").show();
        $('#body_1').hide();
        $('#body_2').show();
        $('.export-print').show();
        $("#export_orginal_btn").hide();
        $('#body_3').hide();
        $('#body_4').hide();
        $('#body_5').hide();
        $("#body_6").hide();
        $("#export_done_btn").hide();
        $("#export_btn1").hide();
        var selected = $("#exportDate").val();
        var type = $(this).data('type');
        var wallet = $(this).data('wallet');
        var sub_type = $(this).data('subtype');
        var wallet_id = $(this).data('wallet_id');
        var send_via = $(this).data('send_via');
        var offset = moment().local().format('Z');
        var email = $("#send_email").val();
        // setting_text(email);
        $.ajax({
            url: "/merchant/transactions/generate_export_file",
            dataType: "json",
            data: {date: selected, type: type,wallet_id: wallet_id, wallet: wallet, send_via: send_via, offset: offset, trans: sub_type,email: email}
        }).done(function(response, status, ajaxOpts) {
            if (status === "success" && response && response.status == "completed") {
                $('#export_btn').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                $("#body_6").hide();
                if(response.file_id == "" || response.file_id == null || response.file_id == "null"){
                    $("#no_tx").show();
                }else{
                    $("#body_4").show();
                    $('#download_link').attr('href', "/merchant/transactions/export_download?id="+response.file_id)
                }
            }else if (status === "success" && response && (response.status == "pending" || response.status == "crashed")) {
                if (response.status == "crashed") {
                    $('#export_btn').hide();
                    $("#transaction_heading_2").show();
                    $("#transaction_heading").hide();
                    // $("#send_merchant_email_div").hide();
                    $("#body_2").show();
                    $(".export-print").hide();
                    $('#send_merchant_email_div').hide();
                    // $("#body_2").hide();

                    // $('#body_message > .export-status').text('');
                    // $('.export-status').hide();
                    $("#body_message").hide();
                    $(".export-heading").text('').text(response.heading);
                    $('.export-status').text('').text(response.message);
                    $("#body_1").hide();
                    $("#body_6").hide();
                    $("#message_h5").hide();
                    $("#message_p").hide();
                    $("#body_message").hide();
                }
                else{
                $('#export_btn').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                $("#body_6").hide();
                $("#body_message").show();
                $("#message_h5").text('').text('Alert!')
                $("#message_p").text('').text(response.message);
                $("#printcount").text('');}
            }else{
                $('#export_btn').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $(".export-heading").text('');
                $('.export-status').text('');
                $("#body_2").show();
                $("#email_user_1").show();
                $('#hidden_job_id').val(response.jid);
                global.jobId = response.jid;
                global.intervalName = "job_" + jobId;
            }
        }).fail(function(error) {
            // $("#export_atag_button").html('').html('<i class="fa fa-file"></i> Export').removeAttr('disabled','disabled');
            console.log(error);
        });
    });

    function setting_text(email) {
        var current_email = $('#current_email').val();
        $(".export-heading").text('');
        $('.export-status').text('');
        $('.export-print').show();
        // $('#send_merchant_email_div').show()
        // $("#body_2").show();
        $("#body_6").hide();
        $("#export_orginal_btn").hide();
        if(email == "true"){
            $(".export-status").html('').html("<span>We will send an email as soon as your export is ready to download.</span>");
        }else {
            // $(".export-status").html('').html("<i class='fa fa-spinner fa-spin'></i> Please wait, Calculating total transactions may take a few minutes.");
        }
    }

    $("#export_close2").on('click', function(){
        $('#body_1').show();
        $('#export_btn').show();
        $('#body_2').hide();
        $('#send_merchant_email_div').hide()
        $('#body_3').hide();
        $('#body_4').hide();
        $('#body_6').hide();
        $("#export_done_btn").hide();
        $("#body_message").hide();
        $("#body_done").hide();
        $("#no_tx").hide();
    });

    $("#export_done_btn").on('click', function(){
        $("#export_close2").show();
        $("#export_done_btn").hide();
        $('#body_4').hide();
        $("#body_done").hide();
        $("#no_tx").hide();
    })
    $('#exportModalDialog').on('hidden.bs.modal', function () {
    // if ($("#hidden_kill_job").val() == 'true' && $("#hidden_job_id").val() != ''){
    //     job_id = $("#hidden_job_id").val();
    //     $.ajax({
    //         url: "/merchant/transactions/kill_job",
    //         type: 'GET',
    //         data: {job_id: job_id},
    //         success: function(data) {
    //         },
    //         error: function(data){
    //         }
    //     });
    // }
    $('#body_1').show();
    $('#export_btn').show();
    $('#body_2').hide();
    $('#send_merchant_email_div').hide()
    $('#body_3').hide();
    $('#body_4').hide();
    $('#download_link').removeClass('disabled');
    $('#body_6').hide();
    $("#export_done_btn").hide();
    $("#body_message").hide();
    $("#body_done").hide();
    $("#no_tx").hide();
    // $('.export_print').text('').text('Please wait, Calculating total transactions may take a few minutes.');
    $(".export-heading").text('');
    $('.export-status').text('');
    // $("#send_merchant_email_div").hide();
    // $("#dummy_export_btn").show();
    $("#export_orginal_btn").show();

    })

    $("#send_merchant_email").on('click', function () {
        var hidden_field_merchant_email = $("#save_cron_job").val()
        $.ajax({
            url: "/merchant/transactions/update_email_status",
            dataType: "json",
            data: {hidden_field_merchant_email: hidden_field_merchant_email}
        })

    });

    //**************** ISO Search Export *****************//

    $(".exportModal").on('click', function () {
        // $("#export_atag_button").html('').html('<i class="fa fa-spinner fa-spin"></i> Export').attr('disabled','disabled');
        $("#body_done").hide();
        $("#no_tx").hide();
        $('#export_btn').hide();
        $('#export_btn1').hide();
        $('#dummy-export').show();
        $("#export_close2").show();
        $('#body_1').hide();
        $('#body_2').hide();
        $('#body_3').hide();
        $('#body_4').hide();
        $('#body_5').show();
        $("#export_done_btn").hide();

        $("#name").val($(this).data('search-name'));
        $("#email").val($(this).data('search-email'));
        $("#phone").val($(this).data('search-phone'));
        $("#amount").val($(this).data('search-amount'));
        $("#date").val($(this).data('search-date'));
        $("#wallets_id").val($(this).data('search-wallets_id'));
        $("#DBA_name").val($(this).data('name'));
        $("#Receiver_name").val($(this).data('search-Receiver_name'));
        $("#search_type").val($(this).data('search-type'));
        $("#time1").val($(this).data('search-time1'));
        $("#time2").val($(this).data('search-time2'));
        $("#selected").val($(this).data('date'));
        $("#start_date").val($(this).data('refund_start_date'));
        $("#end_date").val($(this).data('refund_end_date'));
        $("#type").val($(this).data('type'));
        $("#last4").val($(this).data('search-last4'),);
        $("#wallet").val($(this).data('wallet'));
        $("#wallet_id").val($(this).data('wallet_id'));
        $("#send_via").val($(this).data('send_via'));
        $("#status").val($(this).data('status'));
        $("#trans").val($(this).data('subtype'));
        $("#offset").val(moment().local().format('Z'));
    });

    $("#dummy_export_btn").on('click', function () {
        if($("#exportDate").val().length > 0) {
            $("#body_1").hide();
            $("#body_6").show();
            $("#dummy_export_btn").hide();
            $("#export_orginal_btn").show();
        }
    })

    $("#email_check").on('click', function () {
        var checked = $(this).prop('checked');
        if(checked){
            $("#send_email").val('true');
            $("#button_exp").text('').text('Send')
        }else{
            $("#send_email").val('false')
            $("#button_exp").text('').text('Export')
        }
    })
});
