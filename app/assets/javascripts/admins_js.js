// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//= require js/jquery-2.1.4.min
//= require jquery_ujs
// require turbolinks
//= require tether
//= require bootstrap
//= require adminss
//= require vendor
//= require app
//= require form
//= require cable
//= require admins/jquery.easyPaginate
//= require bootstrap-datepicker
//= require jquery.form
//= require admin-validator
//= require password_validation
//= require merchant/jstz.min
//= require admins/export_files
//= require admins/sync_transactions
//= require merchant/jquery.inputmask.bundle
//= require select2
//= #require admins/reports
//= require best_in_place
//= require jquery.purr
//= require best_in_place.purr
//= require jquery_nested_form
//= require Chart.bundle
//= require chartkick
//= require intlTelInput.js
//= require moment
//= require bootstrap-datetimepicker
//= require admin_shared
//= require merchant/jquery.multiselect

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
function get_bank_by_routing_number(routing_number,url) {
    var routing_number=routing_number;
    if(routing_number!=""){
        $.ajax({
            url: url,
            method: 'post',
            data: {routing_number: routing_number},
            success: function (result) {
                var bank_name=result.response;
                if(bank_name!="") {
                    $('#routing-error').text('').hide();
                    $('#success-div').text(result.response);
                    $('#success-div').show();
                }
                else {
                    $('#routing-error').text('Cannot Find Bank With The Routing Number').show();
                    $('#success-div').text('');
                    $('#success-div').hide();
                }
            },
            error: function(){
                $('#routing-error').text('Cannot Find Bank With The Routing Number error').show();
                $('#success-div').text('');
                $('#success-div').hide();
            }
        });
    }
}

(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };
}(jQuery));
$(".percent").inputFilter(function(value) {
    return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100);});
$(".two_decimal").inputFilter(function(value) {
    return /^\d*(\.\d{0,2})?$/.test(value);});

    $(document).on('click', "#cc_reports", function () {
        var url = $(this).data("url");
        show_declines(url)
    });
    $(document).on('keyup', ".amount_field",function () {
        $(".amount_field").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    });

    // $(document).on('keyup', ".percentage_validation",function () {
    //     // $(".percentage_validation").inputmask('Regex', {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"});
    //     // if(parseFloat($(this).val()) > 100){
    //     //     alert("Percentage can't be greater than 100.");
    //     //     $(this).val($(this).val().slice(0,-1));
    //     // }
    //     $(".percentage_validation").inputFilter(function(value) {
    //         return /^(?:\d{1,2}(?:\.\d{1,2})?|100(?:\.0?0)?)$/.test(value);
    //         // {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"}
    //     });
    // });
    $(document).ready(function () {
        update_transaction_url();
        update_dispute_url();
        update_cc_reports_url()
        update_all_reports_url()
    })
      function update_transaction_url(){
          var  url = $('#transactions').data("url");
          var id = "transactions"
          var add_params = "?trans=success&offset=" + encodeURIComponent(moment().local().format('Z'))
          update_url(id,url,add_params)
      }
      function update_dispute_url(){
          var  url = $('#dispute_cases').data("url");
          var id = "dispute_cases"
          var add_params = "?case=all"
          update_url(id,url,add_params)
      }
      function update_cc_reports_url(){
          var  url = $('#cc_reports').data("url");
          var id = "cc_reports"
          var add_params = "?query[date]=" + moment().local().format('MM/DD/YYYY')+"-"+moment().local().format('MM/DD/YYYY') + "&offset=" + encodeURIComponent(moment().local().format('Z'))
          update_url(id,url,add_params)
      }
      function update_all_reports_url(){
          var  url = $('#cc_reports').attr("href");
          var id = "all_reports"
          var add_params = "?query[date]=" + moment().local().format('MM/DD/YYYY') + " - " + moment().local().format('MM/DD/YYYY') + "&offset=" + encodeURIComponent(moment().local().format('Z'))
          update_url(id,url,add_params)
      }
      function update_url(id,url,add_params){
        var a = $("#"+id).attr("href", url+add_params)
      }
    $(document).on('click', "#dispute_cases", function () {
        var url = $(this).data("url");
        show_dispute(url)
    });
    $(document).on('click', "#transactions", function () {
        var url = $(this).data("url");
        show_transactions(url)
    });

    $(document).on('click', "#merchant_configs_path", function () {
        var url = $(this).data("url");
        show_active_gateway(url)
    });
    $(document).on('click', "#inactive_gateway", function () {
        var url = $(this).data("url");
        show_inactive_gateway(url)
    });
    $(document).on('click', "#active_gateway", function () {
        var url = $(this).data("url");
        show_gateway(url, "active")
    });
    $(document).on('change', "#gateway_entries_form", function () {
        $(this).submit();
    });

    function show_declines(url) {
        window.location.href = url + "?query[date]=" + moment().local().format('MM/DD/YYYY') + " - " + moment().local().format('MM/DD/YYYY') + "&offset=" + encodeURIComponent(moment().local().format('Z'))
    }
    function show_dispute(url) {
        window.location.href = url + "?case=all"
    }

    function show_transactions(url) {
        window.location.href = url + "?trans=success&offset=" + encodeURIComponent(moment().local().format('Z'))
    }
    function show_active_gateway(url) {
        window.location.href = url + "?status=active"
    }
    function show_inactive_gateway(url) {
        window.location.href = url + "?status=inactive"
    }
    function show_gateway(url,status) {
        window.location.href =
            url + "?status="+ status
    }

    function value_present(val) {
        if(val == ""){
            return false;
        }else if(val == null){
            return false;
        }else if(val == undefined){
            return false;
        }else if(val == 0) {
            return false;
        }else{
            return true;
        }

    }

$(document).ready(function(){
    $(".show-dynamic-modal").on("click", function (ev) {
        ev.preventDefault();
        var url = $(this).data("url");
        var modal_selector = $(this).data("modal_selector");
        var modal_container_selector = $(this).data("modal_container_selector");
        var method = $(this).data("method");
        $.ajax({
            url: url,
            type: method,
            success: function (data) {
                $(modal_container_selector).empty().html(data);
                $(modal_selector).modal();
                $('#cover-spin').fadeOut();
                validateUserForm("greenbox_user_");
                activate_phone_number_plugin()
            }
        });
        return false;
    });

    $(document).on('click', ".show-cover-spin", function () {
        $('#cover-spin').show(0)
    });

    $('.show-cover-spin').on('click',function(){
        $('#cover-spin').show();
    });

    $('.custom-cover').on('click',function(){
        $('.custom-cover').addClass('pointer-none-only');
        $('#cover-spin').show();
    });

    $(document).on("click", ".show-dynamic-modal", function (ev) {
        ev.preventDefault();
        var url = $(this).data("url");
        var modal_selector = $(this).data("modal_selector");
        var modal_container_selector = $(this).data("modal_container_selector");
        var method = $(this).data("method");
        $.ajax({
            url: url,
            type: method,
            success: function (data) {
                $(modal_container_selector).empty().html(data);
                $(modal_selector).modal();
                $('#cover-spin').fadeOut();
            }
        });
        return false;
    });


    if ($("#location_ach_flag").is(':checked')) {
        $("#ach_flag_text").show();
    }

    $(document).on('change', '#location_ach_flag', function () {
        if ($(this).is(':checked')) {
            $("#ach_flag_text").show();
        } else {
            $("#ach_flag_text").hide();
        }
    });

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    var select_reason,reason_detail,transaction_detail,tran_Id,destination,source,refund_fee,total_amount,partial_amt;
    var check1 = false, check2 = false;
    $('.next_transactions').click(function () {
        $('#cover-spin').fadeIn();
    });
    $(document).on("click", ".refundModalDialog", function () {
        $('#reason_select option[value="0"]').prop("selected",true);
        $('#partialamount').val('');
        $('#reason_detail').val('');
        $('#reason_detail').hide();
        $('.refund_fee').text('0');
        $('.modal-footer a button').prop('disabled',true);
        transaction_detail=JSON.stringify($(this).data('transaction'));
        parsed_transction=JSON.parse(transaction_detail);
        tran_Id = parsed_transction.id;
        parent_id=parsed_transction.parent_id;
        destination=parsed_transction.receiver_wallet_id;
        source=parsed_transction.sender_wallet_id;
        $.ajax({
            url: '/admins/refund_check',
            type: 'GET',
            data: {trans_id: tran_Id},
        })
            .done(function () {
                $.ajax({
                    url: '/admins/get_partialAmount',
                    type: 'GET',
                    data: {trans_id: tran_Id,parent_id: parent_id,destination: destination,source: source},
                })
                    .done(function (res) {
                        if(res.partial_amount==""){
                            $('#partial_error').text("No Transaction Found");
                            $('#partial_error').fadeIn();
                            setTimeout(function(){
                                $('#partial_error').fadeOut()
                            }, 2000);
                        }else{
                            $('#partialamount').val(parseFloat(res.partial_amount).toFixed(2));
                            total_amount=parseFloat(res.partial_amount);
                            partial_amt=total_amount;
                            last4="";
                            if(parsed_transction["tags"]["card"]){
                                if(parsed_transction["tags"]["card"]["last4"]){
                                    last4=parsed_transction["tags"]["card"]["last4"];
                                }
                            }else{
                                if(parsed_transction["tags"]["previous_issue"]){
                                    if(parsed_transction["tags"]["previous_issue"]["last4"]){
                                        last4=parsed_transction["tags"]["previous_issue"]["last4"];
                                    }
                                }
                            }
                            $('#refund_user').text(last4);
                            refund_fee = res.fee;
                            var fee = refund_fee + parseFloat(res.partial_amount);
                            $('.refund_fee').text(parseFloat(fee).toFixed(2));
                            $('#confirm_refund_amount').text(parseFloat(res.partial_amount).toFixed(2));
                        }
                    });
            });

    });
    $(document).on('change','#partialamount', function(){
        select_reason=$('#reason_select').val();
        var partial_amount = $('#partialamount').val();
        partial_amt=partial_amount;
        if (partial_amount != "0") {
            $.ajax({
                url: '/admins/get_partialAmount',
                type: 'GET',
                data: {trans_id: tran_Id,destination: destination,source: source,amount:partial_amount},
            })
                .done(function (res) {
                    var partial= res.partial_amount - parseFloat(partial_amount);
                    if (partial < 0){
                        $('#partial_error').text("Amount is greater than amount to refund");
                        $('#partial_error').fadeIn();
                        setTimeout(function(){
                            $('#partial_error').fadeOut()
                        }, 2000);

                        $('.modal-footer a button').prop('disabled',true);
                        check1 = false;
                    }
                    else {
                        $('#partial_error').text("");
                        check1  =true;
                        refund_fee=res.fee;
                        var fee = refund_fee+parseFloat(partial_amount);
                        $('.refund_fee').text(parseFloat(fee).toFixed(2));
                        $('#confirm_refund_amount').text(parseFloat(partial_amount).toFixed(2));
                    }
                    if (check1 && check2) {
                        if(select_reason!=0){
                            $('.modal-footer a button').prop('disabled',false);
                        }
                    }
                });
        }
        updateRefundPostLink();
    });

    function updateRefundPostLink() {
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        reason_detail=$("#reason_select option:selected").text();
        if (select_reason==0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false;
        } else if (select_reason!=4 && select_reason!=0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('#footerConfirmLink').attr('href',"/admins/refund_transaction"+"?js=js&refund_fee="+refund_fee+"&reason=" + select_reason + "&partial_amount=" + partial_amount + "&reason_detail=" + encodeURIComponent(reason_detail) + "&transaction=" + tran_Id);
            if(total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
            check2 = true;
        } else{
            $('#reason_detail').show();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false
        }
        if (check1 && check2) {
            if (total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
        }
    }

    $('#reason_select').change(function(){
        updateRefundPostLink();
    });

    $('#reason_detail').keyup(function () {
        reason_detail=$(this).val();
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        if (reason_detail=='' || reason_detail == null){
            $('.modal-footer a button').prop('disabled', true);
        } else{
            $('#footerConfirmLink').attr('href',"/admins/refund_transaction"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + tran_Id);
//            $('#footerConfirmLink').attr('href',"<%#= refund_transaction_admins_path %>"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + encodeURIComponent(transaction_detail));
            if(total_amount >= partial_amount){
                $('#refundModalFooter a button').prop('disabled',false);
            }
        }
    });
    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    })
});
    function activate_phone_number_plugin(){
        $('.auto_off').on('focus',function(){
            $(this).attr('autocomplete', 'off');
        });
        $('.auto_off').attr('readonly', 'readonly');
        $("input[readonly]").css("background-color", "white");
        $('.auto_off').click(function () {
            $('.auto_off').removeAttr('readonly');
        });
        $('.auto_off').prop('autocomplete', Math.random().toString(36).slice(2));
        $("#phone_number_cor").intlTelInput({
            formatOnInit: true,
            separateDialCode: true,
            formatOnDisplay: false,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                country=selectedCountryPlaceholder;
                country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                $('#phone_code_cor').val(Object.values(selectedCountryData)[2]);
                $('#phone_number_cor').val('');
                space_remove(Object.values(selectedCountryData)[2], 'phone_number_cor');
                if(selectedCountryData.iso2 == 'us')
                {
                    country = "555-555-5555"
                }
                return  country;
            },
            excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
            utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
        });

        $('#phone_number_cor').each(function () {
            var id = $(this).attr('id');
            var phone_code_id = $(this).next().attr("id");
            $("#"+id).intlTelInput({
                formatOnInit: true,
                separateDialCode: true,
                formatOnDisplay: false,
                customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                    country=selectedCountryPlaceholder;
                    country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                    $("#"+phone_code_id).val(Object.values(selectedCountryData)[2]);
                    space_remove(Object.values(selectedCountryData)[2], id);
                    if(selectedCountryData.iso2 == 'us')
                    {
                        country = "555-555-5555"
                    }
                    return  country;
                },
                excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
                utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
            });
        })
    }

$("#btnSave").click(function (e) {
    e.preventDefault();
    var baked_profit_split = $("#baked_iso_switch").is(":checked");
    if(baked_profit_split){
        $(".first-user").each(function () {
            var id = $(this).attr("id");
            var val = $(this).val();
            var hidden_val = $(this).next().val();
            var count = $(this).data("count");
            if(value_present(val) && value_present(hidden_val)){
                var percent = $(".user-percent"+count).val();
                var dollar = parseInt($(".user-dollar"+count).val());
                if(!value_present(percent) && !value_present(dollar)){
                    return false
                    $(".split_error"+count).html("Must enter % or $")

                }else{
                    $(".split_error"+count).html("")
                }
            }
        })
    }
});
$(document).on("keyup", ".user_percent, .user_dollar", function () {
    var count = $(this).data("count");
    $(".split_error"+count).html("");
})
$( "#close1" ).click(function(){
    $('#modal_id').hide()
})
$('.next_transactions').click(function () {
    $('#cover-spin').fadeIn();
});
var phone = $('#phone_number_field1').val();
var email = $('#email_field1').val();
function validate_edit_email() {
    if((email != $('#email_field1').val()) && ($('#email_field1').length > 0 ) ) {
        var url = "/admins/companies/form_validate?email_address=" + ($('#email_field1').val());
        if($(".i_am_location").val().length > 0){
            var url = "/admins/companies/form_validate?email_address=" + ($('#email_field1').val()) + "&location=iamlocation";
        }
        $.ajax({
            url: url,
            type: 'GET',
            data: {user_id: $('#company_hidden_field').val()},
            success: function (data) {
                $('#email_field1').css('border', '')
                $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
                if (($("#phone_number_label_field1").text() == "Phone")) {
                    $('#submit_button').prop('disabled', false);
                }
                if ($('#email_field1')) {
                    if (data['value'] == true) {
                        $('#email_label_field1').replaceWith("<label id='email_label_field1' class='string optional' for='Email'	>Email<span style='color:red;'> This email has already been registered. Try again.</span></label>")
                        $('#email_field1').css('border', '1px solid red');
                        $('#submit_button').prop('disabled', true);
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }else{
        $('#email_field1').css('border', '')
        $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
        if (($("#phone_number_label_field1").text() == "Phone number") && ($('#password_field1').text() == "Confirm Password")) {
            $('#submit_button').prop('disabled', false);
        }
    }
}
function validate_phone_edit_number() {
    if((phone != $('#phone_number_field1').val()) && ($('#phone_number_field1').length > 0 ) ) {
        var url = "/admins/companies/form_validate?phone_number=" + ($('#phone_number_field1').val());
        if($(".i_am_location").val().length > 0){
            var url = "/admins/companies/form_validate?phone_number=" + ($('#phone_number_field1').val()) + "&location=iamlocation";
        }
        $.ajax({
            url: url,
            type: 'GET',
            data: {user_id: $('#company_hidden_field').val()},
            success: function (data) {
                $('#phone_number_field1').css('border', '')
                $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='Phone number'>Phone</label>")
                if ($("#email_label_field1").text() == "Email") {
                    $('#submit_button').prop('disabled', false);
                }
                if ($('#phone_number_field1')) {
                    if (data['value'] == true) {
                        $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='Phone number'>Phone <span style='color:red'>  This phone number has already been registered. Try again.</span></label>")
                        $('#phone_number_field1').css('border', '1px solid red');
                        $('#submit_button').prop('disabled', true);
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }else{
        $('#phone_number_field1').css('border', '')
        $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='company_Phone number'>Phone number</label>")
        if (($("#email_label_field1").text() == "Email") && ($('#password_field1').text() == "Confirm Password")) {
            $('#submit_button').prop('disabled', false);
        }
    }
}

$(document).on('click','#duplicate-location',function () {
    var location_id = $(this).attr('data-location_id');
    swal({
        title: "Are you sure you want to duplicate?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        if (isConfirm.value === true) {
            $('#cover-spin').show(0);
            $.ajax({
                url: "/admins/locations/"+location_id + "/edit?duplicated=true" ,
                type: 'get',
                data: {  },
                success: function(data) {
                    $('#cover-spin').fadeOut("slow");
                    swal("Verified!", "Duplicated successfully", "success");
                    $('.main').html('').html(data);
                }
            });
        }
    });
});
$(document).on('click','#approve-merchant-status',function () {
    var user_id = $(this).attr('data-user_id')
    swal({
        title: "Are you sure you want to verify?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        if (isConfirm.value === true) {
            $('#cover-spin').show(0);
            $.ajax({
                url: "/admins/approve_status?id="+user_id,
                type: 'get',
                success: function(data) {
                    $('#cover-spin').fadeOut("slow");
                    if(data.success == true){
                        swal("Verified!", "Merchant Verified successfully", "success");
                        setTimeout(function(){
                            location.reload();
                        }, 3000)
                    }
                    else{
                        swal("Verified!", "Merchant Verified successfully", "success");
                        setTimeout(function(){
                            location.reload();
                        }, 3000)
                    }
                }

            });
        }
    });
});

