$(document).ready(function () {
    $(".alert").fadeOut(5000);
    var t;
    $("#try_again").on('click',function () {
        var user_id = $('#current_user_id').val();
        if($('#try_again').hasClass("true")){
            $('#try_again').html('');
            $.ajax({
                url: '/try_again',
                method: 'get',
                data: {id:  user_id,send_with:'mobile'},
                success: function (result) {
                    $('#try_again').removeClass("true");
                    var counter = 60;
                    var interval = setInterval(function() {
                        counter--;
                        if (counter <= 0) {
                            $('#try_again1').text('Send code via email');
                            clearInterval(interval);
                            $('#try_again').text("Resend code");
                            $('#try_again').addClass("true");
                            return;
                        }else{
                            $('#try_again').text(counter+"sec");
                            $('#try_again1').text('');
                        }
                    }, 1000);
                },
                error: function (result){
                    $('#try_again').text('Resend code');
                }
            });
        }
    });
    $("#try_again1").on('click',function () {
        var user_id = $('#current_user_id').val();
        if($('#try_again1').hasClass("true")){
            $('#try_again1').html('');
            $.ajax({
                url: '/try_again',
                method: 'get',
                data: {id:  user_id, send_with:'email'},
                success: function (result) {
                    $('#try_again1').removeClass("true");
                    var counter = 60;
                    var interval = setInterval(function() {
                        counter--;
                        if (counter <= 0) {
                            $('#try_again').text('Resend code');
                            clearInterval(interval);
                            $('#try_again1').text("Send code via email");
                            $('#try_again1').addClass("true");
                            return;
                        }else{
                            $('#try_again1').text(counter+"sec");
                            $('#try_again').text('');
                        }
                    }, 1000);
                },
                error: function (result){
                    $('#try_again1').text('Send code via email');
                }
            });
        }
    });
    $("#try_again2").on('click',function () {
        var user_id = $('#current_user_id').val();
        if($('#try_again2').hasClass("true")){
            $('#try_again2').html('');
            $.ajax({
                url: '/try_again',
                method: 'get',
                data: {id:  user_id, send_with:'email', check: 'two-step'},
                success: function (result) {
                    $('#try_again2').removeClass("true");
                    var counter = 60;
                    var interval = setInterval(function() {
                        counter--;
                        if (counter <= 0) {
                            $('#try_again').text('Resend code');
                            clearInterval(interval);
                            $('#try_again2').text("Send code via email");
                            $('#try_again2').addClass("true");
                            return;
                        }else{
                            $('#try_again2').text(counter+"sec");
                            $('#try_again').text('');
                        }
                    }, 1000);
                },
                error: function (result){
                    $('#try_again2').text('Send code via email');
                }
            });
        }
    });
    $('.pincode-input-text').css('height','60px');
    $('.pincode-input-text').css('width','60px');
    $('.pincode-input-text').css('text-align','center');
    $('.pincode-input-text').css('border','1px solid gray');
    $('.pincode-input-text').css('font-size','30px');
    $("body").css("background","#f0f3f6");
    $('.navbar').hide();
    $('.footer').hide();
    $(".login-input-group > input").focus(function(e){
        $(this).parent().addClass("input-group-focus");
    }).blur(function(e){
        $(this).parent().removeClass("input-group-focus");
    });
    $("#token").inputmask('Regex', {regex: "^[0-9]+$"});


});
$('form input').keydown(function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        return false;
    }
});