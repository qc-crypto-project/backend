$(document).ready(function() {
    String.prototype.replaceAt=function(index, replacement) {
        return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
    };
    //$('#login-form-submit').attr("disabled",true);
    $('#alert-message-top').fadeOut(5000)
    var email_part ="";
    $('.navbar').hide();
    $('.footer').hide();
    $("body").css("background","#f0f3f6");
    $(".login-input-group > input").focus(function(e){
        $(this).parent().addClass("input-group-focus");
    }).blur(function(e){
        $(this).parent().removeClass("input-group-focus");
    });
    $('.username').focusout(function(){
        var pattern = re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,20}\b$/i;
        var userinput = $("#username").val();
        if (userinput == ""){
            $("#email_error").text("");
            // $('#login-form-submit').attr("disabled",true);
        }else if (!pattern.test(userinput) ){
            $("#email_error").text("please enter valid email");
            // $('#login-form-submit').attr("disabled",true);
        }
        else{
            $("#email_error").text("");
            if($('#password').val() != ""){
                // $('#login-form-submit').attr("disabled",false);
            }
        }

    });
    // $('#password').keyup(function(){
    //     if($('#password').val() == "" || $('.username').val() == ""){
    //         // $('#login-form-submit').attr("disabled",true);
    //     }
    //     else{
    //         $('#login-form-submit').attr("disabled",false);
    //     }
    // });
    $('#login-form-submit').click(function(){
        $('#alert-message').html('');
    });
    $("#input-group-addon-login-qc-eye").click(function(){
        const icon = this.querySelector('i');
        if (icon.classList.contains('fa-eye')) {
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
            $('#password').attr('type', 'text');
        } else {
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
            $('#password').attr('type', 'password');
        }
    });

    // reset password form js users/passwords/edit.html.erb
    $(".email-input").on('keyup change', function (){
        if($(this).val() !==''){
            $('.email-login-error').hide();
            $('.email-input').css("border","solid 2px #35bab6");
        }
        else
        {
            $('.email-login-error').show();
            $('.email-input').css("border","solid 2px #d45858");
        }
    });
    validatePasswordForm("password_form");
    
    $("#match_or_not").hide();
    $('.navbar').hide();
    $('.footer').hide();
    $(".login-input-group > input").focus(function(e){
        $(this).parent().addClass("input-group-focus");
    }).blur(function(e){
        $(this).parent().removeClass("input-group-focus");
    });

    // $(".password_confirmation").bind("keyup", function() {
    //     if( $(".password").val() == $(".password_confirmation").val() ) {
    //         $("#match_or_not").hide();
    //         $("#login-form-submit").prop('disabled', false);
    //     } else {
    //         $("#match_or_not").show();
    //         $("#login-form-submit").prop('disabled', true);
    //     }
    // });

    var pass_score = 0;
    $(document).on("focusin", "#username", function () {
        $('.strength').css('display', "block");
    });

    $(document).on("focusout", "#username", function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            $('.strength').css('display', "none");
        }else if($(this).val() == ""){
            $('.strength').css('display', "none");
        }
    });

    $("#username").on("keyup", function() {
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else if (password.length > 0){
            $('#username-error').html('');
            $('#userPass').removeClass("redError");
        }
        else if(password.length == 0){
            $('#username-error').html('').text("Please Enter Password");
            $('#username-error').addClass("redError");
            $('#userPass').addClass("redError");
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });

    var pass_message = $("label.message").text();
    if(pass_message != ""){
        setTimeout(function(){
            $("label.message").fadeOut()
        }, 2000)
    }
});

function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80)
        return "strong";
    if (score > 60)
        return "good";
    if (score >= 30)
        return "weak";

    return "";
}
