//locations/index.html

$(document).ready(function() {
    $('#alert-message-top').fadeOut(5000)
var full_down=false;
var agree_check=false;
jQuery(function($) {
    $('#overflows_handling').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && agree_check==true) {
            full_down=true;
            $('#submitBtn').removeAttr('disabled');
        }
        else if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
            full_down=true;
            $('#tos_checker').removeAttr('disabled');
        }
    })
});
$("#tos_checker").on('click',function (){
    if($("#tos_checker").is(':checked')){
        agree_check=true;
        if(full_down==true){
            $('#submitBtn').removeAttr('disabled');
        }
    }
    else {
        agree_check=false;
        $('#submitBtn').attr('disabled', true);
    }
});


$('.wallet_info').on('click', function () {
    $(".spnr").fadeIn();
});
$(".setting_btn").on('click', function () {
    var id = $(this).attr('data-id');
    $('.text_for_location').html('').html(id);
});
$(".all_location_view_link").click(function () {
    $('.spnr').fadeIn().fadeOut(10000);
});
$(".pay_tip").click(function () {
    $('.spnr').fadeIn();
});
$(document).on('click', '#btn_pay_tip', function () {
    if ($('#step1').css('display') == 'none') {
        $('#step1').css('display', 'block');
        $('#vf_mer').html('Submit').removeClass('btn-success').addClass('btn-primary');
        $('#mer_email').val('');
        $('#mer_pwd').val('');
    }
});
$(document).on('click', '#vf_mer', function () {
    var email = $('#mer_email');
    var password = $('#mer_pwd');
    var input_email = $('#email_tested');
    var input_pwd = $('#pwd_testd');
    var w_b = $('#wallet_balance');
    $('#verify_merchant').submit(function (form) {
        form.preventDefault();
        return false;
    });
    $('#vf_mer').append('<i class="fa fa-spinner fa-spin"></i>').attr('disabled', 'disabled');
    $.ajax({
        url: "/merchant/base/verify_user",
        method: 'get',
        data: {email: email.val(), password: password.val()},
        success: function (result) {
            if (result.user != null) {
                $('#vf_mer').html('Verified').removeClass('btn-primary').addClass('btn-success').prop('disabled', false).find('i').remove();
                setTimeout(function () {
                    $('#step1').css('display', 'none').fadeOut("slow");
                    $('#step2').css('display', 'block');
                }, 1000)
            }else{
                $('#vf_mer').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger').prop('disabled', false).find('i').remove();
                setTimeout(function () {
                    $('#vf_mer').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                }, 1000);
            }
        }, error: function (result) {
            $('#vf_mer').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger').prop('disabled', false).find('i').remove();
            setTimeout(function () {
                $('#vf_mer').html('Submit').removeClass('btn-danger').addClass('btn-primary');
            }, 1000);
        }
    });
});
    $("#amount").inputmask('Regex', {regex: "^[0-9,]{1,99}(\\.\\d{1,2})?$"});
    // $("#memo").inputmask('Regex', {regex: "/^[ ]*[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]{1,64}@(?=.{10,20}$)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*[ ]*$/;\n"});
    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });
    // $('#transfer_date').datepicker({
    //     dateFormat: 'mm/dd/yy',
    //     startDate: new Date(),
    //     autoclose: true
    // })
    // $('#transfer_date').datepicker("setDate", new Date());
$('#transfer_money').on('hidden.bs.modal', function () {
    // $('#transfer_date').datepicker("setDate", new Date());
    $("#transfer_from_wallet option[value='']").attr('selected', true)
    $("#transfer_to_wallet option[value='']").attr('selected', true)
    $("#amount").val('0.00');
});
    var target_to = 1;
    var target_from = 1;
    $('#transfer_from_wallet').change(function () {
        var source = $(this).val();
        $('#transfer_to_wallet option[value="' + target_from + '"]').removeAttr('style');
        if (!isNaN(parseInt(source))) {
            showWalletBalance(source, 'from');
            $('#transfer_to_wallet option[value="' + source + '"]').hide();
            target_from = source;
        } else {
            $('#from_available_amount').text('0.00');
            $('#transfer_to_wallet option[value="' + target_from + '"]').removeAttr('style');
        }
    $('#transfer_to_wallet').change(function () {
        var source = $(this).val();
        $('#transfer_from_wallet option[value="' + target_to + '"]').removeAttr('style');
        if (!isNaN(parseInt(source))) {
            showWalletBalance(source, 'to');
            $('#transfer_from_wallet option[value="' + source + '"]').hide();
            target_to = source;
        } else {
            $('#to_available_amount').text('0.00');
            $('#transfer_from_wallet option[value="' + target_to + '"]').removeAttr('style');
        }
    });

$('#submit_transfer_btn').click(function () {
    var from = $('#transfer_from_wallet').val();
    var to = $('#transfer_to_wallet').val();
    var amount = $('#amount').val();
    var transfer_date = $('#transfer_date').val();
    if (from != "" && to != "" && amount != "" && transfer_date != "") {
        $('.spnr').fadeIn();
    }
});
//.....................................inline click event.............//
        $(document).on('click','.wallet_succes',function () {
            var wallet_id = $(this).data()
            show_transaction('/merchant/locations/previous_transactions','success',wallet_id)
        })
        $(document).on('click','.wallet_refund',function () {
                    var wallet_id = $(this).data()
            show_transaction('/merchant/locations/previous_transactions','refund',wallet_id)
                })
        $(document).on('click','.wallet_check',function () {
                    var wallet_id = $(this).data()
            show_transaction('/merchant/locations/previous_transactions','check',wallet_id)
                })
        $(document).on('click','.wallet_b2b',function () {
                    var wallet_id = $(this).data()
            show_transaction('/merchant/locations/previous_transactions','b2b',wallet_id)
                });
        function numberWithCommas(x) {
            $('#amount,#trans_amount').val(( x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")))
        }

//----------------------------transafers.html.erb---------------


        $("#amount").inputmask('Regex', {regex: "^[0-9,]{1,99}(\\.\\d{1,2})?$"});
        // $("#memo").inputmask('Regex', {regex: "/^[ ]*[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]{1,64}@(?=.{10,20}$)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*[ ]*$/;\n"});
        // $('#transfer_date'). ({
        //     dateFormat: 'mm/dd/yy',
        //     startDate: new Date(),
        //     autoclose: true
        // })
        // $('#transfer_date').datepicker("setDate", new Date());

        function numberWithCommas(x) {
            $('#amount').val(( x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")));
        }
        $("#amount").keyup(function (e) {
            var str =$('#amount').val();
            str=str.replace(/,/g, "")
            numberWithCommas(str)
        });

        var target_to =1;
        var target_from =1;
        $('#transfer_from_wallet').change(function(){
            var source=$(this).val();
            $('#transfer_to_wallet option[value="'+ target_from +'"]').removeAttr('style');
            if(!isNaN(parseInt(source)))
            {
                showWalletBalance(source, 'source');
                $('#transfer_to_wallet option[value="'+ source +'"]').hide();
                target_from=source;
            }
            else
            {
                $('#source_available_amount').text('0.00');
                $('#transfer_to_wallet option[value="'+ target_from +'"]').removeAttr('style');
            }
        });
        function showWalletBalance(wallet_id, type) {
            $.ajax({
                url: "/merchant/locations/show_merchant_balance",
                type: "GET",
                data: {source: wallet_id},
                success: function (data) {
                    if (data.balance != "") {
                        $('#' + type + '_available_amount').text(data.balance);
                    } else {
                        $('#' + type + '_available_amount').text('0.00');
                    }
                },
                error: function (data) {
                    $('#' + type + '_available_amount').text('0.00');
                }
            });
        }

    });
        $("#Fdate_range_picker").daterangepicker({
            format: 'MM/DD/YYYY',
            autoclose: true,
            autoUpdateInput: false,
            parentEl: $(".date_parent"),
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
        $('#transfer_date').daterangepicker({
            format: 'MM/DD/YYYY',
            // minDate: moment().format('DD/MM/YYYY'),
            autoclose: true,
            autoUpdateInput:true,
            minDate: moment(),
            singleDatePicker: true,
            parentEl: $("#date_parent_for_transfer")
        });
        $('#transfer_date1').daterangepicker({
            format: 'MM/DD/YYYY',
            // minDate: moment().format('DD/MM/YYYY'),
            autoclose: true,
            autoUpdateInput:true,
            minDate: moment(),
            singleDatePicker: true,
            parentEl: $("#date_parent_for_transfer")
        });

        $('#transfer_to_wallet').change(function(){
            var source=$(this).val();
            $('#transfer_from_wallet option[value="'+ target_to +'"]').removeAttr('style');
            if(!isNaN(parseInt(source)))
            {
                showWalletBalance(source, 'destination');
                $('#transfer_from_wallet option[value="'+ source +'"]').hide();
                target_to=source;
            }
            else
            {
                $('#destination_available_amount').text('0.00');
                $('#transfer_from_wallet option[value="'+ target_to +'"]').removeAttr('style');
            }
        });

        function showWalletBalance(wallet_id, type){
            $.ajax({
                url: "/merchant/locations/show_merchant_balance",
                type: "GET",
                data: {source: wallet_id},
                success: function (data) {
                    if (data.balance!=""){
                        $('#' + type + '_available_amount').text(data.balance);
                    }else{
                        $('#' + type + '_available_amount').text('0.00');
                    }
                },
                error: function (data) {
                    $('#' + type + '_available_amount').text('0.00');
                }
            });
        }
    $('#transfer_new').click(function () {
        $('#transfer_id').val('');
        $('#transfer_from_wallet option:eq(0)').prop('selected', true);
        $('#transfer_to_wallet option:eq(0)').prop('selected', true);
        $('#amount').val('');
        $('#amount').attr('placeholder','0.00');
        $('#memo').val('');
        $("#transfer_from_wallet").trigger("change");
        $("#transfer_to_wallet").trigger("change");
        $('#source_available_amount').text('');
        $('#destination_available_amount').text('');
        $("#transfer_date").data('daterangepicker').setStartDate(new Date());
        $("#transfer_date").data('daterangepicker').setEndDate(new Date);
        // $('#transfer_date').daterangepicker("setDate", new Date());
        $('#newTransferForm').attr('action',"/merchant/locations/transfer_post")
    });
    (function($) {// restrict user to add any characters other than numbers and alphabets
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    $("#errmsg").html("Use letters & numbers only").show().fadeOut(3000);
                }
            });
        };
    }(jQuery));
    // Install input filters.
    // $("#memo").inputFilter(function(value) {
    //     return /^^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(value); });


    $('.transfer_edit').click(function () {
        var date=new Date($(this).data('date')+" 00:00:00");
        var from_wallet=$(this).data('from');
        var to_wallet=$(this).data('to');
        $('#transfer_id').val($(this).data('transfer_id'));
        $("#transfer_from_wallet option[value="+from_wallet+"]").prop('selected', true);
        $("#transfer_to_wallet option[value="+to_wallet+"]").prop('selected', true);
        $('#amount').val($(this).data('amount'));
        $('#memo').val($(this).data('memo'));
        $("#transfer_from_wallet").trigger("change");
        $("#transfer_to_wallet").trigger("change");
        // $('#transfer_date').daterangepicker("setDate",$(this).data('date'));
        $("#transfer_date").data('daterangepicker').setStartDate($(this).data('date'));
        $("#transfer_date").data('daterangepicker').setEndDate($(this).data('date'));
        // $('#transfer_date').val($(this).data('date'));
        $('#newTransferForm').attr('action',"/merchant/locations/transfer_update")
    })
    $('#submit_transfer_btn').click(function () {
        var from=$('#transfer_from_wallet').val();
        var to=$('#transfer_to_wallet').val();
        var amount=$('#amount').val();
        $('#amount').val(amount.replace(/,/g, ""));
        var transfer_date=$('#transfer_date').val();
        if(from!="" && to!="" && amount!="" && transfer_date!=""){
            $('.spnr').fadeIn();
        }
    })

    //    ----------------------------Cancel Modal Function Started-------------------
    $(document).on('click','#cancelModal',function(){
        $('#delete_transaction_btn').data('transfer_id',$(this).data('transfer_id'));
    });
    $(document).on('click','#delete_transaction_btn',function(){
        $('.spnr').fadeIn();
        var transfer_id=$(this).data('transfer_id');
        delete_transfer(transfer_id);
    });

//-----------------wallets_location_details----------------

   /* $("#date_id").on('keyup focusout', function () {
        if ($(this).val() == "") {
            $(this).datepicker('setDate', null);
        }
    });*/
    $("#amount,#trans_amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $("#last4").inputmask('Regex', {regex: "^[0-9]+$"});
    $('.details_modal').click(function () {
        $('.spnr').fadeIn();
    });
    $('#submit_transfer_btn').click(function () {
        var from=$('#transfer_from_wallet').val();
        var to=$('#transfer_to_wallet').val();
        var amount=$('#trans_amount').val();
        $('#trans_amount').val(amount.replace(/,/g, ""));
        var transfer_date=$('#transfer_date').val();
        if(from!="" && to!="" && amount!="" && transfer_date!=""){
            $('#transfer_date').attr('disabled',false);
            $('#transfer_from_wallet').attr('disabled',false);
            $('#transfer_to_wallet').attr('disabled',false);
            $('.spnr').fadeIn();
        }
    })
        $('#amount,#trans_amount').val('');
        $('#transfer_date').datepicker({
            dateFormat: 'mm/dd/yy',
            startDate: new Date(),
            autoclose: true
        })

        $('#transfer_date').datepicker("setDate", new Date());


    $(".location_show_link").click(function () {
        $('.spnr').fadeIn().fadeOut(10000);
    });
    $(".pay_tip").click(function () {
        $('.spnr').fadeIn();
    });
    $(document).on('click','#btn_pay_tip',function () {
        if( $('#step1').css('display')=='none'){
            $('#step1').css('display','block');
            $('#vf_mer').html('Submit').removeClass('btn-success').addClass('btn-primary');
            $('#mer_email').val('');
            $('#mer_pwd').val('');
        }
    });
    $(document).on('click','#vf_mer', function () {
        var email=$('#mer_email');
        var password=$('#mer_pwd');
        var input_email=$('#email_tested');
        var input_pwd=$('#pwd_testd');
        var w_b=$('#wallet_balance');
        $('#verify_merchant').submit( function (form) {
            form.preventDefault();
            return false ;
        });
        $('#vf_mer').append('<i class="fa fa-spinner fa-spin"></i>').attr('disabled', 'disabled');
        $.ajax({
            url: "<%=/merchant/base/verify_user",
            method: 'get',
            data: {email: email.val() , password: password.val()},
            success: function(result){
                if(result.user != null){
                    $('#vf_mer').html('Verified').removeClass('btn-primary').addClass('btn-success').prop('disabled', false).find('i').remove();
                    setTimeout(function () {
                        $('#step1').css('display','none').fadeOut("slow");
                        $('#step2').css('display','block');
                    },1000)
                }
            },error: function (result) {
                $('#vf_mer').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger').prop('disabled', false).find('i').remove();
                setTimeout(function () {
                    $('#vf_mer').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                },1000);
            }
        });
    });
    $('#transfer_new').click(function(){
        $('#transfer_date').datepicker("setDate", new Date());
        var from_wallet=$(this).data('from');
        var to_wallet=$(this).data('to');
        $("#transfer_from_wallet option[value="+from_wallet+"]").prop('selected', true);
        $("#transfer_to_wallet option[value="+to_wallet+"]").prop('selected', true);
        var source=$('#transfer_from_wallet').val();
        var destination=$('#transfer_to_wallet').val();
        getTransferWalletBalance(source,'from')
        getTransferWalletBalance(destination,'to')
    });

    $('#modal_close_btn').click(function(){
        $('#notification_modal').hide();
    });
    $('.next_transactions').click(function () {
        $('.spnr').fadeIn();
    });
    $(function() {

        $('#query_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false
        });
        $('#query_gateway_').multipleSelect({
            placeholder: "Select Gateway",
            selectAll: false
        });
    });

    $('.datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('')
    });
    $(function () {
        var flash = $('.flash_messsage').val()
        if(flash==true)
            $('#notification_modal').show();
    })
        $("[type='checkbox']").css("margin-top","-1px").css("margin-right","15px");
    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":31
            },
            "startDate": nd,
            "maxDate": nd
        });
    })
    $('.detail_link').on('click', function () {
        var transaction_id = $('#transaction_id_input').val();
        $('#cover-spin').show(0)
    })

    //------------------------------------------------location/location_transaction_html...........//
    $(document).on('click','.succes_button',function () {
        var wallet_id = $('#wallet_id_input').val();
        show_transaction('/merchant/locations/transactions','success', wallet_id)
    })
    $(document).on('click','.decline_button',function () {
        var wallet_id = $('#wallet_id_input').val();
        show_transaction('/merchant/locations/transactions','decline',wallet_id)
    })
    $(document).on('click','.refunds_button',function () {
        var wallet_id = $('#wallet_id_input').val();
        show_transaction('/merchant/locations/transactions','refund',wallet_id)
    })
    $(document).on('change','#tx_filter',function () {
        $('#location_transaction_filter').submit();
    })
    $(document).on('change','.tx_filter',function () {
        $('#location_transaction_filter').submit();
    })
    $(document).on('change','.wallet_filter',function () {
        $('#wallet_filter').submit();
    })
    $(document).on('change','#filter',function () {
        $('#transfer_filter').submit();
    })

        $("#query_time1").on('change', function () {
            var time1 = $("#query_time1").val();
            $('#query_time2 > option').each(function () {
                if (parseInt($(this).val()) <= parseInt(time1)) {
                    $(this).attr('disabled', 'disabled');
                }
            });
        });
        $("#last4").inputmask('Regex', {regex: "^[0-9]+$"});
        $('#query_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false
        });
        $('.number').keypress(function (event) {
            var $this = $(this);
            if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                ((event.which < 48 || event.which > 57) &&
                    (event.which != 0 && event.which != 8))) {
                event.preventDefault();
            }

            var text = $(this).val();
            if ((event.which == 46) && (text.indexOf('.') == -1)) {
                setTimeout(function () {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                        $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                }, 1);
            }

            if ((text.indexOf('.') != -1) &&
                (text.substring(text.indexOf('.')).length > 2) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 2)) {
                event.preventDefault();
            }
        });

        $('.number').bind("paste", function (e) {
            var text = e.originalEvent.clipboardData.getData('Text');
            if ($.isNumeric(text)) {
                if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                    e.preventDefault();
                    $(this).val(text.substring(0, text.indexOf('.') + 3));
                }
            } else {
                e.preventDefault();
            }
        });


        var select_reason, reason_detail, transaction_detail, tran_Id, destination, source, refund_fee, total_amount,
            partial_amt, parent_id, parsed_transaction;
        var check1 = false, check2 = false;
        $(document).on("click", ".refundModalDialog", function () {
            $('#reason_select option[value="0"]').prop("selected", true);
            $('#partialamount').val('');
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('.refund_fee').text('0');
            $('.modal-footer a button').prop('disabled', true);
            transaction_detail = JSON.stringify($(this).data('transaction'));
            parsed_transction = JSON.parse(transaction_detail);
            tran_Id = parsed_transction.id;
            parent_id = parsed_transction.parent_id;
            destination = parsed_transction.receiver_wallet_id;
            source = parsed_transction.sender_wallet_id;
            $.ajax({
                url: '/merchant/transactions/refund_check',
                type: 'GET',
                data: {trans_id: tran_Id}
            });
            $.ajax({
                url: '/merchant/transactions/get_merchant_amount',
                type: 'GET',
                data: {trans_id: tran_Id, parent_id: parent_id, destination: destination, source: source},
                success: function (res) {
                    if (res.partial_amount == "") {
                        $('#partial_error').text("No Transaction Found");
                        $('#partial_error').fadeIn();
                        setTimeout(function () {
                            $('#partial_error').fadeOut()
                        }, 2000);
                    } else {
                        $('#partialamount').val(parseFloat(res.partial_amount).toFixed(2));
                        total_amount = parseFloat(res.partial_amount);
                        partial_amt = total_amount;
                        refund_fee = res.fee;
                        var fee = refund_fee + parseFloat(res.partial_amount);
                        $('.refund_fee').text(parseFloat(fee).toFixed(2));
                    }
                }
            });
        });
        $(document).on('change','#partialamount',function () {
            var amount = $(this).val();
            if (amount <= 0) {
                $('.modal-footer a button').prop('disabled', true);
                $('.refund_fee').text('0.00');
                return false;
            }
            select_reason = $('#reason_select').val();
            var partial_amount = $('#partialamount').val();
            partial_amt = partial_amount;
            if (partial_amount != "0") {
                $.ajax({
                    url: '/merchant/transactions/get_merchant_amount',
                    type: 'GET',
                    data: {trans_id: tran_Id, destination: destination, source: source}
                })
                    .done(function (res) {
                        var partial = res.partial_amount - parseFloat(partial_amount);
                        if (partial < 0) {
                            $('#partial_error').text("Amount is greater than amount to refund");
                            $('#partial_error').fadeIn();
                            setTimeout(function () {
                                $('#partial_error').fadeOut()
                            }, 2000);

                            $('.modal-footer a button').prop('disabled', true);
                            check1 = false;
                        } else {
                            $('#partial_error').text("");
                            check1 = true;
                            refund_fee = res.fee;
                            var fee = refund_fee + parseFloat(partial_amount);
                            $('.refund_fee').text(parseFloat(fee).toFixed(2));
                        }
                        if (check1 && check2) {
                            if (select_reason != 0) {
                                $('.modal-footer a button').prop('disabled', false);
                            }
                        }
                    });
            }
            updateRefundPostLink();
        });

        function updateRefundPostLink() {
            var select_reason = $('#reason_select').val();
            var partial_amount = $('#partialamount').val();
            reason_detail = $("#reason_select option:selected").text();
            if (select_reason == 0) {
                $('#reason_detail').val('');
                $('#reason_detail').hide();
                $('.modal-footer a button').prop('disabled', true);
                check2 = false;
            } else if (select_reason != 4 && select_reason != 0) {
                $('#reason_detail').val('');
                $('#reason_detail').hide();
                $('#footerLink').attr('href', "/merchant/transactions/refund_merchant_transaction" + "?js=js&reason=" + select_reason + "&partial_amount=" + partial_amount + "&refund=" + refund_fee + "&reason_detail=" + reason_detail + "&transaction=" + tran_Id);
                if (total_amount >= partial_amt) {
                    $('.modal-footer a button').prop('disabled', false);
                }
                check2 = true;
            } else {
                $('#reason_detail').show();
                $('.modal-footer a button').prop('disabled', true);
                check2 = false
            }
            if (check1 && check2) {
                $('.modal-footer a button').prop('disabled', false);
            }
        }


        $('#reason_select').change(function () {
            updateRefundPostLink();
        });

        $('#reason_detail').keyup(function () {
            reason_detail = $(this).val();
            var select_reason = $('#reason_select').val();
            var partial_amount = $('#partialamount').val();
            if (reason_detail == '' || reason_detail == null) {
                $('#refundModalFooter a button').prop('disabled', true);
            } else {
                $('#footerLink').attr('href', "/merchant/transactions/refund_merchant_transaction" + "?js=js&reason=" + select_reason + "&partial_amount=" + partial_amount + "&reason_detail=" + reason_detail + "&transaction=" + tran_Id);
                if (total_amount >= partial_amount) {
                    $('#refundModalFooter a button').prop('disabled', false);
                }
            }
        });
        $('.pagination').find('li').find('a').click(function () {
            $('.spnr').fadeIn();
        });
        $('.refund_btn').click(function () {
            $('.spnr').fadeIn();
        });
        $(".details_modal").click(function () {
            $(".spnr").fadeIn();
        });

        $('#sender').bind('keyup change', function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
            if ($(this).val().length == 0) {
                $('.search_button').attr('disabled', true);
            } else {
                $('.search_button').attr('disabled', false);
            }
        });
        $('#receiver').bind('keyup change', function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
            if ($(this).val().length == 0) {
                $('.search_button').attr('disabled', true);
            } else {
                $('.search_button').attr('disabled', false);
            }
        });
        // $('#phone').bind('keyup change', function(){
        //     this.value = this.value.replace(/[^0-9\.]/g,'');
        //     if($(this).val().length == 0){
        //         $('.search_button').attr('disabled', true);
        //     }else{
        //         $('.search_button').attr('disabled', false);
        //     }
        // });
        $('#Price').bind('keyup change', function () {
            if ($(this).val().length == 0) {
                $('.search_button').attr('disabled', true);
            } else {
                $('.search_button').attr('disabled', false);
            }
        });
        $('#id').bind('keyup change', function () {
            if ($(this).val().length == 0) {
                $('.search_button').attr('disabled', true);
            } else {
                $('.search_button').attr('disabled', false);
            }
        });
        $('.search_transactions').submit(function () {
            $('.spnr').fadeIn();
        });


        $('#detail_transaction_modal').click(function () {
            $('.spnr').fadeIn();
        });
        $('.next_transactions').click(function () {
            $('.spnr').fadeIn();
        });
        $('#password_btn').on('click', function () {
            var password = $('#password').val();
            $.ajax({
                url: "/merchant/transactions/verify_user",
                method: 'get',
                data: {password: password},
                success: function (data) {
                    $("#password_btn").css("background", "green").text('').text("Verified");
                    $("#info-text").text('');
                    setTimeout(function () {
                        // $('#password').attr("disabled","disabled");
                        $('#verifymodal').fadeOut("slow");
                        $('#verify_btn').hide();
                        $('#footerLink').show();
                        $('.modal-backdrop').remove();
                    }, 1000);
                },
                error: function (data) {
                    $("#password_btn").css("border", "1px solid red");
                    $("#info-text").text('').css("color", "red").text('Please try again!');
                }
            });
        });
        $("#refundModalDialog").on('hidden.bs.modal', function () {
            $('#verify_btn').show();
            $('#footerLink').hide();
            $("#password_btn").css("background", "#3c8dbc").text('').text("Verify");
        });
        $("#export_class").on('click', function () {
            var nowDate = new Date();
            var newdate = (nowDate.setDate(nowDate.getDate()));
            var nd = new Date(newdate);
            $("#exportDate").daterangepicker({
                "maxSpan": {
                    "days": 90
                },
                "startDate": nd,
                "maxDate": nd
            });
        });
    var lt= $('.modalTimeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
//           var gmtDateTime = moment.utc(v.innerText).toDate();
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

// _location_transaction_table.html.erb


// local_details.html.erb


        $(document).mouseup(function (e)
        {
            var container = $("#tx_id"); // YOUR CONTAINER SELECTOR
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });
        $(document).on('click','#tx_idd',function() {
            // $('#welcome').show()
            $('#tx_id').toggle();
        });

        $("#modal_id").modal('show')
//          $(".tip-top").tooltip({placement : 'top'});

        $('[data-toggle="popover"]').popover({container: 'body'});
        if($('#btn-id a').length===1){
            $('#btn-id').removeClass('col-md-12').addClass('col-md-6 pull-right')
        }
        function copyToClipboard(element) {
            var copyText = element;
            copyText.select();
            document.execCommand("copy");
        }
        $('.datepicker12').datepicker({
            format: 'mm-dd-yyyy'
        });

    $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
    });

    if ($("#daterange").val() != ""){
        if ($("#daterange").val() == "" || $("#daterange").val() == undefined ){
            $('input[name="query[date]"]').val('');
        }
    } else{
        $('input[name="query[date]"]').val('');
    }
        $('#password_btn1').on('click',function () {
            var password = $('#password1').val();
            $.ajax({
                url: "/merchant/transactions/verify_user",
                method: 'get',
                data: {password: password},
                success:function(data){
//                $("#password_btn").css("background","green").text('').text("Verified");
                    $("#info-text1").text('');
                    setTimeout(function(){
                        // $('#password').attr("disabled","disabled");
//                    $('#verifymodal').fadeOut("slow");
                        $('#verify_btn1').hide();
                        $('#password_btn1').hide();
                        $('#send1').show();
                        $('.modal-backdrop').remove();
                    }, 1000);
                },
                error: function(data){
                    $("#password_btn1").css("border","1px solid red");
                    $("#info-text1").text('').css("color","red").text('Please try again!');
                }
            });
        });

        $("#send1").on('click', function () {
            var id = $('#dispute_id').val();
//          var btn_url = $(this).attr('href');
            var btn_url='/merchant/disputes/accept_dispute'
            btn_url += "?dispute_id="+id;
            $(this).attr('href', btn_url);
        })
        $('#verifymodal1').on('hidden.bs.modal', function () {
            $('#send1').hide();
            $('#password_btn1').show();
            $('#password1').val('')
            // do something…
        });
    $( "#transaction tr" ).click(function(){
        // event.stopPropagation();
        var link  = $(this).data("href");
        setTimeout(function() {
            $('#cover-spin').fadeOut("slow");
            // alert("Something went wrong with this detail. Please try again later!")
        }, 15000 );
        $.ajax({
            url: link,
            type: "get"
        });
    });

//--------------------------------------transaction/exported_files.html-----------------//


    
    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    //.............................show.html.erb................//

    $(".location_show_link").click(function () {
        $('.spnr').fadeIn().fadeOut(10000);
    });
    $(".pay_tip").click(function () {
        $('.spnr').fadeIn();
    });
    $(document).on('click','#btn_pay_tip',function () {
        if( $('#step1').css('display')=='none'){
            $('#step1').css('display','block');
            $('#vf_mer').html('Submit').removeClass('btn-success').addClass('btn-primary');
            $('#mer_email').val('');
            $('#mer_pwd').val('');
        }
    });
    $(document).on('click','#vf_mer', function () {
        var email=$('#mer_email');
        var password=$('#mer_pwd');
        var input_email=$('#email_tested');
        var input_pwd=$('#pwd_testd');
        var w_b=$('#wallet_balance');
        $('#verify_merchant').submit( function (form) {
            form.preventDefault();
            return false ;
        });
        $('#vf_mer').append('<i class="fa fa-spinner fa-spin"></i>').attr('disabled', 'disabled');
        $.ajax({
            url: "/merchant/base/verify_user",
            method: 'get',
            data: {email: email.val() , password: password.val()},
            success: function(result){
                if(result.user != null){
                    $('#vf_mer').html('Verified').removeClass('btn-primary').addClass('btn-success').prop('disabled', false).find('i').remove();
                    setTimeout(function () {
                        $('#step1').css('display','none').fadeOut("slow");
                        $('#step2').css('display','block');
                    },1000)
                }


            },error: function (result) {
                $('#vf_mer').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger').prop('disabled', false).find('i').remove();
                setTimeout(function () {
                    $('#vf_mer').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                },1000);


            }

        });

    });


    //.............................._pa_tip.html.erb...............//

    $('#transfer').on('keyup change',function (e) {
        current_val = $(this).val();
        current_balance = $("#total_amount").text();
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
        if(parseFloat(current_val) > parseFloat(current_balance)){
            $("#error").text('Not Enough Balance').fadeOut(4000);
            $(this).val('');
            $('#transfer_now').prop('disabled',true);
        }else{
            $('#transfer_now').prop('disabled',false);
        }
    });


        $(".transfer_amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
        $(document).on("keyup change", '#transfer', function (e) {
            current_val = $(this).val();
            current_balance = $("#total_amount").text();
            if (parseFloat(current_val) > parseFloat(current_balance)) {
                $("#transfer_error").text('');
                $('#transfer_now').prop('disabled', true);
                $("#transfer_error").text('Not Enough Balance');
            } else if ((current_val <= 0 || current_val <= 0.00) && current_val != "") {
                $("#transfer_error").text('');
                $('#transfer_now').prop('disabled', true);
                $("#transfer_error").text('Amount must be greater than 0')
            } else if (current_val == "") {
                $("#transfer_error").text('');
                $('#transfer_now').prop('disabled', true);
                $("#transfer_error").text('Amount is required')
            } else {
                $("#transfer_error").text('');
                $('#transfer_now').prop('disabled', false);
            }
        });

    $('#vf_mer').attr('disabled','disabled');
    $("#mer_email").on('keyup',function() {
        if (($('#mer_email').val().length!= 0) && ($('#mer_pwd').val().length != 0)){
            if ($('label[class = "error"]').length) {
                if($('label[class = "error"]').html() == ''){
                    $('#vf_mer').removeAttr('disabled');
                }else{
                    $('#vf_mer').attr('disabled','disabled');
                }
            }else{
                $('#vf_mer').removeAttr('disabled');
            }
        }else {
            $('#vf_mer').attr('disabled','disabled');
        }
    });
    $("#mer_pwd").on('keyup',function() {
        if (($('#mer_pwd').val().length != 0) && ($('#mer_email').val().length != 0)){
            if ($('label[class = "error"]').length) {
                if($('label[class = "error"]').html() == ''){
                    $('#vf_mer').removeAttr('disabled');
                }else{
                    $('#vf_mer').attr('disabled','disabled');
                }
            }else{
                $('#vf_mer').removeAttr('disabled');
            }
        }else {
            $('#vf_mer').attr('disabled','disabled');
        }
    });



        $("#transfer").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});

    $('#password_submit').attr('disabled',true);
    $( document ).ready(function() {
        $('.footer').hide();
        $('.navbar-static-top').hide();
        $('#password_submit').attr('disabled',true);
    });

    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            $('#strength_human').hide();
            $('#password_submit').attr('disabled',false);
        } else{
            $('#password_submit').attr('disabled',true);
        }
    });


    $("#user_password").on("keyup", function() {
        $('#strength_human').show();
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input style="margin-right: 10px;" id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input style="margin-right: 10px;" id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input style="margin-right: 10px;" id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input style="margin-right: 10px;" id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else{
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });











    // Search from the list of the Veirfied or un verified
    $('#employee_search').on('keyup', function () {
        var searchString = $(this).val();
        $("#sub_merchants li").each(function(index, value) {
            currentName = $(value).text();
            if( currentName.toUpperCase().indexOf(searchString.toUpperCase()) > -1) {
                $(value).show();
            } else {
                $(value).hide();
            }

        });
    });

//  Getting the All the checkboxes checked
    $(document).on(' change', 'input[id="all"]', function() {
        if ($(this).is(':checked')){
            $('#pay_now').removeAttr('disabled');
            $('#split_event').show();
            if ($('.check_all:checked').length > 0 ){

            }

        }else{
            $('#split_event').hide();
            $('.mer_tip').each(function (i,v) {
                v.value = '';
                $('#pay_now').prop('disabled', 'disabled');
            });
        }

        $('.show_division').html('');
        $('.check_all').prop("checked", this.checked);
    });
    $(document).on('change','input[class="check_all"]', function () {
        $('#pay_now').prop('disabled', false);
        if ($('.check_all:checked').length == $('.check_all').length){
            $('input[id="all"]').prop("checked", true);
            $('#split_event').show();

        }else if ($('.check_all:checked').length > 1){
            $('#split_event').show();
        }

        else{
            $('input[id="all"]').prop("checked", false);
            $('#split_event').hide();
        }
//      $('input[id=show_'+this.id.split('_')['1']+']').val('');
//      returnAmountwithData();


    });
//  split even the Total amount of the Tip wallet
    $(document).on('click','#split_event',function () {
        returnAmountwithData();
    });
    $("#split_event").click(function () {
        $('ul#sub_merchants input[type=checkbox]').each(function() {
            if (!$(this).is(":checked")) {
                $(".mer_tip").val('');
            }
        });
    });

    function returnAmountwithData() {
        var selected = [];
        $('ul#sub_merchants input[type=checkbox]').each(function() {
            if ($(this).is(":checked")) {
                selected.push($(this).attr('id'));
            }
        });

        var divide= selected.length;
        $.each(selected,function(k,v){
            var id= v.split('_')['1'];
            var li_find= document.getElementById("item_"+id);
            var total_amount= $('#total_amount').html();
            var cut=(parseFloat(total_amount) / parseFloat(divide)).toFixed(3);
            $('input[id=show_'+id+']').val(cut);

        })
    }



    $(".pay_tip_login").validate({
        rules: {
            "password": {
                required: true
            }
        },
        messages: {
            "password": {
                required: "Password is required"
            }
        }
    });
    $(".transfer_tip").validate({
        rules: {
            "amount": {
                required: true
            }
        },
        messages: {
            "amount": {
                required: "Amount is required"
            }
        }
    });
    $(document).on('click','#btn_pay_tip',function () {
        "$('#cover-spin').show(0)"
    })
//................................location_transaction.js.erb...........//
    const urlParams = new URLSearchParams(window.location.search);
    const queryParam = urlParams.get('query');
    const notification = urlParams.get('notification_msg');
     if(notification!=""){
         $('#modal_id').show();
     }
    $('#modal_close_btn').click(function(){
        $('#modal_id').hide();
    })
     if(queryParam!=="") {
         var nowDate = new Date();
         var newdate = (nowDate.setDate(nowDate.getDate()));
         var nd = new Date(newdate);
         if (queryParam == "") {
             // $('input[name="query[date]"]').val('');
         } else {
             // $('input[name="query[date]"]').val('');
         }
         if (queryParam == "") {
             $("#exportDate").daterangepicker({
                 "maxSpan": {
                     "days": 90
                 },
                 "startDate": nd,
                 "maxDate": nd
             });
         }
     }
     var tos= $('#tos_vefied_input').val();
          if (tos == "false") {
              $('.sidebar').css('opacity', '0.5');
              $('.sidebar').css('pointer-events', 'none');
          }



});


//    -----------Delete Transfer---------------------------
function delete_transfer(transfer_id){
    $.ajax({
        url: "/merchant/locations/transfer_delete",
        type: "GET",
        data: {id: transfer_id},
        success: function (data) {
            if (data.status=="failed"){
                alert('Cannot Delete Transfer');
                $('.spnr').fadeOut();
                $('#closeModal').modal('hide')
            }else{
                $('.spnr').fadeOut();
                $('#closeModal').modal('hide')
                location.reload()
            }
        },
        error: function (data) {
            alert('Cannot Delete Transfer');
            $('#closeModal').modal('hide')
        }
    });
}

//--------------------------------------transaction/exported_files.html-----------------//


    function reports_count_rel(id) {
        count = parseInt($("#reports_read_count").text());
        status = $("#a_" + id).data('status');
        if (status == "false" || status == false) {
            if (count == 1) {
                $('#reports_read_count').parent().parent().hide();
            } else {
                $("#reports_read_count").text(count - 1);
            }
            $("#a_" + id).data('status', "true");
        }
        $('#' + id).hide();
    }
    function show_transaction(url, trans, wallet_id) {
        window.location.href =
            url + "?trans=" + trans + "&wallet_id=" + wallet_id + "&offset=" + encodeURIComponent(moment().local().format('Z'))
    }
function getTransferWalletBalance(wallet_id,type){
    $.ajax({
        url: "/merchant/locations/show_merchant_balance",
        type: "GET",
        data: {source: wallet_id},
        success: function (data) {
            if (data.balance!=""){
                $('#'+type+'_available_amount').text(data.balance);
            }else{
                $('#'+type+'_available_amount').text('0.00');
            }
        },
        error: function (data) {
            $('#'+type+'_available_amount').text('0.00');
        }
    });
}
$(document).on("click", "#transfer_details", function () {
    $("#transaction_id").text($(this).data('transaction_id'));
    $("#transfer_datee").text($(this).data('date'));
    $("#transfer_sender").text($(this).data('from'));
    $("#transfer_receiver").text($(this).data('to'));
    $("#transfer_memo").text($(this).data('memo'));

});
//.........................wallet_transactions.html.erb
$("#amount,#trans_amount").keyup(function (e) {
    var str =$('#amount,#trans_amount').val();
    // str=str.replace(/,/g, "")
    // numberWithCommas(str)
});
$('#modal_close_btn').click(function () {
    $('#modal_id').hide()
})
$('input[name="query[date]"]').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('')
})
$('input[name="query[date]"]').on('apply.daterangepicker', function (ev, picker) {
    var startDate = picker.startDate;
    var endDate = picker.endDate;
    $(this).val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
})
var nowDate = new Date();
var newdate = (nowDate.setDate(nowDate.getDate()));
var nd = new Date(newdate);
$('input[name="query[date]"]').daterangepicker({
    format: 'MM/DD/YYYY',
    autoUpdateInput: false,
    "maxSpan": {
        "days":90
    },
    "startDate": nd,
    "maxDate": nd
});
$('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
    $(this).val('');
});

validatePasswordForm("password_form");