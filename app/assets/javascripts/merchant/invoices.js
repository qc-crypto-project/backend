//-------------------------------invoice/index.html.erb---------

$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000)
    
   /* if (tab_name == 'approved') {
        document.getElementById('nav-approved-tab').click();
    } else if (tab_name == 'pending') {
        document.getElementById('nav-pending-tab').click();
    } else if (tab_name == 'rejected') {
        document.getElementById('nav-rejected-tab').click();
    } else {
        document.getElementById('nav-home-tab').click();
    }*/
    $('#send_money_btn').prop('disabled', 'disabled');

    $(document).on('change','#filter',function () {
        $('#b2b_filter').submit();
    })
    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local

    });


    $(".view_request_money").click(function () {
        $(".spnr").fadeIn();
    });
    $(".new_request_btn").click(function () {
        $(".spnr").fadeIn();
    });

    $(document).on("click",".show_cover_new", function () {
        $('#cover-spin').show(0);
    })
    // --------------------- invoice_detail.html.erb---------------//



    $(".accept_reject_btn").attr("disabled",true);

    $(document).on('change','#status', function () {
        if($(this).val()==2 ){
            $('#from_wallet').hide();
            $(".accept_reject_btn").attr("disabled",false);
            $(".wallet_balance").hide();
//          $(".spnr").fadeIn();
        }else{
            $('#from_wallet').show();
            $(".wallet_balance").show();
            $(".accept_reject_btn").attr("disabled",true);
            if($('#from_wallet').val().length != 0){
                $(".accept_reject_btn").attr("disabled",false);
            }else{
                $(".accept_reject_btn").attr("disabled",true);
            }
//          $(".spnr").fadeIn();
        }
    });

    $('#from_wallet').on('change', function () {

        if(this.value.length != 0 && $('#status').val().length != 0){
            $(".accept_reject_btn").attr("disabled",false);
        }else{
            $(".accept_reject_btn").attr("disabled",true);
        }
    });

    $(document).on("click", "#accept_reject_btn", function () {
        if($('#from_wallet > option').length == 1){
            wallet_balance=$("#one_wallet").attr("data-balance");
            status = $("#one_wallet").attr("data-lstatus");
            $("#balance").text(wallet_balance);
            if(status == "true"){
                $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked</strong></div>');
                $(".accept_reject_btn").attr("disabled",true);
            }else{
                $("#errormessage").css('display','none').html('');
                $(".accept_reject_btn").attr("disabled",false);
            }
        }
    });

    var gmtDateTime = moment.utc($('#timeUtc').text(),'YYYY-MM-DD HH:mm:ss');
    var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
    $('#timeUtc').text(local);


//    $('#from_wallet').on('click', function () {
//        $.ajax({
//            url: '/merchant/base/show_wallet_balance',
//            type: 'GET',
//            data: {wallet_id: $(this).val()},
//            success: function (data) {
//                $("#balance").text(data.balance);
//
//            },
//            error: function (data) {
//            }
//        });
//    });
    $('.accept_reject_btn').click(function () {
        $(".spnr").fadeIn();
    });


//----------------------------new_request.html.erb------------------//



        $('#myModal').modal();
        $('#spiner').hide()
        $('#send_money_btn').prop('disabled', true);
        $('#browse_field').css('display','none');
//            if($('#wallet_ids > option').length == 1){
//                wallet_balance=$("#one_wallet").attr("data-balance");
//                status = $("#one_wallet").attr("data-lstatus");
//                $("#balance").text(wallet_balance);
//                if(status == "true"){
//                    $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked</strong></div>');
//                }else{
//                    $("#errormessage").css('display','none').html('');
//                }
//            }
//
//            wallet_balance=$("#wallet_ids").children(":selected").data().balance;
//            wallet_balance = parseFloat(wallet_balance).toFixed(2).toLocaleString();
//            if($("#wallet_ids").val() == ""){
//                $("#balance").text("");
//            }
//            else{
//                $("#balance").text(wallet_balance);
//            }

    $(document).on("change",".wallet_id", function () {
        status = $(this).children(":selected").data().lstatus;
        if (this.value!=''){
            $("#balance").text('');
            $('#spiner').show()
            $.ajax({
                url: '/merchant/invoices/get_balance',
                type: 'GET',
                data: {wallet_id: this.value},
                success: function (data) {
                    $('#spiner').hide()
                    balance=data.balance
                    $("#balance").text(data.balance);
                },
                error: function (data) {

                }
            });
        }
        else{
            $("#balance").text('');
        }
        // console.log(status);
        if(status == "true"){
            $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked</strong></div>');
        }else{
            // $("#errormessage").css('display','none').html('');
        }
    });

    var form_submit=false;
    $('#transfer_form').submit( function (event) {
        if(!form_submit){
            event.preventDefault();
            return false;
        }});
    var globalTimeout = null;
    $(document).on('keyup','#receiver_phone',function(e)
    {
        // if($('#balance').text() < 1)
        // {
        //     $("#message").html("").append('<div class="alert alert-warning" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Insufficient balance to transfer !</strong></div>');
        //     e.preventDefault();
        //     return false;

        // }else {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            var phone = $("#receiver_phone").val();
            if(phone.length != 0) {
                $.ajax({
                    url: '/merchant/base/verify_user',
                    type: 'GET',
                    data: {user_phone: phone},
                    success: function (data) {
                        if (data.success == "Verified"){
                            form_submit = true;
                            $("#errormessage").css("display","block").html("").html('<div class="alert alert-success" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Phone Number Verified!</strong></div>');
                            $("#to_id").val(data.user.id);
                            $("#from_user").html('').html('Name :<span class="font-weight-bold text-success verify_div">' + data.user.name + '</span>');
                            if (($('.amount').val().length != 0) && ($('.wallet_change').val().length != 0 )) {
                                $('#send_money_btn').attr('disabled', false);
                                $('#ref_no').removeAttr('required');
                            } else {
                                $('#send_money_btn').attr('disabled', true);
                            }
                        }else{
                            $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Phone Number Not Verified!</strong></div>');
                            form_submit = false;
                            $("#from_user").html('');
                            $('#send_money_btn').prop('disabled', true);

                        }
                    },
                    error: function (data) {
                        $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Phone Number Not Verified!</strong></div>');
                        form_submit = false;
                        $("#from_user").html('');
                        $('#send_money_btn').prop('disabled', true);
                    }
                });
            }else{
                $('#send_money_btn').prop('disabled', true);
                $("#from_user").html('');
                $("#errormessage").html("");
            }
            // }
        }, 1500);
    });
    var globalTimeout = null;
    $(document).on('keyup','#ref_no',function(e)
    {
        // if($('#balance').text() < 1)
        // {
        //     $("#message").html("").append('<div class="alert alert-warning" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Insufficient balance to transfer !</strong></div>');
        //     e.preventDefault();
        //     return false;

        // }else {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            var ref_no = $("#ref_no").val();
            if($("#ref_no").val().length != 0) {
                $.ajax({
                    url: '/merchant/base/verify_user',
                    type: 'GET',
                    data: {ref_no: ref_no},
                    success: function (data) {
                        console.log(data.success)
                        if (data.success == "Verified"){
                            form_submit = true;
                            $("#errormessage").css("display","block").html("").html('<div class="alert alert-success" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Reference Number Verified!</strong></div>');
                            $("#to_id").val(data.user.id);
                            $("#from_user").html('').html('Name :<span class="font-weight-bold text-success verify_div">' + data.user.name + '</span>');
                            if (($('.amount').val().length != 0) && ($('.wallet_change').val().length != 0 )) {
                                $('#receiver_phone').removeAttr('required');
                                $('#send_money_btn').attr('disabled', false);
                            } else {
                                $('#send_money_btn').attr('disabled', true);
                            }
                        }else{
                            $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Reference Number Not Verified!</strong></div>');
                            form_submit = false;
                            $("#from_user").html('');
                            $('#send_money_btn').prop('disabled', true);
                        }
                    },
                    error: function (data) {
                        $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Reference Number Not Verified!</strong></div>');
                        form_submit = false;
                        $("#from_user").html('');
                        $('#send_money_btn').prop('disabled', true);
                    }
                });
            }else{
                $('#send_money_btn').prop('disabled', true);
                $("#from_user").html('');
                $("#errormessage").html("");
            }
        }, 1500);
        // }
    });


    var array_of_docs= [];

        $('.file_field_jq').hide();

    $("#new_field").on('click',function () {
        $('#browse_field').click();
    });
    $('.file_field_jq').change(function () {

        if (this.value == ""){
            alert('Please select a file to upload')
        }else{
            var form = $('#image_upload');
            var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
            var ext = this.value.split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf']) == -1) {
                alert("Only 'Png', 'Jpeg', 'Docx', 'Doc' ,'Pdf' allowed");

            }else{
                if (file_size <= 4){

                    $('.spnr').fadeIn();
                    $('#image_upload').ajaxSubmit({
                        dataType: 'json',
                        error: function (result) {
                        },
                        complete: function(data, XMLHttpRequest, textStatus, response , result) {
                            var image = JSON.parse(data.responseText);
                            array_of_docs.push(image.doc.id);
                            $('input[name="attachments[]"]').val(array_of_docs);
                            $('#files_section').append('<div class="row file_section_row remove_'+image.doc.id +'">' + '<div class="col-md-2"><i class="fa fa-check-circle fa-2x" style="color:#199b3e;padding-top: 6px"></i></div> ' +
                                '<div class="col-md-8 doc_view" style="    padding-left: 4px" >'+image.doc.add_image_file_name +' <p class="m-0"> '+parseFloat(image.doc.add_image_file_size/1000).toFixed(1)+' kb</p></div> '+'' +
                                '<div class="col-md-2"><a onclick="delete_item('+image.doc.id+')" data-remote="true" method="post"> <i class="fa fa-trash fa-1x" style="color:#9167a7;padding-top: 15px"></i> </a></div> ');
                            $('.spnr').fadeOut("slow");
                        }
                    });


                }else{
                    alert('Please select a file less than 4MB')
                }
            }
        }
    });
    function delete_item(id) {
        var doc= id;
        $('.remove_'+id).remove();
        array_of_docs=  array_of_docs.filter(function (val) {
            return  val !== doc
        });
        $('input[name="attachments[]"]').val(array_of_docs);
        $.ajax({
            url: '/admins/images/cancel_upload_doc' ,
            data:{document_id: doc},
            dataType: 'JSON',
            method: 'get',
            success: function () {
                $('#message').html('').html(  partial= 'shared/flash', locals={key: 'info', value:'Successfuly deleted'})
            },
            error: function () {

            }

        })
    }
    //..............................................mew.js.erb.............//

    $('#wallet_id').on('change',function () {
        $.ajax({
            url: '/merchant/base/show_wallet_balance',
            type: 'GET',
            data: {wallet_id: $(this).val()},
            success: function (data) {
                $("#balance").text(data.balance);
            },
            error: function (data) {
            }
        })
    })

    var form_submit=false;
    $('#transfer_form').submit( function (event) {
        if(!form_submit){
            event.preventDefault();
            return false;
        }});

    $('.verifyUser').click(function(e)
    {
        if($('#balance').text() < 1)
        {
            $("#message").html("").append('<div class="alert alert-warning" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Insufficient balance to transfer !</strong></div>');
            e.preventDefault();
            return false;

        }else {
            var phone = $("#receiver").val();
            $.ajax({
                url: '/merchant/base/verify_user',
                type: 'GET',
                data: {user_phone: phone},
                success: function (data) {
                    form_submit=true;
                    $("#message").html("").html('<div class="alert alert-success" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Verified!</strong></div>');
                    $("#to_id").val(data.user.id);

                    setTimeout(2000 , function () {
                        $('#transfer_form').submit();
                    })

                },
                error: function (data) {
                    $("#message").html("").empty().append('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Not Verified!</strong></div>');
                    form_submit=false;
                }
            });

        }
    });

});
function show_navs(id, status){
    $('#new_request_footer').show();
    $('.pending-table tr:gt(0)').each(function() {
        $('#new_request_footer').show();
        if (status === "Approved") {
            var headLength= $(this).find('th').length;
            if ( ($(this).find('#invoice_td span').text() !== 'Approved')
                && headLength < 1 ) {
                $(this).hide();
            } else {
                $(this).show();
            }
        } else if (status === "In Progress"){
            var headLength= $(this).find('th').length;
            if ( ($(this).find('#invoice_td span').text() !== 'In Progress')
                && headLength < 1 ) {
                $(this).hide();
            } else {
                $(this).show();
            }
        } else if (status === "Cancelled"){
            var headLength= $(this).find('th').length;
            if ( ($(this).find('#invoice_td span').text() !== 'Cancelled')
                && headLength < 1 ) {
                $(this).hide();
            } else {
                $(this).show();
            }
        } else {
            $(this).show();
        }
    });
}

function display() {
    if ($('#nav-request-tab')) {
        $('#new_request_footer').hide();
    }
}