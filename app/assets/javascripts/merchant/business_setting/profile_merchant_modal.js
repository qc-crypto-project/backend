$(document).ready(function () {
    //----------------------setting/_merchant_modal.html.erb------------------//

    $('.spnr').fadeOut("slow");
    $(".edit_user").validate({
        rules: {
            "user[name]": {
                required: true,
                minlength: 3
            },
            "user[last_name]": {
                required: true,
                minlength: 3
            },
            "user[email]": {
                required: true,
                email: true,
            },
            "user[password]": {
                //required: true,
                maxlength: 20,
                minlength: 8
            },
            "user[password_confirmation]": {
                //required: true,
                equalTo: "#user_password"
            },
            "user[wallets][]": {
                required: function () {
                    if ($('#wallets').find('option:selected').length == 0)
                    {
                        return true;
                    }
                    return false
                }
            },
            "user[permission_id]": {
                required: function () {
                    if ($('#permission').find('option:selected').length == 0) {
                        return true;
                    }
                    return false
                }
            },
        },
        messages: {
            "user[email]":{
                required: "Email is required",
                email: "Please enter a valid email address",
                remote: "This Email address already exist!"
            },
            password: {
                required: "Password is required",
                minlength: "Password 6 characters long"
            },
            password_confirmation: {
                required: "Password confirmation is required",
                equalTo: "Password not Matched"
            },
            "user[wallets][]": {
                required: "Please Select Atleast One Location",
            },
            "user[permission_id]": {
                required: "Please Select Permission",
            },
        },
        ignore: ':hidden:not("#wallets,#permission")',
        submitHandler: function(form) { // for demo
            $('.spnr').fadeIn();
            return true;
        }
    });
    $('#user_phone_number').on('keyup', function () {
        var phone = $("#user_phone_number").val();
        var code = $("#user_phone_code").val();
        var reg = new RegExp('^[0-9]+$');
        var phone_number = code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var user_id = $("#user_id").val();
        if(phone.length > 0){
            $('#phone-error').html('');
            if(phone_number.match(reg)) {
                $(':input[type="submit"]').prop('disabled', true);
            $.ajax({
                url: '/merchant/base/verifying_user_phone_email',
                method: 'get',
               // dataType: 'json',
                data: {phone_number: code + $(this).val(), user_id: $("#user_id").val()},
                success: function (result) {
                    if (result.success == "notverified") {
                        if ($('#phone-error').text() == "") {
                            $('#phone-error').html('').html(result.message);
                            $(':input[type="submit"]').prop('disabled', true);
                        }
                    } else {
                        if ($('#error_email').text() == "" && $('user_password_confirmation-error').text() == "") {
                            $('#phone-error').html('');
                            $(':input[type="submit"]').prop('disabled', false);
                        } else {
                            $('#phone-error').html('');
                            $(':input[type="submit"]').prop('disabled', true);
                        }
                    }
                },
                error: function (result) {
                    $('#phone-error').html('');
                    $(':input[type="submit"]').prop('disabled', 'disabled');
                }
            });
            }else{
                $(':input[type="submit"]').prop('disabled', 'disabled');
                $("#phone-error").text('').text('Must be Number');
            }
        }else{
            $(':input[type="submit"]').prop('disabled', 'disabled');
            $("#phone-error").text('').text('Please enter Phone Number');
        }
    });
    $('#user_email').on('keyup', function () {
        var email= $('#user_email').val();
        var user_id = $('#user_id').val();
        $.ajax({
            url: '/merchant/base/verifying_user_phone_email',
            method: 'get',
            dataType: 'json',
            data: {email: email, user_id: user_id },
            success: function (result) {
                if (result.success == "notverified"){
                    $('#error_email').html('').html('<span style="color: red;" >A user with this email already exists</span>');
                    $('#user_email').addClass('is-invalid');
                    $(':input[type="submit"]').prop('disabled', true);
                    $('#add_user').prop('disabled','disabled');
                } else {
                    if($('#error_phone').text()== "" && $('#error_email').text()== "" && $('user_password_confirmation-error').text()==""){
                        $('#error_email').html('')
                        $('#user_email').removeClass('is-invalid');
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                    else {

                        $('#error_email').html('');
                        $('#user_email').removeClass('is-invalid');
                        $(':input[type="submit"]').prop('disabled', true);
                    }
                }
//               $('#add_user').prop('disabled','disabled');
            },
            error: function (result) {
                $('#user_email').removeClass('is-invalid').addClass('is-valid')
                $('#error_email').html('');
                // $('#errorTxt').html('').html('<span class="text-success">Good</span>');
//               if($('#errorTxt1').text() !='Good'){
//
//                   $('#add_user').prop('disabled','disabled');
//               }else{
//                   $('#add_user').prop('disabled','');
//               }
//               $('#add_user').removeAttr('disabled');

            }
        })
    });

    $("#user_password").focusin(function () {
        $('.strength').css('display', "block");
    });
    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            $('.strength').css('display', "none");
        }
    });
    $('#user_password ,#user_password_confirmation').on('keyup change', function () {
        var all_check = $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true
        if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && all_check) {
            $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password</label>");
            if($('#email_label_field').text() == "Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        } else {
            if ($("#user_password_confirmation").val() != "" && all_check){
                $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password<span style='color:red;'> Passwords do not match. Try again.</span></label>");
                $(':input[type="submit"]').prop('disabled', false);
            }
            else
                $(':input[type="submit"]').prop('disabled', true);
        }
        if ($('#user_password').val() == "" && $('#user_password_confirmation').val() == ""){
            $('#message').html('');
            if($('#email_label_field').text() == "Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        }
    });
    $('#update_current_user').click(function (e) {
        var  password = $('#user_password').val()
        var  confirm_password = $('#user_password_confirmation').val()
        var all_check = $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true
        if (value_present(password) ){
            if ((password == confirm_password) && !all_check){
                e.preventDefault();
                alert("Please Enter Valid Password");
            }}

    });
    $("#user_password").on("keyup", function() {
        var password = $(this).val();
        if (password.length == 0){
            $('.strength').css('display', "none");
        }
        else {
            $("#strength_human").html('').append('<li class="list-group-item"><input style="margin-right: 10px;" id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input style="margin-right: 10px;" id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input style="margin-right: 10px;" id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input style="margin-right: 10px;" id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        }
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else{
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });
    $('#user_email, #user_password').focusin(function () {
        $(this).removeAttr('readonly');
    });
    $('#user_password_confirmation-error').css("color","red");

    function countrycode () {
        return $('#user_phone_number').intlTelInput("isValidNumber");
    }

    var reset = function() {
        $('#phone_msg').removeClass('text-danger text-success').text('');
    };
     $("#user_phone_number").inputmask('Regex', {regex: "^[0-9+]+$"});

    $("#user_phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#user_phone_code').val(Object.values(selectedCountryData)[2]);
            $('#user_phone_number').val('');
            reset();
            $('#country').val('');
            space_remove(Object.values(selectedCountryData)[2]);
            if(selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "/assets/libphonenumber/utils.js"
    });
});

function space_remove(code_number) {
    if(code_number.length==1) {
        $('#user_phone_number').addClass('one_digit_code').removeClass('two_digit_code').removeClass('three_digit_code').removeClass('digit_code');
    } else if(code_number.length==2) {
        $('#user_phone_number').removeClass('one_digit_code').addClass('two_digit_code').removeClass('three_digit_code').removeClass('digit_code');
    } else if(code_number.length==3) {
        $('#user_phone_number').removeClass('one_digit_code').removeClass('two_digit_code').addClass('three_digit_code').removeClass('digit_code');
    } else {
        $('#user_phone_number').removeClass('one_digit_code').removeClass('two_digit_code').removeClass('three_digit_code').addClass('digit_code');
    }
}

