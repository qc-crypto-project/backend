$(document).ready(function () {
    $('.permission_form :checkbox').change(function() {
        $(".has-error").hide()
    });


    $("#permission_refund").change(function() {
        if(this.checked) {
            $("#permission_wallet").prop('checked',true);
        }
    });

    $("#permission_wallet").change(function() {
        if(!this.checked) {
            if($("#permission_refund").prop('checked') == true) {
                $("#permission_wallet").prop('checked',true);
            }
        }
    });

    $("#permission_user_view_only").change(function() {
        if(this.checked) {
            $("#permission_user_edit").prop('checked',false);
            $("#permission_user_add").prop('checked',false);
        }
    });

    $("#permission_dispute_view_only").change(function() {
        if(this.checked) {
            $("#permission_dispute_submit_evidence").prop('checked',false);
            $("#permission_accept_dispute").prop('checked',false);
        }
    });

    $("#permission_user_edit").change(function() {
        if(this.checked) {
            $("#permission_user_view_only").prop('checked',false);
        }
    });

    $("#permission_user_add").change(function() {
        if(this.checked) {
            $("#permission_user_view_only").prop('checked',false);
        }
    });

    $("#permission_dispute_submit_evidence").change(function() {
        if(this.checked) {
            $("#permission_dispute_view_only").prop('checked',false);
        }
    });

    $("#permission_accept_dispute").change(function() {
        if(this.checked) {
            $("#permission_dispute_view_only").prop('checked',false);
        }
    });

    $("#permission_qr_redeem").change(function() {
        if(this.checked) {
            $("#permission_qr_view_only").prop('checked',true);
        }
    });


    $("#permission_qr_scan").change(function() {
        if(this.checked) {
            $("#permission_qr_view_only").prop('checked',true);
        }
    });

    $("#permission_qr_view_only").change(function() {
        if(!this.checked) {
            if($("#permission_qr_scan").prop('checked') == true) {
                $(this).prop('checked',true);
            }
        }
    });

    $("#permission_qr_view_only").change(function() {
        if(!this.checked) {
            if($("#permission_qr_redeem").prop('checked') == true) {
                $(this).prop('checked',true);
            }
        }
    });

    validatePermissionForm('permission_form');

    function validatePermissionForm(role) {
        $(function() {
            var formRules = {
                rules: {
                    "permission[name]": {
                        required: true,
                        minlength: 3,
                    },
                    "permission[select]": {
                        required: function (element) {
                            var boxes = $(".checkbox-u");
                            if (boxes.filter(':checked').length == 0) {
                                return true;
                            }
                            return false;
                        },
                        minlength: 1
                    }
                },
                messages:{
                    "permission[name]": {
                        required: "Permission Name is required.",
                        minlength: "Permission Name must have 3 characters"
                    },
                    "permission[select]": "Please Select at least one types of Permission."
                }
            };
            $.extend(formRules, config.validations);
            $('.' + role).validate(formRules);
        });
    };

    $('#select_all').on('change',function () {
        console.log($(this).prop('checked'))
        if($(this).prop('checked'))
        {
            $('.checkbox-u').prop('checked',true)
        }
        else
        {
            $('.checkbox-u').prop('checked',false)
        }
    });

    $('.checkbox-u').change(function () {
        if($(this).filter(':checked').length > 0) {
            $('#permission[select]-error').hide();
        }
    });

    $('#permission_button').on('click', function(){
        var boxes = $(".checkbox-u");
        var permission_name = $("#permission_name").val();
        if (boxes.filter(':checked').length != 0 && permission_name != "") {
            $('#cover-spin').show(0);
        }
    });
});