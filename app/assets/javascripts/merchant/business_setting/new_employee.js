$(document).ready(function () {

    $('#permission').multiselect({
        texts: {
            placeholder    : 'Select Permission', // text to use in dummy input
            search         : 'Search Location(s)',         // search input placeholder text
            // selectedOptions: ' selected',      // selected suffix text
            selectAll      : 'Select All Location(s)',     // select all text
            unselectAll    : 'Unselect All Location(s)',   // unselect all text
            // noneSelected   : 'None Selected'   // None selected text
        },
        search             : false,
        selectAll          : false, // add select all option
        minHeight          : 200,   // minimum height of option overlay
        maxHeight          : 300,  // maximum height of option overlay
        maxWidth           : 212,  // maximum width of option overlay (or selector)
        maxPlaceholderOpts : 1,    // maximum number of placeholder options to show until "# selected" shown instead
        checkboxAutoFit    : true,  // auto calc checkbox padding
        onOptionClick      : function( element, option ){
            var id = option;
            $(id).parent().parent().parent().children().removeClass('selected');
            $(id).parent().parent().parent().children().find('input:checkbox').prop('checked',false);
            $('#ms-list-1 button span').text('').text($(id).prop('title'));
            $(id).parent().parent().addClass('selected');
            $(id).prop('checked',true);
            $('#permission').val('').val($(id).val());
            $('#permission-error').text('');
        },
        onLoad            : function (element) {
            var list_id = element.classList[1];
            $('#'+list_id+'>div.ms-options>ul>li').addClass('li-style').append('<a href="#" class="link"><i class="fa fa-eye"></i></a>');
        }
    });

    $('#wallets').multiselect({
        texts: {
            placeholder    : 'Select Location(s)', // text to use in dummy input
            search         : 'Search Location(s)',         // search input placeholder text
            // selectedOptions: ' selected',      // selected suffix text
            selectAll      : 'Select All Location(s)',     // select all text
            unselectAll    : 'Unselect All Location(s)',   // unselect all text
            // noneSelected   : 'None Selected'   // None selected text
        },
        search             : false,
        selectAll          : true, // add select all option
        minHeight          : 200,   // minimum height of option overlay
        maxHeight          : 300,  // maximum height of option overlay
        maxWidth           : 212,  // maximum width of option overlay (or selector)
        maxPlaceholderOpts : 2,    // maximum number of placeholder options to show until "# selected" shown instead
        checkboxAutoFit    : true,  // auto calc checkbox padding
        onOptionClick      : function( element, option ){
            $('#wallets-error').text('');
        },
    });

    $('a.link').click(function () {
        var permission_id = $(this).siblings('label').children('input:checkbox').val();
        if (permission_id != undefined && permission_id != "") {
            $.ajax({
                url: '/merchant/permission/' + permission_id,
                method: 'get',
                data:{ id: permission_id },
                success: function (result) {
                },
                error: function (result) {

                }
            });
        }
    });

    //--------------------------------------setting/sub_merchant_modal.html-------------------//

    $('.spnr').fadeOut("slow");
    $(".add_new_employee_form").validate({
        rules: {
            "user[name]": {
                required: true,
                minlength: 3
            },
            "user[last_name]": {
                required: true,
                minlength: 3
            },
            "user[email]": {
                required: true,
                email: true
            },
            "user[password]": {
                //required: true,
                maxlength: 20,
                minlength: 8
            },
            "user[password_confirmation]": {
                //required: true,
                equalTo: "#user_password"
            },
            "user[phone_number]": {
                required: true,
                // number: true,
                minlength: 6,
                maxlength: 15
            },
            "user[wallets][]": {
                required: function () {
                    if ($('#wallets').find('option:selected').length == 0)
                    {
                        return true;
                    }
                    return false
                }
            },
            "user[permission_id]": {
                required: function () {
                    if ($('#permission').find('option:selected').length == 0)
                    {
                        return true;
                    }
                    return false
                }
            },
        },
        messages: {
            "user[name]":{
                required: "First Name is required.",
                minlength: "Name must have 3 characters"
            },
            "user[last_name]":{
                required: "First Name is required.",
                minlength: "Name must have 3 characters"
            },
            "user[email]":{
                required: "Email is required",
                email: "Please enter a valid email address"
            },
            password: {
                required: "Password is required",
                minlength: "Password 6 characters long"
            },
            password_confirmation: {
                required: "Password confirmation is required",
                equalTo: "Password not Matched"
            },
            "user[phone_number]": {
                required: "Phone Number is required",
                // number: "Valid Number",
                minlength: "Phone Number must have 6 digits",
                maxlength: "Phone Number Only have 15 digits"
            },
            "user[wallets][]": {
                required: "Please Select Atleast One Location",
            },
            "user[permission_id]": {
                required: "Please Select Permission",
            },
        },
        ignore: ':hidden:not("#wallets,#permission")',
        submitHandler: function(form) { // for demo
            $('.spnr').fadeIn();
            return true;
        }
    });

    $('#user_phone_number').on('keyup', function () {
        var phone = $("#user_phone_number").val();
        var code = $("#user_phone_code").val();
        var reg = new RegExp('^[0-9]+$');
        var phone_number = code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var user_id = $("#user_merchant_id").val();
        if(phone.length > 0){
            $('#phone-error').html('');
            if(phone_number.match(reg)) {
                $(':input[type="submit"]').prop('disabled', true);
                $.ajax({
                    url: '/merchant/base/verifying_user',
                    method: 'get',
                    // dataType: 'json',
                    data: {phone_number: code + $(this).val() , user_id: user_id},
                    success: function (result) {
                        if (result.success == "notverified") {
                            if ($('#phone-error').text() == "") {
                                $('#phone-error').html('').html(result.message);
                                $(':input[type="submit"]').prop('disabled', true);
                            }
                        } else {
                            if ($('#error_email').text() == "" && $('user_password_confirmation-error').text() == "") {
                                $('#phone-error').html('');
                                $(':input[type="submit"]').prop('disabled', false);
                            } else {
                                $('#phone-error').html('');
                                $(':input[type="submit"]').prop('disabled', true);
                            }
                        }
                    },
                    error: function (result) {
                        $('#phone-error').html('');
                        $(':input[type="submit"]').prop('disabled', 'disabled');
                    }
                });
            }else{
                $(':input[type="submit"]').prop('disabled', 'disabled');
                $("#phone-error").text('').text('Must be Number');
            }
        }else{
            $(':input[type="submit"]').prop('disabled', 'disabled');
            $("#phone-error").text('').text('Please enter Phone Number');
        }
    });
    $('#user_email').on('keyup', function () {
        var email= $('#user_email').val();
        var user_id = $('#user_id').val();
        $.ajax({
            url: '/merchant/base/verifying_user',
            method: 'get',
            dataType: 'json',
            data: {email: email, user_id: user_id},
            success: function (result) {
                if (result.success == "notverified"){
                    $('#error_email').html('').html('<span style="color: red;" >A user with this email already exists</span>');
                    $('#user_email').addClass('is-invalid');
                    $(':input[type="submit"]').prop('disabled', true);
                    $('#add_user').prop('disabled','disabled');
                } else {
                    if($('#error_phone').text()== "" && $('#error_email').text()== "" && $('user_password_confirmation-error').text()==""){
                        $('#error_email').html('')
                        $('#user_email').removeClass('is-invalid');
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                    else {

                        $('#error_email').html('');
                        $('#user_email').removeClass('is-invalid');
                        $(':input[type="submit"]').prop('disabled', true);
                    }
                }
//               $('#add_user').prop('disabled','disabled');
            },
            error: function (result) {
                $('#user_email').removeClass('is-invalid').addClass('is-valid')
                $('#error_email').html('');
                // $('#errorTxt').html('').html('<span class="text-success">Good</span>');
//               if($('#errorTxt1').text() !='Good'){
//
//                   $('#add_user').prop('disabled','disabled');
//               }else{
//                   $('#add_user').prop('disabled','');
//               }
//               $('#add_user').removeAttr('disabled');

            }
        })
    });


        if ($('#admin_user_check').is(':checked'))
        {
            $(".locations12").prop('checked', "checked");

        }

    $("#admin_user_check").click(function(){
        if($(this).is(':checked')==true)
        {
            $(".locations12").attr('checked', true);
        }
    });

    $(".locations12").on('click',function () {
        if($("#admin_user_check").is(":checked") == true)
        {
            return false;
        }
    });

    $("#regular_user_check").click(function(){
        if($(this).is(':checked')==true)
        {
            $(".locations12:first").attr('checked', true);
        }

    });


        $('#add_user').click(function (e) {
            if ($("#regular_user_check").is(':checked') == true) {
                if ($(".locations12").is(':checked') == false) {
                    e.preventDefault();
                    alert("Please Select at least One Location");
                }
            }
           var  password = $('#user_password').val()
           var  confirm_password = $('#user_password_confirmation').val()
            var all_check = $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true
            if (value_present(password) ){
            if ((password == confirm_password) && !all_check){
                e.preventDefault();
                alert("Please Enter Valid Password");
            }}

        });


    $("#user_password").focusin(function () {
        $('.strength').css('display', "block");
    });
    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            $('.strength').css('display', "none");
        }
    });
    $('#user_password ,#user_password_confirmation').on('keyup change', function () {
        var all_check = $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true
        if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && all_check) {
            $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password</label>");
            if($('#email_label_field').text() == "Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        } else {
            if ($("#user_password_confirmation").val() != "" && all_check){
                $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password<span style='color:red;'> Passwords do not match. Try again.</span></label>");
                $(':input[type="submit"]').prop('disabled', false);
            }
            else
                $(':input[type="submit"]').prop('disabled', true);
        }
        if ($('#user_password').val() == "" && $('#user_password_confirmation').val() == ""){
            $('#message').html('');
            if($('#email_label_field').text() == "Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        }
    });
    $("#user_password").on("keyup", function() {
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input style="margin-right: 10px;" id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input style="margin-right: 10px;" id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input style="margin-right: 10px;" id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input style="margin-right: 10px;" id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else{
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });
    $('#user_email, #user_password').focusin(function () {
        $(this).removeAttr('readonly');
    });



    function countrycode () {
        return $('#user_phone_number').intlTelInput("isValidNumber");
    }

    var reset = function() {
        $('#phone_msg').removeClass('text-danger text-success').text('');
    };
    $("#user_phone_number").inputmask('Regex', {regex: "^[0-9+]+$"});
    $("#user_phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#user_phone_code').val(Object.values(selectedCountryData)[2]);
            $('#user_phone_number').val('');
            reset();
            $('#country').val('');
            space_remove(Object.values(selectedCountryData)[2]);
            if(selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "/assets/libphonenumber/utils.js"
    });
});
$('#user_phone_number').each(function () {
    var id = $(this).attr('id');
    var phone_code_id = $(this).next().attr("id");
    $("#"+id).intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $("#"+phone_code_id).val(Object.values(selectedCountryData)[2]);
            space_remove(Object.values(selectedCountryData)[2], id);
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
})

function space_remove(code_number) {
    if(code_number.length==1) {
        $('#user_phone_number').addClass('one_digit_code').removeClass('two_digit_code').removeClass('three_digit_code').removeClass('digit_code');
    } else if(code_number.length==2) {
        $('#user_phone_number').removeClass('one_digit_code').addClass('two_digit_code').removeClass('three_digit_code').removeClass('digit_code');
    } else if(code_number.length==3) {
        $('#user_phone_number').removeClass('one_digit_code').removeClass('two_digit_code').addClass('three_digit_code').removeClass('digit_code');
    } else {
        $('#user_phone_number').removeClass('one_digit_code').removeClass('two_digit_code').removeClass('three_digit_code').addClass('digit_code');
    }
}


