//-------------------checks.html.erb-----------------

$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000)
    $('button.btn.btn-default').on('click', function () {
        $('.modal.fade').removeClass('show in').attr('style', 'display: none;');
    });


    checkImportValidation();
    $('#bulk_check_tab, #check_tab, .view_check_details, .import_checks').click(function () {
        $('.spnr').fadeIn();
    });
    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });

    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local

    });


    $('#new_check').on('click' , function () {
        $('body').css('padding-right', '0px');

    });

    $('.send_check').on('click' , function () {
        $('.spnr').fadeIn();
        $('#export_class').attr('disabled',true);
    });

    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    })

//------------------------------all_bulk_checks.html.erb-----------------

    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });


    $('#bulk_check_tab, #check_tab, .import_checks').click(function () {
        $('.spnr').fadeIn();
    });



    //--------------------_all_bulks.html.--------------

    $('#bulk_check_detail').click(function () {
        $('.spnr').fadeIn();
    });

//--------------------debit_card_deposit.html---------------------//
    $('#export_modal').on("click", function () {
        $('#download_link').removeClass('disabled');
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    });

    $('#bulk_check_tab, #check_tab, .view_check_details, .import_checks , .send_check').click(function () {
        $('.spnr').fadeIn();
    });
    $('#new_check').on('click' , function () {
        $('body').css('padding-right', '0px');

    });
    $('.send_check').on('click' , function () {
        $('.spnr').fadeIn();
        $('#export_modal').attr('disabled',true);
    });

//---------------------------instant_ach/index.html.erb---------------------


    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });

//---------------------------giftcard-----merchant_orders.html---------------------//
    $('.form-inline').css('display','inherit');
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#giftcard_data_datatable').DataTable({
        responsive: true,
        Paginate: true
        ,
        language : {
            sLengthMenu: "Show _MENU_",
            emptyTable: "Record Not Found!"
        }
    });
    var verify = $('#query_input').val();
    if(verify == "false"){
        $('.hide_default').css('display', 'none');
    }else{
        $('.hide_default').css('display', 'block');
    }

    $('#transaction-gift-card').DataTable({
        responsive: true,
        language : {
            sLengthMenu: "Show _MENU_"
        }
    });
    $('#transaction-my-gift-card').DataTable({
        responsive: true,
        language : {
            sLengthMenu: "Show _MENU_"
        }
    });
//--------------------------------giftcard_catalogues.html.erb--------------------//

    $('.form-inline').css('display', 'inherit');
    $('select[name=transaction_length]').addClass('ml-2');

    $('.view_gift_card').click(function () {
        $('.spnr').fadeIn();
    });
    $("#txt_searchall").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#transaction tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });

    });

});