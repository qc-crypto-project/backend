//-------------------checks.html.erb-----------------

$(document).ready(function () {
    $(document).on("keypress focus keypress","#exp_date_debit",function () {
        $('#exp_date_debit').inputmask('99/99')
    });

    $('#alert-message-top').fadeOut(5000)
    $('button.btn.btn-default').on('click', function () {
        $('.modal.fade').removeClass('show in').addClass("display-none");
    });

    $(window).keydown(function(event){
        if(event.keyCode == 13 || event.keyCode == 38|| event.keyCode == 40) {
            event.preventDefault();
            return false;
        }
    });
    checkImportValidation();
    $('#bulk_check_tab, #check_tab, .view_check_details, .import_checks').click(function () {
        $('.spnr').fadeIn();
    });
    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });
    $(document).on('change','#filter',function () {
        $('#check_filter').submit();
    })
    $(document).on('change','#location_id',function () {
        $("#close-btn2").prop("disabled", true);
    });
    $(document).on('change','.p2c_filter',function () {
        $('#p2c_filter').submit();
    })

    $('#new_check').on('click' , function () {
        $('body').css('padding-right', '0px');

    });

    $('.send_check').on('click' , function () {
        $('.spnr').fadeIn();
        $('#export_class').attr('disabled',true);
    });

    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    })

//------------------------------all_bulk_checks.html.erb-----------------

    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });


    $('#bulk_check_tab, #check_tab, .import_checks').click(function () {
        $('.spnr').fadeIn();
    });



    //--------------------_all_bulks.html.--------------

    $('#bulk_check_detail').click(function () {
        $('.spnr').fadeIn();
    });

//--------------------debit_card_deposit.html---------------------//
    $('#export_modal').on("click", function () {
        $('#download_link').removeClass('disabled');
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    });

    $('#bulk_check_tab, #check_tab, .view_check_details, .import_checks , .send_check').click(function () {
        $('.spnr').fadeIn();
    });
    $('#new_check').on('click' , function () {
        $('body').css('padding-right', '0px');

    });
    $('.send_check').on('click' , function () {
        $('.spnr').fadeIn();
        $('#export_modal').attr('disabled',true);
    });

    //.....................inline function................//
    $(document).on('click','.check_disabled_function',function () {
        disableButton1('','submit_form')
    })
    $(document).on('click','.p2c_disabled_function',function () {
        disableButton('','submit_form');
    })
    $(document).on('click','.check_function',function () {
        myFunction()
    });
    $(document).on('click','.bulk_submit_function',function () {
        resend_check();
    });
    $(document).on('click','.show_bulk_check_function',function () {
        var id = $(this).data();
        checkRemove(id);
    })
//---------------------------instant_ach/index.html.erb---------------------
    $(document).on('click','.check_under_review',function () {
        var c_id = $('#check_id_input').val();
        check_under_review(c_id);
    })
 $(document).on('click','.void_function',function () {
     var c_id = $(this).data('id');
     void_function(c_id);
    })

    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });

        $('#alert-message-top').fadeOut(5000);


//---------------------------giftcard-----merchant_orders.html---------------------//
    $('.form-inline').css('display','inherit');
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });


    $('.send_check').on('click' , function () {
        $(this).html("<i class='fa fa-spinner fa-spin'></i>").addClass("pointer-none");
        $('#export_class').attr('disabled',true);
    });
    $('.send_check1').on('click' , function () {
        // $(this).html("<i class='fa fa-spinner fa-spin'></i>");
        $('#export_class').attr('disabled',true);
    });

    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local

    });
//...........................................show_bulk_checks.erb............//
    $(".resendModalDialog").click(function () {
        $(".modal-body #routing_number").val($(this).data('routing'));
        $(".modal-body #account_number").val($(this).data('account'));
        $(".modal-body #confirm_account").val($(this).data('confirmacc'));
        $(".modal-body #check_id").val($(this).data('cid'));
    });

    $('.delete').on('click',function (e) {
        if (confirm("Are you sure?")) {
            id = $(this).attr('id');
            $("#" + id).text("");
            $("#" + id).replaceWith("<button class='btn btn-danger btn-sm' style='color: orange;' disabled>Removing</button>");
        } else {
            e.preventDefault();
            return false;
        }
    });
});
function void_function(id) {
    $("#void_form").attr('action','/merchant/checks/'+id+'');
    $("#check_id").val('').val(id);
    check_under_review(id);
}

var check_under_review = function (ach_id) {
    $.ajax({
        url: "/merchant/checks/check_under_review",
        type: 'GET',
        data: {id: ach_id},
        success: function (data) {
            $('#cover-spin').fadeOut();
            if (data) {
                $('#modal-id2').addClass('show in display-block').removeClass('display-none');
            }
            else {
                $('#modal-id').addClass('show in display-block').removeClass('display-none');
            }
        }
    });
}

function resend_check() {
    var che_id = $("#check_id").val();
    $("#check_id_"+che_id).text("");
    $("#check_id_"+che_id).text("Resending");
    var valuesToSubmit = $('#form_1258').serialize();

    $.ajax({
        type: "POST",
        url: '/merchant/checks/resend_check', //sumbits it to the given url of the form
        data: valuesToSubmit,
    });
    $('#resendModalDialog').modal('toggle');
}

function checkRemove(checkId){
    if (confirm("Are you sure?")) {
        $("#delete-" + checkId).text("");
        $("#delete-" + checkId).replaceWith("<button class='btn btn-danger btn-sm' style='color: orange;' disabled>Removing</button>");
    } else {
        e.preventDefault();
        return false;
    }
    wallet_id = $("#from_wallet").children(":selected").val();
    $.ajax({
        url: "/merchant/checks/" + checkId,
        type: 'DELETE',
        data: {id: checkId, wallet_id: wallet_id},
        success: function (data) {
            // do something here like for example replace some arbitrary container's html with the stuff returned by server.
        }
    });
}
