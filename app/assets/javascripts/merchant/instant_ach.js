//-------------------checks.html.erb-----------------
$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000);
    $('button.btn.btn-default').on('click', function () {
        $('.modal.fade').removeClass('show in').addClass("display-none");
    });

    $(document).on('change','#filter',function () {
        $('#instant_ach_filter').submit();
    })
    checkImportValidation();
    $('#bulk_check_tab, #check_tab, .view_check_details, .import_checks').click(function () {
        $('.spnr').fadeIn();
    });
    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });
    $('#new_check').on('click' , function () {
        $('body').css('padding-right', '0px');

    });

    $('.send_check').on('click' , function () {
        $('.spnr').fadeIn();
        $(this).html("<i class='fa fa-spinner fa-spin'></i>");
        $('#export_class').attr('disabled',true);
    });

    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    })

    $(document).on('click','.instant_ach_sumit',function () {
        submit_ach()
    })
    $(document).on('click','.ach_under_review',function () {
        var id = $(this).data("id")
        check_under_review(id)
    })
    $(document).on('click','.ach_void_function',function () {
        var id = $(this).data("id");
        void_function(id);
    })
    function void_function(id) {
        $("#void_form").attr('action','/merchant/checks/'+id+'');
        $("#check_id").val('').val(id);
        check_under_review(id);
    }
    var check_under_review = function (ach_id) {
        $.ajax({
            url: "/merchant/checks/check_under_review",
            type: 'GET',
            data: {id: ach_id},
            success: function (data) {
                $('#cover-spin').fadeOut();
                if (data) {
                    $('#modal-id2').addClass('show in display-block').removeClass("display-none");
                }
                else {
                    $('#modal-id').addClass('show in display-block').removeClass("display-none");
                }
            }
        });
    }
//------------------------------all_bulk_checks.html.erb-----------------

    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });


    $('#bulk_check_tab, #check_tab, .import_checks').click(function () {
        $('.spnr').fadeIn();
    });



    //--------------------_all_bulks.html.--------------

    $('#bulk_check_detail').click(function () {
        $('.spnr').fadeIn();
    });

//--------------------debit_card_deposit.html---------------------//
    $('#export_modal').on("click", function () {
        $('#download_link').removeClass('disabled');
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    });

    $('#bulk_check_tab, #check_tab, .view_check_details, .import_checks , .send_check').click(function () {
        $('.spnr').fadeIn();
    });
    $('#new_check').on('click' , function () {
        $('body').css('padding-right', '0px');

    });
    $('.send_check').on('click' , function () {
        $('.spnr').fadeIn();
        $('#export_modal').attr('disabled',true);
    });

//---------------------------instant_ach/index.html.erb---------------------


    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });

//---------------------------giftcard-----merchant_orders.html---------------------//
    $('.form-inline').css('display','inherit');
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
});
var lt= $('.timeUtc').each(function (i,v) {
    var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
    var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
    v.innerText = local

});