$(document).ready(function () {
    count=$('#transaction_count').val();
    new_message_count=$('#new_message_count').val();
    if(count==1){
        tab_notifi=parseInt($('#chat_read_count').text());
        b=tab_notifi-new_message_count;
        $('#chat_read_count').text(b);
        $('#chat_notification').addClass('display-none');
    }
    else if(new_message_count!=undefined && parseInt(new_message_count) !=0){
        tab_notifi=parseInt($('#chat_read_count').text());
        b=tab_notifi-new_message_count;
        $('#chat_read_count').text(b);
        if(b==0){
            $('#chat_notification').addClass('display-none');
        }
    }
});
$(document).on("click", "#chat_click", function (ev) {
    var url = $(this).data("url-path");
    var method = $(this).data("method");
    transaction_id=$(this).data('transactionId');
    count= $('.badge-warning_'+transaction_id).text();
    notify=0
    if($('.badge-warning_'+transaction_id).text()!=''){
        notify=parseInt($('.badge-warning_'+transaction_id).text());
    }
    else{
    notify=notify;
    }
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $("#inbox-messages").animate({ scrollTop: 20000000 }, "slow");
            $('#inbox-messages').on("scroll", function(){
                scrollFunction($(this));
            })

            $("#chat_body").html("").html(data);
            tab_notifi=parseInt($('#chat_read_count').text());
            if (tab_notifi>0){
                b=tab_notifi-notify;
                $('#chat_read_count').text(b);
                if(b==0){
                    $('#chat_notification').addClass('display-none');
                }
            }
            $('.badge-warning_'+transaction_id).text('');
        }
    });
    return false;
});

$(document).on('submit',".chat_form",function( event ) {
    chat_id=$('#transaction_id').val();
    message=$('#footer-searchbar').val();
    $('.temp_'+chat_id).append('<div class="col-lg-12 col-sm-12 col-md-12">\n' +
        '                  <div class="row margin-15" >\n' +
        '                    <div class="col-lg-6 col-sm-6 col-md-6"></div>\n' +
        '                    <div class="col-lg-6 col-sm-6 col-md-6">\n' +
        '                      <div class="chat-respond-messages pull-right">\n' +
        '                        <div class="respond-chat-container">\n' +
        '                          <div class="chat-respond msg">\n' +
        '                            <div class="flippd">\n' +
        '                              <div class="chatmsg">'+message+'</div>\n' +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                  </div>\n' +
        '                </div>');
    setTimeout(function(){
        $('#footer-searchbar').val('');
    }, 500);
});

$(document).on('click',".send-btn",function( event ) {
    $("#filename").html("");
    text = $("#footer-searchbar").val();
    if (text == "") {
        event.preventDefault();
        return false;
    }
});

var dropZone = $("#" + "drop-zone1");
var inputFile = dropZone.find("input");
var finalFiles = {};

$(function() {
    $(document).on('change',"#attachments_document", function(e) {

        finalFiles = {};
        $('#filename').html("");
        var fileNum = this.files.length,
            initial = 0,
            counter = 0;

        $.each(this.files,function(idx,elm){
            finalFiles[idx]=elm;
        });

        for (initial; initial < fileNum; initial++) {
            counter = counter + 1;
            $('#filename').append('<div class="file_div" id="file_'+ initial +'"><span class="fa-stack fa-lg"><i class="fa fa-file fa-stack-1x "></i><strong class="fa-stack-1x" >' + counter + '</strong></span>' + this.files[initial].name + '&nbsp;&nbsp;<div class="fa fa-trash fa-lg closeBtn1" title="remove"></div></div>');
        }
    });
});


$('div').on("click", ".closeBtn1", function (e) {
    removeLine1($(this))
});

function removeLine1(obj)
{
    inputFile.val('');
    var jqObj = $(obj);
    var container = jqObj.parent('div');
    var index = container.attr("id").split('_')[1];
    container.remove();
    delete finalFiles[index];
    var array_values = new Array();
    for (var key in finalFiles) {
        array_values.push(finalFiles[key]);
    }
    attachments_document.files = new FileListItem1(array_values)
}

function FileListItem1(a) {
    a = [].slice.call(Array.isArray(a) ? a : arguments)
    for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
    if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
    for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
    return b.files
}

$(document).ready(function () {
    $("#inbox-messages").animate({ scrollTop: 20000000 }, "slow");
});

$('#inbox-messages').on("scroll", function(){
    scrollFunction($(this));
})
function scrollFunction(elem){

    let div = elem.get(0);

    var scrollTop = elem.scrollTop();
    console.log("div.scrollTop - " +div.scrollTop+ " - div.clientHeight - "+ div.clientHeight + " - div.scrollHeight - " + div.scrollHeight)
    // if(div.scrollTop + div.clientHeight <= div.scrollHeight){
    if(div.scrollTop == 0) {
        let t_id = $("#t_id").val();
        let last_msg_id = $("#last_msg_id").val();
        let current_user = $("#current_user_id").val();
        $.ajax({
            url: "/merchant/messages/"+t_id,
            type: 'GET',
            dataType: "json",
            data: {last_msg_id: last_msg_id},
            success: function (data) {
                msgs = data
                if(msgs.length > 0) {
                    jQuery.each(msgs, function (i, data) {
                        // merchant or what
                        if(this.text == null && this.read_at != null){
                            $("#inbox-messages").prepend(' <div class="col-lg-12 col-sm-12 col-md-12">\n' +
                                '                  <p class="available-time no-more">No more messages</p>\n' +
                                '               </div>')
                        }
                        else{
                            let text = this.text != null ? this.text: " <a href="+this.document_url+" target='_blank'>"+this.document_file_name+"</a>"
                            if(current_user == this.recipient_id || current_user == this.document_file_size){
                                $("#inbox-messages").prepend(" <div class=\"col-lg-12 col-sm-12 col-xs-10 col-md-12\">\n" +
                                    "                        <div class=\"row chatbox_responsive_row margin-15\">\n" +
                                    "                          <div class=\"col-lg-6 col-sm-6 col-md-6\">\n" +
                                    "                            <div class=\"chat-sender-messages\">\n" +
                                    "                              <div class=\"chat-container\">\n" +
                                    "                                <div class=\"chat-sender msg\">\n" +
                                    "                                  <div class=\"chatmsg\">\n" +
                                    "                                    " + text + "\n" +
                                    "                                  </div>\n" +
                                    "                                </div>\n" +
                                    "                              </div>\n" +
                                    "                            </div>\n" +
                                    "                          </div>\n" +
                                    "                          <div class=\"col-lg-6 col-sm-6 col-md-6\"></div>\n" +
                                    "                        </div>\n" +
                                    "                      </div>")
                            }else {

                                $("#inbox-messages").prepend("<div class=\"col-lg-12 col-sm-12 col-md-12\">\n" +
                                    "                        <div class=\"row margin-15\" >\n" +
                                    "                          <div class=\"col-lg-6 col-sm-6 col-md-6\"></div>\n" +
                                    "                          <div class=\"col-lg-6 col-sm-6 col-md-6\">\n" +
                                    "                            <div class=\"chat-respond-messages pull-right\">\n" +
                                    "                              <div class=\"respond-chat-container\">\n" +
                                    "                                <div class=\"chat-respond msg\">\n" +
                                    "                                  <div class=\"flippd\">\n" +
                                    "                                    <div class=\"chatmsg\">\n" +
                                    "                                     " + text +"\n" +
                                    "                                    </div>\n" +
                                    "                                  </div>\n" +
                                    "                                </div>\n" +
                                    "                              </div>\n" +
                                    "                            </div>\n" +
                                    "                          </div>\n" +
                                    "                        </div>\n" +
                                    "                      </div>")
                            }
                        }
                    });
                    $("#last_msg_id").val($(msgs).get(-1).id);  // test it
                    $('#inbox-messages').scrollTop(div.clientHeight)

                }
            },
            error: function (data) {
                $('#cover-spin').fadeOut();
            }
        });

    }
}
