$(document).ready(function () {
    //...................................disputes index.html.erb........//

        date_val = setInterval(function(){
            if($('#export_class').data('search-date').length > 0){
                clearInterval(date_val);
            }
            $('#export_class').data('search-date', $('#date_range_picker').val());
        }, 500);
        $('#export_class').data('search-wallets_id',$('#location_ids').val());

    $('#search_field').on('keyup', function () {
        $('#export_class').data('q',$('#search_field').val());

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }});

    $('#search_field').focusin(function () {
        $(this).removeAttr('readonly');
    });
    $('#search_field').keyup(delay(function (e) {
        console.log("Hello ");
        data= $('#search_field').val();
        loc_ids= $('#location_ids').val();
        date_query= $('#date_query').val();
        tx_filter= $('#filter').val();
        $.ajax({
            type: 'GET',
            url : "/merchant/disputes",
            data: {q : data,location_ids:loc_ids,date_query:date_query,tx_filter:tx_filter}
        });
    }, 1500));

    $('#details').click(function(){

        $('#cover-spin').fadeIn();

    });

    $('#password_btn').on('click',function () {
        var password = $('#password').val();
        $.ajax({
            url: "/merchant/transactions/verify_user",
            method: 'get',
            data: {password: password},
            success:function(data){
//                $("#password_btn").css("background","green").text('').text("Verified");
                $("#info-text").text('');
                setTimeout(function(){
                    // $('#password').attr("disabled","disabled");
//                    $('#verifymodal').fadeOut("slow");
                    $('#verify_btn').hide();
                    $('#password_btn').hide();
                    $('#send').show();
                    $('.modal-backdrop').remove();
                }, 1000);
            },
            error: function(data){
                $("#password_btn").css("border","1px solid red");
                $("#info-text").text('').css("color","red").text('Please try again!');
            }
        });
    });
    $('#verifymodal').on('hidden.bs.modal', function () {
        $('#send').hide();
        $('#password_btn').show();
        $('#password').val('')
        // do something…
    })
    function myFunction(id) {
        $("#dispute_id").val(id)
    }

    $("#send").on('click', function () {
        var id = $('#dispute_id').val();
        var btn_url = $(this).attr('href');
        btn_url += "?dispute_id="+id;
        $(this).attr('href', btn_url);
    });
    $('.datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    const urlParams = new URLSearchParams(window.location.search);
        const queryParam = urlParams.get('query');

        $(function() {

            var start = queryParam
            if (start==''){
                var start= new Date(new Date().getFullYear(), new Date().getMonth(), 1)
            }
            var end = queryParam
            if (end==''){
                end = moment();
            }
            function cb(start, end) {
//                $('#date_range_picker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            var date = new Date('January 1, 2019 23:15:30');

            $('#date_range_picker').daterangepicker({
                startDate: queryParam || new Date(new Date().getFullYear(), new Date().getMonth(), 1),
                endDate: end,
                minDate: date,
                maxDate: new Date(),
                ranges: {
                    'Today': [moment(), moment()],
//                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Previous Week': [moment().subtract(6, 'days'), moment()],
//                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'Current Month': [moment().startOf('month'), moment().endOf('month')],
                    'Previous Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            cb(start, end);

        });


        for(i=1;i<6;i++){
            if($('.highcharts-xaxis-labels > :nth-child('+i+')').text()=='lost'){
                $('.highcharts-xaxis-labels > :nth-child('+i+')').text('Lost')
                $('.highcharts-color-0 > :nth-child('+i+')').css('fill','red');
            }
            else if($('.highcharts-xaxis-labels > :nth-child('+i+')').text()=='sent_pending'){
                $('.highcharts-xaxis-labels > :nth-child('+i+')').text('Sent Pending')
                $('.highcharts-color-0 > :nth-child('+i+')').css('fill','#97b2e0');
            }
            else if($('.highcharts-xaxis-labels > :nth-child('+i+')').text()=='under_review'){
                $('.highcharts-xaxis-labels > :nth-child('+i+')').text('Under Review')
                $('.highcharts-color-0 > :nth-child('+i+')').css('fill','#ffa425');
            }
            else if($('.highcharts-xaxis-labels > :nth-child('+i+')').text()=='dispute_accepted'){
                $('.highcharts-xaxis-labels > :nth-child('+i+')').text('Dispute Accepted')
                $('.highcharts-color-0 > :nth-child('+i+')').css('fill','#80000d');
            }
            else if($('.highcharts-xaxis-labels > :nth-child('+i+')').text()=='won_reversed'){
                $('.highcharts-xaxis-labels > :nth-child('+i+')').text('Won Reversed')
                $('.highcharts-color-0 > :nth-child('+i+')').css('fill','green');
            }
        }

    $('#exports_btn').on('click',function () {
        const urlParams = new URLSearchParams(window.location.search);
        const filterParam = urlParams.get('tx_filter');
        search_val=$('#search_field').val();
        filter= filterParam
        loc_ids= $('#location_ids').val();
        date_query= $('#date_query').val();
        var newUrl = $("#exports_btn").attr("href") +"?status="+filter+ "&q="+ search_val+ "&location_ids="+ loc_ids+ "&date_query="+ date_query;
        $("#exports_btn").attr('href', newUrl)
    });
//....................................dispute inline function...............................//
    $(document).on('click','.dispute_function',function () {
        var id= $(this).data()
        myFunction(id);
    })
    $(document).on('click','.dispute_cover_spiner',function () {
        $('#cover-spin').show(0)
    });
    $(document).on('click','.evidence_function',function () {
        del_img('receipt','del_receipt')
    })
    $(document).on('click','.evidence_delete_image',function () {
        var id= $(this).data();
        delete_img(id,'receipt');
    })
    $(document).on('click','.customer_evidence_function',function () {
        del_img('customer_communication','del_customer')
    })
    $(document).on('click','.customer_delete_image',function () {
        var id= $(this).data();
        delete_img(id,'receipt');
    })
    $(document).on('click','.customer_signature',function () {
        del_img('customer_signature','del_sign');
    })
    $(document).on('click','.signature_del',function () {
        var id= $(this).data();
        delete_img(id,'signature');
    })
    //..............................upload_evidance.html.erb...........

        $('.datepicker').datepicker({clearBtn: true,autoclose:true,showButtonPanel: true});
        var evid= $('#evidence_input').val();
        if(evid!="")
        {
            $(".field-disable").prop("disabled", true);
        }

        $('#edit_email').click(function () {
            $('#email_field').toggle();
            $('#email').toggle();
            $('#edit_email').toggle();
        })
        $("[type=file]").on("change", function(){
            // Name of file and placeholder
            var file = this.files[0].name;
            var dflt = $(this).attr("placeholder");
            if($(this).val()!=""){
                $(this).next().text(file);
                $(this).next().next().show()

            } else {
                $(this).next().text(dflt);
            }
        });
        //..............................

        $(document).on('click','.del_shipping',function () {
            var parent= $('.del_shipping').val()
            delete_img(parent)
        })
        $(document).on('click','.delet_image',function () {
            var id= $('.delet_image').val()
            delete_img(id)
        })
        function delete_img(id){
            $("#"+id).hide();
            $.ajax({
                url:  "<%= delete_doc_merchant_disputes_path %>",
                type: 'GET',
                data: {id: id},
                success: function(data) {
                    $('#img_form').show();
                },
                error: function(data){
                    console.log(data);
                }
            });

        }

        function del_img(parent){
            $("#"+parent).val('')
            $("#"+child).hide()
            $("#"+parent).next().text('+Upload a file')

        }


})