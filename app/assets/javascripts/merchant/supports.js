
    $(document).ready(function () {
        $('#alert-message-top').fadeOut(5000)
        //-------------------------supports/index.html.erb----------------------------//
        $(".support_form").submit(function () {
            $('.spnr').fadeIn().fadeOut(8000);
        });
        $(document).on('click','.support_click',function () {
            formReset()
        })
        $(".ticket_view").click(function () {
            $('.spnr').fadeIn();
        });
        var t_id = $('ticked_id_input').val()
        $( "#attention-"+t_id).click(function() {
            $('#attention-'+t_id).removeAttr( "style" );
        });
        function formReset()
        {
            document.getElementById("title").value = "";
            document.getElementById("last_name").value = "";
        }

        var lt= $('.timeUtc').each(function (i,v) {
            var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
    //           var gmtDateTime = moment.utc(v.innerText).toDate();
            var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
            v.innerText = local
        });

        $('.pagination').find('li').find('a').click(function () {
            $('.spnr').fadeIn();
        });
        $('.datepicker').datepicker({
            format: 'mm-dd-yyyy'
        });
        $(".alert").fadeOut(10000);
        $(".alert1").fadeOut(30000);

    ////// ....................................comment.html...........
        $('.button_comment').attr('disabled', true);

        $('.comment_field').bind('keyup', function(){
            if($(this).val().length != 0){
                $('.button_comment').attr('disabled', false);
            }else{
                $('.button_comment').attr('disabled', true);
            }
        });

        $('.comment_form').submit(function () {
            $(".spnr").fadeIn();
        });

    // ----------------------------------support/show.html.....................//
        $('.ticket_open_close_button').click(function () {
            $('.spnr').fadeIn();
        });$(".load").on('click',function () {
            $('.spnr').fadeIn();
            $('.spnr').fadeOut();
        });

        //-----------------------------------------setting/index.html.erb-----------------//
        $('#add_user').on('click', function(){
            $('button.close').click();
            $('button.close').click();
        });
        $('.add_new_employee').click(function () {
            $('.spnr').fadeIn();
        });
        $('.edit_new_employee').click(function () {
            $('.spnr').fadeIn();
        });
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
        $('.ticket_open_close_button').click(function () {
            $('.spnr').fadeIn();
        });
    });
    //..............................index.html.erb..............//
    var array_of_docs= [];
    $(document).ready(function () {
        $('.file_field_jq').hide();
    });
    $("#new_field").on('click',function () {
        $('#browse_field').click();
    });
    $('.file_field_jq').change(function () {

        if (this.value == ""){
            alert('Please select a file to upload')
        }else{
            var form = $('#image_upload');
            var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
            var ext = this.value.split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf','csv','xlsx']) == -1) {
                alert("Only 'Png', 'Jpeg', 'Docx', 'Doc' ,'Pdf', 'csv', 'xlsx' allowed");

            }else{
                if (file_size <= 4){
                    $('.spnr').fadeIn();
                    $('#image_upload').ajaxSubmit({
                        dataType: 'json',
                        error: function (result) {
                        },
                        complete: function(data, XMLHttpRequest, textStatus, response , result) {
                            var image = JSON.parse(data.responseText);
                            array_of_docs.push(image.doc.id);
                            $('input[name="attachments[]"]').val(array_of_docs);
                            $('#files_del').val(array_of_docs);
                            $('#files_section').append('<div class="row file_section_row remove_'+image.doc.id +'" style="margin: 0px ">' + '<div class="col-md-2"><i class="fa fa-check-circle fa-2x" style="color:#199b3e;padding-top: 6px"></i></div> ' +
                                '<div class="col-md-8 doc_view" >'+image.doc.add_image_file_name +' <p class="m-0"> '+parseFloat(image.doc.add_image_file_size/1000).toFixed(1)+' kb</p></div> '+'' +
                                '<div class="col-md-2"><a onclick="delete_item('+image.doc.id+')" data-remote="true" method="post"> <i class="fa fa-trash fa-1x" style="color:#9167a7;padding-top: 15px"></i> </a></div> ');
                            $('.spnr').fadeOut("slow");
                        }
                    });


                }else{
                    alert('Please select a file less than 4MB')
                }
            }



        }

    });

    function delete_item(id) {
        var doc= id;
        $('.remove_'+id).remove();
        array_of_docs=  array_of_docs.filter(function (val) {
            return  val !== doc
        });
        $('input[name="attachments[]"]').val(array_of_docs);
        $.ajax({
            url: '/admins/images/cancel_upload_doc' ,
            data:{document_id: doc},
            dataType: 'JSON',
            method: 'get',
            success: function () {
                $('#message').html('').html(  partial= 'shared/flash', locals={key: 'info', value:'Successfuly deleted'})
            },
            error: function () {

            }

        })
    }

    /*  var array_of_docs= [];
    $(document).ready(function () {
        $('.file_field_jq').hide();
    });
    $("#new_field").on('click',function () {
        $('#browse_field').click();
    });
    $('.file_field_jq').change(function () {

        if (this.value == ""){
            alert('Please select a file to upload')
        }else{
            var form = $('#image_upload');
            var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
            var ext = this.value.split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf','csv','xlsx']) == -1) {
                alert("Only 'Png', 'Jpeg', 'Docx', 'Doc' ,'Pdf', 'csv', 'xlsx' allowed");

            }else{
                if (file_size <= 4){
                    $('.spnr').fadeIn();
                    $('#image_upload').ajaxSubmit({
                        dataType: 'json',
                        error: function (result) {
                        },
                        complete: function(data, XMLHttpRequest, textStatus, response , result) {
                            var image = JSON.parse(data.responseText);
                            array_of_docs.push(image.doc.id);
                            $('input[name="attachments[]"]').val(array_of_docs);
                            $('#files_del').val(array_of_docs);
                            $('#files_section').append('<div class="row file_section_row remove_'+image.doc.id +'" style="margin: 0px ">' + '<div class="col-md-2"><i class="fa fa-check-circle fa-2x" style="color:#199b3e;padding-top: 6px"></i></div> ' +
                                '<div class="col-md-8 doc_view" >'+image.doc.add_image_file_name +' <p class="m-0"> '+parseFloat(image.doc.add_image_file_size/1000).toFixed(1)+' kb</p></div> '+'' +
                                '<div class="col-md-2"><a onclick="delete_item('+image.doc.id+')" data-remote="true" method="post"> <i class="fa fa-trash fa-1x" style="color:#9167a7;padding-top: 15px"></i> </a></div> ');
                            $('.spnr').fadeOut("slow");
                        }
                    });


                }else{
                    alert('Please select a file less than 4MB')
                }
            }



        }

    });

    function delete_item(id) {
        var doc= id;
        $('.remove_'+id).remove();
        array_of_docs=  array_of_docs.filter(function (val) {
            return  val !== doc
        });
        $('input[name="attachments[]"]').val(array_of_docs);
        $.ajax({
            url: '<%= cancel_upload_doc_images_path %>' ,
            data:{document_id: doc},
            dataType: 'JSON',
            method: 'get',
            success: function () {
                $('#message').html('').html('<%= j render partial: 'shared/flash', locals:{key: 'info', value:'Successfuly deleted'}%>')
            },
            error: function () {

            }

        })
    }*/


