
$(document).ready(function(){
    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $("#maxmind_date_range_picker").daterangepicker({
        format: 'MM/DD/YYYY',
        autoclose: true,
        autoUpdateInput: false,
        parentEl: $(".date_parent"),
        "maxSpan": {
            "days":90
        },
        // "startDate": nd,
        "maxDate": nd
    });
    $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
    });
    $('#alert-message-top').fadeOut(5000)
});

$(document).on('change','#tx_maxmind_filter',function () {
    $('#maxmind_filter').submit();
});