
$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000)
    //-------------------------supports/index.html.erb----------------------------//
    $(".support_form").submit(function () {
        $('.spnr').fadeIn().fadeOut(8000);
    });
    $(".ticket_view").click(function () {
        $('.spnr').fadeIn();
    });
    $(document).on('change','#tx_filter',function () {
        $('#permission_filter').submit();
    })
    function formReset()
    {
        document.getElementById("title").value = "";
        document.getElementById("last_name").value = "";
    }

    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
//           var gmtDateTime = moment.utc(v.innerText).toDate();
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });

    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });
    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy'
    });
    $(".alert").fadeOut(10000);
    $(".alert1").fadeOut(30000);

////// ....................................comment.html...........
    $('.button_comment').attr('disabled', true);

    $('.comment_field').bind('keyup', function(){
        if($(this).val().length != 0){
            $('.button_comment').attr('disabled', false);
        }else{
            $('.button_comment').attr('disabled', true);
        }
    });

    $('.comment_form').submit(function () {
        $(".spnr").fadeIn();
    });

// ----------------------------------support/show.html.....................//
    $('.ticket_open_close_button').click(function () {
        $('.spnr').fadeIn();
    });$(".load").on('click',function () {
        $('.spnr').fadeIn();
        $('.spnr').fadeOut();
    });

    //-----------------------------------------setting/index.html.erb-----------------//
    $('#add_user').on('click', function(){
        $('button.close').click();
        $('button.close').click();
    });
    $('.add_new_employee').click(function () {
        $('.spnr').fadeIn();
    });
    $('.edit_new_employee').click(function () {
        $('.spnr').fadeIn();
    });

});

