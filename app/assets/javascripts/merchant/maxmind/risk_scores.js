// risk-score-guage
$(document).ready(function() {
    $('.risk-score-guage').kumaGauge({
        value: Math.floor($('#ac-risk-score').html()),
        gaugeWidth: 15,
        radius: 40,
        paddingX: 18,
        paddingY: 0,
        showNeedle: false,
        valueLabel: {
            display: false
        },
        label : {
            display: false,
            fontSize : 1
        },
        fill :'0-#1cb42f:0-#fdbe37:50-#fa4133:100',

    });
});

$(document).ready(function() {
    $('.ip-risk-score-guage').kumaGauge({
        value: Math.floor($('#ip-risk-score').html()),
        gaugeWidth: 15,
        radius: 40,
        paddingX: 18,
        paddingY: 0,
        showNeedle: false,
        valueLabel: {
            display: false
        },
        label : {
            display: false,
            fontSize : 1
        }
    });
});

$( "#tx_idd-maxmind" ).click(function() {
// $('#welcome').show()
    $('#tx_id-maxmind').toggle();
});

$(document).mouseup(function (e)
{
    var container = $("#tx_id-maxmind"); // YOUR CONTAINER SELECTOR
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
});