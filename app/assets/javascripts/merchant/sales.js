
$( document ).ready(function() {
    validateVirtualTerminalForm('vt-form');
    $(function() {
    var tos_val = $('.tos_verify').val()
        if(tos_val== false){
            $('.sidebar').css('opacity', '0.5');
            $('.sidebar').css('pointer-events', 'none');
        }
    })
    $('#alert-message-top').fadeOut(5000)
    //----------------virtual_terminal.html.erb-------//
    $("#personalInformation").hide();
    $("#accountInformation").hide();
    $("#user_exist").hide();

$(window).on('load',function(){
    $('#modal_id').modal('show');
    $('#spinner').fadeOut();
});
    var country="";
    var errorMap = [ "Country code Invalid", "Invalid selected country", "Too short", "Too long", "Invalid number"];

    $('#country').on('change',function () {
        if($(this).val() != ""){
            $.ajax({
                url: '/merchant/sales/country',
                data: {country: $(this).val() },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#state").empty();
                    $.each(states,function (index, value) {
                        $("#state").append('<option value="' + index+ '">&nbsp;' + index + '</option>');
                    })
                }
            });
        }
    });
    if($('#exp_date_virtual_terminal2').length > 0){
        $('#exp_date_virtual_terminal2').mask('00/00');
    }
    // is_submit_allowed = false;
    // $('.adjust-btn-submit').prop('disabled','true');
    $('#vt1form').hide();
    $('#check_user_exist').click(function () {
        if($(this).prop("checked") == true){
            $('#vt-form').hide();
            $('#vt1form').show();
        } else if($(this).prop("checked") == false) {
            $('#vt1form').hide();
            $('#vt-form').show();
        }
    });

    $("#personalInformation").hide();
    $("#accountInformation").hide();
    $("#user_exist").hide();
    $('#first_name').val('').prop("readonly", true);
    $('#last_name').val('').prop("readonly", true);
    $('#zip_code').val('').prop("readonly", true);
    $('#postal_address').val('').prop("readonly", true);
    $('#city').val('').prop("readonly", true);
    $('#state').val('').prop("readonly", true);
    $('#email').val('').prop("readonly", true);


    $('#phone_number').on('keyup input',function(e)
    {
        if($(this).val().length === 0) {
            $("#personalInformation").hide();
            $("#accountInformation").hide();
            $("#user_exist").hide();
            reset();
        } else if(this.value>=country.length && countrycode()) {
            this.value = this.value.replace(/[^0-9\.]/g,'');
            if (this.value.length !== 0 && this.value.length > 5) {
                $('#spinner').fadeIn();
                $.ajax({
                    url: '/merchant/sales/verify_email_and_phone_number',
                    data: { phone_number: this.value, for_new: "merchants", phone_code: $('#phone_code').val() },
                    method: 'get',
                    success: function (result) {
                        $('#phone_number').prop("readonly", false);
                        $('#phone_msg').text('');
                        if (result.invalid_phone == true) {
                            $('#phone_msg').text('').text('Invalid format of Phone !').removeClass('text-success new_user').addClass('text-danger');
                            // $('#submitform1').prop("disabled", true);
                            $("#personalInformation").hide();
                            $("#accountInformation").hide();
                            $("#existing_user").val('');
                            $('#user_exist').hide();
                        } else if (result.response_json == true) {
                            $("#personalInformation").show();
                            $(".edit_cutomer").show();
                            $(".first_and_last_name").hide();
                            $("#accountInformation").show();
                            $("#user_exist").show();
                            $("#country").hide();
                            $("#country1").show();
                            $("#state").hide();
                            $("#state1").show();
                            $('#first_name').val(result.first_name).prop("readonly", true);
                            $('#last_name').val(result.last_name).prop("readonly", true);
                            $('#zip_code').val(result.zip).prop("readonly", true);
                            $('#postal_address').val(result.address).prop("readonly", true);
                            $('#state1').val(result.state).prop("readonly", true);
                            $('#country1').val(result.country).prop("readonly", true);
                            $('#email').val(result.email).prop("readonly", true);
                            $('#name').val(result.name).prop("readonly", true);
                            $('#postal_address').val(result.address).prop("readonly", true);
                            // $('#country').val(result.country).prop("readonly", true);
                            $('#country').prop('readonly', true);
                            $("#country").val(result.country);
                            // $("#country").append('<option value="' + result.country + '" selected>' + result.country + '</option>');
                            $('#state').prop('readonly', true);
                            $("#state").append('<option value="' + result.state + '" selected>' + result.state + '</option>');
                            $('#city').val(result.city).prop("readonly", true);
                            $('#zip_code').val(result.zip_code).prop("readonly", true);
                            // $('#submitform1').prop('disabled', 'true');
                            $('#phone_msg').removeClass('text-danger new_user').addClass('text-success').text('').text('Verified');
                            $("#existing_user").val('true')
                            $('#verified_user_id').val(result.response_id);
                        }
                        else {
                            $("#personalInformation").show();
                            $(".first_name").hide();
                            $(".edit_cutomer").hide();
                            $(".first_and_last_name").show();
                            $("#accountInformation").show();
                            $("#user_exist").show();
                            $("#existing_user").val('')
                            $('#verified_user_id').val('');
                            $('#phone_msg').removeClass('text-danger text-success').addClass('new_user').text('').text('New User');
                            if ($('#phone_msg').text() == "New User") {
                                // $('#submitform1').prop("disabled", false);
                                $("#country1").hide();
                                $("#country").show();
                                $("#state").show();
                                $("#state1").hide();
                            }
                            $('#first_name').val(result.fname).prop("readonly", false);
                            $('#last_name').val(result.lname).prop("readonly", false);
                            $('#zip_code').val(result.zip).prop("readonly", false);
                            $('#postal_address').val(result.address).prop("readonly", false);
                            $('#city').val(result.city).prop("readonly", false);
                            $('#state').val(result.state).prop("readonly", false);
                            $('#email').val(result.email).prop("readonly", false);
                        }
                        $('#spinner').fadeOut();
                    }
                });
            }else{
            }
        } else {
            if($(this).val().length > 5) {
                var errorCode = $('#phone_number').intlTelInput("getValidationError");
                $('#phone_msg').removeClass("new_user text-success").addClass("text-danger").text(errorMap[errorCode]);
                $("#personalInformation").hide();
                $("#accountInformation").hide();
                $("#user_exist").hide();
            }
        }
    });

    function countrycode () {
        return $('#phone_number').intlTelInput("isValidNumber");
    }

    var reset = function() {
        $('#phone_msg').removeClass('text-danger text-success').text('');
    };

    $('.validate_form,#wallet_id').bind('change focusout',function () {
        if($('#vt-form #wallet_id').val() != "" && $('#vt-form #amount_virtual_terminal').val() != ""&& $('#vt-form #card_name').val() != ""&& $('#vt-form #card_number').val() != "" && $('#vt-form #exp_date_virtual_terminal').val() != "" && $('#vt-form #cvv_field_virtual_terminal').val() != "" && $('#vt-form #first_name').val() != "" && $('#vt-form #last_name').val() != "" && $('#vt-form #postal_address').val() != "" && $('#vt-form #city').val() != "" && $('#vt-form #state').val() != "" && $('#vt-form #email').val() != "" && $('#vt-form #phone_number').val() != ""  && $('#vt-form #zip_code').val() != "" ){
            if($('#vt-form').valid()){
                // $('#submitform1').removeAttr('disabled');
            }
            else {
                // $('#submitform1').prop('disabled','true');
            }
        }
        else{
            // $('#submitform1').prop('disabled','true');
        }
    });
    $(".validate_number").on("keypress keyup blur",function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\. ]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#user_number_virtual_terminal_field').focusout(function(e){
        if($('#user_number_virtual_terminal_field').prop('disabled')==false){
            $.ajax({
                url:'/merchant/sales/verify_email_and_phone_number' ,
                data:{phone_number: this.value, for_existing: "user"} ,
                method:'get',
                success: function (result) {
                    if(result.response_json==true){
                        $('#verify_response_text').css('color','green');
                        $('#verify_response_text').css('font-size','25px');
                        if(result.user_role){
                            $('#verify_response_text').text(result.name + '('+ result.user_role +')');
                        }else{
                            $('#verify_response_text').text(result.name);
                        }
                        $('#verified_user_field').val(result.response_id);
                        $('#user_email_virtual_terminal_field').val(result.email);
                        // $('#submitform1').prop("disabled",false);
                    }else{
                        $('#verify_response_text').css('color','red');
                        $('#verify_response_text').css('font-size','25px');
                        $('#verify_response_text').text('its not verified!');
                        // $('#submitform1').prop("disabled",true);
                    }
                },
                error: function (result) {
                }
            });
            if(this.value!=""){
                $('#user_email_virtual_terminal_field').prop( "disabled", true );
            }else{
                $('#user_email_virtual_terminal_field').prop( "disabled", false );
            }
        }
    });
    $("#edit_customer").click(function(){
        $('#name').prop("readonly", false);
        $('#zip_code').prop("readonly", false);
        $('#postal_address').prop("readonly", false);
        $('#city').prop("readonly", false);
        $('#country').prop("readonly", false);
        $('#state').prop("readonly", false);
        $("#country").prop("readonly", false);
        $("#country1").hide();
        $("#country").show();
        $("#state").show();
        $("#state1").hide();
        $('#email').prop("readonly", false);
        $('#first_name').prop('readonly', false);
        $('#last_name').prop('readonly', false);
    });

    $('#card_number').on('keypress change', function () {
        $(this).val(function (index, value) {
            return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1');
        });
    });

    $('#amount_virtual_terminal').on('input', function () {
        this.value = this.value.match(/^\d+\.?\d{0,2}/);
    });

    $("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#phone_code').val(Object.values(selectedCountryData)[2]);
            $('#phone_number').val('');
            reset();
            $('#country').val('');
            space_remove(Object.values(selectedCountryData)[2]);
            if(selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });

    function space_remove(code_number) {
        if(code_number.length==1) {
            $('#phone_number').css('padding-left', '4.2rem', "important");
        } else if(code_number.length==2) {
            $('#phone_number').css('padding-left', '4.8rem', 'important');
        } else if(code_number.length==3) {
            $('#phone_number').css('padding-left', '5.4rem', 'important');
        } else {
            $('#phone_number').css('padding-left', '6rem', 'important');
        }
    }

    $('#cvv_field_virtual_terminal').keyup( function(e){
        if( $(this).val().length >= 3 ) {
            $('#submitform1').attr('disabled',false)
        } else {
            $('#submitform1').attr('disabled',true);
        }
    });

    $(document).on('click','.submit-btn',function () {
        submit_virtual_terminal_form(); return false;
    })
    $(document).on("keyup","#card_number", function (e) {
        e.preventDefault()
        var card_length = $(this).val().replace(/ /g,'');
        if (card_length.length == 6) {
            $.ajax({
                url: '/merchant/sales/card_number_verify',
                data: {card_number: card_length},
                method: 'get',
                success: function (result) {
                    if (result.success == true) {
                        // alert(result.response_json.type)
                        if (result.response_json.scheme == 'visa') {
                            $("#card_number").css("background-image", "url(<%=asset_path('merchant/icon/visacard_icon.png') %>)");
                        }
                        else if (result.response_json.scheme == 'amex') {
                            $("#card_number").css("background-image", "url(<%=asset_path('merchant/icon/american_express_icon.png') %>)");
                        }
                        else if (result.response_json.scheme == 'discover') {
                            $("#card_number").css("background-image", "url(<%=asset_path('merchant/icon/discover_icon.png') %>)");
                        }
                        else if (result.response_json.scheme == 'mastercard') {
                            $("#card_number").css("background-image", "url(<%=asset_path('merchant/icon/mastercard_icon.png') %>)");
                        }
                        else {
                            $("#card_number").css('background-image', 'none');
                        }
                    }
                }
            });
        }
    });

    $(document).on('click', '#vf_mer', function () {
        var email = $('#mer_email');
        var password = $('#mer_pwd');
        var qr_codes = $('#qr_codes_hidden');
        var user_id = $(this).data('id');
        $('#verify_merchant').submit(function (form) {
            form.preventDefault();
            return false;
        });
        $('#vf_mer').append('<i class="fa fa-spinner fa-spin"></i>').attr('disabled', 'disabled');
        $.ajax({
            url: "/merchant/base/verify_user",
            method: 'get',
            data: {email: email.val(), password: password.val(), qr_codes: qr_codes.val()},
            success: function (result) {
                if (result.user != null && result.user.id == parseInt(user_id)) {
                    if(result.qr_codes != null && result.qr_codes != ""){
                        window.location.href = "/merchant/sales/qr_codes";
                    } else {
                        $('#vf_mer').html('Verified').removeClass('btn-primary').addClass('btn-success').prop('disabled', false).find('i').remove();
                        setTimeout(function () {
                            $('#step1').css('display', 'none').fadeOut("slow");
                            $('#step2').css('display', 'block');
                        }, 1000)
                    }
                } else {
                    $('#vf_mer').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger').prop('disabled', false).find('i').remove();
                    setTimeout(function () {
                        $('#vf_mer').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                    }, 1000);
                }
            }, error: function (result) {
                $('#vf_mer').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger').prop('disabled', false).find('i').remove();
                setTimeout(function () {
                    $('#vf_mer').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                }, 1000);
            }
        });
    });

    $('#vf_mer').attr('disabled','disabled');
    $("#mer_email").on('keyup',function() {
        if (($('#mer_email').val().length!= 0) && ($('#mer_pwd').val().length != 0)){
            if ($('label[class = "error"]').length) {
                if($('label[class = "error"]').html() == ''){
                    $('#vf_mer').removeAttr('disabled');
                }else{
                    $('#vf_mer').attr('disabled','disabled');
                }
            }else{
                $('#vf_mer').removeAttr('disabled');
            }
        }else {
            $('#vf_mer').attr('disabled','disabled');
        }
    });
    $("#mer_pwd").on('keyup',function() {
        if (($('#mer_pwd').val().length != 0) && ($('#mer_email').val().length != 0)){
            if ($('label[class = "error"]').length) {
                if($('label[class = "error"]').html() == ''){
                    $('#vf_mer').removeAttr('disabled');
                }else{
                    $('#vf_mer').attr('disabled','disabled');
                }
            }else{
                $('#vf_mer').removeAttr('disabled');
            }
        }else {
            $('#vf_mer').attr('disabled','disabled');
        }
    });

    $(".pay_tip_login").validate({
        rules: {
            "email": {
                required: true,
                email: true
            },
            "password": {
                required: true
            }
        },
        messages: {
            "email":{
                required: "Email is required",
                email: "Please enter a valid email address"
            },
            "password": {
                required: "Password is required"
            }
        }
    });
    $(document).on('keyup','#last4, #first6, #phone',function () {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    });
    $("#disable_right_click").bind("contextmenu", function (e) {
        e.preventDefault();
    });
    var status = "false";

    $("#scan_btn").on('click', function () {
        $(this).hide();
        $(this).data('status','true');
        status = $(this).data('status');
        $(".message-text").hide();
        $('#cover-spin').show();
        $('.warn-text').text('Please Scan QR Code Now!');
    });
    $("#scan_close").on('click', function () {
        $('#cover-spin').hide();
        $("#scan_btn").show();
    });
    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });
    var chars=[];
    var pressed = false;
    $(document).on('keypress', function(e) {
        chars.push(String.fromCharCode(e.which));
        if (pressed == false) {
            setTimeout(function(){
                if (chars.length >= 5 && status == "true") {
                    $("#scan_btn").data('status', 'false');
                    status = "false";
                    pressed = true;
                    var barcode = chars.join("");
                    $('#scanInput').val(qrToken(barcode));
                    $('#scan_close').hide();
                    $('.warn-text').html('').text('Processing Please Wait!');
                    $.ajax({
                        url: "/merchant/sales/scanner_sale",
                        type: "post",
                        data: {qr_token: qrToken(barcode), scanner: true},
                        success: function (data) {
                            if(data.success == false){
                                $("#cover-spin").hide();
                                $(".message-text").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">&times;</a> <strong>'+data.message+'</strong></div>');
                                $("#scan_btn").show();
                                $(".message-text").fadeOut(30000);
                            } else {
                                $("#cover-spin").hide();
                                $(".message-text").css("display", "block").html("").html('<div class="alert alert-success" role="alert"><a href="#" data-dismiss="alert" class="close">&times;</a> <strong>'+data.message+'</strong></div>');
                                $("#scan_btn").show();
                                $(".message-text").fadeOut(30000);
                            }
                        }
                    })
                }else{
                    // $('.message-text').html('').text('Please scan qr code');
                    chars = [];
                    return false;
                }
                chars = [];
                pressed = false;
            }, 2000);
        }
    });
    function qrToken(barcode) {
        var qrData = JSON.parse(barcode);
        var token = null;
        if (qrData.qr_token) {
            token = qrData.qr_token;
        } else {
            token = qrData;
        }
        return token;
    }
});

function submit_virtual_terminal_form() {
    if($('#vt-form').valid())
    {
        $('#cover-spin').fadeIn();
        $('.app-loader').show();
        $('.warn-text').text('Processing Please Wait!');
        $('#submitform1').prop('disabled','true');
        $('#vt-form').submit();
    }
}