$(document).ready(function () {

    $('#query_risk_eval_').multipleSelect({
        placeholder: "Select Status",
        selectAll: false
    });

    $('#modal_id').click(function(){
        $('#cover-spin').fadeIn();
    });
    $('#shipping_date').daterangepicker({
        format: 'MM/DD/YYYY',
        autoclose: true,
        autoUpdateInput:true,
        minDate: moment(),
        singleDatePicker: true,
    });
    rates = {}
    $("#get_rates").on('click',function () {
        var length = $("#shipping_length").val();
        var width = $("#shipping_width").val();
        var height = $("#shipping_height").val();
        var weight = $("#shipping_weight").val();
        var from_address_id = $("#from_address_id").val();
        var to_address_id = $("#to_address_id").val();
        var selected_carrier = "";
        if(length.length > 0 && width.length > 0 && height.length > 0 && weight.length > 0){
            $("#get_rates").text('').text("Please Wait").attr('disabled','disabled');
            $.ajax({
                url: "/merchant/orders/get_rates",
                method: 'get',
                data: {length: length, width: width, height: height, weight: weight, from_address_id: from_address_id, to_address_id: to_address_id},
                success: function (result) {
                    $("#carrier").show();
                    rates = result.rates;
                    $("#shipment_id").val(result.shipment_id);
                    $("#parcel_id").val(result.parcel_id);
                    $.each(result.carriers, function (index, value) {
                        if(index == 0){
                            selected_carrier = value;
                            $("#insert_carrier").append('<label class="radio-inline margin-left-10px"><input type="radio" class="shipping_carrier" name="shipping[carrier]" value="'+ value +'" checked>'+ value +'</label>');
                        }else{
                            $("#insert_carrier").append('<label class="radio-inline margin-left-10px"><input type="radio" class="shipping_carrier" name="shipping[carrier]" value="'+ value +'">'+ value +'</label>');
                        }
                    });
                    $("#carrier").after('<hr>');
                    $("#service_type").show();
                    set_rate_for_carrier(selected_carrier)
                    $("#get_rates").hide();
                    $('#submit_button').show();
                },
                error: function (result){
                    $("#error-span").show().text(result.message);
                    $("#get_rates").text('').text("Get Rates").removeAttr('disabled')
                    setTimeout(function () {
                        $("#error-span").fadeOut("slow");
                    },20000);
                }
            });
        }else{
            $("#error-span").show();
            setTimeout(function () {
                $("#error-span").fadeOut("slow");
            },2000)
        }
    })

    $(document).on('change','.shipping_carrier',function () {
       carrier = this.value;
        set_rate_for_carrier(carrier)
    });
    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    $(".show-cover-spin").on("click", function () {
        $('#cover-spin').show(0);
    });
    $("#address_warehouse").on('change',function () {
        if(this.value != ""){
            $('#cover-spin').show(0);
            $("#submit_button").attr('disabled','disabled');
            $.ajax({
                url: "/merchant/orders/get_warehouse",
                method: 'get',
                data: {id: this.value},
                success: function (result) {
                    $('#submit_button').prop("disabled",false);
                    set_warehouse_value(result.warehouse.first_name, result.warehouse.last_name, result.warehouse.city, result.warehouse.company, result.warehouse.street, result.warehouse.state, result.warehouse.zip, result.warehouse.phone_number, result.warehouse.email)
                    $('#cover-spin').hide();
                },
                error: function (result){
                    $('#cover-spin').hide();
                    $('#submit_button').prop("disabled",false);
                    set_warehouse_value("", "", "", "", "", "", "", "", "");
                }
            });
        }else{
            set_warehouse_value("", "", "", "", "", "", "", "", "");
            $('#submit_button').prop("disabled",false);
        }
    })
});
$(document).on('change','#filter',function () {
    $('#shipment_filter').submit();
});

$(document).ready(function () {
    $('input[name="query[order_date_time]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[order_date_time]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[order_date_time]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[order_date_time]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
    });

    $('input[name="query[order_date_time]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});

$(document).ready(function () {
    $('input[name="query[shipment_date_time]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[shipment_date_time]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[shipment_date_time]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[shipment_date_time]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
    });

    $('input[name="query[shipment_date_time]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});

$(document).ready(function () {
    $('input[name="query[delivery_date_time]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[delivery_date_time]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[delivery_date_time]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[delivery_date_time]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
    });

    $('input[name="query[delivery_date_time]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});

function set_date(date,days) {
    if ($.isNumeric(days)){
        return moment(date,'YYYY-MM-DD').add(parseInt(days), "days").format("dddd, MMMM Do YYYY");
    }else{
        return moment(date,'YYYY-MM-DD').format("dddd, MMMM Do YYYY");
    }
}

function set_rate_for_carrier(carrier) {
    current_rate = rates[carrier];
    $("#rates_body").empty();
    $.each(current_rate, function (index, value) {
        if ($.isNumeric(value.est_delivery_days)){
            days = value.est_delivery_days
        }else{
            days = 0
        }
        if(index == 0){
            $("#rates_body").append('<tr>\n' +
                '                <td><label class="radio-inline"><input type="radio" name="shipping[rate_id]" value="'+ value.id +'" checked></label><input type="hidden" name="shipping[est_delivery_date]" value="'+ days +'"></td>\n' +
                '                <td>'+ value.service +'</td>\n' +
                '                <td>'+ set_date(value.created_at,days) +' // '+ days +'-Day Delivery</td>\n' +
                '                <td>$'+ value.rate +'</td></tr>');
        }else{
            $("#rates_body").append('<tr>\n' +
                '                <td><label class="radio-inline"><input type="radio" name="shipping[rate_id]" value="'+ value.id +'"></label><input type="hidden" name="shipping[est_delivery_date]" value="'+ days +'"></label></td>\n' +
                '                <td>'+ value.service +'</td>\n' +
                '                <td>'+ set_date(value.created_at,days) +' // '+ days +'-Day Delivery</td>\n' +
                '                <td>$'+ value.rate +'</td></tr>');
        }
    });
}
function set_warehouse_value(first_name, last_name, city, company, street, state, zip, phone_number, email) {
    $("#from_first_name").val(first_name);
    $("#from_last_name").val(last_name);
    $("#from_city").val(city);
    $("#from_company").val(company);
    $("#from_street").val(street);
    $("#from_state").val(state);
    $("#from_zip").val(zip);
    $("#from_phone_number").val(phone_number);
    $("#from_email").val(email);
}

$(document).on('click', "#search_btn", function () {
    $("#offset_new").val(moment().local().format('Z'));
});