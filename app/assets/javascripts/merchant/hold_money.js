//-----------------hold_money/index------------------//
$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000)
    $('input[name="query[date]"]').daterangepicker({
        "maxSpan": {"month": 1},
    });

    $(document).on('change','#filter',function () {
        $('#hold_money_filter').submit();
    })
    $(document).on('click','#tx_idd',function() {
        // $('#welcome').show()
        $('#tx_id').toggle();
    });
    $('.detail_link').on('click', function () {
        var transaction_id = $('#transaction_id_input').val();
        $('#cover-spin').show(0)
    })
    $(document).on('change','.filter',function () {
        $('#hold_money_transaction').submit();
    })
    $('#date-value').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    //------------------------hold_money/transaction.html.erb------------------//
/*    $( "#transaction tr" ).click(function(){
        // event.stopPropagation();
        console.log('hey')
        var link  = $(this).data("href");
        var hold_id = $('#hold_money').val()
        setTimeout(function() {
            $('#cover-spin').fadeOut("slow");
            // alert("Something went wrong with this detail. Please try again later!")
        }, 15000 );
        $.ajax({
            url: link,
            type: "get",
            data: {hold_id: hold_id }
        });
    });*/
    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
});