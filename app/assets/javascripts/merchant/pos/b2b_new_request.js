//.................................new_request.htm.erb.................

    $('#myModal').modal();
    $('#spiner').hide();
    $('#send_money_btn').prop('disabled', true);
    $('#browse_field').css('display','none');

    $(".wallet_id").on("change", function () {
        status = $(this).children(":selected").data().lstatus;
        if (this.value!=''){
            $("#balance").text('');
            $('#spiner').show()
            $.ajax({
                url: '/merchant/invoices/get_balance',
                type: 'GET',
                data: {wallet_id: this.value},
                success: function (data) {
                    $('#spiner').hide()
                    balance=data.balance
                    $("#balance").text(data.balance);
                },
                error: function (data) {

                }
            });
        }
        else{
            $("#balance").text('');
        }
        // console.log(status);
        if(status == "true"){
            $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked</strong></div>');
        }else{
            // $("#errormessage").css('display','none').html('');
        }
    });

    var form_submit=false;
    $('#transfer_form').submit( function (event) {
        if(!form_submit){
            event.preventDefault();
            return false;
        }});
    var globalTimeout = null;
    $('#receiver_phone').on('keyup',function(e)
    {
        // if($('#balance').text() < 1)
        // {
        //     $("#message").html("").append('<div class="alert alert-warning" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Insufficient balance to transfer !</strong></div>');
        //     e.preventDefault();
        //     return false;

        // }else {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            var phone = $("#receiver_phone").val();
            if(phone.length != 0) {
                $.ajax({
                    url: '/merchant/base/verify_user',
                    type: 'GET',
                    data: {user_phone: phone},
                    success: function (data) {
                        if (data.success == "Verified"){
                            form_submit = true;
                            $("#errormessage").css("display","block").html("").html('<div class="alert alert-success" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Phone Number Verified!</strong></div>');
                            $("#to_id").val(data.user.id);
                            $("#from_user").html('').html('Name :<span class="font-weight-bold text-success verify_div">' + data.user.name + '</span>');
                            if (($('.amount').val().length != 0) && ($('.wallet_change').val().length != 0 )) {
                                $('#send_money_btn').attr('disabled', false);
                                $('#ref_no').removeAttr('required');
                            } else {
                                $('#send_money_btn').attr('disabled', true);
                            }
                        }else{
                            $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Phone Number Not Verified!</strong></div>');
                            form_submit = false;
                            $("#from_user").html('');
                            $('#send_money_btn').prop('disabled', true);

                        }
                    },
                    error: function (data) {
                        $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Phone Number Not Verified!</strong></div>');
                        form_submit = false;
                        $("#from_user").html('');
                        $('#send_money_btn').prop('disabled', true);
                    }
                });
            }else{
                $('#send_money_btn').prop('disabled', true);
                $("#from_user").html('');
                $("#errormessage").html("");
            }
            // }
        }, 1500);
    });
    var globalTimeout = null;
    $('#ref_no').on('keyup',function(e)
    {
        // if($('#balance').text() < 1)
        // {
        //     $("#message").html("").append('<div class="alert alert-warning" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Insufficient balance to transfer !</strong></div>');
        //     e.preventDefault();
        //     return false;

        // }else {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            var ref_no = $("#ref_no").val();
            if($("#ref_no").val().length != 0) {
                $.ajax({
                    url: '/merchant/base/verify_user',
                    type: 'GET',
                    data: {ref_no: ref_no},
                    success: function (data) {
                        console.log(data.success)
                        if (data.success == "Verified"){
                            form_submit = true;
                            $("#errormessage").css("display","block").html("").html('<div class="alert alert-success" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Reference Number Verified!</strong></div>');
                            $("#to_id").val(data.user.id);
                            $("#from_user").html('').html('Name :<span class="font-weight-bold text-success verify_div">' + data.user.name + '</span>');
                            if (($('.amount').val().length != 0) && ($('.wallet_change').val().length != 0 )) {
                                $('#receiver_phone').removeAttr('required');
                                $('#send_money_btn').attr('disabled', false);
                            } else {
                                $('#send_money_btn').attr('disabled', true);
                            }
                        }else{
                            $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Reference Number Not Verified!</strong></div>');
                            form_submit = false;
                            $("#from_user").html('');
                            $('#send_money_btn').prop('disabled', true);
                        }
                    },
                    error: function (data) {
                        $("#errormessage").css("display","block").html("").empty().html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Reference Number Not Verified!</strong></div>');
                        form_submit = false;
                        $("#from_user").html('');
                        $('#send_money_btn').prop('disabled', true);
                    }
                });
            }else{
                $('#send_money_btn').prop('disabled', true);
                $("#from_user").html('');
                $("#errormessage").html("");
            }
        }, 1500);
        // }
    });



    $('.amount').bind("change keyup", function () {
        var check_verify = false;
        if( $('.verify_div').length ){
            check_verify = true;
        }
        if(($('.amount').val().length != 0) && ($('.wallet_change').val().length != 0 ) && check_verify ){
            $('#send_money_btn').attr('disabled',false);
        }else{
            $('#send_money_btn').attr('disabled',true);
        }
    });
    $('.wallet_change').on("change", function () {
        var check_verify = false;
        if( $('.verify_div').length ){
            check_verify = true;
        }
        if(($('.amount').val().length != 0) && ($('.wallet_change').val().length != 0 ) && check_verify ){
            $('#send_money_btn').attr('disabled',false);
        }else{
            $('#send_money_btn').attr('disabled',true);
        }
    });
    $('#receiver_phone').on('keyup', function () {
        if($('#receiver_phone').val().length != 0){
            $('#ref_no').attr("disabled",true);
        }else{
            $('#ref_no').attr("disabled",false);
        }
    });
    $('#ref_no').on('keyup', function () {
        if($('#ref_no').val().length != 0){
            $('#receiver_phone').attr("disabled",true);
        }else{
            $('#receiver_phone').attr("disabled",false);
        }
    });
    $(".view_request_money").click(function () {
        $(".spnr").fadeIn();
    });





$("#new_field").on('click',function () {
    $('#browse_field').click();
});

jQuery(function ($) {
    $('div').on('click', '.closeDiv', function () {
        $(this).prev().remove();
        $(this).remove();
        $('#browse_field').val("");
    });

    var fileInput = document.getElementById("browse_field");

    fileInput.addEventListener("change", function (e) {

        var filesVAR = this.files;
        if (this.value == ""){
            return false
        }
        showThumbnail(filesVAR);
        $('input[name="attachments[]"]').val(filesVAR);

    }, false);

    function showThumbnail(files) {
        var file = files[0]

        var $thumbnail = $('#thumbnail').get(0);

        var $image = $("<img>", {
            class: "imgThumbnail"
        });
        var $pDiv = $("<div>", {
            class: "divThumbnail",
            style: "float: left"
        });
        var $div = $("<div>", {
            class: "closeDiv",
            style: "float: right"
        }).html('X');

        $("#thumbnail").html('');
        $pDiv.append($image, $div).appendTo($thumbnail);
        var reader = new FileReader()
        reader.onload = function (e) {
            $image[0].src = e.target.result;
        }
        var ret = reader.readAsDataURL(file);
        var canvas = document.createElement("canvas");
        ctx = canvas.getContext("2d");
        $image.on('load', function () {
            ctx.drawImage($image[0], 100, 100);
        })
    }
});


