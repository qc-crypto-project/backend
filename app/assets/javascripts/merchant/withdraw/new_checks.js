$(document).ready(function () {

    validateCheckForm("check_form");


    function disableButton1(form_Id, btn_Id) {
        console.log(form_Id);
        console.log(btn_Id);
        if (form_Id=='') {

            document.getElementById(btn_Id).disabled = true;
            $('#cover-spin').show(0);
        }
        else if ($("#"+form_Id).valid()){
            document.getElementById(btn_Id).disabled = true;
            $('#cover-spin').show(0);
        }

    }

    $(document).on('focusout change keyup', '#location_id, #check_name, #check_recipient, #check_amount,#check_description', function(){
        if($(this).is(':valid') && $(this).val() !== ""){
            $(this).css('border-color', '#d2d6de');
            $(this).prev().css('border-color', '#d2d6de');
            $(this).closest(".form-group").find("label").css('color', 'black');
        }else{
            $(this).css('border-color', 'red');
            $(this).prev().css('border-color', 'red');
            $(this).closest(".form-group").find("label").css('color', 'red');
        }
    });


    $(document).on('keyup','#check_amount', function () {
        var amount = $(this).val();
        var limit = parseFloat($('#checklimit').val());
        var limit_type = $('#checklimittype').val();
        var check_amount = parseFloat($('#check_amount').val());
        var fee_dollar = 0;
        var fee_perc = 0;
        var balance = $('#balance').text();
        balance = balance.replace(/,/g, "");
        balance = parseFloat(balance);

        if($('#fee_dollar1').val().length > 0 ){
            var fee_dollar = parseFloat($('#fee_dollar1').val());
        }
        if ($('#fee_perc1').val().length > 0) {
            var fee_perc = parseFloat($('#fee_perc1').val());
        }

        if (amount == ""){
            $("#fee_deduct").text('');
            $("#amount_limit_error2").text('');
            $('.amount_error').html('');
        } else if (amount <= 0) {
            $(this).val('');
        }
        else {
            var percentage = 0;
            percentage = (parseFloat(amount) * fee_perc)/100;
            var total_fee = fee_dollar + percentage;
            var total_amount = parseFloat(amount) + total_fee;
            $("#fee_deduct").html('').append('Amount= $ ' + parseFloat(amount).toFixed(2) + ' ');
            $("#fee_deduct").append('<span class="fee_deduct_style">Fee= $ ' + total_fee.toFixed(2) + '</span>');
            $("#fee_deduct").append('Total= $ ' + total_amount.toFixed(2) +'');
            if ((parseFloat(amount)+ parseFloat(total_fee.toFixed(2))).toFixed(2) > balance){
                // $('#fee_deduct').html('');
                $('.amount_error').html('').text("Insufficient balance!");

            }
            else{
                $('.amount_error').html('').text("");
            }

            if (limit_type && limit != 0){
                if ( check_amount > limit && limit_type == "Transaction"){
                    $('.amountlimit_error').html('').append('Exceeds Transaction Limit of $ '+limit.toFixed(2))
                }else{
                    $('.amountlimit_error').html('').append('')
                }
                if (limit_type == "Day"){
                    var total = parseFloat($('#checktotalcount').val());
                    total_sum = total + check_amount;
                    if (total_sum > limit){
                        $('.amountlimit_error').html('').append('Exceeds Daily Limit of $ '+limit.toFixed(2))
                    }else{
                        $('.amountlimit_error').html('').append('')
                    }
                }
            }



        }
    });


    $('#confirm_send').on('click', '[data-dismiss="modal"]', function(e) { e.stopPropagation(); });


    $("#submit_form").click(function () {
        $('#cover-spin').show(0)
        $(this).closest("form")[0].submit();
        $(this).prop('disabled',true)
    });

    $(".abs").click(function () {

        var name=$('#check_name').val();
        var amount=$('#check_amount').val();
        if ($("#location_id").val() == "") {
            $("#no-wallet-check").text("");
            $("#no-wallet-check").append("<div class='alert alert-danger'><p class='location_check_style'> Please select a wallet</p></div>");

        } else {
            if($(".check_form").valid() && $(".amountlimit_error").text()=== "" &&  $(".amount_error").text()=== "")  {
                $('.hello2').modal({ show: true });
                amount = parseFloat(amount).toFixed(2);
                $("#send-amount-12").text("");
                $("#rec-name-12").text("");
                $("#send-amount-12").text(amount);
                $("#rec-name-12").text(name);
            }
        }
    });


    function check_ajax_response(){
        var amount =$("#check_amount").val();
        if (amount === "" ){
            $('#fee_deduct').html('');
        }
    }

    $(document).on('click','.close',function () {
        $('.modal').removeClass('fade');
        $('.modal').modal('hide');
        $('.modal').addClass('fade');
    });


    $("#close-btn1").on("click", function(){

        $(".check_form").valid();

    });

    validateCheckForm("check_form");


    if($("#location_id").val() == ""){
        $("#balance").text("");
    }



    $("#new_check").on("hidden.bs.new_check", function(){

        $(this).find("input:not([type=submit]),textarea,select").val('');
        $("#fee_deduct").html('');
        $("#balance").html('0.00');
        $("#check_name").attr("disabled",true);
        $("#check_recipient").attr("disabled",true);
        $("#check_amount").attr("disabled",true);
        $("#check_description").attr("disabled",true);

    });

    // }

    $("#new_check").on("shown.bs.modal", function(){
        if($("#location_id").children(":selected").val() == 0)
        {

            $("#balance").html('0.00');
            $("#check_name").attr("disabled",true);
            $("#check_recipient").attr("disabled",true);
            $("#check_amount").attr("disabled",true);
            $("#check_description").attr("disabled",true);

        }

    });
    $("#location_id").on("change", function () {
        $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Please wait getting location information!</strong></div>');
        $("#check_name").attr("disabled",true);
        $("#check_recipient").attr("disabled",true);
        $("#check_amount").val("").attr("disabled",true);
        $("#check_description").attr("disabled",true);
        $("#balance").html('0.00');
        $('.amountlimit_error').html('');
        $("#fee_deduct").html('');
        $("#no-wallet-check").html('');
    });


    checkImportValidation();



    $('#export_class').attr('disabled',false);
})