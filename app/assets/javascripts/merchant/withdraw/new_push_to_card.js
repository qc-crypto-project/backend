$(document).ready(function () {

    //...............................debit_card_deposit_form.html.erb.................

    $('.spnr').fadeOut('slow');
    $('#export_modal').attr('disabled',false);

    // For new & existing card

    $(document).on('click','#close-btn1',function (e) {
        e.preventDefault();
    });

    $(document).ready(function () {
        $('#new_check').submit(function () {
            $('.spnr').fadeIn();
        });
    });

    $(document).on('click','.close_p2c',function () {
        $('.modal').removeClass('fade');
        $('.confirm_send_modal').modal('hide');
        $('.modal').addClass('fade');

    });
    $(document).on('click','.close',function () {
        $('.modal').removeClass('fade');
        $('.modal').modal('hide');
        $('.modal').addClass('fade');

    });

    $(".abs").click(function () {
        var name=$('#check_name').val();
        var amount = $('#check_amount').val();
        // check_amount_limit();
        if($("#new_p2c").valid() && $("#errorss_new").text() == "" && $('#amount_limit_error').text() == ""){
            $('.confirm_send_modal').modal({
                show: true
            });
            $(".content-abs").html('').append('<div class="modal-header text-left bg-primary text-white"> ' +
                '<h4 class="modal-title confirm-message">' +
                '<i class="fa fa-money"></i>' +
                '<span class="ml-2">Are you sure?</span>' +
                '</h4> ' +
                '<button type="button" class="closen close_p2c" id="close_btn" >&times;</button> ' +
                '</div> ' +
                '<div class="row"> ' +
                '<div class="col-md-12"> ' +
                '<div class="card-header bordered"> ' +
                '<div class="card-body"> ' +
                '<p>Are you sure you want to send $<span>'+parseFloat(amount).toFixed(2)+'</span> to <span>'+name+'</span>?</p> </div> ' +
                '<div class="card-footer"><span id="close-button9"  class=" btn btn-default btn-md modal-padding check-btn_cnl close_p2c" >Cancel</span> <button type="submit" id="submit_form"  class="modal-padding btn btn-info btn-md pull-right submit_check p2c_disabled_function check-btn_cnl" >Confirm & Send </button></div></div></div> </div>');
        }
    });

    // Function to close confirmation block
    function myFunction() {
        $('.confirm_send_modal').modal('toggle')
        // $('.confirm_send_modal').remove();
    }

    $(document).on('keyup','#check_amount', function () {
        var amount = $(this).val();
        var fee_dollar = 0;
        var fee_perc = 0;
        var balance = $('#balance').text();
        balance = balance.replace(/,/g, "");
        balance = parseFloat(balance);
        var p2c_limit = parseFloat($('#pushlimit').val());
        var p2c_type = $('#pushlimittype').val();
        var p2c_done = 0;

        if ($('#pushtotalcount').val().length > 0) {
            var p2c_done = parseFloat($('#pushtotalcount').val());
        }
        if($('#fee_dollar').val().length > 0 ){
            var fee_dollar = parseFloat($('#fee_dollar').val());
        }
        if ($('#fee_perc').val().length > 0) {
            var fee_perc = parseFloat($('#fee_perc').val());
        }
        if (amount == ""){
            $("#fee_deduct").text('');
            $('#amount_limit_error').html('');
        } else if (amount == 0) {
            $(this).val('');
        } else {
            var percentage = 0;
            percentage = (parseFloat(amount) * fee_perc)/100;
            var total_fee = fee_dollar + percentage;
            var total_amount = parseFloat(amount) + total_fee;
            $("#fee_deduct").html('').append('Amount= $ ' + parseFloat(amount).toFixed(2) + ' ');
            $("#fee_deduct").append('<span id="fee_deduct_data">Fee= $ ' + total_fee.toFixed(2) + '</span>');
            $("#fee_deduct").append('Total= $ ' + total_amount.toFixed(2) +'');
            if ((parseFloat(amount)+ parseFloat(total_fee.toFixed(2))).toFixed(2) > balance){
                // $('#fee_deduct').html('');
                $('#amount_limit_error').html('').text("Insufficient balance!");
                $("#close-btn1").prop("disabled", true);
            }
            else if(p2c_type == "Day"){
                total = parseFloat(amount) + p2c_done;
                if(total > p2c_limit ){
                    $('#fee_deduct').html('');
                    $('#amount_limit_error').html('').append('Exceeds Daily Limit of $ ' + p2c_limit.toFixed(2))
                    $("#close-btn1").prop("disabled", true);
                }else{
                    $('#amount_limit_error').html('');
                    $("#close-btn1").prop("disabled", false);
                }
            }else{
                if(amount > p2c_limit ){
                    $('#fee_deduct').html('');
                    $('#amount_limit_error').html('').append('Exceeds Transaction Limit of $ ' + p2c_limit.toFixed(2))
                    $("#close-btn1").prop("disabled", true);
                }else{
                    $('#amount_limit_error').html('');
                    $("#close-btn1").prop("disabled", false);
                }
            }

        }
    });

    $('body').on('hidden.bs.modal', '#confirm_send', function (e) {
        if ($('#new_check').is(':visible')) {
            $('body').addClass('modal-open');
            $('#new_check').focus();

        }
    });

     validateP2CForm("new_p2c");
    $(".my-password").on('click',function() {
        $(this).toggleClass("fa-eye-slash fa-eye");
        if ($(".card_number_field").attr("type") == "password") {
            $(".card_number_field").attr("type", "text");
        } else {
            $(".card_number_field").attr("type", "password");
        }
    });
    $("#other_card").on('click',function () {
        if($("#other_card").is(':checked')){
            $("#banks_div").fadeOut("slow");
            $('#expand').show();
            $('#banks').removeAttr('required');
        }else{
            $('.rem').val('');
            $("#banks_div").fadeIn("slow");
            $('#expand').hide();
            $('#banks').prop('required',true);
        }
    });


    //........................................debit_card_form.html........................

    $(".del_abs").click(function () {
        var banks = $('#banks').val();
        if(banks !== "" && banks !== null){
            $('.confirm_send_modal_del').modal({
                show: true
            });
            $(".del_content-abs").html('').append('<div class="modal-header text-left bg-primary text-white"> ' +
                '<h4 class="modal-title confirm-message">' +
                '<i class="fa fa-money"></i>' +
                '<span class="ml-2">Are you sure?</span>' +
                '</h4> ' +
                '<button type="button" class="closen" id="close_btn close-del-confirm-modal">&times;</button> ' +
                '</div> ' +
                '<div class="row"> ' +
                '<div class="col-md-12"> ' +
                '<div class="card-header bordered"> ' +
                '<div class="card-body"> ' +
                '<p>Are you sure you want to delete this Card?</p> </div> ' +
                '<div class="card-footer"> <span id="close-button9"  class=" btn btn-default btn-md close-del-confirm-modal">Cancel</span> <button type="button" id=""  class="modal-padding btn btn-info btn-md pull-right delete-card-btn">Confirm & Delete </button></div></div></div> </div>');
        }
    });

    $(document).on('focusout change keyup', '#location_id, #check_name, #check_recipient, #check_amount,#check_description,#card_number,#exp_date_debit,#cvv,#zip_code, #banks', function(){
        if($(this).is(':valid') && $(this).val() !== ""){
            $(this).css('border-color', '#d2d6de');
            $(this).prev().css('border-color', '#d2d6de');
            $(this).closest(".form-group").find("label").css('color', 'black');
        }else{
            $(this).css('border-color', 'red');
            $(this).prev().css('border-color', 'red');
            $(this).closest(".form-group").find("label").css('color', 'red');
        }
    });

    $(document).ready(function () {
        $(".amount_field").inputmask('Regex', {regex: "^[0-9,]{1,99}(\\.\\d{1,2})?$"});
        $("#cvv").inputmask('Regex', {regex: "^[0-9]+$"});
        $(".name_field").inputmask('Regex', {regex:"^[a-zA-Z0-9. ]*$" });
    });

    $(document).ready(function () {
        $('#banks').on('change', function () {
            if ($('#banks').val() != '' ) {
                verify_card_number($('#banks').val(),"present")
            } else {
                // $('#close-btn1').attr('disabled', false);
                $('#errorss').text('');
                $('#errorss_new').text('');
            }
        });
    });

    $("#location_id").on('change',function () {
        $("#balance").html('0.00');
        $("#check_name").prop("disabled", true);
        $("#check_recipient").prop("disabled", true);
        $("#check_amount").val("").prop("disabled", true);
        $("#close-btn1").prop("disabled", true);
        $("#fee_deduct").text('');
        $("#amount_limit_error").text('');
        $("#check_description").prop("disabled", true);
        $("#banks").prop("disabled", true);
        $("#zip_code").prop("disabled", true);
        // $("#close-btn1").prop("disabled", true);
        $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Please wait getting location information!</strong></div>');
    });
    $("#card_number").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $(document).on('change focusout', '#card_number, #banks','#opt', function () {
        var card_number = $('#card_number').val();
        if (card_number.length >= 13 ) {
            verify_card_number(card_number,"not_present")
        } else {
            // $('#close-btn1').attr('disabled', true);
            $("#errorss").text('');
            $("#errorss_new").text('');
        }
    });

    $('#card_number').on('keyup', function () {
        var card_number = $('#card_number').val();
        if (card_number.length === 0 ) {
            $("#errorss").text('').text('');
            $("#errorss_new").text('').text('');
        }
    });

    $('#card_number').keydown(function(event) {
        if(event.keyCode != 8 ){
            if (event.keyCode == 32 || $.isNumeric( event.key )== false ) {
                //event.preventDefault();
            }
        }
        var txt = $("#card_number");
        var func = function() {
            txt.val(txt.val().replace(/\s/g, ''));
        }
        txt.keyup(func).blur(func);
    });

    $("#new_check").on("shown.bs.modal", function(){
        if($("#location_id").children(":selected").val() == 0)
        {
            $("#balance").html('0.00');
            $("#check_name").attr("disabled",true);
            $("#check_recipient").attr("disabled",true);
            $("#check_amount").attr("disabled",true);
            $("#check_description").attr("disabled",true);
            $("#banks").prop("disabled", true);
            $("#zip_code").prop("disabled", true);
        }
    });

    $(document).on('keyup', '#check_amount', function () {
        $(this).val($(this).val().replace(/[^0-9. ]/g, ''));
    });

    $(document).on('focusout', '.decription_field', function () {
        var x= $(".decription_field").val().trim();
        if (x.length === 0)
        {
            $(this).val("")
        }
    })
    //
    // $('#exp_date_debit').keydown(function(event) {
    //     if(event.keyCode != 8 ){
    //         if (event.keyCode == 32 || $.isNumeric( event.key )== false ) {
    //             //event.preventDefault();
    //         }
    //     }
    //     var func = function() {
    //         txt.val(txt.val().replace(/\s/g, ''));
    //     }
    //     var txt = $("#exp_date_debit");
    //     txt.keyup(func).blur(func);
    //     $("#expiry_error").text("");
    // });

    $('.confirm_send_modal_del').on('hidden.bs.modal', function () {
        $('body').css('overflow-y', 'hidden')
    });

    $(document).on("click", ".close-del-confirm-modal", function () {
        myFunction_del();
    });

    $(document).on("click", ".delete-card-btn", function () {
        delete_card();
    })

    $(document).on("click", "#create_p2c_btn", function () {
        $(this).html("<i class='fa fa-spinner fa-spin'></i>");
        $('#export_modal').attr('disabled',true);
    });

    $(document).on("shown.bs.modal", "#new_check", function(){
        $(".send_check").html("Create New").removeClass("pointer-none");
    });

    $(document).on("click", ".submit_check", function () {
        submit_ach();
    })

    $(".revoke-paste").on("paste", function () {
        return false;
    })

    $(document).on("focusout", "#exp_date_debit", function () {
        var expiry = $(this).val();
        var expiry_error = $("#exp_date_debit-error").text();
        if(value_present(expiry)){
            verify_card_expiry(expiry)
        }
        if(value_present(expiry_error)){
            $("#expiry_error").text('');
        }
    });
    $(document).on("keyup", "#exp_date_debit", function () {
        var expiry_error = $("#exp_date_debit-error").text();
        if(value_present(expiry_error)){
            $("#expiry_error").text('');
        }
    });
});
function myFunction_del() {
    $('.confirm_send_modal_del').modal('toggle')
    // $('.confirm_send_modal').remove();
}

function verify_card_number(val,present){
    $("#close-btn1").prop("disabled", true);
    if(val != null ) {
        $.ajax({
            url: "/merchant/checks/present_card_verification",
            method: 'get',
            dataType: 'json',
            data: {card_number: val,present: present},
            success: function (result) {
                var type_check = result.success;
                $("#result").val(type_check);
                if (type_check === "true") {
                    $('#errorss').text('');
                    $('#errorss_new').text('');
                    $("#close-btn1").prop("disabled", false);
                }else{
                    console.log("false");
                    $("#errorss").text('').text('For Debit Card Use Only.');
                    $("#errorss_new").text('').text('For Debit Card Use Only.');
                    $("#close-btn1").prop("disabled", true);
                }
            }
        });
    }else{
        $("#errorss").text('');
        $("#errorss_new").text('');
        // $('#close-btn1').attr('disabled', false);
    }
}

function verify_card_expiry(expiry){
    var expiry_date = expiry.split("/");
    var month = expiry_date[0];
    var year = expiry_date[1];
    var d = new Date();
    var current_month = d.getMonth() + 1;
    var current_year = d.getFullYear().toString().substr(-2);
    if (year > current_year && month <= 12 && month > 0) {
        $("#expiry_error").text('');
        $("#close-btn1").prop("disabled", false);
    } else if(month >= current_month && month <= 12 && year == current_year && month > 0){
        $("#expiry_error").text('');
        $("#close-btn1").prop("disabled", false);
    }else{
        $("#expiry_error").text('').text('Invalid Expiry Date.');
        $("#close-btn1").prop("disabled", true);
    }
}

function delete_card() {

    var bank = $("#banks").children('option').length; // check if there are more then default options in select
    var card_no = $("#banks").val();
    if(bank > 1){
        $("#banks option:selected").remove();//delete the selected option
        $.ajax({
            url: "/merchant/checks/delete_debit_card",
            method: 'get',
            dataType: 'json',
            data: {card_number: card_no},
            success: function (result) {
            }
        });
        $("#banks").val(""); // afrer deleting the card number select the default value of select
        $('#errorss').text('');
        $('.confirm_send_modal_del').modal('toggle')

    }
}

function submit_ach() {
    $('#cover-spin').show(0)
    $('.submit_check').prop('disabled',true);
    $('.submit_check').closest("form")[0].submit();

}