
$(document).ready(function () {

//---------------------------------gift_card----show.html.erb---------------------------


    if($('#wallet_id > option').length == 1){
        status = $('#wallet_id > option').attr("data-lstatus");
        block_gift = $('#wallet_id > option').attr("data-giftcard");
        if (status == "true"){
            $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked </strong></div>');
        }else if (status == "false" && block_gift == true){
            $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Error Code 4004 - Gift Card Option Blocked </strong></div>');
        }else{
            $("#errormessage").css('display','none').html('');
        }

    }
    if($("#wallet_id").val() == ""){
        $("#balance").text("");
    }
    $("#wallet_id").on("change", function () {
        wallet_balance=$(this).children(":selected").data().balance;
        wallet_balance = parseFloat(wallet_balance).toFixed(2);
        wallet_balance = parseFloat(wallet_balance).toLocaleString();
        var sNumber = (wallet_balance).toLocaleString(undefined,
            {'minimumFractionDigits':2,'maximumFractionDigits':2});
        if($("#wallet_id").val() == ""){
            $("#balance").text("");
        }
        else{
            $("#balance").text(sNumber);
        }
        status = $(this).children(":selected").data().lstatus;
        block_gift = $(this).children(":selected").data().giftcard;
//        $("#balance_wallet").html('').append('<span class="">Balance:$ </span>'+ parseFloat(wallet_balance).toFixed(2));
        if (status == "true"){
            $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Location is Blocked </strong></div>');
        }else if (status == "false" && block_gift == true){
            $("#errormessage").css("display","block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">×</a> <strong>Error Code 4004 - Gift Card Option Blocked </strong></div>');
        }else{
            $("#errormessage").css('display','none').html('');
        }
    });
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    $('#gft_amount_input').on("keyup",function (e) {
        //if the letter is not digit then display error and don't type anything
        //  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //     //display error message
        //     $("#errmsg").html("Digits Only").show().fadeOut("slow");
        //            return false;
        // }
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57) ) {
            e.preventDefault();
        }

//      $('#gft_amount_input').on('keyup',function () {
        var min_val = $(".min_val").html();
        var max_val = $(".max_val").html();
        var input_val = $(this).val();
        var f = parseFloat(input_val);
        if(f > parseFloat(max_val) || f < parseFloat(min_val)){
            $('#errorTxt').html('').html('Amount must in between '+min_val+' to '+max_val);
            console.log(f);
        } else{
            $('#errorTxt').html('');
            console.log(f);
        }
        if(($(this).val()> 0) && (emailReg.test( $('.email_field').val() )) && ($('.email_field').val().length != 0) && ($('#errorTxt').text().length == 0)  && ($('#balance').text()!='0.00')){
            $('#buy_now').removeAttr('disabled');
        } else{
            $('#buy_now').prop('disabled', true );
        }
//      });

        // if($.isNumeric($('#gft_amount_input').val())==false){
        //   $('#gft_amount_input').css('border','1px solid red')
        //   $('#gft_amount_input_error').text('your input is not valid')
        //   $("#buy_now").prop('disabled', true);
        // }else{
        //   $('#gft_amount_input').css('border','')
        //   $('#gft_amount_input_error').text('')
        //   $("#buy_now").prop('disabled', false);
        // }
    });
    // console.log($('#gft_amount_input').val());

    $('.spnr').fadeOut('slow');
    $('.buy_button').attr('disabled', true);

    $('.email_field').on('keyup', function () {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if( (emailReg.test( this.value )) && (this.value.length != 0) && ($('#gft_amount_input').val().length != 0)  && ($('#errorTxt').text().length == 0) && ($('#balance').text()!='0.00')){
            $('.buy_button').attr('disabled', false);
        }else{
            $('.buy_button').attr('disabled', true);
        }
    });

    $("#buy_now").click( function (e) {
        if($('#wallet_id').val() == ""){
            $("#gc-wallet").append("<div class='alert alert-danger' style=''><a href='#' data-dismiss='alert' class='close'>×</a><p style='text-align: center;'> Please select a wallet</p></div>");
            e.preventDefault();
            return false;
        }
        else{
            $('.spnr').fadeIn();
            // $('#gc-form').submit();
        }
    });


});