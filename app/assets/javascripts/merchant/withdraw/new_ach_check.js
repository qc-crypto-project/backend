$(document).ready(function () {
//-----------------------new_form.html.erb-----------------//

    $(".account-number").inputmask('Regex', {regex: "^[0-9]+$"});
    $('#export_class').attr('disabled',false);
    jQuery(function() {
        jQuery('form').bind('submit', function() {
            //jQuery(this).find(':disabled').removeAttr('disabled');
            $("#bank_account_name").removeAttr("disabled");
        });
    });
    $(document).on('click','.close_ach',function () {
        $('.modal').removeClass('fade');
        $('.confirm_send_modal').modal('hide');
        $('.modal').addClass('fade');
        $("#submit_button").attr("disabled", false);
    });

    $("#amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
        if($("#location_id").children(":selected").val() == 0) {
            $("#amount").prop("disabled", true);
            $("#description").prop("disabled", true);
            $("#submit_button").prop("disabled", true);
        }
        validateACHForm('ach-form');

    $(".abs").unbind("click").click(function () {
        var amount = $('#amount').val();
        $("#fee_deduct").css({"color": "black"});
        if ($('#ach-form').valid() && $('.amount_limit_error').text() == ''){
            $(this).attr("disabled", true);
            $('.confirm_send_modal').modal({
                show: true
            });
            $(".content-abs").html('').append('<div class="modal-header text-left bg-primary text-white"> ' +
                '<h4 class="modal-title">' +
                '<i class="fa fa-money"></i>' +
                '<span class="ml-2">Are you sure?</span>' +
                '</h4> ' +
                '<button type="button" class="closen close_ach" id="close_btn1">&times;</button> ' +
                '</div> ' +
                '<div class="row"> ' +
                '<div class="col-md-12"> ' +
                '<div class="card-header bordered"> ' +
                '<div class="card-body"> ' +
                '<p>Are you sure you want to send $<span>'+parseFloat(amount).toFixed(2)+'</span>?</p> </div> ' +
                '<div class="card-footer"><span id="close-button9"  class=" btn btn-default close_ach btn-md">Cancel</span> ' +
                '<button type="button" id="submit_form"  class="modal-padding btn btn-info btn-md pull-right submit_check hello-world">Confirm & Send </button></div></div></div> </div>');

        }
    });

    $(document).unbind("click").on('click','.submit_check',function () {
        $(this).attr("disabled", true);
        submit_ach();
    })

    $('.confirm_send_modal').on('hidden.bs.modal', function () {
        $("#submit_button").attr("disabled", false);
    });

    $(document).on('keyup','#amount', function () {
        var amount = $(this).val();
        var fee_dollar = 0;
        var fee_perc = 0;
        var balance = $('#balance').text();
        balance = balance.replace(/,/g, "");
        balance = parseFloat(balance);
        var ach_limit = parseFloat($('#achlimit').val());
        var ach_type = $('#achlimittype').val();
        var ach_done = 0;

        if ($('#achtotalcount').val().length > 0) {
            var ach_done = parseFloat($('#achtotalcount').val());
        }
        if($('#fee_dollar').val().length > 0 ){
            var fee_dollar = parseFloat($('#fee_dollar').val());
        }
        if ($('#fee_perc').val().length > 0) {
            var fee_perc = parseFloat($('#fee_perc').val());
        }
        if (amount == ""){
            $("#fee_deduct").text('');
            $('.amount_limit_error').html('');
        } else if (amount == 0) {
            $(this).val('');
        } else {
            var percentage = 0;
            percentage = (parseFloat(amount) * fee_perc)/100;
            var total_fee = fee_dollar + percentage;
            var total_amount = parseFloat(amount) + total_fee;
            $("#fee_deduct").html('').append('Amount= $ ' + parseFloat(amount).toFixed(2) + ' ');
            $("#fee_deduct").append('<span class="fee_deduct_style">Fee= $ ' + total_fee.toFixed(2) + '</span>');
            $("#fee_deduct").append('Total= $ ' + total_amount.toFixed(2) +'');
            if (parseFloat(amount)+ parseFloat(total_fee) > balance){
                $('#fee_deduct').html('');
                $('.amount_limit_error').html('').text("Insufficient Balance");
                $("#close-btn1").prop("disabled", true);
            }
            else if(ach_type == "Day"){
                total = parseFloat(amount) + ach_done;
                if(total > ach_limit ){
                    $('#fee_deduct').html('');
                    $('.amount_limit_error').html('').append('Exceeds Daily Limit of $ ' + ach_limit.toFixed(2))
                    $("#close-btn1").prop("disabled", true);
                }else{
                    $('.amount_limit_error').html('');
                    $("#close-btn1").prop("disabled", false);
                }
            }else{
                if(amount > ach_limit ){
                    $('#fee_deduct').html('');
                    $('.amount_limit_error').html('').append('Exceeds Transaction Limit of $ ' + ach_limit.toFixed(2))
                    $("#close-btn1").prop("disabled", true);
                }else{
                    $('.amount_limit_error').html('');
                    $("#close-btn1").prop("disabled", false);
                }
            }

        }
    });

    //---------------------------intant_ach/new.html.erb-----------------

    $('#cover-spin').fadeOut("slow");
    $('#test_submit').on('click',function () {
        $("#ach_req_form").click();
    });

    $('.field input, .field textarea').keyup(function () {
        var empty = false;
        $('.field input').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        $('.field textarea').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('.actions input').attr('disabled', 'disabled');
        } else {
            if ($('#myInput').val() == $('#bank_account').val() && $('#bank_account').val() != "" && $('#myInput').val() != "") {
                $('.actions input').removeAttr('disabled');
            }
        }
    });

    $('#bank_account, #myInput').on('keyup', function () {
        if ($('#myInput').val() == $('#bank_account').val() && $('#bank_account').val() != "" && $('#myInput').val() != "") {
            $('#account_p_c').hide();
            $('.ach-submit-btn').removeAttr('disabled');
        } else {
            $('#account_p_c').show();
            $('.ach-submit-btn').prop('disabled','disabled');
            $('.actions input').prop('disabled', true);
        }
        if ($('#bank_account').val() == "" && $('#myInput').val() == "") {
            $(':input[type="submit"]').prop('disabled', false);
        }
    });

    $("#bank_name, #bank_routing, #bank_account, #myInput").on('keyup', function () {
    if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
        this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
    }
});

    $("#email_address").on('keyup', function () {
    if (this.value.match(/[^a-zA-Z0-9@. ]/g)) {
        this.value = this.value.replace(/[^a-zA-Z0-9@. ]/g, '');
    }
});

    $(document).on('keyup', '#amount', function () {
    $(this).val($(this).val().replace(/[^0-9. ]/g, ''));
    })

    $(document).on("change","#location_id",function () {
        var merchant_user = $("#merchant_user").val();
        if(merchant_user == "true"){
            $('#bank_account_name').val($('#location_id').val());
        }
        $("#errormessage").css('display', 'none').html('');
        $("#amount").prop("disabled", true);
        $("#amount").val('');
        $("#fee_deduct").text('');
        $('.amount_limit_error').html('');
        $("#description").prop("disabled", true);
        $("#submit_button").prop("disabled", true);
        if($("#location_id").children(":selected").val() != 0) {
            $("#errormessage").css("display", "block").html("").html('<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close"></a> <strong>Please wait getting location information!</strong></div>');
        }
    });

    $(document).on("shown.bs.modal", "#new_check", function(){
        $(".send_check").html("New ACH");
    });

})
function confirm_ach_submit() {
    validateNewAchForm('ach-form');
    if ($("#ach-form").valid()) {
        if ($("#bank_routing").val() !== "" && $("#bank_account").val() !== "" && $("#bank_name").val() !== "" && $("#email_address").val() !== "" && $("#description").val() !== "" && $("#location_id").val() !== "" && $("#bank_account_type").val() !== "") {
            var name = $('#bank_name').val();
            var amount = $('#amount').val();
            amount = parseFloat(amount).toFixed(2);
            $("#ach-confirm-amount").text("");
            $("#ach-receiver-name").text("");
            $("#ach-confirm-amount").text(amount);
            $("#ach-receiver-name").text(name);
            $('.confirmationDialog').modal({ show: true });
        }
    }
}

function submit_ach() {

    if ($("#ach-form").valid()) {
        if ($("#bank_routing").val() !== "" && $("#bank_account").val() !== "" && $("#bank_name").val() !== "" && $("#email_address").val() !== "" && $("#description").val() !== "" && $("#location_id").val() !== "" && $("#bank_account_type").val() !== "") {
            $(".hello-world").prop('disabled',true);
            // document.getElementById("btn_confirm").disabled = true;
//            $("#ach-form").submit();
            $("#ach-form").closest("form")[0].submit();
            $('#cover-spin').show(0);
            // $('.sub_btn').prop('disabled',true);
        }
    }
}

function validateACHForm(role) {
    var formRules = {
        rules: {
            "amount": {
                required: true,
            },
            "description": {
                required: true,
            },
        },
        messages: {
            "amount": "Please enter Amount",
            "description": "Please enter Description",
        },
    };
    $.extend(formRules, config.validations);
    $('#'+ role).validate(formRules);
}

function myFunction() {
    $('.confirm_send_modal').modal('toggle')
     $('.confirm_send_modal').remove();
}