
$(document).ready(function () {
    //----------------show_bulk.html.erb--------------
    $(".resendModalDialog .resend-button12").prop('disabled',true);
    $(".detail_check").prop('disabled',false);
    $(".view_check_details").prop('disabled',false);

    //-------------------------bulk_checks.html.erb----------


    //------------------bulk_checks.html.erb

    $("#send_bulk").click(function () {
        $("#message-process").empty();
        $('#message-process').append("Processing");
        $('#message-process').addClass("color-orange");
        $("#processing-1243").addClass("color-orange");
        $("#total-1243").addClass("color-orange");
        $(".edit-1243").text("");
        $(".remove-1243").text("");

        var wallets_id = $("#from_wallet").val();
        var inst_id = $("#instant_id").val();
        document.getElementById("send_bulk").disabled = true;
        $.ajax({
            url: "/merchant/checks/create_bulk_checks",
            method: 'get',
            data: {bulk_instance_id: inst_id, wallet_id: wallets_id}
    });
    });

    $("#send_bulk").click(function () {
        var x = setInterval(function(){get_fb();}, 20000);
        function get_fb() {
            var processed = $("#all_processed").text();
            var total = $("#total-1243").text();
            if(parseInt(processed) == parseInt(total)){
                clearInterval(x);
            }
            else{
                $.ajax({
                    url: "/merchant/checks/load_updated_checks",
                    method: 'get'
                });
            }
        }
    });

    //----------------end-------------------------------

    $("#from_wallet").on("change", function () {
        balance = $(this).children(":selected").data().balance;
        fee = $(this).children(":selected").data().fee;
        total = $(this).children(":selected").data().total;
        if($("#from_wallet").val() == ""){
            $("#balance").text("");
            $("#feex").text("");
            $("#t_amount").text("");
        }else{
            $("#balance").text(balance);
            $("#feex").text(fee);
            $("#t_amount").text(total);
        }

        var wallet_bal = parseFloat($("#balance").text().replace(/,/g, ""));
        var check_bal = parseFloat($("#t_amount").text().replace(/,/g, ""));

        if(wallet_bal < check_bal){
            document.getElementById("send_bulk").disabled = true;
        }else if($('#from_wallet').val() == ""){
            document.getElementById("send_bulk").disabled = true;
        }else{
            document.getElementById("send_bulk").disabled = false;
        }

    });

        var wallet_bal = parseFloat($("#balance").text().replace(/,/g, ""));
        var check_bal = parseFloat($("#t_amount").text().replace(/,/g, ""));
        if(wallet_bal < check_bal){
            document.getElementById("send_bulk").disabled = true;
        }else if($('#from_wallet').val() == ""){
            document.getElementById("send_bulk").disabled = true;
            $("#feex").text("");
            $("#t_amount").text("");
            $("#balance").text("");
        }else{
            document.getElementById("send_bulk").disabled = false;
        }

//----------------_show_check_modal.html.erb---------------------//


    wallet_id = $("#from_wallet").children(":selected").val();
    $("#wallet_id").val(wallet_id);
})

$("#form_1558").on('submit',function () {
    var che_id = $("#check_id_single").val();
    $("#check-"+che_id).text("");
    $("#check-"+che_id).append("<b style='color: orange;'>Updating</b>");
    $("#updateModalDialog").modal('toggle');
});

$('.routing_number_field').on('keyup',function () { // for routing number validation
    var len =  $('.routing_number_field').val().length;
    if(len == 9){
        if(($("#confirm_account_number_error").html() == "") && ($("#account_number_error").html() == "")){
            $('#routing_number_error').text('');
            $('.update_form_button').prop('disabled', false);
        }else{
            $("#routing_number_error").text("");
        }
    }else{
        $('.update_form_button').prop('disabled', true);
        $("#routing_number_error").text("").text("Routing number must be 9 digits");
    }
});
$('.account_number_field').on('keyup',function () { // for account number validation
    var len =  $('.account_number_field').val().length;
    if(len >= 9 && len <= 13){
        if(($("#confirm_account_number_error").html() == "") && ($("#routing_number_error").html() == "")){
            $('#account_number_error').text('');
            $('.update_form_button').prop('disabled', false);
        }else{
            $("#account_number_error").text("");
        }
    }else{
        $('.update_form_button').prop('disabled', true);
        $("#account_number_error").text("").text("Account number must be 9-13 digits");
    }
});
$('.confirm_account_number_field').keyup(function() { // for confirm account number
    var pass = $('.account_number_field').val();
    var repass = $('.confirm_account_number_field').val();
    if (pass != repass) {
        $('#confirm_account_number_error').text('').text('Account number must be same');
        $('.update_form_button').prop('disabled', true);
    }
    else {
        if(($("#routing_number_error").html() == "") && ($("#account_number_error").html() == "")){
            $('#confirm_account_number_error').text('');
            $('.update_form_button').prop('disabled', false);
        }else{
            $('#confirm_account_number_error').text('');
        }
    }


});