

$("#query_time1").on('change',function () {
    var time1 = $("#query_time1").val();
    $('#query_time2 > option').each(function () {
        if(parseInt($(this).val()) <= parseInt(time1) ){
            $(this).attr('disabled','disabled');
        }
    });
});
$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000)

    $('.pagination').find('li').find('a').click(function () {
        $('.spnr').fadeIn();
    });
    $('.refund_btn').click(function () {
        $('.spnr').fadeIn();
    });
    $(".details_modal").click(function () {
        $(".spnr").fadeIn();
    });
    $('.search_button').attr('disabled', false);

    $('#sender').bind('keyup change', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
        if($(this).val().length == 0){
//                $('.search_button').attr('disabled', true);
        }else{
            $('.search_button').attr('disabled', false);
        }
    });
    $('#receiver').bind('keyup change', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
        if($(this).val().length == 0){
//                $('.search_button').attr('disabled', true);
        }else{
            $('.search_button').attr('disabled', false);
        }
    });
    $('#phone').bind('keyup change', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
        if($(this).val().length == 0){
//                $('.search_button').attr('disabled', true);
        }else{
            $('.search_button').attr('disabled', false);
        }
    });
    $('#Price').bind('keyup change', function(){
        if($(this).val().length == 0){
//                $('.search_button').attr('disabled', true);
        }else{
            $('.search_button').attr('disabled', false);
        }
    });
    $('#id').bind('keyup change', function(){
        if($(this).val().length == 0){
        }else{
            $('.search_button').attr('disabled', false);
        }
    });
    $('.search_transactions').submit(function () {
        $('.spnr').fadeIn();
    });


$('#detail_transaction_modal').click(function () {
    $('.spnr').fadeIn();
});
$('.next_transactions').click(function () {
    $('.spnr').fadeIn();
});
$(function() {
    $('#query_type_').multipleSelect({
        placeholder: "Select Type",
        selectAll: true,
    });
    $('#query_type1_').multipleSelect({
        placeholder: "Select Type",
        selectAll: true,
    });
    $('#query_wallet_ids_').multipleSelect({
        placeholder: "Select Merchant Name",
        selectAll: false
    });
    $('#query_DBA_name_').multipleSelect({
        placeholder: "Select DBA Name",
        selectAll: false
    });
});

    $("[type='checkbox']").css("margin-top","-1px").css("margin-right","15px");




$("#export_class").on('click', function () {
    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $("#exportDate").daterangepicker({
        "maxSpan": {
            "days":90
        },
        "startDate": nd,
        "maxDate": nd
    });
})
    //.............................local_iso_agent_view.html.erb.................//
    $( "#transaction tr" ).click(function(){
        // event.stopPropagation();
        var link  = $(this).data("href");
        setTimeout(function() {
            $('#cover-spin').fadeOut("slow");
            // alert("Something went wrong with this detail. Please try again later!")
        }, 15000 );
        $.ajax({
            url: link,
            type: "get"
        });
    });
    var lt= $('.timeUtctransactions').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });


    $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
    });
    $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
        $('input[name="query[date]"]').val('');
    });
    $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $("#trans_date").daterangepicker({
        autoUpdateInput: false,
        "maxSpan": {
            "days": 30
        },
    });

//........................export_modal.html.erb.................//

    $('#download_link').on("click", function () {
        $(this).addClass('disabled');
    });

    const urlParams = new URLSearchParams(window.location.search);
    const queryParam = urlParams.get('query');
    const date = urlParams.get('date');
    if(queryParam!=="") {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        // if (queryParam == "") {
        //     $('input[name="query[date]"]').val('');
        // } else {
        //     $('input[name="query[date]"]').val('');
        // }
        if (queryParam == "") {
            $("#exportDate").daterangepicker({
                "maxSpan": {
                    "days": 90
                },
                "startDate": nd,
                "maxDate": nd
            });
        }
    }
    if(queryParam!=="" && date !== "" ){
        $('#date-value').val('');
    }

    $("#exportDate").on("change",function () {
        if($("#exportDate").val().length > 0){
            $("#dummy_export_btn").prop("disabled", false);
        } else{
            $("#dummy_export_btn").prop("disabled", true);
        }
    });
    $('.detail_link').on('click', function () {
        var transaction_id = $('#transaction_id_input').val();
        $('#cover-spin').show(0)
    })
    $(document).on('click','.report_count',function () {
        var file_id = $('#file_id_input').val()
        reports_count_rel('new_'+file_id)
    })
    $(document).on('change','#tx_filter',function () {
        $('#iso_agent_form').submit();
    })
    //............................previous/next transaction.html.erb................//
    $('.next_transactions').click(function () {
        $('.spnr').fadeIn();
    });
    $('.previous_transactions').click(function () {
        $('.spnr').fadeIn();
    });

    //......................................index.html.erb.../////////////
    /*

  var select_reason;
  var reason_detail;
  var transaction_detail;
  var tran_Id;
  var parentId;
  var wallet_Id;
  var refund_fee;
  $('#partialamount').text('0');
  var check1 = false, check2 = false;

  $(document).on("click", ".refundModalDialog", function () {
    $('#reason_select option[value="0"]').prop("selected",true);
    $('#reason_detail').val('');
    $('.modal-footer a button').prop('disabled',true);
    transaction_detail = JSON.stringify($(this).data('transaction'));

    var tran_id = JSON.parse(transaction_detail).id;
    var parent_id = JSON.parse(transaction_detail).parent_id;
    var wallet_id = JSON.parse(transaction_detail).destination;
    parentId = parent_id;
    tran_Id = tran_id;
    wallet_Id = wallet_id;

    $.ajax({
      url: '<%= get_merchant_amount_merchant_transactions_path %>',
      type: 'GET',
      data: {trans_id: tran_Id,parent_id: parentId,wallet_id: wallet_Id},
      success: function(res){
        refund_fee = res.fee;
        $('#partialamount').val(res.partial_amount);
      }
    });

    $.ajax({
      url: '<%= refund_check_merchant_transactions_path %>',
      type: 'GET',
      data: {trans_id: tran_id}
    });
  });

  $('#partialamount').change(function(){
    var amount=$(this).val();
    if(amount<=0){
      $('.modal-footer a button').prop('disabled',true);
      return false;
    }
    select_reason = $('#reason_select').val();
    var partial_amount = $('#partialamount').val();

    if (partial_amount != "0" && select_reason == "0") {
      $.ajax({
        url: '<%= get_merchant_amount_merchant_transactions_path %>',
        type: 'GET',
        data: {trans_id: tran_Id,parent_id: parentId,wallet_id: wallet_Id}
      }).done(function (res) {
        refund_fee = res.fee;
        var partial= res.partial_amount - parseFloat(partial_amount);
        if (partial < 0) {
          $('#partial_error').text("Amount is greater the amount to refund");
          $('.modal-footer a button').prop('disabled',true);
          check1 = false;
        }
        else {
          $('#partial_error').text("");
          check1  =true;
        }
        if (check1 && check2) {
          $('.modal-footer a button').prop('disabled',false);
        }
      });
    }
    updateRefundPostLink();
  });

  function updateRefundPostLink() {
    var select_reason=$('#reason_select').val();
    var partial_amount=$('#partialamount').val();
    reason_detail=$("#reason_select option:selected").text();
    if (select_reason==0) {
      $('#reason_detail').val('');
      $('#reason_detail').hide();
      $('.modal-footer a button').prop('disabled',true);
      check2 = false;
    } else if(select_reason!=4 && select_reason!=0) {
      $('#reason_detail').val('');
      $('#reason_detail').hide();
      $('#footerLink').attr('href',"<%= refund_merchant_transaction_merchant_transactions_path %>"+"?js=js&reason="+select_reason+"&partial_amount="+partial_amount+"&refund="+refund_fee+"&reason_detail="+reason_detail+"&transaction="+transaction_detail);
      $('.modal-footer a button').prop('disabled',false);
      check2 = true;
    } else {
      $('#reason_detail').show();
      $('.modal-footer a button').prop('disabled',true);
      check2 = false
    }
    if (check1 && check2) {
      $('.modal-footer a button').prop('disabled',false);
    }
  }

  $('#reason_select').change(function(){
    updateRefundPostLink();
  });

  $('#reason_detail').keyup(function () {
    reason_detail=$(this).val();
    var select_reason=$('#reason_select').val();
    var partial_amount=$('#partialamount').val();
    if (reason_detail=='' || reason_detail==null) {
      $('#refundModalFooter a button').prop('disabled',true);
    } else {
      $('#footerLink').attr('href',"<%= refund_merchant_transaction_merchant_transactions_path %>"+"?js=js&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+reason_detail+"&transaction="+transaction_detail);
      $('#refundModalFooter a button').prop('disabled',false);
    }
  });

  $(document).ready(function () {
    $('.pagination').find('li').find('a').click(function () {
      $('.spnr').fadeIn();
    });

    $('.refund_btn').click(function () {
      $('.spnr').fadeIn();
    });

    $(".details_modal").click(function () {
      $(".spnr").fadeIn();
    });

    $('.search_button').attr('disabled', true);

    $('#sender').bind('keyup change', function(){
      this.value = this.value.replace(/[^0-9\.]/g,'');
      if ($(this).val().length == 0) {
        $('.search_button').attr('disabled', true);
      } else {
        $('.search_button').attr('disabled', false);
      }
    });

    $('#receiver').bind('keyup change', function(){
      this.value = this.value.replace(/[^0-9\.]/g,'');
      if ($(this).val().length == 0) {
        $('.search_button').attr('disabled', true);
      } else {
        $('.search_button').attr('disabled', false);
      }
    });

    $('#phone').bind('keyup change', function(){
      this.value = this.value.replace(/[^0-9\.]/g,'');
      if ($(this).val().length == 0) {
        $('.search_button').attr('disabled', true);
      } else {
        $('.search_button').attr('disabled', false);
      }
    });

    $('#Price').bind('keyup change', function(){
      if ($(this).val().length == 0) {
        $('.search_button').attr('disabled', true);
      } else {
        $('.search_button').attr('disabled', false);
      }
    });

    $('#id').bind('keyup change', function(){
      if ($(this).val().length == 0) {
        $('.search_button').attr('disabled', true);
      } else {
        $('.search_button').attr('disabled', false);
      }
    });

    $('.search_transactions').submit(function () {
      $('.spnr').fadeIn();
    });

    $('.datepicker').daterangepicker({format: 'mm-dd-yyyy'});

    var lt= $('.timeUtc').each(function (i,v) {
      var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
      var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
      v.innerText = local
    });
  });

  $(function() {
    $('.datepicker').val('');
    $('.datepicker').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  });
  $("#export_class").on('click', function () {
      var nowDate = new Date();
      var newdate = (nowDate.setDate(nowDate.getDate()));
      var nd = new Date(newdate);
      $("#exportDate").daterangepicker({
          "maxSpan": {
              "days":31
          },
          "startDate": nd,
          "maxDate": nd
      });
  })*/
    //.........................refund.html.erb........................//
    /*
    $(document).ready(function () {
        $('.pagination').find('li').find('a').click(function () {
            $('.spnr').fadeIn();
        });
        $('.search_button').attr('disabled', true);
        $('#start_date').bind('keyup change', function(){
            if($(this).val().length == 0){
                $('.search_button').attr('disabled', true);
            }else{
                $('.search_button').attr('disabled', false);
            }
        });
        $('#end_date').bind('keyup change', function(){
            if($(this).val().length == 0){
                $('.search_button').attr('disabled', true);
            }else{
                $('.search_button').attr('disabled', false);
            }
        });
        $('.search_transactions').submit(function () {
            $('.spnr').fadeIn();
        });
    });
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
        var lt= $('.timeUtc').each(function (i,v) {
            var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
            var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
            v.innerText = local
        });
    });
    $(document).on("click", ".refundModalDialog", function () {
        $(".modal-body #date").text($(this).data('date'));
        $(".modal-body #type").text($(this).data('type'));
        $(".modal-body #total_amount").text($(this).data('total_amount'));
        $(".modal-body #bank_amount").text($(this).data('bank_amount'));
        $(".modal-body #wallet_amount").text($(this).data('wallet_amount'));
        $(".modal-body #sender").text($(this).data('sender'));
        $(".modal-body #receiver").text($(this).data('receiver'));
        $(".modal-body #source").text($(this).data('source'));
        $(".modal-body #reason").text($(this).data('reason'));

        var lt= $('.modalTime').each(function (i,v) {
            var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
            var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
            v.innerText = local
        });
    });
    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":31
            },
            "startDate": nd,
            "maxDate": nd
        });
    })*/
    $(".amount_field").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    validatePasswordForm("password_form");

    $('#password_submit').attr('disabled',true);
    $( document ).ready(function() {
        $('.footer').hide();
        $('.navbar-static-top').hide();
        $('#password_submit').attr('disabled',true);
    });

    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            $('#strength_human').hide();
            $('#password_submit').attr('disabled',false);
        } else{
        $('#password_submit').attr('disabled',true);
    }
    });



    $("#user_password").on("keyup", function() {
        $('#strength_human').show();
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input style="margin-right: 10px;" id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input style="margin-right: 10px;" id="one_number" name="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input style="margin-right: 10px;" id="upper_case" name="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input style="margin-right: 10px;" id="lower_case" name="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else{
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });
    // $('#submit_edit_button1').on("click",function(event) {
    //     var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}/;
    //     if (!re.test($('#user_password').val())){
    //         $('#password_Error').html('');
    //         $('#user_password').after('<div id= "password_Error" style="color: red"> password validation requires capital letter, small letter and a number </div>')
    //         return false;
    //     }
    // });

});
