// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require js/jquery-2.1.4.min
//= require jquery_ujs
//=# require rails-ujs
//= require jquery
//= require jquery-ui
//= require merchant/jstz.min
//= require turbolinks
//= require export_files
//= require Chart.bundle
//= require vendor
//= require app
//= require devise.js
//= require libphonenumber/utils
// require admins/companies
// require bootstrap
// require merchants_js
// require side_panel/js/adminlte.min


$(document).ready(function () {
    var tz = jstz.determine();
    document.cookie = "timezone=" + tz.name();
});

