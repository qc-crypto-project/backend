
$(document).ready(function () {
    var table = $('#datatable-keytable-transactions').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": false,
        "ordering": true,
        "searching": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "columnDefs": [
            { "orderable": false, "targets": 5 },
        ],
        "order": [[ 2, "desc" ]]
    });

    var table = $('#datatable-keytable-bank-verification').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": false,
        "ordering": true,
        "searching": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "order": [[ 0, "desc" ]]
    });

    //............................verifications.html.erb..............//
    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    window.onload = $('.utc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    $(document).on('change','#filter',function () {
        $('#verification_filter').submit();
    })
    //error message and export extra

    $('.dt-buttons').css('display','none')
    //error message show remove
    $('#alert-message-top').fadeOut(5000);
    //...........................admin_transactions_table.html.erb......................//
    $("#query_time1").on('change',function () {
        var time1 = $("#query_time1").val();
        $('#query_time2 > option').each(function () {
            if(parseInt($(this).val()) <= parseInt(time1) ){
                $(this).attr('disabled','disabled');
            }
        });
    });
    var select_reason,reason_detail,transaction_detail,tran_Id,destination,source,refund_fee,total_amount,partial_amt;
    var check1 = false, check2 = false;
    $('.next_transactions').click(function () {
        $('#cover-spin').fadeIn();
    });

    $(document).on("click", ".refundFailedModalDialog", function () {
        $('#partialfailedamount').val('');
        $('#reason_detail').val('');
        $('#reason_detail').hide();
        transaction_detail=JSON.stringify($(this).data('transaction'));
        parsed_transction=JSON.parse(transaction_detail);
        tran_Id = parsed_transction.id;
        total_amount = parsed_transction.amount;
        partial_amt = total_amount;
        check1 = true
        $('input#partialfailedamount').val(parseFloat(parsed_transction.amount).toFixed(2));
        $('#confirm_refund_failed_amount').text($('input#partialfailedamount').val());
        updateFailedRefundLink()
    });

    $("#partialfailedamount").on("keyup",function(){
        select_reason=$('#reason_failed_select').val();
        var partial_amount = $('#partialfailedamount').val();
        partial_amt=partial_amount;
        $('#confirm_refund_failed_amount').text(parseFloat($('input#partialfailedamount').val()).toFixed(2));
        if (parseFloat(total_amount)>=parseFloat(partial_amt)){
            $('.modal-footer a button').prop('disabled',false);
            check1 = true
        }else{
            check1 = false
        }
        updateFailedRefundLink()
    })

    function updateFailedRefundLink(){

        var select_reason=$('#reason_failed_select').val();
        var partial_amount=$('#partialfailedamount').val();
        reason_detail=$("#reason_failed_select option:selected").text();
        if (check1 && check2) {
            if (parseFloat(total_amount)>=parseFloat(partial_amt)){
                $('.modal-footer a button').prop('disabled',false);
                $('#footerFailedConfirmLink').attr('href',"/admins/refund_failed_transaction"+"?partial_amount=" + partial_amount + "&reason_detail=" + encodeURIComponent(reason_detail) + "&transaction=" + tran_Id);
            }
        }else{
            $('.modal-footer a button').prop('disabled',true);
        }
    }

    $('#reason_failed_detail').keyup(function () {
        reason_detail=$(this).val();
        var select_reason=$('#reason_failed_select').val();
        var partial_amount=$('#partialfailedamount').val();
        if (reason_detail=='' || reason_detail == null){
            $('.modal-footer a button').prop('disabled', true);
            check2 = false;
        } else{
            $('#footerFailedConfirmLink').attr('href',"/admins/refund_failed_transaction"+"?partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + tran_Id);
            if(parseFloat(total_amount) >= parseFloat(partial_amount)){
                $('#refundModalFooter a button').prop('disabled',false);
                check2 = true;
            }
        }
    });

    $(document).on("click", ".refundModalDialog", function () {
        $('#reason_select option[value="0"]').prop("selected",true);
        $('#partialamount').val('');
        $('#reason_detail').val('');
        $('#reason_detail').hide();
        $('.refund_fee').text('0');
        $('.modal-footer a button').prop('disabled',true);
        transaction_detail=JSON.stringify($(this).data('transaction'));
        parsed_transction=JSON.parse(transaction_detail);
        tran_Id = parsed_transction.id;
        parent_id=parsed_transction.parent_id;
        destination=parsed_transction.receiver_wallet_id;
        source=parsed_transction.sender_wallet_id;
        $.ajax({
            url: '/admins/refund_check',
            type: 'GET',
            data: {trans_id: tran_Id},
        })
            .done(function () {
                $.ajax({
                    url: '/admins/get_partialAmount',
                    type: 'GET',
                    data: {trans_id: tran_Id,parent_id: parent_id,destination: destination,source: source},
                })
                    .done(function (res) {
                        if(res.partial_amount==""){
                            $('#partial_error').text("No Transaction Found");
                            $('#partial_error').fadeIn();
                            setTimeout(function(){
                                $('#partial_error').fadeOut()
                            }, 2000);
                        }else{
                            $('#partialamount').val(parseFloat(res.partial_amount).toFixed(2));
                            total_amount=parseFloat(res.partial_amount);
                            partial_amt=total_amount;
                            last4="";
                            if(parsed_transction["tags"]["card"]){
                                if(parsed_transction["tags"]["card"]["last4"]){
                                    last4=parsed_transction["tags"]["card"]["last4"];
                                }
                            }else{
                                if(parsed_transction["tags"]["previous_issue"]){
                                    if(parsed_transction["tags"]["previous_issue"]["last4"]){
                                        last4=parsed_transction["tags"]["previous_issue"]["last4"];
                                    }
                                }
                            }
                            $('#refund_user').text(last4);
                            refund_fee = res.fee;
                            var fee = refund_fee + parseFloat(res.partial_amount);
                            $('.refund_fee').text(parseFloat(fee).toFixed(2));
                            $('#confirm_refund_amount').text(parseFloat(res.partial_amount).toFixed(2));
                        }
                    });
            });

    });
    $(document).on('change','#partialamount', function(){
        select_reason=$('#reason_select').val();
        var partial_amount = $('#partialamount').val();
        partial_amt=partial_amount;
        if (partial_amount != "0") {
            $.ajax({
                url: '/admins/get_partialAmount',
                type: 'GET',
                data: {trans_id: tran_Id,destination: destination,source: source,amount:partial_amount},
            })
                .done(function (res) {
                    var partial= res.partial_amount - parseFloat(partial_amount);
                    if (partial < 0){
                        $('#partial_error').text("Amount is greater than amount to refund");
                        $('#partial_error').fadeIn();
                        setTimeout(function(){
                            $('#partial_error').fadeOut()
                        }, 2000);

                        $('.modal-footer a button').prop('disabled',true);
                        check1 = false;
                    }
                    else {
                        $('#partial_error').text("");
                        check1  =true;
                        refund_fee=res.fee;
                        var fee = refund_fee+parseFloat(partial_amount);
                        $('.refund_fee').text(parseFloat(fee).toFixed(2));
                        $('#confirm_refund_amount').text(parseFloat(partial_amount).toFixed(2));
                    }
                    if (check1 && check2) {
                        if(select_reason!=0){
                            $('.modal-footer a button').prop('disabled',false);
                        }
                    }
                });
        }
        updateRefundPostLink();
    });

    function updateRefundPostLink() {
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        reason_detail=$("#reason_select option:selected").text();
        if (select_reason==0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false;
        } else if (select_reason!=4 && select_reason!=0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('#footerConfirmLink').attr('href',"/admins/refund_transaction"+"?js=js&refund_fee="+refund_fee+"&reason=" + select_reason + "&partial_amount=" + partial_amount + "&reason_detail=" + encodeURIComponent(reason_detail) + "&transaction=" + tran_Id);
            if(total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
            check2 = true;
        } else{
            $('#reason_detail').show();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false
        }
        if (check1 && check2) {
            if (total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
        }
    }

    $('#reason_select').change(function(){
        updateRefundPostLink();
    });

    $('#reason_detail').keyup(function () {
        reason_detail=$(this).val();
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        if (reason_detail=='' || reason_detail == null){
            $('.modal-footer a button').prop('disabled', true);
        } else{
            $('#footerConfirmLink').attr('href',"/admins/refund_transaction"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + tran_Id);
//            $('#footerConfirmLink').attr('href',"<%#= refund_transaction_admins_path %>"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + encodeURIComponent(transaction_detail));
            if(total_amount >= partial_amount){
                $('#refundModalFooter a button').prop('disabled',false);
            }
        }
    });

    $(function() {

        $('#query_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false
        });
        $('#query_gateway_').multipleSelect({
            placeholder: "Select Gateway",
            selectAll: false
        });
        $('#query_risk_eval_').multipleSelect({
            placeholder: "Select Status",
            selectAll: false
        });

        $( function() {
            $( document ).tooltip();
        } );
        /*$(document).ready(function () {
            $(function() {
            <% if params[:query].present? %>
            <% if params[:query][:date].blank? %>
                $('input[name="query[date]"]').val('');
            <% end %>
                <% else %>
                $('input[name="query[date]"]').val('');
            <% end %>*/
                $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });
                $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
                    var startDate = picker.startDate;
                    var endDate = picker.endDate;
                    $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
                });
            });
            var nowDate = new Date();
            var newdate = (nowDate.setDate(nowDate.getDate()));
            var nd = new Date(newdate);
            $('input[name="query[date]"]').daterangepicker({
               autoUpdateInput: false,
                locale: {
                    format: 'MM/DD/YYYY',
                },
                "maxSpan": {
                    "days": 90
                },
                "maxDate": nd,
                "parentEl": $("#date_parent")
            });

            $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
            $("#exportDate").daterangepicker({
                "maxSpan": {
                    "days": 90
                },
                "maxDate": nd,
            });



        $("[type='checkbox']").css("margin-top","-1px").css("margin-right","15px");



        $('.number').keypress(function(event) {
            var $this = $(this);
            if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                ((event.which < 48 || event.which > 57) &&
                    (event.which != 0 && event.which != 8))) {
                event.preventDefault();
            }

            var text = $(this).val();
            if ((event.which == 46) && (text.indexOf('.') == -1)) {
                setTimeout(function() {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                        $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                }, 1);
            }

            if ((text.indexOf('.') != -1) &&
                (text.substring(text.indexOf('.')).length > 2) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 2)) {
                event.preventDefault();
            }
        });

        $('.number').bind("paste", function(e) {
            var text = e.originalEvent.clipboardData.getData('Text');
            if ($.isNumeric(text)) {
                if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                    e.preventDefault();
                    $(this).val(text.substring(0, text.indexOf('.') + 3));
                }
            }
            else {
                e.preventDefault();
            }
        });
    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    })
    $("#amount").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
//..............................verfication tabe
    var globalTimeout = null;
    $(document).on('change keyup','#srch-term',function () {
        search_value = this.value.split('.')[0];
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        if(value_present(search_value)){
            time_out = 2000;
        }else{
            time_out = 0
        }
        globalTimeout = setTimeout(function() {
            // search_value = this.value;
            globalTimeout = null;
        $('#q').val(search_value);
        var filter_id = $('#filter_input').val();
         var req_val = $('#params_req_input').val();
         var req_other = $('#req_others_input').val();
        $.ajax({
            url: '/admins/verifications',
            type: 'GET',
            data: {q: search_value,filter: filter_id,req: req_val, others: req_other, search_input:"search_input"},
        });
        }, time_out);
    });
//.................................verification_table.html.erb......................//
    $(".padd").css("margin","-15px 20px 20px 0px");
        //.......................................admins_transactions_table.html.erb................/

    // $( "#datatable-keytable tr" ).click(function(){
    //     var link  = $(this).data("href");
    //     setTimeout(function() {
    //         $('#cover-spin').fadeOut("slow");
    //     }, 15000 );
    //     $.ajax({
    //         url: link,
    //         type: "post"
    //     });
    // });
    // $( "#datatable-keytable1 tr" ).click(function(){
    //     var link  = $(this).data("href");
    //     setTimeout(function() {
    //         $('#cover-spin').fadeOut("slow");
    //     }, 15000 );
    //     $.ajax({
    //         url: link,
    //         type: "post"
    //     });
    // });

    // admins/shared/admins_transactions

    $(document).on('click', "#successful_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "success")
    });
    $(document).on('click', "#decline_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "decline")
    });
    $(document).on('click', "#refund_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "refund")
    });
    $(document).on('click', "#checks_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "checks")
    });
    $(document).on('click', "#p2c_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "push_to_card")
    });
    $(document).on('click', "#ach_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "ach")
    });
    $(document).on('click', "#b2b_txn", function () {
        var url = $(this).data("url");
        show_transaction(url, "b2b")
    });



    // admins/shared/_admin_transaction_table

    $(document).on("click", "#datatable-keytable1 tr", function(){
        $('#cover-spin').show(0);
    });
    $(document).on("click", "#datatable-keytable1 tr", function(){
       var link  = $(this).data("href");
        setTimeout(function() {
            $('#cover-spin').fadeOut("slow");
        }, 15000 );
       $.ajax({
                url: link,
                type: "post",
        });
    });
    // admins/shared/decline_transaction
    var table = $('#datatable-keytable-decline').DataTable({
        responsive:false,
        "sScrollX": true,
        "searching": false,
        "bfilter": true,
        "bLengthChange": false,
        "bPaginate": false,
        "entries":   false,
        "bInfo" : false,
        "columnDefs": [
            { "orderable": true,
                "render": function ( data, type, row ) {
                    var gmtDateTime = moment.utc(data,'YYYY-MM-DD HH:mm:ss');
                    var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
                    return local;
                },
                "targets": 1 },
        ],
        "order": [[ 0, "desc" ]]
    });
    $(document).on("click", "#datatable-keytable-decline tr", function() {
        var link  = $(this).data("href");
        setTimeout(function() {
            $('#cover-spin').fadeOut();
        }, 10000 );
        $.ajax({
            url: link,
            type: "get"
        });
    });

    $(document).on("change", "#tx_filter", function () {
        $("#tx_form").submit();
    })
    $(document).on("change", "#tx_filter1", function () {
        $("#user_dispute").submit();
    })
    $(document).on("click", "#txn-tr", function () {
        $('#cover-spin').show(0);
    })
});

function show_transaction(url,trans) {
    window.location.href =
        url + "?trans="+ trans +"&offset=" + encodeURIComponent(moment().local().format('Z'))
}



