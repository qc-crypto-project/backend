/**
 * Global admin functions
 */
$(document).ready(function() {

    /**
     * Enable tooltips
     */
    if ($('.tooltips').length) {
        $('.tooltips').tooltip();
    }


    /**
     * Activate any date pickers
     */
    if ($(".input-group.date").length) {
        $(".input-group.date").datepicker({
            autoclose      : true,
            todayHighlight : true
        });
    }


    /**
     * Detect items per page change on all list pages and send users back to page 1 of the list
     */

    /**
     * Enable Summernote WYSIWYG editor on any textareas with the 'editor' class
     */
    if ($('textarea.editor').length) {
        $('textarea.editor').each(function() {
            var id = $(this).attr('id');
            $('#' + id).summernote({
                height: 300
            });
        });
    }

});
