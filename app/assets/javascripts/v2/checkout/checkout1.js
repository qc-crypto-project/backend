validateBillingForm("billing_form");

$('.data-wizard-next-button').click(function () {
    if($("#billing_form").valid()){
        $("#submit_b2").removeClass('pointer--none');
        $("#rtp_step2").val("true").trigger("change");
        var name = $("#address_name").val();
        var email = $("#address_email").val();
        var code = $('#phone_code').val()
        var number = $('#phone_number').val()
        var phone_number = code + number
        var street = $("#address_street").val();
        var suite = $("#address_suit").val();
        var city = $("#address_city").val();
        var zip = $("#address_zip").val();
        var state = $("#billing_state").val();
        var country = $("#b_country").val();
        var billing_id = $("#billing_id").val();
        billing_address = {id: billing_id, name: name,email: email,phone_number: phone_number,street: street,suit: suite,city: city,zip: zip,state: state,country: country};
        $("#billing_params").val(JSON.stringify(billing_address));

        $("#step1_popover").popover('show');
        $("#welcome_popover").popover('hide');
        $('.m_wizard_form_step_1').removeClass('m-wizard__step--current');
        $('.m_wizard_formm_step_1').removeClass('m-wizard__form-step--current');
        $('.m_wizard_form_step_1').addClass('m-wizard__step--done');
        $('.m_wizard_form_step_2').removeClass('m-wizard__step--done');
        $('.m_wizard_form_step_2').addClass('m-wizard__step--current');
        $('.m_wizard_formm_step_2').addClass('m-wizard__form-step--current');
        $('.m_wizard_form_step_3').removeClass('m-wizard__step--current');
        $('.m_wizard_formm_step_3').removeClass('m-wizard__form-step--current');
        $('.m_wizard_formm_step_3').removeClass('m-wizard__step--done');
        $('#wizard-progress-bar').removeClass('width-0-percent');
        $('#wizard-progress-bar').addClass('width-49-percent');
        $('.m_wizard_form_step_1 .m-wizard__step-number .wizard_dot').addClass('hide');
        $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('show');
        $('.m_wizard_form_step_1 .m-wizard__step-number > span > i').addClass('color-fff');
        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
        $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('show');
        $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').removeClass('hide');
        var phone_code = $("#phone_number").intlTelInput("getSelectedCountryData").dialCode;
        $('#phone_code').val(phone_code);
    }
});

validateShippingForm("shipping_form");

$('.data-shipping-wizard-next-button').click(function () {
    if($("#shipping_form").valid()) {
        $("#submit_b").removeClass('pointer--none');
        var street = $("#shipping_street").val();
        var suite = $("#shipping_suit").val();
        var city = $("#shipping_city").val();
        var zip = $("#shipping_zip").val();
        var state = $("#shipping_state").val();
        var country = $("#s_country").val();
        shipping_address = {street: street,suit: suite,city: city,zip: zip,state: state,country: country};
        $("#shipping_params").val(JSON.stringify(shipping_address));

        $("#step1_popover").popover('hide');
        $('.ready-message').hide();
        $('.main-content').show();
        $("#step2_popover").popover('show');
        $('#wizard-progress-bar').addClass('width-100-percent');
        $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
        $('.m_wizard_formm_step_3').addClass('m-wizard__form-step--current');
        $('.m_wizard_formm_step_2').removeClass('m-wizard__form-step--current');
        $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
        $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
        $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
        $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
        $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
        $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').addClass('show');
        $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').removeClass('hide');
    }

});

$('.m_wizard_form_step_3').click(function () {
    return false;
});

$('.m_wizard_form_step_1').click(function () {
    return false;
});

$('.btn_send').click(function () {
    $('#term-service-alert-message').show();
});

$('.close-toast').click(function () {
    $('#term-service-alert-message').hide();
});
$('.close_pwd_toast').click(function () {
    $('#pw-alert-message').hide();
});
$("#data-wizard-back-button").click(function () {
    $("#step1_popover").popover('hide');
    $("#welcome_popover").popover('show');
    $("#submit_b2").addClass('pointer--none');
    $("#rtp_step2").val("false").trigger("change");
    $('.m_wizard_form_step_1').addClass('m-wizard__step--current');
    $('.m_wizard_formm_step_1').addClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_1').removeClass('m-wizard__step--done');
    $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
    $('.m_wizard_formm_step_2').removeClass('m-wizard__form-step--current');
    $('#wizard-progress-bar').addClass('width-0-percent');
    $('#wizard-progress-bar').removeClass('width-49-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_1 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_1 .m-wizard__step-number .wizard_dot').removeClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').removeClass('color-fff');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span').removeClass('background-ffa701');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span').removeClass('border-none');
});

$("#data-wizard-card-button").click(function () {
    $("#submit_b").addClass('pointer--none');
    $("#step1_popover").popover('show');
    $("#step2_popover").popover('hide');
    $('.m_wizard_form_step_2').addClass('m-wizard__step--current');
    $('.m_wizard_formm_step_2').addClass('m-wizard__form-step--current');
    $('.m_wizard_form_step_2').removeClass('m-wizard__step--done');
    $('.m_wizard_form_step_3').removeClass('m-wizard__step--current');
    $('.m_wizard_formm_step_3').removeClass('m-wizard__form-step--current');
    $('#wizard-progress-bar').removeClass('width-100-percent');
    $('#wizard-progress-bar').addClass('width-49-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('show');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').removeClass('hide');
    $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
    $('.m_wizard_form_step_3 .m-wizard__step-number > span').removeClass('background-fff');
    $('.m_wizard_form_step_3 .m-wizard__step-number > span').removeClass('border-none');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').removeClass('show');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').addClass('hide');
    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').removeClass('show');

});
$("#cardNo").on("blur",function () {
    var card_no=$("#cardNoSafe").val();
    card_no=card_no.replace(/\s/g, '');
    if (card_no != "") {
        $.ajax({
            url: "/checkout/get_card_information",
            type: 'POST',
            data: {card_number: card_no},
            success: function (data) {
                var scheme = data["scheme"]
                if (scheme == "mastercard") {
                    $('.logo-card').prop('src', 'https://quickard.s3-us-west-2.amazonaws.com/master_card_without_line.png');
                } else if (scheme == "discover") {
                    $('.logo-card').prop('src', 'https://quickard.s3-us-west-2.amazonaws.com/discover.png');
                } else if (scheme == "jcb") {
                    $('.logo-card').prop('src', 'https://quickard.s3-us-west-2.amazonaws.com/jcb.png');
                }else if (scheme == "visa") {
                    $('.logo-card').prop('src', 'https://quickard.s3-us-west-2.amazonaws.com/visa.svg');
                }else if (scheme == "amex") {
                    $('.logo-card').prop('src', 'https://quickard.s3-us-west-2.amazonaws.com/american_express.png');
                }
            },
            error: function (data) {

            }
        });
    }
});
$(document).ready(function () {
    $("#cardNo,#card_name,#card_exp,#card_cvv,#term_service,#privacy").on("keyup change",function () {
        var card_no=$("#cardNo").val();
        var card_name=$("#card_name").val();
        var card_exp=$("#card_exp").val();
        var card_cvv=$("#card_cvv").val();
        var terms=$("#term_service").is(':checked');
        var privacy=$("#privacy").is(':checked');
        if (card_no.replace(/\s/g, '').length >= 13 && card_exp.length == 5 && card_cvv.length >=3 && card_no != "" && card_exp != "" && card_name != "" && card_cvv != "" && terms == true && privacy == true){
            $(".btn-pay").css("background-color","#ffd95a");
            $(".btn-pay").css("color","#4c4c4c");
        }else{
            $(".btn-pay").css("background-color","#e8e8e8");
            $(".btn-pay").css("color","#adadad");
        }
    })
});
$("#new_card").on("submit",function (e) {
    if($("#new_card").valid() && $("#term_service").is(':checked') && $("#privacy").is(':checked')) {
        $(".checkout_box").addClass("img--hide");
        $(".checkout_box_loader").removeClass("img--hide");
        randomize(0);
        setTimeout(function(){
            randomize(25);
        }, 1000);
    }
    else{
        e.preventDefault();
        return false;
    }
});

$("#submit_b").on("click",function (e) {
    if($("#new_card").valid() && $("#term_service").is(':checked') && $("#privacy").is(':checked')) {
        $("#step2_popover").popover('hide');
        $("#new_card").submit();
    }else{
        e.preventDefault();
        return false;
    }
});
$("#submit_b2").on("click",function (e) {
    if($("#shipping_form").valid() && $("#term_service2").is(':checked') && $("#privacy2").is(':checked')) {
        var street = $("#shipping_street").val();
        var suite = $("#shipping_suit").val();
        var city = $("#shipping_city").val();
        var zip = $("#shipping_zip").val();
        var state = $("#shipping_state").val();
        var country = $("#s_country").val();
        var shipping_id = $("#shipping_id").val();
        shipping_address = {id: shipping_id, street: street,suit: suite,city: city,zip: zip,state: state,country: country};
        $("#shipping_params").val(JSON.stringify(shipping_address));
        $("#submit_b2").addClass('m-loader m-loader--right m-loader--light pointer--none').prop("disabled",true);
        $('.api_checkout_box_loader').addClass('img--show');
        $('.checkout_box').addClass('img--hide');
        $('.popover-body').addClass('img--hide');
        randomize(0);
        setTimeout(function(){randomize(25);}, 1000);
        $("#rtp_form").submit();
    }else{
        e.preventDefault();
        return false;
    }
});

function validateBillingForm(form_id) {
    $(function() {
        var formRules = {
            rules: {
                "address[name]": {
                    required: true
                },
                "address[email]":{
                    required:true
                },
                "phone_number":{
                    required:true
                },
                "address[street]":{
                    required:true
                },
                "address[city]":{
                    required:true
                },
                "address[zip]":{
                    required:true
                },
                "billing[state]":{
                    required:true
                },
                "billing[country]":{
                    required:true
                }


            },
            messages: {
                "address[name]": {
                    required: "Please enter Customer Name"
                },
                "address[email]":{
                    required: "Please enter Email"
                },
                "phone_number":{
                    required: "Please enter Phone Number"
                },
                "address[street]":{
                    required: "Please enter Street"
                },
                "address[city]":{
                    required: "Please enter City"
                },
                "address[zip]":{
                    required: "Please enter ZipCode"
                },
                "billing[state]":{
                    required: "Please select State"
                },
                "billing[country]":{
                    required: "Please select Country"
                }

            },

        };
        $('#' + form_id).validate(formRules);
    });
};


function validateShippingForm(form_id) {
    $(function() {
        var formRules = {
            rules: {
                "address[street]":{
                    required:true
                },
                "address[city]":{
                    required:true
                },
                "address[zip]":{
                    required:true
                },
                "shipping[state]":{
                    required:true
                },
                "shipping[country]":{
                    required:true
                }


            },
            messages: {
                "address[street]":{
                    required: "Please enter Street"
                },
                "address[city]":{
                    required: "Please enter City"
                },
                "address[zip]":{
                    required: "Please enter ZipCode"
                },
                "shipping[state]":{
                    required: "Please select State"
                },
                "shipping[country]":{
                    required: "Please select Country"
                }

            },

        };
        $('#' + form_id).validate(formRules);
    });
};
$(document).ready(function(){
    var img_src = $(".cvv-icon-img").attr("src");
    $(".cvv-icon").popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<img class="img-fluid" src="'+img_src+ '" />';
        },
    })
})
$('.last_step').click(function () {
    $('#wizard-progress-bar').addClass('width-100-percent');
});
$(document).ready(function () {
    $("#term_service2,#privacy2,#rtp_step2").on("keyup change",function () {
        var terms=$("#term_service2").is(':checked');
        var privacy=$("#privacy2").is(':checked');
        var rtp_step2=$("#rtp_step2").val();
        if (terms == true && privacy == true && rtp_step2 == "true"){
            $("#submit_b2").css("background-color","#ffd95a").css("color","#4c4c4c");
        }else{
            $("#submit_b2").css("background-color","#e8e8e8").css("color","#adadad");
        }
    })
});
$('.btn-rtp-api-submit').on('click' ,function (e) {
    validateBillingForm('new_api_checkout');
    if($("#new_api_checkout").valid()) {
        $('.api_checkout_box_loader').addClass('img--show');
        $('.main_rtp_section').addClass('img--hide');
        randomize(0);
        setTimeout(function(){randomize(25);}, 1000);
    }
    else{
        e.preventDefault();
        return false;
    }
});
// if(window.location.pathname == "/checkout/invoice_rtp_request"){
//     $('img').attr("src",window.location.origin+"assets/v2/login/QCLogoAnimation.gif").attr("width","100").attr("height","100");
//     setTimeout(function () {
//         $('form[name="process"]').submit();
//     }, 3000);
// }

$(document).on('change',"#b_country", function () {
    if($(this).val() != ""){
        $.ajax({
            url: '/api/checkout/country_data',
            data: {country: $(this).val()},
            method: 'GET',
            success: function (response) {
                $("#billing_state").empty();
                var states = response["states"];
                if(!$.isEmptyObject(states)){
                    $.each(states, function(key,value) {
                        $("#billing_state").append('<option value=' + key + '>' + value + '</option>');
                    });
                    $('#billing_state').prop('disabled',false);
                    $('#billing_state').selectpicker('refresh');
                }else{
                    $('#billing_state').prop('disabled',true);
                    $('#billing_state').selectpicker('refresh');
                }
            }
        });
    }else{
        $('#billing_state').prop('disabled', true);
    }
});

$(document).on('change',"#s_country", function () {
    if($(this).val() != ""){
        $.ajax({
            url: '/api/checkout/country_data',
            data: {country: $(this).val()},
            method: 'GET',
            success: function (response) {
                $("#shipping_state").empty();
                var states = response["states"];
                if(!$.isEmptyObject(states)){
                    $.each(states, function(key,value) {
                        $("#shipping_state").append('<option value=' + key + '>' + value + '</option>');
                    });
                    $('#shipping_state').prop('disabled',false);
                    $('#shipping_state').selectpicker('refresh');
                }else{
                    $('#shipping_state').prop('disabled',true);
                    $('#shipping_state').selectpicker('refresh');
                }
            }
        });
    }else{
        $('#shipping_state').prop('disabled', true);
    }
});
