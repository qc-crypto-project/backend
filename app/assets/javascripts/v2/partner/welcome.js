//wizard js
$('.data-wizard-ok-button').click(function () {
    $("#step1_popover").popover('show');
    $("#welcome_popover").popover('hide');
    $('#wizard-progress-bar').removeClass('width-0-percent');
    $('#wizard-progress-bar').addClass('width-50-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').show();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
});

$('#agreeTermsAndServiceBtn').click(function () {
    $(this).addClass('m-loader m-loader--right m-loader--light').prop("disabled",true);

    $('#term-service-alert-message').show();
    $('#tos-alert-text').text("The Terms of Service Accepted");
    setTimeout(function(){
        if(!$('.ready-message').hasClass('show')){
            setTimeout(function () {
                $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
                $('.m_wizard_form_step_2').removeClass('m-wizard__form-step--current');
                $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
                $('#wizard-progress-bar').addClass('width-50-percent');
                $('.ready-message').show();
            },2000);
            setTimeout(function () {
                $("#step1_popover").popover('hide');
                $('.ready-message').hide();
                $('.main-content').show();
                $("#step2_popover").popover('show');
                $('#wizard-progress-bar').addClass('width-100-percent');
                $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
                $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
                $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
                $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
                $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
                $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
                $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').hide();
                $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').show();
            },5000);
        }
        else{
            $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
            $('.m_wizard_form_step_2').removeClass('m-wizard__form-step--current');
            $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
            $('.ready-message').hide();
            $('.main-content').show();
            $("#step2_popover").popover('show');
            $('#wizard-progress-bar').addClass('width-100-percent');
            $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
            $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
            $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
            $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
            $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
            $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
            $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').hide();
            $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').show();
        }
    }, 1000);

});


$('.login-welcome-change-password-button').click(function () {
    var new_password_value = $(".hide_new_welcome_password").val();
    var old_password_value = $(".hide_old_welcome_password").val();
    $(this).addClass('m-loader m-loader--right m-loader--light').prop("disabled",true);
    user_type = $("#user_type").val()
    user_type == "merchant" ? pas_url = '/v2/update_password_tos' : pas_url = '/v2/partner/update_password_tos'
    user_type == "merchant" ? tos_app_url = '/v2/tos_approval' : tos_app_url = '/v2/partner/tos_approval'
    $.ajax({
        url: pas_url,
        type: 'POST',
        data: {new_password: new_password_value, old_password: old_password_value},
        success: function(data) {
            $('#pw-alert-message').show();
            setTimeout(function(){
                $('#wizard-progress-bar').addClass('width-100-percent');
                $.ajax({
                    url: tos_app_url,
                    type: 'POST',
                    data: {data: "data"},
                    success: function(data) {
                        // $('#term-service-alert-message').show();
                        // $('#tos-alert-text').text("The Terms of Service Accepted");
                        user_type == "merchant" ?  window.location.href  = "/v2/merchant/accounts" :  window.location.href  = '/v2/partner/accounts'
                    },
                    error: function(data){
                        $('#term-service-alert-message').show();
                        $('#tos-alert-text').text("Something Went Wrong!");
                    }
                });
            }, 2000);
        },
        error: function(success){
            if(success.responseText == "Your session expired. Please sign in again to continue."){
                location.reload(true)
            }
            if(success.responseJSON.success == "failed") {
                $('.login-welcome-change-password-button').removeClass('m-loader m-loader--right m-loader--light').prop("disabled", false);
                $("#invalid_old_password").show().text("Wrong Password").fadeOut(4000);
            }
            else{
                $('.login-welcome-change-password-button').removeClass('m-loader m-loader--right m-loader--light').prop("disabled", false);
                $("#invalid_old_password").show().text(success.responseJSON.success).fadeOut(4000);
            }
        }
    });
});
