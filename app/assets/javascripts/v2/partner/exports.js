$(document).ready(function () {
    $(".payee_checkbox").prop("checked", true);
    var globalTimeout = null;
    $(document).on('keyup','.search_input_datatable',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var search_value = $('.search_input_datatable').val();
            var filter_data = "";
            if(filter != ""){
                filter_data = filter;
            }else{
                filter_data = 10;
            }
            if (search_value.length >= 3 || search_value == "") {
                $.ajax({
                    url: '/v2/partner/exports',
                    type: 'GET',
                    data: {query: search_value, filter: 10},
                    success: function (data) {
                        $('#exports_list').html('').html(data);
                        $("#m_table_1").DataTable();
                        $("#page").selectpicker();
                        $("#filter").selectpicker();
                        $('.search_input_datatable').val(search_value);
                        $('.search_input_datatable').focus();
                    }, error: function (data) {


                    }
                });
            }

        }, 1500);
    });
});

function search_call(type){
    var search_value = $('.search_input_datatable').val();
    data = {"q[data_search]": search_value,filter: $("#filter option:selected").val()};
    if($('.payee_checkbox:checked').length == 0)//if none is check still want to send something in order to get empty results
        {
            data["no_data"] = "no_data"
        }
    $('.payee_checkbox').each(function(index){
        if($('.payee_checkbox')[index].checked){
            data[$('.payee_checkbox')[index].name] = $('.payee_checkbox')[index].value
        }
    })
    var filter_data = "";
    $.ajax({
        url: '/v2/partner/exports',
        type: 'GET',
        data: data,
        success: function (data) {
            $('#exports_list').html('').html(data);
            $("#m_table_1").DataTable();
            if(type == "status"){
                $(".btn-status").trigger('click');
                $('.payee_checkbox').removeAttr("disabled");
            }
            $("#page").selectpicker();
            $("#filter").selectpicker();
            $('.search_input_datatable').val(search_value);
            $('.search_input_datatable').focus();
        }, error: function (data) {
        }
    });
}

$(document).on('change','.payee_checkbox',function () {
    $('.payee_checkbox').attr("disabled",true);
    if($(this).attr("id") == "all"){
        if( $(this).is(":checked")) {
            $(".payee_checkbox").prop("checked", true)
        }else{
            $('.payee_checkbox').prop("checked", false)
        }
    }else{
        if($(this).is(":checked")){
            $(this).prop("checked",true)
        }else{
            $(this).prop("checked",false)
        }
        $('#all').prop("checked",false)
    }
    if($(".payee_checkbox:not(:checked)").length == 1  )// when all is check except all then all checkbox will automatically checked
    {
        $("#all").prop("checked",true)
    }
    // if($('.payee_checkbox:checked').length == 0)//if none is check still want to send something in order to get empty results
    // {
    //     $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    // }

    search_call("status");
    $(".btn-status").trigger('click');
});