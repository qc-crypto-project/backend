//Steps JS
$(document).ready(function () {
  var currentStep = 0;
  var totalSteps = 2;
  var passStrength = false;

  var toggleModal = function () {
    var modalName = $(".email-modal");

    if ($(modalName).hasClass("open")) {
      $(modalName).removeClass("open");
      $(modalName).fadeOut(600);
    } else {
      $(modalName).addClass("open");
      $(modalName).fadeIn(600);
    }
  };

  var closeAlert = function (id) {
    $("#" + id).fadeOut(500);
  };

  var alertBox = function (id) {
    $("#" + id).fadeIn(500, function () {
      setTimeout(function () {
        closeAlert(id);
      }, 5000);
    });

    $(".close-alert").click(function () {
      closeAlert(id);
    });
  };

  var showTooltip = function (step, step2) {
    if (step2 === undefined) {
      $(".point" + step)
        .find(".tooltip")
        .fadeTo(600, 1);
    } else {
      $(".point" + step2)
        .find(".tooltip")
        .fadeTo(600, 1);
      $(".point" + step)
        .find(".tooltip")
        .fadeTo(300, 0);
    }
  };

  var setStep = function (step) {
    setCurrentStep();
    $(".step" + currentStep).fadeOut(500, function () {
      $(".step" + step).fadeIn(500);
    });
    $(".point" + currentStep).addClass("current");
  };

  var animateLoading = function (step, timing) {
    $(".loading" + step)
      .delay(500)
      .animate({ width: "100%" }, timing);
  };

  var addStepDone = function (step) {
    $(".point" + step).removeClass("current");
    $(".point" + step).addClass("checked");
  };

  var setCurrentStep = function () {
    $(".current-step").html(currentStep);
  };

  //steps flow

  //set steps
  setCurrentStep();
  $(".total-steps").html(totalSteps);

  //welcome
  $("#step1").click(function () {
    2;
    currentStep = 1;
    setStep("2");
    showTooltip("1");
  });

  //step1
  $("#termsCheck").click(function () {
    if ($("#termsCheck:checkbox:checked").length > 0) {
      $("#step2Btn").prop("disabled", false);
    } else {
      $("#step2Btn").prop("disabled", true);
    }
  });

  $("#step2Btn").click(function () {
    currentStep = 2;
    setStep("3");
    showTooltip("1", "2");
    addStepDone("1");
    animateLoading("1", 5000);
    setTimeout(function () {
      $(".loading-msg").fadeOut(500, function () {
        $(".step3-form1").fadeIn(500);
      });
    }, 4500);
  });

  //step2
  $("#passwordForm").validate({
    submitHandler: function (form) {
      alertBox("alert1");
      addStepDone("2");
      animateLoading("2", 1200);
      setTimeout(function () {
        window.location.href = "accounts.html";
      }, 4000);
    },
    rules: {
      new_password: {
        minlength: 8,
      },
      confirm_password: {
        equalTo: "#newPassword",
      },
    },
    messages: {
      confirm_password: "Password does not match",
    },
  });

  //Modal Email
  $(".button-send-email").click(function () {
    // toggleModal();
    $('#email_modal').modal("toggle")
  });

  $(".other-email").click(function () {
    $(".confirm-email").fadeOut(500, function () {
      $(".new-email").fadeIn(500);
    });
  });

  //COnfirm Email form
  $("#confirmEmail").validate({
    highlight: function (element, errorClass, validClass) {
      $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass(errorClass).addClass(validClass);
    },
    // submitHandler: function (form) {
    //   //$(form).submit();
    //   alertBox("alert2");
    //   toggleModal("email-modal");
    // },
  });

  //New email form
  $("#newEmail").validate({
    highlight: function (element, errorClass, validClass) {
      $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass(errorClass).addClass(validClass);
    },
    // submitHandler: function (form) {
    //   //$(form).submit();
    //   alertBox("alert2");
    //   toggleModal("email-modal");
    // },
  });

  //pass strnght plugin
  $(".pass-check-input")
    .keyup(function () {
      var pswd = $(this).val();

      //onme var for each validation
      var val1 = false;
      var val2 = false;
      var val3 = false;
      var val4 = false;

      if (pswd.length < 8) {
        $("#length").removeClass("valid").addClass("invalid");
      } else {
        $("#length").removeClass("invalid").addClass("valid");
        val1 = true;
        checkValidation();
      }
      //validate letter
      if (pswd.match(/[A-z]/)) {
        $("#letter").removeClass("invalid").addClass("valid");
        val2 = true;
        checkValidation();
      } else {
        $("#letter").removeClass("valid").addClass("invalid");
      }

      //validate capital letter
      if (pswd.match(/[A-Z]/)) {
        $("#capital").removeClass("invalid").addClass("valid");
        val3 = true;
        checkValidation();
      } else {
        $("#capital").removeClass("valid").addClass("invalid");
      }

      //validate number
      if (pswd.match(/\d/)) {
        $("#number").removeClass("invalid").addClass("valid");
        val4 = true;
        checkValidation();
      } else {
        $("#number").removeClass("valid").addClass("invalid");
      }

      //check if all validations are true and set main var validation to true
      function checkValidation() {
        if (val1 === true && val2 === true && val3 === true && val4 === true) {
          passStrength = true;
        }
      }
    })
    .focus(function () {
      $(".pass-strength").fadeIn(500);
    })
    .blur(function () {
      $(".pass-strength").fadeOut(300);
      if (passStrength === false) {
        $(".pass-check-input").removeClass("valid").addClass("error");
      } else {
        $(".pass-check-input").removeClass("error").addClass("valid");
      }
    });



    $('#confirmEmail').submit(function(evt) {
     	evt.preventDefault()
    })

    $('#newEmail').submit(function(evt) {
     	evt.preventDefault()
    })


    $('.login-welcome-change-password-button-aff2').click(function () {
	    var new_password_value = $(".hide_new_welcome_password").val();
	    var old_password_value = $(".hide_old_welcome_password").val();
	    $(this).addClass('m-loader m-loader--right m-loader--light').prop("disabled",true);
	    user_type = $("#user_type").val()
	    user_type == "merchant" ? pas_url = '/v2/update_password_tos' : pas_url = '/v2/partner/update_password_tos'
	    $.ajax({
	        url: pas_url,
	        type: 'POST',
	        data: {new_password: new_password_value, old_password: old_password_value},
	        success: function(data) {
	        	alertBox("alert1");
      			addStepDone("2");
      			animateLoading("2", 1200);
      			setTimeout(function(){
                $.ajax({
                    url: '/v2/partner/tos_approval',
                    type: 'POST',
                    data: {data: "data"},
                    success: function(data) {
                        window.location.href  = '/v2/partner/accounts'
                    },
	                    error: function(data){
	                        $('#term-service-alert-message').show();
	                        $('#tos-alert-text').text("Something Went Wrong!");
	                    }
	                });
	            }, 2000);
	        },
	        error: function(success){
	            if(success.responseText == "Your session expired. Please sign in again to continue."){
	                location.reload(true)
	            }
	            if(success.responseJSON.success == "failed") {
	                $('.login-welcome-change-password-button').removeClass('m-loader m-loader--right m-loader--light').prop("disabled", false);
	                $("#invalid_old_password").show().text("Wrong Password").fadeOut(4000);
	                $('.login-welcome-change-password-button-aff2').removeClass('m-loader m-loader--right m-loader--light').prop("disabled",false);
	            }
	            else{
	                $('.login-welcome-change-password-button').removeClass('m-loader m-loader--right m-loader--light').prop("disabled", false);
	                $("#invalid_old_password").show().text(success.responseJSON.success).fadeOut(4000);
	                $('.login-welcome-change-password-button-aff2').removeClass('m-loader m-loader--right m-loader--light').prop("disabled",false);
              }
	        }
	    });
	});





  //----
});
