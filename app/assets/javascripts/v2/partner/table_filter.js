$('.partner-sort-table').click(function(){
    $(this).find('img').toggleClass('flip');
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc){rows = rows.reverse()}
    for (var i = 0; i < rows.length; i++){table.append(rows[i])}
})
function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
    }
}
function getCellValue(row, index){ return $(row).children('td').eq(index).text() }

$(document).ready(function () {
    $("#m_table_1_daily_p").DataTable({
        "paging":   false,
        "info":     false,
        "searching": false,
        "order": [[ 0, "desc" ]]
    });
});