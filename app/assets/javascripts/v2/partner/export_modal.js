$("#export_button").on("click",function () {
    start_date=$('#date_start').val();
    end_date=$('#date_end').val();
    date = start_date +" - "+ end_date;
    var type = $("#type").val();
    var sub_type = $(this).data("subtype");
    var wallet_id= $(this).data("wallet_id");

    controller_call(date, type, sub_type, wallet_id,'', "");
});

$("#batch_export").on("click", function(){
    var start_date = $(this).data("date");
    var batch_date = start_date
    var type = $("#type").val();
    var sub_type = $(this).data("sub_type");
    var wallet_id = $(this).data("wallet_id");
    controller_call("", type, sub_type, wallet_id,'', batch_date);
})

$("#search_export").on("click",function () {
    if ($(this).data('search-date')==undefined){
        start_date=$('#date_start').val();
        end_date=$('#date_end').val();
        date = start_date +" - "+ end_date;
    }
    else {
        date = $(this).data('search-date');
    }
    if ($(this).data('search-type1')==undefined){
        type1 = $("#type1").val();
    }
    else {
        type1 = $(this).data('search-type1');

    }
    var type = $(this).data("type");
    var sub_type=$(this).data("subtype");
    var wallet_id=$(this).data("wallet_id");
    var batch_date = $(this).data("batch-date");
    var search_query = {
        name:  $("#name").val(),
        email: $(this).data('search-email'),
        phone: $(this).data('search-phone'),
        amount: $(this).data('search-amount'),
        last4: $(this).data('search-last4'),
        type: $(this).data('search-type'),
        type1: type1,
        time1: $(this).data('search-time1'),
        time2: $(this).data('search-time2'),
        first6: $(this).data('search-first6'),
        date: date,
        block_id: $(this).data('search-block-id'),
        batch_id: $(this).data('batch-id'),
        status:  $('#checkboxes').val(),
        sender_name: $("#sender_name").val(),
        sender_email: $(this).data("sender-email"),
        dba_name: $("#dba_name").val() || $(this).data("search-dba_name"),
        sender_phone_number: $(this).data("phone"),


    };
    controller_call(null, type, sub_type, wallet_id, search_query, batch_date);
});
$(document).on("click","#sales_batch_export",function () {
    if($("#start_date_export").val() != "" && $("#end_date_export").val() != "" ){
        var startdate = $("#start_date_export").val()
        startdate = startdate.split("/")
        start_day = startdate[0]
        start_month = startdate[1]
        start_year = startdate[2]
        startdate = start_month+"/"+start_day+"/"+start_year
        var enddate = $("#end_date_export").val()
        enddate = enddate.split("/")
        end_day = enddate[0]
        end_month = enddate[1]
        end_year = enddate[2]
        enddate = end_month+"/"+end_day+"/"+end_year
        $("#exportProcessingModal").modal('show');
        $("#date_start").val(startdate);
        $("#date_end").val(enddate);
        $("#export_button").click();
    }
});
function controller_call(date, type, sub_type, wallet_id, search_query, batch_date) {
    var offset = moment().local().format('Z');
    if($('#export_button').hasClass('loader_exp')){
        $('#export_button').addClass('m-loader');
        $('#export_button').addClass('m-loader--right');
        $('#export_button').addClass('pointer--none');
    }
    $.ajax({
        url: "/v2/generate_export_file",
        dataType: "json",
        data: {date: date, batch_date: batch_date, type: type, offset: offset,trans: sub_type,wallet_id: wallet_id, search_query: search_query},
        success: function (data) {
            if($('#export_button').hasClass('loader_exp')){
                $('#export_button').removeClass('m-loader');
                $('#export_button').removeClass('m-loader--right');
                $('#export_button').removeClass('pointer--none');
            }
            if(data.status == "pending"){
                $("#processing_export").hide();
                $("#pending_export").removeClass('hide_image');
            }else{
                $("#processing_export").show();
                if (!$("#pending_export").hasClass('hide_image')){
                    $("#pending_export").addClass('hide_image');
                }
            }
            // $('#exportProcessingModal').modal('toggle');
        }
    })
}