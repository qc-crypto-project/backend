function setTooltip(btn, message) {
    $(btn).tooltip('hide')
        .attr('data-original-title', message)
        .tooltip('show');
}

function hideTooltip(btn) {
    setTimeout(function() {
        $(btn).tooltip('hide');
    }, 1000);
}

$(document).ready(function(){
    var clipboard = new Clipboard('.copy_transection_id');
    clipboard.on('success', function(e) {
        e.clearSelection();
        setTooltip(e.trigger, 'Copied to Clipboard!');
        hideTooltip(e.trigger);
    });
    console.log(clipboard);
});