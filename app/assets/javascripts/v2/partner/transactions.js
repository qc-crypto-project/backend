$(document).on('click', "#personal", function () {
    var url = $(this).data("url");
    show_transaction(url,"personal")
});

$(document).on('click', ".commission", function () {
    var url = $(this).data("url");
    show_transaction(url, "commission")
});

$(document).on('click', "#commission", function () {
    var url = $(this).data("url");
    show_transaction(url, "commission")
});

function show_transaction(url,type) {
    window.location.href = url + "?type="+ type
}

$("tr[data-path]").click(function() {
  window.location = $(this).data("path")
})