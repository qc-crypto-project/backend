$( "#partnercookiesbtn" ).click(function() {
    $( ".cookie-overlay" ).addClass('hide');
    $( ".cookie-overlay" ).removeClass('show');
    $.ajax({
        url: "/v2/partner/base/accept_cookies",
        type: "POST",
        data: {},
        success: function(data){

        }
    })
});

// To prevent reload on hit enter in g-card search field
$(document).on('submit', '#search_form_shared', function(e){
    e.preventDefault();
});