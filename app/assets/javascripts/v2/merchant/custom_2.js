$(".mdp-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.mdp-input').attr('style', 'background-color:transparent');
    }
    else
    {
        $('.mdp-input').attr('style', 'background-color:#f7f7f7 !important');
    }
});
// invoice toggle js
$("#togBtn").on('change', function() {
    if ($(this).is(':checked')) {
        $('#discount_sign').addClass('addon_left');
        $('.p_dollar_discount').removeClass('th-bold');
        $('.p_percent_discount').addClass('th-bold');
    }
    else {
        $('#discount_sign').removeClass('addon_left');
        $('.p_dollar_discount').addClass('th-bold');
        $('.p_percent_discount').removeClass('th-bold');
    }
});

$("#fee_togBtn").on('change', function() {
    if ($(this).is(':checked')) {
        $('#fee_sign').addClass('addon_left');
        $('.p_dollar_fee').removeClass('th-bold');
        $('.p_percent_fee').addClass('th-bold');
    }
    else {
        $('#fee_sign').removeClass('addon_left');
        $('.p_dollar_fee').addClass('th-bold');
        $('.p_percent_fee').removeClass('th-bold');

    }
});

$("#late_fee_togBtn").on('change', function() {
    if ($(this).is(':checked')) {
        $('#late_fee_sign').addClass('addon_left');
        $('.p_dollar_late_fee').removeClass('th-bold');
        $('.p_percent_late_fee').addClass('th-bold');
    }
    else {
        $('#late_fee_sign').removeClass('addon_left');
        $('.p_dollar_late_fee').addClass('th-bold');
        $('.p_percent_late_fee').removeClass('th-bold');

    }
});

// Business Setting js
$('.reveal_key_button').click(function () {
$('.reveal_key_button').hide();
$('.blur-text').css('filter','blur(0px)');
$('.copy_image').show();
$('.component_image').hide();
$('.hide_key_button').show();
});
$('.hide_key_button').click(function () {
    $('.hide_key_button').hide();
    $('.blur-text').css('filter','blur(3px)');
    $('.reveal_key_button').show();
    $('.copy_image').hide();
    $('.component_image').show();
});

// select all checkbox js in create user permission modal
// $(document).ready(function(){
// $('input[class="select_all_permissions"]').click(function(){
//     debugger;
//     if($(this).prop("checked") == true){
//         $("input[type=checkbox]").prop("checked", true);
//     }
//     else if($(this).prop("checked") == false){
//         $("input[type=checkbox]").prop("checked", false);
//     }
// });
// });

$("select.permission_dropdown").change(function(){
    var selectedCountry = $(this).children("option:selected").val();
    if(selectedCountry =='Admin (Full Access)'){
        $('#adminFullAccessModal').modal('show');
    }
});

$('.accept_chargeback').click(function () {
    $('.chargeback-accepted-status').show();
    $('.fight-chargeback-and-accept-btn').hide();
    $('.vl_1').show();
    $('.refund_accept_images').show();
    $('.vl_2').show();
    $('.refund_group_images').show();
    $('.charback-accepted-case').show();
    $('.fight-chargeback-part').hide();
});

$('.chargeback-submit-btn').click(function () {
    $('.fight-chargeback-case').show();
    $('.vl_3').show();
    $('.vl_4').show();
    $('.case-won-section').show();
    $('.refund_fcharge_images').show();
    $('.refund_fcharge1_images').show();

    $('.refund_group_images').show();
    $('.fight-chargeback-and-accept-btn').hide();
    $('.vl_1').show();
    $('.charback-accepted-case').hide();
    $('.fight-chargeback-part').hide();
});


$('#declinedSearchFilter').click(function () {
   $('#declinedFilterForm').show();
   $('#declinedSearchFilter').hide();
});


// loader js
function p2c(numb) {

    var rand = numb;
    var x = document.querySelector('.p2c');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.success-p2c-call').on('click' ,function () {
    setTimeout(p2c(0), 200);
    setTimeout(function(){ p2c(25); }, 1000);
    setTimeout(function(){ p2c(50);  }, 2000);
    setTimeout(function(){ p2c(75); }, 3000);
    setTimeout(function(){ p2c(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function echeck(numb) {
    var rand = numb;
    var x = document.querySelector('.echk');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}

$('.success-echeck-call').on('click' ,function () {

    setTimeout(echeck(0), 200);
    setTimeout(function(){ echeck(25); }, 1000);
    setTimeout(function(){ echeck(50);  }, 2000);
    setTimeout(function(){ echeck(75); }, 3000);
    setTimeout(function(){ echeck(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

function giftCard(numb) {
    var rand = numb;
    var x = document.querySelector('.giftCarD');
    x.style.strokeDasharray = (rand * 4.65) + ' 999';
}
// old password js
var starPasswordValue;
$( ".login-old-password-open-eye" ).click(function() {
    var password=$(".hide_old_welcome_password").val();
    starPasswordValue=$(".m-old-welcome-password").val();
    $(".m-old-welcome-password").val(password);
    $(".login-old-password-open-eye").removeClass('show');
    $(".login-old-password-open-eye").addClass('hide');
    $(".login-old-password-eye").addClass('show');
    $(".login-old-password-eye").removeClass('hide');
});

$( ".login-old-password-eye").click(function() {
    var password=$(".hide_old_welcome_password").val();
    $(".m-old-welcome-password").val(starPasswordValue);
    $(".login-old-password-open-eye").addClass('show');
    $(".login-old-password-open-eye").removeClass('hide');
    $(".login-old-password-eye").removeClass('show');
    $(".login-old-password-eye").addClass('hide');
});
// dropdown opetion image js
$('.permission-dropdown--menu a').click(function () {
   var value=$(this).attr('data-value');
   $('.permission-dropdown').text(value);
   $('.dropdown-menu--pad').removeClass('show');
});

$('.success-giftCard-call').on('click' ,function () {

    setTimeout(giftCard(0), 200);
    setTimeout(function(){ giftCard(25); }, 1000);
    setTimeout(function(){ giftCard(50);  }, 2000);
    setTimeout(function(){giftCard(75); }, 3000);
    setTimeout(function(){ giftCard(100); }, 4000);
    setTimeout(function(){
        $('.working').hide();
        $('.first--text').hide();
        $('.last--text').show();
        $('.success--image').show();
    }, 4500);
});

$('.p2c-confirm-btn').click(function () {
    setTimeout(function(){
        $('.first--text').hide();
        $('.account-transfer-success .small').hide();
        $('.last--text').show();
    }, 45000);
});

$('.ach-confirm-btn').click(function () {
    setTimeout(function(){
        $('.first--text').hide();
        $('.account-transfer-success .small').hide();
        $('.last--text').show();
    }, 45000);
});

$('.giftcard-confirm-btn').click(function () {
    setTimeout(function(){
        $('.first--text').hide();
        $('.account-transfer-success .small').hide();
        $('.last--text').show();
    }, 45000);
});

$('#hide_declin_form').click(function () {
  $('#declinedFilterForm').hide();
  $('#declinedSearchFilter').show();
});

$('.fight_chargeback_btn').click(function () {
  $('.fight-chargeback-part').show();
    $('.fight-chargeback-and-accept-btn').hide();
});

$('.gift-card-back-btn').click(function () {
    $('#body--aa > div.daterangepicker.dropdown-menu.ltr.opensleft').removeClass('show-calendar');
    $('.opensleft').css('display','none');
});

//add giftcard enter amount addons
$(".product_amount1").on('keyup change', function (){
    if($(this).val() !==''){
        $(".dollar-addon").css("cssText", "display: block !important;");
    }
    else
    {
        $(".dollar-addon").css("cssText", "display: none !important;");
    }
});

//notification sidebar js
$(".close_all_notify").hover(function(){
    $('.clear-all-notification').addClass('show');
    $('.close_all_notify').addClass('postion-30-bottom');
}, function(){
    $('.clear-all-notification').removeClass('show');
    $('.close_all_notify').removeClass('postion-30-bottom');
});

// $(".show-more").click(function () {
//    $('.all-notify').addClass("hide");
//    $('.empty-notification-message').addClass('show');
// });

$(".close_notification-1").hover(function(){
    $('.close-notify-1').removeClass('vissiblity-hidden');
    $('.close-notify-1').addClass('vissible');
}, function(){
    $('.close-notify-1').addClass('vissiblity-hidden');
    $('.close-notify-1').removeClass('vissible');
});

$(".close_notification-2").hover(function(){
    $('.close-notify-2').removeClass('vissiblity-hidden');
    $('.close-notify-2').addClass('vissible');
}, function(){
    $('.close-notify-2').addClass('vissiblity-hidden');
    $('.close-notify-2').removeClass('vissible');
});

$(".close_notification-3").hover(function(){
    $('.close-notify-3').removeClass('vissiblity-hidden');
    $('.close-notify-3').addClass('vissible');
}, function(){
    $('.close-notify-3').addClass('vissiblity-hidden');
    $('.close-notify-3').removeClass('vissible');
});

$(".close_notification-4").hover(function(){
    $('.close-notify-4').removeClass('vissiblity-hidden');
    $('.close-notify-4').addClass('vissible');
}, function(){
    $('.close-notify-4').addClass('vissiblity-hidden');
    $('.close-notify-4').removeClass('vissible');
});

$(".close_notification-5").hover(function(){
    $('.close-notify-5').removeClass('vissiblity-hidden');
    $('.close-notify-5').addClass('vissible');
}, function(){
    $('.close-notify-5').addClass('vissiblity-hidden');
    $('.close-notify-5').removeClass('vissible');
});

$(".close_notification-6").hover(function(){
    $('.close-notify-6').removeClass('vissiblity-hidden');
    $('.close-notify-6').addClass('vissible');
}, function(){
    $('.close-notify-6').addClass('vissiblity-hidden');
    $('.close-notify-6').removeClass('vissible');
});

$(".close_notification-7").hover(function(){
    $('.close-notify-7').removeClass('vissiblity-hidden');
    $('.close-notify-7').addClass('vissible');
}, function(){
    $('.close-notify-7').addClass('vissiblity-hidden');
    $('.close-notify-7').removeClass('vissible');
});

$(".close_notification-8").hover(function(){
    $('.close-notify-8').removeClass('vissiblity-hidden');
    $('.close-notify-8').addClass('vissible');
}, function(){
    $('.close-notify-8').addClass('vissiblity-hidden');
    $('.close-notify-8').removeClass('vissible');
});

$(".close_notification-9").hover(function(){
    $('.close-notify-9').removeClass('vissiblity-hidden');
    $('.close-notify-9').addClass('vissible');
}, function(){
    $('.close-notify-9').addClass('vissiblity-hidden');
    $('.close-notify-9').removeClass('vissible');
});

$(".close_notification-10").hover(function(){
    $('.close-notify-10').removeClass('vissiblity-hidden');
    $('.close-notify-10').addClass('vissible');
}, function(){
    $('.close-notify-10').addClass('vissiblity-hidden');
    $('.close-notify-10').removeClass('vissible');
});

$(".close_notification-11").hover(function(){
    $('.close-notify-11').removeClass('vissiblity-hidden');
    $('.close-notify-11').addClass('vissible');
}, function(){
    $('.close-notify-11').addClass('vissiblity-hidden');
    $('.close-notify-11').removeClass('vissible');
});

$(".close_notification-12").hover(function(){
    $('.close-notify-12').removeClass('vissiblity-hidden');
    $('.close-notify-12').addClass('vissible');
}, function(){
    $('.close-notify-12').addClass('vissiblity-hidden');
    $('.close-notify-12').removeClass('vissible');
});

//close single notification
$(".close-notify-1").click(function () {
    $('.close_today_notify_1').addClass("hide");
});
$(".close-notify-2").click(function () {
    $('.close_today_notify_2').addClass("hide");
});
$(".close-notify-3").click(function () {
    $('.close_today_notify_3').addClass("hide");
});

$(".close-notify-4").click(function () {
    $('.close_yesterday_notify_1').addClass("hide");
});
$(".close-notify-5").click(function () {
    $('.close_yesterday_notify_2').addClass("hide");
});
$(".close-notify-6").click(function () {
    $('.close_yesterday_notify_3').addClass("hide");
});

$(".close-notify-7").click(function () {
    $('.close_to_day-ago_notify_1').addClass("hide");
});
$(".close-notify-8").click(function () {
    $('.close_to_day-ago_notify_2').addClass("hide");
});
$(".close-notify-9").click(function () {
    $('.close_to_day-ago_notify_3').addClass("hide");
});

$(".close-notify-10").click(function () {
    $('.close_older_notify_1').addClass("hide");
});
$(".close-notify-11").click(function () {
    $('.close_older_notify_2').addClass("hide");
});
$(".close-notify-12").click(function () {
    $('.close_older_notify_3').addClass("hide");
});

// Dateranger modal js

$('#tab_7_1').click(function () {
  $('#withdraw_heading').text("New ACH");
});

$('#tab_7_2').click(function () {
    $('#withdraw_heading').text("New P2C");
});

$('#tab_7_3').click(function () {
    $('#withdraw_heading').text("New Check");
});

$('#tab_7_4').click(function () {
    $('#withdraw_heading').text("New Gift Card");
});

// copy tooltip clipboard js
$('.copy_help_email').on("click",function () {

    $('.copy_help_email').tooltip('hide')
        .attr('data-original-title', 'Copied to Clipboard')
        .tooltip('show');
});

$('.copy_customer_support_number').on("click",function () {

    $('.copy_customer_support_number').tooltip('hide')
        .attr('data-original-title', 'Copied to Clipboard')
        .tooltip('show');
});

$('.copy_api_key_1').on("click",function () {
    $(this).tooltip('hide')
        .attr('data-original-title', 'Copied to Clipboard')
        .tooltip('show');
});

$('.copy_api_key_2').on("click",function () {
    $('.copy_api_key_2').tooltip('hide')
        .attr('data-original-title', 'Copied to Clipboard')
        .tooltip('show');
});

// $('.copy_transection_id').on("click",function () {
//     $('.copy_transection_id').tooltip('hide')
//         .attr('data-original-title', 'Copied to Clipboard')
//         .tooltip('show');
// });
$('.copy_chargeback_id').on("click",function () {
    $('.copy_chargeback_id').tooltip('hide')
        .attr('data-original-title', 'Copied to Clipboard')
        .tooltip('show');
});
$(document).on('click','.app_key', function () {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($.trim($('.app_key_text').text())).select();
    document.execCommand("copy");
    $temp.remove();
})
$(document).on('click','.app_secret', function () {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($.trim($('.app_secret_text').text())).select();
    document.execCommand("copy");
    $temp.remove();
})
//wizard js
$('.data-wizard-ok-button').click(function () {
    $("#step1_popover").popover('show');
    $("#welcome_popover").popover('hide');
    $('#wizard-progress-bar').removeClass('width-0-percent');
    $('#wizard-progress-bar').addClass('width-33-percent');
    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').show();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
});

$('#agreeTermsAndServiceBtn').click(function () {
    $(this).addClass('m-loader m-loader--right m-loader--light').prop("disabled",true);

            $('#term-service-alert-message').show();
            $('#tos-alert-text').text("The Terms of Service Accepted");
            setTimeout(function(){
                if(!$('.ready-message').hasClass('show')){
                    setTimeout(function () {
                        $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
                        $('.m_wizard_form_step_2').removeClass('m-wizard__form-step--current');
                        $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
                        $('#wizard-progress-bar').addClass('width-42-percent');
                        $('.ready-message').show();
                    },2000);
                    setTimeout(function () {
                        $("#step1_popover").popover('hide');
                        $('.ready-message').hide();
                        $('.main-content').show();
                        $("#step2_popover").popover('show');
                        $('#wizard-progress-bar').addClass('width-66-percent');
                        $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
                        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
                        $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
                        $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
                        $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
                        $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
                        $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').hide();
                        $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').show();
                    },5000);
                }
                else{
                    $('.m_wizard_form_step_3').addClass('m-wizard__form-step--current');
                    $('.m_wizard_form_step_2').removeClass('m-wizard__form-step--current');
                    $('.m_wizard_form_step_2').addClass('m-wizard__form-step--done');
                    $('.ready-message').hide();
                    $('.main-content').show();
                    $("#step2_popover").popover('show');
                    $('#wizard-progress-bar').addClass('width-66-percent');
                    $('.m_wizard_form_step_2').removeClass('m-wizard__step--current');
                    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
                    $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span >i').addClass('color-fff');
                    $('.m_wizard_form_step_2 .m-wizard__step-number > span').addClass('background-ffa701 border-none');
                    $('.m_wizard_form_step_2 .m-wizard__step-number > span > i').addClass('color-fff');
                    $('.m_wizard_form_step_3 .m-wizard__step-number > span').addClass('background-fff border-none');
                    $('.m_wizard_form_step_2 .m-wizard__step-number .wizard_dot').hide();
                    $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').show();
                }
            }, 1000);

});
$('.tos_update').click(function () {
    user_type = $("#user_type").val()
    user_type == "merchant" ? tos_app_url = '/v2/tos_approval' : tos_app_url = '/v2/partner/tos_approval'
    $.ajax({
        url: tos_app_url,
        type: 'POST',
        data: {data: "data"},
        success: function(data) {
            $('#term-service-alert-message').show();
            $('#tos-alert-text').text("The Terms of Service Accepted");
            user_type == "merchant" ?  window.location.href  = "/v2/merchant/accounts" :  window.location.href  = '/v2/partner/accounts'
        },
        error: function(data){
            $('#term-service-alert-message').show();
            $('#tos-alert-text').text("Something Went Wrong!");
        }
    });
});
function validateEmail($email) {
    var emailReg = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return emailReg.test( $email );
}
$(".btn_send_1").on('click',function () {
    user_email = $("#user_email").val();
    input_email = $("#original_email").val();
    if(validateEmail(input_email)){
        $(".message_error").hide()
    }else{
        $(".message_error").show()
        return;
    }
    user_type = $("#user_type").val()
    user_type == "merchant" ? tos_url = '/v2/send_tos' : tos_url = '/v2/partner/send_tos'
    if (user_email == input_email){
        $.ajax({
            url: tos_url,
            type: 'POST',
            data: {email: input_email},
            success: function(data) {
                $('#term-service-alert-message').show();
                $('#tos-alert-text').text("The Terms of Service was sent to email");
                alertBox("alert2")
            },
            error: function(data){
                if(data.responseText == "Your session expired. Please sign in again to continue."){
                    location.reload(true)
                }
            }
        });
        // $('#email_modal').modal('toggle');
    }else{
        $("#error_original_email").show().text('Incorrect Email!').fadeOut(2000);
        return;
    }
    $('#email_modal').modal('toggle');
});
$(document).on('focusin focusout click','#original_email',function(){
    $(this).addClass('success_field');
    var user_error = $("#original_email-error").text();
    var user_user = $("#original_email").val();
    var user_error_mobile = $("#mobile_phone_error").text();
       if (user_user == "" && user_error == ""){
           $(this).removeClass('error_field');
        $(this).addClass('success_field');
         }
    else if (user_error != ""){
           $(this).removeClass('success_field');
        $(this).addClass('error_field');
    }else{
        $(this).removeClass('error_field');
        $(this).addClass('success_field');
    }
});
$(document).on('focusin focusout click keyup','#email',function(){
    $(this).addClass('success_field');
    var user_error = $("#email-error").text();
    var user_user = $("#email").val();
       if (user_user == "" && user_error == ""){
           $('.btn_send_2').prop('disabled',true)
           $(this).removeClass('error_field');
           $(this).addClass('success_field');
         }
    else if (user_error != ""){
        $('.btn_send_2').prop('disabled',true)
           $(this).removeClass('success_field');
        $(this).addClass('error_field');
    }else{
        $('.btn_send_2').prop('disabled',false)
        $(this).removeClass('error_field');
        $(this).addClass('success_field');
    }
});
$(".btn_send_2").on('click',function () {
    input_email = $("#email").val();
    email_format = validateEmail(input_email);
    if(validateEmail(input_email)){
        $(".message_error").hide()
    }else{
        $(".message_error").show()
        return;
    }
    user_type = $("#user_type").val()
    user_type == "merchant" ? tos_url = '/v2/send_tos' : tos_url = '/v2/partner/send_tos'
    if(email_format == true){
    $.ajax({
        url: tos_url,
        type: 'POST',
        data: {email: input_email},
        success: function(data) {
            $('#term-service-alert-message').show();
            $('#tos-alert-text').text("The Terms of Service was sent to email");
            alertBox("alert2")
        },
        error: function(data){
        }
    });
        $('#email_modal').modal('toggle')}
});
$('.m_wizard_form_step_3').click(function () {
    return false;
});
$('.m_wizard_form_step_1').click(function () {
    return false;
});
// $('.btn_send').click(function () {
//     $('#term-service-alert-message').show();
// });
$('.close-toast').click(function () {
    $('#term-service-alert-message').addClass('hide');
});
$('.close_pwd_toast').click(function () {
    $('#pw-alert-message').hide();
});
$('.cross_align').click(function () {
    $('#term-service-alert-message').hide();
});
$('.login-welcome-change-password-button').click(function () {
    var new_password_value = $(".hide_new_welcome_password").val();
    var old_password_value = $(".hide_old_welcome_password").val();
    $(this).addClass('m-loader m-loader--right m-loader--light').prop("disabled",true);
    user_type = $("#user_type").val()
    user_type == "merchant" ? pas_url = '/v2/update_password_tos' : pas_url = '/v2/partner/update_password_tos'
    $.ajax({
        url: pas_url,
        type: 'POST',
        data: {new_password: new_password_value, old_password: old_password_value},
        success: function(data) {
            $('#pw-alert-message').show();
            setTimeout(function(){
                $("#step2_popover").popover('hide');
                $("#step3_popover").popover('show');
                $('.m_wizard_form_step_3 .m-wizard__step-number .wizard_dot').hide();
                $('.m_wizard_form_step_4').addClass('m-wizard__form-step--current');
                $('.m_wizard_form_step_3').removeClass('m-wizard__form-step--current');
                $('.m_wizard_form_step_3').addClass('m-wizard__form-step--done');
                $('#wizard-progress-bar').addClass('width-100-percent');
                $('.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number .wizard_dot').hide();
                $('.m_wizard_form_step_3 .m-wizard__step-number > span >i').addClass('color-fff');
                $('.m-wizard.m-wizard--2 .m-wizard__head .m-wizard__nav .m-wizard__steps .m-wizard__step .m-wizard__step-number > span').addClass('background-ffa701 border-none');
                $('.m-wizard.m-wizard--2 .m-wizard__head .m-wizard__nav .m-wizard__steps .m-wizard__step .m-wizard__step-number > span').addClass('background-fff border-none');
                $('.m_wizard_form_step_4 .m-wizard__step-number >span').addClass('color-fff');
                $('.m_wizard_form_step_4 .m-wizard__step-number >span').removeClass('background-ffa701');
                $('.m_wizard_form_step_4 .m-wizard__step-number .wizard_dot').show();
            }, 2000);
        },
        error: function(success){
            if(success.responseText == "Your session expired. Please sign in again to continue."){
                location.reload(true)
            }
            if(success.responseJSON.success == "failed") {
                $('.login-welcome-change-password-button').removeClass('m-loader m-loader--right m-loader--light').prop("disabled", false);
                $("#invalid_old_password").show().text("Wrong Password").fadeOut(4000);
            }
            else{
                $('.login-welcome-change-password-button').removeClass('m-loader m-loader--right m-loader--light').prop("disabled", false);
                $("#invalid_old_password").show().text(success.responseJSON.success).fadeOut(4000);
            }
        }
    });
});
// login wellcome faq js
$('.m_tabs_3_1_next').click(function () {
 $('#m_tabs_3_1').removeClass('active show');
 $('.m_tabs_3_1').removeClass('active show');
 $('.m_tabs_3_2').addClass('active show');
 $('#m_tabs_3_2').addClass('active show');
});

$('.m_tabs_3_2_next').click(function () {
    $('#m_tabs_3_2').removeClass('active show');
    $('.m_tabs_3_2').removeClass('active show');
    $('.m_tabs_3_3').addClass('active show');
    $('#m_tabs_3_3').addClass('active show');
});


$('.m_tabs_3_3_next').click(function () {
    $('#m_tabs_3_3').removeClass('active show');
    $('.m_tabs_3_3').removeClass('active show');
    $('.m_tabs_3_4').addClass('active show');
    $('#m_tabs_3_4').addClass('active show');
});

$('.m_tabs_3_4_next').click(function () {
    $('#m_tabs_3_4').removeClass('active show');
    $('.m_tabs_3_4').removeClass('active show');
    $('.m_tabs_3_5').addClass('active show');
    $('#m_tabs_3_5').addClass('active show');
});

$('.m_tabs_3_5_next').click(function () {
    $('#m_tabs_3_5').removeClass('active show');
    $('.m_tabs_3_5').removeClass('active show');
    $('.m_tabs_3_6').addClass('active show');
    $('#m_tabs_3_6').addClass('active show');
});
$('.m_tabs_3_6_next').click(function () {
    $('#m_tabs_3_6').removeClass('active show');
    $('.m_tabs_3_6').removeClass('active show');
    $('.m_tabs_3_7').addClass('active show');
    $('#m_tabs_3_7').addClass('active show');
});
$('.m_tabs_3_7_next').click(function () {
    $('#m_tabs_3_7').removeClass('active show');
    $('.m_tabs_3_7').removeClass('active show');
    $('.m_tabs_3_1').addClass('active show');
    $('#m_tabs_3_1').addClass('active show');
});

//for prev
$('.m_tabs_3_1_Prev').click(function () {
    $('#m_tabs_3_2').removeClass('active show');
    $('.m_tabs_3_2').removeClass('active show');
    $('.m_tabs_3_1').addClass('active show');
    $('#m_tabs_3_1').addClass('active show');
});

$('.m_tabs_3_2_Prev').click(function () {
    $('#m_tabs_3_3').removeClass('active show');
    $('.m_tabs_3_3').removeClass('active show');
    $('.m_tabs_3_2').addClass('active show');
    $('#m_tabs_3_2').addClass('active show');
});


$('.m_tabs_3_3_Prev').click(function () {
    $('#m_tabs_3_4').removeClass('active show');
    $('.m_tabs_3_4').removeClass('active show');
    $('.m_tabs_3_3').addClass('active show');
    $('#m_tabs_3_3').addClass('active show');
});

$('.m_tabs_3_4_Prev').click(function () {
    $('#m_tabs_3_5').removeClass('active show');
    $('.m_tabs_3_5').removeClass('active show');
    $('.m_tabs_3_4').addClass('active show');
    $('#m_tabs_3_4').addClass('active show');
});
$('.m_tabs_3_5_Prev').click(function () {
    $('#m_tabs_3_6').removeClass('active show');
    $('.m_tabs_3_6').removeClass('active show');
    $('.m_tabs_3_5').addClass('active show');
    $('#m_tabs_3_5').addClass('active show');
});
$('.m_tabs_3_6_Prev').click(function () {
    $('#m_tabs_3_7').removeClass('active show');
    $('.m_tabs_3_7').removeClass('active show');
    $('.m_tabs_3_6').addClass('active show');
    $('#m_tabs_3_6').addClass('active show');
});

//popover js
$(document).ready(function () {
    $("#welcome_popover").popover('show');

});

// Create old welcome password stars js
function createOldPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-new-welcome-password, .m-confirm-welcome-password").on("paste", function () {
        return false;
    });
    $(".m-old-welcome-password, .m-new-welcome-password, .m-confirm-welcome-password").on("cut", function () {
        return false;
    });
    $(".m-old-welcome-password, .m-new-welcome-password, .m-confirm-welcome-password").on("copy", function () {
        return false;
    });
    $(".m-old-welcome-password").attr("type", "text").removeAttr("name");

    $(".m-old-welcome-password").bind("paste",function(e){
        var data = e.originalEvent.clipboardData.getData('Text')
        $(".hide_old_welcome_password").val(data)
    })

    $("body").on("keypress", ".m-old-welcome-password", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hide_old_welcome_password").val($(".hide_old_welcome_password").val() + character);
        }else if(e.type == "paste"){
            catchPaste(e, this, function(input) {
                $(".hide_old_welcome_password").val(input);
            });
        }
    });
    $("body").on("keyup", ".m-old-welcome-password", function(e) {
        var code = e.which;

        if (code == 8 || $(".m-old-welcome-password").val().length < $(".hide_old_welcome_password").val().length) {
            var length = $(".m-old-welcome-password").val().length;
            $(".hide_old_welcome_password").val($(".hide_old_welcome_password").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.m-old-welcome-password').val().length;
            $(".m-old-welcome-password").val(createOldPasswordWelcomeStars(current_val - 1) + $(".m-old-welcome-password").val().substring(current_val - 1));
        }
        change_password();
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-old-welcome-password").val(createstars($(".m-old-welcome-password").val().length));
        }, 200);
    });
});

// Create confirm welcome  password stars js
function createConfirmPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-confirm-welcome-password").attr("type", "text").removeAttr("name");
    $("body").on("keypress", ".m-confirm-welcome-password", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hide_confirm_welcome_password").val($(".hide_confirm_welcome_password").val() + character);
        }
    });
    $("body").on("keyup", ".m-confirm-welcome-password", function(e) {
        var code = e.which;
        if (code == 8 || $(".m-confirm-welcome-password").val().length < $(".hide_confirm_welcome_password").val().length) {
            var length = $(".m-confirm-welcome-password").val().length;
            $(".hide_confirm_welcome_password").val($(".hide_confirm_welcome_password").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.m-confirm-welcome-password').val().length;
            $(".m-confirm-welcome-password").val(createConfirmPasswordWelcomeStars(current_val - 1) + $(".m-confirm-welcome-password").val().substring(current_val - 1));
        }
        if($(".hide_new_welcome_password").val() == $(".hide_confirm_welcome_password").val()){
            $(".m-confirm-welcome-password").removeClass("m-confirm-welcome-password_empty");
            $(".password_mismatch_error").text('');
        }else{
            $(".m-confirm-welcome-password").addClass("m-confirm-welcome-password_empty");
            $(".password_mismatch_error").text('Password Mismatch');
        }
        change_password();
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-confirm-welcome-password").val(createstars($(".m-confirm-welcome-password").val().length));
        }, 200);
    });
});

// Create New welcome password stars js
function createNewPasswordWelcomeStars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {
    var timer = "";
    $(".m-new-welcome-password").attr("type", "text").removeAttr("name");
    $("body").on("keypress", ".m-new-welcome-password", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hide_new_welcome_password").val($(".hide_new_welcome_password").val() + character);
        }
    });
    $("body").on("keyup", ".m-new-welcome-password", function(e) {
        var code = e.which;

        if (code == 8 || $(".m-new-welcome-password").val().length < $(".hide_new_welcome_password").val().length) {
            var length = $(".m-new-welcome-password").val().length;
            $(".hide_new_welcome_password").val($(".hide_new_welcome_password").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.m-new-welcome-password').val().length;
            $(".m-new-welcome-password").val(createNewPasswordWelcomeStars(current_val - 1) + $(".m-new-welcome-password").val().substring(current_val - 1));
        }
        if(password_validation($(".hide_new_welcome_password").val())){
            $('.m-new-welcome-password').addClass('success_field').removeClass('error_field');
        }else{
            $('.m-new-welcome-password').removeClass('success_field').addClass('error_field');
        }
        change_password();
        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".m-new-welcome-password").val(createstars($(".m-new-welcome-password").val().length));
        }, 200);
    });
});

// welcome password tooltip js
$('.m-new-welcome-password').on('click focus',function (event) {
    $(".m-new-welcome-password").addClass("error_field");
    $('#tooltips').show();
    if($('#tooltips').hasClass('display_none')){
        $('#tooltips').removeClass('display_none')
    }

    event.stopPropagation();
});
$('.m-new-welcome-password').on('blur',function (event) {
    $('#tooltips').hide();
    event.stopPropagation();
    if(password_validation($(".hide_new_welcome_password").val())){
        $('.m-new-welcome-password').addClass('success_field').removeClass('error_field');
    }else{
        $('.m-new-welcome-password').removeClass('success_field').addClass('error_field');
    }
});
$('#tooltips').click(function (event) {
    $(".m-new-welcome-password").addClass("error_field");
    $('#tooltips').show();
    event.stopPropagation();
});
$('body').click(function () {
    $(".m-new-welcome-password").addClass('lightgray-border');
    $('#tooltips').hide();
});


// welcome change password screen valdidation
$(".m-old-welcome-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.m-old-welcome-password').addClass('success_field').removeClass('error_field');
    }
    else
    {
        $('.m-old-welcome-password').removeClass('success_field').addClass('error_field');
    }
});

$(".m-confirm-welcome-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.m-confirm-welcome-password').addClass('success_field').removeClass('error_field');
    }
    else
    {
        $('.m-confirm-welcome-password').removeClass('success_field').addClass('error_field');
    }
});





// Get current date in invoice
var today = new Date();
var day = today.getDate().toString();
var m = today.getMonth()+1;
if(m < 10){
    m='0'.concat(m);
}
var month=m.toString();
var yyyy = today.getFullYear().toString();
$('#m_datepicker_1_modal').val(month.concat('/',day,'/',yyyy));
$('#m_datepicker_2_modal').val(month.concat('/',day,'/',yyyy));

// file upload js in fight chargeback case
// $('#get_file').on('click',function () {
// document.getElementById('get_file').onclick = function() {
// });
// $('#my_file').on('change',function () {
// document.getElementById('my_file').onchange = function () {
//     $('.attatchments').show();
//     $('.filename').html(this.files.item(0).name);
//         $('.filename').html('')
//         for(i=0;i< this.files.length;i++){
//             $('.filename').append('<br>'+(this.files.item(i).name));
//         }
// });
// $('.file-close').click(function () {
//     // $('.attatchments').hide();
//     $('#my_file').val('');
//     $('.close2').remove();
// });
function createstars(n) {
    return new Array(n+1).join("*")
}
function password_validation(value) {
    var upperCase= new RegExp('[A-Z]');
    var lowerCase= new RegExp('[a-z]');
    var numbers = new RegExp('[0-9]');
    var v_digits = false;
    var v_numbers = false;
    var v_upcase = false;
    var v_downcase = false;
    if(value.length >= 8){
        $("#8_characters").removeClass('fa-times').addClass('fa-check');
        $("#8_characters").parent().removeClass('invalid').addClass('valid');
        v_digits = true;
    }else{
        v_digits = false;
        $("#8_characters").addClass('fa-times').removeClass('fa-check');
        $("#8_characters").parent().addClass('invalid').removeClass('valid');
    }
    if(value.match(upperCase)) {
        $("#upcase").removeClass('fa-times').addClass('fa-check');
        $("#upcase").parent().removeClass('invalid').addClass('valid');
        v_upcase = true;
    }else{
        v_upcase = false;
        $("#upcase").addClass('fa-times').removeClass('fa-check');
        $("#upcase").parent().addClass('invalid').removeClass('valid');
    }
    if(value.match(lowerCase)) {
        $("#downcase").removeClass('fa-times').addClass('fa-check');
        $("#downcase").parent().removeClass('invalid').addClass('valid');
        v_downcase = true;
    }else{
        v_downcase = false;
        $("#downcase").addClass('fa-times').removeClass('fa-check');
        $("#downcase").parent().addClass('invalid').removeClass('valid');
    }
    if(value.match(numbers)){
        $("#1_number").removeClass('fa-times').addClass('fa-check');
        $("#1_number").parent().removeClass('invalid').addClass('valid');
        v_numbers = true;
    }else{
        v_numbers = false;
        $("#1_number").addClass('fa-times').removeClass('fa-check');
        $("#1_number").parent().addClass('invalid').removeClass('valid');
    }
    if(v_digits && v_numbers && v_upcase && v_downcase){
        return true;
    }else{
        return false;
    }

}

function change_password() {
    var new_password_value = $(".hide_new_welcome_password").val()
    var new_password_validation = password_validation($(".hide_new_welcome_password").val());
    var confirm_password_value = $(".hide_confirm_welcome_password").val();
    var old_password_value = $(".hide_old_welcome_password").val();
    if (new_password_validation && (new_password_value == confirm_password_value) && old_password_value.length > 5){
        $(".change_password_tos").prop("disabled", false);
    }else{
        $(".change_password_tos").prop("disabled", true);
    }
}
function catchPaste(evt, elem, callback) {
    if (navigator.clipboard && navigator.clipboard.readText) {
        // modern approach with Clipboard API
        navigator.clipboard.readText().then(callback);
    } else if (evt.originalEvent && evt.originalEvent.clipboardData) {
        // OriginalEvent is a property from jQuery, normalizing the event object
        callback(evt.originalEvent.clipboardData.getData('text'));
    } else if (evt.clipboardData) {
        // used in some browsers for clipboardData
        callback(evt.clipboardData.getData('text/plain'));
    } else if (window.clipboardData) {
        // Older clipboardData version for Internet Explorer only
        callback(window.clipboardData.getData('Text'));
    } else {
        // Last resort fallback, using a timer
        setTimeout(function() {
            callback(elem.value)
        }, 100);
    }
}
$(document).on("keyup", "#user_password", function () {
    $("#tooltips").show();
    password_validation($(this).val());
});

$(document).on("click",".refund-yes-btn",function (e) {
    $('.refund-yes-btn').addClass("pointer-none");
});

$(document).on("submit","#refund_submit_form",function (e) {
    $('.Refund-dia-p').addClass("pointer-none-refund");
});

var closeAlert = function (id) {
    $("#" + id).fadeOut(500);
};
var alertBox = function (id) {
    $("#" + id).fadeIn(500, function () {
      setTimeout(function () {
        closeAlert(id);
      }, 5000);
    });

    $(".close-alert").click(function () {
      closeAlert(id);
    });
};