var config = window.config = {};
config.validations = {
    debug: false,
    errorClass:'has-error',
    validClass:'success1',
    errorElement:"span",

    // add error class
    highlight: function(element, errorClass, validClass) {
        $(element).parents("div.form-group")
            .addClass(errorClass)
            .removeClass(validClass);
    },

    // add error class
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents(".has-error")
            .removeClass(errorClass)
            .addClass(validClass);
    },

    // submit handler
    submitHandler: function(form) {
        form.submit();
    }
};


validateTipTransferForm("tip_transfer");


$("#tip_transfer_btn").on("click",function (e) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });
    if ($("#tip_transfer").valid()) {

        var transfer_amount = $("#tip_amount").val();
        var transfer_date = $("#tip_date").val();
        var memo = $("#tip_memo").val();
        var available_amount = $("#available_balance").val();
        var remain_balance = available_amount - transfer_amount;
        if (remain_balance >= 0) {
            remain_balance = formatter.format(remain_balance);
            transfer_amount = formatter.format(transfer_amount);

            $("#tip_transfer_date").text(transfer_date);
            $("#tip_confirm_memo").text(memo);
            $("#tip_total_amount").text(transfer_amount);
            $("#tip_confirm_amount").text(transfer_amount);
            $("#remain_tip_balance").text(remain_balance);
        }else{
            $("#tip_amount-error").text("Insufficient Balance");
            e.preventDefault();
            return false;
        }

    }else{
        e.preventDefault();
        return false;

    }

});

$("#tip_transfer_submit").on("click",function () {
    $("#tip_transfer").submit();
});

function validateTipTransferForm(role) {
    $.validator.addMethod('minStrict', function (value, el, param) {
        return value > param;
    });
    $(function() {
        var formRules = {
            rules: {
                "amount": {
                    required: true,
                    minStrict: 0,
                }
            },
            messages: {
                "amount": {
                    required: "Please Enter Amount",
                    minStrict: "Amount should be greater than 0",
                }
            },

        };
        $.extend(formRules, config.validations);
        $('#' + role).validate(formRules);
    });
};

// $("#tip_close").on("click",function () {
//     $("#tip_transfer").trigger("reset");
//     $("#tip_transfer_btn").attr("disabled",true);
//     $("#tip_amount-error").text("");
// });

$('#tip_amount').on('keypress keyup blur',function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
    var input = $(this).val();
    if ((input.indexOf('.') != -1) && (input.substring(input.indexOf('.')).length > 2)) {
        event.preventDefault();
    }
    $(this).val($(this).val().replace(/(\.[0-9][0-9])([0-9])/, "$1"));

});

$("#tip_transfer_btn").attr("disabled",true);
$("#tip_amount").on("keypress keyup blur",function (event) {
    var input = $(this).val();

    if (input == ""){
        $("#tip_transfer_btn").attr("disabled",true);
    }else{
        $("#tip_transfer_btn").attr("disabled",false);
    }
});
