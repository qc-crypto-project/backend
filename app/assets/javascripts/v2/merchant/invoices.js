//-------------------------------invoice/index.html.erb---------
//= require intlTelInput
$(document).ready(function () {
    $("#daily_batch").daterangepicker({
        buttonClasses: "m-btn btn ",
        applyClass: "btn-primary apply_btn_daily_batch",
        cancelClass: "btn-secondary",
        startDate: a,
        endDate: t,
        ranges: {
            Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
            "Last 30 Days": [moment().subtract(29, "days"), moment()],
            "Last 60 Days": [moment().subtract(59, "days"), moment()],
            "Last 90 Days": [moment().subtract(89, "days"), moment()],
        },
    
    },function(a, t, n) {
        $("#daily_batch .form-control").val(n);
        $.ajax({
            url: '/v2/merchant/invoices/client_details',
            type: 'GET',
            data: {
                first_date: moment(a)._d,
                second_date: moment(t)._d,
                id: $("#hidden_client").val(),
                type: $("#hidden_type").val()
            },
            success: function (data) {
                $("#m_tabs_6_1").empty().append(data);
            }
        });
    });
    validateInvoiceForm('invoice_form');
    $('.payee_checkbox').prop("checked", true);
    delete_product();
    $(".on_select").on("click focusin", function () {
        $(this).select();
    });
    $(".on_select").on('keypress',function(e){
        if ($(this).val().length == 1 && e.which == 48 && $(this).val() == "0"){
            $(this).val('');
        }
    });
    $("#wallet_id").on('change', function () {
        if($(this).find(':selected').data('location-status') == true){
            $("#location_error").html(BLOCKED_LOCATION);
            $(".open_pdf_modal").prop("disabled", true);
        }else{
            $("#location_error").html("");
            $(".open_pdf_modal").prop("disabled", false);
        }
    });
    setTimeout(function () {
        if($('#invoice_memo').length > 0) {
            var left = 500 - $('#invoice_memo').val().length;
            if (left < 0) {
                left = 0;
                e.preventDefault;
            }
        }
        $('.remaining_char').text(left + ' characters remaining');
    }, 100);

    var max = 500;
    $('#invoice_memo')
        .keydown(function(event){
            if (event.keyCode != 8 && event.keyCode != 46 && $(this).val().length >= max) {
                event.preventDefault();
            }
        })
        .keyup(function(){
            var $this = $(this),
                val = $this.val().slice(0, max),
                val_length = val.length,
                left = max - val_length;
            $('.remaining_char').text(left + ' characters remaining');
            $this.val(val);
        });
    $(document).on('click', '.clickable_row', function () {
        window.location.href = $(this).data("path");
    });

    $("#download_pdf_btn").on('click',function () {
        setTimeout(function() {
            $("#send_invoice").prop("disabled", false);
            $("#save_draft").prop("disabled", false);
        }, 2000);
    });
    $(function() {
        // $(".percentage_validation").inputFilter(function (value) {
        //     return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100);
        // });
    });
    $(document).on('keyup', ".percentage_validation",function () {
        value = $(this).val();
        if(parseFloat(value) > 100){
            $(this).val($(this).val().substr(0, 2));
        }
    });

    var today = new Date();
    var day = today.getDate().toString();
    var m = today.getMonth()+1;
    if(m < 10){
        m='0'.concat(m);
    }
    var month=m.toString();
    var yyyy = today.getFullYear().toString();
    if($('#due_date_for_invoices').length > 0 && $('#due_date_for_invoices').val().length == 0){
        $('#due_date_for_invoices').val(month.concat('/',day,'/',yyyy));
    }
    if($('#date_to_send_invoices').length > 0 && $('#date_to_send_invoices').val().length == 0){
        $('#date_to_send_invoices').val(month.concat('/',day,'/',yyyy));
    }

    $('.payment_option_checkboxes').change(function () {
        checked = $("input[type=checkbox]:checked").length;
        if (checked == 0) {
            alert("Please Select atleast one payment method!");
            $("#payment_option_quickcard").prop('checked', true);
        }
    });

    var tax_plus = 0;
    var fee_plus = 0;
    var late_fee_plus = 0;
    var discount_minus = 0;
    var shipping_handling_plus = 0;
    // $("#customer_name").select2({
    //     tags: true
    // });
    // $('#customer_name').on("change", function(e) {
    //     if(!isNaN(this.value)) {
    //         $.ajax({
    //             url: '/v2/get_user',
    //             type: 'GET',
    //             'data-type': "js",
    //             'data': {id: this.value},
    //             'success': function (result) {
    //                 set_customer_info(result.user.zip_code, result.user.phone_number, result.user.state, result.user.email, result.user.address, result.user.country, result.user.city, "true", result.user.id, result.user.name)
    //             },
    //             'error': function (data) {
    //                 set_customer_info("", "", "", "", "", "", "", "", "","")
    //             }
    //         });
    //     }else{
    //         set_customer_info("", "", "", "", "", "", "", "", "", this.value)
    //     }
    // });

    $("#sub_total").prop("readonly", true);
    $("#grand_total").prop("readonly", true);
    $(".product_amount").prop("readonly", true);

    $("#add_another_item").on('click',function () {
        var count = $(this).data("count");
        add_new_item(count,"","","","0.0");
        new_count = parseInt(count) + 1;
        $(this).data("count",new_count)
        delete_product();

    });

    function add_new_item(number,description,qty,unit_price,amount) {
        $('#add_another_item_div').before(
            '  <div class="row main_product_'+number+' more_than_one">\n' +
            '    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">\n' +
            '      <div class="form-group m-form__group">\n' +
            '        <input type="text" value="'+ description +'" id="description_'+ number +'" name="product['+ number +'][description]" autocomplete="new-password" placeholder="Product Name" class="form-control invoice-input m-input" required>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
            '      <div class="form-group m-form__group">\n' +
            '        <div class="input-icon icon_inside_input_field">\n' +
            '           <span>#</span>\n'+
            '        <input type="text" value="'+ qty +'" id="qty_'+ number +'" data-id="'+ number +'" name="product['+ number +'][qty]" autocomplete="new-password" class="form-control invoice-price-input m-input calculate_amount quantity_reg" placeholder="0" required>\n' +
            '       </div>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
            '      <div class="form-group m-form__group">\n' +
            '        <div class="input-icon icon_inside_input_field">\n' +
            '          <span>$</span>\n' +
            '            <input type="text" value="'+ unit_price +'" id="unit_price_'+ number +'" data-id="'+ number +'" autocomplete="new-password" name="product['+ number +'][unit_price]" class="form-control invoice-price-input m-input calculate_amount ez_dollar" placeholder="0.0" required>\n' +
            '        </div>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">\n' +
            '      <div class="form-group m-form__group">\n' +
            '        <div class="input-icon icon_inside_input_field">\n' +
            '          <span>$</span>\n' +
            '            <input type="number" value="'+ amount +'" id="amount_'+ number +'" name="product['+ number +'][amount]" class=" form-control invoice-price-input m-input product_amount" placeholder="$" required>\n' +
            '        </div>\n' +
            '      </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-1">\n' +
            '      <a class="cursor-pointer delete_product" data-id="'+number+'">\n' +
            '        <img class="clear_invoice_btn" src="https://quickard.s3-us-west-2.amazonaws.com/clear_btn.svg">\n' +
            '      </a>' +
            '    </div>\n' +
            '  </div>')
        $(".product_amount").prop("readonly", true);
        $(".quantity_reg").inputmask('Regex', {regex: "^[0-9]+$"});
        $(".ez_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
        $(".valid_percent").inputmask('Regex', {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"});
    }

    $(document).on("click",".delete_product", function () {
        if($("#product_section > div").length > 3){
            var id = $(this).data("id");
            $(".main_product_"+id).remove();
            setting_sub_total();
        }else if(($("#product_section > div").length == 3)){

        }
        delete_product();

    });
    $(document).on('keyup','.calculate_amount',function () {
        var id = $(this).data("id");
        qty = parseInt($("#qty_"+id).val());
        unit_price = parseFloat($("#unit_price_"+id).val());
        if(qty > 0 && unit_price > 0){
            $("#amount_"+id).val((qty * unit_price).toFixed(2));
            setting_sub_total();
        }
    });

    function setting_sub_total() {
        var inputs = $(".product_amount");
        var sub_total = 0;
        for (var i = 0; i < inputs.length; i++) {
            sub_total = parseFloat($(inputs[i]).val()) + sub_total;
        }
        $("#sub_total").val(sub_total.toFixed(2));
        $("#grand_total").val(sub_total.toFixed(2));
        calculation_grand_total('discount')
    }

    $(document).on('change','.dollar_percent_button',function () {
        field = $(this).data("type");
        if($(this).prop("checked") == true){
            $("#"+field+"_method").val('percent');
            $("#"+field+"_sign").text('').text('%');
            $("#"+field).addClass("valid_percent");
            $("#"+field).val("0");
            $("#"+field).removeClass("ez_dollar");
        }else{
            $("#"+field+"_method").val('dollar');
            $("#"+field+"_sign").text('').text('$');
            $("#"+field).removeClass("valid_percent");
            $("#"+field).addClass("ez_dollar");
            $("#"+field).val("0");
        }
        calculation_grand_total(field)
    });

    $(document).on('click','.open_pdf_modal',function () {
        validateInvoiceForm('invoice_form');
        if($("#invoice_form").valid()){
            // preview PDF or preview invoice Button
            if($(this).data('type') == "preview_pdf"){
                $(".buttons_invoice").hide();
            }else{
                $(".buttons_invoice").show();
            }

            // header
            $("#preview_invoice_id").text('').text($("#invoice_id").val());
            $("#preview_name").text('').text($("#name").val());
            $("#preview_merchant_name").text('').text($("#merchant_name").val());
            $("#preview_grand_total").text('').text(parseFloat($("#grand_total").val()).toFixed(2));
            if($(".due_date").val().length > 0){
                $("#preview_due_date").text('').text(moment($(".due_date").val()).format('ll'));
            }else{
                $("#preview_due_date").text('').text(moment(new Date()).format('ll'));
            }

            // checkboxes
            var active_option = $('.payment_option_checkboxes:checkbox:checked').length;
            $(".options_1").hide();
            $(".options_2").hide();
            $(".options_3").hide();
            if(active_option == 1){
                $(".option_1_image").attr("src",$('.payment_option_checkboxes:checkbox:checked').data("src"));
                $("#option_1_text").html($('.payment_option_checkboxes:checkbox:checked').data("title"));
                $(".options_1").show();
                $("#like_to_pay").hide();
            }
            if(active_option == 2){
                index = 1
                $.each($('.payment_option_checkboxes:checkbox:checked'), function(){
                    $('.option_2_' + index + '_image').attr("src",$(this).data("src"));
                    $('#option_2_' + index + '_text').html($(this).data("title"));
                    index = index + 1
                });
                $(".options_2").show();
                $("#like_to_pay").show();
            }
            if(active_option == 3){
                $(".options_3").show();
                $("#like_to_pay").show();
            }

            // products
            $(".new_preview_products").remove();
            $.each($("#product_section > div"), function(index, value) {
                index=index-1
                if($(value).hasClass("main_product_"+index)){
                    if($("#unit_price_"+index).val().length > 0){
                        unit_price = "$ "+parseFloat($("#unit_price_0").val()).toFixed(2);
                    }else{
                        unit_price = "$ 0.0";
                    }
                    if(index == 0){
                        $("#preview_description_0").text('').text($("#description_0").val());
                        $("#preview_qty_0").text('').text($("#qty_0").val());
                        $("#preview_unit_price_0").text('').text(unit_price);
                        $("#preview_amount_0").text('').text("$ "+$("#amount_0").val());
                    }else{
                        new_index = index - 1;
                        $("#unit_price_"+index).val()
                        $("#preview_main_product_"+new_index).after('<div class="row new_preview_products" id="preview_main_product_'+ index +'">\n' +
                            '                  <div class="col-lg-12">\n' +
                            '                    <div class="row">\n' +
                            '                      <div class="col-lg-4">\n' +
                            '                        <div class="pull-left simple-14-normal-text">\n' +
                            '                          <p id="preview_description_'+ index +'">'+ $("#description_"+index).val() +'</p>\n' +
                            '                        </div>\n' +
                            '                      </div>\n' +
                            '                      <div class="col-lg-8">\n' +
                            '                        <div class="row">\n' +
                            '                          <div class="col-lg-1"></div>\n' +
                            '                          <div class="col-lg-3">\n' +
                            '                            <div class="pull-right simple-14-normal-text">\n' +
                            '                              <p id="preview_qty_'+ index +'">'+ $("#qty_"+index).val() +'</p>\n' +
                            '                            </div>\n' +
                            '                          </div>\n' +
                            '                          <div class="col-lg-4">\n' +
                            '                            <div class="pull-right simple-14-normal-text">\n' +
                            '                              <p id="preview_unit_price_'+ index +'">$ '+ $("#unit_price_"+index).val() +'</p>\n' +
                            '                            </div>\n' +
                            '                          </div>\n' +
                            '                          <div class="col-lg-4">\n' +
                            '                            <div class="pull-right simple-14-normal-text">\n' +
                            '                              <p id="preview_amount_'+ index +'">$ '+ $("#amount_"+index).val() +'</p>\n' +
                            '                            </div>\n' +
                            '                          </div>\n' +
                            '                        </div>\n' +
                            '                      </div>\n' +
                            '                    </div>\n' +
                            '                  </div>\n' +
                            '                </div>')

                    }
                }
            });
            var wallet_id = $("#wallet_id").val();
            // address
            $("#p_b_address").text($("#b_street").val());
            $("#p_b_suit").text($("#b_suit").val());
            $("#p_b_city").text($("#b_city").val());
            $("#p_b_zip").text($("#b_zip").val());
            $("#p_b_country").text($("#b_country option:selected").html());
            $("#p_b_state").text($("#b_state option:selected").html());
            $("#p_s_address").text($("#s_street").val());
            $("#p_s_suit").text($("#s_suit").val());
            $("#p_s_city").text($("#s_city").val());
            $("#p_s_zip").text($("#s_zip").val());
            $("#p_s_country").text($("#s_country option:selected").html());
            $("#p_s_state").text($("#s_state option:selected").html());
            $("#p_memo").text($("#invoice_memo").val());

            // amount calculation
            $("#preview_dba_name").text('').text($("#"+ wallet_id +"_name").val());
            $("#preview_dba_email").text('').text($("#"+ wallet_id +"_email").val());
            $("#preview_dba_phone").text('').text($("#"+ wallet_id +"_phone_number").val());
            $("#preview_sub_total").text('').text("$ "+parseFloat($("#sub_total").val()).toFixed(2));
            $("#preview_grand_totall").text('').text("$ "+parseFloat($("#grand_total").val()).toFixed(2));
            $("#preview_discount").text('').text(set_symbol($("#discount_method").val(), "left") + two_decimal($("#discount").val()) + set_symbol($("#discount_method").val(), "right"));
            // $("#preview_fee").text('').text(set_symbol($("#fee_method").val(), "left") + two_decimal($("#fee").val()) + set_symbol($("#fee_method").val(), "right"));
            $("#preview_tax").text('').text(set_symbol($("#tax_method").val(), "left") + two_decimal($("#tax").val()) + set_symbol($("#tax_method").val(), "right"));
            // $("#preview_discount_amount").text('').text("$ " + two_decimal(discount_minus));
            discount_method = $("#discount_method").val();
            if (discount_method == "percent"){
                discount = ($("#discount").val() / 100) * $("#sub_total").val()
                $("#preview_discount_amount").text('').text("$ " + two_decimal(discount));
            }else if (discount_method != "percent"){
                discount = $("#discount").val();
                $("#preview_discount_amount").text('').text("$ " + two_decimal(discount));
            }
            // $("#preview_fee_amount").text('').text("$ " + two_decimal(fee_plus));
            // $("#preview_shipping_amount").text('').text("$ " + two_decimal(shipping_handling_plus));
            shipping_handling_fee = $("#shipping_handling_fee").val();
            $("#preview_shipping_amount").text('').text("$ " + two_decimal(shipping_handling_fee));
            // $("#preview_tax_amount").text('').text("$ " + two_decimal(tax_plus));
            tax_method = $("#tax_method").val()
            if ($("#apply_tax").is(":checked")){
                if (tax_method == "percent"){
                    tax_on_SH = ($("#tax").val() / 100) * $("#shipping_handling_fee").val()
                    taxes = (($("#tax").val() / 100) * $("#sub_total").val()) + tax_on_SH
                    $("#preview_tax_amount").text('').text("$ " + two_decimal(taxes));
                }
            }
            else if ($("#apply_tax").not(":checked")){
                if (tax_method == "percent"){
                    taxes = ($("#tax").val() / 100) * $("#sub_total").val()
                    $("#preview_tax_amount").text('').text("$ " + two_decimal(taxes));
                }
            }
            $("#pdfPreviewModal").modal('toggle');
        }else{
            // $("#invoice_form").validate();
            // validateInvoiceForm('invoice_form');
            $(".form-error").show().html("<b>Please fill all fields!</b>").fadeOut(3000);
            if($(".b_select_state").hasClass('has-danger')){
                error = $("#b_state-error");
                $("#b_state").next().remove();
                $("#b_state").next().after(error);
            }

            if($(".s_select_state").hasClass('has-danger')){
                error = $("#s_state-error");
                $("#s_state").next().remove();
                $("#s_state").next().after(error);
            }
        }
    });

    $(".calculation_grand_total").on('keyup',function () {
        var field = $(this).data("type");
        calculation_grand_total(field);
    });

    $(document).on('click', '#apply_tax', function () {
        calculation_grand_total("");
    });

    function calculation_grand_total(field) {
        total_amount = parseFloat($("#sub_total").val());

        discount_minus = 0;
        if($("#discount").val().length > 0) {
            discount_minus = parseFloat($("#discount").val());
        }
        discount_method = $("#discount_method").val();

        tax_plus = 0;
        if($("#tax").val().length > 0) {
            tax_plus = parseFloat($("#tax").val());
        }
        tax_method = $("#tax_method").val();

        fee_plus = 0;
        // if($("#fee").val().length > 0) {
        //     fee_plus = parseFloat($("#fee").val());
        // }
        fee_method = $("#fee_method").val();

        shipping_handling_plus = 0;
        if($("#shipping_handling_fee").val().length > 0) {
            shipping_handling_plus = parseFloat($("#shipping_handling_fee").val());
        }
        shipping_handling_method = $("#shipping_handling_method").val();

        late_fee_plus = 0;
        if($("#late_fee").length){
            if($("#late_fee").val().length > 0) {
                late_fee_plus = parseFloat($("#late_fee").val());
            }
            late_fee_method = $("#late_fee_method").val();
            if (late_fee_method == "percent" && late_fee_plus > 0) {
                late_fee_plus = parseFloat(total_amount * late_fee_plus / 100);
            }
        }

        if (discount_method == "percent" && discount_minus > 0) {
            discount_minus = parseFloat(total_amount * discount_minus / 100);
        }
        if (tax_method == "percent" && tax_plus > 0) {
            if ($('#apply_tax').is(":checked")) {
                apply_tax_total_amount = total_amount + parseFloat($("#shipping_handling_fee").val());
                tax_plus = parseFloat(apply_tax_total_amount * tax_plus / 100);
            }else{
                tax_plus = parseFloat(total_amount * tax_plus / 100);
            }
        }
        if (fee_method == "percent" && fee_plus > 0) {
            fee_plus = parseFloat(total_amount * fee_plus / 100);
        }
        if (shipping_handling_method == "percent" && shipping_handling_plus > 0) {
            shipping_handling_plus = parseFloat(total_amount * shipping_handling_plus / 100);
        }
        total_discount = parseFloat($('#discount').val());
        sub_total = parseFloat($('#sub_total').val());
        method = $('#discount_method').val();
        if(total_discount > sub_total && method !="percent"){
            alert('Discount should be less than the subtotal')
            $('#discount').val(0)
            setting_sub_total()
        }
        new_grand_total = ((total_amount + tax_plus + fee_plus + late_fee_plus + shipping_handling_plus) - discount_minus ).toFixed(2);
        $("#grand_total").val(new_grand_total);
    }
    $('#approved').prop("checked",true);
    $('#in_progress').prop("checked",true);
    $('#cancel').prop("checked",true);
    $('#past_due').prop("checked",true);
    $('#saved').prop("checked",true);
    $(document).on('change','.payee_checkbox',(function () {
        $('.payee_checkbox').attr("disabled",true);
        // To Checked or Un-checked all checked box
        if($(this).attr("id") == "all") {
            if ($('#all').is(":checked")) {
                $('.payee_checkbox').prop("checked", true)
            } else {
                $('.payee_checkbox').prop("checked", false)
            }
        }else{
            // $(this).prop("checked", true)
            $('#all').prop("checked", false)
        }
        $(".btn-status").trigger('click');
        filter_invoices("filters");
    }));
    var globalTimeout = null;
    $(document).on('change keyup','#srch-term',(function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function () {
            globalTimeout = null;
            filter_invoices("");
        }, 2500);
    }));

    function filter_invoices(from){
        if ($('#all').is(":checked")) {
            var all = $('#all').val();
            $('#approved').prop("checked",true);
            $('#in_progress').prop("checked",true);
            $('#cancel').prop("checked",true);
            $('#past_due').prop("checked",true);
            $('#saved').prop("checked",true);
        }
        var query = [];
        if ($('#approved').is(":checked")) {
            query.push("approved");
        }
        if ($('#in_progress').is(":checked")) {
            query.push("in_progress");
        }
        if ($('#cancel').is(":checked")) {
            query.push("cancel");
        }
        if ($('#past_due').is(":checked")) {
            query.push("past_due");
        }
        if ($('#saved').is(":checked")) {
            query.push("saved");
        }
        if($('.payee_checkbox input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
        {
            query.push("no_select");
        }

        var filter = $("#filter").val();
        $("#query").val(query);
        search_value = $('#srch-term').val();
        search_value = search_value.trim()
        var filter = $("#filter").val();
        $('#q').val(search_value);

        $.ajax({
            url: "/v2/merchant/invoices/payee",
            type: "GET",
            data: {all: all, checkbox: query, filter: filter, from: from, search: search_value, search_only: "true" },
            success: function (data) {
                $(".payee_datatable").empty().html(data);
                if (from == "filters"){
                    $(".btn-status").trigger('click');
                    $('.payee_checkbox').removeAttr("disable",true);
                }
                $("#m_table_1").DataTable();
                if ( all == "on") {
                    $('#all').prop('checked', true);
                }else{
                    $('#all').prop('checked', false);
                }
                $.each(query, function( index, value ) {
                    $("#"+value).prop('checked', true);
                });
            }
        });
    }

    $(document).on('change keyup','#client_search',(function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function () {
            globalTimeout = null;
            var filter = $("#filter").val();
            var search_value = $('#client_search').val();
            search_value = search_value.trim()
                $.ajax({
                    url: "/v2/merchant/invoices/clients",
                    type: "GET",
                    data: {filter: filter, search: search_value, search_only: "true"},
                    success: function (data) {
                        $(".client_datatable").empty().append(data);
                        $("#m_table_1").DataTable();
                    }
                });
        }, 1000);

    }));


    $("#edit_invoice_number").on('click',function () {
        $(".invoice_number_label").animate({top: '0px'});
        $("#edit_invoice_number").fadeOut("slow");
        $("#invoice_id").css('margin-top','24px').fadeIn("slow");
    });

    function set_symbol(symbol, position) {

        if(symbol == "percent" && position == "right"){
            return "%"
        }else if(symbol == "dollar" && position == "left"){
            return "$"
        }else{
            return ""
        }
    }

    function two_decimal(amount) {
        if(jQuery.type( amount ) === "string"){
            if(amount.length > 0){
                return parseFloat   (amount).toFixed(2);
            }else{
                return "0.0"
            }
        }else {
            return amount.toFixed(2);
        }
    }

    // pay with quickcard
    $("#pay_with_quickcard").attr("disabled",true);
    $('#from_wallet').on('change', function () {
        wallet_balance=$(this).find(':selected').data('balance');
        balance=$(this).find(':selected').data('amount');
        status = $("#one_wallet").attr("data-lstatus");
        balance_validation(status, balance, wallet_balance, $(this).val());
        total_amount = parseFloat($("#total_amount").val());
        rem_balance = (balance - total_amount).toFixed(2);
        var num2 = rem_balance.toString().split('.');
        var thousands = num2[0].split('').reverse().join('').match(/.{1,3}/g).join(',');
        $("#rem_balance").text("$"+ thousands.split('').reverse().join('') +"."+ num2[1]);
        // if(value.length > 0){
        //     $("#pay_with_quickcard").attr("disabled",false);
        // }else{
        //     $("#pay_with_quickcard").attr("disabled",true);
        // }
    });

    $("#pay_with_quickcard").on("click", function () {
        $("#trans_from").text($("#from_wallet option:selected").html());
        $("#trans_memo").text($("#invoice_memo").val());
        $("#avail_balance").text("$"+ $("#from_wallet").find(':selected').data('balance'));
        balance = parseFloat($("#from_wallet").find(':selected').data('amount'));
        total_amount = parseFloat($("#total_amount").val());
        rem_balance = (balance - total_amount).toFixed(2);
        var num2 = rem_balance.toString().split('.');
        var thousands = num2[0].split('').reverse().join('').match(/.{1,3}/g).join(',');
        $("#remain_balance").text("$"+ thousands.split('').reverse().join('') +"."+ num2[1]);
        if ($("#from_wallet").val().length > 0){
            $("#submit_payment").attr("disabled",false);
        }else{
            $("#submit_payment").attr("disabled",true);
        }
        $("#PayCardConfirm").modal('toggle');
        if (balance < total_amount){
            $("#submit_payment").prop("disabled", true);
        }
    });

    $("#submit_payment").on("click", function () {
        randomize(30);
        setTimeout(randomize(45), 200);
        $("#PayCardConfirm").modal('toggle');
        $("#PaySuccessModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        $("#submit_payment").prop("disabled",true);
        invoice_id = $("#invoice_id").val();
        wallet_id = $("#from_wallet").val();
        $.ajax({
            url: "/v2/merchant/invoices/update_invoice_status",
            type: "POST",
            data: {status: "approved", invoice_id: invoice_id, from_wallet: wallet_id},
            success: function (data) {
                randomize(80);
                setTimeout(randomize(100), 400);
                setTimeout(function(){
                    $('.working').hide();
                    $('.first--text').hide();
                    $('.last--text').show();
                    $('.success--image').show();
                }, 1000);
            },
            error: function (data) {
                setTimeout(function(){
                    $("#PaySuccessModal").modal('toggle');
                    $("#PayErrorModal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $(".trans_error").text(data.responseJSON.message);
                }, 1000);
            },
        });
    })

    // client detail chargeback search
    $(document).on('change','.chargeback_checkbox',(function () {

        filter_chargeback("filters");
    }));
    var globalTimeout = null;
    $(document).on('change keyup','#chargeback_search',(function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function () {
            globalTimeout = null;
            filter_chargeback("");
        }, 1500);
    }));

    function filter_chargeback(from){
        if ($('#all').is(":checked")) {
            var all = $('#all').val();
        }
        var query = [];
        if ($('#sent_pending').is(":checked")) {
            query.push("0");
        }
        if ($('#won_reversed').is(":checked")) {
            query.push("1");
        }
        if ($('#lost').is(":checked")) {
            query.push("2");
        }
        if ($('#dispute_accepted').is(":checked")) {
            query.push("3");
        }
        if ($('#under_review').is(":checked")) {
            query.push("4");
        }
        var filter = $("#filter").val();
        $("#checkboxes").val(query);
        search_value = $('#chargeback_search').val();
        var filter = $("#filter").val();
        var id = $("#client_id").val();
        $('#search_val').val(search_value);

        $.ajax({
            url: "/v2/merchant/invoices/client_details",
            type: "GET",
            data: {all: all, checkbox: query, filter: filter, from: from, search: search_value, id: id, search_only: "true" },
            success: function (data) {
                $(".chargeback_datatable").empty().append(data);
                if ( all == "on") {
                    $('#all').prop('checked', true);
                }else{
                    $('#all').prop('checked', false);
                }
                $.each(query, function( index, value ) {
                    if ( value == "0") {
                        $('#sent_pending').prop('checked', true);
                    }
                    if (value == "1" ) {
                        $('#won_reversed').prop('checked', true);
                    }
                    if (value == "2" ) {
                        $('#lost').prop('checked', true);
                    }
                    if (value == "3" ) {
                        $('#dispute_accepted').prop('checked', true);
                    }
                    if (value == "4") {
                        $('#under_review').prop('checked', true);
                    }
                });
            }
        });
    }

    $(".quantity_reg").inputmask('Regex', {regex: "^[0-9]+$"});
    $(".ez_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $(document).on("keyup", ".valid_percent", function () {
        $(".valid_percent").inputmask('Regex', {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"});
    });
    $(document).on("keyup", ".ez_dollar", function () {
        $(".ez_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    });
});

function set_customer_info( phone_number, email, existing_user, verified_user_id, name) {

    $('.intl-tel-input').remove();
    $('#phone_number').remove();
    $('#phone_parent').append('<input type="text" class="form-control invoice-input m-input quantity_reg" id="phone_number" name="phone_number" value="+'+ phone_number +'" required>');

    $('#email').val(email);
    $("#existing_user").val(existing_user);
    $('#verified_user_id').val(verified_user_id);
    $('#name').val(name);

    $("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#phone_code').val(Object.values(selectedCountryData)[2]);
            space_remove(Object.values(selectedCountryData)[2]);
            if(selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        // utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
}
function balance_validation(status, balance, wallet_balance, value) {
    if(status == "true"){
        $("#error_message").text('Account is blocked!');
        $("#pay_with_quickcard").attr("disabled",true);
    }else{
        $("#error_message").text('');
        $("#pay_with_quickcard").attr("disabled",false);
    }
    total_amount = parseFloat($("#total_amount").text());
    $("#balance").text(wallet_balance);
    if(parseFloat(balance) < total_amount){
        $("#error_message").text('Not enough funds!');
        $("#pay_with_quickcard").attr("disabled",true);
    }else{
        $("#error_message").text('');
        $("#pay_with_quickcard").attr("disabled",false);
    }
}
var search_with = null;
$(document).on('keyup','#customer_name',function () {
    search_with = "customer_name";
    var search = $("#customer_name").val();
    $("#name").val(search);
    if(search.length >= 3){
        getUsers(search)
    }
});
$(document).on('change click blur paste keyup','#phone_number',function () {
    search_with = "phone_number";
    var phone = $("#phone_number").val();
    var phone_code = $(".selected-dial-code").text();
    var  code = phone_code.slice(1)
    var search = code + phone;
    if(search.length > 5){
        getUsers(search)
    }else if(search.length == code.length){
        // $('#phone-error').text('');
        $("#verified_user_id").val('');
        $("#existing_user").val('');
    }else{
        // $('#phone-error').text('Invalid Phone Number');
        $("#verified_user_id").val('');
        $("#existing_user").val('');
    }
});
$(document).on('change click blur paste keyup','#phone_number',function () {
    $('#phone-error').html("");
    var phone = $("#phone_number").val();
    var phone_code = $(".selected-dial-code").text();
    var  code = phone_code.slice(1)
    var search = code + phone
    if(search.length > 5) {
        $.ajax({
            url: "/v2/merchant/sales/check_phone_number",
            type: "GET",
            data: {phone_number: search},
            success: function (data) {
                if (data.valid == true){
                    $('#phone-error').html("");
                    $(".open_pdf_modal").removeClass('pointer--none');
                }else{
                    $('#phone-error').html("Invalid Phone Number");
                    $(".open_pdf_modal").addClass('pointer--none');

                }

            }
        })
    }
});

function getUsers(search){
    $("#"+search_with).catcomplete({
        minLength: 3,
        maxShowItems: 15,
        source: function (request, response) {
            $.ajax({
                url: "/v2/merchant/invoices/customer_search",
                type: "GET",
                data: {query: search, type: "invoices", search_with: search_with},
                success: function (result) {
                    if (result.success == "notverified") {
                        // $('#phone-error').text() == ""
                        // $('#phone-error').html('').html(result.message);
                        $('#existing_user').val('');
                        $('#verified_user_id').val('');
                    } else {
                        if (result.users.length > 0){
                            response(result.users);
                        }
                        // $('#phone-error').text('')
                    }
                },
                error: function (data) {
                    // set_customer_info("", "", "", "", "", "", "", "", "",search)
                }
            });
        },
        select: function(e, ui) {
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}
$( function() {
    $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
            this._super();
            this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        },
        _renderMenu: function( ul, items ) {
            var that = this;
            // $(".searched_dropdown").empty().append(ul);
            // ul.addClass('add_class')
            // if(ul.hasClass('ui-menu')){}else{
            //     ul.addClass('ui-menu');
            // }
            ul.parent().css('position','relative');
            $.each( items, function( index, item ) {
                if(search_with == "phone_number"){
                    ul.append( "<li class='ui-autocomplete-category' data-id='"+ item.id +"' data-email='"+ item.email +"' data-phone='"+ item.phone_number +"' data-name='"+ item.name +"'>" + item.phone_number + "</li>" );
                }else{
                    ul.append( "<li class='ui-autocomplete-category' data-id='"+ item.id +"' data-email='"+ item.email +"' data-phone='"+ item.phone_number +"' data-name='"+ item.name +"'>" + item.name + "  (" + item.phone_number + ")</li>" );
                }
            });
        }
    });
} );

$(document).on('click',".ui-autocomplete-category", function () {
    set_customer_info($(this).data("phone"), $(this).data("email"), "true", $(this).data("id"), $(this).data("name"));
    $.ajax({
        url: "/v2/merchant/invoices/customer_address",
        type: "GET",
        data: {id: $(this).data("id"), type: "invoices"},
        success: function (data) {
            if(data.billing != null){
                $('#b_suit').val(data.billing.suit);
                $('#temp_b_state').val(data.billing.state);
                $('#b_country').selectpicker('val', data.billing.country);
                $('#b_street').val(data.billing.street);
                $('#b_zip').val(data.billing.zip);
                $('#b_city').val(data.billing.city);
            }
            if(data.shipping != null) {
                $('#temp_s_state').val(data.shipping.state);
                $('#s_country').selectpicker('val', data.shipping.country);
                $('#s_suit').val(data.shipping.suit);
                $('#s_street').val(data.shipping.street);
                $('#s_zip').val(data.shipping.zip);
                $('#s_city').val(data.shipping.city);
            }
            // checkbox checked same address
            if( (data.billing.suit.toLowerCase() == data.shipping.suit.toLowerCase()) && (data.billing.state.toLowerCase() == data.shipping.state.toLowerCase()) && (data.billing.country.toLowerCase() == data.shipping.country.toLowerCase()) && (data.billing.street.toLowerCase() == data.shipping.street.toLowerCase()) && (data.billing.zip.toLowerCase() == data.shipping.zip.toLowerCase()) && (data.billing.city.toLowerCase() == data.shipping.city.toLowerCase()) ){
                $("#same_as_billing").prop("checked", true);
            }
            // $('#s_state').selectpicker('val', data.shipping.state);
            // $('#b_state').selectpicker('val', data.billing.state);
            // $(".address_section").empty().append(data);
        },
        error: function (data) {
        }
    });

    $("#customer_name").val($(this).data("name"));
    $("#customer_name").prop('readonly',false);
    $(this).parent().hide();
    remove_errors();
});

var datepicker = $.fn.datepicker.noConflict(); // return $.fn.datepicker to previously assigned value
$.fn.bootstrapDP = datepicker;

$("#due_date_for_invoices").bootstrapDP( {
        startDate: new Date(),
        autoclose: true,
        todayHighlight:!0,
        orientation:"bottom left",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
);

$('.due--date').click('click' ,function () {
    setTimeout(function(){ $('#due_date_for_invoices').trigger("focus");}, 100);

});
$("#date_to_send_invoices").bootstrapDP( {
        startDate: new Date(),
        todayHighlight:!0,
        autoclose: true,
        orientation:"bottom left",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
);
$("#date_to_send_invoices").on('change', function () {
    $("#due_date_for_invoices").bootstrapDP('destroy');
    $("#due_date_for_invoices").bootstrapDP( {
            startDate: new Date($("#date_to_send_invoices").val()),
            autoclose: true,
            todayHighlight:!0,
            orientation:"bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'
            }
        }
    );
    $("#due_date_for_invoices").val($("#date_to_send_invoices").val());
    if($("#date_to_send_invoices-error").length > 0 && $("#date_to_send_invoices").val() != ""){
        $("#date_to_send_invoices-error").remove();
    }
});
$("#due_date_for_invoices").on('change', function () {
    if($("#due_date_for_invoices-error").length > 0 && $("#due_date_for_invoices").val() != ""){
        $("#due_date_for_invoices-error").remove();
    }
});
$('.send--date').click('click' ,function () {
    setTimeout(function(){ $('#date_to_send_invoices').trigger("focus");}, 100);
});

// $(document).on('change',"#b_country", function () {
//     if($(this).val() != ""){
//         $.ajax({
//             url: '/v2/merchant/sales/country_data',
//             data: {country: $(this).val()},
//             method: 'GET',
//             success: function (response) {
//                 $("#state").empty();
//                 var states = response["states"];
//                 $.each(states, function(key,value) {
//                     $("#state").append('<option value=' + key + '>' + value + '</option>');
//                 });
//                 $('#state').selectpicker('refresh');
//             }
//         });
//     }
//
// });
$(document).on('change',"#s_country", function () {
    if($(this).val() != ""){
        if($(".s_select_country").hasClass('has-danger') && $(this).val() != ""){
            $(".s_select_country").removeClass('has-danger');
            $("#s_country-error").remove();
        }
        $.ajax({
            url: '/v2/merchant/sales/country_data',
            data: {country: $(this).val()},
            method: 'GET',
            success: function (response) {
                $("#s_state").empty();
                var states = response["states"];
                if(!$.isEmptyObject(states)) {
                    $.each(states, function (key, value) {
                        $("#s_state").append('<option value=' + key + '>' + value + '</option>');
                    });
                    $('#s_state').prop('disabled', false);
                }else{
                    $('#s_state').prop('disabled', true);
                }
                var statesval = $("#temp_s_state").val();
                if(statesval.length > 0){
                    $('#s_state').val(statesval);
                    $("#temp_s_state").val('');
                }
                $('#s_state').selectpicker('refresh');
            }
        });
    }else{
        $('#s_state').prop('disabled', true);
        $('#s_state').selectpicker('refresh');
    }
});
$(document).on('change',"#b_country", function () {
    if($(this).val() != ""){
        if($(".b_select_country").hasClass('has-danger') && $(this).val() != ""){
            $(".b_select_country").removeClass('has-danger');
            $("#b_country-error").remove();
        }
        $.ajax({
            url: '/v2/merchant/sales/country_data',
            data: {country: $(this).val()},
            method: 'GET',
            success: function (response) {
                $("#b_state").empty();
                var states = response["states"];
                if(!$.isEmptyObject(states)){
                    $.each(states, function(key,value) {
                        $("#b_state").append('<option value=' + key + '>' + value + '</option>');
                    });
                    $('#b_state').prop('disabled',false);
                }else{
                    $('#b_state').prop('disabled',true);
                }
                var bstatesval = $("#temp_b_state").val();
                if(bstatesval.length > 0){
                    $('#b_state').val(bstatesval);
                    $("#temp_b_state").val('');
                }
                $('#b_state').selectpicker('refresh');
            }
        });
    }else{
        $('#b_state').prop('disabled', true);
        $('#b_state').selectpicker('refresh');
    }
});
$(document).on('change',"#b_state", function () {
    if($(".b_select_state").hasClass('has-danger') && $(this).val() != ""){
        $(".b_select_state").removeClass('has-danger');
        $("#b_state-error").remove();
    }
});
$(document).on('change',"#s_state", function () {
    if($(".s_select_state").hasClass('has-danger') && $(this).val() != ""){
        $(".s_select_state").removeClass('has-danger');
        $("#s_state-error").remove();
    }
});

function remove_errors() {
    if($("#b_street").val() != ""){
        if($("#b_street").next().hasClass("form-control-feedback")){
            $("#b_street").next().remove();
        }
        if($("#b_street").parent().hasClass("has-danger")){
            $("#b_street").parent().removeClass('has-danger');
        }
    }
    if($("#b_suit").val() != ""){
        if($("#b_suit").next().hasClass("form-control-feedback")){
            $("#b_suit").next().remove();
        }
        if($("#b_suit").parent().hasClass("has-danger")){
            $("#b_suit").parent().removeClass('has-danger');
        }
    }
    if($("#b_city").val() != ""){
        if($("#b_city").next().hasClass("form-control-feedback")){
            $("#b_city").next().remove();
        }
        if($("#b_city").parent().hasClass("has-danger")){
            $("#b_city").parent().removeClass('has-danger');
        }
    }
    if($("#b_zip").val() != ""){
        if($("#b_zip").next().hasClass("form-control-feedback")){
            $("#b_zip").next().remove();
        }
        if($("#b_zip").parent().hasClass("has-danger")){
            $("#b_zip").parent().removeClass('has-danger');
        }
    }

    if($("#s_street").val() != ""){
        if($("#s_street").next().hasClass("form-control-feedback")){
            $("#s_street").next().remove();
        }
        if($("#s_street").parent().hasClass("has-danger")){
            $("#s_street").parent().removeClass('has-danger');
        }
    }
    if($("#s_suit").val() != ""){
        if($("#s_suit").next().hasClass("form-control-feedback")){
            $("#s_suit").next().remove();
        }
        if($("#s_suit").parent().hasClass("has-danger")){
            $("#s_suit").parent().removeClass('has-danger');
        }
    }
    if($("#s_city").val() != ""){
        if($("#s_city").next().hasClass("form-control-feedback")){
            $("#s_city").next().remove();
        }
        if($("#s_city").parent().hasClass("has-danger")){
            $("#s_city").parent().removeClass('has-danger');
        }
    }
    if($("#s_zip").val() != ""){
        if($("#s_zip").next().hasClass("form-control-feedback")){
            $("#s_zip").next().remove();
        }
        if($("#s_zip").parent().hasClass("has-danger")){
            $("#s_zip").parent().removeClass('has-danger');
        }
    }

    if($("#email").val() != ""){
        if($("#email").next().hasClass("form-control-feedback")){
            $("#email").next().remove();
        }
        if($("#email").parent().hasClass("has-danger")){
            $("#email").parent().removeClass('has-danger');
        }
    }
    if($("#phone_number").val() != ""){
        if($("#phone_number").next().hasClass("form-control-feedback")){
            $("#phone_number").next().remove();
        }
        if($("#phone_number").parent().hasClass("has-danger")){
            $("#phone_number").parent().removeClass('has-danger');
        }
    }
}
$("body").delegate(".cancel_invoice","click",function(){
    id = $(this).data("id");
    number = $(this).data("number");
    $("#inv_id").text(number);
    // $("a.cancel_inv").attr("href","/v2/merchant/invoices/cancel?id="+id);
    $("a.cancel_inv").attr("href","/v2/merchant/invoices/cancel?id="+id+"&filter="+$('.filter-option-inner').text());
    $("#cancelInvoiceModal").modal("show")
});

$("body").delegate(".cancel_invoice_in_progress","click",function(){
    id = $(this).data("id");
    number = $(this).data("number");
    $("#inv_id").text(number);
    $("a.cancel_inv").attr("href","/v2/merchant/invoices/cancel?id="+id);
    $("#cancelInvoiceModal").modal("show")
});


$("body").delegate(".cancel_invoices","click",function(){
    $("a.cancel_invs").attr("href","/v2/merchant/invoices/payee");
    $("#cancelInvoiceModal").modal("show")
});

function delete_product() {
    if($(".delete_product").length == 1){
        $(".delete_product").hide();
    }else if($(".delete_product").length > 1){
        $(".delete_product").show();
    }

}

$(document).ready(function () {

    $("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            // $('#phone_code').val(Object.values(selectedCountryData)[2]);
            if(selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        // utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
});
$(document).on('submit',"#invoice_form", function () {
    var phone_code = $("#phone_number").intlTelInput("getSelectedCountryData").dialCode;
    $('#phone_code').val(phone_code);
});
$(document).on('click',"#same_as_billing", function () {
    if ($('#same_as_billing').is(":checked")) {
        $('#s_street').val($('#b_street').val());
        $('#s_suit').val($('#b_suit').val());
        $('#s_city').val($('#b_city').val());
        $('#s_zip').val($('#b_zip').val());
        $('#s_country').val($('#b_country').val());
        $('#s_country').selectpicker('refresh');
        if($('#b_state').html() != ""){
            $('#s_state').html($('#b_state').html());
            $('#s_state').val($('#b_state').val()).prop('disabled',false);
            $('#s_state').selectpicker('refresh');
        }
    }else{
        $('#s_street').val('');
        $('#s_suit').val('');
        $('#s_city').val('');
        $('#s_zip').val('');
        $('#s_country').val('');
        $('#s_country').selectpicker('refresh');
        $('#s_state').html('').val('').prop('disabled',true);
        $('#s_state').selectpicker('refresh');
    }
});


function validateInvoiceForm(role) {
    $.validator.addMethod("email",function(t,e){return!!/^([a-zA-Z0-9_+\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(t)},"Please enter a valid Email.")
    $(function() {
        var formRules = {
            rules: {
                "customer_name": {
                    required: true
                },
                "phone_number": {
                    required: true
                },
                "email": {
                    required: true,
                    email: true
                },
                "billing[street]": {
                    required: true
                },
                "billing[city]": {
                    required: true
                },
                "billing[country]": {
                    required: true
                },
                "billing[state]": {
                    required: true
                },
                "billing[zip]": {
                    required: true
                },
                "shipping[street]": {
                    required: true
                },
                "shipping[city]": {
                    required: true
                },
                "shipping[country]": {
                    required: true
                },
                "shipping[state]": {
                    required: true
                },
                "shipping[zip]": {
                    required: true
                },
                "wallet_id": {
                    required: true
                },
                "memo": {
                    required: true
                },
            },
            messages: {
                "customer_name": {
                    required: "Please enter Customer Name"
                },
                "phone_number": {
                    required: "Please enter Customer Phone Number"
                },
                "email": {
                    required: "Please enter Customer Email"
                },
                "billing[street]": {
                    required: "Please enter address"
                },
                "billing[city]": {
                    required: "Please enter city"
                },
                "billing[country]": {
                    required: "Please select country"
                },
                "billing[state]": {
                    required: "Please select state"
                },
                "billing[zip]": {
                    required: "Please enter zip code"
                },
                "shipping[street]": {
                    required: "Please enter address"
                },
                "shipping[city]": {
                    required: "Please enter city"
                },
                "shipping[country]": {
                    required: "Please select country"
                },
                "shipping[state]": {
                    required: "Please select state"
                },
                "shipping[zip]": {
                    required: "Please enter zip code"
                },
                "wallet_id": {
                    required: "Please select an Account"
                },
                "memo": {
                    required: "Please enter memo"
                },
            },
        };
        // $.extend(formRules, config.validations);
        $('#invoice_form').validate(formRules);
    });
};

$(document).on("change", "#wallet_id", function () {
    if($(this).val() != ""){
        $("#wallet_id-error").hide();
        $(this).parent().prev().addClass("color-black")
    }else{
        $(this).parent().prev().removeClass("color-black")
    }
})
// same adddress billing checkbox true
$(document).on("change keyup"," #temp_s_state, #s_country, #s_suit, #s_street, #s_zip, #s_city,#temp_b_state, #b_country, #b_suit, #b_street, #b_zip, #b_city",function () {
    value_checked();
})
$(document).ready(function () {
    value_checked();
})
function value_checked(){
    if (($('#s_suit').val().toLowerCase() == $('#b_suit').val().toLowerCase()) && ($('#s_street').val().toLowerCase() == $('#b_street').val().toLowerCase()) && ($('#s_zip').val().toLowerCase() == $('#b_zip').val().toLowerCase()) && ($('#s_city').val().toLowerCase() == $('#b_city').val().toLowerCase())) {
        $("#same_as_billing").prop("checked", true);
    } else {
        $("#same_as_billing").prop("checked", false);
    }
}
