$("#export_button").on("click",function () {
    start_date=$('#date_start').val();
    end_date=$('#date_end').val();
    date = start_date +" - "+ end_date;
    var type = $("#type").val();
    var sub_type = $(this).data("subtype");
    var wallet_id=$(this).data("wallet_id");
    var batch_id = $(this).data("batch-id");
    controller_call(date, type, sub_type, wallet_id,batch_id,'', '');
});

$("#batch_export").on("click", function(){
    var start_date = $(this).data("date");
    var batch_date = start_date
    var type = $("#type").val();
    var sub_type = $(this).data("sub_type");
    var wallet_id = $(this).data("wallet_id");
    var batch_id = $(this).data("batch_id");
    controller_call("", type, sub_type, wallet_id,batch_id, '', batch_date);
})

$("#search_export").on("click",function () {
    start_date=$('#date_start').val();
    end_date=$('#date_end').val();
    date = start_date +" - "+ end_date;
    var type = $(this).data("type");
    var sub_type=$(this).data("subtype");
    var wallet_id=$(this).data("wallet_id");
    var batch_id = $(this).data("batch-id");
    if($(this).data('search-date') != "" && $(this).data('search-date')!=undefined){
        date = $(this).data('search-date');
    }else{
        date = date;
    }
    var search_query = {
        name:  $(this).data('search-name'),
        email: $(this).data('search-email'),
        phone: $(this).data('search-phone'),
        amount: $(this).data('search-amount'),
        // date: $(this).data('search-date'),
        last4: $(this).data('search-last4'),
        type: $(this).data('search-type'),
        time1: $(this).data('search-time1'),
        time2: $(this).data('search-time2'),
        first6: $(this).data('search-first6'),
        date: date,
        transaction_id: $(this).data('search-transaction-id'),
        batch_id: $(this).data('batch-id'),
        status:  $('#checkboxes').val()
    };
    controller_call(null, type, sub_type, wallet_id,batch_id, search_query);
});
function controller_call(date, type, sub_type, wallet_id,batch_id, search_query, batch_date) {
    var offset = moment().local().format('Z');
    if($('#export_button').hasClass('loader_exp')){
        $('#export_button').addClass('m-loader');
        $('#export_button').addClass('m-loader--right');
        $('#export_button').addClass('pointer--none');
    }
    $.ajax({
        url: "/v2/generate_export_file",
        dataType: "json",
        data: {date: date, batch_date: batch_date, type: type, offset: offset,trans: sub_type,wallet_id: wallet_id,batch_id: batch_id, search_query: search_query},
        success: function (data) {
            if($('#export_button').hasClass('loader_exp')){
                $('#export_button').removeClass('m-loader');
                $('#export_button').removeClass('m-loader--right');
                $('#export_button').removeClass('pointer--none');
            }
            if(data.status == "pending"){
                $("#processing_export").hide();
                $("#pending_export").removeClass('hide_image');
            }else{
                $("#processing_export").show();
                if (!$("#pending_export").hasClass('hide_image')){
                    $("#pending_export").addClass('hide_image');
                }
            }
            $('#exportProcessingModal').modal('toggle');
        }
    })
}