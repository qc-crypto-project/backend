$(document).on("change keyup",".two_decimal",function (e) {
    $(function() {
        $(".two_decimal").on('input', function() {
            this.value = this.value.match(/\d{0,12}(\.\d{0,2})?/)[0];
        });
    });
});
$(document).on("click","#GiftBuybtn",function () {
    $('#giftcard-detail').remove();
    $("#loaderhehe1").css("display","block");
    $('#giftCard').hide();
    $('#giftBuy').show();

    var path = $(this).data("path");

    $.ajax({
        url: path,
        type: 'GET',
        success: function (data) {
            $("#loaderhehe1").css("display","none");
            $("#giftBuy").append(data);
             // if ($('#wallet_id > option').length == 1){
             //     var wallet_id = $('#wallet_id > option').val()
                 var wallet_id = $('#wallet_id').val()
                 // $.ajax({
                 //     url: "/v2/merchant/accounts/get_giftcard_data",
                 //     type: "GET",
                 //     data: {wallet_id: wallet_id,type: "giftcard"},
                 //     success: function(data){
                 //         $("#giftcard_fee").val(data.fee_dollar);
                 //         $("#account_balance").val(data.balance.toFixed(2));
                 //
                 //         if(data.location_block == true || data.location_block_giftcard == true){
                 //             $("#gc_amount").attr("disabled",true);
                 //             $("#gift_email").attr("disabled",true);
                 //             $("#giftConfirmbtn").attr("disabled",true);
                 //             if(data.location_block == true ){
                 //                 $("#giftcardmessage .account-information").css("display", "block").html("").html('Account is Blocked');
                 //             }else{
                 //                 $("#giftcardmessage .account-information").css("display","block").html("").html('Giftcard is blocked for this Account');
                 //             }
                 //         }else if(data.location_block == false && data.location_block_giftcard == false){
                 //             $("#from_available_amount_gc").text(data.balance);
                 //             $("#gc_amount").attr("disabled",false);
                 //             $("#gift_email").attr("disabled",false);
                 //             $("#giftConfirmbtn").attr("disabled",false);
                 //             $("#giftcardmessage").hide();
                 //             if ($("#gc_amount").val() != ""){
                 //                 amount=parseFloat($("#gc_amount").val());
                 //                 if($('#giftcard_fee').val().length > 0 ){
                 //                     fee_dollar = parseFloat($('#giftcard_fee').val());
                 //                     total_amount = amount + fee_dollar;
                 //                     $("#amount_gc").text('').append('$'+ amount.toFixed(2));
                 //                     $("#fee_gc").text('').append('$'+ fee_dollar.toFixed(2));
                 //                     $("#total_gc").text('').append('$'+ total_amount.toFixed(2));
                 //                     $('.gc_amount_details').removeClass('display_none').show();
                 //
                 //                 }
                 //                 if(amount + fee_dollar <= data.balance)
                 //                 {
                 //                     $("#bal-error").text("")
                 //                 }else{
                 //                     $("#bal-error").text("").text("Insufficient Balance")
                 //                     $("#gc_amount").attr("disabled",true);
                 //                     $("#gift_email").attr("disabled",true);
                 //                     $("#giftConfirmbtn").attr("disabled",true);
                 //                 }
                 //             }else {
                 //                 $("#bal-error").text("").text("Insufficient Balance")
                 //             };
                 //         }
                 //     }
                 // });
            if(wallet_id != ""){
                $("#giftcardmessage .account-information").text('').text('Please wait, getting account information.');
                $("#giftcardmessage").removeClass('display_none').show();
            }
            $.ajax({
                url: "/v2/merchant/accounts/get_giftcard_data",
                type: "GET",
                data: {wallet_id: wallet_id,type: "giftcard"},
                success: function(data){
                    $("#giftcard_fee").val(data.fee_dollar);
                    $("#account_balance").val(data.balance);

                    if(data.location_block == true || data.location_block_giftcard == true){
                        $("#gc_amount").attr("disabled",true);
                        $("#gift_email").attr("disabled",true);
                        $("#giftConfirmbtn").attr("disabled",true);
                        $("#fee_div").hide();
                        if(data.location_block == true ){
                            $("#giftcardmessage .account-information").css("display", "block").html("").html(BLOCKED_LOCATION);
                        }else{
                            $("#giftcardmessage .account-information").css("display","block").html("").html(BLOCKED_GIFT_CARD_CONST);
                        }
                    }else if(data.location_block == false && data.location_block_giftcard == false){
                        $("#from_available_amount_gc").text(data.balance);
                        $("#gc_amount").attr("disabled",false);
                        $("#gift_email").attr("disabled",false);
                        $("#giftConfirmbtn").attr("disabled",false);
                        $("#giftcardmessage").hide();
                        if ($("#gc_amount").val() != ""){
                            amount=parseFloat($("#gc_amount").val());
                            if($('#giftcard_fee').val().length > 0 ){
                                fee_dollar = parseFloat($('#giftcard_fee').val());
                                total_amount = amount + fee_dollar;
                                $("#amount_gc").text('').append('$'+ amount.toFixed(2));
                                $("#fee_gc").text('').append('$'+ fee_dollar.toFixed(2));
                                $("#total_gc").text('').append('$'+ total_amount.toFixed(2));
                                $('.gc_amount_details').removeClass('display_none').show();

                            }

                        };
                    }
                    var balance = $("#account_balance").val();
                    var fee = $("#giftcard_fee").val();
                    var input_val = $("#gc_amount").val();
                    var f = parseFloat(input_val) + parseFloat(fee);
                    var total = parseFloat(balance.replace(',', ''))
                    if(f> total){
                        $('#error_giftcard').html('')
                        $("#amount_error").text('').text('Insufficient Balance');
                        $("#giftConfirmbtn").prop('disabled', true );
                    }else{
                        $("#giftConfirmbtn").prop('disabled', false );
                        $("#amount_error").text("");
                    }
                }
            });
             // }
            $(".m_selectpicker").selectpicker();
            $('#giftBuy').show();
            $('#giftConfirm').hide();
            $('#giftCard').hide();
            validateGiftcardForm("giftcard-form");

        }
    });
});

validateGiftcardForm("giftcard-form");

$(document).on("click","#giftConfirmbtn",function () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });
    if ($("#giftcard-form").valid()) {
        var balance = $("#account_balance").val();
        var account = $("#wallet_id option:selected").text();
        var gc_amount = $("#gc_amount").val();
        var fee_gc = $("#giftcard_fee").val();
        var total_gc = parseFloat(gc_amount) + parseFloat(fee_gc)
        var email = $("#gift_email").val();
        var name = $("#gc_name").text();
        var image_url = $("#gc_image").attr('src');
        var converted_banalce = parseFloat(balance.replace(',', ''))
        var remain_balance = converted_banalce - total_gc;
        remain_balance = formatter.format(remain_balance);
        gc_amount = formatter.format(gc_amount);
        total_gc = formatter.format(total_gc);
        fee_gc = formatter.format(fee_gc);

        $("#fee_amount_gc").text(fee_gc);
        $("#avail_balance_gc").text(balance);
        $("#giftcard_amount").text(gc_amount);
        $("#total_amount_gc").text(total_gc);
        $("#remaining_balance").text(remain_balance);
        $("#gc_account").text(account);
        $("#gc_email").text(email);
        $("#gc_name_confirm").text(name);
        $("#gc_image_confirm").attr('src', image_url);

        $('#giftBuy').hide();
        $('#giftConfirm').show();
    }else{
        if($(".select_giftcard_drop").hasClass('has-danger')){
            error = $("#wallet_id").next();
            $("#wallet_id").next().remove();
            $("#wallet_id").next().after(error);
        }
    }

});

$(document).on("click","#submit_giftcard",function () {
    $("#giftcard-form").submit();
});
$(document).on("click","#gc_back",function (e) {
    e.preventDefault();
    $("#giftBuy").hide();
    $("#giftCard").show();
});

$(document).on("keyup","#gc_amount",function () {
    $("#gc_amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    var balance = $("#account_balance").val();
    var fee = $("#giftcard_fee").val();
    var min_val = $(".min_val").html();
    var max_val = $(".max_val").html();
    var input_val = $(this).val();
    var f = parseFloat(input_val);
    var total = parseFloat(balance.replace(',', ''))
    if(parseFloat(input_val) > parseFloat(max_val) || parseFloat(input_val) < parseFloat(min_val)){
        $("#amount_error").text("");
        $('#errorTxt').html('').html('Amount must in between '+min_val+' to '+max_val);
        // $("#bal-error").text("").text("Insufficient Balance");
    } else{
        $('#errorTxt').html('');
        // $("#bal-error").text("");
    }
    if(f> total){
        $('#error_giftcard').html('')
        $("#amount_error").text('').text('Insufficient Balance');
        $("#giftConfirmbtn").prop('disabled', true );
    }else{
        $("#giftConfirmbtn").prop('disabled', false );
        $("#amount_error").text("");
    }
    if (input_val == ""){
        $(".gc_amount_details").hide();
        $("#amount_gc").text('').append("$0.00");
        $("#fee_gc").text('').append("$0.00");
        $("#total_gc").text('').append("$0.00");
    }else {
        fee_dollar = parseFloat($('#giftcard_fee').val());
        total_amount = f + fee_dollar;
        $("#amount_gc").text('').append('$' + parseFloat(input_val).toFixed(2));
        $("#fee_gc").text('').append('$' + fee_dollar.toFixed(2));
        $("#total_gc").text('').append('$' + total_amount.toFixed(2));
        $('.gc_amount_details').removeClass('display_none').show();
    }
});

$(document).on("keyup","#gc_amount",function () {
    var account_balance = $("#account_balance").val();
    var input_val = $(this).val();
    var f = parseFloat(input_val);
    fee_dollar = parseFloat($('#giftcard_fee').val());
    total_amount = f + fee_dollar;
    debugger;
    if(account_balance < total_amount){
        $('#error_giftcard').html('')
        $("#amount_error").text('').text('Insufficient Balance');
        $("#giftConfirmbtn").prop('disabled', true );
    }
});

$(document).on('keyup',"#gc_search", function () {
    var filter, tr, td, i, txtValue;
    filter=$("#gc_search").val().toUpperCase();
    tr=$("#gc_table").find('> tbody > tr');
    for (i = 0; i < tr.length; i++) {
        td = tr[i].children[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }

});

$('#gc_back').click(function () {
    $('#giftBuy').show();
    $('#giftConfirm').hide();
});

$(document).on('change',"#wallet_id", function () {
    $("#amount_error").text("")
    $('.gc_amount_details').addClass('display_none').hide();
    $("#gc_amount").attr("disabled",true);
    $("#gift_email").attr("disabled",true);
    $("#giftConfirmbtn").attr("disabled",true);
    if($("#wallet_id").val() != ""){
        $("#giftcardmessage .account-information").text('').text('Please wait, getting account information.');
        $("#giftcardmessage").removeClass('display_none').show();
    }else{
        $("#giftcardmessage .account-information").text('')
        $('#giftcardmessage').addClass('display_none').hide();
    }

    $.ajax({
        url: "/v2/merchant/accounts/get_giftcard_data",
        type: "GET",
        data: {wallet_id: $(this).val(),type: "giftcard"},
        success: function(data){
            $("#giftcard_fee").val(data.fee_dollar);
            $("#account_balance").val(data.balance);

            if(data.location_block == true || data.location_block_giftcard == true){
                $("#gc_amount").attr("disabled",true);
                $("#gift_email").attr("disabled",true);
                $("#giftConfirmbtn").attr("disabled",true);
                $("#fee_div").hide();
                if(data.location_block == true ){
                    $("#giftcardmessage .account-information").css("display", "block").html("").html(BLOCKED_LOCATION);
                }else{
                    $("#giftcardmessage .account-information").css("display","block").html("").html(BLOCKED_GIFT_CARD_CONST);
                }
            }else if(data.location_block == false && data.location_block_giftcard == false){
                $("#from_available_amount_gc").text(data.balance);
                $("#gc_amount").attr("disabled",false);
                $("#gift_email").attr("disabled",false);
                $("#giftConfirmbtn").attr("disabled",false);
                $("#fee_div").show();
                $("#giftcardmessage").hide();
                if ($("#gc_amount").val() != ""){
                    amount=parseFloat($("#gc_amount").val());
                    if($('#giftcard_fee').val().length > 0 ){
                        fee_dollar = parseFloat($('#giftcard_fee').val());
                        total_amount = amount + fee_dollar;
                        $("#amount_gc").text('').append('$'+ amount.toFixed(2));
                        $("#fee_gc").text('').append('$'+ fee_dollar.toFixed(2));
                        $("#total_gc").text('').append('$'+ total_amount.toFixed(2));
                        $('.gc_amount_details').removeClass('display_none').show();

                    }

                };
            }
            var balance = $("#account_balance").val();
            var fee = $("#giftcard_fee").val();
            var input_val = $("#gc_amount").val();
            var f = parseFloat(input_val) + parseFloat(fee);
            var total = parseFloat(balance.replace(',', ''))
            if(f> total){
                $('#error_giftcard').html('')
                $("#amount_error").text('').text('Insufficient Balance');
                $("#giftConfirmbtn").prop('disabled', true );
            }else{
                $("#giftConfirmbtn").prop('disabled', false );
                $("#amount_error").text("");
            }
        }
    });

});

function validateGiftcardForm(role) {
    $(function() {
        var formRules = {
            rules: {
                "wallet_id": {
                    required: true
                },
                "email": {
                    required: true,
                    email: true

                },
                "amount": {
                    required: true
                }
            },
            messages: {
                "wallet_id": {
                    required: "Please Select an Account"
                },
                "email": {
                    required: "Please Enter Email"
                },
                "amount": {
                    required: "Please Enter Amount",
                    min: '',
                    max: ''
                }
            }

        };
        // $.extend(formRules, config.validations);
        $('#' + role).validate(formRules);
    });
};

$(document).on("change",".two_decimal",function (e) {
    $(function() {
        $(".two_decimal").on('input', function() {
            this.value = this.value.match(/\d{0,12}(\.\d{0,2})?/)[0];
        });
    });
});
// To prevent reload on hit enter in g-card search field
$(document).on('submit', '#search_form_shared_account', function(e){
    e.preventDefault();
});