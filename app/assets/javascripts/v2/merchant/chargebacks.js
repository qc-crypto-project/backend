$(document).ready(function () {
    $('[data-toggle="popoverpr"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<p class="hover-font-change" src="'+$(this).data('p') + '">Total number of chargeback cases <br> created against this account for the <br> selected date range</p>';
        },

    })
    $('[data-toggle="popoverpp"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<p class="hover-font-change2" src="'+$(this).data('p') + '">Total number of chargeback cases <br> that have not been settled</p>';
        },

    })
    $('[data-toggle="popoverpw"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<p class="hover-font-change" src="'+$(this).data('p') + '">Total number of chargeback cases<br> that have been settled in favor of the<br> merchant</p>';
        },

    })
    $('[data-toggle="popoverpl"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<p class="hover-font-change" src="'+$(this).data('p') + '">Total number of chargeback cases<br> that have been settled in favor of the<br> customer</p>';
        },

    })
    $('[data-toggle="popoverpc"]').popover({
        //trigger: 'focus',
        trigger: 'hover',
        html: true,
        content: function () {
            return '<p class="hover-font-change" src="'+$(this).data('p') + '">Percentage of chargeback cases<br> created against this account for the<br> selected date range</p>';
        },

    })
    $(document).on('click','.fa-caret-down',function () {
        $(this).addClass('fa-caret-up').removeClass('fa-caret-down')
        sortTable($('#mytable'),'asc');
    });
    // $("body").delegate('.fa-caret-up','click',function() {
    $(document).on('click','.fa-caret-up',function () {
        $(this).removeClass('fa-caret-up').addClass('fa-caret-down')
        sortTable($('#mytable'),'desc');
    });
    function sortTable(table, order) {
        var asc   = order === 'asc',
            tbody = table.find('tbody');

        tbody.find('tr').sort(function(a, b) {
            if (asc) {
                return $('td:first', a).text().localeCompare($('td:first', b).text());
            } else {
                return $('td:first', b).text().localeCompare($('td:first', a).text());
            }
        }).appendTo(tbody);
    }


    // %Y-%m-%d %H:%M:%S %z                  %m/%d/%Y at %I:%M%p
    var lt= $('.timeutc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'MM/DD/YYYY HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });
    $('.case-won-section1').hide();
    $('.payee_checkbox').prop("checked", true);

    if($('#dispute_status').val()=='dispute_accepted'){
    $('.chargeback-accepted-status').show();
    $('.fight-chargeback-and-accept-btn').hide();
    $('.vl_1').show();
    $('.refund_accept_images').show();
    $('.vl_2').show();
    $('.refund_group_images').show();
    $('.charback-accepted-case').show();
    $('.fight-chargeback-part').hide();

    // $('.fight-chargeback-case').show();
    // $('.vl_3').show();
    // $('.vl_4').show();
    // $('.case-won-section').show();
    // $('.refund_fcharge_images').show();
    // $('.refund_fcharge1_images').show();
    //
    // $('.refund_group_images').show();
    // $('.fight-chargeback-and-accept-btn').hide();
    // $('.vl_1').show();
    // $('.charback-accepted-case').hide();
    // $('.fight-chargeback-part').hide();






    // $('.btn_cancel').click();
    // $('.chargeback-accepted-status').show();
    // $('.fight-chargeback-and-accept-btn').hide();
    // $('.vl_1').show();
    // $('.chargeback-icon-1').show();
    // $('.vl_2').show();
    // $('.chargeback-icon-2').show();
    // $('.charback-accepted-case').show();
    // $('.fight-chargeback-part').hide();
}

if($('#dispute_status').val()=='sent_pending' && $('#dispute_evidence').val()=='true'){

    $('.fight-chargeback-case').show();
    $('.case-won-section1').show();
    $('.vl_1').show();
    $('.vl_3').show();
    $('.vl_4').show();
    $('.refund_group_images').show();

    $('.refund_fcharge_images').show();
    $('.refund_fcharge1_images').show();
    $('.vl_4').hide();
    $('.refund_fcharge1_images').hide();
    $('.case-won-section1').hide();
    // $('.chargeback-open-cases').css('height','90%');
    if($('#evidence_id').val()!==undefined && $('#evidence_id').val()!=''){
        if(parseInt($('#evidence_id').val()) == 1 || parseInt($('#evidence_id').val()) == 2 )
        {
            $('.vl_3 ').css('height','77%');
        }
        else{
            $('.vl_3 ').css('height','81%');
        }
    }
    else{
        $('.vl_3 ').css('height','75%');
    }

    // $('.vl_2').show();
    // $('.refund_group_images').show();
    //
    // $('.refund_fcharge_images').show();
    // $('.refund_fcharge1_images').show();

    // $('.fight-chargeback-case').show();
    // $('.fight-chargeback-and-accept-btn').hide();
    // $('.vl_1').css('height',"42%");
    // $('.vl_1').show();
    // $('.chargeback-icon-1').show();
    // $('.vl_2').css('height',"37.7%");
    // $('.vl_2').show();
    // $('.chargeback-icon-3').show();
    // $('.vl_3').hide();
    // $('.chargeback-icon-4').hide();
    // // $('.vl_4').show();
    // // $('.chargeback-icon-5').show();
    // $('.fight-chargeback-part').hide();
    // $('.case-won-section').hide();
}
    if($('#won_lost_case').val()=='true'){

        $('.fight-chargeback-case').show();
        $('.vl_3').show();
        $('.vl_4').show();
        $('.case-won-section').show();
        $('.refund_fcharge_images').show();
        $('.refund_fcharge1_images').show();

        $('.refund_group_images').show();
        $('.fight-chargeback-and-accept-btn').hide();
        $('.vl_1').show();
        $('.charback-accepted-case').hide();
        $('.fight-chargeback-part').hide();
        $('.evidence_submit').hide();


        // $('.fight-chargeback-case').show();
        // $('.fight-chargeback-and-accept-btn').hide();
        // $('.vl_1').css('height',"33%");
        // $('.vl_1').show();
        // $('.chargeback-icon-1').show();
        // $('.vl_2').css('height',"18.7%");
        // $('.vl_2').show();
        // $('.chargeback-icon-3').show();
        // $('.vl_3').hide();
        // $('.chargeback-icon-4').hide();
        // // $('.vl_4').show();
        // // $('.chargeback-icon-5').show();
        // $('.fight-chargeback-part').hide();
        // $('.bank_reponse').hide();
        // $('.chargeback-icon-5').show();
        // $('.vl_4').css('height',"28.5%");
        // $('.vl_4').show();
        // if($('#dispute_evidence').val()=='true'){
        //     $('.vl_1').css('height',"30%");
        //     $('.vl_4').css('height',"25.5%");
        //     $('.vl_2').css('height',"25.7%")
        // }
        //
        // $('.case-won-section').show();
    }

    // document.getElementById('get_file').onclick = function() {
    //     document.getElementById('my_file').click();
    // };
    //
    // document.getElementById('my_file').onchange = function () {
    //     $('.attatchments').show();
    //     $('.filename').html('')
    //     for(i=0;i< this.files.length;i++){
    //         $('.filename').append('<br>'+(this.files.item(i).name));
    //     }
    // };



    var globalTimeout = null;
    $(document).on('keyup','.search_input_datatable',function () {
        if ($('#total_chargeback').val() != undefined && $('#total_chargeback').val() == 'true') {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        }
        else{

        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function () {
            filter_chargeback("filters", "search");
            globalTimeout = null;
            var search_value = $('.search_input_datatable').val();
            var filter = $('#filter').val();
            var filter_data = "";
            if (filter != "") {
                filter_data = filter;
            } else {
                filter_data = 10;
            }
            // $.ajax({
            //     url: '/v2/merchant/chargebacks',
            //     type: 'GET',
            //     data: {q: search_value,filter: filter_data,search:'true'},
            //     success: function (data) {
            //         $('.table-data').html('').html(data);
            //         $('.search_input_datatable').val(search_value);
            //         $('.search_input_datatable').focus();
            //     }, error: function (data) {
            //
            //
            //     }
            // });

        }, 1500);
    }

    });

    $(document).on('click','.seq_id', function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($.trim($(this).data('id'))).select();
        document.execCommand("copy");
        $temp.remove();
    })

});

$(document).on('click','.charge_back_hover',function (){
    window.location.href=$(this).data('href');

});

$(document).on('click','.accept_chargebacks',function () {
    var password = $('#password').val();
    var dispute_id = $('#dispute_id').val();
    $.ajax({
        url: "/v2/merchant/chargebacks/verify_admin_password",
        method: 'get',
        data: {password: password},
        success:function(data){
            $('.btn_cancel').click();
            window.location.href = 'accept_dispute?dispute_id=' + dispute_id


        },
        error: function(data){
            $("#password_btn").css("border","1px solid red");
            $("#info-text").text('').css("color","red").text('Please try again!');
        }
    });
});

$(document).on('change','.payee_checkbox',(function () {
    // To Checked or Un-checked all checked box
    $('.payee_checkbox').attr("disabled", true);
    if($(this).attr("id") == "all") {
        if ($('#all').is(":checked")) {
            $('.payee_checkbox').prop("checked", true)
        } else {
            $('.payee_checkbox').prop("checked", false)
        }
    }else{
        // $(this).prop("checked", true)
        $('#all').prop("checked", false)
    }
    $(".btn-status").trigger('click');
    filter_chargeback("filters", "status");
}));
function filter_chargeback(from,type){

    if ($('#all').is(":checked")) {
        var all = $('#all').val();
    }
    var query = [];
    if ($('#sent_pending').is(":checked")) {
        query.push("0");
    }
    if ($('#won_reversed').is(":checked")) {
        query.push("1");
    }
    if ($('#lost').is(":checked")) {
        query.push("2");
    }
    if ($('#dispute_accepted').is(":checked")) {
        query.push("3");
    }
    if ($('#under_review').is(":checked")) {
        query.push("4");
    }
    var filter = $("#filter").val();
    $("#checkboxes").val(query);
    search_value = $('.search_input_datatable').val();
    search_value = search_value.trim()
    var filter = $("#filter").val();
    var id = $("#client_id").val();
    $('#search_val').val(search_value);
    var filter = $('#filter').val();
    var filter_data = "";
    if(filter != ""){
        filter_data = filter;
    }else{
        filter_data = 10;
    }
    first_date='';
    last_date='';    // /v2/merchant/invoices/client_details
    if($('#first_date').val()!=undefined && $('#first_date').val()!=''){
    first_date= $('#first_date').val();
    last_date =$('#last_date').val();
    }
    else if($('.m-input').attr('placeholder')=='Month to Date'){
        first_date = moment().startOf("month")._d;
        last_date = moment()._d;
    }
    $.ajax({
        url: "/v2/merchant/chargebacks/account_chargeback",
        type: "GET",
        data: {all: all, checkbox: query, filter: filter_data, from: from, search: search_value, id: id, search_only: "true",wallet_id:$('#wallet_id').val(),first_date: first_date,second_date: last_date },
        success: function (data) {
            $("#chargeback_parent_div").empty().append(data);

            // $('.table-data').html(data);
            if(type == "status"){
                $(".btn-status").trigger('click');
                $('.payee_checkbox').removeAttr("disabled");
            }
            $('.search_input_datatable').val(search_value);
            $('.search_input_datatable').focus();
            $("#filter").selectpicker()





            if ( all == "on") {
                $('#all').prop('checked', true);
            }else{
                $('#all').prop('checked', false);
            }
            $.each(query, function( index, value ) {
                if ( value == "0") {
                    $('#sent_pending').prop('checked', true);
                }
                if (value == "1" ) {
                    $('#won_reversed').prop('checked', true);
                }
                if (value == "2" ) {
                    $('#lost').prop('checked', true);
                }
                if (value == "3" ) {
                    $('#dispute_accepted').prop('checked', true);
                }
                if (value == "4") {
                    $('#under_review').prop('checked', true);
                }
            });
        }
    });
}
var counter=0
$('#get_file').on('click',function () {
// document.getElementById('get_file').onclick = function() {
    document.getElementById('my_file'+counter).click();
    counter=counter+1
    if(counter!=0){
        $('.abcd').append('<input class="uploadFiles" data-id="'+counter+'" id="my_file'+counter+'" accept=".doc,.rtf,.xlsx,.docx,.csv,image/x-png,image/gif,image/jpeg,image/bmp,application/pdf" data-max-file-size="10485760" type="file" name="evidence[image[evidence]['+counter+']][]">')
    }

});
$(document).on('change', "input[type='file']", function () {
// document.getElementById('my_file').onchange = function () {
    $('.attatchments').show();
    $(this.files).each(function (index, file) {
        file_name = file.name
        if(file_name.length > 18) {
            file_extension = file_name.split('.')[1];
            file_extension = file_name.substring(file_name.length - 4, file_name.length);
            file_name = file_name.split('.')[0];
            file_name = file_name.slice(0,18);
        }else {
            file_extension = ''
        }
        var file_size= parseFloat((file.size/1024)/1024).toFixed(2);
        progressHandler(file_size, counter);
        if(file_size > 10) {
            alert("Please select a file less than 10MB ");
            $(this).val('');
            $('#my_file').val('');
            $('.close2').remove();
        } else{
            $(".file-box").append('<div class="charge_back_attachment mb-3"><div class="alert file-alert myfile alert-dismissible close2 fade show mb-0" role="alert">\n' +
                '                      <span class="file-icon"></span>\n' +
                '                       <span class="filename">'+file_name+'.'+file_extension+'</span>\n' +
                '                       <span class="del-btn file-close-btn'+counter+'" data-count="'+counter+'"></span>' +
                '                    </div><div class="progress chageback_loader'+counter+'">\n' +
                '                      <progress id="progressBar'+counter+'" value="0" max="100"></progress>\n' +
                '                    </div></div>')
        }
        $(".del-btn").html($(".del-img-div").html());
        $(".file-icon").html($(".file-icon-div").html());
    })
    // $('.filename').html('')
    // for(i=0;i< this.files.length;i++){
    //     $('.filename').append('<br>'+(this.files.item(i).name));
    // }
});
$(document).on('click', '.del-btn', function () {
    var count = $(this).data("count");
    count = count - 1
    $("#my_file"+count).remove();
    $(this).parent().parent().remove();
});


$("body").delegate('.uploadFiles','change',function() {
    file_name=$(this).val().split("fakepath")[1].substring(1)
    if(file_name.length > 18){
    file_extension=file_name.split('.')[1];
    file_extension= file_name.substring(file_name.length - 4, file_name.length);
    file_name=file_name.split('.')[0];
    file_name=file_name.slice(0,18);
    }
    else{
        file_extension=''
    }
    var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
    var ext = this.value.split('.').pop().toLowerCase();
    if(file_size > 10) {
        alert("Please select a file less than 10MB ");
        $(this).val('');
    }

    else{
    // $('.filename').append('<br><span>'+file_name+'</span>'+'<button type="button" class="close margin_class close_'+$(this).data('id')+' file-close" aria-label="Close"></button>');
    $('.attatchments').after('<br> <div class="alert file-alert myfile alert-dismissible fade show close_'+$(this).data('id')+'" role="alert"><button type="button" class="close file-close close_files" data-id="'+$(this).data('id')+'" aria-label="Close" ></button><span class="filename">'+file_name+'.'+file_extension+'</span></div>');

    }

});
function progressHandler(size, count) {
    var unit_val = parseInt(size);
    var total = unit_val * 100;
    for (var i = 0; i <= 100; i++) {
        (function(i) {
            setTimeout(function() {
                $("#progressBar"+count).val(i);
                if(i == 100){
                    $(".chageback_loader"+count).hide();
                    $(".file-close-btn"+count).show();
                }
            }, 10 * i * unit_val);
        })(i);
    }
}
$("body").delegate('.close_files','click',function() {
id=$(this).data('id');
$('#my_file'+id).val('');
    $('.close_'+id).remove();
});

$(document).on('click', '.ranges ul li', function () {
    var startdate = '';
    var enddate = '';
    var filter_day = ""
    var path = $("#path").val();
    if (path == ""){
        path = '/v2/merchant/chargebacks'
    }
    else{
        if ($('#all').is(":checked")) {
            var all = $('#all').val();
        }
        var query = [];
        if ($('#sent_pending').is(":checked")) {
            query.push("0");
        }
        if ($('#won_reversed').is(":checked")) {
            query.push("1");
        }
        if ($('#lost').is(":checked")) {
            query.push("2");
        }
        if ($('#dispute_accepted').is(":checked")) {
            query.push("3");
        }
        if ($('#under_review').is(":checked")) {
            query.push("4");
        }
        var filter = $("#filter").val();
        $("#checkboxes").val(query);
    }
    if($(this).text() == "Yesterday"){
        startdate = moment().subtract(1, "days")._d;
        enddate = moment().subtract(1, "days")._d;
        filter_day = "Yesterday"
    } else if($(this).text() == "Today") {
        startdate = moment()._d;
        enddate = moment()._d;
        filter_day = "Today"
    } else if($(this).text() == "Last 6 Days") {
        startdate = moment().subtract(5, "days")._d;
        enddate = moment()._d;
        filter_day = "Last 6 Days"
    } else if($(this).text() == "Month to Date") {
        startdate = moment().startOf("month")._d;
        enddate = moment()._d;
        filter_day = "Month to Date"
    } else if($(this).text() == "Previous month") {
        startdate =  new moment().subtract(1, 'months').date(1)._d
        enddate =  moment().subtract(1,'months').endOf('month')._d;
        filter_day = "Previous month"
    }
    if($(this).text() != "Custom"){
        $('#first_date').val(startdate);
        $('#last_date').val(enddate);
        $.ajax({
            url: path,
            type: 'GET',
            data: {first_date: startdate, second_date: enddate, wallet_id: $("#wallet_id").val(),checkbox: query, filter_day: filter_day},
            success: function(data) {
                $('#chargeback_parent_div').html(data);
                if (path=='/v2/merchant/chargebacks/account_chargeback'){
                    if ( all == "on") {
                        $('#all').prop('checked', true);
                    }else{
                        $('#all').prop('checked', false);
                    }
                    $.each(query, function( index, value ) {
                        if ( value == "0") {
                            $('#sent_pending').prop('checked', true);
                        }
                        if (value == "1" ) {
                            $('#won_reversed').prop('checked', true);
                        }
                        if (value == "2" ) {
                            $('#lost').prop('checked', true);
                        }
                        if (value == "3" ) {
                            $('#dispute_accepted').prop('checked', true);
                        }
                        if (value == "4") {
                            $('#under_review').prop('checked', true);
                        }
                    });
                }

                // update_total();
            }
        });
    }
});
$(document).on('click', '.applyBtn', function () {
    var filter_day = ''
    var custom = ''
    var startdate = $("input[name=daterangepicker_start]").last().val();
    var enddate = $("input[name=daterangepicker_end]").last().val();
       $('#first_date').val(moment(startdate)._d);
      $('#last_date').val(moment(enddate)._d);
    var path = $("#path").val();
    if (path == ""){
        path = '/v2/merchant/chargebacks'
    }
    else{
        if ($('#all').is(":checked")) {
            var all = $('#all').val();
        }
        var query = [];
        if ($('#sent_pending').is(":checked")) {
            query.push("0");
        }
        if ($('#won_reversed').is(":checked")) {
            query.push("1");
        }
        if ($('#lost').is(":checked")) {
            query.push("2");
        }
        if ($('#dispute_accepted').is(":checked")) {
            query.push("3");
        }
        if ($('#under_review').is(":checked")) {
            query.push("4");
        }
        var filter = $("#filter").val();
        $("#checkboxes").val(query);
    }

    if (moment().subtract(1, "month").startOf("month").format('MM/DD/YYYY') == moment(startdate).format('MM/DD/YYYY') && moment().subtract(1, "month").endOf("month").format('MM/DD/YYYY') == moment(enddate).format('MM/DD/YYYY') ){
        filter_day = "Previous month"
        custom = false
    }else if (moment().startOf("month").format('MM/DD/YYYY') == moment(startdate).format('MM/DD/YYYY') && moment().format('MM/DD/YYYY') == moment(enddate).format('MM/DD/YYYY') ){
        filter_day = "Month to Date"
        custom = false
    }else if (moment().subtract(5, "days").format('MM/DD/YYYY') == moment(startdate).format('MM/DD/YYYY') && moment().format('MM/DD/YYYY') == moment(enddate).format('MM/DD/YYYY') ){
        filter_day = "Last 6 Days"
        custom = false
    }else if (moment().format('MM/DD/YYYY') == moment(startdate).format('MM/DD/YYYY') && moment().format('MM/DD/YYYY') == moment(enddate).format('MM/DD/YYYY') ){
        filter_day = "Today"
        custom = false
    }else{
        filter_day = 'Custom'
        custom = true
    }
    $.ajax({
        url: path,
        type: 'GET',
        data: {first_date: moment(startdate)._d, second_date: moment(enddate)._d, wallet_id: $("#wallet_id").val(), custom: custom,checkbox: query, filter_day: filter_day},
        success: function(data) {
            $('#chargeback_parent_div').html(data);
            // update_total();
            if (path=='/v2/merchant/chargebacks/account_chargeback'){
                if ( all == "on") {
                    $('#all').prop('checked', true);
                }else{
                    $('#all').prop('checked', false);
                }
                $.each(query, function( index, value ) {
                    if ( value == "0") {
                        $('#sent_pending').prop('checked', true);
                    }
                    if (value == "1" ) {
                        $('#won_reversed').prop('checked', true);
                    }
                    if (value == "2" ) {
                        $('#lost').prop('checked', true);
                    }
                    if (value == "3" ) {
                        $('#dispute_accepted').prop('checked', true);
                    }
                    if (value == "4") {
                        $('#under_review').prop('checked', true);
                    }
                });
            }

        }
    });
});

 $('#e-checks').on('click', function (){
     if ($('#filter_day').val() == "Custom") {
         $('.daterangepicker').css('height', '400px');
         $('.daterangepicker').css('width', '790px');
         $('.daterangepicker').css('margin-left', '0px');
         $('.daterangepicker_input').show();
         $('.applyBtn').show();
     }
 })


