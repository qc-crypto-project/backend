        $(document).ready(function () {
            var expired = $("#days_left").text();
            if(expired == "(Review Expired)"){
                $(".risk_accept_button").css("display","none");
                $(".risk_refund_button").css("display","none");
            }
        });

        $(document).ready(function(){
            var height_in = 20;
            var moderate_risk = $("#moderate_risk").text();
            var high_risk = $("#high_risk").text();
            var score = $(".risk-score").text();

            // //override for testing for testing
            // score = 40.43
            // moderate_risk = 35.5;
            // high_risk = 90.09

            var risk_h = height_in + "px"
            var d_nut = (height_in * 2) + "px";
            var risk_container = {
                class: "risk_container",
                css:{
                    display: 'inline-table',
                    'border-radius': '30px',
                    width: '100%',
                    'background-color': 'gainsboro',
                    height:risk_h,
                    padding: '7px',
                }
            }
            var $risk_container = $("<div>", risk_container);
            $("#laka-risk-bar").append($risk_container);

            var main = {
                class: "main",
                css:{
                    position: "relative"
                }
            }
            var $main = $("<div>", main);
            $(".risk_container").append($main);

            var g = {
                class: "g",
                css:{
                    display: 'inline-block',
                    'border-radius': '30px',
                    width: moderate_risk + '%',
                    'background-color': '#47cf73',
                    height:risk_h,
                    float: 'left',
                }
            }
            var $g = $("<div>", g);
            $(".main").append($g);

            var m = {
                class: "m",
                css:{
                    display: 'inline-block',
                    'border-radius': '30px',
                    width: (high_risk - moderate_risk) + '%',
                    'background-color': '#ffad19',
                    height:risk_h,
                    float: 'left',
                }
            }
            var $m = $("<div>",  m);
            $(".main").append($m);

            var h = {
                class: "h",
                css:{
                    display: 'inline-block',
                    'border-radius': '30px',
                    width: (100 - high_risk) + '%',
                    'background-color': '#ff5559',
                    height:risk_h,
                    float: 'left',
                }
            }
            var $h = $("<div>",  h);
            $(".main").append($h);


            var doughnut = {
                class: "doughnut",
                css:{
                    border: '10px solid #88727266',
                    'border-radius': '100px',
                    height:d_nut,
                    width:d_nut,
                    top: '-10px',
                    position: 'absolute',
                    left: 'calc('+score +'% - 20px)',
                    'text-align': 'center',
                    color: 'black',
                    background: '#ffffff9c',
                }
            }
            var $doughnut = $("<div>",  doughnut);
            $doughnut.html("|");
            $(".main").append($doughnut);

            var risk = {
                class: "risk",
                css:{
                    top:'-30px',
                    position: 'absolute',
                    background: '#344652',
                    border: '1px solid #000000',
                    'border-radius': '40px',
                    //width: '30px',
                    height: '20px',
                    'font-family': 'monospace',
                    'font-weight': 'bold',
                    margin: '0px auto',
                    color: 'white',
                    'font-size': '0.7rem',
                    left: 'calc('+score +'% - 20px)',
                    'text-align': 'center',
                    'line-height': '18px',
                    width: '40px'
                }
            }
            var $risk = $("<div>",  risk);
            $risk.html(score);
            $(".main").append($risk);
        });
        function setTooltip(btn, message) {
            $(btn).tooltip('hide')
                .attr('data-original-title', message)
                .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                $(btn).tooltip('hide');
            }, 1000);
        }

        $(document).ready(function(){
            var clipboard = new Clipboard('.copy_transection_id');
            clipboard.on('success', function(e) {
                e.clearSelection();
                setTooltip(e.trigger, 'Copied to Clipboard!');
                hideTooltip(e.trigger);
            });
            console.log(clipboard);
        });
        $(document).on("change","#refund_reason",function () {
            if ($(this).val() == 4){
                $("#refund_description").css("display","block");
                $("#exampleTextarea").prop("disabled",false);
            }else{
                $("#reason_detail").val($("#refund_reason option:selected").text());
                $("#refund_description").css("display","none");
                $("#exampleTextarea").prop("disabled",true);
            }
        });
        $(document).on("click","#refund_transaction_btn",function () {
            var txn_id = $(this).data("transaction-id");
            var fees
            $.ajax({
                url: '/v2/merchant/accounts/get_refund_fee',
                type: 'GET',
                data: {trans_id: txn_id},
                success: function(data) {
                    fees = data.fee.toFixed(2);
                    $("#fee").text(data.fee.toFixed(2));
                    $("#refund_fee").val(data.fee.toFixed(2));
                    total_refund(fees);
                }
            })
        });
        $(document).on("keyup change","#refund_amount",function () {
            if($(this).val() != "") {
                var input = parseFloat($(this).val());
                var fee = parseFloat($("#fee").text());
                var total = input + fee
            }else{
                var total = 0.00;
                var input = 0.00;
            }
            $("#amount").text(input.toFixed(2));
            $("#total").text(total.toFixed(2));
        });

        $(document).on("submit","#refund_submit_form",function () {
            $("#RefundsTransactionModal").modal("hide");
            $("#RefundSuccessModal").modal("show");
        });
        $(document).on("click",".refund-yes-btn",function () {
            $("#TransactionDecline").modal("hide");
            $("#RefundSuccessModal").modal("show");
        });

        $(document).on("click",".ok--btn1",function () {
            window.location.reload();
        });
         function  total_refund(fees){
                var input = parseFloat($("#refund_amount").val());
                var fee = parseFloat(fees)
                var total = input + fee
            $("#amount").text(input.toFixed(2));
            $("#total").text(total.toFixed(2));
        }



