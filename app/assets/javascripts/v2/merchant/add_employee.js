// require v2/merchant/custom
//= require merchant/jquery.multiselect
//= require intlTelInput

$(document).ready(function () {

    $("body").delegate('#edit-employee','click',function() {

        // if ($('#employee-edit').valid()) {
        //     $('#edit-employee').html("").append("Save Changes &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
        // }
        // else if (!$('#employee-edit').valid()){
        //     $('#edit-employee').html("").append("Not Valid");
        // }
    });


    $("body").delegate('.permission_select a','click',function() {
        // $('.permission-dropdown--menu a').click(function () {
        var value=$(this).attr('data-value');
        $('#permission_ids').val($(this).data('id'));
        $('.permission-dropdown').text(value);
        $('.dropdown-menu--pad').removeClass('show');
    });



    $("body").delegate('#phone_number','keyup',function(){
        // $('#phone_number').on('keyup', function () {
        $('#phone-error').css('color','#d45858;');
        var phone = $(this).val();
        var reg = new RegExp('^[0-9]+$');
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var user_id = $("#user_id").val();
        var phone_code = $(this).closest('div').find('.selected-dial-code').text();
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        if(phone.length > 0){
            $('#phone-error').html('');
            if(phone_number.match(reg)) {
                $(':input[type="submit"]').prop('disabled', true);
                $.ajax({
                    url: '/v2/merchant/base/verifying_user',
                    method: 'get',
                    // dataType: 'json',
                    data: {phone_number: phone_number , user_id: user_id},
                    success: function (result) {
                        if (result.success == "notverified") {
                            if ($('#phone-error').text() == "") {
                                $('#phone-error').html('').html(result.message);
                                $('.add_employe_btn').prop('disabled', true);
                            }
                        } else {
                            if ($('#error_email').text() == "" && $('user_password_confirmation-error').text() == "") {
                                $('#phone-error').html('');
                                $('.add_employe_btn').prop('disabled', false);
                            } else {
                                $('#phone-error').html('');
                                $('.add_employe_btn').prop('disabled', true);
                            }
                        }
                    },
                    error: function (result) {
                        $('#phone-error').html('');
                        $('.add_employe_btn').prop('disabled', 'disabled');
                    }
                });
            }else{
                $(':input[type="submit"]').prop('disabled', 'disabled');
                $("#phone-error").text('').text('Must be Number');
            }
        }else{
            $(':input[type="submit"]').prop('disabled', 'disabled');
            // $("#phone-error").text('').text('Please enter Phone Number');
        }
    });
    $("body").delegate('#email','keyup',function() {
        // $('#email').on('keyup', function () {
        var email = $('#email').val();
        var user_id = $('#user_id').val();
        $.ajax({
            url: '/v2/merchant/base/verifying_user',
            method: 'get',
            dataType: 'json',
            data: {email: email, user_id: user_id},
            success: function (result) {
                if (result.success == "notverified") {
                    $('#error_email').html('').html('<span style="color: red;" >A user with this email already exists</span>');
                    $('#email').addClass('is-invalid');
                    $('.add_employe_btn').prop('disabled', true);
                    $('#add_user').prop('disabled', 'disabled');
                } else {
                    if ($('#error_phone').text() == "" && $('#error_email').text() == "" && $('user_password_confirmation-error').text() == "") {
                        $('#error_email').html('')
                        $('#email').removeClass('is-invalid');
                        $('.add_employe_btn').prop('disabled', false);
                    } else {

                        $('#error_email').html('');
                        $('#email').removeClass('is-invalid');
                        $('.add_employe_btn').prop('disabled', true);
                    }
                }
            },
            error: function (result) {
                $('#email').removeClass('is-invalid').addClass('is-valid')
                $('#error_email').html('');
            }
        });
    });
    $(document).on('click change','.country',function () {
        $('#phone_number').val('');
    })
    $("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            // $('#phone_code').val(Object.values(selectedCountryData)[2]);
            if(selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        // utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
    $(document).on("click", ".obj-message-create", function (ev) {
        // ev.preventDefault();
        var PermissionName = $(this).data("value");
        if (PermissionName === 'admin (Full Access)' || PermissionName === 'Admin (Full Access)') {
            $('#show-notice').show();
            $('#show-notice-edit').show();
        } else {
            $('#show-notice').hide();
            $('#show-notice-edit').hide();
        }
    });

    $(document).on("click", "#perm1", function (ev) {
        // ev.preventDefault();
        var url = $(this).data("url");
        var modal_selector = $(this).data("modal_selector");
        var modal_container_selector = $(this).data("modal_container_selector");
        var method = $(this).data("method");
        $.ajax({
            url: url,
            type: method,
            success: function (data) {
                $(modal_container_selector).empty().html(data);
                $(modal_selector).modal();
                $('.permission_dropdown').selectpicker();
            }
        });
        return false;
    });
    $(document).on("click", "#perm12", function (ev) {
        // ev.preventDefault();
        var selectedVal = $(this).data("id");
        $.ajax({
            url: '/v2/merchant/permission/' + selectedVal,
            type: 'get',
            data: {from_employee: 'true'},
            success: function (data) {
                $('#permission_modal').empty().html(data);
                $('#adminAccessModal').modal();
                // $('.permission_dropdown').selectpicker();
            }
        });
        return false;
    });


    // $("body").delegate("#new_user", "submit", function () {
    //     $('.add_employe_btn').prop('disabled', true);
    // });


    $("body").delegate('#select_permission', 'change', function () {
        // $("#select_permission").on("change", function() {

        // Pure JS
        var selectedVal = this.value;
        $.ajax({
            url: '/v2/merchant/permission/' + selectedVal,
            type: 'get',
            data: {from_employee: 'true'},
            success: function (data) {
                $('#permission_modal').empty().html(data);
                $('#adminAccessModal').modal();
                // $('.permission_dropdown').selectpicker();
            }
        });
    });

    $('#wallets').multiselect({
        texts: {
            placeholder: 'Select Location(s)', // text to use in dummy input
            search: 'Search Location(s)',         // search input placeholder text
            // selectedOptions: ' selected',      // selected suffix text
            selectAll: 'Select All Location(s)',     // select all text
            unselectAll: 'Unselect All Location(s)',   // unselect all text
            // noneSelected   : 'None Selected'   // None selected text
        },
        search: false,
        selectAll: true, // add select all option
        minHeight: 200,   // minimum height of option overlay
        maxHeight: 300,  // maximum height of option overlay
        maxWidth: 212,  // maximum width of option overlay (or selector)
        maxPlaceholderOpts: 2,    // maximum number of placeholder options to show until "# selected" shown instead
        checkboxAutoFit: true,  // auto calc checkbox padding
        onOptionClick: function (element, option) {
            $('#wallets-error').text('');
        },
    });

    var locs = [];
    $("body").delegate('#locs_dropdown a', 'click', function (event) {
        form_type = $('#form_type').val();
        if (form_type == 'true') {
            a = $('#locs_show_selected').text();
            b = a.trim();
            locs = b.split(',');
            locs = locs.filter(e => e !== 'Select Location');
        }
        // $( '#locs_dropdown a' ).on( 'click', function( event ) {
        var $target = $(event.currentTarget),
            val = $target.attr('data-value'),
            $inp = $target.find('input'),
            idx;
        console.log($inp);
        if (val === 'all') {
            locs = [];
            setTimeout(function () {
                $('.locs_checkboxes').prop('checked', true)
            }, 0);
            $("#locs_dropdown a").each(function () {
                locs.push($(this).attr('data-value'));
            });
            locs.shift();
        } else {
            console.log(locs);
            if ((idx = locs.indexOf(val)) > -1) {
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
                locs.splice(idx, 1);
            } else {
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
                locs.push(val);

            }
            $(event.target).blur();
        }
        if (locs.length === 0) {
            $('#locs_show_selected').text('Select Location');
        } else {
            $('#locs_show_selected').text(locs.join(','));
        }
        console.log(locs);
        return false;
    });

    $("body").delegate('.modal', 'hidden.bs.modal', function () {
        locs = [];
        // $(".modal").on("hidden.bs.modal", function () {
        if ($('.modal.show').length) {
            setTimeout(function () {
                $('#body--aa').addClass('modal-open');
            }, 50);
        }
    });


    $("body").delegate('#submit_btn', 'click', function () {
        $.validator.addMethod("email",function(t,e){return!!/^([a-zA-Z0-9_+\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(t)},"Please enter a valid Email.");
        $('#employee-new').validate({
            errorClass: 'error-message',
            errorPlacement: function (error, element) {
                if (element.attr("name") == "user[permission_id]") {
                    error.insertBefore($("#permission-error-placement"));
                    $("#permission-error-placement").html(error);
                } else if (element.attr("name") == "user[wallets][]") {
                    error.insertBefore($("#location-error-placement"));
                    $("#location-error-placement").html(error);
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                "user[first_name]": {
                    required: true
                },
                "user[last_name]": {
                    required: true
                },
                "user[email]": {
                    required: true,
                    email: true
                },
                "user[password]": {
                    required: true
                },
                "user[password_confirmation]": {
                    required: true
                },
                "user[phone_number]": {
                    required: true
                },
                "user[permission_id]": {
                    required: true
                },
                "user[wallets][]": {
                    required: true
                }
            },
            messages: {
                "user[first_name]": {
                    required: "Please select First Name"
                },
                "user[last_name]": {
                    required: "Please select Last Name"
                },
                "user[email]": {
                    required: "Please select email"
                },
                "user[password]": {
                    required: "Please Enter Password"
                },
                "user[password_confirmation]": {
                    required: "Please Enter Password Confirmation",
                    equalTo: "#password"
                },
                "user[phone_number]": {
                    required: "Please select Phone Number"
                },
                "user[permission_id]": {
                    required: "Please select Permissions"
                },
                "user[wallets][]": {
                    required: "Please select Location"
                }
            },
            submitHandler: function (form) {
                if ($(form).valid()) {
                    var phone_code = $("#phone_number").intlTelInput("getSelectedCountryData").dialCode;
                    $('#user_phone_code').val(phone_code);
                    $('.add-employee-processing').html("").append("Add Employee &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
                }
                form.submit();
            }
        });

    });
    $("body").delegate('.add_btn','click',function(){
        if($("#employee-new").valid()) {
            $('.add_employe_btn').prop('disabled', true);
        }
        else {
            $('.add_employe_btn').prop('disabled', false);
        }
    });

    $("body").delegate('#edit-employee', 'click', function () {
        $.validator.addMethod("email",function(t,e){return!!/^([a-zA-Z0-9_+\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(t)},"Please enter a valid Email.");
        $('#employee-edit').validate({
            errorClass: 'error-message',
            errorPlacement: function (error, element) {
                if (element.attr("name") == "user[permission_id]") {
                    error.insertBefore($("#edit-permission-error-placement"));
                    $("#edit-permission-error-placement").html(error);
                } else if (element.attr("name") == "user[wallets][]") {
                    error.insertBefore($("#edit-location-error-placement"));
                    $("#edit-location-error-placement").html(error);
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                "user[first_name]": {
                    required: true
                },
                "user[last_name]": {
                    required: true
                },
                "user[email]": {
                    required: true,
                    email: true
                },
                "user[phone_number]": {
                    required: true
                },
                "user[permission_id]": {
                    required: true
                },
                "user[wallets][]": {
                    required: true
                }
            },
            messages: {
                "user[first_name]": {
                    required: "Please select First Name"
                },
                "user[last_name]": {
                    required: "Please select Last Name"
                },
                "user[email]": {
                    required: "Please select email"
                },
                "user[phone_number]": {
                    required: "Please select Phone Number"
                },
                "user[permission_id]": {
                    required: "Please select Permissions"
                },
                "user[wallets][]": {
                    required: "Please select Location"
                }
            },
            submitHandler: function (form) {
                if ($(form).valid()) {
                    var phone_code = $("#phone_number").intlTelInput("getSelectedCountryData").dialCode;
                    $('#user_phone_code').val(phone_code);
                    $('#edit-employee').html("").append("Save Changes &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
                }
                form.submit();
            }
        });
    });


});

var xSeconds = 20000;
setTimeout(function() {
    $('#alert-message').fadeOut('slow');
    $('#alert-message').hide();
}, xSeconds);


$(document).ready(function () {

    $("body").delegate('#user_password_confirmation','focusin ',function(){
        $('#tooltips').removeClass('tooltip_display');
        $('#tooltips').addClass('tooltip_display_hide');
    });

    $("body").delegate('#user_password','focusout', function () {
        $('#tooltips').removeClass('tooltip_display');
        $('#tooltips').addClass('tooltip_display_hide');
    });
    $(document).on("keyup", ".user_password", function () {
        $('#tooltips').removeClass('tooltip_display_hide');
        $('#tooltips').addClass('tooltip_display');
        var upperCase= new RegExp('[A-Z]');
        var lowerCase= new RegExp('[a-z]');
        var numbers = new RegExp('[0-9]');
        var v_digits = false;
        var v_numbers = false;
        var v_upcase = false;
        var v_downcase = false;
        if(value.length >= 8){
            $("#8_characters").removeClass('fa-times').addClass('fa-check btm');
            $("#8_characters").parent().removeClass('invalid').addClass('valid');
            v_digits = true;
        }else{
            v_digits = false;
            $("#8_characters").addClass('fa-times').removeClass('fa-check');
            $("#8_characters").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(upperCase)) {
            $("#upcase").removeClass('fa-times').addClass('fa-check btm');
            $("#upcase").parent().removeClass('invalid').addClass('valid');
            v_upcase = true;
        }else{
            v_upcase = false;
            $("#upcase").addClass('fa-times').removeClass('fa-check');
            $("#upcase").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(lowerCase)) {
            $("#downcase").removeClass('fa-times').addClass('fa-check btm');
            $("#downcase").parent().removeClass('invalid').addClass('valid');
            v_downcase = true;
        }else{
            v_downcase = false;
            $("#downcase").addClass('fa-times').removeClass('fa-check');
            $("#downcase").parent().addClass('invalid').removeClass('valid');
        }
        if(value.match(numbers)){
            $("#1_number").removeClass('fa-times').addClass('fa-check btm');
            $("#1_number").parent().removeClass('invalid').addClass('valid');
            v_numbers = true;
        }else{
            v_numbers = false;
            $("#1_number").addClass('fa-times').removeClass('fa-check');
            $("#1_number").parent().addClass('invalid').removeClass('valid');
        }
        if(v_digits && v_numbers && v_upcase && v_downcase){
            return true;
        }else{
            return false;
        }
    });

    $(document).on("keyup", "#user_password_confirmation", function () {
        if ($('#user_password_confirmation').val() !== $('#user_password').val()) {
            $('#error').html("").append("<span class='error-message'> Passwords do not match.</span>");
            $(this).css("border", "1px solid red");
        } else if ($('#user_password_confirmation').val() === $('#user_password').val()) {
            $('#error').html("");
            $(this).css("border", "1px solid green");
            $('#tooltips').hide(300)
        }
    });

    $("body").delegate('.submit-confirmation', 'click', function () {
        if ($('#user_password_confirmation').val() !== $('#user_password').val()) {
            $('#error').html("").append("<span class='error-message'> Password confirmation not successful</span>");
        } else if ($('#user_password_confirmation').val() === $('#user_password').val()) {
            $(".auctionform").submit();
        }
        return false;

    });
});