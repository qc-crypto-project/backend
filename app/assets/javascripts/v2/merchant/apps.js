$(document).ready(function() {
    $("#locations").on('change',function () {
        $('.fee_loader').html("").append(" &nbsp;<i class=\"fa fa-circle-o-notch fa-spin\"></i>");
        var location = $("#locations").val();
        $.ajax({
            url: '/v2/merchant/location_fee',
            type: 'GET',
            data: {id: location},
            success: function(data) {
                $('.fee_loader').html("");
                $("#location_name").text(data.location.business_name);
                $("#transaction_fee_app_dollar").text("$"+data.fee.transaction_fee_app_dollar);
                $("#transaction_fee_app").text(data.fee.transaction_fee_app +"%");
                $("#transaction_fee_dcard_dollar").text("$"+data.fee.transaction_fee_dcard_dollar);
                $("#transaction_fee_dcard").text(data.fee.transaction_fee_dcard +"%");
                $("#transaction_fee_ccard_dollar").text("$"+data.fee.transaction_fee_ccard_dollar);
                $("#transaction_fee_ccard").text(data.fee.transaction_fee_ccard +"%");
                if(!isNaN(parseFloat(data.fee.invoice_card_fee_perc))){
                    $("#invoice_card_fee_perc").text(data.fee.invoice_card_fee_perc.toFixed(2) +"%");
                }else{
                    $("#invoice_card_fee_perc").text("0.00%");
                }
                if(!isNaN(parseFloat(data.fee.invoice_card_fee))){
                    $("#invoice_card_fee").text("$"+data.fee.invoice_card_fee.toFixed(2));
                }else{
                    $("#invoice_card_fee").text("$0.00");
                }
                if(!isNaN(parseFloat(data.fee.invoice_rtp_fee_perc))){
                    $("#invoice_rtp_fee_perc").text(data.fee.invoice_rtp_fee_perc.toFixed(2) +"%");
                }else{
                    $("#invoice_rtp_fee_perc").text("0.00%");
                }
                if(!isNaN(parseFloat(data.fee.invoice_rtp_fee))){
                    $("#invoice_rtp_fee").text("$"+data.fee.invoice_rtp_fee.toFixed(2));
                }else{
                    $("#invoice_rtp_fee").text("$0.00");
                }
                if(!isNaN(parseFloat(data.fee.rtp_fee_perc))){
                    $("#rtp_fee_perc").text(data.fee.rtp_fee_perc.toFixed(2) +"%");
                }else{
                    $("#rtp_fee_perc").text("0.00%");
                }
                if(!isNaN(parseFloat(data.fee.rtp_fee))){
                    $("#rtp_fee").text("$"+data.fee.rtp_fee.toFixed(2));
                }else{
                    $("#rtp_fee").text("$0.00");
                }
                if(!isNaN(parseFloat(data.fee.invoice_debit_card_fee_perc))){
                    $("#invoice_debit_card_fee_perc").text(data.fee.invoice_debit_card_fee_perc.toFixed(2) +"%");
                }else{
                    $("#invoice_debit_card_fee_perc").text("0.00%");
                }
                if(!isNaN(parseFloat(data.fee.invoice_debit_card_fee))){
                    $("#invoice_debit_card_fee").text("$"+data.fee.invoice_debit_card_fee.toFixed(2));
                }else{
                    $("#invoice_debit_card_fee").text("$0.00");
                }

                $("#b2b_fee").text(data.fee.b2b_fee+"%");
                $("#giftcard_fee").text("$"+data.fee.giftcard_fee);
                $("#send_check").text("$"+data.fee.send_check);
                $("#redeem_fee").text(data.fee.redeem_fee +"%");
                $("#failed_check_fee").text("$"+data.fee.failed_check_fee);
                $("#ach_fee_dollar").text("$"+data.fee.ach_fee_dollar);
                $("#ach_fee").text(data.fee.ach_fee +"%");
                $("#failed_ach_fee").text(data.fee.failed_ach_fee +"%");

                $("#dc_deposit_fee_dollar").text("$"+data.fee.dc_deposit_fee_dollar);
                $("#dc_deposit_fee").text(data.fee.dc_deposit_fee +"%");
                $("#failed_push_to_card_fee").text(data.fee.failed_push_to_card_fee +"%");

                $("#add_money_check").text("$"+data.fee.add_money_check);
                $("#redeem_check").text(data.fee.redeem_check +"%");
                $("#reserve_fee").text(data.fee.reserve_fee+"%");
                $("#days").text(data.fee.days +" days");
                $("#charge_back_fee").text("$"+data.fee.charge_back_fee);
                $("#retrievel_fee").text("$"+data.fee.retrievel_fee);
                $("#refund_fee").text("$"+data.fee.refund_fee);
                $("#service").text("$"+data.fee.service);
                $("#statement").text("$"+data.fee.statement);
                $("#misc").text("$"+data.fee.misc);
                $("#statement_date").text(data.statement_date);
                $("#misc_date").text(data.misc_date);
                $("#service_date").text(data.service_date);

            },
            error: function(data){
                $('.fee_loader').html("")

            }
        });
    });
    $(document).on('click','.seq_id', function () {
        var id = $(this).attr('id');
        var data = $('.'+id).text();
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(data).select();
        document.execCommand("copy");
        $temp.remove();
    })
});