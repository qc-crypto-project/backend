$(document).ready(function () {
    var lt = $('.timeutc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    // $('input[name="query[date]"]').daterangepicker({
    //     locale: {
    //         format: 'MM/DD/YYYY',
    //     },
    //     maxDate: new Date(),
    // });
    
    if ($("#params_q").val() != "") {
        if ($("#query_date").val() == "" || $("#query_date").val() == undefined) {
            $('input[name="query[date]"]').val('');
        }
        else{
            $('input[name="query[date]"]').val($("#query_date").val());
        }
    }
    // $('#date-value').on('cancel.daterangepicker', function(ev, picker) {
    //     $(this).val('');
    // });
    $(document).on('click','.clickable_row',function (){
        window.location.href=$(this).data('path');

    });
});