$(document).on('change click','#add_new_card_check', function () {
    if($(this).prop("checked") == true){
        $("#Add_new_div_fields").show();
    }
    else if($(this).prop("checked") == false){
        $("#Add_new_div_fields").hide();
    }
});

if($("#add_new_card_check").prop("checked") == true){
    $("#Add_new_div_fields").show();
}
else if($("#add_new_card_check").prop("checked") == false){
    $("#Add_new_div_fields").hide();
}
// this is for file input js
var dropZoneId = "drop-zone";
var buttonId = "clickHere";
var mouseOverClass = "mouse-over";
var dropZone = $("#" + dropZoneId);
var inputFile = dropZone.find("input");
var finalFiles = {};
$(function() {
    inputFile.on('change', function(e) {
        finalFiles = {};
        $('#filename').html("");
        var fileNum = this.files.length,
            initial = 0,
            counter = 0;
        $.each(this.files,function(idx,elm){
            finalFiles[idx]=elm;
        });
        for (initial; initial < fileNum; initial++) {
            counter = counter + 1;
            $('#filename').append('<div class="file_div" id="file_'+ initial +'"> <span class="fa-stackk fa-lg"><img alt="file" id="file-doc" src="/assets/merchant/icons/doc-file.svg"> </span>' + this.files[initial].name + '&nbsp;&nbsp;<div class="fa fa-trashh fa-lg closeBtn" title="remove"><img alt="trash" src="/assets/merchant/icons/file-trash.svg"></div></div>');
        }
    });
})
$('div').on("click", ".closeBtn", function (e) {
    removeLine($(this))
});
function removeLine(obj)
{
    inputFile.val('');
    var jqObj = $(obj);
    var container = jqObj.parent('div');
    var index = container.attr("id").split('_')[1];
    container.remove();
    delete finalFiles[index];
    var array_values = new Array();
    for (var key in finalFiles) {
        array_values.push(finalFiles[key]);
    }
    customer_dispute_evidences_attributes_0_document.files = new FileListItem(array_values)
}
function FileListItem(a) {
    a = [].slice.call(Array.isArray(a) ? a : arguments)
    for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
    if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
    for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
    return b.files
}
$("#upload_icon").click(function() {
    $("#customer_dispute_evidences_attributes_0_document").click();
})
// file input end here