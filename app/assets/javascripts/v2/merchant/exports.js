$(document).ready(function () {
    $("#filter_form input[type='checkbox']").prop("checked", true)
    var globalTimeout = null;
    $(document).on('keyup','.search_input_datatable',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
        search_call("search")
        }, 1500);
    });
});
function search_call(type){

        globalTimeout = null;
        var search_value = $('.search_input_datatable').val();
        search_value = search_value.trim()
        data = {"q[data_search]": search_value,filter: $("#filter option:selected").val(),search_form:'true'};
        $('#filter_form input[type="checkbox"]').each(function(index){
            if($('#filter_form input[type="checkbox"]')[index].checked){
                data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
            }
        })
        var filter_data = "";
        if(filter != ""){
            filter_data = filter;
        }else{
            filter_data = 10;
        }

            $.ajax({
                url: '/v2/merchant/exports',
                type: 'GET',
                data: data,
                success: function (data) {
                    $('#exports_list').html('').html(data);
                    $("#m_table_1").DataTable();
                    $("#page").selectpicker();
                    $("#filter").selectpicker();
                    $('.search_input_datatable').val(search_value);
                    $('.search_input_datatable').focus();
                    if(type == "status"){
                        $(".btn-status").trigger('click');
                        $('#filter_form input[type="checkbox"]').removeAttr("disabled");
                    }
                }, error: function (data) {


                }
            });


}
$(document).on('change','#filter_form input[type="checkbox"]',function () {
    $('#filter_form input[type="checkbox"]').attr("disabled",true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#filter_form input[type="checkbox"]').prop("checked", false)
            $("#filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    search_call("status");
});