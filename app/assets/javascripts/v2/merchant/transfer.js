var config = window.config = {};
config.validations = {
    debug: false,
    errorClass:'has-error',
    validClass:'success1',
    errorElement:"span",

    // add error class
    highlight: function(element, errorClass, validClass) {
        $(element).parents("div.form-group")
            .addClass(errorClass)
            .removeClass(validClass);
    },

    // add error class
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents(".has-error")
            .removeClass(errorClass)
            .addClass(validClass);
    },

    // submit handler
    submitHandler: function(form) {
        form.submit();
    }
};


function validateTransferForm(role) {
    $.validator.addMethod('minStrict', function (value, el, param) {
        return value > param;
    });
    $(function() {
        var formRules = {
            rules: {
                "transfer[from_wallet]": {
                    required: true,
                },
                "transfer[to_wallet]": {
                    required: true,

                },
                "transfer[amount]": {
                    required: true,
                    minStrict: 0,

                }
            },
            messages: {
                "transfer[from_wallet]": {
                    required: "Please select Source wallet",
                },
                "transfer[to_wallet]": {
                    required: "Please select Destination wallet",

                },
                "transfer[amount]": {
                    required: "Please enter Amount",
                    minStrict: "Amount should be greater than 0",


                }
            },

        };
        $.extend(formRules, config.validations);
        $('#' + role).validate(formRules);
    });
};

function showWalletBalance(wallet_id, type) {
    $.ajax({
        url: "/v2/merchant/accounts/show_merchant_balance",
        type: "GET",
        data: {source: wallet_id},
        success: function (data) {
            if (data.balance != "") {
                $('#' + type + '_available_amount').text('$ '+ data.balance + '');
                $('#' + type + '_available_amount').val(data.balance_val);

            } else {
                $('#' + type + '_available_amount').text('$ 0.00');
            }
        },
        error: function (data) {
            $('#' + type + '_available_amount').text('$ 0.00');
        }
    });
}

$('#transfer_from_wallet').change(function(){
    var source=$(this).val();
    if(!isNaN(parseInt(source)))
    {
        showWalletBalance(source, 'from');
    }
    else
    {
        $('#from_available_amount').text('$ 0.00');
    }
});

$('#transfer_to_wallet').change(function(){
    var source=$(this).val();
    if(!isNaN(parseInt(source)))
    {
        showWalletBalance(source, 'to');
    }
    else
    {
        $('#to_available_amount').text('$ 0.00');
    }
});

validateTransferForm("new_transfer");

$("#submit_transfer_btn").attr("disabled",true);

$("#submit_transfer_btn").on("click",function (e) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });
    e.preventDefault();

    if ($("#new_transfer").valid()) {
        var transfer_from = $("#transfer_from_wallet option:selected").text();
        var transfer_to = $("#transfer_to_wallet option:selected").text();
        var transfer_amount = $("#transfer_amount").val();
        var transfer_date = $("#transfer_date").val();
        var memo = $("#memo").val();
        var available_amount_view = $("#from_available_amount").text();
        var available_amount = $("#from_available_amount").val();
        var remain_balance = available_amount - transfer_amount;
        if (remain_balance >= 0) {
            remain_balance = formatter.format(remain_balance);
            transfer_amount = formatter.format(transfer_amount);


            $("#AccountTransferModal").modal('hide');

            $("#trans_from").text(transfer_from);
            $("#transf_to").text(transfer_to);
            $("#trans_date").text(transfer_date);
            $("#trans_memo").text(memo);
            $("#avail_balance").text(available_amount_view);
            $("#trans_amount").text(transfer_amount);
            $("#total_amount").text(transfer_amount);
            $("#remain_balance").text(remain_balance);

            // $("#trans_from").val();

            $("#ConfirmAccountTransferModal").modal('show');
        }else{
            $("#transfer_amount-error").text("Insufficient Balance");
        }
    };

});

$("#form_submit").on("click",function () {

    $("#new_transfer").submit();
});

$("#transfer_from_wallet,#transfer_to_wallet").on("change",function () {
    var transfer_from = $("#transfer_from_wallet option:selected").text();
    var transfer_to = $("#transfer_to_wallet option:selected").text();
    if (transfer_to == transfer_from ){
        $("#wallet_error").text("Wallets cannot be same!")
    }else{
        $("#wallet_error").text("");

    }
});

// $("#transfer_close").on("click",function () {
//     $("#new_transfer").trigger("reset");
//     $("#submit_transfer_btn").attr("disabled",true);
//     $("#transfer_amount-error").text("");
//     $("#wallet_error").text("");
//     $("#from_available_amount").empty();
//     $("#to_available_amount").empty();
//     $(".m_selectpicker").selectpicker('refresh');
// });



$('#transfer_amount').on('keypress keyup blur',function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
    var input = $(this).val();
    if ((input.indexOf('.') != -1) && (input.substring(input.indexOf('.')).length > 2)) {
        event.preventDefault();
    }
    $(this).val($(this).val().replace(/(\.[0-9][0-9])([0-9])/, "$1"));

});

$("#transfer_amount,#transfer_from_wallet,#transfer_to_wallet").on("change blur keypress keyup",function () {
    var transfer_from = $("#transfer_from_wallet option:selected").val();
    var transfer_to = $("#transfer_to_wallet option:selected").val();
    var transfer_amount = $("#transfer_amount").val();
    if (transfer_from == "" || transfer_to == "" || transfer_amount == "" || (transfer_from == transfer_to )){
        $("#submit_transfer_btn").attr("disabled",true);
    }else{
        $("#submit_transfer_btn").attr("disabled",false);
    }
});

$('#transfer_amount').on("cut copy paste",function(e) {
    e.preventDefault();
})