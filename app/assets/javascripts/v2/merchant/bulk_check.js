//= require cable
//= require v2/merchant/bootstrap-select
//= require v2/merchant/bootstrap-datepicker

function custom_validate(){
    $("#edit_check_form").validate({
        rules: {
            name: "required",
            last_name: "required",
            phone: "required",
            email: "required",
            account_number: {
                required: true,
                minlength: 3,
                maxlength: 24,
            },
            amount: "required"
        },
        messages: {
            name: "Please enter Name",
            last_name: "Please enter Last Name",
            phone: "Please enter Phone Number",
            email: "Please enter Email",
            account_number: {
                required: "Please enter Account Number",
                minlength: "Please enter min 3 Numbers",
            },
            amount: "Please enter Amount"
        }
    })  
}  

$("body").delegate(".two_decimal", "input", function () {
    this.value = this.value.match(/\d{0,}(\.\d{0,2})?/)[0];
});

$(document).on("keyup","#account_number", function(e) {
    this.value = this.value.replace(/[^\d\\]/g,'');
})
$(document).on("keyup","#phone", function(e) {
    this.value = this.value.replace(/[^\d\\]/g,'');
})

$(document).on("keyup","#amount", function(e) {
    if(parseFloat($(this).val()) > parseFloat( $("#from_wallet option:selected").data("balance").toString().replace(/[^0-9.]/, ''))){
       $('.amount_limit_error').html('').text("Insufficient Balance");
    }else{
       $('.amount_limit_error').html('')
    }
})


$(document).on('hidden.bs.modal', function (event) {
  if ($('.modal:visible').length) {
    $('body').addClass('modal-open');
  }
});


$(document).ready(function () {

    if (window.location.href.indexOf("search") < 1) {
        $('#filter_form input[type="checkbox"]').prop("checked",true);
        $('#befor_filter_form input[type="checkbox"]').prop("checked",true);
    }

    globalTimeout = 5000

    if($(".m-content").length > 1){
        $(".m-content").first().removeClass();
    }

    $(document).on('change','#filter',function () {
            $('#filter_form input[type="checkbox"]').each(function(index){
        if($('#filter_form input[type="checkbox"]')[index].checked){
            $('#bulk_checks_filter').append("<input type='hidden' name="+$('#filter_form input[type="checkbox"]')[index].name+" value='"+
                     $('#filter_form input[type="checkbox"]')[index].value+"' />");
        }
    })
         $('#bulk_checks_filter').submit();
    })

    //----------------show_bulk.html.erb--------------
    $(".resendModalDialog .resend-button12").prop('disabled',true);
    $(".detail_check").prop('disabled',false);
    $(".view_check_details").prop('disabled',false);

    //-------------------------bulk_checks.html.erb----------


    //------------------bulk_checks.html.erb

    $("#send_bulk").click(function () {
        if($("#from_wallet option:selected").data("block_check") == true){
            $("#send_bulk").attr("disabled",true);
            $(".location_block_error").css("display","block");
            return false
        }else{

            if($(this).attr("disabled") == "disabled"){
                return false
            }else{
                $("#SuccessAccountTransferModal").modal("show")
                $("#message-process").empty();
                $('#message-process').append("Processing");
                $('#message-process').addClass("color-orange");
                $("#processing-1243").addClass("color-orange");
                $("#total-1243").addClass("color-orange");
                $(".edit-1243").text("");
                $(".remove-1243").text("");

                var wallets_id = $("#from_wallet").val();
                var inst_id = $("#instant_id").val();
                document.getElementById("send_bulk").disabled = true;
                $.ajax({
                    url: "/v2/merchant/bulk_checks",
                    method: 'post',
                    data: {bulk_instance_id: inst_id, wallet_id: wallets_id}
            });
            }
        }
    });

    $("#from_wallet").on("change", function () {
        $(".location_block_error").css("display","none");
        balance = $(this).children(":selected").data().balance;
        fee = $(this).children(":selected").data().fee;
        total = $(this).children(":selected").data().total;
        if($("#from_wallet").val() == ""){
            $("#balance").text("");
            $("#feex").text("");
            $("#t_amount").text("");
        }else{
            $("#balance").text("$"+balance);
            $("#feex").text("$"+fee);
            $("#t_amount").text("$"+total);
        }

        var wallet_bal = parseFloat($("#balance").text().replace(/,/g, ""));
        var check_bal = parseFloat($("#t_amount").text().replace(/,/g, ""));

        if(wallet_bal < check_bal){
            $("#send_bulk").attr("disabled",true)
        }else if($('#from_wallet').val() == ""){
            $("#send_bulk").attr("disabled",true)
        }else{
            $("#send_bulk").attr("disabled",false)
        }

    });

    var wallet_bal = parseFloat($("#balance").text().replace(/,/g, ""));
    var check_bal = parseFloat($("#t_amount").text().replace(/,/g, ""));
    if(wallet_bal < check_bal){
        $("#send_bulk").attr("disabled",true)
    }else if($('#from_wallet').val() == ""){
        $("#send_bulk").attr("disabled",true)
        $("#feex").text("");
        $("#t_amount").text("");
        $("#balance").text("");
    }else{
       $("#send_bulk").attr("disabled",false)
    }

})


$("body").delegate("#bulk_check_delete","click",function(){
    id = $(this).data("id")
    if($(".total_checks").text()=="1"){
        remain_balance = $("#from_wallet option:selected").data("balance").toString().replace(/[^0-9.]/, '');
    }else{
        total_fee = $("#feex").text().replace(/[^0-9,.]/, '')/$(".total_checks").text()
        total_amount = $("#amount_"+id).text().replace(/[^0-9,.]/, '')
        balance = $("#from_wallet option:selected").data("balance").toString().replace(/[^0-9.]/, '')
        remain_balance = parseFloat(balance) - (parseFloat(total_fee) + parseFloat(total_amount));
    }
    $("span.name").text($("#name_"+id).text())
    $("span.phone").text($("#phone_"+id).text())
    $("span.email").text($("#email_"+id).text())
    $("span.account_number").text($("#acc_"+id).text())
    $("span.remain_balance").text($("#amount_"+id).text())
    $('a.remove_bulk_check').attr('id', id);

    $("#deleteBulkModal").modal("show")
    
});

$("body").delegate(".remove_bulk_check","click",function(){
    id = $('a.remove_bulk_check').attr('id')
    wallet_id = $("#from_wallet").children(":selected").val();
    $("#send_bulk").attr("disabled",true)
    $("#check-"+id).text("Deleting....")
    $.ajax({
        url: "/v2/merchant/bulk_checks/" + id,
        type: 'DELETE',
        data: {wallet_id: wallet_id},
        success: function (data) {
            if(data["selected_wallet_data"] != null && data["selected_wallet_data"][0] != null){
                // $("#balance").text("").text("$"+parseFloat(data["selected_wallet_data"][0]["balance"]).toFixed(2));
                $("#feex").text("").text("$"+parseFloat(data["selected_wallet_data"][0]["fee"]).toFixed(2));
                total_amount_f = parseFloat(data["selected_wallet_data"][0]["fee"]) + parseFloat(data["total_amount"])
                $("#t_amount").text("").text("$"+parseFloat(total_amount_f).toFixed(2));
                $("#t_checks_amount_12").text("").text("$"+parseFloat(data["total_amount"]).toFixed(2));
                $(".total_checks").text($(".total_checks").text()-1)
            }

            $('#row_'+id).remove();

            var wallet_bal = parseFloat($("#balance").text().replace(/[^0-9.,]/, ''));
            var check_bal = parseFloat($("#t_amount").text().replace(/[^0-9.,]/, ''));

            if(wallet_bal < check_bal){
                $("#send_bulk").attr("disabled",true)
            }
            else if($('#from_wallet').val() == ""){
                $("#send_bulk").attr("disabled",true)
                $("#feex").text("");
                $("#t_amount").text("");
                $("#balance").text("");
            }
            else{
                $("#send_bulk").attr("disabled",false)
            }
            if(parseFloat($(".total_checks").text()) <= 0){
                $("#send_bulk").attr("disabled",true)
            }
            filter = 10;
            page = 1;
            import_pagination();
        }
    });
});

$("body").delegate("#update_bulk_check","click" ,function(e){
    custom_validate();
  if($("#edit_check_form").valid() && $('.amount_limit_error').text() == ''){
    $("#editBulkModal").modal("hide");
    var id = $(this).data("id");
    $("#check-"+id).text("Updating....")

    $("#check-"+id).removeClass("paid")
    $("#check-"+id).addClass("pending")

    var name = $("#name").val();
    var last_name = $("#last_name").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var account_number = $("#account_number").val();
    var routing_number = $("#routing_number").val();
    var wallet_id = $("#wallet_id").val();
    var amount = $("#amount").val();
    var confirm_account = $("#confirm_account_number").val();
    $("#send_bulk").attr("disabled",true)
    $.ajax({
      url: "/v2/merchant/bulk_checks/"+id,
      method: "PATCH",
      data: {name : name,last_name : last_name,phone : phone ,email: email , account_number : 
          account_number , amount : amount , routing_number : routing_number , wallet_id : wallet_id,confirm_account : confirm_account
      },
      success: function(data){
        if(data["check"] != null){          
          $("#name_"+data["check"]["id"]).text(data["check"]["name"]);
          $("#lname_"+data["check"]["id"]).text(data["check"]["last_name"]);
          $("#phone_"+data["check"]["id"]).text(data["check"]["phone"]);
          $("#email_"+data["check"]["id"]).text(data["check"]["email"]);
          $("#amount_"+data["check"]["id"]).text("$ "+parseFloat(data["check"]["amount"]).toFixed(2));
          $("#rout_"+data["check"]["id"]).text(data["check"]["routing_number"]);
          $("#acc_"+data["check"]["id"]).text(data["check"]["account_number"]);
          $("#check-"+data["check"]["id"]).text("Ready")
          $("#check-"+id).removeClass("pending")
            $("#check-"+id).addClass("paid")
        }
        if(data["selected_wallet_data"] != null && data["selected_wallet_data"][0] != null){
            // $("#balance").text("").text("$"+parseFloat(data["selected_wallet_data"][0]["balance"]).toFixed(2));
            $("#feex").text("").text("$"+parseFloat(data["selected_wallet_data"][0]["fee"]).toFixed(2));
            total_amount_f = parseFloat(data["selected_wallet_data"][0]["fee"]) + parseFloat(data["total_amount"])
            $("#t_amount").text("").text("$"+parseFloat(total_amount_f).toFixed(2));
            $("#t_checks_amount_12").text("").text("$"+parseFloat(data["total_amount"]).toFixed(2));
        }

        var wallet_bal = parseFloat($("#balance").text().replace(/[^0-9]/, ''));
        var check_bal = parseFloat($("#t_amount").text().replace(/[^0-9]/, ''));
        if(wallet_bal < check_bal){
            $("#send_bulk").attr("disabled",true)
        }
        else if($('#from_wallet').val() == ""){
            $("#send_bulk").attr("disabled",true)
            $("#feex").text("");
            $("#t_amount").text("");
            $("#balance").text("");
        }
        else{
            $("#send_bulk").attr("disabled",false)
        }


      }
    });
  }
});


    $("body").delegate("#amount","keypress",function(event){
        if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
                $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    })


$(document).on("click", "div#bulk_check_show_table  table  tbody  tr", function (ev) {
    id = $(this).data("id")
    bulk_id = $(this).data("bulk_id")
    if(id != null && bulk_id != null){
        $.ajax({
            url: "/v2/merchant/checks/"+id,
            method: "GET",
            success: function(data){
                $("#bulkcheck_detail").modal("show");
                $("#account_name").text(data["account"]);
                $("#recipient").text(data["recipient"]);
                $("#account_number").text(data["number"]);
                $("#phone_number").text(data["phone_number"]);
                $("#tango_memo").text(data["memo"]);
                $("#tango_t_amount").text("$"+data["total_amount"]);
                if(data["status"] == "PENDING"){
                    $("#void_modal").show()
                    $("#void_modal").attr("data-void_id",id)
                    $("#void_modal").attr("data-bulk_id",bulk_id)
                }else{
                    $("#void_modal").hide()
                }
            }
        });
    }
})

$(document).on("click","#yes_void",function(){
    id = $("#void_modal").data("void_id")
    bulk_id =  $("#void_modal").data("bulk_id")
    $('#link_ok').attr("href","/v2/merchant/bulk_checks/"+bulk_id)
    $.ajax({
        url: '/v2/merchant/checks/'+id,
        data: {status: "VOID"},
        method: "PATCH"
    })
});


var dropZoneId = "drop-zone";
var buttonId = "clickHere";
var mouseOverClass = "mouse-over";
var dropZone = $("#" + dropZoneId);
var inputFile = dropZone.find("input");
var finalFiles = {};

$("body").delegate("#file","change",function() {
    finalFiles = {};
    $('#filename').html("");
    var fileNum = this.files.length,
      initial = 0,
      counter = 0;

    $.each(this.files,function(idx,elm){
       finalFiles[idx]=elm;
    });

    for (initial; initial < fileNum; initial++) {
      counter = counter + 1;
      $('#filename').append('<div class="file_div" id="file_'+ initial +'"><span class="fa-stackk fa-lg"><img src="/assets/v2/merchant/icons/doc-file.svg" id="file-doc"></span>' + this.files[initial].name + '&nbsp;&nbsp;<div class="fa fa-trashh fa-lg deleteBtn" title="remove"><img src="/assets/v2/merchant/icons/file-trash.svg" ></div></div>');
    }
});


$('div').delegate(".deleteBtn","click", function (e) {
    removeLine1($(this))
});

function removeLine1(obj)
{
    inputFile.val('');
    var jqObj = $(obj);
    var container = jqObj.parent('div');
    var index = container.attr("id").split('_')[1];
    container.remove(); 
    delete finalFiles[index];
    var array_values = new Array();
    for (var key in finalFiles) {
        array_values.push(finalFiles[key]);
    }
    // file.files = new FileListItem(array_values)
    $("#file").val('')
}

function FileListItem(a) {
  a = [].slice.call(Array.isArray(a) ? a : arguments)
  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
  if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
  for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
  return b.files
}

$("body").delegate("#upload_icon","click",function() {
    $("#file").click();
})


$(document).on("click", ".show-dynamic-modal", function (ev) {
    ev.preventDefault();
    var url = $(this).data("url");
    var modal_selector = $(this).data("modal_selector");
    var modal_container_selector = $(this).data("modal_container_selector");
    var method = $(this).data("method");
    $.ajax({
        url: url,
        type: method,
        success: function (data) {
            $(modal_container_selector).empty().html(data);
            $(modal_selector).modal();
            $("#location_id").selectpicker();
            $("#bank_account_name").selectpicker();
            wallet_id = $("#from_wallet").children(":selected").val();
            $("#wallet_id").val(wallet_id);
        }
    });
    return false;
});
$(document).on("click", "#submit_button", function (ev) {
    if($("#file_0").length == 1){
        $('.close').prop("disabled",true);
        // $("#file").prop("disabled",true);
        $("#drop-zone").css("pointer-events","none");
        $("#file_portion").css("pointer-events","none");     
        $(".newbulkbox").css("pointer-events","none");
        $('#newcheckModal').data('bs.modal')._config.backdrop = 'static';
    }
});
var globalTimeout = null;

// $("body").delegate('#filter_form input[type="checkbox"]',"change",function(){
$(document).on('change','#filter_form input[type="checkbox"]',function () {
    $('#filter_form input[type="checkbox"]').attr("disabled",true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#filter_form input[type="checkbox"]').prop("checked", false)
            $("#filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    search_call("status");
});
var mutex = true;
$(document).on('keyup','.search_input_datatable',function () {
    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    if(mutex) {
        mutex = false
            var search_value = $('.search_input_datatable').val();
            search_value = search_value.trim()
            data = {"q[data_search]": search_value, filter: 10, search_form: 'true'};
            $('#filter_form input[type="checkbox"]').each(function (index) {
                if ($('#filter_form input[type="checkbox"]')[index].checked) {
                    data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
                }
            })
            $.ajax({
                url: "/v2/merchant/bulk_checks",
                type: 'GET',
                data: data,
                success: function (data) {
                    mutex = true
                    $('.table-data').html('').html(data);
                    $("#m_table_1").DataTable();
                    $("#page").selectpicker();
                    $("#total_pending").html("").html("$" + $(".total_pending").text());
                    $("#total_void").html("").html("$" + $(".total_void").text());
                    $("#total_failed").html("").html("$" + $(".total_failed").text());
                    $("#total_paid").html("").html("$" + $(".total_paid").text());
                }, error: function (data) {
                    mutex = true
                }
            });
    }
});



$(document).on('keyup','.show_datatable',function () {
    search_call("show_table")
});

function search_call(type){

    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    if(mutex) {
        mutex = false
            var search_value = $('.show_datatable').val();
            search_value = search_value.trim()
            data = {"q[data_search]": search_value, filter: $("#filter option:selected").val(), search_form: 'true'};
            $('#filter_form input[type="checkbox"]').each(function (index) {
                if ($('#filter_form input[type="checkbox"]')[index].checked) {
                    data[$('#filter_form input[type="checkbox"]')[index].name] = $('#filter_form input[type="checkbox"]')[index].value
                }
            })
            url = $("#filter_form").data("id") ? "/v2/merchant/bulk_checks/" + $("#filter_form").data("id") : "/v2/merchant/bulk_checks"

            $.ajax({
                url: url,
                type: 'GET',
                data: data,
                success: function (data) {
                    mutex = true
                    $('.table-data').html('').html(data);
                    if (type == "status") {
                        $(".btn-status").trigger('click');
                        $('#filter_form input[type="checkbox"]').removeAttr("disabled");
                    }
                    $("#m_table_1").DataTable();
                    $("#page").selectpicker();
                    $("#total_pending").html("").html("$" + $(".total_pending").text());
                    $("#total_void").html("").html("$" + $(".total_void").text());
                    $("#total_failed").html("").html("$" + $(".total_failed").text());
                    $("#total_paid").html("").html("$" + $(".total_paid").text());

                    $("#total_checks_count").html("").html($(".total_checks_count").text());
                    $("#total_checks_amount").html("").html("$" + $(".total_checks_amount").text());
                    $("#total_fee_amount").html("").html("$" + $(".total_fee_amount").text());
                    $("#total_amount").html("").html("$" + $(".total_amount").text());
                }, error: function (data) {
                    mutex = true
                }
            });
    }
}

$(document).on("click", "#bulk_check_data_id tr", function() {
    if($(this).data("link") != null){
     window.location = $(this).data("link")
    }
});


$(document).on('change','#befor_filter_form input[type="checkbox"]',function () {
    $('#befor_filter_form input[type="checkbox"]').attr("disabled",true);
    if($(this).attr("id") == "search_all"){
        if( $(this).is(":checked")) {
            $('#befor_filter_form input[type="checkbox"]').prop("checked", false)
            $("#befor_filter_form input[type='checkbox']").prop("checked", true)
        }else{
            $(this).prop("checked", false)
            $('#befor_filter_form input[type="checkbox"]').prop("checked", false)
        }
    }else{
        $('#befor_filter_form input[type="checkbox"]#search_all').prop("checked",false)
    }
    if($("#befor_filter_form input:checkbox:not(:checked)").length == 1)// when all is check except all then all checkbox will automatically checked
    {
        $("#search_all").prop("checked",true)
    }
    if($('#befor_filter_form input[type="checkbox"]:checked').length == 0)//if none is check still want to send something in order to get empty results
    {
        $("#befor_filter_form").append ('<input class="payee_checkbox d-none" type="checkbox" value="1" checked name="search[no_select]" id="no_select">');
    }
    before_search_call("status");
});



$(document).on('click','#import_pagination .pagination li',function (e) {
    e.preventDefault();
    page = $(this).find('a:first').text()
    filter = $("#filter option:selected").val()
    import_pagination()
})


$(document).on('change','.before_import_filter',function (e) {
    e.stopPropagation();
    filter = $(this).val()
    page = 1
    import_pagination()
});

$(document).on('change','#import_pagination .import_pagination',function (e) {
     e.stopPropagation();
    page = $(this).val()
    filter = $("#filter option:selected").val()
    import_pagination()
});


$(document).on('click','#import_pagination #m_table_1_next',function (e) {
    e.preventDefault();
    page = + $('ul.pagination').find('li.active').text()+1
    filter = $("#filter option:selected").val()
    import_pagination()
})

$(document).on('click','#import_pagination #m_table_1_next',function (e) {
    e.preventDefault();
    page = + $('ul.pagination').find('li.active').text()+1
    filter = $("#filter option:selected").val()
    import_pagination()
})

function import_pagination() {
    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    globalTimeout = setTimeout(function() {
        globalTimeout = null;
        var search_value = $('.before_search_input').val();
        search_value = search_value.trim()
        data = {"data_search": search_value,filter: filter,page: page ,search_form:'true'};
        $('#befor_filter_form input[type="checkbox"]').each(function(index){
            if($('#befor_filter_form input[type="checkbox"]')[index].checked){
                data[$('#befor_filter_form input[type="checkbox"]')[index].name] = $('#befor_filter_form input[type="checkbox"]')[index].value
            }
        })
        url = "/v2/merchant/bulk_checks/before_send_index?id="+$("#bulk_check_instance").val()
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
            success: function (data) {
                $('.table-data').html('').html(data);
                $('.before_search_input').val(search_value);
                $('.before_search_input').focus();
                 $("#m_table_1").DataTable();
                 $("#page").selectpicker();
            }, error: function (data) {
            }
        });
    }, 500);

}

$(document).on('keyup','.before_search_input',function () {
    before_search_call("search")
});
function before_search_call(type){

    if (globalTimeout != null) {
        clearTimeout(globalTimeout);
    }
    globalTimeout = setTimeout(function() {
        globalTimeout = null;
        var search_value = $('.before_search_input').val();
        search_value = search_value.trim()
        data = {"data_search": search_value,filter: $("#filter option:selected").val(),search_form:'true'};
        $('#befor_filter_form input[type="checkbox"]').each(function(index){
            if($('#befor_filter_form input[type="checkbox"]')[index].checked){
                data[$('#befor_filter_form input[type="checkbox"]')[index].name] = $('#befor_filter_form input[type="checkbox"]')[index].value
            }
        })
        url = "/v2/merchant/bulk_checks/before_send_index?id="+$("#bulk_check_instance").val()
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
            success: function (data) {
                $('.table-data').html('').html(data);
                if(type == "status"){
                    $(".btn-status").trigger('click');
                    $('#filter_form input[type="checkbox"]').removeAttr("disabled");
                }
                 $("#m_table_1").DataTable();
                 $("#page").selectpicker();
            }, error: function (data) {
            }
        });
    }, 500);
}

$(document).on('click', '.ranges:eq(1) ul li', function () {
    var startdate = '';
    var enddate = '';
    if($(this).text() == "Yesterday"){
        startdate = moment().subtract(1, "days")._d;
        enddate = moment().subtract(1, "days")._d;
    } else if($(this).text() == "Today") {
        startdate = moment()._d;
        enddate = moment()._d;
    } else if($(this).text() == "Last 6 Days") {
        startdate = moment().subtract(5, "days")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Month to Date") {
        startdate = moment().startOf("month")._d;
        enddate = moment()._d;
    } else if($(this).text() == "Previous month") {
        startdate =  new moment().subtract(1, 'months').date(1)._d
        enddate =  moment().subtract(1,'months').endOf('month')._d;
    }
    if($(this).text() != "Custom Range"){
        $.ajax({
            url: '/v2/merchant/bulk_checks',
            type: 'GET',
            data: {first_date: startdate, second_date: enddate, wallet_id: $("#hidden_wallet").val()},
            success: function(data) {
                $('.table-data').html(data);
                update_total();
            }
        });
    }
});
$(document).on('click', '.applyBtn:eq(1)', function () {
    var startdate = $("input[name=daterangepicker_start]").last().val();
    var enddate = $("input[name=daterangepicker_end]").last().val();
    $.ajax({
        url: '/v2/merchant/bulk_checks',
        type: 'GET',
        data: {first_date: moment(startdate)._d, second_date: moment(enddate)._d, wallet_id: $("#hidden_wallet").val(), custom: true},
        success: function(data) {
            $('.table-data').html(data);
            update_total();
        }
    });
});

function update_total(){
    $("#m_table_1").DataTable();
    $("#page").selectpicker();
    $("#total_pending").html("").html("$"+$(".total_pending").text());
    $("#total_void").html("").html("$"+$(".total_void").text());
    $("#total_failed").html("").html("$"+$(".total_failed").text());
    $("#total_paid").html("").html("$"+$(".total_paid").text());
}