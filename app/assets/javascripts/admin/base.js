// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
//
//= require js/jquery-2.1.4.min
//= require jquery_ujs
//= require tether
//= require bootstrap
//= require cable
//= require jquery.form
//= require merchant/jstz.min