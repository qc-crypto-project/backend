$.validator.addMethod('checkimagecount', function() {
    var $fileUpload = $("input[type='file']");
    if (parseInt($fileUpload.get(0).files.length)>5 || parseInt($fileUpload.get(0).files.length) == 0){
     	return false
    }else{
    	return true
    }
}, "max 5 files or min 1 file required");

validateimagecount();
// $('.m-menu__item').click(function() {
//     $('.m-menu__item').removeClass("active_menue");
//     $(this).addClass("active_menue");
// });
$(".m-menu__nav a.menu_links").each(function() {
    console.log($(this).attr('href'));
    console.log((window.location.pathname.indexOf($(this).attr('href'))));
    if ((window.location.pathname.indexOf($(this).attr('href'))) > -1) {
        $('.m-menu__item').removeClass("active_menue");
        $(this).parent().addClass('active_menue');
    }
});
function validateimagecount(){
    $(function() {
        if (!$('#new_customer_dispute').length) {
            return false;
        }
        var formRules = {
            rules: {
                // "evidences_attributes[document][]": {
                //  	checkimagecount: true
                // },
                "customer_dispute[description]":{
                    required: true
                },
                "products[][checked]":{
                    required: true
                }
            },
            messages: {
                // "evidences_attributes[document][]": {
                // },
                "customer_dispute[description]":{
                    required: "additional comment required"
                },
                "products[][checked]": {
                    required: "Please select at least one product"
                }
            }
        };

        $.extend(formRules, config.validations);
        $('#new_customer_dispute').validate(formRules);
    });

};

$('#all-products').change(function() {
  var checkboxes = $(this).closest('form').find(':checkbox');
  checkboxes.prop('checked', $(this).is(':checked'));
});


$('.arrow-up').on('click', function() {
    var value = $(this).parent().parent().find('input').val();
    var max = $(this).attr("data-max")
    if(value < max){
        $(this).parent().parent().find('input').val(parseInt(value) + 1);
    }
})

$('.arrow-down').on('click', function() {
    var value = $(this).parent().parent().find('input').val();
    if(value > 1){
        $(this).parent().parent().find('input').val(parseInt(value) - 1);
    }
})


var dropZoneId = "drop-zone";
var buttonId = "clickHere";
var mouseOverClass = "mouse-over";
var dropZone = $("#" + dropZoneId);
var inputFile = dropZone.find("input");
var finalFiles = {};

$(function() {
    inputFile.on('change', function(e) {

    finalFiles = {};
    $('#filename').html("");
    var fileNum = this.files.length,
      initial = 0,
      counter = 0;

    $.each(this.files,function(idx,elm){
       finalFiles[idx]=elm;
    });

    for (initial; initial < fileNum; initial++) {
      counter = counter + 1;
      $('#filename').append('<div class="file_div" id="file_'+ initial +'"><span class="fa-stack fa-lg"><i class="fa fa-file fa-stack-1x "></i><strong class="fa-stack-1x" >' + counter + '</strong></span>' + this.files[initial].name + '&nbsp;&nbsp;<div class="fa fa-trash fa-lg closeBtn" title="remove"></div></div>');
    }
  });
})


$('div').on("click", ".closeBtn", function (e) {
    removeLine($(this))
});

function removeLine(obj)
{
    inputFile.val('');
    var jqObj = $(obj);
    var container = jqObj.parent('div');
    var index = container.attr("id").split('_')[1];
    container.remove(); 
    delete finalFiles[index];
    var array_values = new Array();
    for (var key in finalFiles) {
        array_values.push(finalFiles[key]);
    }
    customer_dispute_evidences_attributes_0_document.files = new FileListItem(array_values)
}

function FileListItem(a) {
  a = [].slice.call(Array.isArray(a) ? a : arguments)
  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
  if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
  for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
  return b.files
}

$("#upload_icon").click(function() {
    $("#customer_dispute_evidences_attributes_0_document").click();
})

function update_notification(id){
  
    $.ajax({
        url: "/customers/disputes/update_notification/" + id,
        success: function(html){
            if (html != null && html.success) {
                $("div[data-id*='"+id+"']").removeClass("unread")
                count = parseInt($('.notification_numbers').text())
                if(count > 0){
                    $('.notification_numbers').html(count-1)
                }
            }
        }
    })
}

$('#notifications').on('mouseenter', 'div', function() {
    var id = $(this).attr('data-id')
    update_notification(id)
});



$('#yesterday-notifications').on('mouseenter', 'div', function() {
    var id = $(this).attr('data-id')
    update_notification(id)   
});

$('#older-notifications').on('mouseenter', 'div', function() {
    var id = $(this).attr('data-id')
    update_notification(id)
});

$('#alert-message').fadeOut(5000);

$("#close-alert-message").on('click',function(){
    $("#alert-message").hide();
})

$("#print_label").on('click',function(){
    $('#return_to_sender').modal('show');
    $('#return_to_sender').addClass("show");
    $('#return_to_sender').css("background-color","rgba(0, 0, 0, 0.5)");
});

// Javascript to enable link to tab
// var url = document.location.toString();
// if (url.match('#')) {
//     $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
// } 

// Change hash for page-reload
// $('.nav-tabs a').on('shown.bs.tab', function (e) {
//     window.location.hash = e.target.hash;
// })
$( "#order_date" ).datepicker();
$( "#deliver_date" ).datepicker();


$('#filter_form input[type="checkbox"]').change(function() {
    this.form.submit()
});
