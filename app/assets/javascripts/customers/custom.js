//= require intlTelInput


// Side bar icon show and dide
$('#m_aside_left_minimize_toggle').click(function () {
    $('.side_bar_close_image').toggle();
});

$(".cancel-order-dd-list").change(function () {
    if(this.value=="Other"){
        $('.cancel-reason').show();
    }
    else{
        $('.cancel-reason').hide();
    }
});

$("#user_phone_number").on('keyup change', function (){
    if($(this).val() !==''){
        $('#mobile_phone_error').hide();
        $("#mobile_pencil").hide();
        $('#user_phone_number').css("border",'background: #ffffff');
        $('#user_phone_number').css("border","2px solid rgb(53, 186, 182)");
        $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#mobile_phone_error').show();
        $("#user_phone_number").css("border", "2px solid #e88a8a");
        $("#mobile_pencil").show();
        $(".save-btn").css("cssText", "background-color: #d6d6d6");
    }
    // $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
});

if($("select#customer_dispute_reason option").filter(":selected").val() == "other"){
    $("#other_details").show();
}else{
    $("#other_details").hide();
}

$("#customer_dispute_reason").change(function(){
    if($(this).val() == "other"){
        $("#other_details").show();
    }else{
        $("#other_details").hide();
    }
});

$("#user_email").on('keyup change', function (){
    if($(this).val() !==''){
        $('#email_error').hide();
        $("#email_pencil").hide();
        $('#user_email').css("border",'background: #ffffff');
        $('#user_email').css("border","2px solid rgb(53, 186, 182)");
        // $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        $('#email_error').show();
        $("#user_email").css("border", "2px solid #e88a8a");
        $("#email_pencil").show();
        // $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");
    }
    // $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
});

$("#user_password").on('keyup change', function (){
    if($(this).val() !==''){
        $('#password_error').hide();
        $("#user_password").css("border", "2px solid #efefef");
        $('#user_password').css("border","2px solid rgb(53, 186, 182)");
        // $(".pencel_icon").hide();
        // $(".login-eye").show();
        // $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        // $(".pencel_icon").show();
        // $(".login-eye").hide();
        // $(".login-open-eye").hide();
        // $('#password_error').show();
        // $("#password_pencil").show();
        // $("#user_password").css("border", "2px solid #e88a8a");
        // $("#eye-slash").hide();
        // $("#eye-icon").hide();
        // $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");

    }
});

// var starPasswordValue;
// $(".login-eye").click(function(){
//     $(".login-open-eye").show();
//     $('.login-eye').hide();
//     starPasswordValue = $("#user_password").val();
//     $('#user_password').val($('.profilehidpassw').val());
// });
// $(".login-open-eye").click(function(){
//     $(".login-eye").show();
//     $(".login-open-eye").hide();
//     $('#user_password').val(starPasswordValue);
// });


// var starPasswordValue;
// $( ".login-eye" ).click(function() {
//     var password=$(".profilehidpassw").val();
//     starPasswordValue=$("#user_password").val();
//     $("#user_password").val(password);
//     $(".login-open-eye").show();
//     $(".login-eye").hide();
// });

// $( ".login-open-eye" ).click(function() {
//     var password=$(".profilehidpassw").val();
//     $("#user_password").val(starPasswordValue);
//     $(".login-open-eye").hide();
//     $(".login-eye").show();
// });



// var starPasswordValue;
// $(".login-eye").click(function(){
//     $(".login-open-eye").show();
//     $('.login-eye').hide();
//     starPasswordValue=$("#user_password").val();
//     $('#user_password').val($('.profilehidpassw').val());
// });
// $(".login-open-eye").click(function(){
//     $(".login-eye").show();
//     $(".login-open-eye").hide();
//     $('#user_password').val(starPasswordValue);
// });

// function createprofilestars(n) {
//     return new Array(n+1).join("*")
// }



// $(document).ready(function() {
//     var timer = "";
//     $(".profilehidpassw").attr("name", $(".profilepass").attr("name"));

//     $(".profilepass").attr("type", "text").removeAttr("name");

//     $("body").on("keypress", ".profilepass", function(e) {
//         var code = e.which;
//         if (code >= 32 && code <= 127) {
//             var character = String.fromCharCode(code);
//             $(".profilehidpassw").val($(".profilehidpassw").val() + character);
//         }
//     });
//     $("body").on("keyup", ".profilepass", function(e) {
//         $(".login-open-eye").hide();
//         var code = e.which;
//         if (code == 8) {
//             var length = $(".profilepass").val().length;
//             $(".profilehidpassw").val($(".profilehidpassw").val().substring(0, length));
//         } else if (code == 37) {

//         } else {
//             var current_val = $('.profilepass').val().length;
//             $(".profilepass").val(createprofilestars(current_val - 1) + $(".profilepass").val().substring(current_val - 1));
//         }

//         clearTimeout(timer);
//         timer = setTimeout(function() {
//             $(".profilepass").val(createprofilestars($(".profilepass").val().length));
//         }, 200);

//     });

// });

$("#user_password_confirmation").on('keyup change', function (){
    if($(this).val() !==''){
        $('#password_error').hide();
        $("#user_password_confirmation").css("border", "2px solid #efefef");
        $('#user_password_confirmation').css("border","2px solid rgb(53, 186, 182)");
        $("#password_pencil").hide();
        $("#eye-slash").show();
        // $(".save-btn").css("cssText", "background-color: #ffd95a !important;");
    }
    else
    {
        // $('#password_error').show();
        // $("#password_pencil").show();
        // $("#user_password_confirmation").css("border", "2px solid #e88a8a");
        // $("#eye-slash").hide();
        // $("#eye-icon").hide();
        // $(".save-btn").css("cssText", "background-color: #d6d6d6!important;");

    }
});

$("#eye-icon").click(function(){
    $("#eye-slash").show();
    $('#password').get(0).type = 'password';

});

$("#eye-slash").click(function(){

    $('#password').get(0).type = 'text';
    $("#eye-slash").hide();
    $('#eye-icon').show();

});

$("input[type=file]").on("click", function(e){
    e.stopPropagation();

})

$('#OpenImgUpload').click(function(e){
    $('#user_profile_image_attributes_document').trigger('click');
});


$("#user_profile_image_attributes_document").change(function(){
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#editProfile').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

    }
}


function openRightMenu() {
    document.getElementById("rightMenu").style.display = "block";
    $('body').append('<div class="m-quick-notification-sidebar-overlay"></div>');
}


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            $('#'+$(this).attr('data-id')).text('Show more');
            $('#'+$(this).attr('data-id')).css('color','rgba(0, 0, 0, 0.5)');
            $('#'+this.id).removeClass('text');
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
            $('#'+$(this).attr('data-id')).text('Show less');
            $('#'+$(this).attr('data-id')).css('color','rgba(0, 0, 0, 0.7)');
            $('#'+this.id).addClass('text');

        }
    });
}

validateaffiliateForm();

function validateaffiliateForm(){
    $(function() {
        if (!$('#edit_user').length) {
            return false;
        }
        var formRules = {
            rules: {
                "user[phone_number]": {
                    minlength: 10,
                    maxlength: 15
                },
                // "user[password]": {
                //     minlength: 8
                // },
                "user[email]": {
                    // required: {
                    //     depends:function(){
                    //         $(this).val($.trim($(this).val()));
                    //         return true;
                    //     }
                    // },
                    // email: true,
                    remote: {
                        url: "/customers/form_validate",
                        type: 'GET',
                        data: {}
                    }
                },
                "user[password_confirmation]": {
                    minlength: 8,
                    equalTo: "#user_password"
                },
            },
            messages: {
                "user[email]": {
                    // required: "Email is required",
                    remote: "This Email address already exist!"
                },

                "user[password_confirmation]": {
                    equalTo: "Confirm Password & Password should be same!"
                },
                "user[phone_number]": {
                    minlength: "Phone Number must have 10 digits",
                    maxlength: "Phone Number Only have 15 digits"
                },

            }, 
            invalidHandler: function() {
                animate({
                    name: 'shake',
                    selector: '.auth-container > .card'
                });
            }
        };

        $.extend(formRules, config.validations);
        $('#edit_user').validate(formRules);
    });

};

$("#user_profile_image_attributes_document").on("change",function(){
     if($(this).val() != ""){
        // $(".delete_profile").toggle();
        $("#delete_profile_image").prop("checked",false)
        $("i.la.la-close.delete_profile").css("display","block")
    }   
})

$(".delete_profile").click(function(e){
    $("#user_profile_image_attributes_document").val('')
    $('#editProfile').attr('src', '/assets/customers/images/profile/edit-profile.svg');
    $("#delete_profile_image").prop("checked",true)
    $("i.la.la-close.delete_profile").css("display","none")
    // $(".delete_profile").toggle();
});


$('html').click(function() {
    $('#rightMenu').hide();
    $('.m-quick-notification-sidebar-overlay').remove();
});

$('#m_header_topbar').click(function(event){
    event.stopPropagation();
});

$('#m_topbar_notification_icon').on('click',function(){
    $("div#rightMenu").show();
    $('body').append('<div class="m-quick-notification-sidebar-overlay"></div>');
});

$('#alert-message-top').fadeOut(5000);

// $("#user_phone_number").intlTelInput({
//     formatOnInit: true,
//     separateDialCode: false
// });

// $("#user_phone_number").on('keydown', function (e) {
//     if ( e.keyCode == 8 && (parseInt($('#user_phone_number').val().length) == $('.country.active').attr('data-dial-code').length+1) ){
//         return false;
//     }
//    if($('#user_phone_number').val() == ''){
//        $('#user_phone_number').val("+"+$('.country.active').attr('data-dial-code'))  
//     }
// });


// $("#edit_user").submit(function(){
//     if ($("#user_phone_number").val()) {
//         number = parseInt($("#user_phone_number").val());
//         $("#user_phone_number").val(number);
//     }
// });

$("#user_phone_number").attr('maxlength','15');

$("#edit_user input").on('keyup change', function (){
    $(".save-btn").css("background-color", "none");
    if( $('#edit_user').valid() ) {
        $(".save-btn").css("background-color", "#ffd95a ");
        $('.save-btn').css("cursor","pointer");
        $(".save-btn").attr("disabled", false);
    } else {
        $(".save-btn").css("background-color", "#d6d6d6 ");
        $('.save-btn').css("cursor","not-allowed");
        $(".save-btn").attr("disabled", true);
    }
});
$('#reset_password_click').on('click',function(){
    $('#user_password').val('');
    $('#user_password_confirmation').val('');
});
$(document).on("focusin", "#user_password", function () {
    $('.strength').css('display', "block");
});
$(document).on("focusout", "#user_password", function () {
    if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
        $('.strength').css('display', "none");

    }else if($(this).val() == ""){
        $('.strength').css('display', "none");
    }
});
$("#user_password").on("keyup", function() {
    $('#tooltips').css('display', "block");
    var password = $(this).val();
    //$("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
    //$("#tooltips").html('').append('<div id="toolti"><div class="tip"></div><p class="pwd-suggestion-title">Your password must have at least:</p><div class="password-suggesstions"><div class="require-format"></div><span class="pwd-suggestion-message">8 Characters</span></div> <div class="password-suggesstions"><div class="invalid"><i class="fa fa-times" aria-hidden="true"></i></div><span class="pwd-suggestion-message">8 Characters</span></div><div class="password-suggesstions"><div class="valid"><i class="fa fa-check" aria-hidden="true"></i></div><span class="pwd-suggestion-message">1 Number</span></div><div class="password-suggesstions"><div class="valid"><i class="fa fa-check" aria-hidden="true"></i></div><span class="pwd-suggestion-message">1 Uppercase Letter (A-Z)</span></div><div class="password-suggesstions"><div class="valid"><i class="fa fa-check" aria-hidden="true"></i></div><span class="pwd-suggestion-message">1 Lowerrcase Letter (a-z)</span> </div></div>   <div class="display-no"><li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li></div>')
    $("#tooltips").html('').append('<div class="display_no"><li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li></div>' +
        '<div class="tip"></div>\n' +
        '    <p class="pwd-suggestion-title">Your password must have at least:</p>\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="chlen"><i class="fa fa-timess" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">8 Characters</span>\n' +
        '</div>\n' +
        '\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="oneno"><i class="fa fa-checkk" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">1 Number</span>\n' +
        '</div>\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="oneul"><i class="fa fa-checkk" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">1 Uppercase Letter (A-Z)</span>\n' +
        '</div>\n' +
        '\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="onell"><i class="fa fa-checkk" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">1 Lowerrcase Letter (a-z)</span>\n' +
        '</div>')
    if (password.length >= 8 && password.length <=20){
        $('#length').attr('checked',true);
        $("#chlen").removeClass('invalid').addClass('valid');
    }else{
        $('#length').attr('checked',false);
        $("#chlen").removeClass('valid').addClass('invalid');
    }
    if (/[0-9]/.test(password)){
        $('#one_number').attr('checked',true);
        $("#oneno").removeClass('invalid').addClass('valid');
    }
    else{
        $('#one_number').attr('checked',false);
        $("#oneno").removeClass('valid').addClass('invalid');
    }
    if (/[A-Z]/.test(password)){
        $('#upper_case').attr('checked',true);
        $("#oneul").removeClass('invalid').addClass('valid');
    }
    else{
        $('#upper_case').attr('checked',false);
        $("#oneul").removeClass('valid').addClass('invalid');
    }
    if (/[a-z]/.test(password)){
        $('#lower_case').attr('checked',true);
        $("#onell").removeClass('invalid').addClass('valid');
    }
    else{
        $('#lower_case').attr('checked',false);
        $("#onell").removeClass('valid').addClass('invalid');
    }
});
// search Filter
$('#searchfilter').click(function () {
       $('.filter-form').toggle();

});

// Order Riview js
$('.review-order-link').click(function () {
    $('.order_review_box').show();
    $('.order_review_box').css({"width":"300px","background-color":"#f7f7f7","border":"1px solid rgb(249, 249, 249)","box-shadow":" 0 0px 0px 0 rgba(0, 0, 0, 0)","position":"relative","bottom":"1em","margin-left":"12px"});
    $('#order_details').removeClass("order-details");
    $('.order-review-para-text').css({"margin": "-0.6875em  0.75em"});
    $('.responsive_order_id').css({"display": "flex","padding":"28px 0px 0px 0px"});
    $('.order_responsive_order_id').css({"display": "flex","padding":"28px 0px 0px 0px"});
    $('.data_value_id').css("text-align","right");
    $('.col-md-9').css("display","none");
    $('.side_bar_line').hide();
    $('.case__opened').hide();
    $('.order-review').hide();
    $('.contact-info').hide();
    $('.m-subheader__title').hide();
    $('.review-order-link').hide();
    $('.order-review-para-text').show();
    $('.dasktop_order_id').hide();
    $('#alert-message').hide();
    $(".order_review_box_heading").hide();
});

//chat box js

$('.chat-box-title').click(function () {
    $('#inbox-sidebar').addClass("chatbox-responsive-title");
    $('.m-subheader__title ').addClass('m-responsive-subheader__title');
    $('#r_m_aside_left_minimize_toggle').addClass('chatbox-sidebar');
    $(".m-footer").addClass("chatbox-footer");
    $('.go-back').addClass("return-chatbox");
    $('#inbox-body').addClass('chat-box');
    $('#m_header_topbar').addClass("chatbox-topbar");
});

$('#close__leftsidebar').on('click',function () {
    $('body').addClass("m-brand--minimize m-aside-left--minimize");
    $('#m_aside_left_minimize_toggle').removeClass('m-brand__toggler--active');
});


$(document).ready(function(){
    $(".filter").click(function(event){
        $('.filter').toggleClass("m-dropdown--open");
        if($('.filter').hasClass("m-dropdown--open")){
            $('.filter').css({"background-color":"#f0cb00","border-radius":"25px"});
            $('.filter_down_arrow').css("visibility","hidden");
            $('.filter_up_arrow').css("visibility","visible");
        }
        else{
            $('.filter').css({"background-color":"transparent","border-radius":"0px"});
            $('.filter_down_arrow').css("visibility","visible");
            $('.filter_up_arrow').css("visibility","hidden");
        }
        event.stopPropagation();
    });

});

// Disputes filter close
$('body').click(function () {
    $('.filter').removeClass('m-dropdown--open');
    $('.filter').css({"background-color":"transparent","border-radius":"25px"});
    $('.filter_down_arrow').css("visibility","visible");
    $('.filter_up_arrow').css("visibility","hidden");
});

$( ".r_aside_left_minimize_toggle" ).click(function(){
   $('.m-aside-left').css("left","auto");
});

$('html').click(function() {
   $(".m-aside-left").removeAttr("style");

});

$('#m_aside_left').click(function(event){
   event.stopPropagation();
});


$('#close_sidebar').click(function(){
    $('body').removeClass('m-brand--minimize m-aside-left--minimize');
});