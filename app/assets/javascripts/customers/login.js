//= require intlTelInput
// $("#new_user").submit(function(){

    // if ($("#user_login").val()) {
    //     country_code = parseInt($('.selected-dial-code').text())
    //     number = parseInt($("#user_login").val());
    //     $("#user_login").val("" + country_code + number);
    // }
    
//     if ($("#user_phone_number").val()) {
//         country_code = parseInt($('.selected-dial-code').text())
//         number = parseInt($("#user_phone_number").val());
//         $("#user_login").val("" + country_code + number);
//     }
// });

// $("#user_phone_number").intlTelInput({
//     formatOnInit: true,
//     separateDialCode: true
    // utilsScript: "assets/libphonenumber/utils.js"
// });
$(document).on("focusin", "#username", function () {
    $('.strength').css('display', "block");
    $('.disp_no').css('display', "none");
});
$(document).on("focusout", "#username", function () {
    if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
        $('.strength').css('display', "none");
    }else if($(this).val() == ""){
        $('.strength').css('display', "none");
    }
});
//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
$("#username").on("keyup", function() {
    $('#tooltips').css('display', "block");
    var password = $(this).val();
    //$("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
    //$("#tooltips").html('').append('<div id="toolti"><div class="tip"></div><p class="pwd-suggestion-title">Your password must have at least:</p><div class="password-suggesstions"><div class="require-format"></div><span class="pwd-suggestion-message">8 Characters</span></div> <div class="password-suggesstions"><div class="invalid"><i class="fa fa-times" aria-hidden="true"></i></div><span class="pwd-suggestion-message">8 Characters</span></div><div class="password-suggesstions"><div class="valid"><i class="fa fa-check" aria-hidden="true"></i></div><span class="pwd-suggestion-message">1 Number</span></div><div class="password-suggesstions"><div class="valid"><i class="fa fa-check" aria-hidden="true"></i></div><span class="pwd-suggestion-message">1 Uppercase Letter (A-Z)</span></div><div class="password-suggesstions"><div class="valid"><i class="fa fa-check" aria-hidden="true"></i></div><span class="pwd-suggestion-message">1 Lowerrcase Letter (a-z)</span> </div></div>   <div class="display-no"><li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li></div>')
    $("#tooltips").html('').append('<div class="display_no"><li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li></div>' +
        '<div class="tip"></div>\n' +
        '    <p class="pwd-suggestion-title">Your password must have at least:</p>\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="chlen"><i class="fa fa-timess" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">8 Characters</span>\n' +
        '</div>\n' +
        '\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="oneno"><i class="fa fa-checkk" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">1 Number</span>\n' +
        '</div>\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="oneul"><i class="fa fa-checkk" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">1 Uppercase Letter (A-Z)</span>\n' +
        '</div>\n' +
        '\n' +
        '<div class="password-suggesstions">\n' +
        '    <div id="onell"><i class="fa fa-checkk" aria-hidden="true"></i></div>\n' +
        '<span class="pwd-suggestion-message">1 Lowerrcase Letter (a-z)</span>\n' +
        '</div>')
    if (password.length >= 8 && password.length <=20){
        $('#length').attr('checked',true);
        $("#chlen").removeClass('invalid').addClass('valid');
    }else{
        $('#length').attr('checked',false);
        $("#chlen").removeClass('valid').addClass('invalid');
    }
    if (/[0-9]/.test(password)){
        $('#one_number').attr('checked',true);
        $("#oneno").removeClass('invalid').addClass('valid');
    }
    else{
        $('#one_number').attr('checked',false);
        $("#oneno").removeClass('valid').addClass('invalid');
    }
    if (/[A-Z]/.test(password)){
        $('#upper_case').attr('checked',true);
        $("#oneul").removeClass('invalid').addClass('valid');
    }
    else{
        $('#upper_case').attr('checked',false);
        $("#oneul").removeClass('valid').addClass('invalid');
    }
    if (/[a-z]/.test(password)){
        $('#lower_case').attr('checked',true);
        $("#onell").removeClass('invalid').addClass('valid');
    }
    else{
        $('#lower_case').attr('checked',false);
        $("#onell").removeClass('valid').addClass('invalid');
    }
});





// Create Password Js
$(".m-create-password-input").focus(function() {
    $("#tooltips").show();
});

var starPasswordValue;
$( ".login-eye" ).click(function() {
    var password=$(".hidpassw").val();
    starPasswordValue=$(".m-login-password").val();
    $(".m-login-password").val(password);
    $(".login-open-eye").show();
    $(".login-eye").hide();
});

$( ".login-open-eye" ).click(function() {
    var password=$(".hidpassw").val();
    $(".m-login-password").val(starPasswordValue);
    $(".login-open-eye").hide();
    $(".login-eye").show();
});



$(".m-create-password-input").on("keyup change",function() {
    var PasswordValue=$('.m-create-password-input').val();
    var confirmPasswordValue=$('.m-confirm-password-input').val();

    if($(".termsandservices_cb").prop("checked") == true && confirmPasswordValue!='' && PasswordValue!=''){
        $(".m_login_signin_submit").removeAttr('id');
        $(".m_login_signin_submit").addClass("m-login__btn--primary");
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

$(".m-confirm-password-input").on("keyup change",function() {
    var PasswordValue=$('.m-create-password-input').val();
    var confirmPasswordValue=$('.m-confirm-password-input').val();
    if($(".termsandservices_cb").prop("checked") == true && PasswordValue!='' && confirmPasswordValue !=''){
        $(".m_login_signin_submit").removeAttr('id');
        $(".m_login_signin_submit").addClass("m-login__btn--primary");
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

$('input[type="checkbox"]').click(function(){
    if($(".termsandservices_cb").prop("checked") == true){
        $PasswordValue=$('.m-create-password-input').val();
        $confirmPasswordValue=$('.m-confirm-password-input').val();
        if($PasswordValue!='' && $confirmPasswordValue!=''){
            $(".m_login_signin_submit").removeAttr('id');
            $(".m_login_signin_submit").addClass("m-login__btn--primary");
        }
    }
    else{
        $(".m_login_signin_submit").attr('id','m_login_signin_submit');
        $(".m_login_signin_submit").removeClass("m-login__btn--primary");
    }
});

//End Create Password js

//Create password starts
function createstars(n) {
    return new Array(n+1).join("*")
}
$(document).ready(function() {

    var timer = "";

    $(".panel").append($('<input type="text" class="hidpassw" />'));

    $(".hidpassw").attr("name", $(".pass").attr("name"));

    $(".pass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".pass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hidpassw").val($(".hidpassw").val() + character);
        }


    });

    $("body").on("keyup", ".pass", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".pass").val().length;
            $(".hidpassw").val($(".hidpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.pass').val().length;
            $(".pass").val(createstars(current_val - 1) + $(".pass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".pass").val(createstars($(".pass").val().length));
        }, 200);

    });

});

//confirm password starts
function createconfirmstars(n) {
    return new Array(n+1).join("*")
}


$(document).ready(function() {

    var timer = "";

    $(".panel").append($('<input type="text" class="hidconfirmpassw" />'));

    $(".hidconfirmpassw").attr("name", $(".pass").attr("name"));

    $(".confirmpass").attr("type", "text").removeAttr("name");

    $("body").on("keypress", ".confirmpass", function(e) {
        var code = e.which;
        if (code >= 32 && code <= 127) {
            var character = String.fromCharCode(code);
            $(".hidconfirmpassw").val($(".hidconfirmpassw").val() + character);
        }


    });

    $("body").on("keyup", ".confirmpass", function(e) {
        var code = e.which;

        if (code == 8) {
            var length = $(".confirmpass").val().length;
            $(".hidconfirmpassw").val($(".hidconfirmpassw").val().substring(0, length));
        } else if (code == 37) {

        } else {
            var current_val = $('.confirmpass').val().length;
            $(".confirmpass").val(createconfirmstars(current_val - 1) + $(".confirmpass").val().substring(current_val - 1));
        }

        clearTimeout(timer);
        timer = setTimeout(function() {
            $(".confirmpass").val(createstars($(".confirmpass").val().length));
        }, 200);

    });
});

var confirmPasswordStarts;
$( ".login-confirm-eye" ).click(function() {
    var password=$(".hidconfirmpassw").val();
    confirmPasswordStarts=$(".m-confirm-password-input").val();
    $(".m-confirm-password-input").val(password);
    $(".login-confirm-open-eye").show();
    $(".login-confirm-eye").hide();
});

$( ".login-confirm-open-eye" ).click(function() {
    var password=$(".hidconfirmpassw").val();
    $(".m-confirm-password-input").val(confirmPasswordStarts);
    $(".login-confirm-open-eye").hide();
    $(".login-confirm-eye").show();
});

$(".m-confirm-password-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.password-confirm-error').hide();
        $('.m-confirm-password-input').css("border","solid 2px #35bab6");
        $(".login-confirm-eye").show();
        $(".login-confirm-open-eye").hide();
    }
    else
    {
        $('.password-confirm-error').show();
        $(".login-confirm-open-eye").hide();
        $(".login-confirm-eye").show();
        $('.m-confirm-password-input').css("border","solid 2px #d45858");
    }
});



// End Confirm Password js

// Login Page Js
$(".email-input").on('keyup change', function (){
    if($(this).val() !==''){
        $('.email-login-error').hide();
        $('.email-input').css("border","solid 2px #35bab6");
    }
    else
    {
        $('.email-login-error').show();
        $('.email-input').css("border","solid 2px #d45858");
    }
});

$(".m-login-password").on('keyup change', function (){
    if($(this).val() !==''){
        $('.password-login-error').hide();
        $('.m-login-password').css("border","solid 2px #35bab6");
        $(".login-eye").show();
        $(".login-open-eye").hide();
    }
    else
    {
        $(".login-open-eye").hide();
        $(".login-eye").show();
        $('.password-login-error').show();
        $('.m-login-password').css("border","solid 2px #d45858");
    }
});



$('#terms_and_services').click(function () {
    $('.term_service_text').show();
    const element =  document.querySelector('.term_service_text')
    element.classList.add('animated', 'slideInLeft')
});

$('.close_terms_services').click(function () {

    $('.term_service_text').hide();
    $('#terms_and_services').removeClass('slideInLeft');
});

// End Login Page JS

$('#terms_and_services').click(function () {
    $('.term_service_text').show();
    const element =  document.querySelector('.term_service_text')
    element.classList.add('animated', 'slideInLeft')
});

$('.close_terms_services').click(function () {

    $('.term_service_text').hide();
    $('#terms_and_services').removeClass('slideInLeft');
});

$('body').click(function () {
   $('#tooltips').hide();
});

$('#tooltips').click(function (event) {
    $('#tooltips').show();
    event.stopPropagation();
});

$('.m-create-password-input').click(function (event) {
    $('#tooltips').show();
    event.stopPropagation();
});