$(document).ready(function(){
    $(".show-dynamic-modal").click(function (ev) {
        ev.preventDefault();
        var url = $(this).data("url-path");
        var phone_lib = $(this).data("phone-lib");
        var modal_selector = $(this).data("modal-selector");
        var modal_container_selector = $(this).data("modal-container-selector");
        var method = $(this).data("method");
        $.ajax({
            url: url,
            type: method,
            success: function (data) {
                $(modal_container_selector).html(data);
                $(modal_selector).addClass("show");
                $(modal_selector).modal();
                $('#cover-spin').fadeOut();
                if(phone_lib || phone_lib.length > 0){
                    activate_phone_number_plugin();
                }
            }
        });
        return false;
    });

    $(".hit-dynamic-href").click(function (ev) {
        var url = $(this).data("url-path");
        var method = $(this).data("method");
        $.ajax({
            url: url,
            type: method,
            success: function (data) {
                window.location.reload(true);
            }
        });
        return false;
    });

});
function activate_phone_number_plugin(){
    $("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#phone_code').val(Object.values(selectedCountryData)[2]);
            $('#phone_number').val('');
            space_remove(Object.values(selectedCountryData)[2], 'phone_number');
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });

    $('#phone_number').each(function () {
        var id = $(this).attr('id');
        var phone_code_id = $(this).next().attr("id");
        $("#"+id).intlTelInput({
            formatOnInit: true,
            separateDialCode: true,
            formatOnDisplay: false,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                country=selectedCountryPlaceholder;
                country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                $("#"+phone_code_id).val(Object.values(selectedCountryData)[2]);
                space_remove(Object.values(selectedCountryData)[2], id);
                if(selectedCountryData.iso2 == 'us')
                {
                    country = "555-555-5555"
                }
                return  country;
            },
            excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
            utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
        });
    })
}

function space_remove(code_number, id) {
    if(code_number.length==1) {
        $('#'+id).removeClass('phone-length-2').removeClass('phone-length-3').removeClass('phone-length-4').addClass("phone-length-1");
    } else if(code_number.length==2) {
        $('#'+id).removeClass('phone-length-1').removeClass('phone-length-3').removeClass('phone-length-4').addClass('phone-length-2');
    } else if(code_number.length==3) {
        $('#'+id).removeClass('phone-length-1').removeClass('phone-length-2').removeClass('phone-length-4').addClass('phone-length-3');
    } else {
        $('#'+id).removeClass('phone-length-1').removeClass('phone-length-2').removeClass('phone-length-3').addClass('phone-length-4');
    }
}
