$(document).ready(function () {
    //........................merchant_header.html.erb
    var header = document.getElementById("myStickHeader");
    var sticky = header.offsetTop;
    $('.edit_current_employee').click(function () {
        $('.spnr').fadeIn();
    });
    //.............................merchant_menu.html.erb................//
    $(".sidebar_a_tag").click(function (e) {
        $(".spnr").show();
        $('.spnr').fadeIn();
    });

    //..................................suppoort_sidebar.html.erb...................//

    function show_declines(url) {
        window.location.href =
            url + "&query[date]=" + moment().local().format('MM/DD/YYYY') + " - " + moment().local().format('MM/DD/YYYY') + "&offset=" + encodeURIComponent(moment().local().format('Z'))
    }

    function show_transaction(url) {
        window.location.href =
            url + "?offset=" + encodeURIComponent(moment().local().format('Z'))
    }

    //............................merchant.html.erb..............//
    $('[data-toggle="tooltip"]').tooltip();
    jQuery(function() {
        var tz = jstz.determine();
        document.cookie = "timezone="+tz.name();
    });
})