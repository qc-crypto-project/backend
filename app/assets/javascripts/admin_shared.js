$(document).ready(function () {


    $(function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days": 90
            },
            "maxDate": nd,
        });
    });
    $(document).on('change','#tx_filter_ach',function () {
        $("#filter_form").submit();
    })
    $(document).on('change','#tx_filter_p2c',function () {
        $("#filter_form_p2c").submit();
    })
    $(document).on('change','#tx_filter_checks',function () {
        $("#filter_form_checks").submit();
    })
    // $('#export_done_btn').on('click',function () {
    // });
    $('#exportModalDialog').on('hidden.bs.modal', function () {
        $('#export_btn').prop('disabled',false);
        $('#exportDate').val('');
    });


    $('div.toggle.btn.btn-danger.off').removeAttr('style');
    $('div.toggle.btn.btn-success').removeAttr('style');

    $(document).on('change','#block',function () {
        var user_id = $("#user_id").val();
        $('div.toggle.btn.btn-danger.off').removeAttr('style');
        $('div.toggle.btn.btn-success').removeAttr('style');
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/isos/update_status",
                    type: 'get',
                    data: { user_id: user_id},
                    success: function(data) {
                        swal("Updated!", "User updated successfully", "success");
                    },
                    error: function(error){
                        swal("Cancelled", "An unexpected error occured :)", "error");
                        if($('#block').parent().hasClass('btn-danger')){
                            $('#block').parent().removeClass('btn-danger').removeClass('off');
                            $('#block').parent().addClass('btn-success');
                        }else if($('#block').parent().hasClass('btn-success')) {
                            $('#block').parent().removeClass('btn-success');
                            $('#block').parent().addClass('btn-danger').addClass('off');
                        }
                    }
                });
            } else {
                if($('#block').parent().hasClass('btn-danger')){
                    $('#block').parent().removeClass('btn-danger').removeClass('off');
                    $('#block').parent().addClass('btn-success');
                }else if($('#block').parent().hasClass('btn-success')) {
                    $('#block').parent().removeClass('btn-success');
                    $('#block').parent().addClass('btn-danger').addClass('off');
                }
                $(this).prop("checked", !$(this).is(':checked'));
            }
        });
    });

    $(document).on('click','.two_step',function () {
        var user_id = $("#user_id").val();
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/isos/update_two_step",
                    type: 'get',
                    data: { user_id: user_id },
                    success: function(data) {
                        if($('.two_step_text').text() == "On"){
                            $('.two_step_text').text('').text('Off');
                            $('.two_step_color').css('color','red');
                        }else if($('.two_step_text').text() == "Off"){
                            $('.two_step_text').text('').text('On');
                            $('.two_step_color').css('color','green');
                        }
                        swal("Updated!", "User updated successfully", "success");
                    },
                    error: function(error){
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
            }
        });
    });
    $(document).on('click','#reset',function () {
        var user_email = $("#user_email").val();
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
                $('#cover-spin').show(0);
                $.ajax({
                    url: "/admins/merchant/reset.user",
                    type: 'post',
                    data: { user: {email: user_email},csrf_token: "c7058a2ee1eb40b3c06330cf2a7be92d",from_admin: true },
                    success: function(data) {
                        $('#cover-spin').fadeOut("slow");
                        swal("Reset!", "Reset instructions sent successfully", "success");
                    },
                    error: function(error){
                        $('#cover-spin').fadeOut("slow");
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
            }
        });
    });
    $(document).ready(function () {
    $(document).on('click',"#reset_unlock",function () {
        var user_id = $("#user_id").val();
        if($("#unlock_token").val().length == 0 && $("#locked_at").val().length == 0){
            swal("Cancelled", "Already Unlocked", "error");
        }else {
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                closeOnCancel: true
            }).then((isConfirm) => {
                if (isConfirm.value === true) {
                    $.ajax({
                        url: "/admins/merchants/send_unblock_email",
                        type: 'get',
                        data: {merchant_id: user_id},
                        success: function (data) {
                            swal("Updated!", "Reset email sent successfully", "success");
                        },
                        error: function (error) {
                            swal("Cancelled", "An unexpected error occured :)", "error");
                        }
                    });
                } else {
                }
            });
        }
    });
    });
    $(document).ready(function () {
    $(document).on('click',"#resend_welcome",function () {
        var user_id = $("#user_id").val();
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/merchants/resend_welcome_email",
                    type: 'get',
                    data: {merchant_id: user_id},
                    success: function (data) {
                        swal("Updated!", "Welcome email sent successfully", "success");
                    },
                    error: function (error) {
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
            }
        });
    });
    })

 /*   $('#phone_number').on('keyup', function () {
        var phone = $("#phone_number").val();
        var phone_code = $(".phone-box .selected-dial-code").text();
        var reg = new RegExp('^[0-9]+$');
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        $.ajax({
            url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
            type: 'GET',
            data: {phone_number: phone_number},
            success: function (result) {
                if(result){
                    $(".phone-error").text('')
                    $('.wizard-submit-btn').prop('disabled', false);
                }else{
                    $('.wizard-submit-btn').prop('disabled', 'disabled');
                    $(".phone-error").text('Invalid format or phone number is already taken!')
                }
            },
            error: function (error) {
                console.log(error)
            }
        });
    });*/

    function show_transaction(url,trans,wallet_id) {
        $('#cover-spin').show(0);
        window.location.href = url + "?trans="+ trans +"&wallet_id="+ wallet_id +"&offset=" + encodeURIComponent(moment().local().format('Z'))
    }


    $(function(){
        var merchants = gon.merchants
        var agents = gon.agents
        var affiliates = gon.affiliates
        var data = [];
        if((merchants != null && merchants.length != 0 ) || (agents != null && agents.length != 0) || (affiliates != null && affiliates.length != 0)){
            data = [
                [agents ? "Agents" : "", agents ? agents.length : ""],
                [merchants ? "Merchants" : "", merchants ? merchants.length : ""],
                [affiliates ? "Affiliates" : "", affiliates ? affiliates.length : ""]
            ]
        }else{
            data = [
                ['No Data', 1]
            ]
        }
        if($("#container").length > 0){
            Highcharts.chart('container', {
                chart: {
                    type: 'pie'
                },

                title: {
                    text: null
                },

                legend: {
                    //  symbolWidth: 40,
                    //       symbolHeight: 20,
                    //    squareSymbol: false,
                    //   symbolRadius: 0
                },

                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                series: [{
                    data: data
                }],
            })
        }

    });

    $(document).on('click','.show_click',function () {
       var id = $(wallet_id).val()
        show_transaction('/admins/isos/specfic_transactions','reserve',id)
    })
    $(function () {
        $('#image_store').on('change', function (event) {
            $('#choose_img .col-md-3 img').remove();
            var files = event.target.files;
            var image = files[0];
            var reader = new FileReader();
            reader.onload = function (file) {
                var img = new Image();
                img.src = file.target.result;
                $('#choose_img .col-md-3').html(img);
            }
            reader.readAsDataURL(image);
        });
    });

    $('#delete_image_button').on('click',function() {
        $('#cover-spin').show(0);
        var a = $(this).data('image-id');
        if(confirm('Are you sure to delete?')){
            $.ajax({
                url: "/admins/image_delete",
                type: 'GET',
                data: {id: a},
                success: function (data) {
                    if(data === true) {
                        $('#img_up').remove();
                        $('#choose_img').removeClass('display_none');
                        $('#cover-spin').fadeOut();
                    }
                },
                error: function (data) {
                }
            });
        }
        else {
            $('#cover-spin').fadeOut();
        }
    });
    if($('#new_image').length > 0 && !$('#new_image').valid()){
        $('#cover-spin').fadeOut();
    }

    $('#save_button').on('click',function (e){
        $('#cover-spin').show(0);
        if($('#img_up').val() === undefined){
            var file = $('#image_store')[0];
            if ($('#image_store').val()==="") {
                alert('Please select a file to upload');
                $('#cover-spin').fadeOut();
                e.preventDefault();
            }
            else {
                var file_size = parseFloat((file.files[0].size / 1024) / 1024).toFixed(2);
                var ext = file.value.split('.').pop().toLowerCase();
                if ($.inArray(ext, ['png', 'jpg', 'jpeg','pdf','csv','xlsx']) == -1) {
                    alert("Only 'Png', 'Jpeg', 'JPG', PDF, CSV, xlsx allowed");
                    $('#cover-spin').fadeOut();
                    e.preventDefault();
                }
                if (file_size > 4) {
                    alert('Please select a file less than 4MB');
                    $('#cover-spin').fadeOut();
                    e.preventDefault();
                }
            }
        }
    });

    $('#password_btn').on('click', function () {
        var password = $('#password').val();
        $.ajax({
            url: "/admins/buy_rate/verify_admin_password",
            method: 'get',
            data: {password: password},
            success: function (data) {
                $("#password_btn").css("background", "green").text('').text("Verified");
                $("#info-text").text('');
                setTimeout(function () {
                    var x = $("#password-field")
                    if (x.attr('type') === "password") {
                        x.attr('type', 'text')
                    }
                    $('#admin_confirm').modal('hide')
                    $('#password').val("");
                    $("#password_btn").css("background", "#7867a7").text('').text("Verify");
                }, 1000);
            },
            error: function (data) {
                $("#password_btn").css("border", "1px solid red");
                $("#info-text").text('').css("color", "red").text('Please try again!');
            }
        });
    });

    // admins/shared/profiles/merchants_table
    var table = $('#datatable-keytable-merchants').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": false,
        "ordering": true,
        "searching": false,
        "bLengthChange": true,
        "bPaginate": false,
        "bInfo": false
    });

    var table = $('#datatable-keytable-agen_aff').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": true,
        "ordering": true,
        "searching": true,
        "bLengthChange": true,
        "bPaginate": true,
        "bInfo": true
    });
    $('#datatable-keytable-agen_aff').on('search.dt', function () {
        var value = $('.dataTables_filter input').val().trim();
        if(!isNaN(value))
        {
            $('.dataTables_filter input').val(value);
        }
    });

    $(".my-password").on('click', function () {
        myFunction();
        
    })

});

function myFunction() {
    var x = $("#password-field")
    if (x.attr('type') === "password") {
        $('#admin_confirm').modal('show');
    }
    else {
        x.attr('type', 'password')
    }
}