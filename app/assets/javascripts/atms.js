$(document).ready(function () {
//................atms/index.html.erb...................//

    $('#user_password_confirmation').on('keyup', function () {
        if ($('#user_password').val() == $('#user_password_confirmation').val()) {
            $('#confirm_password').html('').html('Matched').css({
                'display': 'block',
                'color': 'green',
                'font-weight': 'bold'
            });
        } else
            $('#confirm_password').html('').html('Not matched').css({
                'display': 'block',
                'color': 'red',
                'font-weight': 'bold'

            });
    });
    //..........................atms/list.html.erb...............//
    //............................SHOW.HTML.ERB............//
    $(".datatable-keytable_search").DataTable({
        responsive: true,
        "sScrollX": false,
        "scrollX": false,
        "order": [[ 5, "desc" ]]
    });
    //...........................shared/transaction.html.erb..........//
    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy'
    });
    $(document).on('click','.atms_click',function () {
        $('#cover-spin').show(0)
    })


});
$(document).on('click','.modalwalabutton',function () {
    $('.myModal').modal();
});
$('#datatable-keytable_search').DataTable({
    responsive: true,
    "sScrollX": false,
    "sscrollX": false
});