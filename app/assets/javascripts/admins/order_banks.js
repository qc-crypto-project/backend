$(document).ready(function () {
//........................................order_banks/index.html............//
    $('#alert-message-top').fadeOut(5000);
    $('#datatable-keytable').DataTable({
        responsive: true,
        sorting: false,
        paging: false,
        bfilter: false,
        searching: false,
        columnDefs: [ { orderable: false, targets: 0} ],
        columnDefs: [{
            targets: "_all",
            className: "text-center",
        }]
    });
    $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
        $('input[name="query[date]"]').val('');
    });
    $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
    });

    $('.dateRange').daterangepicker({
        autoUpdateInput: false,
        format: 'MM/DD/YYYY' });
    // $(".exportModalDialog1").on('click', function () {
    //     $('#export_btn').show();
    //     if($('#export_btn').attr("disabled",true))
    //     {
    //         $('#export_btn').attr('disabled', false)
    //     }
    //     $("#export_close2").show();
    //     // $('#body_1').show();
    //     // $('#body_21').show();
    //     $('#body_31').hide();
    //     $('#body_41').hide();
    //     $("#export_done_btn").hide();
    //     $('#hidden_id').val($(this).data('type'));
    //     $('#hidden_wallet_id').val($(this).data('wallet'));
    //     $('#hidden_send_via').val($(this).data('send_via'));
    // });
    $("#gateway").select2({
        multiple: true,
    });
})

