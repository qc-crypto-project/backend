$('#alert-message-top').fadeOut(5000);
$(function() {
    $(".exportModalDialog").on('click', function () {
        $('#export_btn').show();
        $('#admin_export_button').show();
        if($('#export_btn').attr("disabled",true)) {
            $('#export_btn').attr('disabled', false)
        }
        $("#body_done").hide();
        $("#no_tx").hide();
        $("#export_close2").show();
        $('#body_1').show();
        $('#body_2').hide();
        $('#body_3').hide();
        $('#body_4').hide();
        $("#export_done_btn").hide();
        $('#hidden_id').val($(this).data('type'));
        $('#subtype').val($(this).data('trans'));
        $('#case').val($(this).data('case'));
        $('#hidden_wallet_id').val($(this).data('wallet'));
        $('#hidden_send_via').val($(this).data('send_via'));
        $('#hidden_offset').val($(this).data('offset'));
        $('#admin_user_id').val($(this).data('admin_user_id'));
        $('#specific_wallet_id').val($(this).data('specific_wallet_id'));
        $('#search_query').val(JSON.stringify($(this).data('search-q')));
        $("#fromExportHidden").val("false");
    });
    var $statusElm = $("div.export-status");
    $(document).on('click', ".exportReport", function () {
        $("#body_done").hide();
        $("#no_tx").hide();
        $('#export_btn').hide();
        $('#admin_export_button').hide();
        $("#export_close2").show();
        $('#body_1').hide();
        $('#body_2').show();
        $('.export-print').show();
        $('#body_3').hide();
        $('#body_4').hide();
        $("#export_done_btn").hide();
        var selected = $(this).data('date');
        var export_date = $("#exportDate").val();
        var from_export_hidden = $("#fromExportHidden").val()
        if(from_export_hidden == "true"){
            export_date = $("#exportDateHidden").val();
        }
        var type = $(this).data('type');
        var dispute_case = $(this).data('case');
        var wallet = $(this).data('wallet');
        var send_via = $(this).data('send_via');
        var offset = $(this).data('offset');
        var trans = $(this).data('trans');
        var archive = $('#archive').val();
        var specific_wallet_id = $(this).data('specific_wallet_id');
        var total_count = $(this).data('total-count');
        var search_query = {
            amount: $(this).data('search-price'),
            merchant_location: $(this).data('merchant_location'),
            q: $(this).data('search-q'),
            date: $(this).data('search-date'),
            type: $(this).data('search-types'),
            gateway: $(this).data('search-gateway'),
            last4: $(this).data('search-last4'),
            first6: $(this).data('search-firstsix'),
            block_id: $(this).data('search-block_id'),
            sender: $(this).data('search-sender'),
            receiver: $(this).data('search-receiver'),
            state: $(this).data('search-state'),
            city: $(this).data('search-city'),
            time1:$(this).data('search-time1'),
            time2:$(this).data('search-time2'),
            wallet_id:$(this).data('search-wallet_id'),
            iso:$(this).data('iso'),
            all:$(this).data('all'),
            export_date: export_date,
            archive: archive
        };
        $.ajax({
            url: "/admins/transactions/generate_export_file",
            dataType: "json",
            data: {trans: trans, date: selected, type: type, wallet: wallet,total_count: total_count, send_via: send_via, offset: offset, search_query: search_query,specific_wallet_id: specific_wallet_id,dispute_case: dispute_case}
        }).done(function(response, status, ajaxOpts) {
            if (status === "success" && response && response.status == "completed") {
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                if(response.file_id == "" || response.file_id == null || response.file_id == "null"){
                    $("#no_tx").show();
                }else{

                    // $("#body_4").text("").text("Your export is ready to download. Please go to Exported Files tab.");
                    $("#body_4").show();
                    // $('#download_link').attr('href', "/admins/transactions/export_download?id="+response.file_id)
                }
            }else if (status === "success" && response && (response.status == "crashed")) {
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_message").hide();
                $("#body_2").show();
                $('.export-print').hide();
                $(".export-heading").text('').text(response.heading);
                $(".export-status").text('').text(response.message);
            }else if (status === "success" && response && (response.status == "pending")) {
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                $("#body_message").show();
                $("#message_h5").text('').text('Alert!');
                $("#message_p").text('').text(response.message);
                $("#printcount").text('');
            }else{
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").show();
                $("#email_user_1").show();
                global.jobId = response.jid;
                global.intervalName = "job_" + jobId;

                $statusElm.text("Calculating total transactions, few seconds remaining....");
            }

        }).fail(function(error) {
            console.log(error);
        });
    });
    $("#export_close1").on('click', function(){
        $("#no_tx").hide();
        $("#body_done").hide();
        $('#body_1').show();
        $('#export_btn').show();
        $('#admin_export_button').show();
        $('#body_2').hide();
        $(".export-heading").text('');
        $('.export-status').text('');
        $('#body_3').hide();
        $("#body_message").hide();
    });
    $("#export_btn").on("click", function(e) {
        $("#body_done").hide();
        $("#no_tx").hide();
        $(this).prop("disabled",true);
        var selected = $("#exportDate").val();
        var type = $('#hidden_id').val();
        var dispute_case = $('#case').val();
        var wallet = $('#hidden_wallet_id').val();
        var send_via = $("#hidden_send_via").val();
        var offset = $("#hidden_offset").val();
        var trans = $('#subtype').val();
        var specific_wallet_id = $('#specific_wallet_id').val();
        var admin_user_id = $('#admin_user_id').val();
        var search_query = {};
        if($("#search_query").val() != ""){
            search_query = {
                q: JSON.parse($("#search_query").val()),
            };
        }
        $.ajax({
            url: "/admins/transactions/generate_export_file",
            dataType: "json",
            data: {admin_user_id: admin_user_id,trans: trans, date: selected, type: type, wallet: wallet, send_via: send_via, offset: offset,specific_wallet_id: specific_wallet_id,dispute_case: dispute_case, search_query: search_query,}
        }).done(function(response, status, ajaxOpts) {
            if (status === "success" && response && response.status == "completed") {
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                $('.export-print').hide();
                if(response.file_id == "" || response.file_id == null || response.file_id == "null"){
                    $("#no_tx").show();
                }else{
                    $("#body_4").show();
                    $('#download_link').attr('href', "/admins/transactions/export_download?id="+response.file_id)
                }
            }else if (status === "success" && response && (response.status == "crashed")) {
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_message").hide();
                $("#body_2").show();
                $('.export-print').hide();
                $(".export-heading").text('').text(response.heading);
                $(".export-status").text('').text(response.message);
            }else if (status === "success" && response && (response.status == "pending")) {
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").hide();
                $("#body_message").show();
                $("#message_h5").text('').text('Alert!')
                $("#message_p").text('').text(response.message);
                $("#printcount").text('');
            }else{
                $('#export_btn').hide();
                $('#admin_export_button').hide();
                $("#transaction_heading_2").show();
                $("#transaction_heading").hide();
                $("#body_1").hide();
                $("#body_2").show();
                $("#email_user_1").show();
                global.jobId = response.jid;
                global.intervalName = "job_" + jobId;
                $statusElm.text("Calculating total transactions, few seconds remaining....");
            }
        }).fail(function(error) {
            console.log(error);
        });
    });

    $("#email_user_1").on('click', function () {
        $('#body_1').hide();
        $('#body_2').hide();
        $('#body_3').show();
        $('#body_4').hide();
        send_email()
    });

    function send_email(){
        var email = setInterval(function(){
            if ($("#hidden_user_id").val() != '' && $("#hidden_qc_file").val() != ''){
                user_id = $("#hidden_user_id").val();
                qc_file = $("#hidden_qc_file").val();
                $.ajax({
                    url: "/merchant/transactions/send_email_export_file",
                    type: 'GET',
                    data: {user_id: user_id,qc_file: qc_file},
                    success: function(data) {
                        $("#hidden_user_id").val('');
                        $("#hidden_qc_file").val('');
                        clearInterval(email);
                    },
                    error: function(data){
                    }
                });
            }
        }, 1000);
    }

    $("#export_close2").on('click', function(){
        $("#body_done").hide();
        $("#no_tx").hide();
        $('#body_1').show();
        $('#export_btn').show();
        $('#admin_export_button').show();
        $('#body_2').hide();
        $(".export-heading").text('');
        $('.export-status').text('');
        $('#body_3').hide();
        $('#body_4').hide();
        $("#export_done_btn").hide();
        $("#body_message").hide();
    });

    $("#export_done_btn").on('click', function(){
        $("#export_close2").show();
        $("#export_done_btn").hide();
        $('#body_4').hide();
        $("#no_tx").hide();
        $("#body_done").hide();
    })
    $('#exportModalDialog').on('hidden.bs.modal', function () {
        $("#body_done").hide();
        $("#no_tx").hide();
        $('#body_1').show();
        $('#export_btn').show();
        $('#admin_export_button').show();
        $('#body_2').hide();
        $(".export-heading").text('');
        $('.export-status').text('');
        $('#body_3').hide();
        $('#body_4').hide();
        $("#export_done_btn").hide();
        $("#body_message").hide();
    })

});
