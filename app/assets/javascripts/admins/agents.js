$(document).ready(function () {
    var n = 0;
    if (gon.index_session != "" ? true : false) {
        n = gon.index_session != "" ? gon.index_session : 0;
        if (n > 1) {
            $('#a_' + (n - 1)).find('i:nth-child(1)').removeClass('grey-color').addClass("green-clr");
            $('#a_' + (n - 2)).find('i:nth-child(1)').removeClass('grey-color').addClass("green-clr");
            $('#a_' + (n - 3)).find('i:nth-child(1)').removeClass('grey-color').addClass("green-clr");
            $('#a_' + n).find('i:nth-child(1)').removeClass('grey-color').addClass("blu-clr");
            $('#a_' +gon.step_index).find('i:nth-child(1)').removeAttr('style').addClass('blu-color');
        }
    }

    $(".fa-stack.fa-lg").on('click', function (e) {
        if ($(this).children('i').hasClass("grey-color")) {
            e.preventDefault();
        }
    });
//=====================================     start _agent_buyrate.html.erb
// $('.buyrate_fields').bind('keyup change', function(){
//     this.value = this.value.replace(/[^0-9\.]/g,'');
// });
$('#alert-message-top').fadeOut(5000);
//=====================================     end _agent_buyrate.html.erb
//=====================================     start _agent_signup.html.erb
// validateUserForm('agent','notuser');

//=====================================     end _agent_signup.html.erb

//=====================================     start _edit.html.erb
var email = $('#email_field1').val();
var phone = $('#phone_number_field1').val();
var validate_edit_email1 = function() {
    if((email != $('#email_field1').val()) && ($('#email_field1').length > 0 ) ) {
        $.ajax({
            url: "/admins/companies/form_validate?email_address=" + ($('#email_field1').val()),
            type: 'GET',
            data: {user_id: $('#company_hidden_field').val()},
            success: function (data) {
                // $('#email_field1').css('border', '')
                $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
                if (($("#phone_number_label_field1").text() == "Phone number") && ($('#password_field1').text() == "Confirm Password")) {
                    $('#submit_edit_button1').prop('disabled', false);
                }
                if ($('#email_field1')) {
                    if (data['value'] == true) {
                        $('#email_label_field1').replaceWith("<label id='email_label_field1' class='string optional' for='Email'	>Email<span class='color-red-agent'> This email has already been registered. Try again.</span></label>")
                        $('#email_field1').addClass('agent-border');
                        $('#submit_edit_button1').prop('disabled', true);
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }else{
        // $('#email_field1').css('border', '');
        $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
        if (($("#phone_number_label_field1").text() == "Phone number") && ($('#password_field1').text() == "Confirm Password")) {
            $('#submit_edit_button1').prop('disabled', false);
        }
    }
}

var validate_phone_edit_number1 = function() {
    if((phone != $('#phone_number_field1').val()) && ($('#phone_number_field1').length > 0 ) ) {
        $.ajax({
            url: "/admins/companies/form_validate?phone_number=" + ($('#phone_number_field1').val()),
            type: 'GET',
            data: {user_id: $('#company_hidden_field').val()},
            success: function (data) {
                // $('#phone_number_field1').css('border', '')
                $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='Phone number'>Phone number</label>")
                if (($("#email_label_field1").text() == "Email") && ($('#password_field1').text() == "Confirm Password")) {
                    $('#submit_edit_button1').prop('disabled', false);
                }
                if ($('#phone_number_field1')) {
                    if (data['value'] == true) {
                        $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='Phone number'>Phone number <span class='color-red-agent'> This phone number has already been registered. Try again.</span></label>")
                        $('#phone_number_field1').addClass('agent-border');
                        $('#submit_edit_button1').prop('disabled', true);
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }else{
        // $('#phone_number_field1').css('border', '')
        $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='company_Phone number'>Phone number</label>")
        if (($("#email_label_field1").text() == "Email") && ($('#password_field1').text() == "Confirm Password")) {
            $('#submit_edit_button1').prop('disabled', false);
        }
    }
}

$('#password_confirmation1').keyup(function() {
    var pass = $('#password1').val();
    var repass = $('#password_confirmation1').val();
    if (pass != repass) {
        $('#submit_edit_button1').prop('disabled', true);
    }
    else {
        $('#submit_edit_button1').prop('disabled', false);
    }
});
//=====================================     end _eidt.html.erb
//=====================================     start agent_buyrate.html.erb

    $('.buyrate_fields').bind('keyup change', function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });
//=====================================     end agent_buyrate.html.erb

//=====================================     start agent_documentaion.html.erb
$(function () {
    var count = 0;
    $('input').on('change', function (event) {
        var id = $(this).attr('id');
        $(`#${id}-error`).hide();
        $(`.col-md-8.${id} .row .col-md-12 img`).remove();
        var files = event.target.files;
        var files_count = 0;
        if (value_present(files)) {
            files_count = files.length;
            $.each(files, function (index, value) {
                var fileExt = value.name.substr(value.name.lastIndexOf('.') + 1)
                if (fileExt === 'jpeg' || fileExt === 'jpg' || fileExt === 'png' || fileExt === 'gif') {
                    var image = value;
                    var reader = new FileReader();
                    reader.onload = function (file) {
                        var img = new Image();
                        img.id = id + '_' + (count = count + 1);
                        img.src = file.target.result;
                        if (files_count === 1) {
                            $(`.col-md-8.${id} .row .col-md-12`).html(img);
                        } else {
                            $(`.col-md-8.${id} .row .col-md-12`).append(img);
                            $(`.col-md-8.${id} .row .col-md-12 img`).addClass('padding-right-2-percent');
                        }
                    }
                    reader.readAsDataURL(image);
                }
            });
            if (files_count === 0) {
                $(`.col-md-8.${id} .row .col-md-12 img`).remove();
            }
        }
    });
});

$("a.fa.fa-trash.fa-font.panel-heading").click(function (e) {
    var image_id = $(this).attr('id');
    e.preventDefault();
    if (confirm('Are you sure to Delete?')) {
        $.ajax({
            url: "/admins/isos/document_delete",
            type: 'GET',
            data: {image_id: image_id},
            success: function (data) {
                if (data === true) {
                    $('img.' + image_id).hide();
                    $('a.' + image_id).hide();
                    $('.' + image_id).next().hide();
                    $('#cover-spin').fadeOut();
                }
            },
            error: function (data) {
                $('#cover-spin').fadeOut();
            }
        });
    }
    else {
        $('#cover-spin').fadeOut();
    }
});
    $(document).on('click blur paste keyup','#phone_number', function (e) {
        var area_code = $(this).val()
        var area_codes = area_code.slice(0,3)
        $("#phone_number").inputFilter(function(value) {
            return /^\d*$/.test(value); });
        if ($(this).val().length === 0) {
            reset();
        }
        else if ($(this).val() >= country.length && (countrycode() || area_codes=="839")) {
            this.value = $(this).val().replace(/[^0-9\.]/g, '');
            var code = $('.selected-dial-code').text();
            if ($(this).val().length !== 0 && $(this).val().length > 5) {
                $('.phone_class').attr("disabled",true)
                $.ajax({
                    url: '/admins/companies/form_validate',
                    data: {phone_number: code + $(this).val(),user_id: $('#user_form_id').val()},
                    method: 'get',
                    success: function (result) {
                        if (result== true) {
                            $('.phone_class').attr("disabled",false)
                            var form_id=$('#user_form_id').val()
                            if ($('#user_form_id').val()=== undefined || $('#user_form_id').val()==='' || $('#user_form_id').val()===form_id){
                                $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
                                if (code == "+1")
                                {
                                    var number=  $('#phone_number').val();
                                    if (number.length>10)
                                    {
                                        $('#phone_msg').removeClass('text-success').addClass('has-error').text('').text('Too long');
                                    }
                                }
                            }
                            else{
                                $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
                            }

                        }
                        else {
                            $('.phone_class').attr("disabled",false)
                            $('#phone_msg').removeClass('text-success').addClass('has-error').text('').text('User Already Exist!');
                        }
                    }
                });
            }
        }
        else {
            if ($(this).val().length > 0) {
                var errorCode = $('#phone_number').intlTelInput("getValidationError");
                if ($(this).val().length === 1){ errorCode = 2 }
                $('#phone_msg').removeClass("new_user text-success").addClass("has-error").text(errorMap[errorCode]);
            }
        }
    });

    function countrycode() {
        return $('#phone_number').intlTelInput("isValidNumber");
    }

validateagentForm("iso");

$('#ssn').mask('000-00-0000');
$('#tx').mask('000-0000000');
    var code = 0;
    var country="";
    var errorMap = ["Invalid Number", "Invalid Selected Country", "Too short", "Too long", "Invalid Number"];
//=====================================     end agent_signup.html.erb
//=====================================     start edit.html.erbss
    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    var pass_score = 0;
    $("#user_password").focusin(function () {
        $('.strength').css('display', "block");
        // $('.strength').addClass('display-block');
    });
    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            // $('.strength').addClass('display-none');
            $('.strength').css('display', "none");
        }
    });
    $('#user_password ,#user_password_confirmation').on('keyup', function () {
        if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            $(':input[type="submit"]').prop('disabled', false);
        } else {
            $(':input[type="submit"]').prop('disabled', true);
        }
        if ($('#user_password').val() == "" && $('#user_password_confirmation').val() == ""){
            $('#message').html('');
            $(':input[type="submit"]').prop('disabled', false);
        }
    });
    $("#user_password").on("keyup", function() {
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else{
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });
    $(document).on('change','.admins_country_field',function () {
        if($(this).val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $(this).val(),owner_field: 'change' },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#user_profile_attributes_state").empty();
                    if(Object.keys(states).length > 0 ){
                        $.each(states,function (index, value) {
                            $("#user_profile_attributes_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                        });
                        $('.admins_states_field_agent').attr("required",true);
                        $('.admins_states_field_agent').attr("disabled",false);
                    }else {
                        $('.admins_states_field_agent').attr("required",false);
                        $('.admins_states_field_agent').attr("disabled",true);

                    }
                }
            });
        }
    });
    // $("#phone_number").inputmask('Regex', {regex: "^[0-9,]{1,99}(\\.\\d{1,2})?$"});

    // $('#submit_edit_button').on("click",function(event) {
    //     var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}/;
    //     if (!re.test($('#user_password').val())){
    //         $('#password_Error').html('');
    //         $('#user_password').after('<div id= "password_Error" class="color-red-agent"> password validation requires capital letter, small letter and a number </div>')
    //         return false;
    //     }
    // });
    // validateUserForm('agent');
//=====================================     end edit.html.erb
//=====================================     start index.html.erb
    $('.modalwalabutton').click(function(){
        $('.myModal').modal();
    });
    $('#agents-datatable').DataTable({
        "sScrollX": false,
        "scrollX": false,
        "sSearch": true,
        "ordering": true,
        "processing": true,
        "searching": true,
        "serverSide": true,
        "ajax": $('#agents-datatable').data('source')
    });

$('#submit_edit_button').prop('disabled', true);
var validate_company_edit_email = function() {
    $.ajax({
        url: "/admins/companies/form_validate?email_address="+($('#company_email_edit_field').val()),
        type: 'GET',
        data: {user_id:$('#company_hidden_field').val() },
        success: function(data) {
            // $('#company_email_edit_field').css('border','');
            $('#company_email_label_edit_field').replaceWith('<label id="company_email_label_edit_field" class="string optional" for="company_Email">Email</label>')
            if($("#phone_number_label_edit_field").text() == "Phone number") {
                $('#submit_edit_button').prop('disabled', false);
            }
            if($('#company_email_edit_field')){
                if(data['value'] == true){
                    $('#company_email_label_edit_field').replaceWith("<label id='company_email_label_edit_field' class='string optional' for='company_Email'	>Email<span class='color-red-agent'> This email has already been registered. Try again.</span></label>")
                    $('#company_email_edit_field').addClass('agent-border');
                    $('#submit_edit_button').prop('disabled', true);
                }
            }
        },
        error: function(data){
            console.log(data);
        }
    });
}

    $('.agent-create').on('click', function () {
        $('#phone_number').removeAttr('autocomplete')
        if ($("#isoform").valid() && !$('#phone_msg').hasClass('has-error') && ( $("#phone_number").val() == undefined || !$("#phone_number").val() == "")){
            $('#cover-spin').show(0);
        }
    });

var validate_phone_edit_number = function() {
    $.ajax({
        url: "/admins/companies/form_validate?phone_number="+($('#phone_number_edit_field').val()),
        type: 'GET',
        data: {user_id:$('#company_hidden_field').val() },
        success: function(data) {
            // $('#phone_number_edit_field').css('border','')
            $('#phone_number_label_edit_field').replaceWith("<label id='phone_number_label_edit_field' class='string optional' for='company_Phone number'>Phone number</label>")
            if($("#company_email_label_edit_field").text() == "Email"){
                $('#submit_edit_button').prop('disabled', false);
            }

            if($('#phone_number_edit_field')){
                if(data['value'] == true){
                    $('#phone_number_label_edit_field').replaceWith("<label id='phone_number_label_edit_field' class='string optional' for='company_Phone number'>Phone number <span class='color-red-agent'> This phone number has already been registered. Try again.</span></label>")
                    $('#phone_number_edit_field').addClass('agent-border');
                    $('#submit_edit_button').prop('disabled', true);
                }
            }
        },
        error: function(data){
            console.log(data);
        }
    });
}
//=====================================     end index.html.erb
//=====================================     start show.html.erb
$('#datatable-iso').DataTable({
//        responsive: true,
    "sScrollX": false,
    "scrollX": false
});
//=====================================     end show.html.erb
//=====================================     start system_fee.html.erb
    $("#submit_button").on('click', function (e) {
        if($('#user_system_fee_check_limit_type').val() == "" && $("#agent_check_limit").val() != ""){
            $(".agent_check_limit_error").show()
            e.preventDefault()
            $('#cover-spin').fadeOut();
        }else{$(".agent_check_limit_error").hide()}

        if($('#user_system_fee_push_to_card_limit_type').val() == "" && $("#agent_p2c_limit").val() != ""){
            $(".agent_p2c_limit_error").show()
            e.preventDefault()
            $('#cover-spin').fadeOut();
        }else{$(".agent_p2c_limit_error").hide()}

        if($('#user_system_fee_ach_limit_type').val() == "" && $("#agent_ach_limit").val() != ""){
            $(".agent_ach_limit_error").show()
            e.preventDefault()
            $('#cover-spin').fadeOut();
        }else{$(".agent_ach_limit_error").hide()}

    })
    $(".buyrate_fields").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
    $(".buyrate_fields_percent").inputmask('Regex', {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"});

    $(document).on('click', '.btn.butt-color.button-radius', function (e) {
        validateagentForm("iso");
        if ($('#phone_number').val() !== undefined) {
            if ($('#phone_msg').hasClass('text-danger')) {
                e.preventDefault();
                $('#cover-spin').fadeOut();
            }
        }
        if ($('#user_form_id').val() === undefined) {

            if ($('#phone_number').val() !== undefined) {
                if ($('#phone_number').val() === '') {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Please Add Phone Number');
                    e.preventDefault();
                } else if ($('#phone_number').val().length > 0 && $('#phone_number').val().length < 6) {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Length should be greater than 5');
                    e.preventDefault();
                } else {
                    if ($('#phone_msg').hasClass('has-error')) {
                        e.preventDefault();
                    }
                }
            }
        } else {
            if ($('#phone_number').val() !== undefined) {
                if ($('#phone_number').val() === '') {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Please Add Phone Number');
                    e.preventDefault();
                } else if ($('#phone_number').val().length > 0 && $('#phone_number').val().length < 6) {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Length should be greater than 5');
                    e.preventDefault();
                } else {
                    if ($('#phone_msg').hasClass('has-error')) {
                        e.preventDefault();
                    }
                }
            }
        }
    });

 $('#phone_number').intlTelInput({
     formatOnDisplay: false,
     formatOnInit: true,
     separateDialCode: true,
     initialCountry: "us",
     customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
         $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
         country=selectedCountryPlaceholder;
         country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
         code=Object.values(selectedCountryData)[2];
         $('#phone_code').val(code);
         $('#phone_number').val('');
         $('#phone_number').removeAttr('autocomplete')
         space_remove(code.length);
         if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
         return  country;
     },
     excludeCountries: ['bg','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae','hr'],
     utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
 });

    // $(document).on('click blur paste keyup','#phone_number', function (e) {
    //     debugger
    //     $("#phone_number").inputFilter(function(value) {
    //         return /^\d*$/.test(value); });
    //     if ($(this).val().length === 0) {
    //         reset();
    //     }
    //     else if ($(this).val() >= country.length && countrycode()) {
    //         this.value = $(this).val().replace(/[^0-9\.]/g, '');
    //         var code = $('.selected-dial-code').text();
    //         if ($(this).val().length !== 0 && $(this).val().length > 5) {
    //             $('.phone_class').attr("disabled",true)
    //             $.ajax({
    //                 url: '/admins/companies/form_validate',
    //                 data: {phone_number: code + $(this).val(),user_id: $('#user_form_id').val()},
    //                 method: 'get',
    //                 success: function (result) {
    //                     if (result== true) {
    //                         $('.phone_class').attr("disabled",false)
    //                         var form_id=$('#user_form_id').val()
    //                         if ($('#user_form_id').val()=== undefined || $('#user_form_id').val()==='' || $('#user_form_id').val()===form_id){
    //                             $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
    //                             if (code == "+1")
    //                             {
    //                                 var number=  $('#phone_number').val();
    //                                 if (number.length>10)
    //                                 {
    //                                     $('#phone_msg').removeClass('text-success').addClass('has-error').text('').text('Too long');
    //                                 }
    //                             }
    //                         }
    //                         else{
    //                             $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
    //                         }
    //
    //                     }
    //                     else {
    //                         $('.phone_class').attr("disabled",false)
    //                         $('#phone_msg').removeClass('text-success').addClass('has-error').text('').text('User Already Exist!');
    //                     }
    //                 }
    //             });
    //         }
    //     }
    //     else {
    //         if ($(this).val().length > 0) {
    //             var errorCode = $('#phone_number').intlTelInput("getValidationError");
    //             if ($(this).val().length === 1){ errorCode = 2 }
    //             $('#phone_msg').removeClass("new_user text-success").addClass("has-error").text(errorMap[errorCode]);
    //         }
    //     }
    // });
    var reset = function () {
        $('#phone_msg').removeClass('has-error text-success').text('');
    };
});
function validateFiles(inputFile) {
    var maxExceededMessage = "This file exceeds the maximum allowed file size (10 MB)";
    // var extErrorMessage = "Only image file with extension: .jpg, .jpeg, .gif or .png is allowed";
    // var allowedExtension = ["jpg", "jpeg", "gif", "png"];

    // var extName;
    var maxFileSize = $(inputFile).data('max-file-size');
    var sizeExceeded = false;
    // var extError = false;

    $.each(inputFile.files, function() {
        if (this.size && maxFileSize && this.size > parseInt(maxFileSize)) {sizeExceeded=true;};
        extName = this.name.split('.').pop();
        // if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
    });
    if (sizeExceeded) {
        window.alert(maxExceededMessage);
        $(inputFile).val('');
    };

    // if (extError) {
    //     window.alert(extErrorMessage);
    //     $(inputFile).val('');
    // };
}
function validateagentForm(role) {
    $(function() {
        if (!$('#' + role + 'form').length) {
            return false;
        }
        var formRules = {
            rules: {
                // "user[profile_attributes][company_name]": {
                //     required: true
                // },
                "user[profile_attributes][years_in_business]": {
                    required: true,
                },
                // "user[profile_attributes][tax_id]": {
                //     required: true,
                // },
                "user[profile_attributes][street_address]": {
                    required: true,
                },
                // "user[profile_attributes][ein]": {
                //     required: true,
                // },
                "user[profile_attributes][city]": {
                    required: true
                },
                // "user[profile_attributes][state]": {
                //     required: true
                // },
                "user[profile_attributes][zip_code]": {
                    required: true
                },
                "user[name]":{
                    required: true
                },
                // "user[phone_number]":{
                //     required: true,
                //     remote: true,
                //     number: true
                // },
                // "user[phone_number]": {
                //     required: false,
                //     number: true,
                //     remote: {
                //         url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
                //         type: 'GET',
                //         data: {}
                //     }
                // },
                "user[email]": {
                    required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    email: true,
                    remote: {
                        url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
                        type: 'GET',
                        data: {}
                    }
                },
                "user[password]": {
                    // required: true,
                    minlength: 8
                },
                "user[password_confirmation]": {
                    // required: true,
                    minlength: 8,
                    equalTo: "#user_password"
                },
                // "user[documentation][user_image][]":{
                //     required: {
                //         depends:function(){
                //             if($('#user_form_id').val()===undefined)
                //             {
                //                 return true;
                //             }
                //         }
                //     }
                // },
                // "user[documentation][w9_form_image][]":{
                //     required: {
                //         depends:function(){
                //             if($('#user_form_id').val() === undefined)
                //             {
                //                 return true;
                //             }
                //         }
                //     }
                // },
                // "user[documentation][schedule_a_image][]":{
                //     required: {
                //         depends:function(){
                //             if($('#user_form_id').val()===undefined)
                //             {
                //                 return true;
                //             }
                //         }
                //     }
                // }
            },
            messages: {
                // "user[profile_attributes][company_name]": "Please enter Company Name",
                "user[profile_attributes][years_in_business]": "Please enter Time In Business",
                "user[profile_attributes][tax_id]": "Please enter Tax Id",
                "user[profile_attributes][street_address]": "Please enter Address",
                "user[profile_attributes][ein]": "Please enter SSN",
                "user[profile_attributes][city]": "Please enter city Name",
                "user[profile_attributes][postal_address]": "Please enter Postal Address",
                "user[profile_attributes][city]": "Please enter City name",
                // "user[profile_attributes][state]": "Please enter State Name",
                "user[profile_attributes][zip_code]": "Please enter Zip Code",
                "user[name]": "Please enter Full Name",
                "user[documentation][user_image][]": "Please select Form ID image",
                "user[documentation][w9_form_image][]": "Please select W-9 Form image",
                "user[documentation][schedule_a_image][]": "Please select Schedule A image",
                "user[email]": {
                    required: "Please enter Email",
                    remote: "This Email address already exist!"
                },
                 "user[phone_number]": {
                     required: "Please enter Phone Number",
                     remote: "Invalid format or phone number is already taken!",
                     number: "Must be number"
                 },
                "user[password_confirmation]": {
                    equalTo: "Confirm Password & Password should be same!"
                },
            },
            invalidHandler: function() {
                animate({
                    name: 'shake',
                    selector: '.auth-container > .card'
                });
            }
        };
        $.extend(formRules, config.validations);
        $('#' + role + 'form').validate(formRules);
    });
};

function space_remove(code) {
    switch (code)
    {
        case 1:
            $('#phone_number').addClass('phone-length-1');
            break;
        case 2:
            $('#phone_number').addClass('phone-length-2');
            break;
        case 3:
            $('#phone_number').addClass('phone-length-3');
            break;
        case 4:
            $('#phone_number').addClass('phone-length-4');
            break;
    }
}
$(".percentage_validation").on('click', function () {
    $(this).select();
})
$(function()
{
    $(".percentage_validation").inputFilter(function (value) {
        return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100);
    });
});
$(function() {
    $(".buyrate_fields").on('input', function() {
        this.value = this.value.match(/\d{0,12}(\.\d{0,2})?/)[0];
    });
});
$(document).on('click','.autocomplete_off',function () {
    $(this).removeAttr('autocomplete')
})
$(document).ready(emptyStateFunction);
function emptyStateFunction() {
    if($(".admins_country_field").val() != ""){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(".admins_country_field").val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0 ){
                    $('.admins_states_field_agent').attr("required",true);
                    $('.admins_states_field_agent').attr("disabled",false);

                }else {
                    $("#user_profile_attributes_state").empty();
                    $('.admins_states_field_agent').attr("required",false);
                    $('.admins_states_field_agent').attr("disabled",true);

                }
            }
        });
    }
}