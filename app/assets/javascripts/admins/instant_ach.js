$(document).ready(function() {
    window.onload = $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });
    //........................instant_ach/index.html.........................//
    $('#alert-message-top').fadeOut(5000);
    $('#apply_button').click(function () {
        $('.spnr').fadeIn();
    });

    // $('#ach_table').DataTable( {
    //     "bPaginate": false,
    //     "searching":false,
    //     order: [ [ 2, 'desc' ]],
    //     columnDefs: [ { orderable: false, targets: [0,1,3,4,5,6,7,8] } ]
    // } );

    var isselect = false;
    var first = true;

    var checkedAch =[];
    var unChecked = [];
    var checkboxes = null;
    var isPending = false;
    $.each($("[id=ach-value]"), function(){
        if($(this).val() === "PENDING"){
            isPending = true;
        }
    });
    $(document).on('click', '.approve_btn1',function () {
        myFunction()
    })
    function myFunction() {
        $('.approve_btn1').attr("disabled", 'disabled');
        $('.approve_btn1').css({pointerEvents: "none"});
        $('.disapprove_btn').css({pointerEvents: "none"});
        $('.disapprove_btn').attr("disabled", 'disabled');
    }

    $(document).on("click", ".my-password", function () {
        var x = $("#edit-password-field");
        if (x.attr('type') === "password") {
            x.attr('type','text')
        } else {
            x.attr('type','password')
        }
    });
    $(document).on("click", ".dell", function () {
        var location_id = $(this).data('val')
        delete_img(location_id)
    });
    function delete_img(a) {
        $('#img_up').hide();
        $('#img_form').show();
        $('#edit-customer_signature').attr('required','true');
        if (window.confirm("Are you sure?")) {
            $.ajax({
                url: "/admins/image_delete",
                type: 'GET',
                data: {id: a},
                success: function (data) {
                    $('#img_up').hide();
                    $('#img_form').show();
                    $('#edit-customer_signature').attr('required','true');
                },
                error: function (data) {
                }
            });
        }
    }
    if(isPending){
        $('#all-check').removeAttr('disabled','disabled');
    }
    else{
        // $('#all-check').attr('disabled','disabled');
        $('#all-check').removeAttr('disabled','disabled');
    }
    var selected = [];
    var selected_status = [];
    var uniq_select_status = []
    var dropdownStatus = $("#ach_status").val();
    $(document).on("click", '#check_all', function() {
        uniq_select_status = [];
        checkboxes = $(".all");
        if(checkboxes.prop("checked")){// if all are selected
            $('#all_check').val("false");
            isselect = false;
            if($("#all-check").prop("checked")){
                checkboxes.prop("checked",true);
                selected = JSON.parse($("#all_achs").val());
                $("#unchecked_ach").val("");
            }else{
                selected = [];
                checkboxes.prop("checked",false);
            }
            $('.approve_btn').attr('disabled','disabled');
            console.log(selected)
            checkbox_change(selected.length)

        } else {            // if they are not selected
            console.log("selected")
            console.log($("#ach_ids").val())
            $('#all_check').val("true");
            isselect = true;
            if ($("#ach_gateway_id").val() !== ""){
                $('.approve_btn').removeAttr('disabled','disabled');
            }
            if($("#all-check").prop("checked")){
                checkboxes.prop("checked",true);
                selected = JSON.parse($("#all_achs").val());
                $("#unchecked_ach").val("");
            }else{
                checkboxes.prop("checked",false);
            }
            checkbox_change(selected.length)
            first = false;
        }
        $("#ach_ids").val(selected);
    });
    $(document).on('change', '.all', function(){
        var count_selected_status = 1;
        var ach_gateway = $("#ach_gateway_id").val()
        if ($(this).is(':checked')){
            selected_status.push($(this).data('status'));
            selected.push($(this).val());
            uniq_select_status = selected_status.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            });
            uniq_select_status = uniq_select_status.filter(val => !"undefined".includes(val));
            if (uniq_select_status.length > count_selected_status){
                $('.confirmationDialog').modal({ show: true });
                count_selected_status = uniq_select_status.length
            }else{
                count_selected_status = uniq_select_status.length
            }
            checkbox_change(selected.length)
        } else {
            selected = selected.filter(val => !$(this).val().includes(val));
            unChecked.push($(this).val())
            selected_status = selected_status.filter(val => !$(this).data('status').includes(val));
            uniq_select_status = selected_status.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            });
            if(selected.length > 0) {
                count_selected_status = uniq_select_status.length;
               if(ach_gateway.length>0) {
                   $('.approve_btn').removeAttr('disabled', 'disabled');
               }
            }
            else{
                $('.approve_btn').attr('disabled','disabled');
            }
            checkbox_change(selected.length)
        }
        $("#ach_ids").val(selected);
        $("#unchecked_ach").val(unChecked);
    });
    $('.approve_btn').on('click',function () {
        $('#ach_ids').val();
    });
    function getSelectedUsers() {
        var checkedAch =[]
        if (isselect) {
            if($('#all-check').prop('checked')){
                $.each($("input:checkbox:checked"), function(){
                    checkedAch.push($(this).val());
                });
                checkedAch.shift();
            }else{
                $.each($("input:checkbox:checked"), function(){
                    checkedAch.push($(this).val());
                });
            }
            return checkedAch;
        } else {
            return selected;
        }
    }
    $(".void_submit_button").on('click',function(){
        $(".achModal").modal().close();
    });

    $("#ach_gateway_id").on("change",function(){
        gateway_change();
        if ($("input:checkbox:checked").length > 0 && $("#ach_gateway_id").val() !== ""){
            $('.approve_btn').removeAttr('disabled','disabled');
        }else{
            $('.approve_btn').attr('disabled','disabled');
        }
    });

    $("#ach_status").on("change", function () {
        ach_status_change();
    })


    $("#ach_gateway_id").on("change", function () {
        var ach_gateway_id = parseInt($(this).val());
        if ($("#ach_gateway_id").val() !== ""){
    //            console.log(ach_gateway_id)
            var newUrl = $(".export_btn").attr("href") + "&ach_gateway_id="+ach_gateway_id;
            $(".export_btn").attr('href', newUrl)
        }
    });

     $('.approve_btn').attr('disabled','disabled');
    $('.submit_btn').attr('disabled','disabled');


    $(".export_btn").on('click', function () {
        var checkedAchs = [];
        var ach_list = [];
        var ach_ids = [];
        var unCheckedAch = [];
        var batch_date = $('#batch_date_input').val();
        var ach_gateway_id = $("#ach_gateway_id").val()
        var newUrl ="/admins/instant_ach_verification?ach_gateway_id="+ach_gateway_id+"&batch_date="+batch_date;
        var ids = []
        checkedAchs = $("#ach_ids").val();
        unCheckedAch = $("#unchecked_ach").val();
        ach_list = $("#ach_list").val();
        ach_list = JSON.parse(ach_list)
        ach_list = ach_list.filter(val => !unCheckedAch.includes(val));
        if($('#all-check').prop('checked')){
            ach_ids = ach_list
        }else{
            ach_ids = JSON.parse("[" + checkedAchs + "]");
        }
        ids = $.param({ ach_ids: ach_ids });
        newUrl+="&"+ids+"&offset="+encodeURIComponent(moment().local().format('Z'));
        $(".export_btn").attr('href', newUrl)
    });

    $(".submit_btn").click(function (e) {
        var status = $("#ach_status").val();
        var ach_status = "";
        var checkedAchs = [];
        var ach_list = [];
        var ach_ids = [];
        var unCheckedAch = [];
        var ids = []
        var gateway_type = ""

        checkedAchs = $("#ach_ids").val();
        unCheckedAch = $("#unchecked_ach").val();
        ach_list = $("#ach_list").val();
        ach_list = JSON.parse(ach_list)
        checkedAchs = checkedAchs.split(",");
        checkedAchs = checkedAchs.filter(val => !unCheckedAch.includes(val));
        ach_list = ach_list.filter(val => !unCheckedAch.includes(val))
        if($('#all-check').prop('checked')){
            ach_ids = ach_list
        }else{
            ach_ids = checkedAchs
        }
        $(".all:checked:checked").each(function () {
            if ($(this).data("gateway_type") == "manual_integration") {
                gateway_type = "manual_integration";
            }
            if ($(this).data("status") == "PAID" || $(this).data("status") == "IN_PROGRESS") {
                ach_status = $(this).data("status")
            }
        })
        if (gateway_type == "manual_integration" && (ach_status == "PAID" || ach_status == "IN_PROGRESS") && status == "FAILED") {
            e.preventDefault();
            swal({
                text: "Changing the status from "+ ach_status +" to Failed will return the ACH amount without the fees to the user.Are you sure you want to continue?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                closeOnCancel: true
            }).then((isConfirm) => {
                if (isConfirm.value === true) {
                    $("#bulk_status_form")[0].submit();
                } else {

                }
            });
        } else if (ach_status == "PAID" && gateway_type != "manual_integration" && status == "FAILED"){
            e.preventDefault();
            swal("Alert!", "This status can only be applied for Manual Integration", "warning")
        }
        $("#ach_ids").val(ach_ids)
        // ids = $.param({ ach_ids: ach_ids });
        // console.log(ids)
        //
        // if ($("#ach_status").val() !== ""){
        //     var newUrl = $(".submit_btn").attr("href") + "?" + ids + "&status="+status;
        //     $(".submit_btn").attr('href', newUrl)
        // }
    })

    $('#boolValue').change(function () {
        var ids = $('#achs_ids').val()
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            var msg = $(this).is(':checked') == true ? 'enabled' : 'disabled';
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/under_review",
                    type: 'get',
                    data: {id: ids, status: msg},
                    success: function (data) {
                        swal("Updated!", "ACHS Under Reviewed " + msg, "success");
                    },
                    error: function (error) {
                        console.error('Update ACH Issue Error: ', error);
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
                $(this).prop("checked", !$(this).is(':checked'));
            }
        });
    });


    $(".padd").on("click", function () {
        $("#cover-spin").show();
    })

    var create_at = ""
    $("#create_at_sort").on('click', function () {
        create_at = $(this).attr('class')
        $("#create_at_sort_value").val(create_at);
    });
//..........................................instant_ach/_data_table.html.............................//

        var sort_type = "";
        var create_sort = $("#create_at_sort_value").val();
        if(create_sort !== ""){
            create_sort = create_sort.split("_");
            sort_type = create_sort[1];
            $("#create_at_sort").removeClass();
            $("#create_at_sort").addClass(create_sort);
        }
        if(sort_type === ""){
            sort_type = 'desc'
        }
        $('#ach_table').DataTable( {
            "bPaginate": false,
            "searching":false,
            "bInfo":false,
            order: [ [ 2, sort_type ]],
            columnDefs: [ { orderable: false, targets: [0,1,3,4,5,6,7,8] } ]
        });
        // Ach Export End
    var globalTimeout = null;
    $(document).on('keyup','#search',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var search_value = $('#search').val();
            var batch_date = $('#batch_date_input').val();
            var batch_search_params = $('#batch_search_params').val();
            if(search_value === ""){
                search_value = "__"
            }
            var filter = $('#filter_input').val();
            var batch_ach_id = $('#batch_ach_id_input').val();
            $.ajax({
                url: "/admins/instant_ach/achs_search",
                type: "GET",
                data: {q: search_value,filter: filter, batch_date: batch_date,batch_ach_id: batch_ach_id, batch_search_params: batch_search_params}
            })
        }, 1500);
    });

    $(document).on("change", ".withdraw-filter", function () {
        $(this).form().submit();
    })
})
function ach_status_change(){
    var status = "";
    var in_process = "";
    $("input:checkbox:checked").each(function(){
        if($(this).data("status") === "PENDING" ){
            status = "PENDING"
        }
        if($(this).data("status") === "IN_PROCESS" ){
            in_process = "IN_PROCESS"
        }
    });
    var ach_gateway = $("#ach_gateway_id").val();
    var dropdownStatus = $("#ach_status").val();
    var checked = $(".all:checked").length;
    var checkedStatus = $("input:checkbox:checked").data('status')
    if(status === "PENDING" && (dropdownStatus === "PAID_WIRE" || dropdownStatus === "PAID" || dropdownStatus === "FAILED")) {
        alert("Must select Void or In-Progress");
        $("#ach_gateway_id").prop('disabled',true);
        $("#ach_gateway_id").val('');
        $('.submit_btn').attr('disabled', 'disabled');
//          console.log("this 1")
    }
    else if(checked > 0 && dropdownStatus === "IN_PROCESS" && ach_gateway === ""){
        alert("Please select ACH Bank to submit.");
        $('.submit_btn').attr('disabled', 'disabled');
        $("#ach_gateway_id").prop('disabled',false);
    }
    else if(checked > 0 && dropdownStatus === "IN_PROCESS" && ach_gateway !== ""){
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }
    else if(checked > 0 && dropdownStatus === "" && ach_gateway !== ""){
        $('.submit_btn').attr('disabled', 'disabled');
    }
    else if(status !== "PENDING" && checked > 0 && (dropdownStatus === "PAID_WIRE" || dropdownStatus === "PAID" || dropdownStatus === "FAILED")){
        $("#ach_gateway_id").prop('disabled', true);
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }
    else if(checked > 0 && dropdownStatus === "VOID"){
        $("#ach_gateway_id").val('');
        $("#ach_gateway_id").prop('disabled', true);
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }
    else{
        $("#ach_gateway_id").prop('disabled', false);
        $('.submit_btn').attr('disabled', 'disabled');
    }
}
function gateway_change(){
    var status = "";
    var in_process = "";
    $("input:checkbox:checked").each(function(){
        if($(this).data("status") === "PENDING" ){
            status = "PENDING"
        }
        if($(this).data("status") === "IN_PROCESS" ){
            in_process = "IN_PROCESS"
        }
    });
    var ach_gateway = $("#ach_gateway_id").val();
    var dropdownStatus = $("#ach_status").val();
    var checked = $("input:checkbox:checked").length
    if(status === "PENDING" && ($("#ach_status").val() === "PAID_WIRE" || dropdownStatus === "PAID" || dropdownStatus === "FAILED")) {
        alert("Must select Void or In-Progress");
        $("#ach_gateway_id").prop('disabled',true);
        $("#ach_gateway_id").val('');
        $('.submit_btn').attr('disabled', 'disabled');
//           console.log("this 1")
    }
    else if(checked > 0 && dropdownStatus === "IN_PROCESS" && ach_gateway === ""){
//           console.log(ach_gateway,"this 2")
        $('.submit_btn').attr('disabled', 'disabled');
    }
    else if(checked > 0 && dropdownStatus === "IN_PROCESS" && ach_gateway !== ""){
//           console.log(checked)
//           console.log(ach_gateway,"this 3")
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }

}
function checkbox_change(checked_length){
    var status = "";
    var in_process = "";
    $("input:checkbox:checked").each(function(){
        if($(this).data("status") === "PENDING" ){
            status = "PENDING"
        }
        if($(this).data("status") === "IN_PROCESS" ){
            in_process = "IN_PROCESS"
        }
    });
    var isUnCheck = $("input:checkbox").prop('checked');
    var ach_gateway = $("#ach_gateway_id").val();
    var dropdownStatus = $("#ach_status").val();
    var checked = checked_length
    if(status === "PENDING" && isUnCheck === true && ($("#ach_status").val() === "PAID_WIRE" || dropdownStatus === "PAID" || dropdownStatus === "FAILED")) {
        alert("Must select Void or In-Progress");
        $("#ach_gateway_id").prop('disabled',true);
        $("#ach_gateway_id").val('');
        $('.submit_btn').attr('disabled', 'disabled');
    }
    else if(status !== "PENDING" && checked > 0 && (dropdownStatus === "PAID_WIRE" || dropdownStatus === "PAID" || dropdownStatus === "FAILED")){
        $("#ach_gateway_id").prop('disabled', true);
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }
    else if(checked > 0 && dropdownStatus === "IN_PROCESS" && ach_gateway !== ""){
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }
    else if(checked < 1 && dropdownStatus === "IN_PROCESS" && ach_gateway !== ""){
        $('.submit_btn').attr('disabled', 'disabled');
    }
    else if(checked > 0 && ach_gateway !== ""){
        $('.submit_btn').attr('disabled', 'disabled');
        $('.approve_btn').removeAttr('disabled');
    }
    else if(checked < 1){
        $('.submit_btn').attr('disabled', 'disabled');
    }
}

$(document).on('click', '#password_btn_verify',function () {
    var password = $('#password').val();
    $.ajax({
        url: "/admins/buy_rate/verify_admin_password",
        type: 'get',
        data: {password: password},
        success:function(data){
            $("#password_btn_verify").addClass('background-green').text('').text("Verified");
            $("#info-text").text('');
            $('.proceed_now_div').show();
            $('.void_submit_button').show();
            $('#alert-now').show();
        },
        error: function(data){
            $("#password_btn_verify").addClass('btn-border');
            $("#info-text").text('').addClass('color-red').text('Please try again!');
        }
    });
});