var card_array = [];
var is_all_checked = false;
$(document).ready(function () {
    $('#import_modal').on('hidden.bs.modal', function () {
        // do something…
        $("#progress_div").hide();
        $( ".import_btn_modal").removeAttr("disabled");
    })
    $('#alert-message-top').fadeOut(5000);
//.............................blocked_card/_data_table............//
//     var table = $('#datatable-dispute-cases').DataTable({
//         responsive:false,
//         "sorting": true,
//         "searching": false,
// //        "sScrollX": true,
//         "bLengthChange": false,
//         "bInfo":false,
//         "bPaginate": false,
//         "order": [[ 0, "desc" ]],
//         "columnDefs": [
//             { "orderable": false, "targets": 6 }
//         ]
//     });
    $('#details').click(function(){
        $('#cover-spin').fadeIn();
    });
    $(document).on('click', ".pagination_btn",function () {
      $('#cover-spin').fadeIn();
    });
    $(document).on('click', "#import_btn_modal",function () {
        $('#import_modal').modal({backdrop: 'static', keyboard: false});
        $('#import_modal').modal('show');
    });
    $(document).on('click', "#export_modal_button",function () {
        $('#export_modal').modal({backdrop: 'static', keyboard: false});
        $('#export_modal').modal('show');
        $("#before_download_note").show();
        $("#after_download_note").hide();
        var card_type = $("#card_type").val();
        $.ajax({
            url: "/admins/blocked_cards/export_cards",
            type: 'GET',
            data: {card_type: card_type}
        });
    });
    //............................_card.html.erb....................//

     /*   $(function() {
        <% if params[:q].present? %>
        <% if params[:q][:due_date].blank? %>
            $('input[name="q[due_date]"]').val('');
        <% end %>
            <% else %>
            $('input[name="q[due_date]"]').val('');
        <% end %>
            $('input[name="q[due_date]"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
        $('input[name="q[due_date]"]').daterangepicker({
            locale: {
                format: 'MM/DD/YYYY'
            }
        });
    function show_disputes(url,disputeCase) {
        window.location.href =
            url + "?case="+ disputeCase
    }*/
     //...............................import_cards.html.erb....................//
    $('#datatable-keytable_import').DataTable({
        responsive: true,
        sorting: true,
        paging: true,
        bfilter: true,
        searching: true
    });
    $("#progress_div").hide();
    $(document).on('click', "#all_blocked_card",function () {
        if ($(this).is(":checked")){
            is_all_checked = true
            $(".all_check").prop('checked',true);
            $(".remove_btn_blocked").attr("disabled", false);
            card_array = JSON.parse($("#all_blocked_card").val());
            $("#block_card_arr").val(card_array);

        }
        else{
            is_all_checked = false
            $(".all_check").prop('checked',false);
            $(".remove_btn_blocked").attr("disabled", true);
            card_array = [];
            $("#block_card_arr").val(card_array);

        }

    });
    $(document).on('change', "#blocked_card_filter",function () {
        $(this).submit();
    });

    $(document).on('click', ".block_card_checkbox_all",function () {

        if ($(this).is(":checked")){
            card_array.push($(this).val());
            $("#block_card_arr").val(card_array)
        }
        else{
            const index = card_array.indexOf($(this).val());
            if (index > -1) {
                card_array.splice(index, 1);
            }
            $("#block_card_arr").val(card_array)
        }

        if (card_array.length > 1){
            $(".remove_btn_blocked").attr("disabled", false);
        }
        else if (card_array.length < 2){
            $(".remove_btn_blocked").attr("disabled", true);
        }
    });
    $(document).on("click", "#edit_card", function () {
        id=$(this).data('id');
            $('#card_id').val(id);

    });

    $('#password_btn').on('click',function () {
        var password = $('#password').val();
        var wallet_id=$('#wallet_id').val();
        var fee_id=$('#fee_id').val();
        var location_id=$('#location_id').val();
        if(password.length > 0) {
            $.ajax({
                url: "/admins/buy_rate/verify_admin_password",
                method: 'get',
                data: {password: password},
                success: function (data) {
                    $('#unlock').modal('toggle');
                    $('.close_unlock').click();
                    $("#password_btn").css("background", "green").text('').text("Verified");
                    $("#info-text").text('');
                    $.ajax({
                        url: "/admins/transactions/block_card",
                        method: 'post',
                        data: {card_id: $('#card_id').val()},
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            swal("Cancelled!", "Card is already blocked!", "error")
                        }
                    });
                },
                error: function (data) {
                    $("#password_btn").css("border", "1px solid red");
                    $("#info-text").text('').css("color", "red").text('Please try again!');
                }
            });
        }
    });

})