// risk-score-guage
$(document).ready(function() {
    $('.risk-score-guage').kumaGauge({
        value: Math.floor($('#ac-risk-score').html()),
        gaugeWidth: 15,
        radius: 40,
        paddingX: 18,
        paddingY: 0,
        showNeedle: false,
        valueLabel: {
            display: false
        },
        label : {
            display: false,
            fontSize : 1
        },
        fill :'0-#1cb42f:0-#fdbe37:50-#fa4133:100',

    });
});

$(document).ready(function() {
    $('.ip-risk-score-guage').kumaGauge({
        value: Math.floor($('#ip-risk-score').html()),
        gaugeWidth: 15,
        radius: 40,
        paddingX: 18,
        paddingY: 0,
        showNeedle: false,
        valueLabel: {
            display: false
        },
        label : {
            display: false,
            fontSize : 1
        }
    });
});