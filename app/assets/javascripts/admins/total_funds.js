$('#alert-message-top').fadeOut(5000);
$(document).ready(function () {
    $("#search").show()
    $('#boolValue').change(function () {
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            var msg = $(this).is(':checked') == true ? 'enabled!' : 'disabled!';
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/app_configs/",
                    type: 'POST',
                    data: {id: '<%= @issue_checks.id %>', key: '<%= @issue_checks.key %>', boolValue: $(this).checked},
                    success: function (data) {
                        swal("Updated!", "Send Checks are successfully " + msg, "success");
                    },
                    error: function (error) {
                        console.error('Update Check Issue Error: ', error);
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
                $(this).prop("checked", !$(this).is(':checked'));
            }
        });
    });
    const urlParams = new URLSearchParams(window.location.search);
    const queryParam = urlParams.get('q');
    const dateParam = urlParams.get('batch_date');
    if(queryParam != ""){
        if(dateParam == ""){
            $('input[name="q[batch_date]"]').val('')
        }
    }
    else {
        $('input[name="q[batch_date]"]').val('')
    }
    $('input[name="q[batch_date]"]').on('hide.daterangepicker', function (ev, picker) {
        $('input[name="q[batch_date]"]').val('');
    });

    $('input[name="q[batch_date]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="q[batch_date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="q[batch_date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
    });

    $('.datepicker').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY'
        },
        "parentEl": $(".ui-front1")
    });
    var lt= $('.timeUtc1').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY');
        v.innerText = local
    });

});
