$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000);
    $(".padd").css("margin","15px 15px 20px 0px");
var table = $('#datatable-keytable').DataTable({
    responsive:false,
    "sorting": true,
    "bLengthChange": false,
    "searching": false,
    "bInfo":false,
    "bPaginate": false,
    "order": [[ 0, "desc" ]],
    "columnDefs": [
        { "orderable": false, "targets": 6 }
    ]
});
    const urlParams = new URLSearchParams(window.location.search);
    const queryParam = urlParams.get('query');
    const dateParam = urlParams.get('date');
    if(queryParam != ""){
        if(dateParam == ""){
            $('input[name="query[date]"]').val('');
        }
    }
    else {
        $('input[name="query[date]"]').val('');
    }
function show_merchant_transaction(url) {
    window.location.href = url
//            url + "&offset=" + encodeURIComponent(moment().local().format('Z'))
}

    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });


    /*<% if params[:query].present? %>
    <% if params[:query][:date].blank? %>
        $('input[name="query[date]"]').val('');
    <% end %>
        <% else %>
        $('input[name="query[date]"]').val('');
    <% end %>*/
        $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    $('input[name="q[batch_date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="q[batch_date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
    });

    $('input[name="query[date]"]').daterangepicker({
        //autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY'
        }
    });
    $(document).on('click','.hold_click',function () {
        $('#cover-spin').show(0);
    })
    //..............................transaction.html.erb.............//


    $( "#transaction tr" ).click(function(){
        // event.stopPropagation();
        console.log('hey')
        var link  = $(this).data("href");
        setTimeout(function() {
            $('#cover-spin').fadeOut("slow");
            // alert("Something went wrong with this detail. Please try again later!")
        }, 15000 );
        $.ajax({
            url: link,
            type: "post"
        });
    });
});