
$( document ).ready(function() {
    $('#alert-message-top').fadeOut(5000);
    //------------------------payments_gateways...........................//
    var table = $('#keytable').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": true,
        "ordering": true,
        "searching": true,
        "bLengthChange": true,
        "order": [[ 0, "desc" ]]
    });

    $('.datable_gateway').DataTable({
        "responsive": false,
        "scrollX": false,
        "scrollY": "250px",
        "scrollCollapse": true,
        "sSearch": false,
        "ordering": false,
        "searching": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "initComplete" : function () {
            $('.dataTables_scrollBody thead tr').addClass('hidden');
            $('.dataTables_scrollBody tfoot tr').addClass('hidden');
        },
        "columnDefs": [
            { "orderable": false, "targets": [10,11] },
        ]
    });

    $('#mid_disable').change(function() {
        var mid = false;
        if($(this).is(":checked")) {
            mid = true;
        }
        $.ajax({
            url: "/admins/app_configs/mid_disable" ,
            type: 'Post',
            data: {mid_disable: mid},
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $(".change_state").click(function() {
        var change_state = $(this).data('change_state');
        $("#change_s").text(change_state);
        var href = $(this).data('href');
        $(".archive-yes-btn").attr("href", href);
        $("#ArchiveModal").modal('toggle');
    });

    $('.archive_descriptor').change(function() {
        // $("#ArchiveModal").modal('toggle');
        var status = false;
        var id = $(this).data('id');
        if($(this).is(":checked")) {
            status = true;
        }
        $.ajax({
            url: "/admins/payment_gateways/archive_descriptor" ,
            type: 'Post',
            data: {status: status, id: id},
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    $(document).on('change','#srch-term',function () {
        $('#model-form').submit();
    })
    //...........................notification_vertical_types.html.erb................//

    $('#close_modal_button').click(function(){
        setTimeout(function(){
            $(document.body).addClass( 'modal-open' );
        }, 500);
    });
    $('#dispute_reason_form_title_field').keypress(function(){
        if(this.value=="" || this.value==null){
            $('#warning_showing_span').text("Fill Up Your Fields!");
        }else{
            $('#warning_showing_span').text("");
        }
    });

    $( "#dispute_reason_form_submit" ).one( "click", function() {
        $('#notificationReasonModal').modal('toggle');
        var payment_id =$('#format_input').val();
        $.ajax({
            url: "/admins/payment_gateways/postverticaltype",
            type: 'POST',
            data: {title:$('#dispute_reason_form_title_field').val(), pay_id: payment_id},
            success: function(result){
                if(result.message){
                    $('#warning_showing_span').text(result.message);
                }else{
                    $('#table_text_field_transaction_dispute').append($('<option></option>').attr('value',result.id).text(result.title));
                    $('#table_text_field_transaction_dispute_edit').append($('<option></option>').attr('value',result.id).text(result.title));
                }
                $("#dispute_reason_form_title_field2").val('');
                return true;
            }});
    });
    //.......................................archived_gateways.html.erb/.............//
    $('.payment_gateway_client_secret').hide();
    $('.payment_gateway_client_id').hide();
    $('.payment_gateway_authentication_id').hide();
    $('.payment_gateway_signature_maker').hide();
    $('.payment_gateway_processor_id').hide();
    $('.new_gateway').click(function () {
        $('.myModal').modal();
    });


    $('#payment_gateway_type').on('change', function () {
        if ($(this).val() == "stripe") {
            $("label[for='payment_gateway_client_id']").text('').text('Client ID');
            $("label[for='payment_gateway_client_secret']").text('').text('Client Secret');
            $("label[for='payment_gateway_authentication_id']").text('').text('Authentication');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        } else if ($(this).val() == "converge") {
            $("label[for='payment_gateway_client_id']").text('').text('Converge Merchant ID');
            $("label[for='payment_gateway_client_secret']").text('').text('Converge Pin');
            $("label[for='payment_gateway_authentication_id']").text('').text('Converge User ID');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').show();
            $('.payment_gateway_signature_maker').hide();
        } else if ($(this).val() == "fluid_pay") {
            $("label[for='payment_gateway_client_id']").text('').text('Client ID');
            $("label[for='payment_gateway_client_secret']").text('').text('Client Secret');
            $("label[for='payment_gateway_authentication_id']").text('').text('Authentication');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_processor_id').show();
            $('.payment_gateway_client_id').hide();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        } else if ($(this).val() == "i_can_pay") {
            $("label[for='payment_gateway_client_id']").text('').text('Auth Password');
            $("label[for='payment_gateway_client_secret']").text('').text('Secret Key');
            $("label[for='payment_gateway_authentication_id']").text('').text('Authentication ID');
            $("label[for='payment_gateway_signature_maker']").text('').text('Signature Maker');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').show();
            $('.payment_gateway_signature_maker').show();
        } else if ($(this).val() == "bolt_pay") {
            $("label[for='payment_gateway_client_secret']").text('').text('Merchant Token');
            $("label[for='payment_gateway_client_id']").text('').text('API Key');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        } else {
            $('.payment_gateway_client_secret').hide();
            $('.payment_gateway_client_id').hide();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        }
    });
});
$(document).on("blur focusout", "#payment_gateway_name", function () {
    $(".duplicate_descriptor").text("");
    var gateway = $("#payment_gateway_descriptor_id").val();
    if(gateway != ""){
        var input =$(this).val();
        $.ajax({
            url: "/admins/payment_gateways/unique_descriptor",
            type: 'GET',
            data: {id: gateway,descriptor: input},
            success: function(result){
                if (result == false){
                    $(".duplicate_descriptor").text("Descriptor already exists!")
                }

            }
        });
    }
});

$(document).on("blur focusout change", "#payment_gateway_descriptor_id", function () {
    $("#payment_gateway_key").val($("#payment_gateway_descriptor_id option:selected").text());
});