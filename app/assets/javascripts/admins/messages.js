 $('#inbox-messages').on('scroll', function() {
        let div = $(this).get(0);

        var scrollTop = $(this).scrollTop();
        console.log("div.scrollTop - " +div.scrollTop+ " - div.clientHeight - "+ div.clientHeight + " - div.scrollHeight - " + div.scrollHeight)
        // if(div.scrollTop + div.clientHeight <= div.scrollHeight){
        if(div.scrollTop == 0) {
            let t_id = $("#transaction_id").val();
            let last_msg_id = $("#last_msg_id").val(); // test it
            let merchant_id = $("#merchant_id").val(); // test it

            $.ajax({
                url: "/admins/messages/show",
                type: 'GET',
                dataType: "json",
                data: {id: t_id,last_msg_id: last_msg_id},
                success: function (data) {
                    msgs = data
                    if(msgs.length > 0) {
                        jQuery.each(msgs, function (i, data) {
                            // merchant or what
                            if(this.text == null && this.read_at != null){
                                $("#msgs_rows").prepend(' <div class="col-lg-12 col-sm-12 col-md-12">\n' +
                                                '                  <p class="available-time no-more">No more messages</p>\n' +
                                                '               </div>')
                            }
                            else{
                                let text = this.text != null ? this.text: " <a href="+this.document_url+" target='_blank'>"+this.document_file_name+"</a>"
                                if(merchant_id == this.recipient_id || merchant_id == this.document_file_size){
                                    $("#msgs_rows").prepend("<div class=\"col-lg-12 col-sm-12 col-md-12\">\n" +
                                        "                        <div class=\"row margin-15\" >\n" +
                                        "                          <div class=\"col-lg-6 col-sm-6 col-md-6\"></div>\n" +
                                        "                          <div class=\"col-lg-6 col-sm-6 col-md-6\">\n" +
                                        "                            <div class=\"chat-respond-messages pull-right\">\n" +
                                        "                              <div class=\"respond-chat-container\">\n" +
                                        "                                <div class=\"chat-respond msg\">\n" +
                                        "                                  <div class=\"flippd\">\n" +
                                        "                                    <div class=\"chatmsg\">\n" +
                                        "                                     " + text +"\n" +
                                        "                                    </div>\n" +
                                        "                                  </div>\n" +
                                        "                                </div>\n" +
                                        "                              </div>\n" +
                                        "                            </div>\n" +
                                        "                          </div>\n" +
                                        "                        </div>\n" +
                                        "                      </div>")
                                }else {
                                    $("#msgs_rows").prepend(" <div class=\"col-lg-12 col-sm-12 col-xs-10 col-md-12\">\n" +
                                        "                        <div class=\"row chatbox_responsive_row margin-15\">\n" +
                                        "                          <div class=\"col-lg-6 col-sm-6 col-md-6\">\n" +
                                        "                            <div class=\"chat-sender-messages\">\n" +
                                        "                              <div class=\"chat-container\">\n" +
                                        "                                <div class=\"chat-sender msg\">\n" +
                                        "                                  <div class=\"chatmsg\">\n" +
                                        "                                    " + text + "\n" +
                                        "                                  </div>\n" +
                                        "                                </div>\n" +
                                        "                              </div>\n" +
                                        "                            </div>\n" +
                                        "                          </div>\n" +
                                        "                          <div class=\"col-lg-6 col-sm-6 col-md-6\"></div>\n" +
                                        "                        </div>\n" +
                                        "                      </div>")
                                }
                            }
                        });
                        $("#last_msg_id").val($(msgs).get(-1).id);  // test it
                        $('#inbox-messages').scrollTop(div.clientHeight)
                    }
                },
                error: function (data) {
                    $('#cover-spin').fadeOut();
                }
            });

        }
    });
 $('#inbox-messages').scrollTop($('#inbox-messages').height())

 $(document).on('shown.bs.modal', '#customer-dispute-detail', function (e) {
     $('#inbox-messages').scrollTop(99999999999999);
 });