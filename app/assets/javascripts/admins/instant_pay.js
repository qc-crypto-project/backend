$(document).ready(function () {
//-----------------------------------------instat_pay/index.html.erb................//
    var create_at = ""
    $("#create_at_sort").on('click', function () {
        create_at = $(this).attr('class')
        $("#create_at_sort_value").val(create_at);
    });

    var sort_type = "";
    var create_sort = $("#create_at_sort_value").val();
    if(create_sort !== ""){
        create_sort = create_sort.split("_");
        sort_type = create_sort[1];
        $("#create_at_sort").removeClass();
        $("#create_at_sort").addClass(create_sort);
    }
    if(sort_type === ""){
        sort_type = 'desc'
    }

    $('#ach_table').DataTable( {
        "bPaginate": false,
        "searching":false,
        "bInfo":false,
        order: [ [ 2, sort_type ]],
        columnDefs: [ { orderable: false, targets: [0,1,3,4,5,6,7,8] } ]
    });

    $('#alert-message-top').fadeOut(5000);
        $('#apply_button').click(function () {
            $('.spnr').fadeIn();

        var isPending = false;
        $.each($("[id=ach-value]"), function(){
            if($(this).val() === "PENDING"){
                isPending = true;
            }
        });

        if(isPending){
            $('#all-check').removeAttr('disabled','disabled');
        }
        else{
            // $('#all-check').attr('disabled','disabled');
            $('#all-check').removeAttr('disabled','disabled');
        }

        $('.approve_btn').on('click',function () {
            $('#ach_ids').attr('value', getSelectedUsers());
        });
        function getSelectedUsers() {
            var checkedAch =[]
            if (isselect) {
                if($('#all-check').prop('checked')){
                    $.each($("input:checkbox:checked"), function(){
                        checkedAch.push($(this).val());
                    });
                    checkedAch.shift();
                }else{
                    $.each($("input:checkbox:checked"), function(){
                        checkedAch.push($(this).val());
                    });
                }
                return checkedAch;
            } else {
                return selected;
            }
        }
        $(".void_submit_button").on('click',function(){
            $(".achModal").modal().close();
        });
    });

    $("#ach_gateway_id").on("change", function () {
        var ach_gateway_id = parseInt($(this).val());
        if ($("#ach_gateway_id").val() !== ""){
            var newUrl = $(".export_btn").attr("href") + "&ach_gateway_id="+ach_gateway_id;
            $(".export_btn").attr('href', newUrl)
        }
    });

    $(".export_btn").on('click', function () {
        var checkedAch =[];
        var batch_date = "<%= params[:batch_date] %>";
        var ach_gateway_id = $("#ach_gateway_id").val()
        var newUrl ="/admins/instant_ach_verification?ach_gateway_id="+ach_gateway_id+"&batch_date="+batch_date;
        var ids = []
        if(!$('#all-check').prop('checked')){
            $.each($("input:checkbox:checked"), function(){
                checkedAch.push($(this).val());
            });
            ids = $.param({ ach_ids: checkedAch });
            newUrl+="&"+ids+"&offset="+encodeURIComponent(moment().local().format('Z'));
            $(".export_btn").attr('href', newUrl)
        }else{
            $.each($("input:checkbox:checked"), function(){
                checkedAch.push($(this).val());
            });
            not_checked=[]
            $.each($("input:checkbox:not(:checked)"), function(){
                not_checked.push($(this).val());
            });
            checkedAch.shift();
            var all_list=[]
            var all_list= $('#ach_list').val()
            all_list=JSON.parse(all_list)
            for(i=0;i<all_list.length;i++){
                if(all_list[i]!=checkedAch[i]){
                    checkedAch.push(all_list[i])
                }
            }
            //checkedAch = all_list.filter(val => !not_checked.includes(val));
            ids = $.param({ ach_ids: checkedAch });
            newUrl+="&"+ids+"&offset="+encodeURIComponent(moment().local().format('Z'));
            $(".export_btn").attr('href', newUrl)
        }
    });
   var lt= $('.timeUtc2').each(function (i,v) {
       var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
       var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
       v.innerText = local
   });
    $('#boolValue').change(function () {
        var ids=$('#achs_ids').val()
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            var msg = $(this).is(':checked') == true ? 'enabled' : 'disabled';
            if (isConfirm.value === true) {
                $.ajax({
                    url: "<%= admins_under_review_path %>",
                    type: 'get',
                    data: { id: ids, status: msg },
                    success: function(data) {
                        swal("Updated!", "P2C Under Review " + msg, "success");
                    },
                    error: function(error){
                        console.error('Update P2C Issue Error: ', error);
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
                $(this).prop("checked", !$(this).is(':checked'));
            }
        });
    });

    //*************Bulk Status Change Functionality**************

        var selected = [];
        var unChecked = [];
        var checkboxes = [];
        var uniq_select_status = [];
        var selected_status = [];
        $(document).on("click", '#check_all', function() {
            checkboxes = $(".all");
            if(checkboxes.prop("checked")){// if all are selected

                $('#all_check').val("false");
                isselect = false;
                // $('.approve_btn').attr('disabled','disabled');
                if($("#all-check").prop("checked")){
                    checkboxes.prop("checked",true);
                    selected = JSON.parse($("#pc_ids").val());
                }else{
                    selected = [];
                    checkboxes.prop("checked",false);
                }
            } else {            // if they are not selected
                $('#all_check').val("true");
                if($("#all-check").prop("checked")){
                    checkboxes.prop("checked",true);
                    selected = JSON.parse($("#pc_ids").val());
                }else{
                    checkboxes.prop("checked",false);
                }
                first = false;
            }
            $("#p2c_ids").val(selected);
            detect_change_for_submit()
            p2c_checkbox_functionality()
        });
        $("#ach_gateway_id").on("change",function(){
            detect_change_for_submit()
        });

        $("#p2c_status").on("change",function(){
            detect_change_for_submit()
            p2c_checkbox_functionality()
        });

        $(".all").on("change", function () {
            detect_change_for_submit()
        });
        $(document).on('change', '.all', function(){
            var count_selected_status = 1;
            if ($(this).is(':checked')){
                selected_status.push($(this).data('status'));
                selected.push($(this).val());
                unChecked = unChecked.filter(val => !$(this).val().includes(val));
                uniq_select_status = selected_status.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                uniq_select_status = uniq_select_status.filter(val => !"undefined".includes(val));
                if (uniq_select_status.length > count_selected_status){
                    $('.confirmationDialog').modal({ show: true });
                    count_selected_status = uniq_select_status.length
                }else{
                    count_selected_status = uniq_select_status.length
                }
            } else {
                unChecked.push($(this).val());
                selected = selected.filter(val => !$(this).val().includes(val));
                selected_status = selected_status.filter(val => !$(this).data('status').includes(val));
                uniq_select_status = selected_status.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                if(selected.length > 0) {
                    count_selected_status = uniq_select_status.length;
                }
            }
            $("#p2c_ids").val(selected);
            $("#unchecked_ach").val(unChecked);
        });

    //************End of Bulk Status Change Functionality*****************


    //************Search Functionality****************
    var globalTimeout = null;
    $('#search').on('keyup',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var batch_date = $('#batch_date_input').val()
            var batch_search_params = $('#batch_search_params').val();
            var search_value = $('#search').val();
            var batch_p2c_id = $("#batch_p2c_id").val();
            if(search_value == ""){
                search_value = "__"
            }
            var filter = $('#filter_input').val()
            $.ajax({
                url: "/admins/instant_pay_index",
                type: "GET",
                data: {batch_date: batch_date, q: search_value,batch_p2c_id: batch_p2c_id , filter: filter, ach_ids: $("#ach_ids").val(), batch_search_params: batch_search_params}
            })
        }, 1500);
    });

        var create_at = ""
        $("#create_at_sort").on('click', function () {
            create_at = $(this).attr('class')
            $("#create_at_sort_value").val(create_at);
        });
        //---------------------------------instant_pay/_data_table.html.erb.............................//


    $(document).on('click','.view_check_details',function (e) {
        $(".date-modal-class").css("pointer-events", "none");
    })

    $(document).on("change", ".withdraw-filter", function () {
        $(this).form().submit();
    })
})
function p2c_checkbox_functionality(){
    var all_p2c_ids = $("#p2c_ids").val();
    var selected_p2c = JSON.parse($("#p2c_ids_with_status").val());
    var other_status_ids = []
    var p2c_ids = []
    var pending_status_ids = []
    selected_p2c.map(function(val, index){
        if(val[1] === "PENDING"){
            pending_status_ids.push(val[0]);
        }else{
            other_status_ids.push(val[0])
        }
        p2c_ids.push(val[0]);
    });
    var dropdown_status = $("#p2c_status").val();
    var unSelected = [];
    var selected = [];
    var unselected_val = $("#unchecked_ach").val();
    unselected_val = unselected_val.split(",").map(function(n) {
        return Number(n);
    });
    if(dropdown_status === "PAID"){
        $("input:checkbox:checked").each(function(){
            if($(this).data("status") !== "PENDING"){
                unSelected.push($(this).val());
            }else{
                selected = pending_status_ids;
            }
        });
        $("#unselected_p2c").val(unSelected)
    }else if(dropdown_status !== ""){
        for(var i = 0; i < unselected_val.length; i++){
            $("#checkbox_animated_"+unselected_val[i]).prop("checked", true)
        }
    }else if(dropdown_status === "" && $("#all-check").prop('checked')){
        for(var i = 0; i < unselected_val.length; i++){
            $("#checkbox_animated_"+unselected_val[i]).prop("checked", true)
        }
    }
    if(unSelected[0] === "on"){
        unSelected.shift();
    }
    for(var i = 0; i < unSelected.length; i++){
        $("#checkbox_animated_"+unSelected[i]).prop("checked", false)
    }
    if($("#all-check").prop('checked') && dropdown_status === "PAID"){
        all_p2c_ids = all_p2c_ids.split(",");
        all_p2c_ids = all_p2c_ids.filter(item => !unselected_val.includes(item))
        $("#p2c_ids").val(all_p2c_ids);
        $("#unchecked_ach").val(unselected_val);
    }else if($("#all-check").prop('checked') && dropdown_status !== "PAID") {
        $("#p2c_ids").val(all_p2c_ids);
        $("#unchecked_ach").val("");
    }else{
        all_p2c_ids = all_p2c_ids.split(",");
        all_p2c_ids = all_p2c_ids.filter(item => !unSelected.includes(item))
        $("#p2c_ids").val(all_p2c_ids);
        $("#unchecked_ach").val(unselected_val);
    }
}

function detect_change_for_submit(){
    var checked = $(".all:checked").length;
    var ach_gateway_id = $("#ach_gateway_id").val();
    var dropDownStatus = $("#p2c_status").val();
    var status = ""
    var ids = $("#p2c_ids").val();
    $("input:checkbox:checked").each(function(){
        if($(this).data("status") === "PENDING" ){
            status = "PENDING"
        }
    });
    if((ids.length > 0 || checked > 0) && (dropDownStatus === "PAID" || dropDownStatus === "PAID_WIRE") && ach_gateway_id !== ""){
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }else if((ids.length > 0 || checked > 0) && (dropDownStatus === "FAILED" || dropDownStatus === "VOID") && dropDownStatus !== ""){
        $('.submit_btn').removeAttr('disabled', 'disabled');
        $("#ach_gateway_id").prop('disabled',true);
        $("#ach_gateway_id").val('');
    }
    else if((ids.length > 0 || checked > 0) && dropDownStatus !== "" && ach_gateway_id !== ""){
        $('.submit_btn').removeAttr('disabled', 'disabled');
    }else{
        $('.submit_btn').attr('disabled', 'disabled');
        $("#ach_gateway_id").prop('disabled',false);
    }
}