$(document).ready(function () {
    //..............index.html.erb.................//
    $('#alert-message-top').fadeOut(5000);
    $('.invoice_checks_12').on('change', function() {
        if ($('#all_invoices').is(":checked"))
        {
            var all_invoices_check = $('#all_invoices').val();
        }
        if ($('#paid_invoices').is(":checked"))
        {
            var paid_invoices_check = $('#paid_invoices').val();
        }
        if ($('#unpaid_invoices').is(":checked"))
        {
            var unpaid_invoices_check = $('#unpaid_invoices').val();
        }
        if ($('#cancelled_invoices').is(":checked"))
        {
            var cancelled_invoices_check = $('#cancelled_invoices').val();
        }
        if ($('#inprogress_invoices').is(":checked"))
        {
            var inprogress_invoices_check = $('#inprogress_invoices').val();
        }

        $.ajax({
            url: "/admins/invoices/invoice_filter",
            type: "GET",
            data: {all_invoices_check: all_invoices_check, paid_invoices_check: paid_invoices_check, unpaid_invoices_check: unpaid_invoices_check, cancelled_invoices_check: cancelled_invoices_check, inprogress_invoices_check: inprogress_invoices_check },

        });

    });
    //................data_table.html.erb.................//

    $('#datatable-keytable_invoice').DataTable({
        responsive: true,
        "sScrollX": false,
        "sscrollX": false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 8 }
        ]
    });
    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'MM/DD/YYYY HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });
    //..................show.html.erb.........................//

    $(".phone_number_div").hide();
    $(".pincode_div").hide();
    $(".error-phone").hide();
    $(".error-pincode").hide();
    $(".pay_button").on("click", function () {
        $(".details").hide();
        $(".phone_number_div").show();
        $(".pay_button").hide();
        $("#modal-label-69").html('').html('Verify Phone Number');
    });

    $("#phone_number_button").on('click', function () {
        var phone_number=$('#phone_number').val();
        var invoice_id=$('#invoice_id').val();
        if(phone_number.length > 0) {
            $.ajax({
                url: "/admins/invoices/verify_phone_number",
                method: 'get',
                data: {phone_number: phone_number, invoice_id: invoice_id},
                success: function () {
                    $('#phone_number_button').html('Verified').removeClass('btn-primary').addClass('btn-success');
                    setTimeout(function () {
                        $('#phone_number').css('display', 'none').fadeOut("slow");
                        $('.phone_number_div').hide();
                        $('.pincode_div').show();
                        $("#modal-label-69").html('').html('Enter Pin Code You Recieved');
                    }, 1000);
                }, error: function (result) {
                    $('#phone_number_button').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger');
                    setTimeout(function () {
                        $('#phone_number_button').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                    }, 1500);
                    $(".error-phone").show();
                }
            });
        }else{
        }
    });


    $("#pincode_button").on('click', function () {
        var pincode=$('#pincode').val();
        var invoice_id=$('#invoice_id').val();
        if(pincode.length > 0) {
            $.ajax({
                url: "/admins/invoices/verify_phone_number",
                method: 'get',
                data: {pincode: pincode, invoice_id: invoice_id},
                success: function () {
                    $('#pincode_button').html('Verified').removeClass('btn-primary').addClass('btn-success');
                    location.href = "/admins/invoices/pay_invoice/" +invoice_id
                }, error: function (result) {
                    $('#pincode_button').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger');
                    setTimeout(function () {
                        $('#pincode_button').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                    }, 1500);
                    $(".error-pincode").show();
                }
            });
        }else{
        }
    });


    $(document).on('click','#pincode_button',function () {
        submitpincode()
    })

})