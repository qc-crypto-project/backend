$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000);
    $('.checks_12').on('change', function() {
        $("#cover-spin").fadeIn();
        if ($('#all_checks').is(":checked")) {
            var all_checks = $('#all_checks').val();
        }
        if ($('#paid_checks').is(":checked")) {
            var paid_checks = $('#paid_checks').val();
        }
        if ($('#unpaid_checks').is(":checked")) {
            var unpaid_checks = $('#unpaid_checks').val();
        }
        if ($('#void_checks').is(":checked")) {
            var void_checks = $('#void_checks').val();
        }
        if ($('#inprogress_checks').is(":checked")) {
            var inprogress_checks = $('#inprogress_checks').val();
        }
        if ($('#printed_checks').is(":checked")) {
            var printed_checks = $('#printed_checks').val();
        }
        if ($('#pending_checks').is(":checked")) {
            var pending_checks = $('#pending_checks').val();
        }
        if ($('#paid_wire').is(":checked")) {
            var paid_wire = $('#paid_wire').val();
        }
        if ($('#failed').is(":checked")) {
            var failed = $('#failed').val();
        }
        var filter_val = $('#filter_inputt').val();
        var batch_date = $('#batch_date_input').val();
        $.ajax({
            url: "/admins/batch_checks",
            type: "GET",
            data: {from_filter: "true",all_checks: all_checks, paid_checks: paid_checks, unpaid_checks: unpaid_checks, void_checks: void_checks, inprogress_checks: inprogress_checks, printed_checks: printed_checks , pending_checks: pending_checks, paid_wire: paid_wire, failed: failed, filter: filter_val,check_ids: $("#check_ids").val(),
                batch_date: batch_date},
            success: function () {
                $("#cover-spin").fadeOut();
            }
        });
    });

    $(document).on('change', '#batch_filter', function () {
        $('#f-form').submit();
    });

    var globalTimeout = null;
    $('#search').on('keyup',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var search_value = $('#search').val();
            if ($('#all_checks').is(":checked")) {
                var all_checks = $('#all_checks').val();
            }
            if ($('#paid_checks').is(":checked")) {
                var paid_checks = $('#paid_checks').val();
            }
            if ($('#unpaid_checks').is(":checked")) {
                var unpaid_checks = $('#unpaid_checks').val();
            }
            if ($('#void_checks').is(":checked")) {
                var void_checks = $('#void_checks').val();
            }
            if ($('#inprogress_checks').is(":checked")) {
                var inprogress_checks = $('#inprogress_checks').val();
            }
            if ($('#printed_checks').is(":checked")) {
                var printed_checks = $('#printed_checks').val();
            }
            if ($('#pending_checks').is(":checked")) {
                var pending_checks = $('#pending_checks').val();
            }
            if ($('#paid_wire').is(":checked")) {
                var paid_wire = $('#paid_wire').val();
            }
            if(search_value == ""){
                search_value = ""
            }
            var batch_search_params = $('#batch_search_params').val();
            var filter = $('#filter_inputt').val();
            var batch_date = $('#batch_date_input').val();
            $.ajax({
                url: "/admins/batch_checks",
                type: "GET",
                data: {q: search_value,all_checks: all_checks, paid_checks: paid_checks, unpaid_checks: unpaid_checks, void_checks: void_checks, inprogress_checks: inprogress_checks, printed_checks: printed_checks , pending_checks: pending_checks, paid_wire: paid_wire,filter: filter, batch_date: batch_date, batch_search_params: batch_search_params}
            })
        }, 1500);
    });

    $('#boolValue').change(function () {
        var ids=$('#achs_ids').val()
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            var msg = $(this).is(':checked') == true ? 'enabled' : 'disabled';
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/under_review",
                    type: 'get',
                    data: { id: ids, status: msg },
                    success: function(data) {
                        swal("Updated!", "Checks Under Reviewed " + msg, "success");
                    },
                    error: function(error){
                        console.error('Update Check Issue Error: ', error);
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
                $(this).prop("checked", !$(this).is(':checked'));
            }
        });
    });
        var selected = [];
        var unChecked = [];
        var checkboxes = [];
        var uniq_select_status = [];
        var selected_status = [];
        $(document).on("click", '#check_all', function() {
            selected = [];
            checkboxes = $(".all");
            if(checkboxes.prop("checked")){// if all are selected

                $('#all_check').val("false");
                isselect = false;
                // $('.approve_btn').attr('disabled','disabled');
                if($("#all-check").prop("checked")){
                    checkboxes.prop("checked",true);
                    selected = JSON.parse($("#check_ids").val());
                }else{
                    checkboxes.prop("checked",false);
                }
                console.log(selected)
            } else {            // if they are not selected
                $('#all_check').val("true");
                if($("#all-check").prop("checked")){
                    checkboxes.prop("checked",true);
                    selected = JSON.parse($("#check_ids").val());
                }else{
                    checkboxes.prop("checked",false);
                }
                // checkbox_change(selected.length)
            }
            $("#checks_ids").val(selected);
            detect_change_for_submit()
        });

        $(document).on('change', '.all', function(){
            var count_selected_status = 1;
            if ($(this).is(':checked')){
                selected.push($(this).val());
                selected_status.push($(this).data('status'));
                uniq_select_status = selected_status.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                uniq_select_status = uniq_select_status.filter(val => !"undefined".includes(val));
                if (uniq_select_status.length > count_selected_status){
                    $('.confirmationDialog').modal({ show: true });
                    count_selected_status = uniq_select_status.length
                }else{
                    count_selected_status = uniq_select_status.length
                }
            } else {
                unChecked.push($(this).val());
                selected = selected.filter(val => !$(this).val().includes(val));
                selected_status = selected_status.filter(val => !$(this).data('status').includes(val));
                uniq_select_status = selected_status.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                if(selected.length > 0) {
                    count_selected_status = uniq_select_status.length;
                }
            }
            $("#checks_ids").val(selected);
            $("#unchecked_ids").val(unChecked);
        });

        $("#ach_gateway_id").on("change",function(){
            detect_change_for_submit()
        });

        $("#check_status").on("change",function(){
            detect_change_for_submit()
        });

        $(".all").on("change", function () {
            detect_change_for_submit()
        })



    function detect_change_for_submit(){
        var checked = $(".all:checked").length;
        var ach_gateway_id = $("#ach_gateway_id").val();
        var dropDownStatus = $("#check_status").val();
        var status = ""
        $("input:checkbox:checked").each(function(){
            if($(this).data("status") === "PENDING" ){
                status = "PENDING"
            }
        });
        if(status === "PENDING" && (dropDownStatus === "PAID_WIRE" || dropDownStatus === "PAID" || dropDownStatus === "FAILED")) {
            alert("Must select Void or In-Progress");
            $("#ach_gateway_id").prop('disabled',true);
            $("#ach_gateway_id").val('');
            $('.submit_btn').attr('disabled', 'disabled');
//          console.log("this 1")
        } else if (checked > 0 && dropDownStatus === "IN_PROCESS" && ach_gateway_id === ""){
            alert("Please select Check Bank to submit.");
            $('.submit_btn').attr('disabled', 'disabled');
            $("#ach_gateway_id").prop('disabled',false);
        }else if (checked > 0 && (dropDownStatus === "PAID_WIRE" || dropDownStatus === "PAID") && ach_gateway_id === ""){
            alert("Please select Check Bank to submit.");
            $('.submit_btn').attr('disabled', 'disabled');
            $("#ach_gateway_id").prop('disabled',false);
        }else if(checked > 0 && dropDownStatus === "IN_PROCESS" && ach_gateway_id !== ""){
            $('.submit_btn').removeAttr('disabled', 'disabled');
        }
        else if(checked > 0 && (dropDownStatus === "FAILED" || dropDownStatus === "VOID") && dropDownStatus !== ""){
            $('.submit_btn').removeAttr('disabled', 'disabled');
            $("#ach_gateway_id").prop('disabled',true);
            $("#ach_gateway_id").val('');
        }else if(checked > 0 && dropDownStatus !== "" && ach_gateway_id !== ""){
            $('.submit_btn').removeAttr('disabled', 'disabled');
            // $("#ach_gateway_id").prop('disabled',false);
        }else{
            $('.submit_btn').attr('disabled', 'disabled');
            $("#ach_gateway_id").prop('disabled',false);
        }
    }


    //----------------------------data_table....................//

    //---------------------------checks/data_table
    $('.padd').css('margin-top','-10px').css('margin-bottom', '10px').css('margin-right', '10px');
    $('#datatable-keytable_checks').DataTable({
        // responsive: true,
        "sScrollX": false,
        "sscrollX": false,
        "order": [[ 0, "desc" ]],
        // "columnDefs": [{ "orderable": false, "targets": 9 }],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "searching": false
//        "buttons": [
//            {extend: 'csv', text: '<i class="fa fa-file"></i> Export', className: 'btn pull-right'}
//        ],
//        dom: "<'row'<'col-sm-2'l><'col-sm-10'<'col-sm-11'f><'col-sm-1 row-padding-zero'B>>>" +
//            "<'row'<'col-sm-12'tr>>" +
//            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    });
    $(".padd").on("click", function () {
        $("#cover-spin").show();
    })
    $(document).on('click','#pincode_button',function () {
        submitpincode()
    })
    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
})