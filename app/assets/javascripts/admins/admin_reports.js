$('#datatable-keytable_cbk').DataTable({
    "dom": 'Bfrltip',
    "lengthMenu": [[10, 25, 50,100], [10, 25, 50,100]],
    "bLengthChange": true,
    "searching": false,
    "sScrollX": false,
    "scrollX": false,
    "buttons": [
        { extend: 'csv', text: 'Export', className: "csv_button btn btn-success" }
    ],
    "exportOptions": {
        modifer: {
            page: 'all',
            search: 'none'    }
    }
});
var lt= $('.timeUtc-user').each(function (i,v) {
    var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
    var local = moment(gmtDateTime).local().format('YYYY-MM-DD hh:mm:ss A');
    v.innerText = local
});
$('#datatable-keytable_merchant').DataTable();
$('#datatable-keytable_sale').DataTable({
    "searching": false,
    "lengthChange": false,
    "bPaginate": false,
    "responsive": true,
    "bInfo" : false
});
