$(document).on('change','#filter',function () {
    $('#dispute').submit();
});


$(document).ready(function () {
    $('input[name="query[dispute_date]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[dispute_date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[dispute_date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[dispute_date]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
        parentEl: $("#creation_date")

    });

    $('input[name="query[dispute_date]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});
