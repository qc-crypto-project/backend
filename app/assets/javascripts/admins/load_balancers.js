$(document).ready(function () {
    $(".amount_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $(document).on("keyup", ".valid_percent", function () {
        $(".valid_percent").inputmask('Regex', {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"});
    });
    $(document).on("keyup", ".amount_dollar", function () {
        $(".amount_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    });
    $(document).on("keyup", ".no_of_hours", function () {
        $(".no_of_hours").inputmask('Regex', {regex: "^[0-9]+$"});
    });
    $(document).on("keyup", ".rules_count", function () {
        $(".rules_count").inputmask('Regex', {regex: "^[0-9]+$"});
    });

    $('#decline_on_cc_country_country').multipleSelect({
        placeholder: "Select Countries",
        selectAll: false
    });
    $('#block_card_for_reason_decline_reason').multipleSelect({
        placeholder: "Select Reason",
        selectAll: false
    });

    // next Step creating group
    $("#nextStep").on("click",function (e){
        e.preventDefault();
        $(".payment_gateways_section_a").addClass("admin-hide").removeClass('admin-show');
        $(".payment_gateways_section_b").addClass("admin-hide").removeClass('admin-show');
        $(".load_balancer_rules_section_a").addClass("admin-show").removeClass("admin-hide");
        $(".load_balancer_rules_section_b").addClass("admin-show").removeClass("admin-hide");
        $("#g-config").removeClass('active_header').addClass("inactive_header");
        $("#g-auto").addClass('active_header').removeClass("inactive_header");
        $("#create_group").show().prop("disabled",true);
        $("#backStep").show();
        $("#nextStep").hide();
        while_edit_load_balancer();
    });

    // next Step creating group
    $("#backStep").on("click",function (e){
        e.preventDefault();
        $(".payment_gateways_section_a").removeClass("admin-hide").addClass('admin-show');
        $(".payment_gateways_section_b").removeClass("admin-hide").addClass('admin-show');
        $(".load_balancer_rules_section_a").removeClass("admin-show").addClass("admin-hide");
        $(".load_balancer_rules_section_b").removeClass("admin-show").addClass("admin-hide");
        $("#g-config").addClass('active_header').removeClass("inactive_header");
        $("#g-auto").removeClass('active_header').addClass("inactive_header");
        $("#create_group").hide().prop("disabled",true);
        $("#nextStep").show();
        $("#backStep").hide();
    });

    // change rule active , inactive
    $(document).on('change','.active_inactive_rule',function () {
        var type = $(this).data('type');
        var id = $(this).data('id');
        var div_id = type +"_"+ id +"_status";
        var value = $("."+ type +"_hidden_"+id).find("input[id='"+ div_id +"']").val();
        if(value == "active"){
            $("."+ type +"_hidden_"+id).find("input[id='"+ div_id +"']").val('inactive');
            if($(this).data('gateway_ids') != ""){
                // able_to_remove_gateway_from_group($(this).data('gateway_ids'),"inactive");
            }
        }else{
            $("."+ type +"_hidden_"+id).find("input[id='"+ div_id +"']").val('active');
            // able_to_remove_gateway_from_group($(this).data('gateway_ids'),"active");
        }
    });

    // Edit/Update Rules
    $(document).on('click','.edit_rule',function () {
        var type = $(this).data('type');
        var id = $(this).data('id');
        $(".rules_error").hide();
        $(".all_rules").addClass("admin-hide").removeClass('admin-show');
        $(".rules_boxes").addClass("admin-hide").removeClass('admin-show');
        $("#"+type+"_new").addClass("admin-show").removeClass("admin-hide");
        $("#"+ type +"_number").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][count]']").val());
        $("#"+ type +"_card_brand").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][card_brand]']").val());
        $("#"+ type +"_count").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][count]']").val());
        $("#"+ type +"_amount").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][amount]']").val());
        $("#"+ type +"_no_of_hours").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][no_of_hours]']").val());
        $("#"+ type +"_percentage").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][percentage]']").val());
        if(jQuery.inArray(type, ["rotate_processor_with_amount", "rotate_processor_on_cc_brand"]) != -1){
            var selected_desc = $("input[name='"+ type +"["+ id +"][descriptor]']").val();
            $("#on_edit_occupied_gateways").val(selected_desc);
            initialize_descriptor_dropdown(type, selected_desc.split(','), "edit");
        }
        if (type == "decline_on_cc_country"){
            $('#decline_on_cc_country_country').val('');
            var selected_desc = $("input[name='"+ type +"["+ id +"][country]']").val();
            $("#decline_on_cc_country_country").val(selected_desc.split(','));
            $("#decline_on_cc_country_country").multipleSelect({placeholder: "Select Countries", selectAll: false});
        }
        if (type == "block_card_for_reason"){
            $('#block_card_for_reason_decline_reason').val('');
            var selected_desc = $("input[name='"+ type +"["+ id +"][decline_reason]']").val();
            $("#block_card_for_reason_decline_reason").val(selected_desc.split(','));
            $("#block_card_for_reason_decline_reason").multipleSelect({placeholder: "Select Reason", selectAll: false});
        }
        if(jQuery.inArray(type, ["rotate_processor_with_amount", "decline_on_cc_country", "rotate_processor_on_cc_brand", "processor_decline_percent"]) != -1){
            initialize_operator_dropdown(type, $("input[name='"+ type +"["+ id +"][operator]']").val(), "edit");
        }
        $("#"+ type +"_operator").val($("."+ type +"_hidden_"+id).find("input[name='"+ type +"["+ id +"][operator]']").val());
        $("#add_rule_to_group_"+type).text('Update Rule');
        $("#add_rule_to_group_"+type).data('update','true');
        $("#add_rule_to_group_"+type).data('count',id);
        $(".payment_gateways_section_b").removeClass("admin-hide").addClass('admin-show');
        $(".load_balancer_rules_section_b").removeClass("admin-show").addClass("admin-hide");
        $("#remove-descriptor").hide();
        $("#backStep").hide();
    });

    // Delete Rules
    $(document).on('click','.delete_rule',function () {
        var type = $(this).data('type');
        var id = $(this).data('id');
        if(jQuery.inArray(type, ["rotate_processor_with_amount", "rotate_processor_on_cc_brand"]) != -1){
            var selected_desc = $("input[name='"+ type +"["+ id +"][descriptor]']").val();
            $("#on_edit_occupied_gateways").val(selected_desc);
            check_available_gateways(selected_desc, "delete");
        }
        if(jQuery.inArray(type, ["rotate_processor_with_amount", "decline_on_cc_country", "processor_decline_percent"]) != -1){
            check_available_operators(type,"delete", $("input[name='"+ type +"["+ id +"][operator]']").val());
        }
        var deleted_id = [];
        if ($("#deleted_rules_ids").val() != ""){
            deleted_id = $("#deleted_rules_ids").val().split(',');
        }
        if ($(this).data('edit') == "true" || $(this).data('edit') == true){
            deleted_id.push($(this).data('rule_id'));
        }
        $("#deleted_rules_ids").val(deleted_id);
        $("."+type+"_hidden_"+id).remove();
        $("."+type+"_rule_box_"+id).remove();
        able_to_remove_gateway_from_group(""+$(this).data('gateway_ids'))
        enable_disable_add_rule_btn(type);
    });

    // button to open rule form
    $(document).on('click','.add_rule_btn',function () {
        $(".rules_error").hide();
        $(".all_rules").addClass("admin-hide").removeClass('admin-show');
        var type = $(this).data('type');
        $("#"+type+"_new").addClass("admin-show").removeClass("admin-hide");
        if(jQuery.inArray(type, ["rotate_processor_with_amount", "rotate_processor_on_cc_brand"]) != -1){
            initialize_descriptor_dropdown(type, '', "new");
        }
        if(jQuery.inArray(type, ["rotate_processor_with_amount", "decline_on_cc_country", "rotate_processor_on_cc_brand", "processor_decline_percent"]) != -1){
            initialize_operator_dropdown(type, '', "new");
        }
        $(".payment_gateways_section_b").removeClass("admin-hide").addClass('admin-show');
        $(".load_balancer_rules_section_b").removeClass("admin-show").addClass("admin-hide");
        $("#remove-descriptor").hide();
        $("#backStep").hide();
    });

    // Rule form submit button
    $(document).on('click','.add_rule_form',function () {
        var type = $(this).data('type');
        var update = $(this).data('update');
        if (update != "true") {
            var rule_success = false;
            switch (type) {
                case "rotate_processor":
                    if (parseInt($("#rotate_processor_count").val()) > 0) {
                        rule_success = true;
                        var count = $(".rotate_processor").children().length;
                        $(".rotate_processor").append('<div class="col-md-12 rotate_processor_hidden rotate_processor_hidden_' + count + '"">' +
                            '<input type="hidden" name="rotate_processor[' + count + '][count]" value="' + $("#rotate_processor_count").val() + '">' +
                            '<input type="hidden" name="rotate_processor[' + count + '][identifier]" value="rotate_processor">' +
                            '<input type="hidden" id="rotate_processor_' + count + '_status" name="rotate_processor[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="rotate_processor[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var message = '<div class="rotate-every">When transaction come in rotate every<span class="rotate-blue"> ' + parseInt($("#rotate_processor_count").val()) + ' </span>transaction/s between all processor.</div>'
                        create_rule_on_right_side(message, type, count);
                        $("#rotate_processor_count").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "rotate_processor_with_amount":
                    if ($("#rotate_processor_with_amount_operator").val() != "" && parseInt($("#rotate_processor_with_amount_amount").val()) > 0 && $("#rotate_processor_with_amount_descriptor").val() != "" && $("#rotate_processor_with_amount_descriptor").val() != null && parseInt($("#rotate_processor_with_amount_count").val()) > 0) {
                        rule_success = true;
                        var count = $(".rotate_processor_with_amount").children().length;
                        if(count > 0){
                            count = $(".rotate_processor_with_amount_hidden").last().data('id');
                            count = parseInt(count) + 1;
                        }
                        $(".rotate_processor_with_amount").append('<div class="col-md-12 rotate_processor_with_amount_hidden rotate_processor_with_amount_hidden_' + count + '" data-id="'+ count +'">' +
                            '<input type="hidden" name="rotate_processor_with_amount[' + count + '][operator]" value="' + $("#rotate_processor_with_amount_operator").val() + '">' +
                            '<input type="hidden" name="rotate_processor_with_amount[' + count + '][amount]" value="' + $("#rotate_processor_with_amount_amount").val() + '">' +
                            '<input type="hidden" name="rotate_processor_with_amount[' + count + '][descriptor]" value="' + $("#rotate_processor_with_amount_descriptor").val() + '">' +
                            '<input type="hidden" name="rotate_processor_with_amount[' + count + '][count]" value="' + $("#rotate_processor_with_amount_count").val() + '">' +
                            '<input type="hidden" name="rotate_processor_with_amount[' + count + '][identifier]" value="rotate_processor_with_amount">' +
                            '<input type="hidden" id="rotate_processor_with_amount_' + count + '_status" name="rotate_processor_with_amount[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="rotate_processor_with_amount[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var descriptor = "";
                        $(".select_amount_decriptor option:selected").each(function () {
                            if ($(this).val().length != "") {
                                descriptor = descriptor + $(this).text() + ", ";
                            }
                        });
                        var message = '<div class="rotate-every">\n' +
                            '                        If transaction amount <span class="rotate-blue">' + $("#rotate_processor_with_amount_operator").val() + ' $ ' + number_with_precision($("#rotate_processor_with_amount_amount").val()) + ' </span>send transaction\n' +
                            '                        to<span class="rotate-blue"> ' + descriptor.slice(0, -2) + ' </span>and rotate every <span class="rotate-blue"> ' + $("#rotate_processor_with_amount_count").val() + ' </span> transaction/s between processor/s.\n' +
                            '                      </div>';
                        create_rule_on_right_side(message, type, count, $("#rotate_processor_with_amount_descriptor").val());
                        check_available_gateways($("#rotate_processor_with_amount_descriptor").val(),"new");
                        check_available_operators("rotate_processor_with_amount", "", $("#rotate_processor_with_amount_operator").val());
                        $("#rotate_processor_with_amount_operator").val('');
                        $("#rotate_processor_with_amount_amount").val('');
                        $("#rotate_processor_with_amount_count").val('');
                        $("#rotate_processor_with_amount_descriptor").val('');
                        $("#rotate_processor_with_amount_descriptor").select2().trigger('change');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "rotate_processor_with_cc":
                    if (parseInt($("#rotate_processor_with_cc_number").val()) > 0) {
                        rule_success = true;
                        var count = $(".rotate_processor_with_cc").children().length;
                        $(".rotate_processor_with_cc").append('<div class="col-md-12 rotate_processor_with_cc_hidden_' + count + '">' +
                            '<input type="hidden" name="rotate_processor_with_cc[' + count + '][count]" value="' + $("#rotate_processor_with_cc_number").val() + '">' +
                            '<input type="hidden" name="rotate_processor_with_cc[' + count + '][identifier]" value="rotate_processor_with_cc">' +
                            '<input type="hidden" id="rotate_processor_with_cc_' + count + '_status" name="rotate_processor_with_cc[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="rotate_processor_with_cc[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a C.C # been process<span class="rotate-blue"> ' + parseInt($("#rotate_processor_with_cc_number").val()) + ' </span>time/s on the same processor, rotate to the next process.</div>';
                        create_rule_on_right_side(message, type, count);
                        $("#rotate_processor_with_cc_number").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "block_card_for_decline_count":
                    if (parseInt($("#block_card_for_decline_count_number").val()) > 0 && parseInt($("#block_card_for_decline_count_no_of_hours").val()) > 0) {
                        rule_success = true;
                        var count = $(".block_card_for_decline_count").children().length;
                        $(".block_card_for_decline_count").append('<div class="col-md-12 block_card_for_decline_count_hidden_' + count + '">' +
                            '<input type="hidden" name="block_card_for_decline_count[' + count + '][no_of_hours]" value="' + $("#block_card_for_decline_count_no_of_hours").val() + '">' +
                            '<input type="hidden" name="block_card_for_decline_count[' + count + '][count]" value="' + $("#block_card_for_decline_count_number").val() + '">' +
                            '<input type="hidden" name="block_card_for_decline_count[' + count + '][identifier]" value="block_card_for_decline_count">' +
                            '<input type="hidden" id="block_card_for_decline_count_' + count + '_status" name="block_card_for_decline_count[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="block_card_for_decline_count[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a C.C # been Decline<span class="rotate-blue"> ' + parseInt($("#block_card_for_decline_count_number").val()) + ' </span>time/s on the same Processor group block the C.C for <span class="rotate-blue"> ' + parseInt($("#block_card_for_decline_count_no_of_hours").val()) + ' </span> hour/s</div>';
                        create_rule_on_right_side(message, type, count);
                        $("#block_card_for_decline_count_number").val('');
                        $("#block_card_for_decline_count_no_of_hours").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "processor_daily_volume":
                    if (parseFloat($("#processor_daily_volume_percentage").val()) > 0) {
                        rule_success = true;
                        var count = $(".processor_daily_volume").children().length;
                        $(".processor_daily_volume").append('<div class="col-md-12 processor_daily_volume_hidden_' + count + '">' +
                            '<input type="hidden" name="processor_daily_volume[' + count + '][percentage]" value="' + $("#processor_daily_volume_percentage").val() + '">' +
                            '<input type="hidden" name="processor_daily_volume[' + count + '][identifier]" value="processor_daily_volume">' +
                            '<input type="hidden" id="processor_daily_volume_' + count + '_status" name="processor_daily_volume[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="processor_daily_volume[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a processor get to<span class="rotate-blue"> ' + parseInt($("#processor_daily_volume_percentage").val()) + ' </span>percentage of total Daily volume send Slack alert. </div>';
                        create_rule_on_right_side(message, type, count);
                        $("#processor_daily_volume_percentage").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "processor_monthly_volume":
                    if (parseFloat($("#processor_monthly_volume_percentage").val()) > 0) {
                        rule_success = true;
                        var count = $(".processor_monthly_volume").children().length;
                        $(".processor_monthly_volume").append('<div class="col-md-12 processor_monthly_volume_hidden_' + count + '">' +
                            '<input type="hidden" name="processor_monthly_volume[' + count + '][percentage]" value="' + $("#processor_monthly_volume_percentage").val() + '">' +
                            '<input type="hidden" name="processor_monthly_volume[' + count + '][identifier]" value="processor_monthly_volume">' +
                            '<input type="hidden" id="processor_monthly_volume_' + count + '_status" name="processor_monthly_volume[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="processor_monthly_volume[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a processor get to<span class="rotate-blue"> ' + parseInt($("#processor_monthly_volume_percentage").val()) + ' </span>percentage of total Monthly volume send Slack alert. </div>';
                        create_rule_on_right_side(message, type, count);
                        $("#processor_monthly_volume_percentage").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "group_daily_volume":
                    if (parseFloat($("#group_daily_volume_percentage").val()) > 0) {
                        rule_success = true;
                        var count = $(".group_daily_volume").children().length;
                        $(".group_daily_volume").append('<div class="col-md-12 group_daily_volume_hidden_' + count + '">' +
                            '<input type="hidden" name="group_daily_volume[' + count + '][percentage]" value="' + $("#group_daily_volume_percentage").val() + '">' +
                            '<input type="hidden" name="group_daily_volume[' + count + '][identifier]" value="group_daily_volume">' +
                            '<input type="hidden" id="group_daily_volume_' + count + '_status" name="group_daily_volume[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="group_daily_volume[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a group get to<span class="rotate-blue"> ' + parseInt($("#group_daily_volume_percentage").val()) + ' </span>percentage of total Daily volume send Slack alert. </div>';
                        create_rule_on_right_side(message, type, count);
                        $("#group_daily_volume_percentage").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "group_monthly_volume":
                    if (parseFloat($("#group_monthly_volume_percentage").val()) > 0) {
                        rule_success = true;
                        var count = $(".group_monthly_volume").children().length;
                        $(".group_monthly_volume").append('<div class="col-md-12 group_monthly_volume_hidden_' + count + '">' +
                            '<input type="hidden" name="group_monthly_volume[' + count + '][percentage]" value="' + $("#group_monthly_volume_percentage").val() + '">' +
                            '<input type="hidden" name="group_monthly_volume[' + count + '][identifier]" value="group_monthly_volume">' +
                            '<input type="hidden" id="group_monthly_volume_' + count + '_status" name="group_monthly_volume[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="group_monthly_volume[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a group get to<span class="rotate-blue"> ' + parseInt($("#group_monthly_volume_percentage").val()) + ' </span>percentage of total Monthly volume send Slack alert. </div>';
                        create_rule_on_right_side(message, type, count);
                        $("#group_monthly_volume_percentage").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "block_processor_daily_volume":
                    if (parseFloat($("#block_processor_daily_volume_percentage").val()) > 0) {
                        rule_success = true;
                        var count = $(".block_processor_daily_volume").children().length;
                        $(".block_processor_daily_volume").append('<div class="col-md-12 block_processor_daily_volume_hidden_' + count + '">' +
                            '<input type="hidden" name="block_processor_daily_volume[' + count + '][percentage]" value="' + $("#block_processor_daily_volume_percentage").val() + '">' +
                            '<input type="hidden" name="block_processor_daily_volume[' + count + '][identifier]" value="block_processor_daily_volume">' +
                            '<input type="hidden" id="block_processor_daily_volume' + count + '_status" name="block_processor_daily_volume[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="block_processor_daily_volume[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var message = '<div class="rotate-every">When a processor get to<span class="rotate-blue"> ' + parseInt($("#block_processor_daily_volume_percentage").val()) + ' </span>percentage of total Daily volume, block processor until the end of the day (PST) from receiving transaction. </div>';
                        create_rule_on_right_side(message, type, count);
                        $("#block_processor_daily_volume_percentage").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "block_processor_cbk_percent":
                    break;
                case "processor_decline_percent":
                    if (parseFloat($("#processor_decline_percent_percentage").val()) > 0 && $("#processor_decline_percent_operator").val() != "" && parseInt($("#processor_decline_percent_no_of_hours").val()) > 0) {
                        rule_success = true;
                        var count = $(".processor_decline_percent").children().length;
                        $(".processor_decline_percent").append('<div class="col-md-12 processor_decline_percent_hidden_' + count + '">' +
                            '<input type="hidden" name="processor_decline_percent[' + count + '][percentage]" value="' + $("#processor_decline_percent_percentage").val() + '">' +
                            '<input type="hidden" name="processor_decline_percent[' + count + '][operator]" value="' + $("#processor_decline_percent_operator").val() + '">' +
                            '<input type="hidden" name="processor_decline_percent[' + count + '][no_of_hours]" value="' + $("#processor_decline_percent_no_of_hours").val() + '">' +
                            '<input type="hidden" name="processor_decline_percent[' + count + '][identifier]" value="processor_decline_percent">' +
                            '<input type="hidden" id="processor_decline_percent_' + count + '_status" name="processor_decline_percent[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="processor_decline_percent[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a processor decline percentage is <span class="rotate-blue"> ' + $("#processor_decline_percent_operator").val() + ' </span> <span class="rotate-blue"> ' + parseFloat($("#processor_decline_percent_percentage").val()) + ' </span> % in <span class="rotate-blue"> ' + $("#processor_decline_percent_no_of_hours").val() + ' </span> hour/s send alert to Slack. </div>';
                        create_rule_on_right_side(message, type, count);
                        check_available_operators(type, "", $("#"+ type +"_operator").val());
                        $("#processor_decline_percent_percentage").val('');
                        $("#processor_decline_percent_no_of_hours").val('');
                        $("#processor_decline_percent_operator").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "processor_decline_times":
                    if (parseFloat($("#processor_decline_times_count").val()) > 0) {
                        rule_success = true;
                        var count = $(".processor_decline_times").children().length;
                        $(".processor_decline_times").append('<div class="col-md-12 processor_decline_times_hidden_' + count + '">' +
                            '<input type="hidden" name="processor_decline_times[' + count + '][count]" value="' + $("#processor_decline_times_count").val() + '">' +
                            '<input type="hidden" name="processor_decline_times[' + count + '][identifier]" value="processor_decline_times">' +
                            '<input type="hidden" id="processor_decline_times_' + count + '_status" name="processor_decline_times[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="processor_decline_times[' + count + '][rule_type]" value="notification">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a processor decline<span class="rotate-blue"> ' + parseInt($("#processor_decline_times_count").val()) + ' </span>time/s  in a row, send slack alert with decline messages. </div>';
                        create_rule_on_right_side(message, type, count);
                        $("#processor_decline_times_count").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "move_group_on_cc_country":
                    break;
                case "decline_on_cc_country":
                    if ($("#decline_on_cc_country_operator").val() != "" && $("#decline_on_cc_country_country").val() != "") {
                        rule_success = true;
                        var count = $(".decline_on_cc_country").children().length;
                        if(count > 0){
                            count = $(".rotate_processor_with_amount_hidden").last().data('id');
                            count = parseInt(count) + 1;
                        }
                        $(".decline_on_cc_country").append('<div class="col-md-12 decline_on_cc_country_hidden decline_on_cc_country_hidden_' + count + '" data-id="'+ count +'">' +
                            '<input type="hidden" name="decline_on_cc_country[' + count + '][operator]" value="' + $("#decline_on_cc_country_operator").val() + '">' +
                            '<input type="hidden" name="decline_on_cc_country[' + count + '][country]" value="' + $("#decline_on_cc_country_country").val() + '">' +
                            '<input type="hidden" name="decline_on_cc_country[' + count + '][identifier]" value="decline_on_cc_country">' +
                            '<input type="hidden" id="decline_on_cc_country_' + count + '_status" name="decline_on_cc_country[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="decline_on_cc_country[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var countries = "";
                        $(".select_decline_on_cc_country option:selected").each(function () {
                            if ($(this).val().length != "") {
                                countries = countries + $(this).text() + ", ";
                            }
                        });
                        var message = '<div class="rotate-every">If Credit card country <span class="rotate-blue">' + $("#decline_on_cc_country_operator").val()+ '</span>,<span class="rotate-blue"> ' + countries.slice(0, -2) + ' </span> Decline the transaction.</div>';
                        create_rule_on_right_side(message, type, count);
                        check_available_operators(type, "", $("#"+ type +"_operator").val());
                        $("#decline_on_cc_country_country").val('');
                        $("#decline_on_cc_country_operator").val('');
                        $("#decline_on_cc_country_country").multipleSelect({placeholder: "Select Countries", selectAll: false});
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "move_group_on_cc_brand":
                case "rotate_processor_on_cc_brand":
                    if ($("#rotate_processor_on_cc_brand_operator").val() != "" && $("#rotate_processor_on_cc_brand_card_brand").val() != "" && $("#rotate_processor_on_cc_brand_descriptor").val() != "" && $("#rotate_processor_on_cc_brand_descriptor").val() != null && parseInt($("#rotate_processor_on_cc_brand_count").val()) > 0) {
                        rule_success = true;
                        var count = $(".rotate_processor_on_cc_brand").children().length;
                        $(".rotate_processor_on_cc_brand").append('<div class="col-md-12 rotate_processor_on_cc_brand_hidden rotate_processor_on_cc_brand_hidden_' + count + '" data-id="'+ count +'">' +
                            '<input type="hidden" name="rotate_processor_on_cc_brand[' + count + '][operator]" value="' + $("#rotate_processor_on_cc_brand_operator").val() + '">' +
                            '<input type="hidden" name="rotate_processor_on_cc_brand[' + count + '][card_brand]" value="' + $("#rotate_processor_on_cc_brand_card_brand").val() + '">' +
                            '<input type="hidden" name="rotate_processor_on_cc_brand[' + count + '][descriptor]" value="' + $("#rotate_processor_on_cc_brand_descriptor").val() + '">' +
                            '<input type="hidden" name="rotate_processor_on_cc_brand[' + count + '][count]" value="' + $("#rotate_processor_on_cc_brand_count").val() + '">' +
                            '<input type="hidden" name="rotate_processor_on_cc_brand[' + count + '][identifier]" value="rotate_processor_on_cc_brand">' +
                            '<input type="hidden" id="rotate_processor_on_cc_brand_' + count + '_status" name="rotate_processor_on_cc_brand[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="rotate_processor_on_cc_brand[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var cc_descriptor = "";
                        $(".select_amount_cc_descriptor option:selected").each(function () {
                            if ($(this).val().length != "") {
                                cc_descriptor = cc_descriptor + $(this).text() + ", ";
                            }
                        });
                        var message = '<div class="rotate-every">\n' +
                            '                        If Credit card brand <span class="rotate-blue">' + $("#rotate_processor_on_cc_brand_operator").val() + '  ' + $("#rotate_processor_on_cc_brand_card_brand").val() + ' </span>send transaction\n' +
                            '                        to<span class="rotate-blue"> ' + cc_descriptor.slice(0, -2) + ' </span>and rotate every <span class="rotate-blue"> ' + $("#rotate_processor_on_cc_brand_count").val() + ' </span> transaction/s between processor/s.\n' +
                            '                      </div>';
                        create_rule_on_right_side(message, type, count, $("#rotate_processor_on_cc_brand_descriptor").val());
                        check_available_operators(type, "", $("#"+ type +"_operator").val());
                        $("#rotate_processor_on_cc_brand_operator").val('');
                        $("#rotate_processor_on_cc_brand_card_brand").val('');
                        $("#rotate_processor_on_cc_brand_count").val('');
                        check_available_gateways($("#rotate_processor_on_cc_brand_descriptor").val());
                        $("#rotate_processor_on_cc_brand_descriptor").val('');
                        $("#rotate_processor_on_cc_brand_descriptor").select2().trigger('change');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
                case "block_card_for_reason":
                    if ($("#block_card_for_reason_decline_reason").val() != "" && $("#block_card_for_reason_no_of_hours").val() != "") {
                        rule_success = true;
                        var count = $(".block_card_for_reason").children().length;
                        $(".block_card_for_reason").append('<div class="col-md-12 block_card_for_reason_hidden block_card_for_reason_hidden_' + count + '" data-id="'+ count +'">' +
                            '<input type="hidden" name="block_card_for_reason[' + count + '][no_of_hours]" value="' + $("#block_card_for_reason_no_of_hours").val() + '">' +
                            '<input type="hidden" name="block_card_for_reason[' + count + '][decline_reason]" value="' + $("#block_card_for_reason_decline_reason").val() + '">' +
                            '<input type="hidden" name="block_card_for_reason[' + count + '][identifier]" value="block_card_for_reason">' +
                            '<input type="hidden" id="block_card_for_reason_' + count + '_status" name="block_card_for_reason[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="block_card_for_reason[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var decline_reason = "";
                        $(".select_decline_reason option:selected").each(function () {
                            if ($(this).val().length != "") {
                                decline_reason = decline_reason + $(this).text() + ", ";
                            }
                        });
                        var message = '<div class="rotate-every">If C.C # got decline for <span class="rotate-blue">' + decline_reason.slice(0, -2) + '</span>, block the card for <span class="rotate-blue"> ' +$("#block_card_for_reason_no_of_hours").val()+ ' </span> hour/s or Move the card to blacklist.</div>';
                        create_rule_on_right_side(message, type, count);
                        $("#block_card_for_reason_no_of_hours").val('');
                        $("#block_card_for_reason_decline_reason").val('');
                        $("#block_card_for_reason_decline_reason").multipleSelect({placeholder: "Select Reason", selectAll: false});
                        // enable_disable_add_rule_btn(type);
                    }
                    break;
                case "block_card_for_processed_count":
                    if (parseInt($("#block_card_for_processed_count_count").val()) > 0 && parseInt($("#block_card_for_processed_count_no_of_hours").val()) > 0) {
                        rule_success = true;
                        var count = $(".block_card_for_processed_count").children().length;
                        $(".block_card_for_processed_count").append('<div class="col-md-12 block_card_for_processed_count_hidden_' + count + '">' +
                            '<input type="hidden" name="block_card_for_processed_count[' + count + '][no_of_hours]" value="' + $("#block_card_for_processed_count_no_of_hours").val() + '">' +
                            '<input type="hidden" name="block_card_for_processed_count[' + count + '][count]" value="' + $("#block_card_for_processed_count_count").val() + '">' +
                            '<input type="hidden" name="block_card_for_processed_count[' + count + '][identifier]" value="block_card_for_processed_count">' +
                            '<input type="hidden" id="block_card_for_processed_count' + count + '_status" name="block_card_for_processed_count[' + count + '][status]" value="active">' +
                            '<input type="hidden" name="block_card_for_processed_count[' + count + '][rule_type]" value="automation">' +
                            '</div>');
                        var message = '<div class="rotate-every">If a C.C # been process<span class="rotate-blue"> ' + parseInt($("#block_card_for_processed_count_count").val()) + ' </span>time/s on the same group block him for <span class="rotate-blue"> ' + parseInt($("#block_card_for_processed_count_no_of_hours").val()) + ' </span> hour/s</div>';
                        create_rule_on_right_side(message, type, count);
                        $("#block_card_for_processed_count_count").val('');
                        $("#block_card_for_processed_count_no_of_hours").val('');
                        enable_disable_add_rule_btn(type);
                    }
                    break;
            }
            if (rule_success) {
                if($("#automation_tab").data('status') == "active"){
                    $("#automation_rules").addClass("admin-show").removeClass("admin-hide");
                    $("#notification_rules").removeClass("admin-show").addClass("admin-hide");
                }else{
                    $("#notification_rules").addClass("admin-show").removeClass("admin-hide");
                    $("#automation_rules").removeClass("admin-show").addClass("admin-hide");
                }
                $("#" + type + "_new").addClass("admin-hide").removeClass('admin-show');
                $("#create_group").prop("disabled", false);
                $(".payment_gateways_section_b").addClass("admin-hide").removeClass('admin-show');
                $(".load_balancer_rules_section_b").addClass("admin-show").removeClass("admin-hide");
                $("#remove-descriptor").show();
                $("#backStep").show();
            } else {
                $(".rules_error").show().fadeOut(3000);
            }
        }else{
            update_rule(type, $(this).data('count'))
        }
    });

    $(".close_rule_form").on('click', function () {
        if($("#automation_tab").data('status') == "active"){
            $("#automation_rules").addClass("admin-show").removeClass("admin-hide");
            $("#notification_rules").removeClass("admin-show").addClass("admin-hide");
        }else{
            $("#notification_rules").addClass("admin-show").removeClass("admin-hide");
            $("#automation_rules").removeClass("admin-show").addClass("admin-hide");
        }
        $(".payment_gateways_section_b").addClass("admin-hide").removeClass('admin-show');
        $(".load_balancer_rules_section_b").addClass("admin-show").removeClass("admin-hide");
        $("#backStep").show();
        $("#remove-descriptor").show();
        $("#" + $(this).data('type') + "_new").addClass("admin-hide").removeClass('admin-show');
    });

});

$(document).ready(function () {

    $(document).on('change','#filter',function () {
        $('#verification_gate_filter').submit();
    })

    $('#update_load_balancer').on('click',function () {
        var checkedArr = $("#checked_arr").val();
        var unCheckedArr = $("#un_checked_arr").val();
        var all_list = $("#all_locations_map").val();
        all_list = JSON.parse(all_list);
        all_list = all_list.filter(val => !unCheckedArr.includes(val));
        if($("#all-check").prop("checked")){
            $('#locations_').val(all_list);
        }else{
            checkedArr = selected
            $('#locations_').val(checkedArr);
        }
    });

    var isselect = false;
    var first = true;
    var selected = [];
    var isAllChecked = $("#is_all_check").val();
    var checkboxes = $(".all");
    var checkedArr = [];
    var page = $('#page_param_input').val()
    checkedArr = $("#checked_arr").val();
    var unCheckedArr = [];
    unCheckedArr = $("#un_checked_arr").val();
    if(unCheckedArr=="undefined" || checkedArr=="undefined"){
    checkedArr = checkedArr.split(",");
    unCheckedArr = unCheckedArr.split(",");
    if(isAllChecked == "true"){
        checkboxes.prop("checked", true);
        $('#all-check').prop("checked", true)
    }else{
        checkboxes.prop("checked",false);
        // $("#bank_div").css('display', 'none');
    }
    if(page > 1){
        $("#check_all").hide();
    }
    $(".all").on("click", function () {

        var isChecked = $(this).prop("checked");
        var targetValue = $(this).val();
        if(isChecked){
            checkedArr.push(targetValue);
            unCheckedArr = unCheckedArr.filter(val => !targetValue.includes(val));
        }
        else{
            unCheckedArr.push(targetValue);
            checkedArr = checkedArr.filter(val => !targetValue.includes(val));
        }
        if(checkedArr[0] === ""){
            checkedArr.shift();
        }
        if(unCheckedArr[0] === ""){
            unCheckedArr.shift();
        }
        $("#checked_arr").val(checkedArr);
        $("#un_checked_arr").val(unCheckedArr);
    })

    for(var i = 0; i < unCheckedArr.length; i++){
        $("#checkbox_animated_"+unCheckedArr[i]).prop("checked", false)
    }
    checkedArr = checkedArr.filter(val => !unCheckedArr.includes(val));
    for(var i = 0; i < checkedArr.length; i++){
        $("#checkbox_animated_"+checkedArr[i]).prop("checked", true)
    }}

    // check all check boxes
    $(document).on("click", '#check_all', function() {
        // grouping all the checkbox using the classname
        var checkboxes = $(".all");
        // check whether checkboxes are selected.
        if(checkboxes.prop("checked")){
            $("#is_all_check").val("false");
            isselect = false;
            // hide hidden div
            $("#load_balancer_div").hide();
            // if they are selected, unchecking all the checkbox
            checkboxes.prop("checked",false);
            selected = [];
        } else {
            isselect = true;
            $("#is_all_check").val("true");
            // show hidden div
            $("#load_balancer_div").show();
            // if they are not selected, checking all the checkbox
            checkboxes.prop("checked", true);
            selected = $("#all_locations_map").val();
            selected = JSON.parse(selected)
            first = false;
            console.log("success");
        }
    });
    // single checkbox value
    $(document).on('change', 'input[type="checkbox"]', function(){
        if ($(this).is(':checked')){
            // show hidden div
            selected = selected.filter(val => !$(this).val().includes(val));
            selected.push($(this).val());
            selected = selected.filter(val => !"on".includes(val));
            if(selected.length > 1) {
                $("#load_balancer_div").show();
            }
        } else {
            selected.pop($(this).val());
            if(selected.length <= 1){
                $("#load_balancer_div").hide();
            }
        }
    });

    // Load balancer Name
    $("#type_group_name").on('keyup change', function () {
        $(".group_name").text($("#type_group_name").val());
        $("#load_balancer_name").val($("#type_group_name").val());
        next_button_active();
    });

    $(document).on('change','.gateway_checkboxes',function () {
        if($('.gateway_checkboxes:checked').length > 0){
            $("#add_descriptor").prop("disabled", false);
        }else{
            $("#add_descriptor").prop("disabled", true);
        }
    });

    $(document).on('change','.remove_gateway_checkboxes',function () {
        if($('.remove_gateway_checkboxes:checked').length > 0){
            $("#remove-descriptor").prop("disabled", false);
        }else{
            $("#remove-descriptor").prop("disabled", true);
        }
    });

    $(document).on('click','.m-accordion__item-head',function () {
        var sign = $(this).find("div > .plus-sign").text();
        if(sign == "+"){
            $(this).find("div > .plus-sign").text('-');
        }else{
            $(this).find("div > .plus-sign").text('+');
        }
    });

    $(document).on('click change',"#add_descriptor", function () {
        var descriptor_group = {};
        $(".gateway_checkboxes").each(function(value) {
            if($(this).prop("checked") == true) {
                // if (jQuery.inArray($(this).data('gateway_id'), checked_ids) < 0) {
                    var gateway_obj = {
                        "id": $(this).data('gateway_id'),
                        "gateway_descriptor": $(this).data('gateway_descriptor'),
                        "gateway_daily": $(this).data('gateway_daily'),
                        "gateway_monthly": $(this).data('gateway_monthly'),
                        "gateway_ticket": $(this).data('gateway_ticket'),
                        "rule_id": $(this).data('rule_id')
                    };
                    if ($(this).data('gateway_group') in descriptor_group) {
                        descriptor_group[$(this).data('gateway_group')].push(gateway_obj)
                    } else {
                        descriptor_group[$(this).data('gateway_group')] = [gateway_obj]
                    }
                // }
            }
        });
        create_or_update_descriptor_table(descriptor_group, "add");
        $("#add_descriptor").prop("disabled", true);
        next_button_active();
    });

    $(document).on('click',"#remove-descriptor", function () {
        var remove_descriptor_group = {};
        var remove_checked_ids = [];
        var alert_message = false;
        $(".remove_gateway_checkboxes").each(function(value) {
            if($(this).prop("checked") == true) {
                if (parseInt($(this).data('rule_id')) == 0 && ($(this).data('rule_status') == "inactive" || $(this).data('rule_status') == null || $(this).data('rule_status') == "") ){
                    remove_checked_ids.push(""+$(this).data('gateway_id')+"");
                    var gateway_obj = {
                        "id": $(this).data('gateway_id'),
                        "gateway_descriptor": $(this).data('gateway_descriptor'),
                        "gateway_daily": $(this).data('gateway_daily'),
                        "gateway_monthly": $(this).data('gateway_monthly'),
                        "gateway_ticket": $(this).data('gateway_ticket'),
                        "rule_id": $(this).data('rule_id')
                    };
                    if ($(this).data('gateway_group') in remove_descriptor_group) {
                        remove_descriptor_group[$(this).data('gateway_group')].push(gateway_obj)
                    } else {
                        remove_descriptor_group[$(this).data('gateway_group')] = [gateway_obj]
                    }
                }else{
                    alert_message = true;
                }
            }
        });
        create_or_update_descriptor_table(remove_descriptor_group, "remove");
        // remove_descriptor_from_right_side(remove_descriptor_group);
        $("#remove-descriptor").prop("disabled", true);
        next_button_active();
        if(alert_message){
            alert("Can't Remove Rule's Descriptor!");
        }
    });
    setTimeout(function () {
        next_button_active();
    }, 500);
});

function create_or_update_descriptor_table(descriptor_group, action) {
    if(action == "add"){
        var main_div_id = "remove_payment_group";
        var tbody_id = "tbody";
        var tr_id = "tr_gateway_added";
        var remove_main_div_id = "payment_group";
        var remove_tbody_id = "new_tbody";
        var remove_tr_id = "tr_gateway";
        var checkbox_class = "remove_gateway_checkboxes";
        var append_div = "m_accordion_2";
        var table_class = "selected_gateways_tables";
    }else{
        var table_class = "all_gateways_tables";
        var tbody_id = "new_tbody";
        var main_div_id = "payment_group";
        var tr_id = "tr_gateway";
        var remove_main_div_id = "remove_payment_group";
        var remove_tbody_id = "tbody";
        var remove_tr_id = "tr_gateway_added";
        var checkbox_class = "gateway_checkboxes";
        var append_div = "m_accordion_1";
    }
    $.each( descriptor_group, function( key, value ) {
        // no group exits.. create group and its descriptors
        if($("#"+ main_div_id +"_"+ key.replace(/\s/g, '')).length == 0){
            $("#"+append_div).append('<div class="m-accordion__item" id="'+ main_div_id +'_'+ key.replace(/\s/g, '') +'">\n' +
                '                        <div class="m-accordion__item-head collapsed active for-box" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_'+ action +''+ key.replace(/\s/g, '') +'_item_2_body" aria-expanded="false">\n' +
                '                           <span class="m-accordion__item-icon">\n' +
                '                             <i class="fa flaticon-user-ok"></i>\n' +
                '                           </span>\n' +
                '                          <div class="plus-box">\n' +
                '                            <div class="plus-sign">+</div>\n' +
                '                          </div>\n' +
                '                          <span class="Elavon">'+ key +'</span>\n' +
                '                          <span class="m-accordion__item-mode"></span>\n' +
                '                        </div>\n' +
                '                        <div class="m-accordion__item-body collapse border-box" id="m_accordion_'+ action +''+ key.replace(/\s/g, '') +'_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">\n' +
                '                          <div class="m-accordion__item-content box-border">\n' +
                '                            <table class="'+ table_class +' table-striped table-bordered" cellspacing="0" cellpadding="0">\n' +
                '                              <thead class="tr-head">\n' +
                '                              <tr class="tr-head odd-tr">\n' +
                '                                <th class="chk-box"></th>\n' +
                '                                <th class="des-heading">Descriptors</th>\n' +
                '                                <th class="des-heading">High Ticket</th>\n' +
                '                                <th class="des-heading">Daily Limit</th>\n' +
                '                                <th class="des-heading">Monthly Limit</th>\n' +
                '                              </tr>\n' +
                '                              </thead>\n' +
                '                              <tbody id="'+ tbody_id +'_'+ key.replace(/\s/g, '') +'">\n' +
                '                              </tbody>\n' +
                '                            </table>\n' +
                '                          </div>\n' +
                '                        </div>\n' +
                '                      </div>');
            $.each( value, function( index, val ) {

                $("#"+ tbody_id +"_"+ key.replace(/\s/g, '')).append(
                    '                                <tr class="cursor_point even-tr" id="'+ tr_id +'_'+ val.id +'">\n' +
                    '                                  <td class="sorted">\n' +
                    '                                    <input type="checkbox" class="'+ checkbox_class +'" id="gateway_checkboxes_'+ val.id +'" data-gateway_id="'+ val.id +'" data-gateway_daily="'+ val.gateway_daily +'" data-gateway_monthly="'+ val.gateway_monthly +'" data-gateway_descriptor="'+ val.gateway_descriptor +'" data-gateway_ticket="'+ val.gateway_ticket +'" data-gateway_group="'+ key +'"  data-rule_id="0" data-rule_status="inactive">\n' +
                    '                                  </td>\n' +
                    '                                  <td class="esq-hboutique">\n'+ val.gateway_descriptor + '</td>\n' +
                    '                                  <td class="amount-doller">$\n' + number_with_precision(val.gateway_ticket) + '</td>\n' +
                    '                                  <td class="amount-doller">$\n' + number_with_precision(val.gateway_daily) + '</td>\n' +
                    '                                  <td class="amount-doller">$\n' + number_with_precision(val.gateway_monthly) + '</td>\n' +
                    '                                </tr>')
            })
        }
        // group exits.. create its descriptors
        else{
            $.each( value, function( index, val ) {
                $("#"+ tbody_id +"_"+ key.replace(/\s/g, '')).append(
                    '                                <tr class="cursor_point even-tr" id="'+ tr_id +'_'+ val.id +'">\n' +
                    '                                  <td class="sorted">\n' +
                    '                                    <input type="checkbox" class="'+ checkbox_class +'" id="gateway_checkboxes_'+ val.id +'" data-gateway_id="'+ val.id +'" data-gateway_daily="'+ val.gateway_daily +'" data-gateway_monthly="'+ val.gateway_monthly +'" data-gateway_descriptor="'+ val.gateway_descriptor +'" data-gateway_group="'+ key +'"  data-rule_id="0" data-rule_status="inactive">\n' +
                    '                                  </td>\n' +
                    '                                  <td class="esq-hboutique">\n'+ val.gateway_descriptor + '</td>\n' +
                    '                                  <td class="amount-doller">$\n' + number_with_precision(val.gateway_ticket) + '</td>\n' +
                    '                                  <td class="amount-doller">$\n' + number_with_precision(val.gateway_daily) + '</td>\n' +
                    '                                  <td class="amount-doller">$\n' + number_with_precision(val.gateway_monthly) + '</td>\n' +
                    '                                </tr>')
            })
        }
        // remove descriptors & group
        $.each( value, function( index, val ) {
            $("#"+ remove_tbody_id +"_"+ key.replace(/\s/g, '') +" > tr#"+ remove_tr_id +"_"+ val.id).remove();
        });
        if($("#"+ remove_tbody_id +"_"+ key.replace(/\s/g, '') +" > tr").length == 0){
            $("#"+ remove_main_div_id +"_"+key.replace(/\s/g, '')).remove();
        }
    });
    calculate_daily_monthly_limts();
}
function calculate_daily_monthly_limts(){
    var checked_ids = [];
    var daily = 0;
    var monthly = 0;
    $(".remove_gateway_checkboxes").each(function(value) {
        daily = daily + parseFloat($(this).data('gateway_daily'));
        monthly = monthly + parseFloat($(this).data('gateway_monthly'));
        checked_ids.push($(this).data('gateway_id'));
    });
    $("#checked_gateway_ids").val(checked_ids);
    $(".group_daily").text("$"+number_with_precision(daily));
    $(".group_monthly").text("$"+number_with_precision(monthly));
    $(".group_descriptor").text(checked_ids.length);
}

function next_button_active() {
    if($('.remove_gateway_checkboxes').length > 0 && $("#type_group_name").val().length > 0){
        $(".Next-Step").prop("disabled",false);
    }else{
        $(".Next-Step").prop("disabled",true);
    }
}

function create_rule_on_right_side(message, type, count, gateway_ids) {
    var current_user_name = $("#current_user_name").val();
    $("#m_accordion_3").append('<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 '+ type +'_rule_box_'+ count +'">\n' +
        '                    <div class="row">\n' +
        '                      <!-- <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-12">\n' +
        '                         <center>\n' +
        '                           <button type="button" class="edit-btn-b Edit-btn btn-bellow">\n' +
        '                             Edit\n' +
        '                           </button>\n' +
        '                           <p class="hash-num">#1</p>\n' +
        '                         </center>\n' +
        '                       </div>-->\n' +
        '                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">\n' +
        '                        <div class="hash-body">\n' +
        '                          <div class="row">\n' +
        '                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">\n' +
        '                              <div class="row padding015px">\n' +
        '                                <div class="pull-left">\n' +
        '                                   <img alt="rss feed" class="" src="https://quickard.s3-us-west-2.amazonaws.com/sent.svg"> \n'+
        '                                </div>\n' +
        '                                <div class="pull-right">\n' +
        '                                  <div class="dis-flex">\n' +
        '                                    <p class="Created-by">Created by '+ current_user_name +'</p>\n' +
        '                                    <div class="pull-right">\n' +
        '                                      <div class="form-group margin-bottom-0-payment-gateway">\n' +
        '                                        <label class="switch">\n' +
        '                                          <input type="checkbox" data-type="'+ type +'" data-id="'+ count +'" data-gateway_ids="'+ gateway_ids +'" name="" class="active_inactive_rule display-block-margin-left-20-px-p-g" checked>\n' +
        '                                          <div class="slider round"><span class="on">Active</span><span class="off">Inactive</span></div>\n' +
        '                                        </label>\n' +
        '                                      </div>\n' +
        '                                    </div>\n' +
        '                                    <div class="dropdown three-dot-align" id="dropdownid">\n' +
        '                                      <a href="#" class="m-index-st-17" type="button" data-toggle="dropdown">●●●\n' +
        '                                      </a>\n' +
        '                                      <ul class="dropdown-menu m-index-st-18 table-responsive">\n' +
        '                                        <li class="text-center cursor_pointer edit_rule" data-type="'+ type +'"  data-id="'+ count +'" data-gateway_ids="'+ gateway_ids +'"><a class="btn btn-sm btn-info">Edit</a></li>\n' +
        '                                        <li class="text-center cursor_pointer delete_rule" data-type="'+ type +'"  data-id="'+ count +'" data-gateway_ids="'+ gateway_ids +'"><a class="btn btn-sm btn-warning">Delete</a></li>\n' +
        '                                      </ul>\n' +
        '                                    </div>\n' +
        '                                  </div>\n' +
        '                                </div>\n' +
        '                              </div>\n' +
        '                            </div>\n' +
        '                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" id="rule_box_'+ type +'_'+ count +'">\n' + message +
        '                            </div>\n' +
        '                          </div>\n' +
        '                        </div>\n' +
        '                      </div>\n' +
        '                    </div>\n' +
        '                  </div>');
    if (gateway_ids != "" && gateway_ids != null){
        able_to_remove_gateway_from_group(gateway_ids, "new")
    }
}

function update_rule(type, count) {
    var rule_success = false;
    switch (type) {
        case "rotate_processor":
            if (parseInt($("#rotate_processor_count").val()) > 0) {
                rule_success = true;
                $("input[name='rotate_processor[" + count + "][count]']").val($('#rotate_processor_count').val());
                var message = '<div class="rotate-every">When transaction come in rotate every<span class="rotate-blue"> ' + parseInt($("#rotate_processor_count").val()) + ' </span>transaction/s between all processor.</div>'
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#rotate_processor_count").val('');
            }
            break;
        case "rotate_processor_with_amount":
            if ($("#rotate_processor_with_amount_operator").val() != "" && parseInt($("#rotate_processor_with_amount_amount").val()) > 0 && $("#rotate_processor_with_amount_descriptor").val() != "" && $("#rotate_processor_with_amount_descriptor").val() != null && parseInt($("#rotate_processor_with_amount_count").val()) > 0) {
                rule_success = true;
                check_available_operators(type, "edit", $("#rotate_processor_with_amount_operator").val(),$("input[name='rotate_processor_with_amount[" + count + "][operator]']").val());
                $("input[name='rotate_processor_with_amount[" + count + "][count]']").val($('#rotate_processor_with_amount_count').val());
                $("input[name='rotate_processor_with_amount[" + count + "][amount]']").val($('#rotate_processor_with_amount_amount').val());
                $("input[name='rotate_processor_with_amount[" + count + "][operator]']").val($('#rotate_processor_with_amount_operator').val());
                var descriptor = "";
                var descriptor_select = "";
                $(".select_amount_decriptor option:selected").each(function () {
                    if ($(this).val().length != "") {
                        descriptor = descriptor + $(this).text() + ", ";
                        descriptor_select = descriptor_select + $(this).val() + ",";
                    }
                });
                var old_descriptors = $("input[name='rotate_processor_with_amount[" + count + "][descriptor]']").val();
                $("input[name='rotate_processor_with_amount[" + count + "][descriptor]']").val(descriptor_select.slice(0, -1));
                able_to_remove_gateway_from_group($("input[name='rotate_processor_with_amount[" + count + "][descriptor]']").val(), "update", old_descriptors);
                var message = '<div class="rotate-every">If transaction amount<span class="rotate-blue"> ' + $("#rotate_processor_with_amount_operator").val() + ' $ ' + number_with_precision($("#rotate_processor_with_amount_amount").val()) + ' </span>send transaction to<span class="rotate-blue"> ' + descriptor.slice(0, -2) + ' </span>and rotate every <span class="rotate-blue"> ' + $("#rotate_processor_with_amount_count").val() + ' </span> transaction/s between processor/s.</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                check_available_gateways($("#rotate_processor_with_amount_descriptor").val(),"edit");
                $("#rotate_processor_with_amount_operator").val('');
                $("#rotate_processor_with_amount_amount").val('');
                $("#rotate_processor_with_amount_count").val('');
                $("#rotate_processor_with_amount_descriptor").val('');
                $("#rotate_processor_with_amount_descriptor").select2().trigger('change');
            }
            break;
        case "rotate_processor_with_cc":
            if (parseInt($("#rotate_processor_with_cc_number").val()) > 0) {
                rule_success = true;
                $("input[name='rotate_processor_with_cc[" + count + "][count]']").val($('#rotate_processor_with_cc_number').val());
                var message = '<div class="rotate-every">If a C.C # been process<span class="rotate-blue"> ' + parseInt($("#rotate_processor_with_cc_number").val()) + ' </span>time/s on the same processor, rotate to the next process.</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#rotate_processor_with_cc_number").val('');
            }
            break;
        case "block_card_for_decline_count":
            if (parseInt($("#block_card_for_decline_count_number").val()) > 0 && parseInt($("#block_card_for_decline_count_no_of_hours").val()) > 0) {
                rule_success = true;
                $("input[name='block_card_for_decline_count[" + count + "][count]']").val($('#block_card_for_decline_count_number').val());
                $("input[name='block_card_for_decline_count[" + count + "][no_of_hours]']").val($('#block_card_for_decline_count_no_of_hours').val());
                var message = '<div class="rotate-every">If a C.C # been Decline<span class="rotate-blue"> ' + parseInt($("#block_card_for_decline_count_number").val()) + ' </span>time/s on the same Processor group block the C.C for <span class="rotate-blue"> ' + parseInt($("#block_card_for_decline_count_no_of_hours").val()) + ' </span> hour/s</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#block_card_for_decline_count_number").val('');
                $("#block_card_for_decline_count_no_of_hours").val('');
            }
            break;
        case "processor_daily_volume":
            if (parseFloat($("#processor_daily_volume_percentage").val()) > 0) {
                rule_success = true;
                $("input[name='processor_daily_volume[" + count + "][percentage]']").val($('#processor_daily_volume_percentage').val());
                var message = '<div class="rotate-every">If a processor get to<span class="rotate-blue"> ' + parseInt($("#processor_daily_volume_percentage").val()) + ' </span>percentage of total Daily volume send Slack alert. </div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#processor_daily_volume_percentage").val('');
            }
            break;
        case "processor_monthly_volume":
            if (parseFloat($("#processor_monthly_volume_percentage").val()) > 0) {
                rule_success = true;
                $("input[name='processor_monthly_volume[" + count + "][percentage]']").val($('#processor_monthly_volume_percentage').val());
                var message = '<div class="rotate-every">If a processor get to<span class="rotate-blue"> ' + parseInt($("#processor_monthly_volume_percentage").val()) + ' </span>percentage of total Monthly volume send Slack alert. </div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#processor_monthy_volume_percentage").val('');
            }
            break;
        case "group_daily_volume":
            if (parseFloat($("#group_daily_volume_percentage").val()) > 0) {
                rule_success = true;
                $("input[name='group_daily_volume[" + count + "][percentage]']").val($('#group_daily_volume_percentage').val());
                var message = '<div class="rotate-every">If a group get to<span class="rotate-blue"> ' + parseInt($("#group_daily_volume_percentage").val()) + ' </span>percentage of total Daily volume send Slack alert. </div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#group_daily_volume_percentage").val('');
            }
            break;
        case "group_monthly_volume":
            if (parseFloat($("#group_monthly_volume_percentage").val()) > 0) {
                rule_success = true;
                $("input[name='group_monthly_volume[" + count + "][percentage]']").val($('#group_monthly_volume_percentage').val());
                var message = '<div class="rotate-every">If a group get to<span class="rotate-blue"> ' + parseInt($("#group_monthly_volume_percentage").val()) + ' </span>percentage of total Monthly volume send Slack alert. </div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#group_monthy_volume_percentage").val('');
            }
            break;
        case "block_processor_daily_volume":
            if (parseFloat($("#block_processor_daily_volume_percentage").val()) > 0) {
                rule_success = true;
                $("input[name='block_processor_daily_volume[" + count + "][percentage]']").val($('#block_processor_daily_volume_percentage').val());
                var message = '<div class="rotate-every">When a processor get to<span class="rotate-blue"> ' + parseInt($("#block_processor_daily_volume_percentage").val()) + ' </span>percentage of total Daily volume, block processor until the end of the day (PST) from receiving transaction. </div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#block_processor_daily_volume_percentage").val('');
            }
            break;
        case "block_processor_cbk_percent":
            break;
        case "processor_decline_percent":
            if (parseFloat($("#processor_decline_percent_percentage").val()) > 0 && $("#processor_decline_percent_operator").val() != "" && parseInt($("#processor_decline_percent_no_of_hours").val()) > 0) {
                rule_success = true;
                check_available_operators(type, "edit", $("#"+ type +"_operator").val(),$("input[name='processor_decline_percent[" + count + "][operator]']").val());
                $("input[name='processor_decline_percent[" + count + "][percentage]']").val($('#processor_decline_percent_percentage').val());
                $("input[name='processor_decline_percent[" + count + "][operator]']").val($('#processor_decline_percent_operator').val());
                $("input[name='processor_decline_percent[" + count + "][no_of_hours]']").val($('#processor_decline_percent_no_of_hours').val());
                var message = '<div class="rotate-every">If a processor decline percentage is <span class="rotate-blue"> ' + $("#processor_decline_percent_operator").val() + ' </span> <span class="rotate-blue"> ' + parseFloat($("#processor_decline_percent_percentage").val()) + ' </span> % in <span class="rotate-blue"> ' + $("#processor_decline_percent_no_of_hours").val() + ' </span> hour/s send alert to Slack. </div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#processor_decline_percent_percentage").val('');
                $("#processor_decline_percent_no_of_hours").val('');
                $("#processor_decline_percent_operator").val('');
            }
            break;
        case "processor_decline_times":
            if (parseFloat($("#processor_decline_times_count").val()) > 0) {
                rule_success = true;
                $("input[name='processor_decline_times[" + count + "][count]']").val($('#processor_decline_times_count').val());
                var message = '<div class="rotate-every">If a processor decline <span class="rotate-blue"> ' + parseInt($("#processor_decline_times_count").val()) + ' </span> time/s  in a row, send slack alert with decline messages.</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#processor_decline_times_count").val('');
            }
            break;
        case "move_group_on_cc_country":
            break;
        case "decline_on_cc_country":
            if ($("#decline_on_cc_country_operator").val() != "" && $("#decline_on_cc_country_country").val() != "" ) {
                rule_success = true;
                check_available_operators(type, "edit", $("#"+ type +"_operator").val(),$("input[name='decline_on_cc_country[" + count + "][operator]']").val());
                $("input[name='decline_on_cc_country[" + count + "][operator]']").val($('#decline_on_cc_country_operator').val());
                var countries = "";
                var countries_select = "";
                $(".select_decline_on_cc_country option:selected").each(function () {
                    if ($(this).val().length != "") {
                        countries = countries + $(this).text() + ", ";
                        countries_select = countries_select + $(this).val() + ",";
                    }
                });
                $("input[name='decline_on_cc_country[" + count + "][country]']").val(countries_select.slice(0, -1));
                var message = '<div class="rotate-every">If Credit card country <span class="rotate-blue">' + $("#decline_on_cc_country_operator").val()+ '</span>,<span class="rotate-blue"> ' + countries.slice(0, -2) + ' </span> Decline the transaction.</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#decline_on_cc_country_country").val('');
                $("#decline_on_cc_country_operator").val('');
                $("#decline_on_cc_country_country").multipleSelect({placeholder: "Select Countries", selectAll: false});
            }
            break;
        case "move_group_on_cc_brand":
            break;
        case "rotate_processor_on_cc_brand":
            if ($("#rotate_processor_on_cc_brand_operator").val() != "" && $("#rotate_processor_on_cc_brand_card_brand").val() != "" && $("#rotate_processor_on_cc_brand_descriptor").val() != "" && $("#rotate_processor_on_cc_brand_descriptor").val() != null && parseInt($("#rotate_processor_on_cc_brand_count").val()) > 0) {
                rule_success = true;
                check_available_operators(type, "edit", $("#"+ type +"_operator").val(),$("input[name='rotate_processor_on_cc_brand[" + count + "][operator]']").val());
                $("input[name='rotate_processor_on_cc_brand[" + count + "][count]']").val($('#rotate_processor_on_cc_brand_count').val());
                $("input[name='rotate_processor_on_cc_brand[" + count + "][card_brand]']").val($('#rotate_processor_on_cc_brand_card_brand').val());
                $("input[name='rotate_processor_on_cc_brand[" + count + "][operator]']").val($('#rotate_processor_on_cc_brand_operator').val());
                var cc_descriptor = "";
                var cc_descriptor_select = "";
                $(".select_amount_cc_descriptor option:selected").each(function () {
                    if ($(this).val().length != "") {
                        cc_descriptor = cc_descriptor + $(this).text() + ", ";
                        cc_descriptor_select = cc_descriptor_select + $(this).val() + ",";
                    }
                });
                check_available_gateways($("#rotate_processor_on_cc_brand_descriptor").val(),"edit");
                var old_descriptors = $("input[name='rotate_processor_on_cc_brand[" + count + "][descriptor]']").val();
                $("input[name='rotate_processor_on_cc_brand[" + count + "][descriptor]']").val(cc_descriptor_select.slice(0, -1));
                able_to_remove_gateway_from_group($("input[name='rotate_processor_on_cc_brand[" + count + "][descriptor]']").val(), "update", old_descriptors);
                var message = '<div class="rotate-every">If transaction amount <span class="rotate-blue"> ' + $("#rotate_processor_on_cc_brand_operator").val() + $("#rotate_processor_on_cc_brand_card_brand").val() + ' </span>send transaction to<span class="rotate-blue"> ' + cc_descriptor.slice(0, -2) + ' </span>and rotate every <span class="rotate-blue"> ' + $("#rotate_processor_on_cc_brand_count").val() + ' </span> transaction/s between processor/s.</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#rotate_processor_on_cc_brand_operator").val('');
                $("#rotate_processor_on_cc_brand_card_brand").val('');
                $("#rotate_processor_on_cc_brand_count").val('');
                $("#rotate_processor_on_cc_brand_descriptor").val('');
                $("#rotate_processor_on_cc_brand_descriptor").select2().trigger('change');
            }
            break;
        case "block_card_for_reason":
            if ($("#block_card_for_reason_no_of_hours").val() != "" && $("#block_card_for_reason_decline_reason").val() != "" ) {
                rule_success = true;
                $("input[name='block_card_for_reason[" + count + "][no_of_hours]']").val($('#block_card_for_reason_no_of_hours').val());
                var decline_reason = "";
                var decline_reason_select = "";
                $(".select_decline_reason option:selected").each(function () {
                    if ($(this).val().length != "") {
                        decline_reason = decline_reason + $(this).text() + ", ";
                        decline_reason_select = decline_reason_select + $(this).val() + ",";
                    }
                });
                $("input[name='block_card_for_reason[" + count + "][decline_reason]']").val(decline_reason_select.slice(0, -1));
                var message = '<div class="rotate-every">If C.C # got decline for <span class="rotate-blue">' + decline_reason.slice(0, -2) + '</span>, block the card for <span class="rotate-blue"> ' +$("#block_card_for_reason_no_of_hours").val()+ ' </span> hour/s or Move the card to blacklist.</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#block_card_for_reason_no_of_hours").val('');
                $("#block_card_for_reason_decline_reason").val('');
                $("#block_card_for_reason_decline_reason").multipleSelect({placeholder: "Select Reason", selectAll: false});
            }
            break;
        case "block_card_for_processed_count":
            if (parseInt($("#block_card_for_processed_count_count").val()) > 0 && parseInt($("#block_card_for_processed_count_no_of_hours").val()) > 0) {
                rule_success = true;
                $("input[name='block_card_for_processed_count[" + count + "][count]']").val($('#block_card_for_processed_count_count').val());
                $("input[name='block_card_for_processed_count[" + count + "][no_of_hours]']").val($('#block_card_for_processed_count_no_of_hours').val());
                var message = '<div class="rotate-every">If a C.C # been process<span class="rotate-blue"> ' + parseInt($("#block_card_for_processed_count_count").val()) + ' </span>time/s on the same group block him for <span class="rotate-blue"> ' + parseInt($("#block_card_for_processed_count_no_of_hours").val()) + ' </span> hour/s</div>';
                $("#rule_box_"+ type +"_"+ count).html(message);
                $("#block_card_for_processed_count_count").val('');
                $("#block_card_for_processed_count_no_of_hours").val('');
            }
            break;
    }
    if (rule_success) {
        if($("#automation_tab").data('status') == "active"){
            $("#automation_rules").addClass("admin-show").removeClass("admin-hide");
            $("#notification_rules").removeClass("admin-show").addClass("admin-hide");
        }else{
            $("#notification_rules").addClass("admin-show").removeClass("admin-hide");
            $("#automation_rules").removeClass("admin-show").addClass("admin-hide");
        }
        $("#" + type + "_new").addClass("admin-hide").removeClass('admin-show');
        $("#add_rule_to_group_"+type).text('Add Rule to group');
        $("#add_rule_to_group_"+type).data('update', "false");
        $("#add_rule_to_group_"+type).data('count',"null");
        $(".payment_gateways_section_b").addClass("admin-hide").removeClass('admin-show');
        $(".load_balancer_rules_section_b").addClass("admin-show").removeClass("admin-hide");
        $("#backStep").show();
        $("#remove-descriptor").show();
    } else {
        $(".rules_error").show().fadeOut(3000);
    }
}

function enable_disable_add_rule_btn(type) {
    var allow_rule = 1;
    if(type == "rotate_processor_with_amount" || type == "processor_decline_percent"){
        allow_rule = 3;
    }else if (type == "decline_on_cc_country" || type == "rotate_processor_on_cc_brand"){
        allow_rule = 2;
    }
    if($("."+type).children().length == allow_rule){
        $("#"+type).prop("disabled",true);
    }else{
        $("#"+type).prop("disabled",false);
    }
    if($("#m_accordion_3").children().length > 0){
        $("#create_group").prop("disabled", false);
    }else{
        $("#create_group").prop("disabled", true);
    }
}

function while_edit_load_balancer() {
    if($("#load_balancer_id").val() != ""){
        $.each( ["block_card_for_processed_count", "rotate_processor", "rotate_processor_with_amount", "rotate_processor_with_cc", "block_card_for_decline_count", "processor_daily_volume", "processor_monthly_volume", "group_daily_volume", "group_monthly_volume", "block_processor_daily_volume", "block_processor_cbk_percent", "processor_decline_percent", "processor_decline_times", "move_group_on_cc_country", "decline_on_cc_country", "move_group_on_cc_brand", "block_card_for_reason"], function( index, type ) {
            var allow_rule = 1;
            if(type == "rotate_processor_with_amount" || type == "processor_decline_percent"){
                allow_rule = 3;
            }else if (type == "decline_on_cc_country" || type == "rotate_processor_on_cc_brand"){
                allow_rule = 2;
            }
            if(type != "block_card_for_reason" && $("."+type).children().length >= allow_rule){
                $("#"+type).prop("disabled",true);
            }else{
                $("#"+type).prop("disabled",false);
            }
        });
    }
    setTimeout(function () {
        if($("#m_accordion_3").children().length > 0){
            $("#create_group").prop("disabled", false);
        }else{
            $("#create_group").prop("disabled", true);
        }
    },900)
}
// update occupied gateways while adding updating or deleting rules
function check_available_gateways(gateways, action){
    // var occupied_gateways = $("#occupied_gateways").val();
    // if(action == "delete"){
    //     occupied_gateways = occupied_gateways.split(',');
    //     var on_edit_occupied_gateways = gateways.split(',');
    //     $.each( on_edit_occupied_gateways, function( index, remove_value ) {
    //         if( $.inArray( ""+remove_value, occupied_gateways )  != -1 ) {
    //             occupied_gateways.splice( $.inArray(remove_value, occupied_gateways), 1 );
    //         }
    //     });
    //     $("#occupied_gateways").val(occupied_gateways.join());
    // }else{
    //     if(occupied_gateways != ""){
    //         occupied_gateways = occupied_gateways.split(',');
    //         if(action == "edit"){
    //             var on_edit_occupied_gateways = $("#on_edit_occupied_gateways").val().split(',');
    //             $.each( on_edit_occupied_gateways, function( index, remove_value ) {
    //                 if( $.inArray( ""+remove_value, occupied_gateways )  != -1 ) {
    //                     occupied_gateways.splice( $.inArray(remove_value, occupied_gateways), 1 );
    //                 }
    //             });
    //             $("#on_edit_occupied_gateways").val('');
    //         }
    //         if(occupied_gateways.length == 0){
    //             $("#occupied_gateways").val(gateways);
    //         }else{
    //             $("#occupied_gateways").val(occupied_gateways.join()+","+gateways);
    //         }
    //     }else{
    //         $("#occupied_gateways").val(gateways);
    //     }
    // }
}
function check_available_operators(type, action, operator, old_operator){
    var occupied_operators = $("#operator_"+type).val();
    var new_available_operators = [];
    if(action == "delete"){
        occupied_operators = occupied_operators.split(',');
        $.each( occupied_operators, function( index, value ) {
            if( operator != value ) {
                new_available_operators.push(value)
            }
        });
        $("#operator_"+type).val(new_available_operators.join());
    }else{
        if(occupied_operators != ""){
            occupied_operators = occupied_operators.split(',');
            new_available_operators = occupied_operators;
            if(action == "edit"){
                occupied_operators = jQuery.grep(occupied_operators, function(value) {
                    return value != old_operator;
                });
                new_available_operators = occupied_operators;
                new_available_operators.push(operator);
            }
            if(new_available_operators.length == 0){
                $("#operator_"+type).val(operator);
            }else{
                $("#operator_"+type).val(new_available_operators.join()+","+operator);
            }
        }else{
            $("#operator_"+type).val(operator);
        }
    }
}
function initialize_descriptor_dropdown(type, val, action) {
    $('#' + type + '_descriptor').find('option').remove().end();
    // var occupied_gateways = $("#occupied_gateways").val().split(',');
    $(".remove_gateway_checkboxes").each(function (value) {
        // if(jQuery.inArray(""+$(this).data('gateway_id'), occupied_gateways) == -1){
        view = "("+$(this).data('gateway_group')+") - "+$(this).data('gateway_descriptor')
        var o = new Option(view, $(this).data('gateway_id'));
        $(o).html(view);
        // }
        // if(action == "edit"){
        //     if(jQuery.inArray(""+$(this).data('gateway_id'), val) != -1){
        //         var o = new Option($(this).data('gateway_descriptor'), $(this).data('gateway_id'));
        //         $(o).html($(this).data('gateway_descriptor'));
        //     }
        // }
        $('#' + type + '_descriptor').append(o);
    });
    $('#'+ type +'_descriptor').val(val);
    $('#'+ type +'_descriptor').select2().trigger('change');
}
function initialize_operator_dropdown(type, val, action) {
    var available_operator = {"Select": ""};
    var operators = [];
    $('#' + type + '_operator').find('option').remove().end();
    if($("#operator_"+ type).val().length > 0){
        var selected_operators = $("#operator_"+ type).val().split(',');
        if (type == "rotate_processor_with_amount" || type == "processor_decline_percent"){
            operators = ["less_than", "greater_than", "equal_to"]
        }else if(type == "decline_on_cc_country" || type == "rotate_processor_on_cc_brand"){
            operators = ["equal_to", "not_equal_to"]
        }
        $.each( operators, function( index, value ) {
            if($.inArray( ""+value, selected_operators )  == -1 ){
                available_operator[value.replace("_"," ")] = value
            }
        });
        if(action == "edit"){
            available_operator[val.replace("_"," ")] = val
        }
    }else{
        if (type == "rotate_processor_with_amount" || type == "processor_decline_percent"){
            available_operator = {"Select": "", "less than": "less_than", 'equal to': 'equal_to', 'greater than': 'greater_than'}
        }else if(type == "decline_on_cc_country" || type == "rotate_processor_on_cc_brand"){
            available_operator = {"Select": "", 'equal to': 'equal_to', 'not_equal_to': 'not_equal_to'}
        }
    }
    $.each( available_operator, function( key, value ) {
        $('#' + type + '_operator').append(new Option(key, value));
    });
}

$(document).on('change','.active_inactive_load_balancer', function () {
    var status = false;
    var id = $(this).data('id');
    if($(this).is(":checked")) {
        status = true;
    }
    $.ajax({
        url: "/admins/load_balancers/"+id ,
        type: 'PUT',
        data: {status: status, id: id},
        success: function (data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
});

function able_to_remove_gateway_from_group(gateway_ids, action, edit_gateways){
    if(typeof gateway_ids == "string"){
        gateway_ids = gateway_ids.split(',');
    }
    $.each( gateway_ids, function( index, value ) {
        if(action == "new" || action == "update" || action == "active"){
            var temp_count = parseInt($("input#gateway_checkboxes_"+value).data("rule_id"));
            $("input#gateway_checkboxes_"+value).data("rule_id", temp_count + 1);
            // $("input#gateway_checkboxes_"+value).data("rule_status", "active");
        }else{
            var temp_count = parseInt($("input#gateway_checkboxes_"+value).data("rule_id"));
            $("input#gateway_checkboxes_"+value).data("rule_id",temp_count - 1);
            // $("input#gateway_checkboxes_"+value).data("rule_status","inactive");

        }
    });
    if(edit_gateways != null && edit_gateways != ""){
        $.each( edit_gateways.split(','), function( index, value ) {
            var temp_count = parseInt($("input#gateway_checkboxes_"+value).data("rule_id"));
            $("input#gateway_checkboxes_"+value).data("rule_id",temp_count - 1);
            // $("input#gateway_checkboxes_"+value).data("rule_status","inactive");
        });
    }

}

$(document).on('click', ".rule_types", function () {
    if($(this).data('status') != "active"){
        var type = $(this).data('type');;
        if(type == "automation"){
            $("#automation_rules").addClass("admin-show").removeClass("admin-hide");
            $("#notification_rules").removeClass("admin-show").addClass("admin-hide");
            $("#notification_tab").removeClass("underlinee").data("status", "inactive");
            $("#automation_tab").data("status", "active").addClass("underlinee");
        }else{
            $("#notification_rules").addClass("admin-show").removeClass("admin-hide");
            $("#automation_rules").removeClass("admin-show").addClass("admin-hide");
            $("#automation_tab").removeClass("underlinee").data("status", "inactive");
            $("#notification_tab").addClass('underlinee').data("status", "active");
        }
    }
})

$(document).on('keyup', ".search_gateway_field", function() {
    var value = $(this).val();
    var type = $(this).data('type');
    $("."+ type).each(function(i) {
        $(this).find("tr").each(function(index) {
            if (index != 0) {
                $row = $(this);
                var id = $.trim($row.find("td:eq(1)").text());
                if (id.indexOf(value) != 0) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            }
        });
    });
});

function number_with_precision(amount){
    amount = parseFloat(amount).toFixed(2);
    var num2 = amount.toString().split('.');
    var thousands = num2[0].split('').reverse().join('').match(/.{1,3}/g).join(',');
    return thousands.split('').reverse().join('') +"."+ num2[1]
}