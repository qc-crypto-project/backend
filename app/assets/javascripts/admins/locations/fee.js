$(document).ready(function(){
    $(".buyrate_fields").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
});

//return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100); });

(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };
}(jQuery));
$(".percent").inputFilter(function(value) {
    return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100);});
$(".two_decimal").inputFilter(function(value) {
    return /^\d*(\.\d{0,2})?$/.test(value);});

$('.form-control.numeric.float.optional.buyrate_fields').on('change', function () {
    var id = $(this).attr('id');
    if ($(this).val() == 0) {
        $('#' + id + '_date').val('');
        $('#location-btn').attr("disabled", false);
    } else if ($(this).val() != 0) {
        $('#' + id + '_date').val(moment(new Date).format("MM/DD/YYYY"));
    }
});
$('.form-group.string.optional.location_fees_service_date, .form-group.string.optional.location_fees_statement_date, .form-group.string.optional.location_fees_misc_date, .form-group.string.optional.location_fee_service_date, .form-group.string.optional.location_fee_statement_date, .form-group.string.optional.location_fee_misc_date').on('keyup change focusout', function () {
    var id = $(this).find('input.form-control').attr('id');
    var parent_id = id.split('_date')[0];
    var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if ($('#' + parent_id).val() == 0) {
        $('#' + id).val('');
        $('#location-btn').attr("disabled", false);
    } else if ($('#' + parent_id).val() == 0 && $('#' + id).val() != '') {
        $('#' + id).val('');
        $('#location-btn').attr("disabled", false);
    } else if ($('#' + parent_id).val() != 0 && $('#' + id).val() != '' && date_regex.test($('#' + id).val())) {
        $('#location-btn').attr("disabled", false);
    } else {
        $('#' + id).val('');
        $('#location-btn').attr("disabled", true);
    }
});
$('#trans_app_btn').hide();
//    $('#b2b_btn').hide();
function hide_show_btn(app, dc, cc, b2b, giftcard, send_check, add_money) {
    if (app == "true") {
        $('#trans_app_btn').show();
    } else {
        $('#trans_app_btn').hide();
    }
    if (dc == "true") {
        $('#trans_dc_btn').show();
    } else {
        $('#trans_dc_btn').hide();
    }
    if (cc == "true") {
        $('#trans_cc_btn').show();
    } else {
        $('#trans_cc_btn').hide();
    }
    if (b2b == "true") {
        $('#b2b_btn').show();
    } else {
        $('#b2b_btn').hide();
    }
    if (giftcard == "true") {
        $('#giftcard_btn').show();
    } else {
        $('#giftcard_btn').hide();
    }
    if (send_check == "true") {
        $('#send_check_btn').show();
    } else {
        $('#send_check_btn').hide();
    }
    if (add_money == "true") {
        $('#add_check_btn').show();
    } else {
        $('#add_check_btn').hide();
    }
}
$(function () {
    $('#trans_app_btn').click(function () {
        $("#send_check_process").hide();
        $("#giftcard_process").hide();
        $("#add_check_process").hide();
        $("#b2b_process").hide();
        $("#trans_app_process").show();
        $("#trans_dc_process").hide();
        $("#trans_cc_process").hide();
        $(".heading_split").text('').html(' (Mobile App Fee)');
        hide_show_btn("false", "true", "true", "true", "true", "true", "true")
    });
    $('#trans_dc_btn').click(function () {
        $("#send_check_process").hide();
        $("#giftcard_process").hide();
        $("#add_check_process").hide();
        $("#b2b_process").hide();
        $("#trans_app_process").hide();
        $("#trans_dc_process").show();
        $("#trans_cc_process").hide();
        $(".heading_split").text('').html(' (Debit Card Fee)');
        hide_show_btn("true", "false", "true", "true", "true", "true", "true")
    });
    $('#trans_cc_btn').click(function () {
        $("#send_check_process").hide();
        $("#giftcard_process").hide();
        $("#add_check_process").hide();
        $("#b2b_process").hide();
        $("#trans_app_process").hide();
        $("#trans_dc_process").hide();
        $("#trans_cc_process").show();
        $(".heading_split").text('').html(' (Credit Card Fee)');
        hide_show_btn("true", "true", "false", "true", "true", "true", "true")
    });
    $('#b2b_btn').click(function (e) {
        $("#send_check_process").hide();
        $("#giftcard_process").hide();
        $("#add_check_process").hide();
        $("#b2b_process").show();
        $("#trans_app_process").hide();
        $("#trans_dc_process").hide();
        $("#trans_cc_process").hide();
        $(".heading_split").text('').html(' (B2B Fee)');
        hide_show_btn("true", "true", "true", "false", "true", "true", "true")
//            $('#b2b_btn').children().first().removeClass('fa-plus-circle').addClass('fa-arrow-right-circle');
    });
    $('#giftcard_btn').click(function () {
        $("#send_check_process").hide();
        $("#giftcard_process").show();
        $("#add_check_process").hide();
        $("#trans_app_process").hide();
        $("#trans_dc_process").hide();
        $("#trans_cc_process").hide();
        $(".heading_split").text('').html(' (Gift Card Fee)');
        $("#b2b_process").hide();
        hide_show_btn("true", "true", "true", "true", "false", "true", "true")
    });
    $('#send_check_btn').click(function () {
        $("#send_check_process").show();
        $("#giftcard_process").hide();
        $("#add_check_process").hide();
        $("#b2b_process").hide();
        $("#trans_app_process").hide();
        $("#trans_dc_process").hide();
        $("#trans_cc_process").hide();
        $(".heading_split").text('').html(' (Send Check Fee)');
        hide_show_btn("true", "true", "true", "true", "true", "false", "true")
    });
    $('#add_check_btn').click(function () {
        $("#send_check_process").hide();
        $("#giftcard_process").hide();
        $("#add_check_process").show();
        $("#b2b_process").hide();
        $("#trans_app_process").hide();
        $("#trans_dc_process").hide();
        $("#trans_cc_process").hide();
        $(".heading_split").text('').html(' (Add Check Fee)');
        hide_show_btn("true", "true", "true", "true", "true", "true", "false")
    });
});
var trans_app = $("#transaction_fee_app").val();
var trans_app_dollar = $("#transaction_fee_app_dollar").val();
var trans_dc = $("#transaction_fee_dcard").val();
var trans_dc_dollar = $("#transaction_fee_dcard_dollar").val();
var trans_cc = $("#transaction_fee_ccard").val();
var trans_cc_dollar = $("#transaction_fee_ccard_dollar").val();
var bvalue = $("#b_fee").val();
var giftvalue = $("#giftcard_fee").val();
var rvalue = $("#redeem_fees").val();
var chvalue = $("#send_check").val();
var acvalue = $("#add_checks").val();
var rdvalue = $("#redeem_checks").val();
function hidden_check(id, hidden_id) {
    if (hidden_id == 'transaction_fee_app') {
        trans_app = $("#transaction_fee_app").val();
    } else if (hidden_id == 'transaction_fee_app_dollar') {
        trans_app_dollar = $("#transaction_fee_app_dollar").val();
    } else if (hidden_id == 'transaction_fee_dcard') {
        trans_dc = $("#transaction_fee_dcard").val();
    } else if (hidden_id == 'transaction_fee_dcard_dollar') {
        trans_dc_dollar = $("#transaction_fee_dcard_dollar").val();
    } else if (hidden_id == 'transaction_fee_ccard') {
        trans_cc = $("#transaction_fee_ccard").val();
    } else if (hidden_id == 'transaction_fee_ccard_dollar') {
        trans_cc_dollar = $("#transaction_fee_ccard_dollar").val();
    } else if (hidden_id == 'b2b_fee') {
        bvalue = $("#b_fee").val();
    } else if (hidden_id == 'giftcard_fee') {
        giftvalue = $("#giftcard_fee").val();
    } else if (hidden_id == 'redeem_fee') {
        rvalue = $("#redeem_fees").val();
    } else if (hidden_id == 'send_checks') {
        chvalue = $("#send_check").val();
    } else if (hidden_id == 'add_check') {
        acvalue = $("#add_checks").val();
    } else if (hidden_id == 'redeem_check') {
        rdvalue = $("#redeem_checks").val();
    }
}
//function check_max_val(){
$('.trans_app_dollar_fields').keyup(function () {
    var sum = 0;
    $('.trans_app_dollar_fields').each(function () {
        sum += Number($(this).val());
    });
    if (sum <= trans_app_dollar) {
        console.log('sum ' + sum, 'cvalue ' + trans_app_dollar);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.trans_app_fields').keyup(function () {
    var sum = 0;
    $('.trans_app_fields').each(function () {
        sum += Number($(this).val());
    });
    if (sum <= trans_app) {
        console.log('sum ' + sum, 'cfeevalue ' + trans_app);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.trans_dc_dollar_fields').keyup(function () {
    var sum = 0;
    $('.trans_dc_dollar_fields').each(function () {
        sum += Number($(this).val());
    });
    if (sum <= trans_dc_dollar) {
        console.log('sum ' + sum, 'cvalue ' + trans_dc_dollar);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.trans_dc_fields').keyup(function () {
    var sum = 0;
    $('.trans_dc_fields').each(function () {
        sum += Number($(this).val());
    });
    if (sum <= trans_dc) {
        console.log('sum ' + sum, 'cfeevalue ' + trans_dc);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.trans_cc_dollar_fields').keyup(function () {
    var sum = 0;
    $('.trans_cc_dollar_fields').each(function () {
        sum += Number($(this).val());
    });
    if (sum <= trans_cc_dollar) {
        console.log('sum ' + sum, 'cvalue ' + trans_cc_dollar);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.trans_cc_fields').keyup(function () {
    var sum = 0;
    $('.trans_cc_fields').each(function () {
        sum += Number($(this).val());
    });
    if (sum <= trans_cc) {
        console.log('sum ' + sum, 'cfeevalue ' + trans_cc);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.b2b_fields').keyup(function () {
    var b2b_sum = 0;
    $('.b2b_fields').each(function () {
        b2b_sum += Number($(this).val());
    });
    if (b2b_sum <= bvalue) {
        console.log('sum ' + b2b_sum, 'cvalue ' + bvalue);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.giftcard_fields').keyup(function () {
    var giftcard_sum = 0;
    $('.giftcard_fields').each(function () {
        giftcard_sum += Number($(this).val());
    });
    if (giftcard_sum <= giftvalue) {
        console.log('sum ' + giftcard_sum, 'giftvalue ' + giftvalue);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.checks_fields').keyup(function () {
    var check_sum = 0;
    $('.checks_fields').each(function () {
        check_sum += Number($(this).val());
    });
    console.log("check sum----->" + check_sum, "chvalue-------->" + chvalue);
    if (check_sum <= chvalue) {
        console.log('sum ' + check_sum, 'cvalue ' + chvalue);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.redeem_fields').keyup(function () {
    var red_sum = 0;
    $('.redeem_fields').each(function () {
        red_sum += Number($(this).val());
    });
    if (red_sum <= rvalue) {
        console.log('sum ' + red_sum, 'cvalue ' + rvalue);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.add_checks_fields').keyup(function () {
    var add_check_sum = 0;
    $('.add_checks_fields').each(function () {
        add_check_sum += Number($(this).val());
    });
    console.log('chcek------' + acvalue, 'new----------' + add_check_sum);
    if (add_check_sum <= acvalue) {
        console.log('sum ' + add_check_sum, 'cvalue ' + acvalue);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
$('.redeem_checks_fields').keyup(function () {
    var add_redeem_sum = 0;
    $('.redeem_checks_fields').each(function () {
        add_redeem_sum += Number($(this).val());
    });
    if (add_redeem_sum <= rdvalue) {
        console.log('sum ' + add_redeem_sum, 'cvalue ' + rdvalue);
    } else {
        $(this).val('');
        alert('commission limit exceeded');
    }
});
//}
// $('.buyrate_fields').bind('keyup change', function () {
//     this.value = this.value.replace(/[^0-9\.]/g, '');
// });
$(document).ready(function () {
    $('.location_fee_datepicker').datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: new Date()
    });
});
$(document).on('keyup', '#location_fee_days', function () {
    $(this).val($(this).val().replace(/[^0-9]/g, ''));
})
//    $.datepicker.setDefaults({
//        dateFormat: 'yy-mm-dd'
//    });