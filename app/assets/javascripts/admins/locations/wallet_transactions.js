$(document).ready(function(){
    var wid = $("#in").val();
    $("#wallTrans4").attr("width", wid + "%");
});

$(document).ready(function(){
    var wid = $("#in2").val();
    $("#wallTrans5").attr("width", wid + "%");
});
$(function() {
    $('#query_type_').multipleSelect({
        placeholder: "Select Type",
        selectAll: false
    });
    $('#query_gateway_').multipleSelect({
        placeholder: "Select Gateway",
        selectAll: false
    });
    $('#query_risk_eval_').multipleSelect({
        placeholder: "Select Status",
        selectAll: false
    });

    $( function() {
        $( document ).tooltip();
    } );
    $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });
});
var nowDate = new Date();
var newdate = (nowDate.setDate(nowDate.getDate()));
var nd = new Date(newdate);
$('input[name="query[date]"]').daterangepicker({
    autoUpdateInput: false,
    locale: {
        format: 'MM/DD/YYYY',
    },
    "maxSpan": {
        "days": 90
    },
    "maxDate": nd,
    "parentEl": $("#date_parent")
});

$('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
    $(this).val('');
});
$("#exportDate").daterangepicker({
    "maxSpan": {
        "days": 90
    },
    "maxDate": nd,
});
