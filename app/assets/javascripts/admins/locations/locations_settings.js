$(document).ready(function () {

    if( $('#location_standalone').is(":checked")){
        $('#standalonetype').show()
    }
    if($("#location_disable_sales_transaction").is(':checked')){
        $("#salesButton").show();
    }else{
        $('#sales_id').prop('checked', false);
        $('#vt_id').prop('checked', false);
        $('#batch_id').prop('checked', false);
        $("#salesButton").hide();
    }
    if($("#location_block_api").is(':checked')){
        $("#apiButton").show();
    }else{
        $('#vt_api').prop('checked', false);
        $('#vtt_api').prop('checked', false);
        $('#vd_api').prop('checked', false);
        $("#apiButton").hide();
    }

    if($("select#p_type option").filter(":selected").val() == "retail" || $("select#p_type option").filter(":selected").val() == "ecommerce_service"){
        $('#hold_on_delivery_custom').hide();
    }else{
        $('#hold_in_arrear_custom').hide();
    }
    $("#p_type").change(function(){
        if($(this).val() == "retail" || $(this).val() == "ecommerce_service"){
            $('#hold_on_delivery_custom').hide();
            $('#hold_in_arrear_custom').show();
        }else{
            $('#hold_on_delivery_custom').show();
            $('#hold_in_arrear_custom').hide();
        }
    });
    if ($("#location_hold_in_rear").is(':checked')){
        $("#hirDays").show();
        $('#location_hold_in_rear_days').attr('min', 2);
    }else {
        $("#hirDays").hide();
        $('#location_hold_in_rear_days').removeAttr('min');
    }
    $( "#location_hold_in_rear" ).change(function () {
        if($(this).is(':checked')){
            $('#location_on_hold_pending_delivery').prop('checked', false);
            $("#ohpdDays").hide();
            $("#hirDays").show();
            $('#location_hold_in_rear_days').attr('min', 2);
        }
        else{
            $("#hirDays").hide();
            $('#location_hold_in_rear_days').removeAttr('min');
        }
    });
    rtp_change();
    $( "#rtp_hook_checkbox" ).change(function () {
        rtp_change();
    });
    function rtp_change(){
        if ($("#rtp_hook_checkbox").is(':checked')){
            $("#rtpHookDiv").show();
            $("#rtp_hook_field").prop("required","true");
        }else {
            $("#rtpHookDiv").hide();
            $("#rtp_hook_field").prop("required","false").val('');
        }
    }

    if ($("#location_on_hold_pending_delivery").is(':checked')){
        $("#ohpdDays").show();
        $('#location_on_hold_pending_delivery_days').attr('min', 2);
    }else {
        $("#ohpdDays").hide();
        $('#location_on_hold_pending_delivery_days').removeAttr('min');
    }
    $( "#location_on_hold_pending_delivery" ).change(function () {
        if($(this).is(':checked')){
            $('#location_hold_in_rear').prop('checked', false);
            $("#hirDays").hide();
            $("#ohpdDays").show();
            $('#location_on_hold_pending_delivery_days').attr('min', 2);
        }
        else{
            $("#ohpdDays").hide();
            $('#location_on_hold_pending_delivery_days').removeAttr('min');
        }
    });

$( "#location_disable_sales_transaction" ).change(function () {
    if($(this).is(':checked')){
        $("#salesButton").show();
    }
    else{
        $('#sales_id').prop('checked', false);
        $('#vt_id').prop('checked', false);
        $('#batch_id').prop('checked', false);
        $("#salesButton").hide();
    }
});
$("#location_block_api" ).change(function () {
    if($(this).is(':checked')){
        $("#apiButton").show();
    }else{
        $('#vt_api').prop('checked', false);
        $('#vtt_api').prop('checked', false);
        $('#vd_api').prop('checked', false);
        $("#apiButton").hide();
        $( "#location-btn" ).removeClass('pointer-none');
    }
});

$('#location_standalone').change(function() {
    if($(this).is(":checked")) {
        $('#standalonetype').show()
    }else{
        $('#standalonetype').hide()
    }
});

    $('#location_apply_load_balancer').change(function() {
        if($(this).is(":checked")) {
            $('#apply_load_balancer').show()
        }else{
            $('#apply_load_balancer').hide()
        }
    });


    if ($("#location_transaction_limit").is(':checked')){
        $("#tran_amount_limit").show();
    }else {
        $("#tran_amount_limit").hide();
    }

    $( "#location_transaction_limit" ).change(function () {
        if($(this).is(':checked')){
            $("#tran_amount_limit").show();
        }
        else{
            $("#tran_amount_limit").hide();
        }
    });

    $('.add_amount_btn').click(function ()
    {
        if($(".amount_limit_div").length < 10 ){
            $("#tran_amount_limit").append('<div class="form-group col-md-12 amount_limit_div" ><span class="span_dollar_sign">$</span> <div class="col-md-3"> <input value="" name="location[transaction_limit_offset][]" class="form-control two_decimal"></div><div class="col-md-3"><a class="website_remove_field"><i class="fa fa-minus-circle fa-2x"></i></a> </div></div>');
        }
        $(".two_decimal").inputFilter(function(value) {
            return /^\d*(\.\d{0,2})?$/.test(value);});
    });

    $(".remove_amount_btn").on("click",function(){
        $(this).closest('.amount_limit_div').remove()
    });

});

$("#location_block_api").change(function(){
    if (!$(this).is(':checked')){
        $("#disable-api-check-error").html('');
    }
});

$("#location_disable_sales_transaction").change(function(){
    if (!$(this).is(':checked')){
        $("#disable-sales-check-error").html('');
    }
});
