$(document).ready(function () {

    var table = $('#datatable-keytable-bank').DataTable({
        responsive:false,
        "sorting": true,
        "bLengthChange": false,
        "searching": false,
        "bInfo":false,
        "bPaginate": false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 5}
        ]
    });

    var bank_type = $('#bank_detail_bank_account_type1').val();
    if(bank_type == ''){
        $('#bank-routing1').attr('disabled',true);
        $('#password-field1').attr('disabled',true);
        $('#bank-nmae1').attr('disabled',true);
    }
    $('#bank_detail_bank_account_type1').on('change',function () {
        if($(this).val() != '')
        {
            $('#bank-routing1').attr('disabled',false);
            $('#password-field1').attr('disabled',false);
            $('#bank-nmae1').attr('disabled',false);
        } else
        {
            $('#bank-routing1').attr('disabled',true);
            $('#password-field1').attr('disabled',true);
            $('#bank-nmae1').attr('disabled',true);
        }
    })

    $("#admin_confirm").on("hidden.bs.modal", function(){
        $('#password').val("");
        $("#password_btn").css("border", "0px  black");
        $("#info-text").text('');
    });

    $('INPUT[type="file"]').change(function () {
        if((this.files[0].size)/1024 > 6000) {
            alert("Please check upload image not over 5MB");
            this.value = '';
        }
    });

});
function myFunc(num) {
   var location_id = '<%= @location.id %>';
   var status = $("#acc_" + num).attr("data-status");
   if (status == "false") {
   $('#admin_confirm').modal('show')
   $("#password_btn").button().click(function() {
   var password = $("#password").val();
   $.ajax({
   url: "/admins/locations/" + location_id + "/d_account_no",
   method: 'get',
   dataType: 'json',
   data: {bank_id: num, password: password},
   success: function (result) {
   $("#acc_" + num).attr("data-status", "true");
   $("#acc_" + num).html('').html(result.acc_number + " <span  onclick='myFunc(" + num + ")' id='eye_" + num + "' class='fa fa-fw fa-eye-slash field-icon my-password'></span>");

   $('#admin_confirm').modal('hide');
   $('#password').val("");
   $("#password_btn").css("background", "#7867a7").text('').text("Verify");
},
   error: function () {

   $("#password_btn").css("border", "1px solid red");
   $("#info-text").text('').css("color", "red").text('Please try again!');
}
});
});
} else {
   $("#acc_" + num).html('').html("xxxxxxxxxxx <span  onclick='myFunc(" + num + ")' id='eye_" + num + "' class='fa fa-fw fa-eye field-icon my-password'></span>");
   $("#acc_" + num).attr("data-status", "false");
}
}
 // $('#password_btn').on('click',function () {
   //     verifyAdmin();
   // });
function verifyAdmdin() {
       var password = $('#password').val();
       $.ajax({
           url: "<%= verify_admin_password_admins_buy_rate_index_path %>",
           method: 'get',
           data: {password: password},
           success: function (data) {
               $("#password_btn").css("background", "green").text('').text("Verified");
               $("#info-text").text('');
               setTimeout(function () {
                   var x = $("#password-field")
                   if (x.attr('type') === "password") {
                       x.attr('type', 'text')
                   }
                   $('#password').attr("disabled","disabled");
                   $('#admin_confirm').modal('hide');
                   $('#admin_confirm').removeClass('show').removeClass('in').attr('style','display:none')
                   $('#verification_btn').hide();
                   $('#submit_transfer_btn').show();
                   $('#password').val("");
                   $("#password_btn").css("background", "#7867a7").text('').text("Verify");
               }, 1000);
           },
           error: function (data) {
               $("#password_btn").css("border", "1px solid red");
               $("#info-text").text('').css("color", "red").text('Please try again!');
           }
       });
   }
   function showAccount() {
   var x = $("#password-field1")
   if (x.attr('type') === "password") {
   x.attr('type','text')
   } else {
   x.attr('type','password')
    }
}
