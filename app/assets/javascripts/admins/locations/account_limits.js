$(document).ready(function(){
    $(".buyrate_fields").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});


//$("#sale_limit,#sale_limit_percentage,#monthly_processing_volume,#extra_over").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});

(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };
}(jQuery));
$(".percent").inputFilter(function(value) {
    return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100); });

$("#location_ach_limit,#location_push_to_card_limit,#location_check_limit,#attemptamount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
$(".chck-negative").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
$('#location_is_slot').change(function () {
    if ($(this).is(":checked")) {
        $('#locationisslot').fadeIn("slow");
        $('.under_line').css('border-bottom','1px solid lightgray');
    } else {
        $('#locationisslot').fadeOut("slow");
        $('.under_line').css('border-bottom','0px');
    }
});
if ($('#location_is_slot').is(":checked")) {
    $('#locationisslot').show();
    $('.under_line').css('border-bottom','1px solid lightgray');
}else{
    $('#locationisslot').hide();
}
function percentage_cal(key, id) {
    var sum = 0;
    $('.perc_attempt' + key).each(function () {
        sum += Number($(this).val());
    });
    if (sum > 100) {
        $("#attempt" + id).val('');
        alert('Percentage should be equal to 100!');
    }
}
function SelectAll(id) {
    $('#attempt' + id).focus();
    $('#attempt' + id).select();
}
});