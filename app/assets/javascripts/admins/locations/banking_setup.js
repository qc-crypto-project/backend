$(document).ready(function(){
    var length = $("#gateway_size").val();
    $("#div19").attr("width",length);
    $("#div19").attr("float","left");
    $("#bank-routing,#password-field").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    validateNewBankForm('new_bank_detail');

});
