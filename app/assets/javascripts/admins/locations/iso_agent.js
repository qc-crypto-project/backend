$(document).ready(function () {
    $("#super_company_percent").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $("#super_company_dolalr").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    var location_data = null;
    if(value_present(gon.location_edit_data)){
        location_data = gon.location_edit_data;
    }
    if(value_present(location_data)){
        if(value_present(location_data.profit_split) ){
            $('#profit_splits_on_section').show();
        }else{
            $('#profit_splits_on_section').hide();
        }
        if(value_present(location_data.baked_iso)){
            $('#baked_section').show();
        }else{
            $('#baked_section').hide();
        }
        if(value_present(location_data.sub_merchant_split)){
            $("#ez_section").show();
        }else{
            $("#ez_section").hide();
        }
        if(value_present(location_data.dispensary_credit_split)){
            $("#dispensary_section").show();
        }else{
            $("#dispensary_section").hide();
        }
        if(value_present(location_data. dispensary_debit_split)){
            $("#dispensary_debit_section").show();
        }else{
            $("#dispensary_debit_section").hide();
        }
        }


    $(document).on('change','#dispensary_switch',function(){
        if($(this).is(':checked')){
            $('#dispensary_section').show();
        } else{
            $('#dispensary_section').hide();
            $('#ez_merchant_1-error').remove();
        }
    });

    $(document).on('change','#dispensary_debit_switch',function(){
        if($(this).is(':checked')){
            $('#dispensary_debit_section').show();
        } else{
            $('#dispensary_debit_section').hide();
            $('#ez_merchant_1-error').remove();
        }
    });

    $('input#isos').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $('input#agents').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
    $('input#affiliates').bind('cut copy paste', function (e) {
        e.preventDefault();
    });



    $("#ez_percent,#ez_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});


    var wallets = "";
    var key_name = "";
    // profit split js
    if(value_present(location_data) && location_data.profit_split == true){
        $('#profit_check').show();
        if(value_present(location_data.profit_split_detail)){
            var split_detail = JSON.parse(location_data.profit_split_detail);
            var profit_split = split_detail["splits"]["profit_split"];
            delete profit_split["from"];
            for(var index in profit_split){
                var obj = profit_split[index]
                if(value_present(obj["name"]) && value_present(obj["wallet"])){
                    if(obj["role"].includes('Merchants')){
                        wallets = gon.profit_split_data;
                        if(value_present(wallets)){
                            $('#locationDiv'+index).css('visibility', 'visible');
                            $('#locationText'+index).css('visibility', 'visible');
                            var option = new Option("Choose Location", "");
                            $(option).html("Choose Location");
                            $('#locationText'+index).attr('name', "profit_splits[splits][profit_split]["+index+"][wallet]");
                            $("#locationText"+index).append(option);
                            $.map(wallets, function (wallet) {
                                var option = new Option(wallet.name, wallet.location_id);
                                $(option).html(wallet.name);
                                $("#locationText"+index).append(option);

                            });
                            $('#locationText'+index+' option[value='+obj["wallet"]+']').prop("selected", "selected")
                        }
                    }
                }
            }
        }
    }
    else {
        $('#profit_check').hide()
    }

    if(value_present(location_data) && location_data.baked_iso == true){
        $('#baked_check').show()
        if(value_present(location_data.profit_split_detail)){
            if(value_present(location_data.profit_split_detail["share_split"])){
                split_detail=JSON.parse(location_data.profit_split_detail)
                if(split_detail["share_split"] == "gbox"){
                    $("#gbox_radio").attr("checked", "checked");
                }else if(split_detail["share_split"] == "iso"){
                    $("#iso_radio").attr("checked", "checked");
                }
            }
        }
    }
    else {
        $('#baked_check').hide()
        $('#baked_section').hide()
    }

    if(location_data != null && location_data.baked_iso == true){
        $("#baked_iso_switch").attr("checked", "checked");
    }
    if(location_data != null && location_data.sub_merchant_split == true){
        $("#sub_merchant_split_switch").attr("checked", "checked");
    }
    if(location_data != null && location_data.net_profit_split == true){
        $("#net_profit_split_switch").attr("checked", "checked");
    }
    if(location_data != null && location_data.profit_split == true){
        $("#profit_split_switch").attr("checked", "checked");
    }


    // ez_merchant js
    if(value_present(location_data) && location_data.sub_merchant_split == true){
        if(value_present(location_data.profit_split_detail)){
            var split_detail = JSON.parse(location_data.profit_split_detail);
            $(split_detail["splits"]["ez_merchant"]).each(function (key, array) {
                array = array[0];
                var wallet_id = "";
                $.map(array, function (val, i) {
                    wallet_id = val.wallet
                });
                if(value_present(gon)){
                    wallets = gon.ez_merchant_data
                }
                $('#ezLocationDiv'+key).css('visibility', 'visible');
                var option = new Option("Choose Location", "");
                $(option).html("Choose Location");
                $('#ezLocationText'+key).css('visibility', 'visible').attr('name', "profit_splits[splits][ez_merchant]["+ key+"][ez_merchant_1][wallet]");
                $("#ezLocationText"+key).append(option);
                $(wallets).each(function (index, wallet) {
                    var option = new Option(wallet.name, wallet.location_id);
                    $(option).html(wallet.name);
                    $("#ezLocationText"+ key).append(option);
                })
                $('#ezLocationText'+ key + ' option[value='+wallet_id+']').prop("selected", "selected");
            });
        }
    }

//**************** net profit split js ****************************

    if(value_present(location_data) && location_data.net_profit_split){
        $('#net_profit_splits_on_section').show()
        $("#net_gbox_company_radio").prop("checked", true)
        $('#net_check').show()
    }
    else{
        $('#net_profit_splits_on_section').hide()
        $("#net_gbox_company_radio").prop("checked", false)
        $('#net_check').hide()
    };

    if(value_present(location_data) && location_data.net_profit_split == true){
        $('#net_check').show();
        if(value_present(location_data.profit_split_detail)){
            var split_detail = JSON.parse(location_data.profit_split_detail);
            var wallet_id = null;
            var name = "";
            $.map(split_detail["splits"], function (array, key){
                if(key == "net"){
                    $.map(array, function (val, i) {
                        wallet_id = val.wallet;
                        key = i;
                        name = val.name;
                        percent = val.percent;;
                    });
                    key_name = key.toString().replace(/[0-9]/g, '');
                    if(value_present(key_name)){
                        if(key_name.includes('merchant')){
                            wallets = gon.net_profit_locations;
                            $('#net_super_company_percent').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][percent]").val(percent);
                            $('#netCompanyInput').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][name]').val(name);
                            $('#netCompanyInputHidden').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][wallet]').val(wallet_id);
                            $('#net_gbox_company_radio').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][from]");
                            if(value_present(wallets)){
                                $('#netLocationDiv').css('visibility', 'visible');
                                var option = new Option("Choose Location", "");
                                $(option).html("Choose Location");
                                $('#netLocationText').css('visibility', 'visible').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][wallet]");
                                $("#netLocationText").append(option);
                                if(value_present(wallets)){
                                    $.map(wallets, function (wallet) {
                                        var option = new Option(wallet.name, wallet.location_id);
                                        $(option).html(wallet.name);
                                        $("#netLocationText").append(option);
                                    })
                                }
                                $('#netLocationText option[value='+wallet_id+']').prop("selected", "selected");
                            }
                        }else{
                            $('#net_super_company_percent').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][percent]").val(percent);
                            $('#netCompanyInput').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][name]').val(name);
                            $('#netCompanyInputHidden').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][wallet]').val(wallet_id);
                            $('#net_gbox_company_radio').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][from]");
                        }
                    }
                }
            });
        }
    }
    else {
        $('#net_check').hide();
    }
    // *********** End net profit split js ****************
});


