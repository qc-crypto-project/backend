$(function() {
    $(".two_decimal").on('input', function() {
        this.value = this.value.match(/\d{0,12}(\.\d{0,2})?/)[0];
    });
});

(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };
}(jQuery));
$(".percent").inputFilter(function(value) {
    return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100); });
$(".whole_number").inputFilter(function(value) {
    return /^-?\d*$/.test(value); });

$('.form-control.numeric.float.optional.buyrate_fields').on('change',function () {
    var id = $(this).attr('id');
    if ($(this).val() == 0) {
        $('#' + id + '_date').val('');
        $('#location-btn').attr("disabled", false);
    }
    else if($(this).val() != 0) {
        $('#'+id+'_date').val(moment(new Date).format("MM/DD/YYYY"));
    }
});
$('.buyrate_fields').bind('keyup change', function(){
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
function save_image(id){
    var file = $('#image_store')[0];
    if (file.value == "") {
        alert('Please select a file to upload')
        $("#test_submit").attr("disabled", true);
    } else {
//            $("#test_submit").attr("disabled", false);
        $('#image_upload').val(id);
        var form = $('#image_form_class2');
        var file_size = parseFloat((file.files[0].size / 1024) / 1024).toFixed(2);
        var ext = file.value.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert("Only 'Png', 'Jpeg', 'JPG' allowed");
        } else {
            if (file_size <= 4) {
            } else {
                alert('Please select a file less than 4MB')
            }
        }
    }
}
function delete_img(a){
    $.ajax({
        url:  "<%= admins_image_delete_path %>",
        type: 'GET',
        data: {id: a},
        success: function(data) {
            $('#img_up').hide();
            $('#img_form').show();
        },
        error: function(data){
            console.log(data);
        }
    });
}
$(document).ready(function () {
    $(".my-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password")
        {
            input.attr("type", "text");
        }
        else
        {
            input.attr("type", "password");
        }
    });
    $("#tax_id").mask("00-00000000");
});
$(document).on('click','.website_remove_field',function()
{
    $(this).parent().parent().remove();
});
$(document).on('click','.platform_remove_field',function()
{
    $(this).parent().parent().remove();
});
$('#j_btn').click(function ()
{
    $("#url-append").append('<div class="form-group col-md-12" style="padding: 0px !important;"> <div class="col-md-9" style="padding: 0px"> <input value="" name="location[web_site][]" class="form-control website_text_field" autocomplete="on" type="url" id="location_web_site"></div><div class="col-md-3"  style="margin-top: 2%;padding-left: 10px"><a class="website_remove_field"><i class="fa fa-minus-circle fa-2x"></i></a> </div></div>');
});
$('#platform_btn').click(function ()
{
    $("#platform-append").append('<div class="form-group col-md-12" style="padding: 0px !important;"> <div class="col-md-9" style="padding: 0px"><input type="text" name="location[ecom_platform][]" value="" class="form-control platform_text_field" autocomplete="on"></div><div class="col-md-3"  style="margin-top: 2%;padding-left: 10px"><a class="platform_remove_field"><i class="fa fa-minus-circle fa-2x"></i></a> </div></div>');
});

function verifyAdmin(){
    var password = $('#password').val();
    $.ajax({
        url: "<%= verify_admin_password_admins_buy_rate_index_path %>",
        method: 'get',
        data: {password: password},
        success:function(data){
            $("#password_btn").css("background","green").text('').text("Verified");
            $("#info-text").text('');
            setTimeout(function(){
                var x = $("#password-field")
                if (x.attr('type') === "password") {
                    x.attr('type','text')
                }
//                    $('#password').attr("disabled","disabled");
                $('#admin_confirm').modal('hide')
//                    $('#admin_confirm').removeClass('show').removeClass('in').attr('style','display:none')
//                    $('#verification_btn').hide();
//                    $('#submit_transfer_btn').show();
                $('#password').val("");
                $("#password_btn").css("background","#7867a7").text('').text("Verify");
            }, 1000);
        },
        error: function(data){
            $("#password_btn").css("border","1px solid red");
            $("#info-text").text('').css("color","red").text('Please try again!');
        }
    });
}
$('#password_btn').on('click',function () {
    verifyAdmin();
});
$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13 ) {
            if ( $('#admin_confirm').hasClass('in') ) { //Check if the modal is showing
                event.preventDefault();
                verifyAdmin();
                return false;
            }
        }
    });
});
function myFunction() {
    var x = $("#password-field")
    if (x.attr('type') === "password") {
        $('#admin_confirm').modal('show');
    } else {
        x.attr('type','password')
    }
}