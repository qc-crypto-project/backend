$("#query_dba_name_").select2({
    multiple: true,
    // ajax: {
    //     url: "/admins/reports/autocomplete_location",
    //     dataType: 'json',
    //     delay: 250,
    //     data: function (params) {
    //         return {
    //             q: params.term, // search term
    //             // page: params.page
    //         };
    //     },
    //     processResults: function (data, params) {
    //         return {
    //             results: data,
    //         };
    //     },
    //     cache: true
    // },
    // placeholder: 'Search for a location',
    // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    // minimumInputLength: 1,
    // templateResult: formatRepo,
    // templateSelection: formatRepoSelection
});
$("#query_m_names_").select2({
    multiple: true,
});
$("#query_m_ids_").select2({
    multiple: true,
});
$(document).ready(function() {
    $('.type1').multipleSelect({
        placeholder: "Select Type",
    });
    $('.categories').multipleSelect({
        placeholder: "Select Category",
    });
    $('.type').multipleSelect({
        placeholder: "Select Type",
    });
    $('.category').multipleSelect({
        placeholder: "Select Category",
    });
    $('.partner').multipleSelect({
        placeholder: "Select Partner",
    });
    $('.parameter').multipleSelect({
        placeholder: "Select Parameters",
    });
    $('.name').multipleSelect({
        placeholder: "Select Names",
    });
    $('.gateway').multipleSelect({
        placeholder: "Select Gateway",
    });

});
$("#query_dba_name_").select2({
    multiple: true,
    placeholder: "Select DBA"
});
$("#query_partner_name_").select2({
    multiple: true,
});
$("#query_iso_name_").select2({
    multiple: true,
    ajax: {
        url: "/admins/reports/autocomplete_user",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                type: "iso"
                // page: params.page
            };
        },
        processResults: function (data, params) {
            return {
                results: data,
            };
        },
        cache: true
    },
    placeholder: 'Search for ISO',
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
});

$("#query_user_name_").select2({
    multiple: true,
    required: true,
    ajax: {
        url: "/admins/reports/search_user",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            search_with=$('#search_with').val()
            return {
                q: params.term, // search term
                type: search_with
                // page: params.page
            };
        },
        processResults: function (data, params) {
            return {
                results: data,
            };
        },
        cache: true
    },

    placeholder: 'Search',
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
});
$("#query_location_name_").select2({
    multiple: true,
    ajax: {
        url: "/admins/reports/search_user",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                type: "category"
                // page: params.page
            };
        },
        processResults: function (data, params) {
            return {
                results: data,
            };
        },
        cache: true
    },
    placeholder: 'Search for Categories',
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
});


function formatRepo (repo) {
    if (repo.loading) {
        return repo.text;
    }

    var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__title'>" + repo.name + "</div>";

    return markup;
}

function formatRepoSelection (repo) {
    return repo.name
}


$("#affiliate_program").select2({
    multiple: true
});
$("#query_dba_name_.admin_user").on("change",function(e){
    var selected_admin_user = []
    $('#query_dba_name_.admin_user').on('select2:select', function (e) {
        var data = e.params.data;
        if(data.id == "all" && $("#query_dba_name_ option[value='all']").is(':selected')){
            $('#query_dba_name_ option').prop('selected', true);
            update_dropdown();
        }
    });


    $('#query_dba_name_.admin_user').on('select2:unselect', function (e) {
        var data = e.params.data;
        if(data.id == "all" && $("#query_dba_name_ option[value='all']").is(':selected') == false){
            $("#affiliate_program").empty()
            $(".select2-selection__rendered").empty()
            $('#query_dba_name_ option').prop('selected', false);
            update_dropdown();
        }
        if(data.id != "all"){
            $("#query_dba_name_ option[value='all']").prop("selected",false)
        }
    });
    update_dropdown();
});


$("#affiliate_program").on("change",function(e){
    
    $("#hidden_affiliate_name").val(JSON.stringify($("#affiliate_program").val()))
});

function update_dropdown(check){
    selected_admin_user = []
    $("#hidden_aff_dba_name").val(JSON.stringify($("#query_dba_name_").val()))
    $("#query_dba_name_ option:selected").each(function(index){
    if($("#query_dba_name_ option:selected")[index].value != "all"){
            selected_admin_user.push(JSON.parse($("#query_dba_name_ option:selected")[index].value)["id"])
        }
    });
    if(selected_admin_user != null && selected_admin_user.length != 0){
        not_transaction = false
        if($("input[name='query[aff_not_0]']").val() == "true" || check == true){
            not_transaction = true
            $("#hidden_aff_not_0").val("true")
        } 
        $.ajax({
            url:  "/admins/reports/autopopulate_affiliates",
            type: 'GET',
            data: {ids: selected_admin_user,not_transaction: not_transaction},
            success: function(data) {
                $("#affiliate_program").empty()
                $("#affiliate_program").append('<option value=all>All Affiliates</option>')
                $.each(data, function(){
                    $("#affiliate_program").append('<option value="'+ this.value +'">'+ this.name +'</option>')
                })
            },
            error: function(data){
                console.log(data);
            }
        })
    }else{
        $("#affiliate_program").empty()
    }
}

$("#affiliate_program_export").on("click",function(){
    if($("input[name='query[aff_not_0]']").val() == "true"){
        $("#hidden_aff_not_0").val('true')
    }else{
        $("#hidden_aff_not_0").val('')
    }
})

$(".affiliate_program_report").on("click",function(e){

    if(!$("#affiliate_report").valid()){
        e.preventDefault()
    }else{
        $('#reportModal').modal('show');
        $("#wallet_body_2").show();
    }
})
$(".dba_name_search").select2({
    multiple: true,
    ajax: {
        url: "/admins/reports/autocomplete_location",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                // page: params.page
            };
        },
        processResults: function (data, params) {
            return {
                results: data,
            };
        },
        cache: true
    },
    placeholder: 'Search for a location',
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo,
    templateSelection: formatRepoSelection
});
$("#export").removeClass('multi-hide');
