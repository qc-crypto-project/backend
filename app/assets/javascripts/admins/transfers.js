$(document).on('keyup keypress keydown click', 'input', function(e) {
    if(e.which == 13 ) {
        e.preventDefault();
        return false;
    }
});
$(document).on('submit', '#new_transfer', function(e) {
    if( $('#admin_verified').val() == "false") {
        e.preventDefault();
        return false;
    }
});
$(document).ready(function () {
    var table = $('#datatable-transfers_table').DataTable({
    responsive:false,
    "sorting": true,
    "searching": false,
//        "sScrollX": true,
    "bLengthChange": false,
    "bInfo":false,
        "ordering": true,
    "bPaginate": false,
        "order": [[1,'desc']]
});
    $('#alert-message-top').fadeOut(5000);
    $('#submit_transfer_btn').hide();
    $('#verification_box').hide();
    //...............
    var dateToday=new Date();
    $('#transfer_date').datepicker({
        autoUpdateInput: false,
        dateFormat: 'mm/dd/yy',
        minDate: new Date()
    })
//------------------------Memo Validation -----------------------
//     $("#memo").inputmask('Regex', {regex: "/^[ ]*[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]{1,64}@(?=.{10,20}$)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*[ ]*$/;\n"});
// --------------------------------------AUTOCOMPLETE NEW FUNCTION---------------------------------------------

    const urlParams = new URLSearchParams(window.location.search);
    const queryParam = urlParams.get('query');
    const dateParam = urlParams.get('date');
    if(queryParam != ""){
        if(dateParam == ""){
            $('input[name="query[date]"]').val('')
        }
    }
    else {
        $('input[name="query[date]"]').val('')
    }
            $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
            $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
                var startDate = picker.startDate;
                var endDate = picker.endDate;
                $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
            });


        $('input[name="query[date]"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'MM/DD/YYYY',
            },
            "parentEl": $("#date_parent")
        });

        $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
            $('input[name="query[date]"]').val('');
        });
    });
var from_wallet_id="";
var to_wallet_id="";
keyPress = (field) => {
    var e = jQuery.Event("keydown");
    e.ctrlkey = true;
    $(field).trigger(e);
}
$("#fromInput, #toInput").autocomplete({ src: [] }); // to initialize autocomplete on input fields(autocomplete bind with onkeyup event does not work for first time)
$(document).on('keyup','#fromInput',function(e){
    keyPress(this);
    // $('#fromInputHidden').val('');
    $("#source_available_amount").text('');
    from_wallet_id="";
    if(e.keyCode!=18 && e.keyCode!=38 && e.keyCode!=40 && e.keyCode!= 13 && e.keyCode!=9 && e.keyCode!=16 && e.keyCode!= 17 && e.keyCode!=37 && e.keyCode!=20 && e.keyCode!=39 && e.keyCode!=36 && e.keyCode!=35 && e.keyCode!=27 && e.keyCode!=144 && e.keyCode!=undefined){
        $("#fromInputHidden").val("");
    }
    get_users('fromInput',$('#fromParent'));
});
$(document).on('keyup',"#toInput",function(e){
    keyPress(this);
    // $('#toInputHidden').val('');
    $("#destination_available_amount").text('');
    to_wallet_id="";
    if(e.keyCode!=18 && e.keyCode!=38 && e.keyCode!=40 && e.keyCode!= 13 && e.keyCode!=9 && e.keyCode!=16 && e.keyCode!= 17 && e.keyCode!=37 && e.keyCode!=20 && e.keyCode!=39 && e.keyCode!=36 && e.keyCode!=35 && e.keyCode!=27 && e.keyCode!=144 && e.keyCode!=undefined){
        $("#toInputHidden").val("");
    }
    get_users('toInput',$('#toParent'));
});
//------------------------Amount -----------------------
$("#amount").inputmask('Regex', {regex: "^[0-9,]{1,99}(\\.\\d{1,2})?$"});
        $(document).on('keyup change','#amount',function (e) {
            var str =$('#amount').val();
            str=str.replace(/,/g, "")
            numberWithCommas(str)
        });
        $(document).on('click' , '#submit_transfer_btn',function () {
            $('#amount').val($('#amount').val().replace(/,/g, ""));
        });
function numberWithCommas(x) {
    $('#amount').val(x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

    $(document).on("click", "#transfer_details", function () {
        $("#transaction_id").text($(this).data('transaction_id'));
        $("#transfer_datee").text($(this).data('date'));
        getWalletName($(this).data('from'),$(this).data('to'));
        $("#transfer_memo").text($(this).data('memo'));

    });
    $(document).on('click','#transfer_new',function () {
        transfer_clear_form();
        $('#verification_btn').show()
        $('#submit_transfer_btn').hide()
        $('#password').prop('disabled', false)
        $('#admin_verified').val("false")
        $('#transfer_date').datepicker("setDate", new Date());
        $('#new_transfer').attr('action',"/admins/transfers/transfer_post")
    });
    $(document).on('click','.transfer_edit',function () {
        transfer_clear_form();
        var date = new Date($(this).data('date')+" 00:00:00");
        var from_wallet = $(this).data('from');
        var to_wallet = $(this).data('to');
        getUserDetails(from_wallet,'from');
        getUserDetails(to_wallet,'to');
        $('#transfer_id').val($(this).data('transfer_id'));
        $('#amount').val($(this).data('amount'));
        $('#memo').val($(this).data('memo'));
        $('#transfer_date').datepicker("setDate", date);
        $('#new_transfer').attr('action',"/merchant/locations/transfer_update")
    });

    $(document).on('click', '#submit_transfer_btn', function (e) {
    if ($("#fromInputHidden").val() == ""){
        $("#from_wallet_error").show();
        $("#from_wallet_error").text("Please select a valid user.");
        return false;
    }else if($("#toInputHidden").val() == "") {
        $("#to_wallet_error").show();
        $("#to_wallet_error").text("Please select a valid user.");
        return false;
    }
    })

//--------------------------------------GET WALLET NAME-----------------------
    function getWalletName(from_wallet,to_wallet)
    {
        $.ajax({
            url: "/admins/transfers/get_wallet_name",
            type: "GET",
            data: {from_wallet: from_wallet,to_wallet: to_wallet},
            success: function (data) {
                $("#transfer_sender").text(data.from_wallet);
                $("#transfer_receiver").text(data.to_wallet);
            },
            error: function (data) {
                response('')
            }
        });
    }
    $(document).on('change','.transferLocation',function(){
        var loc_id,type;
        loc_id=$(this).val();
        type=$(this).data('type');
        if(loc_id!="" && loc_id!=0){
            $.ajax({
                url: "/admins/transfers/get_location_wallets",
                type: "GET",
                data: {location_id: loc_id},
                success: function (data) {
                    if (data.wallets!=""){
                        $('#'+type+'Wallet').empty();
                        var new_option = new Option("Choose Wallet", '');
                        $(new_option).html('Choose Wallet');
                        $('#'+type+'Wallet').append(new_option);
                        data.wallets.forEach(function(wallet) {
                            var new_option = new Option(wallet.name, wallet.id);
                            $(new_option).html(wallet.name);
                            $('#'+type+'Wallet').append(new_option);
                            $('#'+type+'Wallet').attr('name','transfer['+type+'_wallet]');
                        });
                    }
                    else{
                        $('#'+type+'Wallet').empty();
                        $('#'+type+'Wallet').removeAttr('name');
                        alert('No wallets exist for this location');
                    }
                },
                error: function (data) {
                    $('#'+type+'Wallet').empty();
                    $('#'+type+'Wallet').removeAttr('name');
                    alert('No wallets exist for this location');
                }
            });
        }
    });
//  ----------------------------------Wallet Change Function------------------------
    $(document).on('change','.transferWallet',function(){
        var source=$(this).val();
        var type=$(this).data('type');
        $('#'+type+'_wallet_error').hide();
        var fromWallet = $("#fromWallet").val();
        var toWallet = $("#toWallet").val();
        if(type=="from"){
            if(toWallet == source){
                $("#fromWallet option[value='']").attr('selected', true);
                $('#from_wallet_error').text('Wallets Cannot Be Same');
                $('#from_wallet_error').show();
                $("#submit_transfer_btn").prop('disabled','disabled');
                $('#verification_btn').addClass('pointer-none');
                fromWallet=''
                return false;
            }
            else {
                $('#from_wallet_error').text('');
                $('#submit_transfer_btn').prop('disabled',false);
                $('#verification_btn').removeClass('pointer-none');

            }
            fromWallet=source;
        }else{
            if(fromWallet==source){
                $("#toWallet option[value='']").attr('selected', true);
                $('#to_wallet_error').text('Wallets Cannot Be Same');
                $('#to_wallet_error').show();
                $('#submit_transfer_btn').prop('disabled','disabled');
                $('#verification_btn').addClass('pointer-none');
                toWallet=''
                return false;
            }
            else
            {
                $('#submit_transfer_btn').prop('disabled',false);
                $('#verification_btn').removeClass('pointer-none');
                $('#to_wallet_error').text('');
            }
            toWallet=source;
        }
        if(source!='' || source!=null){
            showWalletBalance(source, type);
        }
    });
//  ---------------------------------Check Wallet Balance----------------------------
    function showWalletBalance(wallet_id, type){
        if (type=="from"){
            type='source';
        }else if(type=="to"){
            type="destination";
        }
        $.ajax({
            url: "/merchant/locations/show_merchant_balance",
            type: "GET",
            data: {source: wallet_id},
            success: function (data) {
                if (data.balance!=""){
                    $('#' + type + '_available_amount').text(data.balance);
                }else{
                    $('#' + type + '_available_amount').text('0.00');
                }
            },
            error: function (data) {
                $('#' + type + '_available_amount').text('0.00');
            }
        });
    }
//  ----------------------------------GET USER DETAILS FOR EDIT CASE------------------------------
    function getUserDetails(wallet_id,type){
        $.ajax({
            url: "/admins/transfers/get_user_details",
            type: "GET",
            data: {wallet_id: wallet_id},
            success: function (data) {
                if (data.user_type!=""){
                    if (data.user_type=="merchant"){
                        $('#'+type+'Location').prop('required',true);
                        $('#'+type+'Wallet').prop('required',true);
                        if(type=="from"){
                            $('#'+type+'MainDiv').show();
                            if($('#toMainDiv').css('display')!='block'){
                                $("#toMainDiv").css('visibility','hidden');
                                $("#toMainDiv").css('display','block');
                            }
                        }else if(type=="to"){
                            if($('#'+type+'MainDiv').css('display')=="none"){
                                $('#'+type+'MainDiv').css('display','block');
                                $('#'+type+'MainDiv').css('visibility','visible');
                            }else if($('#'+type+'MainDiv').css('display')=="block"){
                                $('#'+type+'MainDiv').css('visibility','visible');
                            }
                        }

                        $('#'+type+'Input').val(data.user_name);
//                      ------------------------Get User Wallets--------------------
                        if (data.locations!=""){
                            $('#'+type+'Location').empty();
                            var new_option = new Option("Choose Location", '');
                            $(new_option).html('Choose Location');
                            $('#'+type+'Location').append(new_option);
                            data.locations.forEach(function(location) {
                                var new_option = new Option(location.name, location.id);
                                $(new_option).html(location.name);
                                $('#'+type+'Location').append(new_option);
                            });
                            $('#'+type+'Location option[value="'+data.location_id+'"]').prop("selected", true);
                        }
                        if (data.wallets!=""){
                            if (type=="from"){
                                from_wallet_id=data.wallet_id;
                                showWalletBalance(data.wallet_id, 'source');
                            }
                            else {
                                to_wallet_id=data.wallet_id;
                                showWalletBalance(data.wallet_id, 'destination');
                            }
                            $('#'+type+'Wallet').empty();
                            var new_option = new Option("Choose Wallet", '');
                            $(new_option).html('Choose Wallet');
                            $('#'+type+'Wallet').append(new_option);
                            data.wallets.forEach(function(wallet) {
                                var new_option = new Option(wallet.name, wallet.id);
                                $(new_option).html(wallet.name);
                                $('#'+type+'Wallet').append(new_option);
                                $('#'+type+'Wallet').attr('name','transfer['+type+'_wallet]');
                            });
                            $('#'+type+'Wallet option[value="'+data.wallet_id+'"]').prop("selected", true);
                        }
                    }else{
                        $('#'+type+'MainDiv').hide();
                        if(type=="to"){
                            if($('#fromMainDiv').css('display')=="block"){
                                $('#'+type+'MainDiv').css('visibility','hidden');
                                $('#'+type+'MainDiv').css('display','block');
                            }
                        }
                        if(type=="from"){
                            from_wallet_id=data.wallet_id
                            showWalletBalance(data.wallet_id, 'source');
                        }
                        else {
                            to_wallet_id=data.wallet_id
                            showWalletBalance(data.wallet_id, 'destination');
                        }
                        $('#'+type+'Location').prop('required',false);
                        $('#'+type+'Wallet').prop('required',false);
                        $('#'+type+'Input').val(data.user_name);
                        $('#'+type+'InputHidden').val(wallet_id);
                        $('#'+type+'InputHidden').attr('name','transfer['+type+'_wallet]');
                    }
                }else{
                    alert('Request Error Occured Kindly Refresh Your Page')
                }
            },
            error: function (data) {
                $('#source_available_amount').text('0.00');
                $('#destination_available_amount').text('0.00');
            }
        });
    }

//    ----------------------------Cancel Modal Function Started-------------------
    $(document).on('click','#cancelModal',function(){
        $('#delete_transaction_btn').data('transfer_id',$(this).data('transfer_id'));
    });
    $(document).on('click','#delete_transaction_btn',function(){
        var transfer_id=$(this).data('transfer_id');
        delete_transfer(transfer_id);
    });
//    -----------Delete Transfer---------------------------
    function delete_transfer(transfer_id){
        $.ajax({
            url: "/merchant/locations/transfer_delete",
            type: "GET",
            data: {id: transfer_id},
            success: function (data) {
                if (data.status=="failed"){
                    alert('Cannot Delete Transfer');
                    $('#closeModal').modal('hide')
                }else{
                    $('#closeModal').modal('hide')
                    location.reload()
                }
            },
            error: function (data) {
                alert('Cannot Delete Transfer');
                $('#closeModal').modal('hide')
            }
        });
    }
//---------------Clear Form Function-----------------------
    function transfer_clear_form(){
        $('#fromInput').val('');
        $('#toInput').val('');
        $('#fromWallet').removeAttr('name');
        $('#toWallet').removeAttr('name');
        $('#fromInputHidden').removeAttr('name');
        $('#toInputHidden').removeAttr('name');
        $('#transfer_id').val('');
        $('#fromWallet').empty();
        $('#toWallet').empty();
        $('#fromLocation').empty();
        $('#toLocation').empty();
        $('#fromMainDiv').hide();
        $('#toMainDiv').css('visibility','hidden');
        $('#toMainDiv').hide();
        $('#amount').attr('placeholder','0.00');
        $('#amount').val('');
        $('#memo').val('');
        $('#fromInput').prop('required',true);
        $('#toInput').prop('required',true);
        $('#fromWallet').prop('required',false);
        $('#toWallet').prop('required',false);
        $('#source_available_amount').text('0.00');
        $('#destination_available_amount').text('0.00');
        $('#from_wallet_error').html('');
        $('#to_wallet_error').html('');
        $('#amount-error').html('');
        // $('#to_wallet_error').hide()
        // $('#from_wallet_error').hide()
        from_wallet_id=''
        to_wallet_id=''
    }
//    ---------------------------------END SEARCH FUNCTIONALITY-----------------------------------



    $(document).on('click','#verification_btn',function () {
        $('#verification_btn').hide();
        $('#verification_box').fadeIn("slow");
    });
    $(document).on('click','#password_btn',function () {
        var password = $('#password').val();
        $.ajax({
            url: "/admins/buy_rate/verify_admin_password",
            method: 'get',
            data: {password: password},
            success:function(data){
                $("#password_btn").css("background","green").text('').text("Verified");
                $("#info-text").text('');
                setTimeout(function(){
                    $('#admin_verified').val('true');
                    $('#password').attr("disabled","disabled");
                    $('#verification_box').fadeOut("slow");
                    $('#verification_btn').hide();
                    $('#submit_transfer_btn').attr("disabled",false);
                    $('#submit_transfer_btn').show();
                }, 1000);
            },
            error: function(data){
                $("#password_btn").css("border","1px solid red");
                $("#info-text").text('').css("color","red").text('Please try again!');
            }
        });
    });

    function get_users(field_id,parentID) {
        var field_value = $("#"+field_id).val();
        var id = '#'+$("#"+field_id).attr('id');
        var hidden_field = '#'+field_id+'Hidden';
        var error = '#'+field_id+'Error';
        if (field_value.length < 1 ){
            $(hidden_field).val('');
        }
        if(field_value != ''){
            getUsers(field_value,id,hidden_field,error,parentID);
        }
    }

    function getUsers(field_value,id,hidden_field,error,parentID){
        to_wallet_id = "";
        from_wallet_id = "";
        url = $(id).data('autocomplete');
        var type = $(id).data('type');
        var categoryArray=[];
        var autocomplete=$(id).autocomplete({
            source: url,
            appendTo: parentID,
            create: function () {
                categoryArray=[];
                $(id).data('ui-autocomplete')._renderItem = function (ul, item) {
                    if (categoryArray.includes(item.category)){
                        return $('<li class="ui-menu-item">')
                            .append("<div class='ui-menu-item-wrapper'>"+ item.label+ "</div>")
                            .appendTo(ul);
                    }else{
                        categoryArray.push(item.category);
                        return $('<li class="ui-menu-item">')
                            .append("<div class='aria-label'><strong style='margin-left: 5px'>"+ item.category+ "</strong></div>")
                            .append("<div class='ui-menu-item-wrapper'>"+ item.label+ "</div>")
                            .appendTo(ul);
                    }
                };
            },
            select: function(e,ui){
                $('#'+type+'_wallet_error').text("");
                $(hidden_field).val(ui.item.id);
                // Mantain Response In Categories
                if(ui.item.category=="Merchant"){
                    $('#'+type+'InputHidden').removeAttr('name');
                    $(id).prop('required',true);
                    $('#'+type+'Location').prop('required',true);
                    $('#'+type+'Wallet').prop('required',true);
                    $('#'+type+'MainDiv').show();
                    $('#'+type+'Location').empty();
                    $('#'+type+'Wallet').empty();
                    //  ---------DEFAULT OPTION FOR LOCATION AND WALLET--------------------------
                    var new_option = new Option("Choose Location", '');
                    $(new_option).html('Choose Location');
                    $('#'+type+'Location').append(new_option);
                    var new_wallet_option = new Option("Choose Wallet", '');
                    $(new_wallet_option).html('Choose Wallet');
                    $('#'+type+'Wallet').append(new_wallet_option);
                    //  --------Append Merchant Locations-----------------------------------------
                    $.ajax({
                        url: "/admins/transfers/get_merchant_location",
                        method: 'get',
                        data: {id: ui.item.id},
                        success: function (result) {
                            result.locations.forEach(function(location) {
                                var new_option = new Option(location.name, location.id);
                                $(new_option).html(location.name);
                                $('#'+type+'Location').append(new_option);
                            });
                        }
                    });
                    if(type=="from"){
                        $('#'+type+'MainDiv').addClass("display-block");
                        if($('#toMainDiv').css('display')!='block'){
                            $("#toMainDiv").css('visibility','hidden');
                            $("#toMainDiv").addClass("display-block");
                        }
                    }else if(type=="to"){
                        if($('#'+type+'MainDiv').css('display')=="none"){
                            $('#'+type+'MainDiv').addClass("display-block").removeClass("display-none");
                        }else if($('#'+type+'MainDiv').css('display')=="block"){
                            $('#'+type+'MainDiv').css('visibility','visible');
                        }
                    }
                }
                else {
                    $('#'+type+'InputHidden').removeAttr('name');
                    $('#'+type+'InputHidden').val('');
                    $('#'+type+'MainDiv').hide();
                    $(id).prop('required',true);
                    $('#'+type+'Location').prop('required',false);
                    $('#'+type+'Wallet').prop('required',false);
                    $('#'+type+'Wallet').removeAttr('name');
                    if(type=="from"){
                        from_wallet_id=ui.item.id;
                        to_wallet_id = $("#toInputHidden").val();
                        if(value_present(from_wallet_id) && to_wallet_id==from_wallet_id){
                            $('#from_wallet_error').text('Wallets Cannot Be Same');
                            $('#from_wallet_error').show();
                            $('#verification_btn').addClass('pointer-none');
                            $('#submit_transfer_btn').prop('disabled','disabled');
                            $('#fromInput').val('');
                            from_wallet_id=''
                            return false;
                        }else{
                            $('#from_wallet_error').hide();
                            $('#submit_transfer_btn').prop('disabled',false)
                            $('#verification_btn').removeClass('pointer-none');
                        }
                    }else{
                        to_wallet_id=ui.item.id;
                        from_wallet_id = $("#fromInputHidden").val();
                        if(value_present(to_wallet_id) && to_wallet_id==from_wallet_id){
                            $('#to_wallet_error').text('Wallets Cannot Be Same');
                            $('#to_wallet_error').show();
                            $('#submit_transfer_btn').prop('disabled','disabled');
                            $('#verification_btn').addClass('pointer-none');
                            $('#toInput').val('');
                            to_wallet_id=''
                            return false;
                        }else{
                            $('#to_wallet_error').hide();
                            $('#submit_transfer_btn').prop('disabled',false)
                            $('#verification_btn').removeClass('pointer-none');
                        }
                    }
                    if(type=="to"){
                        if($('#fromMainDiv').css('display')=="block"){
                            $('#'+type+'MainDiv').css('visibility','hidden');
                            $('#'+type+'MainDiv').css('display','block');
                        }
                    }
                    $('#'+type+'InputHidden').attr('name',"transfer["+type+"_wallet]");
                    $('#'+type+'InputHidden').val(ui.item.id);
                    if(type=="from"){
                        showWalletBalance(ui.item.id, 'source');
                    }
                    else {
                        showWalletBalance(ui.item.id, 'destination');
                    }
                }
            },
            response: function( event, ui ) {
                categoryArray=[];
            }
        })
    }


$(document).on('keyup','#amount',function(e){
    var amount = Number($('#amount').val().replace(/,/g, ''));
    var error = $('#wallet-error').text()
    var source_amount = ($('#source_available_amount').text());
    source_amount = source_amount.replace(/,/g, '');
    source_amount = Number(source_amount);
    if (e.which == 9 || e.code == 9)
    {
        e.preventDefault();
    }
    else
    {
        if (amount == '' && error == "")
        {
            $('#amount-error').html("");
            $('#submit_transfer_btn').prop('disabled',false);
            $('#verification_btn').removeClass('pointer-none');
            if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
            {
                $('#verification_btn').addClass('pointer-none');
                $('#submit_transfer_btn').prop('disabled','disabled');
            }
        }
        else if (amount <= 0)
        {
            $('#amount-error').html('Amount should be greater than zero');
            $('#amount-error').addClass('red');
            $('#submit_transfer_btn').prop('disabled','disabled');
            $('#verification_btn').addClass('pointer-none');

        }
        else if (amount > 0 && amount < source_amount && error=="")
        {
            $("#amount-error").html('');
            $('#submit_transfer_btn').prop('disabled',false);
            $('#verification_btn').removeClass('pointer-none');
            if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
            {
                $('#verification_btn').addClass('pointer-none');
                $('#submit_transfer_btn').prop('disabled','disabled');
            }
        }
        else if (amount > source_amount && $('#source_available_amount').text() != ''){
            $('#amount-error').html('Not enough balance in wallet');
            $('#amount-error').addClass('red');
            $('#submit_transfer_btn').prop('disabled','disabled');
            $('#verification_btn').addClass('pointer-none');
        }
        else
        {
            if(error == "") {
                $('#amount-error').html("");
                $('#submit_transfer_btn').prop('disabled', false);
                $('#verification_btn').removeClass('pointer-none');
                if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
                {
                    $('#verification_btn').addClass('pointer-none');
                    $('#submit_transfer_btn').prop('disabled','disabled');
                }
            }else
            {
                $('#submit_transfer_btn').prop('disabled', true);
            }
        }
    }
});
$(document).on('keyup','#toInput',function(e){
    if (e.which == 9 || e.code == 9)
    {
        e.preventDefault();
    }
    else
    {
        if ($(this).val() != '')
        {
            $('#to_wallet_error').html('Please select an existing user');
            $('#to_wallet_error').addClass('red');
            $('#submit_transfer_btn').prop('disabled',true);
            $('#verification_btn').addClass('pointer-none');
        }
        else
        {
            $('#to_wallet_error').html('');
            $('#submit_transfer_btn').prop('disabled',false);
            $('#verification_btn').removeClass('pointer-none');
            if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
            {
                $('#verification_btn').addClass('pointer-none');
                $('#submit_transfer_btn').prop('disabled','disabled');
            }
        }
    }
});
$(document).on('keyup','#fromInput',function(e){
    if (e.which == 9 || e.code == 9)
    {
        e.preventDefault();
    }
    else
    {
        if ($(this).val() != '')
        {
            $('#from_wallet_error').text('Please select an existing user');
            $('#from_wallet_error').addClass('red');
            $('#submit_transfer_btn').prop('disabled',true);
            $('#verification_btn').addClass('pointer-none');
        }
        else
        {
            $('#from_wallet_error').text('');
            $('#submit_transfer_btn').prop('disabled',false);
            $('#verification_btn').removeClass('pointer-none');
            if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
            {
                $('#verification_btn').addClass('pointer-none');
                $('#submit_transfer_btn').prop('disabled','disabled');
            }
        }
    }
});
$(document).on('click','.ui-menu-item-wrapper',function(){
    if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
    {
        $('#verification_btn').addClass('pointer-none');
        $('#submit_transfer_btn').prop('disabled','disabled');
    }
    var amount = Number($('#amount').val());
    var source_amount = ($('#source_available_amount').text());
    source_amount = source_amount.replace(/,/g, '');
    source_amount = Number(source_amount);
    if (source_amount == 0 && $("#toInput").val() == "")
    {
        $("#amount-error").html('');
        $('#submit_transfer_btn').prop('disabled',false);
        $('#verification_btn').removeClass('pointer-none');
        if ($('#amount-error').text() != '' || $('#from_wallet_error').text() != '' || $('#to_wallet_error').text() != '')
        {
            $('#verification_btn').addClass('pointer-none');
            $('#submit_transfer_btn').prop('disabled','disabled');
        }
    }
    if ($("#amount").val() <= 0 && $('#amount').val() != '')
    {
        $('#amount-error').html('Amount should be greater than zero');
        $('#amount-error').addClass('red');
        $('#submit_transfer_btn').prop('disabled','disabled');
        $('#verification_btn').addClass('pointer-none');

    }
});
