$(document).on('change','.update_seq_down_inputt',function () {
    var graph_val= $(this).data('graphValue');
    var id= $(this).data('id');
    update_withdraws(graph_val,id);
})
function update_withdraws(graph_val,id) {
    graph_value=graph_val
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        if (isConfirm.value === true) {
            $.ajax({
                url: "/admins/app_configs/update_accounting_setting",
                type: 'POST',
                data: { id: id,value: graph_val},
                success: function(data) {
                    window.location.reload()
                },
                error: function(error){
                    console.error('Update Issue Error: ', error);
                    swal("Cancelled", "An unexpected error occured :)", "error");
                }
            });
        } else {
            // $('#' + seq.seqDown.key).prop("checked", !$('#' + seq.seqDown.key).is(':checked'));
        }
    });
}