$('#alert-message-top').fadeOut(5000);
$(document).ready(function(){
    $(".balance_field").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
    $(document).on("change", "#tx_filter", function () {
        $("#user_transaction_table").submit();
    })
    $(document).on("change", "#tx_filter1", function () {
        $("#user_dispute").submit();
    })
    const urlParams = new URLSearchParams(window.location.search);
    $('.addcompany').click(function(){
        $('.myModal').modal();
    });
    $('.p').click(function () {
        tabDetailds($(this).data("id"));
    });
    $('#modal-69').on('shown.bs.modal', function () {
        $('#balanceField_id').addClass('bg-white');
    });
    function tabDetailds(id) {
        $.ajax({
            url: "/admins/companies" + id,
            type: 'PUT',
            data: {id: id},
            success: function (data) {
                console.log(data);
                $("#balance_section").append(data);
                $("#modal-69").modal();
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    $('.dt-buttons').css('display','none')
    $('#alert-message-top').fadeOut(5000);
    $('#search').hide();

    $('#click').click(function(){
        tabDetailds($(this).data("id"));
    });

    validateUserForm('edit');

    $(".pincode_div").hide();
    $(".error-phone").hide();
    $(".error-pincode").hide();
    $("#phone_number_button").on('click', function () {
        var phone_number=$('#phone_number').val();
        var wallet_id=$('#wallet_id').val();
        if(phone_number.length > 0) {
            $.ajax({
                url: "/admins/buy_rate/verify_admin",
                method: 'get',
                data: {phone_number: phone_number, wallet_id: wallet_id},
                success: function () {
                    $('#phone_number_button').html('Verified').removeClass('btn-primary').addClass('btn-success');
                    setTimeout(function () {
                        $('#phone_number').css('display', 'none').fadeOut("slow");
                        $('.phone_number_div').hide();
                        $('.pincode_div').show();
                        $("#modal-label-69").html('').html('Enter Pin Code You Recieved');
                    }, 1000);
                }, error: function (result) {
                    $('#phone_number_button').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger');
                    setTimeout(function () {
                        $('#phone_number_button').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                    }, 1500);
                    $(".error-phone").show();
                }
            });
        }else{
        }
    });
    $(document).on('change','#filter',function () {
        $("#user1_form").submit();
    })
    $("#pincode_button").on('click', function () {
        var pincode=$('#pincode').val();
        var wallet_id=$('#wallet_id').val();
        if(pincode.length > 0) {
            $.ajax({
                url: "/admins/buy_rate/verify_admin",
                method: 'get',
                data: {pincode: pincode, wallet_id: wallet_id},
                success: function () {
                    setTimeout(function () {
                        $('#pincode_button').val('Verified').removeClass('btn-primary').addClass('btn-success').attr("disabled",true);
                        $('#btn_retire').show();
                        $('#alert-now').show();
                        $('.error-pincode').hide();
                    }, 1000);
                }, error: function (result) {
                    $('#pincode_button').html('UnAuthorized').removeClass('btn-primary').addClass('btn-danger');
                    setTimeout(function () {
                        $('#pincode_button').html('Submit').removeClass('btn-danger').addClass('btn-primary');
                    }, 1500);
                    $(".error-pincode").show();
                }
            });
        }else{
        }
    });
    $('#cover-spin').fadeOut();
    if((parseFloat($("#blnc").val())) <= 0){
        $("#submit_transfer_btn").attr("disabled", "disabled");
    }
    $(document).on("change", '.retire-all-check', function(){
        if($(this).prop("checked")){
            $("#balanceField_id").prop("disabled", "disabled");
            $(".balance_field").val('').val($("#blnc").val());
        }
        else{
            $("#balanceField_id").prop("disabled", false);
            $(".balance_field").val('');
        }
    });
    $(document).on("keyup",  "#balanceField_id", function (e) {
        if(parseFloat(this.value) <= 0){
            $('#amount_limit_error').html('').text("Amount must be greater than 0");
            $("#submit_transfer_btn").prop("disabled", true);
            // $("#verification_btn").prop("disabled", true);
        }

        if(parseFloat(this.value) > parseFloat($("#blnc").val())){
            $('#amount_limit_error').html('').text("Insufficient Balance");
            $("#submit_transfer_btn").prop("disabled", true);
            // $("#verification_btn").prop("disabled", true);
        }
        else{
            $('#amount_limit_error').html('').text("");
            $("#submit_transfer_btn").prop("disabled", false);
        }
    });
    $('#btn_retire').hide();

    $('#btn_retire_duplicate').on('click',function () {
        $(this).hide();
        $('.hello_bbb').fadeIn("slow");
    });
    $('#login_verification_btn').on('click',function () {
        var password = $("input:password").val();
        $.ajax({
            url: "/admins/buy_rate/verify_admin",
            method: 'get',
            data: {password: password},
            success:function(data){
                $("#login_verification_btn").css("background","green").text('').text("Verified");
                $("#info-text").text('');
                console.log("asd");
                setTimeout(function(){
                    $('#password').attr("disabled","disabled");
                    $('.hello_bbb').fadeOut("slow");
                    $('#open_login_btn').hide();
                    $('#btn_retire').show();
                }, 1000);
            },
            error: function(data){
                $("#login_verification_btn").css("border","1px solid red");
                $("#info-text").text('').css("color","red").text('Please try again!');
            }
        });
    });


    $('#click').click(function(){
        console.log('click');
        tabDetailds($(this).data("id"));

    });
    $(".padd").css("margin","-15px 20px 20px 0px");
    var user_type = urlParams.get("user");
    $('#'+user_type+'-datatable').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": false,
        "searching": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "order": [[ 0, "desc" ]]
    });

    var globalTimeout = null;
    var filter = urlParams.get("filter");
    $('#srch-term').on('keyup change',function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var search_value = $('#srch-term').val();
            $('#q').val(search_value);
            var filter_data = "";
            if(filter != ""){
                filter_data = filter;
            }else{
                filter_data = 10;
            }
            user_data = user_type;
            $('#user_data').val(user_data);
            var user_param_type = user_type != "" ? user_type : "customer";
            var archived = $('#archived-user-value').val();
            $.ajax({
                url: '/admins/users?user='+user_param_type,
                type: 'GET',
                data: {q: search_value,filter: filter_data,user: user_data,search_input:"search_input", archived: archived}
            });

        }, 1500);
    });

    // admins/companies/_company
    $('#ledger_checkbox').on('click', function () {
        if($(this).prop("checked") == true){
            $('#ledger_fields').show();
        }
        else if($(this).prop("checked") == false){
            $('#ledger_fields').hide();
        }
    })

    $('#click').click(function(){
        console.log('click');
        tabDetailds($(this).data("id"));

    });

    $(document).on("click",'.show-cover-spin', function () {
        $('#cover-spin').show(0);
    })

    $("#pincode_button").on('click', function () {
        submitpincode()
    })
    //..............................._local_user_transactions_table.html.erb............................//
    // $( "#datatable-keytable tr" ).click(function(){
    //     var link  = $(this).data("href");
    //     setTimeout(function() {
    //         $('#cover-spin').fadeOut("slow");
    //     }, 15000 );
    //     $.ajax({
    //         url: link,
    //         type: "post"
    //     });
    // });
    var lt= $('.timeUtc-user').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'MM-DD-YYYY HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    var user_id = urlParams.get("user_id")
    if(user_id != ""){
        $('.dataTable').css('width','1339px')
        $('#datatable-keytable').css('width','1338px')
    }
    // $(document).on('click','.local_user_transaction',function () {
    //     $('#cover-spin').show(0)
    // })
    //.........................................dispute_transaction.html.erb....................//
    var table = $('#datatable-keytable-disputes').DataTable({
        responsive: false,
        sScrollX: false,
        "searching": false,
        "sSearch": false,
        "paging":   false,
        "columnDefs": [
            { "orderable": false, "targets": 7 }
        ],
        "order": [ 5, "desc" ]
    });
    // $('.pen_btn').on('click', function () {
    //     $('.cover').show(0);
    // });
    $( '#check_box_filters input').on( 'change', function () {
        if($(this).checked){
            console.log("its checked",this.value);
        }
    });
    //...............................................user_transactions.html.erb..................//
    var phone = $('#phone_number_field1').val();
    var email = $('#email_field1').val();
    function validate_edit_email() {
        if((email != $('#email_field1').val()) && ($('#email_field1').length > 0 ) ) {
            var url = "/admins/companies/form_validate?email_address=" + ($('#email_field1').val());
            if($(".i_am_location").val().length > 0){
                var url = "/admins/companies/form_validate?email_address=" + ($('#email_field1').val()) + "&location=iamlocation";
            }
            $.ajax({
                url: url,
                type: 'GET',
                data: {user_id: $('#company_hidden_field').val()},
                success: function (data) {
                    $('#email_field1').css('border', '')
                    $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
                    if (($("#phone_number_label_field1").text() == "Phone")) {
                        $('#submit_button').prop('disabled', false);
                    }
                    if ($('#email_field1')) {
                        if (data['value'] == true) {
                            $('#email_label_field1').replaceWith("<label id='email_label_field1' class='string optional' for='Email'	>Email<span style='color:red;'> This email has already been registered. Try again.</span></label>")
                            $('#email_field1').css('border', '1px solid red');
                            $('#submit_button').prop('disabled', true);
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }else{
            $('#email_field1').css('border', '')
            $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
            if (($("#phone_number_label_field1").text() == "Phone number") && ($('#password_field1').text() == "Confirm Password")) {
                $('#submit_button').prop('disabled', false);
            }
        }
    }
    function validate_phone_edit_number() {
        if((phone != $('#phone_number_field1').val()) && ($('#phone_number_field1').length > 0 ) ) {
            var url = "/admins/companies/form_validate?phone_number=" + ($('#phone_number_field1').val());
            if($(".i_am_location").val().length > 0){
                var url = "/admins/companies/form_validate?phone_number=" + ($('#phone_number_field1').val()) + "&location=iamlocation";
            }
            $.ajax({
                url: url,
                type: 'GET',
                data: {user_id: $('#company_hidden_field').val()},
                success: function (data) {
                    $('#phone_number_field1').css('border', '')
                    $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='Phone number'>Phone</label>")
                    if ($("#email_label_field1").text() == "Email") {
                        $('#submit_button').prop('disabled', false);
                    }
                    if ($('#phone_number_field1')) {
                        if (data['value'] == true) {
                            $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='Phone number'>Phone <span style='color:red'>  This phone number has already been registered. Try again.</span></label>")
                            $('#phone_number_field1').css('border', '1px solid red');
                            $('#submit_button').prop('disabled', true);
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }else{
            $('#phone_number_field1').css('border', '')
            $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='company_Phone number'>Phone number</label>")
            if (($("#email_label_field1").text() == "Email") && ($('#password_field1').text() == "Confirm Password")) {
                $('#submit_button').prop('disabled', false);
            }
        }
    }
    $('.next_transactions').click(function () {
        $('#cover-spin').fadeIn();
    });

    $(document).on("click", ".refundModalDialog", function () {
        $('#reason_select option[value="0"]').prop("selected",true);
        $('#partialamount').val('');
        $('#reason_detail').val('');
        $('#reason_detail').hide();
        $('.refund_fee').text('0');
        $('.modal-footer a button').prop('disabled',true);
        transaction_detail=JSON.stringify($(this).data('transaction'));
        parsed_transction=JSON.parse(transaction_detail);
        tran_Id = parsed_transction.id;
        parent_id=parsed_transction.parent_id;
        destination=parsed_transction.receiver_wallet_id;
        source=parsed_transction.sender_wallet_id;
        $.ajax({
            url: '/admins/refund_check',
            type: 'GET',
            data: {trans_id: tran_Id},
        })
            .done(function () {
                $.ajax({
                    url: '/admins/get_partialAmount',
                    type: 'GET',
                    data: {trans_id: tran_Id,parent_id: parent_id,destination: destination,source: source},
                })
                    .done(function (res) {
                        if(res.partial_amount==""){
                            $('#partial_error').text("No Transaction Found");
                            $('#partial_error').fadeIn();
                            setTimeout(function(){
                                $('#partial_error').fadeOut()
                            }, 2000);
                        }else{
                            $('#partialamount').val(parseFloat(res.partial_amount).toFixed(2));
                            total_amount=parseFloat(res.partial_amount);
                            partial_amt=total_amount;
                            last4="";
                            if(parsed_transction["tags"]["card"]){
                                if(parsed_transction["tags"]["card"]["last4"]){
                                    last4=parsed_transction["tags"]["card"]["last4"];
                                }
                            }else{
                                if(parsed_transction["tags"]["previous_issue"]){
                                    if(parsed_transction["tags"]["previous_issue"]["last4"]){
                                        last4=parsed_transction["tags"]["previous_issue"]["last4"];
                                    }
                                }
                            }
                            $('#refund_user').text(last4);
                            refund_fee = res.fee;
                            var fee = refund_fee + parseFloat(res.partial_amount);
                            $('.refund_fee').text(parseFloat(fee).toFixed(2));
                            $('#confirm_refund_amount').text(parseFloat(res.partial_amount).toFixed(2));
                        }
                    });
            });

    });
    $( "#transaction tr" ).click(function(){
        var link  = $(this).data("href");
        setTimeout(function() {
            $('#cover-spin').fadeOut("slow");
        }, 15000 );
        $.ajax({
            url: link,
            type: "get"
        });
    });
    $('#partialamount').change(function(){
        select_reason=$('#reason_select').val();
        var partial_amount = $('#partialamount').val();
        partial_amt=partial_amount;
        if (partial_amount != "0") {
            $.ajax({
                url: '/admins/get_partialAmount',
                type: 'GET',
                data: {trans_id: tran_Id,destination: destination,source: source},
            })
                .done(function (res) {
                    var partial= res.partial_amount - parseFloat(partial_amount);
                    if (partial < 0){
                        $('#partial_error').text("Amount is greater than amount to refund");
                        $('#partial_error').fadeIn();
                        setTimeout(function(){
                            $('#partial_error').fadeOut()
                        }, 2000);

                        $('.modal-footer a button').prop('disabled',true);
                        check1 = false;
                    }
                    else {
                        $('#partial_error').text("");
                        check1  =true;
                        refund_fee=res.fee;
                        var fee = refund_fee+parseFloat(partial_amount);
                        $('.refund_fee').text(parseFloat(fee).toFixed(2));
                        $('#confirm_refund_amount').text(parseFloat(partial_amount).toFixed(2));
                    }
                    if (check1 && check2) {
                        if(select_reason!=0){
                            $('.modal-footer a button').prop('disabled',false);
                        }
                    }
                });
        }
        updateRefundPostLink();
    });

    function updateRefundPostLink() {
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        reason_detail=$("#reason_select option:selected").text();
        if (select_reason==0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false;
        } else if (select_reason!=4 && select_reason!=0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('#footerConfirmLink').attr('href',"/admins/refund_transaction"+"?js=js&refund_fee="+refund_fee+"&reason=" + select_reason + "&partial_amount=" + partial_amount + "&reason_detail=" + encodeURIComponent(reason_detail) + "&transaction=" + tran_Id);
            if(total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
            check2 = true;
        } else{
            $('#reason_detail').show();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false
        }
        if (check1 && check2) {
            if (total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
        }
    }

    $('#reason_select').change(function(){
        updateRefundPostLink();
    });

    $('#reason_detail').keyup(function () {
        reason_detail=$(this).val();
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        if (reason_detail=='' || reason_detail == null){
            $('.modal-footer a button').prop('disabled', true);
        } else{
            $('#footerConfirmLink').attr('href',"/admins/refund_transaction"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + tran_Id);
            if(total_amount >= partial_amount){
                $('#refundModalFooter a button').prop('disabled',false);
            }
        }
    });

    $("#export_class").on('click', function () {
        var nowDate = new Date();
        var newdate = (nowDate.setDate(nowDate.getDate()));
        var nd = new Date(newdate);
        $("#exportDate").daterangepicker({
            "maxSpan": {
                "days":90
            },
            "startDate": nd,
            "maxDate": nd
        });
    })

    $("#name").inputmask('Regex', {regex: "^[A-Za-z0-9 ]*[A-Za-z0-9][A-Za-z0-9 ]*$"});
    $(".name").inputmask('Regex', {regex: "^[A-Za-z0-9 ]*[A-Za-z0-9][A-Za-z0-9 ]*$"});
    $(".amount_field").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
    $(".card_number").inputmask('Regex', {regex: "^[0-9]+$"});
    $('input[name="q[timestamp]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="q[timestamp]"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
    $('.datepicker').daterangepicker({
        parentEl: '#date_parent',
        "maxSpan": {"month": 1},
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
    $(function() {
        $('#q_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false
        });
        $('#q_gateway_').multipleSelect({
            placeholder: "Select Gateway",
            selectAll: false
        });
        $('#q_merchant_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false


        });
    });
});

$(document).on('focusin','#user_password',function () {
    $('.strength').css('display', "block");
});

$('.auto_off').on('focus',function(){
    $(this).attr('autocomplete', 'off');
});
$('.auto_off').attr('readonly', 'readonly');
$("input[readonly]").css("background-color", "white");
$('.auto_off').click(function () {
    $('.auto_off').removeAttr('readonly');
});
$('.auto_off').prop('autocomplete', Math.random().toString(36).slice(2));

$(document).on('focusout','#user_password',function () {
    if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
        $('.strength').css('display', "none");
    }
});
$(document).on("keyup","#user_password", function () {
    var password = $(this).val();
    $("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" class="margin-right-10px" type="checkbox">8-20 characters long</li><li class="list-group-item"><input  class="margin-right-10px" id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input class="margin-right-10px" id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input class="margin-right-10px" id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
    if (password.length >= 8 && password.length <= 20) {
        $('#length').attr('checked', true);
    } else {
        $('#length').attr('checked', false);
    }
    if (/[0-9]/.test(password)) {
        $('#one_number').attr('checked', true);
    } else {
        $('#one_number').attr('checked', false);
    }
    if (/[A-Z]/.test(password)) {
        $('#upper_case').attr('checked', true);
    } else {
        $('#upper_case').attr('checked', false);
    }
    if (/[a-z]/.test(password)) {
        $('#lower_case').attr('checked', true);
    } else {
        $('#lower_case').attr('checked', false);
    }
});

function tabDetailds(id) {
    $.ajax({
        url: '/admins/show_balance',
        type: 'GET',
        data: {user_id: id},
        success: function(data) {
            console.log(data);
            $(".in").append(data).after( $("#modal-68").modal());
            $("#modal-68").modal();

        },
        error: function(data){
            console.log(data);
        }
    });

}

function tabDetailds(id) {
    $.ajax({
        url: '/admins/show_balance',
        type: 'GET',
        data: {user_id: id},
        success: function(data) {
            console.log(data);
            $(".in").append(data).after( $("#modal-68").modal());
            $("#modal-68").modal();

        },
        error: function(data){
            console.log(data);
        }
    });

}

function submitpincode(){
    document.getElementById("pincode_button").disabled = true;
}

/*---------------------------------------- retire_balance.html.erb  -------------------------*/
// $(document).on('click','#verification_btn',function () {
//     // $('#verification_btn').hide();
//     $('#verification_box').fadeIn("slow");
// });
$(document).on('click','#password_btn1',function () {
    var password = $('#password').val();
    $.ajax({
        url: "/admins/buy_rate/verify_admin_password",
        method: 'get',
        data: {password: password},
        success:function(data){
            $("#password_btn1").css("background","green").text('').text("Verified");
            $("#info-text").text('');
            setTimeout(function(){
                $('#password').attr("disabled","disabled");
                $('#verification_box').fadeOut("slow");
                $('#submit_transfer_btn').show();
                $('#verification_btn').hide();
            }, 1000);
        },
        error: function(data){
            $("#password_btn1").css("border","1px solid red");
            $("#info-text").text('').css("color","red").text('Please try again!');
        }
    });
});
var check = true;
$(document).on('click','#verification_btn',function(e){
    var amountValue = $('#balanceField_id').val();
    if (amountValue == '')
    {
        e.preventDefault();
        $('#amount_limit_error').html('').text("Amount must be greater then 0");
    }
    else if (amountValue)
    {
        e.preventDefault();
        $('#verification_box').fadeIn("slow");
        // $('#verification_box').show();
        check = false;
    }
});

function space_remove(code_number, id) {
    if(code_number.length==1) {
        $('#'+id).addClass("phone-length-1");
    } else if(code_number.length==2) {
        $('#'+id).addClass('phone-length-2');
    } else if(code_number.length==3) {
        $('#'+id).addClass('phone-length-3');
    } else {
        $('#'+id).addClass('phone-length-4');
    }
}
$(document).on("keyup", "#phone_number_cor", function (){
    $("#phone_number_cor").inputmask('Regex', {regex: "^[0-9+]+$"});
    var phone = $("#phone_number_cor").val();
    var phone_code = $("#phone_code_cor").val();
    var reg = new RegExp('^[0-9]+$');
    // phone_code = phone_code.substring(1);
    var phone_number = phone_code + phone;
    var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
    if(phone.length > 0){
        $(".phone-error").text('');
        if(phone_number.match(reg)) {
            $.ajax({
                url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val() + "&role=greenbox_user",
                type: 'GET',
                data: {phone_number: phone_number},
                success: function (result) {
                    if(result){
                        $(".phone-error").text('')
                        $('.submit_button').prop('disabled', false);
                    }else{
                        $('.submit_button').prop('disabled', 'disabled');
                        if ($(".phone-error").text() == "Please enter Phone Number"){
                            $(".phone-error").text('');
                            $(".phone-error").text('Please enter Phone Number');
                        }else{
                            $(".phone-error").text('Invalid format or phone number is already taken!')
                        }
                    }
                },
                error: function (error) {
                    console.log(error)
                }
            });
        }else{
            $('.submit_button').prop('disabled', 'disabled');
            $(".phone-error").text('');
            $(".phone-error").text('Must be Number')
        }
    }else{
        $('.submit_button').prop('disabled', 'disabled');
        $(".phone-error").text('');
        $(".phone-error").text('Please enter Phone Number');
    }
});
$('#greenbox_user').on('hidden.bs.modal', function () {
    setTimeout(function () {
        $(".in").empty();
    },200);
});

$(document).on('click', "#search_btn", function () {
    $("#offset_new").val(moment().local().format('Z'));
});

$(document).on('change','#tx_filter',function () {
    $('#user_form_search').submit();
});