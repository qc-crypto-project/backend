$(document).ready(function(){
    $(function() {
        if($("#q_params").length > 0){
            if($("#q_params").val().length < 0){
                $('input[name="q[batch_date]"]').val('');
            }
        }
        $('input[name="q[batch_date]"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('input[name="q[batch_date]"]').on('apply.daterangepicker', function(ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            $('input[name="q[batch_date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
        });
    });
    $('.datepicker').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY'
        }
    });

    $('input[name="q[batch_date]"]').on('hide.daterangepicker', function (ev, picker) {
        $('input[name="q[batch_date]"]').val('');
    });

    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    var globalTimeout = null;
    $('#search').on('keyup', function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function () {
            globalTimeout = null;
            var search_value = $('#search').val();
            var batch_id = $('#batch_id').val();
            var filter = $('#filter').val();
            var batch_type = $('#batch_type').val();
            $.ajax({
                url: "/admins/sales/batch_detail",
                type: "GET",
                data: {q: search_value, filter: filter, id: batch_id, type: batch_type}
            })
        }, 1500);
    });
});