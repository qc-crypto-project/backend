$("#merchant_url").on('click', function () {
    var token = $(this).data('transaction-id');
    $.ajax({
        url: "/transactions/status",
        dataType: "json",
        data: {token: token}
    })
});