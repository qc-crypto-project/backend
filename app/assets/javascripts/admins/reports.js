// $(document).ready(function () {
$('#alert-message-top').fadeOut(5000);
$(document).on('click','.pagy_button',function () {
    $("#cover-spin").fadeIn();
})
$(function() {
    $(".run_report").on('click', function () {
        $("div.export-status").text("Running report, few seconds remaining....");
        $("#report_cancel").show();
        $('#report_body_2').show();
        // $('#report_body_2').hide();
        $('#report_body_3').hide();
        $('#report_body_4').hide();
        $("#report_done_btn").hide();
    });
    $(".withdraw-submit-btn").on('click', function () {
        $(".withdraw-export-btn").prop("disabled", false);
    });
    $(".summary_report_submit").on('click', function () {
        $("#summary_report_withdrawal").val("summary_report");
        $("#detail_report_withdrawal").val("");
        $(".export-btn").prop("disabled", false);
        $(".summary-btn").prop("disabled", false);
        $("#tx_filter_withdrawal").val($(".withdrawal-report-filter").val())
    });
    $(".detail_report_submit").on('click', function () {
        $("#summary_report_withdrawal").val("");
        $("#detail_report_withdrawal").val("detail_report");
        $(".export-btn").prop("disabled", false);
        $(".summary-btn").prop("disabled", false);
        $("#tx_filter_withdrawal").val($(".withdrawal-report-filter").val())
    });
    $(".merchant_run_report").on('click',function () {
        $("#offset_merchant").val(moment().local().format('Z'));
        $(".merchant_body_2").show();
    });
    //..............................chargeback.html.erb................///

        $("#date").daterangepicker({
           // autoUpdateInput: false,
            "maxSpan": {
                "days": 60
            }
        });
    $(".report_submit_cb").on('click', function (e) {
        if($("#query_parameters_").val() == null) {
            $("#param_error").show().text("Please select atleast one parameter")
        }
        else {
            $("#param_error").hide().text("")
        }
        if($("#query_dba_name_").val() == null && $("#query_gateway_").val() == null && $("#dba_radio").prop("checked") == false){
            $("#location_error").show().text("Please select location or gateway")
        }
        if($("#query_parameters_").val() != null && ($("#query_dba_name_").val() != null || $("#query_gateway_").val() != null || $("#dba_radio").is(':checked'))) {
            $('#cover-spin').show(0);
            $("#location_error").show().text("")
        }
    })
    $("#query_dba_name_").on("change", function () {
        if($(this).val() != null) {
            $("#query_gateway_,.gateway").attr("disabled", 'disabled');
            $(".gateway .ms-choice").attr("disabled", "disabled");
            $("#query_m_ids_,.id").attr("disabled", 'disabled');
            $(".id .ms-choice").attr("disabled", 'disabled');
            $("#query_m_names_,.name").attr("disabled", 'disabled');
            $(".name .ms-choice").attr("disabled", 'disabled');
        }  else {
            $("#query_gateway_,.gateway").attr("disabled", false);
            $(".gateway .ms-choice").attr("disabled", false);
            $("#query_m_ids_,.id").attr("disabled", false);
            $(".id .ms-choice").attr("disabled", false);
            $("#query_m_names_,.name").attr("disabled", false);
            $(".name .ms-choice").attr("disabled", false);
        }
    });
    $("#query_gateway_").on("change", function () {
        if($(this).val() != null) {
            $("#query_dba_name_").attr("disabled","disabled");
            $("#query_m_ids_,.id").attr("disabled", 'disabled');
            $(".id .ms-choice").attr("disabled", 'disabled');
            $("#query_m_names_,.name").attr("disabled", 'disabled');
            $(".name .ms-choice").attr("disabled", 'disabled');
            // $("#dba_radio").attr("disabled", "disabled");
            // $(".db_class .checkmark").css("background-color", "grey");
        }  else {
            $("#query_dba_name_").attr("disabled",false);
            $("#query_m_ids_,.id").attr("disabled", false);
            $(".id .ms-choice").attr("disabled", false);
            $("#query_m_names_,.name").attr("disabled", false);
            $(".name .ms-choice").attr("disabled", false);
            // $("#dba_radio").attr("disabled", false);
            // $(".db_class .checkmark").css("background-color", "");
        }
    });
    $("#query_m_ids_").on("change", function () {
        if($(this).val() != null) {
            $("#query_gateway_,.gateway").attr("disabled", 'disabled');
            $(".gateway .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled","disabled");
            $("#query_m_names_,.name").attr("disabled", 'disabled');
            $(".name .ms-choice").attr("disabled", 'disabled');
            // $("#dba_radio").attr("disabled", "disabled");
            // $(".db_class .checkmark").css("background-color", "grey");
        }  else {
            $("#query_gateway_,.gateway").attr("disabled", false);
            $(".gateway .ms-choice").attr("disabled", false);
            $("#query_dba_name_").attr("disabled",false);
            $("#query_m_names_,.name").attr("disabled", false);
            $(".name .ms-choice").attr("disabled", false);
            // $("#dba_radio").attr("disabled", false);
            // $(".db_class .checkmark").css("background-color", "");
        }
    });
    $("#query_m_names_").on("change", function () {
        if($(this).val() != null) {
            $("#query_gateway_,.gateway").attr("disabled", 'disabled');
            $(".gateway .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled","disabled");
            $("#query_m_ids_,.id").attr("disabled", 'disabled');
            $(".id .ms-choice").attr("disabled", 'disabled');
            // $("#dba_radio").attr("disabled", "disabled");
            // $(".db_class .checkmark").css("background-color", "grey");
        }  else {
            $("#query_gateway_,.gateway").attr("disabled", false);
            $(".gateway .ms-choice").attr("disabled", false);
            $("#query_dba_name_").attr("disabled",false);
            $("#query_m_ids_,.id").attr("disabled", false);
            $(".id .ms-choice").attr("disabled", false);
            // $("#dba_radio").attr("disabled", false);
            // $(".db_class .checkmark").css("background-color", "");
        }
    });
    // $("#dba_radio").on('click',function () {
    //     if($(this).is(':checked')) {
    //         $("#query_gateway_,.gateway").attr("disabled", 'disabled');
    //         $(".gateway .ms-choice").attr("disabled", "disabled");
    //         $("#query_dba_name_").attr("disabled","disabled");
    //     }
    //     else{
    //         $("#query_gateway_,.gateway").attr("disabled", false);
    //         $(".gateway .ms-choice").attr("disabled", false);
    //         $("#query_dba_name_").attr("disabled",false);
    //     }
    // });
    $(document).on("change", ".merchant-report-filter", function () {
        $('#cover-spin').show(0);
        var type = $("#query_type").val();
        var date = $("#date").val();
        if($("#dba_radio").is(':checked')) {
            $("#hidden_dba_check_merchant").val("true");
        }
        else {
            $("#hidden_dba_check_merchant").val("");
        }
        if($("#dba_radio_not0").is(':checked')) {
            $("#hidden_dba_check_not0").val("true");
        }
        else {
            $("#hidden_dba_check_not0").val("");
        }
        $("#hidden_dba_name_merchant").val(JSON.stringify($("#query_dba_name_").val()));
        $("#hidden_categories_merchant_2").val(JSON.stringify($("#query_category_").val()));
        if ($("#hidden_dba_name_merchant").val() == undefined){
            $("#hidden_dba_name_merchant").val("");
        }
        if ($("#hidden_categories_merchant_2").val() == undefined){
            $("#hidden_categories_merchant_2").val("");
        }
        var select_all = $("#hidden_dba_check").val();
        var dba_name = $("#hidden_dba_name_merchant").val();
        var category = $("#hidden_categories_merchant_2").val();
        var not_0 = $("#hidden_dba_check_not0").val();
        var status = $("#query_status").val();
        var tx_filter = $(this).val();
        var from_filter = "true";
        if ($("#query_dba_name_").is(':disabled')){
            dba_name = "";
        }
        if ($("#query_category_").is(':disabled')){
            category = "";
        }
        var all_location_name = $(".all_locations").val();
        $.ajax({
            url: "/admins/reports/generate_report",
            method: "POST",
            data: {query:{type: type, date: date, select_all: select_all, dba_name: dba_name, category: category, not_0: not_0, status: status, all_location_name: all_location_name}, from_filter: from_filter, tx_filter: tx_filter},
            success: function () {

            }

        })
    });
    $(document).on("change", ".withdrawal-report-filter", function () {
        $('#cover-spin').show(0);
        var type = $("#hidden_type").val();
        var date = $(".withdraw_date").val();
        var select_all = $("#hidden_wd_dba_check").val();
        var dba_name = $("#hidden_dba_name1").val();
        var fee_categories = $("#hidden_wd_categories1").val();
        var status_types = $("#hidden_wd_status_types1").val();
        var ach_category = $("#hidden_wd_ach_category1").val();
        var tx_filter = $(this).val();
        var from_filter = "true";
        var summary_report = $("#summary_report_withdrawal").val();
        var detail_report = $("#detail_report_withdrawal").val();
        if (dba_name == "null"){
            dba_name = ""
        }
        if (fee_categories == "null"){
            fee_categories = ""
        }
        if (status_types == "null"){
            status_types = ""
        }
        if (ach_category == "null"){
            ach_category = ""
        }
        $.ajax({
            url: "/admins/reports/generate_report",
            method: "POST",
            data: {query:{type: type, date: date, select_all: select_all, dba_name: dba_name, fee_categories: fee_categories, ach_category: ach_category, status_types: status_types}, from_filter: from_filter, tx_filter: tx_filter, summary_report: summary_report, detail_report: detail_report},
            success: function () {

            }

        })
    });
    $(document).on("change", ".wallet-report-filter", function () {
        $('#cover-spin').show(0);
        var type = $("#hidden_type").val();
        var not_0 = $("#hidden_dba_check_not0").val();
        var dba_name = $("#hidden_dba_name").val();
        var partner_name = $("#hidden_partner_name_wallet").val();
        var tx_filter = $(this).val();
        if (dba_name == "null"){
            dba_name = ""
        }
        if (partner_name == "null"){
            partner_name = ""
        }
        $.ajax({
            url: "/admins/reports/generate_report",
            method: "POST",
            data: {query:{type: type, dba_name: dba_name, partner_name: partner_name, not_0: not_0},tx_filter: tx_filter},
            success: function () {

            }

        })
    });
    $(document).on("change", ".sale-report-filter", function () {
        $('#cover-spin').show(0);
        var type = $("#hidden_type").val();
        var date = $("#hidden_date").val();
        var types = $("#hidden_types").val();
        var category = $("#hidden_categories").val();
        var dba_name = $("#hidden_dba_name").val();
        var tx_filter = $(this).val();
        if (dba_name == "null"){
            dba_name = ""
        }
        if (category == "null"){
            category = ""
        }
        $.ajax({
            url: "/admins/reports/generate_report",
            method: "POST",
            data: {query:{type: type, dba_name: dba_name, types: types, categories: category, date: date},tx_filter: tx_filter},
            success: function () {

            }

        })
    });
    //.....................reports/audit.html.erb...........//\
    $(document).on('click','.audit_gate_click',function () {
        $('#cover-spin').show(0);
    })
    $(document).on('click','.audit_gate_click-fee-summ',function () {
        if($("#query_partner_name_").val() == null){
            $("#location_error").show();
            $(".run_report").attr("disabled","disabled");
            $("#hidden_dba_check").val("false");
            return false;
        }
        else
        {
            $('#cover-spin').show(0);
            $("#location_error").hide();
            $("#hidden_all_check").val("true");
            $(".run_report").attr("disabled",false);
        }
    })

    $(".export_wallet_report").on('click',function () {
        $("#wallet_body_2").show();
        if($("#dba_radio").is(':checked')) {
            $("#hidden_all_check_wallet").val("true");
        }
        else{
            $("#hidden_all_check_wallet").val("");
        }
        $("#hidden_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
        $("#hidden_partner_name_wallet").val(JSON.stringify($("#query_partner_name_").val()));
    });
    //change hidden field values on change
    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#query_dba_name_").attr("disabled","disabled");
            $("#query_categories_,.categories").attr("disabled", 'disabled');
            $(".categories .ms-choice").attr("disabled", "disabled");
            $("#hidden_dba_check").val("true");
        }
        else{
            $("#query_dba_name_").attr("disabled",false);
            $("#query_categories_,.categories").attr("disabled", false);
            $(".categories .ms-choice").attr("disabled", false);
            $("#hidden_dba_check").val("");
        }
    });
    $("#query_dba_name_").on("change", function () {
        if($(this).val() != null) {
            $("#hidden_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
        }
        else {
            $("#hidden_dba_name").val("");
        }
    });
     $("#query_partner_name_").on("change", function () {
            if($(this).val() != null) {
                $("#hidden_partner_name_wallet").val(JSON.stringify($("#query_partner_name_").val()));
            }
            else {
                $("#hidden_partner_name_wallet").val("");
            }
        });

    $("#date").on('change',function () {

        $("#hidden_date").val($("#date").val());
        $("#hidden_date1").val($("#date").val());
    });
    //...........................reports/audit_gateway.html.erb..........//
/*    jQuery('.mandator_select').multiselect({
        enableHTML: true
        ,optionLabel: function(element) {
            return '<img src="'+ $(element).attr('data-img')+'"> '+ $(element).text();
        }
        ,selectAllText: 'Alle auswählen'
        ,nonSelectedText: 'Keins ausgewählt'
        ,nSelectedText: 'selektiert'
        ,allSelectedText: 'Alle ausgewählt'
    });*/
    $('.run_report').on('click', function(e){
        $('#reportModal').modal('show');
    });

    $('.run_report_merchant').on('click', function(e){
        $("#tx_filter_merchant").val($(".merchant-report-filter").val());

        if ($(".category").val() == null && $("#query_dba_name_").val() == null && $(".merchant_not0_radio").is(':checked') != true && $("#hidden_dba_check").val() == "" && (!$("#date").val() == "" || !$("#date").val() == undefined )){
            return false;
        }
        else if ($("#date").val() == ""){
            $(".merchant_error").text("Please Select Date")
            return false;
        }
        else{
            $(".merchant_error").text("")
        }
    });

    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });
    $("#query_gateway_").on('change',function () {
        $("#hidden_gateway").val(JSON.stringify($("#query_gateway_").val()));
    });
    //............................................reports/sales.html.erb.................//



    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#query_dba_name_").attr("disabled","disabled");
            $("#query_categories_,.categories").attr("disabled", 'disabled');
            $(".categories .ms-choice").attr("disabled", "disabled");
            $("#hidden_dba_check").val("true");
        }
        else{
            $("#query_dba_name_").attr("disabled",false);
            $("#query_categories_,.categories").attr("disabled", false);
            $(".categories .ms-choice").attr("disabled", false);
            $("#hidden_dba_check").val("");
        }
    });
    $(".merchant_select_all").on('click',function () {
        if($(this).is(':checked')) {
            $(".merchant_export_all").val("true");
        }
        else{
            $(".merchant_export_all").val("");
        }
    });
    $("#query_dba_name_").on("change", function () {
        if($(this).val() != null) {
            $("#query_categories_,.categories").attr("disabled", 'disabled');
            $(".categories .ms-choice").attr("disabled", "disabled");
            $("#hidden_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
        } else if($("#query_types_").val() != null) {

        }
        else {
            $("#query_categories_,.categories").attr("disabled", false);
            $(".categories .ms-choice").attr("disabled", false);
            $("#hidden_dba_name").val("");
        }
    });
    $("#query_types_").on("change", function () {
        if($(this).val() != null) {
            $("#query_categories_,.categories").attr("disabled", 'disabled');
            $(".categories .ms-choice").attr("disabled", "disabled");
            $("#hidden_types").val(JSON.stringify($("#query_types_").val()));
        } else if($("#query_dba_name_").val() != null) {

        }
        else {
            $("#query_categories_,.categories").attr("disabled", false);
            $(".categories .ms-choice").attr("disabled", false);
            $("#hidden_types").val("");
        }
    });
    $("#query_categories_").on("change",function () {
        if($(this).val() != null) {
            $("#query_dba_name_").attr("disabled","disabled");
            $("#query_types_,.type").attr("disabled", 'disabled');
            $(".type .ms-choice").attr("disabled", "disabled");
            $(".db_class .checkmark").css("background-color", "grey");
            $("#hidden_categories").val(JSON.stringify($("#query_categories_").val()));
            $("#hidden_types").val("");
            $("#hidden_dba_name").val("");
            $("#hidden_dba_check").val("");
        } else {
            $("#dba_radio").attr("disabled", false);
            $(".db_class .checkmark").css("background-color", "");
            $("#query_dba_name_").attr("disabled",false);
            $("#query_types_,.type").attr("disabled", false);
            $(".type .ms-choice").attr("disabled", false);
            $("#hidden_categories").val("");
        }
    });
    //change hidden field values on change
    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });
    //..........................................reports/gateway.html.erb..........//


    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });
    $("#query_gateway_").on('change',function () {
        $("#hidden_gateway").val(JSON.stringify($("#query_gateway_").val()));
    });
//..................................reports/merchant_reports.html.erb........................//
    $("#query_dba_name_").on('change',function () {
        if($(this).val() != null) {
            $(".category").attr("disabled", "disabled");
            $(".category .ms-choice").attr("disabled", "disabled");
        } else {
            $(".category").attr("disabled", false);
            $(".category .ms-choice").attr("disabled", false);
        }
    });
    var category = []
    $("#query_category_").on('change',function () {
        if($(this).val() != null) {
            $("#query_dba_name_").attr("disabled", "disabled");
            $(".db_class .checkmark").css("background-color", "grey");
            $("#hidden_categories_merchant").val(JSON.stringify($(".category").val()));
        } else {
            $("#query_dba_name_").attr("disabled", false);
            $("#dba_radio").attr("disabled", false);
            $(".db_class .checkmark").css("background-color", "");
        }
        pushall = []
        if ($("#query_category_").val() != null) {
            jQuery.each($("#query_category_").val(), function (index, value) {
                $("#query_dba_name_ option").each(function (index, option) {
                    if (option.value != "all" && JSON.parse(option.value).category_id == value) {
                        pushall.push(option.value)
                    }
                });
            });
        }else{
            $("#query_dba_name_ option").each(function(index, option) {
                if (option.value != "all"){
                    pushall.push(option.value)
                }
            });
        }
        $("#all_locations_all").val(JSON.stringify(pushall))
        $("#all_locations_all_export").val(JSON.stringify(pushall))
        $("#all_locations_all_filter").val(JSON.stringify(pushall))
    });

    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $(".category").attr("disabled", "disabled");
            $(".category .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled", "disabled");
        }
        else{
            $("#query_dba_name_").attr("disabled", false);
            $(".category").attr("disabled", false);
            $(".category .ms-choice").attr("disabled", false);
        }
    });
    //...................................reports/wallet_reports.html.erb...............//

    $("#query_dba_name_").on('change',function () {
        if($(this).val() != null) {
            $("#query_partner_name_").attr("disabled", "disabled");
            // $(".category .ms-choice").attr("disabled", "disabled");
        } else {
            $("#query_partner_name_").attr("disabled", false);

            // $(".category").attr("disabled", false);
            // $(".category .ms-choice").attr("disabled", false);
        }
    });
    $("#query_partner_name_").on('change',function () {
        if($(this).val() != null) {
            $("#query_dba_name_").attr("disabled", "disabled");
            // $(".db_class .checkmark").css("background-color", "grey");
            // $("#hidden_categories_merchant").val(JSON.stringify($(".category").val()));
        } else {
            $("#query_dba_name_").attr("disabled", false);
            // $("#dba_radio").attr("disabled", false);
            // $(".db_class .checkmark").css("background-color", "");
        }
    });

    // $('.run_report').on('click', function(e){
    //     $('#reportModal').modal('show');
    // });

    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $(".category").attr("disabled", "disabled");
            $(".category .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled", "disabled");
        }
        else{
            $("#query_dba_name_").attr("disabled", false);
            $(".category").attr("disabled", false);
            $(".category .ms-choice").attr("disabled", false);
        }
    });
    //...............................reports/iso_summary.html.erb......................//
    $("#iso_sum_repo_btn").prop('disabled',true);


    $(".report_submit").on('click', function (e) {
        if($("#query_iso_name_").val() == null && $("#dba_radio").prop("checked") == false){
            $("#location_error").show().text("Please enter ISO or check 'select all' Box")
        }
        if($("#query_parameters_").val() != null && ($("#query_dba_name_").val() != null || $("#query_gateway_").val() != null || $("#dba_radio").is(':checked'))) {
            $('#cover-spin').show(0);
            $("#location_error").show().text("")
        }
    })

    //change hidden field values on change

    $("#query_iso_name_").on('change',function () {
        $("#iso_radio").prop('checked',true);
        $("#location_error").show().text("")
    });
    // $(".withdraw-dba-radio").on('click',function () {
    //     if($(this).is(':checked')) {
    //         // $(".type").attr("disabled", "disabled");
    //         // $("#hidden_status_types1").val("null");
    //         // $("#hidden_ach_category1").val("null");
    //         // $("#hidden_categories1").val("null");
    //     }
    //     else{
    //         // $(".type").attr("disabled", false);
    //         // $("#hidden_status_types1").val(JSON.stringify($("#query_status_types_").val()));
    //         // $("#hidden_ach_category1").val(JSON.stringify($("#query_ach_category_").val()));
    //         // $("#hidden_categories1").val(JSON.stringify($("#query_fee_categories_").val()));
    //     }
    // });
    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#query_iso_name_").attr("disabled", "disabled");
            $("#iso_sum_repo").data('all','true');
            check_value("true");
            $("#location_error").show().text("")
        }
        else{
            $("#query_iso_name_").attr("disabled", false);
            $("#iso_sum_repo").data('all','false');
            check_value("false");
        }
    });
    $("#date").on('change',function () {
        $("#iso_sum_repo").data('search-date',$("#date").val());
    });
    $("#query_iso_name_").on('change',function () {
        check_value("false");
        $("#iso_sum_repo").data('iso',JSON.stringify($("#query_iso_name_").val()));
    });
    $("#iso_sum_repo").on("click",function () {
        $("#body_2").show();
    });
    function check_value(checked) {
        if(checked == "true"){
            $("#iso_sum_repo").attr('href', "#exportModalDialog");
            $("#iso_sum_repo_btn").prop('disabled', false);
        } else {
            if ($("#query_iso_name_").val() == null){
                $("#iso_sum_repo").attr('href', "#");
                $("#iso_sum_repo_btn").prop('disabled', true);
            } else {
                if ($("#query_iso_name_").val().length != 0) {
                    $("#iso_sum_repo").attr('href', "#exportModalDialog");
                    $("#iso_sum_repo_btn").prop('disabled', false);
                } else {
                    $("#iso_sum_repo").attr('href', "#");
                    $("#iso_sum_repo_btn").prop('disabled', true);
                }
            }
        }
    }
    //.......................iso_summary_table.html.erb...................//


    //......................reports/refund.html.erb........................//

    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#query_dba_name_").attr("disabled","disabled");
        }
        else{
            $("#query_dba_name_").attr("disabled",false);
        }
    });
    //.......................reports/fee_detail.html.erb......................//



    //change hidden field values on change

    // $("#query_dba_name_").on('change',function () {
    //     if($(this).val() != null) {
    //         $(".category").attr("disabled", "disabled");
    //         $(".category .ms-choice").attr("disabled", "disabled");
    //     } else {
    //         $(".category").attr("disabled", false);
    //         $(".category .ms-choice").attr("disabled", false);
    //     }
    // });
    // $("#query_category_").on('change',function () {
    //     if($(this).val() != null) {
    //         $("#query_dba_name_").attr("disabled", "disabled");
    //         $("#dba_radio").attr("disabled", "disabled");
    //         $(".db_class .checkmark").css("background-color", "grey");
    //     } else {
    //         $("#query_dba_name_").attr("disabled", false);
    //         $("#dba_radio").attr("disabled", false);
    //         $(".db_class .checkmark").css("background-color", "");
    //     }
    // });

    $("#query_dba_name_").on("change", function () {
        if($(this).val() != null) {
            $("#hidden_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
        }
    });
    $("#query_fee_categories_").on("change", function () {
        if($(this).val() != null) {
            $("#hidden_categories").val(JSON.stringify($("#query_fee_categories_").val()));
            $("#hidden_categories1").val(JSON.stringify($("#query_fee_categories_").val()));

        }
    });
    $("#query_status_types_").on("change", function () {
        if($(this).val() != null) {
            $("#hidden_status_types1").val(JSON.stringify($("#query_status_types_").val()));
        }
        else{
            $("#hidden_status_types1").val("null");
        }
    });
    $("#query_ach_category_").on("change", function () {
        if($(this).val() != null) {
            $("#hidden_ach_category1").val(JSON.stringify($("#query_ach_category_").val()));
        }
        else{
            $("#hidden_ach_category1").val("null");
        }
    });
    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });
    $('input[name="query[date]"]').val($("#date").val());
    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $(".category").attr("disabled", "disabled");
            $(".category .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled", "disabled");
        }
        else{
            $("#query_dba_name_").attr("disabled", false);
            $(".category").attr("disabled", false);
            $(".category .ms-choice").attr("disabled", false);
        }
    });



        $("#hidden_date").val($("#date").val());
  //...................................................fee_details.html.erb..........//

    //change hidden field values on change
    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });
    $("#query_dba_name_").on('change',function () {
        $("#hidden_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
        $("#hidden_dba_name1").val(JSON.stringify($("#query_dba_name_").val()));
    });
    $("#query_fee_types_").on('change',function () {
        $("#hidden_fee_types").val(JSON.stringify($("#query_fee_types_").val()));
    });

    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#hidden_dba_check").val("true");
        }
        else{
            $("#hidden_dba_check").val("");
        }
    });
//...........................fee_summary.html.erb...............//



    //change hidden field values on change

    $("#query_iso_name_").on('change',function () {
        if($(this).val() != null) {
            $("#hidden_iso_name").val(JSON.stringify($("#query_iso_name_").val()));
        }
    });
    $("#query_fee_categories_").on('change',function () {
        if($(this).val() != null) {
            $("#hidden_categories").val(JSON.stringify($("#query_fee_categories_").val()));
        }
    });
    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });

    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $(".category").attr("disabled", "disabled");
            $(".category .ms-choice").attr("disabled", "disabled");
            $("#query_iso_name_").attr("disabled", "disabled");
        }
        else{
            $("#query_iso_name_").attr("disabled", false);
            $(".category").attr("disabled", false);
            $(".category .ms-choice").attr("disabled", false);
        }
    });
       //........................withdrawal.html.erb.............//




    //change hidden field values on change

    // $("#query_dba_name_").on('change',function () {
    //     if($(this).val() != null) {
    //         $(".category").attr("disabled", "disabled");
    //         $(".category .ms-choice").attr("disabled", "disabled");
    //     } else {
    //         $(".category").attr("disabled", false);
    //         $(".category .ms-choice").attr("disabled", false);
    //     }
    // });
    // $("#query_category_").on('change',function () {
    //     if($(this).val() != null) {
    //         $("#query_dba_name_").attr("disabled", "disabled");
    //         $("#dba_radio").attr("disabled", "disabled");
    //         $(".db_class .checkmark").css("background-color", "grey");
    //     } else {
    //         $("#query_dba_name_").attr("disabled", false);
    //         $("#dba_radio").attr("disabled", false);
    //         $(".db_class .checkmark").css("background-color", "");
    //     }
    // });
    $(".wd-date").daterangepicker({
        // autoUpdateInput: false,
        minDate: 2019
    });
    $(document).on("click",'#run_report', function () {
        if($("#query_fee_categories_").val() != null) {
            $("#hidden_wd_categories1").val(JSON.stringify($("#query_fee_categories_").val()));
            $("#hidden_sm_categories1").val(JSON.stringify($("#query_fee_categories_").val()));
        }else{
            $("#hidden_wd_categories1").val('null');
            $("#hidden_sm_categories1").val('null');
        }
        if($("#query_status_types_").val() != null) {
            $("#hidden_wd_status_types1").val(JSON.stringify($("#query_status_types_").val()));
            $("#hidden_sm_status_types1").val(JSON.stringify($("#query_status_types_").val()));
        }else{
            $("#hidden_wd_status_types1").val('null');
            $("#hidden_sm_status_types1").val('null');
        }
        if($("#query_ach_category_").val() != null) {
            $("#hidden_wd_ach_category1").val(JSON.stringify($("#query_ach_category_").val()));
            $("#hidden_sm_ach_category1").val(JSON.stringify($("#query_ach_category_").val()));
        }else{
            $("#hidden_wd_ach_category1").val('null');
            $("#hidden_sm_ach_category1").val('null');
        }
        if($("#date").val() != null) {
            $("#hidden_wd_date1").val($("#date").val());
            $("#hidden_sm_date1").val($("#date").val());
        }else{
            $("#hidden_wd_date1").val('');
            $("#hidden_sm_date1").val('');
        }
        if($("#query_dba_name_").val() != null) {
            $("#hidden_wd_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
            $("#hidden_sm_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
        }
        else{
            $("#hidden_wd_dba_name").val('');
            $("#hidden_sm_dba_name").val('');
        }
        if($("#dba_radio1").is(':checked')) {
            $("#hidden_wd_dba_check").val("true");
            $("#hidden_sm_dba_check").val("true");
        }else{
            $("#hidden_wd_dba_check").val("");
            $("#hidden_sm_dba_check").val("");
        }
        $(".export-btn").prop("disabled", false);
        $(".summary-btn").prop("disabled", false);
    });
    // $("#query_dba_name_").on("change", function () {
    //     if($(this).val() != null) {
    //         $("#hidden_dba_name").val(JSON.stringify($("#query_dba_name_").val()));
    //     }
    // });
    // $("#query_fee_categories_").on("change", function () {
    //     if($(this).val() != null) {
    //         $("#hidden_wd_categories1").val(JSON.stringify($("#query_fee_categories_").val()));
    //         $("#hidden_sm_categories1").val(JSON.stringify($("#query_fee_categories_").val()));
    //     }
    // });
    //
    // $("#date").on('change',function () {
    //     $("#hidden_wd_date1").val($("#date").val());
    //     $("#hidden_sm_date1").val($("#date").val());
    //
    // });
    $("#run_report, #summary, #export").on("click", function () {
        if($("#date").val() == ""){
            return false;
        }
    });

    $("#dba_radio1").on('click',function () {
        if($(this).is(':checked')) {
            // $(".category").attr("disabled", "disabled");
            // $(".category .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled", "disabled");
            push_all = []
            $("#query_dba_name_ option").each(function(index, option) {
                if (option.value != "all"){
                    push_all.push(option.value)
                }
            });
            $("#all_locations_all").val(JSON.stringify(push_all))
            $("#all_locations_all_export").val(JSON.stringify(push_all))
            $("#all_locations_all_filter").val(JSON.stringify(push_all))
        }
        else{
            $("#query_dba_name_").attr("disabled", false);
            $("#all_locations_all").val('')
            $("#all_locations_all_export").val('')
            $("#all_locations_all_filter").val('')
            // $(".category").attr("disabled", false);
            // $(".category .ms-choice").attr("disabled", false);
        }
    });
//.......................iso_profit_split.html.erb...............//

    $("#query_iso_name_").on("change", function () {
        if($(this).val() != null) {
            $("#hidden_iso_name").val(JSON.stringify($("#query_iso_name_").val()));
        }
    });
    // $("#query_partner_name_").on("change", function () {
    //     console.log("dssssss")
    //     if($(this).val() != null) {
    //         console.log($("#query_partner_name_").val())
    //         $("#hidden_partner_names").val($("#query_partner_name_").val());
    //     }
    // });
    $("#date").on('change',function () {
        $("#hidden_date").val($("#date").val());
    });


        $("#hidden_date").val($("#date").val());

        $("#query_iso_name_").on("change", function () {
            $('#cover-spin').show(0)
            var partners = {};
            var isos = $(this).val();
            isos.forEach(function (iso){
                var iso_data = JSON.parse(iso)
                var iso_id = iso_data.id
                $.ajax({
                    url:  "/admins/reports/autopopulate_partner",
                    type: 'GET',
                    data: {id: iso_id},
                    success: function(data) {
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        $(".run_report").on("click", function () {
            var partner_names = $("#query_partner_name_").val()
            $("#hidden_partner_names").val(JSON.stringify(partner_names))
        })
    });
        /////..............................terminal_id.html.erb.........///

    $('#date').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    $('#date').on('cancel.daterangepicker', function(ev, picker) {
        // $(this).val('');
    });
    $("#query_dba_name_").on('change',function () {
        // if($(this).val() != null) {
        //     $("#date").attr("disabled", "disabled");
        //     $("#date .ms-choice").attr("disabled", "disabled");
        // } else {
        //     $("#date").attr("disabled", false);
        //     $("#date .ms-choice").attr("disabled", false);
        // }
    });

    $('.run_report').on('click', function(e){
        if ( ($("#dba_radio").is(":checked")) || ($("#query_dba_name_").val() != null ) || ($("#date").val() != '' )){
            $('#reportModal').modal('show');
        }
    });

    $("#dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#date").attr("disabled", "disabled");
            $("#date .ms-choice").attr("disabled", "disabled");
            $("#query_dba_name_").attr("disabled", "disabled");
        }
        else{
            $("#query_dba_name_").attr("disabled", false);
            $("#date").attr("disabled", false);
            $("#date .ms-choice").attr("disabled", false);
        }
    });
    $(".merchant_dba_radio").on('click',function () {
        if($(this).is(':checked')) {
            $("#date").attr("disabled", false);
            // $(".category").attr("disabled", true);
        }
    });
    $("#dba_radio_not0").on('click',function () {
        if($(this).is(':checked')) {
            $(this).val("true");
            $("#hidden_dba_check_not0").val("true");
        }
        else {
            $(this).val("");
            $("#hidden_dba_check_not0").val("");
        }
    });
    $("#query_status").on('change',function () {
        if($(this).val() == "Active") {
            $("#hidden_status_merchant").val("Active");
        }
        else if ($(this).val() == "Inactive"){
            $("#hidden_status_merchant").val("Inactive");
        }
        else{
            $("#hidden_status_merchant").val("");
        }
    });
    $(".merchant_dba_radio, #query_dba_name_, .category_merchant, #dba_radio1").on('change',function () {
        if($(".merchant_dba_radio").is(':checked') || $(".merchant_not0_radio").is(':checked') || $(".category_merchant").val() != null || $("#query_dba_name_").val() != undefined) {
            $(".run_report_merchant").attr('disabled',false);
        }
        else if (!$(".merchant_dba_radio").is(':checked') && $(".category_merchant").val() == null && $("#query_dba_name_").val() == undefined){
            $(".run_report_merchant").attr('disabled',true);
        }
    });
    $(".withdraw_detailed_summery").on('click',function () {
        $("#wallet_body_2").show();
    });
    $(".withdrawal_categories").on('change',function () {
        if ($(this).val() != null){
            $('.withdrawal_types').attr("disabled", true)
        }
        else{
            $('.withdrawal_types').attr("disabled", false)
        }
    });
});

$('#datatable-keytable').DataTable({
    responsive: true,
    sorting: false,
    paging: false,
    bfilter: false,
    searching: false,
    columnDefs: [ { orderable: false, targets: 0} ],
    columnDefs: [{
        targets: "_all",
        className: "text-center",
    }]
});
// $('#datatable-keytable_cbk').DataTable({
//     "dom": 'Bfrtip',
//     "bLengthChange": false,
//     "searching": false,
//     "sScrollX": false,
//     "scrollX": false,
//     "buttons": [
//         { extend: 'csv', text: 'Export', className: "csv_button btn btn-success" }
//     ],
//     "exportOptions": {
//         modifer: {
//             page: 'all',
//             search: 'none'    }
//     }
// });
var lt= $('.timeUtc-user').each(function (i,v) {
    var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
    var local = moment(gmtDateTime).local().format('YYYY-MM-DD hh:mm:ss A');
    v.innerText = local
});