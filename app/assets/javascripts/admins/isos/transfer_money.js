$("#amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
// --------------------------------------AUTOCOMPLETE NEW FUNCTION---------------------------------------------
var from_wallet_id="";
var to_wallet_id="";
$("input[data-autocomplete]").each(function () {
    url = $(this).data('autocomplete');
    var type = $(this).data('type');
    var categoryArray=[];
    var autocomplete=$(this).autocomplete({
        source: url,
        create: function () {
            categoryArray=[];
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                if (categoryArray.includes(item.category)){
                    return $('<li class="ui-menu-item">')
                        .append("<div class='ui-menu-item-wrapper select-user'>"+ item.label+ "</div>")
                        .appendTo(ul);
                }else{
                    categoryArray.push(item.category);
                    return $('<li class="ui-menu-item">')
                        .append("<div class='aria-label'><strong style='margin-left: 5px'>"+ item.category+ "</strong></div>")
                        .append("<div class='ui-menu-item-wrapper select-user'>"+ item.label+ "</div>")
                        .appendTo(ul);
                }
            };
        },
        select: function(e,ui){
            $('#'+type+'_wallet_error').hide();
            // Mantain Response In Categories
            if(ui.item.category=="Merchant"){
                $('#'+type+'InputHidden').removeAttr('name');
                $(this).prop('required',true);
                $('#'+type+'Location').prop('required',true);
                $('#'+type+'Wallet').prop('required',true);
                $('#'+type+'MainDiv').show();
                $('#'+type+'Location').empty();
                $('#'+type+'Wallet').empty();
//                    ---------DEFAULT OPTION FOR LOCATION AND WALLET--------------------------
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $('#'+type+'Location').append(new_option);
                var new_wallet_option = new Option("Choose Wallet", '');
                $(new_wallet_option).html('Choose Wallet');
                $('#'+type+'Wallet').append(new_wallet_option);
//                    --------Append Merchant Locations-----------------------------------------
                $.ajax({
                    url: "/admins/transfers/get_merchant_location",
                    method: 'get',
                    data: {id: ui.item.id},
                    success: function (result) {
                        result.locations.forEach(function(location) {
                            var new_option = new Option(location.name, location.id);
                            $(new_option).html(location.name);
                            $('#'+type+'Location').append(new_option);
                        });
                    }
                });
                if(type=="from"){
                    $('#'+type+'MainDiv').css('display','block');
                    if($('#toMainDiv').css('display')!='block'){
                        $("#toMainDiv").css('visibility','hidden');
                        $("#toMainDiv").css('display','block');
                    }
                }else if(type=="to"){
                    if($('#'+type+'MainDiv').css('display')=="none"){
                        $('#'+type+'MainDiv').css('display','block');
                    }else if($('#'+type+'MainDiv').css('display')=="block"){
                        $('#'+type+'MainDiv').css('visibility','visible');
                    }
                }
            }
            else {
                $('#'+type+'MainDiv').hide();
                $(this).prop('required',true);
                $('#'+type+'Location').prop('required',false);
                $('#'+type+'Wallet').prop('required',false);
                $('#'+type+'Wallet').removeAttr('name');
                if(type=="from"){
                    from_wallet_id=ui.item.id
                    if(to_wallet_id==from_wallet_id){
                        $('#from_wallet_error').text('Wallets Cannot Be Same');
                        $('#from_wallet_error').show();
                        $('#fromInput').val('');
                        from_wallet_id=''
                        return false;
                    }else{
                        $('#from_wallet_error').hide();
                    }
                }else{
                    to_wallet_id=ui.item.id
                    if(to_wallet_id==from_wallet_id){
                        $('#to_wallet_error').text('Wallets Cannot Be Same');
                        $('#to_wallet_error').show();
                        $('#toInput').val('');
                        to_wallet_id=''
                        return false;
                    }else{
                        $('#to_wallet_error').hide();
                    }
                }
                if(type=="to"){
                    if($('#fromMainDiv').css('display')=="block"){
                        $('#'+type+'MainDiv').css('visibility','hidden');
                        $('#'+type+'MainDiv').css('display','block');
                    }
                }
                $('#'+type+'InputHidden').attr('name',"transfer["+type+"_wallet]");
                $('#'+type+'InputHidden').val(ui.item.id);
                if(type=="from"){
                    showWalletBalance(ui.item.id, 'source');
                }
                else {
                    showWalletBalance(ui.item.id, 'destination');
                }
            }
        },
        response: function( event, ui ) {
            categoryArray=[];
        }
    })
});

$(document).ready(function(){
    function numberWithCommas(x) {
        $('#amount').val(x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    }
    $("#amount").keyup(function (e) {
        var str =$('#amount').val();
        str=str.replace(/,/g, "")
        numberWithCommas(str)
    });
    $("#submit_transfer_btn").click(function () {
        $('#amount').val($('#amount').val().replace(/,/g, ""));
    });
});

$(document).ready(function(){
    var dateToday=new Date();
    $('#transfer_date').datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: new Date()
    }).datepicker("setDate", new Date());
});


//--------------------------------------GET WALLET NAME-----------------------
function getWalletName(from_wallet,to_wallet) {
    console.log('getWalletName');
    $.ajax({
        url: "/admins/transfers/get_wallet_name",
        type: "GET",
        data: {from_wallet: from_wallet,to_wallet: to_wallet},
        success: function (data) {
            $("#transfer_sender").text(data.from_wallet);
            $("#transfer_receiver").text(data.to_wallet);
        },
        error: function (data) {
            response('')
        }
    });
}

$('.transferLocation').change(function(){
    var loc_id,type;
    loc_id=$(this).val();
    type=$(this).data('type');
    if(loc_id!="" && loc_id!=0){
        $.ajax({
            url: "/admins/transfers/get_location_wallets",
            type: "GET",
            data: {location_id: loc_id},
            success: function (data) {
                if (data.wallets!=""){
                    $('#'+type+'Wallet').empty();
                    var new_option = new Option("Choose Wallet", '');
                    $(new_option).html('Choose Wallet');
                    $('#'+type+'Wallet').append(new_option);
                    data.wallets.forEach(function(wallet) {
                        var new_option = new Option(wallet.name, wallet.id);
                        $(new_option).html(wallet.name);
                        $('#'+type+'Wallet').append(new_option);
                        $('#'+type+'Wallet').attr('name','transfer['+type+'_wallet]');
                    });
                }
                else{
                    $('#'+type+'Wallet').empty();
                    $('#'+type+'Wallet').removeAttr('name');
                    alert('No wallets exist for this location');
                }
            },
            error: function (data) {
                $('#'+type+'Wallet').empty();
                $('#'+type+'Wallet').removeAttr('name');
                alert('No wallets exist for this location');
            }
        });
    }
});
//  ----------------------------------Wallet Change Function------------------------
$('.transferWallet').change(function(){
    var source=$(this).val();
    var type=$(this).data('type');
    $('#'+type+'_wallet_error').hide();
    if(type=="from"){
        if(to_wallet_id==source){
            $("#fromWallet option[value='']").attr('selected', true);
            $('#from_wallet_error').text('Wallets Cannot Be Same');
            $('#from_wallet_error').show();
            from_wallet_id=''
            return false;
        }
        from_wallet_id=source;
    }else{
        if(from_wallet_id==source){
            $("#toWallet option[value='']").attr('selected', true);
            $('#to_wallet_error').text('Wallets Cannot Be Same');
            $('#to_wallet_error').show();
            to_wallet_id=''
            return false;
        }
        to_wallet_id=source;
    }
    if(source!='' || source!=null){
        showWalletBalance(source, type);
    }
});
//  ---------------------------------Check Wallet Balance----------------------------
function showWalletBalance(wallet_id, type){
    if (type=="from"){
        type='source';
    }else if(type=="to"){
        type="destination";
    }
    $.ajax({
        url: "/merchant/locations/show_merchant_balance",
        type: "GET",
        data: {source: wallet_id},
        success: function (data) {
            if (data.balance!=""){
                $('#' + type + '_available_amount').text(data.balance);
            }else{
                $('#' + type + '_available_amount').text('0.00');
            }
        },
        error: function (data) {
            $('#' + type + '_available_amount').text('0.00');
        }
    });
}
//  ----------------------------------GET USER DETAILS FOR EDIT CASE------------------------------
function getUserDetails(wallet_id,type){
    $.ajax({
        url: "/admins/transfers/get_user_details",
        type: "GET",
        data: {wallet_id: wallet_id},
        success: function (data) {
            if (data.user_type!=""){
                if (data.user_type=="merchant"){
                    $('#'+type+'Location').prop('required',true);
                    $('#'+type+'Wallet').prop('required',true);
                    if(type=="from"){
                        $('#'+type+'MainDiv').show();
                        if($('#toMainDiv').css('display')!='block'){
                            $("#toMainDiv").css('visibility','hidden');
                            $("#toMainDiv").css('display','block');
                        }
                    }else if(type=="to"){
                        if($('#'+type+'MainDiv').css('display')=="none"){
                            $('#'+type+'MainDiv').css('display','block');
                            $('#'+type+'MainDiv').css('visibility','visible');
                        }else if($('#'+type+'MainDiv').css('display')=="block"){
                            $('#'+type+'MainDiv').css('visibility','visible');
                        }
                    }

                    $('#'+type+'Input').val(data.user_name);
//                      ------------------------Get User Wallets--------------------
                    if (data.locations!=""){
                        $('#'+type+'Location').empty();
                        var new_option = new Option("Choose Location", '');
                        $(new_option).html('Choose Location');
                        $('#'+type+'Location').append(new_option);
                        data.locations.forEach(function(location) {
                            var new_option = new Option(location.name, location.id);
                            $(new_option).html(location.name);
                            $('#'+type+'Location').append(new_option);
                        });
                        $('#'+type+'Location option[value="'+data.location_id+'"]').prop("selected", true);
                    }
                    if (data.wallets!=""){
                        if (type=="from"){
                            from_wallet_id=data.wallet_id
                            showWalletBalance(data.wallet_id, 'source');
                        }
                        else {
                            to_wallet_id=data.wallet_id
                            showWalletBalance(data.wallet_id, 'destination');
                        }
                        $('#'+type+'Wallet').empty();
                        var new_option = new Option("Choose Wallet", '');
                        $(new_option).html('Choose Wallet');
                        $('#'+type+'Wallet').append(new_option);
                        data.wallets.forEach(function(wallet) {
                            var new_option = new Option(wallet.name, wallet.id);
                            $(new_option).html(wallet.name);
                            $('#'+type+'Wallet').append(new_option);
                            $('#'+type+'Wallet').attr('name','transfer['+type+'_wallet]');
                        });
                        $('#'+type+'Wallet option[value="'+data.wallet_id+'"]').prop("selected", true);
                    }
                }else{
                    $('#'+type+'MainDiv').hide();
                    if(type=="to"){
                        if($('#fromMainDiv').css('display')=="block"){
                            $('#'+type+'MainDiv').css('visibility','hidden');
                            $('#'+type+'MainDiv').css('display','block');
                        }
                    }
                    if(type=="from"){
                        from_wallet_id=data.wallet_id
                        showWalletBalance(data.wallet_id, 'source');
                    }
                    else {
                        to_wallet_id=data.wallet_id
                        showWalletBalance(data.wallet_id, 'destination');
                    }
                    $('#'+type+'Location').prop('required',false);
                    $('#'+type+'Wallet').prop('required',false);
                    $('#'+type+'Input').val(data.user_name);
                    $('#'+type+'InputHidden').val(wallet_id);
                    $('#'+type+'InputHidden').attr('name','transfer['+type+'_wallet]');
                }
            }else{
                alert('Request Error Occured Kindly Refresh Your Page')
            }
        },
        error: function (data) {
            $('#source_available_amount').text('0.00');
            $('#destination_available_amount').text('0.00');
        }
    });
}

//    ---------------------------------END SEARCH FUNCTIONALITY-----------------------------------

$('#submit_transfer_btn').hide();
$('#verification_box').hide();

$('#verification_btn').on('click',function () {
    $('#verification_btn').hide();
    $('#verification_box').fadeIn("slow");
});
$('#password_btn').on('click',function () {
    var password = $('#password').val();
    $.ajax({
        url: "/admins/buy_rate/verify_admin_password",
        method: 'get',
        data: {password: password},
        success:function(data){
            $("#password_btn").css("background","green").text('').text("Verified");
            $("#info-text").text('');
            setTimeout(function(){
                $('#password').attr("disabled","disabled");
                $('#verification_box').fadeOut("slow");
                $('#verification_btn').hide();
                $('#submit_transfer_btn').show();
            }, 1000);
        },
        error: function(data){
            $("#password_btn").css("border","1px solid red");
            $("#info-text").text('').css("color","red").text('Please try again!');
        }
    });
});