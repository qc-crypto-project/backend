$(document).ready(function () {
    var iso_user_data = null;
    if (value_present(gon.iso_baked_user)) {
        iso_user_data = gon.iso_baked_user;
    }
    if (value_present(iso_user_data.splits != null)) {
        $('#iso_radio_button').show();
    } else {
        $('#iso_radio_button').hide();
    }
    if (iso_user_data.splits != null) {
        $("#details_switch").attr("checked", "checked");
    }else {
        $("#details_switch").attr("checked", false);
    }

})