
$(document).ready(function () {

    // $("#amount1").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $(".number_field").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
    $(".last4").inputmask('Regex', {regex: "^\\d+\\\\d{0,2}$"});
    $(".alphanumeric").inputmask('Regex', {regex: "^[A-Za-z0-9 _:-]*[A-Za-z0-9][A-Za-z0-9 _:-]*$"});


// ++++++++++++++++++  _data_table.html.erb ++++++++++++++++
$('#alert-message-top').fadeOut(5000);
var table = $('#datatable-dispute-cases').DataTable({
    responsive:false,
    "sorting": true,
    "searching": false,
//        "sScrollX": true,
    "bLengthChange": false,
    "bInfo":false,
    "bPaginate": false,
     "order": [[ 0, "desc" ]],
    // "columnDefs": [
    //     { "orderable": false, "targets": 12}
    // ]
});
$('#details').click(function(){

    $('#cover-spin').fadeIn();

});

    $('.daterange').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY',
            autoApply: false


        }
    });
    $('input[name="q[recieved_date]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name="q[recieved_date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="q[recieved_date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });
    $('input[name="q[due_date]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name="q[due_date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="q[due_date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });
    $('input[name="q[created_at]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name="q[created_at]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="q[created_at]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });
// +++++++++++++ imported_cbk.html.erb

/*$('#datatable-keytable').DataTable({
    responsive: true,
    sorting: true,
    paging: true,
    bfilter: true,
    searching: true
});*/
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});
$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
    console.log("teste");
    var input_label = $(this).closest('.input-group').find('.file-input-label'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;

    if( input_label.length ) {
        input_label.text(log);
    } else {
        if( log ) alert(log);
    }
});


//+++++++++++++++++ _edit_without_save.html.erb ++++++++++++

$('#close_modal_button').click(function(){
    setTimeout(function(){
        $(document.body).addClass( 'modal-open' )
    }, 500);
});
$('#dispute_note_form_title_field').keypress(function(){
    if(this.value=="" || this.value==null){
        $('#warning_showing_span').text("Fill Up Your Fields!");
    }else{
        $('#warning_showing_span').text("");
    }
});

    //.......cases.html.erb.............................//
    $(document).on('click' ,'.all_dispute',function () {
        show_disputes('/admins/disputes/cases', 'all')
    })
    $(document).on('click' ,'.sent_dispute',function () {
        show_disputes('/admins/disputes/cases', 'sent_pending')
    })
    $(document).on('click' ,'.won_dispute',function () {
        show_disputes('/admins/disputes/cases', 'won_reversed')
    })
    $(document).on('click' ,'.lost_dispute',function () {
        show_disputes('/admins/disputes/cases', 'lost')
    })
    $(document).on('click' ,'.dispute_accepted',function () {
        show_disputes('/admins/disputes/cases', 'dispute_accepted')
    })
    $(document).on('click' ,'.under_review',function () {
        show_disputes('/admins/disputes/cases', 'under_review')
    })

// +++++++++ _notification_notes.html.erb ++++++++++

$('#close_modal_button').click(function(){
    setTimeout(function(){
        $(document.body).addClass( 'modal-open' )
    }, 500);
});
$('#dispute_note_form_title_field').keypress(function(){
    if(this.value=="" || this.value==null){
        $('#warning_showing_span').text("Fill Up Your Fields!");
    }else{
        $('#warning_showing_span').text("");
    }
});

function getdate (v) {
    var gmtDateTime = moment.utc(v,'YYYY-MM-DD HH:mm:ss');
    var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm');
    return local;
}

// dispute_evidence

$('.datepicker').datepicker();
$("[type=file]").on("change", function(){
    // Name of file and placeholder
    var file = this.files[0].name;
    var dflt = $(this).attr("placeholder");
    if($(this).val()!=""){
        $(this).next().text(file);
    } else {
        $(this).next().text(dflt);
    }
});
//    $('#edit_email').click(function () {
//        $('#email_field').toggle();
//        $('#email').toggle();
//        $('#edit_email').toggle();
//    })


/* Activating Best In  Place */
 if ( $("#evidence_id").val() != "" ){
     $(".field-disable").prop("disabled", true);
     $("#image_upload").prop("disabled", true);
     $(".name").prop("disabled", true);
 }


 // dispute_case.html.erb

$('.datepicker_dispute').datepicker({
    format: 'mm-dd-yyyy'
});
 $('.datepicker_disput').datepicker({
    format: 'mm-dd-yyyy'
});
$('.file_field_jq').change(function () {
    if (this.value == ""){
        alert('Please select a file to upload')
    }else{
        var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
        var ext = this.value.split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf']) == -1) {
            alert("Only 'Png', 'Jpeg', 'Docx', 'Doc' ,'Pdf' allowed");
        }else{
            if (file_size <= 4){
                console.log(this.files);
                $('#files_section').empty();
                $.each( this.files, function( key, value ) {
                    console.log( key + ": " + value.name );
                    $('#files_section').append('<div class="col-md-10" id=upload_file_list_'+key+'>'+value.name+'</div>');
                });

            }else{
                alert('Please select a file less than 4MB')
            }
        }
    }
});
//  var delete_item = function(name) {
//    console.log($(".upload_file_list_"+name).text("delete_item"));
//    $('#files_section div .upload_file_list_'+name).each(function(){
//     console.log(this)
//    });
//   $.each($('.file_field_jq').files,function(key,value){
//     if(){

//     }
//   })
//   console.log($(".upload_file_list_"+name).text());
// }
// $(document).ready(function () {
//     var lt= $('.timeUtc').each(function (i,v) {
//         var gmtDateTime = moment.utc(v.innerText, 'MM-DD-YYYY hh:mm:ss A');
//         var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
//         v.innerText = local
//     });
// });
// $('#table_text_field_transaction_dispute').append($("<option></option>").attr("value",'Demo Demo').text("DemoDEmo"));


// cases.html.erb

$("#dispute_case_due_date_id").on('change',function (){
    if($(this).val() < $("#dispute_case_recieved_date_id").val()){
        alert("Please enter a Due Date after the Received Date");
        $("#duplicate-btn").prop("disabled",true);
    }
    else if($(this).val() >= $("#dispute_case_recieved_date_id").val()){
        $("#duplicate-btn").prop("disabled",false);
    }
});



//------------------------Amount Validation -----------------------
// $("#dispute_case_amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
var index_value;
var index;
var image_ids = []
$('#duplicate-btn').click(function(){

    if(index_value!='' && index_value!=undefined){
        delete_notification_note($del_notifi)
    }
    if(index!='' && index!=undefined){
        delete_transaction_attachment(image_ids)
    }
    if($add_notifi!='' && $add_notifi!=undefined ){
        save_notes()
    }

    $('#dispute_case_submit_button').click();

    $('#duplicate-btn').prop("disabled",true);
});

function edit_without_save(index) {
    $('#EditWithoutSaveModal').modal();
    object = $add_notifi[index - 1];
    $("#edit_dispute_note_form_title_field").val('').val(object.title);
    $("#edit_dispute_note_form_text_field").val('').val(object.name);
    $("#index").val('').val(index);
}
$('#edit_dispute_note_form_submit').on('click',function () {
    if($("#edit_dispute_note_form_title_field").val() == "" || $("#edit_dispute_note_form_text_field").val() == ""){
        $('#edit_warning_showing_span').text("Fill Up Your Fields!");
    }else {
        $('#edit_warning_showing_span').text("");
        index = $("#index").val();
        object = $add_notifi[parseInt(index) - 1];
        object.title = $("#edit_dispute_note_form_title_field").val();
        object.name = $("#edit_dispute_note_form_text_field").val();
        $('#edit_'+ parseInt(index)).text('').text(object.title);
        $('#EditWithoutSaveModal').modal('toggle');
    }
});
$('#boolValue').change(function () {
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        var msg = $(this).is(':checked') == true ? 'enabled!' : 'disabled!';
        if (isConfirm.value === true) {
            $.ajax({
                url: "/admins/app_configs/",
                type: 'POST',
                data: {id: $("#accept_disputes_id").val() , key: $("#accept_disputes_key").val(), boolValue: $(this).checked},
                success: function (data) {
                    swal("Updated!", "Disputes are successfully " + msg, "success");
                },
                error: function (error) {
                    console.error('Update Disputes Issue Error: ', error);
                    swal("Cancelled", "An unexpected error occured :)", "error");
                }
            });
        } else {
            $(this).prop("checked", !$(this).is(':checked'));
        }
    });
});

     if ($("#params_q").val() != ""){
         if ($("#receieved_date_case").val() == "" || $("#receieved_date_case").val() == undefined ){
             $('input[name="q[recieved_date]"]').val('');
         }
         if ($("#due_date").val() == "" || $("#due_date").val() == undefined){
             $('input[name="q[due_date]"]').val('');
         }
         if ($("#created_at").val() == "" || $("#created_at").val() == undefined){
             $('input[name="q[created_at]"]').val('');
         }
     } else{
         $('input[name="q[recieved_date]"]').val('');
         $('input[name="q[created_at]"]').val('');
         $('input[name="q[due_date]"]').val('');
     }
        $('input[name="q[recieved_date]"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

     $(".dispute-edit-btn").on("click", function () {
        $(this).html("<span class='btn btn-primary btn-sm'><i class='fa fa-refresh fa-spin'> </i> Loading...</span>")
     });
    $(document).on("change", "#tx_filter", function () {
        $("#tx_form").submit();
        $(document).on("click", "#txn-tr", function () {
            $('#cover-spin').show(0);
        })

    })


})
function show_disputes(url,disputeCase) {
    window.location.href =
        url + "?case="+ disputeCase
}