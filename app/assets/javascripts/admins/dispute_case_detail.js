var index_value;
var index;
var image_ids = [];
var note_id;
$del_notifi=[];
$del_image=[];
$add_notifi=[];
$image_store=[];
$(document).ready(function () {


   var dispute_case = gon.dispute_case;
   // var transaction = gon.transaction;
   // var transaction_data = gon.transaction_data;
   // var dispute_case_data = gon.dispute_case_data;
   // if(value_present(dispute_case)){
   //     if(value_present(dispute_case.attachments)){
   //         $.each(dispute_case_data, function( key, value ) {
   //             if(value_present(value)){
   //                 $('#files_section').append('<div class="col-md-10" id=upload_file_list_'+key+'>'+value+'</div>');
   //             }
   //         });
   //     }
   // }
   //  if(value_present(transaction)){
   //      if(value_present(transaction.attachments)){
   //          $.each(transaction_data, function( key, value ) {
   //              if(value_present(value)) {
   //                  $('#files_section').append('<div class="col-md-12" id=upload_file_list_' + key + '>' + value.image_name + '<div style="float:right;"><a style="color:black;" href="' + value.image_url + '" download="' + value.image_name + '" target="_blank" >Download</a><a onclick=store_transaction(' + value.id + ',' + key + ') style="color:red;padding:0px 0px 0px 5px;">Delete</a></div></div>');
   //              }
   //          });
   //      }
   //  }

    //------------------------Amount Validation -----------------------
    $("#dispute_case_amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $('#duplicate-btn').click(function(){
        $('#duplicate-btn').attr('disabled',true)
        if(index_value !='' && index_value!=undefined){
            delete_notification_note($del_notifi)
        }
        if(index!='' && index!=undefined){
            delete_transaction_attachment(image_ids)
        }
        if($add_notifi!='' && $add_notifi!=undefined ){
            save_notes()
        }
        $('#dispute_case_submit_button').click();
    });


    $('#edit_dispute_note_form_submit').on('click',function () {
    if($("#edit_dispute_note_form_title_field").val() == "" || $("#edit_dispute_note_form_text_field").val() == ""){
        $('#edit_warning_showing_span').text("Fill Up Your Fields!");
    }else {
        $('#edit_warning_showing_span').text("");
        index = $("#index").val();
        object = $add_notifi[parseInt(index) - 1];
        object.title = $("#edit_dispute_note_form_title_field").val();
        object.name = $("#edit_dispute_note_form_text_field").val();
        $('#edit_'+ parseInt(index)).text('').text(object.title);
        $('#EditWithoutSaveModal').modal('toggle');
    }
});

    // $('.dispute_case_file_field_jq').change(function () {
    //     if (this.value == ""){
    //         alert('Please select a file to upload')
    //     }else{
    //         var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
    //         var ext = this.value.split('.').pop().toLowerCase();
    //         if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf']) == -1) {
    //             alert("Only 'Png', 'Jpeg', 'Docx', 'Doc' ,'Pdf' allowed");
    //         }else{
    //             if (file_size <= 4){
    //                $('#files_section').empty();
    //               $.each( this.files, function( key, value ) {
    //                 console.log( key + ": " + value.name );
    //                         $('#files_section').append('<div class="col-md-10" id=upload_file_list_'+key+'>'+value.name+'</div>')
    //                       });
    //             }else{
    //                 alert('Please select a file less than 4MB')
    //             }
    //         }
    //     }
    // });
    // <!--    var attachment_count = <%#= raw @dispute_case.attachments.count.to_json %>;-->

    $(document).on("change", ".dispute_case_file_field_jq", function () {
        var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
        var ext = this.value.split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf']) == -1) {
            alert("Please select jpg,png,docx,doc,pdf or jpeg file type!");
            this.value = "";
        }


    });
    // $(document).on('click','#duplicate-btn',function () {
    //     $('#duplicate-btn').attr('disabled',true)
    // })
        $(document).on('click change','#dispute_case_case_number,#dispute_case_dispute_type_selection,#table_text_field_transaction_dispute,#dispute_detail_amount,status_selected_dispute_case',function () {
        $('#duplicate-btn').attr('disabled',false)
    })

    $('#cover-spin').fadeOut();

    var lt= $('.timeUtcmodel').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    var lt= $('.timeUtcmodel1').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm');
        v.innerText = local
    });

    $("#dispute_case_case_number").keypress(function(){
        if(this.value != ""){
            $("#dispute_case_case_number").css("border","");
        }
    });

    $("#dispute_case_recieved_date_id").on('keyup change',function(){
        if(this.value != ""){
            $("#dispute_case_recieved_date_id").css("border","");
        }
    });

    $("#dispute_case_due_date_id").on('keyup change',function(){
        if(this.value != ""){
            $("#dispute_case_due_date_id").css("border","");
        }
    });

    $('#dispute_case_case_number').on('blur',function(){
        if(this.value != ""){
            var case_number=this.value;
            $.ajax({
                url: '/admins/disputes/get_case_number',
                type: 'GET',
                data: {case_number: case_number},
                success: function(data) {
                    if(data.case_number!=""){
                        $("#dispute_case_case_number").addClass("dispute_case_after")
                        $("#duplicate-btn").prop('disabled',true);
                        $("#case_number_error").show();
                    }else{
                        $("#dispute_case_case_number").removeClass("dispute_case_after")
                        $("#duplicate-btn").prop('disabled',false);
                        $("#case_number_error").hide();
                    }
                },
                error: function(data){
                    $("#dispute_case_case_number").css("border","");
                }
            });
        }
    });

    $("#status_selected_dispute_case").keypress(function(){
        if(this.value != ""){
            $("#status_selected_dispute_case").css("border","");
        }
    });

    $("#dispute_case_dispute_type_selection").keypress(function(){
        if(this.value != ""){
            $("#dispute_case_dispute_type_selection").css("border","");
        }
    });

    $("#dispute_case_submit_button").click(function(){
        var dispute_case_dispute_type_selection = $("#dispute_case_dispute_type_selection").val();
        var status_selected_dispute_case = $("#status_selected_dispute_case").val();
        var dispute_case_case_number = $("#dispute_case_case_number").val();
        var dispute_amount = $("#dispute_detail_amount").val();
        var dispute_case_recieved_date_id = $("#dispute_case_recieved_date_id").val();
        var dispute_case_due_date_id = $("#dispute_case_due_date_id").val();
        var dispute_case_reason = $("#table_text_field_transaction_dispute").val();
        var is_error = false
        if(dispute_case_case_number == null || dispute_case_case_number == "") {
            $("#dispute_case_case_number").css("border","1px solid red");
            is_error = true
        }
        else if (dispute_case_dispute_type_selection == null || dispute_case_dispute_type_selection == ""){
            $("#dispute_case_dispute_type_selection").css("border","1px solid red");
            is_error = true
        } else if (dispute_case_recieved_date_id == null || dispute_case_recieved_date_id == ""){
            $("#dispute_case_recieved_date_id").css("border","1px solid red");
            is_error = true
        } else if (dispute_case_due_date_id == null || dispute_case_due_date_id == ""){
            $("#dispute_case_due_date_id").css("border","1px solid red");
            is_error = true
        } else if (dispute_case_reason == null || dispute_case_reason == ""){
            $("#table_text_field_transaction_dispute").css("border","1px solid red");
            $("#dispute_case_dispute_type_selection").css("border","1px solid #ccc");
            is_error = true
        } else if(dispute_amount == null || dispute_amount == "") {
            $("#dispute_detail_amount").css("border","1px solid red");
            $("#table_text_field_transaction_dispute").css("border","1px solid #ccc");
            $("#dispute_case_dispute_type_selection").css("border","1px solid #ccc");
            is_error = true
        } else if(status_selected_dispute_case == null || status_selected_dispute_case == "") {
            $("#status_selected_dispute_case").css("border","1px solid red");
            $("#table_text_field_transaction_dispute").css("border","1px solid #ccc");
            $("#dispute_case_dispute_type_selection").css("border","1px solid #ccc");
            $("#dispute_detail_amount").css("border","1px solid #ccc");
            is_error = true
        } else{
            $("#dispute_case_form").submit();
        }
        if(is_error){
            $('#duplicate-btn').attr('disabled',false)
        }
    });

    $('.datepicker_disp').datepicker({
        format: 'mm/dd/yyyy',
        minDate: '12/01/2018',
        autoclose: true,
        singleDatePicker: true,
        parentEl: $("#date_parent12")
    });
    // $("#ui-datepicker-div").remove();
    if(value_present(dispute_case)){
        if(!value_present(dispute_case.id)){
            $('.datepicker_disp').datepicker('setDate','today');
        }
    }

    $("#dispute_btn").css("pointer-events", "all");

    $("#disputeCaseModal").on("shown.bs.modal", function(){
        $(".dispute-edit-btn").html("<span class='btn btn-primary btn-sm'>Edit</span>");
        $('.custom-cover').removeClass('pointer-none-only');

    })

    $(document).on("click", ".file-del-btn", function () {
        var file_value = $(this).data("value");
        var file_key = $(this).data("key");
        store_transaction(file_value,file_key)
    })

    $(document).on("click", ".edit-save-btn", function () {
        var note_count = $(this).data("note_count");
        edit_without_save(note_count);
    })

    $(document).on("click", ".delete-note-btn", function () {
        var index_id = $(this).data("index_id");
        var id = $(this).data("result_id");
        delete_notification_note(index_id, id);
    })

    $(document).on('click','.delete_notification',function () {
        var id= $('.index_value').val();
        store_id(id)
    })
});
function store_transaction(value_id,value_index) {
    index=value_id
    image_ids.push(index)
    console.log(image_ids);
    $('#files_section #upload_file_list_'+value_index).replaceWith();

};

// function store_image(){
//     // alert($image_store)
//     // for (i = 0; i < $image_store.length; i++){
//     console.table($image_store)
//     $image_store[0].ajaxSubmit({
//         dataType: 'json',
//         error: function (result) {
//             console.log('error')
//         },
//         complete: function(data, XMLHttpRequest, textStatus, response , result) {
//             var image = JSON.parse(data.responseText);
//             $('#files_section').append('<div class="col-md-10" id=upload_file_list_'+attachment_count+'>'+image.doc.add_image_file_name+'<div style="float:right;"><a style="color:black;" href='+image.image_url+' download='+image.doc.add_image_file_name+' target="_blank">Download</a>'+'<a style="color:red;padding:0px 0px 0px 5px;" data-remote="true" href='+"/admins/images/"+image.doc.id+' download='+image.doc.add_image_file_name+'" target="_blank" ></a></div></div>');
//             attachment_count += 1;
//         }
//     });
//
//     // }
//
// };

function delete_transaction_attachment(value_id) {
    $.ajax({
        url: "/admins/images/delete_image?id="+value_id,
        type: 'DELETE',

        complete: function(result){
            console.log(value_index);
            $('#files_section #upload_file_list_'+value_id).replaceWith();
        }
    });
};

function save_notes(){
    $.ajax({
        url: "/admins/disputes/dispute_notification_note",
        type: 'POST',
        data: {all_data:$add_notifi},
        success: function(result){
            if(result.message){
                $('#warning_showing_span').text(result.message);
            }else{
                setTimeout(function(){
                    $(document.body).addClass( 'modal-open' );
                }, 500);
            }
        }
    });
}

function  store_id(id) {
    index_value=id
    $del_notifi.push(id)
    $("#notifications_section #transaction_note"+id).replaceWith();
}

function delete_notification_note(value_id) {
    $.ajax({
        url: "/admins/disputes/delete_dispute_notification_notes",
        type: 'POST',
        data: {note_id:value_id},
        success: function(result){
            $("#notifications_section #transaction_note"+value_id).replaceWith();
            // }
        }
    });
}

function edit_without_save(index) {
    $('#EditWithoutSaveModal').modal();
    object = $add_notifi[index - 1];
    $("#edit_dispute_note_form_title_field").val('').val(object.title);
    $("#edit_dispute_note_form_text_field").val('').val(object.name);
    $("#index").val('').val(index);
}