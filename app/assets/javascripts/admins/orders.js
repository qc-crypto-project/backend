$(document).ready(function () {

    $('#query_risk_eval_').multipleSelect({
        placeholder: "Select Status",
        selectAll: false
    });

    var lt = $('.timeUtc-orders').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
});

$(document).ready(function () {
    $('input[name="query[order_date_time]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[order_date_time]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[order_date_time]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[order_date_time]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
        parentEl: $("#order_date")

    });

    $('input[name="query[order_date_time]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});

$(document).ready(function () {
    $('input[name="query[shipment_date_time]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[shipment_date_time]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[shipment_date_time]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[shipment_date_time]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
        parentEl: $("#shipment_date")

    });

    $('input[name="query[shipment_date_time]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});

$(document).ready(function () {
    $('input[name="query[delivery_date_time]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[delivery_date_time]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[delivery_date_time]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });

    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[delivery_date_time]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
        parentEl: $("#delivery_date")

    });

    $('input[name="query[delivery_date_time]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
});

$(document).on('change','#tx_filter_shippment',function () {
    $("#search_form_shippment").submit();
})