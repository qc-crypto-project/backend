

$(document).ready(function () {
    $('#datatable-keytable1').DataTable({
        responsive: true,
        "sScrollX": false,
        "sscrollX": false,
        "searching": true,
        "order": [[ 1, "desc" ]],
        "columnDefs": [{ "orderable": false }],
        "bInfo": false,
        "bPaginate": false,

    });
    var lt= $('.timeUtc-user').each(function (i,v) {
        // var gmtDateTime =  moment(v.innerText.replace('UTC', '')).utc().format('MM-DD-YYYY HH:mm:ss')
        var gmtDateTime = moment.utc(v.innerText,'MM-DD-YYYY HH:mm:ss');
//           var gmtDateTime = moment.utc(v.innerText).toDate();
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    if ($('#count-value').val() == "1"){
        $("#container"+$('#count-value').val()).css("display","none");
        $("#commission"+$('#count-value').val()).css("display","none");
    }
    const urlParams = new URLSearchParams(window.location.search);
    //=====================================     start _iso_signup.html.erb
    $('#alert-message-top').fadeOut(5000);
    // validateUserForm('iso',"notuser");

    //=====================================     end _iso_signup.html.erb
    $(document).on('change','#tx_filter',function () {
        $('#specfic_transaction_filter').submit();
    })
    $(document).on('change','.tx_filter',function () {
        $('#iso_a_affi_wallet_details').submit();
    })
    //=====================================     start iso_signup.html.erb
    validateIsoForm('iso');

    $('#ssn').mask('000-00-0000');
    $('#tx').mask('00-0000000');
    $('.dt-buttons').css('display','none')
    //error message show remove
    $('#alert-message-top').fadeOut(5000);
    //=====================================     end iso_signup.html.erb

    //=====================================     start iso_buyrate.html.erb
    // ::: ALERT ::::: CAUTION  ::: function in this section are share among iso_buyrate.html.erb and _iso_buyrate.html.erb

    $('#high-risk-tab-button, #low-risk-tab-button').click(function () {
        var id = $(this).attr('id');
        if(id==='high-risk-tab-button'){
            $(this).removeClass('btn-default').addClass('btn-primary');
            $('#low-risk-tab-button').removeClass('btn-primary').addClass('btn-default');
            $('#container0,#commission0').show();
            $('#container1,#commission1').hide();
            $("#apply1").removeAttr('required');
            $(".apply").attr('required', true);
        }
        else{
            $(this).removeClass('btn-default').addClass('btn-primary');
            $('#high-risk-tab-button').removeClass('btn-primary').addClass('btn-default');
            $('#container0,#commission0').hide();
            $('#container1,#commission1').show();
            $(".apply").removeAttr('required');
            $("#apply1").attr('required', true);
        }
    });

    // $('.buyrate_fields').bind('keyup change', function(){
    //     this.value = this.value.replace(/[^0-9\.]/g,'');
    // });

    $(".buyrate_fields").on('click', function () {
        $(this).select();
    })

    $( function() {
        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _create: function () {
                this._super();
                this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
            },
            _renderMenu: function (ul, items) {
                var that = this,
                    currentCategory = "";
                $.each(items, function (index, item) {
                    var li;
                    if (item.category != currentCategory) {
                        ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                        currentCategory = item.category;
                    }
                    li = that._renderItemData(ul, item);
                    if (item.category) {
                        li.attr("aria-label", item.category + " : " + item.label);
                    }
                });
            }
        });
    });

    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term));
        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };

    $(document).on('change','.super_detailss1',function(){
        content = $(this).attr("class");
        idd = content.replace('super_detailss','');
        if($(this).is(':checked')){
            $('#'+content+'').show();
            $('.iso_radio_button1').show()
            $('.gbox_radio1').prop("checked", true);
        }
        else{
            $(".iso_radio0").prop( "checked", false );
            $(".gbox_radio0").prop( "checked", false );
            $('#'+content+'').hide();
            $('.iso_radio_button1').hide()
        }
    });
    $(document).on('change','.super_detailss0',function(){
        content = $(this).attr("class");
        idd = content.replace('super_detailss','');
        if($(this).is(':checked')){
            $('#'+content+'').show();
            $('#iso_radio_button').show()
            $('#gbox_radio').prop("checked", true);
        }
        else{
            $("#iso_radio").prop( "checked", false );
            $("#gbox_radio").prop( "checked", false );
            $('#'+content+'').hide();
            $('#iso_radio_button').hide()
        }
    });

    //=====================================     end iso_buyrate.html.erb
    //=====================================     start iso_documention.html.erb
    $(function () {
        var count = 0;
        $('input[type="file"]').on('change', function (event) {
            var id = $(this).attr('id');
            $(`#${id}-error`).hide();
            $(`.col-md-8.${id} .row .col-md-12 img`).remove();
            var files = event.target.files;
            var files_count = files.length;
            $.each(files, function (index, value) {
                var image = value;
                var reader = new FileReader();
                reader.onload = function (file) {
                    var img = new Image();
                    img.id = id+'_'+(count=count+1);
                    img.src = file.target.result;
                    if (files_count===1){
                        $(`.col-md-8.${id} .row .col-md-12`).html(img);
                    }
                    else{
                        $(`.col-md-8.${id} .row .col-md-12`).append(img);
                        $(`.col-md-8.${id} .row .col-md-12 img`).addClass('padding-right-2');
                    }
                }
                reader.readAsDataURL(image);
            });
            if (files_count === 0) {
                $(`.col-md-8.${id} .row .col-md-12 img`).remove();
            }
        });
    });

    $("a.fa.fa-trash.fa-font.panel-heading").click(function (e) {
        var image_id = $(this).attr('id');
        e.preventDefault();
        if (confirm('Are you sure to Delete?')) {
            $.ajax({
                url: "/admins/isos/document_delete",
                type: 'GET',
                data: {image_id: image_id},
                success: function (data) {
                    if (data === true) {
                        $('img.' + image_id).hide();
                        $('a.' + image_id).hide();
                        $('.' + image_id).next().hide();
                        $('#cover-spin').fadeOut();
                    }
                },
                error: function (data) {
                    $('#cover-spin').fadeOut();
                }
            });
        }
        else {
            $('#cover-spin').fadeOut();
        }
    });

    //=====================================     end iso_documentation.html.erb

    //=====================================     start _add_super_user_details.html.erb

    $(".amount_field").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $("#cvv").inputmask('Regex', {regex: "^[0-9]+$"});
    //$(".name_field").inputmask('Regex', {regex:"^[a-zA-Z0-9. ]*$" });

//=====================================     end _add_super_user_details.html.erb


//=====================================     start _iso_buyrate_commission_0.html.erb


    // $(".number_only").inputmask('Regex', {regex: "^[0-9]+$"});
    $(document).on('keyup', ".number_only", function () {
        $(this).inputmask('Regex', {regex: "^[0-9]+$"});
    })
    $(document).on('keyup', ".amount_field", function () {

        $(this).inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    })
    $(".number_only").inputmask('Regex', {regex: "^[0-9]+$"});
    var commission_lenght = $('#last_tier').val();
    $('.apply').val(commission_lenght);
    $(document).on('onkeypress', ".commission-input-1", function (e) {

        return e.keyCode != 13;
    });
    $(document).on('onkeypress', ".commission-input-2", function (e) {

        return e.keyCode != 13;
    });
    $(document).on('onkeypress', ".tier-2-input", function (e) {

        return e.keyCode != 13;
    });
    $('#add-col').click(function(){
        $(".error-1").text("");
        $("#submit_btn").attr("disabled", false);
        $("#save_btn").attr("disabled", false);
        var cols=$('.apply').val();

        if(cols > 0 && cols!=''){
            $('#action-form-submit').prop("disabled",false);
        }
        else{
            $('#action-form-submit').prop("disabled",true);
        }
        var i;
        var count=$('#commissionTable tbody tr:first td').length;
        if(count > 0){
            $("#commissionTable").find("thead tr:first th:gt(0)").remove();
            $("#commissionTable").find("tbody tr:first td:gt(0)").remove();
            $("#commissionTable").find("tbody tr:last td:gt(0)").remove();
        }
        if(cols==1){
            count=1;
            $('#commission-head').find('tr').append(`<th class="text-align-center"> Tier ${count}</th>`);
            $('#commission-body').find('tr:first').append(`<td><div class="row"><div class="col-md-6"><input class="sales_level_min form-control" type="text" id="sales_level_min_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_min]" value="0.0" readonly required/></div><div class="col-md-6"><input class="sales_level_max form-control pull-right infinity" type="text" id="sales_level_max_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_max]" value="" placeholder="infinity" readonly/></div></div><span class="display-none error-span sales_level_max_${count}"></span></td>`);
            $('#commission-body').find('tr:last').append(`<td><div class="row"><div class="col-md-4"></div><div class="col-md-4 me text-center"><input class="commission-input-1 commission-error-st commission_iso number_only" maxlength="3" type="text" id="commission_iso_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][commission_iso]"/><span class="display-none commission_slash">/</span><input class="commission-input-2 commission-error-st-1 commission_merchant number_only" maxlength="3" type="text" id="commission_merchant_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][commission_merchant]" required/></div><div class="col-md-4"></div></div><p class="display-none text-center error-span margin_top_10px commission_merchant_${count}">Percentage Must be equal to 100</p></td>`);

            //$('.commission_dollar').prop('required',false);
        }
        else{
            count=1;
            for(i=0;i < cols;i++){
                $('#commission-head').find('tr').append(`<th class="comm-head-st"> Tier ${count}</th>`);
                if(count==1){
                    $('#commission-body').find('tr:first').append(`<td class="max-width-field"><div class="row"><div class="col-md-6"><input class="tier-2-input sales_level_min form-control" type="text" id="sales_level_min_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_min]" value="0.0" readonly required/></div><div class="col-md-6"><input class="sales_level_max amount_field form-control pull-right" type="text" id="sales_level_max_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_max]" required/></div></div><span class="display-none error-span sales_level_max_${count}"></span></td>`);
                }
                else if(i==(cols-1)){
                    $('#commission-body').find('tr:first').append(`<td class="max-width-field"><div class="row"><div class="col-md-6"><input class="sales_level_min form-control" type="text" id="sales_level_min_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_min]" readonly required/></div><div class="col-md-6"><input class="sales_level_max amount_field form-control pull-right infinity" type="text" id="sales_level_max_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_max]" value="" placeholder="infinity" readonly/></div></div><span class="display-none error-span sales_level_max_${count}"></span></td>`);
                }
                else{
                    $('#commission-body').find('tr:first').append(`<td class="max-width-field"><div class="row"><div class="col-md-6"><input class="sales_level_min form-control" type="text" id="sales_level_min_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_min]" readonly required/></div><div class="col-md-6"><input class="sales_level_max amount_field form-control pull-right" type="text" id="sales_level_max_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][sales_level_max]" required/></div></div><span class="display-none error-span sales_level_max_${count}"></span></td>`);
                }
                $('#commission-body').find('tr:last').append(`<td class="max-width-field"><div class="row"><div class="col-md-4"></div><div class="col-md-4 me text-center"><input class="commission-error-st commission_iso number_only commission-input-1" maxlength="3" type="text" id="commission_iso_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][commission_iso]"/><span class="display-none commission_slash">/</span><input class="commission-error-st-1 commission_merchant number_only commission-input-2" maxlength="3" type="text" id="commission_merchant_${count}" name="user[fees_attributes][0][commission][commission][tier${count}][commission_merchant]" required/></div><div class="col-md-4"></div></div><p class="display-none text-center error-span margin_top_10px commission_merchant_${count}">Percentage Must be equal to 100</p></td>`);
                count++;
            }
            //$('.commission_dollar').prop('required',false);
        }
        if(cols == 3){
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-3');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-6');
        }
        if (cols == 4) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-2');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-8');
        }
        if (cols == 5) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-1');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-8');
        }
        if (cols > 5) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-2');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-8');
        }
        if(cols > 3){
            $('.commission_merchant').css('margin-top','5%');
            $('.commission_iso').css('margin-top','5%');
        }
    });
    $(document).on('keyup','.sales_level_max',function(){
        var id=$(this).attr('id');
        id = id.split('_max_')[1];
        $('.buyrate_btn_save').prop('disabled',false);
        if($(this).val() != '' && $(this).val()!= 0){
            var val=parseFloat($(this).val())+0.1;
        }
        if(!isNaN(val))
        {
            $(`#sales_level_min_${Number(id)+1}`).val(val.toFixed(2));
        }
    });
    $(document).on('blur','.sales_level_max',function(){
        var id = $(this).attr('id');
        var count_id = id.split('_max_')[1];
        var $this=$(this);
        var thisVal=eval($this.val());
        var nextVal=eval($(`#sales_level_max_${Number(count_id)+1}`).val());
        var previousVal = eval($(`#sales_level_max_${Number(count_id) - 1}`).val());
        $('.buyrate_btn').prop('disabled',false);

        if($('#commission-body td').length > 1 && nextVal!='' && Number(thisVal) >= Number(nextVal)){
            $this.parentsUntil('td').nextAll('td').find('input').val('');
            $this.next('input').val('');
            $this.val('');
            $('.error-span.'+id).addClass('display-inline').text('Value must be less than next max range');
            $('.error-span.'+id).fadeIn();
            setTimeout(function(){
                $('.error-span.'+id).fadeOut();
            }, 2000);
        }
        if($(this).attr('id')=='sales_level_max_1'){
            if ( Number($('#sales_level_max_1').val()) == 0){
                $('.error-span.'+id).addClass('display-inline').text('Value must be greater than 0');
                $('.error-span.'+id).fadeIn();
                setTimeout(function(){
                    $('.error-span.'+id).fadeOut();
                }, 2000);
            }
            return;
        }
         var previousVal_to_comp =  (Number(previousVal)+ 0.1 ).toPrecision(4);
        if(previousVal == null || previousVal_to_comp >= Number(thisVal) || thisVal==null){
            if($('#commission-body td').length > 1 && nextVal != null){
                $this.parentsUntil('td').nextAll('td').find('input').val('');
                $this.next('input').val('');
            }
            $this.val('');
            // changing value of next field to zero starts
            var thisIndex = $(this).index('input:text');
            var next = thisIndex + 1;
            $('input:text').eq(next).val('0.0');
            // changing value of next field to zero ends
            if( $(this).next().text() == "" ){
                $('.error-span.'+id).text('Value must be greater than previous max range');
                $('.error-span.'+id).fadeIn();
                setTimeout(function(){
                    $('.error-span.'+id).fadeOut();
                }, 2000);
            }

        }
    });

    $(document).on('keyup focus','.commission_iso',function(e){
        if($(this).val().length == 3 || ($(this).val().length == 1 && $(this).val() == 0)){
            $(this).siblings('.commission_slash').show();
            $(this).focusout;
            $(this).siblings('input').focus();
        }
        else if(e.keyCode == 191 || e.keyCode == 111) {
            $(this).siblings('.commission_slash').show();
            $(this).focusout;
            $(this).siblings('input').focus();
        }
        else{
            $(this).siblings('.commission_slash').hide();
        }
    });
    $(document).on('keyup focus','.commission_merchant',function(e){
        if(e.keyCode == 8){
            if($(this).val()==''){
                $(this).siblings('.commission_slash').hide();
                $(this).focusout;
                $(this).siblings('input').focus();
            }
        }
        if($(this).val()['0'] == 0 && $(this).val().length == 2){
            $(this).val(0);
        }
        if($(this).siblings('input').val() == '') {
            $(this).siblings('input').focus();
        }
    });
    $(document).on('blur','.commission_merchant',function () {
        var $this = $(this);
        var id = $(this).attr('id');
        var iso_commission=parseFloat($(this).siblings('input').val());
        var merchant_commission=parseFloat($(this).val());
        if(!isNaN(iso_commission) && !isNaN(merchant_commission) )
        {
            var total_commission=iso_commission+merchant_commission;
            if(total_commission != 100){
                $('.error-span.' + id).show();
                setTimeout(function () {
                    $('.error-span.' + id).fadeOut();
                }, 2000);
                $(this).siblings('.commission_slash').hide();
                $(this).siblings('input').val('');
                $(this).val('');
            }
        }
        else {
            $(this).siblings('.commission_slash').hide();
            $(this).siblings('input').val('');
            $(this).val('');
        }
    });

    $(document).on('click','#submit_btn',function () {
        var low_apply = $('#apply1').val();
        var highly_apply = $('.apply').val();
        if (highly_apply === '' && low_apply !== ""){
           $(".apply").removeAttr("required")
        }else if(highly_apply !== '' && low_apply === ""){
            $("#apply1").removeAttr("required")
        }
    });

//=====================================     end _iso_buyrate_commission_0.html.erb

//=====================================     start _iso_buyrate_commission_1.html.erb

    $(".number_only").inputmask('Regex', {regex: "^[0-9]+$"});
    $(".amount_field").inputmask('Regex', {regex: "^[0-9,]{1,99}(\\.\\d{1,2})?$"});


    var commission1_lenght = $('#last_tier1').val();
    $('#apply1').val(commission1_lenght);
    $('#add-col1').click(function () {
        $(".error-2").text("");
        $("#submit_btn").attr("disabled", false);
        $("#save_btn").attr("disabled", false);
        var cols1 = $('#apply1').val();

        if (cols1 > 0 && cols1 != '') {
            $('#action-form-submit').prop("disabled", false);
        }
        else {
            $('#action-form-submit').prop("disabled", true);
        }
        var i1;
        var count1 = $('#commissionTable1 tbody tr:first td').length;
        if (count1 > 0) {
            $("#commissionTable1").find("thead tr:first th:gt(0)").remove();
            $("#commissionTable1").find("tbody tr:first td:gt(0)").remove();
            $("#commissionTable1").find("tbody tr:last td:gt(0)").remove();
        }
        if (cols1 == 1) {
            count1 = 1;
            $('#commission-head1').find('tr').append(`<th class="text-align-center"> Tier ${count1}</th>`);
            $('#commission-body1').find('tr:first').append(`<td><div class="row"><div class="col-md-6"><input class="sales_level_min1 form-control" type="text" id="sales_level_min1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_min]" value="0.0" readonly required/></div><div class="col-md-6"><input class="sales_level_max1 amount_field form-control pull-right infinity" type="text" id="sales_level_max1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_max]" value="" placeholder="infinity" readonly/></div></div><span class="error-span display-none sales_level_max1_${count1}"></span></td>`);
            $('#commission-body1').find('tr:last').append(`<td><div class="row"><div class="col-md-4"></div><div class="col-md-4 me text-center"><input class="commission_iso1 commission-iso number_only" maxlength="3" onkeypress="return event.keyCode != 13;" type="text" id="commission_iso1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][commission_iso]"/><span class="display-none commission_slash_1">/</span><input class="commission_merchant1 commission-merchant number_only" maxlength="3" type="text" id="commission_merchant1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][commission_merchant]" required/></div><div class="col-md-4"></div></div><p class="text-center display-none error-span margin_top_10px commission_merchant1_${count1}">Percentage Must be equal to 100</p></td>`);

            //$('.commission_dollar').prop('required',false);
        }
        else {
            count1 = 1;
            for (i = 0; i < cols1; i++) {
                $('#commission-head1').find('tr').append(`<th class="text-align-center"> Tier ${count1}</th>`);
                if (count1 == 1) {
                    $('#commission-body1').find('tr:first').append(`<td><div class="row"><div class="col-md-6"><input class="sales_level_min1 form-control" type="text" id="sales_level_min1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_min]" value="0.0" readonly required/></div><div class="col-md-6"><input class="sales_level_max1 amount_field form-control pull-right" type="text" id="sales_level_max1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_max]" required/></div></div><span class="error-span display-none sales_level_max1_${count1}"></span></td>`);
                }
                else if (i == (cols1 - 1)) {
                    $('#commission-body1').find('tr:first').append(`<td><div class="row"><div class="col-md-6"><input class="sales_level_min1 form-control" type="text" id="sales_level_min1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_min]" readonly required/></div><div class="col-md-6"><input class="sales_level_max1 amount_field form-control pull-right infinity" type="text" id="sales_level_max1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_max]" value="" placeholder="infinity" readonly/></div></div><span class="error-span display-none sales_level_max1_${count1}"></span></td>`);
                }
                else {
                    $('#commission-body1').find('tr:first').append(`<td><div class="row"><div class="col-md-6"><input class="sales_level_min1 form-control" type="text" id="sales_level_min1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_min]" readonly required/></div><div class="col-md-6"><input class="sales_level_max1 amount_field form-control pull-right" type="text" id="sales_level_max1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][sales_level_max]" required/></div></div><span class="error-span display-none sales_level_max1_${count1}"></span></td>`);
                }
                $('#commission-body1').find('tr:last').append(`<td><div class="row"><div class="col-md-4"></div><div class="col-md-4 me text-center"><input class="commission_iso1 commission-iso number_only" maxlength="3" onkeypress="return event.keyCode != 13;" type="text" id="commission_iso1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][commission_iso]"/><span class="display-none commission_slash_1">/</span><input class="commission_merchant1 commission-merchant number_only" maxlength="3" type="text" id="commission_merchant1_${count1}" name="user[fees_attributes][1][commission][commission][tier${count1}][commission_merchant]" required/></div><div class="col-md-4"></div></div><p class="text-center display-none error-span margin_top_10px commission_merchant1_${count1}">Percentage Must be equal to 100</p></td>`);
                count1++;
            }
            // //$('.commission_dollar').prop('required',false);
        }

        if (cols1 == 3) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-3');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-6');
        }
        if (cols1 == 4) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-2');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-8');
        }
        if (cols1 == 5) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-1');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-10');
        }
        if (cols1 > 5) {
            $('.col-md-4.me.text-center').siblings().removeClass().addClass('col-md-2');
            $('.col-md-4.me.text-center').removeClass('col-md-4').addClass('col-md-8');
        }
        if (cols1 > 3) {
            $('.commission_merchant1').css('margin-top', '5%');
            $('.commission_iso1').css('margin-top', '5%');
        }
    });
    $(document).on('keyup', '.sales_level_max1', function () {
        var id = $(this).attr('id');
        id = id.split('_max1_')[1];
        $('.buyrate_btn').prop('disabled',false);
        if ($(this).val() != '' && $(this).val() != 0) {
            var val = parseFloat($(this).val()) + 0.1;
        }
        if (!isNaN(val)) {
            $(`#sales_level_min1_${Number(id) + 1}`).val(val.toFixed(2));
        }
    });
    $(document).on('blur', '.sales_level_max1', function () {
        var id = $(this).attr('id');
        var count_id = id.split('_max1_')[1];
        $('.buyrate_btn_save').prop('disabled',false);
        var $this = $(this);
        var thisVal = eval($this.val());
        var nextVal = eval($(`#sales_level_max1_${Number(count_id) + 1}`).val());
        var previousVal = eval($(`#sales_level_max1_${Number(count_id) - 1}`).val());

        if ($('#commission-body1 td').length > 1 && nextVal != '' && Number(thisVal) >= Number(nextVal)) {
            $this.parentsUntil('td').nextAll('td').find('input').val('');
            $this.next('input').val('');
            $this.val('');
            $('.error-span.' + id).attr('style', 'display:inline').text('Value must be less than next max range');
            $('.error-span.' + id).fadeIn();
            setTimeout(function () {
                $('.error-span.' + id).fadeOut();
            }, 2000);
        }
        if ($(this).attr('id') == 'sales_level_max1_1') {
            return;
        }

        if (previousVal == null || Number(previousVal) >= Number(thisVal) || thisVal == null) {
            if ($('#commission-body1 td').length > 1 && nextVal != null) {
                $this.parentsUntil('td').nextAll('td').find('input').val('');
                $this.next('input').val('');
            }
            $this.val('');
            // changing value of next field to zero starts
            var thisIndex = $(this).index('input:text');
            var next = thisIndex + 1;
            $('input:text').eq(next).val('0.0');
            // changing value of next field to zero ends
            $('.error-span.' + id).text('Value must be greater than previous max range');
            $('.error-span.' + id).fadeIn();
            setTimeout(function () {
                $('.error-span.' + id).fadeOut();
            }, 2000);
        }
    });

    $(document).on('keyup', '.commission_iso1', function (e) {
        if ($(this).val().length == 3 || ($(this).val().length == 1 && $(this).val() == 0)) {
            $(this).siblings('.commission_slash_1').show();
            $(this).focusout;
            $(this).siblings('input').focus();
        }
        else if(e.keyCode == 191 || e.keyCode == 111) {
            $(this).siblings('.commission_slash_1').show();
            $(this).focusout;
            $(this).siblings('input').focus();
        }
        else {
            $(this).siblings('.commission_slash_1').hide();
        }
    });
    $(document).on('keyup focus', '.commission_merchant1', function (e) {
        if (e.keyCode == 8) {
            if ($(this).val() == '') {
                $(this).siblings('.commission_slash_1').hide();
                $(this).focusout;
                $(this).siblings('input').focus();
            }
        }
        if($(this).val()['0'] == 0 && $(this).val().length == 2){
            $(this).val(0);
        }
        if ($(this).siblings('input').val() == '') {
            $(this).siblings('input').focus();
        }
    });
    $(document).on('blur', '.commission_merchant1', function () {
        var $this = $(this);
        var id = $(this).attr('id');
        var iso_commission = parseFloat($(this).siblings('input').val());
        var merchant_commission = parseFloat($(this).val());
        if (!isNaN(iso_commission) && !isNaN(merchant_commission)) {
            var total_commission = iso_commission + merchant_commission;
            if (total_commission != 100) {
                $('.error-span.' + id).show();
                setTimeout(function () {
                    $('.error-span.' + id).fadeOut();
                }, 2000);
                $(this).siblings('.commission_slash_1').hide();
                $(this).siblings('input').val('');
                $(this).val('');
            }
        }
        else {
            $(this).siblings('.commission_slash_1').hide();
            $(this).siblings('input').val('');
            $(this).val('');
        }
    });


    //=====================================     end _iso_buyrate_commission_1.html.erb


    //=====================================     start system_fee.html.erb

        // $(".buyrate_fields").inputmask('Regex', {regex: "^\\d+\\.\\d{0,2}$"});
    $(".btnSaves").on('click', function (e) {
        if($('#user_system_fee_check_limit_type').val() == "" && $("#isos_check_limit").val() != ""){
            $(".isos_check_limit_error").show()
            e.preventDefault()
        }else{$(".isos_check_limit_error").hide()}

        if($('#user_system_fee_push_to_card_limit_type').val() == "" && $("#isos_p2c_limit").val() != ""){
            $(".isos_p2c_limit_error").show()
            e.preventDefault()
        }else{$(".isos_p2c_limit_error").hide()}

        if($('#user_system_fee_ach_limit_type').val() == "" && $("#isos_ach_limit").val() != ""){
            $(".isos_ach_limit_error").show()
            e.preventDefault()
        }else{$(".isos_ach_limit_error").hide()}

    })
    //=====================================     end system_fee.html.erb

    //=====================================     start _edit.html.erb
    function validate_company_edit_email1() {
        if(($('#iso_hidden_current_email').val() != $('#email_field1').val()) && ($('#email_field1').length > 0 ) ) {
            $.ajax({
                url: "/admins/companies/form_validate?email_address=" + ($('#email_field1').val()),
                type: 'GET',
                data: {user_id: $('#iso_hidden_field').val()},
                success: function (data) {
                    $('#email_field1').css('border', '')
                    $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
                    if (($("#phone_number_label_field1").text() == "Phone number") && ($('#password_field1').text() == "Confirm Password")) {
                        $('#submit_edit_button1').prop('disabled', false);
                    }
                    if ($('#email_field1')) {
                        if (data['value'] == true) {
                            $('#email_label_field1').replaceWith("<label id='email_label_field1' class='string optional' for='Email'	>Email<span class='red-clr'> This email has already been registered. Try again.</span></label>")
                            $('#email_field1').css('border', '1px solid red')
                            $('#submit_edit_button1').prop('disabled', true);
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        } else {
            $('#email_field1').css('border', '')
            $('#email_label_field1').replaceWith('<label id="email_label_field1" class="string optional" for="Email">Email</label>')
            if (($("#phone_number_label_field1").text() == "Phone number") && ($('#password_field1').text() == "Confirm Password")) {
                $('#submit_edit_button1').prop('disabled', false);
            }
        }
    }

    function validate_phone_edit_n0umber1() {
        if ( ($('#iso_hidden_current_phone_no').val() != $('#phone_number_field1').val()) && ($('#phone_number_field1').length > 0 ) ) {
            $.ajax({
                url: "/admins/companies/form_validate?phone_number=" + ($('#phone_number_field1').val()),
                type: 'GET',
                data: {user_id: $('#iso_hidden_field').val()},
                success: function (data) {
                    $('#phone_number_field1').css('border', '')
                    $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='company_Phone number'>Phone number</label>")
                    if (($("#company_email_label_field1").text() == "Email") && ($('#password_field1').text() == "Confirm Password")) {
                        $('#submit_edit_button1').prop('disabled', false);
                    }
                    if ($('#phone_number_field1')) {
                        if (data['value'] == true) {
                            $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='company_Phone number'>Phone number <span class='red-clr'> This phone number has already been registered. Try again.</span></label>")
                            $('#phone_number_field1').css('border', '1px solid red')
                            $('#submit_edit_button1').prop('disabled', true);
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        } else {
            $('#phone_number_field1').css('border', '');
            $('#phone_number_label_field1').replaceWith("<label id='phone_number_label_field1' class='string optional' for='company_Phone number'>Phone number</label>");
            if (($("#email_label_field1").text() == "Email") && ($('#password_field1').text() == "Confirm Password")) {
                $('#submit_edit_button1').prop('disabled', false);
            }
        }
    }

    $('#password_confirmation1').keyup(function() {
        var pass = $('#password1').val();
        var repass = $('#password_confirmation1').val();
        if (pass != repass) {
            $('#password_field1').replaceWith("<label id='password_field1' class='string optional' for='company_Phone number'>Confirm Password <span class='red-clr'> Passwords do not match. Try again.</span></label>");
            $('#submit_edit_button1').prop('disabled', true);
        } else {
            $('#password_field1').replaceWith("<label id='password_field1' class='string optional' for='company_Phone number'>Confirm Password</label>");
            if (($("#email_label_field1").text() == "Email") && ($("#phone_number_label_field1").text() == "Phone number")) {
                $('#submit_edit_button1').prop('disabled', false);
            }
        }
    });
    //=====================================     end _edit.html.erb

    //=====================================     start _new.html.erb
    $(document).on('change',".apply", function () {
        if ($("#sales_level_min_1").length == 0){
            $(".error-1").text("Sales level and Commission Split are required!")
            $("#submit_btn").attr("disabled", "disabled");
            $("#save_btn").attr("disabled", "disabled");
        }
    })
    $("#apply1").on('change', function () {
        if ($("#sales_level_min1_1").length == 0){
            $(".error-2").text("Sales level and Commission Split are required!")
            $("#submit_btn").attr("disabled", "disabled");
            $("#save_btn").attr("disabled", "disabled");
        }
    })
    //=====================================     end _new.html.erb

    //=====================================     start details.html.erb
    $('#details-high-risk-tab-button').click(function(){
        if($(this).hasClass('btn-default')){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
        }
        if($('#details-low-risk-tab-button').hasClass('btn-success')){
            $('#details-low-risk-tab-button').removeClass('btn-success');
            $('#details-low-risk-tab-button').addClass('btn-default');
        }
        $('.low-risk').hide();
        $('.high-risk').show();
    });
    $('#details-low-risk-tab-button').click(function(){
        if($(this).hasClass('btn-default')){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
        }
        if($('#details-high-risk-tab-button').hasClass('btn-success')){
            $('#details-high-risk-tab-button').removeClass('btn-success');
            $('#details-high-risk-tab-button').addClass('btn-default');
        }
        $('.high-risk').hide();
        $('.low-risk').show();
    });
    //=====================================     end details.html.erb

    //=====================================     start edit.html.erb

    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    var pass_score = 0;
    $("#user_password").focusin(function () {
        $('.strength').css('display', "block");
    });

    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            $('.strength').css('display', "none");
        }
    });

    $('#user_password ,#user_password_confirmation').on('keyup', function () {
        if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password</label>");
            if($('#email_label_field').text() == "Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        } else {
            $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password<span class='red-clr'> Passwords do not match. Try again.</span></label>");
            $(':input[type="submit"]').prop('disabled', true);
        }
        if ($('#user_password').val() == "" && $('#user_password_confirmation').val() == ""){
            $('#message').html('');
            if($('#email_label_field').text() == "Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        }
    });
    $("#user_password").on("keyup", function() {
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <=20){
            $('#length').attr('checked',true);
        }else{
            $('#length').attr('checked',false);
        }
        if (/[0-9]/.test(password)){
            $('#one_number').attr('checked',true);
        }
        else{
            $('#one_number').attr('checked',false);
        }
        if (/[A-Z]/.test(password)){
            $('#upper_case').attr('checked',true);
        }
        else{
            $('#upper_case').attr('checked',false);
        }
        if (/[a-z]/.test(password)){
            $('#lower_case').attr('checked',true);
        }
        else{
            $('#lower_case').attr('checked',false);
        }
    });

    // $('#submit_edit_button').on("click",function(event) {
    //     var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}/;
    //     if (!re.test($('#user_password').val())){
    //         $('#password_Error').html('');
    //         $('#user_password').after('<div id= "password_Error" class="red-clr"> password validation requires capital letter, small letter and a number </div>')
    //         return false;
    //     }
    // });

    var validate_company_edit_email = function() {
        $.ajax({
            url: "/admins/companies/form_validate?email_address="+($('#email_field').val()),
            type: 'GET',
            data: {user_id:$('#company_hidden_field').val() },
            success: function(data) {
                $('#email_field').css('border','')
                $('#email_label_field').replaceWith('<label id="email_label_field" class="string optional" for="Email">Email</label>')
                if($("#phone_number_label_field").text() == "Phone number") {
                    $('#submit_edit_button').prop('disabled', false);
                }
                if($('#email_field')){
                    if(data['value'] == true){
                        $('#email_label_field').replaceWith("<label id='email_label_field' class='string optional' for='Email'	>Email<span class='red-clr'> Error - Email Already Registered </span></label>")
                        $('#email_field').css('border','1px solid red')
                        $('#submit_edit_button').prop('disabled', true);
                    }
                }
            },
            error: function(data){
                console.log(data);
            }
        });
    }
    $(document).on('change','.admins_country_field',function () {
        if($(this).val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $(this).val(),owner_field: 'change' },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#user_profile_attributes_state").empty();
                    if(Object.keys(states).length > 0 ){
                        $.each(states,function (index, value) {
                            $("#user_profile_attributes_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                        });
                        $('.admins_states_field').attr("required",true);
                        $('.admins_states_field').attr("disabled",false);


                    }else{
                        $('.admins_states_field').attr("required",false);
                        $('.admins_states_field').attr("disabled",true);

                    }

                }
            });
        }
    });
    var validate_phone_edit_number = function() {
        $.ajax({
            url: "/admins/companies/form_validate?phone_number="+($('#phone_number_field').val()),
            type: 'GET',
            data: {user_id:$('#company_hidden_field').val() },
            success: function(data) {
                $('#phone_number_field').css('border','')
                $('#phone_number_label_field').replaceWith("<label id='phone_number_label_field' class='string optional' for='company_Phone number'>Phone number</label>")
                $('#submit_edit_button').prop('disabled', false)
                if($("#company_email_label_field").text() == "Email"){
                    $('#submit_edit_button').prop('disabled', false);
                }
                if($('#phone_number_field')){
                    if(data['value'] == true){
                        $('#phone_number_label_field').replaceWith("<label id='phone_number_label_field' class='string optional' for='company_Phone number'>Phone number <span class='red-clr'>Error - Phone# Already Registered</span></label>")
                        $('#phone_number_field').css('border','1px solid red')
                        $('#submit_edit_button').prop('disabled', true);
                    }
                }
            },
            error: function(data){
                console.log(data);
            }
        });
    }
    validateUserForm('iso');

    // $('#submit_btn').on('click',function () {
    //     setTimeout( function(){
    //         $("span.has-error").css('color','red');
    //
    //         setTimeout( function(){
    //             $("span.has-error").fadeOut("slow");
    //         },2000);
    //     },100);
    //
    // });
    //=====================================     end edit.html.erb
    // =====================================     start wallet_details.html.erb
    $(document).on('click','.description_function',function () {
        var id= $(this).data();
        myFunction(id)
    })

    function show_transaction(url,trans,wallet_id) {
        $('#cover-spin').show(0);
        window.location.href = url + "?trans="+ trans +"&wallet_id="+ wallet_id +"&offset=" + encodeURIComponent(moment().local().format('Z'))
    }
    $('[data-toggle="popover"]').popover({container: 'body'});

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    $(document).mouseup(function (e)
    {
        var container = $(".tx_id"); // YOUR CONTAINER SELECTOR
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
    $( "#tx_idd" ).click(function() {
        // $('#welcome').show()
        $('#tx_id').toggle();
    });
    function myFunction(id) {
        $('#tx_id'+id.id).toggle();

    }
    // =====================================     end wallet_details.html.erb

    //=====================================     start index.html.erb

    $('.modalwalabutton').click(function(){
        $('.myModal').modal();
    });

    $('#isos-datatable').DataTable({
        "sScrollX": false,
        "scrollX": false,
        "sSearch": true,
        "ordering": true,
        "processing": true,
        "searching": true,
        "serverSide": true,
        "ajax": $('#isos-datatable').data('source')
    });


    //=====================================     end edit.html.erb
    //=====================================     start new.html.erb
     validateUserForm('iso');
    $('#buyrate-text').addClass('lightblue-clr');
    //=====================================     end new.html.erb
    //=====================================     start show.html.erb
    $('#datatable-iso').DataTable({
        //        responsive: true,
        "sScrollX": false,
        "scrollX": false
    });
    $('#datatable-keytable').DataTable({
        //        responsive: true,
        "responsive": true,
        "sScrollX": false,
        "sSearch": false,
        "ordering": true,
        "searching": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "order": []
    });
    //=====================================     end show.html.erb

    // super_details_show

    if($("#param_action").val() == "show" && $("#param_id").val() == "iso_buyrate"){
        if($("#is_baked0").val() == "true"){
            $('.super_details0').show()
        }
        else{
            $('.super_details0').hide()
        }
        if($("#is_baked1").val() == "true"){
            $('.super_details1').show()
        }
        else{
            $('.super_details1').hide()
        }
    } else{
        $('.super_details0').hide();
        $('.super_details1').hide();
    }

    var n = 0;
    if (gon.index_session != "" ? true : false) {
        n = gon.index_session != "" ? gon.index_session : 0;
        if (n > 1) {
            $('#a_' + (n - 1)).find('i:nth-child(1)').removeClass('grey-color').addClass("green-clr");
            $('#a_' + (n - 2)).find('i:nth-child(1)').removeClass('grey-color').addClass("green-clr");
            $('#a_' + (n - 3)).find('i:nth-child(1)').removeClass('grey-color').addClass("green-clr");
            $('#a_' + n).find('i:nth-child(1)').removeClass('grey-color').addClass("blu-clr");
            $('#a_' +gon.step_index).find('i:nth-child(1)').removeAttr('style').addClass('blu-color');
        }
    }

    $(".fa-stack.fa-lg").on('click', function (e) {
        if ($(this).children('i').hasClass("grey-color")) {
            e.preventDefault();
        }
    });

    $(document).on('click', '.btn.butt-color.button-radius', function (e) {
        if ($('#phone_number').val() !== undefined) {
            if ($('#phone_msg').hasClass('text-danger')){
                e.preventDefault();
                $('#cover-spin').fadeOut();
            }
        }
        if($('#user_form_id').val()=== undefined) {

            if($('#phone_number').val()!== undefined)
            {
                if ($('#phone_number').val() === '') {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Please Add Phone Number');
                    e.preventDefault();
                }
                else if ($('#phone_number').val().length > 0 && $('#phone_number').val().length < 6) {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Length should be greater than 5');
                    e.preventDefault();
                }
                else {
                    if ($('#phone_msg').hasClass('has-error')) {
                        e.preventDefault();
                    }
                }
            }
        }
        else {
            if ($('#phone_number').val() !== undefined) {
                if ($('#phone_number').val() === '') {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Please Add Phone Number');
                    e.preventDefault();
                }
                else if ($('#phone_number').val().length > 0 && $('#phone_number').val().length < 6) {
                    $('#phone_msg').removeClass("text-success").addClass("has-error").text('Length should be greater than 5');
                    e.preventDefault();
                }
                else {
                    if ($('#phone_msg').hasClass('has-error')) {
                        e.preventDefault();
                    }
                }
            }
        }
        if($("#iso_new_record").val() == "true" || $("#agent_new_record").val() == "true" || $("#affiliate_new_record").val() == "true"){
            // validateIsoForm('iso');
            if (!$('#isoform').valid()) {
                // $('#cover-spin').fadeOut();
            }
            setTimeout(function () {
                $('#cover-spin').fadeOut();
            }, 2000);
        }else if(($("#iso_present").val() == "true" && $("#iso_new_record").val() == "false") || ($("#agent_present").val() == "true" && $("#iso_new_record").val() == "false") || ($("#affiliate_present").val() == "true" && $("#affiliate_new_record").val() == "false")) {
            validateEditIsoForm('edit_user');
            if (!$('.edit_user').valid()) {
                $('#cover-spin').fadeOut();
            }
            setTimeout(function () {
                $('#cover-spin').fadeOut();
            }, 2000);
        }
    });

    validateEditAgentAffiliateForm('edit_user');

    var code = 0;
    var country="";
    var errorMap = ["Invalid Number", "Invalid Selected Country", "Too short", "Too long", "Invalid Number"];
    $('#phone_number').intlTelInput({
        formatOnDisplay: false,
        formatOnInit: true,
        separateDialCode: true,
        initialCountry: "us",
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            code=Object.values(selectedCountryData)[2];
            $('#phone_code').val(code);
            $('#phone_number').val('');
            $('#phone_number').removeAttr('autocomplete');
            $('#phone_number').removeAttr('title')
            space_remove(code.length);
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['bg','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae','hr'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });

    $(document).on('click blur paste keyup','#phone_number', function (e) {
        $("#phone_number").inputFilter(function(value) {
            return /^\d*$/.test(value); });
        if ($(this).val().length === 0) {
            reset();
        }
        else if ($(this).val() >= country.length && countrycode()) {
            this.value = $(this).val().replace(/[^0-9\.]/g, '');
            var code = $('.selected-dial-code').text();
            if ($(this).val().length !== 0 && $(this).val().length > 5) {
                $('.phone_class').attr("disabled",true)
                $.ajax({
                    url: '/admins/companies/form_validate',
                    data: {phone_number: code + $(this).val(),user_id: $('#user_form_id').val()},
                    method: 'get',
                    success: function (result) {
                        if (result== true) {
                            $('.phone_class').attr("disabled",false)
                            var form_id=$('#user_form_id').val()
                            if ($('#user_form_id').val()=== undefined || $('#user_form_id').val()==='' || $('#user_form_id').val()===form_id){
                                $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
                                if (code == "+1")
                                {
                                    var number=  $('#phone_number').val();
                                    if (number.length>10)
                                    {
                                        $('#phone_msg').removeClass('text-success').addClass('has-error').text('').text('Too long');
                                    }
                                }
                            }
                            else{
                                $('#phone_msg').removeClass('has-error').addClass('text-success').text('').text('');
                            }

                        }
                        else {
                            $('.phone_class').attr("disabled",false)
                            $('#phone_msg').removeClass('text-success').addClass('has-error').text('').text('User Already Exist!');
                        }
                    }
                });
            }
        }
        else {
            if ($(this).val().length > 0) {
                var errorCode = $('#phone_number').intlTelInput("getValidationError");
                if ($(this).val().length === 1){ errorCode = 2 }
                $('#phone_msg').removeClass("new_user text-success").addClass("has-error").text(errorMap[errorCode]);
            }
        }
    });
    var reset = function () {
        $('#phone_msg').removeClass('has-error text-success').text('');
    };

    var pass_score = 0;
    $("#user_password").focusin(function () {
        $('.strength').css('display', "block");
    });
    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true){
            $('.strength').css('display', "none");
        }
        if($(this).val().length == ""){
            $('.strength').css('display', "none");
        }
    });

    $("#user_password , #user_password_confirmation").on('keyup', function () {
        if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            $('#pass_error').replaceWith("<div class='with-errors' id='pass_error'></div>");
            if($('#email_label_field').text() == "Contact Email Address") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        } else {
            if ( $('#user_password_confirmation').val() != "" && ($('#user_password').val() != $('#user_password_confirmation').val()) ){
                //$('#pass_error').replaceWith("<div class='help-block with-errors' id='pass_error'><span class='red-clr'>Confirme Password and Password should be same!</span></div>");
            }
            //$(':input[type="submit"]').prop('disabled', true);
        }
        if ($('#user_password').val() == "" && $('#user_password_confirmation').val() == ""){
            $('#message').html('');
            if($('#email_label_field').text() == "Contact Email Address") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        }
    });

    // $("#user_password").on("keyup", function() {
    //     var password = $(this).val();
    //     $("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
    //     if (password.length >= 8 && password.length <=20){
    //         $('#length').attr('checked',true);
    //     }else{
    //         $('#length').attr('checked',false);
    //     }
    //     if (/[0-9]/.test(password)){
    //         $('#one_number').attr('checked',true);
    //     }
    //     else{
    //         $('#one_number').attr('checked',false);
    //     }
    //     if (/[A-Z]/.test(password)){
    //         $('#upper_case').attr('checked',true);
    //     }
    //     else{
    //         $('#upper_case').attr('checked',false);
    //     }
    //     if (/[a-z]/.test(password)){
    //         $('#lower_case').attr('checked',true);
    //     }
    //     else{
    //         $('#lower_case').attr('checked',false);
    //     }
    // });

    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                }
            });
        };
    }(jQuery));

    // add super user details js
    //  var count = $(this).data("count");
    $(".add_baked_users0").on('click', function () {
        var count = $(this).data("count");
        next_baked_form(count)
    });
    $(".add_baked_users1").on('click', function () {
        var count = $(this).data("count");
        next_baked_form(count)
    });
    $(document).on('click', ".remove_baked_users", function () {
        var id = $(this).attr("id");
        remove_baked_form(id)
    });
    $(document).on('keyup', '.for_super_users', function () {
        var id = $(this).attr("id");
        for_super_users(id);
    })
    // $(document).on('keyup', '#user_'+count+'1', function () {
    //     for_super_users('user_'+count+'1')
    // })
    // $(document).on('keyup', '#user_'+count+'2', function () {
    //     for_super_users('user_'+count+'2')
    // })
    // $(document).on('keyup', '#user_'+count+'3', function () {
    //     for_super_users('user_'+count+'3')
    // })
    $(document).on('keypress', '.number_only', function (e) {
        return e.keyCode != 13;
    })
    $(document).on("click", ".show-cover-spin", function (e) {
        if ($('#isoform').valid()){
            $('#cover-spin').show(0);
            $('#create_iso').addClass('pointer-none')
        }else{
            $('#cover-spin').hide(0);
        }
    })

    $(document).on("click",".btnSaves",function(e) {
        // $("#cover-spin").fadeIn();
        error=''
        var error_message = ""
        $('.error_class').text('');
        $('#phone_number').removeAttr('autocomplete')
        $('.input-field').each(function(){
            var field_id = $(this).attr('id');
            var hidden_field = $(this).next().attr('id');
            var error_field = $(this).next().next().attr('id');
            if ($(".super_detailss0").is(':checked')){
                if($("#"+field_id).parent().prev().find('input').val() == "" && $("#"+field_id).val() !== ""){
                    error_message = "Select baked user";
                    $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("Please Select the baked user");
                    $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).css('color', 'red');
                    $('.buyrate_btn').prop('disabled','disabled');
                    e.preventDefault();
                    return false
                }else{
                    $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("");
                    $('.buyrate_btn').prop('disabled',false);
                }
            }
            if ($(".super_detailss1").is(':checked')){
                if($("#"+field_id).parent().prev().find('input').val() == "" && $("#"+field_id).val() !== ""){
                    error_message = "Select baked user";
                    $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("Please Select the baked user");
                    $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).css('color', 'red');
                    e.preventDefault();
                    $('.buyrate_btn').prop('disabled','disabled');
                    return false
                }else{
                    $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("");
                    $('.buyrate_btn').prop('disabled',false);
                }
            }
        });
        $('.first-user').each(function() {
                var field_id = $(this).attr('id');
                var error_field = $(this).next().next().attr('id');
                if ($(".super_detailss0").is(':checked')) {
                    if ($("#" + field_id).val() == "" && $('.error-msg').text() == "") {
                        if (field_id != "user_11") {
                            error_message = "Select baked user";
                            $($("#" + error_field)).text("Please Select the baked user");
                            $($("#" + error_field)).css('color', 'red');
                            $('.buyrate_btn').prop('disabled','disabled');
                            e.preventDefault();
                            return false
                        }
                    }
                }
                if ($(".super_detailss1").is(':checked')) {
                    if ($("#" + field_id).val() == "" && $('.error-msg').text() == "") {
                        if (field_id != "user_01") {
                            error_message = "Select baked user";
                            $($("#" + error_field)).text("Please Select the baked user");
                            $($("#" + error_field)).css('color', 'red');
                            $('.buyrate_btn').prop('disabled','disabled');
                            e.preventDefault();
                            return false
                        }
                    }
                }
            });

        var baked_profit_split1 = $(".super_detailss1").is(":checked");
        var baked_profit_split = $(".super_detailss0").is(":checked");
        if(baked_profit_split || baked_profit_split1){
            $(".for_super_users").each(function (e) {
                var id = $(this).attr("id");
                var percent = $("." + id + "_percent").val();
                var dollar = $("." + id + "_dollar").val();
                var baked_user = $(this).val()
                if (value_present(baked_user)) {
                    if (!value_present(percent) && !value_present(dollar)) {
                        if (percent == "" && dollar == "") {
                            error_message = "Must enter % or $";
                            $('.buyrate_btn').prop('disabled','disabled');
                            $("#" + id + "_percent_error").text("Must enter % or $");
                            e.preventDefault();
                            $('.buyrate_btn').prop('disabled','disabled');
                            return false
                        } else {
                            error_message = "Value should be greater than 0";
                            $("#" + id + "_percent_error").text("Value should be greater than 0");
                            $('.buyrate_btn').prop('disabled','disabled');
                            e.preventDefault();
                            $('.buyrate_btn').prop('disabled','disabled');

                            return false
                        }
                    } else {
                        if ($(this).val() == "" && (value_present(percent) || value_present(dollar)) ) {
                            error_message = "Select baked user";
                            $("#" + id + "_error").text("Please Select the baked user");
                            $("#" + id + "_error").css('color', 'red');
                            $('.buyrate_btn').prop('disabled','disabled');
                            e.preventDefault();
                            return false
                        } else {
                            $("#" + id + "_percent_error").html("");
                            $('.buyrate_btn').prop('disabled',false);
                        }
                    }
                 }
                    else {
                    if ( $(this).val() == "" && (value_present(percent) || value_present(dollar))) {
                        error_message = "Select baked user";
                        $("#" + id + "_error").text("Please Select the baked user");
                        $("#" + id + "_error").css('color', 'red');
                        $('.buyrate_btn').prop('disabled','disabled');
                        e.preventDefault();
                        return false
                    }
                    else
                    {
                        $('.buyrate_btn').prop('disabled',false);
                    }
                    }
            })
        }
        error = error + error_message;
        // $('.user-1').each( function () {
        //     var field_id = $(this).attr('id');
        //     var hidden_field = $(this).next().attr('id');
        //     var error_field = $(this).next().next().attr('id');
        //     if($('#'+ field_id).val()=='' && $('#details_switch').is(':checked')){
        //         if(error!=''){
        //             error=error+","
        //         }
        //
        //         error=error
        //     }
        // });

        if(!$('.edit_user').valid()){
            if(error!=''){
                $("#cover-spin").fadeOut();
                $("#error").fadeIn();
                $('#error').html('Please fill fields: ').append(error);
                $("#error").fadeOut(30000);
                return false;

            }

            return false
        }
        else{
            error_se = $('.error_class').text();
            if (error_se != ""){
                return false
            }
            else {
                return true
            }
        }

    });

    $('.input-field').on('keyup',function(e){
        if ($(this).val() == ""){
            $('.buyrate_btn').prop('disabled',false);
            $(this).next().next().text('');
        }
        if ($(this).val() != ""){
            $(this).next().next().text('Please select an existing user');
            $(this).next().next().addClass('red');
            $('.buyrate_btn').prop('disabled','disabled');
            e.preventDefault();
        }
    });

    $('.sub-field').on('keyup',function(){
       var field_name = $(this).attr('id').split('_');
       if ($(this).val() != "")
       {
           $('.buyrate_btn').prop('disabled',false);
           if (field_name[0] == 'percent')
           {
               $(this).parent().next().next().text('');
           }
           else {
               $(this).parent().next().text('');
           }
       }
    });


    // $(".btnSave1").on('click',function() {
    //     var error_message = ""
    //     $('.input-field').each(function(e){
    //         var field_id = $(this).attr('id');
    //         var hidden_field = $(this).next().attr('id');
    //         var error_field = $(this).next().next().attr('id');
    //         if ($("#details_switch").is(':checked')){
    //             if($("#"+field_id).parent().prev().find('input').val() == "" && $("#"+field_id).val() !== ""){
    //                 error_message = "Select baked user";
    //                 $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("Please Select the baked user");
    //                 $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).css('color', 'red');
    //                 e.preventDefault();
    //             }else{
    //                 $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("");
    //             }
    //         }
    //     });
    //     $('.first-user').each(function(e){
    //         var field_id = $(this).attr('id');
    //         var error_field = $(this).next().next().attr('id');
    //         if ($("#details_switch").is(':checked')){
    //             if($("#"+field_id).val() == "" && $('.error-msg').text() == ""){
    //                 error_message = "Select baked user";
    //                 $($("#"+error_field)).text("Please Select the baked user");
    //                 $($("#"+error_field)).css('color', 'red');
    //                 e.preventDefault();
    //             }
    //         }
    //     });
    //
    // });
    // $(document).on("click",".btnSaves",function () {
    //     var baked_profit_split = $("#details_switch").is(":checked");
    //     if(baked_profit_split){
    //         $(".input-field").each(function (e) {
    //             var id = $(this).attr("id");
    //             var percent = $("." + id + "_percent").val();
    //             var dollar = $("." + id + "_dollar").val();
    //             var baked_user = $(this).val()
    //             if (baked_user != "") {
    //                 if (!value_present(percent) && !value_present(dollar) && dollar != undefined && dollar != undefined) {
    //                     if (percent == "" && dollar == "") {
    //                         $("#" + id + "_percent_error").html("Must enter % or $");
    //                     } else {
    //                         $("#" + id + "_percent_error").html("Value should be greater than 0");
    //                     }
    //                     e.preventDefault();
    //                 } else {
    //                     if ($(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
    //                         $("#" + id + "_error").text("Please Select the baked user");
    //                         $("#" + id + "_error").css('color', 'red');
    //                         e.preventDefault();
    //                     } else {
    //                         $("#" + id + "_percent_error").html("")
    //                         $("#" + $("#" + id).parent().prev().find('div').attr('id')).text("");
    //                     }
    //                 }
    //             }else {
    //                 if ( $(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
    //                     $("#" + id + "_error").text("Please Select the baked user");
    //                     $("#" + id + "_error").css('color', 'red');
    //                     e.preventDefault();
    //                 }}
    //         })
    //     }
    // });
    //.....................spexfic_wallet_transactiion.html.erb................................//
    var select_reason,reason_detail,transaction_detail,tran_Id,destination,source,refund_fee,total_amount,partial_amt;
    var check1 = false, check2 = false;
    $('.next_transactions').click(function () {
        $('#cover-spin').fadeIn();
    });
    $(document).on("click", ".refundModalDialog", function () {
        $('#reason_select option[value="0"]').prop("selected",true);
        $('#partialamount').val('');
        $('#reason_detail').val('');
        $('#reason_detail').hide();
        $('.refund_fee').text('0');
        $('.modal-footer a button').prop('disabled',true);
        transaction_detail=JSON.stringify($(this).data('transaction'));
        parsed_transction=JSON.parse(transaction_detail);
        tran_Id = parsed_transction.id;
        parent_id=parsed_transction.parent_id;
        destination=parsed_transction.destination;
        source=parsed_transction.source;
        $.ajax({
            url: '/admins/refund_check',
            type: 'GET',
            data: {trans_id: tran_Id},
        })
            .done(function () {
                $.ajax({
                    url: '<%= get_partialAmount_admins_path %>',
                    type: 'GET',
                    data: {trans_id: tran_Id,parent_id: parent_id,destination: destination,source: source},
                })
                    .done(function (res) {
                        if(res.partial_amount==""){
                            $('#partial_error').text("No Transaction Found");
                            $('#partial_error').fadeIn();
                            setTimeout(function(){
                                $('#partial_error').fadeOut()
                            }, 2000);
                        }else{
                            $('#partialamount').val(parseFloat(res.partial_amount).toFixed(2));
                            total_amount=parseFloat(res.partial_amount);
                            partial_amt=total_amount;
                            last4="";
                            if(parsed_transction["card"]){
                                if(parsed_transction["card"]["last4"]){
                                    last4=parsed_transction["card"]["last4"];
                                }
                            }else{
                                if(parsed_transction["reference"]["previous_issue"]){
                                    if(parsed_transction["reference"]["previous_issue"]["last4"]){
                                        last4=parsed_transction["reference"]["previous_issue"]["last4"];
                                    }
                                }
                            }
                            $('#refund_user').text(last4);
                            refund_fee=res.fee;
                            var fee=refund_fee+parseFloat(res.partial_amount);
                            $('.refund_fee').text(parseFloat(fee).toFixed(2));
                            $('#confirm_refund_amount').text(parseFloat(res.partial_amount).toFixed(2));
                        }
                    });
            });

    });
    $('#partialamount').change(function(){
        select_reason=$('#reason_select').val();
        var partial_amount = $('#partialamount').val();
        partial_amt=partial_amount;
        if (partial_amount != "0") {
            $.ajax({
                url: '<%= get_partialAmount_admins_path %>',
                type: 'GET',
                data: {trans_id: tran_Id,destination: destination,source: source},
            })
                .done(function (res) {
                    var partial= res.partial_amount - parseFloat(partial_amount);
                    if (partial < 0){
                        $('#partial_error').text("Amount is greater than amount to refund");
                        $('#partial_error').fadeIn();
                        setTimeout(function(){
                            $('#partial_error').fadeOut()
                        }, 2000);

                        $('.modal-footer a button').prop('disabled',true);
                        check1 = false;
                    }
                    else {
                        $('#partial_error').text("");
                        check1  =true;
                        refund_fee=res.fee;
                        var fee=refund_fee+parseFloat(partial_amount);
                        $('.refund_fee').text(parseFloat(fee).toFixed(2));
                        $('#confirm_refund_amount').text(parseFloat(partial_amount).toFixed(2));
                    }
                    if (check1 && check2) {
                        if(select_reason!=0){
                            $('.modal-footer a button').prop('disabled',false);
                        }
                    }
                });
        }
        updateRefundPostLink();
    });

    function updateRefundPostLink() {
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        reason_detail=$("#reason_select option:selected").text();
        if (select_reason==0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false;
        } else if (select_reason!=4 && select_reason!=0){
            $('#reason_detail').val('');
            $('#reason_detail').hide();
            $('#footerConfirmLink').attr('href',"<%= refund_transaction_admins_path %>"+"?js=js&refund_fee="+refund_fee+"&reason=" + select_reason + "&partial_amount=" + partial_amount + "&reason_detail=" + encodeURIComponent(reason_detail) + "&transaction=" + tran_Id);
            if(total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
            check2 = true;
        } else{
            $('#reason_detail').show();
            $('.modal-footer a button').prop('disabled',true);
            check2 = false
        }
        if (check1 && check2) {
            if (total_amount>=partial_amt){
                $('.modal-footer a button').prop('disabled',false);
            }
        }
    }

    $('#reason_select').change(function(){
        updateRefundPostLink();
    });

    $('#reason_detail').keyup(function () {
        reason_detail=$(this).val();
        var select_reason=$('#reason_select').val();
        var partial_amount=$('#partialamount').val();
        if (reason_detail=='' || reason_detail == null){
            $('.modal-footer a button').prop('disabled', true);
        } else{
            $('#footerConfirmLink').attr('href',"<%= refund_transaction_admins_path %>"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + tran_Id);
//            $('#footerConfirmLink').attr('href',"<%#= refund_transaction_admins_path %>"+"?js=js&refund_fee="+refund_fee+"&reason="+select_reason+"&partial_amount="+partial_amount+"&reason_detail="+encodeURIComponent(reason_detail)+"&transaction=" + encodeURIComponent(transaction_detail));
            if(total_amount >= partial_amount){
                $('#refundModalFooter a button').prop('disabled',false);
            }
        }
    });


    // $( function() {
    //     $( document ).tooltip();
    // } );

    $("#amount").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $(function() {
        $('input[name="query[timestamp]"]').on('hide.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        $('.datepicker').daterangepicker({
            autoUpdateInput: false,
            "maxSpan": {"month": 1},
            "parentEl": $("#date_parent")
        });
    });
    $('.datepicker').daterangepicker({
        autoUpdateInput: false,
        "maxSpan": {"month": 1}
    });
    ///..........................iso_agent_affiliate.html.erb..............
    $(function() {
        $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
            // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
        });
        $('input[name="query[timestamp]"]').on('apply.daterangepicker', function(ev, picker) {
            var startDate = picker.startDate;
            var endDate = picker.endDate;
            $('input[name="query[timestamp]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
            // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
        });
        $('input[name="query[timestamp]"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });
    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[date]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY'
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
    });
    $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
        $('input[name="query[date]"]').val('');
    });

    /*
     $(document).ready(function () {
     $(function() {
     <% if params[:query].present? %>
     <% if params[:query][:date].blank? %>
     $('input[name="query[date]"]').val('');
     <% end %>
     <% else %>
     $('input[name="query[date]"]').val('');
     <% end %>
     $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
     $(this).val('');
     });
     $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
     var startDate = picker.startDate;
     var endDate = picker.endDate;
     $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
     // alert("New date range selected: '" + startDate.format('YYYY-MM-DD') + "' to '" + endDate.format('YYYY-MM-DD') + "'");
     });
     });
     var nowDate = new Date();
     var newdate = (nowDate.setDate(nowDate.getDate()));
     var nd = new Date(newdate);
     $('input[name="query[date]"]').daterangepicker({
     locale: {
     format: 'MM/DD/YYYY'
     },
     "maxSpan": {
     "days": 90
     },
     "maxDate": nd,
     });
     $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
     $('input[name="query[date]"]').val('');
     });
     });*/

    $(function() {

        $('#query_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false
        });
        $('#query_merchant_txn_type_').multipleSelect({
            placeholder: "Select Type",
            selectAll: false
        });
        $('#query_gateway_').multipleSelect({
            placeholder: "Select Gateway",
            selectAll: false
        });
    });
    $(document).ready(function () {
        $("[type='checkbox']").css("margin-top","-1px").css("margin-right","15px");
    });

    $(document).ready(function(){
        $('.number').keypress(function(event) {
            var $this = $(this);
            if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                ((event.which < 48 || event.which > 57) &&
                (event.which != 0 && event.which != 8))) {
                event.preventDefault();
            }

            var text = $(this).val();
            if ((event.which == 46) && (text.indexOf('.') == -1)) {
                setTimeout(function() {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                        $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                }, 1);
            }

            if ((text.indexOf('.') != -1) &&
                (text.substring(text.indexOf('.')).length > 2) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 2)) {
                event.preventDefault();
            }
        });

        $('.number').bind("paste", function(e) {
            var text = e.originalEvent.clipboardData.getData('Text');
            if ($.isNumeric(text)) {
                if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                    e.preventDefault();
                    $(this).val(text.substring(0, text.indexOf('.') + 3));
                }
            }
            else {
                e.preventDefault();
            }
        });
    });
//..............................profile.html.erb............//
    $(document).ready(function () {
        $('div.toggle.btn.btn-danger.off').removeAttr('style');
        $('div.toggle.btn.btn-success').removeAttr('style');
    });
    $('#block').change(function () {
        $('div.toggle.btn.btn-danger.off').removeAttr('style');
        $('div.toggle.btn.btn-success').removeAttr('style');
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
            $.ajax({
                url: "<%= update_status_isos_path %>",
                type: 'get',
                data: { user_id: '<%= @user.id %>' },
                success: function(data) {
                    swal("Updated!", "User updated successfully", "success");
                },
                error: function(error){
                    swal("Cancelled", "An unexpected error occured :)", "error");
                    if($('#block').parent().hasClass('btn-danger')){
                        $('#block').parent().removeClass('btn-danger').removeClass('off');
                        $('#block').parent().addClass('btn-success');
                    }else if($('#block').parent().hasClass('btn-success')) {
                        $('#block').parent().removeClass('btn-success');
                        $('#block').parent().addClass('btn-danger').addClass('off');
                    }
                }
            });
        } else {
            if($('#block').parent().hasClass('btn-danger')){
                $('#block').parent().removeClass('btn-danger').removeClass('off');
                $('#block').parent().addClass('btn-success');
            }else if($('#block').parent().hasClass('btn-success')) {
                $('#block').parent().removeClass('btn-success');
                $('#block').parent().addClass('btn-danger').addClass('off');
            }
            $(this).prop("checked", !$(this).is(':checked'));
        }
    });
    });
    /*
     $('.two_step').click(function () {
     swal({
     title: "Are you sure?",
     type: "warning",
     showCancelButton: true,
     closeOnConfirm: false,
     closeOnCancel: true
     }).then((isConfirm) => {
     if (isConfirm.value === true) {
     $.ajax({
     url: "<%= update_two_step_isos_path %>",
     type: 'get',
     data: { user_id: '<%= @user.id %>' },
     success: function(data) {
     if($('.two_step_text').text() == "On"){
     $('.two_step_text').text('').text('Off');
     $('.two_step_color').css('color','red');
     }else if($('.two_step_text').text() == "Off"){
     $('.two_step_text').text('').text('On');
     $('.two_step_color').css('color','green');
     }
     swal("Updated!", "User updated successfully", "success");
     },
     error: function(error){
     swal("Cancelled", "An unexpected error occured :)", "error");
     }
     });
     } else {
     }
     });
     });
     $('#reset').click(function () {
     swal({
     title: "Are you sure?",
     type: "warning",
     showCancelButton: true,
     closeOnConfirm: false,
     closeOnCancel: true
     }).then((isConfirm) => {
     if (isConfirm.value === true) {
     $('#cover-spin').show(0);
     $.ajax({
     url: "<%= admins_merchant_reset_path(:user) %>",
     type: 'post',
     data: { user: {email: '<%= @user.email %>'},csrf_token: "c7058a2ee1eb40b3c06330cf2a7be92d",from_admin: true },
     success: function(data) {
     $('#cover-spin').fadeOut("slow");
     swal("Reset!", "Reset instructions sent successfully", "success");
     },
     error: function(error){
     $('#cover-spin').fadeOut("slow");
     swal("Cancelled", "An unexpected error occured :)", "error");
     }
     });
     } else {
     }
     });
     });*/

    function show_transaction(url,trans,wallet_id) {
        $('#cover-spin').show(0);
        window.location.href = url + "?trans="+ trans +"&wallet_id="+ wallet_id +"&offset=" + encodeURIComponent(moment().local().format('Z'))
    }

    $('#phone_number').on('keyup', function () {
        var phone = $("#phone_number").val();
        var phone_code = $(".phone-box .selected-dial-code").text();
        var reg = new RegExp('^[0-9]+$');
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        $.ajax({
            url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
            type: 'GET',
            data: {phone_number: phone_number},
            success: function (result) {
                if(result){
                    $(".phone-error").text('')
                    $('.wizard-submit-btn').prop('disabled', false);
                }else{
                    $('.wizard-submit-btn').prop('disabled', 'disabled');
                    $(".phone-error").text('Invalid format or phone number is already taken!')
                }
            },
            error: function (error) {
                console.log(error)
            }
        });



    });
    if( $('#ach_international_hidden').val() == "true" ){

        $("#ach_international").prop("checked",true);
    }else {
        $("#ach_international").prop("checked",false);
    }
});// end of $(document).ready(function () { above the all


function next_baked_form(risk) {
    count = $(".super_details"+risk).children().length;
    $.ajax({
        url:"/admins/isos/next_baked",
        type: "GET",
        data: {count: count,risk: risk}
    });
}

function remove_baked_form(id) {
    console.log("testing")
    id  = id.trim();
    $("#"+id).parent().parent().parent().parent().parent().parent().parent().remove();
}

function for_super_users(field_id) {
    var all_keyword = $("#" + field_id).val();
    var id = '#' + $("#" + field_id).attr('id');
    var user = '#' + field_id;
    var user_hidden = '#' + field_id + 'Hidden';
    all_keyword = all_keyword.split('-');
    var length = all_keyword.length;
    all_keyword = all_keyword[length-1];
    all_keyword = all_keyword.trim();
    if (all_keyword != '') {
        getUsers(all_keyword, id, user, user_hidden, field_id)
    }
}

function getUsers(all_keyword, id, user, user_hidden, field_id) {
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword, users: "for_users"},
                success: function (data) {
                    if (data.users != "") {
                        response(data.users);
                    }
                    else {
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function (e, ui) {
            $(user_hidden).val(ui.item.id);
            $(user).val(ui.item.value);
            var end_id = user.slice(1)
            $(user + "_percent").addClass(end_id + "_percent");
            $(user + "_dollar").addClass(end_id + "_dollar");
            ValidateFields(e,user,user_hidden);
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function countrycode() {
    return $('#phone_number').intlTelInput("isValidNumber");
}

function space_remove(code) {
    switch (code)
    {
        case 1:
            $('#phone_number').addClass('phone-length-1');
            break;
        case 2:
            $('#phone_number').addClass('phone-length-2');
            break;
        case 3:
            $('#phone_number').addClass('phone-length-3');
            break;
        case 4:
            $('#phone_number').addClass('phone-length-4');
            break;
    }
}

function validateFiles(inputFile) {
    var maxExceededMessage = "This file exceeds the maximum allowed file size (10 MB)";
    // var extErrorMessage = "Only image file with extension: .jpg, .jpeg, .gif or .png is allowed";
    // var allowedExtension = ["jpg", "jpeg", "gif", "png"];

    // var extName;
    var maxFileSize = $(inputFile).data('max-file-size');
    var sizeExceeded = false;
    // var extError = false;

    $.each(inputFile.files, function() {
        if (this.size && maxFileSize && this.size > parseInt(maxFileSize)) {sizeExceeded=true;};
        extName = this.name.split('.').pop();
        // if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
    });
    if (sizeExceeded) {
        window.alert(maxExceededMessage);
        $(inputFile).val('');
    };
}
/*---------------------------------------- retire_balance.html.erb  -------------------------*/
$(document).on('click','#verification_btn',function () {
    $('#verification_btn').hide();
    $('#verification_box').fadeIn("slow");
});
$(document).on('click','#password_btn',function () {
    var password = $('#password').val();
    $.ajax({
        url: "/admins/buy_rate/verify_admin_password",
        method: 'get',
        data: {password: password},
        success:function(data){
            $("#password_btn").css("background","green").text('').text("Verified");
            $("#info-text").text('');
            setTimeout(function(){
                $('#password').attr("disabled","disabled");
                $('#verification_box').fadeOut("slow");
                $('#verification_btn').hide();
                $('#cover-spin').fadeOut();
                $('#submit_transfer_btn').show();
            }, 1000);
        },
        error: function(data){
            $("#password_btn").css("border","1px solid red");
            $("#info-text").text('').css("color","red").text('Please try again!');
        }
    });
});
$(document).on("change", '.retire-all-check', function(){
    if($(this).prop("checked")){
        $("#balanceField_id").prop("disabled", "disabled");
        $(".balance_field").val('').val($("#blnc").val());
        if(parseFloat($(".balance_field").val()) <= 0){

            $('#amount_limit_error').html('').text("Insufficient Balance");
            $("#submit_transfer_btn").prop("disabled", true);
            $("#verification_btn").prop("disabled", true);
        }
        else{
            $("#submit_transfer_btn").prop("disabled", false);
            $("#verification_btn").prop("disabled", false);
        }
    }
    else{
        $("#balanceField_id").prop("disabled", false);
        $(".balance_field").val('');
        $("#submit_transfer_btn").prop("disabled", true);
    }
});
$(document).on("keyup",  "#balanceField_id", function (e) {
    if(this.value==''){
        $('#amount_limit_error').html('');
        $("#submit_transfer_btn").prop("disabled", true);
        $("#verification_btn").prop("disabled", true);
    }
    if(parseFloat(this.value) <= 0){
        $('#amount_limit_error').html('').text("Amount must be greater than 0");
        $("#submit_transfer_btn").prop("disabled", true);
        $("#verification_btn").prop("disabled", true);
        // $("#verification_btn").prop("disabled", true);
    }
    if(parseFloat(this.value) > parseFloat($("#blnc").val())){
        $('#amount_limit_error').html('').text("Insufficient Balance");
        $("#submit_transfer_btn").prop("disabled", true);
        $("#verification_btn").prop("disabled", true);
    }
    else if(parseFloat(this.value) > 0) {
        $('#amount_limit_error').html('');
        $("#submit_transfer_btn").prop("disabled", false);
        $("#verification_btn").prop("disabled", false);
    }
});
$(document).on('click', '#password_btn_verify',function () {
    var password = $('#admin_password').val();
    $.ajax({
        url: "/admins/buy_rate/verify_admin_password",
        type: 'get',
        data: {password: password},
        success:function(data){
            $("#password_btn_verify").addClass('background-green').text('').text("Verified");
            $("#info-text1").hide();
            var wallet_slug=$('#wallet_slug').val();
            $('.close').click();
            // $("#admin_verific").empty();
            $('#unlock1').hide();
            $('#cover-spin').show(0);
            $.ajax({
                type: 'GET',
                url: "/admins/isos/unlock",
                data: {wallet_id: wallet_slug}
            });
        },
        error: function(data){
            $("#password_btn_verify").addClass('btn-border');
            $("#info-text1").show();
        }
    });
});
$(document).on('click', '.retire_wallet',function () {
    $('#unlock1').modal('show');
    $('#info-text1').hide();
    $("#password_btn_verify").addClass('background-green').text('').text("Verify");
    $('#admin_password').val('');
});
$(document).on('click','.buyrate_btn',function (e) {
    var apply = $('.apply').val()
    var comission = $('#commission_iso_1').val()
    if (value_present(apply) && !value_present(comission))
    var apply = $('.apply').val();
    var comission = $('#commission_iso_1').val();
    if (value_present(apply) && !value_present(comission))
    {
        e.preventDefault();
        $('.buyrate_btn').prop('disabled','disabled');
        $(".error-1").text("Sales level and Commission Split are required!");
    }
    else
    {
        $('.buyrate_btn').prop('disabled',false);
        $(".error-1").text("");
    }
});
$(function()
{
    $(".percentage_validation").inputFilter(function (value) {
        return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100);
    });
});
$(function() {
    $(".buyrate_fields").on('input', function() {
        this.value = this.value.match(/\d{0,12}(\.\d{0,2})?/)[0];
    });
});
$(document).on('keyup','.commission_tier_in',function(){
   if ($(this).val() > 10)
   {
        $('#add-col').addClass('disable_link');
        $('#commission_tier_error').html("Commission Tiers cannot be greater than 10");
       $(".tier_table").text("")
   }
   else
   {
       $('#add-col').removeClass('disable_link');
       $('#commission_tier_error').html('');
   }
});
$(document).on('click','.buyrate_btn_save',function () {
    var apply_val =  $('#apply').val()
    if(apply_val == undefined){
      apply_val =  $('.apply').val()
    }
    var val = parseInt(apply_val)
    if ($('#apply').val() != '')
    {
        for (i = 1; i < val; i++) {
            id = '#sales_level_max_' +i;
            if( $(id).val() == "" && $('#sales_level_max_'+i+'-error').text() == '' ) {

                $('.buyrate_btn_save').prop('disabled',true);
                // $('.error-span.' + "sales_level_max_"+i).attr('style', 'display:inline').text('Please define max range of Tier');
                // $('.error-span.'+ "sales_level_max_"+i).fadeIn();
                // $('.error-span.'+ "sales_level_max_"+i).fadeOut(5000);
            }
        }
    }
});

// $(document).on('click','.buyrate_btn',function () {
//
//     if ( Number($('#sales_level_max_1').val()) == 0){
//         $('.buyrate_btn').prop('disabled',true);
//         $('#cover-spin').fadeOut();
//         $('.error-span.'+id).addClass('display-inline').text('Value must be greater than 0');
//         $('.error-span.'+id).fadeIn();
//         setTimeout(function(){
//             $('.error-span.'+id).fadeOut();
//         }, 2000);
//     }
// });


function ValidateFields(e,id,id_hidden){
    if (e.keyCode!=18 && e.keyCode!=38 && e.keyCode!=40 && e.keyCode!= 13 && e.keyCode!=9 && e.keyCode!=16 && e.keyCode!=17 && e.keyCode!=37 && e.keyCode!=20 && e.keyCode!=39 && e.keyCode!=36 && e.keyCode!=35 && e.keyCode!=27 && e.keyCode!=undefined) {
        //keyCode are 18 = alt -- 38 = arrowUp --arrowDown = 40 -- Enter = 13 -- tab = 9 -- Shift = 16 -- CTRL == 17 -- leftArrow = 37 -- rightArrow = 39 -- capsLock = 20  -- home = 36 -- end = 35 --esc =27
        $(id_hidden).val("");
        if (id.toLowerCase().indexOf("ez") >= 0)
            $(merchant_location).val(null);
    }
    var checkError=checkErrorAndDisplay(id, id_hidden);
    if (checkError === true)
    {
        $('.buyrate_btn').attr("disabled", "disabled");
    }
    else
    {
        $('.buyrate_btn').attr("disabled", false);
    }
    if (!($(id).val()!="" && $(id_hidden).val()==""))
        removeError($(id+'_error'));
}
function checkErrorAndDisplay(id, hidden_id){
    var error_div=check_hidden_value(id, hidden_id);
    console.log("3")
    console.log(error_div.length);
    if (error_div.length>0)
    {
        for (i = 0; i < error_div.length; i++) {
            addError(error_div[i]);
        }
        return true;
    }
    else
        return false;
}
function addError(error_div) {
    $(error_div["id"]).html("Please use an existing "+error_div["for"]);
    $(error_div["id"]).addClass('error-msg');
}

function removeError(error_div) {
    error_div.removeClass("error-msg");
    error_div.empty()
}

function check_hidden_value(id, hidden_id){
    $('.input-field').on('paste', function(){
        var hidden_field = $(this).next().attr('id');
        $('#' + hidden_field).val('');
    });
    var error_ids = [];
    var feed = {};
    if ($('#isos').val()!="" && $("#isosHidden").val()==""){
        feed = {id: "#isos_error", for:"ISO"};
        error_ids.push(feed);
    }
    if ($('#agents').val()!="" && $("#agentsHidden").val()==""){
        feed = {id: '#agents_error', for:"Agent"};
        error_ids.push(feed);
    }
    if ($('#affiliates').val()!="" && $("#affiliatesHidden").val()==""){
        feed = {id: '#affiliates_error', for:"Affiliate"};
        error_ids.push(feed);
    }
    if ($('#baked_iso_switch').is(':checked')){
        $('input.input-field').each(function () {
            var field_id = $(this).attr('id');
            var hidden_field_id = $("#" + field_id).next().attr('id');
            if ($("#" + field_id).val() !== "" && $("#" + hidden_field_id).val() === ""){
                feed = {id: '#' + $('#' + hidden_field_id).next().attr('id'), for:"User"};
                error_ids.push(feed);
            }
        });
    }
    if ($('#profit_split_switch').is(':checked'))
    {
        if ($('#companyInput').val()!="" && $("#companyInputHidden").val()==""){
            feed = {id: '#companyInput_error', for:"User"};
            error_ids.push(feed);
        }
        else if($('#companyInput').val()!="" && $("#companyInputHidden").val() === "merchant"){
            if ($("#locationText").val() ==="" || $("#locationText").val()==null){
                feed = {id: "#locationText_error", for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    if ($('#net_profit_split_switch').is(':checked'))
    {
        if ($('#netCompanyInput').val()!="" && $("#netCompanyInputHidden").val()==""){
            feed = {id: '#netCompanyInput_error', for:"User"};
            error_ids.push(feed);
        }
        else if($('#netCompanyInput').val()!="" && $("#netCompanyInputHidden").val()  === "merchant"){
            if ($("#netLocationText").val() ==="" || $("#netLocationText").val()==null){
                feed = {id: "#netLocationText_error", for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    if (($('#ez_merchant_switch').is(':checked')))
    {
        if ($('#'+ez_merchant).val()!="" && $('#'+ez_merchant+'Hidden').val()==""){
            feed = {id: '#'+ez_merchant+'_error', for:"User"};
            error_ids.push(feed);
        }
        else if($('#'+ez_merchant).val()!="" && $('#'+ez_merchant+'Hidden').val()!="")
        {
            if ($(merchant_location).val()=="" || $(merchant_location).val()==null){
                feed = {id: merchant_location+'_error', for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    return error_ids;
}
$(document).on('click','.autocomplete_off',function () {
    $(this).removeAttr('autocomplete')
})
$(document).ready(emptyStateFunction);
function emptyStateFunction() {
    if($(".admins_country_field").val() != ""){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(".admins_country_field").val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0 ){
                    $('.admins_states_field').attr("required",true);
                    $('.admins_states_field').attr("disabled",false);

                }else{
                    $("#user_profile_attributes_state").empty();
                    $('.admins_states_field').attr("required",false);
                    $('.admins_states_field').attr("disabled",true);

                }
            }
        });
    }
}
