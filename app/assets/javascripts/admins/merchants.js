
$('#alert-message-top').fadeOut(5000);
var ez_merchant = "ez_merchant_";
var merchant_location;
var selected_location_ids = [];
if ($("#key").val()==undefined)
{
    ez_merchant=ez_merchant+"1";
    merchant_location = '#ezLocationText'+"0";
}
else{
    merchant_location = '#ezLocationText'+$("#key").val();
    ez_merchant=ez_merchant+$("#key").val();
}

const urlParams = new URLSearchParams(window.location.search);
$(document).ready(function(){

    // ****************** Index page javascript *****************
    const queryParam = urlParams.get('query');
    $('#exports_btn').click(function(){
        if(queryParam != ""){
            $('.export_type').val('export');
        }
    });
    $('.datepicker_owner').daterangepicker({
        autoUpdateInput:false,
        minDate: 1901,
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: moment().subtract(18, 'years'),
        startDate: moment().subtract(18, 'years'),
        parentEl: '#date_parent',
    });
    $('.datepicker_owner').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('.datepicker_owner').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        $('.datepicker_owner').val(startDate.format('MM/DD/YYYY'));
    });
    $(".padd").css("margin","3px 15px 20px 0px");

    //error message and export extra

    $('.dt-buttons').css('display','none')
    //error message show remove
    $('#alert-message-top').fadeOut(5000);

    if($('.option_drop').val()=="gateway"){
        $('.gate').show();
        $('.all_search').hide();
    } else {
        $('.gate').hide();
    }

    $('.option_drop').change(function() {
        if($('.option_drop').val()=="gateway"){
            $('.gate').show();
            $('.all_search').hide();
        }else{
            $('.all_search').show();
            $('.gate').hide();
        }
    });

    $('.modalwalabutton').click(function(){
        $('.myModal').modal();
    });

    $(".page").click(function () {
        $(".page").removeClass("active");
        $(this).addClass("active");
    });

    $('.locs_id').click(function(){
        var id = $(this).attr('id');
        $.ajax({
            url: '/admins/merchants/get_data',
            type: 'GET',
            data: {merchant_id: id},
            success: function(data) {
                $('#cover-spin').fadeOut();
                keys=data.location[1];
                locations=data.location[0];
                if (locations.length==0 && keys.length !=0){
                    var mymodal = $('#myModal');
                    mymodal.find('.modal-title').text('Merchant Keys');
                    mymodal.find('.modal-body').html('<div style="padding-left: 12px;"> <strong style="padding-right: 337px;">Merchant Key:</strong>' + keys.key+'</div>'+'<br> <div style="padding-left: 12px;"> <strong style="padding-right: 320px;">Merchant Secret:</strong>' + keys.secret+'</div>'+'<br>');
                    mymodal.modal('show');
                    return;
                }else if(locations.length==0 && keys.length ==0){
                    return;
                }
                locData = '';
                for(i=0;i<locations.length;i++){
                    location_name=locations[i].location_name
                    location_key=locations[i].location_key
                    // $(element).after("<tr><td>"+location_name+"</td><td>"+location_key+"</td></tr>")
                    locData +='<tr>'+
                        '<td>'+location_name+'</td>'+
                        '<td>'+location_key+'</td>'+
                        '</tr>';
                }
                var mymodal = $('#myModal');
                mymodal.find('.modal-title').text('Merchant Locations');
                mymodal.find('.modal-body').html('<div style="padding-left: 12px;"> <strong style="padding-right: 337px;">Merchant Key:</strong>' + keys.key+'</div>'+'<br> <div style="padding-left: 12px;"> <strong style="padding-right: 320px;">Merchant Secret:</strong>' + keys.secret+'</div>'+'<br> <table class = "table"">'+
                    '<tr>'+
                    '<th>Location Name:</th>'+
                    '<th>Location Key:</th>'+
                    '</tr>'+
                    locData+
                    '</table> ');
                mymodal.modal('show');
                $('#cover-spin').fadeOut();
            },
            error: function(data){
                $('#cover-spin').fadeOut();
            }
        });
    });


    //  ***************** Mian Form steps js ******************


    $('.buyrate_fields, .hasDatepicker').prop('autocomplete', "<%= [*('A'..'Z')].sample(8).join %>");
    //const urlParams = new URLSearchParams(window.location.search);
    const status = urlParams.get('status');
    const loc = urlParams.get('loc');
    const corporate = urlParams.get('corporate');
    // if(status === "l1"){
    //     $("#current-step5 + p").style("margin-left", "-20px", "important")
    // }
    if(status === "new1"){
        sessionStorage.setItem('totalVisitedSteps', null)
    }
    if(loc === "1" || corporate === "1"){
        sessionStorage.setItem('totalVisitedSteps', null)
    }
    let lastVistedStep   = sessionStorage.getItem('totalVisitedSteps')
    step = $("#current-active-step").data("value")
    if(lastVistedStep !== "null"){
        sessionStorage.setItem('totalVisitedSteps', null)
    }
    sessionStorage.setItem('totalVisitedSteps', step)
    if(lastVistedStep > step && lastVistedStep <= 7) {
        sessionStorage.setItem('totalVisitedSteps', lastVistedStep)
    }
    if(lastVistedStep > step){
        while(lastVistedStep > 0){
            $(".step"+lastVistedStep).css("background-color", "green")
            $(".step"+lastVistedStep).removeAttr("disabled");
            if(lastVistedStep === step){
                $(".step"+lastVistedStep).css("background-color", "#7867a7")
            }
            lastVistedStep = lastVistedStep - 1;
        }
        step = lastVistedStep
    }
    step = step - 1
    while(step > 0){
        $(".step"+step).css("background-color", "green")
        $(".step"+step).removeAttr("disabled");
        step = step - 1;
    }

//  *************** Edit Merchant Javascript ***********************

    $('.modalTime').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local;
    });

    $(".percentage_validation").inputFilter(function (value) {
        return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100);
    });
// ****************input intel phone gem validation js******************

    $("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#phone_code').val(Object.values(selectedCountryData)[2]);
            $('#phone_number').val('');
            $('#phone_number').removeAttr('autocomplete')
            space_remove(Object.values(selectedCountryData)[2], '#phone_number');
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
    $("#cs_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#cs_number_code').val(Object.values(selectedCountryData)[2]);
            $('#cs_number').val('');
            $('#cs_number').removeAttr('autocomplete')
            space_remove(Object.values(selectedCountryData)[2], "#cs_number");
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });

    $('.phone_number').each(function () {
        var id = $(this).attr('id');
        var phone_code_id = $(this).next().attr("id");
        $("#"+id).intlTelInput({
            formatOnInit: true,
            separateDialCode: true,
            formatOnDisplay: false,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                country=selectedCountryPlaceholder;
                country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                $("#"+phone_code_id).val(Object.values(selectedCountryData)[2]);
                space_remove(Object.values(selectedCountryData)[2], "#"+id);
                if(selectedCountryData.iso2 == 'us')
                {
                    country = "555-555-5555"
                }
                return  country;
            },
            excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
            utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
        });
    })

    function space_remove(code_number, phone_id) {
        if(code_number.length==1)
        {
            $(phone_id).css('padding-left', '3.9rem', 'important');

            // $(phone_id).addClass('phone-length-1');

        }
        else if(code_number.length==2)
        {
            $(phone_id).css('padding-left', '4.3rem', 'important');

            // $(phone_id).addClass('phone-length-2');
        }
        else if(code_number.length==3)
        {
            $(phone_id).css('padding-left', '4.8rem', 'important');

            // $(phone_id).addClass('phone-length-3');
        }
        else
        {
            $(phone_id).css('padding-left', '5.2rem', 'important');

            // $(phone_id).addClass('phone-length-4');
        }
    }

    $('.corporate_phone').on('click blur paste keyup', function () {
        var phone = $("#phone_number").val();
        // var phone_code = $(this).closest('.phone-box').find('.selected-dial-code').text();
        var phone_code = $("#phone_code").val();
        var reg = new RegExp('^[0-9]+$');
        // phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        if(phone.length > 0){
            if(reg.test(phone_number)){
                $.ajax({
                    url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if($("#phone_number").val() > 0){
                            if(result){
                                $("#"+phone_error_div).text('')
                                $("#phone_heading").removeClass("phone-valid");
                                $('.wizard-submit-btn').prop('disabled', false);
                            }else{
                                $('.wizard-submit-btn').prop('disabled', 'disabled');
                                $("#phone_heading").addClass("phone-valid");
                                $("#"+phone_error_div).text('Invalid format or phone number is already taken!')
                            }
                        }
                    },
                    error: function (error) {
                    }
                });
            }else{
                $('.wizard-submit-btn').prop('disabled', 'disabled');
                $("#"+phone_error_div).text('');
                $("#"+phone_error_div).text('Must be Number')
            }
        }else{
            $('.wizard-submit-btn').prop('disabled', 'disabled');
            $("#"+phone_error_div).text('');
            $("#phone_heading").addClass("phone-valid");
            $("#"+phone_error_div).text('Please enter Phone Number');
        }

    });

//  ************************ End Phone Validation *************************

    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                }
            });
        };
    }(jQuery));


    $('.wizard-submit-btn').on("click",function () {
        if($(".phone-error").text() != ""){
            $('.wizard-submit-btn').prop('disabled', true);

        }else{
            $('.wizard-submit-btn').prop('disabled', false);

        };
    });


    $('#email_exist_val').on('keyup', function (){
        if ($("#email-error").text().length >0  ){
            $('.wizard-submit-btn').prop('disabled',true);
        }else
        {
            $('.wizard-submit-btn').prop('disabled',false);
            // $(this).find("input[type='submit']").prop('disabled',false);
        }
    })

    $(".duplicate-location").on('click', function () {
        $("#duplicateLocationModal").modal('show');
    })

    $("#location_id").on('change', function () {
        var location_id = $(this).val();
        var merchant_id = '<%= params[:merchant_id] %>';
        console.log(merchant_id)
        var newUrl = "/admins/merchants/location?loc=duplicate&status=l1&location_id="+location_id+"&merchant_id="+merchant_id;
        console.log(newUrl)
        $('.duplicate-submit-btn').attr('href',newUrl);

    })

    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    $("#user_password").focusin(function () {
        $('.strength').css('display', "block");
    });

    $("#user_password").focusout(function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            $('.strength').css('display', "none");
        }
    });

    $("#user_password , #user_password_confirmation").on('keyup', function () {
        if ($('#user_password').val() == $('#user_password_confirmation').val() && $('#user_password').val() != "" && $('#user_password_confirmation').val() != "" && $('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password</label>");
            if($('#email_label_field1').text() == "Pers. Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        } else {
            if ($("#user_password_confirmation").val() != ""){
                $('#confirm_password_label_field').replaceWith("<label id='confirm_password_label_field' class='string optional'>Confirm Password<span style='color:red;'> Passwords do not match. Try again.</span></label>");
            }
//                $(':input[type="submit"]').prop('disabled', true);
        }
        if ($('#user_password').val() == "" && $('#user_password_confirmation').val() == ""){
            $('#message').html('');
            if($('#email_label_field1').text() == "Pers. Email") {
                $(':input[type="submit"]').prop('disabled', false);
            }
        }
    });

    $(document).on("click", "#corporate_li", function () {
        myFunction('corporate','owner','setting','documentation')
    });

    $(document).on("click", "#owner_li", function () {
        myFunction('owner','corporate','setting','documentation')
    });

    $(document).on("click", "#setting_li", function () {
        myFunction('setting','owner','corporate','documentation')
    });
    $(document).on("click", "#documentation_li", function () {
        myFunction('documentation','setting','owner','corporate')
    });

    $(document).on("change", ".ownership", function (e) {
        var l = $('.ownership').length;
        var all = 0;
        var index = 0;
        for (i = 0; i < l; i++) {
            if($('.ownership').eq(i).val() !== ""){
                all = parseFloat(all) + parseFloat($('.ownership').eq(i).val());
                index = i+1;
            }
        }

        var ownership = e.target.value;
        if(ownership !== ""){
            if(all > 100){
                e.target.value = '';
                //alert("Ownership can't be greater than 100%");
                swal("Alert!","Ownership can't be greater than 100%", "error");
            }else if(all < 50){
                if($('.ownership').eq(index).val() !== ""){
                    duplicateOwner()
                }
            }else if(all >= 50){
                if($('.ownership').eq(index).val() === ""){
                    $('.duplicate_owner').html('');
                    $(".count").text(index + 1)
                    if($("#new_user").valid()){
                        $("#btnSave").prop("disabled",false)
                    }

                }
            }
        }
    });
//..............................merchant_owner_edit_form......................//
    $(document).on("click", '.cancel_owner', function (e) {
        var owner_number = parseInt($(this).attr("id"));
        var no_of_owners = $(".owners_form").length;
        if(no_of_owners <= 1){
            alert("You can't remove all owners.")
        }else{
            $("#owners"+owner_number).remove();
            var count = $(".count").text();
            $(".count").text(count-1);
            $(".first-count").each(function (key, index) {
                $(this).text('').text(key + 1)
            });
            $(this).find("span").text(owner_number);
            $('.wizard-submit-btn').prop('disabled', false);
        }
    });
    $(document).on("click", ".duplicate-owner-button", function () {
        duplicateOwner();
    })
    function duplicateOwner(){
        nextOwnerCount = parseInt($(".count").text())
        ownerCount = nextOwnerCount
        var form_number = parseInt($('.form_number:last').val());
        var no_of_owners = $(".owners_form").length;
        nextOwnerCount = nextOwnerCount + 1;
        $(".count").text(nextOwnerCount);
        $.ajax({
            url: "/admins/merchants/duplicate_owner",
            type: 'GET',
            data: {count: no_of_owners+1},
            success: function(data) {
                $(".first-count").each(function (key, index) {
                    $(this).text('').text(key + 1)
                });
            },
            error: function(data){
            }
        });
    }
    $("#user_password").on("keyup", function () {
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input style="margin-right: 10px;" id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input style="margin-right: 10px;" id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input style="margin-right: 10px;" id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input style="margin-right: 10px;" id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <= 20) {
            $('#length').attr('checked', true);
        } else {
            $('#length').attr('checked', false);
        }
        if (/[0-9]/.test(password)) {
            $('#one_number').attr('checked', true);
        } else {
            $('#one_number').attr('checked', false);
        }
        if (/[A-Z]/.test(password)) {
            $('#upper_case').attr('checked', true);
        } else {
            $('#upper_case').attr('checked', false);
        }
        if (/[a-z]/.test(password)) {
            $('#lower_case').attr('checked', true);
        } else {
            $('#lower_case').attr('checked', false);
        }
    });

    $('#new_user').submit(function() {
        if($("#new_user").valid()){
            $(this).find("input[type='submit']").prop('disabled',true);
        }
    });


//  **********************validation rules*******************


    $.validator.methods.email = function( value, element ) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test( value )
    }

    $(".percent").inputFilter(function(value) {
        return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100); });

    $(function() {
        $(".two_decimal").on('input', function() {
            this.value = this.value.match(/\d{0,12}(\.\d{0,2})?/)[0];
        });
    });

    $(".whole_number").inputFilter(function(value) {
        return /^-?\d*$/.test(value);
    });

    $("#phone_number").inputmask('Regex', {regex: "^[0-9+]+$"});
    $(".number-regex").inputmask('Regex', {regex: "^[0-9]+$"});

    $("#tax_id").mask("00-00000000");



// **************************************** Location Form javascript ******************************************


    $(".my-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") === "password")
        {
            input.attr("type", "text");
        }
        else
        {
            input.attr("type", "password");
        }
    });

    if($('#user_location_bank_account_type').val() === '')
    {
        $('#bank-routing').attr('disabled',true);
        $('#password-field').attr('disabled',true);
        $('#bank-name').attr('disabled',true);
    }
    $("#tax_id").mask("00-00000000")
    $('#user_location_bank_account_type').on('change',function () {
        if($(this).val()!='')
        {
            $('#bank-routing').attr('disabled',false);
            $('#password-field').attr('disabled',false);
            $('#bank-name').attr('disabled',false);
        }
        else
        {
            $('#bank-routing').attr('disabled',true);
            $('#password-field').attr('disabled',true);
            $('#bank-name').attr('disabled',true);

        }

    });
    $('#password_btn').on('click',function () {
        var password = $('#password').val();
        $.ajax({
            url: "<%= verify_admin_password_admins_buy_rate_index_path %>",
            method: 'get',
            data: {password: password},
            success:function(data){
                $("#password_btn").css("background","green").text('').text("Verified");
                $("#info-text").text('');
                setTimeout(function(){
                    var x = $("#password-field")
                    if (x.attr('type') === "password") {
                        x.attr('type','text')
                    }
                    $('#admin_confirm').modal('hide')
                    $('#password').val("");
                    $("#password_btn").css("background","#7867a7").text('').text("Verify");
                }, 1000);
            },
            error: function(data){
                $("#password_btn").css("border","1px solid red");
                $("#info-text").text('').css("color","red").text('Please try again!');
            }
        });
    });
    function confirm_account_modal() {
        var x = $("#password-field")
        if (x.attr('type') === "password") {
            $('#admin_confirm').modal('show');
        } else {
            console.log("f")

            x.attr('type','password')
        }
    }

// *************************************** ISO & Agent Form Javascript ****************************************

    $( function() {
        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _create: function () {
                this._super();
                this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
            },
            _renderMenu: function (ul, items) {
                var that = this,
                    currentCategory = "";
                var exist_ids = [];
                $('input[name$="[wallet]"]').each(function(){
                    exist_ids.push(parseInt($(this).val()));
                });
                $('input[name^="user[ids]"]').each(function(){
                    exist_ids.push(parseInt($(this).val()));
                });
                exist_ids = exist_ids.filter( value => !Number.isNaN(value) ); // we are getting the ids of user which are already selected in other fields
                console.log(exist_ids);
                items = $.grep(items, function(e){
                    //return e.id != 10;
                    return !exist_ids.includes(e.id); // here we are removing objects in items
                });
                $.each(items, function (index, item) {
                    var li;
                    if (item.category != currentCategory) {
                        ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                        currentCategory = item.category;
                    }
                    li = that._renderItemData(ul, item);
                    if (item.category) {
                        li.attr("aria-label", item.category + " : " + item.label);
                    }
                });
            }
        });
    });

    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term));
        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };

    $('input#isos').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $('input#agents').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $('input#affiliates').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $('#isos').on('keyup', function (e){
        for_main_users("isos");
        ValidateFields(e,'#isos','#isosHidden')
    });

    $('#agents').on('keyup', function (e){
        for_main_users("agents")
        ValidateFields(e,'#agents','#agentsHidden');
    });

    $(' #affiliates').on('keyup', function (e){
        for_main_users("affiliates")
        ValidateFields(e,'#affiliates','#affiliatesHidden');
    });

    $(document).on('keyup','#companyInput',(function () {
        var all_keyword = $(this).val();
        var id = '#'+$(this).attr('id');
        var user = '#companyInput';
        var user_hidden = '#companyInputHidden';
        var merchant_div = '#locationDiv';
        var merchant_location = '#locationText';
        var span_error = '#spanError';
        var split = '#splitInput';
        var close_btn = '#closeBtn';
        if(all_keyword != ''){
            getAllUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location)
        }
    }));

    $(document).on('change','#profit_split_switch',function(){
        if($(this).is(':checked')){
            if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked')){
                $("#btnSave").prop("disabled", true);
            }
            $('#gbox_company_radio').prop("checked", true);
            $('#profit_splits_radio_section').show()
            $('#profit_splits_on_section').show();
            $('#profit_check').show();
        } else{
            $('#profit_splits_radio_section').hide()
            $('#profit_splits_on_section').hide();
            $('#profit_check').hide();
            $('#companyInput-error').remove();
        }
    });

    $(document).on('change','#baked_iso_switch',function(){
        if($(this).is(':checked')){
            if( !$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked')){
                $("#btnSave").prop("disabled", true);
            }
            $('#gbox_radio').prop("checked", true);
            $('#baked_profit_radio_section').show()
            $('#baked_section').show();
            $('#baked_check').show();
        } else{
            console.log("B")
            $('#baked_check').hide();
            $('#baked_section').hide();
            $('#user_1-error').remove();
            $('#baked_profit_radio_section').hide()
            if ($("#btnSave").is(":disabled")){
                $("#btnSave").prop("disabled", true);
            }else{
                $("#btnSave").prop("disabled", false);
            }

        }
    });
    $(document).on('click','.baked_switch_checked', function () {
        OnChangeRadioBaked(this);
    })
    $(document).on('click','.profit_split_gbox_checked', function () {
        OnChangeRadioProfit (this)
    })
    function OnChangeRadioProfit (radio) {
        if ($("#baked_iso_switch").is(":checked")){
            if( !$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked')){
                $("#btnSave").prop("disabled", true);
            }else{
                $("#btnSave").prop("disabled", false);
            }
        }else{
            $("#btnSave").prop("disabled", false);
        }
    }

    function OnChangeRadioBaked (radio) {

        if ($("#profit_split_switch").is(":checked")){
            if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked')){
                $("#btnSave").prop("disabled", true);
            }else{
                if($('.error-msg').text()=="") {
                    $("#btnSave").prop("disabled", false);
                }
                else {
                    $("#btnSave").prop("disabled", true);
                }
            }
        }else{
            if($('.error-msg').text()=="") {
                $("#btnSave").prop("disabled", false);
            }
            else {
                $("#btnSave").prop("disabled", true);
            }
        }
    }

    $(document).on('change','#sub_merchant_split_switch',function(){
        if($(this).is(':checked')){
            $('#ez_section').show();
            if( $(".location-errors ").text() != "") {
                $("#submit_edit_button1").prop("disabled", true);
            }
        } else{
            $('#ez_section').hide();
            $("#submit_edit_button1").prop("disabled", false);
        }
    });

    $('#add_baked_users').click(function () { // add new baked user
        count = $('.input-field').last().attr('id');
        count = count.split('_');
        count = count[1];
        count = Number(count);
        $.ajax({
            url:"/admins/merchants/next_baked_section",
            type: "GET",
            data: {count: count}
        });
    });

    $(document).on('click','#remove_baked_users',function(){ // remove baked user
        $(this).parent().parent().parent().parent().parent().parent().remove();
    });

    $(document).on('click blur paste keypress change', '.for_super_users', function () {
        var id = $(this).attr("id");
        for_super_users(id);
    })

    $('#user_1').on('click blur paste keypress change', function (e) {
        // for_super_users("user_1");
        ValidateFields(e,'#user_1','#user_1Hidden')
    })

    $('.user-1').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        ValidateFields(e,id,hidden_id)
    })

    $('#user_2').on('click blur paste keypress change', function (e) {
        // for_super_users("user_2")
        ValidateFields(e,'#user_2','#user_2Hidden')
    })

    $('.user-2').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        ValidateFields(e,id,hidden_id)
    })

    $('#user_3').on('click blur paste keypress change', function (e) {
        // for_super_users("user_3")
        ValidateFields(e,'#user_3','#user_3Hidden')
    })

    $('.user-3').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        ValidateFields(e,id,hidden_id)
    });
    $('.input-field').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var class_id = id.slice(1)
        var value = $(this).val()
        if (value == "") {
            $(id + "_percent").removeClass(class_id + "_percent")
            $(id + "_dollar").removeClass(class_id + "_dollar")
            $(id + "_percent_error").text('');
        }
    });

    $(document).on('keyup', '.next-baked-users', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        for_super_users(e.target.id);
        ValidateFields(e,id,hidden_id)
    })
    $(document).on('keyup', '.user_percent', function () {
        if($('.error-msg').text()==""){
            $("#btnSave").prop("disabled", false);
        }else{
            $("#btnSave").prop("disabled", true);
        }
    })

    $('#companyInput').on('keyup',function (e) {
        ValidateFields(e,'#companyInput','#companyInputHidden')
    })

    $(document).on('keyup', '#netCompanyInput', function (e) {
        ValidateFields(e,'#netCompanyInput','#netCompanyInputHidden')
    })

    $('#'+ez_merchant).on('keyup',function (e) {
        for_ez_merchant('ez_merchant_1','0')
        ValidateFields(e,'#'+ez_merchant,'#'+ez_merchant+'Hidden')
    })

    $('.edit-ez-merchant').on('keyup',function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + $(id).next('.edit-ez-hidden').attr('id');
        ValidateFields(e, id, hidden_id)
    })

    // net profit split location error
    $(document).on('change', "#netLocationText", function (e) {
        var id = "#" + $(this).attr('id');
        var netLocationError = $(id +'_error')
        if($(this).val() !== ""){
            removeError(netLocationError);
            ValidateFields(e,null,null,null);
        }
        if (($('#net_profit_split_switch').is(':checked'))){
            if($('#netCompanyInput').val() != "" && $('#netCompanyInputHidden').val()!="")
            {
                if ($(id).val() == "")
                {
                    addError({id: netLocationError, for: 'Location'});
                    $("#btnSave").prop("disabled", true);
                }
            }
        }
    });
    $(document).on('change','.merchant_filter',function () {
        $('#merchant_filter').submit();
    })
    $(document).on('change','.sub_merchant_filter',function () {
        $('#sub_merchant_filter').submit();
    })
    //............................ add_super_user_iso_and_agent.tml............//
    $('.error_check_usser').on('click',function (e) {
        if($('.error-msg').text()!= ""){
            e.preventDefault();
        }
    })

    $(".btnSave1").on('click',function() {
        var count = 0;
        $('.input-field').each(function(e){
            count = count +1;
            var field_id = $(this).attr('id');
            field_id = field_id.split('_');
            field_id = field_id[1];
            var field_count = field_id-1;
            var hidden_field = $(this).next().attr('id');
            var error_field = $(this).next().next().attr('id');
            if ($("#baked_iso_switch").is(':checked')) {
                // if($("#"+field_id).parent().prev().find('input').val() == "" && $("#"+field_id).val() !== ""){
                //     error_message = "Select baked user";
                //     $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("Please Select the baked user");
                //     $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).css('color', 'red');
                //     e.preventDefault()
                // }else{
                //         $("#"+$("#"+field_id).parent().prev().find('div').attr('id')).text("");
                //
                // }

                if (field_id % 3 == 0) {
                    if ($("#user_" + field_id).val() != '' && $('#user_' + (field_id - 1)).val() == '') {
                        $("#user_" + (field_id - 1)).next().next().text("Please Select the baked user");
                        $("#user_" + (field_id - 1)).next().next().css('color', 'red');
                        e.preventDefault();
                    } else {
                        $("#user_" + (field_id - 1)).next().next().text("");
                    }
                }
                if (field_id % 3 == 0 && field_id != 3) {
                    if ($("#user_" + field_id).val() == '' && $("#user_" + (field_id - 1)).val() == '' && $("#user_" + (field_id - 2)).val() == '') {
                        $(this).parent().parent().parent().parent().remove();
                    }
                }
            }
        });
        $('.first-user').each(function(e){
            var field_id = $(this).attr('id');
            var error_field = $(this).next().next().attr('id');
            if ($("#baked_iso_switch").is(':checked')){
                if($("#"+field_id).val() == "" && $('.error-msg').text() == ""){
                    error_message = "Select baked user";
                    $($("#"+error_field)).text("Please Select the baked user");
                    $($("#"+error_field)).css('color', 'red');
                    e.preventDefault();
                }
            }
        });
    });
    $(".btnSaves").click(function () {
        var error_message = "";
        $('#phone_number').removeAttr('autocomplete')
        var baked_profit_split = $("#baked_iso_switch").is(":checked");
        if(baked_profit_split){
            $(".input-field").each(function (e) {
                var id = $(this).attr("id");
                var percent = $("#" + id + "_percent").val();
                var dollar = $("#" + id + "_dollar").val();
                var baked_user = $(this).val()
                if ($("#baked_iso_switch").is(':checked')) {
                    if (baked_user != "") {
                        if (!value_present(percent) && !value_present(dollar) && dollar != undefined && dollar != undefined) {
                            if (percent == "" && dollar == "") {
                                $("#" + id + "_percent_error").html("Must enter % or $");
                                e.preventDefault();
                            } else {
                                $("#" + id + "_percent_error").html("Value should be greater than 0");
                                e.preventDefault();
                            }
                        } else {
                            if ($(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
                                $("#" + id + "_error").text("Please Select the baked user");
                                $("#" + id + "_error").css('color', 'red');
                                e.preventDefault();
                            } else {
                                $("#" + id + "_percent_error").html("")
                                // $("#" + $("#" + id).parent().prev().find('div').attr('id')).text("");
                            }
                        }
                    } else {
                        if ($(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
                            $("#" + id + "_error").text("Please Select the baked user");
                            $("#" + id + "_error").css('color', 'red');
                            e.preventDefault();
                        }
                    }
                }
            })
        }
    });
    // $('.btnSave1').on('click',function (e) {
    //     var year = $('#user_years_in_business_year').val()
    //     var month = $('#user_years_in_business_month').val()
    //     $('.error_years').text('');
    //     if((year == "" || month != "") && (year != "" || month == "")){
    //         $('.error_years').text('Please select at least one')
    //         e.preventDefault();
    //     }
    // });
    $('.btnSaves').on('click',function (e) {
        var error = $('.date_of_birth_error').text()
        if (error != ""){
            e.preventDefault();
        }
    })
    $('.error_text').on('change',function () {
        $('.error_years').text('');
    })
    // profit split location error
    $(document).on('change', "#locationText", function (e) {
        var id = "#" + $(this).attr('id');
        var locationError = $(id +'_error')
        if($(this).val() !== ""){
            removeError(locationError);
            ValidateFields(e,null,null,null);
        }
        if (($('#profit_split_switch').is(':checked'))){
            if($('#companyInput').val() != "" && $('#companyInputHidden').val()!="")
            {
                if ($(id).val() == "")
                {
                    addError({id: locationError, for: "Location"});
                    $("#btnSave").prop("disabled", true);
                }
            }
        }
    });


    // sub merchant split location error
    $(merchant_location).on('change',function(e){
        ezLocationError=$(merchant_location+'_error')
        if ($(merchant_location).val()!="")
        {
            removeError(ezLocationError);
            ValidateFields(e,null,null,null);
        }
        if (($('#sub_merchant_split_switch').is(':checked'))){
            if($('#'+ez_merchant).val()!="" && $('#'+ez_merchant+'Hidden').val()!="")
            {
                if ($(merchant_location).val()=="")
                {
                    addError({id: ezLocationError, for: 'Location'})
                    $("#btnSave").prop("disabled", true);
                }
            }
        }
    });

    $(document).on("change", "#baked_iso_switch, #profit_split_switch, #net_profit_split_switch, #sub_merchant_split_switch", function (e) {
        ValidateFields(e,null,null,null)
    });

    $(document).on('keyup','#super_company_percent',function () {
        var zval = $('#super_company_percent').val()
        if (zval<=0 && zval!="")
        {
            $('#super_company_percent_error').text("Value should be greater than 0");
            $("#btnSave").prop("disabled", true);
        }
        else {
            $('#super_company_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="" && ($('#gbox_radio').is(':checked') || $('#iso_radio').is(':checked'))) {
                if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked') && $("#profit_split_switch").is(":checked")){
                    $("#btnSave").prop("disabled", true);
                }else if (!$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked') && $("#baked_iso_switch").is(":checked")){
                    $("#btnSave").prop("disabled", true);
                }else{
                    $("#btnSave").prop("disabled", false);
                }
            }
            else
            {
                $("#btnSave").prop("disabled", true);
            }
        }
    })
    $(document).on('keyup','#net_super_company_percent',function () {
        var zval = $('#net_super_company_percent').val()
        if (zval<=0 && zval!="")
        {
            $('#net_super_company_percent_error').text("Value should be greater than 0");
            $("#btnSave").prop("disabled", true);
        }
        else {
            $('#net_super_company_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="" && $('.percent_error').text()=="" && ($('#gbox_radio').is(':checked') || $('#iso_radio').is(':checked'))) {
                $("#btnSave").prop("disabled", false);
            }
            else
            {
                $("#btnSave").prop("disabled", true);
            }
        }
    });

    $(document).on('keyup','#ez_percent',function () {
        var zval = $('#ez_percent').val()
        if (zval<=0 && zval!="")
        {
            $('#ez_percent_error').text("Value should be greater than 0");
            $("#btnSave").prop("disabled", true);
        }
        else {
            $('#ez_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="" && $('.percent_error').text()=="" ) {
                $("#btnSave").prop("disabled", false);
            }
            else
            {
                $("#btnSave").prop("disabled", true);
            }
        }
    });

    $("#attach_radio").on('click', function () {
        if($(this).is(":checked")){
            $(this).val('true');
        }
        else if($(this).is(":not(:checked)")){
            $(this).val('false');
        }
    });

    $(document).on('keyup', ".for_super_users", function () {
        var key = $(this).data("key");
        var user_no = $(this).data("user_no");
        for_super_users("user_"+user_no+key);
    })

    // net profit split js

    $(document).on('keyup','#netCompanyInput',(function () {
        var all_keyword = $(this).val();
        var id = '#'+$(this).attr('id');
        var user = '#netCompanyInput';
        var user_hidden = '#netCompanyInputHidden';
        var merchant_div = '#netLocationDiv';
        var merchant_location = '#netLocationText';
        var span_error = '#spanError';
        var split = '#splitInput';
        var close_btn = '#closeBtn';
        if(all_keyword != ''){
            getAllNetUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location)
        }
    }));

    $(document).on('change','#net_profit_split_switch',function(){
        if($(this).is(':checked')){
            $('#net_profit_split_gbox').show()
            $("#net_gbox_company_radio").prop("checked", true);
            $('#net_profit_splits_on_section').show();
            $("#net_gbox_company_radio").removeAttr('disabled', 'disabled');

        }else{
            $('#net_profit_split_gbox').hide()
            $("#net_gbox_company_radio").prop("checked", false);
            $('#net_profit_splits_on_section').hide();
            $("#net_gbox_company_radio").attr('disabled', 'disabled');
            $("#net_gbox_company_radio").prop('checked', false);

        }
    });

// ***************** validation rules ***************
    $('.date_of_birth').mask('00/00/0000');
    $(".amount_field").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $("#cvv").inputmask('Regex', {regex: "^[0-9]+$"});
    $(".name_field").inputmask('Regex', {regex:"^[a-zA-Z0-9. ]*$" });
    $(document).on('change keyup','.date_of_birth',function () {
        element_obj = $(this);
        error_span = $(this).parent().find(".date_of_birth_error");
        var date = $(this).val();
        var value = date.split('/')
        var  month= value[0];
        var day = value[1];
        var year = value[2];
        //$('.drp-calendar').show();
        var after_vale = isDate18orMoreYearsOld(day,month,year)
        error_span.text('');
        if(value_present(value)){
            if(!value_present(day) || !value_present(month) || !value_present(year)){
                $('.show-calendar').hide();
                error_span.html('Invalid date(format: MM/DD/YYYY)');
            }else if((value_present(month) && month > 12) || (value_present(day) && day > 31) || (value_present(year) && year.length < 4)){
                $('.show-calendar').hide();
                error_span.html('Invalid date(format: MM/DD/YYYY)');
            }else if((value_present(year)  && year<1920)){
                $('.show-calendar').hide();
                error_span.html('Years Upto 1920 (format: 01/01/1920)');
            }else{
                if (after_vale == false){
                    $('.show-calendar').hide();
                    error_span.html('Age is less than 18 years.');
                }}
        }else{
            error_span.text('');
        }
    })
    $(document).on('click','.autocomplete_off',function () {
        $(this).removeAttr('autocomplete')
    })
    $(document).on('focusin click','.date_of_birth',function () {
        var id = $(this).attr('id');
        ids = id.split('_');
        count = ids[3];
        $('.datepicker_'+count).show();
        if(id=="date_of_birth"){
            $('.single').show();
            $('.datepicker_1').hide();
            $('.datepicker_2').hide();
            $('.datepicker_3').hide();
            $('.datepicker_4').hide();
            $('.datepicker_6').hide();
            $('.datepicker_7').hide();
            $('.datepicker_8').hide();
        }
    })
    function isDate18orMoreYearsOld(day, month, year) {
        setDate1 = new Date(parseInt(year)+parseInt(18), parseInt(month) - parseInt(1), day)
        currentDate = new Date()
        return  setDate1 <= currentDate
    }
// ********************************************* Fee Form Javascript ********************************************

    $('.fee-datepicker').datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: new Date()
    });

    $('.form-control.buyrate_fields.calender-fields').on('change',function () {
        var id = $(this).attr('id');
        if ($(this).val() == 0) {
            $('#' + id + '_date').val('');
            $('#location-btn').attr("disabled", false);
        }
        else if($(this).val() != 0) {
            $('#'+id+'_date').val(moment(new Date).format("MM/DD/YYYY"));
        }
    });

    $('#user_location_fee_service_date, #user_location_fee_statement_date, #user_location_fee_misc_date').on('keyup change focusout', function () {
        var id = $(this).attr('id')
        var parent_id = id.split('_date')[0];
        var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
        if ($('#' + parent_id).val() == 0) {
            $('#' + id).val('');
            $('#location-btn').attr("disabled", false);
        }
        else if ($('#'+parent_id).val() == 0 && $('#'+id).val() != '') {
            $('#'+id).val('');
            $('#location-btn').attr("disabled", false);
        }
        else if($('#'+parent_id).val()!=0 && $('#'+id).val()!='' && date_regex.test($('#'+id).val()) ) {
            $('#location-btn').attr("disabled", false);
        }
        else {
            $('#' + id).val('');
            $('#location-btn').attr("disabled", true);
        }
    });


// ********************************************* Setting Form Javascript **************************************


    if($("#user_location_disable_sales_transaction").is(':checked')){
        $("#salesButton").show();
    }else{
        $('#sales_id').prop('checked', false);
        $('#vt_id').prop('checked', false);
        $('#batch_id').prop('checked', false);
        $("#salesButton").hide();
    }

    if($("#user_location_block_api").is(':checked')){
        $("#apiButton").show();
    }else{
        $('#vt_api').prop('checked', false);
        $('#vtt_api').prop('checked', false);
        $('#vd_api').prop('checked', false);
        $("#apiButton").hide();
    }

    if($("select#p_type option").filter(":selected").val() == "retail" || $("select#p_type option").filter(":selected").val() == "ecommerce_service"){
        $('#hold_on_delivery_custom').hide();
    }else{
        // $('#hold_in_arrear_custom').hide();
    }
    $("#p_type").change(function(){
        if($(this).val() == "retail" || $(this).val() == "ecommerce_service"){
            $('#hold_on_delivery_custom').hide();
            $('#hold_in_arrear_custom').show();
        }else{
            $('#hold_on_delivery_custom').show();
            $('#hold_in_arrear_custom').hide();
        }
    });
    if ($("#user_location_hold_in_rear").is(':checked')){
        $("#hirDays").show();
        $('#user_location_hold_in_rear_days').attr('min', 2);
    }else {
        $("#hirDays").hide();
        $('#user_location_hold_in_rear_days').removeAttr('min');
    }

    if ($("#user_location_on_hold_pending_delivery").is(':checked')){
        $("#ohpdDays").show();
        $('#user_location_on_hold_pending_delivery_days').attr('min', 2);
    }else {
        $("#ohpdDays").hide();
        $('#user_location_on_hold_pending_delivery_days').removeAttr('min');
    }
    $( "#user_location_on_hold_pending_delivery" ).change(function () {
        if($(this).is(':checked')){
            $('#user_location_hold_in_rear').prop('checked', false);
            $("#hirDays").hide();
            $("#ohpdDays").show();
            $('#user_location_on_hold_pending_delivery_days').attr('min', 2);
        }
        else{
            $("#ohpdDays").hide();
            $('#user_location_on_hold_pending_delivery_days').removeAttr('min');
        }
    });
    $( "#user_location_hold_in_rear" ).change(function () {
        if($(this).is(':checked')){
            $('#user_location_on_hold_pending_delivery').prop('checked', false);
            $("#hirDays").show();
            $("#ohpdDays").hide();
            $('#user_location_hold_in_rear_days').attr('min', 2);
        }
        else{
            $("#hirDays").hide();
            $('#user_location_hold_in_rear_days').removeAttr('min');
        }
    });

    $( "#user_location_disable_sales_transaction" ).change(function () {
        if($(this).is(':checked')){
            $("#salesButton").show();
        }
        else{
            $('#sales_id').prop('checked', false);
            $('#vt_id').prop('checked', false);
            $('#batch_id').prop('checked', false);
            $("#salesButton").hide();
        }
    });

    $("#user_location_block_api" ).change(function () {
        if($(this).is(':checked')){
            $("#apiButton").show();
        }else{
            $('#vt_api').prop('checked', false);
            $('#vtt_api').prop('checked', false);
            $('#vd_api').prop('checked', false);
            $("#apiButton").hide();
        }
    });

    $( "#user_location_hold_in_rear" ).change(function () {
        if($(this).is(':checked')){
            $("#hirDays").show();
            $("#ohpdDays").hide();
            $('#user_location_hold_in_rear').attr('min', 2);
        }
        else{
            $("#hirDays").hide();
            $('#user_location_hold_in_rear').removeAttr('min');
        }
    });

    $(".add-web").on("click", function(){
        $(".web-row").append("<div class='col-md-12 new-web'><div class='form-group col-md-3'><input type='url' name='user[location][web_site][]' pattern='https?://.+' class='form-control' placeholder='https://www.google.com'></div><div class='col-sm-2' style='padding-right: 7%;'><span class='fa fa-minus remove-web'></span></div></div>")
    })

    $(document).on('click','.remove-web',function(){ // remove baked user
        $(this).parent().parent().remove();
    });

    var location_setting_data = gon.setting_location_data

    if(value_present(location_setting_data) && (location_setting_data.disable_sales_transaction == true || location_setting_data.disable_sales_transaction == "1")){
        $("#user_location_disable_sales_transaction").attr("checked", "checked");
    }
    if(value_present(location_setting_data) && (location_setting_data.sales == true || location_setting_data.sales == "1")){
        $('#sales_id').prop('checked', true);
    }else{
        $('#sales_id').prop('checked', false);
    }
    if(value_present(location_setting_data) && (location_setting_data.virtual_terminal == true || location_setting_data.virtual_terminal == "1")){
        $('#vt_id').prop('checked', true);
    }else{
        $('#vt_id').prop('checked', false);
    }
    if(value_present(location_setting_data) && (location_setting_data.close_batch == true || location_setting_data.close_batch == "1")){
        $('#batch_id').prop('checked', true);
    }else{
        $('#batch_id').prop('checked', false);
    }
    if(value_present(location_setting_data) && (location_setting_data.block_api == true || location_setting_data.block_api == "1")){
        $("#user_location_block_api").attr("checked", "checked");
    }
    if(value_present(location_setting_data) && (location_setting_data.virtual_transaction_api == true || location_setting_data.virtual_transaction_api == "1")){
        $('#vt_api').prop('checked', true);
    }else{
        $('#vt_api').prop('checked', false);
    }
    if(value_present(location_setting_data) && (location_setting_data.virtual_terminal_transaction_api == true || location_setting_data.virtual_terminal_transaction_api == "1")){
        $('#vtt_api').prop('checked', true);
    }else{
        $('#vtt_api').prop('checked', false);
    }
    if(value_present(location_setting_data) && (location_setting_data.virtual_debit_api == true || location_setting_data.virtual_debit_api == "1")){
        $('#vd_api').prop('checked', true);
    }else{
        $('#vd_api').prop('checked', false);
    }
    if(value_present(location_setting_data) && (location_setting_data.risk == "high" || location_setting_data.risk == "1")){
        $("#high_merchant_risk_radio").attr("checked", "checked")
    }else if(value_present(location_setting_data) && (location_setting_data.risk == "low" || location_setting_data.risk == "0")){
        $("#low_merchant_risk_radio").attr("checked", "checked")
    }else{
        $("#high_merchant_risk_radio").attr("checked", "checked")
    }
    if(value_present(location_setting_data) && (location_setting_data.hold_in_rear == true || location_setting_data.hold_in_rear == "1")){
        $("#user_location_hold_in_rear").attr("checked", "checked");
    }else{
        $("#user_location_hold_in_rear").attr("checked",false);
    }


//  ************************************** Documentation Form Javascript ***********************************

    $(function() {
        $('input').on('change', function(event) {
            var id=$(this).attr('id');
            var files = event.target.files;
            if(files != "" && files != null){
                $.each(files, function( index, value){
                    fileExt = value.name.substr(value.name.lastIndexOf('.') + 1)
                    if(fileExt === 'jpeg' || fileExt === 'jpg' || fileExt === 'png' || fileExt === 'gif'){
                        var image = value
                        var reader = new FileReader();
                        reader.onload = function(file) {
                            var img = new Image();
                            console.log(file);
                            img.src = file.target.result;
                            img.style = "margin-top:5%"
                            $('.'+id+ " .row").append(img);
                            $('.'+id+'_br').addClass('display-break').removeClass('line-break');
                            $('.file-btn').css('margin-left', '3%')
                        }
                        reader.readAsDataURL(image);
                    }
                })
            }
        });
    });
    $(".delete-btn a").click(function (e) {
        var image_id = $(e.currentTarget).data('image')
        $.ajax({
            url: "/admins/merchants/document_delete",
            type: 'GET',
            data: {image_id: image_id},
            success: function(data) {
                console.log(data);
                if($(e.currentTarget).parent().parent().prev().find("img").length > 0){
                    $(e.currentTarget).parent().parent().prev().find("img").hide();
                }else{
                    $(e.currentTarget).parent().parent().prev().find("a").hide();
                }

                $(e.currentTarget).parent().parent().hide();
            },
            error: function(data){
                console.log(data);
            }
        })
    });

    $("input[type=file]").on("change", function (e) {
        validateFiles(e.target)
    })

    // var status = ""
    // <% if params[:loc].present? %>                   // currently not required
    //         <% if params[:loc] == "duplicate" %>
    //             $('.custom-style').addClass('duplicate-location-style')
    // <% end %>
    // <% end %>
    // if(status === "duplicate"){
    //     $('.custom-style').addClass('duplicate-location-style')
    // }


//   ************************************ Api Key Js ******************************************************

    $('#hide_btn').on('click',function () {
        if($('#hide_text').text() == "Reveal"){
            $('#hide_text').text('').text('Conceal');
            $('#token_field').css('color','gray').css('text-shadow', '0 0 0px rgba(0, 0, 0, 0)');
        }else if($('#hide_text').text() == "Conceal"){
            $('#hide_text').text('').text('Reveal');
            $('#token_field').css('color','transparent').css('text-shadow', '0 0 5px rgba(0, 0, 0, 0.5)');
        }
    });

//   ************************************ Show page js ****************************************************

    $('.modalwalabutton').click(function() {
        $('.myModal').modal();
    });
    var table = $('#datatable-keytable-admin-merchant').DataTable({
        // "responsive": true,
        "sScrollX": false,
        "sSearch": false,
        "ordering": true,
        "searching": false,
        "bLengthChange": true,
        "bPaginate": false,
        "bInfo": false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            // { "orderable": false, "targets": 0 },
            // { "orderable": false, "targets": 7 },

        ]
    });

    $('#datatable-wallets').DataTable({
        responsive: true,
        "sScrollX": false,
        "scrollX": false,
        "sSearch": true,
        "ordering": true,
        "processing": true,
        "searching": true,
        "serverSide": true
    });

    // $(".duplicate-owner-button").on('click', function () {
    //     duplicateOwner()
    // })
    $(".select-field").on("click", function () {
        $(this).select();
    });
    //.............................merchant.....click function.......................//
    $(document).on('click','.remove_owner_function',function () {
        var owner_id = $('.remove_owner_id').val();
        var merchant_id = $('.remove_merchant_id').val();
        remove_owner(owner_id, merchant_id)
    });
    $(document).on('click','.merchant_cover_spin',function () {
        $('#cover-spin').show(0)
    });

    $(document).on('click','#add_sub_merchant',function () {// add new baked user
        var count = parseInt($('.ez_merchant_class:last').data('count')) ;
        var error_count = count;
        var ez_merchant = $('#ez_merchant_'+count).val();
        var ez_location_error = $('#ezLocationText'+error_count+'_error').text()
        if (ez_merchant == "" ||  ez_merchant == undefined) {
            $('#ez_merchant_'+count+'_error').text('Please select merchant');
        }else if ( ez_location_error == ""){
            var count1 = count + 1;
            var new_sub_merchant_fields = "<div class='row remove_ez_merchant'><div class='col-md-12'><div class='col-md-1 sub_merchant_remove_btn' id='div4'><span><a id='remove_sub_merchant' class='col-md-12 div5 remove_sub_merchant'><i class='fa fa-minus-circle fa-2x'></i></a></span></div><div class='sub-merchant-block'><div class='col-md-6 ui-front'><p class='cursor_default label-text-M'>Merchant</p><input type='text' class='form-control ez_merchant_class' data-count=" + count1 + " value='' id='ez_merchant_" + count1 + "' name='profit_splits[splits][ez_merchant][" + count1 + "][ez_merchant_1][name]' autocomplete='off' required='required'><input type='hidden' id='merchant_id' value=''><input type='hidden' id='ez_merchant_" + count1 + "Hidden' value='' name='profit_splits[splits][ez_merchant][" + count1 + "][ez_merchant_1][wallet]' class='form-control'><div class='ez_merchant_error' id='ez_merchant_" + count1 + "_error'></div><p id='ezspanError" + count1 + "' class='ezspan_css'></p></div><div class='col-md-5' id='ezLocationDiv" + count1 + "'><p class='cursor_default label-text-L'>Location</p><select data-count=" + count1 + " id='ezLocationText" + count1 + "' class='form-control ezLocationText w_97 ez_location_text_class'><option value=''>Select Location</option></select><div id='ezLocationText" + count1 + "_error' class='location-errors'></div></div></div></div></div>"
            $(".new_sub_merchant").append(new_sub_merchant_fields);
            $("#sub_merchant_counter").val(count);
        }
    });
    $(document).on("click", ".remove_sub_merchant", function () {
        var locationTextError = $(this).parent().parent().parent().find(".location-errors").text();
        if(locationTextError != "" && $(".submit_edit_button1").prop("disabled")){
            $(".submit_edit_button1").prop("disabled", false);
        }
        $(this).closest(".remove_ez_merchant").remove();
    })

    var country="";
    var errorMap = [ "Country code Invalid", "Invalid selected country", "Too short", "Too long", "Invalid number"];

    $(document).on('change','.admins_country_field_cor',function () {
        if($(this).val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $(this).val(),owner_field: 'change' },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#user_profile_attributes_state").empty();
                    if(Object.keys(states).length > 0){
                        $.each(states,function (index, value) {
                            $("#user_profile_attributes_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                        });
                        $('.admins_states_field').attr("required",true);
                        $('.admins_states_field').attr("disabled",false);
                    } else{
                        $('.admins_states_field').attr("required",false);
                        $('.admins_states_field').attr("disabled",true);
                    }

                }
            });
        }
    });
    $(document).on('change','.admins_country_field_location',function () {
        if($(this).val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $(this).val(),owner_field: 'change' },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#user_location_state").empty();
                    if(Object.keys(states).length > 0 ){
                        $.each(states,function (index, value) {
                            $("#user_location_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                        })
                        $('.admins_states_field_state').attr("required",true);
                        $('.admins_states_field_state').attr("disabled",false);

                    }else{
                        $('.admins_states_field_state').attr("required",false);
                        $('.admins_states_field_state').attr("disabled",true);

                    }

                }
            });
        }
    });
    $(document).on('change','.admins_country_field',function () {

        var value = $(this).val();
        var count = $(this).attr('data');
        var id = $(this).attr('id');
        if($(this).val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $(this).val() , owner_field: 'change'},
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#user_owner_"+count+"_state").empty();
                    $("#user_owner_"+count+"_dl_state_issue").empty();
                    if(Object.keys(states).length > 0){
                        $("#user_owner_"+count+"_dl_state_issue").append('<option value="">&nbsp;Select State</option>');
                        $.each(states,function (index, value) {
                            $("#user_owner_"+count+"_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                            $("#user_owner_"+count+"_dl_state_issue").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                        });
                        $("#user_owner_"+count+"_state").attr("required",true);
                        $("#user_owner_"+count+"_dl_state_issue").attr("required",true);
                        $("#user_owner_"+count+"_state").attr("disabled",false);
                        $("#user_owner_"+count+"_dl_state_issue").attr("disabled",false);
                    }else{
                        $("#user_owner_"+count+"_state").attr("required",false);
                        $("#user_owner_"+count+"_dl_state_issue").attr("required",false)
                        $("#user_owner_"+count+"_state").attr("disabled",true);
                        $("#user_owner_"+count+"_dl_state_issue").attr("disabled",true);
                    }

                }
            });
        }
    });

    if ($("#country1").val() == ""  )
    {
        $("#country1").hide();
        $("#country").show();
        if ($("#state1").val() == "" )
        {
            $("#state1").hide();
            $("#state").show();
        }else{
            $("#state").hide();
            $("#state1").show();
        }
    }else{
        $("#country").hide();
        $("#country1").show();
        if ($("#state1").val() == "" )
        {
            $("#state1").hide();
            $("#state").show();
        }else{
            $("#state").hide();
            $("#state1").show();
        }
    }

    $(document).on('change keyup','#country1',function (){
        if ($("#country1").val() == "" )
        {
            $("#country1").hide();
            $("#country").show();
            $("#state1").hide();
            $("#state").show();
        }else {
            $("#country").hide();
            $("#country1").show();
            $("#state").hide();
            $("#state1").show();
        }

    });

});

function getAllUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }
                    else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            // Mantain Response In Categories
            if(ui.item.category=="Merchants"){
                $(user_hidden).removeAttr('name');
                $(user_hidden).val("merchant");
                getMerchantLocations(ui.item.id,merchant_div,split,close_btn,user,span_error,merchant_location);
            }
            else if(ui.item.category=="Companies"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][company"+ui.item.id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][company"+ui.item.id+"][amount]");
                $(close_btn).data('category',"Company");
                $(user_hidden).val(ui.item.id);
                $(user).attr('name',"profit_splits[splits][company"+ui.item.id+"][name]");
                $("#super_company_percent").attr('name', "profit_splits[splits][company"+ui.item.id+"][percent]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][company"+ui.item.id+"][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][company"+ui.item.id+"][from]");
            }
            else if(ui.item.category=="Isos"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][iso"+ui.item.id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][iso"+ui.item.id+"][amount]");
                $(close_btn).data('category',"Iso");
                $(user_hidden).val(ui.item.id);
                $(user).attr('name',"profit_splits[splits][iso"+ui.item.id+"][name]");
                $("#super_company_percent").attr('name', "profit_splits[splits][iso"+ui.item.id+"][percent]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][iso"+ui.item.id+"][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][iso"+ui.item.id+"][from]");

            }else if(ui.item.category=="Agents"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][agent"+ui.item.id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][agent"+ui.item.id+"][amount]");
                $(close_btn).data('category',"Agent");
                $(user_hidden).val(ui.item.id);
                $(user).attr('name',"profit_splits[splits][agent"+ui.item.id+"][name]");
                $("#super_company_percent").attr('name', "profit_splits[splits][agent"+ui.item.id+"][percent]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][agent"+ui.item.id+"][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][agent"+ui.item.id+"][from]");
            }else if(ui.item.category=="Affiliates"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][affiliate"+ui.item.id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][affiliate"+ui.item.id+"][amount]");
                $(close_btn).data('category',"Agent");
                $(user_hidden).val(ui.item.id);
                $(user).attr('name',"profit_splits[splits][affiliate"+ui.item.id+"][name]");
                $("#super_company_percent").attr('name', "profit_splits[splits][affiliate"+ui.item.id+"][percent]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][affiliate"+ui.item.id+"][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][affiliate"+ui.item.id+"][from]");
            }
            $(merchant_location).css('visibility', 'hidden');
            $("#companyInput_error").css('visibility', 'hidden');
            ValidateFields(e,user,user_hidden)
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function getMerchantLocations(merchant_id,merchant_div,split,close_btn,user,span_error,merchant_location){
    $.ajax({
        url: "/admins/buy_rate/getMerchantLocations",
        type: "GET",
        data: {merchant_id: merchant_id},
        success: function (data) {
            if (data.locations!=""){
                $(merchant_location).empty();
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $(merchant_location).append(new_option);
                data.locations.forEach(function(location) {
                    var new_option = new Option(location.name, location.id);
                    $(new_option).html(location.name);
                    $(merchant_location).append(new_option);
                });
                $(merchant_location).attr('name',"profit_splits[splits][merchant"+merchant_id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][merchant"+merchant_id+"][amount]");
                $(close_btn).data('category',"Merchant");
                $(merchant_div).css('visibility','visible');
                $(user).attr('name',"profit_splits[splits][merchant"+merchant_id+"][name]");
                $("#super_company_percent").attr('name', "profit_splits[splits][merchant"+merchant_id+"][percent]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][merchant"+merchant_id+"][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][merchant"+merchant_id+"][from]");
                // if($('#iso_company_radio').is(':checked')) { $('#iso_company_radio').attr('name', "profit_splits[splits][merchant"+ui.item.id+"][from]"); }
                // if($('#gbox_company_radio').is(':checked')) { $('#gbox_company_radio').attr('name', "profit_splits[splits][merchant"+ui.item.id+"][from]"); }
//                    $(user).attr('readonly',true);
                $(merchant_location).css('visibility', 'visible');
                $("#companyInput_error").css('visibility', 'visible');
            }else{
                $(merchant_location).empty();
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(span_error).text('No Location Exists Select Any Other Merchant');
                $(span_error).fadeIn();
                setTimeout(function(){
                    $(span_error).fadeOut();
                }, 2000);
            }
        },
        error: function (data) {
            $(merchant_location).empty();
            $(merchant_div).css('visibility','hidden');
            $(merchant_location).removeAttr('name');
            $(span_error).text('No Location Exists Select Any Other Merchant')
            $(span_error).fadeIn();
            setTimeout(function(){
                $(span_error).fadeOut();
            }, 2000);
        }
    });
}

function ValidateFields(e,id,id_hidden){
    if (e.keyCode!=18 && e.keyCode!=38 && e.keyCode!=40 && e.keyCode!= 13 && e.keyCode!=9 && e.keyCode!=16 && e.keyCode!= 17 && e.keyCode!=37 && e.keyCode!=20 && e.keyCode!=39 && e.keyCode!=36 && e.keyCode!=35 && e.keyCode!=27 && e.keyCode!=144 && e.keyCode!=undefined) {
        //keyCode are 18 = alt -- 38 = arrowUp --arrowDown = 40 -- Enter = 13 -- tab = 9 -- Shift = 16 -- CTRL == 17 -- leftArrow = 37 --   rightArrow = 39 -- capsLock = 20  -- home = 36 -- end = 35 --esc =27 -- NUMLOCK =144
        $(id_hidden).val("");
        if (id.toLowerCase().indexOf("ez") >= 0)
            $(merchant_location).val(null);
    }
    var checkError=checkErrorAndDisplay(id, id_hidden);
    if (checkError === true)
    {
        $('#btnSave').attr("disabled", "disabled");
    }
    else
    {
        if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked') && $("#profit_split_switch").is(":checked")){
            $("#btnSave").prop("disabled", true);
        }else if (!$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked') && $("#baked_iso_switch").is(":checked")){
            $("#btnSave").prop("disabled", true);
        }else{
            $('#btnSave').attr("disabled", false);
        }
    }
    if (!($(id).val() !="" && $(id_hidden).val()==""))
        removeError($(id+'_error'));
}

function getMainUsers(all_keyword,id,user,user_hidden,field_id){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword,users: field_id},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }
                    else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            $(user_hidden).val(ui.item.id);
            $(user).val(ui.item.value);
            ValidateFields(e,user,user_hidden);
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function for_main_users(field_id) {
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    all_keyword = all_keyword.split('-');
    var length = all_keyword.length;
    all_keyword = all_keyword[length-1];
    all_keyword = all_keyword.trim();
    if (all_keyword.length < 1 ){
        $(user_hidden).val('');
    }
    if(all_keyword != ''){
        getMainUsers(all_keyword,id,user,user_hidden,field_id)
    }
}

function for_super_users(field_id) {
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    all_keyword = all_keyword.split(' ');
    var length = all_keyword.length;
    all_keyword = all_keyword[length-1];
    all_keyword = all_keyword.trim();
    if(all_keyword != ''){
        getUsers(all_keyword,id,user,user_hidden,field_id,"for_users")
    }
}

function getUsers(all_keyword,id,user,user_hidden,field_id,type){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword,users: type},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }
                    else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            $(user_hidden).val(ui.item.id);
            $(user).val(ui.item.value);
            var name_id = $(user).attr('name');
            var end_id = user.slice(1)
            $(user + "_percent").addClass(end_id + "_percent");
            $(user + "_dollar").addClass(end_id + "_dollar");
            ValidateFields(e,user,user_hidden)
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function checkErrorAndDisplay(id, hidden_id){
    var error_div=check_hidden_value(id, hidden_id);
    if (error_div.length>0)
    {
        for (i = 0; i < error_div.length; i++) {
            addError(error_div[i]);
        }
        return true;
    }
    else
        return false;
}

function addError(error_div) {
    $('#ezLocationText0-error').text("");
    $(error_div["id"]).html("Please use an existing "+error_div["for"]);
    $(error_div["id"]).addClass('error-msg');
}

function removeError(error_div) {
    error_div.removeClass("error-msg");
    error_div.empty()
}

function  check_hidden_value(id, hidden_id){
    var error_ids=[];
    var feed = {};
    if ($('#isos').val()!="" && $("#isosHidden").val()==""){
        feed = {id: "#isos_error", for:"ISO"};
        error_ids.push(feed);
    }
    if ($('#agents').val()!="" && $("#agentsHidden").val()==""){
        feed = {id: '#agents_error', for:"Agent"};
        error_ids.push(feed);
    }
    if ($('#affiliates').val()!="" && $("#affiliatesHidden").val()==""){
        feed = {id: '#affiliates_error', for:"Affiliate"};
        error_ids.push(feed);
    }
    if ($('#baked_iso_switch').is(':checked')){
        $('input.input-field').each(function () {
            var field_id = $(this).attr('id');
            var hidden_field_id = $("#" + field_id).next().attr('id');
            if ($("#" + field_id).val() !== "" && $("#" + hidden_field_id).val() === ""){
                feed = {id: '#' + $('#' + hidden_field_id).next().attr('id'), for:"User"};
                error_ids.push(feed);
            }
        });
    }
    if ($('#profit_split_switch').is(':checked'))
    {
        if ($('#companyInput').val()!="" && $("#companyInputHidden").val()=="") {
            feed = {id: '#companyInput_error', for:"User"};
            error_ids.push(feed);
        }
        else if($("#companyInput").val()!="" && $("#companyInputHidden").val() === "merchant"){
            if ($("#locationText").val() ==="" || $("#locationText").val() == null){
                feed = {id: "#locationText_error", for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    if ($('#net_profit_split_switch').is(':checked'))
    {
        if ($('#netCompanyInput').val()!="" && $("#netCompanyInputHidden").val()==""){
            feed = {id: '#netCompanyInput_error', for:"User"};
            error_ids.push(feed);
        }
        else if($("#netCompanyInput").val()!="" && $("#netCompanyInputHidden").val() === "merchant"){
            if ($("#netLocationText").val() ==="" || $("#netLocationText").val()==null){
                feed = {id: "#netLocationText_error", for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    if ($('#sub_merchant_split_switch').is(':checked'))
    {
        if ($("#ez_merchant_0").val()!="" && $("#ez_merchant_0Hidden").val()==""){
            feed = {id: id+'_error', for:"User"};
            error_ids.push(feed);
        }
        else if($("#ez_merchant_0").val()!="" && $("#ez_merchant_0Hidden").val()!="")
        {
            if ($(merchant_location).val()=="" || $(merchant_location).val()==null){
                feed = {id: merchant_location+'_error', for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    return error_ids;
}

function for_ez_merchant(field_id, key){
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    var merchant_div = '#ezLocationDiv'+key;
    merchant_location = '#ezLocationText'+key;
    var span_error = '#ezspanError'+key;
    var split = '#splitInput';
    var close_btn = '#closeBtn';
    var current_merchant = $("#merchant_id").val();
    if(all_keyword !== ''){
        // getUsers(all_keyword,id,user,user_hidden,field_id, "ez_merchant")
        getAllEzUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location,key,current_merchant)
    }
}

function getAllEzUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location,key,current_merchant){
    var location_id = $("#ez_location_id").val();
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword, users: "ez_merchant", location: location_id,current_merchant: current_merchant},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }else{
                        response('')
                    }
                },
                error: function (error) {
                    $("#ez_merchant_"+parseInt(key + 1)+"_error").text('').css("color","red").text('Please select a Different User!');
                    response('')
                }
            });
        },
        select: function(e, ui) {
            // Mantain Response In Categories
            if(ui.item.category=="Merchants"){
                $(user_hidden).removeAttr('name');
                $(user_hidden).val(ui.item.id);
                getEzMerchantLocations(ui.item.id,merchant_div,split,close_btn,user,span_error,merchant_location,key);
            }
            ValidateFields(e,user,user_hidden);
            checkLocationError(user)
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function getEzMerchantLocations(merchant_id,merchant_div,split,close_btn,user,span_error,merchant_location,key){
    $.ajax({
        url: "/admins/buy_rate/getMerchantLocations",
        type: "GET",
        data: {merchant_id: merchant_id},
        success: function (data) {
            if (data.locations!=""){
                if($(merchant_location+'_error').hasClass('error-msg')){
                    $("#location-btn").prop("disabled", true);
                } else{
                    $("#location-btn").prop("disabled", false);
                }
                $(merchant_location).empty();
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $(merchant_location).append(new_option);
                locations = $(".ez_location_text_class");
                locations.each(function () {
                    selected_location_ids.push($(this).val());
                })
                selected_location_ids = selected_location_ids.filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                data.locations.forEach(function(location) {
                    if(!selected_location_ids.includes(location.id.toString())){
                        var new_option = new Option(location.name, location.id);
                        $(new_option).html(location.name);
                        $(merchant_location).append(new_option);
                    }
                });
                $(merchant_location).attr('name',"profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][wallet]");
                $(split).attr('name',"profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][amount]");
                $(merchant_div).css('display','block');
                $(user).attr('name',"profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][name]");
                $("#attach_radio").attr('name', "profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][attach]");
            }else{
                ezmerchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
            }
        },
        error: function (data) {
            ezmerchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
        }
    });
}

function ezmerchantsLocationNotAvailable(merchant_location,merchant_div,span_error) {
    // $(merchant_location).empty();
    $(merchant_div).css('visibility','hidden');
    // $(merchant_location).removeAttr('name');
    $(span_error).css('visibility','visible');
    $(span_error).text('No Location Exists Select Any Other Merchant');
    $(span_error).fadeIn();
    $("#location-btn").prop("disabled", true);
    setTimeout(function(){
        $(span_error).fadeOut("slow");
    }, 4000);
}

function getAllNetUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            // Mantain Response In Categories
            if(ui.item.category=="Merchants"){
                $(user_hidden).removeAttr('name');
                $(user_hidden).val("merchant");
                getNetMerchantLocations(ui.item.id,merchant_div,split,close_btn,user,span_error,merchant_location);
            }
            else {
                $(merchant_div).css('visibility', 'hidden');
                $(merchant_location).css('visibility', 'hidden');
                $("#netLocationText_error").css('visibility', 'hidden');
                $(merchant_location).removeAttr('name');
                if (ui.item.category == "Companies") {
                    setting_some_fields("company" + ui.item.id,ui.item.id,user_hidden,split,user);
                }else if (ui.item.category == "Isos") {
                    setting_some_fields("iso" + ui.item.id,ui.item.id,user_hidden,split,user);
                } else if (ui.item.category == "Agents") {
                    setting_some_fields("agent" + ui.item.id,ui.item.id,user_hidden,split,user);
                } else if (ui.item.category == "Affiliates") {
                    setting_some_fields("affiliate" + ui.item.id,ui.item.id,user_hidden,split,user);
                }
            }
            ValidateFields(e,user,user_hidden);
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function setting_some_fields(role,id,user_hidden,split,user) {
    $(user_hidden).attr('name', "profit_splits[splits][net][" + role + "][wallet]");
    $(split).attr('name', "profit_splits[splits][net][" + role + "][amount]");
    $(user_hidden).val(id);
    $(user).attr('name', "profit_splits[splits][net][" + role + "][name]");
    $("#net_super_company_percent").attr('name', "profit_splits[splits][net][" + role + "][percent]");
    $('.net_gbox').attr('name', "profit_splits[splits][net][" + role + "][from]");
}

function getNetMerchantLocations(merchant_id,merchant_div,split,close_btn,user,span_error,merchant_location){
    $.ajax({
        url: "/admins/buy_rate/getMerchantLocations",
        type: "GET",
        data: {merchant_id: merchant_id},
        success: function (data) {
            if (data.locations!=""){
                $(merchant_location).empty();
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $(merchant_location).append(new_option);
                data.locations.forEach(function(location) {
                    var new_option = new Option(location.name, location.id);
                    $(new_option).html(location.name);
                    $(merchant_location).append(new_option);
                });
                $(merchant_location).attr('name',"profit_splits[splits][net][merchant"+merchant_id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][net][merchant"+merchant_id+"][amount]");
                $(merchant_div).css('visibility','visible');
                $(user).attr('name',"profit_splits[splits][net][merchant"+merchant_id+"][name]");
                $("#net_super_company_percent").attr('name', "profit_splits[splits][net][merchant"+merchant_id+"][percent]");
                $('#net_gbox_company_radio').attr('name', "profit_splits[splits][net][merchant"+merchant_id+"][from]");
                $(merchant_location).css('visibility', 'visible');
                $("#netLocationText_error").css('visibility', 'visible');
            }else{
                merchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
            }
        },
        error: function (data) {
            merchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
        }
    });
}

function merchantsLocationNotAvailable(merchant_location,merchant_div,span_error) {
    $(merchant_location).empty();
    $(merchant_div).css('visibility','hidden');
    $(merchant_location).removeAttr('name');
    $(span_error).css('visibility','visible');
    $(span_error).text('No Location Exists Select Any Other Merchant');
    $(span_error).fadeIn();
    setTimeout(function(){
        $(span_error).fadeOut("slow");
    }, 4000);
}

function download_file(image){
    $.ajax({
        url: "/admins/merchants/document_download",
        type: 'GET',
        data: {image: image},
        success: function(data) {
            console.log(data);
        },
        error: function(data){
            console.log(data);
        }
    });

}

function validateFiles(inputFile) {
    var maxExceededMessage = "This file exceeds the maximum allowed file size (10 MB)";
    // var extErrorMessage = "Only image file with extension: .jpg, .jpeg, .gif or .png is allowed";
    // var allowedExtension = ["jpg", "jpeg", "gif", "png"];

    // var extName;
    var maxFileSize = $(inputFile).data('max-file-size');
    var sizeExceeded = false;
    // var extError = false;

    $.each(inputFile.files, function() {
        if (this.size && maxFileSize && this.size > parseInt(maxFileSize)) {sizeExceeded=true;};
        extName = this.name.split('.').pop();
        // if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
    });
    if (sizeExceeded) {
        window.alert(maxExceededMessage);
        $(inputFile).val('');
    };

    // if (extError) {
    //     window.alert(extErrorMessage);
    //     $(inputFile).val('');
    // };
}

function myFunction(id,id1,id2,id3) {
    var from_tab = $("#from_tab").val();
    if(id === "owner" && id1 === "corporate" && from_tab !== "setting"){
        corporate_form = $("#merchantform").valid();
    }
    phone_numb = ($(".phone-error").text() != "") ? false : true;
    $("#from_tab").val(id);
    $('#submit_edit_button1').data("active",""+id);
    $('#'+id).show();
    $('#'+id+'_li').addClass('active');
    $('#'+id1).hide();
    $('#'+id1+'_li').removeClass('active');
    $('#'+id2).hide()
    $('#'+id2+'_li').removeClass('active');
    $('#'+id3).hide()
    $('#'+id3+'_li').removeClass('active');
}
// $(".wizard-submit-btn").click(function (e) {
//     validateIsoAndAgentForm("new_user");
//     if($(".new_user").valid()){
//         $('#new_user').submit();
//     }
// })
// function duplicateOwner(){
//     nextOwnerCount = parseInt($(".count").text())
//     ownerCount = nextOwnerCount
//     var form_number = parseInt($('.form_number:last').val());
//     var no_of_owners = $(".owners_form").length;
//     $(".first-count").each(function (key, index) {
//         $(this).text('').text(key + 1)
//     });
//     nextOwnerCount = nextOwnerCount + 1
//     $(".count").text(nextOwnerCount)
//   $.ajax({
//         url: "/admins/merchants/duplicate_owner",
//         type: 'GET',
//         data: {count: form_number + 1},
//         success: function(data) {
//         },
//         error: function(data){
//         }
//     });
// }
// *********** End net profit split js ****************
$(document).on('keyup', '.ez_merchant_class', function (e) {
    var temp = $(this).data('count') + 1;
    for_ez_merchant(this.id,$(this).data('count').toString())
    ValidateFields(e,this.id,"#ez_merchant_"+temp+"Hidden")
})
$(document).on('keyup change', '.ez_merchant_class', function (e) {
    var value = $(this).val();
    var count = parseInt($(this).data('count')) ;
    var ez_location_error = $('#ezLocationText'+count+'_error').text()
    if (value == ""){
        $("#ezLocationText"+count).empty();
        var new_option = new Option("Choose Location", '');
        $(new_option).html('Choose Location');
        $("#ezLocationText"+count).append(new_option);
        $("#submit_edit_button1").prop("disabled", true);
    }

})

// sub merchant split location error
$(document).on('change', ".ez_location_text_class", function(e){
    var temp = $(this).data('count')
    var temp1 = temp;
    ezLocationError = $("#ezLocationText"+temp+"_error");
    if (value_present($("#ezLocationText"+temp+"").val())) {
        $("#ez_merchant_"+temp1+"Hidden").val($("#ezLocationText"+temp+"").val());
        removeError(ezLocationError);
        ValidateFields(e,null,null,null);
    }
    if (($('#sub_merchant_split_switch').is(':checked'))){
        if($("#ez_merchant_"+temp1+"").val()!="" && $("#ez_merchant_"+temp+"Hidden").val()!="") {
            if ($("#ezLocationText"+temp+"").val()=="") {
                addError({id: ezLocationError, for: 'Location'});
                $("#submit_edit_button1").prop("disabled", true);
            }else{
                $("#submit_edit_button1").prop("disabled", false);
            }
        }
    }
});

function  checkLocationError(id) {
    var temp = $(id).data('count');
    if($("#ezLocationText"+temp+"_error").text() != ""){
        $("#submit_edit_button1").prop("disabled", true);
    }else{
        $("#submit_edit_button1").prop("disabled", false);
    }
}
$("a.fa.fa-trash.fa-font.panel-heading").click(function (e) {
    var image_id = $(this).attr('id');
    e.preventDefault();
    if (confirm('Are you sure to Delete?')) {
        $.ajax({
            url: "/admins/merchant/document_delete",
            type: 'GET',
            data: {image_id: image_id},
            success: function (data) {
                if (data === true) {
                    $('img.' + image_id).hide();
                    $('a.' + image_id).hide();
                    $('.' + image_id).next().hide();
                    $('#cover-spin').fadeOut();
                }
            },
            error: function (data) {
                $('#cover-spin').fadeOut();
            }
        });
    }
    else {
        $('#cover-spin').fadeOut();
    }
});


// var country="";
// var errorMap = [ "Country code Invalid", "Invalid selected country", "Too short", "Too long", "Invalid number"];
//
// $('#country').on('change',function () {
//     debugger
//     if($(this).val() != ""){
//         $.ajax({
//             url: '/merchant/sales/country',
//             data: {country: $(this).val() },
//             method: 'get',
//             success: function (response) {
//                 var states = response["states"];
//                 $("#state").empty();
//                 $.each(states,function (index, value) {
//                     $("#state").append('<option value="' + index+ '">&nbsp;' + index + '</option>');
//                 })
//             }
//         });
//     }
// });

$(document).on("click", "#add_bank", function(){
    var count = $(this).data("count");
    var add_bank_html = $(".add-another-bank").html();
    var next_count = parseInt(count)+1
    add_bank_html = add_bank_html.replace(/XXXXX/g, next_count)
    $(".banks").append(add_bank_html)
    $(this).data("count", next_count);
})
$(document).on("click", ".remove-bank", function(){
    var count = $(this).data("count");
    $(".section"+count).remove();
})

$(document).on('change', '.bank-account-type', function () {
    var count = $(this).data("count");
    if($(this).val() != '')
    {
        $('#bank-routing'+count).attr('disabled',false);
        $('#password-field'+count).attr('disabled',false);
        $('#bank-name'+count).attr('disabled',false);
        $("#customer_signature").attr('disabled',false);

    } else
    {
        $('#bank-routing'+count).attr('disabled',true);
        $('#password-field'+count).attr('disabled',true);
        $('#bank-name'+count).attr('disabled',true);
        $("#customer_signature").attr('disabled',true);
    }
})
var del_img_ids = []
$(document).on("click", ".edit_bank_del_img", function () {
    var count = $(this).data("count")
    var img_id = $(this).data("img_id")
    del_img_ids.push(img_id)
    $('#image_space'+count).remove();
    $('.edit-image-tag'+count).remove();
    $('#customer_signature'+count).removeClass("hide-edit-img");
    $('#customer_signature'+count).attr('required','true');
    $("#del_imgs_ids").val(del_img_ids);
})
$(document).on('change', 'input[type="file"]', function(event) {
    var id = $(this).attr('id');
    var files = event.target.files;
    if(files != "" && files != null){
        $.each(files, function( index, value){
            fileExt = value.name.substr(value.name.lastIndexOf('.') + 1)
            if(fileExt === 'jpeg' || fileExt === 'jpg' || fileExt === 'png' || fileExt === 'gif'){
                var image = value
                var reader = new FileReader();
                reader.onload = function(file) {
                    var img = new Image();
                    img.src = file.target.result;
                    $("#"+id).next("span").append(img);
                    $("#"+id).next("span").append("<i class='fa fa-close fa-2x del-preview-img'></i>")
                }
                reader.readAsDataURL(image);
            }
        })
    }
});
$(document).on("click", ".del-preview-img", function () {
    $(this).parent().prev("input").val("");
    $(this).prev().remove();
    $(this).remove();
})
$(document).ready(emptyStateFunction);
$(document).ready(emptyStateFunctionPartners);
$(document).ready(emptyStateFunctionLocation);
function emptyStateFunction() {
    if($(".admins_country_field_cor").val() != "" && $(".admins_country_field_cor").val() != undefined){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(".admins_country_field_cor").val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0){
                    $('.admins_states_field').attr("required",true);
                    $('.admins_states_field').attr("disabled",false);

                } else{
                    $(".admins_states_field").empty();
                    $('.admins_states_field').attr("required",false);
                    $('.admins_states_field').attr("disabled",true);

                }

            }
        });
    }
}
function emptyStateFunctionPartners(){
    var no_of_owners = $(".owners_form").length;
    for (count_owner = 0; count_owner < no_of_owners; count_owner++) {
        resetOwnersStates(count_owner);
    }
}
function resetOwnersStates(count) {
    if($("#user_owner_"+count+"_country").val() != "" && $("#user_owner_"+count+"_country").val() != undefined){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $("#user_owner_"+count+"_country").val() , owner_field: 'change'},
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if (Object.keys(states).length > 0) {
                    // $("#user_owner_"+count+"_dl_state_issue").append('<option value="">&nbsp;Select State</option>');
                    $("#user_owner_" + count + "_state").attr("required", true);
                    $("#user_owner_" + count + "_dl_state_issue").attr("required", true);
                    $("#user_owner_" + count + "_state").attr("disabled", false);
                    $("#user_owner_" + count + "_dl_state_issue").attr("disabled", false);
                } else {
                    $("#user_owner_" + count + "_state").empty();
                    $("#user_owner_" + count + "_dl_state_issue").empty();
                    $("#user_owner_" + count + "_state").attr("required", false);
                    $("#user_owner_" + count + "_dl_state_issue").attr("required", false);
                    $("#user_owner_" + count + "_state").attr("disabled", true);
                    $("#user_owner_" + count + "_dl_state_issue").attr("disabled", true);

                }
            }
        });
    }
}

function emptyStateFunctionLocation() {
    if($(".admins_country_field_location").val() != "" && $(".admins_country_field_location").val() != undefined){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(".admins_country_field_location").val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0){
                    $('.admins_states_field_state').attr("required",true);
                    $('.admins_states_field_state').attr("disabled",false);
                } else{
                    $(".admins_states_field_state").empty();
                    $('.admins_states_field_state').attr("required",false);
                    $('.admins_states_field_state').attr("disabled",true);

                }

            }
        });
    }
}
