$('#alert-message-top').fadeOut(5000);
$(function() {
    // methods for sync process start
    $(".syncModal").on('click', function () {
        $('#sync_btn').show();
        $("#sync_close2").show();
        $('#body_1').show();
        $('#body_2').hide();
        $("#sync_done").hide();
    });
    var $statusElm = $("div.sync-status");
    $("#sync_close1").on('click', function(){
        $('#body_1').show();
        $('#sync_btn').show();
        $('#body_2').hide();
    });
    $("#sync_btn").on("click", function(e) {
        $(this).css("pointer-events", "none");
        var selected = $("#syncDate").val();
        $.ajax({
            url: "/admins/block_transactions/sync_transactions",
            dataType: "json",
            data: {date: selected}
        }).done(function(response, status, ajaxOpts) {
            console.log('Sync BTN regular: ', response);
            console.log('Sync BTN status: ', status);
            if (status === "success" && response && response.jid) {
                $('#sync_btn').hide();
                $("#body_1").hide();
                $("#body_2").show();
                global.jobId = response.jid;
                global.intervalName = "job_" + jobId;

                $statusElm.text("Calculating total transactions, few seconds remaining....");

                window[intervalName] = setInterval(function() {
                    getExportJobStatus(jobId, intervalName);
                }, 5000);
            }
        }).fail(function(error) {
            console.log('sync_transactions ERROR', error);
        });
    });

    function getExportJobStatus(jobId, intervalName) {
        $.ajax({
            url: "/admins/block_transactions/sync_status",
            dataType: "json",
            data: {
                job_id: jobId
            }
        }).done(function(response, status, ajaxOpt) {
            console.log("getExportJobStatus->response", response);
            console.log("getExportJobStatus", status);
            if (status === "success") {
                var total_transactions = response.total_transactions;
                if (total_transactions != 0) {
                    $statusElm.text("Exporting " + total_transactions + " transactions, few seconds remaining....");
                } else {
                    $statusElm.text("Calculating total transactions, few seconds remaining....");
                }
                $("#sync_close2").hide();
                if (response.status === "completed") {
                    // $statusElm.text("Export completed. Downloading...");
                    setTimeout(function() {

                        clearInterval(window[intervalName]);
                        delete window[intervalName];

                        $("#sync_close2").hide();
                        $("#sync_done").show();
                        $statusElm.text("Sync completed.");
                        $(location).attr("href", "/admins/block_transactions");
                    }, 5000);
                }
            }
        }).fail(function(error) {
            console.log(error);
        });
    }

    $("#sync_close2").on('click', function(){
        $('#body_1').show();
        $('#sync_btn').show();
        $('#body_2').hide();
        $("#sync_done").hide();
    });

    $("#sync_done").on('click', function(){
        $("#sync_close2").show();
        $("#sync_done").hide();
    });
    // methods for sync process end

    // methods for balance update start
    // var $statusBalance = $("div.balance-status");
    // $("#balance_btn").on("click", function(e) {
    //     $.ajax({
    //         url: "/admins/block_transactions/update_balances",
    //         dataType: "json"
    //     }).done(function(response, status, ajaxOpts) {
    //         if (status === "success" && response && response.jid) {
    //             global.jobId = response.jid;
    //             global.intervalName = "job_" + jobId;
    //
    //             window[intervalName] = setInterval(function() {
    //                 getUpdateBalanceStatus(jobId, intervalName);
    //             }, 8000);
    //         }
    //     }).fail(function(error) {
    //         console.log(error);
    //     });
    // });

    // function getUpdateBalanceStatus(jobId, intervalName) {
    //     $.ajax({
    //         url: "/admins/block_transactions/balance_status",
    //         dataType: "json",
    //         data: {
    //             job_id: jobId
    //         }
    //     }).done(function(response, status, ajaxOpt) {
    //         if (status === "success") {
    //             if (response.status === "completed") {
    //                 // $statusElm.text("Export completed. Downloading...");
    //                 setTimeout(function() {
    //
    //                     clearInterval(window[intervalName]);
    //                     delete window[intervalName];
    //
    //                     $("#balance_done").show();
    //                     $statusBalance.text("Balance update completed.");
    //                     $(location).attr("href", "/admins/block_transactions");
    //                 }, 5000);
    //             }
    //         }
    //     }).fail(function(error) {
    //         console.log(error);
    //     });
    // }
});