$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000);
    $(".padd").css("margin", "3px 15px 20px 0px");
    var table = $('#datatable-keytable1').DataTable({
        responsive: false,
        "sorting": true,
        "bLengthChange": false,
        "searching": false,
        "bInfo": false,
        "bPaginate": false,
        // "order": [[ 0, "asc" ]],
        "columnDefs": [
            {"orderable": false, "targets": 6}
        ]
    });
    $(document).on('click','.merchant_bank_click',function () {
        $('#cover-spin').show(0)
    })
    $(document).on('keypress','.key_9_number',function () {
        if($(this).val().length>=9) {
           return false
        }
    })
    $(document).on('keypress','.key_5_number',function () {
        if($(this).val().length>=5) {
           return false
        }
    })
    $(document).on('keypress','.key_17_number',function () {
        if($(this).val().length>=17) {
           return false
        }
    })
    $(document).on('keypress','.key_10_number',function () {
        if($(this).val().length>=10) {
           return false
        }
    })
});