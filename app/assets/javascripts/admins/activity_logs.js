
$(document).ready(function () {
    $(document).on('change','.merchant_filter',function () {
        $('#merchant_filter').submit();
    })
    $('#alert-message-top').fadeOut(5000)
//.............................index.html.erb....................................//
    window.onload  = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    $("#offset").val(moment().local().format('Z'));
    //........................email_track.html.erb.....................//

    $('input[name="q[sent_at_eq]"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name= "q[sent_at_eq]"]').daterangepicker({
        format: 'mm-dd-yyyy'
    });
    $('input[name="query[date]"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="query[date]"]').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        var endDate = picker.endDate;
        $('input[name="query[date]"]').val(startDate.format('MM/DD/YYYY') + ' - ' + endDate.format('MM/DD/YYYY'));
    });
    var nowDate = new Date();
    var newdate = (nowDate.setDate(nowDate.getDate()));
    var nd = new Date(newdate);
    $('input[name="query[date]"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY',
        },
        "maxSpan": {
            "days": 90
        },
        "maxDate": nd,
        "parentEl": $("#date_parent")
    });

    $('input[name="query[date]"]').on('hide.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    //.............................table.html.erb............................//
    $('#logs').DataTable({
        "order": [[2, "desc"]],
        "bPaginate": false,
        "searching": false,
        "bInfo": false,
        "columnDefs": [
            {"orderable": false, "targets": 5}
        ]
    });

    ///......................email_track.js.erb..................
    const urlParams = new URLSearchParams(window.location.search);
    const queryParam = urlParams.get('q');
    const dateParam = urlParams.get('date');
    if(queryParam != ""){
        if(dateParam == ""){
            $('input[name="q[sent_at_eq]"]').val('')
        }
    }
    else {
        $('input[name="q[sent_at_eq]"]').val('')
    }
    $(document).on('change','.api_state_input',function () {
        var api_name = $(this).attr('id');
        api_state_change(api_name);
    })
    $(document).on('change','.update_seq_down_input',function () {
        var update_seq= $('.update_seq_down_input').data();
        update_sequence_down(update_seq);
    });
    $(".number_only_field").inputmask('Regex', {regex: "^\\d+\\\\d{0,2}$"});
});

function api_state_change(api) {
    seq= api;
    var api_auth = gon.apis_config;
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        if (isConfirm.value === true) {
            var api_auths = JSON.parse(api_auth);
            $.ajax({
                url: "/admins/app_configs",
                type: 'POST',
                data: { id: api_auths.id, key: api_auths.key, app_config: {stringValue: api}},
                success: function(data) {
                    swal("Updated!", "API updated successfully ", "success");
                    // api_auth = $.parseJSON(data.stringValue);
                },
                error: function(error){
                    console.error('Update Check Issue Error: ', error);
                    swal("Cancelled", "An unexpected error occured :)", "error");
                }
            });
        } else {
            $('#' + seq).prop("checked", !$('#' + seq).is(':checked'));
        }
    });
}

function update_sequence_down(api) {
    var api_auth = gon.apis_config;
    seq= api;
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        if (isConfirm.value === true) {
            $.ajax({
                url: "/admins/app_configs/",
                type: 'POST',
                data: { id: seq.seqDown.id },
                success: function(data) {
                    swal("Updated!", "API updated successfully ", "success");
                    api_auth = $.parseJSON(data.stringValue);
                },
                error: function(error){
                    console.error('Update Check Issue Error: ', error);
                    swal("Cancelled", "An unexpected error occured :)", "error");
                }
            });
        } else {
            $('#' + seq.seqDown.key).prop("checked", !$('#' + seq.seqDown.key).is(':checked'));
        }
    });
}