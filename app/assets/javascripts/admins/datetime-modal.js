$(document).ready(function () {
    $('#password').val('')
    var lt= $('.modaltime').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    $('#password_btn').on('click',function () {
        var password = $('#password').val();
        if(password.length > 0) {
            $.ajax({
                url: "/admins/buy_rate/verify_admin_password",
                method: 'get',
                data: {password: password},
                success: function (data) {
                    $('#unlock').modal('toggle');
                    $('.close_unlock').click();
                    $("#password_btn").css("background", "green").text('').text("Verified");
                    $("#info-text").text('');
                    $.ajax({
                        url: "/admins/transactions/block_card",
                        method: 'post',
                        data: {card_id: $('#card_id').val(),from_admin: true},
                        success: function (data) {
                            if (data.success=='blocked'){
                                $('.unblock_btn').text('').text('Unblock');
                                swal("Updated!", "Card is Blocked successfully", "success")
                            }
                                else if(data.success=='Unblocked'){
                                $('.unblock_btn').text('').text('Block');
                                swal("Updated!", "Card is Unblocked successfully", "success")
                            }

                        },
                        error: function (data) {
                            swal("Cancelled!", "There is something wrong!", "error")
                        }
                    });
                },
                error: function (data) {
                    $("#password_btn").css("border", "1px solid red");
                    $("#info-text").text('').css("color", "red").text('Please try again!');
                }
            });
        }
    });
});