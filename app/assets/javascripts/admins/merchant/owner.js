var owner_form_id = [];
$(document).ready(function () {
    
    // $('.owners_form').each(function (key,owner) {
    //     var countryId = $("#" + owner.id).find(".admins_country_field").attr('data');
    //     var countryName = $("#" + owner.id).find(".admins_country_field").val();
    //     if (countryName == "Canada") {
    //         $(".disable" + countryId).val("");
    //         $(".disable" + countryId).attr('disabled', 'disabled');
    //         $(".disable" + countryId).attr('required', false);
    //         $(".disable" + countryId).parent().removeClass('has-error');
    //         $(".disable" + countryId).parent().find('span').hide();
    //     } else {
    //         $(".disable" + countryId).attr('disabled', false);
    //         $(".disable" + countryId).attr('required', true);
    //     }
    //
    // });
    //
    // $(document).on('change', ".admins_country_field", function () {
    //     var countryId = $(this).attr('data');
    //     var countryName = $(this).val();
    //
    //     if(countryName == "Canada"){
    //         $(".disable" + countryId).val("");
    //         $(".disable" + countryId).attr('required', false);
    //         $(".disable" + countryId).attr('disabled', 'disabled');
    //         $(".disable" + countryId).parent().removeClass('has-error');
    //         $(".disable" + countryId).parent().find('span').hide();
    //     } else {
    //         $(".disable" + countryId).attr('disabled', false);
    //         $(".disable" + countryId).attr('required', true);
    //     }
    //
    // });

    validateOwnersForm("new_user");
    $(document).on('click','.wizard-submit-btn', function () {
        var all = calculateOwnership();
        if(all < 50){
            //alert("Ownership can't be less than 50%");
            swal("Alert!","Ownership can't be less than 50%", "error");
            return false;
        }
    });


    var l = $('.ownership').length;
    var all = 0;
    for (i = 0; i < l; i++) {
        if($('.ownership').eq(i).val() !== ""){
            all = parseFloat(all) + parseFloat($('.ownership').eq(i).val());
            index = i+1;
        }
    }
    $(document).on('click','.wizard-submit-btn', function () {
        var phone = $("#phone_number").val();
        if(phone != undefined && phone.length < 1 ){
            if(!$(".new_user").valid()){
                $('.wizard-submit-btn').prop('disabled', 'disabled');
                $(".phone-error").text('');
                $("#phone_number-error").text('');
                $(".phone-error").text('Please enter Phone Number');
                $(".phone-heading").addClass("phone-valid");
            }

        }

    })

    $('#submit_edit_button1').on('click', function () {
        if ($("#merchantform").valid() && $(".has-error").text()=== ""){
            event.preventDefault(event);
        }
        else{
            event.preventDefault(event);

        }

    });

    // ************** Datepicker js ***************
    var no_of_owners = $(".owners_form").length;
    if(no_of_owners>0){
        for(var i=0;i<=no_of_owners-1;++i){
    $('#date_of_birth_'+i).daterangepicker({
        autoUpdateInput:false,
        minDate: 1901,
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: moment().subtract(18, 'years'),
        startDate: moment().subtract(18, 'years'),
        parentEl: '#date_parent',
        locale: { direction: 'datepicker_'+i }
    });}}
    $('.datepicker_owner_edit').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('.datepicker_owner_edit').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate;
        $(this).val(startDate.format('MM/DD/YYYY'));
    });
    $("#ownership").inputmask('Regex', {regex: "^[0-9]{1,99}?$"});
    $("#phone_number").inputmask('Regex', {regex: "^[0-9+]+$"});
    $('.re_default').val('');
    $('.ssn').mask('000-00-0000');

    $('body').on('keyup', '.phone-class', function (e) {
        var phone = $(this).val();
        var heading = $(this).parent().prev();
        var phone_code = $(this).closest('div').find('.selected-dial-code').text();
        var reg = new RegExp('^[0-9]+$');
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var phone_id = $(this).attr("id");
        var submit_btn_id = "#"+$(this).closest('form').find(':submit').attr("id")
        if(phone.length > 0){
            if(reg.test(phone_number)){
                $("#"+phone_error_div).text('');
                $(submit_btn_id).prop( 'disabled', false);
                 $.ajax({
                     url: "/admins/companies/check_phone_format",
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if (!result) {
                            heading.addClass("phone-valid");
                            $(submit_btn_id).prop('disabled', 'disabled');
                            $("#" + phone_error_div).text('Invalid Format!');
                        }
                        else{
                            heading.removeClass("phone-valid");
                            $("#" + phone_error_div).text('')
                            $(submit_btn_id).prop('disabled', false);
                        }
                    },
                    error: function (error) {

                        console.log(error)
                    }
                });
            }else{
                $(submit_btn_id).prop('disabled', 'disabled');
                heading.removeClass("phone-valid");
                $("#"+phone_error_div).text('')
                $("#"+phone_error_div).text('Must be Number')
            }
        }else{
            $(submit_btn_id).prop('disabled', 'disabled');
            $("#"+phone_error_div).text('');
            heading.addClass("phone-valid");
            $("#"+phone_error_div).text('Please enter Phone Number')
        }

    });
    $(document).on('click','.remove_owner',function () {
        var temp = [];
        var no_of_owners = $(".owners_form").length;
        if(no_of_owners > 1) {
            var owner_id = $(this).data('attr');
            var form_id = $("#edit_from_remove").val();
            owner_form_id.push(owner_id);
            $.unique(owner_form_id);
            $("#edit_from_remove").val(owner_form_id.join(','));
        }
    });

    var edit_from = $("#edit_from").val();
    if(edit_from == "edit-owners"){
        $(".owners_form .col-md-6").addClass("col-md-5").removeClass("col-md-6")
        $(".owners_form .states-md").addClass("col-md-2").removeClass("col-md-3")
    }
})

$('body').on('keyup', '#phone_number', function (e) {
    var phone = $(this).val();
    var phone_code = $(this).closest('div').find('.selected-dial-code').text();
    var reg = new RegExp('^[0-9]+$');
    phone_code = phone_code.substring(1);
    var phone_number = phone_code + phone;
    var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
    var phone_id = $(this).attr("id");
    var submit_btn_id = "#"+$(this).closest('form').find(':submit').attr("id")
    if(phone.length > 0){
        if(reg.test(phone_number)){
            $("#"+phone_error_div).text('');
            $(submit_btn_id).prop( 'disabled', false);
            $.ajax({
                url: "/admins/companies/check_phone_format",
                type: 'GET',
                data: {phone_number: phone_number},
                success: function (result) {
                    if (!result) {
                        $("#phone_heading_second").addClass("phone-valid");
                        $(submit_btn_id).prop('disabled', 'disabled');
                        $("#" + phone_error_div).text('Invalid Format!');
                    }
                    else{
                        $("#phone_heading_second").removeClass("phone-valid");
                        $("#" + phone_error_div).text('')
                        $(submit_btn_id).prop('disabled', false);
                    }
                },
                error: function (error) {

                    console.log(error)
                }
            });
        }else{
            $(submit_btn_id).prop('disabled', 'disabled');
            $("#phone_heading_second").removeClass("phone-valid");
            $("#"+phone_error_div).text('')
            $("#"+phone_error_div).text('Must be Number')
        }
    }else{
        $(submit_btn_id).prop('disabled', 'disabled');
        $("#"+phone_error_div).text('');
        $("#phone_heading_second").addClass("phone-valid");
        $("#"+phone_error_div).text('Please enter Phone Number')
    }

});

function calculateOwnership(){
    var owner_length = $('.ownership').length;
    var all = 0;
    var index = 0;
    for (i = 0; i < owner_length; i++) {
        if($('.ownership').eq(i).val() !== ""){
            all = parseFloat(all) + parseFloat($('.ownership').eq(i).val());
            index = i+1;
        }
    else if(all < 50){
        if($('.ownership').eq(index).val() !== ""){
            duplicateOwner()
        }
    }else if(all >= 50){
        if($('.ownership').eq(index).val() === ""){
            //$('.duplicate_owner').html('');
            $(".count").text(owner_length + 1)
        }
    }
    }
    return all
}
function myFunc(id){
    var phone = $("#"+id).val();
    var phone_code_id=id.split('r')
    phone_code_id=phone_code_id[1]
    var txt1 = "<span style='color:red' class=\"phone-error1\"></span>";
    var phone_code = $(".phone-box .selected-dial-code").text()

    var reg = new RegExp('^[0-9]+$');
    phone_code = phone_code.substring(1);
    if (phone_code==''){
        phone_code= $('#phone_code'+phone_code_id).val()
    }
    var phone_number = phone_code + phone;
    if(phone.length > 0){
        if(phone_number.match(reg)){
            $(".phone-error1").text('');
            $('#submit_edit_button1').prop('disabled', false);
        }else{
            $('#submit_edit_button1').prop('disabled', 'disabled');
            $(".phone-error1").text('');
            $("span").remove(".phone-error1");
            $("#"+id).after(txt1);
            $(".phone-error1").text('Must be Number')
        }
    }else{
        $('#submit_edit_button1').prop('disabled', 'disabled');
        $(".phone-error1").text('');
        $("span").remove(".phone-error1");
        $("#"+id).after(txt1);
        $(".phone-error1").text('Please enter Phone Number')
    }
}
// if(performance.navigation.type == 2){
//     location.reload(true);
// }