$(document).ready(function () {
    $(".wizard-submit-btn").click(function (e) {
        validateIsoAndAgentForm("new_user");
        if($(".new_user").valid()){
            $('#new_user').submit();
        }
    })
    var location_data = null;
    if(value_present(gon.location_data)){
     location_data = gon.location_data.table;
    }
    if(location_data != null){
        if(location_data.profit_split != null){
            $('#profit_splits_on_section').show();
        }else{
            $('#profit_splits_on_section').hide();
        }
        if(location_data.baked_iso != null){
            $('#baked_section').show();
        }else{
            $('#baked_section').hide();
        }
        if(location_data.sub_merchant_split != null){
            $("#ez_section").show();
        }else{
            $("#ez_section").hide();
        }
        if(location_data.net_profit_split != null){
            $("#net_profit_splits_on_section").show();
        }else{
            $("#net_profit_splits_on_section").hide();
        }
    }else{
        $('#baked_section').hide();
        $("#ez_section").hide();
    }
    var wallets = "";
    var key_name = "";
    ;
    // profit split js
    if(value_present(location_data) && location_data.profit_split == true){
        $('#profit_splits_radio_section').show()
        if(value_present(location_data.profit_split_detail)){
            var split_detail = JSON.parse(location_data.profit_split_detail);
            $(split_detail["splits"]).each(function (key, array) {
                var arr_data = [];
                $.map(array, function (val, i) {
                    if(!i.includes('net') && !i.includes('ez') && (i.includes('company') || i.includes('merchant') || i.includes('iso') || i.includes('agent') || i.includes('affiliate'))){
                        key_name = i.toString().replace(/[0-9]/g, '');
                        arr_data.push(val);
                    }
                });
                arr_data = arr_data[0];
                if(value_present(key_name) && value_present(arr_data) && !key_name.includes('ez')){
                    if(key_name.includes('merchant')){
                        wallets = gon.profit_split_data;
                        $('#super_company_percent').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][percent]");
                        $('#super_company_percent').val(arr_data.percent);
                        $('#companyInput').attr('name', 'profit_splits[splits]['+key_name+arr_data.wallet+'][name]');
                        $('#companyInputHidden').attr('name', 'profit_splits[splits]['+key_name+arr_data.wallet+'][wallet]');
                        $('#companyInput').val(arr_data.name);
                        $('#iso_company_radio').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][from]");
                        $('#gbox_company_radio').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][from]");
                        if(value_present(arr_data.from) && arr_data.from == "gbox"){
                            $("#gbox_company_radio").attr("checked", "checked");
                        }else if(value_present(arr_data.from) && arr_data.from == "iso"){
                            $("#iso_company_radio").attr("checked", "checked");
                        }
                        $('#companyInputHidden').val(arr_data.wallet);
                        if(value_present(wallets)){
                            $('#locationDiv').css('visibility', 'visible');
                            $('#locationText').css('visibility', 'visible');
                            var option = new Option("Choose Location", "");
                            $(option).html("Choose Location");
                            $('#locationText').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][wallet]");
                            $("#locationText").append(option);
                            $.map(wallets, function (wallet) {
                                var option = new Option(wallet.name, wallet.location_id);
                                $(option).html(wallet.name);
                                $("#locationText").append(option);

                            });
                            $('#locationText option[value='+arr_data.wallet+']').prop("selected", "selected")
                        }
                    }else{
                        $('#super_company_percent').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][percent]");
                        $('#super_company_percent').val(arr_data.percent);
                        $('#companyInput').attr('name', 'profit_splits[splits]['+key_name+arr_data.wallet+'][name]');
                        $('#companyInputHidden').attr('name', 'profit_splits[splits]['+key_name+arr_data.wallet+'][wallet]');
                        $('#companyInput').val(arr_data.name);
                        $('#iso_company_radio').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][from]");
                        $('#gbox_company_radio').attr('name', "profit_splits[splits]["+key_name+arr_data.wallet+"][from]");
                        if(value_present(arr_data.from) && arr_data.from == "gbox"){
                            $("#gbox_company_radio").attr("checked", "checked");
                        }else if(value_present(arr_data.from) && arr_data.from == "iso"){
                            $("#iso_company_radio").attr("checked", "checked");
                        }
                        $('#companyInputHidden').val(arr_data.wallet);
                    }
                }
            })
        }
    }
    else {
        $('#profit_splits_radio_section').hide()
    }
    if(value_present(location_data) && location_data.baked_iso == true){
        $('#baked_profit_radio_section').show()
        if(value_present(location_data.profit_split_detail)){
                split_detail=JSON.parse(location_data.profit_split_detail)
                if(split_detail["share_split"] == "gbox"){
                    $("#gbox_radio").attr("checked", "checked");
                }else if(split_detail["share_split"] == "iso"){
                    $("#iso_radio").attr("checked", "checked");
            }
        }
    }
    else{
        $('#baked_profit_radio_section').hide()
    }

    if(location_data != null && location_data.baked_iso == true){
        $("#baked_iso_switch").attr("checked", "checked");
    }
    if(location_data != null && location_data.sub_merchant_split == true){
        $("#sub_merchant_split_switch").attr("checked", "checked");
    }
    if(location_data != null && location_data.net_profit_split == true){
        $("#net_profit_split_switch").attr("checked", "checked");
    }
    if(location_data != null && location_data.profit_split == true){
        $("#profit_split_switch").attr("checked", "checked");
    }

    // ez_merchant js
    if(value_present(location_data) && location_data.sub_merchant_split == true){
        if(value_present(location_data.profit_split_detail)){
            var split_detail = JSON.parse(location_data.profit_split_detail);
            $(split_detail["splits"]["ez_merchant"]).each(function (key, array) {
                array = array[0];
                var wallet_id = "";
                $.map(array, function (val, i) {
                    wallet_id = val.wallet
                });
                if(value_present(gon)){
                    wallets = gon.ez_merchant_data
                }
                $('#ezLocationDiv'+key).css('visibility', 'visible');
                var option = new Option("Choose Location", "");
                $(option).html("Choose Location");
                $('#ezLocationText'+key).css('visibility', 'visible').attr('name', "profit_splits[splits][ez_merchant]["+ key+"][ez_merchant_1][wallet]");
                $("#ezLocationText"+key).append(option);
                $(wallets).each(function (index, wallet) {
                    var option = new Option(wallet.name, wallet.location_id);
                    $(option).html(wallet.name);
                    $("#ezLocationText"+ key).append(option);
                })
                $('#ezLocationText'+ key + ' option[value='+wallet_id+']').prop("selected", "selected");
            });
        }
    }

    //**************** net profit split js ****************************

    if(value_present(location_data) && location_data.net_profit_split){
        $('#net_profit_splits_on_section').show()
        $("#net_gbox_company_radio").prop("checked", true)
        $('#net_check').show()
    }
    else{
        $('#net_profit_splits_on_section').hide()
        $("#net_gbox_company_radio").prop("checked", false)
        $('#net_check').hide()
    };

    if(value_present(location_data) && location_data.net_profit_split == true){
        $('#net_profit_split_gbox').show()
        if(value_present(location_data.profit_split_detail)){
            var split_detail = JSON.parse(location_data.profit_split_detail);
            var wallet_id = null;
            var name = "";
            $.map(split_detail["splits"], function (array, key){
                ;
                if(key == "net"){
                    $.map(array, function (val, i) {
                        wallet_id = val.wallet;
                        key = i;
                        name = val.name;
                        percent = val.percent;;
                    });
                    key_name = key.toString().replace(/[0-9]/g, '');
                    if(value_present(key_name)){
                        if(key_name.includes('merchant')){
                            wallets = gon.net_profit_locations;
                            $('#net_super_company_percent').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][percent]").val(percent);
                            $('#netCompanyInput').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][name]').val(name);
                            $('#netCompanyInputHidden').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][wallet]').val(wallet_id);
                            $('#net_gbox_company_radio').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][from]");
                            ;
                            if(value_present(wallets)){
                                $('#netLocationDiv').css('visibility', 'visible');
                                var option = new Option("Choose Location", "");
                                $(option).html("Choose Location");
                                $('#netLocationText').css('visibility', 'visible').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][wallet]");
                                $("#netLocationText").append(option);
                                if(value_present(wallets)){
                                    $.map(wallets, function (wallet) {
                                        var option = new Option(wallet.name, wallet.location_id);
                                        $(option).html(wallet.name);
                                        $("#netLocationText").append(option);
                                    })
                                }
                                $('#netLocationText option[value='+wallet_id+']').prop("selected", "selected");
                            }
                        }else{
                            $('#net_super_company_percent').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][percent]").val(percent);
                            $('#netCompanyInput').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][name]').val(name);
                            $('#netCompanyInputHidden').attr('name', 'profit_splits[splits][net]['+key_name+wallet_id+'][wallet]').val(wallet_id);
                            $('#net_gbox_company_radio').attr('name', "profit_splits[splits][net]["+key_name+wallet_id+"][from]");
                        }
                    }
                }
            });
        }
    }
    else {
        $('#net_profit_split_gbox').hide()
    }
    // *********** End net profit split js ****************
});