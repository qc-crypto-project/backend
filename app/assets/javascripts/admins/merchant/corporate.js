$(document).ready(function () {
    validateCorporateInfoForm("new_user");
    $("#years_in_business").inputmask('Regex', {regex: "^[0-9]{1,99}(\\d{1,2})?$"});




    $('#phone_number').on('click blur paste keyup change', function () {
        var phone = $("#phone_number").val();
        var phone_code = $(".selected-dial-code").text();
        var reg = new RegExp('^[0-9]+$');
        var code = $('.selected-dial-code').text();
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;

        if(phone.length > 0){
            if(phone_number.match(reg)){
                $.ajax({
                    url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if(result){
                            if (code == "+1")
                            {
                                var number=  $('#phone_number').val();
                                if (number.length>10)
                                {
                                    $('.wizard-submit-btn').prop('disabled', 'disabled');
                                    if ($("#phone_number-error").text() == "") {
                                        $(".phone-error").text('Invalid format or phone number is already taken!')
                                    }
                                    else{
                                        $(".phone-error").text('')
                                    }
                                }
                                else{

                                    $(".phone-error").text('')
                                    $('.wizard-submit-btn').prop('disabled', false);
                                }
                            }
                            else{

                                $(".phone-error").text('')
                                $('.wizard-submit-btn').prop('disabled', false);
                            }
                        }else{
                            $('.wizard-submit-btn').prop('disabled', 'disabled');
                            if ($("#phone_number-error").text() == "") {
                                $(".phone-error").text('Invalid format or phone number is already taken!')
                            }
                            else{
                                $(".phone-error").text('')
                            }
                        }
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }else{
                $('.wizard-submit-btn').prop('disabled', 'disabled');
                $(".phone-error").text('');
                $(".phone-error").text('Must be Number')
            }
        }else{
            $('.wizard-submit-btn').prop('disabled', 'disabled');
            $(".phone-error").text('');
            if (!$(".corporate_phone_number").hasClass('has-error')){
                $("#corporate_phone_error").text('');
            }
        }
    })
    if(performance.navigation.type == 2){
        if($("#user_profile_attributes_country").val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $("#user_profile_attributes_country").val(),owner_field: 'change' },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                $("#user_profile_attributes_state").empty();
                    $.each(states,function (index, value) {
                        $('#merchant_state').val()
                        $("#user_profile_attributes_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                    })
                }
            });
        };
    }
})