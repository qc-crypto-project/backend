$(document).ready(function () {
    $(".wizard-submit-btn").click(function (e) {
        validateLocationFeeForm("new_user")
    })
    $(".new_user").validate({
        ignore: "#user_location_fee_service_date, #user_location_fee_statement_date, #user_location_fee_misc_date"
    })
})