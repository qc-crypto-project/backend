$(document).ready(function () {
    $(".number_fields").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $(".wizard-submit-btn").click(function (e) {
        validateSettingForm("new_user")
        if($(".new_user").valid()){
            $('#new_user').submit();
        }
    })

    $('#user_location_apply_load_balancer').change(function() {
        if($(this).is(":checked")) {
            $('#apply_load_balancer').show()
        }else{
            $('#apply_load_balancer').hide()
        }
    });


    if ($("#user_location_transaction_limit").is(':checked')){
        $("#tran_amount_limit").show();
    }else {
        $("#tran_amount_limit").hide();
    }


    $( "#user_location_transaction_limit" ).change(function () {
        if($(this).is(':checked')){
            $("#tran_amount_limit").show();
        }
        else{
            $("#tran_amount_limit").hide();
        }
    });

    $('.add_amount_btn').click(function ()
    {
        if($(".amount_limit_div").length < 10 ){
            $("#tran_amount_limit").append('<div class="form-group col-md-12 amount_limit_div" ><div class="col-md-3"></div><span class="span_dollar_sign">$</span> <div class="col-md-3"> <input value="" name="user[location][transaction_limit_offset][]" class="form-control two_decimal"></div><div class="col-md-3"><a class="website_remove_field remove_amount_btn"><i class="fa fa-minus-circle fa-2x"></i></a> </div></div>');
            $(".remove_amount_btn").on("click",function(){
                $(this).closest('.amount_limit_div').remove()
            });
        }
        $(".two_decimal").inputFilter(function(value) {
            return /^\d*(\.\d{0,2})?$/.test(value);});
    });

    $(".remove_amount_btn").on("click",function(){
        $(this).closest('.amount_limit_div').remove()
    });


})