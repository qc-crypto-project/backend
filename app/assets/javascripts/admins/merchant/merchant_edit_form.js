var corporate_form = true;
var phone_numb = true;
var password_format = false;
$(document).ready(function () {
    edit_merchant_validation('merchant', "notuser");
    $("#years_in_business").inputmask('Regex', {regex: "^[0-9]{1,99}(\\d{1,2})?$"});

    // edit form phone javascript

    $("#phone_number_cor").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#phone_code_cor').val(Object.values(selectedCountryData)[2]);
            $('#phone_number_cor').val('');
            space_remove(Object.values(selectedCountryData)[2], 'phone_number_cor');
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });

    $(".phone_number").inputmask('Regex', {regex: "^[0-9+]+$"});

    $("#phone_numberr").inputmask('Regex', {regex: "^[0-9+]+$"});

    $("#phone_number_cor").inputmask('Regex', {regex: "^[0-9+]+$"});

    $('.phone_number').each(function () {
        var id = $(this).attr('id');
        var phone_code_id = $(this).next().attr("id");
        $("#"+id).intlTelInput({
            formatOnInit: true,
            separateDialCode: true,
            formatOnDisplay: false,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                country=selectedCountryPlaceholder;
                country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                $("#"+phone_code_id).val(Object.values(selectedCountryData)[2]);
                space_remove(Object.values(selectedCountryData)[2], id);
                if(selectedCountryData.iso2 == 'us')
                {
                    country = "555-555-5555"
                }
                return  country;
            },
            excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
            utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
        });
    })

    $('#phone_number_cor').on('keyup', function () {
        var phone = $("#phone_number_cor").val();
        var phone_code = $("#phone_code_cor").val();
        var reg = new RegExp('^[0-9]+$');
        // phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        if(phone.length > 0){
            $(".phone-error").text('');
            if(phone_number.match(reg)) {
                $.ajax({
                    url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if(result){
                            $(".phone-error").text('')
                            $('#submit_edit_button1').prop('disabled', false);
                        }else{
                            $('#submit_edit_button1').prop('disabled', 'disabled');
                            if ($(".phone-error").text() == "Please enter Phone Number"){
                                $(".phone-error").text('');
                                $(".phone-error").text('Please enter Phone Number');
                            }else{
                                $(".phone-error").text('Invalid format or phone number is already taken!')
                            }
                        }
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }else{
                $('#submit_edit_button1').prop('disabled', 'disabled');
                $(".phone-error").text('');
                $(".phone-error").text('Must be Number')
            }
        }else{
            $('#submit_edit_button1').prop('disabled', 'disabled');
            $(".phone-error").text('');
            $(".phone-error").text('Please enter Phone Number');
        }
    });
    $('.datepicker_owner').daterangepicker({
        parentEl: '#date_parent',
        minYear: 1901,
        changeMonth: true,
        changeYear: true,
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: moment().subtract(18, 'years'),
        startDate: moment().subtract(18, 'years'),
    });
    $(document).on('change','.error_text',function () {
        $('.error_years').text('');
    })
    $(document).on('click', "#submit_edit_button1", function (e) {
        e.preventDefault()
        var all = calculateOwnership();
        var password = $("#user_password").val();
        var password_confirm = $("#user_password_confirmation").val();
        var year = $('#user_years_in_business_year-error').text()
        var month = $('#user_years_in_business_month-error').text()
        var dob_error = $('#date_of_birth-error').text()
        var owner_country = $('.owner_country_val').val();
        var owner_state = $('.owner_state_val').val();
        var owner_dl_state = $('.owner_dl_state_val').val();
        var corporate_field_validation = true
        var owner_field_validation = true
        $('.error_years').text('');
        var active_tab = $(".from-tabs.active").attr("id")
        if(active_tab != "corporate_li"){
            $(".corporate-field-validation").each(function(){
                if(!value_present($(this).val())){
                    if(($(this).attr('id') == "user_years_in_business_year" && $(this).val()=="0") || ($(this).attr('id') == "user_years_in_business_month" && $(this).val()=="0") ){
                        owner_field_validation = true
                    }else {
                        corporate_field_validation = false
                    }
                }
            });
        }else if(active_tab == "corporate_li"){
            corporate_field_validation = $("#merchantform").valid();
        }
        if(active_tab != "owner_li"){
            $(".owner-field-validation").each(function(){
                if(!value_present($(this).val())){
                    owner_field_validation = false
                }
            });
        }else if(active_tab == "owner_li"){
            owner_field_validation = $("#merchantform").valid();
        }
        if(all < 50){
            console.log(all);
            //alert("Ownership can't be less than 50%");
            swal("Alert!","Ownership can't be less than 50%", "error");
            return false;
        }
        if ($("#resetpassword").hasClass("show")){
            if(password == "" && password_confirm == "" ){
                password_format = false;
            }else if (password == password_confirm){
                password_format = true;
            }else {
                password_format = false;
            }
        }else{
            password_format = true;
        }
        
        if ($("#merchantform").valid() && corporate_field_validation && owner_field_validation && phone_numb && all >=50 && password_format && (password == password_confirm) && (year == "" || month == "") && $('.date_of_birth_error').text()=="") {
            $('#merchantform').submit();
        }
        else {
            $('#error').html('Please fill required fields of ').addClass('error_class');
            if(!password_format || password != password_confirm){
                $('#error').append('- Password Info ');
            }
            else if(!corporate_field_validation || !owner_field_validation || !phone_numb || $('.date_of_birth_error').text()=="" || dob_error==""){
                if(dob_error != "" || $('.date_of_birth_error').text() != "" || !corporate_field_validation ){
                    $('#error').append('- Corporate Info ');
                }else {
                    $('#error').append('- Owner Info ');
                }
            }else{
                if(year != "" || month != "" || value_present(owner_country) !="" || value_present(owner_state) !="" || value_present(owner_state) != ""){
                    $('#error').append('- Corporate Info ');
                }else {
                    $('#error').append('- Owner Info ');
                }
            }
            $("#error").fadeIn();
            $("#error").delay(3000).fadeOut(300);
            e.preventDefault(e);
        }
    });

    $(document).on("click", ".from-tabs", function () {
        $("#merchantform").valid();
    })

//    ************************ Settings js ******************************

    if($("#sub_merchant_split_switch").is(':checked')){
        $('#ez_merchant_on').show();
    } else{
        $('#ez_merchant_on').hide();
    }

    $('#user_password').val('');
    $(document).on('change','#sub_merchant_split_switch',function(){
        if($(this).is(':checked')){
            $('#ez_merchant_on').show();
        } else{
            $('#ez_merchant_on').hide();
        }
    });
    if ($('#sub_merchant_split_switch').is(':checked'))
    {
        if ($("#ez_merchant_0").val()!="" && $("#ez_merchant_0Hidden").val()==""){
            feed = {id: id+'_error', for:"User"};
            error_ids.push(feed);
        }
        else if($("#ez_merchant_0").val()!="" && $("#ez_merchant_0Hidden").val()!="")
        {
            if ($(merchant_location).val()=="" || $(merchant_location).val()==null){
                feed = {id: merchant_location+'_error', for:"Location"};
                error_ids.push(feed);
            }
        }
        $('#ez_merchant_on').show();
    }

    $('.two_step').click(function () {
        var merchant_id = $("#setting_merchant_id").val();
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/merchants/update_two_step",
                    type: 'get',
                    data: {merchant_id: merchant_id},
                    success: function (data) {
                        if ($('.two_step_text').text() == "On") {
                            $('.two_step_text').text('').text('Off');
                            $('.two_step_color').css('color', 'red');
                        } else if ($('.two_step_text').text() == "Off") {
                            $('.two_step_text').text('').text('On');
                            $('.two_step_color').css('color', 'green');
                        }
                        swal("Updated!", "User updated successfully", "success");
                    },
                    error: function (error) {
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
            }
        });
    });

    $('#reset').click(function () {
        var merchant_eamil = $("#setting_merchant_email").val();
        if ($("#unlock_token").val().length > 0) {
            swal("Cancelled", "Please use Re-send Unlock Email", "error");
        } else {
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                closeOnCancel: true
            }).then((isConfirm) => {
                if (isConfirm.value === true) {
                    $('#cover-spin').show(0);
                    $.ajax({
                        url: "/admins/merchant/reset.user",
                        type: 'post',
                        data: {
                            user: {email: merchant_eamil},
                            csrf_token: "c7058a2ee1eb40b3c06330cf2a7be92d",
                            from_admin: true
                        },
                        success: function (data) {
                            $('#cover-spin').fadeOut("slow");
                            swal("Reset!", "Reset instructions sent successfully", "success");
                        },
                        error: function (error) {
                            $('#cover-spin').fadeOut("slow");
                            swal("Cancelled", "An unexpected error occured :)", "error");
                        }
                    });
                } else {
                }
            });
        }
    });

    $('#reset_unlock').click(function () {
        var merchant_id = $("#setting_merchant_id").val();
        if($("#unlock_token").val().length == 0 && $("#locked_at").val().length == 0){
            swal("Cancelled", "Already Unlocked", "error");
        }else {
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                closeOnCancel: true
            }).then((isConfirm) => {
                if (isConfirm.value === true) {
                    $.ajax({
                        url: "/admins/merchants/send_unblock_email",
                        type: 'get',
                        data: {merchant_id: merchant_id},
                        success: function (data) {
                            swal("Updated!", "Reset email sent successfully", "success");
                        },
                        error: function (error) {
                            swal("Cancelled", "An unexpected error occured :)", "error");
                        }
                    });
                } else {
                }
            });
        }
    });

    $('#resend_welcome').click(function () {
        var merchant_id = $("#setting_merchant_id").val();
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        }).then((isConfirm) => {
            if (isConfirm.value === true) {
                $.ajax({
                    url: "/admins/merchants/resend_welcome_email",
                    type: 'get',
                    data: {merchant_id: merchant_id},
                    success: function (data) {
                        swal("Updated!", "Welcome email sent successfully", "success");
                    },
                    error: function (error) {
                        swal("Cancelled", "An unexpected error occured :)", "error");
                    }
                });
            } else {
            }
        });
    });

    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    var lt= $('.modalTime1').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    var lt= $('.modalTime').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    $(document).on('keyup','#ownership',function () {
        var value = $(this).val()
        if(value<1 && value>0) {
            var number = parseFloat(value)
            $(this).val(number);
        }
    });

    $(document).on("focusin", "#user_password", function () {
        $('.strength').css('display', "block");
    });

    $(document).on("focusout", "#user_password", function () {
        if ($('#length').prop("checked") == true && $('#one_number').prop("checked") == true && $('#upper_case').prop("checked") == true && $('#lower_case').prop("checked") == true) {
            password_format = true;
            $('.strength').css('display', "none");
        }
        else {
            password_format = false;
        }
    });

    $("#user_password").on("keyup", function () {
        var password = $(this).val();
        $("#strength_human").html('').append('<li class="list-group-item"><input id="length" disabled="true" type="checkbox">8-20 characters long</li><li class="list-group-item"><input id="one_number"  disabled="true"  type="checkbox">one number</li><li class="list-group-item"><input id="upper_case" disabled="true" type="checkbox">one uppercase letter (A-Z)</li><li class="list-group-item"><input id="lower_case" disabled="true"  type="checkbox">one lowercase letter (a-z)</li>')
        if (password.length >= 8 && password.length <= 20) {
            $('#length').attr('checked', true);
        } else {
            $('#length').attr('checked', false);
        }
        if (/[0-9]/.test(password)) {
            $('#one_number').attr('checked', true);
        } else {
            $('#one_number').attr('checked', false);
        }
        if (/[A-Z]/.test(password)) {
            $('#upper_case').attr('checked', true);
        } else {
            $('#upper_case').attr('checked', false);
        }
        if (/[a-z]/.test(password)) {
            $('#lower_case').attr('checked', true);
        } else {
            $('#lower_case').attr('checked', false);
        }
    });
    // $.validator.addMethod("user[password]", function(value) {
    //     return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) && /[a-z]/.test(value) && /\d/.test(value) && /[A-Z]/.test(value);
    // });

    //..........................corporate.html.erb...................//

    $('#phone_number').on('click blur paste keyup', function () {
        var phone = $("#phone_number").val();
        var phone_code = $(".selected-dial-code").text();
        var reg = new RegExp('^[0-9]+$');
        var code = $('.selected-dial-code').text();
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;

        if (phone.length > 0) {
            if (phone_number.match(reg)) {
                $.ajax({
                    url: "/admins/companies/form_validate?user_id=" + $('#user_form_id').val(),
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if (result) {
                            if (code == "+1") {
                                var number = $('#phone_number').val();
                                if (number.length > 10) {
                                    $('.wizard-submit-btn').prop('disabled', 'disabled');
                                    $(".phone-error").text('Invalid format or phone number is already taken!')
                                } else {

                                    $(".phone-error").text('')
                                    $('.wizard-submit-btn').prop('disabled', false);
                                }
                            } else {

                                $(".phone-error").text('')
                                $('.wizard-submit-btn').prop('disabled', false);
                            }
                        } else {
                            $('.wizard-submit-btn').prop('disabled', 'disabled');
                            $(".phone-error").text('Invalid format or phone number is already taken!')
                        }
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            } else {
                $('.wizard-submit-btn').prop('disabled', 'disabled');
                $(".phone-error").text('');
                $(".phone-error").text('Must be Number')
            }
        } else {
            $('.wizard-submit-btn').prop('disabled', 'disabled');
            $(".phone-error").text('');
            // $(".phone-error").text('Please enter Phone Number');
        }
    });


    //...........merchant_profile.html.erb...///////////
    /*$("#phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
            country = selectedCountryPlaceholder;
            country = country.replace(/[^0-9\. ]/g, '').replace(/\W/gi, '');
            $('#phone_code').val(Object.values(selectedCountryData)[2]);
            $('#phone_number').val('');
            $(".phone-error").text('')
            space_remove(Object.values(selectedCountryData)[2], '#phone_number');
            if (selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return country;
        },
        excludeCountries: ['ba', 'cf', 'cd', 'cg', 'do', 'gq', 'pf', 'gw', 'mk', 'mg', 'nc', 'kp', 'mf', 'pm', 'vc', 'st', 'ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });*/
    $("#cs_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
            country = selectedCountryPlaceholder;
            country = country.replace(/[^0-9\. ]/g, '').replace(/\W/gi, '');
            $('#cs_number_code').val(Object.values(selectedCountryData)[2]);
            $('#cs_number').val('');
            space_remove(Object.values(selectedCountryData)[2], "#cs_number");
            if (selectedCountryData.iso2 == 'us') {
                country = "555-555-5555"
            }
            return country;
        },
        excludeCountries: ['ba', 'cf', 'cd', 'cg', 'do', 'gq', 'pf', 'gw', 'mk', 'mg', 'nc', 'kp', 'mf', 'pm', 'vc', 'st', 'ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });

    $(".editModal").on("shown.bs.modal", function(){

    })
})

function space_remove(code_number, id) {
    if(code_number.length==1) {
        $('#'+id).addClass("phone-length-1");
    } else if(code_number.length==2) {
        $('#'+id).addClass('phone-length-2');
    } else if(code_number.length==3) {
        $('#'+id).addClass('phone-length-3');
    } else {
        $('#'+id).addClass('phone-length-4');
    }
}
$(function () {
    var count = 0;
    $('input[type="file"]').on('change', function (event) {
        var id = $(this).attr('id');
        $(`#${id}-error`).hide();
        // $(`.col-md-8.${id} .row .col-md-12 img`).remove();
        var files = event.target.files;
        var files_count = files.length;
        $.each(files, function (index, value) {
            var image = value.name;
            var reader = new FileReader();
            var div_id = id.slice(0,-1);
            if(image.length>14){
                image_name = image.substring(0,11) +"..." + image.slice(-5)
            }else{
                image_name = image
            }
            if (files_count===1){
                $('.'+id).prepend("<div class='row "+div_id+"' id="+div_id+"><div class='col-md-12'><div class='col-sm-3'></div><div class='col-sm-5 selected-file'>" + image_name +"</div></div></div>");
            }
            else{
                $('.'+id).prepend("<div class='row "+div_id+"'   id="+div_id+"><div class='col-md-12'> <div class='col-sm-3'></div><div class='col-sm-5 selected-file'>" + image_name +"</div></div></div>");
            }
        });
        // if (files_count === 0) {
        //     $(`.col-md-8.${id} .row .col-md-12 img`).remove();
        // }
    });
});
$('input[type="file"]').on('click',function(){
    var files = $(this)[0].files;
    var id = $(this).attr('id');
    var div_id = id.slice(0,-1);
    $('div.row #'+div_id).remove();
    if(files.length>0) {
        $(this).addClass('image_have')
    }else{
        $(this).addClass('image_have');
    }
});
$("a.fa.fa-close.fa-2x.panel-heading").click(function (e) {
    var image_id = $(this).attr('id');
    e.preventDefault();
    if (confirm('Are you sure to Delete?')) {
        $.ajax({
            url: "/admins/merchants/document_delete",
            type: 'GET',
            data: {image_id: image_id},
            success: function(data) {
                $('img.' + image_id).hide();
                $('span.' + image_id).hide();
                $('.' + image_id).hide();
                $('#cover-spin').fadeOut();
            },
            error: function (data) {
                $('#cover-spin').fadeOut();
            }
        });
    }
    else {
        $('#cover-spin').fadeOut();
    }
});
$(document).on('change','.admins_country_field_cor_edit',function () {
    if($(this).val() != ""){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(this).val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                $("#state").empty();
                if(Object.keys(states).length > 0){
                    $.each(states,function (index, value) {
                        $("#state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                    });
                    $(".admins_states_field_cor_edit").attr("required",true);
                    $(".admins_states_field_cor_edit").attr("disabled",false);
                }else{
                    $(".admins_states_field_cor_edit").attr("required",false);
                    $(".admins_states_field_cor_edit").attr("disabled",true);
                }

            }
        });
    }
});
$(document).ready(emptyStateFunction);
function emptyStateFunction() {
    if($(".admins_country_field_cor_edit").val() != "" && $(".admins_country_field_cor_edit").val() != undefined){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(".admins_country_field_cor_edit").val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0){
                    $('.admins_states_field_cor_edit').attr("required",true);
                    $('.admins_states_field_cor_edit').attr("disabled",false);
                    $(".admins_states_field_cor_edit").addClass('corporate-field-validation');
                } else{
                    $(".admins_states_field_cor_edit").empty();
                    $('.admins_states_field_cor_edit').attr("required",false);
                    $('.admins_states_field_cor_edit').attr("disabled",true);
                    $(".admins_states_field_cor_edit").removeClass('corporate-field-validation');
                }

            }
        });
    }
}
$(document).ready(emptyStateFunctionPartners);
function emptyStateFunctionPartners(){
    var no_of_owners = $(".owners_form").length;
    for (count_owner = 0; count_owner < no_of_owners; count_owner++) {
        resetOwnersStates(count_owner);
    }
}
function resetOwnersStates(count) {
    if($("#user_owner_"+count+"_country").val() != "" && $("#user_owner_"+count+"_country").val() != undefined){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $("#user_owner_"+count+"_country").val() , owner_field: 'change'},
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0){
                    // $("#user_owner_"+count+"_dl_state_issue").append('<option value="">&nbsp;Select State</option>');
                    $("#user_owner_"+count+"_state").attr("required",true);
                    $("#user_owner_"+count+"_dl_state_issue").attr("required",true);
                    $("#user_owner_"+count+"_state").attr("disabled",false);
                    $("#user_owner_"+count+"_dl_state_issue").attr("disabled",false);
                    $("#user_owner_"+count+"_state").addClass('owner-field-validation');
                }else{
                    $("#user_owner_"+count+"_state").empty();
                    $("#user_owner_"+count+"_state").removeClass('owner-field-validation');
                    $("#user_owner_"+count+"_dl_state_issue").removeClass('owner-field-validation');
                    $("#user_owner_"+count+"_dl_state_issue").empty();
                    $("#user_owner_"+count+"_state").attr("required",false);
                    $("#user_owner_"+count+"_dl_state_issue").attr("required",false)
                    $("#user_owner_"+count+"_state").attr("disabled",true);
                    $("#user_owner_"+count+"_dl_state_issue").attr("disabled",true);
                }

            }
        });
    }
}