$(document).ready(function () {
    $("#email").on('keyup',function () {
        $("#email").val($("#email").val().replace(/\s/g, ''));
    })
    validateMerchantLocationForm("new_user");

    $("#location_phone_number, #cs_number").inputmask('Regex', {regex: "^[0-9+]+$"});

    $('#location_phone_number').on('keyup', function (e) {
        var phone = $(this).val();
        var phone_code = $(this).closest('div').find('.selected-dial-code').text();
        var reg = new RegExp('^[0-9]+$');
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var submit_btn_id = "#"+$(this).closest('form').find(':submit').attr("id")
        if(phone.length > 0){
            if(reg.test(phone_number)){
                $("#"+phone_error_div).text('');
                $(submit_btn_id).prop( 'disabled', false);
                $.ajax({
                    url: "/admins/companies/check_phone_format",
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if (!result) {
                            $("#phone_heading_second").addClass("phone-valid");
                            $(submit_btn_id).prop('disabled', 'disabled');
                            $("#" + phone_error_div).text('Invalid Format!');
                        }
                        else{
                            $("#phone_heading_second").removeClass("phone-valid");
                            $("#" + phone_error_div).text('')
                            $(submit_btn_id).prop('disabled', false);
                        }
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }else{
                $(submit_btn_id).prop('disabled', 'disabled');
                $("#phone_heading_second").removeClass("phone-valid");
                $("#"+phone_error_div).text('')
                $("#"+phone_error_div).text('Must be Number')
            }
        }else{
            $(submit_btn_id).prop('disabled', 'disabled');
            $("#"+phone_error_div).text('');
            $("#phone_heading_second").addClass("phone-valid");
            setTimeout(function(){
                if($("#location_phone_number-error").text()==''){
                    $("#"+phone_error_div).text('Please enter Phone Number')
                }
            }, 1);
        }
    });

    $('#cs_number').on('keyup', function (e) {
        var phone = $(this).val();
        var phone_code = $(this).closest('div').find('.selected-dial-code').text();
        var reg = new RegExp('^[0-9]+$');
        phone_code = phone_code.substring(1);
        var phone_number = phone_code + phone;
        var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
        var submit_btn_id = "#"+$(this).closest('form').find(':submit').attr("id")
        if(phone.length > 0){
            if(reg.test(phone_number)){
                $("#"+phone_error_div).text('');
                $(submit_btn_id).prop( 'disabled', false);
                $.ajax({
                    url: "/admins/companies/check_phone_format",
                    type: 'GET',
                    data: {phone_number: phone_number},
                    success: function (result) {
                        if (!result) {
                            $("#cspn_heading").addClass("phone-valid");
                            $(submit_btn_id).prop('disabled', 'disabled');
                            $("#" + phone_error_div).text('Invalid Format!');
                        }
                        else{
                            $("#cspn_heading").removeClass("phone-valid");
                            $("#" + phone_error_div).text('')
                            $(submit_btn_id).prop('disabled', false);
                        }
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }else{
                $(submit_btn_id).prop('disabled', 'disabled');
                $("#cspn_heading").removeClass("phone-valid");
                $("#"+phone_error_div).text('')
                $("#"+phone_error_div).text('Must be Number')
            }
        }else{
            $(submit_btn_id).prop('disabled', 'disabled');
            $("#"+phone_error_div).text('');
            $("#cspn_heading").addClass("phone-valid");
            setTimeout(function(){
                if($("#cs_number-error").text()==''){
                    $("#"+phone_error_div).text('Please enter Customer Service Number')
                }
            }, 1);
        }
    });

    $("#location_phone_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#location_phone_code').val(Object.values(selectedCountryData)[2]);
            $('#location_phone_number').val('');
            space_remove(Object.values(selectedCountryData)[2], '#location_phone_number');
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
})

var table = $('#datatable-keytable-locations').DataTable({
    // "responsive": true,
    "sScrollX": false,
    "sSearch": false,
    "ordering": true,
    "searching": false,
    "bLengthChange": true,
    "bPaginate": false,
    "bInfo": false,
    "order": [[ 0, "asc" ]],

});
function space_remove(code_number, phone_id) {
    if(code_number.length==1)
    {
        $(phone_id).css('padding-left', '3.9rem', 'important');

        // $(phone_id).addClass('phone-length-1');

    }
    else if(code_number.length==2)
    {
        $(phone_id).css('padding-left', '4.3rem', 'important');

        // $(phone_id).addClass('phone-length-2');
    }
    else if(code_number.length==3)
    {
        $(phone_id).css('padding-left', '4.8rem', 'important');

        // $(phone_id).addClass('phone-length-3');
    }
    else
    {
        $(phone_id).css('padding-left', '5.2rem', 'important');

        // $(phone_id).addClass('phone-length-4');
    }
}
if(performance.navigation.type == 2){
    location.reload(true);
}

