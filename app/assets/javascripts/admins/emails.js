
$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000);
    function close_master_modal(id) {
        $('.modal-backdrop').remove();
    }

    function submitTemplate(id) {
        if($('#subject').val() != ''){
            $('#sub_error').hide();
            if (id!='custom_date1'){
                $('#schedule_date1').val($('#'+id).text());
                $('#from_suggestion').val("true");
            }
            else{
                $('#datepickererror').hide();
                $('#schedule_date1').val($('#datetimepicker12').val());
            }
            $("#new_form").submit();
        }
        else{
            $('#sub_error').show();
            $('#datepickererror').text('')
            $("#datetimepicker12").after("<span class='error' style='color: red;' id='datepickererror'>Please fill missing fields!</span>")
            $('#datepickererror').show();
        }
    }

    var globalTimeout = null;
    $('#srch-term').on('keyup change', function () {
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function () {
            globalTimeout = null;
            search_value = $('#srch-term').val();
            var filter = $('#filter_input').val();
            $('#q').val(search_value);
            filter_data = filter
            $('#user_data').val(user_data);
            $.ajax({
                url: '/admins/emails',
                type: 'GET',
                data: {q: search_value, filter: filter_data}
            });

        }, 1500);
    });
    //.............................emails/_data_table.html.erb..................//

    $('#email_table').DataTable({
        "searching": false,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "ordering": false
    });

    var lt = $('.timeUtc').each(function (i, v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });
    $(document).on('click','.email_click',function () {
        $('#cover-spin').show(0);
    });
    $(document).on('change','#filter',function () {
        this.form.submit();
    });

});