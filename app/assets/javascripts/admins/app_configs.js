
$(document).ready(function () {
    $('#errormessage').fadeOut(2500);
    $('#alert-message-top').fadeOut(5000);
//..............................app_config/merchant_configs_logs.html.erb....................//
        var lt= $('.timeUtc').each(function (i,v) {
            var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
            var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
            v.innerText = local

        });

        var table = $("#app_config-datatable").DataTable({
            "responsive": true,
            //"sScrollX": false,
            "sSearch": false,
            "bInfo": false,
            "bPaginate": false,
            "searching": false,
            "bLengthChange": false,
            "ordering": true,
            "order": [[ 3,"desc" ]],
            "columnDefs": [
                { "targets":5, "orderable": false }
            ]

        });
//..........................................app_configs/index.html....................//

    var val = $('input[type="radio"]:checked').val();
    if (val == 'bridge_pay'){
        $('#bridgeCollapse').addClass('in');
    }
    var globalTimeout = null;
    $(document).on('change keyup','#srch-term',function () {
        if ($(this).data('load-balancer')) {
            search(this.value, '/admins/app_configs/load_balancer_configs');
        } else {
            search(this.value, '/admins/app_configs/merchant_app_configs');
        }
    });

    search = (value, url) => {
        search_value = value.split('.')[0];
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        if(value_present(search_value)){
            time_out = 1000;
        }else{
            time_out = 0
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            $('#q').val(search_value);
            var filter_id = $('#filter_input').val();
            var status_val = $('#params_req_input').val();
            $.ajax({
                url: url,
                type: 'GET',
                data: {q: search_value,filter: filter_id,status: status_val,  search_input:"search_input"},
            });
        }, time_out);
    }
    //.....................................system_configs_logs.html.erb............//

    $("#datatable-keytable").DataTable({
        responsive: true,
        "sScrollX": false,
        "sscrollX": false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "orderable": false },
        ],
        destroy: true
    });

    //........................................app_configs.html.erb...//////////

    var val = $('input[type="radio"]:checked').val();
    if (val == 'bridge_pay') {
        $('#bridgeCollapse').addClass('in');
    }
    // $('#bank_form input[type="radio"]').on('change', function (e) {
    //     var val = $('input[type="radio"]:checked').val();
    //     var stripe_id = $('#stripe_config_id').val()
    //     if (val == 'bridge_pay') {
    //         $('#bridgeCollapse').addClass('in');
    //     } else {
    //         $('#bridgeCollapse').removeClass('in');
    //     }
    //     $.ajax({
    //         url: "/admins/app_configs/atm_config/" + stripe_id,
    //         type: 'get',
    //         data: {id: stripe_id, value: e.target.value},
    //         success: function (data) {
    //             $('article.content').prepend(data);
    //         },
    //         error: function (data) {
    //             $('article.content').prepend(data);
    //         }
    //     });
    // });
    //.......................................data_table.js.erb....................//
    var table = $('#gatetable').DataTable({
        "responsive": true,
        //"sScrollX": false,
        "sSearch": false,
        "bInfo": false,
        "bPaginate": false,
        "searching": false,
        "bLengthChange": false,
        "ordering": true,
        "order": [[ 1,"desc" ]],
        "columnDefs": [
            { "targets": [0,3,4,5,6,7], "orderable": false }
        ]
    });
    $(document).on('change','#filter',function () {
        $('#verification_gate_filter').submit();
    })
    var isAllChecked = $("#is_all_check").val();
    var checkboxes = $(".all");
    var checkedArr = [];
    var page = $('#page_param_input').val()
    checkedArr = $("#checked_arr").val();
    var unCheckedArr = [];
    unCheckedArr = $("#un_checked_arr").val();
    if(unCheckedArr=="undefined" || checkedArr=="undefined"){
    checkedArr = checkedArr.split(",");
    unCheckedArr = unCheckedArr.split(",");
    if(isAllChecked == "true"){
        checkboxes.prop("checked", true);
        $('#all-check').prop("checked", true)
    }else{
        checkboxes.prop("checked",false);
        // $("#bank_div").css('display', 'none');
    }
    if(page > 1){
        $("#check_all").hide();
    }
    $(".all").on("click", function () {

        var isChecked = $(this).prop("checked");
        var targetValue = $(this).val();
        if(isChecked){
            checkedArr.push(targetValue);
            unCheckedArr = unCheckedArr.filter(val => !targetValue.includes(val));
        }
        else{
            unCheckedArr.push(targetValue);
            checkedArr = checkedArr.filter(val => !targetValue.includes(val));
        }
        if(checkedArr[0] === ""){
            checkedArr.shift();
        }
        if(unCheckedArr[0] === ""){
            unCheckedArr.shift();
        }
        $("#checked_arr").val(checkedArr);
        $("#un_checked_arr").val(unCheckedArr);
    })

    for(var i = 0; i < unCheckedArr.length; i++){
        $("#checkbox_animated_"+unCheckedArr[i]).prop("checked", false)
    }
    checkedArr = checkedArr.filter(val => !unCheckedArr.includes(val));
    for(var i = 0; i < checkedArr.length; i++){
        $("#checkbox_animated_"+checkedArr[i]).prop("checked", true)
    }}

    $(document).on('click','#adavance_setting',function () {
        var checkbox = $(this).val()
        if (checkbox == 1){
            $('.merchant-app-config-container-fluid').show()
            $(this).val('0')
        }else {
            $('.merchant-app-config-container-fluid').hide()
            $(this).val('1')
        }
    })

    //....................................merchant_list.html.erb..................//
    var isselect = false;
    var first = true;
    var selected = [];
    $(document).on('keyup','#percentage_field',function () {
        on_change_percentage();
    })
    $('#easyPaginate').easyPaginate({
        elementsPerPage: 5
    });

    $('.easyPaginateNav > a').click(function () {
        // alert('yesy')
        // sleep(1);
        if (isselect) {
            var checkboxes = $(".all");
            checkboxes.prop("checked", true);
        }
    });
    // check all check boxes
    $(document).on("click", '#check_all', function() {
        // grouping all the checkbox using the classname
        var checkboxes = $(".all");
        // check whether checkboxes are selected.
        if(checkboxes.prop("checked")){
            $("#is_all_check").val("false");
            isselect = false;
            // hide hidden div
            $("#bank_div").hide();
            // if they are selected, unchecking all the checkbox
            checkboxes.prop("checked",false);
            selected = [];
        } else {
            isselect = true;
            $("#is_all_check").val("true");
            // show hidden div
            $("#bank_div").show();
            // if they are not selected, checking all the checkbox
            checkboxes.prop("checked", true);
            selected = $("#all_locations_map").val();
            selected = JSON.parse(selected)
            first = false;
            console.log("success");
        }
    });
    // single checkbox value
    $(document).on('change', 'input[type="checkbox"]', function(){
        if ($(this).is(':checked')){
            // show hidden div
            selected = selected.filter(val => !$(this).val().includes(val));
            selected.push($(this).val());
            selected = selected.filter(val => !"on".includes(val));
            if(selected.length > 1) {
                $("#bank_div").show();
            }
        } else {
            selected.pop($(this).val());
            if(selected.length <= 1){
                $("#bank_div").hide();

            }
        }
    });

    $('#update_gateway').on('click',function () {
        var checkedArr = $("#checked_arr").val();
        var unCheckedArr = $("#un_checked_arr").val();
        var all_list = $("#all_locations_map").val();
        all_list = JSON.parse(all_list);
        all_list = all_list.filter(val => !unCheckedArr.includes(val));
        if($("#all-check").prop("checked")){
            $('#locations_').val(all_list);
        }else{
            checkedArr = selected
            $('#locations_').val(checkedArr);
        }
    });

})
