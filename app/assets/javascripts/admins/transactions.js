$(document).ready(function () {
    $('#alert-message-top').fadeOut(5000);
    $('#alert-message-top').fadeOut(5000)
    $( "#close1" ).click(function(){
        $('#modal_id').removeClass();
        $('#modal_id').modal('hide');
    })
    $(document).mouseup(function (e)
    {
        var container = $("#tx_id"); // YOUR CONTAINER SELECTOR
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
    $( "#tx_idd" ).click(function() {
        // $('#welcome').show()
        $('#tx_id').toggle();
    });
    // $(document).ready(function(){
    //     $(".tip-top").tooltip({placement : 'top'});
    // });
    $('#cover-spin').fadeOut("slow");
    $('[data-toggle="popover"]').popover({container: 'body'});

    $(".tip-top").tooltip({placement : 'top'});

    function copyToClipboard(element) {
        var copyText = element;
        copyText.select();
        document.execCommand("copy");
    }
    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy'
    });
    $('.modalTime').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local;
    });
    $(document).on('change','#tx_filter',function () {
        $('#iso_agent_form').submit();
    })
    $(".tip-top").tooltip({placement : 'top'});

    function copyToClipboard(element) {
        var copyText = element;
        copyText.select();
        document.execCommand("copy");
    }
    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy'
    });

    $('.modalTime').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local;
    });

    var table = $('#transactions-datatable').DataTable({
        "dom": 'Bfrtip',
        "sScrollX": false,
        "scrollX": false,
        "sSearch": true,
        "ordering": true,
        "processing": true,
        "searching": true,
        "order": [[ 7, "desc" ]],
        "serverSide": true,
        "buttons": ['copyHtml5', 'excelHtml5', 'pdfHtml5']
    });
    $(document).on('click','.released_click',function () {
        $('#cover-spin').show(0)
    })
    var lt= $('.timeUtctransactions').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });

    $( "#close1" ).click(function(){
        $('#modal_id').removeClass();
        $('#modal_id').modal('hide');
    })
    $(document).mouseup(function (e)
    {
        var container = $("#tx_id"); // YOUR CONTAINER SELECTOR
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
    $( "#tx_idd" ).click(function() {
        // $('#welcome').show()
        $('#tx_id').toggle();
    });
    // $(document).ready(function(){
    //     $(".tip-top").tooltip({placement : 'top'});
    // });
    $('#cover-spin').fadeOut("slow");
    $('[data-toggle="popover"]').popover({container: 'body'});
    // $(document).ready(function () {
    //     hello
    //     function copyToClipboard(element) {
    //         var copyText = element;
    //         copyText.select();
    //         document.execCommand("copy");
    //     }
    //     $('.datepicker').datepicker({
    //         format: 'mm-dd-yyyy'
    //     });
    //
    //     $('.modalTime').each(function (i,v) {
    //         var gmtDateTime = moment.utc(v.innerText, 'YYYY-MM-DD HH:mm:ss');
    //         var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
    //         v.innerText = local;
    //     });
    // });

    var table = $('#transactions-datatable_export').DataTable({
        "dom": 'Bfrtip',
        "sScrollX": false,
        "scrollX": false,
        "sSearch": true,
        "ordering": true,
        "processing": true,
        "searching": true,
        "order": [[ 7, "desc" ]],
        "serverSide": true,
        "buttons": ['copyHtml5', 'excelHtml5', 'pdfHtml5']
    });

    $(".padd").css("margin","15px 15px 20px 0px");

    var table = $('#datatable-keytable').DataTable({
        responsive:false,
        "sorting": true,
        "bLengthChange": false,
        "searching": false,
        "bInfo":false,
        "bPaginate": false,
        "order": [[ 4, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 4 }
        ]
    });
});
function reports_count(id) {
    count = parseInt($("#reports_read_count").text());
    status = $("#a_"+id).data('status');
    if(status == "false" || status == false){
        if(count == 1){
            $('#reports_read_count').parent().parent().hide();
        }else {
            $("#reports_read_count").text(count - 1);
        }
        $("#a_"+id).data('status',"true");
    }
    $('#'+id).hide();
}