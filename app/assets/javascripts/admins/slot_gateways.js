$('#alert-message-top').fadeOut(5000);
$( document ).ready(function() {
    //.................................slot_gateways/index.html.................//
$('.payment_gateway_client_secret').hide();
$('.payment_gateway_client_id').hide();
$('.payment_gateway_authentication_id').hide();
$('.payment_gateway_signature_maker').hide();
$('.payment_gateway_processor_id').hide();
$('.new_gateway').click(function(){
    $('.myModal').modal();
});

    var table = $('#keytable').DataTable({
        "responsive": true,
        "sScrollX": false,
        "sSearch": true,
        "ordering": true,
        "searching": true,
        "bLengthChange": true,
        "order": [[ 0, "desc" ]]
    });
    $(document).on('click','.slot_gate_click',function () {
        $('#cover-spin').show(0);
    });
    $(document).on('click','#attemptamount',function () {
        SelectAll('amount');
    })
    //.....................................slot_gateways/form.html.erb............//

    $('#submit_btn').attr('disabled','disabled');
    var fewSeconds = 2;
    $('#plus_id').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        setTimeout(function(){
            btn.prop('disabled', false);
        }, fewSeconds*1000);
    });
    $('.fee_fields').bind('keyup change', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });
    $("#payment_gateway_slot_name").inputmask({"mask": "aaaa_9"});
    $("#payment_gateway_slot_name").on("blur",function () {
        var value = $("#payment_gateway_slot_name").val()
        $.ajax({
            url: "/admins/slot_gateways/validate_slot",
            type: 'GET',
            data: {slot_name: value},
            success: function(data) {
                $('#submit_btn').removeAttr('disabled');
                $('#error_span').text('');
            },
            error: function(data){
                $('#error_span').text('Slot Already Exit, Please try with new slot!');
                $('#submit_btn').attr('disabled','disabled');
            }
        });
    });
    //......................................slot_gateways/archived_slot_gateways.html.............//

    $('#payment_gateway_type').on('change', function () {
        if ($(this).val() == "stripe") {
            $("label[for='payment_gateway_client_id']").text('').text('Client ID');
            $("label[for='payment_gateway_client_secret']").text('').text('Client Secret');
            $("label[for='payment_gateway_authentication_id']").text('').text('Authentication');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        } else if ($(this).val() == "converge") {
            $("label[for='payment_gateway_client_id']").text('').text('Converge Merchant ID');
            $("label[for='payment_gateway_client_secret']").text('').text('Converge Pin');
            $("label[for='payment_gateway_authentication_id']").text('').text('Converge User ID');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').show();
            $('.payment_gateway_signature_maker').hide();
        } else if ($(this).val() == "fluid_pay") {
            $("label[for='payment_gateway_client_id']").text('').text('Client ID');
            $("label[for='payment_gateway_client_secret']").text('').text('Client Secret');
            $("label[for='payment_gateway_authentication_id']").text('').text('Authentication');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_processor_id').show();
            $('.payment_gateway_client_id').hide();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        } else if ($(this).val() == "i_can_pay") {
            $("label[for='payment_gateway_client_id']").text('').text('Auth Password');
            $("label[for='payment_gateway_client_secret']").text('').text('Secret Key');
            $("label[for='payment_gateway_authentication_id']").text('').text('Authentication ID');
            $("label[for='payment_gateway_signature_maker']").text('').text('Signature Maker');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').show();
            $('.payment_gateway_signature_maker').show();
        } else if ($(this).val() == "bolt_pay") {
            $("label[for='payment_gateway_client_secret']").text('').text('Merchant Token');
            $("label[for='payment_gateway_client_id']").text('').text('API Key');
            $('.payment_gateway_client_secret').show();
            $('.payment_gateway_client_id').show();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        } else {
            $('.payment_gateway_client_secret').hide();
            $('.payment_gateway_client_id').hide();
            $('.payment_gateway_authentication_id').hide();
            $('.payment_gateway_signature_maker').hide();
        }
    });
    //................................slot_managment.html.erb.................////////////
    $(document).on('click keyup','.percent1_over',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        percentage_cal('1_over',"1_over_"+id)
    });
    $(document).on('click','.percent1_over',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        SelectAll('1_over_'+id);
    });
    $(document).on('click keyup','.perc_over2',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        percentage_cal('2_over',"2_over_"+id)
    });
    $(document).on('click','.perc_over2',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        SelectAll('2_over_'+id);
    });
    $(document).on('click keyup','.perc_under1',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        percentage_cal('1_under',"1_under_"+id)
    });
    $(document).on('click','.perc_under1',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        SelectAll('1_under'+id);
    });
    $(document).on('click keyup','.perc_under2',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        percentage_cal('2_under',"2_under_"+id)
    });
    $(document).on('click','.perc_under2',function () {
        var var_id = $(this).attr('id')
        var id = var_id.slice(-1)[0];
        SelectAll('2_under_'+id);
    });

    function percentage_cal(key,id){
        var sum = 0;
        $('.perc_attempt'+key).each(function () {
            sum += Number($(this).val());
        });
        if (sum > 100) {
            $("#attempt"+id).val('');
            alert('Percentage should be equal to 100!');
        }
    }

    function SelectAll(id) {
        $('#attempt' + id).focus();
        $('#attempt' + id).select();
    }
//.............................notification_vertical_types.html.erb..................//
    (document).on('change', '#attempt11', function(){
        var size = $("#slot_gateway_size_val11").val()
        $(".slot_management_size11").css( "width", size );
    });
    (document).on('change', '#attempt12', function(){
        var size = $("#slot_gateway_size_val12").val()
        $(".slot_management_size12").css( "width", size );
    });
    (document).on('change', '#attempt21', function(){
        var size = $("#slot_gateway_size_val21").val()
        $(".slot_management_size21").css( "width", size );
    });
    (document).on('change', '#attempt22', function(){
        var size = $("#slot_gateway_size_val22").val()
        $(".slot_management_size22").css( "width", size );
    });
    $('#close_modal_button').click(function(){
        setTimeout(function(){
            $(document.body).addClass( 'modal-open' );
        }, 500);
    });
    $('#dispute_reason_form_title_field').keypress(function(){
        if(this.value=="" || this.value==null){
            $('#warning_showing_span').text("Fill Up Your Fields!");
        }else{
            $('#warning_showing_span').text("");
        }
    });

    $( "#dispute_reason_form_submit" ).one( "click", function() {
        $('#notificationReasonModal').modal('toggle');
        var payment_id = $('#format_input').val();
        $.ajax({
            url: "<%=postverticaltype_admins_payment_gateways_path%>",
            type: 'POST',
            data: {title:$('#dispute_reason_form_title_field').val(), pay_id: payment_id},
            success: function(result){
                if(result.message){
                    $('#warning_showing_span').text(result.message);
                }else{
                    $('#table_text_field_transaction_dispute').append($('<option></option>').attr('value',result.id).text(result.title));
                    $('#table_text_field_transaction_dispute_edit').append($('<option></option>').attr('value',result.id).text(result.title));
                }
                $("#dispute_reason_form_title_field2").val('');
                return true;
            }});
    });

});