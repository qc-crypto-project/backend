$('#alert-message-top').fadeOut(5000);
$(document).ready(function() {
    $('select#assigned_to').select2({
        placeholder: "Select User",
        // allowClear: true
    });

    var lt= $('.timeUtc').each(function (i,v) {
        var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
//           var gmtDateTime = moment.utc(v.innerText).toDate();
        var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
        v.innerText = local
    });

    // var lt= $('.timeUtc').each(function (i,v) {
    //     var gmtDateTime = moment.utc(v.innerText,'YYYY-MM-DD HH:mm:ss');
    //     var local = moment(gmtDateTime).local().format('MM/DD/YYYY hh:mm:ss A');
    //     v.innerText = local
    // });

    $('.padd').css('margin-top','5px').css('margin-bottom', '10px').css('margin-right', '15px');
    $('#datatable-keytable-all').DataTable({
        responsive: true,
        "sScrollX": false,
        "sscrollX": false,
        "order": [[ 5, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 6 },
        ],
        "bFilter": false,
        "bInfo": false,
        "bPaginate": false,
        "searching": false
    });

    $("#assigned_to").on('change', function () {
        $("#assign_form").submit();
    })
    $('#selecttag > option').each(function() {
        if($(this).text() == "no_flag"){
            $(this).text('').text("No Flag");
        }else if($(this).text() == "red"){
            $(this).text('').text("Tech Support");
        }else if($(this).text() == "blue"){
            $(this).text('').text("Merchant Services");
        }else{
        }
    });

    var array_of_docs= [];
    $('.file_field_jq').hide();
    $("#new_field").on('click',function () {
        $('#browse_field').click()
    });
    $('.file_field_jq').change(function () {
        if(this.value == ""){
            alert('Please select a file to upload')
        }else{
            var form = $('#image_upload');
            var file_size= parseFloat((this.files[0].size/1024)/1024).toFixed(2);
            var ext = this.value.split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg', 'docx', 'doc', 'pdf','csv','xlsx']) == -1) {
                alert("Only 'Png', 'Jpeg', 'Docx', 'Doc' ,'Pdf','csv','xlsx' allowed");
            }else{
                if (file_size <= 4){
                    $('.spnr').fadeIn();
                    form.ajaxSubmit({
                        dataType: 'json',
                        error: function (result) {
                        },
                        complete: function(data, XMLHttpRequest, textStatus, response , result) {
                            var image = JSON.parse(data.responseText);
                            array_of_docs.push(image.doc.id);
                            $('input[name="attachments[]"]').val(array_of_docs);
                            $('#files_section').append('<div class="row file_section_row remove_'+image.doc.id +'">' + '<div class="col-md-2"><i class="fa fa-check-circle fa-1x" style="color:#199b3e;padding-top: 15px"></i></div> ' +
                                '<div class="col-md-8 doc_view" >'+image.doc.add_image_file_name +' <p class="m-0"> '+parseFloat(image.doc.add_image_file_size/1000).toFixed(1)+' kb</p></div> '+'' +
                                '<div class="col-md-2"><a onclick="delete_item('+image.doc.id+')" data-remote="true" method="post"> <i class="fa fa-trash fa-1x" style="color:#9167a7;padding-top: 15px"></i> </a></div> ');
                            $('.spnr').fadeOut("slow");
                        }
                    });

                }else{
                    alert('Please select a file less than 4MB')
                }
            }
        }
    });
$(document).on('click','.tickets_id',function () {
    var tickets = $(this).val();
    $(''+tickets+'').removeAttr( "style" );
})
//.....................inde.js.erb................//
    const urlParams = new URLSearchParams(window.location.search);
    const tabParam = urlParams.get('tab');
        $(".all_tickets").click()
        $('#nav-home').removeClass("fade");
     if (tabParam !="" && tabParam=='flagged') {
         $(".flagged_tickets").click();
     }

    function delete_item(id) {
        var doc= id;
        $('.remove_'+id).remove();
        array_of_docs=  array_of_docs.filter(function (val) {
            return  val !== doc
        });
        $('input[name="attachments[]"]').val(array_of_docs);
        $.ajax({
            url: '/admins/images/cancel_upload_doc' ,
            data:{document_id: doc},
            dataType: 'JSON',
            method: 'get',
            success: function () {
                $('#message').html('').html("render :partial => 'layouts/flash', :locals=>{:key => 'info', :value => 'Successfuly deleted'}")
            },
            error: function () {

            }

        })
    }
    var globalTimeout = null;
    $('#search').on('keyup',function () {
        console.log($('#search').val())
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);
        }
        globalTimeout = setTimeout(function() {
            globalTimeout = null;
            var search_value = $('#search').val();
            const filterParam = urlParams.get('filter');
            const ticketParam = urlParams.get('ticket');
            var filter = filterParam

            var ticket = ticketParam
            $.ajax({
                url: "/admins/tickets",
                type: "GET",
                data: {q: search_value,ticket: ticket,filter: filter}
            })
        }, 1500);
    });

});




function hello(id) {
    flag = $('#a_tag_'+id).attr("data-flagvalue");
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        closeOnCancel: true
    }).then((isConfirm) => {
        var msg = $(this).is(':checked') == true ? 'enabled!' : 'disabled!';
        if (isConfirm.value === true) {
            $.ajax({
                url: "/admins/tickets/change_flag",
                type: 'get',
                data: { id: id, flag: flag},
                success: function(data) {
                    if(flag == "red"){
                        $('#flag_btn_'+id).css('color','red');
                        $('#a_tag_'+id).css('color','blue');
                        $('#a_tag_'+id).data("flagvalue","blue");
                    }else if(flag == "blue"){
                        $('#flag_btn_'+id).css('color','blue');
                        $('#a_tag_'+id).css('color','red');
                        $('#a_tag_'+id).data("flagvalue","red");
                    }
                    swal("Updated!", "Flag Changed Successfully ", "success");
                },
                error: function(error){
                    swal("Cancelled", "An unexpected error occured :)", "error");
                }
            });
        } else {
            return false;
        }
    });
};