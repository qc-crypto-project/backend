
$(document).ready(function () {

    //validation
    $('input, select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'right',
        theme: 'tooltipster-light'
    });

    $("form").submit(function (event) {
        var btn = $("#submit_btn");
        btn.prop('disabled', true);
        setTimeout(function () {
            btn.prop('disabled', false);
        }, 4 * 1000);
    });

    $("#form").validate({
        errorPlacement: function (error, element) {
            var lastError = $(element).data('lastError'),
                newError = $(error).text();

            $(element).data('lastError', newError);

            if (newError !== '' && newError !== lastError) {
                $(element).tooltipster('content', newError);
                $(element).tooltipster('show');
            }
        },
        success: function (label, element) {
            $(element).tooltipster('hide');
        }
    });


    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            $('input, select').tooltipster("hide");
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input"),
            isValid = true;

        for (var i = 0; i < curInputs.length; i++) {
            if (!$(curInputs[i]).valid()) {
                isValid = false;
            }
        }

        if (isValid) {
            nextStepWizard.removeClass('disabled').trigger('click');
        }
    });

    $('div.setup-panel div a.btn-primary').trigger('click');


$('.datepicker').datepicker({
    dateFormat: 'dd/mm/yy',
    startDate: new Date(),
    autoclose: true
});
$(document).on('keyup', ".con_account_number", function () {
    if($('.account_number').val() != $('.con_account_number').val()){
        $("#acc_error").text("").text("Account number must be same!");
    }else {
        $("#acc_error").text("");
    }
})
$(document).on('keyup', ".con_routing_number", function () {
    if($('.routing_number').val() != $('.con_routing_number').val()){
        $("#rou_error").text("").text("Rounting number must be same!");
    }else {
        $("#rou_error").text("");
    }
})
});