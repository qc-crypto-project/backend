//Setting company email address with requirement
// function setCompanyEmail( id) {
//     var company_name= document.getElementById("company_name").value;
//     company_name = company_name.replace(/\s+/g, '-').toLowerCase();
//     document.getElementById('company_company_email').value=company_name+"@quickcard.me"
//
// }
$('#alert-message-top').fadeOut(5000);
function validateCompanyForm(name) {
    $(function() {
       id= $('#'+ name).find('.company_hidden_field1').val();
        var formRules = {
            rules: {
                "company[name]": {
                    required: true,

                },
                "company[category]": {
                    required: true,

                },
                "company[company_email]": {
                    required: true,
                    email: true,
                    emailExt: true,
                    remote: {
                        url: "/admins/companies/form_validate?user_id=" +  id,
                        type: 'GET',
                        data: {}
                    }
                },
                "company[phone_number]": {
                    required: true,
                    number: true,
                    remote: {
                        url: "/admins/companies/form_validate?user_id=" +  id,
                        type: 'GET',
                        data: {}
                    }
                },
            },
            messages: {
                "company[company_email]": {
                    required: "Please enter Email",
                    remote: "This Email address already exist!"
                },
                "company[phone_number]": {
                    required: "Please enter Phone Number",
                    remote: "Invalid format or phone number is already taken!",
                    number: "Must be number"
                },
            },
            invalidHandler: function() {
                animate({
                    name: 'shake',
                    selector: '.auth-container > .card'
                });
            }
        };

        $.extend(formRules, config.validations);
        $('#' + name ).validate(formRules);
    });
};
jQuery.validator.addMethod("emailExt", function(value, element, param) {
    return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
},'Your E-mail is wrong');

$("#companyform").on("submit", function () {
    disableButton('locationform','submit_button')
});
function disableButton(form_Id, btn_Id) {
    console.log(form_Id);
    console.log(btn_Id);
    if ($("#"+form_Id).valid()){
        document.getElementById(btn_Id).disabled = true;
    }

}
validateCompanyForm('companyform');

$('.modalwalabutton').click(function(){
    $('.myModal').modal();
});


/*$('#datatable-keytable').DataTable({
    responsive: true,
    "sScrollX": false,
    "scrollX": false,
    "sSearch": true,
    "ordering": true,
    "processing": true,
    "searching": true,
    "serverSide": true,
    "ajax": "/admins/companies"

});*/

$('.p').click(function () {
    tabDetailds($(this).data("id"));
});

function tabDetailds(id) {
    $.ajax({
        url: "/admins/companies" + id,
        type: 'PUT',
        data: {id: id},
        success: function (data) {
            console.log(data);
            $("#balance_section").append(data);
            $("#modal-69").modal();
        },
        error: function (data) {
            console.log(data);
        }
    });
}
