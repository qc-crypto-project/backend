var ez_merchant = "ez_merchant_";
var merchant_location;
if ($("#key").val()==undefined)
{
    ez_merchant=ez_merchant+"1";
    merchant_location = '#ezLocationText'+"0";
}
else{
    merchant_location = '#ezLocationText'+$("#key").val();
    ez_merchant=ez_merchant+$("#key").val();
}
$(document).ready(function () {
    $("#location_phone_number").inputmask('Regex', {regex: "^\\d+\\\\d{0,2}$"});
    $(".split").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $('#alert-message-top').fadeOut(5000);
    // Location index data table js
    $(".padd").css("margin","15px 15px 20px 0px");
    var lt= $('.timeUtc-user').each(function (i,v) {
        // var gmtDateTime =  moment(v.innerText.replace('UTC', '')).utc().format('MM-DD-YYYY HH:mm:ss')
        var gmtDateTime = moment.utc(v.innerText,'MM-DD-YYYY HH:mm:ss');
//           var gmtDateTime = moment.utc(v.innerText).toDate();
        var local = moment(gmtDateTime).local().format('MM-DD-YYYY hh:mm:ss A');
        v.innerText = local
    });
    var table = $('#datatable-keytable').DataTable({
        responsive: true,
        "bPaginate": false,
        "eSearch": false,
        "searching": false,
        "bInfo": false,
        "ordering": true,
        "order": [[ 1, "desc" ]],
        "columnDefs": [
            { "type": "date", targets: 1 }
        ]
    });
    var table = $('#datatable-keytable_wall').DataTable({
        responsive: true,
        "sScrollX": false,
        "bPaginate": false,
    });
    /*$(document).on('click','#tx_filter2',function () {
        $('#location_wallet_transaction').submit();
    })*/

    $(document).on('change','#tx_filter',function () {
        $('#location_serach_form').submit();
    })
    /*$(document).on('click','#tx_filter',function () {
        $('#user_transaction_table').submit();
    })*/

    $(document).on('change','.location_filter',function () {
        $('#location_filter').submit();
    })
    // edit location js
    $(document).on('click','#location-btn',function () {
        //$("#cover-spin").fadeIn();
         error=''
         $('#error').html('')
       //  if($('#location_cs_number-error').text()!="" || $('#location_cs_number').val().length < 10 ) {
       //      error=error+" Customer service number"
       //  }
       //  if($('#phone_number_cor-error').text()!="" || $('#phone_number_cor').val().length < 10){
       //      error=error+" Contact phone number"
       //  }
        if($('#business_name').val()==''){
            if(error!=''){
                error = error+","
            }
            error = error+" DBA Name"
        }
        // if($('#phone_number').val()==''){
        //     if(error!=''){
        //         error=error+","
        //     }
        //
        //     error=error+" Phone Number"
        // }
        if($('#web_site').val()==''){
            if(error!=''){
                error=error+","
            }
            error=error+" Web Site"
        }
        // if($('#primary_bank').val()==0){
        //     if(error!=''){
        //         error=error+","
        //     }
        //
        //     error=error+" Primary Bank"
        // }
        if($('#isos').val()==''){
            if(error!=''){
                error=error+","
            }

            error=error+" Select iso"
        }
        var error_message = ""
        var count=0;
        $('.input-field').each(function(){
            count = count +1;
            var field_id = $(this).attr('id');
            field_id = field_id.split('_');
            field_id = field_id[1];
            var field_count = field_id-1;
            var hidden_field = $(this).next().attr('id');
            var error_field = $(this).next().next().attr('id');
            if ($("#baked_iso_switch").is(':checked')){
                // for (field_count; field_count > 0; field_count--){
                //     if (field_id % 3 == 0 && field_id != 3){
                //         if ($("#user_"+field_id).val()=='' && $("#user_"+(field_id-1)).val()=='' && $("#user_"+(field_id-2)).val()==''){
                //             error_message = "Select baked user";
                //             $("#user_"+field_id).next().next().text("Please Select the baked user");
                //             $("#user_"+field_id).next().next().css('color', 'red');
                //             $("#user_"+(field_id-1)).next().next().text("Please Select the baked user");
                //             $("#user_"+(field_id-1)).next().next().css('color', 'red');
                //             $("#user_"+(field_id-2)).next().next().text("Please Select the baked user");
                //             $("#user_"+(field_id-2)).next().next().css('color', 'red');
                //             if ($("#user_3").val() == '')
                //             {
                //                 error_message = 'Select baked user';
                //                 $("#user_3").next().next().text("Please Select the baked user");
                //                 $("#user_3").next().next().css('color', 'red');
                //             }
                //             if ($("#user_"+(field_id-3)).val() == '' && $("#user_"+(field_id-4)).val() == '')
                //             {
                //                 error_message = 'Select baked user';
                //                 $("#user_"+(field_id-3)).next().next().text("Please Select the baked user");
                //                 $("#user_"+(field_id-3)).next().next().css('color', 'red');
                //                 $("#user_"+(field_id-4)).next().next().text("Please Select the baked user");
                //                 $("#user_"+(field_id-4)).next().next().css('color', 'red');
                //             }
                //         }
                //         else{
                //             if ($('#user_'+field_id).val()!='')
                //             {
                //                 $("#user_"+field_id).next().next().text("");
                //             }
                //             if ($('#user_'+(field_id-1)).val()!='')
                //             {
                //                 $("#user_"+(field_id-1)).next().next().text("");
                //             }
                //             if ($('#user_'+(field_id-2)).val()!='' && $('#user_'+(field_id-1)).val()=='')
                //             {
                //                 $("#user_"+(field_id-2)).next().next().text("");
                //             }
                //         }
                //     }
                //     if($('#user_'+field_count).val() == "" && $("#user_"+field_id).val() != "" && $("#user_"+field_id).val() != undefined){
                //         error_message = "Select baked user";
                //         $("#user_"+field_count).next().next().text("Please Select the baked user");
                //         $("#user_"+field_count).next().next().css('color', 'red');
                //     }else{
                //         // $("#user_"+field_id).next().next().text("");
                //     }
                // }
            }
            if (field_id %3 == 0)
            {
                if ($("#user_"+field_id).val()!='' && $('#user_'+(field_id-1)).val()=='')
                {
                    error_message = "Select baked user";
                    $("#user_"+(field_id-1)).next().next().text("Please Select the baked user");
                    $("#user_"+(field_id-1)).next().next().css('color', 'red');
                }
                else
                {
                    $("#user_"+(field_id-1)).next().next().text("");
                }
            }
            if (field_id % 3 == 0 && field_id != 3)
            {
                if ($("#user_"+field_id).val()=='' && $("#user_"+(field_id-1)).val()=='' && $("#user_"+(field_id-2)).val()=='')
                {
                    $(this).parent().parent().parent().parent().remove();
                }
            }
        });
        $('.first-user').each(function(){
            var field_id = $(this).attr('id');
            var error_field = $(this).next().next().attr('id');
            if ($("#baked_iso_switch").is(':checked')){
                if($("#"+field_id).val() == ""){
                    error_message = "Select baked user";
                    $($("#"+error_field)).text("Please Select the baked user");
                    $($("#"+error_field)).css('color', 'red');
                }
            }
        });
        $(".input-field").each(function () {
            var id = $(this).attr("id");
            var percent = $("#" + id + "_percent").val();
            var dollar = $("#" + id + "_dollar").val();
            var baked_user = $(this).val();
            if ($("#baked_iso_switch").is(':checked')) {
            if (baked_user != "") {
                if (!value_present(percent) && !value_present(dollar) && dollar != undefined && dollar != undefined) {
                    if (percent == "" && dollar == "") {
                        error_message = "Must enter % or $";
                        $("#" + id + "_percent_error").html("Must enter % or $");
                    } else {
                        error_message = "Value should be greater than 0";
                        $("#" + id + "_percent_error").html("Value should be greater than 0");
                    }
                } else {
                    if ($(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
                        error_message = "Select baked user";
                        $("#" + id + "_error").text("Please Select the baked user");
                        $("#" + id + "_error").css('color', 'red');
                    } else {
                        $("#" + id + "_percent_error").html("")
                        // $("#" + $("#" + id).parent().prev().find('div').attr('id')).text("");
                    }
                }
            }
            else {
                $("#" + id + "_percent_error").html("");
                if ($(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
                    error_message = "Select baked user";
                    $("#" + id + "_error").text("Please Select the baked user");
                    $("#" + id + "_error").css('color', 'red');
                }
            }
        }
        });

        $(".dollar").each(function () {
            var id = $(this).attr("id");
            var index = id.match(/\d+/)[0];
            if(id == "super_company_percent_"+index){
                var percent = $("#"+id).val();
                var dollar = $("#super_company_dollar_"+index).val();
            } else if (id == "super_company_dollar_"+index) {
                var dollar = $("#"+id).val();
                var percent = $("#super_company_percent_"+index).val();
            }
            var baked_user = $("#companyInput_"+index).val();
            if ($("#profit_split_switch").is(':checked')) {
                if (baked_user != "") {
                    if (!value_present(percent) && !value_present(dollar) && dollar != undefined && dollar != undefined) {
                        if (percent == "" && dollar == "") {
                            error_message = "Must enter % or $";
                            $("#super_company_percent_error_"+index).html("Must enter % or $");
                        } else {
                            error_message = "Value should be greater than 0";
                            $("#super_company_percent_error_"+index).html("Value should be greater than 0");
                        }
                    } else {
                        if (baked_user == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
                            error_message = "Select profit user";
                            $("#companyInput_error_"+index).text("Please Select the profit user");
                            $("#companyInput_error_"+index).css('color', 'red');
                        } else {
                            $("#super_company_percent_error_"+index).html("")
                            $("#" + $("#" + id).parent().prev().find('div').attr('id')).text("");
                        }
                    }
                }
                else {
                    if ($(this).val() == "" && (dollar != 0 || percent != 0) && (dollar != undefined || percent != undefined)) {
                        error_message = "Select profit user";
                        $("#companyInput_error_"+index).text("Please Select the profit user");
                        $("#companyInput_error_"+index).css('color', 'red');
                    }
                }
            }
        })

        error = error + error_message;

        $('.user-1').each( function () {
            var field_id = $(this).attr('id');
            var hidden_field = $(this).next().attr('id');
            var error_field = $(this).next().next().attr('id');
            if($('#'+ field_id).val()=='' && $('#baked_iso_switch').is(':checked')){
                if(error!=''){
                    error=error+","
                }

                error=error+" Select baked user"
            }
        });

        if($('#companyInput-error').val()=='' || ($('#companyInput').val()=='' && $('#profit_split_switch').is(':checked'))){
            if(error!=''){
                error=error+","
            }

            error=error+" Select profit split company"
        }

        if($('#netCompanyInput-error').val()=='' || ($('#netCompanyInput').val()=='' && $('#net_profit_split_switch').is(':checked'))){
            if(error!=''){
                error=error+","
            }

            error=error+" Select net profit split company"
        }
        $('.ez_merchant_1').each( function () {
            var field_id_ez = $(this).attr('id');
            var hidden_field = $(this).next().attr('id');
            var error_field = $(this).next().next().attr('id');
            if ($('#'+ field_id_ez).val() == '' && $('#ez_merchant_switch').is(':checked')) {
                if (error != '') {
                    error = error + ","
                }

                error = error + " Select sub merchant "
            }
        })
        var apis_checked = $("#location_block_api").is(':checked')
        var apis_checked_message = ''
        var sales_checked = $("#location_disable_sales_transaction").is(':checked')
        var sales_checked_message = ''
        if (apis_checked && !$("#vt_api").is(':checked') && !$("#vtt_api").is(':checked') && !$("#vd_api").is(':checked')){
            apis_checked_message = 'Error message'
            error = apis_checked_message
        }else if(sales_checked && !$("#sales_id").is(':checked') && !$("#vt_id").is(':checked') && !$("#batch_id").is(':checked')){
            sales_checked_message = 'Error message'
            error = sales_checked_message
        }
        if(!$('#locationform').valid()){
            if(error!=''){
                $("#cover-spin").fadeOut();
                $("#error").fadeIn();
                $("#disable-api-check-error").html('');
                $("#disable-sales-check-error").html('');
                if (apis_checked_message != ''){
                    $("#disable-api-check-error").append('<span id="exp_error" class="p_error text-danger padding-0">Please select at least one option</span>');
                }else if (sales_checked_message != ''){
                    $("#disable-sales-check-error").append('<span id="exp_error" class="p_error text-danger padding-0">Please select at least one option</span>');
                } else{
                    $('#error').html('Please fill fields: ').append(error);
                }
                $("#error").fadeOut(30000);
                return false;

            }
            // return false
        } else{
            if(error!=''){
                $("#cover-spin").fadeOut();
                $("#error").fadeIn();
                $("#disable-api-check-error").html('');
                if (apis_checked_message != ''){
                    $("#disable-api-check-error").append('<span id="exp_error" class="p_error text-danger padding-0">Please select at least one option</span>');
                }else if (sales_checked_message != ''){
                    $("#disable-sales-check-error").append('<span id="exp_error" class="p_error text-danger padding-0">Please select at least one option</span>');
                } else{
                    $('#error').html('Please fill fields: ').append(error);
                }
                $("#error").fadeOut(30000);
                return false;
            }else{
                var dba_name = $("#business_name").val();
                var user_id = $('#user_id').val();
                var location_id = $('#location_id').val();
                var duplicate_loc = $('#duplicate_loc').val();
                var result = check_dba_name(dba_name, user_id, location_id, duplicate_loc);
                if(result){
                    $( "#location-btn" ).addClass('pointer-none')
                    return true;
                }else{
                    $('#error').html('DBA Name already exists');
                    $("#error").fadeIn();
                    $("#error").fadeOut(30000);
                    $('#DBA_error').html("").html('DBA Name already exists');
                    $("#DBA_error").fadeOut(30000);
                    $("#cover-spin").fadeOut();
                    return false;
                }
            }
        }
    });
    validateLocationForm('locationform');

    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                }
            });
        };
    }(jQuery));

    $(".percent").inputFilter(function(value) {
        return /^\d*(\.\d{0,2})?$/.test(value) && (value === "" || parseFloat(value) <= 100); });
    $(".whole_number").inputFilter(function(value) {
        return /^-?\d*$/.test(value); });

    $('.form-control.numeric.float.optional.buyrate_fields').on('change',function () {
        var id = $(this).attr('id');
        if ($(this).val() == 0) {
            $('#' + id + '_date').val('');
            $('#location-btn').attr("disabled", false);
        }
        else if($(this).val() != 0) {
            $('#'+id+'_date').val(moment(new Date).format("MM/DD/YYYY"));
        }
    });

    $('.buyrate_fields').bind('keyup change', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });

    $(document).on("change", "#image_store", function () {
        save_image();
    })

    $(document).on("click", "#delete_bank_img", function () {
        var location_id = $(this).data("location_id")
        delete_img(location_id)
    })

    var selected = $("#selected").val();
    $('#'+selected).attr('checked','checked');


    // ************ Wallet Transactions js ********************
    $(document).on('click',".bank_show-cover-spin", function () {
        $('#cover-spin').show(0)
    })
    $('#click_to_return').click();
    $('#click_to_return').click();
    $(document).on('click','.location_select',function () {
        $(this).select();
    });
    $(document).on('click','.split',function () {
        $(this).select();
    });
    $(document).on('click','.attempt_amount',function () {
        SelectAll('amount');
    });
    $(document).on('click','.location_cover_span',function () {
        $('#cover-spin').show(0)
    });
    $(document).on('click','.show_account',function () {
        showAccount()
    });
    $(document).on('click','.bank_account_function',function () {
        myFunctionTest()
    });
    $(document).on('keyup','.percent_function',function () {
        var id = $(this).data();
        percentage_cal('1_over','1_over_'+id)
    });
    $(document).on('click','.percent_function',function () {
        var id = $(this).data();
        SelectAll('1_over_'+id)
    });
    $(document).on('keyup','.percent_function2',function () {
        var id = $(this).data();
        percentage_cal('2_over','2_over_'+id)
    });
    $(document).on('click','.percent_function2',function () {
        var id = $(this).data();
        SelectAll('2_over_'+id)
    });
    $(document).on('keyup','.under_function',function () {
        var id = $(this).data();
        percentage_cal('1_under','1_under_'+id)
    });
    $(document).on('click','.under_function',function () {
        var id = $(this).data();
        SelectAll('1_under_'+id)
    });
    $(document).on('keyup','.under_function2',function () {
        var id = $(this).data();
        percentage_cal('2_under','2_under_'+id)
    });
    $(document).on('click','.under_function2',function () {
        var id = $(this).data();
        SelectAll('2_under_'+id)
    });
    $(document).on('keyup','.super_function1',function () {
        for_super_users("user_1")
    });
    $(document).on('keyup','.super_function2',function () {
        for_super_users("user_2")
    });
    $(document).on('keyup','.super_function3',function () {
        for_super_users("user_3")
    });
    $(document).on('keyup','.iso_function',function () {
        for_main_users("isos");
    });
    $(document).on('keyup','.input-field',function () {
        field_id=$(this).data("type");
        var all_keyword = $("#"+field_id).val();
        var id = '#'+$("#"+field_id).attr('id');
        var user = '#'+field_id;
        var user_hidden = '#'+field_id+'Hidden';
        if(all_keyword != ''){
            getUsers(all_keyword,id,user,user_hidden,field_id, "for_users")
        }
    });
    $(document).on('keyup','.agent_function',function () {
        for_main_users("agents");
    });
    $(document).on('keyup','.affiliate_function',function () {
        for_main_users("affiliates");
    });
    $(document).on('keypress','.fee_no_function',function () {
        var val= $(this).val();
        if(val == (event.charCode == 8 || event.charCode == 0 || event.charCode == 13)) {
            return val;
        }
        else if(val == (event.charCode >= 48 && event.charCode <= 57)){
            return val;
        }
        else{
            return null;
        }

        return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57
    });

    $(document).on('keyup','#companyInput_0',(function () {
        for_profit_users('companyInput_0', "0")
    }));

    $(document).on('change','#profit_split_switch',function(){
        if($(this).is(':checked')){
            if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked')){
                $("#location-btn").prop("disabled", true);
            }
            $('#gbox_company_radio').prop("checked", true);
            $('#profit_splits_on_section').show();
            $('#profit_check').show();
        } else{
            $('#profit_splits_on_section').hide();
            $('#profit_check').hide();
            $('#companyInput-error').remove();
        }
    });
    $(document).on('change','#baked_iso_switch',function(){
        if($(this).is(':checked')){
            if( !$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked')){
                $("#location-btn").prop("disabled", true);
            }
            $('#gbox_radio').prop("checked", true);
            $('#baked_section').show();
            $('#baked_check').show();
        } else{
            console.log("B")
            $('#baked_check').hide();
            $('#baked_section').hide();
            $('#user_1-error').remove();
            if ($("#location-btn").is(":disabled")){
                $("#location-btn").prop("disabled", true);
            }else{
                $("#location-btn").prop("disabled", false);
            }

        }
    });
    $(document).on('change','#ez_merchant_switch',function(){
        if($(this).is(':checked')){
            $('#ez_section').show();
        } else{
            $('#ez_section').hide();
            $('#ez_merchant_1-error').remove();
        }
    });

    $('input[name$="[name]"]').keyup(function(e){
        if(e.keyCode == 8 || e.keyCode == 46){
            $(this).next().val("");
        }
    });

    $( function() {
        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _create: function () {
                this._super();
                this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
            },
            _renderMenu: function (ul, items) {
                var that = this,
                    currentCategory = "";
                var exist_ids = [];
                $('input[name$="[wallet]"]').each(function(){
                    exist_ids.push(parseInt($(this).val()));
                });
                exist_ids = exist_ids.filter( value => !Number.isNaN(value) ); // we are getting the ids of user which are already selected in other fields
                items = $.grep(items, function(e){
                    //return e.id != 10;
                    return !exist_ids.includes(e.id); // here we are removing objects in items
                });
                $.each(items, function (index, item) {
                    var li;
                    if (item.category != currentCategory) {
                        ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                        currentCategory = item.category;
                    }
                    li = that._renderItemData(ul, item);
                    if (item.category) {
                        li.attr("aria-label", item.category + " : " + item.label);
                    }
                });
            }
        });
    });

    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term));
        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };
    var count=0;

    $('input#isos').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $('input#agents').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $('input#affiliates').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    $(document).on('click','#remove_baked_users',function(){ // remove baked user
        $(this).parent().parent().parent().parent().parent().parent().remove();
        var count =0;
        $('.input-field').each(function(){
            count++;
        });
        if (count == 3)
        {
            if ($("#user_"+count).val() == '' && $("#user_"+(count-1)).val() == '' && $("#user_"+(count-2)).val() != '')
            {
                $("#user_"+count).next().next().text("");
                $("#user_"+(count-1)).next().next().text("");
            }
            if ($("#user_"+count).val() == '' && $("#user_"+(count-1)).val() != '' && $("#user_"+(count-2)).val() != '')
            {
                $("#user_"+count).next().next().text("");
            }
        }
    });

    $(document).on('click','#remove_profit_users',function(){ // remove baked user
        $(this).parent().parent().parent().next().next().remove();
        $(this).parent().parent().parent().next().remove();
        $(this).parent().parent().parent().remove();

    });

    $(document).on('keyup', ".for_super_users", function () {
        var user_no = $(this).attr("id");
        for_super_users(user_no);
    })

    $(document).on('keyup', '.next-baked-users', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        for_super_users(e.target.id);
        ValidateFields(e,id,hidden_id)
    });

    $(document).on('keyup', '.next-profit-users', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        for_profit_users(e.target.id, e.target.id.match(/\d+/)[0]);
        ValidateFields(e,id,hidden_id)
    });

    $('#add_baked_users').click(function () {// add new baked user
        count = $('.input-field').last().attr('id');
        count = count.split('_');
        count = count[1];
        count = Number(count);
        $.ajax({
            url:"/admins/merchants/next_baked_section",
            type: "GET",
            data: {count: count}
        });
    });

    $('#add_profit_users').click(function () {// add new baked user
        count = $("#profit_splits_on_section .count_div").length;
        $.ajax({
            url:"/admins/locations/next_profit_section",
            type: "GET",
            data: {count: count}
        });
    });

    $('#add_ez_merchant').click(function () { // add sub merhcant user
        ez_count = $("#ez_section").children().length;
        $("#location-btn").prop("disabled", false);
        $.ajax({
            url:"/admins/locations/next_ez_merchant_section",
            type: "GET",
            dataType: 'script',
            data: {ez_count: ez_count}
        });
    });

    var user1Key = "#user_1";
    var user2Key = "#user_2";
    var user3Key = "#user_3";

    if ($('#user_key').val()!=undefined)
    {
        user1Key+=$('#user_key').val();
        user2Key+=$('#user_key').val();
        user3Key+=$('#user_key').val();
    }

    $('#isos').on('keyup', function (e){
        ValidateFields(e,'#isos','#isosHidden')
    });

    $('#agents').on('keyup', function (e){
        ValidateFields(e,'#agents','#agentsHidden');
    });

    $(' #affiliates').on('keyup', function (e){
        ValidateFields(e,'#affiliates','#affiliatesHidden');
    });

    $(user1Key).on('click blur paste keypress change', function (e) {
        ValidateFields(e,user1Key,user1Key+'Hidden')
    });

    $('.user-1').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        ValidateFields(e,id,hidden_id)
    });

    $(user2Key).on('click blur paste keypress change', function (e) {
        ValidateFields(e,user2Key,user2Key+'Hidden')
    });

    $('.user-2').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        ValidateFields(e,id,hidden_id)
    });

    $(user3Key).on('click blur paste keypress change', function (e) {
        ValidateFields(e,user3Key,user3Key+'Hidden')
    });

    $('.user-3').on('click blur paste keypress change', function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + e.target.nextElementSibling.id;
        ValidateFields(e,id,hidden_id)
    });
    $('.input-field').on('keyup', function (e) {
        var id = '#' + e.target.id;
        var class_id = id.slice(1)
        var value = $(this).val()
        if (value == "") {
            $(id + "_percent").removeClass(class_id + "_percent")
            $(id + "_dollar").removeClass(class_id + "_dollar")
            $(id + "_percent_error").text('');
        }
    });
    $(document).on('change','.admins_country_field',function () {
        if($(this).val() != ""){
            $.ajax({
                url: '/admins/merchants/country_data',
                data: {country: $(this).val(),owner_field: 'change' },
                method: 'get',
                success: function (response) {
                    var states = response["states"];
                    $("#location_state").empty();
                    if(Object.keys(states).length > 0 ){
                        $.each(states,function (index, value) {
                            $("#location_state").append('<option value="' + value+ '">&nbsp;' + index + '</option>');
                        });
                        $('.admins_states_field_loc').attr("required",true);
                        $('.admins_states_field_loc').attr("disabled",false);
                    }else{
                        $('.admins_states_field_loc').attr("required",false);
                        $('.admins_states_field_loc').attr("disabled",true);
                    }

                }
            });
        }
    });
    $(document).on('keyup', "#companyInput", function (e) {
        ValidateFields(e,'#companyInput','#companyInputHidden')
    })
    $('#netCompanyInput').on('keyup',function (e) {
        ValidateFields(e,'#netCompanyInput','#netCompanyInputHidden')
    })
    $('#'+ez_merchant).on('keyup',function (e) {
        ValidateFields(e,'#'+ez_merchant,'#'+ez_merchant+'Hidden')
    })

    // net profit split location error
    $("#netLocationText").on('change', function (e) {
        var id = "#" + $(this).attr('id');
        var netLocationError = $(id +'_error')
        if($(this).val() !== ""){
            removeError(netLocationError);
            ValidateFields(e,null,null,null);
        }
        if (($('#net_profit_split_switch').is(':checked'))){
            if($('#netCompanyInput').val() != "" && $('#netCompanyInputHidden').val()!="")
            {
                if ($(id).val() == "")
                {
                    addError({id: netLocationError, for: 'Location'});
                    $('#location-btn').attr("disabled", true);
                }
            }
        }
    });

    // profit split location error
    $(document).on('change', "#locationText", function (e) {
        var id = "#" + $(this).attr('id');
        var locationError = $(id +'_error')
        if($(this).val() !== ""){
            removeError(locationError);
            ValidateFields(e,null,null,null);
        }
        if (($('#profit_split_switch').is(':checked'))){
            if($('#companyInput').val() != "" && $('#companyInputHidden').val()!="")
            {
                if ($(id).val() == "")
                {
                    addError({id: locationError, for: "Location"});
                    $("#btnSave").prop("disabled", true);
                }
            }
        }
    });

    // sub merchant split location error
    $(merchant_location).on('change',function(e){
        ezLocationError=$(merchant_location+'_error')
        if ($(merchant_location).val()!="")
        {
            removeError(ezLocationError);
            ValidateFields(e,null,null,null);
        }
        if (($('#ez_merchant_switch').is(':checked'))){
            if($('#'+ez_merchant).val()!="" && $('#'+ez_merchant+'Hidden').val()!="")
            {
                if ($(merchant_location).val()=="")
                {
                    addError({id: ezLocationError, for: 'Location'})
                    $("#location-btn").prop("disabled", true);
                }
            }
        }
    });

    $(document).on("change", "#baked_iso_switch, #profit_split_switch, #net_profit_split_switch, #sub_merchant_split_switch", function (e) {
        ValidateFields(e,null,null,null)
    });
    $(document).on('keyup','#super_company_percent',function () {
        var zval = $('#super_company_percent').val()
        if (zval<=0 && zval!="")
        {
            $('#super_company_percent_error').text("Value should be greater than 0");
            $("#location-btn").prop("disabled", true);
        }
        else {
            $('#super_company_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="") {
                if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked') && $("#profit_split_switch").is(":checked")){
                    $("#location-btn").prop("disabled", true);
                }else if (!$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked') && $("#baked_iso_switch").is(":checked")){
                    $("#location-btn").prop("disabled", true);
                }else{
                    $("#location-btn").prop("disabled", false);
                }
            }
            else
            {
                $("#location-btn").prop("disabled", true);
            }
        }
    });
    $(document).on('keyup','#super_company_dollar',function () {
        var zval = $('#super_company_dollar').val()
        if (zval<=0 && zval!="")
        {
            $('#super_company_dollar_error').text("the value must be greater than 0");
            // $("#location-btn").prop("disabled", true);
        }
        else {
            $('#super_company_dollar_error').text('');
            if($('#super_company_dollar_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="") {
                if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked') && $("#profit_split_switch").is(":checked")){
                    $("#location-btn").prop("disabled", true);
                }else if (!$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked') && $("#baked_iso_switch").is(":checked")){
                    $("#location-btn").prop("disabled", true);
                }else{
                    $("#location-btn").prop("disabled", false);
                }
            }
            else
            {
                $("#location-btn").prop("disabled", true);
            }
        }
    });
    $(document).on('keyup','#user_1_percent',function () {
        var zval = $('#user_1_percent').val()
        if (zval!="" && zval<=0)
        {
            $('#user_1_percent_error').text("the value is greater than 0");
            $("#location-btn").prop("disabled", true);
        }
        else {
            $('#user_1_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="" && ($('#gbox_radio').is(':checked') || $('#iso_radio').is(':checked')) && ($('.error-msg').text()=="")) {
                $("#location-btn").prop("disabled", false);
            }
            else
            {
                $("#location-btn").prop("disabled", true);
            }
        }
    })
    $(document).on('keyup','#ez_percent',function () {
        var zval = $('#ez_percent').val()
        if (zval<=0 && zval!="")
        {
            $('#ez_percent_error').text("Value should be greater than 0");
            $("#location-btn").prop("disabled", true);
        }
        else {
            $('#ez_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()==""  && $('.error-msg').text()=="") {
                $("#location-btn").prop("disabled", false);
            }
            else
            {
                $("#location-btn").prop("disabled", true);
            }
        }
    });
    $(function () {
        $('input[type="file"]').on('change click', function (event) {
            var id = $(this).attr('id');
            $(`#${id}-error`).hide();
            var count = 0;
            // $(`.col-md-5.${id} .row .col-md-12`).remove();
            var files = event.target.files;
            var files_count = files.length;
            $.each(files, function (index, value) {
                var images = value;
                var image = value.name;
                count = count+1
                var reader = new FileReader();
                var div_id = id.slice(0,-2);
                if(image.length>14){
                    image = image.substring(0,11)+"..." +image.slice(-5)
                }
                if (files_count===1){
                    $('.'+id).prepend("<div class='row' id="+div_id+"><div class='col-md-12'><div class='col-sm-8 selected-file top-padding '>" +image+"</div></div></div>");
                }
                else{
                    // <a class='fa fa-download fa-2x panel-heading margin-top'></a><a class='fa fa-close fa-2x panel-heading margin-top'></a>
                    $('.'+id).prepend("<div class='row' id="+div_id+"><div class='col-md-12'><div class='col-sm-8 selected-file top-padding'>" +image+"</div></div></div>");
                }
            });
            if (files_count === 0) {
                $(`.col-md-8.${id} .row .col-md-12 img`).remove();
            }
        });
    });
    $(document).on('click','.remove_image',function () {
        var id= $(this).attr('id');
        $('.'+id).remove()
    })
    $("a.fa.fa-close.fa-2x.panel-heading").click(function (e) {
        var image_id = $(this).attr('id');
        e.preventDefault();
        if (confirm('Are you sure to Delete?')) {
            $.ajax({
                url: "/admins/locations/document_delete",
                type: 'GET',
                data: {image_id: image_id},
                success: function (data) {
                        $('img.' + image_id).hide();
                        $('span.' + image_id).hide();
                        $('.' + image_id).hide();
                        $('#cover-spin').fadeOut();

                },
                error: function (data) {
                    $('#cover-spin').fadeOut();
                }
            });
        }
        else {
            $('#cover-spin').fadeOut();
        }
    });
    $(document).on('keyup','#net_super_company_percent',function () {
        var zval = $('#net_super_company_percent').val()
        if (zval<=0 && zval!="")
        {
            $('#net_super_company_percent_error').text("Value should be greater than 0");
            $("#location-btn").prop("disabled", true);
        }
        else {
            $('#net_super_company_percent_error').text('');
            if($('#super_company_percent_error').text()=="" && $('#net_super_company_percent_error').text()=="" && $('#ez_percent_error').text()=="" && $('#user_1_percent_error').text()=="" ) {
                $("#location-btn").prop("disabled", false);
            }
            else
            {
                $("#location-btn").prop("disabled", true);
            }
        }
    });

    $("#attach_radio").on('click', function () {
        if($(this).is(":checked")){
            $(this).val('true');
        }
        else if($(this).is(":not(:checked)")){
            $(this).val('false');
        }
    });

    $("#ez_percent,#ez_dollar").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});
    $("#bank-routing1,#password-field1").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});

    $(document).on("keyup", "#ez_merchant_1", function () {
        for_ez_merchant("ez_merchant_1","0")
    })
    $(document).on("keyup", ".ez_merchant_function", function () {
        for_ez_merchant("ez_merchant_0","0")
    })
    $(document).on("keyup", ".ez_merchant_remove", function (e) {
        var id = '#' + e.target.id;
        var hidden_id = '#' + $(id).next('.edit-ez-hidden').attr('id');
        ValidateFields(e, id, hidden_id)
    })

    $(document).on('change','#net_profit_split_switch',function(){
        if($(this).is(':checked')){
            $("#net_gbox_company_radio").prop("checked", true);
            $('#net_profit_splits_on_section').show();
            $('#net_check').show();
        }else{
            $("#net_gbox_company_radio").prop("checked", false);
            $('#net_profit_splits_on_section').hide();
            $('#net_check').hide();
            $('#netCompanyInput-error').remove();
            $('#spanError').remove();
        }
    });
    $(document).on('click','.baked_switch_checked', function () {
        OnChangeRadioBaked(this);
    })
    $(document).on('click','.profit_split_gbox_checked', function () {
        OnChangeRadioProfit (this)
    })
    $('input[type="file"]').on('change click',function(){
        var files = $(this)[0].files;
        var id = $(this).attr('id');
        var div_id = id.slice(0,-2);
        $('div.row #'+div_id ).remove();
        if(files.length>0) {
            if(files.length==1){
                $(this).addClass('image_have');
        }else{
            $(this).addClass('image_have');
        }}
    });
    $(document).on('keyup','#netCompanyInput',(function () {
        var all_keyword = $(this).val();
        var id = '#'+$(this).attr('id');
        var user = '#netCompanyInput';
        var user_hidden = '#netCompanyInputHidden';
        var merchant_div = '#netLocationDiv';
        var merchant_location = '#netLocationText';
        var span_error = '#spanError';
        var split = '#splitInput';
        var close_btn = '#closeBtn';
        if(all_keyword != ''){
            getAllNetUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location)
        }
    }));

    $(".split").inputmask('Regex', {regex: "^[0-9]{1,99}(\\.\\d{1,2})?$"});

    $(".input-field").autocomplete({
        disabled: true
    });


    $("#phone_number_cor").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#phone_code_cor').val(Object.values(selectedCountryData)[2]);
            $('#phone_number_cor').val('');
            space_remove(Object.values(selectedCountryData)[2], 'phone_number_cor');
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });
    $("#location_cs_number").intlTelInput({
        formatOnInit: true,
        separateDialCode: true,
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            country=selectedCountryPlaceholder;
            country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
            $('#cs_phone_code').val(Object.values(selectedCountryData)[2]);
            $('#location_cs_number').val('');
            space_remove(Object.values(selectedCountryData)[2], "location_cs_number");
            if(selectedCountryData.iso2 == 'us')
            {
                country = "555-555-5555"
            }
            return  country;
        },
        excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
        utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
    });


    $("#location_cs_number").inputmask('Regex', {regex: "^\\d+\\\\d{0,2}$"});
    $("#phone_number_cor").inputmask('Regex', {regex: "^\\d+\\\\d{0,2}$"});

    $('.phone_number').each(function () {
        var id = $(this).attr('id');
        var phone_code_id = $(this).next().attr("id");
        $("#"+id).intlTelInput({
            formatOnInit: true,
            separateDialCode: true,
            formatOnDisplay: false,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                country=selectedCountryPlaceholder;
                country=country.replace(/[^0-9\. ]/g,'').replace(/\W/gi,'');
                $("#"+phone_code_id).val(Object.values(selectedCountryData)[2]);
                space_remove(Object.values(selectedCountryData)[2], id);
                if(selectedCountryData.iso2 == 'us')
                {
                    country = "555-555-5555"
                }
                return  country;
            },
            excludeCountries: ['ba','cf','cd','cg','do','gq','pf', 'gw','mk','mg','nc','kp','mf','pm','vc','st','ae'],
            utilsScript: "<%= asset_path 'libphonenumber/utils.js' %>"
        });
    });


    function space_remove(code_number, id) {
        if(code_number.length==1) {
            $('#'+id).addClass("phone-length-1");
        } else if(code_number.length==2) {
            $('#'+id).addClass('phone-length-2');
        } else if(code_number.length==3) {
            $('#'+id).addClass('phone-length-3');
        } else {
            $('#'+id).addClass('phone-length-4');
        }
    }

    if( $('#ach_international_hidden').val() == "true" ){

        $("#ach_international").prop("checked",true);
    }else {
        $("#ach_international").prop("checked",false);
    }
});

function validateLocationForm(loc) {
    $(function() {
        var formRules = {
            rules: {
                // "location[business_name]": {
                //     required: true,
                //     remote: {
                //         url: "/admins/locations/check_location_name?user="+$('#user_id').val()+"&location_id="+$('#location_id').val()+"&duplicate="+$('#duplicate_loc').val(),
                //         method: 'GET',
                //         data: {},
                //         success: function (data) {
                //             debugger;
                //             if(data){
                //                 $('#DBA_error').html("");
                //                 $("#locationform")[0].submit();
                //             }else{
                //                 $('#DBA_error').html("").html('DBA Name already exists');
                //                 $("#DBA_error").fadeOut(30000);
                //                 $("#cover-spin").fadeOut();
                //             }
                //         }
                //     }
                // },
                "location[cs_number]": {
                    required: true
                },
                "location[phone_number]": {
                    required: true
                },
            },
            messages: {
                // "location[business_name]": {
                //     remote: "A location with same name already exists under this merchant, Please enter a new name",
                // }
            },
            invalidHandler: function() {
                animate({
                    name: 'shake',
                    selector: '.auth-container > .card'
                });
            }
        };

        $.extend(formRules, config.validations);
        $('#' + loc ).validate(formRules);
    });
};

function save_image(id){
    var file = $('#image_store')[0];
    if (file.value == "") {
        alert('Please select a file to upload')
        $("#test_submit").attr("disabled", true);
    } else {
//            $("#test_submit").attr("disabled", false);
        $('#image_upload').val(id);
        var form = $('#image_form_class2');
        var file_size = parseFloat((file.files[0].size / 1024) / 1024).toFixed(2);
        var ext = file.value.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert("Only 'Png', 'Jpeg', 'JPG' allowed");
        } else {
            if (file_size <= 4) {
            } else {
                alert('Please select a file less than 4MB')
            }
        }
    }
}

function for_profit_users(field_id, key){
    // var all_keyword = $(this).val();
    // var id = '#'+$(this).attr('id');
    // var user = '#companyInput';
    // var user_hidden = '#companyInputHidden';
    // var merchant_div = '#locationDiv';
    // var merchant_location = '#locationText';
    // var span_error = '#spanError';
    // var split = '#splitInput';
    // var close_btn = '#closeBtn';
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    var merchant_div = '#locationDiv'+key;
    var merchant_location = '#locationText'+key;
    var role = '#companyrole_'+key+'Hidden';
    var span_error = '#spanError'+key;
    var split = '#splitInput';
    var close_btn = '#closeBtn';
    var dollar = '#super_company_dollar_'+key;
    var percent = '#super_company_percent_'+key;
    if(all_keyword != ''){
        getAllUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location, role, key, dollar, percent)
    }
}

function getAllUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location, role, key, dollar, percent){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }
                    else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            // Mantain Response In Categories
            if(ui.item.category=="Merchants"){
                $(user_hidden).removeAttr('name');
                $(user_hidden).val("merchant");
                getMerchantLocations(ui.item.id,merchant_div,split,close_btn,user,span_error,merchant_location, role, key, dollar, percent);
            }
            else if(ui.item.category=="Companies"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][profit_split]["+key+"][wallet]");
                $(split).attr('name',"profit_splits[splits][profit_split]["+key+"][amount]");
                $(close_btn).data('category',"Company");
                $(user_hidden).val(ui.item.id);
                $(role).val(ui.item.category);
                $(user).attr('name',"profit_splits[splits][profit_split]["+key+"][name]");
                $(percent).attr('name', "profit_splits[splits][profit_split]["+key+"][percent]");
                $(dollar).attr('name', "profit_splits[splits][profit_split]["+key+"][dollar]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
            }
            else if(ui.item.category=="Isos"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][profit_split]["+key+"][wallet]");
                $(split).attr('name',"profit_splits[splits][profit_split]["+key+"][amount]");
                $(close_btn).data('category',"Iso");
                $(user_hidden).val(ui.item.id);
                $(role).val(ui.item.category);
                $(user).attr('name',"profit_splits[splits][profit_split]["+key+"][name]");
                $(percent).attr('name', "profit_splits[splits][profit_split]["+key+"][percent]");
                $(dollar).attr('name', "profit_splits[splits][profit_split]["+key+"][dollar]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][profit_split][from]");

            }
            else if(ui.item.category=="Agents"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][profit_split]["+key+"][wallet]");
                $(role).attr('name',"profit_splits[splits][profit_split]["+key+"][role]");
                $(split).attr('name',"profit_splits[splits][profit_split]["+key+"][amount]");
                $(close_btn).data('category',"Agent");
                $(user_hidden).val(ui.item.id);
                $(role).val(ui.item.category);
                $(user).attr('name',"profit_splits[splits][profit_split]["+key+"][name]");
                $(percent).attr('name', "profit_splits[splits][profit_split]["+key+"][percent]");
                $(dollar).attr('name', "profit_splits[splits][profit_split]["+key+"][dollar]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
            }else if(ui.item.category=="Affiliates"){
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(user_hidden).attr('name',"profit_splits[splits][profit_split]["+key+"][wallet]");
                $(split).attr('name',"profit_splits[splits][profit_split]["+key+"][amount]");
                $(close_btn).data('category',"Agent");
                $(user_hidden).val(ui.item.id);
                $(role).val(ui.item.category);
                $(user).attr('name',"profit_splits[splits][profit_split]["+key+"][name]");
                $(percent).attr('name', "profit_splits[splits][profit_split]["+key+"][percent]");
                $(dollar).attr('name', "profit_splits[splits][profit_split]["+key+"][dollar]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
            }
            ValidateFields(e,user,user_hidden)
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function getMerchantLocations(merchant_id,merchant_div,split,close_btn,user,span_error,merchant_location, role ,key, dollar, percent){
    $.ajax({
        url: "/admins/buy_rate/getMerchantLocations",
        type: "GET",
        data: {merchant_id: merchant_id},
        success: function (data) {
            if (data.locations!=""){
                $(merchant_location).empty();
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $(merchant_location).append(new_option);
                data.locations.forEach(function(location) {
                    var new_option = new Option(location.name, location.id);
                    $(new_option).html(location.name);
                    $(merchant_location).append(new_option);
                });
                $(merchant_location).attr('name',"profit_splits[splits][profit_split]["+key+"][wallet]");
                $(split).attr('name',"profit_splits[splits][profit_split]["+key+"][amount]");
                $(close_btn).data('category',"Merchant");
                $(merchant_div).css('visibility','visible');
                $(role).val("Merchants");
                $(user).attr('name',"profit_splits[splits][profit_split]["+key+"][name]");
                $(percent).attr('name', "profit_splits[splits][profit_split]["+key+"][percent]");
                $(dollar).attr('name', "profit_splits[splits][profit_split]["+key+"][dollar]");
                $('#iso_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
                $('#gbox_company_radio').attr('name', "profit_splits[splits][profit_split][from]");
                // if($('#iso_company_radio').is(':checked')) { $('#iso_company_radio').attr('name', "profit_splits[splits][merchant"+ui.item.id+"][from]"); }
                // if($('#gbox_company_radio').is(':checked')) { $('#gbox_company_radio').attr('name', "profit_splits[splits][merchant"+ui.item.id+"][from]"); }
//                    $(user).attr('readonly',true);
                $(merchant_location).css('visibility', 'visible');
                $("#companyInput_error").css('visibility', 'visible');
            }else{
                $(merchant_location).empty();
                $(merchant_div).css('visibility','hidden');
                $(merchant_location).removeAttr('name');
                $(span_error).text('No Location Exists Select Any Other Merchant');
                $(span_error).fadeIn();
                setTimeout(function(){
                    $(span_error).fadeOut();
                }, 2000);
            }
        },
        error: function (data) {
            $(merchant_location).empty();
            $(merchant_div).css('visibility','hidden');
            $(merchant_location).removeAttr('name');
            $(span_error).text('No Location Exists Select Any Other Merchant')
            $(span_error).fadeIn();
            setTimeout(function(){
                $(span_error).fadeOut();
            }, 2000);
        }
    });
}

function for_super_users(field_id) {
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    all_keyword = all_keyword.split('-');
    var length = all_keyword.length;
    all_keyword = all_keyword[length-1];
    all_keyword = all_keyword.trim();
    if(all_keyword != ''){
        getUsers(all_keyword,id,user,user_hidden,field_id, "for_users")
    }else{
        $(user + "_percent").attr('required', false);
    }
}

function getUsers(all_keyword,id,user,user_hidden,field_id, type){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword,users: type},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }
                    else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            $(user_hidden).val(ui.item.id);
            $(user).val(ui.item.value);
            var end_id = user.slice(1)
            $(user + "_percent").addClass(end_id + "_percent");
            $(user + "_dollar").addClass(end_id + "_dollar");
            ValidateFields(e,user,user_hidden)
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function for_main_users(field_id) {
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    all_keyword = all_keyword.split('-');
    var length = all_keyword.length;
    all_keyword = all_keyword[length-1];
    all_keyword = all_keyword.trim();
    if (all_keyword.length < 1 ){
        $(user_hidden).val('');
    }
    console.log("3")
    console.log(all_keyword.length);
    if(all_keyword != ''){
        getMainUsers(all_keyword,id,user,user_hidden,field_id)
    }
}

function getMainUsers(all_keyword,id,user,user_hidden,field_id){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword,users: field_id},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }
                    else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            $(user_hidden).val(ui.item.id);
            $(user).val(ui.item.value);
            ValidateFields(e,user,user_hidden);
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function ValidateFields(e,id,id_hidden){
    if (e.keyCode!=18 && e.keyCode!=38 && e.keyCode!=40 && e.keyCode!= 13 && e.keyCode!=9 && e.keyCode!=16 && e.keyCode!=17 && e.keyCode!=37 && e.keyCode!=20 && e.keyCode!=39 && e.keyCode!=36 && e.keyCode!=35 && e.keyCode!=27 && e.keyCode!=undefined) {
        //keyCode are 18 = alt -- 38 = arrowUp --arrowDown = 40 -- Enter = 13 -- tab = 9 -- Shift = 16 -- CTRL == 17 -- leftArrow = 37 -- rightArrow = 39 -- capsLock = 20  -- home = 36 -- end = 35 --esc =27
        $(id_hidden).val("");
        if (id.toLowerCase().indexOf("ez") >= 0)
            $(merchant_location).val(null);
    }
    var checkError=checkErrorAndDisplay(id, id_hidden);
    if (checkError === true)
    {
        $('#location-btn').attr("disabled", "disabled");
    }
    else
    {
        $('#location-btn').attr("disabled", false);
    }
    if (!($(id).val()!="" && $(id_hidden).val()==""))
        removeError($(id+'_error'));
}
function OnChangeRadioProfit (radio) {
    if ($("#baked_iso_switch").is(":checked")){
        if( !$('#gbox_radio').is(':checked') && !$('#iso_radio').is(':checked')){
            $("#location-btn").prop("disabled", true);
        }else{
            $("#location-btn").prop("disabled", false);
        }
    }else{
        $("#location-btn").prop("disabled", false);
    }
}

function OnChangeRadioBaked (radio) {
    if ($("#profit_split_switch").is(":checked")){
        if( !$('#gbox_company_radio').is(':checked') && !$('#iso_company_radio').is(':checked')){
            $("#location-btn").prop("disabled", true);
        }else{
            $("#btnSave").prop("disabled", false);
        }
    }else{
        $("#btnSave").prop("disabled", false);
    }
}
function checkErrorAndDisplay(id, hidden_id){
    var error_div=check_hidden_value(id, hidden_id);
    console.log("3")
    console.log(error_div.length);
    if (error_div.length>0)
    {
        for (i = 0; i < error_div.length; i++) {
            addError(error_div[i]);
        }
        return true;
    }
    else
        return false;
}

function addError(error_div) {
    $(error_div["id"]).html("Please use an existing "+error_div["for"]);
    $(error_div["id"]).addClass('error-msg');
}

function removeError(error_div) {
    error_div.removeClass("error-msg");
    error_div.empty()
}

function check_hidden_value(id, hidden_id){
    $('.input-field').on('paste', function(){
       var hidden_field = $(this).next().attr('id');
       $('#' + hidden_field).val('');
    });
    var error_ids = [];
    var feed = {};
    if ($('#isos').val()!="" && $("#isosHidden").val()==""){
        feed = {id: "#isos_error", for:"ISO"};
        error_ids.push(feed);
    }
    if ($('#agents').val()!="" && $("#agentsHidden").val()==""){
        feed = {id: '#agents_error', for:"Agent"};
        error_ids.push(feed);
    }
    if ($('#affiliates').val()!="" && $("#affiliatesHidden").val()==""){
        feed = {id: '#affiliates_error', for:"Affiliate"};
        error_ids.push(feed);
    }
    if ($('#baked_iso_switch').is(':checked')){
        $('input.input-field').each(function () {
            var field_id = $(this).attr('id');
            var hidden_field_id = $("#" + field_id).next().attr('id');
            if ($("#" + field_id).val() !== "" && $("#" + hidden_field_id).val() === ""){
                feed = {id: '#' + $('#' + hidden_field_id).next().attr('id'), for:"User"};
                error_ids.push(feed);
            }
        });
    }
    if ($('#profit_split_switch').is(':checked'))
    {
        if ($('#companyInput').val()!="" && $("#companyInputHidden").val()==""){
            feed = {id: '#companyInput_error', for:"User"};
            error_ids.push(feed);
        }
        else if($('#companyInput').val()!="" && $("#companyInputHidden").val() === "merchant"){
            if ($("#locationText").val() ==="" || $("#locationText").val()==null){
                feed = {id: "#locationText_error", for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    if ($('#net_profit_split_switch').is(':checked'))
    {
        if ($('#netCompanyInput').val()!="" && $("#netCompanyInputHidden").val()==""){
            feed = {id: '#netCompanyInput_error', for:"User"};
            error_ids.push(feed);
        }
        else if($('#netCompanyInput').val()!="" && $("#netCompanyInputHidden").val()  === "merchant"){
            if ($("#netLocationText").val() ==="" || $("#netLocationText").val()==null){
                feed = {id: "#netLocationText_error", for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    if (($('#ez_merchant_switch').is(':checked')))
    {
        if ($('#'+ez_merchant).val()!="" && $('#'+ez_merchant+'Hidden').val()==""){
            feed = {id: '#'+ez_merchant+'_error', for:"User"};
            error_ids.push(feed);
        }
        else if($('#'+ez_merchant).val()!="" && $('#'+ez_merchant+'Hidden').val()!="")
        {
            if ($(merchant_location).val()=="" || $(merchant_location).val()==null){
                feed = {id: merchant_location+'_error', for:"Location"};
                error_ids.push(feed);
            }
        }
    }
    return error_ids;
}

function for_ez_merchant(field_id, key){
    var all_keyword = $("#"+field_id).val();
    var id = '#'+$("#"+field_id).attr('id');
    var user = '#'+field_id;
    var user_hidden = '#'+field_id+'Hidden';
    var merchant_div = '#ezLocationDiv'+key;
    var merchant_location = '#ezLocationText'+key;
    var span_error = '#ezspanError'+key;
    var split = '#splitInput';
    var close_btn = '#closeBtn';
    if(all_keyword != ''){
        // getUsers(all_keyword,id,user,user_hidden,field_id, "ez_merchant")
        getAllEzUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location,key)
    }
}

function getAllEzUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location,key){
    var location_id = $("#ez_location_id").val();
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword, users: "ez_merchant", location: location_id},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            // Mantain Response In Categories
            if(ui.item.category=="Merchants"){
                $(user_hidden).removeAttr('name');
                $(user_hidden).val(ui.item.id);
                getEzMerchantLocations(ui.item.id,merchant_div,split,close_btn,user,span_error,merchant_location,key);
            }
            ValidateFields(e,user,user_hidden);
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function getEzMerchantLocations(merchant_id,merchant_div,split,close_btn,user,span_error,merchant_location,key){
    $.ajax({
        url: "/admins/buy_rate/getMerchantLocations",
        type: "GET",
        data: {merchant_id: merchant_id},
        success: function (data) {
            if (data.locations!=""){
                if ($(merchant_location+'_error').hasClass('error-msg')){
                    $("#location-btn").prop("disabled", true);
                } else{
                    $("#location-btn").prop("disabled", false);
                }
                $(merchant_location).empty();
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $(merchant_location).append(new_option);
                data.locations.forEach(function(location) {
                    var new_option = new Option(location.name, location.id);
                    $(new_option).html(location.name);
                    $(merchant_location).append(new_option);
                });
                $(merchant_location).attr('name',"profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][wallet]");
                $(split).attr('name',"profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][amount]");
                $(merchant_div).css('display','block');
                $(user).attr('name',"profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][name]");
                $("#attach_radio").attr('name', "profit_splits[splits][ez_merchant]["+ key +"][ez_merchant_1][attach]");
            }else{
                ezmerchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
            }
        },
        error: function (data) {
            ezmerchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
        }
    });
}

function ezmerchantsLocationNotAvailable(merchant_location,merchant_div,span_error) {
    // $(merchant_location).empty();
    $(merchant_div).css('visibility','hidden');
    // $(merchant_location).removeAttr('name');
    $(span_error).css('visibility','visible');
    $(span_error).text('No Location Exists Select Any Other Merchant');
    $(span_error).fadeIn();
    $("#location-btn").prop("disabled", true);
    setTimeout(function(){
        $(span_error).fadeOut("slow");
    }, 4000);
}

function getAllNetUsers(all_keyword,id,user,user_hidden,merchant_div,split,close_btn,span_error,merchant_location){
    $(id).catcomplete({
        minLength: 1,
        source: function (request, response) {
            $.ajax({
                url: "/admins/buy_rate/getUsers",
                type: "GET",
                data: {query: all_keyword},
                success: function (data) {
                    if (data.users!=""){
                        response(data.users);
                    }else{
                        response('')
                    }
                },
                error: function (data) {
                    response('')
                }
            });
        },
        select: function(e, ui) {
            // Mantain Response In Categories
            if(ui.item.category=="Merchants"){
                $(user_hidden).removeAttr('name');
                $(user_hidden).val("merchant");
                getNetMerchantLocations(ui.item.id,merchant_div,split,close_btn,user,span_error,merchant_location);
            }
            else {
                $(merchant_div).css('visibility', 'hidden');
                $(merchant_location).css('visibility', 'hidden');
                $("#netLocationText_error").css('visibility', 'hidden');
                $(merchant_location).removeAttr('name');
                if (ui.item.category == "Companies") {
                    setting_some_fields("company" + ui.item.id,ui.item.id,user_hidden,split,user);
                }else if (ui.item.category == "Isos") {
                    setting_some_fields("iso" + ui.item.id,ui.item.id,user_hidden,split,user);
                } else if (ui.item.category == "Agents") {
                    setting_some_fields("agent" + ui.item.id,ui.item.id,user_hidden,split,user);
                } else if (ui.item.category == "Affiliates") {
                    setting_some_fields("affiliate" + ui.item.id,ui.item.id,user_hidden,split,user);
                }
            }
            ValidateFields(e,user,user_hidden);
        }
    })
        ._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item);
    };
}

function getNetMerchantLocations(merchant_id,merchant_div,split,close_btn,user,span_error,merchant_location){
    $.ajax({
        url: "/admins/buy_rate/getMerchantLocations",
        type: "GET",
        data: {merchant_id: merchant_id},
        success: function (data) {
            if (data.locations!=""){
                $(merchant_location).empty();
                var new_option = new Option("Choose Location", '');
                $(new_option).html('Choose Location');
                $(merchant_location).append(new_option);
                data.locations.forEach(function(location) {
                    var new_option = new Option(location.name, location.id);
                    $(new_option).html(location.name);
                    $(merchant_location).append(new_option);
                });
                $(merchant_location).attr('name',"profit_splits[splits][net][merchant"+merchant_id+"][wallet]");
                $(split).attr('name',"profit_splits[splits][net][merchant"+merchant_id+"][amount]");
                $(merchant_div).css('visibility','visible');
                $(user).attr('name',"profit_splits[splits][net][merchant"+merchant_id+"][name]");
                $("#net_super_company_percent").attr('name', "profit_splits[splits][net][merchant"+merchant_id+"][percent]");
                $('#net_gbox_company_radio').attr('name', "profit_splits[splits][net][merchant"+merchant_id+"][from]");
                $(merchant_location).css('visibility', 'visible');
                $("#netLocationText_error").css('visibility', 'visible');
            }else{
                merchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
            }
        },
        error: function (data) {
            merchantsLocationNotAvailable(merchant_location,merchant_div,span_error)
        }
    });
}

function merchantsLocationNotAvailable(merchant_location,merchant_div,span_error) {
    $(merchant_location).empty();
    $(merchant_div).css('visibility','hidden');
    $(merchant_location).removeAttr('name');
    $(span_error).css('visibility','visible');
    $(span_error).text('No Location Exists Select Any Other Merchant');
    $(span_error).fadeIn();
    setTimeout(function(){
        $(span_error).fadeOut("slow");
    }, 4000);
}

function setting_some_fields(role,id,user_hidden,split,user) {
    $(user_hidden).attr('name', "profit_splits[splits][net][" + role + "][wallet]");
    $(split).attr('name', "profit_splits[splits][net][" + role + "][amount]");
    $(user_hidden).val(id);
    $(user).attr('name', "profit_splits[splits][net][" + role + "][name]");
    $("#net_super_company_percent").attr('name', "profit_splits[splits][net][" + role + "][percent]");
    $('.net_gbox').attr('name', "profit_splits[splits][net][" + role + "][from]");
}

function check_dba_name(dba_name, user_id, location_id, duplicate_loc) {
    var result = null
    $.ajax({
        url: "/admins/locations/check_location_name?user=" + user_id + "&location_id=" + location_id + "&duplicate=" + duplicate_loc,
        method: 'GET',
        data: {location: {business_name: dba_name}},
        async: false
    }).done(function(data){
        result = data
    });
    return result
}

$(document).on('click', "#search_btn", function () {
    $("#offset_new").val(moment().local().format('Z'));
});
/*------------------ Location Phone number fields Validations --------------------*/

$(document).on('click blur paste keyup change', '.phone_number', function (e) {
    var phone = $(this).val();
    var heading = $(this).parent().prev();
    var phone_code = $(this).closest('div').find('.selected-dial-code').text();
    var reg = new RegExp('^[0-9]+$');
    phone_code = phone_code.substring(1);
    var phone_number = phone_code + phone;
    var phone_error_div = $(this).parent().nextAll('.phone-error').attr('id');
    var phone_id = $(this).attr("id");
    var submit_btn_id = "#"+$(this).closest('form').find(':submit').attr("id");
    if(phone.length > 0){
        if(reg.test(phone_number)){
            $("#"+phone_error_div).text('');
            $(submit_btn_id).prop( 'disabled', false);
            $.ajax({
                url: "/admins/companies/check_phone_number_format",
                type: 'GET',
                data: {phone_number: phone_number},
                success: function (result) {
                    if (!result) {
                        heading.addClass("phone-valid");
                        $(submit_btn_id).prop('disabled', 'disabled');
                        $("#" + phone_error_div).text('Invalid Format!');
                    }
                    else{
                        heading.removeClass("phone-valid");
                        $("#" + phone_error_div).text('')
                        $(submit_btn_id).prop('disabled', false);
                    }
                },
                error: function (error) {

                    console.log(error)
                }
            });
        }else{
            $(submit_btn_id).prop('disabled', 'disabled');
            heading.removeClass("phone-valid");
            $("#"+phone_error_div).text('')
            $("#"+phone_error_div).text('Must be Number')
        }
    }else{
        $(submit_btn_id).prop('disabled', 'disabled');
        $("#"+phone_error_div).text('');
        heading.addClass("phone-valid");
        $("#"+phone_error_div).text('')
    }

});
$(document).on('click blur paste keyup change', '.cs_number', function (e) {
    var phone = $(this).val();
    var heading = $(this).parent().prev();
    var phone_code = $(this).closest('div').find('.selected-dial-code').text();
    var reg = new RegExp('^[0-9]+$');
    phone_code = phone_code.substring(1);
    var phone_number = phone_code + phone;
    var phone_error_div = $(this).parent().nextAll('.cs-phone-error').attr('id');
    var submit_btn_id = "#"+$(this).closest('form').find(':submit').attr("id");
    var phone_id = $(this).attr("id");
    if(phone.length > 0){
        if(reg.test(phone_number)){
            $("#"+phone_error_div).text('');
            $(submit_btn_id).prop( 'disabled', false);
            $.ajax({
                url: "/admins/companies/check_phone_number_format",
                type: 'GET',
                data: {phone_number: phone_number},
                success: function (result) {
                    if (!result) {
                        heading.addClass("phone-valid");
                        $(submit_btn_id).prop('disabled', 'disabled');
                        $("#" + phone_error_div).text('Invalid Format!');                    }
                    else{
                        heading.removeClass("phone-valid");
                        $("#" + phone_error_div).text('');
                        $("submit_btn_id").prop('disabled', false);
                    }
                },
                error: function (error) {

                    console.log(error);
                }
            });
        }else{
            $("submit_btn_id").prop('disabled', 'disabled');
            heading.removeClass("phone-valid");
            $("#"+phone_error_div).text('')
            $("#"+phone_error_div).text('Must be Number')
        }
    }else{
        $("submit_btn_id").prop('disabled', 'disabled');
        $("#"+phone_error_div).text('');
        heading.addClass("phone-valid");
        $("#"+phone_error_div).text('')
    }

});
$(document).on('keyup', ".user_percent",function () {
    $(".user_percent").inputmask('Regex', {regex: "^(?:\\d{1,2}(?:\\.\\d{1,2})?|100(?:\\.0?0)?)$"});
});
$('#cover-spin').fadeOut();

$(document).ready(emptyStateFunction);
function emptyStateFunction() {
    if($(".admins_country_field").val() != ""){
        $.ajax({
            url: '/admins/merchants/country_data',
            data: {country: $(".admins_country_field").val(),owner_field: 'change' },
            method: 'get',
            success: function (response) {
                var states = response["states"];
                if(Object.keys(states).length > 0 ){
                    $('.admins_states_field_loc').attr("required",true);
                    $('.admins_states_field_loc').attr("disabled",false);
                }else{
                    $("#location_state").empty();
                    $('.admins_states_field_loc').attr("required",false);
                    $('.admins_states_field_loc').attr("disabled",true);

                }

            }
        });
    }
}
