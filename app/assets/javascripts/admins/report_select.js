
$(document).ready(function() {
    $('.type1').multipleSelect({
        placeholder: "Select Type",
    });
    $('.categories').multipleSelect({
        placeholder: "Select Category",
    });
    $('.type').multipleSelect({
        placeholder: "Select Type",
    });
    $('.category').multipleSelect({
        placeholder: "Select Category",
    });
    $('.partner').multipleSelect({
        placeholder: "Select Partner",
    });
    $('.parameter').multipleSelect({
        placeholder: "Select Parameters",
    });
    $('.gateway').multipleSelect({
        placeholder: "Select Gateway",
    });

});