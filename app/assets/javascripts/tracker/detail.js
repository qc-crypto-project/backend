$(document).ready(function(){
    var status = $("#tracker_status").val();
    // complete  -- active --- disabled
    //pre_transit -- in_transit -- out_for_delivery  -- delivered -- return_to_sender -- failure -- unknown
    if (status == "pre_transit"){
        $("#pre_transit").addClass("active");
        $("#in_transit").addClass("disabled");
        $("#out_for_delivery").addClass("disabled");
        $("#delivered").addClass("disabled");

    }else if (status == "in_transit"){
        $("#pre_transit").addClass("complete");
        $("#in_transit").addClass("active");
        $("#out_for_delivery").addClass("disabled");
        $("#delivered").addClass("disabled");
    }
    else if (status == "out_for_delivery") {
        $("#pre_transit").addClass("complete");
        $("#in_transit").addClass("complete");
        $("#out_for_delivery").addClass("active");
        $("#delivered").addClass("disabled");
    }
    else if (status == "delivered"){
        $("#pre_transit").addClass("complete");
        $("#in_transit").addClass("complete");
        $("#out_for_delivery").addClass("complete");
        $("#delivered").addClass("complete");
    }
    else if (status == "return_to_sender"){
        $("#pre_transit").addClass("disabled");
        $("#in_transit").addClass("disabled");
        $("#out_for_delivery").addClass("disabled");
        $("#delivered").addClass("disabled");
    }
    else if (status == "failure"){
        $("#pre_transit").addClass("disabled");
        $("#in_transit").addClass("disabled");
        $("#out_for_delivery").addClass("disabled");
        $("#delivered").addClass("disabled");
    }
    else{
        // unknown case
        $("#pre_transit").addClass("disabled");
        $("#in_transit").addClass("disabled");
        $("#out_for_delivery").addClass("disabled");
        $("#delivered").addClass("disabled");
    }
});