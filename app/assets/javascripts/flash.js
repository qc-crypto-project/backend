function pushHtmlAlert(message, type) {
  $('#alert-message-top').html(
    "<div class='alert alert-'" + type + "' text-center bg-primary'>"
      + "<a href='#' data-dismiss='alert' class='close'>×</a><ul><h5>"
       + message + "</h5></ul></div>"
  );
}
