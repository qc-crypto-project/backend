App.cbk = App.cable.subscriptions.create "CbkChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    if data["action"] == "crash_worker"
      $('#error_body').show()
      $("#error_msg").text(data["message"])
      $('#info_box').hide()
    if data["action"] == "adminreport"
      $("#report_body_2").hide()
      $('#info_box').hide()
      $download_link = $('#download_link')
      $download_link.attr('href', "/admins/transactions/export_download?id="+data["message"])
      $download_button = $('#report_body_4').show()
    if data["action"] == "circulation"
      $('#info_box').show();
      $(".circulation").text(data["message"])
  speak: ->
    @perform 'speak'
