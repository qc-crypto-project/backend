App.cards = App.cable.subscriptions.create "CardsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    if data["action"] == "export_completed"
      $("#before_download_note").hide()
      $("#after_download_note").show()
    if data["action"] == "circulation"
      $("#progress_div").show();
      $("#progress").text(data["message"])
      $("#progress1").text(data["message"])
    if data["action"] == "circulation_completed"
      href = "/admins/blocked_cards/import_cards"+data["message"]["is_blocked"]+"&status=" + data["message"]["is_all_saved"]
      window.location.href = href