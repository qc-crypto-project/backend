App.customer_notification = App.cable.subscriptions.create "CustomerNotificationChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    #$('#notifications').prepend(data.html)
    $('#today_panel').find('div.no-notify').hide()
    if $("div[data-id*='"+data.id+"']").length == 0
     $('.empty_notification').hide();
     $('.all-notify').removeClass("hide");
     $('#today_panel').prepend(data.html);
     $('.notifiations').on 'click', '.notification-details', (e) ->
      if $(e.target).is('img.env') or $(e.target).is('img.noti-fa-close')
       e.preventDefault()
       return
      if $(this).data('href') != ''
       window.location.href = $(this).data('href')
       return
     if $('.notification_numbers').text() != ""
      $('.notification_numbers').html(parseInt($('.notification_numbers').text())+1)
     else
      $(".noti_count").append("<span class='alert_notification notification_numbers'>1</span></span>")