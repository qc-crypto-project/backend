App.notification = App.cable.subscriptions.create "NotificationChannel",
	connected: ->
		# Called when the subscription has been terminated by the server
    console.log("into notification connected")

	disconnected: ->
		# Called when the subscription has been terminated by the server
    console.log("into notification disconnected")

	received:(data)->
		$notifications_block = $('#notifications_block')
		$notifications_count = $('#notifications_count')
		# console.log $notifications_count.text()
		$notifications_block.show()
		$text = parseInt($notifications_count.text())
		$notifications_count = $('#notifications_count')
		if data["action"] == 'add'
			$display_text = $text + data["message"]
			if $text == 0
			  $notifications_block.show()
    else
			$display_text = $text - data["message"]

		$notifications_count.text($display_text)
		if $display_text == 0
			$notifications_block.hide()
	speak: (message)->
		@perform 'speak', message: message
