App.report = App.cable.subscriptions.create "ReportChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server
    $('#body_message').hide();
#    $('#body_done').show();
  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    if data["action"] == "sendemail"
      if window.location.pathname == data["message"]["url"]
        if data["message"]["report"] == "admin"
          window.location.href = '/admins/transactions/export_download?id=' + data["message"]["qc_file"]
        else
          window.location.href = '/merchant/transactions/export_download?id=' + data["message"]["qc_file"]
      $('#body_2').hide();
      $('#body_message').show();
      $('.export-status').text('');
      $("#message_h5").text('').text('Please Wait!')
      $('#printcount').text('').html("<span class='fa fa-spinner fa-pulse' id='hello'></span>&nbsp;Downloading File " + data["message"]["count"]);
      $("#message_p").text('');
    if data["action"] == "sendemailtango"
      $("#hidden_user_id").val(data["message"]["user_id"])
      $("#hidden_qc_file").val(data["message"]["qc_file"])
      $('#body_2').hide();
      $(".export-status").text("Exporting "+ data["message"]["count"] +" transactions, few seconds remaining....")
    if data["action"] == "sidebarcount"
      if $("#report_notifications_block1").length == 0
        $("#report_notifications_block2").show()
      $("#reports_read_count").text(data["message"]["count"])
    if data["action"] == "merchantexportdone"
      $('#body_message').hide();
      if data["message"]["no_tx"] == "no_tx"
        $('#body_2').hide();
        if data["message"]["message"] == "No Merchants found!"
          $('#no_tx').show().text("No Merchants found!");
        else
          $('#no_tx').show();
      else
        $('#body_done').show();
    if data["action"] == "crash_worker"
      $('#error_body').show()
      $("#error_msg").text(data["message"])
      $('#info_box').hide()
    if data["action"] == "merchant_email"
      $("#save_cron_job").val('')
      $("#save_cron_job").val(data["message"])
      $('.export_print').show()
      save_cron_job_value = $("#save_cron_job").val()
#      if save_cron_job_value != ""
#        $('#send_merchant_email_div').show()
    if data["action"] == "adminreport"
      $("#report_body_2").hide()
      $('#info_box').hide()
      $download_link = $('#download_link')
      $download_link.attr('href', "/admins/transactions/export_download?id="+data["message"])
      $download_button = $('#report_body_4').show()
    if data["action"] == "error"
      $(".export-print").hide();
      $heading = $('.export-heading')
      $heading.attr('style', "color: red; text-align: center; font-weight: bold;")
      $status = $(".export-status")
      $status.attr('style', "color: red;")
      $("#send_merchant_email_div").hide()
      $(".export-heading").text('').text(data["heading"])
      $(".export-status").text('').text(data["message"])
      $("#email_user_1").hide()
    if data["action"] == "no_reports"
      $("#report_body_2").hide()
      $('#info_box').hide()
      $('#no_reports').show()
    if data["action"] == "cc_report_export"
      $('#body_31').hide()
      $download_link = $('#download_link')
      $download_link.attr('href', "/admins/transactions/export_download?id="+data["message"])
      $download_button = $('#body_41').show()
    if data["action"] == "export"
      if data["message"]["count"]?
        $("div.export-status").text("Exporting "+ data["message"]["count"] +" transactions, few seconds remaining....")
      else
        $('#body_2').hide()
        $download_link = $('#download_link')
        $download_link.attr('href', "/admins/transactions/export_download?id="+data["message"])
        $download_button = $('#body_4').show()
    if data["action"] == "merchantexport"
      if data["message"]["count"]?
        $(".export-status").text("Exporting "+ data["message"]["count"] + " transactions, few seconds remaining....")
      else
        $('#body_2').hide()
        $download_link = $('.download_link')
        $download_link.attr('href', "/merchant/transactions/export_download?id="+data["message"])
        $download_button = $('#body_4').show()


  speak: (message) ->
    @perform 'speak', message: message
