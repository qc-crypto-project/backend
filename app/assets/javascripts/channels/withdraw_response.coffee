App.withdraw_response = App.cable.subscriptions.create "WithdrawResponseChannel",
 # Called when the subscription is ready for use on the server

  disconnected: ->
# Called when the subscription has been terminated by the server

  received: (data) ->
# Called when there's incoming data on the websocket for this channel
    if data["action"] == "fifty"
      randomize(25);
    if data["action"] == "finalize"
      randomize(50);
    if data["action"] == "done"
      randomize(100);
      $(".number").html(data["ach_id"]);
      $(".ach_number").html(data["message"]);
      $(".p2c_number").html(data["message"]);
      $(".gc_number").html(data["message"]);
      $('.working').hide();
      $('.success--image').show();
      $(".before_message").hide();
      $(".after_message").show();
    if data["action"] == "error"
      setTimeout ->
        $("#SuccessAchModal").modal("hide");
        $("#SuccessAccountTransferModal").modal("hide");
        $("#SuccessGiftModal").modal("hide");
        $("#SuccessVoidAccountTransferModal").modal("hide")
      , 500;
      $("#GeneralErrorMsgModal").modal("show");
      $("#error_body").html(data["message"]);
      $(".error-code").html(data["error_code"]);