//= require js/jquery-2.1.4.min
//= require jquery_ujs
//= require jquery.validate
//= require bootstrap-datepicker
//= require tether
//= require bootstrap.bundle
//= require adminss
//= require Chart.bundle
//= require highcharts
//= require chartkick
//= require vendor
//= require select2
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require app
//= require form
//= require cable
//= require merchant/jstz.min
//= require merchant/jquery.inputmask.bundle
//= require export_files
//= require jquery.fileDownload
//= require lightbox-bootstrap
//= require intlTelInput.js
//= require ajax_submit
//= require validator
//= require merchant/jquery.multiselect
//= require merchant_shared_js


// $(function () {
//     $('[data-toggle="tooltip"]').tooltip()
// });

function get_bank_by_routing_number(routing_number,url) {
    var routing_number=routing_number;
    if(routing_number!=""){
        $.ajax({
            url: url,
            method: 'post',
            data: {routing_number: routing_number},
            success: function (result) {
                var bank_name=result.response;
                if(bank_name!="") {
                    $('#routing-error').text('').hide();
                    $('#success-div').text(result.response);
                    $('#success-div').show();
                }
                else {
                    $('#routing-error').text('Cannot Find Bank With The Routing Number').show();
                    $('#success-div').text('');
                    $('#success-div').hide();
                }
            },
            error: function(){
                $('#routing-error').text('Cannot Find Bank With The Routing Number error').show();
                $('#success-div').text('');
                $('#success-div').hide();
            }
        });
    }
}
$(document).on('click','#btn_refund',function () {
    disableButton('','btn_refund')
})
$(document).on('click','.refund_btn',function () {
    disableButton('','btn_refund_verified')
})
function disableButton(form_Id, btn_Id) {
    console.log(form_Id);
    console.log(btn_Id);
    if (form_Id=='')
        document.getElementById(btn_Id).disabled = true;
    else if ($("#"+form_Id).valid()){
        document.getElementById(btn_Id).disabled = true;
    }

}
// $(document).ready(function(){
//     $(document).on('click', ".show-dynamic-modal", function () {
//         var url = $(this).data("url-path");
//         alert(url);
//         var modal_selector = $(this).data("modal-selector");
//         var modal_container_selector = $(this).data("modal-container-selector");
//         var method = $(this).data("method");
//         $.ajax({
//             url: url,
//             type: method,
//             success: function (data) {
//                 $(modal_container_selector).html(data);
//                 $(modal_selector).modal();
//                 $('#cover-spin').fadeOut();
//             }
//         })
//         return false;
//     });
//
// });

$(document).on("click", ".show-cover-spin", function () {
    $('#cover-spin').show(0);
})

function value_present(val) {
    if(val == ""){
        return false;
    }else if(val == null){
        return false;
    }else if(val == undefined){
        return false;
    }else{
        return true;
    }
}