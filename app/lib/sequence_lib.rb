class SequenceLib

  class TransactReference
    attr_accessor :type, :fee, :source, :fee_type, :company_id, :denominations

      def initialize(type, fee = 0, fee_type = 'app', source = nil, company_id = nil, denoms = nil, merchant = nil, location = nil, last4 = nil, fee_perc = nil,send_via=nil, send_check_user_info = nil, bpay_reference = nil, ip = nil, previous_issue = nil, tip = nil, sales_info = nil, location_fee = nil, api_type = nil,sub_type=nil, card_type = nil, dispute_case = nil,check_number=nil,bank_details=nil,refund_details=nil,card=nil,transfer_details=nil,source2=nil, local_transaction = nil,privacy_fee = nil,instrument=nil,descriptor=nil,gateway_fee_details=nil,flag=nil, main_transaction_info=nil,batch_info = nil, baked_type = nil)
        raise StandardError, 'Invalid Reference Data!' if type.nil? || source.nil?
        @type = type if type.present?
        @fee = fee if fee.present?
        @sub_type = fee_type if fee_type.present?
        @source = source if source.present?
        @company_id = company_id if company_id.present?
        @instrument = instrument || PaymentInstrument::Card
        @denominations = denoms if denoms.present?
        @merchant = merchant if merchant.present?
        @location = location if location.present?
        @last4 = last4 if last4.present?
        @fee_perc = fee_perc if fee_perc.present?
        @send_via= send_via if send_via.present?
        @send_check_user_info = send_check_user_info if send_check_user_info.present?
        @bride_pay_reference = bpay_reference if bpay_reference.present?
        @ip = ip if ip.present?
        @previous_issue = previous_issue if previous_issue.present?
        @card_type = card_type if card_type.present?
        @dispute_case = dispute_case if dispute_case.present?
        @bank_details = bank_details if bank_details.present?
        @refund_details = refund_details if refund_details.present?
        @card = card if card.present?
        @sub_type = sub_type if sub_type.present?
        @check_number = check_number if check_number.present?
        @tip = tip if tip.present?
        @sales_info = sales_info if sales_info.present?
        @location_fee = location_fee if location_fee.present?
        @api_type = api_type if api_type.present?
        @transfer_details = transfer_details if transfer_details.present?
        @source2 = source2.try(:name) if source2.present?
        @block_transaction_id = local_transaction if local_transaction.present?
        @privacy_fee = privacy_fee if privacy_fee.present?
        @gateway_fee_details = gateway_fee_details if gateway_fee_details.present?
        @descriptor = descriptor if descriptor.present?
        @flag = flag if flag.present?
        @main_transaction_info = main_transaction_info if main_transaction_info.present?
        @batch_info = batch_info if batch_info.present?
        @baked_type = baked_type if baked_type.present?
        @payment_gateway_id = source2.try(:id) if source2.present?
      end
  end

  class << self
    include ActionView::Helpers::NumberHelper
    include ApplicationHelper
    include Merchant::SalesHelper
    include Admins::BlockTransactionsHelper

    require 'open-uri'

    # @@ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential:  ENV['LEDGER_CREDENTIAL'] )

    def instance(ledger = nil)
      if ledger.present?
        @@ledger = Sequence::Client.new(ledger_name: ledger, credential:  ENV['LEDGER_CREDENTIAL'] )
      else
        @@ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential:  ENV['LEDGER_CREDENTIAL'] )
      end
    end

    def create(id, type, name = nil, number = nil, email = nil, ledger = nil)
      # if ledger.present?
        key = instance(ledger).keys.create(id: "#{id}")
        wallet = instance(ledger).accounts.create(id: "#{id}", key_ids: [key.id], tags: {type: type, name: name, number: number, email: email}, quorum: 1)
      # else
      #   key = instance.keys.create(id: "#{id}")
      #   wallet = instance.accounts.create(id: "#{id}", key_ids: [key.id], tags: {type: type, name: name, number: number, email: email}, quorum: 1)
      # end
      raise StandardError, 'Wallet creation failure' unless wallet.present?
      wallet
    end

    def balance(id, ledger = nil)
      return 0 if id.nil?
      sequence_down = AppConfig.sequence_down.first
      return 0 if sequence_down.present? && sequence_down.boolValue == true
      begin
        balance = instance(ledger).tokens.sum(
            filter: 'account_id=$1',
            filter_params: ["#{id}"],
            group_by: ['flavor_id']
        )
        if balance.first.blank?
          wallet = Wallet.find_by(id: id)
          wallet.update(balance: 0) if wallet.present?
          return dollars(wallet.balance)
        else
          wallet = Wallet.find_by(id: id)
          wallet.update(balance: dollars(balance.first.amount)) if wallet.present?
          return dollars(balance.first.amount)
        end
      rescue Sequence::APIError => exc
        # wallet = Wallet.find_by(id: id)
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, nil,error_details,"balance")
          return 0
        # raise SaleValidationError.new "Transaction failed due to an unexpected error [QC-002]"
          # return wallet.balance.to_f
      rescue => exc
        record_error(id, exc)
        return 0
        # raise SaleValidationError.new "Transaction failed due to an unexpected error [QC-002]"
      end
    end

    def balance_report(id, ledger = nil)
      #using for report to not update wallets
      return 0 if id.nil?
      begin
        balance = instance(ledger).tokens.sum(
            filter: 'account_id=$1',
            filter_params: ["#{id}"],
            group_by: ['flavor_id']
        )
        if balance.present? && balance.count <= 0
          return 0.00
        else
          return dollars(balance.first.amount)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, nil,error_details,"balance_report")

        return 0
      end
    end

    def issue(id, amountInDollars, merchant_wallet, type, fee, fee_type, source, company_id = nil, last4 = nil, merchant_name = nil, bpay_reference = nil, user_info = nil, ip = nil, check_info = nil, ledger = nil,previous_issue = nil,reserve_detail=nil, tip = nil, sales_info = nil, location_fee = nil, api_type=nil, sub_type=nil,check_number=nil, card=nil,key=nil,source2=nil, privacy_fee = nil,descriptor=nil, gateway_fee_details=nil,flag=nil,void_ach=nil, transfer_details = nil, refund_details = nil, bank_details = nil)
      begin
        wallet = Wallet.find(id)
        location = nil
        merchant = nil
        if wallet.location.present?
          location = location_to_parse(wallet.location)
          merchant = merchant_to_parse(wallet.location.merchant)
        elsif wallet.try('users').try('first').present? &&  wallet.try('users').try('first').role!='merchant'
          location=nil
          merchant = merchant_to_parse(wallet.users.first)
        end
        if merchant.nil?
          merchant = merchant_name
        end
        main_transaction_info = {
            amount: amountInDollars,
            fee: fee,
            transaction_type: type,
            transaction_sub_type: sub_type
        }
        tip_amount = tip[:sale_tip] if tip.present?
        tip_wallet = tip[:wallet_id] if tip.present?
        source = source if source.present?
        source = source2.try(:name) if source.nil? && source2.present?

        reference = JSON.parse(TransactReference.new(type,fee,fee_type,source,company_id,nil,merchant,location,last4, user_info,nil,check_info, bpay_reference, ip, previous_issue, tip_amount, sales_info, location_fee, api_type, sub_type,nil,nil,check_number,bank_details,refund_details,card,transfer_details,source2,nil, privacy_fee,nil,descriptor,gateway_fee_details,flag).to_json)
        if card.present?
          # doing this for 3ds transaction in case if card is not allowed to save
          old_card = reference["card"]
          if old_card.try(:[], "table").present?
            reference = reference.reject{|e| e["card"]}
            reference = reference.merge({"card" => old_card["table"]})
          end
        end
        #=for testing. needs to change after testing
        instance(ledger).transactions.transact do |builder|
          if tip.present? && cents(amountInDollars.to_f + tip_amount.to_f) > 0 && (sub_type.present? && sub_type != "sale_issue_api")
            builder.issue(
                flavor_id: 'usd',
                amount: cents(amountInDollars + tip_amount),
                # amount: cents(0),
                destination_account_id: "#{id}",
                action_tags: reference
            )
          else
            builder.issue(
                flavor_id: 'usd',
                amount: cents(amountInDollars),
                # amount: cents(0),
                destination_account_id: "#{id}",
                action_tags: reference
            )
          end

          tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
          if type.present?
            fee_sub_type = type
            tr_fee_type = type == "Failed ACH" ? [TypesEnumLib::TransactionType::FailedAchFee, nil] : tr_fee_type
          else
            fee_sub_type = sub_type
          end
          escrow = Wallet.escrow.first
          qc_wallet = Wallet.qc_support.first
          if cents(fee) > 0  && !["qr_debit_card","qr_credit_card"].include?(sub_type)
            if void_ach.present? && void_ach == true
              p "Transferring Fee from Qc Wallet to merchant => SOURCE: #{qc_wallet.id} - DESTINATION: #{id} - AMOUNT: #{cents(fee)}"
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{qc_wallet.id}",
                  destination_account_id: "#{id}",
                  action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant,location,last4, user_info, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
              )
            else
              if key.nil?
                p "Transferring Fee to ESCROW => SOURCE: #{id} - DESTINATION: #{escrow.id} - AMOUNT: #{cents(fee)}"
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(fee),
                    source_account_id: "#{id}",
                    destination_account_id: "#{escrow.id}",
                    action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant,location,last4, user_info, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
                )
              else # for virtual terminal transaction
                p "Direct Transfer: Transferring Fee to QC-Wallet => transaction SOURCE: #{escrow.id} - DESTINATION: #{qc_wallet.id} - AMOUNT: #{cents(fee)}"
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(fee),
                    source_account_id: "#{id}",
                    destination_account_id: "#{qc_wallet.id}",
                    action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant,location,last4, user_info, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
                )
              end
            end
          end
          if location_fee.present? && cents(location_fee) > 0
            p "Transferring Location Fee to ESCROW => SOURCE: #{id} - DESTINATION: #{escrow.id} - AMOUNT: #{cents(location_fee)}"
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(location_fee),
                source_account_id: "#{id}",
                destination_account_id: "#{escrow.id}",
                action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant,location,last4, user_info, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
            )
            p "Direct Transfer: Transferring Fee to QC-Wallet => transaction SOURCE: #{escrow.id} - DESTINATION: #{qc_wallet.id} - AMOUNT: #{cents(fee)}"
            # builder.transfer(
            #     flavor_id: 'usd',
            #     amount: cents(location_fee),
            #     source_account_id: "#{escrow.id}",
            #     destination_account_id: "#{qc_wallet.id}",
            #     action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant,location,last4, user_info, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,sub_type).to_json)
            # )
          end
          if user_info.present? && !["qr_debit_card","qr_credit_card"].include?(sub_type)
            divide_fee(builder ,user_info, qc_wallet, escrow, type, source, company_id, merchant, location, last4, nil, nil, ip,reserve_detail,sub_type,id, main_transaction_info)
          end
          if tip.present? && tip_wallet.present? && (sub_type.present? && sub_type != "sale_issue_api")
            if cents(tip_amount) > 0
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(tip_amount),
                  source_account_id: "#{id}",
                  destination_account_id: "#{tip_wallet}",
                  action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::TipTransfer, 0, 'sales_tip_fee', source, company_id, nil,merchant,location,last4, nil, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::TipTransfer).to_json)
              )
            end
          end
        end
      rescue Sequence::APIError => exc
        action_error = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, user_info,action_error,"issue")
        return []
      end
    end

    def transfer_issue(id, amountInDollars, destination_id, type, fee, fee_type, source, company_id = nil, last4 = nil, merchant_name = nil, bpay_reference = nil, user_info = nil, ip = nil, check_info = nil, ledger = nil,previous_issue = nil,reserve_detail=nil, tip = nil, sales_info = nil, location_fee = nil, api_type=nil, sub_type=nil,check_number=nil, card=nil,key=nil,source2=nil, privacy_fee = nil,descriptor=nil, gateway_fee_details=nil,flag=nil,void_ach=nil, transfer_details = nil,transfer_sub_type=nil,issue_user_info=nil)
      begin
        wallet = Wallet.find(id)
        location = nil
        merchant = nil
        if wallet.location.present?
          location = location_to_parse(wallet.location)
          merchant = merchant_to_parse(wallet.location.merchant)
        elsif wallet.try('users').try('first').present? &&  wallet.try('users').try('first').role!='merchant'
          location=nil
          merchant = merchant_to_parse(wallet.users.first)
        end
        merchant = merchant_name if merchant.nil?
        main_transaction_info = {
            amount: amountInDollars,
            fee: fee,
            transaction_type: type,
            transaction_sub_type: sub_type
        }
        tip_amount = tip.present? ? tip[:sale_tip].to_f : 0.0
        tip_wallet = tip[:wallet_id] if tip.present?
        source = source if source.present?
        source = source2 if source.nil? && source2.present?
        reference = TransactReference.new(type,fee,fee_type,source,company_id,nil,merchant,location,last4, user_info,nil,check_info, bpay_reference, ip, previous_issue, tip_amount, sales_info, location_fee, api_type, transfer_sub_type,nil,nil,check_number,nil,nil,card,transfer_details,source2,nil, privacy_fee,nil,descriptor,gateway_fee_details,flag).to_json
        instance(ledger).transactions.transact do |builder|
          amount_issue = amountInDollars + tip_amount
          issue_user_info = JSON.parse(issue_user_info)
          issue_user_info[1]["gbox"]["amount"] = 0 if issue_user_info[1].try(:[],"gbox").try(:[],"amount").present?
          issue_reference = TransactReference.new(type,issue_user_info.third,fee_type,source,company_id,nil,merchant,location,last4, issue_user_info.second,nil,check_info, bpay_reference, ip, previous_issue, tip_amount, sales_info, location_fee, api_type, sub_type,nil,nil,check_number,nil,nil,card,transfer_details,source2,nil, privacy_fee,nil,descriptor,gateway_fee_details,flag).to_json
          builder.issue(
              flavor_id: 'usd',
              amount: cents(amount_issue),
              destination_account_id: "#{destination_id}",
              action_tags: JSON.parse(issue_reference)
          )
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(amountInDollars),
              source_account_id: "#{destination_id}",
              destination_account_id: "#{id}",
              action_tags: JSON.parse(reference)
          )
          tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
          if type.present?
            fee_sub_type = type
          else
            fee_sub_type = sub_type
          end
          escrow = Wallet.escrow.first
          qc_wallet = Wallet.qc_support.first
          if cents(fee) > 0
            p "Transferring Fee to ESCROW => SOURCE: #{id} - DESTINATION: #{escrow.id} - AMOUNT: #{cents(fee)}"
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(fee),
                source_account_id: "#{id}",
                destination_account_id: "#{escrow.id}",
                action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant,location,last4, user_info, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
            )
          end
          if user_info.present?
            divide_fee(builder ,user_info, qc_wallet, escrow, type, source, company_id, merchant, location, last4, nil, nil, ip,reserve_detail,sub_type,id, main_transaction_info)
          end
          if tip.present?
            if cents(tip_amount) > 0
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(tip_amount),
                  source_account_id: "#{destination_id}",
                  destination_account_id: "#{tip_wallet}",
                  action_tags: JSON.parse(TransactReference.new('sale', 0, 'sales_tip_fee', source, company_id, nil,merchant,location,last4, nil, nil, nil, bpay_reference, ip,nil,nil,nil,nil,nil,"tip_vt_transaction_&_debit").to_json)
              )
            end
          end
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, user_info,error_details,"transfer_issue")
        return []
      end
    end

    def atm_deposit(amountInDollars, source, destination, instrument, data, company = nil, denominations = {}, last4 = nil)
      begin
        reference = TransactReference.new('KIOSK Cash Deposit', 0, 'KIOSK Fee', source, company, denominations,nil,nil, last4).to_json
        instance.transactions.transact do |builder|
          if instrument == PaymentInstrument::Cash
            builder.issue(
                flavor_id: 'usd',
                amount: cents(amountInDollars),
                destination_account_id: "#{source}",
                action_tags: JSON.parse(reference)
            )
          end
          if destination.present?
            builder.issue(
                flavor_id: 'usd',
                amount: cents(amountInDollars),
                destination_account_id: "#{destination}",
                action_tags: JSON.parse(reference)
            )
          end
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(source, exc, destination,nil,error_details,nil)
        return []
      end
    end

    def multiple_transfers(array_amount_and_wallet,source_id)
      begin
        instance.transactions.transact do |builder|
          array_amount_and_wallet.each do |amount_wallet|
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(amount_wallet[:amount]),
                source_account_id:source_id.to_s,
                destination_account_id:amount_wallet[:wallet_id].to_s,
                action_tags: JSON.parse(TransactReference.new('escrow_transfer',0,'Escrow Fee', 'Escrow', nil, nil,nil, nil, nil,nil, nil,nil,nil,nil).to_json)
            )
          end
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(source_id, exc,nil, nil,error_details,"multiple_transfers")
        return []
      end
    end

    def transfer_monthly_fees(amountInCents, source_id, type, escrow_wallet, qc_wallet, user_info, merchant, location, ledger = nil)
      # raise StandardError, I18n.t('merchant.controller.insufficient_balance') if (balance("#{source_id}", ledger)).to_f < amountInCents.to_f
      begin
        if (cents(balance("#{source_id}", ledger))).to_f > cents(amountInCents).to_f
          if merchant.present? || location.present?
            location_detail = location_to_parse(location)
            merchant_detail = merchant_to_parse(merchant)
          end
          tr = instance(ledger).transactions.transact do |builder|
            if cents(amountInCents) > 0
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(amountInCents),
                  source_account_id: "#{source_id}",
                  destination_account_id: "#{escrow_wallet}",
                  action_tags: JSON.parse(TransactReference.new(type,nil,nil,'Quickcard',nil,nil,merchant_detail,location_detail,nil,user_info).to_json)
              )
              if user_info.present?
                divide_monthly_fee(builder, user_info, qc_wallet, escrow_wallet, merchant_detail, location_detail)
              end
            end
          end
          tr
        else
          return []
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(source_id, exc,nil, user_info,error_details,"transfer_monthly_fees")
        return []
      end
    end

    def divide_monthly_fee(builder,user_info, qc_wallet, escrow_wallet, merchant, location)
      if user_info["gbox"].present? && user_info["gbox"]["wallet"].present?
        if cents(user_info["gbox"]["service"].to_f) > 0
          builder.transfer(
            flavor_id: 'usd',
            amount: cents(user_info["gbox"]["service"]),
            source_account_id: "#{escrow_wallet}",
            destination_account_id: "#{qc_wallet}",
            action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Service, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
          )
        end
        if cents(user_info["gbox"]["misc"].to_f) > 0
          builder.transfer(
            flavor_id: 'usd',
            amount: cents(user_info["gbox"]["misc"]),
            source_account_id: "#{escrow_wallet}",
            destination_account_id: "#{qc_wallet}",
            action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Misc, 0, TypesEnumLib::TransactionSubType::Misc, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Misc).to_json)
          )
        end
        if cents(user_info["gbox"]["statement"].to_f) > 0
          builder.transfer(
            flavor_id: 'usd',
            amount: cents(user_info["gbox"]["statement"]),
            source_account_id: "#{escrow_wallet}",
            destination_account_id: "#{qc_wallet}",
            action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Statement, 0, TypesEnumLib::TransactionSubType::Statement, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Statement).to_json)
          )
        end
      end
      if user_info["iso"].present? && user_info["iso"]["wallet"].present?
        if cents(user_info["iso"]["service"].to_f) > 0
          if user_info["iso"]["iso_buyrate"].present? && user_info["iso"]["iso_buyrate"] == true
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["service"]),
                source_account_id: "#{user_info["iso"]["wallet"]}",
                destination_account_id: "#{escrow_wallet}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Service, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["service"]),
                source_account_id: "#{escrow_wallet}",
                destination_account_id: "#{qc_wallet}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Service, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
          else
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["service"]),
                source_account_id: "#{escrow_wallet}",
                destination_account_id: "#{user_info['iso']['wallet']}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Service, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
          end
        end
        if cents(user_info["iso"]["misc"].to_f) > 0
          if user_info["iso"]["iso_buyrate"].present? && user_info["iso"]["iso_buyrate"] == true
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["misc"]),
                source_account_id: "#{user_info["iso"]["wallet"]}",
                destination_account_id: "#{escrow_wallet}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Misc, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["misc"]),
                source_account_id: "#{escrow_wallet}",
                destination_account_id: "#{qc_wallet}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Misc, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
          else
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["misc"]),
                source_account_id: "#{escrow_wallet}",
                destination_account_id: "#{user_info['iso']['wallet']}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Misc, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
          end
        end
        if cents(user_info["iso"]["statement"].to_f) > 0
          if user_info["iso"]["iso_buyrate"].present? && user_info["iso"]["iso_buyrate"] == true
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["statement"]),
                source_account_id: "#{user_info["iso"]["wallet"]}",
                destination_account_id: "#{escrow_wallet}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Statement, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["statement"]),
                source_account_id: "#{escrow_wallet}",
                destination_account_id: "#{qc_wallet}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Statement, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
          else
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(user_info["iso"]["statement"]),
                source_account_id: "#{escrow_wallet}",
                destination_account_id: "#{user_info['iso']['wallet']}",
                action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionSubType::Statement, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
            )
          end
        end
      end
      if user_info["agent"].present? && user_info["agent"]["wallet"].present?
        if cents(user_info["agent"]["service"].to_f) > 0
          builder.transfer(
            flavor_id: 'usd',
            amount: cents(user_info["agent"]["service"]),
            source_account_id: "#{user_info['iso']['wallet']}",
            destination_account_id: "#{user_info['agent']['wallet']}",
            action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::FeeTransfer, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
          )
        end
        if cents(user_info["agent"]["misc"].to_f) > 0
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["agent"]["misc"]),
              source_account_id: "#{user_info['iso']['wallet']}",
              destination_account_id: "#{user_info['agent']['wallet']}",
              action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::FeeTransfer, 0, TypesEnumLib::TransactionSubType::Misc, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Misc).to_json)
          )
        end
        if cents(user_info["agent"]["statement"].to_f) > 0
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["agent"]["statement"]),
              source_account_id: "#{user_info['iso']['wallet']}",
              destination_account_id: "#{user_info['agent']['wallet']}",
              action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::FeeTransfer, 0, TypesEnumLib::TransactionSubType::Statement, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Statement).to_json)
          )
        end
      end
      if user_info["affiliate"].present? && user_info["affiliate"]["wallet"].present?
        if cents(user_info["affiliate"]["service"].to_f) > 0
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["affiliate"]["service"]),
              source_account_id: "#{user_info['agent']['wallet']}",
              destination_account_id: "#{user_info['affiliate']['wallet']}",
              action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::FeeTransfer, 0, TypesEnumLib::TransactionSubType::Service, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Service).to_json)
          )
        end
        if cents(user_info["affiliate"]["misc"].to_f) > 0
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["affiliate"]["misc"]),
              source_account_id: "#{user_info['agent']['wallet']}",
              destination_account_id: "#{user_info['affiliate']['wallet']}",
              action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::FeeTransfer, 0, TypesEnumLib::TransactionSubType::Misc, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Misc).to_json)
          )
        end
        if cents(user_info["affiliate"]["statement"].to_f) > 0
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["affiliate"]["statement"]),
              source_account_id: "#{user_info['agent']['wallet']}",
              destination_account_id: "#{user_info['affiliate']['wallet']}",
              action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::FeeTransfer, 0, TypesEnumLib::TransactionSubType::Statement, 'Quickcard', nil,nil,merchant,location,nil, user_info,nil, nil, nil,nil,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::Statement).to_json)
          )
        end
      end
    end

    def transfer(amountInDollars, source_id, destination_id, type, fee, fee_type, source, company_id, merchant_name = nil, location_name = nil, fee_perc = nil, user_info = nil, ip = nil, reserve_detail = nil, ledger = nil, previous_issue = nil, sub_type=nil,card_type = nil,dispute_case = nil,bank_details=nil,refund_details=nil,card=nil,tip_in_qr_sale=nil,transfer_details=nil,source2=nil,privacy_fee =nil,instrument=nil,pg_fees = nil,flag=nil,send_check_case=nil,check_info=nil,void_withdrawal=nil,failed_fee=nil,ach_total_fee=nil,tracker_fee=nil,batch_info = nil,buyrate_fee=nil,iso_buyrate_case=false)
      begin
        if (type.present? && type.split(' ')[0] != "Void")
          raise StandardError, I18n.t('merchant.controller.insufficient_balance') if (balance("#{source_id}", ledger)).to_f < amountInDollars.to_f
        end
        location= nil
        merchant= nil
        if ( merchant_name.present? or location_name.present? ) && send_check_case.nil?
          location = location_to_parse(location_name)
          merchant = merchant_to_parse(merchant_name)
        elsif send_check_case.present?
          location = location_name
          merchant = merchant_name
        end
        tip_sale = tip_in_qr_sale[:sale_tip] if tip_in_qr_sale.present?
        tip_wallet = tip_in_qr_sale[:wallet_id] if tip_in_qr_sale.present?
        source = source if source.present?
        source = source2.try(:name) if source.nil? && source2.present?
        descriptor = source2.try(:name)
        if source2.present? && (source2.knox_payments? || source2.payment_technologies? || source2.total_pay?) && source.present?
          descriptor = source
        end
        # descriptor = PaymentGateway.find_by(key: source).try(:name)
        user_info = fee_perc if user_info.blank?
        main_transaction_info = {
            amount: amountInDollars,
            fee: fee,
            transaction_type: type,
            transaction_sub_type: sub_type
        }
        tr = instance(ledger).transactions.transact do |builder|
          main_reference = nil
          if cents(amountInDollars) > 0
            main_reference = JSON.parse(TransactReference.new(type,fee,fee_type,source,company_id,nil,merchant,location,nil,fee_perc,nil,check_info,nil,ip,previous_issue,tip_sale,nil,nil,nil,sub_type,card_type,dispute_case,nil,bank_details,refund_details,card,transfer_details,source2, nil, privacy_fee,instrument,descriptor,pg_fees,flag,nil,batch_info).to_json)
            if card.present?
              # doing this for 3ds transaction in case if card is not allowed to save
              old_card = main_reference["card"]
              if old_card.try(:[], "table").present?
                main_reference = main_reference.reject{|e| e["card"]}
                main_reference = main_reference.merge({"card" => old_card["table"]})
              end
            end
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(amountInDollars),
                source_account_id: "#{source_id}",
                destination_account_id: "#{destination_id}",
                action_tags: main_reference
            )
          end
          if tracker_fee.present?
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(tracker_fee[:amount]),
                source_account_id: "#{source_id}",
                destination_account_id: "#{destination_id}",
                action_tags: JSON.parse(TransactReference.new(tracker_fee[:type].first,fee,fee_type,source,company_id,nil,merchant,location,nil,fee_perc,nil,check_info,nil,ip,previous_issue,tip_sale,nil,nil,nil,sub_type,card_type,dispute_case,nil,bank_details,refund_details,card,transfer_details,source2, nil, privacy_fee,instrument,descriptor,pg_fees,flag).to_json)
            )
          end
          if tip_wallet.present?
            if cents(tip_sale) > 0
              tip_reference = TransactReference.new(TypesEnumLib::TransactionType::TipTransfer, 0, nil, source, company_id, nil,merchant,location,nil, nil, nil, nil, nil, ip,nil,nil,nil,nil,nil,TypesEnumLib::TransactionSubType::TipTransfer,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(tip_sale),
                  source_account_id: "#{destination_id}",
                  destination_account_id: "#{tip_wallet}",
                  action_tags: JSON.parse(tip_reference)
              )
            end
          end
          escrow = Wallet.escrow.first
          qc_wallet = Wallet.qc_support.first
          if (cents(fee) > 0 || cents(buyrate_fee) > 0)  && !iso_buyrate_case
            if source == TypesEnumLib::TransactionType::RetrievelFee
              tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::RetrievelFee)
            elsif type == TypesEnumLib::TransactionType::B2b
              tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::B2bFee)
            # elsif type == TypesEnumLib::TransactionType::ACH
            #   tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::ACH)
            else
              tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
            end
            if type.present?
              fee_sub_type = type
            else
              fee_sub_type = sub_type
            end
            if type == "refund"
              refund_reference = TransactReference.new(tr_fee_type.first, fee, fee_sub_type+'_fee', source, company_id, nil,merchant, location, nil,fee_perc, nil, nil, nil, ip,previous_issue,nil,nil,nil,nil,sub_type,card_type,dispute_case,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{source_id}",
                  destination_account_id: "#{escrow.try(:id)}",
                  action_tags: JSON.parse(refund_reference)
              )
            elsif void_withdrawal.present?
              void_reference = JSON.parse(TransactReference.new(tr_fee_type.first,fee,fee_type,source,company_id,nil,merchant,location,nil,fee_perc,nil,check_info,nil,ip,previous_issue,tip_sale,nil,nil,nil,sub_type,card_type,dispute_case,nil,bank_details,refund_details,card,transfer_details,source2, nil, privacy_fee,instrument,descriptor,pg_fees,flag,nil,batch_info).to_json)
              if cents(fee).to_f > 0
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(fee),
                    source_account_id: "#{source_id}",
                    destination_account_id: "#{destination_id}",
                    action_tags: void_reference
                )
              end
              if cents(buyrate_fee).to_f > 0
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(buyrate_fee),
                    source_account_id: "#{source_id}",
                    destination_account_id: "#{void_withdrawal}",
                    action_tags: void_reference
                )
              end
              if failed_fee.present? && cents(failed_fee) > 0
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(failed_fee),
                    source_account_id: "#{source_id}",
                    destination_account_id: "#{escrow.id}",
                    action_tags: void_reference
                )
              end
            elsif cents(ach_total_fee) > 0
              in_process_fee_reference = JSON.parse(TransactReference.new(tr_fee_type.first,fee,fee_type,source,company_id,nil,merchant,location,nil,fee_perc,nil,check_info,nil,ip,previous_issue,tip_sale,nil,nil,nil,sub_type,card_type,dispute_case,nil,bank_details,refund_details,card,transfer_details,source2, nil, privacy_fee,instrument,descriptor,pg_fees,flag).to_json)
              builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(ach_total_fee),
                    source_account_id: "#{source_id}",
                    destination_account_id: "#{escrow.id}",
                    action_tags: in_process_fee_reference
                )
            elsif type == "cbk_fee" or type == "retrievel_fee"
              cbk_type = tr_fee_type.first
              cbk_reference = TransactReference.new(cbk_type, fee, fee_sub_type, source, company_id, nil,merchant, location, nil,fee_perc, nil, nil, nil, ip,previous_issue,nil,nil,nil,nil,sub_type,card_type,dispute_case,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{source_id}",
                  destination_account_id: "#{escrow.id}",
                  action_tags: JSON.parse(cbk_reference)
              )
            elsif type == "buy_rate_fee"
              cbk_type = "Buy Rate Fee"
              cbk_reference = TransactReference.new(cbk_type, fee, fee_sub_type, source, company_id, nil,merchant, location, nil,fee_perc, nil, nil, nil, ip,previous_issue,nil,nil,nil,nil,sub_type,card_type,dispute_case,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json             
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{source_id}",
                  destination_account_id: "#{escrow.id}",
                  action_tags: JSON.parse(cbk_reference)
              )
              cbk_type_transfer = "Fee Transfer"
              cbk_reference_transfer = TransactReference.new(cbk_type_transfer, fee, fee_sub_type, source, company_id, nil,merchant, location, nil,fee_perc, nil, nil, nil, ip,previous_issue,nil,nil,nil,nil,sub_type,card_type,dispute_case,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json                          
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{escrow.id}",
                  destination_account_id: "#{qc_wallet.id}",
                  action_tags: JSON.parse(cbk_reference_transfer)
              )
            elsif (type == "send_check" || type == "instant_pay" || type == TypesEnumLib::GatewayType::ACH ) && send_check_case.present?
              send_reference = TransactReference.new(tr_fee_type.first, fee, fee_sub_type, source, company_id, nil,merchant, location, nil,fee_perc, nil, nil, nil, ip,previous_issue,nil,nil,nil,nil,sub_type,card_type,dispute_case,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
              sender_account_id = source_id
              withdrawal_fee = fee
              if fee_perc.try(:[],"iso").try(:[],"iso_buyrate")
                gbox_amount = fee_perc.try(:[],"gbox").try(:[],"amount")
                if cents(gbox_amount) > 0
                  builder.transfer(
                      flavor_id: 'usd',
                      amount: cents(gbox_amount),
                      source_account_id: "#{source_id}",
                      destination_account_id: "#{destination_id}",
                      action_tags: JSON.parse(send_reference)
                  )
                end
                withdrawal_fee = fee_perc.try(:[],"iso").try(:[],"amount")
                sender_account_id = fee_perc.try(:[],"iso").try(:[],"wallet")
                send_reference = JSON.parse(send_reference)
                send_reference["type"] = "Buy Rate Fee"
                send_reference = send_reference.to_json
              end
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(withdrawal_fee),
                  source_account_id: "#{sender_account_id}",
                  destination_account_id: "#{destination_id}",
                  action_tags: JSON.parse(send_reference)
              )
            else
              tx_type = type == "Failed ACH" ? TypesEnumLib::TransactionType::FailedAchFee : tr_fee_type.first
              source_account_id = get_source_account_id(tr_fee_type, type, source_id, destination_id, send_check_case)
              fee_reference = TransactReference.new(tx_type, fee, fee_sub_type+'_fee', source, company_id, nil,merchant, location, nil,fee_perc, nil, nil, nil, ip,previous_issue,nil,nil,nil,nil,sub_type,card_type,dispute_case,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{source_account_id}",
                  destination_account_id: "#{escrow.id}",
                  action_tags: JSON.parse(fee_reference)
              )
            end
          end
          user_info = fee_perc if user_info.nil?
          ispaid = true if cents(ach_total_fee) > 0
          if user_info.present? && (send_check_case.nil? || [TypesEnumLib::TransactionType::FailedPushtoCard.humanize.downcase, TypesEnumLib::TransactionType::FailedAch.humanize.downcase, TypesEnumLib::TransactionType::FailedCheck.humanize.downcase].include?(type.try(:downcase)))
            divide_fee(builder, user_info, qc_wallet, escrow, type, source, company_id, merchant, location, nil, nil, nil, ip, reserve_detail, sub_type,destination_id, main_transaction_info,source_id, ispaid,iso_buyrate_case)
          end
        end
        raise StandardError, 'Wallet Transfer Failed' unless tr.present?
        tr
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(source_id, exc, destination_id, user_info,error_details,"transfer")
        return []
      end
    end

    def direct_transfer(amountInDollars, source_id, destination_id, type, fee, fee_type, source, ip, ledger = nil)
      #in case of b2b from iso, agent, affilaite
      begin
        raise StandardError, I18n.t('merchant.controller.insufficient_balance') if (balance("#{source_id}", ledger)).to_f < amountInDollars.to_f
        tr = instance(ledger).transactions.transact do |builder|
          if cents(amountInDollars) > 0
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(amountInDollars),
                source_account_id: "#{source_id}",
                destination_account_id: "#{destination_id}",
                action_tags: JSON.parse(TransactReference.new(type,fee,fee_type,source,nil,nil,nil,nil,nil,nil,nil,nil,nil,ip).to_json)
            )
          end
          if cents(fee) > 0
            qc_wallet = Wallet.qc_support.first
            tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::B2bFee)
            fee_sub_type = type
            main_type = tr_fee_type.first
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(fee),
                source_account_id: "#{destination_id}",
                destination_account_id: "#{qc_wallet.id}",
                action_tags: JSON.parse(TransactReference.new(main_type, 0, fee_sub_type, source, nil, nil,nil, nil, nil,nil, nil, nil, nil, ip, nil,nil,nil,nil,nil,"invoice").to_json)
            )
          end
        end
        tr
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(source_id, exc, destination_id,nil,error_details,"direct_transfer")
        return []
      end
    end

    def retire(amountInDollars, id, type, fee, fee_type, source, company_id = nil,merchant=nil,location=nil , last4 = nil, send_via = nil,user_info = nil, send_check_user_info = nil, ip = nil, ledger = nil,sub_type=nil,bank_details=nil,refund_details=nil,gift=nil,descriptor=nil,dispute_case_body=nil, ispaid=nil, transfer_details = nil, gateway = nil)
      begin
        raise StandardError, I18n.t('merchant.controller.insufficient_balance') if (balance("#{id}", ledger)).to_f < amountInDollars.to_f if type != "refund"
        wallet = Wallet.find(id)
        location_primary_wallet = nil
        if location.present?
          if location.class == Hash
            location_primary_wallet = location.try(:wallets).try(:primary).try(:first)
            merchant = merchant
            location = location
          else
            location_primary_wallet = location.try(:wallets).try(:primary).try(:first)
            merchant = merchant_to_parse(location.merchant)
            location = location_to_parse(location)
          end
        elsif wallet.try(:location).present?
          location = location_to_parse(wallet.location)
          merchant = merchant_to_parse(wallet.location.merchant)
        end
        main_transaction_info = {
            amount: amountInDollars,
            fee: fee,
            transaction_type: type,
            transaction_sub_type: sub_type
        }
        instance(ledger).transactions.transact do |builder|
          if cents(amountInDollars) > 0
            builder.retire(
                flavor_id: 'usd',
                amount: cents(amountInDollars),
                source_account_id: "#{id}",
                action_tags: JSON.parse(TransactReference.new(type, fee, fee_type, source,company_id, nil,merchant,location,last4,user_info,send_via, send_check_user_info, nil,ip,nil,nil,nil,nil,nil,nil,nil,dispute_case_body,nil,bank_details,refund_details,nil,transfer_details,gateway,nil,nil,nil,descriptor).to_json)
            )
          end
          qc_wallet = Wallet.qc_support.first
          escrow = Wallet.escrow.first
          if cents(fee) > 0
            tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
            if type.present?
              fee_sub_type = type
            else
              fee_sub_type = sub_type
            end
            if gift.present?
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{id}",
                  destination_account_id: "#{qc_wallet.id}",
                  action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
              )
            elsif fee_type == "qr_credit_card" || fee_type == "qr_debit_card"
              if location_primary_wallet.present?
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(fee),
                    source_account_id: "#{location_primary_wallet.id}",
                    destination_account_id: "#{escrow.id}",
                    action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
                )
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(fee),
                    source_account_id: "#{escrow.id}",
                    destination_account_id: "#{qc_wallet.id}",
                    action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
                )
              end
              user_info = nil
            else
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(fee),
                  source_account_id: "#{id}",
                  destination_account_id: "#{escrow.id}",
                  action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
              )
              if user_info["iso"]["iso_buyrate"].present? && user_info["iso"]["iso_buyrate"] == true && ispaid.present?
                builder.transfer(
                    flavor_id: 'usd',
                    amount: cents(user_info["iso"]["amount"]),
                    source_account_id: "#{id}",
                    destination_account_id: "#{escrow.id}",
                    action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
                )
              end
            end
          else
            tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
            if type.present?
              fee_sub_type = type
            else
              fee_sub_type = sub_type
            end
            if user_info.present? && user_info.try(:[], "iso").try(:[], "iso_buyrate").present? && user_info.try(:[], "iso").try(:[], "iso_buyrate") == true && ispaid.present?
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(user_info["iso"]["amount"]),
                  source_account_id: "#{id}",
                  destination_account_id: "#{escrow.id}",
                  action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
              )
            end
          end
          if user_info.present?
            divide_fee(builder ,user_info, qc_wallet, escrow, type, source, company_id, merchant, location, last4, send_via, send_check_user_info, ip,nil,sub_type, id,main_transaction_info, nil, ispaid)
          end
        end
      rescue Sequence::APIError => exc
        p "============RETIRE ERROR============", exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, user_info,error_details,"retire")
        return []
      end
    end

    def custom_retire(amountInDollars, id, type, fee, fee_type, source, company_id = nil,merchant=nil,location=nil , last4 = nil, send_via = nil,user_info = nil, send_check_user_info = nil, ip = nil, ledger = nil,sub_type=nil,bank_details=nil,refund_details=nil)
      begin
        raise StandardError, I18n.t('merchant.controller.insufficient_balance') if (balance("#{id}", ledger)).to_f < amountInDollars.to_f if type != "refund"
        instance(ledger).transactions.transact do |builder|
          builder.retire(
              flavor_id: 'usd',
              amount: cents(amountInDollars),
              source_account_id: "#{id}",
              action_tags: JSON.parse(TransactReference.new(type, fee, fee_type, source,company_id, nil,merchant,location,last4,user_info,send_via, send_check_user_info, nil,ip,nil,nil,nil,nil,nil,nil,nil,nil,nil,bank_details,refund_details).to_json)
          )
          if cents(fee) > 0
            escrow = Wallet.escrow.first
            qc_wallet = Wallet.qc_support.first
            tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
            if type.present?
              fee_sub_type = type
            else
              fee_sub_type = sub_type
            end
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(fee),
                source_account_id: "#{id}",
                destination_account_id: "#{qc_wallet.id}",
                action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, user_info,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type).to_json)
            )
            if user_info.present?
              divide_fee(builder ,user_info, qc_wallet, escrow, type, source, company_id, merchant, location, last4, send_via, send_check_user_info, ip,nil,sub_type)
            end
          end
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, user_info,error_details,"custom_retire")
        return []
      end
    end

    def divide_fee(builder, user_info, qc_wallet, escrow_wallet, type, source, company_id, merchant, location, last4, send_via, send_check_user_info, ip = nil, reserve_detail = nil,sub_type = nil, destination_id = nil, main_transaction_info = nil, source_id = nil, ispaid=nil,iso_buyrate_case=false)
      # for dividing commissions between iso,agent, partner
      tr_fee_type = get_transaction_type(TypesEnumLib::TransactionType::FeeTransfer)
      if type.present?
        fee_sub_type = type
      else
        fee_sub_type = sub_type
      end
      if user_info["gbox"].present? && cents(user_info["gbox"]["amount"]) > 0 && user_info["gbox"]["wallet"].present?
        puts "gbox"
        gbox_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info, nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
        builder.transfer(
            flavor_id: 'usd',
            amount: cents(user_info["gbox"]["amount"]),
            source_account_id: "#{escrow_wallet.id}",
            destination_account_id: "#{user_info["gbox"]["wallet"]}",
            action_tags: JSON.parse(gbox_reference)
        )
      end
      if user_info["iso"].present? && cents(user_info["iso"]["amount"]) > 0 && user_info["iso"]["wallet"].present?
        # puts "ISO ====>> AMOUNT: #{cents(user_info["iso"]["amount"])}--> SOURCE: #{escrow_wallet.id}--> DESTINATION: #{user_info["iso"]["wallet"]}"
        if user_info["iso"]["iso_buyrate"].present? && user_info["iso"]["iso_buyrate"] == true && !ispaid.present? && type != "cbk_fee"
          puts "ISO ====>> AMOUNT: #{cents(user_info["iso"]["amount"])}--> SOURCE: #{escrow_wallet.id}--> DESTINATION: #{user_info["iso"]["wallet"]}"
          puts "into buyrate iso"
          buyrate_sender_wallet = iso_buyrate_case ? source_id : user_info["iso"]["wallet"]
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["iso"]["amount"]),
              source_account_id: "#{buyrate_sender_wallet}",
              destination_account_id: "#{escrow_wallet.id}",
              action_tags: JSON.parse(TransactReference.new(TypesEnumLib::TransactionType::BuyRateFee, 0, nil, source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
          )
          puts "sfter second buyt"
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["iso"]["amount"]),
              source_account_id: "#{escrow_wallet.id}",
              destination_account_id: "#{qc_wallet.id}",
              action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
          )
        elsif user_info["iso"]["iso_buyrate"].present? && user_info["iso"]["iso_buyrate"] == true && ispaid.present? && type != "cbk_fee"
          puts "sfter second buyt"
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["iso"]["amount"]),
              source_account_id: "#{escrow_wallet.id}",
              destination_account_id: "#{qc_wallet.id}",
              action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json)
          )
        elsif user_info["iso"]["iso_buyrate"].blank?
          puts "ISO ====>> AMOUNT: #{cents(user_info["iso"]["amount"])}--> SOURCE: #{escrow_wallet.id}--> DESTINATION: #{user_info["iso"]["wallet"]}"
          builder.transfer(
              flavor_id: 'usd',
              amount: cents(user_info["iso"]["amount"]),
              source_account_id: "#{escrow_wallet.id}",
              destination_account_id: "#{user_info["iso"]["wallet"]}",
              action_tags: JSON.parse(TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info, nil, "iso_profit").to_json)
          )
        end
      end
      if user_info["agent"].present? && cents(user_info["agent"]["amount"]) > 0 && user_info["agent"]["wallet"].present?
        puts "into agent"
        agent_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info, nil, "agent_profit").to_json
        builder.transfer(
          flavor_id: 'usd',
          amount: cents(user_info["agent"]["amount"]),
          source_account_id: "#{user_info['iso']['wallet']}",
          destination_account_id: "#{user_info["agent"]["wallet"]}",
          action_tags: JSON.parse(agent_reference)
        )
      end
      if user_info["affiliate"].present? && cents(user_info["affiliate"]["amount"]) > 0 && user_info["affiliate"]["wallet"].present?
        puts "into affiliate"
        affiliate_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info,nil, "affiliate_profit").to_json
        builder.transfer(
          flavor_id: 'usd',
          amount: cents(user_info["affiliate"]["amount"]),
          source_account_id: "#{user_info["agent"]["wallet"]}",
          destination_account_id: "#{user_info["affiliate"]["wallet"]}",
          action_tags: JSON.parse(affiliate_reference)
        )
      end
      if user_info["total_bonus"].present? && cents(user_info["total_bonus"]) > 0
        puts "into bonus"
        bonus_reference = TransactReference.new("Iso bonus", 0, nil, source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
        builder.transfer(
          flavor_id: 'usd',
          amount: cents(user_info["total_bonus"]),
          source_account_id: "#{qc_wallet.id}",
          destination_account_id: "#{user_info["iso"]["wallet"]}",
          action_tags: JSON.parse(bonus_reference)
        )
      end
      #---------------dispensary split credit starts-------------#
      if user_info["dispensary_credit_split"].present? && user_info["gbox"].present? && cents(user_info["gbox"]["amount"]) > 0
        reference = TransactReference.new("Fee Transfer", 0, nil, source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
        user_info["dispensary_credit_split"].each do |key,value|
          if cents(value["dollar"]) > 0
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(value["dollar"]),
                source_account_id: "#{escrow_wallet.id}",
                destination_account_id: "#{value["wallet"]}",
                action_tags: JSON.parse(reference)
            )
          end
          if cents(value["percent"]) > 0
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(value["percent"]),
                source_account_id: "#{escrow_wallet.id}",
                destination_account_id: "#{value["wallet"]}",
                action_tags: JSON.parse(reference)
            )
          end
        end
      end
      #---------------dispensary split credit end-------------#
      # #---------------dispensary split debit starts-------------#
      if user_info["dispensary_debit_split"].present? && user_info["gbox"].present? && cents(user_info["gbox"]["amount"]) > 0
        reference = TransactReference.new("Fee Transfer", 0, nil, source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
        user_info["dispensary_debit_split"].each do |key,value|
          if cents(value["dollar"]) > 0
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(value["dollar"]),
                source_account_id: "#{escrow_wallet.id}",
                destination_account_id: "#{value["wallet"]}",
                action_tags: JSON.parse(reference)
            )
          end
          if cents(value["percent"]) > 0
            builder.transfer(
                flavor_id: 'usd',
                amount: cents(value["percent"]),
                source_account_id: "#{escrow_wallet.id}",
                destination_account_id: "#{value["wallet"]}",
                action_tags: JSON.parse(reference)
            )
          end
        end
      end
      # #---------------dispensary split debit end-------------#



      ##  BAKED USERS FEE STARTS ##
      ### Location.find(location.values.first).baked_users.where(split_type: "baked_profit_split").pluck(:position).uniq

      if user_info["super_users_details"].present? # array of super users
        puts "into user deatils"
        user_info["super_users_details"].each do |super_user|  # loop for super users
          super_user.each do|key,value|
            if key.present?
              if value["dollar"].present? && value["wallet"].present? && value["from_wallet"].present?
                if cents(value["dollar"]) > 0
                  super_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info, nil, "baked_split").to_json
                  builder.transfer(
                      flavor_id: 'usd',
                      amount: cents(value["dollar"]),
                      source_account_id: "#{value["from_wallet"]}",
                      destination_account_id: "#{value["wallet"]}",
                      action_tags: JSON.parse(super_reference)
                  )
                end
              end
            end
          end
        end
      end
      if user_info["super_company"].present?
        puts "super compnay"
        user_info["super_company"].each do |key,value|
            if cents(value["amount"]) > 0 && value["wallet"].present?
              super_company_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info,nil,"profit_split").to_json
              if value["from"].present?
                from = value["from"].to_i
              else
                from = qc_wallet.id
              end
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(value["amount"]),
                  source_account_id: "#{from}",
                  destination_account_id: "#{value["wallet"]}",
                  action_tags: JSON.parse(super_company_reference)
              )
            end
        end
      end
      if user_info["net_super_company"].present? && user_info["net_super_company"]["wallet"].present? && cents(user_info["net_super_company"]["amount"]) > 0
        puts "New Net Super Company"
        net_super_company_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info,nil, "net_profit_split").to_json
        if user_info["net_super_company"]["from"].present?
          from = user_info["net_super_company"]["from"].to_i
        else
          from = qc_wallet.id
        end
        builder.transfer(
            flavor_id: 'usd',
            amount: cents(user_info["net_super_company"]["amount"]),
            source_account_id: "#{from}",
            destination_account_id: "#{user_info["net_super_company"]["wallet"]}",
            action_tags: JSON.parse(net_super_company_reference)
        )
      end
      #= divide sub merchant profit
      if user_info["sub_merchant_details"].present?
        if type == "refund"
          from_wallet = source_id
        else
          from_wallet = destination_id
        end
        user_info["sub_merchant_details"].each do |info|
          details = info.second
          if details.present?
            if cents(details["percent"]) > 0 && details["wallet"].present?
              super_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type).to_json
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(details["percent"]),
                  source_account_id: "#{from_wallet}",
                  destination_account_id: "#{escrow_wallet.id}",
                  action_tags: JSON.parse(super_reference)
              )
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(details["percent"]),
                  source_account_id: "#{escrow_wallet.id}",
                  destination_account_id: "#{details["wallet"]}",
                  action_tags: JSON.parse(super_reference)
              )
            end
            if cents(details["dollar"]) > 0 && details["wallet"].present?
              super_reference = TransactReference.new(tr_fee_type.first, 0, fee_sub_type+'_fee', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,sub_type).to_json
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(details["dollar"]),
                  source_account_id: "#{from_wallet}",
                  destination_account_id: "#{escrow_wallet.id}",
                  action_tags: JSON.parse(super_reference)
              )
              builder.transfer(
                  flavor_id: 'usd',
                  amount: cents(details["dollar"]),
                  source_account_id: "#{escrow_wallet.id}",
                  destination_account_id: "#{details["wallet"]}",
                  action_tags: JSON.parse(super_reference)
              )
            end
          end
        end
      end
      #= sub merchant profit end

      ##  BAKED USERS FEE END ##


      if reserve_detail.present? && reserve_detail[:wallet].present? && reserve_detail[:amount].present? && cents(reserve_detail[:amount]) > 0
        puts "iamsupperReserve==== >>#{reserve_detail[:amount]}-->#{escrow_wallet.id}-->#{reserve_detail[:wallet]}"
        wallet = Wallet.find reserve_detail[:wallet]
        tr_type = get_transaction_type(TypesEnumLib::TransactionType::Reserve)
        user = wallet.try(:users).try(:first)
        reserve_reference = TransactReference.new(tr_type.first, 0, fee_sub_type+'_reserve', source, company_id,nil,merchant,location,last4, nil,send_via, send_check_user_info,nil,ip,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,main_transaction_info).to_json
        builder.transfer(
          flavor_id: 'usd',
          amount: cents(reserve_detail[:amount]),
          source_account_id: "#{destination_id}",
          destination_account_id: "#{reserve_detail[:wallet]}",
          action_tags: JSON.parse(reserve_reference)
        )
        maintain_batch(user.id, reserve_detail[:wallet], reserve_detail[:amount], TypesEnumLib::Batch::Reserve, reserve_detail[:days])
      end
    end

    def transaction_with_time(account_id, time, time2, qc_wallet, type = nil, sub_type = nil,from_worker=nil,count=nil, cursor = nil)
      # for getting transactions between two times
      begin
        if cursor.present? && from_worker.present?
          page = instance.transactions.list().page(cursor: cursor)
          return page(page)
        end
        if type.present?
          if sub_type.present?
            # get sales transactions
            if sub_type == "sale"
              filter = 'actions((destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND tags.type = $4 )'
              filter_params = ["#{account_id}" , "#{time}", "#{time2}","#{sub_type}"]
            end
          else
            if type == "transfer"
              filter = 'actions((destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND type=$4 )'
              filter_params = ["#{account_id}" , "#{time}", "#{time2}", "#{type}"]
            elsif type == "retire"
              filter = 'actions((source_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND type=$4 )'
              filter_params = ["#{account_id}","#{time}", "#{time2}", "#{type}"]
            elsif type == "p2p_payment"
              filter = 'actions((source_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND tags.type=$4)'
              filter_params = ["#{account_id}","#{time}", "#{time2}", "#{type}"]
            end
          end
          # filter = 'actions((source_account_id=$1 AND destination_account_id=$4 AND (timestamp >= $2 AND timestamp <= $3)) OR (destination_account_id=$1 AND (timestamp >= $2 AND timestamp <= $3)))'
        elsif qc_wallet.present?
          filter = 'actions((source_account_id=$1 OR destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3))'
          filter_params = ["#{qc_wallet}","#{time}", "#{time2}"]
        else
          filter = 'actions((source_account_id=$1 OR destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3))'
          filter_params = ["#{account_id}","#{time}", "#{time2}"]
        end
        if from_worker
          page = instance.transactions.list( filter: filter, filter_params: filter_params).page(size: count || 10)
          return page(page)
        else
          return instance.transactions.list(filter: filter, filter_params: filter_params)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def all_locations_tranx_with_time(sources_ids, time, time2, type = nil, sub_type = nil)
      begin
        transax = nil
        if sub_type.present? && type.present?
          filter = 'actions((destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND (tags.type=$4 OR tags.type = $5))'
          filter_params = ["#{sources_ids}" , "#{time}", "#{time2}", "#{sub_type}", "#{type}"]
        elsif sub_type.present?
          filter = 'actions((destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND tags.type=$4)'
          filter_params = ["#{sources_ids}" , "#{time}", "#{time2}", "#{sub_type}"]
        elsif type.present?
          filter = 'actions((destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND tags.type=$4)'
          filter_params = ["#{sources_ids}" , "#{time}", "#{time2}", "#{type}"]
        else
          filter = 'actions((destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) )'
          filter_params = ["#{sources_ids}" , "#{time}", "#{time2}"]
        end
        transax =  instance.transactions.list(
            filter: filter,
            filter_params: filter_params
        )
        return transax
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(sources_ids, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def get_transaction_multiple(wallets_ids, time, time2, type,from_worker=nil,count=nil, cursor = nil)
      begin
        if cursor.present? && from_worker.present?
          page = instance.transactions.list().page(cursor: cursor)
          return page(page)
        end
        filter = ""
        filter_params = []
        date=""
        subtype = ""
        wallets_ids.each.with_index(1) do |wallet, index|
          if index != 1
            filter = filter + " OR "
          end
          filter = filter + "destination_account_id=$#{index}"
          filter_params << "#{wallet}"
        end

        if time.present? && time2.present?
          filter_params << time
          filter_params << time2
          date = "AND (timestamp >= $#{filter_params.size - 1} AND timestamp <= $#{filter_params.size})"
        end
        filter_params << type
        main_type = " AND tags.type=$#{filter_params.size}"
        filter = "actions((" + filter + ") " + "#{date}" + "#{main_type}" + ")"
        if from_worker
          page = instance.transactions.list(filter: filter, filter_params: filter_params).page(size: count || 10)
          return page(page)
        else
          transax =  instance.transactions.list(filter: filter, filter_params: filter_params)
          return transax
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(wallets_ids, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def transactions(account_id, destination_id, type, page_size, cursor = nil, ledger = nil, first_date = nil, second_date = nil)
      begin
        if cursor.present?
          return instance(ledger).transactions.list().page(cursor: cursor)
        end
        if account_id.present? && page_size != 0
          return instance(ledger).transactions.list().page(size: page_size)
        elsif account_id.present? || type.present?
          return user_transactions(account_id, destination_id, type, page_size, first_date, second_date)
        end
        return instance(ledger).transactions.list().page(size: page_size)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc, destination_id, nil,error_details,nil)
        return []
      end
    end

    def single_transactions(account_id, destination_id, type, page_size, cursor = nil)
      begin
        if cursor.present?
          return instance.transactions.list().page(cursor: cursor)
        end
        filter = 'actions(source_account_id=$1 OR destination_account_id=$2)'
        filter_params = ["#{account_id}", "#{destination_id}"]
        return instance.transactions.list(
            filter: filter,
            filter_params: filter_params
        ).page(size: page_size)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,destination_id, nil,error_details,nil)
        return []
      end
    end

    def get_transaction_from_id(id, page_size)
      begin
        filter = 'actions(id=$1)'
        filter_params = ["#{id}"]
        return instance.transactions.list(
            filter: filter,
            filter_params: filter_params
        ).page(size: page_size)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def user_transactions(account_id, destination_id, type, page_size = nil, ledger = nil, first_date = nil, second_date = nil)
      begin
        if type != ''
          if first_date.present? && second_date.present?
            filter = 'actions((source_account_id=$1 OR destination_account_id=$2) AND type=$3 AND (timestamp >= $4 AND timestamp <= $5 ))'
            filter_params = ["#{account_id}", "#{destination_id}", "#{type}", "#{first_date}", "#{second_date}"]
          else
            filter = 'actions((source_account_id=$1 OR destination_account_id=$2) AND type=$3)'
            filter_params = ["#{account_id}", "#{destination_id}", "#{type}"]
          end
        else
          if first_date.present? && second_date.present?
            filter = 'actions(source_account_id=$1 OR destination_account_id=$2 AND (timestamp >= $3 AND timestamp <= $4 ))'
            filter_params = ["#{account_id}", "#{destination_id}", "#{first_date}", "#{second_date}"]
          else
            filter = 'actions(source_account_id=$1 OR destination_account_id=$2)'
            filter_params = ["#{account_id}", "#{destination_id}"]
          end
        end
        if filter.present? && filter_params.present?
          return instance(ledger).transactions.list( filter: filter, filter_params: filter_params).map{|tx| tx if tx.present?}
        end
        return []
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,destination_id, nil,error_details,nil)
        return []
      end
    end

    def get_user_sales_count(wallets,start_date = nil,end_date = nil)
      begin
        filter = ""
        filter_params = []
        date=""
        wallets.each.with_index(1) do |wallet, index|
          if index != 1
            filter = filter + " OR "
          end
          p "index",index
          filter = filter + "destination_account_id=$#{index}"
          filter_params << "#{wallet}"
        end

        if start_date.present? && end_date.present?
          filter_params << start_date
          filter_params << end_date
          date = "AND (timestamp >= $#{filter_params.size - 1} AND timestamp <= $#{filter_params.size})"
        end
        filter = "(" + filter + ") " + "#{date}"
        count = instance(nil).actions.list(
            filter: filter,
            filter_params: filter_params
        ).count
      return count
      rescue Sequence::APIError => exc
        p exc
        return exc
      end
    end

    def user_array_transactions(wallets, type, ledger = nil,start_date = nil,end_date = nil)
      # ----get transcations from sequence againt each wallet given in array---------
      begin
        filter = ""
        filter_params = []
        # filter and filter params creation for sending to sequence
        wallets.each.with_index(1) do |wallet, index|
          filter = filter + "(source_account_id=$#{index} OR destination_account_id=$#{index}) OR "
          filter_params << ["#{wallet}"]
        end
        date=")"
        if start_date.present? && end_date.present?
          filter_params << start_date
          filter_params << end_date
          date = "AND (timestamp >= $#{filter_params.size - 1} AND timestamp <= $#{filter_params.size}) )"
        elsif start_date.present?
          filter_params << start_date
          date = "AND (timestamp >= $#{filter_params.size}) )"
          # -------------------------Only END Date Present-----------------------
        elsif end_date.present?
          filter_params << end_date
          date = "AND (timestamp <= $#{filter_params.size} ))"
        end
        if type.present?
          # ----------------------------REFUND MERCHANT WALLETS----------------------------------
          if type == "refund"
            # -------------------------Start Date and End Date Both Present-----------------------
            filter_params << "refund"
            filter = "actions(((#{filter[0...-3]} ) AND tags.type=$#{filter_params.size}) #{date}"
            # ----------------------------REFUND MERCHANT WALLETS----------------------------------
          else
            filter = "actions(((#{filter[0...-3]}) AND type=$2) #{date}"
          end
        else
          filter = "actions((#{filter[0...-3]}) #{date}"
        end
        if filter.present? && filter_params.present?
          # sequence function to list transactions
          return instance(ledger).transactions.list( filter: filter, filter_params: filter_params.flatten).map{|tx| tx if tx.present?}
        end
        return []
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(wallets, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def build_query(wallets, type, ledger = nil, start_date = nil, end_date = nil, count = 10)
      # ----building query for sequence---------
      # begin
        filter = ""
        filter_params = []
        # filter and filter params creation for sending to sequence
        wallets.each.with_index(1) do |wallet, index|
          filter = filter + "(source_account_id=$#{index} OR destination_account_id=$#{index}) OR "
          filter_params << ["#{wallet}"]
        end
        date=")"
        if start_date.present? && end_date.present?
          filter_params << start_date
          filter_params << end_date
          date = "AND (timestamp >= $#{filter_params.size - 1} AND timestamp <= $#{filter_params.size}) )"
        elsif start_date.present?
          filter_params << start_date
          date = "AND (timestamp >= $#{filter_params.size}) )"
          # -------------------------Only END Date Present-----------------------
        elsif end_date.present?
          filter_params << end_date
          date = "AND (timestamp <= $#{filter_params.size} ))"
        end
        if type.present?
          # ----------------------------REFUND MERCHANT WALLETS----------------------------------
          if type == "refund"
            # -------------------------Start Date and End Date Both Present-----------------------
            filter_params << "refund"
            filter = "actions((#{filter[0...-3]} ) AND tags.type=$#{filter_params.size} #{date}"
            # ----------------------------REFUND MERCHANT WALLETS----------------------------------
          else
            filter = "actions((#{filter[0...-3]}) AND type=$2 #{date}"
          end
        else
          filter = "actions((#{filter[0...-3]}) #{date}"
        end
        return {filter: filter, filter_params: filter_params.flatten}
      # rescue Sequence::APIError => exc
      #   record_error(wallets, exc)
      #   return nil
      # end
    end

    def query_transactions(wallets, type, ledger = nil, start_date = nil, end_date = nil, count = nil,cursor = nil)
      begin
        if cursor.present?
          page = instance.transactions.list().page(cursor: cursor)
          return page(page)
        end
        query = build_query(wallets, type, ledger, start_date, end_date, count)
        p "Query Built for Sequence: ", query.to_json
        if query[:filter].present? && query[:filter_params].present?
          page = instance.transactions.list( filter: query[:filter], filter_params: query[:filter_params]).page(size: count || 10)
          return page(page)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(wallets, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def query_export_transactions(wallets, type, ledger = nil, start_date = nil, end_date = nil, count = nil, cursor = nil)
      # for getting transactions fro iso, agent, affiliate in export
      begin
        if cursor.present?
          page = instance.transactions.list().page(cursor: cursor)
          return page(page)
        end
        query = build_query(wallets, type, ledger, start_date, end_date, count)
        p "Query Built for Sequence: ", query.to_json
        if query[:filter].present? && query[:filter_params].present?
          if count.present?
            page = instance.transactions.list( filter: query[:filter], filter_params: query[:filter_params]).page(size: count)
          else
            page = instance.transactions.list( filter: query[:filter], filter_params: query[:filter_params]).map{|tx| tx if tx.present?}
          end
          return page(page)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(wallets, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def find_tab_type(wallet_id=nil,tab_type=nil)
      if wallet_id.present?
        if tab_type=='checks'
          filter = "actions((tags.type=$1 OR tags.type=$2 OR tags.type=$3) AND (source_account_id=$4 OR destination_account_id=$4))"
          filter_params = ["send_check", "Void_Check","bulk_check","#{wallet_id}"]
        elsif tab_type=='b2b'
          filter = "actions((tags.type=$1) AND (source_account_id=$2 OR destination_account_id=$2))"
          filter_params = ["b2b_transfer","#{wallet_id}"]
        else
          filter = "actions((tags.type=$1 OR tags.type=$2) AND (source_account_id=$3 OR destination_account_id=$3))"
          filter_params = ["refund", "refund_bank","#{wallet_id}"]
        end
      else
        if tab_type=='checks'
          filter = "actions(tags.type=$1 OR tags.type=$2 OR tags.type=$3)"
          filter_params = ["send_check", "Void_Check","bulk_check"]
        elsif tab_type=='b2b'
          filter = "actions(tags.type=$1)"
          filter_params = ["b2b_transfer"]
        else
          filter = "actions(tags.type=$1 OR tags.type=$2)"
          filter_params = ["refund", "refund_bank"]
        end
      end
      [filter,filter_params]
    end

    def all_transactions_clone(first_date = nil, second_date = nil, page_size = nil, cursor = nil)
      begin
        if cursor.present?
          return instance.actions.list().page(cursor: cursor)
        end
        if first_date.present? && second_date.present?
          filter = 'timestamp >= $1 AND timestamp <= $2'
          filter_params = ["#{first_date}", "#{second_date}"]
          page = instance.actions.list(filter: filter, filter_params: filter_params).page(size: page_size || 10)
        elsif first_date.present? && second_date.nil?
          filter = 'timestamp > $1'
          filter_params = ["#{first_date}"]
          page = instance.actions.list(filter: filter, filter_params: filter_params).page(size: page_size || 10)
        else
          page = instance.actions.list().page(size: page_size || 10)
        end
        return page(page)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def all_transactions_clone_new(first_date = nil, second_date = nil, count = nil, cursor = nil)
      begin
        if first_date.present? && second_date.present?
          filter = 'timestamp >= $1 AND timestamp <= $2'
          filter_params = ["#{first_date}", "#{second_date}"]

        elsif first_date.present? && second_date.nil?
          filter = 'timestamp > $1'
          filter_params = ["#{first_date}"]
        else
          if cursor.present?
            page = instance.actions.list().page(cursor: cursor)
          else
            page = instance.actions.list().page(size: count)
          end
        end
        if cursor.present? && page.blank?
          page = instance.actions.list().page(cursor: cursor)
        elsif cursor.blank? && page.blank?
          page = instance.actions.list(filter: filter, filter_params: filter_params).page(size: count)
        end
        return page(page)
      rescue Sequence::APIError => exc
        puts "clone transaction error", exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return {}
      end
    end

    # For Merchnat Transaction search
    def transactions_merchant_search(wallets,name_wallets=nil, email_wallet=nil,phone_wallet=nil,amount=nil,date=nil,last4=nil, page_size = nil,type = nil,block_id=nil)
      begin
        @key = nil
        @filter = []
        search_params = {personal_wallets: wallets,name_wallets: name_wallets,email_wallets: email_wallet,phone_wallets: phone_wallet,amount: amount, date: date,last4: last4,type:type,block_id: block_id}
        result = building_search_query_merchant(search_params)
        @key = result.first
        @filter = result.second
        filter = "actions(#{@key})"
        filter_params = @filter
        if filter.present? && filter_params.present?
          page = instance.transactions.list( filter: filter, filter_params: filter_params).page(size: page_size || 10)
        end
        return page(page)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def all_transactions(wallet)
      begin
        filter = 'actions(source_account_id=$1 OR destination_account_id=$1)'
        filter_params = ["#{wallet}"]
        parse(wallet, instance.transactions.list( filter: filter, filter_params: filter_params).reject(&:nil?))
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(wallet, exc,nil, nil,error_details,nil)
        return nil
      end
    end

    def all_count_transactions(wallet)
      begin
        filter = 'actions(source_account_id=$1 OR destination_account_id=$1)'
        filter_params = ["#{wallet}"]
        parse_count(wallet,instance.transactions.list( filter: filter, filter_params: filter_params).reject(&:nil?))
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(wallet, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def next_page(cursor)
      begin
        if cursor.present?
          cursor_string = cursor.delete('\\"')
          return page(instance.transactions.list().page(cursor: cursor_string))
        end
        return nil
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def page(page)
      if page.present?
        {
            items: page.items.reject(&:nil?),
            cursor: page.cursor,
            last_page: page.last_page
        }
      end
    end

    def get_transaction_id(trans_id)
      begin
        list = []
        instance.transactions.list(
            filter: 'actions(id=$1)',
            filter_params: ["#{trans_id}"]
        ).each{ |tx| list.push(tx) if tx.present? }
        if list.blank?
          instance.transactions.list(
              filter: 'id=$1',
              filter_params: ["#{trans_id}"]
          ).each{ |tx| list.push(tx) if tx.present? }
        end
        return list
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        raise SaleValidationError.new "Transaction failed due to an unexpected error [QC-002]"
        # return []
      end
    end

    def get_action_id(action_id)
      begin
        instance.actions.list(
            filter: 'id=$1',
            filter_params: ["#{action_id}"]).map{|tx| tx}
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def between_transactions(account_id,destination_id, type, page_size)
      begin
        list = []
        filter = nil
        filter_params = nil
        filter = 'actions((source_account_id=$1 OR destination_account_id=$1) AND (source_account_id=$2 OR destination_account_id=$2))'
        filter_params = ["#{account_id}", "#{destination_id}"]
        # elsif account_id == '' && type == ''
        #   instance.transactions.list().page(size: page_size).each{|tx| list.push(tx) if tx.present?}
        if filter.present? && filter_params.present? && list.count <= 0
          instance.transactions.list(
              filter: filter,
              filter_params: filter_params
          ).each{|tx| list.push(tx) if tx.present?}
        end
        return list
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,destination_id, nil,error_details,nil)
        return []
      end
    end

    def total_transactions
      begin
        instance.transactions.list().count
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def account_tag_sum(key)
      begin
        sum = instance.tokens.sum(
            filter: 'account_tags.type=$1',
            filter_params: [key]
        )
        if sum.present? && sum.count <= 0
          return 0.00
        else
          return dollars(sum.first.amount)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return 0.00
      end
    end

    def total_sum_amounts(account_id, time, time2, type = nil)
      begin
        filter = nil
        filter_params = nil
        group_by = nil
        if type == "fee"
          qc_wallet = Wallet.qc_support.first.id
          filter = '(source_account_id=$1 AND destination_account_id=$4) AND (timestamp >= $2 AND timestamp <= $3)'
          filter_params = ["#{account_id}","#{time}", "#{time2}","#{qc_wallet}"]
          group_by = ['type']

        elsif type == "sale"
          filter = '(destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3)'
          filter_params = ["#{account_id}","#{time}", "#{time2}"]
          group_by = ['type']

        elsif type == "retire"
          filter = '(source_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND type=$4'
          filter_params = ["#{account_id}","#{time}", "#{time2}", "#{type}"]
          group_by = ['type']

        elsif type == "issue"
          filter = '(destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND type=$4'
          filter_params = ["#{account_id}","#{time}", "#{time2}", "#{type}"]
          group_by = ['type']
        end

        if filter.present? && filter_params.present? && group_by.present?
          sum = instance.actions.sum(
              filter: filter,
              filter_params: filter_params,
              group_by: group_by
          )
          total = 0
          sum.each do |deposit|
            if type == "sale"
              if deposit.type == "transfer"
                total = deposit.amount
              end
            else
              total = deposit.amount
            end
          end
          return number_with_precision(dollars(total), precision: 2)
        else
          return 0.0
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)
        return 0.00
      end
    end

    #getting total fee between two dates
    def total_fee_sum(account_id, time, time2)
      begin
        qc_wallet = Wallet.qc_support.first.id
        sum = instance.actions.sum(
            filter: '(source_account_id=$1 AND destination_account_id=$4) AND (timestamp >= $2 AND timestamp <= $3)',
            filter_params: ["#{account_id}","#{time}", "#{time2}","#{qc_wallet}"],
            group_by: ['source_account_id', 'type']
        )

        total_transfer = 0

        sum.each do |deposit|
          if deposit.type == 'transfer'
            total_transfer = deposit.amount
          end
        end
        return number_with_precision(dollars(total_transfer), precision: 2)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)
        return 0.00
      end
    end

    def total_destination_sum(destination_id,type)
      begin
        sum = 0
        if type.present? and destination_id.present?
          sum = instance.actions.sum(
              filter: '(destination_account_id=$1 AND type=$2)',
              filter_params: ["#{destination_id}","#{type}"],
              group_by: ['destination_account_id', 'type']
          )
        end
        return JSON(sum.to_json).first
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(destination_id, exc,nil, nil,error_details,nil)
        return {amount: 0}
      end
    end

    def total_source_sum(source_id,type)
      begin
        sum = 0
        if type.present? and source_id.present?
          sum = instance.actions.sum(
              filter: '(source_account_id=$1 AND type=$2)',
              filter_params: ["#{source_id}","#{type}"],
              group_by: ['source_account_id', 'type']
          )
        end
        return JSON(sum.to_json).first
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(source_id, exc,nil, nil,error_details,nil)
        return {amount: 0}
      end
    end

    def total_withdraw(account_id, time, time2, type)
      begin
        #for getting total withdraw
        sum = instance.actions.sum(
            filter: '(source_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3) AND type=$4',
            filter_params: ["#{account_id}","#{time}", "#{time2}", "#{type}"],
            group_by: ['type']
        )

        total_retire = 0

        sum.each do |deposit|
          if deposit.type == 'retire'
            total_retire = deposit.amount
          end
        end
        return number_with_precision(dollars(total_retire), precision: 2)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)
        return 0.00
      end
    end

    #total profit of wallet
    def total_profit_sum(account_id, time, time2)
      begin
        sum = instance.actions.sum(
            filter: '(destination_account_id=$1) AND (timestamp >= $2 AND timestamp <= $3)',
            filter_params: ["#{account_id}","#{time}", "#{time2}"],
            group_by: ['type']
        )
        total_profit = 0
        sum.each do |deposit|
          if deposit.type == 'transfer'
            total_profit = deposit.amount
          end
        end
        number_with_precision(dollars(total_profit), precision: 2)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)
        return 0.00
      end
    end

    #for getting single user circulation in the system
    def user_total_circulation(account_id)
      begin
        page1 = instance.actions.sum(
            filter: 'source_account_id=$1 OR destination_account_id=$1',
            filter_params: ["#{account_id}"],
            group_by: ['type', 'asset_alias', 'asset_id']
        )
        total_issue = 0
        total_withdraw = 0
        total_transfer = 0
        page1.each do |total|
          if total.type == 'issue'
            total_issue = total.amount
          elsif total.type == 'retire'
            total_withdraw = total.amount
          else
            total_transfer = total.amount
          end
        end
        {
            total_issue: number_with_precision(number_to_currency(dollars(total_issue)), precision: 2),
            total_transfer: number_with_precision(number_to_currency(dollars(total_transfer)), precision: 2),
            total_withdraw: number_with_precision(number_to_currency(dollars(total_withdraw)), precision: 2)
        }
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)
        return {
            total_issue: 0.00,
            total_transfer: 0.00,
            total_withdraw: 0.00
        }
      end
    end

    def user_total_recieved(account_id)
      begin
        page1 = instance.actions.sum(
            filter: 'destination_account_id=$1',
            filter_params: ["#{account_id}"],
            group_by: ['type', 'asset_alias', 'asset_id']
        )

        total_recieved = 0
        page1.each do |total|
          if total.type == 'transfer'
            total_recieved = total.amount
          end
        end
        { total_recieved: number_with_precision(number_to_currency(dollars(total_recieved)), precision: 2)}
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(account_id, exc,nil, nil,error_details,nil)

        return { total_recieved: 0.00}
      end
    end

    #for getting circulation of amount in whole system
    def total_circulation(flavor)
      begin
        page1 =  instance.tokens.sum(
            filter: 'flavor_id=$1',
            filter_params: ["#{flavor}"]
        ).page(size: 100)

        page1.each do |circulation|
          return number_with_precision(number_to_currency(dollars(circulation.amount)), precision: 2)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return 0.00
      end
    end

    def export_accounts_list(flavor)
      begin
        # export list of accounts in ledger
        page1 = instance.tokens.sum(
            filter: 'flavor_id=$1',
            filter_params: ["#{flavor}"],
            group_by: ['account_id','flavor_id']
        )
        list = []
        page1.map do |account|
          if account.amount > 0
            wallet = Wallet.find_by_id(account.account_id.to_i)
            user = wallet.users.first if wallet.present?
            if user.present?
              hash = {
                  name: user.name,
                  email: user.email,
                  amount: number_with_precision(number_to_currency(dollars(account.amount)), precision: 2),
                  phone_number: user.phone_number
              }
              hash.merge({})
            end
            list.push(hash)
          end
        end
        list.flatten.compact
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(nil, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def transaction(id, ledger = nil,from_block_tx = nil)
      begin
        filter = 'actions(id=$1)'
        filter_params = ["#{id}"]
        if from_block_tx
          transaction = instance(ledger).transactions.list( filter: filter, filter_params: filter_params)
          return transaction
        else
          transaction = instance(ledger).transactions.list( filter: filter, filter_params: filter_params).map{|tx| tx if tx.present?}.first
          parse_action(transaction.actions.first, transaction.timestamp)
        end
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def list_transaction(id, ledger = nil)
      begin
        filter = "id=$1"
        filter_params = ["#{id}"]
        transaction =  instance(ledger).actions.list(filter: filter, filter_params: filter_params).map{|tx| tx if tx.present?}.first
        parse_action(transaction, transaction.timestamp)
      rescue Sequence::APIError => exc
        error_details = JSON.parse(exc.response.body).try(:[],'data').try(:[],'actions')
        record_error(id, exc,nil, nil,error_details,nil)
        return []
      end
    end

    def parse(wallet, transactions)
      list = []
      transactions.each do |obj|
        if obj.present?
          list << filter_actions(obj, wallet).map do |t|
            item = parse_action(t, obj.timestamp)
            item[:type] = parse_type(wallet, t)
            item
          end
        end
      end
      list.reduce([], :concat).reject(&:nil?)
    end

    def parse_count(wallet, transactions)
      list = []
      transactions.each do |obj|
        if obj.present?
          list << filter_actions(obj, wallet)
        end
      end
      list.reduce([], :concat).reject(&:nil?)
    end

    def parse_action(t, timestamp)
      {
          id: t.id,
          timestamp: timestamp,
          main_type: t.type,
          type: updated_type(t.tags["type"],t.tags["sub_type"]),
          fee: t.tags["fee"].to_f,
          tip: getTip(t.tags),
          amount: number_with_precision(SequenceLib.dollars(t.amount), precision: 2),
          amount_fee: number_with_precision(parse_merchant_amount(t), precision: 2),
          destination: t.destination_account_id,
          source: t.source_account_id,
          reference: t.tags,
          fee_updated: (get_fee_updated(t.tags)).abs,
          hold_money: get_hold_money(t.tags).to_f,
          net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
          amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type,t.tags).to_f, precision: 2),
          sub_merchant_detail: t.tags["fee_perc"].try(:[],"sub_merchant_details"),
          merchant: t.tags["merchant"]
      }
    end

    def db_parse_action(t, timestamp)
      tags = t.tags
      {
          fee: tags["fee"].to_f,
          tip: getTip(tags),
          amount: number_with_precision(t.amount, precision: 2),
          destination: t.from,
          source: t.to,
          reference: tags,
          fee_updated: (get_fee_updated(tags)).abs,
          hold_money: get_hold_money(t.tags).to_f,
          net: number_with_precision(get_new_net(t.amount,tags["fee"].to_f,"null", tags).to_f, precision: 2),
          amount_updated: number_with_precision(get_new_amount(t.amount,tags["fee"].to_f,"null",tags).to_f, precision: 2),
      }
    end

    def parse_type(wallet, action)
      return action.type if wallet.nil?
      if (action.type == 'transfer' && action.destination_account_id == wallet.to_s) || action.type == "issue"
        'Deposit'
      else
        'Withdraw'
      end
    end

    def filter_actions(obj, wallet)
      return obj.actions.select{|o| o.source_account_id == wallet.to_s || o.destination_account_id == wallet.to_s} if wallet.present?
      obj.actions
    end

    def cents(dollars)
      number_with_precision(dollars.to_f * 100, precision: 2).to_i
    end

    def dollars(cents)
      number_with_precision(cents.to_f / 100, precision: 2).to_f
    end

    def format_dollar(amount)
      number_with_precision(amount, precision: 2)
    end

    #for merchant transaction search
    def building_search_query_merchant(params)
      paramss = params.delete_if{|k,v| v.blank?}
      query = ''
      query_params = []
      i = 1
      paramss.each do |p|
        if p.first == :personal_wallets
          filter = ''
          filter_params = []
          query_index = 1
          p.second.each.with_index(1) do |wallet, index|
            filter = filter + "(source_account_id=$#{index} OR destination_account_id=$#{index}) OR "
            query_params << "#{wallet}"
            query_index = index
          end
          query = "(#{filter[0...-4]})"
          i = query_index + 1
        end
        if p.first == :name_wallets
          name_index = 1
          names = ''
          p.second.each.with_index(i) do |wallet, index|
            names = names + "(source_account_id=$#{index} OR destination_account_id=$#{index}) OR "
            query_params << "#{wallet}"
            name_index = index
          end
          query += " AND (#{names[0...-4]})"
          i = name_index + 1
        end

        if p.first == :email_wallets
            query += " AND (source_account_id=$#{i} OR destination_account_id=$#{i})"
            query_params << "#{p.second}"
            i = i + 1
        end
        if p.first == :phone_wallets
            query += " AND (source_account_id=$#{i} OR destination_account_id=$#{i})"
            query_params << "#{p.second}"
            i = i + 1
        end
        if p.first == :amount
          query += " AND amount=$#{i}"
          query_params << p.second
          i = i + 1
        end
        if p.first == :date
            query += " AND (timestamp >= $#{i} AND timestamp <= $#{i+1})"
            query_params << p.second[:first_date]
            query_params << p.second[:second_date]
            i = i + 2
        end
        if p.first == :last4
            if query.empty?
              query = "(tags.last4=$#{i} OR tags.previous_issue.tags.card.last4=$#{i})"
              query_params << p.second
              i = i + 1
            else
              query += " AND (tags.last4=$#{i} OR tags.previous_issue.tags.card.last4=$#{i})"
              query_params << p.second
              i = i + 1
            end
        end
        if p.first == :type
          if query.empty?
            result = transaction_type(i,p.second)
            query = "(#{result.first[0...-4]})"
            query_params = result.second
            i = i + query_params.count
          else
            result = transaction_type(i,p.second)
            query += " AND (#{result.first[0...-4]})"
            query_params = query_params + result.second
            i = i + query_params.count
          end
        end
        if p.first == :block_id
          if query.empty?
            query = "(id=$#{i})"
            query_params << p.second
            i = i + 1
          else
            query += " AND (id=$#{i})"
            query_params << p.second
            i = i + 1
          end
        end
      end
      return [query,query_params]
    end

    def city_state(index,type)
      filter = ''
      filter_abc = []
      type.each.with_index(index) do |t, index|
        filter = filter + "(source_account_id=$#{index} OR destination_account_id=$#{index}) OR "
        filter_abc << "#{t}"
      end
      return [filter,filter_abc]
    end

    def transaction_type(index,type)
      filter = ''
      filter_abc = []
      type.each.with_index(index) do |t, index|
        if t == "sale_tip" || t == "sale_giftcard" || t == "debit_charge" || t == "virtual_terminal_sale" || t == "virtual_terminal_sale_api" || t == "QR_Scan" || t == "cbk_lost" || t == "cbk_lost_retire" || t == "cbk_won" || t == "cbk_dispute_accepted" || t == "sale_issue_api" || t == "cbk_fee" || t == "credit_card"
          filter = filter + "tags.sub_type=$#{index} OR "
        else
          filter = filter + "tags.type=$#{index} OR "
        end
        filter_abc << "#{t}"
      end
      return [filter,filter_abc]
    end

    def gateway_type(index,type)
      filter_gatway = ''
      filter_params = []
      type.each.with_index(index) do |t, index|
        filter_gatway = filter_gatway + "tags.source=$#{index} OR "
        filter_params << "#{t}"
      end
      [filter_gatway,filter_params]
    end

    def record_error(id, reason, destination = nil, user_info = nil,error_details = nil, crash_from = nil)
      wallet = Wallet.find_by_id(id)
      user = wallet.try(:users).try(:first) if wallet.present?
      message = {success: false, backtrace: reason.backtrace.first(15), error: {reason: reason, source_wallet: id, destination_wallet: destination, user_info: user_info}, seq_error_detail: error_details, crash_from: crash_from}
      activity = ActivityLog.new
      activity.url = RequestInfo.try(:request_url)
      activity.host = RequestInfo.try(:req_host)
      activity.browser = RequestInfo.try(:browser_req)
      if RequestInfo.try(:host_name).present?
        activity.ip_address = RequestInfo.host_name
      end
      activity.controller = RequestInfo.try(:controller)
      activity.action = RequestInfo.try(:action)
      activity.respond_with = reason.present? ? message : ''
      unless user.nil?
        activity.user = user
      end
      activity.save
      activity.sequence!
    end

    def import_transactions(file_name, chunk_size = nil)
      begin
        options = {
          chunk_size:  chunk_size || 10000
        }
        count = options[:chunk_size]
        csv_path  = file_name
        csv_file  = open(csv_path,'r')
        SmarterCSV.process(csv_file, options) do |chunck|
          block_transactions = []
          chunck.each do |obj|
            tx_object = OpenStruct.new(obj)
            data = parse_single_block(tx_object)
            block_transactions << BlockTransaction.new(
                sender_wallet_id: data[:sender].to_i,
                receiver_wallet_id: data[:receiver].to_i,
                amount_in_cents: data[:amount],
                tx_amount: data[:transaction_amount],
                fee_in_cents: data[:fee],
                tip_cents: data[:tip],
                hold_money_cents: data[:hold_money],
                net_fee: data[:net_fee].to_f,
                total_net: data[:net_amount].to_f,
                privacy_fee: data[:privacy_fee],
                iso_fee: data[:iso_fee],
                agent_fee: data[:agent_fee],
                partner_fee: data[:partner_fee],
                gbox_fee: data[:gbox_fee],
                affiliate_fee: data[:affiliate_fee],
                action: data[:action].parameterize.underscore.to_sym,
                sequence_id: data[:sequence_id],
                seq_block_id: data[:seq_block_id],
                timestamp: data[:timestamp].to_datetime,
                main_type: data[:type],
                sub_type: data[:sub_type],
                tx_type: data[:tx_type],
                tx_sub_type: data[:tx_sub_type],
                tags: data[:tags],
                last4: data[:last4],
                first6: data[:first6],
                card_id: data[:card_id],
                charge_id: data[:charge_id],
                gateway: data[:gateway],
                api_type: data[:api_type],
                ip: data[:ip],
                position: data[:position],
                status: :approved,
                location_dba_name: data[:location_dba_name],
                merchant_name: data[:merchant_name],
                source_name: data[:source_name],
                destination_name: data[:destination_name]
            )
          end
          if block_transactions.present? && block_transactions.count > 0
            BlockTransaction.import block_transactions, on_duplicate_key_update: {
                conflict_target: [:sequence_id],
                columns:
                    [
                        :sender_wallet_id,
                        :receiver_wallet_id,
                        :amount_in_cents,
                        :tx_amount,
                        :fee_in_cents,
                        :tip_cents,
                        :hold_money_cents,
                        :net_fee,
                        :total_net,
                        :privacy_fee,
                        :iso_fee,
                        :agent_fee,
                        :partner_fee,
                        :gbox_fee,
                        :affiliate_fee,
                        :action,
                        :sequence_id,
                        :seq_block_id,
                        :timestamp,
                        :main_type,
                        :sub_type,
                        :tx_type,
                        :tx_sub_type,
                        :tags,
                        :last4,
                        :first6,
                        :card_id,
                        :charge_id,
                        :gateway,
                        :api_type,
                        :ip,
                        :position,
                        :status,
                        :location_dba_name,
                        :merchant_name,
                        :source_name,
                        :destination_name,
                        :updated_at,
                        :created_at
                    ]
            }
          end
        end
      rescue => exc
        p exc.message
        p exc.backtrace
      end
    end
  end
end

module PaymentInstrument
  Card = 'card'
  Cash = 'cash'
  Account = 'bank_account'
end
