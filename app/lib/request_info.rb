module RequestInfo
  class << self;
    attr_accessor :var, :browser_req, :request_url, :request_server_name, :request_ip_address, :req_host, :action, :current_user, :controller, :host_name,:ref_id;

    def current_request(request, user)
      require 'resolv'
      RequestInfo.browser_req = request.env['HTTP_USER_AGENT']
      RequestInfo.request_url = request.url
      RequestInfo.request_ip_address = request.remote_ip || request.ip
      RequestInfo.action = request.params[:action] == I18n.t("event_types.transfer_post") ? I18n.t("event_types.account_transfer") : request.params[:action]
      RequestInfo.current_user = user
      RequestInfo.controller = request.params[:controller]
      RequestInfo.ref_id = request.params[:RefId] || request.params[:ref_id]
      RequestInfo.req_host = request.env["SERVER_NAME"]
      begin
        remote_ip = Resolv.getname(request.remote_ip)
      rescue Resolv::ResolvError => exc
        remote_ip = request.remote_ip
      end
      RequestInfo.host_name = remote_ip
    end
  end

end