class ErrorMessageCache
  @timeout = 60 * 60 * 3 #5 minutes
  @@errorMessages = {}

  ErrorMessage.all.each do |error_message|
    @@errorMessages[error_message.error_code&.to_s] = {
        data: error_message,
        updated_at: Time.now
    }
  end

  class << self
    # To store & reterive data From Cache Constant

    def get(params)
      begin
        result = []
        if @@errorMessages.present? && @@errorMessages[params[:error_code]].present?
          if params[:error_code].present? && Time.now > @@errorMessages[params[:error_code]][:updated_at] + @timeout
            puts "Error Message cache timeout"
            data = ErrorMessage.find_by_sql("SELECT * FROM error_messages WHERE error_messages.error_code = '#{params[:error_code]}'").last
            @@errorMessages[params[:error_code]] = {
                data: data,
                updated_at: Time.now
            }
          end
        end
        params.values.each do |v|
          result << @@errorMessages[v][:data] if @@errorMessages[v].present?
        end
        if result.size <= 0 && params[:error_message].present?
          result = result.concat(list.select{|obj| (obj[:error_message]&.downcase)&.match?((params[:error_message]&.strip)&.downcase || "")})
        end
        return result.uniq
      rescue Exception => e
        puts 'Error Exception'
        puts e
      end
    end

    # To Update Cache Constant when new Error Code is Save or Upate
    def update(code,data)
      code = code&.to_s
      puts "app config cache update For Error Code \'#{code}\'..........."
      @@errorMessages[code] = {
          data: data,
          updated_at: Time.now
      }
      return @@errorMessages[code][:data]
    end

    def refresh_cache
      ErrorMessage.all.each do |error_message|
        @@errorMessages[error_message.error_code&.to_s] = {
            data: error_message,
            updated_at: Time.now
        }
      end
    end

    def list
      @@errorMessages.values.map{|obj| obj[:data]}
    end
  end

  private_class_method :new

end