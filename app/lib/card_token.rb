require 'net/http'
class CardToken
  include ApplicationHelper
  attr_accessor :token, :cvv, :number, :exp_date, :name, :amount, :payeezy, :brand, :stripe_api

  def initialize(name = nil, number = nil, exp_date = nil, amount = nil)
    @name = name
    @number = number
    @exp_date = exp_date
    @amount = amount
    @brand = card_brand(number)
    @payeezy = nil

    app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    if app_config.present?
      if app_config.stringValue == "stripe"
        publishable_key = ENV['stripe_publishable_key']
        secret_key = ENV['stripe_secret_key']
      elsif app_config.stringValue == "stripe_pop"
        publishable_key = ENV['stripe_pop_publishable_key']
        secret_key = ENV['stripe_pop_secret_key']
      elsif app_config.stringValue == "stripe_pop_2"
        publishable_key = ENV['stripe_pop_2_publishable_key']
        secret_key = ENV['stripe_pop_2_secret_key']
      end
    end
    @stripe_api = secret_key
  end

  def create_all(name, number, expiry, cvv, customer, user = nil, key = nil)
    {
        :payeezy => payeezy(name, number, expiry, cvv),
        :stripe => stripe(name, number, expiry, cvv, customer, user, key),
        # :elavon => nil,
        # :elavon => elavon(name, number, expiry, cvv),
        :bridge_pay => bridge_pay_token(number, expiry, cvv)
    }
  end

  def bridge_pay_token(number, expiry, cvv)
    brand = card_brand(number)
    # length = 16
    # length = 14 if brand == 'Diners Club'
    # length = 15 if brand == 'American Express'
    raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) unless brand.present?

    post = {}
    post[:ClientIdentifier] = "SOAP"
    post[:TransactionID] = "123"
    post[:RequestType] = "001"
    # post[:RequestDateTime] = Time.now.to_i
    # ENV["BRIDGE_PAY_USER"] = 'gbp078test'
    # ENV["BRIDGE_PAY_PASS"] = '57!sE@3Fm'
    post[:User] = ENV["BRIDGE_PAY_USER"]
    post[:Password] = ENV["BRIDGE_PAY_PASS"]

    message = {}
    message[:PaymentAccountNumber] = number
    message[:ExpirationDate] = expiry
    post[:requestMessage] = message

    response = bridge_pay_data(post)

    parsed_response = parse_xml(response.body)
    decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
    json = Hash.from_xml(decode).to_json
    json = JSON.parse(json)
    if response.is_a?(Net::HTTPSuccess)
      if json["ErrorResponse"].present?
        raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
      else
        return bridge_pay_cvv(json["GetToken"]["responseMessage"]["Token"], cvv)
      end
    else
      raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
    end
  end

  def bridge_pay_cvv(token, cvv)
    raise StandardError.new('No Token Available') if token.nil?

    post = {}
    post[:ClientIdentifier] = "SOAP"
    post[:TransactionID] = "123"
    post[:RequestType] = "002"
    # post[:RequestDateTime] = Time.now.to_i
    post[:User] = ENV["BRIDGE_PAY_USER"]
    post[:Password] = ENV["BRIDGE_PAY_PASS"]

    message = {}
    message[:Token] = token
    message[:SecurityCode] = cvv
    post[:requestMessage] = message

    response = bridge_pay_data(post)

    parsed_response = parse_xml(response.body)
    decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
    json = Hash.from_xml(decode).to_json
    json = JSON.parse(json)

    if response.is_a?(Net::HTTPSuccess)
      if json["ErrorResponse"].present?
        raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
      else
        return token
        # return json["WalletSecurityCode"]["ResponseDescription"]
      end
    else
      raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
    end
  end

  # def charge_ACH_bridge_pay(token, amount, name, expiry)
  # def charge_ACH_bridge_pay(amount, name, bank_account_number, routing_num, account_type)

  #   amount = SequenceLib.cents(amount)

  #     post = {}
  #     post[:ClientIdentifier] = "SOAP"
  #     post[:TransactionID] = "259691704"
  #     post[:RequestType] =  '004' #'011' # "004"
  #     post[:RequestDateTime] = Date.today
  #     # ENV["BRIDGE_PAY_USER"] = 'gbp078test'
  #     # ENV["BRIDGE_PAY_PASS"] = '57!sE@3Fm'
  #     post[:User] = ENV["BRIDGE_PAY_USER"]
  #     post[:Password] = ENV["BRIDGE_PAY_PASS"]

  #     message = {}
  #     message[:BankAccountNum] = '4099999992' #bank_account_number || '4099999992'
  #     message[:RoutingNum] = '021000021' #routing_num || '021000021'
  #     message[:AcctType] = 'C' #account_type || 'Checking'
  #     # message[:TransIndustryType] = ''
  #     message[:DUKPT] = '1243232'
  #     # message[:PINCode] = ''
  #     message[:TransactionType] = "sale"
  #     message[:TransIndustryType] = "CCD"
  #     message[:HolderType] = "O"
  #     # message[:TransactionMode] = 'N'
  #     # message[:TransCatCode] = "B"
  #     # message[:AcctType] = "R"
  #     # message[:TransactionMode] = "P"
  #     # message[:TransCatCode] = "B"
  #     # message[:Token] = '11110000000000279992'
  #     message[:Amount] = 100 #amount.to_i
  #     message[:AccountHolderName] = 'Testing Alex' #name
  #     # message[:ExpirationDate] = expiry
  #     # message[:InvoiceNum] = 'INV-1532982296'
  #     # message[:AccountStreet] = '123 Main Street'
  #     # message[:AccountZip] = '28540'
  #     message[:MerchantCode] = "4985000"
  #     message[:MerchantAccountCode] = "4985001"
  #     # message[:CustomerAccountCode] = "4985001"
  #     post[:requestMessage] = message

  #     response = bridge_pay_data(post)

  #     parsed_response = parse_xml(response.body)
  #     decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
  #     json = Hash.from_xml(decode).to_json
  #     json = JSON.parse(json)

  #   if response.is_a?(Net::HTTPSuccess)
  #     if json["ErrorResponse"].present?
  #       raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
  #     else
  #       if json["Auth"]["ResponseCode"] != "00000"
  #     message[:MerchantAccountCode] = "4985001"          raise StandardError.new(json["Auth"]["ResponseDescription"])
  #       else
  #         return json["Auth"]["responseMessage"]["GatewayTransID"]
  #       end
  #       # return json["WalletSecurityCode"]["ResponseDescription"]
  #     end
  #   else
  #     puts 'JSON Response', parsed_response
  #     raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
  #   end
  # end

  def charge_bridge_pay_merchant(token, amount, name, expiry)

    amount = SequenceLib.cents(amount)

    post = {}
    post[:ClientIdentifier] = "SOAP"
    post[:TransactionID] = "123"
    post[:RequestType] = "004"
    # post[:RequestDateTime] = Time.now.to_i
    post[:User] = ENV["BRIDGE_PAY_USER"]
    post[:Password] = ENV["BRIDGE_PAY_PASS"]

    message = {}
    message[:TransactionType] = "sale"
    message[:TransIndustryType] = "EC"
    message[:TransCatCode] = "B"
    message[:AcctType] = "R"
    message[:TransactionMode] = "P"
    message[:Token] = token
    message[:Amount] = amount.to_i
    message[:AccountHolderName] = name
    message[:ExpirationDate] = expiry
    message[:InvoiceNum] = 'INV-1532982296'
    message[:AccountStreet] = '123 Main Street'
    message[:AccountZip] = '28540'
    message[:MerchantCode] = "4985000"
    message[:MerchantAccountCode] = "4985001"
    message[:SoftwareVendor] = "USAG - Greenboxpos QuickCard v1.36"
    post[:requestMessage] = message

    response = bridge_pay_data(post)

    parsed_response = parse_xml(response.body)
    decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
    json = Hash.from_xml(decode).to_json
    json = JSON.parse(json)

    if response.is_a?(Net::HTTPSuccess)
      if json["ErrorResponse"].present?
        raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
      else
        if json["Auth"]["ResponseCode"] != "00000"
          return nil
          # raise StandardError.new(json["Auth"]["ResponseDescription"])
        else
          return json["Auth"]["responseMessage"]["GatewayTransID"]
        end
        # return json["WalletSecurityCode"]["ResponseDescription"]
      end
    else
      raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
    end
  end

  def charge_bridge_pay(token, amount, name, expiry)

    amount = SequenceLib.cents(amount)

    post = {}
    post[:ClientIdentifier] = "SOAP"
    post[:TransactionID] = "123"
    post[:RequestType] = "004"
    # post[:RequestDateTime] = Time.now.to_i
    post[:User] = ENV["BRIDGE_PAY_USER"]
    post[:Password] = ENV["BRIDGE_PAY_PASS"]

    message = {}
    message[:TransactionType] = "sale"
    message[:TransIndustryType] = "EC"
    message[:TransCatCode] = "B"
    message[:AcctType] = "R"
    message[:TransactionMode] = "P"
    message[:Token] = token
    message[:Amount] = amount.to_i
    message[:AccountHolderName] = name
    message[:ExpirationDate] = expiry
    message[:InvoiceNum] = 'INV-1532982296'
    message[:AccountStreet] = '123 Main Street'
    message[:AccountZip] = '28540'
    message[:MerchantCode] = "4985000"
    message[:MerchantAccountCode] = "4985001"
    message[:SoftwareVendor] = "USAG - Greenboxpos QuickCard v1.36"
    post[:requestMessage] = message

    response = bridge_pay_data(post)

    parsed_response = parse_xml(response.body)
    decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
    json = Hash.from_xml(decode).to_json
    json = JSON.parse(json)

    if response.is_a?(Net::HTTPSuccess)
      if json["ErrorResponse"].present?
        raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
      else
        if json["Auth"]["ResponseCode"] != "00000"
          raise StandardError.new(json["Auth"]["ResponseDescription"])
        else
          return json["Auth"]["responseMessage"]["GatewayTransID"]
        end
        # return json["WalletSecurityCode"]["ResponseDescription"]
      end
    else
      raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
    end
  end

  def refund_bridge_pay(token, amount)

    amount = SequenceLib.cents(amount)

    post = {}
    post[:ClientIdentifier] = "SOAP"
    post[:TransactionID] = "123"
    post[:RequestType] = "012"
    # post[:RequestDateTime] = Time.now.to_i
    post[:User] = ENV["BRIDGE_PAY_USER"]
    post[:Password] = ENV["BRIDGE_PAY_PASS"]

    message = {}
    message[:Amount] = amount
    message[:ReferenceNumber] = token
    message[:TransactionType] = "refund"
    message[:TransactionCode] = "123"
    post[:requestMessage] = message

    response = bridge_pay_data(post)

    parsed_response = parse_xml(response.body)
    decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
    json = Hash.from_xml(decode).to_json
    json = JSON.parse(json)

    if response.is_a?(Net::HTTPSuccess)
      if json["ErrorResponse"].present?
        raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
      else
        if json["VoidRefund"]["ResponseCode"] != "00000"
          raise StandardError.new(json["VoidRefund"]["ResponseDescription"])
        else
          return json["VoidRefund"]["ResponseDescription"]
        end
        # return json["WalletSecurityCode"]["ResponseDescription"]
      end
    else
      raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
    end
  end

  def payeezy(name, number, expiry, cvv)
    brand = card_brand(number)
    # length = 16
    # length = 14 if brand == 'Diners Club'
    # length = 15 if brand == 'American Express'
    raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) unless brand.present?

    params = {}
    params[:type] = 'FDToken'
    params[:auth] = false
    params[:ta_token] = ENV['TA_TOKEN']

    card = {}
    card[:type] = brand
    card[:cardholder_name] = name
    card[:card_number] = number
    card[:exp_date] = expiry
    card[:cvv] = cvv if cvv.present?

    params[:credit_card] = card
    body = params.to_json

    uri = URI.parse("#{ENV['PAYEEZY_API_URL']}/transactions/tokens")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER

    request = Net::HTTP::Post.new(uri.request_uri)
    http_headers(request, body)
    request.body = body

    response = http.request(request)
    parsed_response = parse(response.body)
    if response.is_a?(Net::HTTPSuccess)
      if parsed_response['status'] == 'success'
        token = parsed_response['token']
        return token['value']
      else
        raise StandardError.new('Payeezy Card Tokenization Failed')
      end
    else

      response=JSON.parse(response.body)
      error=response["Error"]["messages"].first["description"]
      raise StandardError.new "#{error}"

      # raise StandardError.new("Payeezy #{parsed_response['Error']['messages'][0]['description']}")
    end
    return nil
  end

  def stripe(name, number, expiry, cvv, customer, user = nil, key = nil)
    Stripe.api_key = key if key.present?
    Stripe.api_key = @stripe_api if key.nil?

    brand = card_brand(number)
    # length = 16
    # length = 14 if brand == 'Diners Club'
    # length = 15 if brand == 'American Express'
    raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) unless brand.present?

    exp_month, exp_year = expiry[0...2], expiry[2...4]

    if customer.blank?
      customer = get_stripe_customer(user)
    else
      customer = customer
    end

    token = Stripe::Token.create(
        :card => {
            :number => number,
            :exp_month => exp_month,
            :exp_year => "20#{exp_year}",
            :cvc => cvv,
            :customer => customer
        }
    )
    stripe_customer = Stripe::Customer.retrieve(customer)
    stripe_customer.sources.create(source: token)
    raise StandardError.new('Stripe Card Tokenization Failed') if token.nil? || !token.card
    return {card_id: token.card.id, token: token.id, error: nil}
  end

  def create_stripe_customer(user)
    app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
    if app_config.stringValue == "stripe"
      if user.stripe_customer_id.nil?
        customer = Stripe::Customer.create({:email => user.email}, {api_key: ENV["stripe_secret_key"]})
        user.stripe_customer_id = customer.id if customer.present?
      end
      user.save
      return user.stripe_customer_id
    elsif app_config.stringValue == "stripe_pop"
      if user.second_stripe_id.nil?
        customer = Stripe::Customer.create({:email => user.email}, {api_key: ENV["stripe_pop_secret_key"]})
        user.second_stripe_id = customer.id if customer.present?
      end
      user.save
      return user.second_stripe_id
    elsif app_config.stringValue == "stripe_pop_2"
      if user.third_stripe_id.nil?
        customer = Stripe::Customer.create({:email => user.email}, {api_key: ENV["stripe_pop_2_secret_key"]})
        user.third_stripe_id = customer.id if customer.present?
      end
      user.save
      return user.third_stripe_id
    end
  end

  def get_stripe_customer(user)
    if user.second_stripe_id.nil?
      customer2 = Stripe::Customer.create({:email => user.email},{api_key: ENV["stripe_pop_secret_key"]})
      user.second_stripe_id = customer2.id if customer2.present?
    end
    user.save
    return user.second_stripe_id
  end
 # this function is not used in charge mobile api
  def get_card_source(card)
    app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
    if app_config.stringValue == "stripe"
      return card.stripe
    elsif app_config.stringValue == "stripe_pop"
      return card.stripe2
    end
  end



  def stripe_merchant(name, number, expiry, cvv, customer, user = nil, key = nil)
    Stripe.api_key = key if key.present?
    Stripe.api_key = @stripe_api if key.nil?
    brand = card_brand(number)
    # length = 16
    # length = 14 if brand == 'Diners Club'
    # length = 15 if brand == 'American Express'
    raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) unless brand.present?

    exp_month, exp_year = expiry[0...2], expiry[2...4]
    if customer.nil?
      customer = get_stripe_customer(user)
    else
      customer = customer
    end
    begin
      token = Stripe::Token.create(
          :card => {
              :number => number,
              :exp_month => exp_month,
              :exp_year => "20#{exp_year}",
              :cvc => cvv,
              :customer => customer
      })
      stripe_customer = Stripe::Customer.retrieve(customer)
      stripe_customer.sources.create(source: token)
    rescue Stripe::CardError, Stripe::InvalidRequestError => e
      return {card_id: nil,token: nil,  error: e}
    end
    raise StandardError.new('Stripe Card Tokenization Failed') if token.nil? || !token.card
    return {card_id: token.card.id, token: token.id, error: nil}
  end

  def stripe_charge(amount, customer, card_token, key = nil)
    Stripe.api_key = key if key.present?
    Stripe.api_key = @stripe_api if key.nil?
    begin

      charge = Stripe::Charge.create({
                                         :amount => (dollars_to_cents(amount).to_i),
                                         :currency => "usd",
                                         :customer => customer,
                                         :source => card_token
                                     })
    rescue Stripe::CardError, Stripe::InvalidRequestError => e
      return nil
    end
    return charge
  end

  def stripe_refund(charge_id, amount, payment_gateway = nil)
    if payment_gateway.present?
      key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
    end
    Stripe.api_key = key if key.present?
    Stripe.api_key = @stripe_api if key.blank?
    refund = Stripe::Refund.create({
                                       charge: charge_id,
                                       amount: dollars_to_cents(amount.to_f)
                                   })
    return refund
  end

  def elavon(name, number, expiry, cvv)
    brand = card_brand(number)
    # length = 16
    # length = 14 if brand == 'Diners Club'
    # length = 15 if brand == 'American Express'
    raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) unless brand.present?

    parsed_hash = {}
    url = "#{ENV['CONVERGE_URL']}?ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{number}&ssl_exp_date=#{expiry}&ssl_transaction_type=ccgettoken&ssl_cvv2cvc2_indicator=1&cvv=#{cvv}&ssl_verify=N"
    response = HTTParty.get(url)
    if response.success?
      response.parsed_response.split("\n").map do |item|
        arr = item.split("=")
        parsed_hash[arr[0]] = arr[1]
      end
      if parsed_hash["errorCode"].present?
        message = parsed_hash["errorName"]
        raise StandardError.new(message)
      else
        return parsed_hash["ssl_token"]
      end
    else
      raise StandardError.new('Error getting Elavon Token!')
    end
  end

  def charge_elavon_raw(amount, number, exp_date, cvv)
    parsed_hash = {}
    url = "#{ENV['CONVERGE_URL']}?ssl_test_mode=false&ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_transaction_type=ccsale&ssl_entry_mode=02&ssl_card_number=#{number}&ssl_exp_date=#{exp_date}&ssl_amount=#{amount}&ssl_cvv2cvc2=#{cvv}"
    response = HTTParty.get(url)
    if response.success?
      response.parsed_response.split("\n").map do |item|
        arr = item.split("=")
        parsed_hash[arr[0]] = arr[1]
      end
      message = parsed_hash["ssl_result_message"]
      raise StandardError.new(message) if parsed_hash["ssl_result_message"] != "APPROVAL"
    end
  end

  def charge_elavon(token, amount, exp_date, email)
    parsed_hash = {}
    url = "#{ENV['CONVERGE_URL']}?ssl_test_mode=false&ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_exp_date=#{exp_date}&ssl_email=#{email}&ssl_amount=#{amount}&ssl_transaction_type=ccsale&ssl_token=#{token}"
    response = HTTParty.get(url)
    if response.success?
      response.parsed_response.split("\n").map do |item|
        arr = item.split("=")
        parsed_hash[arr[0]] = arr[1]
      end
      message = parsed_hash["ssl_result_message"]
      raise StandardError.new(message) if parsed_hash["ssl_result_message"] != "APPROVAL"
    end
  end

  def charge_direct_post_ach
      #working on it till its finalized
     direct_post_ach_transaction = DirectPostHelper::TransactionACH.new
     direct_post_ach_transaction.setLogin('demo','password')
     direct_post_ach_transaction.setAchTransactionWithVaultId('sale','','347736111','300')
     response = direct_post_ach_transaction.do_transaction
     if response['response'] == '1'
      # need to save response['transactionid']
      # need to save response['customer_vault_id'] if you didn't get any customer vault id before
       return "Successfully Transfered!"
     else
      # type attribute and its these values 'debit','auth','validate', and 'offline' give error of Invalid Transaction Type REFID:1186875532
       return response["responsetext"]
     end
  end
  # -----------------------------------Payzee Refund-------------------------------------------------------------
  def refund_payzee(transaction_id,transaction_tag,amount)
    params = {}
    params[:merchant_ref] = ENV['PAYEEZY_MERCHANT_ID']
    params[:transaction_type] = 'refund'
    params[:method]='credit_card'
    params[:amount] = amount
    params[:currency_code] = 'USD'
    params[:transaction_tag]=transaction_tag
    body = params.to_json
    uri = URI.parse("#{ENV['PAYEEZY_API_URL']}/#{ENV['PAYEEZY_TRANSACTION_URL']}/#{transaction_id}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Post.new(uri.request_uri)
    http_headers(request, body)
    request.body = body
    response = http.request(request)
    parsed_response = parse(response.body)
    if response.is_a?(Net::HTTPSuccess)
      if parsed_response['transaction_status'] == "approved" && parsed_response['bank_message'] == "Approved"
        responseData={
            "success" => true,
            "message" => 'Refunded Successfully',
            "amount" => amount,
            "transaction_status" => "Approved",
            "bank_messsage" => 'Approved',
            "transaction_tag" => parsed_response['transaction_tag']
        }
        return responseData
      elsif parsed_response['transaction_status']== 'declined' && parsed_response['bank_message'] != 'Approved'
        error= parsed_response['bank_message']
        raise StandardError.new(error)
      else
        error=parsed_response["Error"]["messages"].first["description"]
        raise StandardError.new(error)
      end
    else
      response=JSON.parse(response.body)
      error=response["Error"]["messages"]
      raise StandardError.new(error)
      # raise StandardError.new("Payeezy #{parsed_response['Error']['messages'][0]['description']}")
    end
  end
  # -------------------------------------Payzee Refund End-----------------------------------------------
  def charge_payeezy(card, amount, pay_token = nil)
    pay_token = card.payeezy if pay_token.nil?
    params = {}
    params[:merchant_ref] = ENV['PAYEEZY_MERCHANT_ID']
    params[:transaction_type] = ENV['PAYEEZY_TRANSACTION_TYPE']
    params[:method] = 'token'
    params[:amount] = (amount.to_f * 100).to_i
    params[:currency_code] = 'USD'

    token = {}
    token[:token_type] = 'FDToken'
    data = {}
    data[:type] = card.brand
    data[:value] = pay_token
    data[:cardholder_name] = card.name
    data[:exp_date] = card.exp_date
    token[:token_data] = data
    params[:token] = token
    body = params.to_json
# "https://api.payeezy.com/v1/transactions"
    uri = URI.parse("#{ENV['PAYEEZY_API_URL']}/#{ENV['PAYEEZY_TRANSACTION_URL']}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER


    request = Net::HTTP::Post.new(uri.request_uri)
    http_headers(request, body)
    request.body = body
    response = http.request(request)
    parsed_response = parse(response.body)

    if response.is_a?(Net::HTTPSuccess)
      if parsed_response['transaction_status'] == 'approved' && parsed_response['bank_message'] == 'Approved'
        return "#{parsed_response['transaction_id']}-#{parsed_response['transaction_tag']}"
      elsif parsed_response['transaction_status'] == 'declined' && parsed_response['bank_message'] != 'Approved'
           error = parsed_response['bank_message']
           raise StandardError.new(error)
      else
        error = parsed_response["Error"]["messages"].first["description"]
        raise StandardError.new(error)
      end
    else
      response=JSON.parse(response.body)
      error=response["Error"]["messages"]
      raise StandardError.new(error)
      # raise StandardError.new("Payeezy #{parsed_response['Error']['messages'][0]['description']}")
    end
    return nil
  end

  def charge_payeezy_merchant(card, amount, pay_token = nil)
    pay_token = card.payeezy if pay_token.nil?
    params = {}
    params[:merchant_ref] = ENV['PAYEEZY_MERCHANT_ID']
    params[:transaction_type] = ENV['PAYEEZY_TRANSACTION_TYPE']
    params[:method] = 'token'
    params[:amount] = (amount.to_f * 100).to_i
    params[:currency_code] = 'USD'

    token = {}
    token[:token_type] = 'FDToken'
    data = {}
    data[:type] = card.brand
    data[:value] = pay_token
    data[:cardholder_name] = card.name
    data[:exp_date] = card.exp_date
    token[:token_data] = data
    params[:token] = token
    body = params.to_json
# "https://api.payeezy.com/v1/transactions"
    uri = URI.parse("#{ENV['PAYEEZY_API_URL']}/#{ENV['PAYEEZY_TRANSACTION_URL']}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER


    request = Net::HTTP::Post.new(uri.request_uri)
    http_headers(request, body)
    request.body = body
    response = http.request(request)
    parsed_response = parse(response.body)

    if response.is_a?(Net::HTTPSuccess)
      if parsed_response['transaction_status'] == 'approved' && parsed_response['bank_message'] == 'Approved'
        return "#{parsed_response['transaction_id']}-#{parsed_response['transaction_tag']}"
      elsif parsed_response['transaction_status'] == 'declined' && parsed_response['bank_message'] != 'Approved'
        error = parsed_response['bank_message']
        return nil
        # raise StandardError.new(error)
      else
        error = parsed_response["Error"]["messages"].first["description"]
        raise StandardError.new(error)
      end
    else
      response=JSON.parse(response.body)
      error=response["Error"]["messages"]
      raise StandardError.new(error)
      # raise StandardError.new("Payeezy #{parsed_response['Error']['messages'][0]['description']}")
    end
    return nil
  end

  def charge_payeezy_raw(amount,name,number,exp_date,cvv)
    params = {}
    params[:merchant_ref] = ENV['PAYEEZY_MERCHANT_ID']# || '3176752955'
    params[:transaction_type] = ENV['PAYEEZY_TRANSACTION_TYPE']# || 'authorize'
    params[:method] = 'credit_card'
    params[:amount] = (amount.to_f * 100).to_i
    params[:currency_code] = 'USD'

    # token = {}
    # token[:token_type] = 'FDToken'
    data = {}
    tokenization = CardToken.new
    data[:type] = tokenization.card_brand(number)
    # data[:value] = pay_token
    data[:cardholder_name] = name
    data[:card_number] = number
    data[:cvv] = cvv
    data[:exp_date] = exp_date
    params[:credit_card] = data
    # params[:token] = token
    body = params.to_json
   # "https://api.payeezy.com/v1/transactions"
    # uri = URI.parse("https://api-cert.payeezy.com/v1/transactions")
    uri = URI.parse("#{ENV['PAYEEZY_API_URL']}/#{ENV['PAYEEZY_TRANSACTION_URL']}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER


    request = Net::HTTP::Post.new(uri.request_uri)
    http_headers(request, body)
    request.body = body
    response = http.request(request)
    parsed_response = parse(response.body)
    if response.is_a?(Net::HTTPSuccess)
      if parsed_response['transaction_status'] == 'approved' && parsed_response['bank_message'] == 'Approved'
        return "#{parsed_response['transaction_id']}-#{parsed_response['transaction_tag']}"
      elsif parsed_response['transaction_status']== 'declined' && parsed_response['bank_message'] != 'Approved'
           error= parsed_response['bank_message']
           raise StandardError.new(error)
      else
        error=parsed_response["Error"]["messages"].first["description"]
        raise StandardError.new(error)
      end
    else

      response=JSON.parse(response.body)
      error=response["Error"]["messages"]
      raise StandardError.new(error)
      # raise StandardError.new("Payeezy #{parsed_response['Error']['messages'][0]['description']}")
    end
    return nil
  end

  def charge_stripe_raw(amount,number,exp_month,exp_year,cvc)
    charge = Stripe::Charge.create({
       :amount => (amount * 100).to_i,
       :currency => "usd",
       :description => "KIOSK Card Charge",
       :source => {
        :card => {
          :number => number,
          :exp_month => exp_month,
          :exp_year => exp_year,
          :cvc => cvc
        }
      }
    })
    if charge.present?
      raise StandardError.new("#{charge}")
    else
      raise StandardError.new('Your transaction is declined!')
    end
  end

  def charge_stripe(token, amount)
    charge = nil
    begin
      charge = Stripe::Charge.create({
                                         :amount => (amount.to_f * 100).to_i,
                                         :currency => "usd",
                                         :description => "KIOSK Card Charge",
                                         :source => token
                                     })
      if charge.paid != true && charge.status != 'succeeded'
        raise StandardError.new 'Stripe rejected Card Charge!'
      end
    rescue => ex
      raise StandardError.new, ex.message
    end
  end

  def charge
    config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    if self.number.present? && self.exp_date.present? && self.name.present?
      self.chargeWithCardDetails(config.stringValue)
    else
      raise StandardError.new 'Incomplete Card Details for Transaction!'
    end
    return true
  end

  def chargeStripeWithCard
    exp_month, exp_year = self.exp_date[0...2], self.exp_date[2...4]
    token = Stripe::Token.create(
        :card => {
          :number => self.number,
          :exp_month => exp_month,
          :exp_year => "20#{exp_year}"
        }
    )
    charge_stripe(token, self.amount)
  end

  def chargeWithCardToken

  end

  def chargeWithCardDetails(type)
    if type == 'stripe'
      chargeStripeWithCard
    elsif type == 'payeezy'
      token = payeezy(self.name, self.number, self.exp_date, nil)
      if token.present?
        # self.payeezy = token
        self.charge_payeezy(self, self.amount, token)
      end
    elsif type == 'converge'
      token = elavon(self.name, self.number, self.exp_date, nil)
      if token.present?
        charge_elavon(token, self.amount, self.exp_date, '')
      end
    end
  end

  def card_brand(number)
    if number =~ /^4[0-9]{6,}$/
      return 'Visa'
    elsif number =~ /^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/
      return 'Mastercard'
    elsif number =~ /^3[47][0-9]{5,}$/
      return 'American Express'
    elsif number =~ /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/
      return 'Diners Club'
    elsif number =~ /^6(?:011|5[0-9]{2})[0-9]{3,}$/
      return 'Discover'
    elsif number =~ /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/
      return 'JCB'
    end
  end

  def card_type(number)

    number = number[0..8]

    post = {}
    post[:ClientIdentifier] = "SOAP"
    post[:TransactionID] = "123"
    post[:RequestType] = "005"
    # post[:RequestDateTime] = Time.now.to_i
    post[:User] = ENV["BRIDGE_PAY_USER"]
    post[:Password] = ENV["BRIDGE_PAY_PASS"]

    message = {}
    message[:BIN] = number
    post[:requestMessage] = message

    response = bridge_pay_data(post)

    parsed_response = parse_xml(response.body)
    decode = Base64.strict_decode64(parsed_response[:body][:processrequestresponse])
    json = Hash.from_xml(decode).to_json
    json = JSON.parse(json)

    if response.is_a?(Net::HTTPSuccess)
      if json["ErrorResponse"].present?
        raise StandardError.new(json["ErrorResponse"]["ResponseDescription"])
      else
        if json["BINLookup"]["ResponseCode"] != "00000"
          raise StandardError.new(json["BINLookup"]["ResponseDescription"])
        else
          return json["BINLookup"]["responseMessage"]["CardIdentifier"]
        end
        # return json["WalletSecurityCode"]["ResponseDescription"]
      end
    else
      raise StandardError.new("BridgePay #{parsed_response[:body][:fault]}")
    end
  end

  private

  def generate_hmac(nonce, current_timestamp, payload)
    message = [
        ENV['PAYEEZY_API_KEY'], #|| 'GAGNdtjvTEJfltOgXlAGK7og0bz9HMbj',
        nonce.to_s,
        current_timestamp.to_s,
        ENV['PAYEEZY_MERCHANT_TOKEN'], #|| 'fdoa-86e0dc37f48a86324620df9d1337c4a886e0dc37f48a8632'],
        payload
    ].join("")
    # ENV['PAYEEZY_API_SECRET'] || '5f206e134699debe49ef4ce1f3ad76a8cbac7c198b4f30665fc2dc14a0314af5'
    hash = Base64.strict_encode64(OpenSSL::HMAC.hexdigest('sha256',ENV['PAYEEZY_API_SECRET'], message))
    hash
  end

  def headers(payload)
    nonce = (SecureRandom.random_number * 10_000_000_000)
    current_timestamp = (Time.now.to_f * 1000).to_i
    {
        'Content-Type' => 'application/json',
        'apikey' => ENV['PAYEEZY_API_KEY'],
        'token' => ENV['PAYEEZY_MERCHANT_TOKEN'],
        'nonce' => nonce.to_s,
        'timestamp' => current_timestamp.to_s,
        'Authorization' => generate_hmac(nonce, current_timestamp, payload)
    }
  end

  def http_headers(req, payload)
    nonce = (SecureRandom.random_number * 10_000_000_000)
    current_timestamp = (Time.now.to_f * 1000).to_i
    # {
    req['Content-Type'] = 'application/json'
    req['apikey'] = ENV['PAYEEZY_API_KEY']# || 'y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a'
    req['token'] = ENV['PAYEEZY_MERCHANT_TOKEN']# || 'fdoa-a480ce8951daa73262734cf102641994c1e55e7cdf4c02b6'
    req['nonce'] = nonce.to_s
    req['timestamp'] = current_timestamp.to_s
    req['Authorization'] = generate_hmac(nonce, current_timestamp, payload)
    # }
    req
  end

  def parse(body)
    JSON.parse(body)
  end

  def parse_xml(xml)
    response = {}
    doc = Nokogiri::XML(xml)
    doc.root.xpath('*').each do |node|
      if (node.elements.size == 0)
        response[node.name.downcase.to_sym] = node.text
      else
        response[node.name.downcase.to_sym] = {}
        node.elements.each do |childnode|
          name = "#{childnode.name.downcase}"
          response[node.name.downcase.to_sym][name.to_sym] = childnode.text
        end
      end
    end unless doc.root.nil?
    response
  end

  def bridge_pay_data(post_data)

    postData = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:req='http://bridgepaynetsecuretx.com/requesthandler'>
                    <soapenv:Header/>
                        <soapenv:Body>
                            <req:ProcessRequest>
                                <req:requestMsg>#{Base64.strict_encode64(post_data.to_xml(:root => 'requestHeader', :skip_instruct => true))}</req:requestMsg>
                            </req:ProcessRequest>
                        </soapenv:Body>
                    </soapenv:Envelope>"

    ENV["BRIDGE_PAY_URL"] = 'https://www.bridgepaynetsecuretest.com/PaymentService/RequestHandler.svc'
    uri = URI.parse(ENV["BRIDGE_PAY_URL"])
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER

    request = Net::HTTP::Post.new(uri.request_uri)
    request['Content-Type'] = 'text/xml;charset=UTF-8'
    ENV['BRIDGE_PAY_SOAP_URL'] = 'http://bridgepaynetsecuretx.com/requesthandler/IRequestHandler/ProcessRequest'
    request['SOAPAction'] = ENV["BRIDGE_PAY_SOAP_URL"]

    request.body = postData

    return http.request(request)
  end
end