class AppConfigCache 
	@timeout = 60 * 60 * 3 #5 minutes
	@@appConfigs = {}

	AppConfig.all.each do |app_config|
		@@appConfigs[app_config.key] = {
			data: app_config,
			updated_at: Time.now
		}
	end

	class << self

		def getAppConfig(key)
			begin
				if Time.now > @@appConfigs[key][:updated_at] + @timeout
					puts "app config cache timeout"
					data =  AppConfig.find_by_sql("SELECT * FROM app_configs WHERE app_configs.key = '#{key}'").last
					@@appConfigs[key] = {
						data: data,
						updated_at: Time.now
					}
				end
				return @@appConfigs[key][:data]	
			rescue Exception => e
			end
		end

		def updateAppConfigByKey(key,data)
			puts "app config cache update"
			@@appConfigs[key] = {
				data: data,
				updated_at: Time.now
			}
			return @@appConfigs[key][:data]
		end

		def list
			@@appConfigs.values.map{|obj| obj[:data]}
		end
	end

	private_class_method :new
end