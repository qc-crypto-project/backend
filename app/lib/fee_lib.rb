class FeeLib
  include ApplicationHelper

  def get_card_fee(amount=nil, instrument)
    if instrument == PaymentInstrument::DepositCard
      a = AppConfig.where(key: AppConfig::Fee::DepositCard).pluck(:user_fee).first
      return fee = number_with_precision(amount.to_f * (a.to_f/100), precision: 2)
    elsif instrument == PaymentInstrument::ECheck
      return a = AppConfig.where(key: AppConfig::Fee::UserCheckFee).pluck(:user_fee).first
    elsif instrument == PaymentInstrument::VisaCard
      return a = AppConfig.where(key: AppConfig::Fee::UserVisaCardFee).pluck(:user_fee).first
    elsif instrument == PaymentInstrument::GiftCard
      return a = AppConfig.where(key: AppConfig::Fee::UserGiftCardFee).pluck(:user_fee).first
    elsif instrument == PaymentInstrument::Paypal
      return a = AppConfig.where(key: AppConfig::Fee::UserPaypalCardFee).pluck(:user_fee).first
    end
  end

  def card_fee(instrument)
    if instrument == PaymentInstrument::DepositCard
      a = AppConfig.where(key: AppConfig::Fee::DepositCard).pluck(:user_fee).first
      return a
    elsif instrument == PaymentInstrument::ECheck
      return a = AppConfig.where(key: AppConfig::Fee::UserCheckFee).pluck(:user_fee).first
    elsif instrument == PaymentInstrument::GiftCard
      return a = AppConfig.where(key: AppConfig::Fee::UserGiftCardFee).pluck(:user_fee).first
    end
  end

  def get_total_location_fee(wallet, amount)
    # for getting total fee for sale
    location = wallet.location
    location_fee = location.fees if location.present?
    fee_object = location_fee.buy_rate.first
    #get customer fee
    dollar_fee = fee_object.transaction_fee_app_dollar.to_f
    #get percent of fee from total amount
    perc_fee = deduct_fee(fee_object.transaction_fee_app.to_f, amount.to_f)
    return dollar_fee.to_f + perc_fee.to_f
  end


  #to show user limit in error
  def show_limit(type)
    if type == 'deposit'
      deposit_limit = AppConfig.where(key: AppConfig::Fee::UserDepositLimit).pluck(:user_fee).first
      remaining = deposit_limit.to_f
      if remaining > 0
      return number_to_currency(number_with_precision(remaining, precision: 2))
      else
      return number_to_currency(number_with_precision(0, precision: 2))
      end
    elsif type == 'transfer'
      spending_limit = AppConfig.where(key: AppConfig::Fee::UserSpendingLimit).pluck(:user_fee).first
      remaining=spending_limit.to_f

      if remaining > 0
        return number_to_currency(number_with_precision(remaining, precision: 2))
      else
        return number_to_currency(number_with_precision(0, precision: 2))
      end
    end

  end

  # get weekly limit of user
  def get_limit(amount, type)
    if type == 'deposit'
      limit = AppConfig.where(key: AppConfig::Fee::UserDepositLimit).pluck(:user_fee).first
      total_amount = amount.to_f
      if total_amount <= limit.to_f
        return true
      else
        return false
      end
    elsif type == 'transfer'
      limit = AppConfig.where(key: AppConfig::Fee::UserSpendingLimit).pluck(:user_fee).first
      total_amount =  amount.to_f
      if total_amount <= limit.to_f
        return true
      else
        return false
      end
    end
  end


  def divide_send_check_fee(location,location_fee,company ,redeem_fee ,perc,check_fee, company_wallet)
    #percent fee + dollar fee for send check
    hash = {}
    if redeem_fee
      if redeem_fee.partner.to_f > 0
        par_fee = (redeem_fee.partner.to_f/location.fees.redeem_fee.first.redeem_fee.to_f) * 100
        par_fee = number_with_precision((perc.to_f/100)*par_fee, precision:  2)
      end

      if redeem_fee.iso.to_f > 0
        iso_fee = (redeem_fee.iso.to_f/location.fees.redeem_fee.first.redeem_fee.to_f) * 100
        iso_fee = number_with_precision((perc.to_f/100)*iso_fee, precision:  2)
      end

      if redeem_fee.agent.to_f > 0
        agent_fee = (redeem_fee.agent.to_f/location.fees.redeem_fee.first.redeem_fee.to_f) * 100
        agent_fee = number_with_precision((perc.to_f/100)*agent_fee, precision:  2)
      end

    end

    if location_fee
      p_send_check = location_fee.partner.to_f
      i_send_check = location_fee.iso.to_f
      a_send_check = location_fee.agent.to_f
    end

    if location.present?
      location.users.each do |k|
        if k.iso?
          hash = hash.merge({
                                iso_wallet: k.wallets.first.id,
                                iso_fee: iso_fee,
                                iso_check_fee: i_send_check,
                                iso_total_fee: iso_fee.to_f + i_send_check.to_f,
                                iso_name: k.name,
                                iso_email: k.email,
                                iso_number: k.phone_number
                            })
        end
        if k.agent?
          hash = hash.merge({
                                agent_wallet: k.wallets.first.id,
                                agent_fee: agent_fee,
                                agent_check_fee: a_send_check,
                                agent_total_fee: agent_fee.to_f + a_send_check.to_f,
                                agent_name: k.name,
                                agent_email: k.email,
                                agent_number: k.phone_number
                            })
        end
      end

      return hash = hash.merge({
                                   partner_wallet: company_wallet,
                                   partner_fee: par_fee ,
                                   partner_check_fee: p_send_check ,
                                   partner_total_fee: par_fee.to_f + p_send_check.to_f,
                                   partner_name: company.name,
                                   partner_email: company.company_email,
                                   partner_number: company.phone_number
                               })

    end
  end

  def divide_add_money_fee(location,location_fee,company ,redeem_check ,perc, company_wallet)
    #percent fee + dollar fee for add money
    hash = {}
    if redeem_check
      if redeem_check.partner.to_f > 0
        par_fee = (redeem_check.partner.to_f/location.fees.redeem_check.first.redeem_check.to_f) * 100
        par_fee = number_with_precision((perc.to_f/100)*par_fee, precision:  2)
      end

      if redeem_check.iso.to_f > 0
        iso_fee = (redeem_check.iso.to_f/location.fees.redeem_check.first.redeem_check.to_f) * 100
        iso_fee = number_with_precision((perc.to_f/100)*iso_fee, precision:  2)
      end

      if redeem_check.agent.to_f > 0
        agent_fee = (redeem_check.agent.to_f/location.fees.redeem_check.first.redeem_check.to_f) * 100
        agent_fee = number_with_precision((perc.to_f/100)*agent_fee, precision:  2)
      end

    end

    if location_fee
      partner_add_money = location_fee.partner.to_f
      iso_add_money = location_fee.iso.to_f
      agent_add_money = location_fee.agent.to_f
    end

    if location.present?
      location.users.each do |k|
        if k.iso?
          hash = hash.merge({
                                iso_wallet: k.wallets.first.id,
                                iso_fee: iso_fee,
                                iso_check_fee: iso_add_money,
                                iso_total_fee: iso_fee.to_f + iso_add_money.to_f,
                                iso_name: k.name,
                                iso_email: k.email,
                                iso_number: k.phone_number
                            })
        end
        if k.agent?
          hash = hash.merge({
                                agent_wallet: k.wallets.first.id,
                                agent_fee: agent_fee,
                                agent_check_fee: agent_add_money,
                                agent_total_fee: agent_fee.to_f + agent_add_money.to_f,
                                agent_name: k.name,
                                agent_email: k.email,
                                agent_number: k.phone_number
                            })
        end
      end

      return hash = hash.merge({
                                   partner_wallet: company_wallet,
                                   partner_fee: par_fee ,
                                   partner_check_fee: partner_add_money ,
                                   partner_total_fee: par_fee.to_f + partner_add_money.to_f,
                                   partner_name: company.name,
                                   partner_email: company.company_email,
                                   partner_number: company.phone_number
                               })

    end
  end

  def giftcard_fee_divsion(location,location_fee, company, company_wallet)
    # only percent fee
    hash = {}
    par_fee = nil
    iso_fee = nil
    agent_fee = nil

    if location.present?
      location.users.each do |k|
        if k.iso?
          hash = hash.merge({
                                iso_total_fee: location_fee.iso.to_f,
                                iso_wallet: k.wallets.first.id,
                                iso_name: k.name,
                                iso_email: k.email,
                                iso_number: k.phone_number
                            })
        end
        if k.agent?
          hash = hash.merge({
                                agent_total_fee: location_fee.agent.to_f,
                                agent_wallet: k.wallets.first.id,
                                agent_name: k.name,
                                agent_email: k.email,
                                agent_number: k.phone_number
                            })
        end
      end
    end
    return hash = hash.merge({
                                 partner_total_fee: location_fee.partner.to_f,
                                 partner_wallet: company_wallet,
                                 partner_name: company.name,
                                 partner_email: company.company_email,
                                 partner_number: company.phone_number
                             })
  end


  def get_merchant_users_info(location,location_fee, company, company_wallet, cus_fee_ded, actual_fee, reserve_fee = nil, customer_fee_dollar=nil)
    # only percent fee
    hash = {}
    par_fee = nil
    par_fee_in_dollar =0
    iso_fee_in_dollar = 0
    agent_fee_in_dollar = 0
    iso_fee = nil
    agent_fee = nil
    reserve_wallet = nil

    if location_fee.present?
      if location_fee.partner.to_f > 0
        par_fee = (location_fee.partner.to_f/actual_fee.to_f) * 100
        par_fee = number_with_precision((cus_fee_ded.to_f/100)*par_fee, precision:  2)
      end

      if location_fee.iso.to_f > 0
        iso_fee = (location_fee.iso.to_f/actual_fee.to_f) * 100
        iso_fee = number_with_precision((cus_fee_ded.to_f/100)*iso_fee, precision:  2)
      end
      if location_fee.agent.to_f > 0
        agent_fee = (location_fee.agent.to_f/actual_fee.to_f) * 100
        agent_fee = number_with_precision((cus_fee_ded.to_f/100)*agent_fee, precision:  2)
      end
    end

    if customer_fee_dollar.present?
      par_fee_in_dollar = customer_fee_dollar.partner.to_f
      iso_fee_in_dollar = customer_fee_dollar.iso.to_f
      agent_fee_in_dollar = customer_fee_dollar.agent.to_f
    end
    # if agent_fee.present? && agent_fee_in_dollar.present?
    #   agent_fee = agent_fee + agent_fee_in_dollar
    # elsif agent_fee.nil? && agent_fee_in_dollar.present?
    #   agent_fee = agent_fee_in_dollar
    # end

    if location.present?
      reserve_wallet = location.wallets.where(wallet_type: 4).first
      location.users.each do |k|
        if k.iso?
            hash = hash.merge({
              iso_total_fee: number_with_precision(iso_fee.to_f + iso_fee_in_dollar.to_f, precision: 2),
              iso_wallet: k.wallets.first.id,
              iso_name: k.name,
              iso_email: k.email,
              iso_number: k.phone_number
            })
        end
        if k.agent?
              hash = hash.merge({
                agent_total_fee: number_with_precision(agent_fee.to_f + agent_fee_in_dollar.to_f, precision: 2),
                agent_wallet: k.wallets.first.id,
                agent_name: k.name,
                agent_email: k.email,
                agent_number: k.phone_number
              })
        end
      end
      if reserve_wallet.present?
        hash = hash.merge({
                              reserve_wallet: reserve_wallet.id,
                              hold_money: reserve_fee.to_f
                          })
      end
    end
    return hash = hash.merge({
        partner_total_fee: number_with_precision(par_fee.to_f + par_fee_in_dollar.to_f, precision: 2),
        partner_wallet: company_wallet,
        partner_name: company.name,
        partner_email: company.company_email,
        partner_number: company.phone_number
    })
  end

end

module PaymentInstrument
  DepositCard = 'card'
  ECheck = 'echeck'
  VisaCard = 'visa_cash'
  Paypal = 'paypal'
  GiftCard = 'gift_card'
end