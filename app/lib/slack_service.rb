class SlackService

  CLIENT = Slack::Web::Client.new

  def self.notify msg, channel = (ENV['SLACK_ALERT_CHANNEL'] || I18n.t('slack.channels.quickcard_exception')), liveOnly=false
    return if liveOnly && ENV["STACK"] != I18n.t('environments.live')
    channel = I18n.t('slack.channels.qc_live_alerts') if Rails.env.production? && ENV['HEROKU_APP'].blank? && channel.blank?
    CLIENT.auth_test
    CLIENT.chat_postMessage(channel: channel, text: msg , as_user: true)
  end

  def self.upload_files(title, file, channel, file_name)
    CLIENT.files_upload(
              channels: channel,
              as_user: false,
              file: Faraday::UploadIO.new(file.path,'text/csv'),
              title: title,
              file_name: file_name
    )
  end

  def self.handle_exception topic, exception, channel = (ENV['SLACK_ALERT_CHANNEL'] || I18n.t('slack.channels.quickcard_exception'))
    channel = I18n.t('slack.channels.qc_live_alerts') if Rails.env.production? && ENV['HEROKU_APP'].blank? && channel.blank?
    CLIENT.auth_test
    params.select!{|k,v| k != 'card_number' && k != 'cvv' && k != 'expiry_date'}
    msg = "*#{topic} - #{@exception.class.name}* \n Request Parameters: \`\`\`#{params.to_json}\`\`\` User: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` Execption: \`\`\` #{exception.to_json} \`\`\`"
    CLIENT.chat_postMessage(channel: channel, text: msg , as_user: true)
  end

  def self.create_message(params, error, current_user, topic)
    "*#{topic}* \n Request Parameters: \`\`\`#{params.to_json}\`\`\` User: \`\`\`#{{id: current_user.id, email: current_user.email, role: current_user.role}.to_json}\`\`\` Execption: \`\`\` #{error} \`\`\`"
  end

  # def self.handle_exception ex, channel = '#quickcard-exceptions', context = nil
  #   CLIENT.auth_test
  #   CLIENT.chat_postMessage(
  #     channel: channel,
  #     text: {Exception: (ex.class.name == 'String' ? ex : ex.message.to_s), Context: context.to_s[0..600].gsub(/\s\w+\s*$/,'...')}.to_s,
  #     as_user: true
  #   )
  # end
end