class TangoClient
	def self.instance 
    # if Rails.env == "development" or ENV["HEROKU_APP"].present?
      # raas_env = Raas::Configuration::Environment::SANDBOX
    # elsif Rails.env == "production"
      raas_env = Raas::Configuration::Environment::PRODUCTION
    # end
    @@client ||= Raas::RaasClient.new(platform_name: ENV["TANGO_PLATFORM_NAME"], platform_key: ENV["TANGO_PLATFORM_KEY"], env: raas_env)
	end
end
