class CustomLib
  class << self
    include Admins::BlockTransactionsHelper
    include ActionView::Helpers::NumberHelper
    include Admins::BuyRateHelper
    include ApplicationHelper

    def create_ach_manual(sequence_id)
      if sequence_id.present?
        sequence_tx = SequenceLib.get_transaction_id(sequence_id)
        if sequence_tx.present?
          main_tx = sequence_tx.first.actions.first
          sender_wallet = Wallet.where(id: main_tx.source_account_id).first
          sender_user = sender_wallet.users.first if sender_wallet.present?
          last_ach = TangoOrder.instant_ach.where(wallet_id: main_tx.source_account_id).last
          if last_ach.present?
            amount = SequenceLib.dollars(main_tx.amount)
            fee = main_tx.tags["fee"].to_f
            splits = main_tx.tags["fee_perc"]
            transaction = Transaction.create(
                status: "pending",
                amount: amount,
                sender: sender_user,
                sender_wallet_id: sender_wallet.id,
                action: 'retire',
                fee: fee.to_f,
                net_amount: amount.to_f,
                net_fee: fee.to_f,
                total_amount: amount.to_f + fee.to_f,
                gbox_fee: splits.try(:[],"gbox").try(:[],"amount").to_f,
                iso_fee: splits.try(:[],"iso").try(:[],"amount").to_f,
                agent_fee: splits.try(:[],"agent").try(:[],"amount").to_f,
                affiliate_fee: splits.try(:[],"affiliate").try(:[],"amount").to_f,
                main_type: "ACH",
                sub_type: main_tx.tags["sub_type"],
                tags: main_tx.tags,
                timestamp: sequence_tx.first.timestamp,
                seq_transaction_id: sequence_id
            )
            new_ach = last_ach.dup
            new_ach.update(
                status: "PENDING",
                checkId: "#{rand(100..999)}#{Time.now.to_i.to_s}",
                actual_amount: amount.to_f,
                amount: amount.to_f + fee.to_f,
                batch_date: Time.zone.now + 1.day,
                approved: false,
                settled: :false,
                send_via: "ACH",
                transaction_id: transaction.id
            )
            if new_ach.try(:id).present?
              transaction.update(status: "approved")
              parsed_transactions = parse_block_transactions(sequence_tx.first.actions, sequence_tx.first.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              puts "successfully created ach"
            end
          end
        end
      end
    end

    def locations_iso(category_name)
      export = {}
      categories_id = Category.where('lower(name) ILIKE ? ', "%#{category_name}%").pluck(:id)
      Location.where(category_id: categories_id).group_by{|a| a.iso }.each do |iso, locations|
        company_name = ""
        name = ""
        iso_id = iso.try(:id)
        if iso.present?
          if iso.profile.present?
            company_name = iso.profile.company_name.present? ? iso.profile.company_name : 'n/a'
          else
            company_name = iso.company_name.present? ? iso.company_name : 'n/a'
          end  
          name = iso.name || "#{iso.first_name} #{iso.last_name}"
        end
        export[iso_id] = [{
          name: name,
          company_name: company_name,
          merchant_count: locations.pluck(:merchant_id).uniq.size,
          location_count: locations.count,
          merchant_id: "",
          merchant_location_count: "",
          locations_names: ""
        }]
        # locations.group_by{|l| l.merchant_id}.each do|merchant_id, locations|
        #   export[iso_id].push({
        #     name: "",
        #     company_name: "",
        #     merchant_count: "",
        #     location_count: "",
        #     merchant_id: merchant_id,
        #     merchant_location_count: locations.count,
        #     locations_names: locations.pluck(:business_name).join(' __ ')
        #   })
        # end
      end
      begin
        file = File.new("#{Rails.root}/tmp/locations_iso-#{DateTime.now.to_i}.csv", "w")
        file << CSV.generate do |csv|
          csv << ['ISO_ID', 'Company_Name', 'Name', 'Total Merchants', 'Total Locations']#, 'Merchant ID', "Merchant's Total Location", 'DBA Names']
          
          export.each do |id, data|
            data.each.with_index do |tx, index|
              isoID = index == 0 ? id : ""
              csv << [isoID, tx[:company_name], tx[:name], tx[:merchant_count], tx[:location_count]]#, tx[:merchant_id], tx[:merchant_location_count], tx[:locations_names]]
            end
          end
        end
        file.close
      end

      SlackService.upload_files("locations_iso-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "locations_iso-#{DateTime.now.to_i}")
    
    end
    
    def iso_transaction_export(iso_id)
      all_txs = []
      BlockTransaction.where('receiver_id = ? OR sender_id = ?', iso_id, iso_id).each_slice(1000) do |batch|
        batch.each do |t|
          if t.main_type == "Subscription fee"
            main_type = show_iso_local_type(t)
          elsif t.main_type == "Service Fee"
            main_type = "Monthly Fee"
          else 
            txn_type = main_type_format(t.main_type)
            if txn_type == "Push to Card"
              main_type = t.action == "retire"? "Push To Card Deposit" : "Push To Card"
            elsif txn_type == "Send Check"
              main_type = t.action == "retire"? "Check Deposit" : "Send Check"
            else
              main_type = txn_type
            end
          end
        
        sender = change_wallets_name(t.sender_name || t.tags.try(:[], "merchant").try(:[], "name").try(:truncate, 24) || "--")

        dba_name = t.location_dba_name || t.tags.try(:[], "location").try(:[], "business_name") || "--"
        
        tx_type = main_type_format(show_iso_local_type(t) || "---")
        if tx_type == "Push To Card" 
          merchant_tx_type = t.action == "retire" ? "Push to Card Deposit" : "Push to Card"
        elsif tx_type == "Send Check"
          merchant_tx_type = t.action == "retire" ? "Check Deposit" : "Send Check"
        elsif  tx_type == "B2B Transfer" && (t.sub_type == "invoice" || t.tx_sub_type == "invoice")
          merchant_tx_type = "Invoice - QC"
        elsif  tx_type == "Qcp Secure" && (t.sub_type == "QCP Secure_fee" || t.tx_sub_type == "QCP Secure_fee")
          merchant_tx_type = "QCP Secure"
        elsif  tx_type == "Invoice   Credit Card" && (t.sub_type == "Invoice - Credit Card_fee" || t.tx_sub_type == "Invoice - Credit Card_fee")
          merchant_tx_type = "Invoice - Credit Card"
        elsif  tx_type == "Invoice   Debit Card" && (t.sub_type == "Invoice - Debit Card_fee" || t.tx_sub_type == "Invoice - Debit Card_fee")
          merchant_tx_type = "Invoice - Debit Card"
        else
          merchant_tx_type = tx_type.downcase == "ach deposit" ? "ACH Deposit" : tx_type
        end
        
        if t.main_amount.to_f.present? && t.net_fee.to_f.present? && t.sub_type == "ACH Deposit_fee"
          tx_amount = t.main_amount.to_f + t.net_fee.to_f
        else
          tx_amount = t.main_amount.to_f > 0 ? t.main_amount.to_f : cents_to_dollars(t.amount_in_cents)
        end
        total_tx_amount = number_with_precision(number_to_currency(tx_amount.to_f), precision: 2)
         
        if ["Send Check", "ACH Deposit", "Instant Pay"].include? t.main_type
          commission = "--"
        elsif iso_id == t.receiver_id
          commission = "+ $#{cents_to_dollars(t.amount_in_cents)}"
        else
          commission = "- $#{cents_to_dollars(t.amount_in_cents)}"
        end
        
        receiver = change_wallets_name(t.receiver_name.try(:truncate, 24) || "---" )
      
        all_txs.push({
          txn_id: t.sequence_id.try(:first, 6),
          total_amount: total_tx_amount,
          commission: commission,
          merchant_tx_type: merchant_tx_type,
          main_type: main_type,
          dba_name: dba_name,
          timestamp: t.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
          sender: sender,
          receiver: receiver
        })
        end
      end
      begin
        file = File.new("#{Rails.root}/tmp/iso-#{iso_id}-#{DateTime.now.to_i}.csv", "w")
        file << CSV.generate do |csv|
          csv << ['Txn ID', 'DateTime', 'Transaction Type', 'Sender Name', 'DBA Name', 'Merchant Txn Type', 'Total Tx Amount', 'Commission', 'Receiver Name']

          all_txs.each do |tx|
            csv << [tx[:txn_id], tx[:timestamp], tx[:main_type], tx[:sender], tx[:dba_name], tx[:merchant_tx_type], tx[:total_amount], tx[:commission], tx[:receiver]]
          end
        end
        file.close
      end

      SlackService.upload_files("iso-#{iso_id}-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "iso-#{iso_id}-#{DateTime.now.to_i}")
    end

    def checkbook_status
      data = TangoOrder.where('created_at > ?',"1/1/2020").where(order_type: ["instant_pay","check"]).where(status: ["PAID", "PAID_WIRE", "IN_PROCESS"])
      begin
        # .created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p")
        file = File.new("#{Rails.root}/tmp/checkbook_export-#{DateTime.now.to_i}.csv", "w")
        file << CSV.generate do |csv|
          csv << [
              'QC_ID', 'Checkbook_ID', 'Checkbook_Number', 'Amount', 'Type', 'CheckImage', 'Created Date', 'Update Date', 'Name', 'Recipient', 'User ID', 'Wallet ID', 'First6', 'Last4'
          ]
          data.each do |tango|
            type = tango.instant_pay? ? "P2C" : tango.check? ? "Check" : tango.order_type
            created_at = tango.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p")
            updated_at = tango.updated_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p")
            user = tango.user
            user_id = ""
            if user.present?
              if user.merchant?
                user_id = "M - #{user.id}"
              elsif user.iso?
                user_id = "ISO - #{user.id}"
              elsif user.affiliate?
                user_id = "AFF - #{user.id}"
              elsif user.agent?
                user_id = "A - #{user.id}"
              else
                user_id = user.id
              end
            end
            csv << [tango.id, tango.checkId, tango.number, tango.actual_amount, type, tango.catalog_image, created_at, updated_at, tango.name, tango.recipient, user_id, tango.wallet_id, tango.first6, tango.last4]
          end
        end
        file.close
      end
      SlackService.upload_files("checkbook_export_status-#{DateTime.now.to_i}", file, "#qc-files", "checkbook_export_status-#{DateTime.now.to_i}")
    end

    def merchant_disputes(merchant_wallet_id)
      data = DisputeCase.includes(:reason,:payment_gateway).where(merchant_wallet_id: merchant_wallet_id)
      begin
        file = File.new("#{Rails.root}/tmp/dispute-cases-#{merchant_wallet_id}-#{DateTime.now.to_i}.csv", "w")

        file << CSV.generate do |csv|
          csv << [
              'Case Number', 'Customer Name', 'Merchant ID', 'Merchant Name', 'DBA Name', 'DBA Category', 'Received Date', 'Created Date', 'CBK Amount', 'Due Date', 'Description', 'Original Transaction Date', 'Original Txn Amount', 'Original Txn ID', 'Bank Descriptor', 'Status', 'Card Brand', "Card First 6", "Card Last 4"
          ]

          data.each do |dispute|
            transaction = Transaction.where(id: dispute.transaction_id).select("id AS id", "seq_transaction_id AS seq_transaction_id","total_amount AS total_amount","fee AS fee","created_at as created_at", "timestamp as timestamp", "card_id as card_id", "tags as tags","receiver_id","receiver_name","first6","last4").first
            csv << [dispute.case_number,transaction.tags.try(:[],"card").try(:[],"name"),transaction.receiver_id,transaction.receiver_name, transaction.tags.try(:[],"location").try(:[],"business_name"),"Dispensary",dispute.recieved_date.nil? ? 'NA' : dispute.recieved_date.strftime("%m-%d-%Y"), dispute.created_at.nil? ? 'NA' : dispute.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),number_to_currency(number_with_precision(dispute.try(:amount).to_f,precision: 2, :delimiter => ',')), dispute.due_date.nil? ? 'NA' : dispute.due_date.strftime("%m-%d-%Y"),dispute.reason.try(:title),transaction.try(:timestamp).present? ? transaction.try(:timestamp).to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : "N/A", number_to_currency(number_with_precision(transaction.try(:total_amount).to_f,precision: 2, :delimiter => ',')),transaction.try(:seq_transaction_id),transaction.tags.try(:[],"descriptor"),dispute.status,transaction.tags["card"].try(:[], "brand"), transaction.first6,transaction.last4]
          end
        end
        file.close
      end

      SlackService.upload_files("dispute-cases-#{merchant_wallet_id}-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "dispute-cases-#{merchant_wallet_id}-#{DateTime.now.to_i}")
    end

    # Export All Dispute
    def all_disputes
      date = '01/01/2020'.to_datetime.in_time_zone("Pacific Time (US & Canada)")
      data = DisputeCase.includes(:reason, :payment_gateway).where('recieved_date > ?', date)
      begin
        file = File.new("#{Rails.root}/tmp/all_dispute-cases-#{DateTime.now.to_i}.csv", "w")

        file << CSV.generate do |csv|
          csv << [
              'Case Number', 'Customer Name', 'Merchant ID', 'Merchant Name', 'DBA Name', 'DBA Category', 'Received Date', 'Created Date', 'CBK Amount', 'Due Date', 'Description', 'Original Transaction Date', 'Original Txn Amount', 'Original Txn ID', 'Bank Descriptor', 'Status', 'Card Brand', "Card First 6", "Card Last 4"
          ]

          data.each do |dispute|
            transaction = Transaction.where(id: dispute.transaction_id).select("id AS id", "seq_transaction_id AS seq_transaction_id","total_amount AS total_amount","fee AS fee","created_at as created_at", "timestamp as timestamp", "card_id as card_id", "tags as tags","receiver_id","receiver_name","first6","last4").first
            csv << [dispute.case_number, transaction.tags.try(:[],"card").try(:[],"name").present? ? transaction.tags.try(:[],"card").try(:[],"name") : transaction.try(:sender_id).present? ? transaction.try(:sender).try(:name) : "",transaction.receiver_id,transaction.receiver_name, transaction.tags.try(:[],"location").try(:[],"business_name"),"Dispensary",dispute.recieved_date.nil? ? 'NA' : dispute.recieved_date.strftime("%m-%d-%Y"), dispute.created_at.nil? ? 'NA' : dispute.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),number_to_currency(number_with_precision(dispute.try(:amount).to_f,precision: 2, :delimiter => ',')), dispute.due_date.nil? ? 'NA' : dispute.due_date.strftime("%m-%d-%Y"),dispute.reason.try(:title),transaction.try(:timestamp).present? ? transaction.try(:timestamp).to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : "N/A", number_to_currency(number_with_precision(transaction.try(:total_amount).to_f,precision: 2, :delimiter => ',')),transaction.try(:seq_transaction_id),transaction.tags.try(:[],"descriptor"),dispute.status,transaction.tags["card"].try(:[], "brand"), transaction.first6,transaction.last4]
          end
        end
        file.close
      end

      SlackService.upload_files("all_dispute-cases-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "all_dispute-cases-#{DateTime.now.to_i}")
    end

    def decline_txs
      date = '01/09/2020'.to_datetime.in_time_zone("Pacific Time (US & Canada)")
      transactions= Transaction.where('created_at > ?', date).where(status: "pending").where(main_type:["eCommerce", "Virtual Terminal", "Credit Card", "PIN Debit", "Sale Issue", "Debit Card","QCP Secure","QC Secure", "Invoice - RTP", "RTP"]).order('created_at DESC')
      
      begin
        file = File.new("#{Rails.root}/tmp/decline_txs-#{DateTime.now.to_i}.csv", "w")

        file << CSV.generate do |csv|
          csv << [
            "Txn ID", "Date/Time", "Sender Name", "Receiver Name", "Type", "Status", "Descriptor", "CC#", "Total Tx Amount", "Reason"
          ]

          transactions.each do |tx|
            sender = ""
            if tx.try(:from).present? && tx.try(:from) == "Credit"
              sender = "Credit"
            elsif (tx.try(:from).nil? && tx.try(:tag).try(:[],"api_type") == "debit") || (tx.try(:from).present? && tx.try(:from) == "Debit")
              sender = "Debit"
            elsif tx.try(:main_type) == "Sale Issue" || tx.try(:main_type) == TypesEnumLib::TransactionType::Issue3DS || tx.try(:tag).try(:[],"sub_type") == "sale_issue" || tx.try(:tag).try(:[],"sub_type") == "sale_issue_api"
              sender = "--"
            elsif tx.try(:main_type) == "Service Fee" ||tx.try(:main_type) == "Subscription Fee" || tx.try(:main_type) == "Misc Fee" || tx.try(:main_type) == "Void Check"
              if tx.sender_name.present?
                sender = change_wallet_name(tx.sender_name) 
              else 
                sender = change_wallet_name(Wallet.find_by(id: tx.try(:from).try(:to_i)).try(:[],'name').present? ?  Wallet.find_by(id: tx.try(:from).try(:to_i)).try(:[],'name') : '--') 
              end 
            elsif tx.try(:main_type) == "PIN Debit"
               if tx.sender_name.present?
                sender = change_wallet_name((tx.sender_name).truncate(24) || '---')
              else
                sender = "--"
              end
            else
              if tx.sender_name.present?
                sender = change_wallet_name((tx.sender_name).truncate(24) || '---')
              else
                sender = "--"
              end
            end
            receiver = ""
             if tx.try(:main_type) == "PIN Debit"
              if tx.receiver_name.present?
                receiver = change_wallet_name(tx.receiver_name)
              else
                receiver = "--"
              end
            else
              if tx.receiver_name.present?
                receiver = change_wallet_name((tx.receiver_name).truncate(24) || '---')
              else
                receiver = "--"
              end
            end
            activity = ActivityLog.where(transaction_id: tx.id).first
            transaction_info = tx.tags
            if transaction_info.present?
              if transaction_info["message"].present?
                reason = transaction_info["message"]
              elsif activity.try(:[],'respond_with').present?
                decline_reason = activity.respond_with.class == String ? JSON.parse(activity.respond_with) : activity.respond_with
                reason = decline_reason.try(:[],'message').present? ? decline_reason.try(:[],'message') : 'Decline From Bank'
              else
                reason = "Decline from Bank"
              end
            end
            
            type = main_type_format(show_local_type(tx)? show_local_type(tx) : show_local_type_for_decline(tx)) 
            csv << [
              tx.id,
              tx.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%Y-%m-%d %H:%M:%S %z"),
              sender, 
              receiver,
              type, 
              "Declined",
              tx.try(:tags).try(:[],"descriptor").present? ? tx.try(:tags).try(:[],"descriptor") : "--",
              "#{tx.first6}****#{tx.last4}",
              number_with_precision(number_to_currency(tx.try(:total_amount).try(:to_f))),
              reason
            ]
          end
        end
        file.close
      end

      SlackService.upload_files("decline_txs-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "decline_txs-#{DateTime.now.to_i}")
    end

    # for exporting locations
    def export_locations
      data = Location.joins(:merchant,:category).select("locations.id as id, locations.business_name as business_name, locations.first_name as location_contact_name, locations.email as email, locations.phone_number as phone_number, locations.cs_number as customer_service_number, locations.address as address, locations.country as country, locations.city as city, locations.state as state, locations.zip as zip, locations.business_type as business_type, users.id as merchant_id,  users.name as merchant_name, categories.name as category_name").order('id DESC')
      begin
        file = File.new("#{Rails.root}/tmp/locations-#{DateTime.now.to_i}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["ID","Business Name","Location Contact Name","Email","Phone Number","Customer Service Number","Address","Country","City","State","Zip","Merchant Id","Merchant Name","Business Type", "Category"]
          data.each do |file|
            csv << [file[:id], file[:business_name] , file[:location_contact_name], file[:email],file[:phone_number],file[:customer_service_number],file[:address],file[:country],file[:city],file[:state],file[:zip],file[:merchant_id],file[:merchant_name],file[:business_type],file[:category_name]]
          end
        end
        file.close
      end
      SlackService.upload_files("locations-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "locations-#{DateTime.now.to_i}")
    end

    def export_iso_agent_affiliate
      data = User.where(role:[:iso,:agent,:affiliate]).select("id,ref_no,name,email,company_name,phone_number,role").order('id DESC')
      begin
        file = File.new("#{Rails.root}/tmp/partners-#{DateTime.now.to_i}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["ID","Ref No","Name","Email","Company Name","Phone Number", "Role"]
          data.each do |file|
            csv << [file[:id], file[:ref_no] , file[:name], file[:email],file[:company_name],file[:phone_number], file[:role]]
          end
        end
        file.close
      end
      SlackService.upload_files("partners-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "partners-#{DateTime.now.to_i}")
    end

    def issue_in_wallet(amount, main_type, wallet_id, memo)
      begin
        transfer_details = {
            amount: amount,
            memo: memo,
            date: Time.now,
            transfer_timezone: nil
        }
        seq_tx  = SequenceLib.issue(wallet_id, amount, nil, main_type, 0, nil, TypesEnumLib::GatewayType::Quickcard,nil,nil, nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
        if seq_tx.present?
          receiver = Wallet.where(id: wallet_id).first.try(:users).try(:first)
          tx = Transaction.create(
                         receiver_wallet_id: wallet_id,
                         receiver_id: receiver.try(:id),
                         receiver_name: seq_tx.try(:actions).try(:first).try(:tags).try(:[],"location").try(:[],"business_name") || receiver.try(:name),
                         amount: amount,
                         net_amount: amount,
                         total_amount: amount,
                         fee: 0,
                         net_fee: 0,
                         main_type: main_type,
                         status: "approved",
                         timestamp: seq_tx.timestamp,
                         seq_transaction_id: seq_tx.actions.first.id,
                         tags: seq_tx.actions.first.tags,
                         action: "issue",
          )
          parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          puts "transaction id: ",tx.id
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
      end
    end

    def retire_from_wallet(amount, main_type, wallet_id, memo)
      begin
        transfer_details = {
            amount: amount,
            memo: memo,
            date: DateTime.now,
            transfer_timezone: nil
        }
        seq_tx = SequenceLib.retire(amount,wallet_id, main_type, 0, nil, TypesEnumLib::GatewayType::Quickcard, nil,nil,nil, nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
        if seq_tx.present?
          sender = Wallet.where(id: wallet_id).first.try(:users).try(:first)
          tx = Transaction.create(
              sender_wallet_id: wallet_id,
              sender_id: sender.try(:id),
              sender_name: sender.try(:name),
              amount: amount,
              net_amount: amount,
              total_amount: amount,
              fee: 0,
              net_fee: 0,
              main_type: main_type,
              status: "approved",
              timestamp: seq_tx.timestamp,
              seq_transaction_id: seq_tx.actions.first.id,
              tags: seq_tx.actions.first.tags,
              action: "retire",
              )
          parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          puts "transaction id: ",tx.id
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
      end
    end

    def return_monthly_fee(amount, main_type, wallet_id, memo)
      begin
        transfer_details = {
            amount: amount,
            memo: memo,
            date: Time.now,
        }
        escrow_wallet = Wallet.escrow.first
        seq_tx = SequenceLib.transfer(amount, escrow_wallet.id, wallet_id, main_type, 0, nil, TypesEnumLib::GatewayType::Quickcard, nil, nil, nil,nil, nil,nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, transfer_details)
        if seq_tx.present?
          receiver = Wallet.where(id: wallet_id).first.try(:users).try(:first)
          tx = Transaction.create(
              sender_wallet_id: escrow_wallet.id,
              sender_name: escrow_wallet.name,
              receiver_wallet_id: wallet_id,
              receiver_id: receiver.try(:id),
              receiver_name: receiver.try(:name),
              amount: amount,
              net_amount: amount,
              total_amount: amount,
              fee: 0,
              net_fee: 0,
              main_type: main_type,
              status: "approved",
              timestamp: seq_tx.timestamp,
              seq_transaction_id: seq_tx.actions.first.id,
              tags: seq_tx.actions.first.tags,
              action: "transfer",
              )
          parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          puts "transaction id: ",tx.id
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
      end
    end

    def issue_back_p2c(merchant_id)
      #this function is for issuing back amount of failed p2c to merchant
      merchant = User.where(id: merchant_id).first
      if merchant.present?
        tango_orders = merchant.tango_orders.where(status: "FAILED", order_type: "instant_pay")
        if tango_orders.present?
          escrow_wallet = Wallet.check_escrow.first
          tango_orders.each do |tango_order|
            transfer_details = {
                amount: tango_order.amount,
                memo: "issuing back failed p2c",
                date: Time.now,
            }
            seq_tx = SequenceLib.transfer(tango_order.amount, escrow_wallet.id, tango_order.wallet_id, "Failed P2C", 0, nil, TypesEnumLib::GatewayType::Quickcard, nil, nil, nil,nil, nil,nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, transfer_details)
            if seq_tx.present?
              receiver = Wallet.where(id: tango_order.wallet_id).first.try(:users).try(:first)
              tx = Transaction.create(
                  sender_wallet_id: escrow_wallet.id,
                  sender_name: escrow_wallet.name,
                  receiver_wallet_id: tango_order.wallet_id,
                  receiver_id: receiver.try(:id),
                  receiver_name: receiver.try(:name),
                  amount: tango_order.amount,
                  net_amount: tango_order.amount,
                  total_amount: tango_order.amount,
                  fee: 0,
                  net_fee: 0,
                  main_type: "Failed P2C",
                  status: "approved",
                  timestamp: seq_tx.timestamp,
                  seq_transaction_id: seq_tx.actions.first.id,
                  tags: seq_tx.actions.first.tags,
                  action: "transfer",
                  )
              parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              puts "transaction id: ",tx.id
          end
          end
        end
      end
    end

    def charge_monthly_fee(location_key)
      begin
        if location_key.present?
          job = CronJob.new(name: 'monthly_fee_worker', description: 'running monthly fee', success: true)
          result = {service_fee_charged: [],statement_fee_charged: [], misc_fee_charged: [],service_fee_not_charged: [],statement_fee_not_charged: [], misc_fee_not_charged: [], error: nil}
          location = Location.where(location_secure_token: location_key).first
          if location.present?
            merchant = location.merchant
            fee_object = location.fees.buy_rate.first
            if fee_object.present?
              fee_class = Payment::FeeCommission.new(merchant, nil, fee_object, location.wallets.primary.first, location,TypesEnumLib::CommissionType::Monthly)
              service_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Service) if fee_class.present?
              statement_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Statement) if fee_class.present?
              misc_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Misc) if fee_class.present?
              escrow_wallet = Wallet.escrow.first.id
              qc_wallet = Wallet.qc_support.first.id
              if service_fee.present? && service_fee["amount"].to_f > 0
                gbox_fee = service_fee.try(:[],"splits").try(:[],"gbox").try(:[], "service").to_f
                iso_fee = service_fee.try(:[],"splits").try(:[],"iso").try(:[], "service").to_f
                agent_fee = service_fee.try(:[],"splits").try(:[],"agent").try(:[], "service").to_f
                affiliate_fee = service_fee.try(:[],"splits").try(:[],"affiliate").try(:[], "service").to_f
                tags = {"fee_perc" => service_fee["splits"]}
                db_transaction = create_transaction_helper_for_monthly_fee(escrow_wallet, location.wallets.primary.first.id, merchant,Wallet.escrow.first.users.first,"transfer",service_fee["amount"],TypesEnumLib::TransactionSubType::Service,TypesEnumLib::CommissionType::Monthly, gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
                trans = SequenceLib.transfer_monthly_fees(service_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Service, escrow_wallet, qc_wallet, service_fee["splits"], merchant, location)
                if trans.present?
                  result[:service_fee_charged] << location.id
                  transaction_update(db_transaction,trans.actions.first)
                  parsed_transactions = parse_block_transactions(trans.actions, trans.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                else
                  result[:service_fee_not_charged] << location.id
                end
              end
              if statement_fee.present? && statement_fee["amount"] > 0
                gbox_fee = statement_fee.try(:[],"splits").try(:[],"gbox").try(:[], "statement").to_f
                iso_fee = statement_fee.try(:[],"splits").try(:[],"iso").try(:[], "statement").to_f
                agent_fee = statement_fee.try(:[],"splits").try(:[],"agent").try(:[], "statement").to_f
                affiliate_fee = statement_fee.try(:[],"splits").try(:[],"affiliate").try(:[], "statement").to_f
                tags = {"fee_perc" => statement_fee["splits"]}
                db_transaction = create_transaction_helper_for_monthly_fee(escrow_wallet, location.wallets.primary.first.id, merchant,Wallet.escrow.first.users.first,"transfer",statement_fee["amount"],TypesEnumLib::TransactionSubType::Statement,TypesEnumLib::CommissionType::Monthly, gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
                trans = SequenceLib.transfer_monthly_fees(statement_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Statement, escrow_wallet, qc_wallet, statement_fee["splits"], merchant, location)
                if trans.present?
                  result[:statement_fee_charged] << location.id
                  transaction_update(db_transaction,trans.actions.first)
                  parsed_transactions = parse_block_transactions(trans.actions, trans.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                else
                  result[:statement_fee_not_charged] << location.id
                end
              end
              if misc_fee.present? && misc_fee["amount"] > 0
                gbox_fee = misc_fee.try(:[],"splits").try(:[],"gbox").try(:[], "misc").to_f
                iso_fee = misc_fee.try(:[],"splits").try(:[],"iso").try(:[], "misc").to_f
                agent_fee = misc_fee.try(:[],"splits").try(:[],"agent").try(:[], "misc").to_f
                affiliate_fee = misc_fee.try(:[],"splits").try(:[],"affiliate").try(:[], "misc").to_f
                tags = {"fee_perc" => misc_fee["splits"]}

                db_transaction = create_transaction_helper_for_monthly_fee(escrow_wallet, location.wallets.primary.first.id, merchant,Wallet.escrow.first.users.first,"transfer",statement_fee["amount"],TypesEnumLib::TransactionSubType::Misc,TypesEnumLib::CommissionType::Monthly, gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
                trans = SequenceLib.transfer_monthly_fees(misc_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Misc, escrow_wallet, qc_wallet, misc_fee["splits"], merchant, location)
                if trans.present?
                  result[:misc_fee_charged] << location.id
                  transaction_update(db_transaction,trans.actions.first)
                  parsed_transactions = parse_block_transactions(trans.actions, trans.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                else
                  result[:misc_fee_not_charged] << location.id
                end
              end
            else
              puts "Please set fees for this location!"
            end
          else
            puts "No location Found!"
          end
          job.result = result
          job.monthly_fee!
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
      end
    end

    def refund_txs(charge_ids = nil, tx_ids = nil)
      begin
        job = CronJob.new(name: 'refund_task', description: 'Refunding task because of seq down', success: true)
        object = {updates: [], error: nil}
        if charge_ids.present?
          txs = Transaction.where(charge_id: charge_ids, main_type: TypesEnumLib::TransactionViewTypes::SaleIssue)
        elsif tx_ids.present?
          txs = Transaction.where(id: tx_ids, main_type: TypesEnumLib::TransactionViewTypes::SaleIssue)
        end
        if txs.present?
          txs.each do |tx|
            balance = SequenceLib.balance(tx.receiver_wallet_id)
            if balance.present? && balance.to_f > 0
              refund =  task_refund_transaction(tx)
              if refund.first
                object[:updates].push({id: tx.id, status: true})
                if balance.present? && balance.to_f > 0
                  pg = tx.payment_gateway
                  manual_tx = Transaction.create(
                      status: "pending",
                      sender_wallet_id: tx.receiver_wallet_id,
                      sender_id: tx.receiver_id,
                      sender_name: tx.receiver.try(:name),
                      amount: tx.total_amount,
                      total_amount: tx.total_amount,
                      net_amount: tx.total_amount,
                      last4: tx.last4,
                      first6: tx.first6,
                      action: "retire",
                      main_type: "refund_bank"
                  )
                  seq_retire = SequenceLib.retire(tx.total_amount,tx.receiver_wallet_id, 'refund_bank',0,0,pg.try(:name) || "Quickcard",nil,nil,nil,tx.last4,nil,nil,nil,nil,nil,nil,nil,nil, nil, pg.try(:name))
                  if seq_retire.present?
                    SequenceLib.balance(tx.receiver_wallet_id)
                    manual_tx.update(
                        status: "manual-refund-dan",
                        timestamp: seq_retire.timestamp,
                        seq_transaction_id: seq_retire.actions.first.id,
                        tags: seq_retire.actions.first.tags
                    )
                    transactions = parse_block_transactions(seq_retire.actions, seq_retire.timestamp)
                    save_block_trans(transactions)
                  end
                end
              else
                object[:updates].push({id: tx.id, status: false})
              end
            end
          end
        end
      rescue StandardError => exc
        job.success = false
        object[:error] = "Error: #{exc.message}"
        puts 'Error occured during refund: ', exc
      end
      job.result = object
      job.rake_task!
    end

    def task_refund_transaction(transaction)
      transaction_id = transaction.charge_id
      bank_amount = transaction.total_amount.to_f
      payment_gateway = transaction.payment_gateway
      order_bank = OrderBank.where(transaction_id: transaction.id).try(:first)
      return [false,"Refund Unsuccessful"] if payment_gateway.blank?
      if bank_amount > 0
        if payment_gateway.stripe?
          card_token = CardToken.new
          refund = card_token.stripe_refund(transaction_id, bank_amount, payment_gateway)
          if refund["status"] != "succeeded"
            puts "stripe failed to refund #{transaction.id}"
            return [false,"Refund Unsuccessful"]
          else
            transaction.update(status: "manual-refund-dan")
            order_bank.update(status: "manual-refund-dan") if order_bank.present?
            return [true,"Refund successful"]
          end
        elsif payment_gateway.converge?
          elavon = Payment::ElavonGateway.new
          refund = elavon.refund(transaction_id, bank_amount,payment_gateway)
          if refund["message"].present?
            puts "converge failed to refund #{transaction.id} : #{refund["message"]}"
            return [false, "Refund Unsuccessful"]
          else
            transaction.update(status: "manual-refund-dan")
            order_bank.update(status: "manual-refund-dan") if order_bank.present?
            return [true, "Refund successful"]
          end
        elsif payment_gateway.bolt_pay?
          bolt_pay = Payment::BoltPayGateway.new
          refund = bolt_pay.refund(transaction_id, bank_amount, payment_gateway)
          if refund[:message].present?
            puts "boltPay failed to refund #{transaction.id} : #{refund["message"]}"
            return [false, "Refund Unsuccessful"]
          else
            transaction.update(status: "manual-refund-dan")
            order_bank.update(status: "manual-refund-dan") if order_bank.present?
            return [true, "Refund successful"]
          end
        elsif payment_gateway.i_can_pay? || payment_gateway.payment_technologies?
          iCanPay = Payment::ICanPay.new(payment_gateway)
          refund = iCanPay.refund(transaction_id, bank_amount,request)
          if refund["message"].present?
            puts "icanpay failed to refund #{transaction.id} : #{refund["message"]}"
            return [false, "Refund Unsuccessful"]
          else
            transaction.update(status: "manual-refund-dan")
            order_bank.update(status: "manual-refund-dan") if order_bank.present?
            return [true, "Refund successful"]
          end
        elsif payment_gateway.fluid_pay?
          fluid_pay = FluidPayHelper::FluidPayPayment.new
          fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
          if (fluid_transaction_status != "settled" && fluid_transaction_status != "refunded")
            refund = fluid_pay.voidFluidPay(transaction_id,payment_gateway)
          else
            refund = fluid_pay.refundFluidAmount(transaction_id,SequenceLib.cents(bank_amount),payment_gateway)
          end
          if refund["status"] != "success"
            puts "fluid pay failed to refund #{transaction.id}"
            return [false,"Refund Unsuccessful"]
          else
            transaction.update(status: "manual-refund-dan")
            order_bank.update(status: "manual-refund-dan") if order_bank.present?
            return [true,"Refund successful"]
          end
        elsif bank_amount > 0
          puts "payment gateway not found failed to refund #{transaction.id}"
          return [false,"Refund Unsuccessful Payment Method Not Supported"]
        end
      end
    end

    def create_transfer(seq_id)
      begin
        if seq_id.present?
          sequence_tx = SequenceLib.get_transaction_id(seq_id)
          if sequence_tx.present?
            main_tx = sequence_tx.first.actions.first
            sender_wallet = Wallet.where(id: main_tx.source_account_id).first
            sender_user = sender_wallet.users.first if sender_wallet.present?
            receiver_wallet = Wallet.where(id: main_tx.destination_account_id).first
            receiver_user = receiver_wallet.users.first if receiver_wallet.present?
            fee_perc = main_tx.tags.try(:[],"fee_perc")
            main_type = updated_type(main_tx.tags[:type], main_tx.tags.try(:[],"sub_type"))
            pg = PaymentGateway.where(name: main_tx.tags.try(:[],"descriptor")).first
            pg = PaymentGateway.where(key: main_tx.tags.try(:[],"source")).first if pg.blank?
            card = Card.where(id: main_tx.tags.try(:[],"previous_issue").try(:[],"tags").try(:[],"card").try(:[],"id")).first
            user_fee = main_tx.tags.try(:[], "gateway_fee_details")
            Transaction.create(
                           sender_id: sender_user.try(:id),
                           receiver_id: receiver_user.try(:id),
                           sender_wallet_id: main_tx.source_account_id,
                           receiver_wallet_id: main_tx.destination_account_id,
                           amount: SequenceLib.dollars(main_tx.amount),
                           total_amount: SequenceLib.dollars(main_tx.amount).to_f + main_tx.tags.try(:[],"privacy_fee").to_f + main_tx.tags.try(:[],"tip").to_f,
                           fee: main_tx.tags.try(:[],"fee"),
                           net_fee: main_tx.tags.try(:[], "fee").try(:to_f) - main_tx.tags.try(:[],"privacy_fee").to_f,
                           net_amount: SequenceLib.dollars(main_tx.amount).to_f - main_tx.tags.try(:[], "fee").to_f - main_tx.tags.try(:[],"tip").to_f - fee_perc.try(:[], "reserve").try(:[],"amount").to_f,
                           privacy_fee: main_tx.tags.try(:[],"privacy_fee").try(:to_f),
                           tip: main_tx.tags.try(:[],"tip").try(:to_f),
                           reserve_money: main_tx.tags.try(:[],"reserve").try(:[],"amount").to_f,
                           action: "transfer",
                           main_type: main_type,
                           sub_type: main_tx.tags.try(:[],"sub_type"),
                           seq_transaction_id: main_tx.id,
                           timestamp: sequence_tx.first.timestamp,
                           status: "approved",
                           ip: main_tx.tags.try(:[],"ip"),
                           card_id: card.try(:id),
                           payment_gateway_id: pg.try(:id),
                           load_balancer_id: pg.try(:load_balancer_id),
                           first6: card.try(:first6),
                           last4: card.try(:last4),
                           tags: main_tx.tags,
                           charge_id: main_tx.tags.try(:[],"previous_issue").try(:[],"transaction_id"),
                           gbox_fee: fee_perc.try(:[], "gbox").try(:[], "amount"),
                           iso_fee: fee_perc.try(:[], "iso").try(:[], "amount"),
                           agent_fee: fee_perc.try(:[], "agent").try(:[], "amount"),
                           affiliate_fee: fee_perc.try(:[], "affiliate").try(:[], "amount"),
                           account_processing_limit: user_fee.try(:[],"account_processing_limit"),
                           transaction_fee: user_fee.try(:[],"transaction_fee"),
                           charge_back_fee: user_fee.try(:[],"charge_back_fee"),
                           retrieval_fee: user_fee.try(:[],"retrieval_fee"),
                           per_transaction_fee: user_fee.try(:[],"per_transaction_fee"),
                           reserve_money_fee: user_fee.try(:[],"reserve_money_fee"),
                           reserve_money_days: user_fee.try(:[],"reserve_money_days"),
                           monthly_service_fee: user_fee.try(:[],"monthly_service_fee"),
                           misc_fee: user_fee.try(:[],"misc_fee"),
                           misc_fee_dollars: user_fee.try(:[],"misc_fee_dollars")
            )
            transactions = parse_block_transactions(sequence_tx.first.actions, sequence_tx.first.timestamp)
            save_block_trans(transactions)
          end
        else
          puts "Please provide sequence id"
        end
      rescue => exc
        puts "Error occured: ", exc.message
        puts "backtrace: ", exc.backtrace
      end
    end

    def create_block_transaction(sequence_id)
      begin
        if sequence_id.present?
          block_tx = BlockTransaction.where(sequence_id: sequence_id).first
          if block_tx.blank?
            sequence_tx = SequenceLib.get_transaction_id(sequence_id)
            if sequence_tx.present?
              transactions = parse_block_transactions(sequence_tx.first.actions, sequence_tx.first.timestamp)
              save_block_trans(transactions)
              puts "successfully created"
            end
          else
            puts "already exists"
          end
        end
      rescue => exc
        puts "Error Occured", exc.message
      end
    end

    def create_missing_transaction(sequence_ids)
      begin
        cron_job = CronJob.create(name: "transaction/orderbank")
        result = {updates: []}
        sequence_ids.each do |sequence_id|
          block_transaction = BlockTransaction.where(sequence_id: sequence_id).first
          transaction = Transaction.where(seq_transaction_id: sequence_id).first
          if block_transaction.present? && transaction.blank?
            fee_perc = block_transaction.tags.try(:[],"fee_perc")
            pg = block_transaction.payment_gateway
            pg = PaymentGateway.where(name: block_transaction.tags.try(:[],"descriptor")).first if pg.blank? && block_transaction.payment_gateway.blank?
            pg = PaymentGateway.where(key: block_transaction.tags.try(:[],"source")).first if pg.blank? && block_transaction.payment_gateway.blank?
            card = block_transaction.card
            card = Card.where(id: block_transaction.tags.try(:[],"previous_issue").try(:[],"tags").try(:[],"card").try(:[],"id")).first if card.blank? && block_transaction.card.blank?
            user_fee = block_transaction.tags.try(:[], "gateway_fee_details")
            transaction = Transaction.create(
                sender_id: block_transaction.sender_wallet.try(:users).try(:first).try(:id),
                receiver_id: block_transaction.receiver_wallet.try(:users).try(:first).try(:id),
                sender_wallet_id: block_transaction.sender_wallet_id,
                receiver_wallet_id: block_transaction.receiver_wallet_id,
                amount: SequenceLib.dollars(block_transaction.tx_amount),
                total_amount: SequenceLib.dollars(block_transaction.amount_in_cents).to_f,
                fee: SequenceLib.dollars(block_transaction.fee_in_cents),
                net_fee: block_transaction.net_fee,
                net_amount: block_transaction.total_net,
                privacy_fee: block_transaction.privacy_fee,
                tip: SequenceLib.dollars(block_transaction.tip_cents),
                reserve_money: SequenceLib.dollars(block_transaction.hold_money_cents),
                action: "issue",
                main_type: block_transaction.main_type,
                sub_type: block_transaction.sub_type,
                seq_transaction_id: block_transaction.sequence_id,
                timestamp: block_transaction.timestamp,
                status: "approved",
                ip: block_transaction.ip,
                card_id: card.try(:id),
                payment_gateway_id: pg.try(:id),
                load_balancer_id: pg.try(:load_balancer_id),
                first6: card.try(:first6),
                last4: card.try(:last4),
                tags: block_transaction.tags,
                charge_id: block_transaction.charge_id || block_transaction.tags.try(:[],"previous_issue").try(:[],"transaction_id"),
                gbox_fee: fee_perc.try(:[], "gbox").try(:[], "amount"),
                iso_fee: fee_perc.try(:[], "iso").try(:[], "amount"),
                agent_fee: fee_perc.try(:[], "agent").try(:[], "amount"),
                affiliate_fee: fee_perc.try(:[], "affiliate").try(:[], "amount"),
                account_processing_limit: user_fee.try(:[],"account_processing_limit"),
                transaction_fee: user_fee.try(:[],"transaction_fee"),
                charge_back_fee: user_fee.try(:[],"charge_back_fee"),
                retrieval_fee: user_fee.try(:[],"retrieval_fee"),
                per_transaction_fee: user_fee.try(:[],"per_transaction_fee"),
                reserve_money_fee: user_fee.try(:[],"reserve_money_fee"),
                reserve_money_days: user_fee.try(:[],"reserve_money_days"),
                monthly_service_fee: user_fee.try(:[],"monthly_service_fee"),
                misc_fee: user_fee.try(:[],"misc_fee"),
                misc_fee_dollars: user_fee.try(:[],"misc_fee_dollars"),
                created_at: block_transaction.timestamp,
                sender_name: block_transaction.sender_name || block_transaction.sender.try(:name),
                receiver_name: block_transaction.receiver_name || block_transaction.receiver.try(:name)
            )
            order_bank = OrderBank.where(transaction_id: transaction.id).last
            if transaction.present? && order_bank.blank?
              order_bank = OrderBank.create(
                  user_id: transaction.sender_id,
                  merchant_id: transaction.receiver_id,
                  bank_type: pg.try(:name),
                  card_type: card.try(:brand),
                  status: "approve",
                  amount: transaction.total_amount,
                  created_at: transaction.timestamp,
                  card_sub_type: card.try(:card_type),
                  transaction_id: transaction.id,
                  payment_gateway_id: pg.try(:id)
              )
            end
            result[:updates].push({transaction_id: transaction.id, order_bank: order_bank.try(:id)})
          end
        end
        cron_job.result = result
        cron_job.rake_task!
        puts "created transactions successfully"
      rescue => exc
        puts "Error occured: ", exc.message
        puts 'backtrace: ', exc.backtrace
      end
    end

    def get_terminal_ids(wallet_id, first_date, second_date)
      terminal_ids = []
      Transaction.where(receiver_wallet_id: wallet_id, main_type: ["Credit Card","PIN Debit"]).where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).find_in_batches(batch_size: 1000) do |block|
        if block.present?
          block.each do |transaction|
            terminal_id = transaction.tags.try(:[], "sales_info").try(:[], "terminal_id")
            if transaction.terminal_id.present?
              terminal_ids = terminal_ids.push(transaction.terminal_id)
            elsif terminal_id.present?
              terminal_ids = terminal_ids.push(terminal_id)
            elsif terminal_id.blank?
              activity = transaction.activity_logs.first
              if activity.present?
                params = activity.params
                if params.present? && params["terminal_id"].present?
                  terminal_ids = terminal_ids.push(params["terminal_id"])
                end
              end
            end
          end
        end
      end
      puts "terminal ids: ", terminal_ids
    end

    def charge_excessive_cbk_fine(amount, to_wallet_id, from_wallet_id)
        begin
          memo = "Asked by Dan to Charge Fine"
          main_type = I18n.t("types.transaction.excessive_cbk_fine")
          transfer_details = {
              amount: amount,
              memo: memo,
              date: DateTime.now,
              transfer_timezone: nil
          }
          sender_balance = SequenceLib.balance(from_wallet_id)
          receiver_balance = SequenceLib.balance(to_wallet_id)
          if sender_balance >= amount.to_f
            seq_tx = SequenceLib.transfer(amount,from_wallet_id, to_wallet_id, main_type, 0, nil, TypesEnumLib::GatewayType::Quickcard, nil,nil,nil, nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
            if seq_tx.present?
              sender_wallet = Wallet.where(id: from_wallet_id).first
              sender = sender_wallet.try(:users).try(:first)
              receiver = Wallet.where(id: to_wallet_id).first.try(:users).try(:first)
              location = sender_wallet.try(:location)

              tx = Transaction.create(
                  sender_wallet_id: from_wallet_id,
                  receiver_wallet_id: to_wallet_id,
                  sender_id: sender.try(:id),
                  receiver_id: receiver.try(:id),
                  sender_name: location.try(:business_name) || sender.try(:name),
                  receiver_name: receiver.try(:name),
                  sender_balance: sender_balance,
                  receiver_balance: receiver_balance,
                  amount: amount,
                  net_amount: amount,
                  total_amount: amount,
                  fee: 0,
                  net_fee: 0,
                  main_type: main_type,
                  status: "approved",
                  timestamp: seq_tx.timestamp,
                  seq_transaction_id: seq_tx.actions.first.id,
                  tags: seq_tx.actions.first.tags,
                  action: "transfer",
                  )
              parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              puts "transaction id: ",tx.id
              puts "seq transaction id", tx.seq_transaction_id
              puts "sender wallet id",tx.sender_wallet_id
              puts "receiver wallet id", tx.receiver_wallet_id
              puts "amount", tx.amount
            end
          else
            puts "Sender Balance is less than amount sender_balance: #{sender_balance} , amount_fined: #{amount}"
          end
        rescue => exc
          puts exc.message
          puts exc.backtrace
        end

    end

    def transfer_wallets(amount, main_type, to_wallet_id, from_wallet_id, memo = nil)
      begin
        transfer_details = {
            amount: amount,
            memo: memo,
            date: DateTime.now,
            transfer_timezone: nil
        }
        sender_balance = SequenceLib.balance(from_wallet_id)
        receiver_balance = SequenceLib.balance(to_wallet_id)
        seq_tx = SequenceLib.transfer(amount,from_wallet_id, to_wallet_id, main_type, 0, nil, TypesEnumLib::GatewayType::Quickcard, nil,nil,nil, nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
        if seq_tx.present?
          sender = Wallet.where(id: from_wallet_id).first.try(:users).try(:first)
          receiver = Wallet.where(id: to_wallet_id).first.try(:users).try(:first)
          tx = Transaction.create(
              sender_wallet_id: from_wallet_id,
              receiver_wallet_id: to_wallet_id,
              sender_id: sender.try(:id),
              receiver_id: receiver.try(:id),
              sender_name: sender.try(:name),
              receiver_name: receiver.try(:name),
              sender_balance: sender_balance,
              receiver_balance: receiver_balance,
              amount: amount,
              net_amount: amount,
              total_amount: amount,
              fee: 0,
              net_fee: 0,
              main_type: main_type,
              status: "approved",
              timestamp: seq_tx.timestamp,
              seq_transaction_id: seq_tx.actions.first.id,
              tags: seq_tx.actions.first.tags,
              action: "transfer",
              )
          parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          puts "transaction id: ",tx.id
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
      end
    end

    def lost_ckb(date)
      begin
        if date.present?
          job = CronJob.new(name: 'cbk_lost', description: 'update disputes status', success: true)
          result = {updates: [], error: nil}
          from_id = Wallet.charge_back.last
          DisputeCase.charge_back.where('created_at <= ?', date).each do |cas|
            if cas.present? && cas.sent_pending?
              merchant = cas.merchant_wallet.location.merchant
              to_id = cas.merchant_wallet_id
              dispute_case_body = {
                  case_number: cas.case_number,
                  amount: cas.amount,
                  merchant_wallet_id: cas.merchant_wallet_id,
                  last4: cas.card_number.present? ? cas.card_number : "NA"
              }
              charge_back_amount = cas.amount.to_f
              cbk_balance = SequenceLib.balance(from_id.id).to_f
              if charge_back_amount > 0 && (cbk_balance.to_f > 0 && cbk_balance.to_f >= charge_back_amount.to_f)
                transaction = Transaction.create(to: to_id, from: from_id.id, main_type: "cbk_lost",sender_balance: cbk_balance, sender_wallet_id: from_id.id, status: "CBK Lost", charge_id: nil, amount: charge_back_amount, total_amount: charge_back_amount, net_amount: charge_back_amount, sender_id: from_id.try(:users).try(:first).try(:id), action: 'retire')
                cbk_lost = SequenceLib.retire(number_with_precision(charge_back_amount,precision:2),from_id.id,"charge_back",0,"cbk_lost",TypesEnumLib::GatewayType::Quickcard,nil, merchant.id,nil,nil,nil,nil,nil,nil,nil,"cbk_lost",nil,nil,nil,nil,dispute_case_body)
                if cbk_lost.present?
                  transaction.update(tags: cbk_lost.actions.first.tags, seq_transaction_id: cbk_lost.actions.first.id, timestamp: cbk_lost.timestamp)
                  parsed_transactions = parse_block_transactions(cbk_lost.actions, cbk_lost.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                  cas.update(dispute_result_transaction_id: transaction.id, status: 'lost')
                  result[:updates].push({dispute_id: cas.id, sequence_id: transaction.seq_transaction_id, case_number: cas.card_number})
                end
              end
            end
          end
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
        job.success = false
        result[:error] = "Error: #{exc.message}"
      ensure
        job.result = result if job.present?
        job.rake_task! if job.present?
        return job.result
      end
    end

    def won_ckb(case_numbers)
      begin
        if case_numbers.present?
          job = CronJob.new(name: 'cbk_won', description: 'update disputes status', success: true)
          result = {updates: [], error: nil}
          DisputeCase.where(case_number: case_numbers).each do |cas|
            if cas.present? && cas.lost?
              location = cas.merchant_wallet.location
              merchant = location.merchant
              to_id = cas.merchant_wallet_id
              dispute_case_body = {
                  case_number: cas.case_number,
                  amount: cas.amount,
                  merchant_wallet_id: cas.merchant_wallet_id,
                  last4: cas.card_number.present? ? cas.card_number : "NA",
                  memo: "asked by Dan to make them won from lost.",
                  old_lost_id: cas.dispute_result_transaction_id
              }
              main_type = "cbk_won"
              charge_back_amount = cas.amount.to_f

              seq_tx  = SequenceLib.issue(to_id, charge_back_amount, nil, main_type, 0, nil, TypesEnumLib::GatewayType::Quickcard,nil,nil, merchant.name,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,dispute_case_body)
              tx = Transaction.create(
                  receiver_wallet_id: to_id,
                  receiver_id: merchant.try(:id),
                  receiver_name: location.business_name || merchant.try(:name),
                  amount: charge_back_amount,
                  net_amount: charge_back_amount,
                  total_amount: charge_back_amount,
                  fee: 0,
                  net_fee: 0,
                  main_type: main_type,
                  status: "approved",
                  timestamp: seq_tx.timestamp,
                  seq_transaction_id: seq_tx.actions.first.id,
                  tags: seq_tx.actions.first.tags,
                  action: "issue",
                  )
              parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              puts "transaction id: ",tx.id
              cas.update(dispute_result_transaction_id: tx.id, status: 'won_reversed')
              result[:updates].push({dispute_id: cas.id, sequence_id: tx.seq_transaction_id, tx_id:tx.id, case_number: cas.case_number})

            end
          end
        end
      rescue => exc
        puts exc.message
        puts exc.backtrace
        job.success = false
        result[:error] = "Error: #{exc.message}"
      ensure
        job.result = result if job.present?
        job.rake_task! if job.present?
        return job.result
      end
    end

    def refund_manually(seq_id, reason = nil, refunded_by = nil)
      #task for refunding credit card transactions without refunding on bank
      begin
        job = CronJob.new(name: 'refund_manually', description: 'refund without bank', success: true)
        object = {updates: [], error: nil}
        tx = Transaction.where(seq_transaction_id: seq_id).first
        if tx.present?
          merchant_wallet = tx.receiver_wallet
          merchant = tx.receiver
          location = merchant_wallet.location
          merchant_fee = location.fees.buy_rate.first
          payment_gateway = tx.payment_gateway
          fee_class = Payment::FeeCommission.new(merchant, nil, merchant_fee, merchant_wallet, location,TypesEnumLib::TransactionType::RefundFee)
          fee_object = fee_class.apply(tx.total_amount)
          if fee_object.present?
            fee_splits = fee_object["splits"].present? ? fee_object["splits"] : nil
            refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
          end
          transaction_id = nil
          if tx.tags["previous_issue"].present?
            transaction_id = tx.tags["previous_issue"]["transaction_id"] if tx.tags["previous_issue"]["transaction_id"].present?
            transaction_id = tx.tags["previous_issue"]["fluid_transaction_id"] if tx.tags["previous_issue"]["fluid_transaction_id"].present?
          else
            transaction_id = tx.charge_id
          end
          lastFour = tx.tags["last4"] ? tx.tags["last4"] : "NA"
          bank_details = {
              transaction_id: transaction_id,
              amount: dollars_to_cents(tx.total_amount),
              last4: lastFour
          }
          refund_details = {
              parent_transaction_id: tx[:seq_transaction_id],
              reason: reason
          }
          gateway_name=payment_gateway.try(:name)
          if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
            gateway_name = tx.tags.try(:[], "source").present? ? tx.tags["source"] : gateway_name
          end
          tags = {
              "bank_details" => bank_details,
              "refund_details" => refund_details,
              "fee_perc" => fee_splits,
              "descriptor" => gateway_name,
              "location" => tx.tags.try(:[], "location"),
              "merchant" => tx.tags.try(:[], "merchant")
          }
          gbox_fee = save_gbox_fee(tags["fee_perc"], refund_fee, "refund")
          total_fee = save_total_fee(tags["fee_perc"], refund_fee, "refund")
          retire_refund = create_refund_transaction(tx.receiver_wallet_id, nil, tx.total_amount, total_fee.to_f, tags, 'retire', 'refund', payment_gateway,nil,refunded_by, tx.try(:card).try(:id), tx.try(:card).try(:last4), tx.try(:card).try(:first6), nil, gbox_fee)
          source = tx.tags["source"] ? tx.tags["source"].downcase : "NA"
          #----------------------------Perform Sequence Retire Refund--------------------------------
          descriptor = payment_gateway.try(:name)
          if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
            descriptor = tx.tags.try(:[],'descriptor').present? ? tx.tags["descriptor"] : descriptor
          end
          seq_transaction = SequenceLib.retire(tx.total_amount,tx.receiver_wallet_id, 'refund',refund_fee,0,source,nil,merchant,location,lastFour,nil,fee_splits,nil,nil,nil,nil,bank_details,refund_details, nil, descriptor, nil, nil,nil, payment_gateway)
          if seq_transaction.present?
            update_refund_transaction(retire_refund, seq_transaction)
            update_log(tx,seq_transaction["actions"].first["id"])
            #= creating block transaction
            transactions = parse_block_transactions(seq_transaction.actions, seq_transaction.timestamp)
            save_block_trans(transactions)
            tx.update(status: 'refunded',refund_reason: reason,refunded_by: refunded_by)
          end
        end
      rescue StandardError => exc
        job.success = false
        object[:error] = "Error: #{exc.message}"
        puts 'Error occured during refund: ', exc
      ensure
        job.result = object
        job.rake_task!
      end
    end

  end

end