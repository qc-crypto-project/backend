require 'base64'
require 'digest'
require 'openssl'


module DESCrypt

  def DESCrypt.encrypt(password, plain_text)
    secret = Digest::MD5.digest(password)
    secret += secret[0..7]
    cipher = OpenSSL::Cipher.new 'des-ede3'
    cipher.key = secret
    cipher.encrypt
    encrypted = cipher.update(plain_text)
    encrypted = encrypted + cipher.final
    return Base64.encode64(encrypted).gsub(/\n/, "")
  end

  def DESCrypt.decrypt_dd(password, secretdata)
    secret = Digest::MD5.digest(password)
    secret += secret[0..7]
    cipher = OpenSSL::Cipher.new 'des-ede3'
    cipher.key = secret
    cipher.decrypt
    encrypted = cipher.update(secretdata)
    encrypted = encrypted + cipher.final
    return Base64.encode64(encrypted).gsub(/\n/, "")
    # iv = "AAAAaaaaQQQQqqqq"
    # secretdata = Base64::decode64(secretdata)
    # decipher = OpenSSL::Cipher::Cipher.new('aes-256-cbc')
    # password = Digest::SHA256.digest(password)
    # decipher.decrypt
    # decipher.key = password
    # decipher.iv = iv
    # decipher.update(secretdata) + decipher.final
  end

  def DESCrypt.b64enc(data)
    Base64.encode64(data).gsub(/\n/, '')
  end

end