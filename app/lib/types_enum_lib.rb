class TypesEnumLib
  module TransactionType
    SaleType = 'sale'
    Sales = 'sales'
    SendCheck = 'send_check'
    CheckDeposit = 'check deposit'
    PushtoCardDeposit = 'push to card deposit'
    DebitCardDeposit = 'instant_pay'
    BulkCheck = 'bulk_check'
    GiftCardPurchase = 'giftcard_purchase'
    GiftCard = "giftcard"
    ACHdeposit = 'ACH_deposit'
    InstantAch = 'instant_ach'
    VoidCheck = 'Void_Check'
    VoidAch = 'Void_Ach'
    FailedAch = 'Failed_Ach'
    FailedAchFee = 'Failed_Ach_Fee'
    FailedPushtoCard = 'Failed_Push_To_Card'
    FailedCheck = 'Failed_Check'
    B2b = 'b2b_transfer'
    InvoiceQC = 'invoice_qc'
    Reserve = 'Reserve_Money_Deposit'
    ReserveTransfer = 'Reserve_Money_Return'
    PayedTip = 'payed_tip'
    FeeTransfer = 'Fee_Transfer'
    Fee = 'fee'
    ApproveRequest = 'approve_request_issue'
    RetrievelFee = 'retrievel_fee'
    CreditTransaction = 'Credit'
    DebitTransaction = 'Debit'
    RefundFee = 'Refund Fee'
    Service = "Service Fee"
    Misc = "Misc Fee"
    Statement = "Subscription Fee"
    RefundBank = "refund_bank"
    Refund = "refund"
    P2P = "p2p_payment"
    ManualRetire = "manual_retire"
    Cbk_retire_gbox = "CBK Lost Retire"
    BuyRateFee = "Buy Rate Fee"
    AccountTransfer = "account_transfer"
    CbkHold = "cbk_hold"
    VoidPushtoCard="void_push_to_card"
    RewardIssue='reward issue'
    RewardTransfer='reward transfer'
    PinDebitIssue = 'Pin Debit Issue'
    QCSecure = "QC Secure"
    Transfer3DS = "QCP Secure"
    InvoiceCreditCard = "Invoice - Credit Card"
    InvoiceDebitCard = "Invoice - Debit Card"
    InvoiceRTP = "Invoice - RTP"
    RTP = "RTP"
    Issue3DS = "Sale Issue"
    TrackingFee = "Tracking Fee"
    GenerateLabelFee = "Generate Label Fee"
    TipTransfer = "Tip Transfer"
    DebitCard = 'Debit Card'
    B2bFee = "B2B Fee"
    ACH = 'ach'
    AFP = "afp"
    RefundFailed = "Refund Failed"
    CbkFee = "cbk_fee"
    CbkWon = "CBK Won"
    ReserveMoneyDeposit = "Reserve money deposit"
    ##########################################
  end

  module RiskType
    Accept = 'accept'
    Reject = 'reject'
    PendingReview = 'pending_review'
    ExpiredReview = 'expired_review'
  end

  module RiskReason
    Default = 'default'
    ManualReview = 'manual_review'
  end

  module DeclineTransacitonTpe
    Ecommerce = 'eCommerce'
    VirtualTerminal = 'Virtual Terminal'
    Credit = "Credit"
    Debit = "Debit"
    DebitCard = "Debit Card"
    IssueQCSecure = "Sale Issue"
    Issue3DS = "Sale Issue"
  end

  module TransactionSubType
    SaleTip = 'sale_tip'
    SaleGiftcard = 'sale_giftcard'
    SaleVirtual = 'virtual_terminal_sale'
    SaleVirtualApi = 'virtual_terminal_sale_api'
    DebitCharge = 'debit_charge'
    SaleIssue = 'sale_issue'
    IssueQCSecure = "Sale Issue"
    Issue3DS = "Sale Issue"
    SaleIssueApi = 'sale_issue_api'
    QRCreditCard = "qr_credit_card"
    QRDebitCard = "qr_debit_card"
    VirtualTerminalTransaction = 'credit_card'
    DebitCard = "debit_card"
    Service = "Service Fee"
    Misc = "Misc Fee"
    Statement = "Subscription Fee"
    TipVtTransaction = "tip_vt_transaction_&_debit"
    GiftcardFee = "giftcard_fee"
    ICanPay3ds = 'i_can_pay_3ds'
    TipTransfer = "Tip Transfer"
  end

  module TransactionViewTypes
    Ecommerce = "eCommerce"
    VirtualTerminal = "Virtual Terminal"
    CreditCard = "Credit Card"
    PinDebit = "PIN Debit"
    SaleTip = "Sale Tip"
    Sale = "Sale"
    QrCreditCard = "QR Credit Card"
    QrDebitCard = "QR Debit Card"
    SaleIssue = "Sale Issue"
    ReserveMoneyDeposit = "Reserve Money Deposit"
    SendCheck = "Send Check"
    BulkCheck = "Bulk Check"
    VirtualTerminalSale = "virtual_terminal_sale"
    Refund = "refund"
    B2B = "B2B Transfer"
    Issue3DS = "Sale Issue"
    DebitCard = "Debit Card"
    GiftCardPurchase = 'Giftcard Purchase'
  end

  module CommissionType
    SendCheck = 'send_check'
    Failedcheck = 'failed_check'
    DebitCardDeposit = 'instant_pay'
    Failedinstant_pay = 'failed_instant_pay'
    AddMoney = 'add_money'
    C2B = 'c2b'
    B2B = 'b2b'
    QrCard = 'qr_card'
    C2C = 'c2c'
    Giftcard = 'gift_card'
    ChargeBack = 'charge_back'
    Retrieval = 'retrieval'
    TransactionFeeCcard = 'transaction_fee_ccard'
    TransactionFeeApp = 'transaction_fee_app'
    TransactionFeeDcard = 'transaction_fee_dcard'
    TransactionFeeInvoicecard = 'transaction_fee_invoice_card'
    TransactionFeeInvoiceDebitcard = 'transaction_fee_invoice_debit_card'
    TransactionFeeInvoiceRTP = 'transaction_fee_invoice_rtp'
    TransactionFeeRTP = 'transaction_fee_rtp'
    TierName = 'Tier'
    Monthly = "monthly Fee"
    ACH = 'ach'
    Failedinstant_ach = 'failed_instant_ach'
    # InstantPay = 'instant_ach'
    #####################

  end

  module Batch
    Primary = 'primary'
    Reserve = 'reserve'
    BuyRateCommission = 'buy_rate_commission'
    High = 'high'
    Low = 'low'
  end

  module GatewayType
    Checkbook = 'Checkbox.io'   #= types -> ACH Deposit
    Quickcard = 'Quickcard'     #= types -> B2b, Void_check, Payed_tip, sale, sale_giftcard, refund, Manual_retire
    PinGateway = 'Pin Gateway'  #= types -> Pin Debit
    Tango = 'Tango'  #= types -> giftcard_purchase
    FluidPay = 'fluid_pay'
    StripeGbox = 'stripe'
    StripePop = 'stripe_pop'
    StripePop2 = 'stripe_pop_2'
    BridgePay = 'bridge_pay'
    Converge = 'converge'
    Payeezy = 'payeezy'
    ICanPay = 'i_can_pay'
    PaymentTechnologies = 'payment_technologies'
    BoltPay = "bolt_pay"
    ACH = 'ach'
    InstantAch = "instant_ach"
    Stripe = "stripe"
    KnoxPayments = "knox_payments"
    TotalPay = "total_pay"
    MentomPay = "mentom_pay"
  end

  module Users
    BlockedEmail = "blocked@quickcard.com"
    Iso = "iso"
    SuperIso = "super_iso"
    Agent = "agent"
    SuperAgent = "super_agent"
    Merchant = "merchant"
    SuperMerchant = "super_merchant"
    Partner = "partner"
    Gbox = "gbox"
    SuperPartner = "super_partner"
    Company = 'company'
    Affiliate = "affiliate"
    SuperAffiliate = "super_affiliate"
  end

  module WorkerType
    Transactions = "transactions"
    Refund = "refund"
    AddMoney = "add_money"
    RequestMoney = "request_money"
    SendCheck = "send_check"
    InstantACH = "instant_ach"
    InstantPAY = "instant_pay"
    Transfer = "transfer"
    Retire = "retire"
    Giftcard = "giftcard"
    BulkCheck = "bulk_check"
    Admin = "admin"
    AdminMerchant = "admin_merchant"
    AdminMerchantProfile = "admin_merchant_profile"
    AdminUsers = "admin_users" # at admins portal => user's transaction
    #types for using in admin reports
    ChargeBack = "chargeback"
    Gateway = "gateway"
    Merchant = "merchant"
    Sale = "sale"
    Wallet = "wallet"
    AuditSale = "audit_sale"
    AuditGateway = "audit_gateway"
    FeeDetail = "fee_detail"
    FeeSummary = "fee_summary"
    Withdrawal = "withdrawal"
    IsoSummaryReport = "iso_summary"
    IsoProfitSplit = "iso_profit_split"
    TerminalID = "terminal_id"
    Dispute = "dispute"
    MerchantDispute = "merchant_dispute"
    MerchantExport = "merchant_export"
    MerchantUsersExport = "merchant_users"
    LocationExport = "location_export"
    WalletExport = "wallet_export"
    BlockVIPExport = "block_vip_export"
    PermissionExport = "permission_export"
    HoldRearExport = "hold_in_rear"
    RserveReleaseExport = "reserve_release"
    EmployeeExport = "employee_export"
    SaleBatches = "sales_batches"
    BatchTransactions = "batch_transactions"
    MerchantLocationDispute = "merchant_location_dispute"
    AffiliateProgram = "affiliate_program"
  end

  module Statuses
    Success = "approved"
    Failed = "failed"
    Declined = "declined"
    Approved = 'approved'
    Pending = 'pending'
  end

  module Actions
    Transfer = "transfer"
    Issue = "issue"
    Retire = "retire"
  end

  module PaymentType
    PreAuth = "preAuth"
    Authorize = "authorize"
  end

  module ErrorMessage
    Rebill = "Their is no initial transaction found against this customer."
    Initial = "Amount cannot exceed $15 for an initial transaction."
    PartialRefund = "You can't do partial refund within 24 hours. Please try complete refund or try again after 24 hours."
  end

  module Message
    Pending = "An export is already in progress! please wait for it to finish before starting a new export."
    CrashedHeading = "Download Incomplete!"
    Crashed = "Unfortunately, your export report could not be downloaded at this time. Our support team has been notified and are working to resolve as soon as possible. Sorry for the inconvenience and please try again later."
  end
end