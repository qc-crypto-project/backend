namespace :users do
  desc "Fix User names correctly"
  task fix_names: :environment do
    puts 'Updating User Names'
    begin
      id = nil
      job = CronJob.new(name: 'users:fix_names', description: 'Fix User Names', success: true)
      result = {updates: [], error: nil}
      User.all.each do |user|
        id = user.id
        puts "Updating User Role Enums For USER ID: #{user.id} with EMAIL: #{ user.email}"
        if user.name.blank? && user.first_name.present? && user.last_name.present?
          name = user.name
          user.update_attributes(name: "#{user.first_name} #{user.last_name}")
          result[:updates].push({id: user.id, old_name: name, new_name: user.name})
        elsif user.name.present? && user.first_name.blank? & user.last_name.blank? && user.name.split(' ').length > 1
          names = user.name.split(' ')
          user.update_attributes(first_name: names[0], last_name: names[1])
          result[:updates].push({
            id: user.id,
            old_name: {first_name: nil, last_name: nil},
            new_name: {first_name: user.first_name, last_name: user.last_name}
          })
        elsif user.name.present? && user.first_name.blank? & user.last_name.blank? && user.name.split(' ').length <= 1
          user.update_attributes(first_name: user.name)
          result[:updates].push({
            id: user.id,
            old_name: {first_name: nil},
            new_name: {first_name: user.first_name}
          })
        elsif user.name.present? && user.first_name.blank? && user.last_name.present? && user.name.split(' ').length <= 1
          user.update_attributes(first_name: user.name)
          result[:updates].push({
            id: user.id,
            old_name: {first_name: nil},
            new_name: {first_name: user.name}
          })
        elsif user.name.blank? && user.first_name.present? && user.last_name.blank?
          user.update_attributes(name: user.first_name)
          result[:updates].push({
            id: user.id,
            old_name: {name: nil},
            new_name: {name: user.first_name}
          })
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during ENUM Update: ', exc
    end
    job.result = result
    job.rake_task!
    puts 'User Names Fixed Successfully!'
  end
end
