namespace :sequence_accounts do

  desc "update account tags"
  task update_location_account_tags: :environment do
    ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential:  ENV['LEDGER_CREDENTIAL'] )
    Wallet.where("location_id IS NOT NULL").find_each do |wallet|
      begin
        location = wallet.location
        p "updating wallet #{wallet.id}"
        ledger.accounts.update_tags(id: "#{wallet.id}", tags: {type: wallet.wallet_type, name: wallet.name, number: location.try(:phone_number), email: location.try(:email)})
      rescue Sequence::APIError => exc
        p exc
      end
    end
  end

  task issue_balance_in_accounts: :environment do
    ledger = Sequence::Client.new(ledger_name: "quickcard-production-test", credential:  "PGYXVV7M6KKRKOQSPRRZDBVP4WWGY2ZN" )
    Wallet.where("balance::float > 0").find_each do |wallet|
      id = wallet.id
      balance = wallet.balance.to_f * 100
      balance = balance.to_i
      p "------------------------"
      p "updating wallet #{id} and balance is #{balance}"
      p "------------------------"
      begin
        ledger.transactions.transact do |builder|
          builder.issue(
              flavor_id: 'usd',
              amount: balance,
              destination_account_id: "#{id}",
              )
        end
      rescue Sequence::APIError => exc
        p "============id--------------"
        p exc.message
        p exc.seq_code
        p "============"
      end
    end
  end
end