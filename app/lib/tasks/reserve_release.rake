namespace :reserve_release do
  task release_reserve: :environment do
    include ApplicationHelper
    # to release old reserve amounts which caused us issue
    begin
      description = []
      wallets = Wallet.reserve
      cron_job = CronJob.create(name: 'fix_old_reserve')
      wallets.each do |wallet|
        release = false
        reserve_days = 0
        @wallet_id = wallet.id
        if @wallet_id != 47129 #this wallet is not letting us transfer from sequence
          location = wallet.location
          fees = location.fees.buy_rate.first if location.present?
          reserve_days = fees.days if fees.present?
          reserve_amount = fees.reserve_fee if fees.present?
          if reserve_amount.to_f > 0 && reserve_days > 0
            release = true
          elsif reserve_amount.to_f == 0 && reserve_days == 0
            release = true
          end
          if release == true
            today_date = DateTime.now
            if reserve_days == 0
              previous_date = DateTime.now - 1.days
            else
              previous_date = DateTime.now - reserve_days.days
            end
            received_amount = BlockTransaction.where(receiver_wallet_id: @wallet_id, main_type: "Reserve Money Deposit").where("timestamp BETWEEN :first AND :second",first: previous_date,second: today_date).pluck(:amount_in_cents).sum
            total_amount = SequenceLib.dollars(received_amount)
            balance = SequenceLib.balance(@wallet_id)
            net_amount = balance.to_f - total_amount.to_f
            if net_amount > 0
              primary_wallet = location.wallets.primary.first.try(:id) if location.present?
              user = wallet.users.first
              tr_type = get_transaction_type(TypesEnumLib::TransactionType::ReserveTransfer)
              transaction = Transaction.create(
                  sender_wallet_id: @wallet_id,
                  receiver_wallet_id: primary_wallet,
                  sender_name: user.name,
                  receiver_name: user.name,
                  amount: net_amount,
                  total_amount: net_amount,
                  net_amount: net_amount,
                  status: "pending",
                  sender_id: user.id,
                  receiver_id: user.id,
                  main_type: tr_type.first,
                  sub_type: tr_type.second,
                  action: "transfer"
              )
              transfer = SequenceLib.transfer(net_amount, @wallet_id, primary_wallet, tr_type.first, 0 ,nil, "Quickcard", nil)
              if transfer.present?
                description = description.push({amount_in_released: net_amount,wallet_id: @wallet_id, user_id: user.id, status: "success"})
                transaction.update(seq_transaction_id: transfer.actions.first.id,
                                   timestamp: transfer.timestamp,
                                   status: "approved",
                                   tags: transfer.actions.first.tags)
                parsed_transactions = parse_block_transactions(transfer.actions, transfer.timestamp)
                save_block_trans(parsed_transactions) if parsed_transactions.present?
              else
                description = description.push({amount_not_released: net_amount,wallet_id: @wallet_id, user_id: user.id, status: "false"})
              end
            end
          end
        end
      end
      cron_job.description = description
      cron_job.release_batch!
      puts "successfully released all reserve"
    rescue StandardError => exc
      cron_job.description = {error: exc.message, backtrace: exc.backtrace.first(8), wallet_id: @wallet_id}
      cron_job.release_batch!
      puts "some error occured please see CronJob.release_batch.last for error"
    end
  end

  task reserve_amount: :environment do
    include ActionView::Helpers::NumberHelper
    wallets = Wallet.reserve
    file = File.new("#{Rails.root}/tmp/reserve_wallets.csv", "w")
    file << CSV.generate do |csv|
      csv << ["Wallet Id", "DBA name", "Release Amount"]
      wallets.each do |wallet|
        reserve_days = 0
        @wallet_id = wallet.id
        if @wallet_id != 47129 #this wallet is not letting us transfer from sequence
          location = wallet.location
          fees = location.fees.buy_rate.first if location.present?
          reserve_days = fees.days if fees.present?
          reserve_amount = fees.reserve_fee if fees.present?
          if reserve_amount.to_f > 0 && reserve_days > 0
            today_date = DateTime.now
            previous_date = DateTime.now - reserve_days.days
            received_amount = BlockTransaction.where(receiver_wallet_id: @wallet_id, main_type: "Reserve Money Deposit").where("timestamp BETWEEN :first AND :second",first: previous_date,second: today_date).pluck(:amount_in_cents).sum
            total_amount = SequenceLib.dollars(received_amount)
            balance = SequenceLib.balance(@wallet_id)
            net_amount = balance.to_f - total_amount.to_f
            if net_amount > 0
              csv << [@wallet_id, location.try(:business_name), number_with_precision(net_amount, precision: 2)]
            end
          elsif reserve_amount.to_f == 0 && reserve_days == 0
            today_date = DateTime.now
            previous_date = DateTime.now - reserve_days.days
            received_amount = BlockTransaction.where(receiver_wallet_id: @wallet_id, main_type: "Reserve Money Deposit").where("timestamp BETWEEN :first AND :second",first: previous_date,second: today_date).pluck(:amount_in_cents).sum
            total_amount = SequenceLib.dollars(received_amount)
            balance = SequenceLib.balance(@wallet_id)
            net_amount = balance.to_f - total_amount.to_f
            if net_amount > 0
              csv << [@wallet_id, location.try(:business_name), number_with_precision(net_amount, precision: 2)]
            end
          end
        end
      end
    end
    file.close
    SlackService.upload_files("reserve_amount", file, "#quickcard-internal", "reserve_amount")
  end
end