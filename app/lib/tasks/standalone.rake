namespace :standalone do
  desc "Update Locations Of Standalone"
  task update_standalone_type: :environment do
    puts 'Updating Location'
    begin
      job = CronJob.new(name: 'standalone:update_standalone_type', description: 'Update Locations Of Standalone', success: true)
      result = {updates: [],  error: nil}
      @locations = Location.where('standalone =? ', true)
      @locations.update_all(standalone_type: Location.standalone_types[:cultivate])
      result[:updates] = @locations.pluck(:id)
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during Location Update: ', exc
    end
    job.result = result
    job.rake_task!
    puts 'Locations Updated Successfully!'
  end
  # --------------Batch Date Update Task Revert--------------------
  task revert_batchdate_update: :environment do
    puts 'Reverting Locations Update'
    jobs = CronJob.rake_task.where(success: true, name: 'standalone:update_standalone_type')
    jobs.each do |job|
      if job["error"].nil? && job.result["updates"].present?
        begin
          ids = job.result["updates"]
          locations = Location.where(id: ids)
          locations.update_all(standalone_type: nil)
        rescue StandardError => exc
          puts 'Error occured during Batch Date Update: ', exc
        end
        job.rake_task_revert!
      end
    end
  end
end
