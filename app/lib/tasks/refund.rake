namespace :refund do
  task refund_task: :environment do
    begin
      job = CronJob.new(name: 'refund_task', description: 'Refunding task because of seq down', success: true)
      object = {updates: [], error: nil}
      time = Time.utc(2019, 7, 7, 0, 0)
      time2 = Time.utc(2109,7,13,23,59)
      transactions = Transaction.includes(:payment_gateway).where("status = :status AND created_at >= :date AND created_at <= :date2 AND charge_id is not NULL",date: time.to_datetime.utc, date2: time2.to_datetime.utc, status: "pending")
      transactions.each do |t|
        tx = task_refund_transaction(t)
        if tx.first
          object[:updates].push({id: t.id, status: true})
        else
          object[:updates].push({id: t.id, status: false})
        end
        puts "refunding #{t.id}"
      end
      puts 'Done Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during refund: ', exc
    end
    job.result = object
    job.rake_task!
  end

  task refund_gateway: :environment do
    begin
      job = CronJob.new(name: 'refund_task', description: 'fixing gateway id in refund retire transactions', success: true)
      object = {updates: [], error: nil}
      updates = []
      BlockTransaction.where('((main_type = :main_type AND action = :action_id) OR main_type = :bank_type) AND (payment_gateway_id is null) AND (timestamp BETWEEN :first AND :second)', first: '2020-07-01 00:00:00', second: '2020-8-31 23:59:59',bank_type: "Refund Bank", main_type: "Refund", action_id: 2).select("id as id","tags as tags","payment_gateway_id as payment_gateway_id","sequence_id as sequence_id").find_in_batches(batch_size: 500) do |batch|
        if batch.present?
          batch.each do |tx|
            if tx.tags["refund_details"].present?
              p_tx = BlockTransaction.where(sequence_id: tx.tags["refund_details"]["parent_transaction_id"]).select("payment_gateway_id as payment_gateway_id").last
              if p_tx.present?
                object[:updates].push({id: tx.id, gateway_id: p_tx.payment_gateway_id})
                tx.payment_gateway_id = p_tx.payment_gateway_id
                updates << tx
              end
            end
          end
          BlockTransaction.import updates, on_duplicate_key_update: {conflict_target: [:id], columns: [:payment_gateway_id,:updated_at]}
        end
      end
    rescue => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during refund: ', exc
    end
    job.result = object
    job.rake_task!
  end

  def task_refund_transaction(transaction)
    transaction_id = transaction.charge_id
    bank_amount = transaction.amount.to_f
    payment_gateway = transaction.payment_gateway
    order_bank = OrderBank.where(transaction_id: transaction.id).try(:first)
    return [false,"Refund Unsuccessful"] if payment_gateway.blank?
    if bank_amount > 0
      if payment_gateway.stripe?
        card_token = CardToken.new
        refund = card_token.stripe_refund(transaction_id, bank_amount, payment_gateway)
        if refund["status"] != "succeeded"
          puts "stripe failed to refund #{transaction.id}"
          return [false,"Refund Unsuccessful"]
        else
          transaction.update(status: "manual-refund")
          order_bank.update(status: "manual-refund") if order_bank.present?
          return [true,"Refund successful"]
        end
      elsif payment_gateway.converge?
        elavon = Payment::ElavonGateway.new
        refund = elavon.refund(transaction_id, bank_amount,payment_gateway)
        if refund["message"].present?
          puts "converge failed to refund #{transaction.id} : #{refund["message"]}"
          return [false, "Refund Unsuccessful"]
        else
          transaction.update(status: "manual-refund")
          order_bank.update(status: "manual-refund") if order_bank.present?
          return [true, "Refund successful"]
        end
      elsif payment_gateway.bolt_pay?
        bolt_pay = Payment::BoltPayGateway.new
        refund = bolt_pay.refund(transaction_id, bank_amount, payment_gateway)
        if refund[:message].present?
          puts "boltPay failed to refund #{transaction.id} : #{refund["message"]}"
          return [false, "Refund Unsuccessful"]
        else
          transaction.update(status: "manual-refund")
          order_bank.update(status: "manual-refund") if order_bank.present?
          return [true, "Refund successful"]
        end
      elsif payment_gateway.i_can_pay? || payment_gateway.payment_technologies?
        iCanPay = Payment::ICanPay.new(payment_gateway)
        refund = iCanPay.refund(transaction_id, bank_amount,request)
        if refund["message"].present?
          puts "icanpay failed to refund #{transaction.id} : #{refund["message"]}"
          return [false, "Refund Unsuccessful"]
        else
          transaction.update(status: "manual-refund")
          order_bank.update(status: "manual-refund") if order_bank.present?
          return [true, "Refund successful"]
        end
      elsif payment_gateway.fluid_pay?
        fluid_pay = FluidPayHelper::FluidPayPayment.new
        fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
        if (fluid_transaction_status != "settled" && fluid_transaction_status != "refunded")
          refund = fluid_pay.voidFluidPay(transaction_id,payment_gateway)
        else
          refund = fluid_pay.refundFluidAmount(transaction_id,SequenceLib.cents(bank_amount),payment_gateway)
        end
        if refund["status"] != "success"
          puts "fluid pay failed to refund #{transaction.id}"
          return [false,"Refund Unsuccessful"]
        else
          transaction.update(status: "manual-refund")
          order_bank.update(status: "manual-refund") if order_bank.present?
          return [false,"Refund successful"]
        end
      elsif bank_amount > 0
        puts "payment gateway not found failed to refund #{transaction.id}"
        return [false,"Refund Unsuccessful Payment Method Not Supported"]
      end
    end
  end
end