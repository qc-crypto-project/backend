namespace :update_wallets do
  desc "update wallet name"
  task update_wallet_name: :environment do
    puts 'Updating Wallets name'
    puts 'All User wallets names are updated(except merchant)'
    begin
      id = nil
      job = CronJob.new(name: 'wallet_name_update:update_wallet_name', description: 'update wallet name', success: true)
      result = {updates: [],  error: nil}
      User.all.each do |user|
        id=user.id
        if user.iso? || user.affiliate? || user.agent?
          user.wallets.primary.each do |wallet|
            update_wallets = {id: wallet.id, old_value: '', new_value: ''}
            if wallet.name!=user.name
              wallet_name=wallet.name.clone
              wallet.name=user.name if user.name.present?
              if wallet.save!
                puts "Updated Name successfuly of #{wallet.name}"
                update_wallets[:old_value] = wallet_name
                update_wallets[:new_value] = wallet.name
                result[:updates].push(update_wallets)
              else
                puts 'Not Updated Successfully'
              end
            end
          end
        end

      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during wallets Update: ', exc
    end
    job.result = result
    job.rake_task!
    puts 'Wallets Updated Successfully!'
  end
  # --------------Wallet name Task Revert--------------------
  task revert_update_wallet: :environment do
    puts 'Reverting Updated wallets name'
    job =CronJob.rake_task.where(success: true, name: 'wallet_name_update:update_wallet_name').last
    if job["error"].nil? && job.result["updates"].present?
        begin
          job.result["updates"].each do |wallet_record|
            wallet=Wallet.find_by_id(wallet_record["id"])
            id=wallet.id
            if wallet.present?
              if wallet.name!=wallet_record["old_value"]
                wallet.name=wallet_record["old_value"]
                wallet.save
                puts "Reverting wallet's name For #{id}"
              end
            end
          end
        rescue StandardError => exc

          puts 'Error occured during wallet name Update: ', exc
        end
      end
  end
end
