namespace :monthly_tasks do
  desc "TODO"
  task monthly_fees: :environment do
    date = Date.today
    # if date == date.beginning_of_month
      locations = Location.all
      locations.each do |location|
        unless location.wallets.count < 3 #= only work for location with primary wallet
          merchant = location.merchant
          fee_object = location.fees.buy_rate.first if location.fees.present?
          if fee_object.present?
            fee_class = Payment::FeeCommission.new(merchant, nil, fee_object, location.wallets.primary.first, location,TypesEnumLib::CommissionType::Monthly)
            service_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Service) if fee_class.present?
            statement_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Statement) if fee_class.present?
            misc_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Misc) if fee_class.present?

            if fee_object.service_date.present?
              if fee_object.service_date.strftime('%d').to_i == Date.today.strftime('%d').to_i
                if service_fee.present?
                  if service_fee["amount"] > 0
                    escrow_wallet = Wallet.escrow.first.id
                    qc_wallet = Wallet.qc_support.first.id
                    trans = SequenceLib.transfer_monthly_fees(service_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Service, escrow_wallet, qc_wallet, service_fee["splits"], merchant, location)
                    if trans.present?
                      description = {location_id: location.id, wallet_id: location.wallets.primary.first.id, user_id: merchant.id, user_info: service_fee["splits"], total_amount: service_fee["amount"], transaction_info: trans}
                      cron_job = CronJob.create(name: 'monthly_service_fee', description: description.to_json)
                      cron_job.monthly_fee!
                    end
                  end
                end
              end
            end

            if fee_object.statement_date.present?
              if fee_object.statement_date.strftime('%d').to_i == Date.today.strftime('%d').to_i
                if statement_fee.present?
                  if statement_fee["amount"] > 0
                    escrow_wallet = Wallet.escrow.first.id
                    qc_wallet = Wallet.qc_support.first.id
                    trans = SequenceLib.transfer_monthly_fees(statement_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Statement, escrow_wallet, qc_wallet, statement_fee["splits"], merchant, location)
                    if trans.present?
                      description = {location_id: location.id, wallet_id: location.wallets.primary.first.id, user_id: merchant.id, user_info: statement_fee["splits"], total_amount: statement_fee["amount"], transaction_info: trans}
                      cron_job = CronJob.create(name: 'monthly_statement_fee', description: description.to_json)
                      cron_job.monthly_fee!
                    end
                  end
                end
              end
            end

            if fee_object.misc_date.present?
              if fee_object.misc_date.strftime('%d').to_i == Date.today.strftime('%d').to_i
                if misc_fee.present?
                  if misc_fee["amount"] > 0
                    escrow_wallet = Wallet.escrow.first.id
                    qc_wallet = Wallet.qc_support.first.id
                    trans = SequenceLib.transfer_monthly_fees(misc_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Misc, escrow_wallet, qc_wallet, misc_fee["splits"], merchant, location)
                    if trans.present?
                      description = {location_id: location.id, wallet_id: location.wallets.primary.first.id, user_id: merchant.id, user_info: misc_fee["splits"], total_amount: misc_fee["amount"], transaction_info: trans}
                      cron_job = CronJob.create(name: 'monthly_misc_fee', description: description.to_json)
                      cron_job.monthly_fee!
                    end
                  end
                end
              end
            end

          end
        end
      end
    # end
  end
end
