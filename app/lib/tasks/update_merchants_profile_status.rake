namespace :merchant_profile_status do
  desc "Update merchant profile status"
  task update_merchant_profile_status: :environment do
    puts 'Updating profile status for merchants'
    begin
      User.merchant.find_each do |merchant|
        merchant.update(is_profile_completed: true)
      end
    rescue StandardError => exc
      puts 'Error occured during Merchant update: ', exc
    end
    puts 'Merchants updated Successfully!'
  end
end