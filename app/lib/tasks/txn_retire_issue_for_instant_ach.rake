namespace :txn_action_retire_to_transfer do
  desc "Updating action from retire to transfer for instant_ach txns"
  task action_transfer_for_ach_txn: :environment do
    puts 'Updating action for instant_ach txn'
    begin
      id = nil
      job = CronJob.new(name: 'txn_action_retire_to_transfer:action_transfer_for_ach_txn', description: 'Updating action for ach txn', success: true)
      result = {updates: [],  error: nil}
      txns = Transaction.joins(:sender, :tango_order).where("action = ? AND main_type = ? AND users.role in (?) AND tango_orders.amount_escrow in (?)", "retire", "ACH", [7,9,10],[true,false])
      txns.each do |txn|
          id = txn.id
          txn.update(action: "transfer")
          result[:updates].push(id)
        end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during transaction action processing: ', exc
    end
    job.result = result
    job.update_txn_action_for_achs!
    puts 'Successfully updated transaction action!'
  end
end