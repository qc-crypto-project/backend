namespace :failed_p2c do

  desc "Sending money back to merchant in case of failed p2c"
  task withdrawal: :environment do
    include AdminsHelper
    puts "Start......."
    files = []
    job = CronJob.new(name: 'failed_p2c:withdrawal', description: 'Update Merchant Balance', success: true)
    result = {updates: [], error: nil}
    TangoOrder.instant_pay.where(status: "FAILED", amount_escrow: true).each do |check|
      puts "Check ID =>#{check.id}"
      begin
        job_data = {check_id: check.id}
        user = check.user
        file_data = {check_id: check.id, name: check.name, amount: check.amount}
        check_info = {
            name: check.name,
            check_email: check.recipient,
            check_id: check.checkId,
            check_number: check.number,
            notes: "Manually refund for failed p2c"
        }
        wallet = Wallet.eager_load(:location).find(check.wallet_id)
        old_balance = SequenceLib.balance(wallet.id)
        file_data[:old_balance] = old_balance
        if wallet.location.present?
          location = location_to_parse(wallet.location)
          merchant = merchant_to_parse(wallet.location.merchant)
        elsif wallet.try('users').try('first').present? &&  wallet.try('users').try('first').role!='merchant'
          location=nil
          merchant = merchant_to_parse(wallet.users.first)
        end
        check_escrow = Wallet.check_escrow.first
        escrow_user = check_escrow.try(:users).try(:first)
        tags = {
            "location" => location,
            "merchant" => merchant,
            "send_check_user_info" => check_info,
        }
        ach_check_type=updated_type(TypesEnumLib::TransactionType::FailedPushtoCard)
        check_total_amount = check.amount.to_f
        fee_perc_details = {}
        fee = {}
        total_fee = 0
        if user.merchant?
          check_location = wallet.location
          location_fee = check_location.fees
          fee_object = location_fee.buy_rate.first if location_fee.present?
          fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, check_location,TypesEnumLib::CommissionType.const_get("Failed#{check.order_type}"))
          fee = fee_class.apply(check.actual_amount.to_f, TypesEnumLib::CommissionType.const_get("Failed#{check.order_type}"))
          total_fee = fee["fee"].to_f
        else
          fee_object = user.system_fee if user.system_fee.present? && user.system_fee != "{}"
          db_fee_perc = 0
          if fee_object.present?
            dollar_fee = check.instant_pay? ? fee_object["failed_push_to_card_fee"] : check.instant_ach? ? fee_object["failed_ach_fee"] : fee_object["failed_check_fee"]
            db_check_fee = dollar_fee.present? ? dollar_fee.to_f : 0
          else
            db_check_fee = 0
          end
          total_fee = db_fee_perc + db_check_fee
        end
        file_data[:fee] = total_fee
        if check.amount_escrow.present?
          main_tx = check.try(:transaction)
          fee_perc_details["fee_perc"] = main_tx.tags.try(:[],"fee_perc")
          receiver_dba=get_business_name(Wallet.find_by(id:wallet.id))
          transaction = create_withdrawal_transaction(wallet, check, check_total_amount, user, ach_check_type, tags, 'transfer', escrow_user, check_escrow, receiver_dba, main_tx, fee,true)
          job_data[:transaction_id] = transaction.id
          file_data[:transaction_id] = transaction.id
          issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, total_fee,fee["splits"], check_info,check,nil,nil,true) if user.MERCHANT? || user.ISO? || user.AGENT? || user.AFFILIATE?
          check.amount_escrow = false
        else
          transaction = create_withdrawal_transaction(wallet, check, check.actual_amount.to_f, user, ach_check_type, tags, 'issue',nil,nil,nil,nil,nil,true)
          job_data[:transaction_id] = transaction.id
          file_data[:transaction_id] = transaction.id
          issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info, check,nil,nil,true)
        end
        new_balance = SequenceLib.balance(wallet.id)
        file_data[:new_balance] = new_balance
        if issue.present?
          transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
          #= creating block transaction
          check.void_transaction_id = transaction.id
          parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
        end
        check.approved = true
        check.settled = true
        check.save
        job_data[:status] = true
        job_data[:message] = "success"
        file_data[:message] = "success"
      rescue => e
        puts "Error: #{e.message}"
        job_data[:status] = false
        job_data[:message] = e.message
        file_data[:message] = e.message
      ensure
        result[:updates].push(job_data)
        files.push(file_data)
      end
    end
    job.rake_task!
    puts "completed"
    upload_too_slack("p2c_failed",files) if files.present?
  end

  def upload_too_slack(name,files)
    file = File.new("#{Rails.root}/tmp/#{name}.csv", "w")
    file << CSV.generate do |csv|
      csv << ["CheckID", "Name", "TransactionID","P2C Amount","Failed Fee","Old Balance","New Balance","Status"]
      files.each do |file|
        csv << [file[:check_id], file[:name], file[:transaction_id] , file[:amount] ,file[:fee] , file[:old_balance] , file[:new_balance] , file[:message]]
      end
    end
    file.close
    SlackService.upload_files(name, file, "#qc-files", "#{name} withdrawals")
  end

end
