namespace :transactions do
  desc "Updating transaction information"

  task transactions_main_type: :environment do
    transactions = Transaction.joins(:order_bank).where.not(payment_gateway_id:nil).where(main_type:nil,status:["approved", "refunded","complete"])
    puts "#{transactions.count}"
    transactions.each do |trans|
      t_order_bank = trans.order_bank
      t_transaction_info = t_order_bank.transaction_info
      t_transaction_info = JSON.parse(t_transaction_info) if t_transaction_info.present? && t_transaction_info.class == String
      if t_transaction_info.present? && t_transaction_info["type"].present?
        if t_transaction_info["type"] == "Credit"
          trans.main_type = TypesEnumLib::TransactionViewTypes::CreditCard
        elsif t_transaction_info["type"] == "eCommerce"
          trans.main_type = TypesEnumLib::TransactionViewTypes::Ecommerce
        else
          trans.main_type = "needToFix"
        end
        puts "updating #{trans.id}"
        trans.save
        puts "updated #{trans.id}"
      else
        puts "#{trans.id} is missing"
      end
    end
  end
end
