namespace :sub_merchant_name do
  task sub_merchant_name_upadate: :environment do
    puts "Start updating Sub Merchants"
    job = CronJob.new(name: 'sub_merchant_name:sub_merchant_name_upadate', description: 'Sub merchant name and first name updation', success: true)
    object = {updates: [],  error: nil}
    User.merchant.where.not(merchant_id: nil).where(archived: "false").find_in_batches(batch_size: 5000) do |batch|
      updated_users = []
      batch.each do |user|
        if user.name.present? && user.first_name.nil?
          before_name = user.name
          before_first_name = user.first_name
          user.first_name = user.name
          user.name = user.last_name.present? ? user.first_name  + " #{ user.last_name}" : user.first_name
          updated_users.push(user)
          object[:updates].push({id:user.id, before_name: before_name , after_name: user.name , before_first_name: before_first_name ,  after_first_name: user.first_name, last_name: user.last_name})
        end
      end
      User.import updated_users, on_duplicate_key_update: {conflict_target: [:id], columns: [:name, :first_name]}
      puts "Updated Successfully"
    end
    job.result = object
    job.sub_merchant_name!
  end
  # --------------Sub-Merchants Name  and First Name Task Revert--------------------
  task revert_submerchnat_name_update: :environment do
    puts 'Reverting Sub-Merchants Name and First Name '
    job = CronJob.where(success: true, name: 'sub_merchant_name:sub_merchant_name_upadate').last
      if job.result["updates"].present?
        begin
          job.result["updates"].each do |user_record|
            id = user_record["id"]
            user_update = User.find(id) if id.present?
            if user_update.present?
               user_update.update(name: user_record["before_name"], first_name: user_record["before_first_name"] )
                puts "Reverting Sub-Merchant User Name and First Name For #{id}"
            end
          end
        rescue StandardError => exc
          puts 'Error occured during Sub-Merchant Name and First Name  Update: ', exc
        end
      end
  end
end