namespace :company_name_update do
  desc "Updating company name of Agents and Affiliates"
  task update_company_name: :environment do
    puts "Updating Company name of Aggents and Affiliates"
    puts "=======Start========= "
    begin
      id = nil
      job = CronJob.new(name: 'company_name_update:update_company_name', description: 'Update Compny name for Agents and Affiliates', success: true)
      result = {updates: [],  error: nil}

      users = User.agent + User.affiliate
       # users = User.where(id: [23,15])
      users.each do|u|
        id = u.id
        if u.profile.present?
          if u.company_name.nil? && u.profile.try(:company_name).present?
            u.update(company_name: u.profile.try(:company_name))
            result[:updates].push(id)
          elsif u.profile.try(:company_name).nil? && u.company_name.present?
            u.profile.update(company_name: u.company_name)
            result[:updates].push(id)
          end
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Updating company name processing: ', exc
    end
    job.result = result
    job.update_company_name!
    puts "Users Updated"
  end
end