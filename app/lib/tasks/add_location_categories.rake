namespace :location_categories do
  desc "update categories"
  task update_categories: :environment do
    job = CronJob.new(name: "location_categories:update_categories", description: 'update categories', success: true)
    result = {
        updated_locations: [],
        error: nil
    }
    begin
      puts "started updating locations"
      locations = Location.where("category IS NOT NULL AND category_id IS NULL")
      categories = Category.all
      if locations.present?
        categories.each do |cat|
          puts "adding category #{cat.id}"
          locs = locations.where("category = ?", cat.name)
          if locs.present?
            locs.update_all(category_id: cat.id)
            result[:updated_locations] << {cat_id: cat.id, loc_ids: locs.pluck(:id)}
          end
        end
        puts "categories update successfully"
      end
      # result[:updated_categories] = categories
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during location Update: ', exc
    end
    job.result = result
    job.rake_task!
  end

  desc "removing duplicate categories"
  task remove_dup: :environment do
    job = CronJob.new(name: "location_categories:remove_categories", description: 'removing duplicate categories', success: true)
    result = {updates: [], error: nil}
    begin
      puts "started updating locations"
      locations = Location.where("category_id IS NOT NULL")
      cat_hash = {}
      Category.all.each do |a|
        striped_name = a.name.try(:strip)
        if cat_hash["#{striped_name}"].present?
          cat_hash["#{striped_name}"].push(a.id)
        else
          cat_hash["#{striped_name}"] = [a.id]
        end
      end
      if locations.present?
        locations.each do |l|
          if l.category_id.present?
            if l.category.try(:name).present?
              cat_name = l.category.try(:name).try(:strip)
              if cat_hash.keys.include? cat_name
                if cat_hash["#{cat_name}"].count > 1
                  a = {id: l.id, old_category_id: l.category_id}
                  l.update(category_id: cat_hash["#{cat_name}"].min)
                  result[:updates].push(a.merge({new_category_id: l.category_id}))
                end
              end
            end
          end
        end
        puts "categories update successfully"
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during location Update: ', exc
    end
    job.result = result
    job.rake_task!
  end

  desc "deleting categories"
  task delete_cat: :environment do
    puts "start deleting............."
    cat_hash = {}
    Category.all.each do |a|
      striped_name = a.name.try(:strip)
      if cat_hash["#{striped_name}"]
        cat_hash["#{striped_name}"].push(a.id)
      else
        cat_hash["#{striped_name}"] = [a.id]
      end
    end
    cat_hash.each do |k,v|
      if v.count > 1
        v.delete(v.min)
        Category.where(id: v).delete_all
      end
    end
    puts "deleted"
  end

end

