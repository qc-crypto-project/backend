namespace :seed_update_live do
  desc "update seed on live"
  task location_fee_save: :environment do
    puts "start updating"
    locations = []
    locations.each do |location|
      if location.present?
        if !location.fees.buy_rate.present?
          puts "into buyrate fee"
          customer_fee = location.fees.where(:fee_status => "customer_fee").first
          b2b_fee = location.fees.where(:fee_status => "b2b_fee").first
          redeem_fee = location.fees.where(:fee_status => "redeem_fee").first
          send_check =  location.fees.where(:fee_status => "send_check").first
          add_check = location.fees.where(:fee_status => "add_check").first
          redeem_check = location.fees.where(:fee_status => "redeem_check").first
          giftcard_fee = location.fees.where(:fee_status => "giftcard_fee").first
          reserve_fee = location.fees.where(:fee_status => "reserve_fee").first
          customer_fee_dollar = location.fees.where(:fee_status => "customer_fee_dollar").first
          transaction_fee_app = location.fees.where(:fee_status => "transaction_fee_app").first
          transaction_fee_app_dollar = location.fees.where(:fee_status => "transaction_fee_app_dollar").first
          transaction_fee_dcard = location.fees.where(:fee_status => "transaction_fee_dcard").first
          transaction_fee_dcard_dollar = location.fees.where(:fee_status => "transaction_fee_dcard_dollar").first
          transaction_fee_ccard = location.fees.where(:fee_status => "transaction_fee_ccard").first
          transaction_fee_ccard_dollar = location.fees.where(:fee_status => "transaction_fee_ccard_dollar").first
          dispute_chargeback_fee = location.fees.where(:fee_status => "dispute_chargeback_fee").first
          dispute_retrivel_fee = location.fees.where(:fee_status => "dispute_retrivel_fee").first

          commission = {
              customer_fee: {
                  gbox: customer_fee.present? ? customer_fee.gbox.to_f : 0,
                  partner: customer_fee.present? ? customer_fee.partner.to_f : 0,
                  agent: customer_fee.present? ? customer_fee.agent.to_f : 0,
                  iso: customer_fee.present? ? customer_fee.iso.to_f : 0
              },
              b2b_fee: {
                  gbox: b2b_fee.present? ? b2b_fee.gbox.to_f : 0,
                  partner: b2b_fee.present? ? b2b_fee.partner.to_f : 0,
                  agent: b2b_fee.present? ? b2b_fee.agent.to_f : 0,
                  iso: b2b_fee.present? ? b2b_fee.iso.to_f : 0
              },
              redeem_fee: {
                  gbox: redeem_fee.present? ? redeem_fee.gbox.to_f : 0,
                  partner: redeem_fee.present? ? redeem_fee.partner.to_f : 0,
                  agent: redeem_fee.present? ? redeem_fee.agent.to_f : 0,
                  iso: redeem_fee.present? ? redeem_fee.iso.to_f : 0
              },
              send_check: {
                  gbox: send_check.present? ? send_check.gbox.to_f : 0,
                  partner: send_check.present? ? send_check.partner.to_f : 0,
                  agent: send_check.present? ? send_check.agent.to_f : 0,
                  iso: send_check.present? ? send_check.iso.to_f : 0
              },
              add_check: {
                  gbox: add_check.present? ? add_check.gbox.to_f : 0,
                  partner: add_check.present? ? add_check.partner.to_f : 0,
                  agent: add_check.present? ? add_check.agent.to_f : 0,
                  iso: add_check.present? ? add_check.iso.to_f : 0
              },
              redeem_check: {
                  gbox: redeem_check.present? ? redeem_check.gbox.to_f : 0,
                  partner: redeem_check.present? ? redeem_check.partner.to_f : 0,
                  agent: redeem_check.present? ? redeem_check.agent.to_f : 0,
                  iso: redeem_check.present? ? redeem_check.iso.to_f : 0
              },
              giftcard_fee: {
                  gbox: giftcard_fee.present? ? giftcard_fee.gbox.to_f : 0,
                  partner: giftcard_fee.present? ? giftcard_fee.partner.to_f : 0,
                  agent: giftcard_fee.present? ? giftcard_fee.agent.to_f : 0,
                  iso: giftcard_fee.present? ? giftcard_fee.iso.to_f : 0
              },
              customer_fee_dollar: {
                  gbox: customer_fee_dollar.present? ? customer_fee_dollar.gbox.to_f : 0,
                  partner: customer_fee_dollar.present? ? customer_fee_dollar.partner.to_f : 0,
                  agent: customer_fee_dollar.present? ? customer_fee_dollar.agent.to_f : 0,
                  iso: customer_fee_dollar.present? ? customer_fee_dollar.iso.to_f : 0
              },
              transaction_fee_app: {
                  gbox: transaction_fee_app.present? ? transaction_fee_app.gbox.to_f : 0,
                  partner: transaction_fee_app.present? ? transaction_fee_app.partner.to_f : 0,
                  agent: transaction_fee_app.present? ? transaction_fee_app.agent.to_f : 0,
                  iso: transaction_fee_app.present? ? transaction_fee_app.iso.to_f : 0
              },
              transaction_fee_app_dollar: {
                  gbox: transaction_fee_app_dollar.present? ? transaction_fee_app_dollar.gbox.to_f : 0,
                  partner: transaction_fee_app_dollar.present? ? transaction_fee_app_dollar.partner.to_f : 0,
                  agent: transaction_fee_app_dollar.present? ? transaction_fee_app_dollar.agent.to_f : 0,
                  iso: transaction_fee_app_dollar.present? ? transaction_fee_app_dollar.iso.to_f : 0
              },
              transaction_fee_dcard: {
                  gbox: transaction_fee_dcard.present? ? transaction_fee_dcard.gbox.to_f : 0,
                  partner: transaction_fee_dcard.present? ? transaction_fee_dcard.partner.to_f : 0,
                  agent: transaction_fee_dcard.present? ? transaction_fee_dcard.agent.to_f : 0,
                  iso: transaction_fee_dcard.present? ? transaction_fee_dcard.iso.to_f : 0
              },
              transaction_fee_dcard_dollar: {
                  gbox: transaction_fee_dcard_dollar.present? ? transaction_fee_dcard_dollar.gbox.to_f : 0,
                  partner: transaction_fee_dcard_dollar.present? ? transaction_fee_dcard_dollar.partner.to_f : 0,
                  agent: transaction_fee_dcard_dollar.present? ? transaction_fee_dcard_dollar.agent.to_f : 0,
                  iso: transaction_fee_dcard_dollar.present? ? transaction_fee_dcard_dollar.iso.to_f : 0
              },
              transaction_fee_ccard: {
                  gbox: transaction_fee_ccard.present? ? transaction_fee_ccard.gbox.to_f : 0,
                  partner: transaction_fee_ccard.present? ? transaction_fee_ccard.partner.to_f : 0,
                  agent: transaction_fee_ccard.present? ? transaction_fee_ccard.agent.to_f : 0,
                  iso: transaction_fee_ccard.present? ? transaction_fee_ccard.iso.to_f : 0
              },
              transaction_fee_ccard_dollar: {
                  gbox: transaction_fee_ccard_dollar.present? ? transaction_fee_ccard_dollar.gbox.to_f : 0,
                  partner: transaction_fee_ccard_dollar.present? ? transaction_fee_ccard_dollar.partner.to_f : 0,
                  agent: transaction_fee_ccard_dollar.present? ? transaction_fee_ccard_dollar.agent.to_f : 0,
                  iso: transaction_fee_ccard_dollar.present? ? transaction_fee_ccard_dollar.iso.to_f : 0
              },
              dispute_chargeback_fee: {
                  gbox: dispute_chargeback_fee.present? ? dispute_chargeback_fee.gbox.to_f : 0,
                  partner: dispute_chargeback_fee.present? ? dispute_chargeback_fee.partner.to_f : 0,
                  agent: dispute_chargeback_fee.present? ? dispute_chargeback_fee.agent.to_f : 0,
                  iso: dispute_chargeback_fee.present? ? dispute_chargeback_fee.iso.to_f : 0
              },
              dispute_retrivel_fee: {
                  gbox: dispute_retrivel_fee.present? ? dispute_retrivel_fee.gbox.to_f : 0,
                  partner: dispute_retrivel_fee.present? ? dispute_retrivel_fee.partner.to_f : 0,
                  agent: dispute_retrivel_fee.present? ?  dispute_retrivel_fee.agent.to_f : 0,
                  iso: dispute_retrivel_fee.present? ? dispute_retrivel_fee.iso.to_f : 0
              }
          }
          fee = location.fees.create!(
            customer_fee: customer_fee.present? ? customer_fee.customer_fee : 0,
            customer_fee_dollar: customer_fee_dollar.present? ? customer_fee_dollar.customer_fee_dollar : 0,
            b2b_fee: b2b_fee.present? ? b2b_fee.b2b_fee : 0,
            redeem_fee: redeem_fee.present? ? redeem_fee.redeem_fee : 0,
            redeem_check: redeem_check.present? ? redeem_check.redeem_check : 0,
            send_check: send_check.present? ? send_check.send_check : 0,
            add_money_check: add_check.present? ? add_check.add_money_check : 0,
            reserve_fee: reserve_fee.present? ? reserve_fee.reserve_fee : 0,
            days: reserve_fee.present? ? reserve_fee.days : 0,
            giftcard_fee: giftcard_fee.present? ? giftcard_fee.giftcard_fee : 0,
            transaction_fee_app: transaction_fee_app.present? ? transaction_fee_app.transaction_fee_app : 0,
            transaction_fee_app_dollar: transaction_fee_app_dollar.present? ? transaction_fee_app_dollar.transaction_fee_app_dollar : 0,
            transaction_fee_ccard_dollar: transaction_fee_ccard_dollar.present? ? transaction_fee_ccard_dollar.transaction_fee_ccard_dollar : 0,
            transaction_fee_ccard: transaction_fee_ccard.present? ? transaction_fee_ccard.transaction_fee_ccard : 0,
            transaction_fee_dcard: transaction_fee_dcard.present? ? transaction_fee_dcard.transaction_fee_dcard : 0,
            transaction_fee_dcard_dollar: transaction_fee_dcard_dollar.present? ? transaction_fee_dcard_dollar.transaction_fee_dcard_dollar : 0,
            charge_back_fee: dispute_chargeback_fee.present? ? dispute_chargeback_fee.charge_back_fee : 0,
            retrievel_fee: dispute_retrivel_fee.present? ? dispute_retrivel_fee.retrievel_fee : 0,
            commission: commission,
          )
          fee.buy_rate!
          fee.high!
        end
      end
    end
    puts "updation done"
  end
  task user_fee_save: :environment do
    puts "start updating user fee"
    User.all.each do |user|
      if user.present?
        if user.ISO? || user.AGENT? || user.AFFILIATE?
          if user.fees.present? || user.fees.user_fee.present?
            puts "removing user_fee for user #{user.id} #{user.role}"
            user.fees.user_fee.delete_all
          end
        end
      end
    end
    puts "updation done"
  end
end