namespace :configs do
  desc "Create app config to enable/disable APIs from admin pannel"
  task create_apis_configs: :environment do
    checking = AppConfig.where(key: "apis_authorization").first
    if checking.nil?
      AppConfig.create(key: AppConfig::Key::APIsAuthorization, stringValue: {virtual_existing: true, virtual_debit_charge: true, virtual_new_signup: true, virtual_terminal_transaction: true, virtual_transaction: true, refund: true}.to_json) unless @apis_config.present?
      puts "Created Successfully"
    else
      puts "Already created"
    end
  end
end