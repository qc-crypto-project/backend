namespace :merchant_locations_attached do
  desc "Update Merchant Locations profit split"
  task merchant_attached_locations: :environment do
    puts "Start updating Merchant Locations profit split"
    begin
      job = CronJob.new(name: 'merchant_locations_attached:merchant_attached_locations', description: 'Updating Merchant Locations profit split data', success: true)
      result = {updates: [], error: nil}
      Location.where(sub_merchant_split:true).where.not(profit_split_detail:nil).each do |location|
            profit_split_detail = JSON(location.profit_split_detail)
            old_data = profit_split_detail
            profit_split_detail["splits"] = profit_split_detail["splits"].reject{|k,v| k.present? ? k == "ez_merchant" : ""}
            location.update(profit_split_detail: profit_split_detail.to_json)
            result[:updates] << {id: location.id, profit_split_detail: old_data}
        end
      puts 'Updated Successfully'
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during Location Profit split Update: ', exc
    end
    job.result = result
    job.update_attached_locations!
  end
end