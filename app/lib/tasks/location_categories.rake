namespace :location_categories do
  task add_categories: :environment do
    job = CronJob.new(name: "location_categories:add_categories", description: 'add categories', success: true)
    result = {
        updated_categories: [],
        updated_ecoms: [],
        error: nil
    }
    begin
      categories = Location.pluck(:category).uniq.reject {|v| v=="" || v == nil}
      ecom_platforms = Location.where.not(ecom_platform: nil).pluck(:ecom_platform).uniq
      ecom_arr = []
      ecom_platforms.each do |ecom|
        ecom_arr.push(JSON.parse(ecom)) if ecom.present?
      end
      final_ecom_platforms = ecom_arr.flatten.uniq.reject {|v| v=="" || v == nil}

      puts "started updating categories"
      categories.each do |l|
        c = Category.where(name: l).first_or_create
        puts "#{c.id} Category"
      end
      puts "categories update successfully"
      result[:updated_categories] = categories

      final_ecom_platforms.each do |e|
        c = EcommPlatform.where(name: e).first_or_create
        puts "#{c.id} Ecom"
      end
      result[:updated_ecoms] = final_ecom_platforms
      puts "ecom plateforms updated successfully"
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during Categories Update: ', exc
    end
    job.result = result
    job.rake_task!
  end

end
