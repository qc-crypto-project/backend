namespace :payment_gateway do
  desc "Merchant Payment Gateway's updation"
  task update: :environment do
    begin
      id = nil
      job = CronJob.new(name: "payment_gateway:update", description: 'update payment gateway', success: true)
      result = {updated_merchants: [], not_updated_merchants: [], error: nil}
      merchants = User.merchant
      merchants.each do |merchant|
        id = merchant.id
        merchant_gateway = {merchant_id: merchant.id, old_gateway: [], new_gateway: []}
        # if merchant.primary_gateway.present? || merchant.secondary_gateway.present? || merchant.ternary_gateway.present?
        #   merchant_gateway[:reason] = "new already exist"
        #   result[:not_updated_merchants] << merchant_gateway
        #   puts 'NEW Payment Gateway Exist FOR USER ID: ', merchant.id
        if merchant.payment_gateway.present? || merchant.secondary_payment_gateway.present? ||  merchant.third_payment_gateway.present?

          first = merchant.payment_gateway
          if first.present?
            primary = PaymentGateway.find_by(key: first)
            raise "#{first} PaymentGateway should must exist" if primary.blank?
            if merchant.primary_gateway_id.blank?
              puts 'Updating first Payment Gateway FOR USER ID: ', merchant.id
              merchant.update(primary_gateway_id: primary.id)
              merchant_gateway[:old_gateway] << first
              merchant_gateway[:new_gateway] << primary.id
            else
              merchant_gateway[:reason] = "already done"
              puts "already done merchant_id: #{merchant.id}"
            end
          end

          second = merchant.secondary_payment_gateway
          if second.present?
            secondary = PaymentGateway.find_by(key: second)
            raise "#{second} PaymentGateway should must exist" if secondary.blank?
            if merchant.secondary_gateway_id.blank?
              puts 'Updating second Payment Gateway FOR USER ID: ', merchant.id
              merchant.update(secondary_gateway_id: secondary.id)
              merchant_gateway[:old_gateway] << second
              merchant_gateway[:new_gateway] << secondary.id
            else
              merchant_gateway[:reason] = "already done"
              puts "already done merchant_id: #{merchant.id}"
            end
          end

          third = merchant.third_payment_gateway
          if third.present?
            ternary = PaymentGateway.find_by(key: third)
            raise "#{third} PaymentGateway should must exist" if ternary.blank?
            if merchant.ternary_gateway_id.blank?
              puts 'Updating third Payment Gateway FOR USER ID: ', merchant.id
              merchant.update(ternary_gateway_id: ternary.id)
              merchant_gateway[:old_gateway] << third
              merchant_gateway[:new_gateway] << ternary.id
            else
              merchant_gateway[:reason] = "already done"
              puts "already done merchant_id: #{merchant.id}"
            end
          end

          if merchant.primary_gateway.blank?
            converge = PaymentGateway.find_by(key: "converge")
            raise "Converge should must exist" if converge.blank?
            merchant.update(primary_gateway_id: converge.id)
          end
          result[:updated_merchants] << merchant_gateway

        else
          merchant_gateway[:reason] = "both old and new gateway not exist"
          result[:not_updated_merchants] << merchant_gateway
          puts "NO Gateway Exist FOR MERCHANT ID: #{merchant.id}"
        end
      end
      puts 'Updated Successfully'
      puts result
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Gateway Update: ', exc
    end

    job.result = result
    job.rake_task!
  end
end
