namespace :fee_missing do
  desc "instant_pay txn fee missing"
  task txn_fee_missing: :environment do
    puts 'Processing txn missing fee'
    begin
      id = nil
      job = CronJob.new(name: 'fee_missing:txn_fee_missing', description: 'instant_pay txn fee missing', success: true)
      result = {updates: [],  error: nil}
      txns = Transaction.joins(:sender).where("main_type = ? AND users.role IN (?)", 'instant_pay', [7,9,10]).where(fee: nil)
      txns.each do |txn|
          id = txn.id
          fee = txn.try(:tags).try(:[], "send_check_user_info").try(:[], "fee")
          fee = txn.try(:total_amount).to_f - txn.try(:net_amount).to_f if fee.blank?
          txn.update(fee: fee, net_fee: fee, gbox_fee: fee)
          result[:updates].push(id)
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during instant_pay txn fee update: ', exc
    end
    job.result = result
    job.txn_fee_missing!
    puts 'Successfully updated fee!'
  end
end