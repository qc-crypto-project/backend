namespace :payment_gateway do
  desc "Merchant Payment Gateway's updation"
  task update: :environment do
    puts '================>>  Updating Merchants gateway...'
    merchants = User.merchant
    merchants.each do|merchant|
      job = CronJob.new(name: merchant.id, description: 'update payment gateway', success: true)
      object = {updates: [], error: nil}
      first_gateway = merchant.payment_gateway
      if first_gateway.present?
        first_payment_gateway = PaymentGateway.find_by(key: first_gateway)
        if first_payment_gateway.present?
          merchant.update(primary_gateway_id: first_payment_gateway.id)
          object[:updates].push({:primary => {id: merchant.id, old_value: merchant.payment_gateway, new_value: first_payment_gateway.key, sucess: true}})
        else
          object[:updates].push({:primary => {id: merchant.id, old_value: merchant.payment_gateway, new_value: first_payment_gateway.key, sucess: false}})
        end
      else
        object[:updates].push({:primary => {id: merchant.id, old_value: merchant.payment_gateway, new_value: first_payment_gateway.key, sucess: false}})
      end

      second_gateway = merchant.secondary_payment_gateway
      if second_gateway.present?
        second_payment_gateway = PaymentGateway.find_by(key: second_gateway)
        if second_payment_gateway.present?
          merchant.update(secondary_gateway_id: second_payment_gateway.id)
          object[:updates].push({:secondary => {id: merchant.id, old_value: merchant.secondary_payment_gateway, new_value: second_payment_gateway.key, sucess: true}})
        else
          object[:updates].push({:secondary => {id: merchant.id, old_value: merchant.secondary_payment_gateway, new_value: second_payment_gateway.key, sucess: false}})
        end
      else
        object[:updates].push({:secondary => {id: merchant.id, old_value: merchant.secondary_payment_gateway, new_value: second_payment_gateway.key, sucess: false}})
      end

      third_gateway = merchant.third_payment_gateway
      if third_gateway.present?
        third_payment_gateway = PaymentGateway.find_by(key: third_gateway)
        if third_payment_gateway.present?
          merchant.update(ternary_gateway_id: third_payment_gateway.id)
          object[:updates].push({:ternary => {id: merchant.id, old_value: merchant.third_payment_gateway, new_value: third_payment_gateway.key, sucess: true}})
        else
          object[:updates].push({:ternary => {id: merchant.id, old_value: merchant.third_payment_gateway, new_value: third_payment_gateway.key, sucess: true}})
        end
      else
        object[:updates].push({:ternary => {id: merchant.id, old_value: merchant.third_payment_gateway, new_value: third_payment_gateway.key, sucess: true}})
      end
      job.result = object
      job.gateway_update!
    end
    puts '================>>  Updated Merchants gateway...'
  end

  task volume_update: :environment do
    puts '================>>  Updating...'
    PaymentGateway.main_gateways.each do |payment_gateway|
      date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").at_beginning_of_month
      d_date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day
      monthly_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
      daily_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",d_date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
      puts "monthly_amount_limit"
      puts monthly_amount_limit
      puts "daily_amount_limit"
      puts daily_amount_limit
      payment_gateway.daily_volume_achived = daily_amount_limit
      payment_gateway.monthly_volume_achived = monthly_amount_limit
      payment_gateway.total_transaction_count = payment_gateway.transactions.count
      payment_gateway.total_cbk_count = DisputeCase.where(payment_gateway_id: payment_gateway.id).where('created_at >= :date', date: date).count
      payment_gateway.save

      puts "saved---monthly--vol"
      puts payment_gateway.monthly_volume_achived
      puts "saved---daily--vol"
      puts payment_gateway.daily_volume_achived


    end
    puts '================>>  Updated Merchants gateway...'
  end
end
