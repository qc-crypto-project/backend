namespace :card_tokenization do
  desc "remove card_number and exp"
  task remove_card: :environment do
    puts "Starting....."
    begin
      @error_card = []
      Card.where.not(qc_token: nil, fingerprint: nil).find_in_batches(batch_size: 10000).with_index do |cards,index|
        puts "========>>>> index : #{index}"
        updating_card(cards)
      end
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
      puts "Card Id : #{@card_id}"
      puts "Error card ids",@error_card
      puts "Total Error card #{@error_card.count}"
    end
    puts @error_card
    puts "Done...."
  end

  def updating_card(cards)
    if cards.present?
      updating_card = cards.each do |card|
        @card_id = card.id
        begin
          result = AESCrypt.decrypt("#{card.fingerprint}-#{card.user_id}-#{ENV['CARD_ENCRYPTER']}",card.qc_token)
          if result.present?
            result = JSON(result)
            result.select!{|k,v| k == "cvv"}
            card.qc_token = AESCrypt.encrypt_ee("#{ENV['SALTER']}-#{card.user_id}-#{ENV['CARD_ENCRYPTER']}", result.to_json)
            card.fingerprint = nil
          end
        rescue => ex
          @error_card.push(@card_id)
        end
      end
      Card.import updating_card, on_duplicate_key_update: {conflict_target: [:id], columns: [:qc_token,:fingerprint]}
    end
  end
end