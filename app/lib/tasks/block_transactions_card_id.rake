namespace :block_transactions_card_id do
  desc "Update Card ID of Block Transactions"
  task update_card_id: :environment do
    include Admins::BlockTransactionsHelper
    puts 'Updating Card ID'
    begin
      job = CronJob.new(name: 'block_transactions_card_id:update_card_id', description: 'Update Card ID of Block Transactions', success: true)
      BlockTransaction.where("main_type IN (?)",["Credit Card","eCommerce","Virtual Terminal"]).find_in_batches(batch_size: 10000) do |block_transactions|
        if block_transactions.present?
          block_transactions.each do |b_t|
            card_id = b_t.card.try(:id) || retrieve_card_id(b_t[:tags])
            last4 =  retrieve_last4(b_t[:tags])
            first6 = retrieve_first6(b_t[:tags])
            child_transactions = b_t.child_transactions.where(main_type:["Reserve Money Deposit"])
            child_transactions.update_all(card_id: card_id,last4: last4,first6: first6)
          end
        end
      end
    rescue StandardError => exc
      puts 'Error occured during Block Transaction Update: ', exc
    end
    puts 'Block Transaction Updated Successfully!'
  end
end
