namespace :update_locations_ach_limit do
  desc "Update Locations ACH Limit"
  task update_locations_ach_limit: :environment do
    puts 'Updating Locations ACH Limits'
    puts 'Update All Location ACH Limit(except location existing ach limits values)'
    begin
      id = nil
      job = CronJob.new(name: 'location_ach_limit:location_ach_limit_update', description: 'Update Locations ACH Limit', success: true)
      result = {updates: [],  error: nil}
      Location.all.find_in_batches(batch_size: 1000) do |batch|
        updated_location = []
        batch.each do |location|
          id = location.id
          if location.ach_limit.to_f == 0.0
            location_update = {id: location.id, old_value: '', new_value: ''}
            old_ach_limit = location.ach_limit
            location.ach_limit = 100000
            updated_location.push(location)
            location_update[:old_value] = old_ach_limit
            location_update[:new_value] = location.ach_limit
            result[:updates].push(location_update)
          end
        end
        Location.import updated_location, on_duplicate_key_update: {conflict_target: [:id], columns: [:ach_limit]}
        puts "Updated Successfully"
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occurred during Location ACH Limit Update: ', exc
    end
    job.result = result
    job.update_ach_limit!
    puts 'Location ACH Limit Updated Successfully!'
  end
  # --------------Locations ACH Limit Task Revert--------------------
  task revert_update_locations_ach_limit: :environment do
    puts 'Reverting Locations ACH Limit'
    job =CronJob.update_ach_limit.where(success: true, name: 'location_ach_limit:location_ach_limit_update').last
    if job["error"].nil? && job.result["updates"].present?
      begin
        job.result["updates"].each do |loc|
          location = Location.find_by_id(loc["id"])
          id = location.id
          if location.present?
            if location.ach_limit != loc["old_value"]
              location.update(ach_limit: loc["old_value"])
              puts "Reverting Locations ACH Limit For #{id}"
            end
          end
        end
      rescue StandardError => exc
        puts 'Error occurred during Locations ACH Limit Update: ', exc
      end
    end
  end
end
