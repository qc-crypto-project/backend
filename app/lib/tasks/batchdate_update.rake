namespace :batchdate do
  desc "Update Batch Date of ach Transaction"
  task batchdate_update: :environment do
    puts 'Updating Batch Date'
    begin
      id = nil
      job = CronJob.new(name: 'batchdate:batchdate_update', description: 'Update Batch Date', success: true)
      result = {updates: [],  error: nil}
      @achs=TangoOrder.instant_pay.where(:batch_date => nil).or(TangoOrder.ach.where(:batch_date => nil))
      @achs.each do |ach|
        id=ach.id
        if ach.present?
          ach.update(batch_date: (ach.created_at + 1.day))
          result[:updates].push(id)
          puts "Updated Successfully"
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Batch Date Update: ', exc
    end
    job.result = result
    job.rake_task!
    puts 'Batch Date Successfully!'
  end
  # --------------Batch Date Update Task Revert--------------------
  task revert_batchdate_update: :environment do
    puts 'Reverting Updated Batch Date Update'
    jobs = CronJob.rake_task.where(success: true, name: 'batchdate:batchdate_update')
    jobs.each do |job|
      if job["error"].nil? && job.result["updates"].present?
        begin
          job.result["updates"].each do |ach_record|
            ach_update=TangoOrder.find(ach_record)
            id=ach_record
            if ach_update.present?
              if ach_update.batch_date.blank?
                ach_update.update(batch_date: nil)
                puts "Reverting Ach Transaction For #{id}"
              end
            end
          end
        rescue StandardError => exc
          puts 'Error occured during Batch Date Update: ', exc
        end
      end
    end
  end
end
