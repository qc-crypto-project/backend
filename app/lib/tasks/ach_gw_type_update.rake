namespace :ach_gw_type_update do
  desc "update ach_type of ACH Gateways"
  task update_ach_gw_type: :environment do

    AchGateway.where(gateway_type: 'ach', ach_type: nil).update_all(ach_type: 'manual_integration')

  end
end