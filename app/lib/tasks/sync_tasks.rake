namespace :sync_tasks do

  task update_fees: :environment do
    transactions = BlockTransaction.all
    updates = []
    if transactions.present?
      transactions.each do |transaction|
        if transaction.tags.present?
          if transaction.tags["fee_perc"].present?
            puts "updating transaction# #{transaction.id}"
            tags = transaction.tags["fee_perc"]
            if tags.try(:[], "iso").present?
              puts "iso fee is #{tags["iso"]["amount"].to_f}"
              transaction.iso_fee = tags["iso"]["amount"].to_f
            elsif tags.try(:[], "iso_fee").present?
              puts "iso fee is #{tags["iso_fee"].to_f}"
              transaction.iso_fee = tags["iso_fee"].to_f
            elsif tags.try(:[], "iso_total_fee").present?
              puts "iso fee is #{tags["iso_total_fee"].to_f}"
              transaction.iso_fee = tags["iso_total_fee"].to_f
            else
              transaction.iso_fee = 0.0
            end
            if tags.try(:[], "agent").present?
              puts "agent fee is #{tags["agent"]["amount"].to_f}"
              transaction.agent_fee = tags["agent"]["amount"].to_f
            elsif tags.try(:[], "agent_fee").present?
              puts "agent fee is #{tags["agent_fee"].to_f}"
              transaction.agent_fee = tags["agent_fee"].to_f
            elsif tags.try(:[], "agent_total_fee").present?
              puts "agent fee is #{tags["agent_total_fee"].to_f}"
              transaction.agent_fee = tags["agent_total_fee"].to_f
            else
              transaction.agent_fee = 0.0
            end
            if tags.try(:[], "partner").present?
              puts "partner fee is #{tags["partner"]["amount"].to_f}"
              transaction.partner_fee = tags["partner"]["amount"].to_f
            elsif tags.try(:[], "partner_fee").present?
              puts "partner fee is #{tags["partner_fee"].to_f}"
              transaction.partner_fee = tags["partner_fee"].to_f
            elsif tags.try(:[], "partner_total_fee").present?
              puts "partner fee is #{tags["partner_total_fee"].to_f}"
              transaction.partner_fee = tags["partner_total_fee"].to_f
            else
              transaction.partner_fee = 0.0
            end
            if tags.try(:[], "affiliate").present?
              puts "affiliate fee is #{tags["affiliate"]["amount"].to_f}"
              transaction.affiliate_fee = tags["affiliate"]["amount"].to_f
            else
              transaction.affiliate_fee = 0.0
            end
            if tags.try(:[], "gbox").present?
              puts "gbox fee is #{tags["gbox"]["amount"].to_f}"
              transaction.gbox_fee = tags["gbox"]["amount"].to_f
            elsif tags.try(:[], "partner_total_fee").present? || tags.try(:[], "agent_total_fee").present? || tags.try(:[], "iso_total_fee").present? || tags.try(:[], "partner_fee").present? || tags.try(:[], "agent_fee").present? || tags.try(:[], "iso_fee").present?
              transaction.gbox_fee = SequenceLib.dollars(transaction.fee_in_cents).to_f - tags.try(:[], "partner_total_fee").to_f - tags.try(:[], "agent_total_fee").to_f - tags.try(:[], "iso_total_fee").to_f - tags.try(:[], "partner_fee").to_f - tags.try(:[], "agent_fee").to_f - tags.try(:[], "iso_fee").to_f
            else
              transaction.gbox_fee = 0.0
            end
            updates << transaction
          end
        end
      end
      slice = updates.each_slice(10000).to_a
      slice.each do |t|
        BlockTransaction.import t, on_duplicate_key_update: {conflict_target: [:id], columns: [:iso_fee,:agent_fee,:affiliate_fee,:partner_fee,:gbox_fee]}
      end
      # BlockTransaction.import updates, on_duplicate_key_update: {conflict_target: [:id], columns: [:iso_fee,:agent_fee,:affiliate_fee,:partner_fee,:gbox_fee]}
      puts "updated successfully"
    end
  end

  task update_fields: :environment do
    include AdminsHelper
    include Admins::BlockTransactionsHelper
    begin
      transactions = BlockTransaction.all
      job = CronJob.new(name: 'sync_tasks:update_fields', description: 'update fields in block transaction', success: true)
      result = {updates: [], error: nil}
      updates = []
      transactions.try(:each) do |transaction|
        old_value = {
            amount_in_cents: transaction.amount_in_cents,
            tx_amount: transaction.tx_amount,
            privacy_fee: transaction.privacy_fee,
            net_fee: transaction.net_fee,
            total_net: transaction.total_net
        }
        total_net = 0
        if transaction.action == "retire" || transaction.tags["type"] == "send_check"
          total_net = SequenceLib.dollars(transaction.amount_in_cents)
        elsif transaction.tags["fee_perc"].try(:[], "reserve").present? && (transaction.tags["type"] != "Fee_Transfer")
          total_net = SequenceLib.dollars(transaction.amount_in_cents).to_f - SequenceLib.dollars(transaction.fee_in_cents).to_f - transaction.tags["fee_perc"]["reserve"]["amount"].to_f
        elsif transaction.tags["type"] == "Fee_Transfer"
          total_net = SequenceLib.dollars(transaction.amount_in_cents)
        else
          total_net = SequenceLib.dollars(transaction.amount_in_cents).to_f - SequenceLib.dollars(transaction.fee_in_cents).to_f
        end
        transaction.tx_amount = SequenceLib.cents(get_main_amount(SequenceLib.dollars(transaction.amount_in_cents), transaction.tags))
        transaction.privacy_fee = transaction.tags.try(:[], "privacy_fee")
        transaction.net_fee = SequenceLib.dollars(transaction.fee_in_cents).to_f - transaction.privacy_fee.to_f
        transaction.total_net = total_net
        transaction.amount_in_cents = SequenceLib.cents(get_total_amount(SequenceLib.dollars(transaction.amount_in_cents), transaction.tags))
        updates << transaction
        new_value = {
            amount_in_cents: transaction.amount_in_cents,
            tx_amount: transaction.tx_amount,
            privacy_fee: transaction.privacy_fee,
            net_fee: transaction.net_fee,
            total_net: transaction.total_net
        }
        result[:updates].push({id: transaction.id,old_value: old_value, new_value: new_value})
      end
      slice = updates.each_slice(10000).to_a
      slice.each do |t|
        BlockTransaction.import t, on_duplicate_key_update: {conflict_target: [:id], columns: [:tx_amount,:amount_in_cents,:privacy_fee,:net_fee,:total_net]}
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during the task process: ', exc
    end
    job.result = result
    job.rake_task!
  end

  task revert_update_fields: :environment do
    begin
      job = CronJob.where(name: "sync_tasks:update_fields").first
      updates = []
      job.result["updates"].each do |data|
        transaction = BlockTransaction.find_by_id(data["id"])
        if transaction.present?
          old_values = data["old_value"]
          transaction.amount_in_cents = old_values["amount_in_cents"]
          transaction.privacy_fee = old_values["privacy_fee"]
          transaction.net_fee = old_values["net_fee"]
          transaction.total_net = old_values["total_net"]
          transaction.tx_amount = old_values["tx_amount"]
          updates << transaction
        end
      end
      slice = updates.each_slice(10000).to_a
      slice.each do |t|
        BlockTransaction.import t, on_duplicate_key_update: {conflict_target: [:id], columns: [:tx_amount,:amount_in_cents,:privacy_fee,:net_fee,:total_net]}
      end
      puts "success fullly reverted"
    rescue StandardError => exc
      puts 'Error occured during the task process: ', exc
    end
  end

  task add_sender_destination: :environment do
    transactions = BlockTransaction.all
    updates = []
    transactions.each do |transaction|
      if transaction.sender_id.blank?
        if transaction.sender_wallet_id.present?
          wallet = Wallet.find_by_id(transaction.sender_wallet_id)
          if wallet.present?
            transaction.sender_id = wallet.users.try(:first).try(:id)
          end
        end
      end
      if transaction.receiver_id.blank?
        if transaction.receiver_wallet_id.present?
          wallet = Wallet.find_by_id(transaction.receiver_wallet_id)
          if wallet.present?
            transaction.receiver_id = wallet.users.try(:first).try(:id)
          end
        end
      end
      updates << transaction
    end
    update = updates.each_slice(10000).to_a
    update.each do |t|
      BlockTransaction.import t, on_duplicate_key_update: {conflict_target: [:id], columns: [:sender_id, :receiver_id]}
    end
  end

  task update_gateway: :environment do
    puts "start updating"
    update_time = Time.now
    limit = 2000
    offset = 1
    # trs = Transaction.where("payment_gateway_id IS NULL AND updated_at < :time", time: update_time).limit(limit)
    gateways = PaymentGateway.all.select(:id, :key, :name)
    while offset > 0
      offset = 0
      puts "processing #{limit} transactions with offset #{offset}"
      trs = Transaction.where("payment_gateway_id IS NULL AND updated_at < :time", time: update_time).limit(limit).offset(offset)
      updates = trs.map do |tx|
        if tx.tags.try(:[],"source").present?
          gateway = gateways.detect{|g| g.key == tx.tags["source"] || g.name == tx.tags["source"]}
          tx.payment_gateway_id = gateway.try(:id)
          offset += 1
        end
        tx.updated_at = Time.now
        tx
      end
      puts "importing transactions...", updates
      Transaction.import updates, on_duplicate_key_update: {conflict_target: [:id], columns: [:payment_gateway_id, :updated_at]}
      puts "imported #{offset} transactions"
    end
  end

  task update_gateway_fees: :environment do
    puts "start updating"
    gateways = PaymentGateway.where.not(type: "slot").select(:id, :key, :name,:account_processing_limit, :transaction_fee,:charge_back_fee, :retrieval_fee, :per_transaction_fee, :reserve_money, :reserve_money_days, :monthly_service_fee, :misc_fee, :misc_fee_dollars)
    gateways.find_each do |gateway|
      BlockTransaction.where("payment_gateway_id = :gateway OR gateway LIKE :name OR gateway LIKE :key", gateway: gateway.id, name: gateway.name, key: gateway.key).update_all(
          transaction_fee: gateway.transaction_fee,
          per_transaction_fee: gateway.per_transaction_fee,
          # charge_back_fee: gateway.charge_back_fee,
          # retrieval_fee: gateway.retrieval_fee,
          # account_processing_limit: gateway.account_processing_limit,
          # reserve_money: gateway.reserve_money,
          # reserve_money_days: gateway.reserve_money_days,
          # monthly_service_fee: gateway.monthly_service_fee,
          # misc_fee: gateway.misc_fee,
          # misc_fee_dollars: gateway.misc_fee_dollars,
      )
    end
    puts "updated successfully"
  end

  task update_time: :environment do
    puts "Start updating"
    transactions = BlockTransaction.where(timestamp: nil)
    if transactions.present?
      transactions.each do |tx|
        seq_tx = SequenceLib.get_action_id(tx.sequence_id)
        if seq_tx.present?
          tx.update(timestamp: seq_tx.first.timestamp)
        end
      end
    end
    puts "updation done"
  end

  task update_tx_time: :environment do
    puts "Start updating"
    transactions = Transaction.where(timestamp: nil).where.not(seq_transaction_id: nil)
    if transactions.present?
      transactions_slice = transactions.each_slice(10000).to_a
      transactions_slice.each do |tx_slice|
        transactions_batch = tx_slice.each do|tx|
          seq_tx = SequenceLib.get_action_id(tx.seq_transaction_id) if tx.seq_transaction_id.present?
          if seq_tx.present?
            tx.timestamp = seq_tx.first.timestamp
          end
        end
        Transaction.import transactions_batch, on_duplicate_key_update: {conflict_target: [:id], columns: [:timestamp]}
      end
    end
    puts "updation done"
  end

  task seq_parent: :environment do
    puts "Start updating"
    transactions = BlockTransaction.where(seq_parent_id: nil)
    if transactions.present?
      transactions.each do |tx|
        parent_tx = tx.parent_transaction
        if parent_tx.present?
          tx.update(seq_parent_id: parent_tx.sequence_id)
        end
      end
    end
    puts "updation done"
  end

  task tx_2018: :environment do
    include ActionView::Helpers::NumberHelper
    begin
      t1 = "01/06/2018".to_datetime
      t2 = "01/01/2019".to_datetime
      block_transactions = BlockTransaction.where('timestamp >= :t1 AND timestamp < :t2', t1: t1, t2: t2).order(timestamp: :asc)
      if block_transactions.present?
        file = File.new("#{Rails.root}/tmp/report_2018.csv", "w")
        file << CSV.generate do |csv|
          csv << ["Block Id", "Date/Time","Main Type", "Sub Type","Action","Gateway", "Sender Name", "Receiver Name", "Sender Wallet", "Receiver Wallet", "Total Amount", "Tx Amount","Privacy Fee","Fee","Tip","Net Fee", "Net Amount","Reserve Money",
                  "Gbox Fee","Iso Fee","Agent Fee", "Affiliate Fee"]
          block_transactions.find_each do |tx|
            sender_user = Wallet.where(id: tx.sender_wallet_id).try(:first).try(:users) if tx.sender_wallet_id.present? && tx.sender_wallet_id != 0
            receiver_user = Wallet.where(id: tx.receiver_wallet_id).try(:first).try(:users) if tx.receiver_wallet_id.present? && tx.receiver_wallet_id != 0
            csv << [tx.sequence_id.first(6),
                    tx.timestamp.to_datetime.strftime("%m/%d/%Y %I:%M:%S %p"),
                    tx.main_type,
                    tx.sub_type,
                    tx.action,
                    tx.gateway,
                    sender_user.try(:name),
                    receiver_user.try(:name),
                    tx.sender_wallet_id,
                    tx.receiver_wallet_id,
                    number_with_precision(number_to_currency(SequenceLib.dollars(tx.amount_in_cents)), precision: 2),
                    number_with_precision(number_to_currency(SequenceLib.dollars(tx.tx_amount)), precision: 2),
                    number_with_precision(number_to_currency(tx.privacy_fee), precision: 2),
                    number_with_precision(number_to_currency(SequenceLib.dollars(tx.fee_in_cents)), precision: 2),
                    number_with_precision(number_to_currency(SequenceLib.dollars(tx.tip_cents)), precision: 2),
                    number_with_precision(number_to_currency(tx.net_fee), precision: 2),
                    number_with_precision(number_to_currency(tx.total_net), precision: 2),
                    number_with_precision(number_to_currency(SequenceLib.dollars(tx.hold_money_cents)), precision: 2),
                    number_with_precision(number_to_currency(tx.gbox_fee), precision: 2),
                    number_with_precision(number_to_currency(tx.iso_fee), precision: 2),
                    number_with_precision(number_to_currency(tx.agent_fee), precision: 2),
                    number_with_precision(number_to_currency(tx.affiliate_fee), precision: 2),
            ]
          end
        end
        file.close
        SlackService.upload_files("2018 transactions report", file, "#quickcard-internal", "2018_report")
      end
      puts "file exported successfully"
    rescue => exc
      puts exc.message
      puts exc.backtrace
    end
  end

  task update_iso_wallet: :environment do
    # task for updating mony zoe wallet in baked location users
    begin
      locations = Location.all
      locations.find_each do |location|
        if location.profit_split_detail.present?
          profit_detail = JSON.parse(location.profit_split_detail)
          splits = profit_detail["splits"]
          baked_splits = splits.reject{|e| e == "super_company" || e == "net" || e == "ez_merchant"}
          if baked_splits.present?
            baked_splits.each do |split|
              split.second.values.map! do |value|
                if value["wallet"] == "40495"
                  value["wallet"] = "606091"
                end
              end
            end
            location.profit_split_detail = profit_detail.to_json
            location.save
          end
        end
      end
      puts "updated baked users successfully"
    rescue => exc
      puts exc.message
      puts exc.backtrace
    end
  end

  task update_main_amount: :environment do
    #   task for updating main amount in block transaction table
    BlockTransaction.where(main_amount: nil).order(timestamp: :asc).find_in_batches(batch_size: 1000) do |block|
      if block.present?
        batch = block.each do |transaction|
          main_amount = 0
          main_amount = transaction.tags.try(:[],"main_transaction_info").try(:[],"amount")
          main_amount = SequenceLib.dollars(transaction.parent_transaction.try(:amount_in_cents)) if main_amount.blank? || main_amount.to_f == 0
          main_amount = SequenceLib.dollars(BlockTransaction.where(seq_parent_id: transaction.seq_block_id, position: '0').first.try(:amount_in_cents)) || 0 if transaction.seq_block_id.present? && (main_amount.blank? || main_amount.to_f == 0)
          transaction.main_amount = main_amount.to_f
          transaction
        end
        BlockTransaction.import batch, on_duplicate_key_update: {conflict_target: [:sequence_id], columns: [:main_amount, :updated_at]}
      end
    end
  end
end