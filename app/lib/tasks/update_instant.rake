namespace :update_instant do
  desc "Updating Instant Fee as Send check Fee"
  task fee: :environment do
    puts "Updating Fee Table"
    Fee.all.each do|f|
      puts "updating Fee id => #{f.id}"
      send_check_dollar = f.send_check
      send_check_perc = f.redeem_fee
      f.update(dc_deposit_fee_dollar: send_check_dollar,dc_deposit_fee: send_check_perc)
    end
    puts "Fee Updated"
    puts "============="
    puts "Updating ISO AGENT Affiliate"
    users = User.iso + User.agent + User.affiliate
    users.each do|u|
      send_check_perc = 0
      send_check_dollar = 0
      if u.system_fee.present? && u.system_fee.class == Hash
        puts "updating User id => #{u.id}"
        system_fees = u.system_fee
        send_check_perc = u.system_fee["redeem_fee"] if u.system_fee["redeem_fee"].present?
        send_check_dollar = u.system_fee["send_check"] if u.system_fee["send_check"].present?
        system_fees["push_to_card_dollar"] = "#{send_check_dollar}"
        system_fees["push_to_card_percent"] = "#{send_check_perc}"
        u.update(system_fee: system_fees)
      end
    end
    puts "Users Updated"
  end
end