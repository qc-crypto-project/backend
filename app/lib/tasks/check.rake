namespace :check do
  desc "Checking environment variables"
  task env_var: :environment do

    if !ENV['AWS_ACCESS_KEY_ID'].present?
      puts "AWS_ACCESS_KEY_ID not present"
      return false
    end
    if !ENV['AWS_REGION'].present?
      puts "AWS_REGION not present"
      return false
    end
    if !ENV['AWS_SECRET_ACCESS_KEY'].present?
      puts "AWS_SECRET_ACCESS_KEY not present"
      return false
    end
    if !ENV['LANG'].present?
      puts "LANG not present"
      return false
    end
    if !ENV['LEDGER_CREDENTIAL'].present?
      puts "LEDGER_CREDENTIAL not present"
      return false
    end
    if !ENV['RAILS_LOG_TO_STDOUT'].present?
      puts "RAILS_LOG_TO_STDOUT not present"
      return false
    end
    if !ENV['RAILS_SERVE_STATIC_FILES'].present?
      puts "RAILS_SERVE_STATIC_FILES not present"
      return false
    end
    if !ENV['S3_BUCKET_NAME'].present?
      puts "S3_BUCKET_NAME not present"
      return false
    end
    if !ENV['S3_HOST_NAME'].present?
      puts "S3_HOST_NAME not present"
      return false
    end
    if !ENV['SECRET_KEY_BASE'].present?
      puts "SECRET_KEY_BASE not present"
      return false
    end
    if !ENV['SLACK_API_TOKEN'].present?
      puts "SLACK_API_TOKEN not present"
      return false
    end
    if !ENV['TWILIO_SID'].present?
      puts "TWILIO_SID not present"
      return false
    end
    if !ENV['TWILIO_TOKEN'].present?
      puts "TWILIO_TOKEN not present"
      return false
    end
    if !ENV['stripe_publishable_key'].present?
      puts "stripe_publishable_key not present"
      return false
    end
    if !ENV['stripe_secret_key'].present?
      puts "stripe_secret_key not present"
      return false
    end
    if !ENV['converge_merchant_id'].present?
      puts "converge_merchant_id not present"
      return false
    end
    if !ENV['converge_user_id'].present?
      puts "converge_user_id not present"
      return false
    end
    if !ENV['converge_pin'].present?
      puts "converge_pin not present"
      return false
    end
    if !ENV['LEDGER_NAME'].present?
      puts "LEDGER_NAME not present"
      return false
    end
    if !ENV['CHECKBOOK_KEY'].present?
      puts "CHECKBOOK_KEY not present"
      return false
    end
    if !ENV['CHECKBOOK_SECRET'].present?
      puts "CHECKBOOK_SECRET not present"
      return false
    end
    if !ENV['CHECKBOOK_URL_LIVE'].present?
      puts "CHECKBOOK_URL_LIVE not present"
      return false
    end
    if !ENV['stripe'].present?
      puts "stripe not present"
      return false
    end
    if !ENV['PLAID_CLIENT_ID'].present?
      puts "PLAID_CLIENT_ID not present"
      return false
    end
    if !ENV['PLAID_SECRET'].present?
      puts "PLAID_SECRET not present"
      return false
    end
    if !ENV['PLAID_PUBLIC_KEY'].present?
      puts "PLAID_PUBLIC_KEY not present"
      return false
    end
    if !ENV['SENDGRID_API_KEY'].present?
      puts "SENDGRID_API_KEY not present"
      return false
    end
    if !ENV['SENDGRID_PASSWORD'].present?
      puts "SENDGRID_PASSWORD not present"
      return false
    end
    if !ENV['SENDGRID_USERNAME'].present?
      puts "SENDGRID_USERNAME not present"
      return false
    end
    if !ENV['SMTP_ADDRESS'].present?
      puts "SMTP_ADDRESS not present"
      return false
    end
    if !ENV['SMTP_DOMAIN'].present?
      puts "SMTP_DOMAIN not present"
      return false
    end
    if !ENV['APP_ROOT'].present?
      puts "APP_ROOT not present"
      return false
    end
    if !ENV['CONVERGE_URL'].present?
      puts "CONVERGE_URL not present"
      return false
    end
    if !ENV['PAYEEZY_API_KEY'].present?
      puts "PAYEEZY_API_KEY not present"
      return false
    end
    if !ENV['PAYEEZY_API_SECRET'].present?
      puts "PAYEEZY_API_SECRET not present"
      return false
    end
    if !ENV['PAYEEZY_MERCHANT_ID'].present?
      puts "PAYEEZY_MERCHANT_ID not present"
      return false
    end
    if !ENV['PAYEEZY_MERCHANT_TOKEN'].present?
      puts "PAYEEZY_MERCHANT_TOKEN not present"
      return false
    end
    if !ENV['PAYEEZY_REPORT_TOKEN'].present?
      puts "PAYEEZY_REPORT_TOKEN not present"
      return false
    end
    if !ENV['PAYEEZY_TRANSACTION_TYPE'].present?
      puts "PAYEEZY_TRANSACTION_TYPE not present"
      return false
    end
    if !ENV['PAYEEZY_TRANSACTION_URL'].present?
      puts "PAYEEZY_TRANSACTION_URL not present"
      return false
    end
    if !ENV['PAYEEZY_API_URL'].present?
      puts "PAYEEZY_API_URL not present"
      return false
    end
    if !ENV['TA_TOKEN'].present?
      puts "TA_TOKEN not present"
      return false
    end
    if !ENV['RECAPTCHA_SECRET_KEY'].present?
      puts "RECAPTCHA_SECRET_KEY not present"
      return false
    end
    if !ENV['RECAPTCHA_SITE_KEY'].present?
      puts "RECAPTCHA_SITE_KEY not present"
      return false
    end
    if !ENV['BRIDGE_PAY_PASS'].present?
      puts "BRIDGE_PAY_PASS not present"
      return false
    end
    if !ENV['BRIDGE_PAY_SOAP_URL'].present?
      puts "BRIDGE_PAY_SOAP_URL not present"
      return false
    end
    if !ENV['BRIDGE_PAY_URL'].present?
      puts "BRIDGE_PAY_URL not present"
      return false
    end
    if !ENV['BRIDGE_PAY_USER'].present?
      puts "BRIDGE_PAY_USER not present"
      return false
    end
    if !ENV['PHONE_NUMBER_FOR_PAY_INVOICE1'].present? && !ENV['PHONE_NUMBER_FOR_PAY_INVOICE2'].present?
      puts "PHONE_NUMBER_FOR_PAY_INVOICE not present"
      return false
    end
  end


end
