namespace :order_bank_tx do
  desc "Creating transactions from order bank"
  task create_with_gateway_exist: :environment do
    puts "Starting....."
    begin
      limit = 5000
      offset = 1
      while offset > 0
        puts "processing with offset #{offset}"
        order_bank = OrderBank.where('created_at > ? AND created_at < ?', "22/01/2019".to_datetime,"01/05/2019".to_datetime).where(status: "decline",transaction_id: nil).where('id > ?', offset).order('id asc').limit(limit)
        if order_bank.present?
          offset = order_bank.last.id
        else
          offset = 0
        end

        transactions_batch = []
        order_bank.each do|order|
          if order.bank_type.present?
            payment_gateway = PaymentGateway.where(key: order.bank_type).first
            # payment_gateway = PaymentGateway.where(type: PaymentGateway.types["#{order.bank_type}"]).first if payment_gateway.nil?
            if payment_gateway.present?
              if order.transaction_info.present?
                if order.transaction_info.class ==  String && order.transaction_info.present? && order.transaction_info.first == "{"
                  transaction_info  = JSON(order.transaction_info)
                  card_id = nil
                  if transaction_info["last4"].present? && transaction_info["first6"].present?
                    card_id = Card.where(last4: transaction_info["last4"],first6: transaction_info["first6"]).try(:last).try(:id)
                  end
                  if order.user_id == order.merchant_id #credit
                    receiver_id = order.merchant_id
                    receiver_wallet_id = transaction_info["reciever_wallet_id"]
                    to = transaction_info["reciever_wallet_id"]
                    from = "Credit"
                    main_type = TypesEnumLib::TransactionViewTypes::CreditCard
                  else
                    receiver_id = order.user_id
                    receiver_wallet_id = transaction_info["sender_wallet_id"]
                    to = transaction_info["sender_wallet_id"]
                    from = transaction_info["sender_wallet_id"]
                    main_type = TypesEnumLib::TransactionViewTypes::SaleIssue
                  end
                  transactions_batch.push(
                      Transaction.new(
                                  to: to,
                                  from: from,
                                  action: "issue",
                                  status: "pending",
                                  amount: order.amount,
                                  total_amount: order.amount,
                                  net_amount: order.amount,
                                  fee: transaction_info["fee"],
                                  ip: transaction_info["ip"],
                                  last4: transaction_info["last4"],
                                  first6: transaction_info["first6"],
                                  card_id: card_id,
                                  receiver_id: receiver_id,
                                  user_id: receiver_id,
                                  receiver_wallet_id: receiver_wallet_id,
                                  timestamp: order.created_at,
                                  created_at: order.created_at,
                                  payment_gateway_id: payment_gateway.id,
                                  main_type: main_type,
                                  order_bank_id: order.id
                  ))
                else
                  transactions_batch.push(transactions_info_empty(order,payment_gateway))
                end
              else
                transactions_batch.push(transactions_info_empty(order,payment_gateway))
              end
            end
          end
        end

        if transactions_batch.present?
          puts "updating"
          Transaction.import transactions_batch, on_duplicate_key_update: {conflict_target: [:id], columns: [:to, :from, :action, :status, :amount, :total_amount, :net_amount, :fee, :ip, :last4, :first6, :card_id,:receiver_id, :user_id, :receiver_wallet_id,:timestamp,:payment_gateway_id,:main_type,:order_bank_id]}
        else
          puts "Completed"
        end
      end
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
    end
    puts "Done...."
  end

  task create_without_gateway_exist: :environment do
    puts "Starting....."
    begin
      order_bank = OrderBank.where('created_at > ? AND created_at < ?', "01/01/2019".to_datetime,"23/05/2019".to_datetime).where(status: "decline",transaction_id: nil)

      transactions_batch = []
      order_bank.each do|order|
        if order.bank_type.present?
          # payment_gateway = PaymentGateway.where(key: order.bank_type)
          payment_gateway = PaymentGateway.where(type: PaymentGateway.types["#{order.bank_type}"]).first
          if payment_gateway.present?
            if order.transaction_info.present?
              if order.transaction_info.class ==  String && order.transaction_info.present? && order.transaction_info.first == "{"
                transaction_info  = JSON(order.transaction_info)
                card_id = nil
                if transaction_info["last4"].present? && transaction_info["first6"].present?
                  card_id = Card.where(last4: transaction_info["last4"],first6: transaction_info["first6"]).last.try(:id)
                end
                if order.user_id == order.merchant_id #credit
                  receiver_id = order.merchant_id
                  receiver_wallet_id = transaction_info["reciever_wallet_id"]
                  to = transaction_info["reciever_wallet_id"]
                  from = "Credit"
                  main_type = TypesEnumLib::TransactionViewTypes::CreditCard
                else
                  receiver_id = order.user_id
                  receiver_wallet_id = transaction_info["sender_wallet_id"]
                  to = transaction_info["sender_wallet_id"]
                  from = transaction_info["sender_wallet_id"]
                  main_type = TypesEnumLib::TransactionViewTypes::SaleIssue
                end
                transactions_batch.push(
                    Transaction.new(
                        to: to,
                        from: from,
                        action: "issue",
                        status: "pending",
                        amount: order.amount,
                        total_amount: order.amount,
                        net_amount: order.amount,
                        fee: transaction_info["fee"],
                        ip: transaction_info["ip"],
                        last4: transaction_info["last4"],
                        first6: transaction_info["first6"],
                        card_id: card_id,
                        receiver_id: receiver_id,
                        user_id: receiver_id,
                        receiver_wallet_id: receiver_wallet_id,
                        timestamp: order.created_at,
                        created_at: order.created_at,
                        payment_gateway_id: payment_gateway.id,
                        main_type: main_type,
                        order_bank_id: order.id
                    ))
              else
                transactions_batch.push(transactions_info_empty(order,payment_gateway))
              end
            else
              transactions_batch.push(transactions_info_empty(order,payment_gateway))
            end
          end
        end
      end

      if transactions_batch.present?
        puts "updating"
        Transaction.import transactions_batch, on_duplicate_key_update: {conflict_target: [:id], columns: [:to, :from, :action, :status, :amount, :total_amount, :net_amount, :fee, :ip, :last4, :first6, :card_id,:receiver_id, :user_id, :receiver_wallet_id,:timestamp,:payment_gateway_id,:main_type,:order_bank_id]}
      else
        puts "Completed"
      end
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
    end
    puts "Done...."
  end

  task delete_tx: :environment do
    puts "Starting....."
    begin
      transactions = Transaction.where.not(order_bank_id: nil)
      transactions.delete_all
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
    end
    puts "Done...."
  end

  task order_bank_tx_id: :environment do
    puts "Starting....."
    begin
      transactions = Transaction.where.not(order_bank_id: nil)

      order_bank_batch = []
      transactions.each do|tx|
        order = OrderBank.find_by(id: tx.order_bank_id)
        if order.present?
          order.transaction_id = tx.id
          order_bank_batch.push(order)
        end
      end

      if order_bank_batch.present?
        puts "updating"
        OrderBank.import order_bank_batch, on_duplicate_key_update: {conflict_target: [:id], columns: [:transaction_id]}
      else
        puts "Completed"
      end
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
    end
    puts "Done...."
  end

  def transactions_info_empty(order,payment_gateway)
    if order.user_id == order.merchant_id #credit
      receiver_id = order.merchant_id
      to = order.merchant_id
      from = "Credit"
    else
      receiver_id = order.user_id
      to = order.user_id
      from = order.user_id
    end
    receiver_wallet_id = User.find_by(id: receiver_id).try(:wallets).try(:primary).try(:first).try(:id)

    return Transaction.new(
        to: to,
        from: from,
        action: "issue",
        status: "pending",
        amount: order.amount,
        total_amount: order.amount,
        net_amount: order.amount,
        receiver_id: receiver_id,
        user_id: receiver_id,
        receiver_wallet_id: receiver_wallet_id,
        timestamp: order.created_at,
        created_at: order.created_at,
        payment_gateway_id: payment_gateway.id,
        main_type: TypesEnumLib::TransactionViewTypes::SaleIssue,
        order_bank_id: order.id
    )
  end

end