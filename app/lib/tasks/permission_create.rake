namespace :permission do
  desc "ADDING DEFAULT MERCHANT USER PERMISSION"
  task permission_create: :environment do
    puts 'Create Permission for Merchant User'
    job = CronJob.new(name: 'permission', description: 'Create Permission for Merchant User', success: true)
    object = {updates: [], error: nil}
    begin
      if Permission.find_by_permission_type("admin").nil?
        p "Creating #{"admin".upcase} Permission..."
        Permission.create!( name: "Admin (Full Access)", permission_type: "admin", wallet: true, refund: true, transfer: true, b2b: true, virtual_terminal: true, dispute_view_only: true, dispute_submit_evidence: true, accept_dispute: true, funding_schedule: true, check: true, push_to_card: true, ach: true, gift_card: true, sales_report: true, checks_report: true, gift_card_report: true, user_view_only: true, user_edit: true, user_add: true, developer_app: true )
      end
      if Permission.find_by_permission_type("regular").nil?
        p "Creating #{"regular".upcase} Permission..."
        Permission.create!( name: "Regular (VT Only)", permission_type: "regular",virtual_terminal: true )
      end
      p "-------------DEFAULT MERCHANT USER PERMISSION ADDED--------------"
      object[:updates] = Permission.pluck(:id)
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during Permission Create: ', exc
    end
    job.result = object
  end

  task revert_permission_create: :environment do
    puts 'reverting Create Permission for Merchant User'
    jobs = CronJob.rake_task.where(success: true, name: 'permission:permission_create')
    jobs.each do |job|
      puts "reverting job #{job.id}"
      begin
        if job.result["updates"].present? && job.result["error"].blank?
          permission = job.result["updates"]
          puts "reverting permission: #{permission}"
          permission = Permission.find_by_id(permission)
          permission.destroy if permission.present?
          puts "Done"
        end
      rescue StandardError => exc
        job.success = false
        object[:error] = "Error: #{exc.message}"
        puts 'Error occured during Create Permission revert: ', exc
      end
    end
  end
end
