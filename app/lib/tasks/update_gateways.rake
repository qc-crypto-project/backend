namespace :update_gateways do
  desc "move gateways to location"
  task move_gateways: :environment do
    begin
      id = nil
      job = CronJob.new(name: "update_gateways:move_gateways", description: 'move gateways from merchant to locations', success: true)
      result = {updated_locations: [], not_updated_locations: [], error: nil}
      merchants = User.merchant.where(merchant_id: nil)
      merchants.try(:each) do |merchant|
        id = merchant.id
        merchant_gateway = {merchant_id: merchant.id, merchant_gateway: [], location_id: [], location_gateway: []}
        if merchant.primary_gateway_id.present? || merchant.secondary_gateway_id.present? ||  merchant.ternary_gateway_id.present?
          wallets = merchant.wallets.try(:primary)
          first = merchant.primary_gateway_id
          second = merchant.secondary_gateway_id
          third = merchant.ternary_gateway_id
          puts "start updating"
          wallets.try(:each) do |wallet|
            location = wallet.location
            if location.present?
              if first.present?
                puts "update first gateway"
                location.update(primary_gateway_id: merchant.primary_gateway_id)
                merchant_gateway[:merchant_gateway] << merchant.primary_gateway_id
                merchant_gateway[:location_id] << location.id
                merchant_gateway[:location_gateway] << location.primary_gateway_id
              end
              if second.present?
                puts "update second gateway"
                location.update(secondary_gateway_id: merchant.secondary_gateway_id)
                merchant_gateway[:merchant_gateway] << merchant.secondary_gateway_id
                merchant_gateway[:location_id] << location.id
                merchant_gateway[:location_gateway] << location.secondary_gateway_id
              end
              if third.present?
                puts "update third gateway"
                location.update(ternary_gateway_id: merchant.ternary_gateway_id)
                merchant_gateway[:merchant_gateway] << merchant.ternary_gateway_id
                merchant_gateway[:location_id] << location.id
                merchant_gateway[:location_gateway] << location.ternary_gateway_id
              end
            end
          end
          result[:updated_locations] << merchant_gateway
          puts "updated successfully"
        else
          merchant_gateway[:reason] = "no gateway exists"
          result[:not_updated_locations] << merchant_gateway
          puts "NO Gateway Exist FOR MERCHANT ID: #{merchant.id}"
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Gateway Update: ', exc
    end
    job.result = result
    job.rake_task!
  end

  task move_risk: :environment do
    begin
      id = nil
      job = CronJob.new(name: "update_gateways:move_risk", description: 'move risk from merchant to locations', success: true)
      result = {updated_locations: [], not_updated_locations: [], error: nil}
      merchants = User.merchant.where(merchant_id: nil)
      merchants.try(:each) do |merchant|
        id = merchant.id
        merchant_gateway = {merchant_id: merchant.id, merchant_risk: [], location_ids: [], location_risk: []}
        locations = merchant.get_locations
        puts "start updating"
        if merchant.high?
          locations.update_all(risk: :high)
          merchant_gateway[:location_ids] << locations.ids
          merchant_gateway[:merchant_risk] << "high"
          merchant_gateway[:location_risk] << "high"
        elsif merchant.low?
          locations.update_all(risk: :low)
          merchant_gateway[:location_ids] << locations.ids
          merchant_gateway[:merchant_risk] << "low"
          merchant_gateway[:location_risk] << "low"
        end
        result[:updated_locations] << merchant_gateway
        puts "updated successfully"
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Risk Update: ', exc
    end
    job.result = result
    job.rake_task!
  end
end