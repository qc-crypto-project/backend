namespace :merchant_owner do
  desc "create merchant owner if not present"
  task add_owner: :environment do
    puts 'Adding Owner for Merchant'
    begin
      id = nil
      job = CronJob.new(name: 'merchant_owner:add_owner', description: 'Add First Owner For Merchant', success: true)
      result = {updates: [],  error: nil}
      User.merchant.find_each do |m|
        if m.owners.blank?
          id = m.id
          owner = Owner.new(user_id:m.id, name:m.try(:owner_name), ownership: "0%", email: m.email)
          owner.save
          result[:updates].push(owner.id)
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during saving Owner: ', exc
    end
    job.result = result
    job.add_owner!
    puts 'Owners Saved Successfully!'
  end

  # --------------Add Owner Task Revert--------------------

  task revert_add_owner: :environment do
    puts 'Reverting Owner added for Merchant'
    jobs = CronJob.add_owner.where(success: true, name: 'merchant_owner:add_owner')
    jobs.each do |job|
      if job["error"].nil? && job.result["updates"].present?
        begin
          job.result["updates"].each do |owner_id|
            id = owner_id
            owner = Owner.find owner_id
            owner.destroy
            puts "Deleting Owner with id #{id}"
          end
        rescue StandardError => exc
          puts 'Error occured during Owner Revert: ', exc
        end
      end
    end
  end
end