namespace :update_checks do
  desc 'update ACHS User id for decryption'
  task check_user_id: :environment do
    puts 'update ACHS User id for decryption'
    job = CronJob.new(name: 'ach_user_id', description: 'update ACHS User id for decryption', success: true)
    object = {updates: [], error: nil}
    begin
      achs = TangoOrder.includes(:user).instant_ach
      achs.each do |c|
        begin
          user_id = c.user_id
          decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
          data = JSON(decrypted)
        rescue Exception => exc
          if c.user.merchant_id.blank?
            user_ids = c.user.sub_merchants.pluck(:id)
            user_ids.each do |user_id|
              begin
                decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
                data = JSON(decrypted)
                hash= { user_id: user_id, check_id:c.id ,old_user_id: c.user_id , decrypter: "submerchant_id"}
                c.update(user_id:user_id)
                object[:updates] << hash
                break
              rescue Exception => e
              end
            end
          else
            user_id = c.user.merchant_id
            begin
              decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
              data = JSON(decrypted)
              hash = { user_id: user_id, check_id:c.id ,old_user_id: c.user_id , decrypter: "main_merchant"}
              c.update(user_id:user_id)
              object[:updates] << hash
            rescue Exception => e
            end
          end
        end
      end
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during ACHS Update: ', exc
    end
    job.result = object
    job.rake_task!
  end

  task revert_check_user_id: :environment do
    puts 'reverting update ACHS User id for decryption'
    jobs = CronJob.where(name: "ach_user_id", success: true)
    jobs.each do |job|
      begin
        if job.result["updates"].present? && job.result["error"].blank?
          checks = job.result["updates"]
          puts "reverting checks: #{checks}"
          checks.each do |c|
            ach = TangoOrder.find_by(id: c[:check_id])
            ach.update(user_id: c[:old_user_id]) if ach.present?
          end
          puts "Done"
        end
      rescue StandardError => exc
        job.success = false
        result[:error] = "Error: #{exc.message}"
        puts 'Error occured during ACH revert: ', exc
      end
      job.rake_task_revert!
    end
  end
end