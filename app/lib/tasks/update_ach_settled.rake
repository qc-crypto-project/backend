namespace :update_ach_settled do
  desc "update ACH settled to false status"
  task make_ach_settled_false: :environment do
    puts 'update ACH settled to false'
    job = CronJob.new(name: 'ach', description: 'update ACH settled to false', success: true)
    object = {updates: [], error: nil}
    begin
      checks = TangoOrder.ach.where(settled: :true)
      object[:updates] = checks.pluck(:id)
      if checks.present?
        puts "updating checks: #{object[:updates]}"
        checks.update_all(settled: :false)
        puts "Done"
      else
        puts "ALl Done"
      end
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during ACH Update: ', exc
    end
    job.result = object
    job.ach_settled!
  end

  task revert_make_ach_settled_false: :environment do
    puts 'reverting ACH settled to false'
    jobs = CronJob.ach_settled
    jobs.each do |job|
      puts "reverting job #{job.id}"
      begin
        if job.result["updates"].present? && job.result["error"].blank?
          checks = job.result["updates"]
          puts "reverting checks: #{checks}"
          checks = TangoOrder.where(:id => checks)
          checks.update_all(settled: :true)
          puts "Done"
        end
      rescue StandardError => exc
        job.success = false
        object[:error] = "Error: #{exc.message}"
        puts 'Error occured during ACH revert: ', exc
      end
      job.ach_settled_revert!
    end
  end
end
