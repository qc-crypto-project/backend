namespace :app_config do
  desc "Updating respond_with"
  task respond_with: :environment do
    puts "Starting....."
    begin
      limit = 5000
      offset = 1
      while offset > 0
        puts "processing with offset #{offset}"
        actvities = ActivityLog.where('id > ?', offset).order('id asc').limit(limit)
        if actvities.present?
          offset = actvities.last.id
        else
          offset = 0
        end

        activities_batch = actvities.each do|a|
          if a.respond_with.present? && a.respond_with.class == String && a.respond_with.first == "{"
            respond_with = JSON(a.respond_with)
            a.respond_with = respond_with
            if a.params.present? && a.params["card_cvv"].present?
              a.params.delete("card_cvv")
            end
          end
        end

        if activities_batch.present?
          puts "updating"
          ActivityLog.import activities_batch, on_duplicate_key_update: {conflict_target: [:id], columns: [:respond_with,:params]}
        else
          puts "Completed"
        end
      end
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
    end
    puts "Done...."
  end
end