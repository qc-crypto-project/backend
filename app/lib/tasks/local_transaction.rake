namespace :local_transaction do
    task update_transaction: :environment do
      begin
        puts "Local Transaction Update Started..."
        job = CronJob.new(name: 'local_transaction:update_transaction', description: 'update fields in transaction', success: true)
        result = {updates: [], error: nil}
        updates = []
        total_transactions = BlockTransaction.count
        updated = 0
        limit = 2000
        payment_gateways = PaymentGateway.all
        puts "Total #{total_transactions} transactions need to be updated."
        while (updated < total_transactions) do
          puts "Getting #{limit} block transactions with offset #{updated}"
          block_transaction = BlockTransaction.limit(limit).offset(updated)
          local_transactions = Transaction.where(seq_transaction_id: block_transaction.pluck(:sequence_id))
          updated_transactions = block_transaction.map do |block|
            transaction = local_transactions.detect{ |obj| obj.seq_transaction_id == block.sequence_id}
            if transaction.present?
              gateway = payment_gateways.detect{ |obj| obj.key == block.gateway}
              transaction.sender_wallet_id = block.sender_wallet_id
              transaction.receiver_wallet_id = block.receiver_wallet_id
              transaction.receiver_id = block.receiver_id
              transaction.sender_id = block.sender_id
              transaction.privacy_fee = block.privacy_fee
              transaction.reserve_money = SequenceLib.dollars(block.hold_money_cents)
              transaction.tip = SequenceLib.dollars(block.tip_cents)
              transaction.net_fee = block.net_fee
              transaction.net_amount = block.total_net
              transaction.total_amount = SequenceLib.dollars(block.amount_in_cents)
              transaction.amount = SequenceLib.dollars(block.tx_amount).to_s
              transaction.card_id = block.card_id
              transaction.charge_id = block.charge_id
              transaction.tags = block.tags
              transaction.ip = block.ip
              transaction.gbox_fee = block.gbox_fee
              transaction.iso_fee = block.iso_fee
              transaction.agent_fee = block.agent_fee
              transaction.affiliate_fee = block.affiliate_fee
              transaction.payment_gateway_id = gateway.try(:id)
              transaction.timestamp = block.timestamp
              transaction.last4 = block.last4
              transaction.main_type = block.main_type
              transaction.sub_type = block.sub_type
            end
            transaction
          end
          Transaction.import(
            updated_transactions.uniq.compact,
            on_duplicate_key_update: {
              conflict_target: [:id],
              columns: [
                :sender_wallet_id,
                :receiver_wallet_id,
                :receiver_id,
                :sender_id,
                :privacy_fee,
                :reserve_money,
                :tip,
                :net_fee,
                :net_amount,
                :total_amount,
                :amount,
                :card_id,
                :charge_id,
                :tags,
                :ip,
                :fee,
                :gbox_fee,
                :iso_fee,
                :agent_fee,
                :affiliate_fee,
                :payment_gateway_id,
                :last4,:main_type,
                :sub_type
              ]
            }
          )
          updated += limit
          puts "#{updated} transactions updated out of total #{total_transactions}..."
        end
      rescue StandardError => exc
        job.success = false
        result[:error] = "Error: #{exc.message}"
        puts 'Error occured during the task process: ', exc
      ensure
        puts "successfully updated"
        job.result = result if job.present?
        job.rake_task! if job.present?
      end
    end
end