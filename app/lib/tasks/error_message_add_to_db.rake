namespace :error_messges do
  task error_messages_add_to_db: :environment do
    puts "########### Process Start ###########"
    puts "########### Error Messages are saving in DB ###########"
    error_details = I18n.t("quickcard")
    error_details.keys.each do |error|
      error = error_details[error]
      if ErrorMessage.where(error_code: error[:error_code]).first.blank?
        p "Creating  #{ error[:error_code] } ..."
        ErrorMessage.create(error_code: error[:error_code], error_message: error[:error_message], error_reason: error[:error_reason])
      else
        p "Error Code  #{ error[:error_code] } is already exist in DB."
      end
    end
    p ".............Error Codes Successfully Ended..... "
  end
end