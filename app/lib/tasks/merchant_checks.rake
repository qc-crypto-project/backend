namespace :merchant_checks do
  desc "Merchant's admin users Checks"
  task admin_user: :environment do
    begin
      job = CronJob.new(name: 'sub_merchant_checks', description: 'sub merchant checks updation', success: true)
      result = {updates: [], error: nil}

      @admins_users = User.joins(:permission).where.not(merchant_id: nil).where(permission_type: "admin")
      @admins_users.each do |user|
        if user.tango_orders.present?
          main_merchant_id = user.merchant_id
          sub_merchants_checks = user.tango_orders.pluck(:id)
          user.tango_orders.update_all(user_id: main_merchant_id)
          result[:updates].push({sub_merchant_id: user.id, main_merchant_id: main_merchant_id, sub_merchants_checks_ids: sub_merchants_checks})
        end
      end
      puts "Checks records updated successfully!"
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during the task process: ', exc
    end
    job.result = result
    job.checks!
  end

  task revert_admin_user: :environment do
    desc "Revert Merchant's admin users Checks"
    begin
      puts "start reverting"
      job = CronJob.checks.where(success: true, name: 'sub_merchant_checks').last
      if job.present?
        if job.result.present?
          if job.result["updates"].present?
            job.result["updates"].each do |obj|
              obj["sub_merchants_checks_ids"].each do|check_id|
                check = TangoOrder.find_by(id: check_id)
                check.update(user_id: obj["sub_merchant_id"])
              end
            end
          end
        end
      end
      puts "reverted Successfully!"
    rescue StandardError => exc
      puts 'Error occured during the task process: ', exc
    end
  end
end
