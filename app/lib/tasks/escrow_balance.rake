namespace :escrow_balance do
  desc "Just For testing purpose"
  task get_balance: :environment do
    begin
      wallet_id = Wallet.escrow.first.id
      balance = SequenceLib.balance(wallet_id)
      SlackService.notify("Escrow balance is $#{balance} at #{Time.now.strftime("%a, %e %b %Y %H:%M:%S ")}")
    rescue => ex
      SlackService.notify("Error getting Escrow balance : #{ex.message.to_s}")
    end
  end
end
