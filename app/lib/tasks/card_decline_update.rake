namespace :card_decline do
  task update_card_decline: :environment do
    begin
      cards = Card.where("decline_attempts > ?", 0)
      job = CronJob.new(name: "card_decline:update_card_decline", description: 'reset card declines', success: true)
      result = {updated_cards: [], not_updated_cards: [], error: nil}
      cards.each do |card|
        puts "started updating cards"
        decline_time = card.last_decline
        card_settings = {card_id: card.id, old_declines: [], last_decline_time: []}
        if decline_time.present? && ((DateTime.now.utc - decline_time) / 1.hours).round >= 12
          card_settings[:old_declines] << card.decline_attempts
          card_settings[:last_decline_time] << card.last_decline
          card.update(decline_attempts: 0, last_decline: nil)
          result[:updated_cards] << card_settings
        else
          card_settings[:old_declines] << card.decline_attempts
          card_settings[:last_decline_time] << card.last_decline
          result[:not_updated_cards] << card_settings
        end
      end
      puts "cards update successfull"
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during Gateway Update: ', exc
    end
    job.result = result
    job.rake_task!
  end
end