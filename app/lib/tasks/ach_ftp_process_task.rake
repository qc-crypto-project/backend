namespace :ftp_ach_process do
  desc "Process ftp type achs"
  task process_ftp_ach: :environment do
    include ApplicationHelper
    puts 'Processing ftp type achs'
    begin
      id = nil
      job = CronJob.new(name: 'ftp_ach_process:process_ftp_ach', description: 'Process ftp type ach', success: true)
      result = {updates: [],  error: nil}
      tangos = TangoOrder.joins(:ach_gateway).where("tango_orders.status = ? and ach_gateways.ach_type = ?", "PAID", "ftp")
      escrow_wallet = Wallet.check_escrow.first
      escrow_user = escrow_wallet.try(:users).try(:first)
      tangos.each_slice(1000) do |batch_tangos|
        batch_tangos.each do |check|
          id = check.id
          check_info = {
              name: check.name,
              check_email: check.recipient,
              check_id: check.checkId,
              check_number: check.number,
          }
          newstatus="PAID"
          if check.amount_escrow.present?
            check_escrow_transactions(check,nil,check_info,newstatus, escrow_wallet, escrow_user, true)
            check.amount_escrow = false
          end
            result[:updates].push(id)
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during ftp ach processing: ', exc
    end
    job.result = result
    job.ach_ftp_process!
    puts 'Successfully processed ftp ach!'
  end
  # --------------Ftp ach Process Task Revert--------------------
  task revert_process_ftp_ach: :environment do
    puts 'Reverting Processed ftp achs '
    job = CronJob.ach_ftp_process.where(success: true, name: 'ftp_ach_process:process_ftp_ach').last
    if job["error"].nil? && job.result["updates"].present?
      begin
        job.result["updates"].each do |check_id|
          updated_ach = TangoOrder.find(check_id)
          if updated_ach.present?
            
          end
        end
      rescue StandardError => exc
        puts 'Error occured during ftp achs task revert: ', exc
      end
    end
  end

  task void_ach_type: :environment do
    puts 'Updating VOID ACH type'
    begin
    job = CronJob.new(name: 'void_ach_type:update', description: 'Void ach transaction type update', success: true)
    result = {updates: [],  error: nil}
    void_achs = []
    Transaction.where(main_type: "Void_Ach").each do |t|
      t.main_type = "Void ACH"
      void_achs.push(t)
      result[:updates].push(t.id)
    end
    Transaction.import void_achs, on_duplicate_key_update: {conflict_target: [:id], columns: [:main_type]}
    rescue StandardError => exc
      job.success = false
      puts 'Error occured during processing: ', exc
    end
    job.result = result
    job.ach_settled!
    puts 'Successfully updated void ach type'
  end
end