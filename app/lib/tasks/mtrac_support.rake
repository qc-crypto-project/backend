namespace :mtrac_support do
  desc "Create mtrac support user"
  task create: :environment do
    begin
      mtrac_user = User.where(email: "support@mtrac.com").support_mtrac
      if mtrac_user.blank?
        puts "creating...."
        @user = User.new(name: "Mtrac Support",first_name: "Mtrac",last_name: "Support",email: "support@mtrac.com",phone_number: "923211221211",role: "support_mtrac",password: "MTracs000",is_block: false)
        @user.regenerate_token
        if @user.save
          puts 'Created Successfully'
        else
          puts 'Something wrong'
        end
      else
        puts 'Already exist'
      end
    rescue StandardError => exc
      puts 'Error: ', exc
    end
  end

end