namespace :location_slot do
  desc "TODO"

  task create_slot: :environment do
    puts "creating location's slots"
    begin
    id = nil
    job = CronJob.new(name: "location_slot:create_slot", description: 'creating slots for location', success: true)
    result = {updates: [], error: nil}
    Location.all.each do|l|
      unless l.slots.present?
        puts "updating Location => #{l.id}"
        l.slots.create(slot_name: "slot_1",slot_rule: "100")
        l.slots.create(slot_name: "slot_2",slot_rule: "60")
        l.slots.create(slot_name: "slot_3",slot_rule: "40")
        result[:updates].push({:id=> l.id})
      end
    end
    puts "created successfully"
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Location slots: ', exc
    end
    job.result = result
    job.location!
  end


  task revert_create_slot: :environment do
    puts 'Location Slots'
    jobs = CronJob.location.where(success: true, name: 'location_slot:create_slot').last
    result = jobs.result
      if result["error"].nil?
        puts "reverting"
        begin
          result["updates"].each do |l|
            if l["id"].present?
             loca = Location.find_by(id: l["id"])
              if loca.present?
                loca.slots.each do |s|
                  s.delete
                end
              end
            end
          end
          puts "Reverted"
        rescue StandardError => exc
          puts 'Error occured during Revert slots: ', exc
        end
      else
        puts "revert not found"
      end
  end
end
