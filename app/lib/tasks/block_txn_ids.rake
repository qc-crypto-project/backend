namespace :block_txn_ids do
  desc "update balance of wallets"
  task block_txn_add_location_id: :environment do
    puts "start adding location_id"
    wallet_ids = BlockTransaction.pluck(:sender_wallet_id, :receiver_wallet_id).uniq.flatten.uniq.compact
    wallet_ids.each_slice(1000) do |batch_ids|
      wallets = Wallet.where(id: batch_ids).joins(:location).where("locations.id is not null AND locations.category_id is not null").select("wallets.id AS id, locations.id AS location_id, locations.category_id AS category_id")
      wallets.find_each do |wallet|
        BlockTransaction.where("sender_wallet_id = :id OR receiver_wallet_id = :id", id: wallet.id).update_all(location_id: wallet.location_id, category_id: wallet.category_id)
      end
    end
    puts "Updated."
  end

  task block_txn_add_payment_gateway_id: :environment do
    puts "started adding payment_gateway_id"
    payment_gateways = BlockTransaction.select("tags -> 'source' as gateway").pluck(:gateway).uniq.compact
    payment_gateways = payment_gateways.reject(&:empty?)
    payment_gateways.each do |gateway|
        payment_gateway_id = PaymentGateway.where("key = :key OR name = :key", key: gateway).pluck(:id).first
        BlockTransaction.where("tags ->> 'source' = ? ", gateway).update_all(payment_gateway_id: payment_gateway_id)
    end
    puts "Updated."
  end

  task update_block: :environment do
    puts "started updating block transactions"
    BlockTransaction.where(main_type: ["Credit Card","Service fee","CBK Fee","Subscription fee","eCommerce","Misc fee","ACH Deposit","Instant Pay","Account Transfer","Void Ach","PIN Debit","B2B Transfer","Giftcard Purchase","Send Check","Sale","Void Push To Card","Void Check","Refund","Failed ach","Refund Bank","Void ach","Virtual Terminal"]).find_in_batches(batch_size: 50000) do |block|
      if block.present?
        update = block.each do |txn|
          iso_id = Wallet.where(id: txn.tags.try(:[],"fee_perc").try(:[], "iso").try(:[], "wallet")).last.try(:users).try(:first).try(:id) if txn.tags.try(:[],"fee_perc").try(:[], "iso").try(:[], "wallet").present?
          agent_id = Wallet.where(id: txn.tags.try(:[],"fee_perc").try(:[], "agent").try(:[], "wallet")).last.try(:users).try(:first).try(:id) if txn.tags.try(:[],"fee_perc").try(:[], "agent").try(:[], "wallet").present?
          affiliate_id = Wallet.where(id: txn.tags.try(:[],"fee_perc").try(:[], "affiliate").try(:[], "wallet")).last.try(:users).try(:first).try(:id) if txn.tags.try(:[],"fee_perc").try(:[], "affiliate").try(:[], "wallet").present?
          child_transactions = txn.child_transactions
          if child_transactions.present?
            child_transactions.update_all(iso_id: iso_id, affiliate_id: affiliate_id, agent_id: agent_id)
          end
          txn.agent_id = agent_id
          txn.affiliate_id = affiliate_id
          txn.iso_id = iso_id
          txn
        end
        BlockTransaction.import update, on_duplicate_key_update: {conflict_target: [:sequence_id], columns: [:iso_id, :agent_id, :affiliate_id, :updated_at]}
      end
    end
  end
end