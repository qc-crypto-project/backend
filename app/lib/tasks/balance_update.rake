namespace :balance_update do
  desc "update balance of wallets"
  task update_balance: :environment do
    puts "start updating balance"
    wallets = Wallet.all
    wallets.each do |wallet|
      puts "wallet---> #{wallet.id}"
      puts "old balance----> #{wallet.balance}"
      balance = SequenceLib.balance(wallet.id)
      puts "sequence balalnce----> #{balance}"
      wallet.update(balance: balance)
      puts "new_balance -----> #{wallet.balance}"
    end
  end

  desc "update dispensary wallet balance"
  task dispensary_wallet: :environment do
    puts "start........"
    begin
      wallets = []
      Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").each do|wallet|
        puts "wallet_id: => #{wallet.id}"
        wallet.balance = SequenceLib.balance(wallet.id)
        wallets.push(wallet)
        sleep(5)
      end
      puts "updating........"
      Wallet.import wallets, on_duplicate_key_update: {conflict_target: [:id], columns: [:balance]}
      puts "updated........"
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
    end
  end

  task wallet_csv: :environment do
    wallets = Wallet.first(100)
    file = File.new("#{Rails.root}/tmp/wallet_info.csv", "w")
    file << CSV.generate do |csv|
      csv << ["wallet id", "balance"]
      wallets.each do |wallet|
        csv << [wallet.id, wallet.balance]
      end
    end
    file.close
    SlackService.upload_files("dispensary info", file, "#quickcard-internal", "dispensary_info")
  end
end