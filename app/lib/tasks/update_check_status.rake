namespace :update_check_status do
  desc "update check status"
  task check_status: :environment do
    include ApplicationHelper
    puts 'Tango order update check status'
    begin
      job = CronJob.new(name: 'checks & ach', description: 'update checks status from checkbook', success: true)
      object = {updates: [], error: nil}
      @checks = TangoOrder.where(order_type: ["check","ach"], settled: false)
      if @checks.present?
        @checks.each do |check|
          if check.ach?
            result = false
            # result = get_ach(check.checkId)
          else
            result = get_cheq(check.checkId)
          end
          if result != false
            if result["status"] != check.status
              # if result["status"] != "PAID"
              old = check.status
              validation(check,result)
              object[:updates].push({id: check.id, old_value: old, new_value: check.status})
              # end
            end
          end
        end
        puts 'Updated Successfully'
      end
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during checks Update: ', exc
    end
    job.result = object
    job.checks!
  end


  def validation(check, result)
    check_status = result["status"]
    app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
    wallet = Wallet.where(id: check.wallet_id).first if check.wallet_id.present?
    if wallet.present?
      check_info = {
          name: check.name,
          check_email: check.recipient,
          check_id: check.checkId,
          check_number: check.number,
          checkbook_status: check_status,
          check_local_status: check.status
      }
      if check.status == "UNPAID"
        if check_status == "PAID"
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "FAILED"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,app_config.stringValue, 0,nil, check_info)
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{check.wallet_id}.\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "VOID"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,app_config.stringValue, 0,nil, check_info)
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{check.wallet_id}.\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "IN_PROCESS"
          check.update(status: result["status"], settled: false)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == 'UNPAID'
          check.update(status: result["status"], settled: false)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "PRINTED"
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        end
      elsif check.status == "IN_PROCESS" || check.status == "captured"
        if check_status == "PAID"
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "FAILED"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,app_config.stringValue, 0,nil, check_info)
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{check.wallet_id}.\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "VOID"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,app_config.stringValue, 0,nil, check_info)
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{check.wallet_id}.\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "IN_PROCESS"
          check.update(status: result["status"], settled: false)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == 'UNPAID'
          check.update(status: result["status"], settled: false)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        elsif check_status == "PRINTED"
          check.update(status: result["status"], settled: true)
          msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
          SlackService.notify(msg) unless request.host == 'localhost'
        end
      elsif check_status == "PRINTED"
        check.update(status: result["status"], settled: true)
        msg = "*Check Status Update.*\ntype: \`\`\`CHECK\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\`"
        SlackService.notify(msg) unless request.host == 'localhost'
      end
      # if check_status == "RETURNED"
      #   issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
      # elsif check_status == "FAILED"
      #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
      # elsif check_status == "VOID"
      #   # if check.status != 'VOID'
      #   issue_checkbook_amount(check.amount, wallet,'send_check',app_config.stringValue, 0, nil, check_info)
      #   # end
      # elsif check_status == "EXPIRED"
      #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0, nil, check_info)
      # else
      #   puts "no if"
      # end
    end
  end

  def get_cheq(cheque)
    begin
      url =URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{cheque}")
      http = Net::HTTP.new(url.host,url.port)
      http.use_ssl =true
      http.verify_mode =OpenSSL::SSL::VERIFY_PEER
      request= Net::HTTP::Get.new(url)
      request["Authorization"]=ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
      request["Content-Type"]='application/json'
      response =http.request(request)
      case response
      when Net::HTTPSuccess
        cheque = JSON.parse(response.body)
        return cheque
      when Net::HTTPUnauthorized
        return false
      when Net::HTTPNotFound
        return false
      when Net::HTTPServerError
        return false
      else
        return false
      end
    rescue => ex
      p"-----------EXCEPTION HANDLED #{ex.message}"
    end
  end

    # def update_cron_job(check, check_status)
    #   check_info = {
    #       name: check.name,
    #       check_email: check.recipient,
    #       check_db_id: check.id,
    #       check_id: check.checkId,
    #       check_number: check.number,
    #       check_status: check.status,
    #       checkbook_status: check_status
    #   }
    #   cron_job = CronJob.create(name: "check", description: check_info.to_json)
    #   cron_job.checks!
    # end

  task enc_dec: :environment do
    puts 'Updating Encryption Decryption'
    begin
      id = nil
      job = CronJob.new(name: 'update_check_status:enc_dec', description: 'Update/Fix wrong Account Token for Checks because', success: true)
      result = {updates: [], error: nil}
      charges = []
      TangoOrder.check.each do|c|
        id = c.id
        if c.account_token.present?
          dec_key = "#{c.user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}}"
          begin
            decrypted = AESCrypt.decrypt(dec_key,  c.account_token)
          rescue StandardError => exc
            puts "Decrypting new records..."
            dec_key = "#{c.user_id}-#{ENV['ACCOUNT_ENCRYPTOR']}-#{c.wallet_id}"
            decrypted = AESCrypt.decrypt(dec_key,  c.account_token)
          end
          data = JSON(decrypted)
          token =  {
              :account_number => data["account_number"],
              :account_type => data["account_type"]
          }
          acc_token = token.to_json
          enc_key = "#{c.user_id}-#{ENV['ACCOUNT_ENCRYPTOR']}-#{c.wallet_id}"
          encrypted = AESCrypt.encrypt(enc_key,  acc_token)
          result[:updates].push({id: c.id, old_value: c.account_token, new_value: encrypted})
          c.update(account_token: encrypted)
          puts "updated"
        else
          puts "Nothing to Change"
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured ', exc
    end
    job.result = result
    job.rake_task!
    puts 'Checks Account Token Updated Successfully!'
  end

end