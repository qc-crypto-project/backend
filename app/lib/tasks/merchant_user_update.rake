namespace :merchant_user do
  desc "Update Merchant User For Permission"
  task merchant_user_update: :environment do
    puts 'Updating Merchant User'
    begin
      id = nil
      job = CronJob.new(name: 'merchant_user:merchant_user_update', description: 'Update Merchant User', success: true)
      result = { updates: [],  error: nil }
      @user = User.merchant.where.not(:merchant_id => nil)
      @user.each do |user|
        id = user.id
        if user.present?
          if user.submerchant_type == "admin_user"
            user.update( permission_id: Permission.where(permission_type: "admin").pluck(:id).first )
          elsif user.submerchant_type == "regular_user"
            user.update( permission_id: Permission.where(permission_type: "regular").pluck(:id).first )
          end
          result[:updates].push(id)
          puts "Updated Successfully"
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Merchant User Update: ', exc
    end
    job.result = result
    job.rake_task!
    puts 'Merchant User Update Successfully!'
  end
  # --------------Batch Date Update Task Revert--------------------
  task revert_merchant_user_update: :environment do
    puts 'Reverting Updated Merchant User Update'
    jobs = CronJob.rake_task.where(success: true, name: 'merchant_user:merchant_user_update')
    jobs.each do |job|
      if job["error"].nil? && job.result["updates"].present?
        begin
          job.result["updates"].each do |user_record|
            user_update = User.merchant.find_by_id(user_record)
            id = user_record
            if user_update.present?
              if user_update.permission_id.present?
                user_update.update(permission_id: nil)
                puts "Reverting Merchant User For #{id}"
              end
            end
          end
        rescue StandardError => exc
          puts 'Error occured during Merchant User Update: ', exc
        end
      end
    end
  end

  task merchant_user_archive: :environment do
    begin
      User.where.not(merchant_id: nil).update_all(archived: true, permission_id: nil)
    rescue => exc
      exc.message
    end
  end
end
