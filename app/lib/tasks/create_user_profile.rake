namespace :create_users_profiles do
  desc "Create Profiles for Users"
  task create_profiles_for_users: :environment do
    puts "Start Creating Profiles"
    begin
      job = CronJob.new(name: 'create_users_profiles:create_profiles_for_users', description: 'Creating Profiles for Users', success: true)
      object = {updates: [], error: nil}
      User.where(role: [7,9,10]).each_slice(100) do |users|
        create_profiles=[]
        users.each do |user|
          if user.profile.nil?
            create_profiles << Profile.new(user_id: user.id, company_name: user.company_name)
            object[:updates] << user.id
          end
        end
        Profile.import create_profiles
      end
      puts 'Profiles Created Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured while creating profiles: ', exc
    end
    job.result = object
    job.create_user_profile!
  end
end