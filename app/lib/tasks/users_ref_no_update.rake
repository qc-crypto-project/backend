namespace :users_ref_no_update do
  desc "update ref_no for users having role user"
  task update_users_ref_no: :environment do
    puts 'Updating Users ref_no'
    begin
      id = nil
      job = CronJob.new(name: 'users_ref_no_update:update_users_ref_no', description: 'Update User ref_no', success: true)
      result = {updates: [],  error: nil}
      users = User.where(ref_no: nil)
      users.each do |user|
        id = user.id
        if user.iso? #here we display the suggestions in a specific format
          ref_no= "ISO-#{user.id}"
        elsif user.agent?
          ref_no= "A-#{user.id}"
        elsif user.affiliate?
          ref_no= "AF-#{user.id}"
        elsif user.merchant?
          ref_no= "M-#{user.id}"
        elsif user.user?
          ref_no="C-#{user.id}"
        elsif user.partner?
          ref_no="P-#{user.id}"
        elsif user.support?
          ref_no="S-#{user.id}"
        end
        if ref_no.present?
          user.update(ref_no: ref_no)
          result[:updates].push(id)
        end
    end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during User ref_no Update: ', exc
    end
    job.result = result
    job.users_ref_no!
    puts 'Users ref_no Updated Successfully!'
  end
  # --------------User Reference Number Update Task Revert--------------------
  task revert_update_users_ref_no: :environment do
    puts 'Reverting Updated Users ref_no '
    job = CronJob.users_ref_no.where(success: true, name: 'users_ref_no_update:update_users_ref_no').last
    if job["error"].nil? && job.result["updates"].present?
      begin
        job.result["updates"].each do |user_id|
          updated_user = User.find(user_id)
          if updated_user.present?

          end
        end
      rescue StandardError => exc
        puts 'Error occured during User ref_no Update: ', exc
      end
    end
  end
end