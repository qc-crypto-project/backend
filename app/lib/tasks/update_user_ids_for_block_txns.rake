namespace :block_transactions do
  desc "update user_ids for block transactions"
  task update_user_ids_for_block_txns: :environment do
    wallet_ids = BlockTransaction.pluck(:sender_wallet_id, :receiver_wallet_id).uniq.flatten.uniq.compact
    wallet_ids.each_slice(1000) do |batch_ids|
      wallets = Wallet.where(id:batch_ids).joins(:users).where("users.merchant_id is null").select("wallets.id AS id, users.id AS user_id, users.name AS user_name")
      wallets.each do |wallet|
        BlockTransaction.where(sender_wallet_id: wallet.id).update_all(sender_id: wallet.user_id, sender_name: wallet.user_name)
        BlockTransaction.where(receiver_wallet_id: wallet.id).update_all(receiver_id: wallet.user_id, receiver_name: wallet.user_name)
      end
    end
  end

  task update_block_fee: :environment do
    #to update fee in block transaction which was saved 0
    include Admins::BlockTransactionsHelper
    BlockTransaction.where("fee_in_cents = ?", 0).where.not(main_type: ["Fee Transfer", "Service fee","Subscription fee","Misc fee"]).find_in_batches(batch_size: 10000).each do |transactions|
      if transactions.present?
        transactions.each do |transaction|
          if transaction.tags["fee"].present? && transaction.tags["fee"].to_f > 0
            transaction.fee_in_cents = get_block_fee(transaction.tags)
          end
          transaction
        end
        BlockTransaction.import transactions, on_duplicate_key_update: {
            conflict_target: [:sequence_id],
            columns: [:fee_in_cents]
        }
      end
    end
  end

  task sender_recevier_transactions: :environment do
    wallet_ids = Transaction.pluck(:sender_wallet_id, :receiver_wallet_id).uniq.flatten.uniq.compact
    wallet_ids.each_slice(1000) do |batch_ids|
      wallets = Wallet.where(id: batch_ids).joins(:users).where("users.merchant_id is null").select("wallets.id AS id, users.id AS user_id, users.name AS user_name")
      wallets.each do |wallet|
        Transaction.where(sender_wallet_id: wallet.id).update_all(sender_name: wallet.user_name)
        Transaction.where(receiver_wallet_id: wallet.id).update_all(receiver_name: wallet.user_name)
      end
    end
  end
end