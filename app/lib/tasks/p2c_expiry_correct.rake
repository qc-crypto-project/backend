namespace :expiry_correct do
  desc "Correcting wrongly saved expiry dates for Push2Cards"
  task correct_expiry: :environment do
    # include ApplicationHelper

    puts "Getting TangoOrders for all P2Cs"
    job = CronJob.new(name: 'expiry_correct:correct_expiry', description: 'Correcting wrongly saved dates under encrypted card info under P2C TangoOrders', success: true)
    result = {updates: [],  error: []}

    current_check = nil

    begin
      checks = TangoOrder.instant_pay.where.not(account_token: nil, status: ['VOID', 'PAID'])
      checks.find_each do |check|
        if check.account_token.present?
          current_check = check.id
          decrypted = AESCrypt.decrypt("#{check.user_id}-#{ENV['CARD_ENCRYPTER']}-#{check.wallet_id}}",  check.account_token)
          decrypted = JSON.parse(decrypted)
          exp = decrypted["expiry_date"]
          card_num = decrypted["card_number"]
          exp_y = exp.split('-').first
          exp_m = exp.split('-').last
          if exp_y.size > 4
            exp_yyyy = exp_y.last(4)
            exp_date = "#{exp_yyyy}-#{exp_m}"
            account_token_info =  {
                :card_number => card_num,
                :expiry_date => exp_date
            }

            account_token_info = account_token_info.to_json
            encrypted_card_info = AESCrypt.encrypt("#{check.user_id}-#{ENV['CARD_ENCRYPTER']}-#{check.wallet_id}}", account_token_info)
            check.account_token = encrypted_card_info

            if check.save
              puts "Expiry of the encrypted info related to TangoOrder id: #{check.id} successfully updated from #{exp} to #{exp_date}"
              result[:updates].push({tangoorder_id: check.id})
            else
              puts 'Unable to update the Incorrect Expiry!!'
              result[:error].push({tangoorder_id: check.id})

            end

          end
          job.rake_task!
          puts 'Job Created Successfully!'
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}, for TangoOrder with id: #{current_check}"
      puts 'Error occured during expiry date update: ', exc
    end
    job.result = result
    job.rake_task!
  end
end