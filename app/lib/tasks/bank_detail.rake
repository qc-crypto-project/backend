namespace :bank_detail do
  desc "create bank detail for locations"
  task create: :environment do
    begin
      puts "creating bank details"
      locations = Location.all
      create_bank_details = []
      locations.each do |l|
        puts "creating location_id: #{l.id}"
        create_bank_details.push(BankDetail.new( bank_name: l.bank_account_name, account_no: l.bank_account, routing_no: l.bank_routing, location_id: l.id, bank_account_type: l.bank_account_type))
      end
      BankDetail.import create_bank_details
      puts "bank detail created"

      puts "-----------------------------------"

      puts "assigning images"
      assiging_images = []
      locations.each do |l|
        image = Image.find_by(location_id: l.id)
        bank_id = l.bank_details.try(:first).try(:id)
        if image.present?
          image.bank_detail_id = bank_id
          assiging_images.push(image)
        end
      end
      Image.import assiging_images, on_duplicate_key_update: {conflict_target: [:id], columns: [:bank_detail_id]}
      puts "assigned images successfully!"
    rescue StandardError => exc
      puts "Error: #{exc.message}"
    end
  end

end

