namespace :batchdate do
  desc "Update Batch Date of checks Transaction"
  task checks_update: :environment do
    puts 'Updating Batch Date'
    begin
      id = nil
      job = CronJob.new(name: 'batchdate:checks_update', description: 'Update Batch Date', success: true)
      result = {updates: [],  error: nil}
      @checks=TangoOrder.where(batch_date: nil)
      @checks.each do |check|
        id=check.id
        if check.present?
          check.update(batch_date: (check.created_at + 1.day))
          result[:updates].push(id)
          puts "Updated Successfully #{check.id}"
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Batch Date Update: ', exc
    end
    job.result = result
    job.batchdate_task!
    puts 'Batch Date Successfully!'
  end
  # --------------Batch Date Update Task Revert--------------------
  task revert_checks_update: :environment do
    puts 'Reverting Updated Batch Date Update'
    jobs = CronJob.batchdate_task.where(success: true, name: 'batchdate:checks_update')
    jobs.each do |job|
      if job["error"].nil? && job.result["updates"].present?
        begin
          job.result["updates"].each do |check_record|
            check_update=TangoOrder.find(check_record)
            id=check_record
            if check_update.present?
              if check_update.batch_date.blank?
                check_update.update(batch_date: nil)
                puts "Reverting Ach Transaction For #{id}"
              end
            end
          end
        rescue StandardError => exc
          puts 'Error occured during Batch Date Update: ', exc
        end
      end
    end
  end
end