namespace :decline_update do
  desc "TODO"
  task transaction_info: :environment do
    puts 'Checking Transaction Info'
    orders=OrderBank.where.not(transaction_info: nil)
    orders.each do |order|
      if order.transaction_info.class.to_s=="Hash"
        puts "Updating Transaction Info Value of #{order.id}"
        transaction_info_json=order.transaction_info.to_json
        order.update(transaction_info: transaction_info_json)
      end
    end
    puts 'Updated Successfully'
  end
end
