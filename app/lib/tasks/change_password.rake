namespace :change_password do
  desc "Changing password"
  task checking_for_password: :environment do
    all_user = User.merchant + User.iso + User.agent + User.affiliate
    all_user.each do |u|
      last_password_updated = (Date.today.to_date - u.last_password_update.to_date).to_i
      if (140..170).include?(last_password_updated) #after 5 month
        # send Email to change password
        u.send_notification_to_password_change
      elsif (171..205).include?(last_password_updated)
        # update with rand password
        @random_code = rand(1_000000000..9_999999999) # after 6 month
        u.update(password: @random_code,last_password_update: Time.zone.now)
      end
    end
  end

end
