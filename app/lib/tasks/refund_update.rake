namespace :refund_update do
  desc "TODO"
  # --------------Refund Fee Task--------------------
  task change_refund_fee: :environment do
    puts 'Checking Refund Fee Info'
    begin
    id = nil
    job = CronJob.new(name: "refund_update:change_refund_fee", description: 'update refund fee', success: true)
    result = {updated_fees: [], not_updated_fees: [], error: nil}
    fees=Fee.buy_rate
    fees.each do |fee|
      id = fee.id
      buyrate_fees = {fee_id: id, old_refund_fee: [], new_refund_fee: []}
      unless fee.refund_fee == 0.0
          old_refund_fee=fee.refund_fee
          puts "Updating Fee For #{id}"
          if fee.update(refund_fee: 0.0)
            puts 'Updated Successfully'
              buyrate_fees[:old_refund_fee] << old_refund_fee
              buyrate_fees[:new_refund_fee] << fee.refund_fee
            result[:updated_fees].push(buyrate_fees)
          else
            puts 'Not Updated Successfully'
            buyrate_fees[:old_refund_fee] << old_refund_fee
            result[:not_updated_fees].push(buyrate_fees)
          end
      else
          buyrate_fees[:old_refund_fee] << fee.refund_fee
          result[:not_updated_fees].push(buyrate_fees)
      end
    end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during Refund Fee Update: ', exc
    end
    job.result = result
    job.refund_update!
  end
  # --------------Refund Fee Task Revert--------------------
  task revert_refund_fee: :environment do
    puts 'Reverting Refund Fee Info'
    jobs = CronJob.refund_update
    jobs.each do |job|
      if job["error"].nil?
        begin
          result = {updated_fees: [], not_updated_fees: [], error: nil}
          job.result["updated_fees"].each do |fee_record|
            fee=Fee.find_by_id(fee_record["fee_id"])
            id=fee.id
            if fee.present?
              buyrate_fees = {fee_id: id, old_refund_fee: [], new_refund_fee: []}
              old_refund_fee=fee.refund_fee
              new_refund_fee=fee_record["old_refund_fee"][0].to_f
                puts "Reverting Fee For #{id}"
                if fee.update(refund_fee: new_refund_fee)
                  puts 'Reverted Successfully'
                  buyrate_fees[:old_refund_fee] << old_refund_fee
                  buyrate_fees[:new_refund_fee] << fee.refund_fee
                  result[:updated_fees].push(buyrate_fees)
                end
            end
          end
        rescue StandardError => exc
          job.success = false
          result[:error] = "Error: #{exc.message} for #{id}"
          puts 'Error occured during Refund Fee Update: ', exc
        end
        result[:not_updated_fees]=job.result["not_updated_fees"]
        job.result = result
        job.refund_revert!
      end
    end
  end
end
