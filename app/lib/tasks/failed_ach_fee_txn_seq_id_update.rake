namespace :failed_ach_fee_txn do
  desc "update Failed ACh Txn seq_id"
  task seq_id_update: :environment do
    puts "Start updating txns"
    begin
      job = CronJob.new(name: 'failed_ach_fee_txn:seq_id_update', description: 'Updating failed ach fee txn seq id', success: true)
      object = {updates: [], error: nil}
      Transaction.where(main_type:TypesEnumLib::TransactionType::FailedAchFee).each_slice(1000) do |batch|
        txn_update=[]
        batch.each do |txn|
          puts "starting"
          main_txn = Transaction.where(seq_transaction_id: txn.seq_transaction_id, main_type:"Failed ACH")
          if main_txn.count > 0
            updated_seq_id = BlockTransaction.where(seq_parent_id: txn.seq_transaction_id, tx_type: TypesEnumLib::TransactionType::FailedAchFee).last.try(:sequence_id)
            if updated_seq_id.present?
              void_txn_id = main_txn.first.try(:id)
              old_seq_id = txn.seq_transaction_id
              new_seq_id = updated_seq_id
              txn.seq_transaction_id = updated_seq_id
              txn[:tags]["void_txn_id"] = void_txn_id if txn[:tags].present?
              txn_update.push(txn)
              object[:updates].push({id:txn.id,new_value: new_seq_id,old_value:old_seq_id,void_txn_id:void_txn_id})
            end
          end
        end
        Transaction.import txn_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:seq_transaction_id, :tags]}
      end
      puts 'Updated Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during Txn seq_id update: ', exc
    end
    job.result = object
    job.failed_ach_txn_seq_id!
  end

  # --------------Txn Seq_id Update Task Revert--------------------
  task revert_seq_id_update: :environment do
    puts "Start reverting task"
    job = CronJob.find 225
    if job.result["updates"].present?
      begin
        updating_txns = []
        job.result["updates"].each do |txn_record|
          txn_id = txn_record["id"]
          txn = Transaction.find_by(id: txn_id)
          if txn.present?
            txn.tags = txn.tags.reject{|v| v["void_txn_id"]} if txn.tags.present?
            txn.seq_transaction_id = txn_record["old_value"]
            updating_txns.push(txn)
          end
        end
        Transaction.import updating_txns, on_duplicate_key_update: {conflict_target: [:id], columns: [:seq_transaction_id, :tags]}
      rescue StandardError => exc
        puts 'Error occured during Txn seq_id update: ', exc
      end
    end
    puts 'Reverted Successfully'
  end

end