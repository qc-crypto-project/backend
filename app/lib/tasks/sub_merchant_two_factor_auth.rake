namespace :sub_merchant_two_factor_auth do
  task sub_merchant_auth: :environment do
    puts "Start updating sub merchants"
    job = CronJob.new(name: 'sub_merchant_two_factor_auth:sub_merchant_auth', description: 'Sub merchant two factor auth update', success: true)
    object = {updates: [],  error: nil}
    User.merchant.where.not(merchant_id: nil).each_slice(100) do |batch|
      updated_users = []
      batch.each do |user|
        if user.two_step_verification
          user.two_step_verification = false
          updated_users.push(user)
          object[:updates].push({id:user.id})
        end
      end
      User.import updated_users, on_duplicate_key_update: {conflict_target: [:id], columns: [:two_step_verification]}
      puts "Updated Successfully"
    end
    job.result = object
    job.sub_merchant_auth!
  end
end