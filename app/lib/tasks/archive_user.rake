namespace :archive_user do
  desc "update user archive status"
  task blocked_user: :environment do
    puts "Start updating user"
    begin
      job = CronJob.new(name: 'UserArchive', description: 'changing users email if they are archived', success: true)
      object = {updates: [], error: nil}
      users = User.all
      users.each do |user|
        if user.archived == true
          if user.blocked_email.blank?
            a = {}
            a = {id: user.id, old_value: user.email}
            user.update(blocked_email: user.email,email: TypesEnumLib::Users::BlockedEmail)
            object[:updates].push(a.merge({new_value: user.email}))
          end
        end
      end
      puts 'Updated Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during user email Update: ', exc
    end
    job.result = object
    job.user_archive!
  end
end