namespace :vt_country_code do
  desc "Update country code of customers with wrong country code"
  task vt_country_code_for_customers: :environment do
    puts 'Updating customer country code'
    begin
      id = nil
      job = CronJob.new(name: 'vt_country_code:vt_country_code_for_customers', description: 'Update country code of customers with wrong country code', success: true)
      result = {updates: [],  error: nil}
      User.user.find_in_batches(batch_size: 5000) do |customer_batch|
        updating_customers = []
        customer_batch.each do |customer|
          id = customer.id
          if customer.country.present?
            country_code = ISO3166::Country.new(customer.country)
            if country_code.nil?
              customer.country = nil
              result[:updates].push(id)
              updating_customers.push(customer)
            end
          end
        end
        User.import updating_customers, on_duplicate_key_update: {conflict_target: [:id], columns: [:country]}
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message} for #{id}"
      puts 'Error occured during customer country code updation: ', exc
    end
    job.result = result
    job.save!
    puts 'Successfully updated country code for customers!'
  end
  # --------------Country code for customers Task Revert--------------------
  task revert_vt_country_code_for_customers: :environment do
    puts 'Reverting vt country code for customers'
    job = CronJob.where(success: true, name: 'vt_country_code:vt_country_code_for_customers').last
    if job["error"].nil? && job.result["updates"].present?
      begin
        job.result["updates"].each do |customer_id|
          updated_customer = User.find(customer_id)
          if updated_customer.present?
            
          end
        end
      rescue StandardError => exc
        puts 'Error occured during customer country code updation task revert: ', exc
      end
    end
  end
end
