namespace :ach_fee_update do
  desc "update Ach Fee"
  task ach_fee: :environment do
    include ApplicationHelper
    puts "Start updating ACHs"
    begin
      job = CronJob.new(name: 'AchFee', description: 'Updating Merchant Fee perc column', success: true)
      object = {updates: [], error: nil}
      TangoOrder.instant_ach.joins(:user).where('role = ?',4).each_slice(1000) do |batch|
        ach_update=[]
        batch.each do |ach|
          if ach.fee_perc.to_f > 0
            old_fee = ach.fee_perc
            ach.fee_perc=deduct_fee(ach.try(:fee_perc).to_f, ach.actual_amount)
            ach_update.push(ach)
            object[:updates].push({id:ach.id,new_value: ach.fee_perc,old_value:old_fee,amount:ach.actual_amount})
          end
        end
        TangoOrder.import ach_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:fee_perc]}
      end
      puts 'Updated Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during ACH Update: ', exc
    end
    upload_to_slack("",object[:updates]) if object[:updates].present?
    job.result = object
    job.rake_task!
  end


  def upload_to_slack(name,files)
    file = File.new("#{Rails.root}/tmp/#{name}.csv", "w")
    file << CSV.generate do |csv|
      csv << ["ID","Old","New","Amount"]
      files.each do |file|
        csv << [file[:id], file[:old_value] , file[:new_value], file[:amount]]
      end
    end
    file.close
    SlackService.upload_files(name, file, "#qc-files", "#{name} Updated ACH Fee")
  end
end