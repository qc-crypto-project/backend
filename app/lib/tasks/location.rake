namespace :location do
  desc "removing baked_iso_asso_from_location"
  task removing_association: :environment do
    begin
      job = CronJob.new(name: 'locationusers', description: 'updating locations users', success: true)
      object = {updates: [], error: nil}
      puts "start updating"
      Location.all.each do |l|
        puts "updating Location Id:", l.id
        if l.baked_iso || l.profit_split
          if l.profit_split_detail.present?
            @profit_detail = JSON.parse(l.profit_split_detail)
            if l.users.present?
              old = l.users.pluck(:id)
              old.each do |u|
                # if u.iso?
                if @profit_detail.present?
                  if @profit_detail["splits"].present?
                    if @profit_detail["splits"]["super_iso"].present?
                      if @profit_detail["splits"]["super_iso"]["wallet"].present?
                        if u == @profit_detail["splits"]["super_iso"]["wallet"].to_i
                          l.users.delete(User.find_by(id: u))
                        end
                      end
                    end
                  end
                end
                # end
                # if u.agent?
                if @profit_detail.present?
                  if @profit_detail["splits"].present?
                    if @profit_detail["splits"]["super_agent"].present?
                      if @profit_detail["splits"]["super_agent"]["wallet"].present?
                        if u == @profit_detail["splits"]["super_agent"]["wallet"].to_i
                          l.users.delete(User.find_by(id: u))
                        end
                      end
                    end
                  end
                end
                # end
                # if u.affiliate?
                if @profit_detail.present?
                  if @profit_detail["splits"].present?
                    if @profit_detail["splits"]["super_affiliate"].present?
                      if @profit_detail["splits"]["super_affiliate"]["wallet"].present?
                        if u == @profit_detail["splits"]["super_affiliate"]["wallet"].to_i
                          l.users.delete(User.find_by(id: u))
                        end
                      end
                    end
                  end
                end
                # end
                if l.profit_split
                  if @profit_detail.present?
                    if @profit_detail["splits"].keys[3].present? && @profit_detail["splits"].keys[3].include?("merchant")
                      if @profit_detail["splits"].values[3]["wallet"].present?
                        location = Location.find_by_id(@profit_detail["splits"].values[3]["wallet"].to_i)
                        merchant = location.merchant if location.present?
                        if merchant.present?
                          if l.users.include?(merchant)
                            l.users.delete(merchant)
                          end
                        end
                      end
                    else
                      if @profit_detail["splits"].keys[3].present?
                        if u == @profit_detail["splits"].values[3]["wallet"].to_i
                          # if l.users.include?(u)
                          l.users.delete(User.find_by(id: u))
                          # end
                        end
                      end
                    end
                  end
                end
                new = l.users.pluck(:id)
                object[:updates].push({id: l.id, old_value: old, new_value: new})
              end
            end
          end
        end
      end
      puts "updated Successfully!"
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during location user Update: ', exc
    end
    job.result = object
    job.locationusers!
  end

  task removing_association_revert: :environment do
    puts "start reverting"
    begin
      location_users = CronJob.locationusers.where(success: true).last
      if location_users.present?
        if location_users.result.present?
          if location_users.result["updates"].present?
            location_users.result["updates"].each do |c|
              location = Location.find_by(id: c["id"])
              if location.present?
                if location.users.present?
                  if c["new_value"].present?
                    if location.try(:users).present?
                      location.users.delete(location.users)
                    end
                    if location.try(:users).blank? && c["old_value"].present?
                      users = User.where(id: c["old_value"])
                      location.users << users
                    end
                  end
                end
              end
            end
          end
        end
      end
      puts "updated Successfully!"
    rescue StandardError => exc
      puts 'Error occured during reverting location user Update: ', exc
    end
  end

  task updating_profit_split_detail: :environment do
    begin
      puts "start updating"
      job = CronJob.new(name: 'location', description: 'updating profit split detail in locations', success: true)
      object = {updates: [], error: nil}
      Location.all.each do |l|
        puts "location to be edit", l.id
        split = {}
        old = l.profit_split_detail
        @profit_detail = JSON.parse(l.profit_split_detail) if l.profit_split_detail.present?
        if l.profit_split
          puts "into profit split"
          # if l.profit_split_detail.present?
            if @profit_detail.present?
              @profit_detail["splits"].except("ez_merchant", "net").each do |key,value|
                key_name = key.tr("0-9", "")
                unless key_name.blank?
                  if key_name == "company"
                    role = "Companies"
                  elsif key_name == "agent"
                    role = "Agents"
                  elsif key_name == "affiliate"
                    role = "Affiliates"
                  elsif key_name == "iso"
                    role = "Isos"
                  elsif key_name == "merchant"
                    role = "Merchants"
                  end
                  from = value["from"]
                  split = {
                      "profit_split" => {"from" => from,"0" => {"role" => role,"name"=>value["name"], "wallet" => value["wallet"], "percent"=> value["percent"], "dollar"=> "0"}}
                  }
                end
              end
            end
          profit_detail = {}
          @profit_detail["splits"].each do |key, value|
            key_name = key.tr("0-9", "")
            if key_name.blank? || ( key_name.present? && key_name != "company" && key_name != "agent" && key_name != "iso" && key_name != "merchant" && key_name != "affiliate")
              profit_detail = profit_detail.merge(key => value)
            end
          end
          profit_detail = profit_detail.merge(split)
          @profit_detail["splits"] = profit_detail
          puts "before update"
          puts l.profit_split_detail
          l.update_attribute :profit_split_detail, @profit_detail.to_json
          puts "after update"
          puts l.profit_split_detail
          object[:updates].push({id: l.id, old_value: old, new_value: l.profit_split_detail})
        end
      end
      puts "updated Successfully!"
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during location profit split details Update: ', exc
    end
    job.result = object
    job.location!
  end

  task updating_profit_split_detail_revert: :environment do
    begin
      puts "start reverting"
      location_users = CronJob.location.where(success: true).last
      if location_users.present?
        if location_users.result.present?
          if location_users.result["updates"].present?
            location_users.result["updates"].each do |c|
              location = Location.find_by(id: c["id"])
              if location.present?
                if c["old_value"].present?
                  location.update(profit_split_detail: c["old_value"])
                end
              end
            end
          end
        end
      end
      puts "reverted Successfully!"
    rescue StandardError => exc
      puts 'Error occured during location profit split details revert: ', exc
    end
  end

  task updating_isos_name: :environment do
    job = CronJob.new(name: 'usersfee', description: 'updating super iso fees split', success: true)
    object = {updates: [], error: nil}
    begin
      puts "start updating"
      users = User.iso
      users.each do |u|
        if u.fees.present?
          u.fees.each do |f|
            if f.splits.present?
              old = f.splits
              splits = JSON(f.splits)
              if splits["splits"].present?
                if splits["splits"]["super_iso"].present?
                  if splits["splits"]["super_iso"]["user"].present?
                    name = User.find_by(id: splits["splits"]["super_iso"]["user"].to_i).try(:name)
                    splits["splits"]["super_iso"]["name"] = name
                  end
                end
              end
              f.update(splits: splits.to_json)
              object[:updates].push({id: u.id, old_value: old, new_value: f.splits})
            end
          end
        end
      end
      puts "updated Successfully!"
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during ISO fee Update: ', exc
    end
    job.result = object
    job.usersfee!
  end


  task iso_updating_profit_split_detail: :environment do
    begin
      puts "start updating"
      job = CronJob.new(name: 'bakedISO', description: 'updating baked iso data in iso_profile ', success: true)
      object = {updates: [], error: nil}
      User.iso.each do |u|
        if u.fees.present?
          puts "User with id ==( #{u.id} )== going to update"
          u.fees.each do |f|
            if f.splits.present?
              data = JSON(f.splits)
              user_1 = {}
              temp = {"name"=>"","user"=>"","percent"=>"0","dollar"=>"0"}
              if data.present?
                old = f.splits
                if data["splits"].present?
                  if data["splits"]["super_iso"].present?
                    user_1 = data["splits"]["super_iso"]
                    split = {"0"=> {"user_1"=>user_1,"user_2"=>temp,"user_3"=>temp}}
                    data["splits"] = split
                    f.update(is_baked_iso: true, splits: data.to_json)
                    object[:updates].push({id: f.id, old_value: old, new_value: f.splits})
                  end
                end
              end
            end
          end
        end
      end
      puts "updated Successfully!"
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during iso fees Update: ', exc
    end
    job.result = object
    job.usersfee!
  end

  task iso_updating_profit_split_detail_revert: :environment do
    begin
      puts "start reverting"
      baked_users = CronJob.usersfee.where(success: true).last
      if baked_users.present?
        if baked_users.result.present?
          if baked_users.result["updates"].present?
            baked_users.result["updates"].each do |b|
              fee = Fee.find_by(id: b["id"])
              if fee.present?
                if b["old_value"].present?
                  fee.update(splits: b["old_value"], is_baked_iso: false)
                end
              end
            end
          end
        end
      end
      puts "reverted Successfully!"
    rescue StandardError => exc
      puts 'Error occured during ISO baked users profit split details revert: ', exc
    end
  end

  task ach: :environment do
    puts "Starting....."
    locations = Location.all
    locations = locations.each do|l|
      l.bank_account = nil
      l.bank_routing = nil
      l.bank_account_type = nil
    end
    Location.import locations, on_duplicate_key_update: {conflict_target: [:id], columns: [:bank_account,:bank_routing, :bank_account_type]}
    puts "Done...."
  end
end