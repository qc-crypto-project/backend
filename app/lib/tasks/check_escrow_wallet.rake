namespace :check_escrow_wallet do
  desc "Creating new Check escrow wallet"
  task check_escrow: :environment do
    puts 'Creating...'
    begin
      job = CronJob.new(name: 'check_escrow', description: 'Creating new Check escrow wallet', success: true)
      object = {updates: [], error: nil}
      @wallet = Wallet.check_escrow
      unless @wallet.present?
        @user = User.new(name: "Check Escrow",first_name: "Check",last_name: "Escrow",email: "check_escrow@quickcard.me",phone_number: "923211221212",role: "support",password: "#{rand(1..10)}#{Devise.friendly_token.first(8)}")
        if @user.save
          puts 'Created Successfully'
        else
          puts 'Something wrong'
        end
      else
        puts 'Already added'
      end
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during check_escrow creating: ', exc
    end
    job.result = object
    job.checks!
  end

  task change_wallet_name: :environment do
    check_escrow = Wallet.check_escrow.where(name: "Check Escrow")
    if check_escrow.present?
      puts "updating check_escrow"
      user = check_escrow.first.users.first
      check_escrow.first.update(name: "Withdrawal Escrow")
      user.update(name: "Withdrawal Escrow")
      puts "updated check_escrow"
    end
    cbk_escrow = Wallet.charge_back.where(name: "CBK Wallet")
    if cbk_escrow.present?
      puts "updating cbk wallet"
      cbk_user = cbk_escrow.first.users.first
      cbk_escrow.first.update(name: "CBK Escrow")
      cbk_user.update(name: "CBK Escrow")
      puts "updated cbk wallet"
    end
    escrow = Wallet.escrow.where(name: "Escrow Wallet")
    if escrow.present?
      puts "updating escrow wallet"
      escrow_user = escrow.first.users.first
      escrow.first.update(name: "QC Escrow")
      escrow_user.update(name: "QC Escrow")
      puts "updated escrow wallet"
    end
  end


end