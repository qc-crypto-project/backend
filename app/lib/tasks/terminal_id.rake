namespace :terminal_id do
  desc "fetch terminal id from tags and add in column Transaction"
  task add_terminal_id: :environment do
    puts "Starting....."
    begin
      Transaction.where(action: "issue",main_type: ["Credit Card","PIN Debit"],from: ["Credit",nil]).find_in_batches(batch_size: 10000).with_index do |txs,index|
        puts "========>>>> index : #{index}"
        updating_transaction(txs)
      end
    rescue StandardError => exc
      puts 'Error occured during Update: ', exc
      puts "Error ID: #{@tx_id}"
    end
    puts "Done...."
  end

  def updating_transaction(txs)
    if txs.present?
      updating_tx = txs.each do |tx|
        @tx_id = tx.id
        if tx.tags.present?
          if tx.tags.try(:[],"sales_info").present?
            if tx.tags.try(:[],"sales_info").try(:[],"terminal_id").present?
              tx.terminal_id = tx.tags.try(:[],"sales_info").try(:[],"terminal_id")
            else
              if tx.activity_logs.present?
                if tx.activity_logs.try(:first).try(:params).try(:[],"terminal_id").present?
                  tx.terminal_id = tx.activity_logs.try(:first).try(:params).try(:[],"terminal_id")
                end
              end
            end
          else
            if tx.activity_logs.present?
              if tx.activity_logs.try(:first).try(:params).try(:[],"terminal_id").present?
                tx.terminal_id = tx.activity_logs.try(:first).try(:params).try(:[],"terminal_id")
              end
            end
          end
        end
      end
      Transaction.import updating_tx, on_duplicate_key_update: {conflict_target: [:id], columns: [:terminal_id]}
    end
  end
end