namespace :bulk_checks_instance_status do
  desc "update bulk checks status"
  task bulk_check_status: :environment do
    puts 'update bulk check instance status'
    BulkCheckInstance.all.each do |b|
      b.update(start_process: true)
    end
    puts 'Updated Successfully'
  end

end