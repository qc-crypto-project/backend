namespace :paymentcard do
  desc "Cards updated for payment gateways"

  task card_update: :environment do
    job = CronJob.new(name: 'cardupdate', description: 'updating payment gateway cards mastercard to mastercard_credit', success: true)
    object = {updates: [], error: nil}
    begin
      puts "start updating"
      gateways = PaymentGateway.all
      gateways.each do |g|
        if g.card_selection.present?
          unless g.card_selection["mastercard_credit"].present?
            old = g.card_selection.clone
            g.card_selection["mastercard_credit"] = g.card_selection["mastercard"] || "off"
            g.card_selection.delete("mastercard")
            new_cards = g.card_selection
            g.update(card_selection: new_cards)
            object[:updates].push({id: g.id, old_value: old, new_value: new_cards})
          end
        end
      end
      puts "updated Successfully!"
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during card updation: ', exc
    end
    job.result = object
    job.cardupdate!
  end

  task card_update_revert: :environment do
    begin
      puts "start reverting"
      gateways = CronJob.cardupdate.where(success: true).last
      if gateways.present?
        if gateways.result.present?
          if gateways.result["updates"].present?
            gateways.result["updates"].each do |g|
              pg = PaymentGateway.find_by(id: g["id"])
              if pg.present?
                if g["old_value"].present?
                  pg.update(card_selection: g["old_value"])
                end
              end
            end
          end
        end
      end
      puts "reverted Successfully!"
    rescue StandardError => exc
      puts 'Error occured during payment gateway card revert: ', exc
    end
  end
end