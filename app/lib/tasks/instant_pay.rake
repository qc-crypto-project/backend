namespace :instant_pay do
  desc "Create app config to enable/disable APIs from admin pannel"
  task create: :environment do
    instant_daily_limit = AppConfig.where(key: "instant_daily_limit").first
    instant_per_tx_limit = AppConfig.where(key: "instant_per_tx_limit").first
    if instant_daily_limit.nil?
      AppConfig.create(key: AppConfig::Key::InstantDailyLimit, user_fee: 50000) unless @apis_config.present?
      puts "Daily Limit Created Successfully"
    else
      puts "Daily Limit Already created"
    end
    if instant_per_tx_limit.nil?
      AppConfig.create(key: AppConfig::Key::InstantPerTxLimit, user_fee: 10000) unless @apis_config.present?
      puts "Per tx Limit Created Successfully"
    else
      puts "Per tx Limit Already created"
    end
  end
end