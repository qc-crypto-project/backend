namespace :order_bank_fix_card_types do
  desc "Order Bank (Decline/Approve) Updates"
  # namespace :fix_card_types do
    task up: :environment do
      include ApplicationHelper
      puts 'FIX CARD DETAILS FOR ORDER_BANKS'
      begin
        id = nil
        batch_size = 5
        job = CronJob.new(name: 'order_bank:fix_card_types:up', description: 'Update OrderBank Card Details', success: true)
        result = {updates: [], error: nil}
        batch_count = 0
        objects = OrderBank.where(created_at: (Time.now - 5.days).beginning_of_day..(Time.now - 1.days).end_of_day).first(13)
        objects.each do |obj|
          id = obj.id
          if obj.transaction_info.present? && valid_json?(obj.transaction_info.to_s)
            puts 'Processing Object with id: ', id
            info = JSON(obj.transaction_info.clone)
            if info["first6"].present? && info["card_details"].blank?
              card_detail = get_card_info(info["first6"])
              batch_count += 1
              puts 'Got Card Binlist onfo: ', card_detail
              if card_detail.present?
                old = {card_type: obj.card_type, card_sub_type: obj.card_sub_type, transaction_info: obj.transaction_info}
                p "OrderBank Object Old Details: #{{card_type: obj.card_type, card_sub_type: obj.card_sub_type}}"
                info['card_details'] = card_detail
                obj.card_type = (card_detail['scheme'].try(:downcase) || nil)
                obj.card_sub_type = (card_detail['type'].try(:downcase) || nil)
                obj.transaction_info = info.to_json
                if obj.save
                  puts "OrderBank with id #{id} updated"
                  updated = {card_type: obj.card_type, card_sub_type: obj.card_sub_type, transaction_info: obj.transaction_info}
                  result[:updates].push({id: obj.id, old_value: old, new_value: updated})
                  p "OrderBank Object Updates Added: #{{card_type: obj.card_type, card_sub_type: obj.card_sub_type}}"
                end
              end
            end
          end
          if batch_count >= batch_size
            puts 'SLEEPING FOR 1 minute'
            sleep 60
            batch_count = 0
          end
        end
        puts "#{objects.count} OrderBank records updated successfully!"
      rescue StandardError => exc
        job.success = false
        result[:error] = "Error: #{exc.message} for #{id}"
        puts 'Error occured during the task process: ', exc
      end
      job.result = result
      job.rake_task!
    end

    task revert: :environment do
      include ApplicationHelper
      puts 'REVERT fix_card_types task'
      begin
        puts "start reverting"
        job = CronJob.rake_task.where(success: true, name: 'order_bank:fix_card_types:up').last
        if job.present?
          if job.result.present?
            if job.result["updates"].present?
              job.result["updates"].each do |obj|
                puts obj
                order_bank = OrderBank.find(obj["id"])
                if order_bank.present? && obj["old_value"].present?
                  order_bank.update(obj["old_value"])
                end
              end
            end
          end
        end
        puts "reverted Successfully!"
      rescue StandardError => exc
        puts 'Error occured during the task process: ', exc
      end
    end
  # end
end
