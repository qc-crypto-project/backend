namespace :create_baked_user do
  desc "Creating new Baked users"
  task create_baked: :environment do
    puts 'Creating...'
    begin
      job = CronJob.new(name: 'create_baked', description: 'Creating baked users for Location users', success: true)
        LocationsUsers.where(relation_type:'primary').find_in_batches(batch_size: 10000) do |location_batch|
          create_baked_users = []
          if location_batch.present?
            location_batch.each do |batch|
                  create_baked_users.push(BakedUser.new(
                      user_id: batch.try(:user).try(:id),
                      location_id: batch.try(:location_id),
                      wallet_id: batch.try(:user).try(:wallets).try(:primary).try(:first).try(:id),
                      name: batch.try(:user).try(:name),
                      active: true,
                      split_type: 'locations_users',
                      position: '',
                      gbox: false,
                      iso_on_file: false,
                      attach: '',
                      dollar: '',
                      percent: '',
                      user_role: batch.try(:user).try(:role)
                  ))
            end
            BakedUser.import create_baked_users
          end
        end
    rescue StandardError => exc
      job.success = false
      puts 'Error occured during baked user creation: ', exc
    end
    job.rake_task!
  end



end