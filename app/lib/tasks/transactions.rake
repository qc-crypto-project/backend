namespace :transactions do
  desc "Updating transaction information"

  task update_issue: :environment do
    puts 'Updating Transactions...'
    begin
      limit = 2000
      offset = 1
      while offset > 0
        puts "processing #{limit} transactions with offset #{offset}"
        issue_transactions = Transaction.where(action: "issue",status: "approved",receiver_id: nil).where('id > ?', offset).order('id asc').limit(limit)
        if issue_transactions.present?
          offset = issue_transactions.last.id
        else
          offset = 0
        end

        wallets_hash = {}
        wallets = issue_transactions.pluck(:to,:from).flatten.uniq.compact
        wallets.each do |id|
          id = id.tr("a-zA-Z",'')
          if id.present?
            w = Wallet.find_by(id: id)
            if w.present?
              wallets_hash["#{w.id}"] = w
            end
          end
        end

        issue_transactions = issue_transactions.each do |t|
          if t.from.present? && t.from != "Credit"
            wallet = wallets_hash["#{t.from}"]
            user = wallet.try(:users).try(:first)
            if wallet.present? && user.present?
              if t.tags.present? && t.tags.try(:[],"card").present?  && t.tags.try(:[],"card").try(:[],"last4").present?  && t.tags.try(:[],"card").try(:[],"first6").present?
                t.receiver_id = user.id
                t.receiver_wallet_id = wallet.id
                t.last4 = t.tags.try(:[],"card").try(:[],"last4")
                t.first6 = t.tags.try(:[],"card").try(:[],"first6")
                if t.card_id.blank?
                  t.card_id = t.tags.try(:[],"card").try(:[],"id")
                end
              else
                t.receiver_id = user.id
                t.receiver_wallet_id = wallet.id
              end
            end
          elsif t.to.present? && t.from == "Credit"
            wallet = wallets_hash["#{t.to}"]
            user = wallet.try(:users).try(:first)
            if wallet.present? && user.present?
              if t.tags.present? && t.tags.try(:[],"card").present?  && t.tags.try(:[],"card").try(:[],"last4").present?  && t.tags.try(:[],"card").try(:[],"first6").present?
                t.receiver_id = user.id
                t.receiver_wallet_id = wallet.id
                t.last4 = t.tags.try(:[],"card").try(:[],"last4")
                t.first6 = t.tags.try(:[],"card").try(:[],"first6")
                if t.card_id.blank?
                  t.card_id = t.tags.try(:[],"card").try(:[],"id")
                end
              else
                t.receiver_id = user.id
                t.receiver_wallet_id = wallet.id
              end
            end
          end
        end
        if issue_transactions.present?
          puts "importing transactions..."
          Transaction.import issue_transactions, on_duplicate_key_update: {conflict_target: [:id], columns: [:receiver_id,:receiver_wallet_id,:last4,:first6]}
          puts "imported #{offset} transactions"
          puts "<<<<<<<<<==========>>>>>>>>>>>"
        else
          puts "Completed"
        end
      end
    rescue StandardError => exc
      puts 'Error occured during Transactions Update: ', exc
    end
    puts 'Transactions updated Successfully!'
  end

  task update_transfer: :environment do
    puts 'Updating Transactions...'
    begin
      limit = 2000
      offset = 1
      while offset > 0
        puts "processing #{limit} transactions with offset #{offset}"
        transfer_transactions = Transaction.where(action: "transfer",main_type: ["eCommerce","Virtual Terminal"],last4: nil).or(Transaction.where(action: "transfer",main_type: ["eCommerce","Virtual Terminal"],first6: nil)).where('id > ?', offset).order('id asc').limit(limit).uniq
        if transfer_transactions.present?
          offset = transfer_transactions.last.id
        else
          offset = 0
        end

        transfer_transactions = transfer_transactions.each do |t|
          if t.tags.try(:[],"previous_issue").try(:[],"tags").try(:[],"card").present?
            last4 = t.tags["previous_issue"]["tags"]["card"].try(:[],"last4").present? ? t.tags["previous_issue"]["tags"]["card"].try(:[],"last4") : nil
            first6 = t.tags["previous_issue"]["tags"]["card"].try(:[],"first6").present? ? t.tags["previous_issue"]["tags"]["card"].try(:[],"first6") : nil
            card_id = t.tags["previous_issue"]["tags"]["card"].try(:[],"id").present? ? t.tags["previous_issue"]["tags"]["card"].try(:[],"id") : nil
            if last4.present? || first6.present? || card_id.present?
              t.last4 = last4
              t.card_id = card_id
              t.first6 = first6
            end
          end
        end
        if transfer_transactions.present?
          puts "updating Transfer type transactions"
          Transaction.import transfer_transactions, on_duplicate_key_update: {conflict_target: [:id], columns: [:last4,:first6]}
          puts "imported #{offset} transactions"
          puts "<<<<<<<<<==========>>>>>>>>>>>"
        else
          puts "Completed"
        end
      end
    rescue StandardError => exc
      puts 'Error occured during Transactions Update: ', exc
    end
    puts 'Transactions updated Successfully!'
  end

  task update_refund: :environment do
    transactions = Transaction.where(main_type: ["refund"])
    transactions.each do |tx|
      location = tx.sender_wallet.try(:location)
      location_data = {id: location.id,name: location.first_name, email: location.email, business_name: location.business_name} if location.present?
      merchant = tx.sender
      merchant_data = {id: merchant.id,name: merchant.name ,email: merchant.email, phone_number: merchant.phone_number, owner_name: merchant.owner_name} if merchant.present? && merchant.merchant?
      tx.tags = tx.tags.merge({"location"=>location_data}) if location_data.present?
      tx.tags = tx.tags.merge({"merchant"=>merchant_data}) if merchant_data.present?
      if tx.fee.present? && tx.fee.to_f > 0
        tx.total_amount = tx.amount.to_f + tx.fee.to_f
      end
      tx.save
    end
  end

  task update_tx: :environment do
    limit = 10000
    offset = 1
    time = Time.new(2019,05,01,0,0,0)
    update_time = Time.now
    while offset > 0
      tx = BlockTransaction.where("timestamp <= ? AND updated_at < ?", time, update_time).where(main_type: ["Fee Transfer", "Fee"]).order(timestamp: :asc).limit(limit).offset(offset)
      group = tx.group_by{|e| e.seq_parent_id}.uniq
      if tx.present?
        offset = tx.last.id
        group.each do |key, value|
          seq_tx = SequenceLib.get_transaction_id(key)
          if seq_tx.present?
            BlockTransaction.where(seq_parent_id: key).update_all(seq_parent_id: seq_tx.first.actions.first.id, updated_at: Time.now)
          end
        end
      else
        offset = 0
      end
    end
  end

  task update_block_info: :environment do
    limit = 10000
    offset = 1
    while offset > 0
      tx = BlockTransaction.where("id > ?", offset).where(main_type: ["Fee Transfer", "Fee"]).where("(tags->'main_transaction_info') is null").select(:id, :tags, :seq_parent_id, :sequence_id, :updated_at).order(timestamp: :desc).limit(limit).offset(offset)
      group = tx.group_by{|e| e.seq_parent_id}.uniq
      if tx.present?
        offset = tx.last.id
        group.each do |key, value|
          block = BlockTransaction.where(sequence_id: key).first
          if block.present?
            main_info = {"amount" => SequenceLib.dollars(block.amount_in_cents),"transaction_type"=> block.main_type, "sub_type"=> block.sub_type}
            updates = value.map do |obj|
              if obj.tags.try(:[], "main_transaction_info").blank?
                obj.tags = obj.tags.merge({"main_transaction_info" => main_info})
              end
              obj
            end
            BlockTransaction.import updates, on_duplicate_key_update: {conflict_target: [:sequence_id], columns: [:tags,:sequence_id,:updated_at]}
          end
        end
      else
        offset = 0
      end
    end
  end

  task update_withdraws: :environment do
  #   task for updating amounts for withdraws and refund
    BlockTransaction.where(main_type: ["Refund", "Instant Pay", "ACH Deposit", "Send Check", "Giftcard Purchase"]).where.not(fee_in_cents: 0).find_in_batches(batch_size: 10000) do |block|
      if block.present?
        updated_block = block.each do |transaction|
          transaction.total_net = SequenceLib.dollars(transaction.tx_amount)
          transaction.amount_in_cents = transaction.amount_in_cents + transaction.fee_in_cents
          transaction
        end
        BlockTransaction.import updated_block, on_duplicate_key_update: {conflict_target: [:sequence_id], columns: [:amount_in_cents,:total_net,:updated_at]}
      end
    end
  end

  task update_pending_tx: :environment do
    begin
      job = CronJob.new(name: 'pending_txs', description: 'adding created at in pending timestamp', success: true)
      object = {updates: [], error: nil}
      Transaction.where(status: "pending", timestamp: nil).select("id as id","created_at as created_at","slug as slug","action as action", "timestamp as timestamp").find_in_batches(batch_size: 10000) do |batch|
        updates = []
        if batch.present?
          batch.each do |tx|
            object[:updates].push({id: tx.id})
            tx.timestamp = tx.created_at
            updates << tx
          end
          Transaction.import updates, on_duplicate_key_update: {conflict_target: [:id], columns: [:timestamp,:updated_at]}
        end
      end
    rescue => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during process: ', exc
    end
    job.result = object
    job.rake_task!
  end

  task update_pending_tx_revert: :environment do
    begin
      job = CronJob.new(name: 'pending_txs_revert', description: 'adding created at in pending timestamp revert', success: true)
      object = {updates: [], error: nil}
      updates = []
      Transaction.where(status: "pending").select("id as id","created_at as created_at","slug as slug", "timestamp as timestamp").find_in_batches(batch_size: 10000) do |batch|
        if batch.present?
          batch.each do |tx|
            object[:updates].push({id: tx.id})
            tx.timestamp = nil
            updates << tx
          end
          Transaction.import updates, on_duplicate_key_update: {conflict_target: [:id], columns: [:timestamp,:updated_at]}
        end
      end
    rescue => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during refund: ', exc
    end
    job.result = object
    job.rake_task!
  end

end