namespace :dispute_case do
  task update_gateway: :environment do
    begin
      cron_job = CronJob.create(name: 'dispute_case:update_gateway', description: 'update gateway in disputes', success: true)
      result = {updates: [], error: nil}
      updates = []
      disputes = DisputeCase.where(payment_gateway_id: nil)
      if disputes.present?
        disputes.each do |dispute|
          if dispute.transaction_id.present?
            transaction = Transaction.where(id: dispute.transaction_id).first
            if transaction.present?
              if transaction.payment_gateway_id.present?
                old_value = dispute.payment_gateway_id
                dispute.payment_gateway_id = transaction.payment_gateway_id
                updates << dispute
                result[:updates].push({id: dispute.id, old_value: old_value, new_value: dispute.payment_gateway_id})
              end
            end
          end
        end
        slice = updates.each_slice(10000).to_a
        slice.each do |t|
          DisputeCase.import t, on_duplicate_key_update: {conflict_target: [:id], columns: [:payment_gateway_id]}
        end
      end
    rescue StandardError => exc
      cron_job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during the task process: ', exc
    ensure
      cron_job.result = result
      cron_job.rake_task!
    end
  end

  task update_cbks: :environment do
    #task to turn pending cbks to lost
    include ActionView::Helpers::NumberHelper
    include Admins::BlockTransactionsHelper
    begin
      description = []
      time = Time.new(2019,06,30,23,59,59)
      cron_job = CronJob.create(name: "cbk_to_lost")
      cbks = DisputeCase.sent_pending.where('due_date >= ?',time)
      if cbks.present?
        cbks.each do |dispute_case|
          merchant = dispute_case.merchant_wallet.location.merchant
          from_id = Wallet.charge_back.last.id
          dispute_case_body = {
              case_number: dispute_case.case_number,
              amount: dispute_case.amount,
              merchant_wallet_id:dispute_case.merchant_wallet_id,
              last4: dispute_case.card_number.present? ? dispute_case.card_number : "NA"
          }
          charge_back_amount = dispute_case.amount.to_f
          if charge_back_amount > 0
            transaction = Transaction.new(to: nil, from: from_id, main_type: "cbk_lost", status: "CBK Lost", charge_id: nil, amount: charge_back_amount, total_amount: charge_back_amount, net_amount: charge_back_amount, sender: Wallet.charge_back.try(:last).try(:users).try(:first),receiver: nil, action: 'retire')
            begin
              cbk_lost = SequenceLib.retire(number_with_precision(charge_back_amount,precision:2),from_id,"charge_back",0,"cbk_lost",TypesEnumLib::GatewayType::Quickcard,nil, merchant.id,nil,nil,nil,nil,nil,nil,nil,"cbk_lost",nil,nil,nil,nil,dispute_case_body)
              if cbk_lost.present?
                dispute_case.status = :lost
                dispute_case.save(validate: false)
                transaction.update(tags: cbk_lost.actions.first.tags, seq_transaction_id: cbk_lost.actions.first.id, timestamp: cbk_lost.timestamp)
                parsed_transactions = parse_block_transactions(cbk_lost.actions, cbk_lost.timestamp)
                save_block_trans(parsed_transactions) if parsed_transactions.present?
                transaction.save
              end
              description = description.push({transaction_id: transaction.id, cbk_id: dispute_case.id, amount: dispute_case.amount})
            rescue => exc
              description = description.push({cbk_id: dispute_case.id,  amount: dispute_case.amount, error: exc.message})
            end
          end
        end
        cron_job.description = description
      end
      cron_job.rake_task!
    rescue => exc
      cron_job.description = {error: exc.message, backtrace: exc.backtrace.first(8)}
      cron_job.rake_task!
    end
  end

  task update_dates: :environment do
    begin
      cron = CronJob.create(name: "dispute_date")
      updates = []
      result = {updates: [], error: nil}
      DisputeCase.find_in_batches(batch_size: 1000) do |disputes|
        if disputes.present?
          disputes.each do |dispute|
            if dispute.present? && dispute.recieved_date.present?
              year = "#{dispute.recieved_date.year}"
              if year.size == 6
                recieved_date = dispute.recieved_date
                year = year.chars.last(4).join("").to_i
                new_date = Date.new(year, recieved_date.month, recieved_date.day)
                dispute.recieved_date = new_date
                updates << dispute
                result[:updates].push({id: dispute.id, old_date: recieved_date, new_date: dispute.recieved_date})
              end
            end
          end
          if updates.present?
            DisputeCase.import updates, on_duplicate_key_update: {conflict_target: [:id], columns: [:recieved_date]}
          end
        end
      end
      cron.description = result
      cron.rake_task!
    rescue => exc
      cron.description = result[:error].push({message: exc.message})
      cron.rake_task!
    end
  end

  task void_ach: :environment do
    #task for void-issue ach of dispensary
    include ApplicationHelper
    include Admins::BlockTransactionsHelper
    begin
      description = []
      cron_job = CronJob.create(name: "dispensary void ach")
      wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
      ach = TangoOrder.where(wallet_id: wallets, order_type: "instant_ach", status: "PENDING")
      if ach.present?
        ach.each do |ach|
          check_info = {
              name: ach.name,
              check_email: ach.recipient,
              check_id: ach.checkId,
              check_number: ach.number,
              notes: "Dispensary pending ach to void"
          }
          wallet = Wallet.find(ach.wallet_id)
          location = wallet.location
          if location.present?
            location_data = location_to_parse(location)
            merchant = merchant_to_parse(location.merchant)
          end
          escrow_wallet = Wallet.check_escrow.first
          escrow_user = escrow_wallet.try(:users).try(:first)
          tags = {
              "location" => location_data,
              "merchant" => merchant,
              "send_check_user_info" => check_info,
          }
          ach_check_type=updated_type(TypesEnumLib::TransactionType::VoidAch)
          check_total_amount = ach.amount.to_f
          user= wallet.users.where(merchant_id:nil).first
          fee_perc_details = {}
          if ach.amount_escrow.present?
            main_tx = ach.try(:transaction)
            fee_perc_details["fee_perc"] = main_tx.try(:tags).try(:[],"fee_perc")
            transaction = Transaction.create(
                to: nil,
                from: wallet.id,
                status: "pending",
                amount: ach.actual_amount,
                sender: escrow_user,
                ach_gateway_id: ach.ach_gateway_id,
                sender_name: escrow_user.try(:name),
                receiver_wallet_id: wallet.id,
                sender_wallet_id: escrow_wallet.try(:id),
                receiver_id: user.try(:id),
                receiver_name: user.try(:name),
                action: 'transfer',
                net_amount: ach.actual_amount.to_f,
                total_amount: check_total_amount,
                main_type: ach_check_type,
                tags: tags,
                gbox_fee: main_tx.try(:gbox_fee),
                iso_fee: main_tx.try(:gbox_fee),
                affiliate_fee: main_tx.try(:gbox_fee),
                agent_fee: main_tx.try(:gbox_fee),
                fee: main_tx.try(:fee),
                net_fee: main_tx.try(:net_fee)
            )
            issue=issue_checkbook_amount(ach.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info,ach,nil, nil, true)
            ach.amount_escrow = false
          else
            transaction = user.transactions.create(
                to: nil,
                from: wallet.id,
                status: "pending",
                amount: ach.actual_amount,
                sender: user,
                ach_gateway_id: ach.ach_gateway_id,
                receiver_wallet_id: wallet.id,
                action: 'issue',
                net_amount: ach.actual_amount.to_f,
                total_amount: ach.actual_amount.to_f,
                main_type: ach_check_type,
                tags: tags
            )
            issue=issue_checkbook_amount(ach.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info, nil, nil,nil, true)
          end
          if issue.present?
            transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags.merge(fee_perc_details), timestamp: issue["timestamp"])
            #= creating block transaction
            ach.void_transaction_id = transaction.id
            parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
            save_block_trans(parsed_transactions,"sub","void") if parsed_transactions.present?
            ach.status = "VOID"
            ach.approved = true
            ach.settled = true
            ach.save
            description = description.push({id: ach.id, amount: ach.amount, transaction_id: transaction.id, merchant_id: user.id, location_id: location.id})
          end
        end
        cron_job.description = description
        cron_job.success = true
        cron_job.rake_task!
      end
    rescue => exc
      cron_job.description = {error: exc.message, backtrace: exc.backtrace.first(8)}
      cron_job.success = false
      cron_job.rake_task!
    end
  end

  task move_balance: :environment do
    #task for moving balance of dispensary locations
    include ApplicationHelper
    include Admins::BlockTransactionsHelper
    begin
      cron_job = CronJob.create(name: "move balance")
      description = []
      category = Category.where(name: "Dispensary").last
      locations = category.locations
      locations.each do |location|
        buy_rate = location.fees.buy_rate.first
        merchant = location.merchant
        primary_wallet = location.wallets.primary.first
        reserve_wallet = location.wallets.reserve.first
        primary_balance = SequenceLib.balance(primary_wallet.id)
        if primary_balance.to_f > 0
          transaction = Transaction.create(
              status: "pending",
              receiver_wallet_id: reserve_wallet.id,
              sender_wallet_id: primary_wallet.id,
              sender_id: merchant.id,
              receiver_id: merchant.id,
              amount: primary_balance,
              total_amount: primary_balance,
              fee: 0,
              net_amount: primary_balance,
              net_fee: primary_balance,
              sender_name: primary_wallet.name,
              receiver_name: reserve_wallet.name,
              main_type: "account_transfer",
              action: "transfer"
              )
          issue = SequenceLib.transfer(primary_balance, primary_wallet.id, reserve_wallet.id,"account_transfer",0, nil, "quickcard", nil, merchant, location)
          if issue.present?
            transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
            parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
            save_block_trans(parsed_transactions) if parsed_transactions.present?
            description = description.push({wallet_id: primary_wallet.id, reserve_wallet: reserve_wallet.id, amount: primary_balance, message: "transfered"})
            buy_rate.update(days: 0)
          end
        else
          description = description.push({wallet_id: primary_wallet.id, reserve_wallet: reserve_wallet.id, message: "insufficient balance"})
        end
      end
      cron_job.description = description
      cron_job.success = true
      cron_job.rake_task!
    rescue => exc
      cron_job.description = {error: exc.message, backtrace: exc.backtrace.first(8)}
      cron_job.success = false
      cron_job.rake_task!
    end
  end
end