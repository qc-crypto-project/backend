namespace :update_encryption_by_lockbox do
	task users: :environment do
		puts "=====Update all Users====="
		files = []
		job = CronJob.new(name: 'users:re-encryption', description: 'Update Encryted data', success: true)
		result = {updates: [], error: nil}
		begin
			User.where(role: ["iso","agent","affiliate"]).each do |user|
				old_value = nil
				job.success = true
				begin
					if user.system_fee.try(:[],'bank_account').present?
						old_value = user['system_fee']['bank_account'].dup
						decrypted_value = AESCrypt.decrypt("#{user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",user['system_fee']['bank_account'] , true)
						user['system_fee']['bank_account'] = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						if user.save
							puts "user #{user.id} updated"
							result[:updates].push({"#{user.id}"=>{"bank_account" => old_value}})
							files.push({id: user.id, status: "updated",message: "",old: old_value,new: user['system_fee']['bank_account']})
						end
					end
				rescue Exception => e
					job.success = false
					puts "user_id: #{user.id}",e
					files.push({id: user.id, status: "failed",message: e , old: old_value ,new: ""})
				end
			end
		rescue => e
			job.success = false
			result[:error] = "Error: #{e.message}"
			puts 'Error occured during the task process: ', e
		end
		job.result = result
		job.rake_task!
		upload_to_slack("user",files)  if files.present?
		puts "end"
	end
	task revert_users: :environment do
		updated_users = CronJob.where(name: 'users:re-encryption', description: 'Update Encryted data').last
		updating_users = []
		updated_users.result["updates"].each do|user|
			user = user.with_indifferent_access
			user_obj = User.find_by(id: user.keys.first)
			if user_obj.present?
				user_obj['system_fee']['bank_account'] = user["#{user_obj.id}"][:bank_account]
				updating_users.push(user_obj)
			end
		end
		User.import updating_users, on_duplicate_key_update: {conflict_target: [:id], columns: [:system_fee]}
	end

	task payment_gateway: :environment do
		files = []
		job = CronJob.new(name: 'payment_gateway:re-encryption', description: 'Update Encryted data', success: true)
		result = {updates: [], error: nil}
		begin
			PaymentGateway.main_gateways.each do |payment_gateway|
				new_values={}
				job.success = true
				begin
					if payment_gateway.client_secret.present? || payment_gateway.client_id.present? || payment_gateway.authentication_id.present? || payment_gateway.signature_maker.present?
						old_values={client_secret: payment_gateway.client_secret.dup, client_id: payment_gateway.client_id.dup, authentication_id: payment_gateway.authentication_id.dup, signature_maker: payment_gateway.signature_maker.dup, processor_id: payment_gateway.processor_id.dup}
						if payment_gateway.client_secret.present?
							decrypted_value = AESCrypt.decrypt_dd(ENV['SECRET_KEY_GATEWAY'], payment_gateway.client_secret)
							payment_gateway.client_secret = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end

						if payment_gateway.client_id.present?
							decrypted_value = AESCrypt.decrypt_dd(ENV['SECRET_KEY_GATEWAY'], payment_gateway.client_id)
							payment_gateway.client_id = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end

						if payment_gateway.authentication_id.present?
							decrypted_value = AESCrypt.decrypt_dd(ENV['SECRET_KEY_GATEWAY'], payment_gateway.authentication_id)
							payment_gateway.authentication_id = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end

						if payment_gateway.signature_maker.present?
							decrypted_value =  AESCrypt.decrypt_dd(ENV['SECRET_KEY_GATEWAY'], payment_gateway.signature_maker)
							payment_gateway.signature_maker = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end

						if payment_gateway.processor_id.present?
							decrypted_value = AESCrypt.decrypt_dd(ENV['SECRET_KEY_GATEWAY'], payment_gateway.processor_id)
							payment_gateway.processor_id = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end

						if payment_gateway.save
							new_values={client_secret: payment_gateway.client_secret, client_id: payment_gateway.client_id, authentication_id: payment_gateway.authentication_id, signature_maker: payment_gateway.signature_maker, processor_id: payment_gateway.processor_id}
							result[:updates].push({"#{payment_gateway.id}"=>old_values})
							files.push({id: payment_gateway.id, status: "updated",message: "",old: old_values,new: new_values})
							puts("Payment Gateway #{payment_gateway.id} updated")
						end
					end
				rescue Exception => e
					job.success = false
					puts "user_id: #{payment_gateway.id}",e
					files.push({id: payment_gateway.id, status: "failed",message: e , old: old_values ,new: ""})
				end
			end
		rescue => e
			job.success = false
			result[:error] = "Error: #{e.message}"
			puts 'Error occured during the task process: ', e
		end
		job.result = result
		job.rake_task!
		upload_to_slack("payment_gateway",files) if files.present?
		puts "end"
	end
	task revert_payment_gateway: :environment do
		updated_payment_gateway = CronJob.where(name: 'payment_gateway:re-encryption', description: 'Update Encryted data').last
		updating_payment_gateway = []
		updated_payment_gateway.result["updates"].each do|payment_gateway|
			payment_gateway = payment_gateway.with_indifferent_access
			payment_gateway_obj = PaymentGateway.find_by(id: payment_gateway.keys.first)
			if payment_gateway_obj.present?
				old_data = payment_gateway["#{payment_gateway_obj.id}"]
				payment_gateway_obj.client_secret = old_data[:client_secret] if old_data.try(:[],:client_secret).present?
				payment_gateway_obj.client_id = old_data[:client_id] if old_data.try(:[],:client_id).present?
				payment_gateway_obj.authentication_id = old_data[:authentication_id] if old_data.try(:[],:authentication_id).present?
				payment_gateway_obj.signature_maker = old_data[:signature_maker] if old_data.try(:[],:signature_maker).present?
				payment_gateway_obj.processor_id = old_data[:processor_id] if old_data.try(:[],:processor_id).present?
				updating_payment_gateway.push(payment_gateway_obj)
			end
		end
		PaymentGateway.import updating_payment_gateway, on_duplicate_key_update: {conflict_target: [:id], columns: [:client_secret,:client_id,:authentication_id,:signature_maker,:processor_id]}
	end

	task locations: :environment do
		files = []
		puts "=====Update all locations====="
		job = CronJob.new(name: 'locations:re-encryption', description: 'Update Encryted data', success: true)
		result = {updates: [], error: nil}
		begin
			Location.where.not(bank_account: nil).each do |location|
				job.success = true
				old_value = nil
				begin
					old_value = location.bank_account.dup
					decrypted_value = AESCrypt.decrypt("#{location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",location.bank_account , true)
					location.bank_account = AESCrypt.encrypt(ENV['AES_KEY'],decrypted_value)
					if location.save
						puts "location #{location.id} updated"
						result[:updates].push({"#{location.id}"=>{"bank_account" => old_value}})
						files.push({id: location.id, status: "updated",message: "" , old: old_value , new: location.bank_account})
					end
				rescue Exception => e
					puts "location #{location.id}",e
					job.success = false
					files.push({id: location.id, status: "failed",message: e , old: old_value , new: ""})
				end
			end
		rescue => e
			job.success = false
			result[:error] = "Error: #{e.message}"
			puts 'Error occured during the task process: ', e
		end
		job.result = result
		job.rake_task!
		upload_to_slack("location",files) if files.present?
		puts "end"
	end
	task revert_locations: :environment do
		updated_locations = CronJob.where(name: 'locations:re-encryption', description: 'Update Encryted data').last
		updating_location = []
		updated_locations.result["updates"].each do|location|
			location = location.with_indifferent_access
			location_obj = Location.find_by(id: location.keys.first)
			if location_obj.present?
				location_obj.bank_account = location["#{location_obj.id}"][:bank_account]
				updating_location.push(location_obj)
			end
		end
		Location.import updating_location, on_duplicate_key_update: {conflict_target: [:id], columns: [:bank_account]}
	end

	task bank_details: :environment do
		files = []
		puts "=====Update all banks====="
		job = CronJob.new(name: 'bank_details:re-encryption', description: 'Update Encryted data', success: true)
		result = {updates: [], error: nil}
		begin
			BankDetail.where.not(account_no: nil).each do |bank|
				job.success = true
				old_value = nil
				begin
					old_value = bank.account_no.dup
					decrypted_value = AESCrypt.decrypt("#{bank.location_id}-#{ENV['ACCOUNT_ENCRYPTOR']}",bank.account_no , true)
					bank.account_no = AESCrypt.encrypt(ENV['AES_KEY'],decrypted_value)
					if bank.save
						puts "bank #{bank.id} updated"
						result[:updates].push({"#{bank.id}"=>{"account_no" => old_value}})
						files.push({id: bank.id, status: "updated",message: "" , old: old_value , new: bank.account_no})
					end
				rescue Exception => e
					job.success = false
					puts "bank #{bank.id}",e
					files.push({id: bank.id, status: "failed",message: e, old: old_value , new: ""})
				end
			end
		rescue => e
			job.success = false
			result[:error] = "Error: #{e.message}"
			puts 'Error occured during the task process: ', e
		end
		job.result = result
		job.rake_task!
		upload_to_slack("bank",files) if files.present?
		puts "end"
	end
	task revert_bank_details: :environment do
		updated_bank = CronJob.where(name: 'bank_details:re-encryption', description: 'Update Encryted data').last
		updating_bank = []
		updated_bank.result["updates"].each do|bank|
			bank = bank.with_indifferent_access
			bank_obj = BankDetail.find_by(id: bank.keys.first)
			if bank_obj.present?
				bank_obj.account_no = bank["#{bank_obj.id}"][:account_no]
				updating_bank.push(bank_obj)
			end
		end
		BankDetail.import updating_bank, on_duplicate_key_update: {conflict_target: [:id], columns: [:account_no]}
	end

	task tango_orders: :environment do
		puts "=====Update all Tango Orders====="
		files = []
		job = CronJob.new(name: 'tango_orders:re-encryption', description: 'Update Encryted data', success: true)
		result = {updates: [], error: nil}
		begin
			TangoOrder.where.not(account_token: nil).where(order_type: ["instant_pay","instant_ach"]).find_in_batches(batch_size: 1000) do |tango_orders|
				tango_order_updating = []
				tango_orders.each do |tango_order|
					job.success = true
					old_value = nil
					begin
						old_value = tango_order.account_token.dup
						if  tango_order.instant_pay?
							decrypted_value = AESCrypt.decrypt("#{tango_order.wallet.merchant.id}-#{ENV['CARD_ENCRYPTER']}-#{tango_order.wallet.id}}", tango_order.account_token , true)
							tango_order.account_token = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value )
						elsif tango_order.instant_ach?
							decrypted_value = AESCrypt.decrypt("#{tango_order.wallet.merchant.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{tango_order.wallet.id}", tango_order.account_token , true)
							tango_order.account_token = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end
						files.push({id: tango_order.id, status: "updated",message: "",old: old_value,new: tango_order.account_token})
						result[:updates].push({"#{tango_order.id}"=>{"account_token" => old_value}})
						tango_order_updating.push(tango_order)
					rescue Exception => e
						job.success = false
						puts "Tango Order #{tango_order.id} updated",e
						files.push({id: tango_order.id, status: "failed",message: e, old: old_value , new: ""})
					end
				end
				TangoOrder.import tango_order_updating, on_duplicate_key_update: {conflict_target: [:id], columns: [:account_token]}
			end
		rescue StandardError => exc
			job.success = false
			result[:error] = "Error: #{exc.message}"
			puts 'Error occured during the task process: ', exc
		end
		job.result = result
		job.rake_task!
		upload_to_slack("tango_orders",files) if files.present?
		puts "end"
	end
	task revert_tango_orders: :environment do
		updated_tango_orders = CronJob.where(name: 'tango_orders:re-encryption', description: 'Update Encryted data').last
		updating_tango_orders = []
		updated_tango_orders.result["updates"].each do|tango_order|
			tango_order = tango_order.with_indifferent_access
			tango_order_obj = TangoOrder.find_by(id: tango_order.keys.first)
			if tango_order_obj.present?
				tango_order_obj.account_token = tango_order["#{tango_order_obj.id}"][:account_token]
				updating_tango_orders.push(tango_order_obj)
			end
		end
		TangoOrder.import updating_tango_orders, on_duplicate_key_update: {conflict_target: [:id], columns: [:account_token]}
	end

	task qc_cards: :environment do
		files = []
		begin
			job = CronJob.new(name: 'qc_cards:re-encryption', description: 'Update Encryted data', success: true)
			result = {updates: [], error: nil}
      i = 0
			# Card.where.not(qc_token: nil).find_in_batches(batch_size: 10000) do |cards|
      Card.where.not(qc_token: nil).find_in_batches(batch_size: 10000) do |cards|
        puts "==========================="
        i = i + 1
        puts "=========================== batch = #{i}"
        puts "==========================="
        updating_card = []
				files = []
				cards.each do|card|
					begin
						if card.fingerprint.present?
							job.success = true
							decrypted_value =  AESCrypt.decrypt("#{card.fingerprint}-#{card.user_id}-#{ENV['CARD_ENCRYPTER']}",card.qc_token,true)
							if decrypted_value.present? && decrypted_value.class == String && decrypted_value.first == "{" && decrypted_value.include?("number")
								card_info = JSON.parse(decrypted_value)
								if card_info.present? && card_info.class == Hash
									card_info = card_info.with_indifferent_access
									card_info.except!(:cvv) if card_info[:cvv].present?
									card_number = card_info["number"]
									old_fingerprint = card.fingerprint.dup
									old_qc_token = card.qc_token.dup
									card.fingerprint = AESCrypt.encrypt("#{ENV['CARD_AES_KEY']}-#{ENV['AES_KEY']}", card_number,nil,nil,true)
									card.qc_token = AESCrypt.encrypt("#{card.fingerprint}-#{card.user_id}-#{ENV['CARD_AES_KEY']}",card_info.to_json,nil,true)
									result[:updates].push({"#{card.id}"=>{"fingerprint" => "#{old_fingerprint}", "qc_token" => "#{old_qc_token}"}})
									updating_card.push(card)
								end
							end
						end
					rescue Exception => e
						job.success = false
						files.push({id: card.id, status: "failed",message: e.message, old: "" , new: ""})
						puts "#{e} ============= #{card.id}"
					end
        end
				upload_to_slack("failed_cards_batch#{i}",files) if files.present?
				Card.import updating_card, on_duplicate_key_update: {conflict_target: [:id], columns: [:fingerprint,:qc_token]}
        end
			puts "Cards updated successfully!"
		rescue StandardError => exc
			job.success = false
			result[:error] = "Error: #{exc.message}"
			puts 'Error occured during the task process: ', exc
		end
		job.result = result
		job.rake_task!
		puts "end"
	end

	task revert_qc_cards: :environment do
		updated_cards = CronJob.where(name: 'qc_cards:re-encryption', description: 'Update Encryted data', success: true).last
		updating_card = []
		updated_cards.result["updates"].each do|card_record|
			card_record = card_record.with_indifferent_access
			card = Card.find_by(id: card_record.keys.first)
			if card.present?
				card.qc_token = card_record["#{card.id}"][:qc_token]
				card.fingerprint = card_record["#{card.id}"][:fingerprint]
				updating_card.push(card)
			end
		end
		Card.import updating_card, on_duplicate_key_update: {conflict_target: [:id], columns: [:fingerprint,:qc_token]}
  end

  task tango_orders_instant_ach: :environment do
    puts "=====Update all Tango Orders====="
    files = []
    job = CronJob.new(name: 'tango_orders_instant_pay:re-encryption', description: 'Update Encryted data', success: true)
    result = {updates: [], error: nil}
    begin
			TangoOrder.joins(:user).where("(users.role = 7 OR users.role = 9 OR users.role = 10) AND tango_orders.order_type = 7").each do |tango_order|
          job.success = true
          old_value = nil
          begin
            old_value = tango_order.account_token.dup
						if tango_order.instant_ach?
							user = tango_order.user
							system_fee = user.system_fee
							if system_fee.present?
								card_info = {
										:account_number => system_fee.try(:[],"bank_account"),
										:account_type => system_fee.try(:[],"bank_account_type")
								}
								card_info = card_info.to_json
								x = AESCrypt.encrypt("abc-#{ENV['ACCOUNT-ENCRYPTOR']}-a", card_info)
								tango_order.account_token = x
                tango_order.save
								files.push({id: tango_order.id, status: "updated",message: "",old: old_value,new: tango_order.account_token})
								result[:updates].push({"#{tango_order.id}"=>{"account_token" => old_value}})
                puts "TangoOrder Id => #{tango_order.id} updated"
              else
								files.push({id: tango_order.id, status: "failed",message: "system_fee not found",old: old_value,new: "" })
								puts "TangoOrder Id => #{tango_order.id} failed"
							end
						end
          rescue Exception => e
            job.success = false
            puts "failed Tango Order #{tango_order.id} updated",e
            files.push({id: tango_order.id, status: "failed",message: e, old: old_value , new: ""})
          end
        end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during the task process: ', exc
    end
    job.result = result
    job.rake_task!
    upload_to_slack("tango_orders_instant_pay",files) if files.present?
    puts "end"
  end

  	task pending_tango_orders_agent_iso: :environment do
		puts "=====Update all Tango Orders====="
		files = []
		job = CronJob.new(name: 'pending_tango_orders:re-encryption', description: 'Update Encryted data', success: true)
		result = {updates: [], error: nil}
		begin
			TangoOrder.joins(:user).where("users.role = 7 OR users.role = 9 OR users.role = 10").where.not(account_token: nil).where.not(id: [13106,13340,13346,13347,13180,10156,13028,13234,11547,11756,11843,11840,11839,11841,11842,13260,10282,13275,13277,13143,12951,13316,13318]).where(order_type: ["instant_pay","instant_ach"]).find_in_batches(batch_size: 1000) do |tango_orders|
				tango_order_updating = []
				tango_orders.each do |tango_order|
					job.success = true
					old_value = nil
					begin
						old_value = tango_order.account_token.dup
						if  tango_order.instant_pay?
							decrypted_value = AESCrypt.decrypt("#{tango_order.user_id}-#{ENV['CARD_ENCRYPTER']}-#{tango_order.wallet.id}}", tango_order.account_token , true)
							tango_order.account_token = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						elsif tango_order.instant_ach?
							decrypted_value = AESCrypt.decrypt("#{tango_order.user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{tango_order.wallet.id}", tango_order.account_token , true)
							tango_order.account_token = AESCrypt.encrypt(ENV['AES_KEY'], decrypted_value)
						end
						files.push({id: tango_order.id, status: "updated",message: "",old: old_value,new: tango_order.account_token})
						result[:updates].push({"#{tango_order.id}"=>{"account_token" => old_value}})
						tango_order_updating.push(tango_order)
					rescue Exception => e
						job.success = false
						puts "Tango Order #{tango_order.id} updated",e
						files.push({id: tango_order.id, status: "failed",message: e, old: old_value , new: ""})
					end
				end
				TangoOrder.import tango_order_updating, on_duplicate_key_update: {conflict_target: [:id], columns: [:account_token]}
			end
		rescue StandardError => exc
			job.success = false
			result[:error] = "Error: #{exc.message}"
			puts 'Error occured during the task process: ', exc
		end
		job.result = result
		job.rake_task!
		upload_to_slack("tango_orders",files) if files.present?
		puts "end"
	end

	task revert_pending_tango_orders: :environment do
		puts("revert pending tango orders")
		updated_tango_orders = CronJob.where(name: 'pending_tango_orders:re-encryption', description: 'Update Encryted data').last
		updating_tango_orders = []
		updated_tango_orders.result["updates"].each do|tango_order|
			tango_order = tango_order.with_indifferent_access
			tango_order_obj = TangoOrder.find_by(id: tango_order.keys.first)
			if tango_order_obj.present?
				tango_order_obj.account_token = tango_order["#{tango_order_obj.id}"][:account_token]
				updating_tango_orders.push(tango_order_obj)
			end
		end
		TangoOrder.import updating_tango_orders, on_duplicate_key_update: {conflict_target: [:id], columns: [:account_token]}	
		puts("end")
	end


	def upload_to_slack(name,files)
		file = File.new("#{Rails.root}/tmp/#{name}.csv", "w")
		file << CSV.generate do |csv|
			csv << ["ID", "Status", "Message","Old","New"]
			files.each do |file|
				csv << [file[:id], file[:status], file[:message] , file[:old] , file[:new]]
			end
		end
		file.close
		SlackService.upload_files(name, file, "#quickcard-internal", "#{name} encryption updation")
	end



end