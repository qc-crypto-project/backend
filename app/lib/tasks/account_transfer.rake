namespace :account_transfer do
  desc "update Sequence ID"
  task update: :environment do
    puts "Start updating"
    begin
      job = CronJob.new(name: 'AccountTransferSeqID', description: 'Updating Sequence id in Transfer + Transaction', success: true)
      object = {updates: [], error: nil}
      Transfer.all.each_slice(500) do |batch|
        transfer_update=[]
        transaction_update=[]
        batch.each do |transfer|
         
          if transfer.sequence_id.present?
            old_seq_id = transfer.sequence_id.dup
            seq_transaction = SequenceLib.get_transaction_id(old_seq_id)
            if seq_transaction.present?
              new_seq_id = seq_transaction.first.try(:actions).try(:first).try(:id)
              if new_seq_id.present? && new_seq_id != old_seq_id
                transfer.sequence_id = new_seq_id
                transfer_update.push(transfer)
                resultant = {transfer_id: transfer.id, new_value: transfer.sequence_id, old_value:old_seq_id}
                transfer_transaction = Transaction.find_by(seq_transaction_id: old_seq_id)
                if transfer_transaction.present?
                  resultant[:transaction_id] = transfer_transaction.id
                  transfer_transaction.seq_transaction_id = new_seq_id
                  transaction_update.push(transfer_transaction)
                end
                object[:updates].push(resultant)
              end
            end
          end
        end
        if transfer_update.present?
          Transfer.import transfer_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:sequence_id]}
        end
        if transaction_update.present?
          Transaction.import transaction_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:seq_transaction_id]}
        end
      end
      puts 'Updated Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during transfer Update: ', exc
    end
    job.result = object
    job.rake_task!
  end

  task revert_update: :environment do
    puts 'Reverting SeqID for transactions + transfers'
    job = CronJob.where(success: true, name: 'AccountTransferSeqID').last
    if job["error"].nil? && job.result["updates"].present?
      begin
        transfer_update=[]
        transaction_update=[]
        job.result["updates"].each do |trans|
          transfer = Transfer.find_by(id: trans[:transfer_id])
          if transfer.present?
            transfer.sequence_id = trans[:old_value]
            transfer_update.push(transfer)
          end
          transaction = Transaction.find_by(id: trans[:transaction_id])
          if transaction.present?
            transaction.seq_transaction_id = trans[:old_value]
            transaction_update.push(transaction)
          end
        end
        if transfer_update.present?
          Transfer.import transfer_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:sequence_id]}
        end
        if transaction_update.present?
          Transaction.import transaction_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:seq_transaction_id]}
        end
        puts 'Reverted'
      rescue StandardError => exc
        puts 'Error occured during task revert: ', exc
      end
    end
  end
end
