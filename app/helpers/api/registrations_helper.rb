module Api::RegistrationsHelper

  def check_fee_amount(location, receiver, user, wallet, buyrate, amount, balance, fee_type = nil, type = nil, load_fee=nil)
    feeCommission = Payment::FeeCommission.new(user, receiver, buyrate, wallet, location, fee_type,nil,nil,load_fee)
    #amount calculate for each user merchant gbox iso agent and affiliate
    feeCalc = feeCommission.apply(amount.to_f, type, nil)
    if feeCalc.present?
      totalFee = number_with_precision(feeCalc["fee"].to_f, precision: 2).to_f
      minimumAmountRequired = amount.to_f + balance.to_f
      #status true if fee is less than total balance
      if minimumAmountRequired >= totalFee.to_f
        return {status: true, fee: totalFee, splits: feeCalc["splits"]}
      else
        return {status: false, fee: totalFee}
      end
    end
  end

  def check_icanpay(gateway,params)
    if gateway == TypesEnumLib::GatewayType::ICanPay
      if params[:country].present? && params[:city].present? && params[:street].present? && params[:state].present? && params[:zip_code].present?
        true
      else
        false
      end
    end
  end
end
