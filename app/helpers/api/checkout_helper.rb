module Api::CheckoutHelper
  include ApplicationHelper

  ALGORITHM = 'HS256'

  def get_checkout_link(params)
    validate_parameters(
        :auth_token => params['auth_token'],
        :location_id => params['location_id'],
        :website_url => params['website_url'],
        :success_url => params['success_url'],
        :fail_url => params['fail_url'],
        :total_amount => params['total_amount']
    )
    merchant = User.authenticate_by_token(params['auth_token']).first

    return {:success => false, :status => TypesEnumLib::Statuses::Failed, :message => "Authentication Failed"} if merchant.blank?

    location = Location.where(location_secure_token: params['location_id']).last

    return {success: false, status: TypesEnumLib::Statuses::Failed, message: I18n.t('errors.withdrawl.account_not_exist')} if location.blank?

    # checking if merchant website is present under its location setting or not ------- starts -------
    allow = false
    websites = JSON.parse(location.try(:web_site))
    if websites.present? && websites.include?(params['website_url'])
      allow = true
    end

    if allow == false
      if location.ecomm_platform.present? && location.ecomm_platform.name == params['website_url']
        allow = true
      end
    end

    return {success: false, status:  TypesEnumLib::Statuses::Failed, message: "Provided website url not present under location settings!"} if allow == false
    # checking if merchant website is present under its location setting or not ------- end ------
    phone_number=params['shipping_phone_number']
    if phone_number.blank?
      phone_number=rand.to_s[2..11]
    end

    if params['shipping_name'].present?
      name = params['shipping_name']
      name = name.split(" ")
      first_name = name.first
      last_name = name.last
      user = User.where(email: params['shipping_email'],role: :user).or(User.where(phone_number: params['shipping_phone_number'],role: :user))
      user = user.first

      if user.present?
        user.source = merchant.name
        user.is_block = false
        user.archived = false
      else
        user = User.new
        user.name = params['shipping_name']
        user.first_name = first_name
        user.last_name = last_name
        user.email = params['shipping_email']
        user.phone_number = phone_number
        user.source = merchant.name
        password = random_password
        user.password = password
        user.role = :user
        user.is_block = false
        user.archived = false
      end

      user.save
    end
    transaction = nil
    if params[:request_id].present?
      @request = Request.find_by(id: params[:request_id])
      if @request.present?
        transaction = @request.invoice_transaction
      end
      if transaction.blank?
        transaction = Transaction.create(
            sender_name: user.name,
            sender_id: user.id,
            sender_wallet_id: user.wallets.first.try(:id),
            receiver_id: merchant.id,
            receiver_name: merchant.name,
            receiver_wallet_id: location.wallets.primary.first.try(:id),
            action: "transfer"
        )
      end
    else
      transaction = Transaction.create(
          sender_name: user.name,
          sender_id: user.id,
          sender_wallet_id: user.wallets.first.try(:id),
          receiver_id: merchant.id,
          receiver_name: merchant.name,
          receiver_wallet_id: location.wallets.primary.first.try(:id),
          action: "transfer"
      )
    end

    if params['shipping_name'].present?
      shipping_address = Address.new
      shipping_address.name = params['shipping_name']
      shipping_address.email = params['shipping_email']
      shipping_address.phone_number = params['shipping_phone_number']
      shipping_address.street = params['shipping_street']
      shipping_address.city = params['shipping_city']
      shipping_address.state = params['shipping_state']
      shipping_address.country = params['shipping_country']
      shipping_address.zip = params['shipping_zip']
      shipping_address.address_type = "Shipping"
      shipping_address.save
    end

    if params['billing_name'].present?
      # billing_address = ActiveRecord::Base::Address.new
      billing_address = Address.new
      billing_address.name = params['billing_name']
      billing_address.email = params['billing_email']
      billing_address.phone_number = params['billing_phone_number']
      billing_address.street = params['billing_street']
      billing_address.city = params['billing_city']
      billing_address.state = params['billing_state']
      billing_address.country = params['billing_country']
      billing_address.zip = params['billing_zip']
      billing_address.address_type = "Billing"
      billing_address.save
    end

    user.addresses << shipping_address
    user.addresses << billing_address
    total_products_amount = 0
    if params['products'].present?
      params['products'].each do |k,v|
        product = Product.new
        product.name = k["name"]
        product.description = k["description"]
        product.amount = k["amount"]
        product.quantity = k["quantity"]
        product.website_url = params['website_url']
        product.transaction_id = transaction.id
        product.order_id = params['order_id']
        product.save
        total_products_amount += (product.amount * product.quantity)
      end
    end
    payload = {auth_token: params['auth_token'], checkout_id: transaction.id, merchant_name: merchant.name, website_url: params['website_url'],success_url: params['success_url'],fail_url: params['fail_url'],redirect_url: params['redirect_url'],order_id: params['order_id'],request_id: params['request_id'], exp: (Time.zone.now + 60.minute).to_i}
    token = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM

    transaction.shipping_id = shipping_address.try(:id)
    transaction.billing_id = billing_address.try(:id)
    transaction.delivery_status = "pending"
    transaction.status = "pending"
    transaction.main_type = "3DS"
    transaction.sub_type = "invoice" if params['request_id'].present?
    transaction.tax = params['total_tax'].to_f
    transaction.amount = total_products_amount
    transaction.total_amount = params['total_amount'].to_f
    transaction.redirect_url = checkout_index_path(token: token)
    transaction.order_id = params['order_id']
    transaction.save
    return {token: token, success: true}
  end
end