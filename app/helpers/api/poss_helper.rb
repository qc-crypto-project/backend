module Api::PossHelper
  include RegistrationsHelper

  def getting_slot(slot,amount, privacy_fee, tip = nil, location_slot = nil, slot_over_under = nil)
    
    selected_slot_attempt1 = [0,nil,nil,false] # calculated_value, gateway_id, attempt1 or 2,true if count == percentage_value
    selected_slot_attempt2 = [0,nil,nil,false]
    slot1_name = nil
    slot1_decimal = false
    slot2_name = nil
    slot2_decimal = false
    if location_slot.present?
      slot_detail = location_slot
    else
      slot_detail = slot.try(:slot_detail)
    end
    slot_detail.each do|k,v|
      pec_attempt1 = v["value1"].to_f
      count_attempt1 = v["count1"].to_f
      pec_attempt2 = v["value2"].to_f
      count_attempt2 = v["count2"].to_f
      if pec_attempt1 > 0
        perc_calculation1 = pec_attempt1 * count_attempt1/100
        attempt1_calculation = pec_attempt1 - perc_calculation1
        if pec_attempt1 == count_attempt1         #= if value and attempt are equal
          if selected_slot_attempt1.first <= attempt1_calculation
            selected_slot_attempt1 = [attempt1_calculation, k,"attempt1",true]
          end
        else
          if selected_slot_attempt1.first < attempt1_calculation
            selected_slot_attempt1 = [attempt1_calculation, k,"attempt1",false]
          end
        end
      end

      if pec_attempt2 > 0
        perc_calculation2 = pec_attempt2 * count_attempt2/100
        attempt2_calculation = pec_attempt2 - perc_calculation2
        if pec_attempt2 == count_attempt2
          if selected_slot_attempt2.first <= attempt2_calculation
            selected_slot_attempt2 = [attempt2_calculation, k,"attempt2",true]
          end
        else
          if selected_slot_attempt2.first < attempt2_calculation
            selected_slot_attempt2 = [attempt2_calculation, k,"attempt2",false]
          end
        end
      end
    end
    if selected_slot_attempt1.second.present?
      slot1 = PaymentGateway.find_by(id: selected_slot_attempt1.second)
      slot1_name = slot1.try(:slot_name)
      slot1_decimal = slot1.try(:allow_decimal)
      if slot_detail["#{selected_slot_attempt1.second}"]["value1"].to_f > 0
        if selected_slot_attempt1.fourth
          slot_detail["#{selected_slot_attempt1.second}"]["total_count1"] = "#{slot_detail["#{selected_slot_attempt1.second}"]["count1"].to_i + 1}"
          slot_detail["#{selected_slot_attempt1.second}"]["count1"] = 1.to_s
        else
          slot_detail["#{selected_slot_attempt1.second}"]["count1"] = "#{slot_detail["#{selected_slot_attempt1.second}"]["count1"].to_i + 1}"
          slot_detail["#{selected_slot_attempt1.second}"]["total_count1"] = "#{slot_detail["#{selected_slot_attempt1.second}"]["total_count1"].to_i + 1}"
        end
      end
    end

    if selected_slot_attempt2.second.present?
      slot2 = PaymentGateway.find_by(id: selected_slot_attempt2.second)
      slot2_name = slot2.try(:slot_name)
      slot2_decimal = slot2.try(:allow_decimal)
      if slot_detail["#{selected_slot_attempt2.second}"]["value2"].to_f > 0
        if selected_slot_attempt2.fourth
          slot_detail["#{selected_slot_attempt2.second}"]["total_count2"] = "#{slot_detail["#{selected_slot_attempt2.second}"]["count2"].to_i + 1}"
          slot_detail["#{selected_slot_attempt2.second}"]["count2"] = 1.to_s
        else
          slot_detail["#{selected_slot_attempt2.second}"]["count2"] = "#{slot_detail["#{selected_slot_attempt2.second}"]["count2"].to_i + 1}"
          slot_detail["#{selected_slot_attempt2.second}"]["total_count2"] = "#{slot_detail["#{selected_slot_attempt2.second}"]["total_count2"].to_i + 1}"
        end
      end
    end
    if location_slot.present?
      slot.update("#{slot_over_under}": slot_detail)
    else
      slot.update(slot_detail: slot_detail)
    end
    new_amount = amount.to_f + tip.to_f + privacy_fee.to_f
    a_merchant1 = slot1_decimal ? number_with_precision(round_5(new_amount), precision: 2) : number_with_precision(new_amount, precision: 2)
    a_merchant2 = slot2_decimal ? number_with_precision(round_5(new_amount), precision: 2) : number_with_precision(new_amount, precision: 2)
    return {merchant1: slot1_name,merchant2: slot2_name,a_merchant1: a_merchant1, a_merchant2: a_merchant2}
  end

  # def create_transaction_debit(merchant, transaction_to, receiver_id, card, amount,request,ref_id=nil,         privacy_fee=nil, tip = nil, clerk_id = nil,terminal_id=nil, type = nil, sub_type = nil)
  def create_transaction_debit(merchant, transaction_to, receiver_id, card, amount,request,ref_id=nil,type=nil,privacy_fee=nil, tip = nil, clerk_id = nil,terminal_id=nil, fee = nil, discount=nil, sub_type=nil,action=nil, receiver = nil, receiver_wallet = nil,from=nil,load_fee=nil)
      tx = Transaction.where(ref_id: ref_id)
      if tx.present? && type.blank?
        return nil
      else
        if type.present?
          main_type = type
        else
          main_type = updated_type(TypesEnumLib::TransactionType::SaleType, TypesEnumLib::GatewayType::PinGateway)
        end
        if load_fee.present?
          total_amount = amount.to_f + privacy_fee.to_f + load_fee.to_f
        else
          total_amount = amount.to_f + privacy_fee.to_f + tip.to_f + load_fee.to_f
        end
        if action.present? && action == "transfer"
          receiver_ID = receiver.try(:id)
          receiver_wallet_id = receiver_wallet
          receiver_name = receiver.try(:name)
          sender_ID = receiver_id
          sender_wallet_id = transaction_to
          sender_name = merchant.try(:name)
        else
          receiver_ID = receiver_id
          receiver_wallet_id = transaction_to
          receiver_name = merchant.try(:name)
          sender_ID = nil
          sender_wallet_id = nil
          sender_name = nil
        end
        tx = Transaction.create(
            to: transaction_to,
            from: from,
            status: "pending",
            amount: amount,
            user_id: merchant,
            receiver_id: receiver_ID,
            receiver_wallet_id: receiver_wallet_id,
            receiver_name: receiver_name,
            sender_id: sender_ID,
            sender_wallet_id: sender_wallet_id,
            sender_name: sender_name,
            action: action.present? ? action : 'issue',
            last4: card.last4,
            first6: card.first6,
            total_amount: total_amount,
            net_amount: amount.to_f,
            fee: fee.to_f,
            net_fee: fee.to_f,
            tip: tip.to_f,
            ip: get_ip_with_request(request),
            main_type: main_type,
            sub_type: sub_type,
            card_id: card.try(:id),
            ref_id: ref_id,
            discount: discount.present? ? discount : 0,
            privacy_fee: privacy_fee,
            clerk_id: clerk_id,
            terminal_id: terminal_id
        )
      end
      tx
    end

  def getting_or_creating_card
    #create card_info object
    user_id = @card_customer.present? ? @card_customer.id : @user.id
    #check card exist or not by bin list service
    current_user = @card_customer || @user
    merchant_id = @card_customer.present? ? @user.id : nil

    bin_result=check_bin_list(current_user,merchant_id,user_id)
    card=bin_result[:card]
    card_bin_list=bin_result[:card_bin_list]
    #check all required paramters exist
    validate_parameters(
        :card_cvv => params[:card_cvv]
    )
    #check card is blocked or not
    card_block = check_card_blocked(params[:card_number],"#{params[:card_exp_date].first(2)}/#{params[:card_exp_date].last(2)}", @card_info)

    #raise error if card blocked
    if card_block
      msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: Error Code 3018 Cannot process transaction. Please contact support."
      SlackService.notify(msg, "#qc-risk-alert")
      raise StandardError.new(I18n.t('api.errors.blocked_card'))
    end 
    #raise error if card not whitelisted
    if @location.vip_card && !Card.check_vip_card?(params[:card_number],"#{params[:card_exp_date].first(2)}/#{params[:card_exp_date].last(2)}")
      msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: #{I18n.t("api.errors.non_vip_card")}"
      SlackService.notify(msg, "#qc-risk-alert")
      raise StandardError.new(I18n.t('api.errors.non_vip_card'))
    end
    result = recurring_flaged_trans(card,params)
    raise SaleValidationError.new "#{result[:message]}" unless result[:success]
    card.save
    if check_card_decline(card) && params.try(:[], "action") != "virtual_terminal_transaction_debit"
      card.update(decline_attempts: card.decline_attempts.to_i + 1, last_decline: DateTime.now.utc)
      @cultivate_params = true
      @card_number = params[:card_number]
      return {status: false}
    end
    return {status: true,card: card,card_info: @card_info, card_bin_list: card_bin_list,card_bank: card.try(:bank)}
  end
end
