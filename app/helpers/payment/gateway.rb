require 'net/http'
require 'net/https'

module Payment
  class Gateway
    include ActionView::Helpers::NumberHelper

    # == Standardized Error Codes
    #
    # :incorrect_number - Card number does not comply with ISO/IEC 7812 numbering standard
    # :invalid_number - Card number was not matched by processor
    # :invalid_expiry_date - Expiry date does not match correct formatting
    # :invalid_cvc - Security codes does not match correct format (3-4 digits)
    # :expired_card - Card number is expired
    # :incorrect_cvc - Security code was not matched by the processor
    # :incorrect_zip - Zip code is not in correct format
    # :incorrect_address - Billing address info was not matched by the processor
    # :incorrect_pin - Card PIN is incorrect
    # :card_declined - Card number declined by processor
    # :processing_error - Processor error
    # :call_issuer - Transaction requires voice authentication, call issuer
    # :pickup_card - Issuer requests that you pickup the card from merchant
    # :test_mode_live_card - Card was declined. Request was in test mode, but used a non test card.
    # :unsupported_feature - Transaction failed due to gateway or merchant
    #                        configuration not supporting a feature used, such
    #                        as network tokenization.

    STANDARD_ERROR_CODE = {
      :incorrect_number => 'incorrect_number',
      :invalid_number => 'invalid_number',
      :invalid_expiry_date => 'invalid_expiry_date',
      :invalid_cvc => 'invalid_cvc',
      :expired_card => 'expired_card',
      :incorrect_cvc => 'incorrect_cvc',
      :incorrect_zip => 'incorrect_zip',
      :incorrect_address => 'incorrect_address',
      :incorrect_pin => 'incorrect_pin',
      :card_declined => 'card_declined',
      :processing_error => 'processing_error',
      :call_issuer => 'call_issuer',
      :pickup_card => 'pick_up_card',
      :config_error => 'config_error',
      :test_mode_live_card => 'test_mode_live_card',
      :unsupported_feature => 'unsupported_feature',
    }

    def generate_unique_id
      SecureRandom.hex(16)
    end

    # The supported card types for the gateway
    class_attribute :supported_cardtypes
    self.supported_cardtypes = []

    class_attribute :test_url, :live_url

    # Use this method to check if your gateway of interest supports a credit card of some type
    def self.supports?(card_type)
      supported_cardtypes.include?(card_type)
    end

    # Initialize a new gateway.
    #
    # See the documentation for the gateway you will be using to make sure there are no other
    # required options.
    def initialize(options = {})
      @options = options
    end

    protected # :nodoc: all

    def normalize(field)
      case field
        when 'true'   then true
        when 'false'  then false
        when ''       then nil
        when 'null'   then nil
        else field
      end
    end

    private

    def split_names(full_name)
      names = (full_name || '').split
      return [nil, nil] if names.size == 0

      last_name  = names.pop
      first_name = names.join(' ')
      [first_name, last_name]
    end

    def validate_required_params(args)
      args.each do |name, value|
        if value.blank?
          return {response: nil,message: {message: "Required Param: #{name}, missing for Gateway"}}
        end
      end
      return nil
    end

    def validate__totalpay_required_params(args)
      args.each do |name, value|
        if value.blank?
          return {response: nil,message: {message: "Required Param: #{name}, missing for Total Pay Gateway"}}
        end
      end
    end

    def validate_knox_params(args)
      args.each do |name, value|
        if value.blank?
          return {response: nil,message: {message: "Required Param: #{name}, missing for Knox Gateway"}}
        end
      end
    end

    def requires!(hash, *params)
      params.each do |param|
        if param.is_a?(Array)
          raise ArgumentError.new("Missing required parameter: #{param.first}") unless hash.has_key?(param.first)
          # valid_options = param[1..-1]
          # raise ArgumentError.new("Parameter: #{param.first} must be one of #{valid_options.to_sentence(:words_connector => 'or')}") unless valid_options.include?(hash[param.first])
        else
          raise ArgumentError.new("Missing required parameter: #{param}") unless hash.has_key?(param) && hash[param].present?
        end
      end
    end

    def centsFromDollars(dollars)
      return number_with_precision(dollars.to_f * 100, precision: 2).to_i
    end

    def dollarsFromCents(cents)
      return number_with_precision(cents.to_f / 100, precision: 2).to_f
    end

    def format_dollar(amount)
      return number_with_precision(amount, precision: 2)
    end

    def save_payment
    end
  end
end
