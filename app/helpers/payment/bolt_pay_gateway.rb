module Payment
  class BoltPayGateway < Gateway

    attr_accessor :virtual_terminal_transaction

    def initialize
      @virtual_terminal_transaction = false
    end

    def charge(amount,card_info,user,payment_gateway=nil, card_name = nil, card = nil, card_cvv=nil, billing_address=nil)
      begin
        api_key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
        rand = "#{SecureRandom.hex(2)}#{rand(100..999)}"
        @reference = DESCrypt.encrypt(api_key, "#{rand}#{Time.now.to_i.to_s}")
        @merchant_token = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
        if !@virtual_terminal_transaction && card.present? && card.bolt_token.present? && (card.bolt_bank.present? && card.bolt_bank == payment_gateway.try(:name))
          return charge_by_token(amount, card, payment_gateway, api_key, billing_address)
        end
        data = generate_data(billing_address, api_key, @merchant_token, amount, @reference, card_info, nil, card_cvv)
        if card_name.present?
          data[:customer_name] = DESCrypt.encrypt(api_key, card_name)
        else
          data[:customer_name] = DESCrypt.encrypt(api_key, user.name)
        end
        if !@virtual_terminal_transaction
          data[:customer_email] = "#{DESCrypt.encrypt(api_key, user.email)}"
          data[:customer_phone] = DESCrypt.encrypt(api_key, user.phone_number)
        end
        curlObj = building_request("#{ENV['BOLT_PAY_API_URL']}charge/",data)
        curlObj.perform()
        response = curlObj.body_str
        return bolt_handle_response(response,@reference, data)
      rescue Curl::Err::TimeoutError
        SlackService.notify("App: \`\`\ `#{{URL: ENV["APP_ROOT"]}} \`\`\` Error: \`\`\ `Bolt Pay Timeout \`\`\` User: \`\`\`id: #{user.try(:id)}, email: #{user.try(:email)}, role: #{user.try(:role)}  \`\`\` BACKTRACE: \`\`\ ` \`\`\`")
        data = {
            merchant_token: @merchant_token,
            reference: @reference
        }
        curlObj = building_request("#{ENV['BOLT_PAY_API_URL']}status/",data)
        begin
          curlObj.perform()
          response = curlObj.body_str
          return bolt_handle_response(response,@reference,data)
        rescue Curl::Err::TimeoutError
          SlackService.notify("App: \`\`\ `#{{URL: ENV["APP_ROOT"]}} \`\`\` Error: \`\`\ `Bolt Pay Timeout \`\`\` User: \`\`\`id: #{user.try(:id)}, email: #{user.try(:email)}, role: #{user.try(:role)}  \`\`\` BACKTRACE: \`\`\ ` \`\`\`")
          return {response: nil, reference: @reference, data: data,message: {message: "Bad Gateway GB"}}
        end
      end
    end

    def charge_by_token(amount, card, payment_gateway, api_key, billing_address)
      @merchant_token = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
      data = generate_data(billing_address, api_key, @merchant_token, amount, @reference, nil, card.bolt_token)
      curlObj = Curl::Easy.new("#{ENV['BOLT_PAY_API_URL']}charge-token/")
      curlObj.connect_timeout = 30
      curlObj.timeout = 30
      curlObj.header_in_body = false
      curlObj.ssl_verify_peer = false
      curlObj.post_body = data.to_json
      curlObj.perform()
      response = curlObj.body_str
      if response.present?
        response = JSON.parse(response)
        if response["status"] == "success"
          if response["data"]["responsecode"] == "00"
            return {response: response["data"]["payment_reference"].to_s.gsub('BP_LIVE_', ''), message:{message: response["data"]["responsemessage"]}}
          else
            return {response: nil,message: {message: response["data"]["responsemessage"]}}
          end
        elsif response["status"] == "error"
          if response["data"].present?
            return {response: nil,message: {message: response["data"]["responsemessage"]}}
          else
            return {response: nil,message: {message: response["message"]}}
          end
        else
          return {response: nil,message: {message: "Bad Gateway GB"}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway GB"}}
      end
    end

    def refund(reference, amount=nil,payment_gateway=nil)
      api_key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
      if reference.to_s.include?('BP_LIVE') === false
        reference = 'BP_LIVE_' + reference.to_s
      end
      data = {
          merchant_token: AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret),
          reference: DESCrypt.encrypt(api_key, reference),
      }
      if amount.present?
        amount = DESCrypt.encrypt(api_key, "#{amount}") if amount.present?
        data[:amount] = amount
      end
      curlObj = Curl::Easy.new("#{ENV['BOLT_PAY_API_URL']}refund/")
      curlObj.connect_timeout = 30
      curlObj.timeout = 30
      curlObj.header_in_body = false
      curlObj.ssl_verify_peer = false
      curlObj.post_body = data.to_json
      curlObj.perform()
      response = curlObj.body_str
      if response.present?
        response = JSON.parse(response)
        puts "Response from bank", response
        if response["status"] == "success"
          if response["data"]["responsecode"] == "00"
            return {success: response["data"]["refund_reference"]}
          elsif response["data"]["responsemessage"].present?
            return {message: response["data"]["responsemessage"]}
          else
            return {message: "Bad Gateway GB"}
          end
        else
          return {message: "Bad Gateway GB"}
        end
      else
        return {message: "Bad Gateway GB"}
      end
    end

    private

    def valid_card(card_number)
      # card = CardToken.new
      # brand = card.card_brand(card_number)
      # length = 16
      # length = 14 if brand == 'Diners Club'
      # length = 15 if brand == 'American Express'
      # raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) if card_number.length < length
    end

    def bolt_handle_response(response,reference, data = nil)
      if response.present?
        response = JSON.parse(response)
        if response["status"] == "success"
          if response["data"]["responsecode"] == "00"
            return {virtual_terminal_transaction: @virtual_terminal_transaction, data: data,bolt_token: response["data"].try(:[],"card").try(:[],"chargetoken"), response: response["data"]["payment_reference"].to_s.gsub('BP_LIVE_', ''), reference: reference, message:{message: response["data"]["responsemessage"]}}
          else
            return {response: nil, data: data, reference: reference,message: {message: response["data"]["responsemessage"]}}
          end
        elsif response["status"] == "error"
          if response["data"].present?
            return {response: nil, reference: reference, data: data,message: {message: response["data"]["responsemessage"]}}
          else
            return {response: nil, reference: reference, data: data,message: {message: response["message"]}}
          end
        else
          return {response: nil, reference: reference, data: data,message: {message: "Bad Gateway GB"}}
        end
      else
        return {response: nil, reference: reference, data: data,message: {message: "Bad Gateway GB"}}
      end
    end

    def building_request(url,data)
      curlObj = Curl::Easy.new("#{url}")
      curlObj.connect_timeout = 30
      curlObj.timeout = 30
      curlObj.header_in_body = false
      curlObj.ssl_verify_peer = false
      curlObj.post_body = data.to_json
      return curlObj
    end

    # def test_mode
    #   return ENV.fetch("CONVERGE_TEST_MODE") { true }.to_s == 'true'
    # end

    def generate_data(billing_address, api_key, merchant_token, amount, reference, card_info=nil, token=nil, card_cvv=nil)
      data = {
          merchant_token: merchant_token,
          currency: DESCrypt.encrypt(api_key, "USD"),
          amount: DESCrypt.encrypt(api_key, "#{amount}"),
          narration: DESCrypt.encrypt(api_key, "narration"),
          txref: reference
      }
      if token
        data.merge!({source: DESCrypt.encrypt(api_key, token), })
      else
        data.merge!({
            cardnumber: DESCrypt.encrypt(api_key, card_info.number),
            cvv: DESCrypt.encrypt(api_key, card_cvv),
            expiry_month: DESCrypt.encrypt(api_key, card_info.month),
            expiry_year: DESCrypt.encrypt(api_key, card_info.year),
        })
      end
      if billing_address[:require_address]
        data.merge!({
                        billing_address_lineone: DESCrypt.encrypt(api_key, billing_address[:street]),
                        billing_city: DESCrypt.encrypt(api_key, billing_address[:city]),
                        billing_state: DESCrypt.encrypt(api_key, billing_address[:state]),
                        billing_zip: DESCrypt.encrypt(api_key, billing_address[:zip])
                    })
      end
      return data
    end

  end
end
