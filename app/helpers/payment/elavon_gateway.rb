module Payment
  class ElavonGateway < Gateway
    self.live_url = ""

    def tokenize_card(card, expiry, cvv, options = {})
      # TOKENIZE here....
      valid_card(card)

      parsed_hash = {}
      url = "#{ENV['CONVERGE_URL']}?ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{card}&ssl_exp_date=#{expiry}&ssl_transaction_type=ccgettoken&ssl_cvv2cvc2_indicator=1&cvv=#{cvv}&ssl_verify=N"
      response = HTTParty.get(url)
      if response.success?
        response.parsed_response.split("\n").map do |item|
          arr = item.split("=")
          parsed_hash[arr[0]] = arr[1]
        end
        if parsed_hash["errorCode"].present?
          message = parsed_hash["errorName"]
          raise StandardError.new(message)
        else
          return parsed_hash["ssl_token"]
        end
      else
        raise StandardError.new('Error getting Elavon Token!')
      end
    end

    def charge(amount,number,exp_date,cvv,payment_gateway=nil,billing_address=nil)
      ssl_transaction_type = 'ccsale'
      ssl_cvv2_cvc2_indicator = 1
      parsed_hash = {}
      if payment_gateway.present?
        converge_merchant_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
        converge_pin = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
        converge_user_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.authentication_id)
        if billing_address[:require_address]
          url = "#{ENV['CONVERGE_URL']}?ssl_merchant_id=#{converge_merchant_id}&ssl_user_id=#{converge_user_id}&ssl_pin=#{converge_pin}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{number}&ssl_exp_date=#{exp_date}&ssl_amount=#{amount}&ssl_transaction_type=#{ssl_transaction_type}&ssl_cvv2cvc2_indicator=#{ssl_cvv2_cvc2_indicator}&ssl_cvv2cvc2=#{cvv}&ssl_test_mode=#{test_mode()}&ssl_state=#{billing_address[:state]}&ssl_city=#{billing_address[:city]}&ssl_country=#{billing_address[:country]}&ssl_address2=#{billing_address[:street]}&ssl_avs_zip=#{billing_address[:zip]}"
        else
          url = "#{ENV['CONVERGE_URL']}?ssl_merchant_id=#{converge_merchant_id}&ssl_user_id=#{converge_user_id}&ssl_pin=#{converge_pin}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{number}&ssl_exp_date=#{exp_date}&ssl_amount=#{amount}&ssl_transaction_type=#{ssl_transaction_type}&ssl_cvv2cvc2_indicator=#{ssl_cvv2_cvc2_indicator}&ssl_cvv2cvc2=#{cvv}&ssl_test_mode=#{test_mode()}"
        end
      else
        if billing_address[:require_address]
          url = "#{ENV['CONVERGE_URL']}?ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{number}&ssl_exp_date=#{exp_date}&ssl_amount=#{amount}&ssl_transaction_type=#{ssl_transaction_type}&ssl_cvv2cvc2_indicator=#{ssl_cvv2_cvc2_indicator}&ssl_cvv2cvc2=#{cvv}&ssl_test_mode=#{test_mode()}&ssl_state=#{billing_address[:state]}&ssl_city=#{billing_address[:city]}&ssl_country=#{billing_address[:country]}&ssl_address2=#{billing_address[:street]}&ssl_avs_zip=#{billing_address[:zip]}"
        else
          url = "#{ENV['CONVERGE_URL']}?ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{number}&ssl_exp_date=#{exp_date}&ssl_amount=#{amount}&ssl_transaction_type=#{ssl_transaction_type}&ssl_cvv2cvc2_indicator=#{ssl_cvv2_cvc2_indicator}&ssl_cvv2cvc2=#{cvv}&ssl_test_mode=#{test_mode()}"
        end
      end

      response = HTTParty.get(url)
      if response.success?
        response.parsed_response.split("\n").map do |item|
          arr = item.split("=")
          parsed_hash[arr[0]] = arr[1]
        end
        if parsed_hash["ssl_result"].present? && parsed_hash["ssl_result"] == "0"
          return {response: parsed_hash["ssl_txn_id"],approval_code: parsed_hash["ssl_approval_code"], message:{message: parsed_hash["ssl_result_message"]}}
        elsif parsed_hash["ssl_result"].present? && parsed_hash["ssl_result"] != "0"
          return {response: nil,message: {message: parsed_hash["ssl_result_message"]}}
        elsif parsed_hash["errorMessage"].present?
          return {response: nil,message: {message: parsed_hash["errorMessage"]}}
        else
          return {response: nil,message: {message: "Bad Gateway"}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway"}}
      end
    end

    def void(transaction_id,payment_gateway)
      if payment_gateway.present?
        converge_merchant_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
        converge_pin = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
        converge_user_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.authentication_id)
        url = "#{ENV['CONVERGE_URL']}?ssl_test_mode=#{test_mode()}&ssl_merchant_id=#{converge_merchant_id}&ssl_user_id=#{converge_user_id}&ssl_pin=#{converge_pin}&ssl_transaction_type=ccvoid&ssl_txn_id=#{transaction_id}"
      else
        url = "#{ENV['CONVERGE_URL']}?ssl_test_mode=#{test_mode()}&ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_transaction_type=ccvoid&ssl_txn_id=#{transaction_id}"
      end

      parsed_hash = {}
      response = HTTParty.get(url)
      if response.success?
        response.parsed_response.split("\n").map do |item|
          arr = item.split("=")
          parsed_hash[arr[0]] = arr[1]
        end
        if parsed_hash["ssl_result"].present? && parsed_hash["ssl_result"] == "0"
          return {success: parsed_hash["ssl_result_message"]}
        elsif parsed_hash["ssl_result"].present? && parsed_hash["ssl_result"] != "0"
          return {message: parsed_hash["ssl_result_message"]}
        elsif parsed_hash["errorCode"].present?
          return {message: parsed_hash["errorMessage"]}
        end
      end
    end

    def refund(transaction_id, amount, gateway)
      parsed_hash = {}
      url = "#{ENV['CONVERGE_URL']}?ssl_test_mode=#{test_mode()}&ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_amount=#{amount}&ssl_transaction_type=ccreturn&ssl_txn_id=#{transaction_id}"
      if gateway.present?
        # gateway = PaymentGateway.find_by(key: source)
        if gateway.present?
          converge_merchant_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],gateway.client_id)
          converge_user_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],gateway.authentication_id)
          converge_pin = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],gateway.client_secret)
          url = "#{ENV['CONVERGE_URL']}?ssl_test_mode=#{test_mode()}&ssl_merchant_id=#{converge_merchant_id}&ssl_user_id=#{converge_user_id}&ssl_pin=#{converge_pin}&ssl_show_form=false&ssl_result_format=ascii&ssl_amount=#{amount}&ssl_transaction_type=ccreturn&ssl_txn_id=#{transaction_id}"
        end
      end
      response = HTTParty.get(url)
      p "CONVERGE RESPONSE: ", response
      if response.success?
        response.parsed_response.split("\n").map do |item|
          arr = item.split("=")
          parsed_hash[arr[0]] = arr[1]
        end
        if parsed_hash["ssl_result"].present? && parsed_hash["ssl_result"] == "0"
          return {success: parsed_hash["ssl_result_message"],refund_id: parsed_hash["ssl_txn_id"]}
        elsif parsed_hash["ssl_result"].present? && parsed_hash["ssl_result"] != "0"
          return {message: parsed_hash["ssl_result_message"]}
        elsif parsed_hash["errorCode"].present?
          return {message: parsed_hash["errorMessage"]}
        end
      end
      return {message: "Refund Failed!"}
    end

    def verify(transaction_id, amount, options = {})
      # ADD VERIFY code here...
    end

    private

    def valid_card(card_number)
      # card = CardToken.new
      # brand = card.card_brand(card_number)
      # length = 16
      # length = 14 if brand == 'Diners Club'
      # length = 15 if brand == 'American Express'
      # raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) if card_number.length < length
    end

    def capture(token, amount)
      # ...
    end

    def test_mode
      return ENV.fetch("CONVERGE_TEST_MODE") { true }.to_s == 'true'
    end
  end
end
