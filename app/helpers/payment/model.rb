module Payment
  class Model
    def initialize(attributes = {},token=nil,key=nil)
      attributes.each do |key, value|
        send("#{key}=", value)
      end
    end

    def validate
      {}
    end

    def empty?(value = nil)
      case value
      when nil
        true
      when Array, Hash
        value.empty?
      when String
        value.strip.empty?
      when Numeric
        (value == 0)
      else
        false
      end
    end

    private

    def errors_hash(array)
      array.inject({}) do |hash, (attribute, error)|
        (hash[attribute] ||= []) << error
        hash
      end
    end
  end
end
