module Payment
  class FeeCommission
    include ActionView::Helpers::NumberHelper
    include Merchant::SalesHelper
    attr_accessor :type, :fee, :sender, :receiver, :receiver_wallet

    def initialize(sender, receiver, fee, receiver_wallet = nil, location = nil, fee_type = nil,gateway = nil,pin_debit_refund = nil, load_fee=nil)
      #----------------------- Fee Type => 'GiftCard','SendCheck','AddMoney'------------
      @super_users = []
      @users_1 = []
      @users_2 = []
      @users_3 = []
      @super_company = {}
      @net_super_company = []
      @pin_debit_refund = pin_debit_refund
      @load_fee = load_fee
      @payment_gateway = gateway
      if receiver.present? && sender.present? && receiver.merchant? && !sender.merchant? && fee_type == TypesEnumLib::CommissionType::B2B
        @type = TypesEnumLib::CommissionType::B2B
      elsif receiver.present? && sender.present? && receiver.merchant? && !sender.merchant?
        @type = TypesEnumLib::CommissionType::C2B
      elsif receiver.present? && sender.present? && receiver.merchant? && sender.merchant?
        @type = TypesEnumLib::CommissionType::B2B
      elsif receiver.present? &&  receiver.merchant? && sender.nil?
        if fee_type.present?
          @type = TypesEnumLib::CommissionType::C2B
        else
          @type = TypesEnumLib::CommissionType::QrCard
        end
      elsif receiver.present? && sender.present? && receiver.merchant? && sender.partner?
        @type = ''
      elsif receiver.present? && sender.present? && !receiver.merchant? && !sender.merchant?
        @type = TypesEnumLib::CommissionType::C2C
      elsif sender.present? && receiver.nil? && sender.merchant?
        @type = fee_type
      end
      if fee_type.present?
        @type = fee_type
      end
      @fee = fee
      @receiver_wallet = receiver_wallet if receiver_wallet.present?
      @sender = sender if sender.present?
      @receiver = receiver if receiver.present?
      @receiver = sender if @receiver.nil?
      @location = location if @receiver_wallet.present?
      @iso = @location.isos.first if @location.present?
      @iso_wallet = @iso.wallets.first if @iso.present?
      if @location.dispensary_credit_split || @location.dispensary_debit_split
        if @location.profit_split_detail.present?
          @profit_detail = JSON.parse(@location.profit_split_detail) if @location.profit_split_detail.kind_of? String
          if @profit_detail.present?
            if @location.dispensary_credit_split
              @dispensary_credit = @profit_detail["splits"]["dispensary_credit_split"]
            end
            if @location.dispensary_debit_split
              @dispensary_debit = @profit_detail["splits"]["dispensary_debit_split"]
            end
          end
        end
      end
      if @location.profit_split || @location.baked_iso || @location.net_profit_split || @location.sub_merchant_split
        if @location.profit_split_detail.present?
          @profit_detail = JSON.parse(@location.profit_split_detail) if @location.profit_split_detail.kind_of? String
          if @profit_detail.present?
            if @profit_detail["splits"].present?
              @profit_detail["splits"].except("ez_merchant","profit_split").each do |key,value| # Getting all users in @super_users & split_profit's users in @super_company
                key_name = key.tr("0-9", "")
                unless key_name.blank?
                  if key_name == "net"
                    @net_super_company.push(value)
                  end
                else
                  @super_users.push(value)
                end
              end
              @super_users.each do |users| # setting user_1 in @users_1 & so on.
                users.each do |k,v|
                  if k == "user_1"
                    @users_1.push(v)
                  elsif k == "user_2"
                    @users_2.push(v)
                  elsif k == "user_3"
                    @users_3.push(v)
                  end
                end
              end
            end
            if @profit_detail["splits"]["profit_split"].present?
              @profit_detail["splits"]["profit_split"].each do |key,split|
                @super_company = @super_company.merge({"#{key}"=> split})
              end
            end
          end
        end
      end
      @agent = @location.agents.first if @location.present?
      @agent_wallet = @agent.wallets.first if @agent.present?
      @affiliate = @location.affiliates.first if @location.present?
      @affiliate_wallet = @affiliate.wallets.first if @affiliate.present?
      @iso_balance = SequenceLib.balance(@iso_wallet.id) if @iso_wallet.present?
      @agent_balance = SequenceLib.balance(@agent_wallet.id) if @agent_wallet.present?
      @affiliate_balance = SequenceLib.balance(@affiliate_wallet.id) if @affiliate_wallet.present?
      @qc_wallet = Wallet.qc_support.first.id
      @location_wallet = @location.wallets.primary.first.id if @location.present?
    end

    def apply(amount_in_dollars, fee_type = nil, count = nil)
      case @type
      when TypesEnumLib::CommissionType::C2B
        return c2b(amount_in_dollars, fee_type)
      when TypesEnumLib::CommissionType::B2B
        return b2b(amount_in_dollars)
      when TypesEnumLib::CommissionType::QrCard
        return c2b(amount_in_dollars, fee_type)
      when TypesEnumLib::CommissionType::SendCheck
        return send_check(amount_in_dollars, count)
      when TypesEnumLib::CommissionType::ACH
        return ach_fees(amount_in_dollars)
      when TypesEnumLib::CommissionType::AddMoney
        return add_money(amount_in_dollars)
      when TypesEnumLib::CommissionType::Giftcard
        return gift_card(amount_in_dollars)
      when TypesEnumLib::CommissionType::ChargeBack
        return charge_back_fee(amount_in_dollars)
      when TypesEnumLib::CommissionType::Retrieval
        return retrievel(amount_in_dollars)
      when TypesEnumLib::CommissionType::Monthly
        return monthly(fee_type)
      when TypesEnumLib::TransactionType::RefundFee
        return refund_fee(amount_in_dollars)
      when TypesEnumLib::CommissionType::DebitCardDeposit
        return debit_card_deposit(amount_in_dollars, count)
      when TypesEnumLib::CommissionType::Failedinstant_pay, TypesEnumLib::CommissionType::Failedinstant_ach, TypesEnumLib::CommissionType::Failedcheck
        return failed_withdrawals(amount_in_dollars, count)
      end
    end

    # ---------------------------Customer To Merchant----------------------
    def c2b(amount_in_dollars, fee_type = nil)
      percent_fee = 0
      dollar_fee = 0
      if @fee.present?
        if fee_type == "credit"
          percent_fee = deduct_fee(@load_fee.present? ? @fee.load_fee_percent_credit.to_f : @fee.transaction_fee_ccard.to_f, amount_in_dollars.to_f)
          dollar_fee = @load_fee.present? ? @fee.load_fee_credit.to_f : @fee.transaction_fee_ccard_dollar.to_f
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeCcard
        elsif fee_type == "debit"
          percent_fee = deduct_fee(@load_fee.present? ? @fee.load_fee_percent_debit.to_f : @fee.transaction_fee_dcard.to_f, amount_in_dollars.to_f)
          dollar_fee = @load_fee.present? ? @fee.load_fee_debit.to_f : @fee.transaction_fee_dcard_dollar.to_f
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeDcard
        elsif fee_type == "invoice_credit"
          percent_fee = deduct_fee(@fee.invoice_card_fee_perc.to_f, amount_in_dollars.to_f)
          dollar_fee = @fee.invoice_card_fee.to_f
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeInvoicecard
        elsif fee_type == "invoice_debit"
          percent_fee = deduct_fee(@fee.invoice_debit_card_fee_perc.to_f, amount_in_dollars.to_f)
          dollar_fee = @fee.invoice_debit_card_fee.to_f
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeInvoiceDebitcard
        elsif fee_type == "invoice_rtp"
          percent_fee = deduct_fee(@fee.invoice_rtp_fee_perc.to_f, amount_in_dollars.to_f)
          dollar_fee = @fee.invoice_rtp_fee.to_f
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeInvoiceRTP
        elsif fee_type == "rtp"
          percent_fee = deduct_fee(@fee.rtp_fee_perc.to_f, amount_in_dollars.to_f)
          dollar_fee = @fee.rtp_fee.to_f
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeRTP
        else
          percent_fee = deduct_fee(@fee.transaction_fee_app.to_f, amount_in_dollars.to_f)
          dollar_fee = @fee.transaction_fee_app_dollar
          @current_fee_type = TypesEnumLib::CommissionType::TransactionFeeApp
        end
      end
      total_fee = percent_fee.to_f + dollar_fee.to_f
      # if (fee_type == "credit" && @location.dispensary_credit_split) || (fee_type == "debit" && @location.dispensary_debit_split)
      #   dispensary_split(fee_type, amount_in_dollars, total_fee)
      # else
        calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
      # end
    end
    # -------------------------Merchant To Merchant------------------------
    def b2b(amount_in_dollars)
      dollar_fee = @fee.b2b_fee_dollar.to_f
      percent_fee = deduct_fee(@fee.b2b_fee.to_f, amount_in_dollars)
      total_fee = percent_fee.to_f + dollar_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::B2B
      calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
    end
    # ------------------------Send Check-------------------------
    def send_check(amount_in_dollars, count = nil)
      percent_fee = deduct_fee(@fee.redeem_fee.to_f, amount_in_dollars)
      if count.present?
        dollar_fee = @fee.send_check.to_f * count
      else
        dollar_fee = @fee.send_check.to_f
      end
      total_fee = percent_fee.to_f + dollar_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::SendCheck
      calculate_commission(amount_in_dollars.to_f, total_fee.to_f, count)
    end

    # ------------------------Debit Card Deposit-------------------------
    def debit_card_deposit(amount_in_dollars, count = nil)
      percent_fee = deduct_fee(@fee.dc_deposit_fee.to_f, amount_in_dollars)
      if count.present?
        dollar_fee = @fee.dc_deposit_fee_dollar.to_f * count
      else
        dollar_fee = @fee.dc_deposit_fee_dollar.to_f
      end
      total_fee = percent_fee.to_f + dollar_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::DebitCardDeposit
      calculate_commission(amount_in_dollars.to_f, total_fee.to_f, count)
    end
    def failed_withdrawals(amount_in_dollars, count = nil)
      percent = 0
      dollar = 0
      if @type == TypesEnumLib::CommissionType::Failedinstant_pay
        percent = @fee.failed_push_to_card_percent_fee
        dollar = @fee.failed_push_to_card_fee
      elsif @type == TypesEnumLib::CommissionType::Failedinstant_ach
        percent = @fee.failed_ach_percent_fee
        dollar = @fee.failed_ach_fee
      elsif @type == TypesEnumLib::CommissionType::Failedcheck
        percent = @fee.failed_check_percent_fee
        dollar = @fee.failed_check_fee
      end
      percent_fee = deduct_fee(percent.to_f, amount_in_dollars)
      if count.present?
        dollar_fee = dollar.to_f * count
      else
        dollar_fee = dollar.to_f
      end
      total_fee = percent_fee.to_f + dollar_fee.to_f
      @current_fee_type = @type
      calculate_commission(amount_in_dollars.to_f, total_fee.to_f, count)
    end
    # ------------------------ACH-------------------------
    def ach_fees(amount_in_dollars)
      percent_fee = deduct_fee(@fee.ach_fee.to_f, amount_in_dollars)
      dollar_fee = @fee.ach_fee_dollar.to_f
      total_fee = percent_fee.to_f + dollar_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::ACH
      calculate_commission(amount_in_dollars.to_f, total_fee.to_f)
    end

    # ------------------------Add Money by check-------------------------
    def add_money(amount_in_dollars)
      percent_fee = deduct_fee(@fee.redeem_check.to_f, amount_in_dollars)
      dollar_fee = @fee.add_money_check.to_f
      total_fee = percent_fee.to_f + dollar_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::AddMoney
      calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
    end
    # ------------------------Giftcard-------------------------
    def gift_card(amount_in_dollars)
      total_fee = @fee.giftcard_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::Giftcard
      calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
      # split_profit(total_fee.to_f, 0, 0,0, amount_in_dollars, total_fee.to_f, nil, nil)
    end
    # ----------------------ChargeBackFee------------------------
    def charge_back_fee(amount_in_dollars)
      total_fee = @fee.charge_back_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::ChargeBack
      calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
    end

    #-----------------------------Retrievel fee
    def retrievel(amount_in_dollars)
      total_fee = @fee.retrievel_fee.to_f
      @current_fee_type = TypesEnumLib::CommissionType::Retrieval
      calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
    end

    #------------------------------ Monthly fee
    def monthly(fee_type)
      if @fee.present?
        if @iso.present?
          if @location.risk.present?
            if @location.high?
              iso_fee = @iso.fees.buy_rate.high.first
            elsif @location.dispensary?
              iso_fee = @iso.fees.buy_rate.low.first
            end
          else
            # TODO remove after success full working of location risk all around system
            if @receiver.high?
              iso_fee = @iso.fees.buy_rate.high.first
            elsif @receiver.low?
              iso_fee = @iso.fees.buy_rate.low.first
            end
          end
          if iso_fee.present?
            profit = get_monthly_fee_profit(iso_fee, @fee, fee_type)
            if profit.present?
              return calculate_monthly_profit(JSON.parse(iso_fee.commission),profit)
            else
              nil
            end
          else
            nil
          end
        end
      else
        nil
      end
    end

    #-----------------------------Refund fee
    def refund_fee(amount_in_dollars)
      # total_fee = deduct_fee(@fee.refund_fee.to_f, amount_in_dollars)
      total_fee = 0
      if @pin_debit_refund
        if (@fee.qr_refund.present? && @fee.qr_refund > 0) || (@fee.qr_refund_percentage.present? && @fee.qr_refund_percentage > 0)
          fee_dollar = @fee.qr_refund
          fee_percentage = @fee.qr_refund_percentage.present? ? (@fee.qr_refund_percentage/100 * amount_in_dollars) : 0
          total_fee = fee_dollar + fee_percentage
        end
      else
        total_fee = @fee.refund_fee.to_f
      end
      @current_fee_type = TypesEnumLib::TransactionType::RefundFee
      calculate_commission(amount_in_dollars.to_f,total_fee.to_f)
    end
    # -------------------------Calculate Commission Function----------------
    def calculate_commission(amount_in_dollars, fee_amount, count = nil)
      if @receiver.MERCHANT?
        if @location.present?
          if @location.risk.present?
            if @location.high?
              raise StandardError, "ISO not setup. Please Contact Adminstrator!" if @iso.blank?
              raise StandardError, "ISO fees not setup. Please Contact Adminstrator!" if @iso.fees.nil?
              iso_fee = @iso.fees.buy_rate.high.first
            elsif @location.dispensary?
              raise StandardError, "ISO not setup. Please set Iso!" if @iso.blank?
              raise StandardError, "ISO fees not setup. Please set Iso fees!" if @iso.fees.nil?
              iso_fee = @iso.fees.buy_rate.low.first
            end
          else
            # TODO remove after success full working of location risk all around system
            if @receiver.high?
              raise StandardError, "ISO not setup. Please Contact Adminstrator!" if @iso.blank?
              raise StandardError, "ISO fees not setup. Please Contact Adminstrator!" if @iso.fees.nil?
              iso_fee = @iso.fees.buy_rate.high.first
            elsif @receiver.low?
              raise StandardError, "ISO not setup. Please set Iso!" if @iso.blank?
              raise StandardError, "ISO fees not setup. Please set Iso fees!" if @iso.fees.nil?
              iso_fee = @iso.fees.buy_rate.low.first
            end
          end
          profit = get_profit(iso_fee,@current_fee_type,fee_amount,amount_in_dollars, count)
          if profit.present? && iso_fee.present?
            return calculate_profit_percentage(profit[:base_amount],profit[:total_profit],JSON.parse(iso_fee.try(:commission)),amount_in_dollars, fee_amount, profit[:iso_base])
          else
            return nil
          end
        end
      end
    end

    private

    def deduct_fee(fee, amount)
      amount * (fee/100)
    end

    #------------Functions for dividing monthly fee start------------------#
    def get_monthly_fee_profit(iso_fee, merchant_fee, fee_type)
      if iso_fee.present? && merchant_fee.present?
        case fee_type
        when TypesEnumLib::TransactionSubType::Service
          profit_service = merchant_fee.service.to_f - iso_fee.service.to_f
          {
              profit_service: profit_service,
              iso_service: iso_fee.service.to_f,
              merchant_service: merchant_fee.service.to_f
          }
        when TypesEnumLib::TransactionSubType::Statement
          profit_statement = merchant_fee.statement.to_f - iso_fee.statement.to_f
          {
              profit_statement: profit_statement,
              iso_statement: iso_fee.statement.to_f,
              merchant_statment: merchant_fee.statement.to_f
          }
        when TypesEnumLib::TransactionSubType::Misc
          profit_misc = merchant_fee.misc.to_f - iso_fee.misc.to_f
          {
              profit_misc: profit_misc,
              iso_misc: iso_fee.misc.to_f,
              merchant_misc: merchant_fee.misc.to_f
          }
        end
      end
    end

    def calculate_monthly_profit(commissions, profit)
      current_tier = 1
      if commissions.present?
        if @location.risk.present?
          if @location.high?
            #---------------Update batch ---------------------------
            close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission, TypesEnumLib::Batch::High)
            # -------------------------Identify Current Level--------------------------------
            batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::High, nil,false)
          elsif @location.dispensary?
            #---------------Update batch ---------------------------
            close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission,TypesEnumLib::Batch::Low)
            # -------------------------Identify Current Level--------------------------------
            batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::Low,nil, false)
          end
        else
          # TODO remove after success full working of location risk all around system
          if @receiver.high?
            #---------------Update batch ---------------------------
            close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission, TypesEnumLib::Batch::High)
            # -------------------------Identify Current Level--------------------------------
            batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::High, nil,false)
          elsif @receiver.low?
            #---------------Update batch ---------------------------
            close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission,TypesEnumLib::Batch::Low)
            # -------------------------Identify Current Level--------------------------------
            batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::Low,nil, false)
          end
        end
        commission_tiers = commissions["commission"]
        if commission_tiers.present?
          result_tier = get_current_tier(commissions,0,batch)
          gbox_profit = result_tier[:gbox_profit]
          split_monthly_profit(profit, gbox_profit)
        else
          split_monthly_profit(profit, 0)
        end
      else
        split_monthly_profit(profit, 0)
      end
    end

    def split_monthly_profit(base_amounts, gbox_profit)
      iso_buyrate = false
      gbox_service_profit = 0
      iso_service_profit = 0
      gbox_statement_profit = 0
      iso_statement_profit = 0
      gbox_misc_profit = 0
      iso_misc_profit = 0
      agent_service_profit = 0
      agent_statement_profit = 0
      agent_misc_profit = 0
      affiliate_service_profit = 0
      affiliate_statement_profit = 0
      affiliate_misc_profit = 0
      total_amount = 0
      if base_amounts[:profit_service].present?
        total_amount = base_amounts[:merchant_service].to_f
        if base_amounts[:iso_service].to_f > 0 && base_amounts[:iso_service].to_f < total_amount.to_f
          gbox_service_profit = (base_amounts[:profit_service].to_f * (gbox_profit.to_f/100))
          iso_service_profit = base_amounts[:profit_service].to_f - gbox_service_profit.to_f if gbox_service_profit.to_f > 0
          gbox_service_profit = gbox_service_profit + base_amounts[:iso_service].to_f
        elsif base_amounts[:iso_service].to_f == 0 || base_amounts[:iso_service].to_f == total_amount.to_f
          gbox_service_profit = total_amount.to_f
        elsif base_amounts[:iso_service].to_f > total_amount.to_f
          iso_service_profit = base_amounts[:iso_service].to_f - total_amount.to_f
          gbox_service_profit = total_amount.to_f
          iso_buyrate = true
        end
      end
      if base_amounts[:profit_statement].present?
        total_amount = base_amounts[:merchant_statment].to_f
        if base_amounts[:iso_statement].to_f > 0 && base_amounts[:iso_statement].to_f < total_amount.to_f
          gbox_statement_profit = (base_amounts[:profit_statement].to_f * (gbox_profit.to_f/100))
          iso_statement_profit = base_amounts[:profit_statement].to_f - gbox_statement_profit.to_f if gbox_statement_profit.to_f > 0
          gbox_statement_profit = gbox_statement_profit + base_amounts[:iso_statement].to_f
        elsif base_amounts[:iso_statement].to_f == 0 || base_amounts[:iso_statement].to_f == total_amount.to_f
          gbox_statement_profit = total_amount.to_f
        elsif base_amounts[:iso_statement].to_f > total_amount.to_f
          iso_statement_profit = base_amounts[:iso_statement].to_f - total_amount.to_f
          gbox_statement_profit = total_amount.to_f
          iso_buyrate = true
        end
      end
      if base_amounts[:profit_misc].present?
        total_amount = base_amounts[:merchant_misc].to_f
        if base_amounts[:iso_misc].to_f > 0 && base_amounts[:iso_misc].to_f < total_amount.to_f
          gbox_misc_profit = (base_amounts[:profit_misc].to_f * (gbox_profit.to_f/100))
          iso_misc_profit = base_amounts[:profit_misc].to_f - gbox_misc_profit.to_f if gbox_misc_profit.to_f > 0
          gbox_misc_profit = gbox_misc_profit + base_amounts[:iso_misc].to_f
        elsif base_amounts[:iso_misc].to_f == 0 || base_amounts[:iso_misc].to_f == total_amount.to_f
          gbox_misc_profit = total_amount.to_f
        elsif base_amounts[:iso_misc].to_f > total_amount.to_f
          iso_misc_profit = base_amounts[:iso_misc].to_f - total_amount.to_f
          gbox_misc_profit = total_amount.to_f
          iso_buyrate = true
        end
      end
      if @agent.present? && iso_buyrate == false
        agent_fee = @agent.fees.buy_rate.high.first
        if agent_fee.present?
          agent_service_profit = (agent_fee.service.to_f/100) * iso_service_profit.to_f
          agent_statement_profit = (agent_fee.statement.to_f/100) * iso_statement_profit.to_f
          agent_misc_profit = (agent_fee.misc.to_f/100) * iso_misc_profit.to_f
        end
        if @affiliate.present?
          affiliate_fee = @affiliate.fees.buy_rate.high.first
          if affiliate_fee.present?
            affiliate_service_profit = (affiliate_fee.service.to_f/100) * agent_service_profit.to_f
            affiliate_statement_profit = (affiliate_fee.statement.to_f/100) * agent_statement_profit.to_f
            affiliate_misc_profit = (affiliate_fee.misc.to_f/100) * agent_misc_profit.to_f
          end
        end
      end
      # if gbox_service_profit.to_f > 0 && iso_buyrate == false
      #   gbox_service_profit = gbox_service_profit.to_f + base_amounts[:iso_service].to_f
      # # else
      # #   gbox_service_profit = base_amounts[:merchant_service]
      # end

      # if gbox_misc_profit.to_f > 0
      #   gbox_misc_profit = gbox_misc_profit.to_f + base_amounts[:iso_misc].to_f
      # else
      #   gbox_misc_profit = base_amounts[:merchant_misc]
      # end
      # if gbox_statement_profit.to_f > 0
      #   gbox_statement_profit = gbox_statement_profit.to_f  +  base_amounts[:iso_statement].to_f
      # else
      #   gbox_statement_profit = base_amounts[:merchant_statment]
      # end
      {
          "amount" => number_with_precision(total_amount.to_f, precision: 2).to_f,
          "fee" => 0,
          "splits" => {
              "gbox" => {
                  "service" => number_with_precision(gbox_service_profit.to_f, precision: 2).to_f,
                  "misc" => number_with_precision(gbox_misc_profit.to_f, precision: 2).to_f,
                  "statement" => number_with_precision(gbox_statement_profit.to_f, precision: 2).to_f,
                  "wallet" => @qc_wallet,
                  "merchant_wallet" => @location_wallet
              },
              "iso" => {
                  "service" => number_with_precision(iso_service_profit.to_f, precision: 2).to_f,
                  "misc" => number_with_precision(iso_misc_profit.to_f, precision: 2).to_f,
                  "statement" => number_with_precision(iso_statement_profit.to_f, precision: 2).to_f,
                  "wallet" => @iso.present? ? @iso_wallet.id : nil,
                  "name" => @iso.present? ? @iso.name : nil,
                  "email" => @iso.present? ? @iso.email : nil,
                  "starting_balance" => @iso_balance.to_f,
                  "iso_buyrate" => iso_buyrate,
                  "merchant_wallet" => @location_wallet
              },
              "agent" => {
                  "service" => number_with_precision(agent_service_profit.to_f, precision: 2).to_f,
                  "misc" => number_with_precision(agent_misc_profit.to_f, precision: 2).to_f,
                  "statement" => number_with_precision(agent_statement_profit.to_f, precision: 2).to_f,
                  "wallet" => @agent.present? ? @agent_wallet.id : nil,
                  "name" => @agent.present? ? @agent.name : nil,
                  "email" => @agent.present? ? @agent.email : nil,
                  "starting_balance" => @agent_balance.to_f,
                  "merchant_wallet" => @location_wallet
              },
              "affiliate" => {
                  "service" => number_with_precision(affiliate_service_profit.to_f, precision: 2).to_f,
                  "misc" => number_with_precision(affiliate_misc_profit.to_f, precision: 2).to_f,
                  "statement" => number_with_precision(affiliate_statement_profit.to_f, precision: 2).to_f,
                  "wallet" => @affiliate.present? ? @affiliate_wallet.id : nil,
                  "name" => @affiliate.present? ? @affiliate.name : nil,
                  "email" => @affiliate.present? ? @affiliate.email : nil,
                  "starting_balance" => @affiliate_balance.to_f,
                  "merchant_wallet" => @location_wallet
              }
          }.reject{|k,v| v.nil? || v == "" || v == {}}
      }
    end
    #------------Functions for dividing monthly fee  end------------------#

    def get_profit(fee, fee_type, fee_amount, amount, count = nil)
      case fee_type
      when TypesEnumLib::CommissionType::TransactionFeeCcard
        return compare_fees(fee_amount,amount,fee.try(:transaction_fee_ccard),fee.try(:transaction_fee_ccard_dollar), @fee.try(:transaction_fee_ccard), @fee.try(:transaction_fee_ccard_dollar))
      when TypesEnumLib::CommissionType::TransactionFeeDcard
        return compare_fees(fee_amount,amount,fee.try(:transaction_fee_dcard),fee.try(:transaction_fee_dcard_dollar), @fee.try(:transaction_fee_dcard), @fee.try(:transaction_fee_dcard_dollar))
      when TypesEnumLib::CommissionType::TransactionFeeInvoicecard
        return compare_fees(fee_amount,amount,fee.try(:invoice_card_fee_perc),fee.try(:invoice_card_fee), @fee.try(:invoice_card_fee_perc), @fee.try(:invoice_card_fee))
      when TypesEnumLib::CommissionType::TransactionFeeInvoiceDebitcard
        return compare_fees(fee_amount,amount,fee.try(:invoice_debit_card_fee_perc),fee.try(:invoice_debit_card_fee), @fee.try(:invoice_debit_card_fee_perc), @fee.try(:invoice_debit_card_fee))
      when TypesEnumLib::CommissionType::TransactionFeeInvoiceRTP
        return compare_fees(fee_amount,amount,fee.try(:invoice_rtp_fee_perc),fee.try(:invoice_rtp_fee), @fee.try(:invoice_rtp_fee_perc), @fee.try(:invoice_rtp_fee))
      when TypesEnumLib::CommissionType::TransactionFeeRTP
        return compare_fees(fee_amount,amount,fee.try(:rtp_fee_perc),fee.try(:rtp_fee), @fee.try(:rtp_fee_perc), @fee.try(:rtp_fee))
      when TypesEnumLib::CommissionType::TransactionFeeApp
        return compare_fees(fee_amount,amount,fee.try(:transaction_fee_app),fee.try(:transaction_fee_app_dollar), @fee.try(:transaction_fee_app), @fee.try(:transaction_fee_app_dollar))
      when TypesEnumLib::CommissionType::B2B
        return compare_fees(fee_amount,amount,fee.try(:b2b_fee),nil,@fee.try(:b2b_fee))
      when TypesEnumLib::CommissionType::SendCheck
        return compare_fees(fee_amount,amount,fee.try(:redeem_fee),fee.try(:send_check), @fee.try(:redeem_fee), @fee.try(:send_check), count)
      when TypesEnumLib::CommissionType::DebitCardDeposit
        return compare_fees(fee_amount,amount,fee.try(:dc_deposit_fee),fee.try(:dc_deposit_fee_dollar), @fee.try(:dc_deposit_fee), @fee.try(:dc_deposit_fee_dollar), count)
      when TypesEnumLib::CommissionType::Failedinstant_pay
        return compare_fees(fee_amount,amount,fee.try(:failed_push_to_card_percent_fee),fee.try(:failed_push_to_card_fee), @fee.try(:failed_push_to_card_percent_fee), @fee.try(:failed_push_to_card_fee), count)
      when TypesEnumLib::CommissionType::Failedcheck
        return compare_fees(fee_amount,amount,fee.try(:failed_check_percent_fee),fee.try(:failed_check_fee), @fee.try(:failed_check_percent_fee), @fee.try(:failed_check_fee), count)
      when TypesEnumLib::CommissionType::Failedinstant_ach
        return compare_fees(fee_amount,amount,fee.try(:failed_ach_percent_fee),fee.try(:failed_ach_fee), @fee.try(:failed_ach_percent_fee), @fee.try(:failed_ach_fee), count)
      when TypesEnumLib::CommissionType::ACH
        return compare_fees(fee_amount,amount,fee.try(:ach_fee),fee.try(:ach_fee_dollar), @fee.try(:ach_fee), @fee.try(:ach_fee_dollar))
      when TypesEnumLib::CommissionType::AddMoney
        return compare_fees(fee_amount,amount,fee.try(:redeem_check),fee.try(:add_money_check),@fee.try(:redeem_check), @fee.try(:add_money_check))
      when TypesEnumLib::CommissionType::Giftcard
        return compare_fees(fee_amount,amount,nil,fee.try(:giftcard_fee), nil,  @fee.try(:giftcard_fee))
      when TypesEnumLib::CommissionType::ChargeBack
        return compare_fees(fee_amount, amount, nil, fee.try(:charge_back_fee), nil, @fee.charge_back_fee)
      when TypesEnumLib::CommissionType::Retrieval
        return compare_fees(fee_amount, amount, nil, fee.try(:retrievel_fee), nil, @fee.retrievel_fee)
      when TypesEnumLib::TransactionType::RefundFee
        if @pin_debit_refund
          return compare_fees(fee_amount, amount, fee.try(:qr_refund_percentage), fee.try(:qr_refund), @fee.try(:qr_refund_percentage), @fee.try(:qr_refund))
        else
          return compare_fees(fee_amount, amount, nil, fee.try(:refund_fee), nil, @fee.try(:refund_fee))
        end
      end
    end

    def compare_fees(total_fee,total_amount,iso_percent_fee,iso_dollar_fee = nil, merchant_percent_fee = nil , merchant_dollar_fee = nil, count = nil)
      total_profit = 0
      total_iso_base = get_base_amount(iso_percent_fee, iso_dollar_fee, total_amount, count)
      if total_fee > 0
        if total_iso_base > 0
          if total_fee > total_iso_base
            total_profit = total_fee - total_iso_base
          end
        else
          total_base_amount = total_fee
          total_profit = total_fee
        end
      else
        total_base_amount = 0
      end
      {base_amount: total_base_amount, total_profit: total_profit, iso_base: total_iso_base}
    end

    def get_base_amount(iso_percent, iso_dollar, amount, count = nil)
      percent_base_amount = 0
      if iso_percent.present? && iso_percent.to_f > 0 #&& merchant_percent.to_f > 0 && merchant_percent.to_f >= iso_percent.to_f
        percent_base_amount = (iso_percent.to_f/100) * amount
        total_base_amount = percent_base_amount
      end
      if iso_dollar.present? && iso_dollar.to_f > 0 #&& merchant_dollar.to_f > 0 && merchant_dollar.to_f >= iso_dollar.to_f
        dollar_base_amount = iso_dollar.to_f * (count || 1)
        total_base_amount = percent_base_amount.to_f + dollar_base_amount.to_f
      end
      total_base_amount || 0
    end

    def calculate_profit_percentage(fee, profit, commissions, amount, fee_amount, iso_base)
      current_tier = 1
      commission_tiers = commissions["commission"] if commissions.present?
      if profit.to_f > 0
        if commission_tiers.present?
          iso_profit = 0
          gbox_profit = 0
          if @location.risk.present?
            if @location.high?
              #---------------Update batch ---------------------------
              close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission, TypesEnumLib::Batch::High)
              # -------------------------Identify Current Level--------------------------------
              batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::High, nil,false)
            elsif @location.dispensary?
              #---------------Update batch ---------------------------
              close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission,TypesEnumLib::Batch::Low)
              # -------------------------Identify Current Level--------------------------------
              batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::Low,nil, false)
            end
          else
            if @receiver.high?
              #---------------Update batch ---------------------------
              close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission, TypesEnumLib::Batch::High)
              # -------------------------Identify Current Level--------------------------------
              batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::High, nil,false)
            elsif @receiver.low?
              #---------------Update batch ---------------------------
              close_batches(@iso.id, @iso_wallet.id, TypesEnumLib::Batch::BuyRateCommission,TypesEnumLib::Batch::Low)
              # -------------------------Identify Current Level--------------------------------
              batch = get_batch(@iso.id,@iso_wallet.id,TypesEnumLib::Batch::BuyRateCommission,nil,nil, current_tier, TypesEnumLib::Batch::Low,nil, false)
            end
          end
          result_tier = get_current_tier(commissions,amount,batch)
          current_tier = result_tier[:current_tier]
          iso_profit = result_tier[:iso_profit]
          gbox_profit = result_tier[:gbox_profit]
          # ---------------------Bonus From Last Tier's Max Amount---------------------------
          total_bonus = 0
          if current_tier != batch.current_tier && ([TypesEnumLib::CommissionType::TransactionFeeCcard, TypesEnumLib::CommissionType::TransactionFeeDcard, TypesEnumLib::CommissionType::TransactionFeeApp, TypesEnumLib::CommissionType::TransactionFeeInvoicecard, TypesEnumLib::CommissionType::TransactionFeeInvoiceDebitcard, TypesEnumLib::CommissionType::TransactionFeeInvoiceRTP, TypesEnumLib::CommissionType::TransactionFeeRTP].include?(@current_fee_type))
            batch.update(current_tier: current_tier)
            last_max_amount = batch.total_fee.to_f
            bonus_percent = commission_tiers.try(:[],"tier#{current_tier}").try(:[],"commission_iso").to_f - commission_tiers.try(:[],"tier#{current_tier-1}").try(:[],"commission_iso").to_f
            total_bonus = (last_max_amount.to_f/100) * bonus_percent
          end
          iso_let_profit = batch.total_fee.to_f + profit.to_f
          if iso_let_profit.to_f > 0 && ([TypesEnumLib::CommissionType::TransactionFeeCcard, TypesEnumLib::CommissionType::TransactionFeeDcard, TypesEnumLib::CommissionType::TransactionFeeApp, TypesEnumLib::CommissionType::TransactionFeeInvoicecard, TypesEnumLib::CommissionType::TransactionFeeInvoiceDebitcard, TypesEnumLib::CommissionType::TransactionFeeInvoiceRTP, TypesEnumLib::CommissionType::TransactionFeeRTP].include?(@current_fee_type))
            batch.update(total_fee: iso_let_profit)
          end
          split_profit(fee,profit,iso_profit,gbox_profit,amount, fee_amount,current_tier, total_bonus, iso_base)
        else
          split_profit(fee,0,0,0,amount, fee_amount,current_tier, 0, iso_base)
        end
      else
        split_profit(fee,0,0,0,amount, fee_amount,current_tier, 0, iso_base)
      end
    end

    def get_current_tier(commissions,amount,batch)
      current_tier = 1
      total_amount = batch.total_sales.to_f + amount.to_f
      commission_tiers = commissions["commission"] if commissions.present?
      # ------------------------Calculate Commission Logic------------------------------
      # Identify Last Level

      if total_amount >= commission_tiers["tier#{commission_tiers.length}"]["sales_level_min"].to_f
        current_tier = commission_tiers.length
        iso_profit = commission_tiers["tier#{commission_tiers.length}"]["commission_iso"].to_f
        gbox_profit = commission_tiers["tier#{commission_tiers.length}"]["commission_merchant"].to_f
      else
        # Identify Other Levels
        for i in (1...commission_tiers.length+1)
          if total_amount >= commission_tiers["tier#{i}"]["sales_level_min"].to_f && total_amount<=commission_tiers["tier#{i}"]["sales_level_max"].to_f
            current_tier = i
            iso_profit = commission_tiers["tier#{i}"]["commission_iso"].to_f
            gbox_profit = commission_tiers["tier#{i}"]["commission_merchant"].to_f
          end
        end
      end
      return {current_tier: current_tier, iso_profit: iso_profit, gbox_profit: gbox_profit}
    end

    def split_profit(gbox_share,profit,iso_profit,gbox_profit,amount_in_dollars, fee_amount,current_tier, total_bonus, iso_base)
      # ---------------------------Calculate & Split Profit if Profit Present---------------------------------
      if profit > 0
        gbox_profit = (gbox_profit.to_f/100)*profit
        iso_profit = (iso_profit.to_f/100)*profit
        agent_profit = 0
        affiliate_profit = 0
        if @agent.present?
          agent_fee = @agent.fees.buy_rate.high.first
          if agent_fee.present?
            agent_amount = get_agent_fee(agent_fee)
            agent_profit = (agent_amount[:percent_fee].to_f/100) * iso_profit
          end
          if @affiliate.present?
            affiliate_fee = @affiliate.fees.buy_rate.first
            if affiliate_fee.present?
              affiliate_amount = get_agent_fee(affiliate_fee)
              affiliate_profit = (affiliate_amount[:percent_fee].to_f/100) * agent_profit
            end
          end
          # total_agent_profit = agent_profit - affiliate_profit
        end
        totalFeeInDollars = number_with_precision(fee_amount, precision: 2).to_f
        isoProfitIndollars = number_with_precision(iso_profit, precision: 2).to_f

        #= Calculate profits for super users
        @super_users_details = []
        if @type == TypesEnumLib::CommissionType::QrCard || @type == TypesEnumLib::CommissionType::C2B
          #---- calculate if sale type transaction
          if @location.baked_iso && @location.profit_split_detail.present?
            if @profit_detail["share_split"] == "gbox"
              @from_wallet = @qc_wallet
            elsif @profit_detail["share_split"] == "iso"
              @from_wallet = @iso_wallet.id
            end
            @super_users_details = getting_baked_super_users_details(@super_users,amount_in_dollars,@from_wallet)
          end
          #= to get super iso from main iso if super iso in merchant is not present
          if @iso.fees.present? && @super_users_details.blank?
            if @location.risk.present?
              if @location.high?
                iso_fee = @iso.fees.buy_rate.high.first
              elsif @location.dispensary?
                iso_fee = @iso.fees.buy_rate.low.first
              end
            else
              if @receiver.high?
                iso_fee = @iso.fees.buy_rate.high.first
              elsif @receiver.low?
                iso_fee = @iso.fees.buy_rate.low.first
              end
            end
            if iso_fee.present?
              iso_splits = JSON.parse(iso_fee.splits) if iso_fee.splits.present?
              if iso_splits.present?
                baked_isos = []
                if iso_splits["share_split"] == "gbox"
                  from_wallet = @qc_wallet
                elsif iso_splits["share_split"] == "iso"
                  from_wallet = @iso_wallet.id
                end
                iso_splits["splits"].each do|key,value|
                  baked_isos.push(value)
                end
                @super_users_details = getting_baked_super_users_details(baked_isos,amount_in_dollars,from_wallet)

                # super_iso_percent = amount_in_dollars.to_f * (iso_splits["splits"]["super_iso"]["percent"].to_f/100)
                # super_iso_dollar = iso_splits["splits"]["super_iso"]["dollar"].to_f
                # super_iso = User.find(iso_splits["splits"]["super_iso"]["user"])
                # super_iso_details = {
                #     "percent" => super_iso.present? ? number_with_precision(super_iso_percent, precision: 2) : 0,
                #     "dollar" => super_iso.present? ? super_iso_dollar : 0,
                #     "wallet" => super_iso.present? ? super_iso.wallets.primary.first.id : nil,
                #     "email" => super_iso.present? ? super_iso.email : nil,
                #     "name" => super_iso.present? ? super_iso.name : nil,
                #     "from_wallet" => from_wallet
                # }
              end
            end
          end

          #= Super company
          if @location.profit_split && @location.profit_split_detail.present?
            company_user = {}
            if @super_company.present?
              if @super_company["from"].present?
                if @super_company["from"] == "gbox"
                  from = @qc_wallet
                elsif @super_company["from"] == "iso"
                  from = @iso_wallet.id
                end
              else
                from = @qc_wallet
              end

              @super_company.except("from").each do |key,split|
                super_company_value = split
                if super_company_value["wallet"].present? && (super_company_value["percent"].to_f > 0 || super_company_value["dollar"].to_f > 0)
                  amount = @super_company["from"] == "gbox" ? (totalFeeInDollars - isoProfitIndollars) : (gbox_profit.to_f + gbox_share.to_f)
                  company_amount = amount * (super_company_value["percent"].present? ? super_company_value["percent"].to_f/100 : 0)
                  company_amount = company_amount.to_f + super_company_value.try(:[], "dollar").to_f
                  if super_company_value["role"].downcase == "merchants"
                    merchant_location = super_company_value["wallet"].present? ? Location.find_by(id: super_company_value["wallet"].to_i) : nil
                    super_wallet = merchant_location.try(:wallets).try(:primary).try(:first).try(:id)
                    company_user = company_user.merge(key => {
                        "wallet" => super_wallet.present? ? super_wallet  : nil ,
                        "amount" => number_with_precision(company_amount, precision: 2),
                        "email" => super_wallet.present? ? merchant_location.try(:merchant).try(:email) : nil ,
                        "name" => super_company_value.present? ? super_company_value.try(:[],"name") : nil,
                        "from" => from || nil,
                        "role" => "merchant"
                    })
                  else
                    user_company = super_company_value["wallet"].present? ? User.find_by(id: super_company_value["wallet"].to_i) : nil
                    company_user = company_user.merge(key => {
                        "wallet" => super_company_value.present? ? user_company.try(:wallets).try(:primary).try(:first).try(:id) : 0,
                        "amount" => number_with_precision(company_amount, precision: 2),
                        "email" => super_company_value.present? ? user_company.try(:email) : nil,
                        "name" => super_company_value.present? ? user_company.try(:name) : nil,
                        "from" => from || nil,
                        "role" => user_company.try(:role)
                    })
                  end
                end
              end
            end
          end
          #= End Super Company
          #
          #= New Net Super Company
          if @location.net_profit_split && @location.profit_split_detail.present?
            if @net_super_company.present?
              net_super_company_value = @net_super_company.first.values.first
              net_super_company_key = @net_super_company.first.keys.first
              net_company_amount = ( gbox_profit.to_f + (totalFeeInDollars - (gbox_profit.to_f + isoProfitIndollars))  )
              net_company_amount = cal_gateway_fees(amount_in_dollars, net_company_amount,net_super_company_value["percent"].present? ? net_super_company_value["percent"].to_f : 0)
              from = @qc_wallet

              if net_super_company_key.include? TypesEnumLib::Users::Merchant
                net_merchant_location = net_super_company_value["wallet"].present? ? Location.find_by(id: net_super_company_value["wallet"].to_i) : nil
                net_company_user = {
                    "wallet" => net_super_company_value.present? ?  net_merchant_location.try(:wallets).try(:primary).try(:first).try(:id) : 0 ,
                    "amount" => number_with_precision(net_company_amount, precision: 2),
                    "email" => net_super_company_value.present? ? net_merchant_location.try(:wallets).try(:primary).try(:first).try(:users).try(:email) : nil ,
                    "name" => net_super_company_value.present? ? net_super_company_value.first(2).last.last : nil,
                    # "name" => net_super_company_value.present? ? net_super_company_value.first["name"] : nil,
                    "from" => from || nil,
                    "role" => "merchant"
                }
              else
                net_user_company = net_super_company_value["wallet"].present? ? User.find_by(id: net_super_company_value["wallet"].to_i) : nil
                net_company_user = {
                    "wallet" => net_super_company_value.present? ? net_user_company.try(:wallets).try(:primary).try(:first).try(:id) : 0,
                    "amount" => number_with_precision(net_company_amount, precision: 2),
                    "email" => net_super_company_value.present? ? net_user_company.try(:email) : nil,
                    "name" => net_super_company_value.present? ? net_user_company.try(:name) : nil,
                    "from" => from || nil,
                    "role" => net_user_company.try(:role)
                }
              end
            end
          end
          #= End Net Super Company
        end

        #= Get profit split for sub merchant
        if @type == TypesEnumLib::CommissionType::QrCard || @type == TypesEnumLib::CommissionType::C2B
          if @location.sub_merchant_split && @location.profit_split_detail.present?
            sub_merchant_details = {}
            ez_splits = @profit_detail["splits"]["ez_merchant"]
            if ez_splits.present?
              ez_splits.each do |key, value|
                location_new = Location.find_by(id: value["ez_merchant_1"]["wallet"])
                wallet = location_new.wallets.primary.first if location_new.present?
                if wallet.present?
                  percent = amount_in_dollars.to_f * value["ez_merchant_1"]["percent"].to_f/100
                  sub_merchant_details.merge!({
                                                  key => {
                                                      "percent" => percent,
                                                      "dollar" => value["ez_merchant_1"]["dollar"].to_f,
                                                      "wallet" => wallet.try(:id),
                                                      "name" => value["ez_merchant_1"]["name"]
                                                  }
                                              })
                end
              end
            end
          end
        end
        #= end sub merchant split

        #= Fee Details Hash ---------------
        fee_details = {
            "amount" => amount_in_dollars,
            "fee" => totalFeeInDollars,
            "current_tier" => current_tier,
            "splits" => {
                "total_bonus" => total_bonus,
                "gbox" => {
                    "amount" => number_with_precision(totalFeeInDollars - isoProfitIndollars, precision: 2),
                    "wallet" => @qc_wallet,
                    "merchant_wallet" => @location_wallet
                },
                "iso" => {
                    "amount" => isoProfitIndollars,
                    "wallet" => @iso.present? ? @iso_wallet.id : nil,
                    "name" => @iso.present? ? @iso.name : nil,
                    "email" => @iso.present? ? @iso.email : nil,
                    "starting_balance" => @iso_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "agent" => {
                    "amount" => number_with_precision(agent_profit, precision: 2).to_f,
                    "wallet" => @agent.present? ? @agent_wallet.id : nil,
                    "name" => @agent.present? ? @agent.name : nil,
                    "email" => @agent.present? ? @agent.email : nil,
                    "starting_balance" => @agent_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "affiliate" => {
                    "amount" => number_with_precision(affiliate_profit, precision: 2).to_f,
                    "wallet" => @affiliate.present? ? @affiliate_wallet.id : nil,
                    "name" => @affiliate.present? ? @affiliate.name : nil,
                    "email" => @affiliate.present? ? @affiliate.email : nil,
                    "starting_balance" => @affiliate_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "super_users_details" => @super_users_details || nil,
                "super_company" => company_user || nil,
                "net_super_company" => net_company_user || nil,
                "sub_merchant_details" => sub_merchant_details || nil
            }.reject{|k,v| v.nil? || v == "" || v == {}}
        }
      elsif fee_amount.to_f == 0 || fee_amount.to_f < iso_base.to_f
        if fee_amount.to_f == 0
          iso_amount = iso_base.to_f
        elsif fee_amount.to_f < iso_base.to_f
          iso_amount = iso_base.to_f - fee_amount.to_f
        end
        #= Get profit split for sub merchant
        if @type == TypesEnumLib::CommissionType::QrCard || @type == TypesEnumLib::CommissionType::C2B
          if @location.sub_merchant_split && @location.profit_split_detail.present?
            sub_merchant_details = {}
            ez_splits = @profit_detail["splits"]["ez_merchant"]
            if ez_splits.present?
              ez_splits.each do |key, value|
                location_new = Location.find_by(id: value["ez_merchant_1"]["wallet"])
                wallet = location_new.wallets.primary.first if location_new.present?
                if wallet.present?
                  percent = amount_in_dollars.to_f * value["ez_merchant_1"]["percent"].to_f/100
                  sub_merchant_details.merge!({
                                                  key => {
                                                      "percent" => percent,
                                                      "dollar" => value["ez_merchant_1"]["dollar"].to_f,
                                                      "wallet" => wallet.try(:id),
                                                      "name" => value["ez_merchant_1"]["name"]
                                                  }
                                              })
                end
              end
            end
          end
        end
        #= end sub merchant split
        fee_details = {
            "amount" => amount_in_dollars,
            "fee" => fee_amount.to_f,
            "splits" => {
                "total_bonus" => total_bonus,
                "gbox" => {
                    "amount" => fee_amount.to_f,
                    "wallet" => @qc_wallet
                },
                "iso" => {
                    "amount" => number_with_precision(iso_amount, precision: 2).to_f,
                    "wallet" => @iso.present? ? @iso_wallet.id : '',
                    "name" => @iso.present? ? @iso.name : '',
                    "email" => @iso.present? ? @iso.email : '',
                    "iso_buyrate" => true,
                    "starting_balance" => @iso_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "agent" => {
                    "amount" => 0,
                    "wallet" => @agent.present? ? @agent_wallet.id : '',
                    "name" => @agent.present? ? @agent.name : '',
                    "email" => @agent.present? ? @agent.email : '',
                    "starting_balance" => @agent_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "affiliate" => {
                    "amount" => 0,
                    "wallet" => @affiliate.present? ? @affiliate_wallet.id : '',
                    "name" => @affiliate.present? ? @affiliate.name : '',
                    "email" => @affiliate.present? ? @affiliate.email : '',
                    "starting_balance" => @affiliate_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "sub_merchant_details" => sub_merchant_details || nil
            }.reject{|k,v| v.nil? || v == "" || v == {}}
        }
      else
        # ----------------------------------Return Base Amount If no Profit Present---------------------
        # ------------------------Fee Details Hash-----------------------------------------
        if fee_amount.to_f == iso_base.to_f || gbox_share.to_f == 0
          gbox_share = fee_amount
        end
        #= Get profit split for sub merchant
        if @type == TypesEnumLib::CommissionType::QrCard || @type == TypesEnumLib::CommissionType::C2B
          if @location.sub_merchant_split && @location.profit_split_detail.present?
            sub_merchant_details = {}
            ez_splits = @profit_detail["splits"]["ez_merchant"]
            if ez_splits.present?
              ez_splits.each do |key, value|
                location_new = Location.find_by(id: value["ez_merchant_1"]["wallet"])
                wallet = location_new.wallets.primary.first if location_new.present?
                if wallet.present?
                  percent = amount_in_dollars.to_f * value["ez_merchant_1"]["percent"].to_f/100
                  sub_merchant_details.merge!({
                                                  key => {
                                                      "percent" => percent,
                                                      "dollar" => value["ez_merchant_1"]["dollar"].to_f,
                                                      "wallet" => wallet.try(:id),
                                                      "name" => value["ez_merchant_1"]["name"]
                                                  }
                                              })
                end
              end
            end
          end
        end
        #= end sub merchant split
        fee_details = {
            "amount" => amount_in_dollars,
            "fee" => number_with_precision(fee_amount, precision: 2).to_f,
            "current_tier" => current_tier,
            "splits" => {
                "total_bonus" => total_bonus,
                "gbox" => {
                    "amount" => number_with_precision(gbox_share.to_f, precision: 2).to_f,
                    "wallet" => @qc_wallet,
                    "merchant_wallet" => @location_wallet
                },
                "iso" => {
                    "amount" => 0,
                    "wallet" => @iso.present? ? @iso_wallet.id : '',
                    "name" => @iso.present? ? @iso.name : '',
                    "email" => @iso.present? ? @iso.email : '',
                    "starting_balance" => @iso_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "agent" => {
                    "amount" => 0,
                    "wallet" => @agent.present? ? @agent_wallet.id : '',
                    "name" => @agent.present? ? @agent.name : '',
                    "email" => @agent.present? ? @agent.email : '',
                    "starting_balance" => @agent_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "affiliate" => {
                    "amount" => 0,
                    "wallet" => @affiliate.present? ? @affiliate_wallet.id : '',
                    "name" => @affiliate.present? ? @affiliate.name : '',
                    "email" => @affiliate.present? ? @affiliate.email : '',
                    "starting_balance" => @affiliate_balance.to_f,
                    "merchant_wallet" => @location_wallet
                },
                "sub_merchant_details" => sub_merchant_details || nil
            }.reject{|k,v| v.nil? || v == "" || v == {}}
        }
      end
    end

    def getting_baked_super_users_details(baked_users,amount_in_dollars,from_wallet)
      @super_baked_users = []
      baked_users.each do |user|
        user_1_percent = 0
        user_2_percent = 0
        temp_hash = {}
        user.each do |k,v|
          if v["wallet"].present?
            this_user = User.find_by(id: v["wallet"]) if v["wallet"].present?
          elsif v["user"].present?
            this_user = User.find_by(id: v["user"]) if v["user"].present?
          end
          if k == "user_1"
            @user_1_wallet = this_user.present? ? this_user.wallets.primary.first.id : nil
            user_1_percent = amount_in_dollars.to_f * (v["percent"].to_f/100)
            user_1_dollar = v["dollar"].to_f + user_1_percent
            user_1_details = {
                "percent" => user_1_percent.present? ? number_with_precision(user_1_percent, precision: 2) : 0,
                "dollar" => user_1_dollar.present? ? user_1_dollar : 0,
                "wallet" => @user_1_wallet,
                "email" => this_user.present? ? this_user.email : nil,
                "name" => this_user.present? ? this_user.name : nil,
                "from_wallet" => from_wallet,
                "role" => this_user.try(:role)
            }
            temp_hash = temp_hash.merge({user_1_details: user_1_details})
          elsif k == "user_2"
            @user_2_wallet = this_user.present? ? this_user.wallets.primary.first.id : nil
            user_2_percent = user_1_percent.to_f * (v["percent"].to_f/100)
            user_2_dollar = v["dollar"].to_f + user_2_percent
            user_2_details = {
                "percent" => user_2_percent.present? ? number_with_precision(user_2_percent, precision: 2) : 0,
                "dollar" => user_2_dollar.present? ? user_2_dollar : 0,
                "wallet" => @user_2_wallet,
                "email" => this_user.present? ? this_user.email : nil,
                "name" => this_user.present? ? this_user.name : nil,
                "from_wallet" =>  @user_1_wallet,
                "role" => this_user.try(:role)
            }
            temp_hash = temp_hash.merge({user_2_details: user_2_details})
          elsif k == "user_3"
            user_3_percent = user_2_percent.to_f * (v["percent"].to_f/100)
            user_3_dollar = v["dollar"].to_f + user_3_percent
            user_3_details = {
                "percent" => user_3_percent.present? ? number_with_precision(user_3_percent, precision: 2) : 0,
                "dollar" => user_3_dollar.present? ? user_3_dollar : 0,
                "wallet" => this_user.present? ? this_user.wallets.primary.first.id : nil,
                "email" => this_user.present? ? this_user.email : nil,
                "name" => this_user.present? ? this_user.name : nil,
                "from_wallet" =>  @user_2_wallet,
                "role" => this_user.try(:role)
            }
            temp_hash = temp_hash.merge({user_3_details: user_3_details})
          end
        end
        @super_baked_users.push(temp_hash)
      end
      return @super_baked_users
    end

    def cal_gateway_fees(total_amount, amount, net_super_company_percentage)
      # * (net_super_company_value["percent"].present? ? net_super_company_value["percent"].to_f/100 : 0)
      gateway_percent_trans = (total_amount) * (@payment_gateway.present? ? @payment_gateway.transaction_fee.to_f/100 : 0) #getting gateway perc
      gateway_percent_dollar_trans = gateway_percent_trans.to_f + (@payment_gateway.present? ? @payment_gateway.per_transaction_fee.to_f : 0)
      amount = amount - gateway_percent_dollar_trans
      return (amount.to_f * (net_super_company_percentage/100))
    end

    # ---------------------------Get Agent Fee-----------------------------------------
    def get_agent_fee(agent_fee)
      case @current_fee_type
      when TypesEnumLib::CommissionType::TransactionFeeApp
        return {percent_fee: agent_fee.transaction_fee_app.to_f}
      when TypesEnumLib::CommissionType::TransactionFeeDcard
        return {percent_fee: agent_fee.transaction_fee_dcard.to_f}
      when TypesEnumLib::CommissionType::TransactionFeeCcard
        return {percent_fee: agent_fee.transaction_fee_ccard.to_f}
      when TypesEnumLib::CommissionType::TransactionFeeInvoicecard
        return {percent_fee: agent_fee.invoice_card_fee_perc.to_f}
      when TypesEnumLib::CommissionType::TransactionFeeInvoiceDebitcard
        return {percent_fee: agent_fee.invoice_debit_card_fee_perc.to_f}
      when TypesEnumLib::CommissionType::TransactionFeeInvoiceRTP
        return {percent_fee: agent_fee.invoice_rtp_fee_perc.to_f}
      when TypesEnumLib::CommissionType::TransactionFeeRTP
        return {percent_fee: agent_fee.rtp_fee_perc.to_f}
      when TypesEnumLib::CommissionType::B2B
        return {percent_fee: agent_fee.b2b_fee.to_f}
      when TypesEnumLib::CommissionType::SendCheck
        return {percent_fee: agent_fee.redeem_fee.to_f}
      when TypesEnumLib::CommissionType::DebitCardDeposit
        return {percent_fee: agent_fee.dc_deposit_fee.to_f}
      when TypesEnumLib::CommissionType::Failedinstant_pay
        return {percent_fee: agent_fee.failed_push_to_card_percent_fee.to_f}
      when TypesEnumLib::CommissionType::Failedinstant_ach
        return {percent_fee: agent_fee.failed_ach_percent_fee.to_f}
      when TypesEnumLib::CommissionType::Failedcheck
        return {percent_fee: agent_fee.failed_check_percent_fee.to_f}
      when TypesEnumLib::CommissionType::ACH
        return {percent_fee: agent_fee.ach_fee.to_f}
      when TypesEnumLib::CommissionType::AddMoney
        return {percent_fee: agent_fee.redeem_check.to_f}
      when TypesEnumLib::CommissionType::Giftcard
        return {percent_fee: agent_fee.giftcard_fee.to_f}
      when TypesEnumLib::CommissionType::Retrieval
        return {percent_fee: agent_fee.retrievel_fee.to_f}
      when TypesEnumLib::CommissionType::ChargeBack
        return {percent_fee: agent_fee.charge_back_fee.to_f}
      when TypesEnumLib::TransactionType::RefundFee
        return {percent_fee: agent_fee.refund_fee.to_f}
      end
    end

    #-----------------GEt agent users--------------
    def get_users(user, profit_user, role)
      profit_user.each do |key, array|
        key_name = key.tr("0-9", "")
        if key_name != TypesEnumLib::Users::SuperIso
          if key_name == TypesEnumLib::Users::Merchant
            location = Location.find_by_id(array[:wallet])
            @merchant = location.merchant if location.present?
          elsif key_name == TypesEnumLib::Users::Agent
            if user.id == array[:wallet].to_i
              @super_agent = user
            end
            if user.id != array[:wallet].to_i
              @agent = user
            end
          elsif key_name == TypesEnumLib::Users::Affiliate
            if user.id == array[:wallet].to_i
              @super_affiliate = user
            end
            if user.id != array[:wallet].to_i
              @affiliate = user
            end
          end
        end
      end
    end

    #------------ calculating dispensary split fee ----------------#
    def dispensary_split(fee_type, amount_in_dollars, total_fee)
      dispensary_credit = {}
      dispensary_debit = {}
      gbox_fee = 0
      if total_fee.to_f > 0
        if fee_type == "credit"
          @dispensary_credit.try(:each) do |key, value|
            if value["wallet"].present?
              user = User.where(id: value["wallet"]).first #finding user because user id is stored in wallet key
              if user.present?
                percent = amount_in_dollars.to_f * (value["percent"].to_f/100) if value["percent"].to_f > 0
                total_amount = value["dollar"].to_f
                dispensary_credit[key] = {
                    "wallet" => user.wallets.primary.first.id,
                    "dollar" => number_with_precision(total_amount.to_f).to_f,
                    "percent" => number_with_precision(percent.to_f).to_f,
                    "name" => value["name"],
                    "email" => user.email
                }
              end
            end
          end
          if dispensary_credit.present?
            dollar = dispensary_credit.values.pluck("dollar").try(:sum, &:to_f)
            percent = dispensary_credit.values.pluck("percent").try(:sum, &:to_f)
            gbox_fee = total_fee.to_f - dollar.to_f - percent.to_f
          end
        elsif fee_type == "debit"
          @dispensary_debit.try(:each) do |key, value|
            if value["wallet"].present?
              user = User.where(id: value["wallet"]).first #finding user because user id is stored in wallet key
              if user.present?
                percent = amount_in_dollars.to_f * (value["percent"].to_f/100) if value["percent"].to_f > 0
                total_amount = value["dollar"].to_f
                dispensary_debit[key] = {
                    "wallet" => user.wallets.primary.first.id,
                    "dollar" => number_with_precision(total_amount.to_f).to_f,
                    "percent" => number_with_precision(percent.to_f).to_f,
                    "name" => value["name"],
                    "email" => user.email
                }
              end
            end
          end
          if dispensary_debit.present?
            dollar = dispensary_debit.values.pluck("dollar").try(:sum, &:to_f)
            percent = dispensary_debit.values.pluck("percent").try(:sum, &:to_f)
            gbox_fee = total_fee.to_f - dollar.to_f - percent.to_f
          end
        end
        #= New Net Super Company
        if @location.net_profit_split && @location.profit_split_detail.present?
          if @net_super_company.present?
            net_super_company_value = @net_super_company.first.values.first
            net_super_company_key = @net_super_company.first.keys.first
            net_company_amount = gbox_fee
            net_company_amount = cal_gateway_fees(amount_in_dollars, net_company_amount,net_super_company_value["percent"].present? ? net_super_company_value["percent"].to_f : 0)
            from = @qc_wallet

            if net_super_company_key.include? TypesEnumLib::Users::Merchant
              net_merchant_location = net_super_company_value["wallet"].present? ? Location.find_by(id: net_super_company_value["wallet"].to_i) : nil
              net_company_user = {
                  "wallet" => net_super_company_value.present? ?  net_merchant_location.try(:wallets).try(:primary).try(:first).try(:id) : 0 ,
                  "amount" => number_with_precision(net_company_amount, precision: 2),
                  "email" => net_super_company_value.present? ? net_merchant_location.try(:wallets).try(:primary).try(:first).try(:users).try(:email) : nil ,
                  "name" => net_super_company_value.present? ? net_super_company_value.first(2).last.last : nil,
                  # "name" => net_super_company_value.present? ? net_super_company_value.first["name"] : nil,
                  "from" => from || nil,
                  "role" => "merchant"
              }
            else
              net_user_company = net_super_company_value["wallet"].present? ? User.find_by(id: net_super_company_value["wallet"].to_i) : nil
              net_company_user = {
                  "wallet" => net_super_company_value.present? ? net_user_company.try(:wallets).try(:primary).try(:first).try(:id) : 0,
                  "amount" => number_with_precision(net_company_amount, precision: 2),
                  "email" => net_super_company_value.present? ? net_user_company.try(:email) : nil,
                  "name" => net_super_company_value.present? ? net_user_company.try(:name) : nil,
                  "from" => from || nil,
                  "role" => net_user_company.try(:role)
              }
            end
          end
        end
        #= End Net Super Company
      end
      fee_details = {
          "amount" => amount_in_dollars,
          "fee" => total_fee.to_f,
          "splits" => {
              "gbox" => {
                  "amount" => number_with_precision(gbox_fee.to_f).to_f,
                  "wallet" => @qc_wallet,
                  "merchant_wallet" => @location_wallet
              },
              "iso" => {
                  "amount" => 0,
                  "wallet" => @iso.present? ? @iso_wallet.id : '',
                  "name" => @iso.present? ? @iso.name : '',
                  "email" => @iso.present? ? @iso.email : '',
                  "starting_balance" => @iso_balance.to_f,
                  "merchant_wallet" => @location_wallet
              },
              "agent" => {
                  "amount" => 0,
                  "wallet" => @agent.present? ? @agent_wallet.id : '',
                  "name" => @agent.present? ? @agent.name : '',
                  "email" => @agent.present? ? @agent.email : '',
                  "starting_balance" => @agent_balance.to_f,
                  "merchant_wallet" => @location_wallet
              },
              "affiliate" => {
                  "amount" => 0,
                  "wallet" => @affiliate.present? ? @affiliate_wallet.id : '',
                  "name" => @affiliate.present? ? @affiliate.name : '',
                  "email" => @affiliate.present? ? @affiliate.email : '',
                  "starting_balance" => @affiliate_balance.to_f,
                  "merchant_wallet" => @location_wallet
              },
              "dispensary_credit_split" => dispensary_credit || nil,
              "dispensary_debit_split" => dispensary_debit || nil,
              "net_super_company" => net_company_user || nil
          }.reject{|k,v| v.nil? || v == "" || v == {}}
      }
    end
  end
end
