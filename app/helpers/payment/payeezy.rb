module Payment
  class PayeezyGateway < Gateway
    attr_accessor :ta_token
    self.live_url = "#{ENV['PAYEEZY_API_URL']}/transactions/tokens"

    def tokenize_card(card, options = {})
      begin
        params = {}
        params[:type] = 'FDToken'
        params[:auth] = false
        params[:ta_token] = ENV['TA_TOKEN']

        card = {}
        card[:type] = card.brand
        card[:cardholder_name] = card.name
        card[:card_number] = card.number
        card[:exp_date] = "#{card.expiry_date.month}/#{card.expiry_date.year}"
        card[:cvv] = card.cvv if cvv.present?

        params[:credit_card] = card
        body = params.to_json

        uri = URI.parse("#{ENV['PAYEEZY_API_URL']}/transactions/tokens")
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER

        request = Net::HTTP::Post.new(uri.request_uri)
        http_headers(request, body)
        request.body = body

        response = http.request(request)
        parsed_response = parse(response.body)
        if response.is_a?(Net::HTTPSuccess)
          if parsed_response['status'] == 'success'
            token = parsed_response['token']
            return token['value']
          else
            raise StandardError.new('Payeezy Card Tokenization Failed')
          end
        else
          response=JSON.parse(response.body)
          error=response["Error"]["messages"].first["description"]
          p "PAYEEZY RESPONSE BODY : ", error
          raise StandardError.new "#{error}"
          # raise StandardError.new("Payeezy #{parsed_response['Error']['messages'][0]['description']}")
        end
        return nil
      rescue => ex
        raise StandardError.new, ex.message
      end
    end

    def charge(card = nil, token = nil, amount)
      # ADD void code here...
    end

    def void(transaction_id, options = {})
      # ADD void code here...
    end

    def refund(transaction_id, amount, options = {})
      # ADD REFUND code here...
    end

    def verify(transaction_id, amount, options = {})
      # ADD VERIFY code here...
    end

    private

    def capture(token, amount)
      # ...
    end
  end
end
