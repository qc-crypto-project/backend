module Payment
  class StripeGateway < Gateway
  include ApplicationHelper

    def tokenize_card(card, options = {})
      token = Stripe::Token.create(
        :card => {
            :number => card.number,
            :exp_month => card.expiry_date.month,
            :exp_year => card.expiry_date.year,
            :cvc => card.cvv,
            :customer => options[:customer]
        }
      )
      stripe_customer = Stripe::Customer.retrieve(customer) if options[:customer].present?
      stripe_customer.sources.create(source: token) if stripe_customer.present?
      raise StandardError.new('Stripe Card Tokenization Failed') if token.nil? || !token.card
      return {card_id: token.card.id, token: token.id}
    end

    def stripe_customer(user,user_stripe_id=nil,env_key,email)
      email = email || user.email
      begin
        customer = Stripe::Customer.retrieve(user_stripe_id, env_key) if user_stripe_id.present?
        if customer.nil?
          customer = Stripe::Customer.create({:email => email}, {api_key: env_key})
        end
        return customer
      rescue StandardError => e
        my_string = e.message.try(:downcase)
        customer = Stripe::Customer.create({:email => email}, {api_key: env_key}) if my_string.include? "no such customer:"
        SlackService.notify("Stripe Customer retrieval failed : #{e.message.to_s}") if customer.blank?
        StandardError.new('Stripe Customer retrieval failed') if customer.blank?
      end
    end

    def stripe_charge(amount,card, key,user_id,customer_id,card_cvv,capture=nil, billing_address=nil)
      Stripe.api_key = key if key.present?
      capture = capture ? true : false
      begin
        card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user_id)
        customer = Stripe::Customer.retrieve(customer_id)
        card = create_card(customer, card_info, card_cvv, billing_address)
        charge = Stripe::Charge.create({
                                           :amount => (dollars_to_cents(amount).to_i),
                                           :currency => "usd",
                                           :customer => customer_id,
                                           :card => card.id,
                                           :capture => capture
                                       })
      rescue Stripe::CardError, Stripe::InvalidRequestError => e
        body = e.json_body
        err  = body[:error]
        if err[:code].present?
          return {message: e.message, charge: nil,error_code: err[:decline_code].present? ? err[:decline_code].try(:humanize) : err[:code].try(:humanize)}
        else
          return {message: e.message, charge: nil,error_code: "unknown"}
        end
      end
      return {message: nil,charge: charge,error_code: nil}
    end

    def valid_card(card_number)
      # card = CardToken.new
      # brand = card.card_brand(card_number)
      # length = 16
      # length = 14 if brand == 'Diners Club'
      # length = 15 if brand == 'American Express'
      # raise StandardError.new(I18n.t("api.registrations.invalid_card_data")) if card_number.length < length
    end

    def charge(card, token, amount)
      raise ArgumentError.new("Missing required parameter: amount.") if amount.nil? || amount.blank?
      raise ArgumentError.new("Atleast one source is required to complete the charge.") if card.nil? && token.nil?
      unless token.nil?
        return capture(token, centsFromDollars(amount))
      end
      unless card.nil?
        token = tokenize_card(card)
        return capture(token[:token], centsFromDollars(amount))
      end
      return nil
    end

    def void(transaction_id, options = {})
      # ADD void code here...
    end

    def refund(transaction_id, amount, options = {})
      # ADD REFUND code here...
    end

    def verify(transaction_id, amount, options = {})
      # ADD VERIFY code here...
    end

    private

    def capture(token, amount)
      charge = nil
      charge = Stripe::Charge.create({
        :amount => amount,
        :currency => "usd",
        :description => "KIOSK Card Charge",
        :source => token
      })
      if charge.present? && charge.paid != true && charge.status != 'succeeded'
        raise StandardError.new 'Card payment Rejected!'
      end
      return charge
    end

    def create_card(customer, card_info, card_cvv, billing_address)
      if billing_address[:require_address]
        card = customer.sources.create(source: {
            :object => 'card',
            :number => card_info.number,
            :exp_month => card_info.month,
            :exp_year => card_info.year,
            :cvc => card_cvv,
            :address_city => billing_address[:city],
            :address_country => billing_address[:country],
            :address_line1 => billing_address[:street],
            :address_state => billing_address[:state],
            :address_zip => billing_address[:zip]
        })
      else
        card = customer.sources.create(source: {
            :object => 'card',
            :number => card_info.number,
            :exp_month => card_info.month,
            :exp_year => card_info.year,
            :cvc => card_cvv
        })
      end
      return card
    end

  end
end
