module Payment
  class ICanPay < Gateway

    #key for db
    # authenticate_id   = payment_gateway.authentication_id
    # authenticate_pw   = payment_gateway.client_id
    # signature_maker   = payment_gateway.signature_maker
    # secret_key        = payment_gateway.client_secret
    def initialize(payment_gateway)
      @payment_gateway = payment_gateway
      @authenticate_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.authentication_id)
      @authenticate_pw = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
      @signature_maker = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.signature_maker)
      @secret_key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
    end

    #Charge ICanPay
    #
    def charge(amount,card_number,exp_date,cvc,parameters,request)
      first_name  = parameters["first_name"] || parameters["card_holder_name"].try(:split).try(:first) || parameters.try(:[],"card").try(:[],"name").try(:split).try(:first)
      last_name   = parameters["last_name"] || parameters["card_holder_name"].try(:split).try(:last) || parameters.try(:[],"card").try(:[],"name").try(:split).try(:last)
      email       = parameters["email"]
      exp_date    = "#{exp_date.first(2)}/#{exp_date.last(2)}"
      if parameters[:postal_address].present? && parameters[:street].blank?
        parameters[:street] = parameters[:postal_address]
      end
      validation = validate_required_params(
          :first_name => first_name,
          :last_name => last_name,
          :street  => parameters["street"], #'1600 Amphitheatre Parkway',
          :city    => parameters["city"],#'Mountain View',
          :zip_code => parameters["zip_code"],#'94043',
          :state   => parameters["state"], #'CA',
          :country => parameters["country"],#'USA',
          :phone_number => parameters["phone_number"],
          :amount => amount,
          :card_number => card_number,
          :cvv => cvc,
          :exp_date => exp_date,
          :email => email
      )

      return validation if validation.present?

      params = {
          :authenticate_id => @authenticate_id,
          :authenticate_pw => @authenticate_pw,
          :orderid => "#{rand(1..100)+Time.now.to_i}",
          :transaction_type => 'a',
          :amount => amount,
          :currency => 'USD',
          :card_info => generate_hmac(
              {
                  :ccn => card_number,
                  :expire => exp_date,
                  :cvc => cvc,
                  :firstname => first_name,
                  :lastname => last_name
              },
              @secret_key
          ),
          :email => email,
          :street => parameters["street"], #'1600 Amphitheatre Parkway',
          :city => parameters["city"],#'Mountain View',
          :zip => parameters["zip_code"],#'94043',
          :state => parameters["state"], #'CA',
          :country => parameters["country"],#'USA',
          :phone => parameters["phone_number"],#'923237935699',
          :customerip => request.remote_ip || request.ip
      }

      if @payment_gateway.i_can_pay?
        url = ENV["I_CAN_PAY_AUTHORIZE_PAYMENT"]
      else
        url = ENV["PAYMENT_TECHNOLOGIES_AUTHORIZE_PAYMENT"]
      end
      response = post_request(url,params)
      parse_response(response, "Transaction Successful")
    end

    def refund(transaction_id, amount)
      params = {
          :authenticate_id => @authenticate_id,
          :authenticate_pw => @authenticate_pw,
          :transaction_type => "R",
          :amount => amount,
          :currency => "USD",
          :customerip => RequestInfo.request_ip_address,
          :transaction_id => transaction_id
      }
      if @payment_gateway.i_can_pay?
        url = ENV["I_CAN_PAY_REFUND_PAYMENT"]
      else
        url = ENV["PAYMENT_TECHNOLOGIES_REFUND_PAYMENT"]
      end
      response = post_request(url,params)
      parse_refund_response(response)
    end

    private

    def post_request(url,params)
      p "BEFORE SORTING:", params
      params = params.sort.to_h
      p "AFTER SORTING:", params
      raw = params.values.join("") + @signature_maker
      p "RAW Signature", raw
      params[:signature] = Digest::SHA1.hexdigest(raw).downcase
      # p "ENC SIGNATURE", params[:signature]
      params[:tr_mode] = 'API'
      data = params
      response = sending_post_request(url, data)

      response
    end

    def sending_post_request(url, data)
      uri = Addressable::URI.new
      uri.query_values = data
      p "URI QUERY:", uri.query
      curlObj = Curl::Easy.new(url)
      curlObj.connect_timeout = 30
      curlObj.timeout = 30
      curlObj.header_in_body = false
      curlObj.ssl_verify_peer = false
      curlObj.post_body = uri.query
      curlObj.perform()
      response = curlObj.body_str
      p "response: ", response
    end

    def generate_hmac(payload,key = nil)
      payload = "ccn||#{payload[:ccn]}__expire||#{payload[:expire]}__cvc||#{payload[:cvc]}__firstname||#{payload[:firstname]}__lastname||#{payload[:lastname]}"

      alg = "aes-256-cbc"
      # key = key.gsub(/[^A-Za-z0-9 ]/i, SECRET_KEY)
      iv = OpenSSL::Cipher::Cipher.new(alg).random_iv
      aes = OpenSSL::Cipher::Cipher.new(alg)
      aes.encrypt
      aes.key = key
      aes.iv = iv
      hash = aes.update(payload.to_s)
      # p "HASHSHS", hash
      hash = hash + aes.final
      # p "payload.to_s", payload.to_s
      # p "hash ENC", hash
      puts "Our Encrypted data in base64"
      cipher64 = [hash].pack('m')
      cipher64 = Base64.encode64("#{cipher64}::#{iv}")
      # puts cipher64

      # decode_cipher = OpenSSL::Cipher::Cipher.new(alg)
      # decode_cipher.decrypt
      # decode_cipher.key = key
      # # decode_cipher.iv = iv
      # plain = decode_cipher.update(cipher64.unpack('m')[0])
      # plain << decode_cipher.final
      # puts "Decrypted Text"
      # puts plain

      return cipher64
    end

    def parse_response(response, message)
      p "API RESPONSE: ", response
      p "API message: ", message
      if response.present?
        parsed_hash = handle_response(response)
        p "PARSED RESPONSE: ", parsed_hash
        if parsed_hash["status"].present? && parsed_hash["status"] == "1"
          return {response: parsed_hash["transactionid"], message: message, descriptor: parsed_hash.try(:[], "descriptor")}
        elsif parsed_hash["errormessage"].present?
          return {response: nil,message: {message: parsed_hash["errormessage"]}}
        else
          return {response: nil,message: {message: "Bad Gateway Custom"}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway Custom"}}
      end
    end

    def parse_refund_response(parsed_hash)
      p "PARSED RESPONSE: ", parsed_hash
      if parsed_hash.present?
        parsed_hash = handle_response(parsed_hash)
        if parsed_hash["status"].present? && parsed_hash["status"] == "1"
          return {success: parsed_hash["transactionid"]}
        elsif parsed_hash["errormessage"].present?
          return {message: parsed_hash["errormessage"]}
        else
          return {message: "Bad Gateway"}
        end
      else
        return {message: "Bad Gateway"}
      end
    end

    def handle_response(response)
      hash = {}
      if @payment_gateway.payment_technologies?
        response = response.second if response.second.present?
      end
      response.split("&").map do |element|
        e = element.split('=')
        hash[e.first] = e.second
      end
      return hash
    end

  end
end