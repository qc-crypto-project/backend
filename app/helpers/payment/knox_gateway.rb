module Payment
  class KnoxGateway < Gateway

    #key for db
    # authenticate_id   = payment_gateway.authentication_id
    # authenticate_pw   = payment_gateway.client_id
    # signature_maker   = payment_gateway.signature_maker
    # secret_key        = payment_gateway.client_secret
    def initialize(payment_gateway)
      @client_id= AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
      @client_secret= AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
    end



    #Charge Knox
    def charge(amount,card_number,exp_date,cvc,parameters,request,card_info = nil,merchant)
      parameters["city"]=parameters["city"].present? ? parameters["city"] : merchant.try(:city) || merchant.try(:profile).try(:city)
      parameters["address"]=parameters["address"].present? ? parameters["address"] : merchant.try(:address)
      parameters["phone_number"]=parameters["phone_number"].present? ? parameters["phone_number"] : merchant.try(:phone_number)
      parameters["country"]=parameters["country"].present? ? parameters["country"] : merchant.try(:country) || merchant.try(:profile).try(:country)
      parameters["state"]=parameters["state"].present? ? parameters["state"] : merchant.try(:state) || merchant.try(:profile).try(:state)
      parameters["zip_code"]=parameters["zip_code"].present? ? parameters["zip_code"] : merchant.try(:zip_code) || merchant.try(:profile).try(:zip_code)
      parameters["email"]=parameters["email"].present? ? parameters["email"] : merchant.try(:email)
      first_name  = parameters["name"].present? ? parameters["name"].split(' ').try(:first)  : parameters["first_name"].present? ? parameters["first_name"] : merchant.try(:name)
      last_name  = parameters["name"].present? ? parameters["name"].split(' ').try(:last)  : parameters["last_name"].present? ? parameters["last_name"] :merchant.try(:name)
      email = parameters["email"]
      exp_date    = "#{exp_date.first(2)}/#{exp_date.last(2)}"
      validate_knox_params(
          :first_name  => first_name, #'1600 Amphitheatre Parkway',
          :last_name  => last_name, #'1600 Amphitheatre Parkway',
          :street  => parameters["first_name"], #'1600 Amphitheatre Parkway',
          :city    => parameters["last_name"],#'Mountain View',
          :zip     => parameters["zip_code"],#'94043',
          :state   => parameters["state"], #'CA',
          :address => parameters["address"],#'USA',
          :email => parameters["email"],#'USA',
          :phone => parameters["phone_number"],#'USA',
          )
      # description
      if parameters[:postal_address].present? && parameters[:street].blank?
        parameters[:street] = parameters[:postal_address]
      end
      params = {
          :email => email,
          :first_name => first_name,
          :last_name => last_name,
          :mobile => parameters["phone_number"],#'923237935699',
          :billing_address1 => parameters["address"].present? ? parameters["address"] : parameters['city'],
          :billing_address2 =>"",
          :billing_city => parameters["city"],#'Mountain View',
          :billing_zip => parameters["zip_code"],#'94043',
          :billing_state => parameters["state"], #'CA',
          :country => parameters["country"],#'USA',
          :customer_ip => request.remote_ip || request.ip,
          :order_description =>"EYU314",
          :card_no => card_number,
          :card_cvv => cvc,
          :expiry_month => exp_date.split('/').first,
          :expiry_year => "20#{exp_date.split('/').last}",
          :amount => amount,
      }
      url = ENV["KNOX_URL"]
      response = post_request(url,params,@client_id,@client_secret)
      # return {response: rand(0..9999999), message: "Approve",descriptor: "Test Descriptor"}
      parse_response(response, "Transaction Successful")
    end

    def charge_token(amount,card_number,exp_date,cvc,parameters,request,card_info = nil)
      first_name  = parameters["name"].split(' ').try(:first)
      last_name   = parameters["name"].split(' ').try(:last)
      email       = parameters["email"]
      exp_date    = "#{exp_date.first(2)}/#{exp_date.last(2)}"
      if card_info.present? && card_info.try(:[],'scheme').present?
        type=card_info['scheme'].try(:downcase)=='visa' ? 2 : card_info['scheme'].try(:downcase)=='amex' ? 1 : card_info['scheme'].try(:downcase)=='mastercard' ? 3 : card_info['scheme'].try(:downcase)=='discover' ? 4 : ''
      end
      validate_knox_params(
          :first_name  => first_name, #'1600 Amphitheatre Parkway',
          :last_name  => last_name, #'1600 Amphitheatre Parkway',
          :street  => parameters["first_name"], #'1600 Amphitheatre Parkway',
          :city    => parameters["last_name"],#'Mountain View',
          :zip     => parameters["zip_code"],#'94043',
          :state   => parameters["state"], #'CA',
          :address => parameters["address"],#'USA',
          :email => parameters["email"],#'USA',
          :phone => parameters["phone_number"],#'USA',
          )
      # description
      if parameters[:postal_address].present? && parameters[:street].blank?
        parameters[:street] = parameters[:postal_address]
      end
      params = {
          'email' => email,
          'first_name' => first_name,
          'last_name' => last_name,
          'mobile' => parameters["phone_number"],#'923237935699',
          'billing_address1' => parameters["address"],
          'billing_address2' =>"",
          'billing_city' => parameters["city"],#'Mountain View',
          'billing_zip' => parameters["zip_code"],#'94043',
          'billing_state' => parameters["state"], #'CA',
          'country' => parameters["country"],#'USA',
          'customer_ip' => request.remote_ip || request.ip,
          'order_description' =>"EYU314",
          'card_no' => card_number,
          'card_cvv' => cvc,
          'expiry_month' => exp_date.split('/').first,
          'expiry_year' => "20#{exp_date.split('/').last}",
          'amount' => amount,
      }
      url = ENV["KNOX_CREATE_TOKEN"]
      response = post_request(url,params,@client_id,@client_secret)
      parsed_reponse= parse_token_response(response, "Transaction Successful")
      if parsed_reponse.present? && parsed_reponse[:token].present?
        url = ENV["KNOX_TOKEN_PAYMENT"]
        token_payment={
            'token' =>parsed_reponse[:token],
            'order_description' =>'EYU314',
            "customer_ip" => "127.0.0.1",
            "amount"=> amount

        }
        response1 = post_request(url,token_payment,@client_id,@client_secret)
        final_response=parse_response(response1, "Transaction Successful")
        return final_response
      else
        return parsed_reponse
      end
    end

    def refund(transaction_id, amount)
      params = {
          'trans_id' => transaction_id,
          'amount' => amount,
      }
      url = ENV["KNOX_REFUND"]
      response = post_request(url,params,@client_id,@client_secret)
      if response.present? && response.try(:[],'status').present? && response.try(:[],'status')=='success'
        return {success: response["status"],refund_id: response["trans_id"]}
      elsif response.present? && response.try(:[],'error').present?
        return {message: response['error']}
      elsif response.present? && response.try(:[],'status').present? && response.try(:[],'status')=='fail'
        return {message: response['message']}
      else
        return {message: "Refund Failed!"}
      end
    end



    private

    def post_request(url,params,api_key = nil,api_secret)
      if api_key.present?
        curlObj = Curl::Easy.new(url)
        curlObj.connect_timeout = 3000
        curlObj.timeout = 3000
        # curlObj.header_in_body = true
        curlObj.headers = ["Authorization: #{api_key}:#{api_secret}",'Content-Type:application/json']
        # curlObj.ssl_verify_peer = false
        curlObj.post_body = params.to_json
        curlObj.perform()
        data = curlObj.body_str
        JSON(data)
      else
        raise StandardError.new("Please Provide Api Key by setLogin('API_KEY') For KNOX")
      end


    end

    def parse_response(response, message)
      p "API RESPONSE: ", response
      p "API message: ", message
      if response.present?
        if response["status"].present? && response["status"] == "success"
          return {response: response["transaction_id"], message: message,descriptor:response["descriptor"]}
        elsif response.try(:[],'error').present?
          if response['error'].include?('5004')
            return {response: nil,message: {message: 'Transaction amount is over the limit for a single transaction'}}
          elsif response['error'].include?('5003')
            return {response: nil,message: {message: 'Transaction amount is underneath the limit for a single transaction'}}
          elsif response['error'].include?('5001')
            return {response: nil,message: {message: 'Duplicate Failed Transaction'}}
          elsif response['error'].include?('5009')
            return {response: nil,message: {message: 'All gateways uavailable'}}
          else
            return {response: nil,message: {message: response["error"]}}
          end
        elsif response.try(:[],'status').present? && response["status"] == "fail"
          return {response: nil,message: {message: response["message"]}}
        elsif response['errors'].present?
          return {response: nil,message: {message: "#{response["message"]} #{response['errors'].try(:keys)}"}}
        else
          return {response: nil,message: {message: response['message']}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway Custom"}}
      end
    end

    def parse_token_payment(response, message)
      p "API RESPONSE: ", response
      p "API message: ", message
      if response.present?
        if response["status"].present? && response["status"] == "success"
          return {response: response["transaction_id"], message: message}
        elsif response.try(:[],'error').present?
          return {response: nil,message: {message: 'No Gateway available'}}
        else
          return {response: nil,message: {message: "Bad Gateway Custom"}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway Custom"}}
      end
    end

    def parse_token_response(response, message)
      p "API RESPONSE: ", response
      p "API message: ", message
      if response.present?
        if response["status"].present? && response["status"] == "success"
          return {token: response["token"], success: 'true'}
        elsif response.try(:[],'error').present?
          return {response: nil,message: {message: response["error"]}}
        else
          return {response: nil,message: {message: "Bad Gateway Custom"}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway Custom"}}
      end
    end

    def handle_response(response)
      hash = {}
      response.split("&").map do |element|
        e = element.split('=')
        hash[e.first] = e.second
      end
      return hash
    end

  end
end