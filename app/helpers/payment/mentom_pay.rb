module Payment
  class MentomPay < Gateway

    # Mentom New Object Initialization
    def initialize(payment_gateway)
      @payment_gateway = payment_gateway
      @authenticate_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.authentication_id)
    end

    # Mentom Charge ( Sale Transaction)
    def charge(amount,card_number,exp_date,cvc, name)
      exp_date    = "#{exp_date.first(2)}/#{exp_date.last(2)}"
      exp_month = exp_date.first(2)
      exp_year =  exp_date.last(2)
      validation = validate_required_params(
          :name => name,
          :amount => amount,
          :card_number => card_number,
          :cvv => cvc,
          :exp_date => exp_date,
          :exp_month => exp_month,
          :exp_year => exp_year
      )

      return validation if validation.present?

      params = {
          :amount => amount,
          "IsCreateProfile": true,
          "CreditCardInfo": {
              "CardNumber": card_number,
              "CardHolderName": name,
              "ExpMonth": exp_month,
              "ExpYear": exp_year,
              "CVV": cvc,
              "Expiry": exp_date
          },
      }


      url = "#{ENV["MENTOM_PAY_URL"]}Api/Transaction/Sale"
      response = sending_post_request(url, params)
      parse_response(response, "Transaction Successful")
    end

    # Mentom Refund
    def refund(transaction_id, amount)
      params = {
          :amount => amount,
          :TransactionId => transaction_id
      }
      url = "#{ENV["MENTOM_PAY_URL"]}Api/Transaction/Refund"
      response = sending_post_request(url,params)
      parse_refund_response(response)
    end

    def sending_post_request(url1, params)

      url = URI(url1)
      http = Net::HTTP.new(url.host,url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER

      request = Net::HTTP::Post.new(url)
      request.body = params.to_json

      request["Authorization"] = "#{@authenticate_id}"
      request["Content-Type"] = 'application/json'
      response = http.request(request)

      p "response: ", response.body
      return response.body

    end

    def parse_response(response, message)
      p "Sales API RESPONSE: ", response
      p "Sales API message: ", message
      if response.present? && response.valid_json?
        parsed_hash = JSON.parse(response)
        response_from_hash = {}
        if parsed_hash["Response"].present?
          if parsed_hash["Response"].kind_of?(Array)
            response_from_hash = parsed_hash["Response"].first
          elsif parsed_hash["Response"].kind_of?(Hash)
            response_from_hash = parsed_hash["Response"]
          elsif parsed_hash["Response"].kind_of?(String)
            response_from_hash = parsed_hash["Response"]
          end
        end

        p "PARSED RESPONSE: ", parsed_hash
        if parsed_hash["StatusCode"].present? && parsed_hash["StatusCode"] == 0 && response_from_hash.present? && response_from_hash.kind_of?(Hash)
          return {response: response_from_hash.try(:[],"TransactionId"), message:  parsed_hash.try(:[],"StatusMessage"), descriptor: response_from_hash.try(:[],"ProcessorType"), orginal_charge_id: response_from_hash.try(:[],"OriginalChargeID"), approval_code: response_from_hash.try(:[],"OriginalPaymentID")}
        elsif parsed_hash["StatusCode"].present? && parsed_hash["StatusCode"] == 1
          mess = parsed_hash["StatusMessage"]
          if parsed_hash["Response"].present? && !parsed_hash["Response"].valid_json?
            mess = parsed_hash["Response"]
          end
          return {response: nil,message: {message: mess}}
        else
          SlackService.notify(response_from_hash) if response_from_hash.present?
          SlackService.notify(response) if response.present? && response_from_hash.blank?
          return {response: nil,message: {message: response}}
        end
      elsif response.present? && !response.valid_json?
        SlackService.notify(response)
        return {response: nil,message: {message: response}}
      else
        return {response: nil,message: {message: "Bad Gateway Custom Mentom"}}
      end
    end

    def parse_refund_response(response)
      p "Refund API RESPONSE: ", response
      if response.present? && response.valid_json?
        parsed_hash = JSON.parse(response)
        response_from_hash = parsed_hash["Response"].present? ? parsed_hash["Response"] : {}
        p "PARSED RESPONSE: ", parsed_hash
        if parsed_hash["StatusCode"].present? && parsed_hash["StatusCode"] == 0 && response_from_hash.present?
          return {success: parsed_hash.try(:[],"StatusMessage"),refund_id: response_from_hash.try(:[],"TransactionId"),bank_descriptor: response_from_hash.try(:[], "ProcessorType"), original_payment_id: response_from_hash.try(:[],"OriginalPaymentID")}
        elsif parsed_hash["StatusCode"].present? && parsed_hash["StatusCode"] == 1
          return {message: {message: parsed_hash["StatusMessage"]}}
        else
          return {message: {message: response}}
        end
      elsif response.present? && !response.valid_json?
        SlackService.notify(response)
        return {message: {message: response}}
      else
        return {message: {message: "Bad Gateway Custom Mentom"}}
      end
    end

  end
end
