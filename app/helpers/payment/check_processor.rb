module Payment
  class CheckProcessor < Gateway

    def initialize
    end

    def get_response(request_data,dd)
      if dd.present? && dd.to_i == 1
         url = URI("https://checkbook.io/v3/check/direct")
      else
         url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/digital")
      end
      http = Net::HTTP.new(url.host,url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      request = Net::HTTP::Post.new(url)
      request.body = request_data
      request["Authorization"] = "#{ENV['CHECKBOOK_KEY']}:#{ENV['CHECKBOOK_SECRET']}"
      request["Content-Type"] ='application/json'
      response = http.request(request)
      return handle_response(response)
    end

    def handle_response(response)
      custom_response = nil
      case response
      when Net::HTTPSuccess
        flash_msg = "Success"
        success = true
        custom_response = response
      when Net::HTTPUnauthorized
        flash_msg = I18n.t 'merchant.controller.unauthorize_access'
        success = false
      when Net::HTTPNotFound
        flash_msg = I18n.t('merchant.controller.record_notfound')
        success = false
      when Net::HTTPServerError
        flash_msg = I18n.t('merchant.controller.server_notrespnd')
        success = false
      else
        error = JSON.parse(response.body)
        success = false
        flash_msg = "#{error["error"]}"
      end
      return {success:success, msg: flash_msg,response: custom_response}
    end

  end
end
