module Payment
  class Checkbook < Gateway
    attr_accessor :card, :api_url
    self.live_url = "https://checkbook.io/v3"
    # self.sandbox_url = "https://sandbox.checkbook.io/v3"

    def initialize(card)
      raise ArgumentError.new('CheckBook Error:' + I18n.t('api.registrations.invalid_card_data')) if card.nil? || card.class != Payment::QcCard
      @card = card
    end

    def instant_deposit(name, amountInDollars, recipientEmail, description = '')
      requires!({
        :recipient_name => name,
        :amount => amountInDollars,
        :recipient_email => recipientEmail,
      }, :recipient_name, :amount, :recipient_email)
      # You will get card details i-e; number, cvv, exp_date from @card object
      # @card.number
      # @card.cvv
      # @card.month
      # @card.year
      # Card must be Payment::QcCard type object
      # description is an optional parameter for this method

      # Add CheckBook.io instant deposit request code here...
    end
  end
end
