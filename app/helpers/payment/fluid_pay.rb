module Payment
  class FluidPayGateway < Gateway
    self.live_url = ""

    def tokenize_card(card, options = {})
      # TOKENIZE here....
    end

    def charge(card = nil, token = nil, amount)
      # ADD void code here...
    end

    def void(transaction_id, options = {})
      # ADD void code here...
    end

    def refund(transaction_id, amount, options = {})
      # ADD REFUND code here...
    end

    def verify(transaction_id, amount, options = {})
      # ADD VERIFY code here...
    end

    private

    def capture(token, amount)
      # ...
    end
  end
end
