module Payment
  class MaxmindMinfraud
    def initialize(request,params,card,user,billing_address_id,shipping_address_id)
      @request = request
      @params = params
      @card = card
      @assessment = Minfraud::Assessments.new
      begin
        set_device
        set_email(user.email)
        set_credit_card(card)
        if billing_address_id.present?
          set_billing_detail(billing_address_id)
        end
        if shipping_address_id.present?
          set_shipping_detail(shipping_address_id)
        end

        #@result = @assessment.score
        @result_insights=@assessment.insights
        @minfraud_risk_score = @result_insights.body[:risk_score]
        puts "@minfraud_risk_score"
        puts @minfraud_risk_score
        puts "@minfraud_risk_score"
      rescue StandardError => exc
        puts "---- exe----"
        puts exc
        puts "---- exe----"
      end
    end

    # Setters
    def set_email(full_email)
      #if @params[:email].present? && @params[:email].include?("@")
      #  full_email=@params[:email]
        email_split=full_email.split('@')
        email = Minfraud::Components::Email.new(
            address: full_email,
            domain: email_split[1]
        )
        @assessment.email = email
      #end
    end

    def set_device
      if @params[:user_ip].present? || @request.remote_ip.present?
        device = Minfraud::Components::Device.new(
            ip_address: @params[:user_ip] || @request.remote_ip,
            # ip_address: '202.163.76.48',
            user_agent:  @request.user_agent,
            accept_language: @request.accept_language
        )
        @assessment.device = device
      end
    end

    def set_credit_card(card)
      if card.present?
        credit_card = Minfraud::Components::CreditCard.new(
            issuer_id_number: card.first6,
            last_4_digits: card.last4,
            token: card.fingerprint,
            avs_result: "Y",
            cvv_result: "N"
        )
        @assessment.credit_card = credit_card
      end
    end

    def set_billing_detail(id)
      address=Address.find_by(id: id)
      return if address.blank?
      splited_name = address.name.split
      first_name = splited_name.first
      last_name = splited_name.last
      billing_country= ISO3166::Country.find_country_by_name(address.country) if address.country.present?
      billing = Minfraud::Components::Billing.new(
          first_name:    first_name.present? ? first_name : '',
          last_name:    last_name.present? ? last_name : '',
          #address:      @params[:address].present? ? @params[:address] : '',
          #address_2:    @params[:address_2].present? ? @params[:address_2] : '',
          city:         address.city.present? ? address.city : '',
          region:       address.state.present? ? address.state : '',
          #region:       billing_country.present? ? billing_country.region : '',
          country:      billing_country.present? ? billing_country.alpha2 : '',
          postal:       address.zip.present? ? address.zip : '',
          phone_number: address.phone_number.present? ? address.phone_number : '',
          phone_country_code: billing_country.present? ? billing_country.international_prefix : ''
      )
      @assessment.billing = billing
    end

    def set_shipping_detail(id)
      # address=ActiveRecord::Base::Address.find_by(id: id)
      address=Address.find_by(id: id)
      return if address.blank?
      splited_name = address.name.split
      first_name = splited_name.first
      last_name = splited_name.last
      shipping_country= ISO3166::Country.find_country_by_name(address.country) if address.country.present?
      shipping = Minfraud::Components::Shipping.new(
          first_name:    first_name.present? ? first_name : '',
          last_name:    last_name.present? ? last_name : '',
          #address:      @params[:address].present? ? @params[:address] : '',
          #address_2:    @params[:address_2].present? ? @params[:address_2] : '',
          city:         address.city.present? ? address.city : '',
          region:       address.state.present? ? address.state : '',
          #region:       shipping_country.present? ? shipping_country.region : '',
          country:      shipping_country.present? ? shipping_country.alpha2 : '',
          postal:       address.zip.present? ? address.zip : '',
          phone_number: address.phone_number.present? ? address.phone_number : '',
          phone_country_code: shipping_country.present? ? shipping_country.international_prefix : ''
      )
      @assessment.shipping = shipping
    end

    # Getters
    #def get_score
    #  if @result.body.present?
    #    if @result.body["risk_score"].present?
    #      {risk: @result.body[:risk_score],ip_risk: @result.body[:ip_address].present? ? @result.body[:ip_address][:risk] : 100}
    #    else
    #      {risk: 100,ip_risk: 100}
    #    end
    #  else
    #    {risk: 100,ip_risk: 100}
    #  end
    #end

    def minfraud_insights_response
      if @result_insights.present? && @result_insights.status==200
        return @result_insights.body
      else
        return nil
      end
    end

    def get_insights
      details={
          high_risk_country: false, timezone: nil, city: nil, country: nil, user_type: nil,
          local_date_time: nil, is_anonymous: false, is_billing_post_city: false, billing_post_to_ip: nil,
          billing_address_in_ip: false, is_shipping_post_city: false, shipping_post_to_ip: nil,
          shipping_address_in_ip: false, high_risk_shipping: false, shippping_distance_to_billing: nil,
          email_first_seen:nil, is_email_free: false, is_email_high_risk: false, card_country: nil,
          is_card_country_match_ip: false, is_card_country_match_billing: false, card_phone: nil,
          is_card_phone_match_billing: false, card_name: nil, is_card_name_match_billing: false,
          min_score_transaction_id: nil, min_score_transaction_time:nil, min_insight_transaction_id: nil,
          min_insight_transaction_time:nil
      }
      if @result_insights.present? && @result_insights.status==200
        if @result_insights.body.present?
          response=@result_insights.body
          # -------------------------IP Address Section---------------------------
          if response[:ip_address].present?
            ip_address=response[:ip_address]
            # country
            if ip_address[:country].present?
              details[:high_risk_country] = ip_address[:country][:is_high_risk] if ip_address[:country][:is_high_risk].present?
              details[:country] = ip_address[:country][:names][:en] if ip_address[:country][:names].present?
              detail_country=ip_address[:country][:iso_code] if ip_address[:country][:iso_code].present?
            end
            # timezone
            if ip_address[:location].present?
              details[:timezone] = ip_address[:location][:time_zone] if ip_address[:location][:time_zone].present?
              details[:local_date_time] = ip_address[:location][:local_time] if ip_address[:location][:local_time].present?
            end
            # city
            if ip_address[:city].present?
              details[:city] = ip_address[:city][:names][:en] if ip_address[:city][:names].present?
            end
            # traits
            if ip_address[:traits].present?
              details[:user_type] = ip_address[:traits][:user_type] if ip_address[:traits][:user_type].present?
              details[:is_anonymous] = ip_address[:traits][:is_anonymous] if ip_address[:traits][:is_anonymous].present?
            end
          end
          # -------------------------Billing Section---------------------------
          if response[:billing_address].present?
            billing_information=response[:billing_address]
            details[:is_billing_post_city] = billing_information[:is_postal_in_city] if billing_information[:is_postal_in_city].present?
            details[:billing_post_to_ip] = billing_information[:distance_to_ip_location] if billing_information[:distance_to_ip_location].present?
            details[:billing_address_in_ip] = billing_information[:is_in_ip_country] if billing_information[:is_in_ip_country].present?
          end
          # -------------------------Shipping Section---------------------------
          if response[:shipping_address].present?
            shipping_information=response[:shipping_address]
            details[:is_shipping_post_city] = shipping_information[:is_postal_in_city] if shipping_information[:is_postal_in_city].present?
            details[:shipping_post_to_ip] = shipping_information[:distance_to_ip_location] if shipping_information[:distance_to_ip_location].present?
            details[:shipping_address_in_ip] = shipping_information[:is_in_ip_country] if shipping_information[:is_in_ip_country].present?
            details[:high_risk_shipping] = shipping_information[:is_high_risk] if shipping_information[:is_high_risk].present?
            details[:shippping_distance_to_billing] = shipping_information[:distance_to_billing_address] if shipping_information[:distance_to_billing_address].present?
          end
          # -------------------------Email Section---------------------------
          if response[:email].present?
            email_information=response[:email]
            details[:email_first_seen] = email_information[:first_seen] if email_information[:first_seen].present?
            details[:is_email_free] = email_information[:is_free] if email_information[:is_free].present?
            details[:is_email_high_risk] = email_information[:is_high_risk] if email_information[:is_high_risk].present?
          end
          # -------------------------Credit Section---------------------------
          if response[:credit_card].present?
            if response[:credit_card][:country].present?
              card_country=response[:credit_card][:country]
              details[:card_country]=card_country
              if detail_country.present?
                details[:is_card_country_match_ip]=true if card_country==detail_country
              end
            end
            details[:is_card_country_match_billing]=response[:credit_card][:is_issued_in_billing_address_country] if response[:credit_card][:is_issued_in_billing_address_country].present?
            if response[:credit_card][:issuer].present?
              issuer=response[:credit_card][:issuer]
              if issuer[:phone_number].present?
                card_phone=issuer[:phone_number]
                details[:card_phone]=card_phone
                if @params[:phone_number].present?
                  details[:is_card_phone_match_billing]=true if card_phone==@params[:phone_number]
                end
              end
              details[:card_name]= issuer[:name] if issuer[:name].present?
              details[:is_card_name_match_billing]=issuer[:matches_provided_name] if issuer[:matches_provided_name].present?
            end
          end
          details[:min_insight_transaction_id]=@result_insights.body[:id] if @result_insights.body[:id].present?
        end
        if @result_insights.headers.present?
          details[:min_insight_transaction_time]=@result_insights.headers[:date] if @result_insights.headers[:date].present?
        end
      end
      if @result.present?
        if @result.body.present?
          details[:min_score_transaction_id]=@result.body[:id] if @result.body[:id].present?
        end
        if @result.headers.present?
          details[:min_score_transaction_time]=@result.headers[:date] if @result.headers[:date].present?
        end
      end
      details
    end

    def valid_score
      #score=get_score
      #risk_details=get_insights
      moderate_risk=AppConfig.find_by(key: AppConfig::Key::ModerateRiskMaxMind)
      high_risk=AppConfig.find_by(key: AppConfig::Key::HighRiskMaxMind)
      high_risk_score=high_risk.stringValue.to_f
      moderate_risk_score=moderate_risk.stringValue.to_f
      if high_risk_score == 0 || moderate_risk_score == 0
        status=TypesEnumLib::RiskType::Accept
        reason = TypesEnumLib::RiskReason::Default
      else
        if @minfraud_risk_score >= high_risk_score
          status = TypesEnumLib::RiskType::Reject
          reason = TypesEnumLib::RiskReason::Default
        elsif @minfraud_risk_score >= moderate_risk_score && @minfraud_risk_score < high_risk_score
          status=TypesEnumLib::RiskType::PendingReview
          reason = TypesEnumLib::RiskReason::ManualReview
        else
          status=TypesEnumLib::RiskType::Accept
          reason = TypesEnumLib::RiskReason::Default
        end
      end
      {risk: @minfraud_risk_score, action: status, reason: reason }
      #{risk: score[:risk],ip_risk: score[:ip_risk],risk_status: status,risk_details: risk_details}
    end

    def save_result_in_db(tr_id,website_url)
      min_fraud = nil
      scores = valid_score
      min_fraud = MinfraudResult.create(
          transaction_id: tr_id,
          website_url: website_url,
          risk_score:     @result_insights.body[:risk_score],
          mm_disposition: @result_insights.body[:disposition],
          qc_action: scores[:action],
          qc_reason: scores[:reason],
          insight_detail: @result_insights.body ) if @result_insights.present?

      return min_fraud.present? ? min_fraud : nil
    end

  end
end