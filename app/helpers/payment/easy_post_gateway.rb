module Payment
  class EasyPostGateway < Gateway
    include ApplicationHelper
    require 'easypost'
    EasyPost.api_key = ENV['EASY_POST_API_KEY']

    def initialize(request=nil, user_id = nil, api_key=nil)
      @easy_post_api_key = api_key || ENV['EASY_POST_API_KEY']
      @request = request
      @action = nil
      @user_id = user_id
    end

    def create_tracker(tracking_code)
      EasyPost::Tracker.create({
                                             tracking_code: tracking_code,
                                             #carrier: "USPS"
                                         })
    end

    def create_webhook(url)
      EasyPost::Webhook.create({url: url})
    end

    #def tracker_testing(tracker_code,txn_id)
    #  txn = Transaction.find(txn_id)
    #  customer_id = txn.try(:sender_id)
    #  tracker = Tracker.create(
    #      tracker_code: tracker_code,
    #      carrier: nil,
    #      signed_by: nil,
    #      est_delivery_date: nil,
    #      status: 'pre_transit',
    #      tracking_details: nil,
    #      user_id: customer_id,
    #      transaction_id: txn_id
    #      )
    #    track = create_tracker('EZ1000000001')
    #
    #  tracker.carrier = track["carrier"]
    #  tracker.signed_by = track["signed_by"]
    #  tracker.status = track["status"]
    #  tracker.est_delivery_date = track["est_delivery_date"]
    #  tracker.tracking_details = JSON.parse(track["tracking_details"].to_json)
    #
    #  tracker.save
    #end

    def create_child_user(name, phone_number, parent_id=nil)
      uri = URI.parse("https://api.easypost.com/v2/users")
      if parent_id.blank?
        @action = "save_admin_id"
        super_user = setup_request({},uri,"get")
        return super_user unless super_user[:success]
        super_user_id = super_user[:response]["id"]
      end
      data = {
          user: {name: name, phone_number: phone_number, parent_id: super_user_id}
      }
      return setup_request(data,uri)
    end

    def address(company, street1, street2, city, state, zip, phone, email)
      @action = "address"
      data = {
          address: {company: company, street1: street1, street2: street2, city: city, state: state, zip: zip, phone: phone, email: email}
      }
      uri = URI.parse("https://api.easypost.com/v2/addresses")
      return setup_request(data,uri)
    end

    def parcel(length, width, height, weight)
      @action = "parcel"
      data = {
          parcel: {length: length, width: width, height: height, weight: weight}
      }
      uri = URI.parse("https://api.easypost.com/v2/parcels")
      return setup_request(data,uri)
    end

    def create_shipment_get_rate(to_address_id, from_address_id, parcel_id)
      @action = "create_shipment_get_rate"
      data = {
          shipment: {to_address: {id: to_address_id}, from_address: {id: from_address_id}, parcel: {id: parcel_id}}
      }
      uri = URI.parse("https://api.easypost.com/v2/shipments")
      return setup_request(data,uri)
    end

    def generate_label(shipment_id, rate_id)
      @action = "generate_label"
      data = {
          rate: {id: rate_id}
      }
      uri = URI.parse("https://api.easypost.com/v2/shipments/#{shipment_id}/buy")
      return setup_request(data,uri)
    end


    private
    def setup_request(data,uri,request_type="post")
      headers = {
          'Authorization'=>"Bearer #{@easy_post_api_key}",
          'Content-Type' =>'application/json',
          'Accept'=>'application/json'
      }
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER

      if request_type == "get"
        response = http.get(uri.path, headers)
      else
        response = http.post(uri.path, data.to_json, headers)
      end
      result = JSON(response.body)
      if response.is_a?(Net::HTTPSuccess)
        result = {success: true, response: result}
      else
        result = {success: false, response:{}, message: result.try(:[],"error").try(:[],"message")}
      end
      type = result[:success] ? "success" : "error"
      data[:action]=@action
      data[:controller]="#{self.class}"
      record_it(type, result[:response], @request, data, @user_id, @action,"EasyPostGateway")
      return result
    end
  end
end
