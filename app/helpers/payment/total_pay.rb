module Payment
  class TotalPay < Gateway

    #key for db
    # authenticate_id   = payment_gateway.authentication_id
    # authenticate_pw   = payment_gateway.client_id
    # signature_maker   = payment_gateway.signature_maker
    # secret_key        = payment_gateway.client_secret

    #Charge TotalPay
    def charge(amount,card_number,exp_date,cvc,parameters,request,payment_gateway = nil,card_info = nil,merchant=nil)
      parameters["city"]=parameters["city"].present? ? parameters["city"] : merchant.try(:city) || merchant.try(:profile).try(:city)
      parameters["address"]=parameters["address"].present? ? parameters["address"] : merchant.try(:address)
      parameters["phone_number"]=parameters["phone_number"].present? ? parameters["phone_number"] : merchant.try(:phone_number)
      parameters["country"]=parameters["country"].present? ? parameters["country"] : merchant.try(:country) || merchant.try(:profile).try(:country)
      parameters["state"]=parameters["state"].present? ? parameters["state"] : merchant.try(:state) || merchant.try(:profile).try(:state)
      parameters["zip_code"]=parameters["zip_code"].present? ? parameters["zip_code"] : merchant.try(:zip_code) || merchant.try(:profile).try(:zip_code)
      parameters["email"]=parameters["email"].present? ? parameters["email"] : merchant.try(:email)
      parameters["street"]=parameters["street"].present? ? parameters["street"] :  parameters["address"].present? ?  parameters["address"] :  parameters["city"]



      first_name  = parameters["name"].split(' ').try(:first) if parameters["name"].present?
      last_name   = parameters["name"].split(' ').try(:last) if parameters["name"].present?
      email       = parameters["email"]
      exp_date    = "#{exp_date.first(2)}/#{exp_date.last(2)}"
      type=''
      if card_info.present? && card_info.try(:[],'scheme').present?
         type=card_info['scheme'].try(:downcase)=='visa' ? 2 : card_info['scheme'].try(:downcase)=='amex' ? 1 : card_info['scheme'].try(:downcase)=='mastercard' ? 3 : card_info['scheme'].try(:downcase)=='discover' ? 4 : ''
      end
      validation=validate__totalpay_required_params(
          :street  => parameters["street"], #'1600 Amphitheatre Parkway',
          :city    => parameters["city"],#'Mountain View',
          :zip     => parameters["zip_code"],#'94043',
          :state   => parameters["state"], #'CA',
          :address => parameters["address"],#'USA',
          :email => parameters["email"],#'USA',
          :phone => parameters["phone_number"],#'USA',
          :country => parameters["country"],#'USA',
      )
      return validation if validation[:message].present?
      if parameters[:postal_address].present? && parameters[:street].blank?
        parameters[:street] = parameters[:postal_address]
      end
      if payment_gateway.present?
        # authenticate_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.authentication_id)
        authenticate_id= AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
      end
      authenticate_id= Base64.strict_encode64(authenticate_id)
      params = {
          :Authorization => authenticate_id,
          :ext_order_id => "#{rand(1..100)+Time.now.to_i}",
          :amount => amount,
          :currency => 'USD',
          'cardData' =>
              {
                  :number => card_number,
                  :cvv => cvc,
                  :name => parameters["name"],
                  :month => exp_date.split('/').first,
                  :year => exp_date.split('/').last,
                  :type =>type
              },
          'userData'=>{
              :email => email,
              :street => parameters["street"], #'1600 Amphitheatre Parkway',
              :city => parameters["city"],#'Mountain View',
              :zip => parameters["zip_code"],#'94043',
              :state => parameters["state"], #'CA',
              :country => parameters["country"],#'USA',
              :phone => parameters["phone_number"],#'923237935699',
              :ip => request.remote_ip || request.ip, #'54.190.38.247'
              :first_name => first_name,
              :last_name => last_name,
              :address => parameters["address"],
              :birthday   =>"1983-02-22",
              # :username   => "",
              :subscription_status => 0
          }
      }

        url = ENV["TOTAL_PAY"]
        # url = 'https://secure.totalpay.co/rest/v1/transactions'
      response = post_request(url,params,authenticate_id)
      parse_response(response, "Transaction Successful")
    end

    def refund(transaction_id, amount,payment_gateway)
      if payment_gateway.present?
        authenticate_id= AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_id)
      end
      authenticate_id= Base64.strict_encode64(authenticate_id)
      params = {
          'Authorization' => authenticate_id,
          'amount' => amount.to_s,
          'id' => transaction_id
      }
      # url = ENV["Total_Pay_REFUND"]
      url = "#{ENV['TOTAL_PAY']}/refund/#{transaction_id}"
      response = post__refund_request(url,params,authenticate_id)
      if response.present? && response.try(:[],'status').present? && response.try(:[],'status')=='REFUNDED'
        return {success: response["status"],refund_id: response["refund_id"]}
      # elsif response.present? && response.try(:[],'error').present?
      #   return {message: response['error']}
      elsif response.present? && response.try(:[],'status').present? && response.try(:[],'status')=='fail'
        return {message: response['message']}
      else
        return {message: "Refund Failed!"}
      end
    end



    private

    def post_request(url,params,api_key = nil)
      if api_key.present?
        curlObj = Curl::Easy.new(url)
        curlObj.connect_timeout = 3000
        curlObj.timeout = 3000
        # curlObj.header_in_body = true
        curlObj.headers = ["Authorization: #{api_key}",'Content-Type:application/json']
        # curlObj.ssl_verify_peer = false
        curlObj.post_body = params.to_json
        curlObj.perform()
        data = curlObj.body_str
        JSON(data)
      else
        raise StandardError.new("Please Provide Api Key by setLogin('API_KEY') For Total Pay")
      end
    end

    def post__refund_request(url,params,api_key)
      if api_key.present?
        curlObj = Curl::Easy.new(url)
        curlObj.connect_timeout = 3000
        curlObj.timeout = 3000
        curlObj.headers = ["Authorization: #{api_key}",'Content-Type:application/json']
        curlObj.post_body = params.to_json
        curlObj.perform()
        data = curlObj.body_str
        if data.present?
          return JSON(data)
        else
          return {"message": "No Response Custom"}
        end
      else
        raise StandardError.new("Please Provide Api Key For Total Pay")
      end
    end

    # def sending_post_request(url, data)
    #   uri = Addressable::URI.new
    #   uri.query_values = data
    #   p "URI QUERY:", uri.query
    #   curlObj = Curl::Easy.new(url)
    #   curlObj.connect_timeout = 30
    #   curlObj.timeout = 30
    #   curlObj.header_in_body = false
    #   curlObj.ssl_verify_peer = false
    #   curlObj.post_body = uri.query
    #   curlObj.perform()
    #   response = curlObj.body_str
    #   p "response: ", response
    # end

    def parse_response(response, message)
      p "API RESPONSE: ", response
      p "API message: ", message
      if response.present?
        if response["status"].present? && response["status"] == "APPROVED"
          return {response: response["id"], message: message,descriptor:response["descriptor"]}
        elsif response["status"].present? && response["status"] == "DECLINED"
          return {response: nil,message: {message: response.try(:[],'information_data')}}
        else
          return {response: nil,message: {message: "Something Went Wrong"}}
        end
      else
        return {response: nil,message: {message: "Bad Gateway Custom"}}
      end
    end



  end
end