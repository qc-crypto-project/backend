module Payment
  class InstantAch
    def initialize(file)
      @file = file
    end

    def start_process
      curlObj = Curl::Easy.new("https://www.shapefutureconsulting.com/ACHB2B/api/ACHFile/PPSFFilePostWithKey")
      curlObj.connect_timeout = 30
      curlObj.timeout = 30
      curlObj.headers = ["PPSF-AccountID: #{ENV['PPSF_ACCOUNTID']}","PPSF-AccountToken: #{ENV['PPSF_ACCOUNTTOKEN']}", "PPSF-ClientID: #{ENV['PPSF_CLIENTID']}", "PPSF-XKeyBase: #{ENV['PPSF_XKEYBASE']}"]
      curlObj.multipart_form_post = true
      curlObj.http_post(Curl::PostField.file('ach_file_export', "#{@file.path}", "#{@file.path.split('/').last}"))
      # curlObj.http_post(Curl::PostField.file("ach-#{Time.now.to_i}.csv", "#{@file.path}"))
      # curlObj.perform( )
      data = curlObj.body_str
      return data
      # data = curlObj.body_str
    end

  end
end
