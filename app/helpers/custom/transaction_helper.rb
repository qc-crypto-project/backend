module Custom::TransactionHelper

  def transaction_process
    @merchant=@transaction.receiver
    @wallet = @transaction.receiver_wallet
    params[:merchant_wallet_id] = @transaction.receiver_wallet_id
    @user=@transaction.sender
    @user_wallet_id = @transaction.sender_wallet_id
    params[:wallet_id]=@user_wallet_id
    if @wallet.present?
      return {error: I18n.t("errors.withdrawl.blocked_location")} if @location.try(:is_block)

      if @location.try(:sale_limit).present?
        if @location.sale_limit_percentage.present?
          sale_limit = @location.sale_limit
          sale_limit_percentage = @location.sale_limit_percentage
          total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
          if @transaction.total_amount.to_f > total_limit.to_f
            return {error: I18n.t('api.registrations.trans_limit')}
          end
        else
          if @transaction.total_amount.to_f > @location.sale_limit.to_f
            return {error: I18n.t('api.registrations.trans_limit')}
          end
        end
      end
    end
    #Finalize Action
    finalize
    #Load Balancer
    load_balancer_new

    if @seq_transaction_id.try(:[], :blocked).present?
      return {error: I18n.t('merchant.controller.unaval_feature')}
    elsif @seq_transaction_id.try(:[], :decline_message).present?
      return {error: @seq_transaction_id[:decline_message]}
    elsif @seq_transaction_id.try(:[], :response).blank? && @seq_transaction_id.try(:[], :id).blank?
      if @seq_transaction_id.blank?
        return {error: I18n.t('api.errors.no_gateway', location_contact_no: @location.try(:phone_number) , location_email: @location.try(:email)) }
      end
      return {error: @seq_transaction_id[:message][:message] }
    end
  end

  def issue_transaction_process
    @merchant=@transaction.receiver
    @wallet = @transaction.receiver_wallet
    params[:merchant_wallet_id] = @transaction.receiver_wallet_id
    params[:issue_amount] = @transaction.total_amount
    @user=@transaction.sender
    @user_wallet_id = @transaction.sender_wallet_id
    params[:wallet_id]=@user_wallet_id
    main_type = TypesEnumLib::TransactionType::SaleType
    sub_type = TypesEnumLib::TransactionType::Issue3DS
    issue_raw_transaction = TransactionCharge::ChargeATransactionHelper::IssueRawTransaction.new(@user.email,params)
    @seq_transaction_id = issue_raw_transaction.issue_with_rtp(@merchant, request, @request.present? ? "invoice_rtp" :"rtp", @user, @user.email, main_type, 0,sub_type, params[:transaction_id])
    return {error: I18n.t('api.registrations.sequence')} if @seq_transaction_id.blank?
  end

  def transfer

    @data={}
    @data = @seq_transaction_id if @seq_transaction_id.present?
    @data = @data.merge(get_card_info(@card_number))unless @data.empty?
    @data.delete(:gateway_fee_details) if @data[:gateway_fee_details].present?
    user_balance = show_balance(@user_wallet_id)
    if user_balance.to_f < @transaction.total_amount.to_f
      return {error: I18n.t('api.registrations.balance') }
      # record_it(5,I18n.t('api.registrations.balance'))
    end
    @payment_gateway = @location.primary_gateway if @payment_gateway.blank?
    # current_transaction = transaction_between(wallet.id,user_wallet_id,transaction.total_amount.to_f, TypesEnumLib::TransactionType::SaleType,0, nil, data,user.ledger, gateway,TypesEnumLib::TransactionSubType::SaleVirtualApi,card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction, nil,nil,nil,gateway,nil, nil,nil)
    fee_class = Payment::FeeCommission.new(@transaction.sender, @transaction.receiver, @buyRate, @transaction.receiver_wallet_id, @location)
    if @request.present?
      @fee = fee_class.apply(@transaction.total_amount.to_f, 'invoice_card')
    elsif @card_bin_list["type"] == "credit"
      @fee = fee_class.apply(@transaction.total_amount.to_f, 'credit')
    else
      @fee = fee_class.apply(@transaction.total_amount.to_f, 'debit')
    end
    reserve_money = @buyRate.reserve_fee.to_f
    reserveAmount = deduct_fee(reserve_money,@transaction.total_amount)
    @reserve_detail = {amount: reserveAmount, days: @buyRate.days, wallet: @location.try(:wallets).try(:reserve).try(:first).try(:id)} if reserve_money > 0
    @user_info = @fee["splits"]
    if @reserve_detail.present?
      reserve = {"reserve" => @reserve_detail}
      @user_info = @user_info.merge(reserve)
    end
    pay_gat = @payment_gateway
    @gateway_fees = User.get_gateway_details(pay_gat) if pay_gat.present?
    if @data[:complete_trans].present?
      # to reject whole issue transaction present in data variable
      @data = @data.reject{|k| k == :complete_trans}
    end
    tags = {
        "fee_perc" => @user_info,
        "gateway_fee_details" => @gateway_fees,
        "previous_issue" => @data,
        "descriptor" => pay_gat.try(:name),
        "location" => location_to_parse(@location),
        "merchant" => merchant_to_parse(@location.try(:merchant))

    }
    update_transfer_transaction(@transaction, tags, @fee.try(:[], "fee").to_f, reserveAmount, @gateway_fees, @seq_transaction_id[:transaction_id], @payment_gateway)
  end
#Finalizes the transaction that it meets all the requirements before hitting the payment gateway
  def finalize
    merchant_balance = show_balance(@wallet.id, @merchant.ledger)
    @buyRate = @location.try(:fees).try(:buy_rate).try(:first)
    amount_fee_check = nil
    if @buyRate.present? || @location.present?
      amount_fee_check = check_fee_amount(@location, @merchant, @user, @wallet, @buyRate, @transaction.total_amount, merchant_balance, nil, 'credit')
    end
    if amount_fee_check.present? && amount_fee_check[:status] == false
      return {error: I18n.t('errors.checkout.amount')}
      # record_it(5,I18n.t('api.registrations.amount'))
    end

    if amount_fee_check.present? && amount_fee_check.try(:[], :status) == true
      if amount_fee_check[:splits].present?
        agent_fee = amount_fee_check[:splits]["agent"]["amount"]
        iso_fee = amount_fee_check[:splits]["iso"]["amount"]
        iso_balance = show_balance(@location.iso.wallets.primary.first.id)
        if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
          if iso_balance.to_f < iso_fee.to_f
            return {error: I18n.t('errors.checkout.transaction')}
            # record_it(5,I18n.t('api.registrations.cannot_trans'))
          end
        else
          if iso_balance.to_f + iso_fee.to_f < agent_fee.to_f
            return {error: I18n.t('errors.checkout.transaction')}
            # record_it(5,I18n.t('api.registrations.cannot_trans'))
          end
        end
      end
    end
    ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'finalize'
    params[:issue_amount]=@transaction.total_amount.to_f
  end

  def load_balancer_new
    qc_wallet = Wallet.qc_support.first
    issue_raw_transaction = TransactionCharge::ChargeATransactionHelper::IssueRawTransaction.new(@user.email,params, params[:card].try(:[], :cvv))
    issue_main_type = params[:main_type].present? ? TypesEnumLib::DeclineTransacitonTpe::IssueQCSecure : TypesEnumLib::DeclineTransacitonTpe::Issue3DS
    if @transaction.main_type == TypesEnumLib::TransactionType::QCSecure
      params["email"] = @transaction.try(:billing_address).try(:email)
      params["street"] = @transaction.try(:billing_address).try(:street)
      params["city"] = @transaction.try(:billing_address).try(:city)
      params["state"] = @transaction.try(:billing_address).try(:state)
      params["country"] = @transaction.try(:billing_address).try(:country)
      params["zip_code"] = @transaction.try(:billing_address).try(:zip)
    else
      params["email"] = @transaction.try(:shipping_address).try(:email)
      params["street"] = @transaction.try(:shipping_address).try(:street)
      params["city"] = @transaction.try(:shipping_address).try(:city)
      params["state"] = @transaction.try(:shipping_address).try(:state)
      params["country"] = @transaction.try(:shipping_address).try(:country)
      params["zip_code"] = @transaction.try(:shipping_address).try(:zip)
    end
    params["phone_number"] = @user.try(:phone_number)
    params["first_name"] = @user.try(:first_name).present? ? @user.try(:first_name) : @user.try(:name)
    params["last_name"] = @user.try(:last_name).present? ? @user.try(:last_name) : @user.try(:name)
    issue_raw_transaction = TransactionCharge::ChargeATransactionHelper::IssueRawTransaction.new(@user.email,params, params[:card].try(:[], :cvv))
    if @location.apply_load_balancer?
      load_amount = @transaction.total_amount.to_f
      get_gateway(load_amount, @card)
      @seq_transaction_id = load_balancer(issue_raw_transaction, nil, @card, @merchant, qc_wallet, request, nil, nil, @user, @card_number,nil,issue_main_type,nil,issue_main_type,@payment_gateway,@card_bin_list,nil,nil,nil,nil,@location.apply_load_balancer)
      gateway = @payment_gateway.key if @payment_gateway.present?
      if @seq_transaction_id.try(:[],:id).blank?
        update_load_balancer_in_failed_case(@seq_transaction_id, @card)
      else
        update_load_balancer_in_success_case(@seq_transaction_id, @card)
      end
    else
      if @location.primary_gateway.present?
        @payment_gateway = @location.primary_gateway
        #= LOAD BALANCER first_payment_gateway
        @seq_transaction_id = load_balancer(issue_raw_transaction, nil, @card, @merchant, qc_wallet, request, nil, nil, @user, @card_number,nil,issue_main_type,nil,issue_main_type,@location.secondary_gateway,@card_bin_list,nil,nil,nil,nil)
        #= LOAD BALANCER secondary_payment_gateway
        if @seq_transaction_id.try(:[],:response).blank? && @seq_transaction_id.try(:[],:id).blank? && @location.secondary_gateway.present?
          message = ""
          if @seq_transaction_id[:message][:message].present?
            message = @seq_transaction_id[:message][:message]
          end
          saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message},@merchant)
          @payment_gateway = @location.secondary_gateway
          @seq_transaction_id = load_balancer(issue_raw_transaction, nil, @card, @merchant, qc_wallet, request, nil, nil, @user, @card_number,nil,issue_main_type,nil,issue_main_type,@location.ternary_gateway,@card_bin_list,nil,nil,nil,nil)
        end
        #= LOAD BALANCER third_payment_gateway
        if @seq_transaction_id.try(:[],:response).blank? && @seq_transaction_id.try(:[],:id).blank? && @location.ternary_gateway.present?
          if @seq_transaction_id[:message][:message].present?
            message = @seq_transaction_id[:message][:message]
          end
          saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message},@merchant)
          @payment_gateway = @location.ternary_gateway
          @seq_transaction_id = load_balancer(issue_raw_transaction, nil, @card, @merchant, qc_wallet, request, nil,nil, @user, @card_number,nil,issue_main_type,nil,issue_main_type,nil,@card_bin_list,nil,nil,nil,nil)
        end

        if @seq_transaction_id.try(:[],:id).present?
          if @payment_gateway.present?
            @payment_gateway.update_daily_monthly_limits(@seq_transaction_id[:issue_amount])
          end
        end
      end
    end
  end
end