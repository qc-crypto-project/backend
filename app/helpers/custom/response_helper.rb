module Custom::ResponseHelper

  def render_json_response(resource, status)
    mask_card_param
    record_it(resource[:success] == true ? 4 : 5, resource.to_json)
    resource[:status] = resource[:success] == true ? TypesEnumLib::Statuses::Success : TypesEnumLib::Statuses::Failed if resource[:status].blank?
    render json: resource.to_json, status: status, adapter: :json_api, meta: default_meta
  end

  def default_meta
    {
        :licence => 'CC-0',
        :authors => ['Techcreatix'],
        :logged_in => (@current_user ? true : false)
    }
  end

  def update_transfer_transaction(transaction, tags, fee, reserve_amount, gateway_fees, charge, payment_gateway)
    gbox_fee = save_gbox_fee(tags.try(:[],"fee_perc"), fee) || 0
    total_fee = save_total_fee(tags.try(:[],"fee_perc"), fee) || 0
    transaction.update(
        fee: total_fee.to_f,
        reserve_money: reserve_amount.to_f,
        net_fee: number_with_precision(total_fee.to_f, precision: 2),
        net_amount: number_with_precision(transaction.total_amount.to_f - total_fee.to_f - reserve_amount.to_f, precision: 2),
        total_amount: transaction.total_amount.to_f,
        gbox_fee: gbox_fee.to_f,
        iso_fee: save_iso_fee(tags.try(:[],"fee_perc")),
        agent_fee: tags.try(:[], "fee_perc").try(:[],"agent").try(:[],"amount").to_f,
        affiliate_fee: tags.try(:[], "fee_perc").try(:[],"affiliate").try(:[],"amount").to_f,
        ip: get_ip,
        tags: tags,
        account_processing_limit: gateway_fees.try(:[],:account_processing_limit),
        transaction_fee: gateway_fees.try(:[],:transaction_fee),
        charge_back_fee: gateway_fees.try(:[],:charge_back_fee),
        retrieval_fee: gateway_fees.try(:[],:retrieval_fee),
        per_transaction_fee: gateway_fees.try(:[],:per_transaction_fee),
        reserve_money_fee: gateway_fees.try(:[],:reserve_money_fee),
        reserve_money_days: gateway_fees.try(:[],:reserve_money_days),
        monthly_service_fee: gateway_fees.try(:[],:monthly_service_fee),
        misc_fee: gateway_fees.try(:[],:misc_fee),
        misc_fee_dollars: gateway_fees.try(:[],:misc_fee_dollars),
        payment_gateway_id: payment_gateway.try(:id),
        load_balancer_id: payment_gateway.try(:load_balancer_id),
        charge_id: charge
    )
  end

  def saving_response(resource,user)
    mask_card_param
    record_it_duplicate(resource[:success] == true ? 4 : 5, resource.to_json,"3DS Issue",user)
  end

  def mask_card_param
    params[:card_number] = "#{(params[:card_number].to_s.first(4))}********#{(params[:card_number].to_s.last(4))}"
  end

  def record_it(type = nil, reason = nil,action = nil,user = nil)
    clean_parameters
    activity = ActivityLog.new
    activity.url = request.url
    activity.host = request.host
    activity.browser = request.env["HTTP_USER_AGENT"]
    if request.remote_ip.present?
      activity.ip_address = request.remote_ip
    end
    activity.controller = controller_name
    activity.action = action
    activity.ref_id = params[:RefId] || params[:ref_id]
    activity.respond_with = reason.present? ? {response: reason} : response.body
    activity.response_status = response.status
    unless type.nil?
      activity.activity_type = type
    end
    if params[:user_image].present?
      if params[:user_image].original_filename.present?
        params[:user_image] = params[:user_image].original_filename
      else
        params[:user_image] = 'unknown image format'
      end
    end
    activity.transaction_id = params[:transaction_id]
    params[:ip_details][:request_ip] = request.ip if params[:ip_details].try(:[],:request_ip).present?
    params[:ip_details][:request_remote_ip] = request.remote_ip if params[:ip_details].try(:[],:request_remote_ip).present?
    params[:issue_amount] = params[:issue_amount].to_f if params.try(:[],:issue_amount).present?
    clean_parameters(params[:registration]) if params[:registration].present?
    activity.params = params.except(:card_cvv)
    activity.user = user
    activity.save
  end


  def saving_decline_for_seq(issue_raw_transaction,payment_gateway = nil, transaction = nil)
    location_fee = issue_raw_transaction.get_location_fees(transaction.receiver_wallet_id,transaction.sender,transaction.total_amount,nil,TypesEnumLib::TransactionType::CreditTransaction)
    #= building hash for Decline
    transaction_info = {amount: transaction.total_amount.to_f}
    reserve_detail = location_fee.first if location_fee.present?
    user_info = location_fee.second if location_fee.present?
    fee = location_fee.third if location_fee.present?
    transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
    #= end building process
    OrderBank.create(merchant_id: transaction.receiver_id,user_id: transaction.sender_id,bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id))
  end

  def record_it_duplicate(type = nil, reason = nil,action = nil,user = nil)
    clean_parameters
    activity = ActivityLog.new
    activity.url = request.url
    activity.host = request.host
    activity.browser = request.env["HTTP_USER_AGENT"]
    if request.remote_ip.present?
      activity.ip_address = request.remote_ip
    end
    activity.controller = controller_name
    activity.action = action
    activity.ref_id = params[:RefId] || params[:ref_id]
    activity.respond_with = reason.present? ? {response: reason} : response.body
    activity.response_status = response.status
    unless type.nil?
      activity.activity_type = type
    end
    if params[:user_image].present?
      if params[:user_image].original_filename.present?
        params[:user_image] = params[:user_image].original_filename
      else
        params[:user_image] = 'unknown image format'
      end
    end
    activity.transaction_id = params[:transaction_id]
    params[:ip_details][:request_ip] = request.ip if params[:ip_details].try(:[],:request_ip).present?
    params[:ip_details][:request_remote_ip] = request.remote_ip if params[:ip_details].try(:[],:request_remote_ip).present?
    params[:issue_amount] = params[:issue_amount].to_f if params.try(:[],:issue_amount).present?
    clean_parameters(params[:registration]) if params[:registration].present?
    activity.params = params.except(:card_cvv)
    activity.user = user
    activity.save
  end


end