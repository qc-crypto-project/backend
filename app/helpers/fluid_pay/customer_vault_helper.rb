module FluidPay::CustomerVaultHelper
	class CustomerVault
		def initialize
			@api_key = ''
			@customer_vault_ach = {}
			@payment_method = {}
			@billing_address = {}
			@shipping_address = {}
		end

		def setLogin(api_key)
			@api_key = api_key
		end

		def add_ach_payment_method(routing_number,account_number,sec_code,account_type,check_number,dl_state,dl_number)
			@payment_method = {
				"ach"  => {
					"routing_number"  => routing_number,
					"account_number"  => account_number,
					"sec_code"  => sec_code,
					"account_type"  => account_type,
					"check_number"  => check_number,
					"accountholder_authentication"  => {
						"dl_state"  => dl_state,
						"dl_number"  => dl_number
					}
				}
			}
		end

		def add_card_payment_method(number,expiration_date,cvc,condition,eci,cavv,xid)
			@payment_method = {
				"card" => {
					"entry_type" => "keyed",
					"card_number" => number,
					"expiration_date" => expiration_date,
					"cvc" => cvc,
					"cardholder_authentication" => {
						"condition" => condition,
						"eci" => eci,
						"cavv" => cavv,
						"xid" => xid,
					}
				}
			}
		end

		def add_billing_address(first_name,last_name,company,address_line_1,address_line_2,city,state,postal_code,country,email,phone,fax)
			@billing_address = {
				"first_name" => first_name,
				"last_name" => last_name,
				"company" => company,
				"address_line_1" => address_line_1,
				"address_line_2" => address_line_2,
				"city" => city,
				"state" => state,
				"postal_code" => postal_code,
				"country" => country,
				"email" => email,
				"phone" => phone,
				"fax" => fax
			}
		end

		def add_shipping_address(first_name,last_name,company,address_line_1,address_line_2,city,state,postal_code,country,email,phone,fax)
			@shipping_address = {
				"first_name" => first_name,
				"last_name" => last_name,
				"company" => company,
				"address_line_1" => address_line_1,
				"address_line_2" => address_line_2,
				"city" => city,
				"state" => state,
				"postal_code" => postal_code,
				"country" => country,
				"email" => email,
				"phone" => phone,
				"fax" => fax
			}
		end

		def add_customer(description)
			@customer_vault_ach = {
				"description" => description,
				"payment_method" => @payment_method
				# "billing_address" => @billing_address,
				# "shipping_address" => @shipping_address
			}
		end

		def update_customer(description,payment_method,payment_method_id,billing_address_id,shipping_address_id)
			@customer_vault_ach = {
				"description" => description,
				"payment_method" => payment_method,
				"payment_method_id" => payment_method_id,
				"billing_address_id" => billing_address_id,
				"shipping_address_id" => shipping_address_id
			}
		end

		def delete_customer(customer_id)
			@customer_vault_ach = {
				"ID" => customer_id
			}
		end


		def goGet(customer_id)
			if @api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/customer"
				if customer_id.present?
					url += "/#{customer_id}"
				end
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				return "Please Provide Api Key by setLogin(API_KEY')"
			end
		end


		def doPost
			if @api_key.present?
				curlObj = Curl::Easy.new("#{ENV['FLUID_PAY_API_URL']}/customer")
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				# curlObj.header_in_body = true
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				# curlObj.ssl_verify_peer = false
				curlObj.post_body = @customer_vault_ach.to_json
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				return "Please Provide Api Key by setLogin(API_KEY')"
			end
		end
	end
end