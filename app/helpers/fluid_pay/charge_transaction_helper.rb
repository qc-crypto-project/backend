module FluidPay::ChargeTransactionHelper
	class TransactionByFluid
		def initialize
			@api_key = ''
			@charge_detail = {}
		end
		def setLogin(api_key)
			@api_key = api_key
		end
		def make_a_transaction(amount,customer_id,payment_method_id,description)
			@charge_detail = {
					"type" => "sale",
					"amount" => amount,
					"currency" => "USD",
					"description" => description,
					"payment_method" => {
							"customer" => {
									"id" => customer_id,
									"payment_method_type" => "card",
									"payment_method_id" => payment_method_id
							}
					}
			}
		end
		def make_a_transaction_raw(amount,card_number,expiration_date,cvc,first_name,last_name, processor_id = nil,billing=nil)
			@charge_detail = {
					"type" => "sale",
					"amount" => amount,
					"payment_method" => {
							"card" => {
									"entry_type"=>"keyed",
									"number"=>card_number.gsub(' ', ''),
									"expiration_date"=>expiration_date,
									"cvc"=>cvc
							}
					},
					"billing_address"=>{
							"first_name"=>first_name,
							"last_name"=>last_name,
							"address_line_1"=>billing[:street],
							"city"=>billing[:city],
							"state"=>billing[:state],
							"country"=>billing[:country],
							"postal_code"=>billing[:postalcode],
							"phone"=>billing[:phone],
					}
			}
			if processor_id.present?
				@charge_detail = @charge_detail.merge({"processor_id" => processor_id})
			end
			return @charge_detail
		end
		def doPost
			if @api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/transaction"
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				# curlObj.header_in_body = true
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				# curlObj.ssl_verify_peer = false
				curlObj.post_body = @charge_detail.to_json
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				raise StandardError.new("Please Provide Api Key by setLogin('API_KEY') For FluidPay")
			end
		end

		def voidTransaction(transaction_id)
			if !@api_key.present?
				raise StandardError.new("Please Provide Api Key by setLogin('API_KEY') For FluidPay")
			end
			url = "#{ENV['FLUID_PAY_API_URL']}/transaction/#{transaction_id}/void"
			curlObj = Curl::Easy.new(url)
			curlObj.connect_timeout = 3000
			curlObj.timeout = 3000
			curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
			curlObj.http_post()
			data = curlObj.body_str
			if [200,201,202,203].include? curlObj.response_code
				return JSON(data)
			else
				return {"status" => "failed","msg" => "Something went wrong. Please try again later!"}
			end
		end

		def refundTransaction(transaction_id,amount)
			if !@api_key.present?
				raise StandardError.new("Please Provide Api Key by setLogin('API_KEY') For FluidPay")
			end
			amountDetail= {
					"amount" => amount
			}
			url = "#{ENV['FLUID_PAY_API_URL']}/transaction/#{transaction_id}/refund"
			curlObj = Curl::Easy.new(url)
			curlObj.connect_timeout = 3000
			curlObj.timeout = 3000
			# curlObj.header_in_body = true
			curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
			# curlObj.ssl_verify_peer = false
			curlObj.post_body = amountDetail.to_json
			curlObj.perform()
			data = curlObj.body_str
			return JSON(data)
		end
	end
end
