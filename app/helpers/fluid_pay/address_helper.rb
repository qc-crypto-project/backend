module FluidPay::AddressHelper
	
	class FluidPayAddress
		
		def initialize
			@api_key = ''
		end

		def setLogin(api_key)
			@api_key=api_key
		end

		def create_customer_address(first_name,last_name,company,address_line_1,address_line_2,city,state,postal_code,country,email,phone,fax)
			@address = {
				"first_name" => first_name,
				"last_name" => last_name,
				"company" => company,
				"address_line_1" => address_line_1,
				"address_line_2" => address_line_2,
				"city" => city,
				"state" => state,
				"postal_code" => postal_code,
				"country" => country,
				"email" => email,
				"phone" => phone,
				"fax" => fax
			}
		end

		def update_customer_address(first_name,last_name,company,address_line_1,address_line_2,city,state,postal_code,country,email,phone,fax)
			@address = {
				"first_name" => first_name,
				"last_name" => last_name,
				"company" => company,
				"address_line_1" => address_line_1,
				"address_line_2" => address_line_2,
				"city" => city,
				"state" => state,
				"postal_code" => postal_code,
				"country" => country,
				"email" => email,
				"phone" => phone,
				"fax" => fax
			}
		end
		
		def goGet(customer_id,address_id)
			if @api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/customer"
				if customer_id.present? and address_id.present?
					url += "/#{customer_id}/address/#{address_id}"
				elsif customer_id.present? and not address_id.present?
					url += "/#{customer_id}/addresses"
				end
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				return "Please Provide Api Key by setLogin('API_KEY')"
			end
		end

		def goPost(customer_id,address_id)
			if @api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/customer"
				if customer_id.present?
					url += "/#{customer_id}/address"
				end
				if address_id.present?
					url += "/#{address_id}"
				end
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				# curlObj.header_in_body = true
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				# curlObj.ssl_verify_peer = false
				curlObj.post_body = @address.to_json
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				return "Please Provide Api Key by setLogin('API_KEY')"
			end
		end
	end
end