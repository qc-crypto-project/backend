module FluidPay::PaymentGatewayHelper
	class PaymentGatewayFluid
		def initialize
			@api_key = ''
			@payment_card = {}
		end

		def setLogin(api_key)
			# 19VpZDXlUKuLPmkjXLhdIEIOFQi
			@api_key = api_key
		end

		def create_card_token(number,expiration_date,cvc,condition,eci,cavv,xid)
			@payment_card = {
				"card_number" => number,
				"expiration_date" => expiration_date,
				"cvc" => cvc,
				"cardholder_authentication" => {
					"condition" => condition,
					"eci" => eci,
					"cavv" => cavv,
					"xid" => xid,
				}
			}
		end

		def update_card_token(number,expiration_date,cvc,condition,eci,cavv,xid)
			@payment_card = {
				"entry_type" => "keyed",
				"number" => number,
				"expiration_date" => expiration_date,
				"cvc" => cvc,
				"cardholder_authentication" => {
					"condition" => condition,
					"eci" => eci,
					"cavv" => cavv,
					"xid" => xid,
				}
			}
		end

		def goGet(customer_id,payment_id)
			if @api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/customer"
				if customer_id.present?
					url += "/#{customer_id}/paymentmethod/card"
				end
				if payment_id.present?
					url += "/#{payment_id}"
				end
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				return "Please Provide Api Key by setLogin('API_KEY')"
			end
		end


		def doPost(customer_id)
	# be40djtvgs1dgobq7ot0
	# be40cldvgs1dgobq7os0
			if @api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/customer"
				if customer_id.present?
					url += "/#{customer_id}/paymentmethod/card"
				end
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				# curlObj.header_in_body = true
				curlObj.headers = ["Authorization: #{@api_key}",'Content-Type:application/json']
				# curlObj.ssl_verify_peer = false
				curlObj.post_body = @payment_card.to_json
				curlObj.perform()
				data = curlObj.body_str
				return JSON(data)
			else
				return "Please Provide Api Key by setLogin('API_KEY')"
			end
		end
	end
end