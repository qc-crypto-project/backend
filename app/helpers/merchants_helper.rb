module MerchantsHelper


  def ach_check_number(checkId)
    return checkId.last 6 if checkId.present?
  end

  def deduct_fee(fee, amount)
    deduct = amount.to_f * (fee.to_f/100)
    return number_with_precision(deduct, precision: 2).to_f
  end

  def dispute_cases_datas
    wallet_ids=current_user.wallets.primary.pluck(:id)
    DisputeCase.where(merchant_wallet_id:wallet_ids).where('created_at > ?','01/08/2019'.to_datetime).group(:status).count
  end

end
