module LoadBalancerHelper
  # include ActionView::Helpers::NumberHelper
  class TransactionLoadBalancer
    include ActionView::Helpers::NumberHelper

    attr_accessor :load_balancer_rules, :payment_gateways, :error_code
    def initialize(location = nil, load_balancer = nil,card_brand_type)
      @location = location
      @test_case = load_balancer.present? ? true : false
      @card_brand_type = card_brand_type
      @load_balancer = location.present? ? location.load_balancer : load_balancer
      @load_balancer_rules = @load_balancer.load_balancer_rules.select {|lbr| lbr.active? }
      @issue_amount = nil
      @current_amount = nil
      @error_code = nil
    end

    def after_getting_issue_amount(issue_amount)
      unblock_gateways
      @issue_amount = issue_amount
      @payment_gateways = @load_balancer.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount)
      reset_gateways
    end

    def unblock_gateways
      if @load_balancer.payment_gateways.blocked.present?
        @load_balancer.payment_gateways.blocked.where("blocked_till < ?", DateTime.now.in_time_zone(PSTTIMEZONE) ).update_all(is_block: false,blocked_by_load_balancer: false, transaction_count: 0, decline_attempts: [] )
      end
    end

    # priority 1
    def decline_on_cc_country(cc_country)
      decline = false
      if @load_balancer_rules.select{|lbr| lbr.decline_on_cc_country? }.present?
        decline_rules = @load_balancer_rules.select{|lbr| lbr.decline_on_cc_country? }
        decline_rules.each do |decline_rule|
          countries = decline_rule.country.split(",")
          if countries.present?
            if decline_rule.equal_to?
              decline = countries.include?(cc_country)
            elsif decline_rule.not_equal_to?
              decline = !countries.include?(cc_country)
            end
          end
          return decline if decline == true
        end
      end
      return decline
    end

    # def card_types_from_gateway(gateway,card_detail)
    #   card_selected = "#{card_detail["scheme"]}_#{card_detail["type"] || 'credit'}".try(:downcase)
    #   payment_cards = gateway.card_selection
    #   if payment_cards.present? && card_selected.present?
    #     if payment_cards[card_selected].present? && payment_cards[card_selected] == "on"
    #       @payment_gateways
    #     else
    #       gateway
    #     end
    #   else
    #     gateway
    #   end
    #
    # end

    # priority 2
    def block_card_for_processed_count(card)
      group_id = @load_balancer.id
      if @load_balancer_rules.select{|lbr| lbr.block_card_for_processed_count? }.present?
        block_card_for_processed_count = @load_balancer_rules.select{|lbr| lbr.block_card_for_processed_count? }.first
        group_transaction_attempts = card.group_transaction_attempts["#{group_id}"]
        if group_transaction_attempts.count >= block_card_for_processed_count.count
          dates = group_transaction_attempts.select {|d| d.to_datetime > 24.hours.ago }
          if dates.count >= block_card_for_processed_count.count
            card.blocked_by_load_balancer = true
            card.is_blocked= true
            card.error_code = "error_2008"
            card.blocked_till = block_card_for_processed_count.no_of_hours.to_i > 0 ?  block_card_for_processed_count.no_of_hours.to_i.hours.since : nil
          end
        end
      end
    end

    def check_card_blocked(card_number)
      if card_number.present?
        card = Card.blocked_by_load_balancer_cards(card_number.first(6), card_number.last(4))
        if card.present?
          new_card = card.first
          if new_card.blocked_till.present? && new_card.blocked_till <= Time.zone.now
            new_card.update(blocked_by_load_balancer: false, is_blocked: false,blocked_till: nil,error_code:nil)
          else
            return new_card.error_code
          end
        end
      end
      return nil
    end


    # priority 3
    def rotate_processor_on_cc_brand(cc_brand,previous_gateway = nil)
      rotate_processor_on_cc_brands = @load_balancer_rules.select{|lbr| lbr.rotate_processor_on_cc_brand? }
      no_gateway  = []
      no_operator = []
      rotate_processor_on_cc_brands.each do |rotate_processor_on_cc_brand|
        if rotate_processor_on_cc_brand.equal_to? && cc_brand.present?
          operator = cc_brand.try(:downcase) == rotate_processor_on_cc_brand.card_brand.try(:downcase)
        elsif rotate_processor_on_cc_brand.not_equal_to? && cc_brand.present?
          operator = cc_brand.try(:downcase) != rotate_processor_on_cc_brand.card_brand.try(:downcase)
        end

        if operator
          no_operator.push(true)
          if rotate_processor_on_cc_brand.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount).present?
            no_gateway.push(true)
            payment_gateways = rotate_processor_on_cc_brand.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount)
            return payment_gateways.first if payment_gateways.count == 1

            rotation = rotate_processor_on_cc_brand.count
            total_trans = rotation * payment_gateways.count
            transactions_processed = payment_gateways.pluck(:cc_brand_count).sum
            my_calculation = ((transactions_processed % rotation).to_f/total_trans.to_f) * 100
            if my_calculation == 0 || my_calculation == 100
              pay_gate = payment_gateways.sort_by(&:cc_brand_count).first
              pay_gate.cc_brand_count += 1
              return pay_gate
            else
              payment_gateways.each do |pay_gate|
                if (pay_gate.cc_brand_count % rotation) != 0
                  pay_gate.cc_brand_count += 1
                  return pay_gate
                end
              end
            end
          # else
          #   @error_code = "error_2006"
          #   return nil
          end
        # else
        #   return previous_gateway
        end
      end
      return previous_gateway if no_operator.empty?
      return previous_gateway if no_operator.present? && no_gateway.empty?
      @error_code = "error_2006"
      return nil if no_gateway.empty?
    end


    def block_card_for_reason(decline_message,card)
      block_card_for_reasons = @load_balancer_rules.select{|lbr| lbr.block_card_for_reason? }
      block_card_for_reasons.each do|block_card_for_reason|
        if block_card_for_reason.present? && block_card_for_reason.decline_reason.present?
          block_card_for_reasons = block_card_for_reason.decline_reason.split(",")
          block_card_for_reasons.each do |dr|
            dc = card.decline_reason[dr]
            if dc.present?
              if decline_message.downcase.include?(dc.try(:downcase))
                card.is_blocked = true
                card.blocked_by_load_balancer = true
                card.error_code = "error_2007"
                if block_card_for_reason.no_of_hours.to_i > 0
                  SlackService.notify(decline_message)
                  card.blocked_till = block_card_for_reason.no_of_hours.to_i.hours.since
                else
                  msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@location.try(:merchant_id)} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(@issue_amount, :precision => 2, delimiter: ','))}\nCardholder Name: #{card.name}\nC.C #: #{card.first6}******#{card.last4}\nDecline Error: #{decline_message}"
                  SlackService.notify(msg,I18n.t("slack.channels.qc_risk_alert"))
                  card.blocked_till = nil
                end
              end
            end
          end
        end
      end
    end

    # priority 5
    def rotate_processor_with_amount(amount,previous_gateway = nil)
      rotate_processor_with_amounts = @load_balancer_rules.select{|lbr| lbr.rotate_processor_with_amount? }
      no_gateway = []
      no_operator = []
      rotate_processor_with_amounts.each do |rotate_processor_with_amount|
        if rotate_processor_with_amount.greater_than? && amount.present?
          operator = amount > rotate_processor_with_amount.amount
        elsif rotate_processor_with_amount.less_than? && amount.present?
          operator = amount < rotate_processor_with_amount.amount
        elsif rotate_processor_with_amount.equal_to? && amount.present?
          operator = amount == rotate_processor_with_amount.amount
        end

        if operator
          no_operator.push(true)
          if rotate_processor_with_amount.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount).present?
            no_gateway.push(true)
            payment_gateways = rotate_processor_with_amount.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount)
            if payment_gateways.count == 1
              pay_gat = payment_gateways.first
              pay_gat.amount_count += 1
              return pay_gat
            end

            rotation = rotate_processor_with_amount.count
            total_trans = rotation * payment_gateways.count
            transactions_processed = payment_gateways.pluck(:amount_count).sum
            my_calculation = ((transactions_processed % rotation).to_f/total_trans.to_f) * 100
            if my_calculation == 0 || my_calculation == 100
              pay_gate = payment_gateways.sort_by(&:amount_count).first
              pay_gate.amount_count += 1
              return pay_gate
            else
              payment_gateways.each do |pay_gate|
                if (pay_gate.amount_count % rotation) != 0
                  pay_gate.amount_count += 1
                  return pay_gate
                end
              end
            end
          # else
          #   @error_code = "error_2002"
          #   return nil
          end
        # else
        #   return previous_gateway
        end
      end
      return previous_gateway if no_operator.empty?
      return previous_gateway if no_operator.present? && no_gateway.empty?
      @error_code = "error_2002"
      return nil if no_gateway.empty?
    end

    # priority 6
    def rotate_processor_with_cc(card,previous_gateway = nil)
      if @load_balancer_rules.select{|lbr| lbr.rotate_processor_with_cc? }.present?
        rotate_processor_with_cc = @load_balancer_rules.select{|lbr| lbr.rotate_processor_with_cc? }.first
        if previous_gateway.present?
          # return previous_gateway if @payment_gateways.count == 1
          gateway_id = previous_gateway.id
          transaction_attempts = card.cc_number_transaction_attempts
          if transaction_attempts.present? && transaction_attempts["#{gateway_id}"] && transaction_attempts["#{gateway_id}"].count >= rotate_processor_with_cc.count
            dates = transaction_attempts["#{gateway_id}"].select {|d| d.to_datetime > 24.hours.ago }
            if dates.count >= rotate_processor_with_cc.count
              pgs = []
              pay_gates = @payment_gateways.reject{|pg| pg.id == gateway_id }
              pay_gates.each do |gateway|
                gateway_id1 = gateway.id
                transaction_attempts1 = card.cc_number_transaction_attempts["#{gateway_id1}"] if card.cc_number_transaction_attempts.present?
                unless transaction_attempts1.present? && transaction_attempts1.select {|d| d.to_datetime > 24.hours.ago }.count >= rotate_processor_with_cc.count
                  pgs << gateway
                end
              end
              if pgs.blank?
                @error_code = "error_2003"
                return nil
              end
              pay_gate = pgs.sort_by(&:cc_number_count).first
              transaction_attempts = card.cc_number_transaction_attempts
              if transaction_attempts.present? && transaction_attempts["#{pay_gate.id}"].present?
                transaction_attempts["#{pay_gate.id}"] << Time.zone.now
                transaction_attempts["#{pay_gate.id}"].select! {|d| d.to_datetime > 24.hours.ago }
              else
                transaction_attempts["#{pay_gate.id}"] = [Time.zone.now]
              end
              card.cc_number_transaction_attempts = transaction_attempts
              card.save
              pay_gate.cc_number_count += 1
              return pay_gate
            end
          end
          transaction_attempts = {} if transaction_attempts.blank?
          if transaction_attempts.present? && transaction_attempts["#{gateway_id}"].present?
            transaction_attempts["#{gateway_id}"] << Time.zone.now
            transaction_attempts["#{gateway_id}"].select! {|d| d.to_datetime > 24.hours.ago }
          else
            transaction_attempts["#{gateway_id}"] = [Time.zone.now]
          end
          card.cc_number_transaction_attempts = transaction_attempts
          card.save
          previous_gateway.cc_number_count += 1
          return previous_gateway
        else
          pgs = []
          @payment_gateways.each do |gateway|
            gateway_id = gateway.id
            transaction_attempts = card.cc_number_transaction_attempts["#{gateway_id}"] if card.cc_number_transaction_attempts.present?
            unless transaction_attempts.present? && transaction_attempts.select {|d| d.to_datetime > 24.hours.ago }.count >= rotate_processor_with_cc.count
              pgs << gateway
            end
          end
          if pgs.present?
            gateway = pgs.sort_by(&:cc_number_count).first
            transaction_attempts = card.cc_number_transaction_attempts
            if transaction_attempts.present? && transaction_attempts["#{gateway.id}"].present?
              transaction_attempts["#{gateway.id}"] << Time.zone.now
              transaction_attempts["#{gateway.id}"].select! {|d| d.to_datetime > 24.hours.ago }
            else
              transaction_attempts = {} if transaction_attempts.blank?
              transaction_attempts["#{gateway.id}"] = [Time.zone.now]
            end
            card.cc_number_transaction_attempts = transaction_attempts
            card.save
            gateway.cc_number_count += 1
            return gateway
          else
            @error_code = "error_2003"
            return nil
          end
        end
      else
        return previous_gateway
      end
    end

    # priority 7
    def rotate_processor
      if @load_balancer_rules.select{|lbr| lbr.rotate_processor? }.present?
        rotate_processor = @load_balancer_rules.select{|lbr| lbr.rotate_processor? }.first.count
        total_trans = rotate_processor * @payment_gateways.count
        transactions_processed = @payment_gateways.pluck(:rotation_count).sum
        my_calculation = ((transactions_processed % rotate_processor).to_f/total_trans.to_f) * 100
        if my_calculation == 0 || my_calculation == 100
          pay_gate = @payment_gateways.sort_by(&:rotation_count).first
          pay_gate.rotation_count += 1
          return pay_gate
        else
          @payment_gateways.each do |pay_gate|
            if (pay_gate.rotation_count % rotate_processor) != 0
              pay_gate.rotation_count += 1
              return pay_gate
            end
          end
        end
      end
    end

    # priority 9
    def block_card_for_decline_count(card)
      group_id = @load_balancer.id
      if @load_balancer_rules.select{|lbr| lbr.block_card_for_decline_count?}.present? && card.decline_transaction_attempts.present?
        block_card_for_decline_count = @load_balancer_rules.select{|lbr| lbr.block_card_for_decline_count?}.first
        decline_transaction_attempts = card.decline_transaction_attempts["#{group_id}"]
        if decline_transaction_attempts.present? && decline_transaction_attempts.count >= block_card_for_decline_count.count
          dates = decline_transaction_attempts.select {|d| d.to_datetime > 24.hours.ago }
          if dates.count >= block_card_for_decline_count.count
            card.blocked_by_load_balancer = true
            card.is_blocked = true
            card.error_code = "error_2004"
            card.blocked_till = block_card_for_decline_count.no_of_hours.to_i > 0 ? block_card_for_decline_count.no_of_hours.to_i.hours.since : nil
          end
        end
      end
    end

    # priority jocker
    def block_processor_daily_volume(gateway,current_amount)
      if @load_balancer_rules.select{|lbr| lbr.block_processor_daily_volume?}.present?
        block_processor_daily_volume = @load_balancer_rules.select{|lbr| lbr.block_processor_daily_volume?}.first
        if ((current_amount / gateway.daily_limit.to_f) * 100) >= block_processor_daily_volume.percentage.to_f
          gateway.blocked_by_load_balancer = true
          # gateway.is_block = true
          gateway.blocked_till = DateTime.now.in_time_zone(PSTTIMEZONE).end_of_day
        end
      end
    end

    def block_processor_daily_100(gateway,current_amount)
      if ((current_amount / gateway.daily_limit.to_f) * 100) >= 100.00
        gateway.blocked_by_load_balancer = true
        # gateway.is_block = true
        gateway.blocked_till = DateTime.now.in_time_zone(PSTTIMEZONE).end_of_day
      end
    end
    def block_processor_monthly_100(gateway,current_amount)
      if ((current_amount / gateway.monthly_limit.to_f) * 100) >= 100.00
        gateway.blocked_by_load_balancer = true
        # gateway.is_block = true
        gateway.blocked_till = DateTime.now.in_time_zone(PSTTIMEZONE).end_of_month
      end
    end


    def merger_of_rotation_on_amount_and_cc(amount,cc_brand,previous_gateway = nil)
      rotate_processor_on_cc_brands = @load_balancer_rules.select{|lbr| lbr.rotate_processor_on_cc_brand?}
      rotate_processor_with_amounts = @load_balancer_rules.select{|lbr| lbr.rotate_processor_with_amount?}
      operator1 = false
      operator2 = false
      operator1_count = []
      operator2_count = []
      gateway_count = []
      rotate_processor_on_cc_brands.each do |rotate_processor_on_cc_brand|
        if rotate_processor_on_cc_brand.equal_to? && cc_brand.present?
          operator1 = cc_brand.try(:downcase) == rotate_processor_on_cc_brand.card_brand.try(:downcase)
        elsif rotate_processor_on_cc_brand.not_equal_to? && cc_brand.present?
          operator1 = cc_brand.try(:downcase) != rotate_processor_on_cc_brand.card_brand.try(:downcase)
        end
        rotate_processor_with_amounts.each do |rotate_processor_with_amount|
          if rotate_processor_with_amount.greater_than? && amount.present?
            operator2 = amount > rotate_processor_with_amount.amount
          elsif rotate_processor_with_amount.less_than? && amount.present?
            operator2 = amount < rotate_processor_with_amount.amount
          elsif rotate_processor_with_amount.equal_to? && amount.present?
            operator2 = amount == rotate_processor_with_amount.amount
          end
          if operator1 && operator2
            operator1_count.push(true)
            operator2_count.push(true)
            cc_gateways = rotate_processor_on_cc_brand.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount)
            amount_gateways = rotate_processor_with_amount.payment_gateways.active_load_gateways(@card_brand_type,@issue_amount)
            allowed_gateway = cc_gateways & amount_gateways
            if allowed_gateway.present?
              gateway_count.push(true)
              if allowed_gateway.count == 1
                pay_gate = allowed_gateway.first
                pay_gate.amount_count += 1
                pay_gate.cc_brand_count += 1
                return pay_gate
              end
              payment_gateways = allowed_gateway
              rotation = rotate_processor_with_amount.count
              total_trans = rotation * payment_gateways.count
              transactions_processed = payment_gateways.pluck(:transaction_count).sum
              my_calculation = ((transactions_processed % rotation).to_f/total_trans.to_f) * 100
              if my_calculation == 0 || my_calculation == 100
                return payment_gateways.sort_by(&:transaction_count).first
              else
                payment_gateways.each do |pay_gate|
                  if (pay_gate.transaction_count % rotation) != 0
                    return pay_gate
                  end
                end
              end
            end
          elsif !operator1 && !operator2
          elsif operator1
            operator1_count.push(true)
          elsif operator2
            operator2_count.push(true)
          end
        end
      end
      if operator1_count.empty? && operator2_count.empty?
        if previous_gateway.present?
          return previous_gateway
        else
          return @payment_gateways.sort_by(&:transaction_count).first
        end
      elsif gateway_count.empty?
        @error_code = "error_2010"
        return nil
      elsif operator1_count.present? && operator2_count.empty?
        return rotate_processor_on_cc_brand(cc_brand,previous_gateway)
      elsif operator1_count.empty? && operator2_count.present?
        return rotate_processor_with_amount(amount,previous_gateway)
      else
        return rotate_processor_with_amount(amount,previous_gateway)
      end
    end

    # alerts

    def processor_daily_volume(gateway,current_amount)
      if @load_balancer_rules.select{|lbr| lbr.processor_daily_volume? }.present?
        processor_daily_volume = @load_balancer_rules.select{|lbr| lbr.processor_daily_volume?}.first
        if ((current_amount / gateway.daily_limit.to_f) * 100) >= processor_daily_volume.percentage.to_f
          SlackService.notify(I18n.t('mid_limits.daily_limit_volume_exceed',percent: processor_daily_volume.percentage.to_i ,name: @load_balancer.name, id: @load_balancer.id), ENV["NOTIFY_BANK_DECLINES"])
        end
      end
    end

    def processor_decline_times(gateway,message = nil)
      if @load_balancer_rules.select{|lbr| lbr.processor_decline_times?}.present?
        decline_count = gateway.decline_count
        processor_decline_times = @load_balancer_rules.select{|lbr| lbr.processor_decline_times?}.first.count
        if decline_count >= processor_decline_times
          decline_reasons = gateway.error_message.try(:uniq)
          body = "Warning! The Payment Gateway '#{gateway.name}' of bank type '#{gateway.key}' has declined last #{decline_count} consecutive transactions. "
          body = "Warning! The Payment Gateway '#{gateway.name}' of bank type '#{gateway.key}' has declined last #{decline_count} consecutive transactions. Decline Reason: `#{decline_reasons}`" if decline_reasons.present?
          SlackService.notify(body, ENV["NOTIFY_BANK_DECLINES"])
        end
      end
    end

    def group_volume
      if @load_balancer_rules.select{|lbr| lbr.group_daily_volume? || lbr.group_monthly_volume? }.present?
        group_daily_volume = @load_balancer_rules.select{|lbr| lbr.group_daily_volume?}.first
        group_monthly_volume = @load_balancer_rules.select{|lbr| lbr.group_monthly_volume?}.first
        group_selection = @payment_gateways

        if @load_balancer_rules.select{|lbr| lbr.group_daily_volume?}.present?
          group_amount_achived = group_selection.sum(&:daily_volume_achived)
          group_limit = group_selection.sum(&:daily_limit)
          if ((group_amount_achived / group_limit.to_f) * 100) >= group_daily_volume.percentage.to_f
            SlackService.notify(I18n.t('mid_limits.group_daily_limit_volume_exceed',percent: group_daily_volume.percentage.to_i ,name: @load_balancer.name, id: @load_balancer.id), ENV["NOTIFY_BANK_DECLINES"])
          end
        end

        if @load_balancer_rules.select{|lbr| lbr.group_monthly_volume?}.present?
          m_group_amount_achived = group_selection.sum(&:monthly_volume_achived)
          m_group_limit = group_selection.sum(&:monthly_limit)

          if ((m_group_amount_achived / m_group_limit.to_f) * 100) >= group_monthly_volume.percentage.to_f
            SlackService.notify(I18n.t('mid_limits.group_monthly_limit_volume_exceed', percent: group_monthly_volume.percentage.to_i ,name: @load_balancer.name, id: @load_balancer.id), ENV["NOTIFY_BANK_DECLINES"])
          end
        end

      end
    end

    def processor_monthly_volume(gateway,current_amount)
      if @load_balancer_rules.select{|lbr| lbr.processor_monthly_volume?}.present?
        processor_monthly_volume = @load_balancer_rules.select{|lbr| lbr.processor_monthly_volume?}.first
        if ((current_amount / gateway.monthly_limit.to_f) * 100) >= processor_monthly_volume.percentage.to_f
          SlackService.notify(I18n.t('mid_limits.monthly_limit_volume_exceed',percent: processor_monthly_volume.percentage.to_i ,name: @load_balancer.name, id: @load_balancer.id), ENV["NOTIFY_BANK_DECLINES"])
        end
      end
    end

    def processor_decline_percent(gateway)
      if @load_balancer_rules.select {|lbr| lbr.processor_decline_percent? }.present? && gateway.decline_attempts.present? && gateway.transaction_attempts.present?
        processor_decline_percents = @load_balancer_rules.select {|lbr| lbr.processor_decline_percent? }
        processor_decline_percents.each do |processor_decline_percent|
          operator = false
          no_of_hours = processor_decline_percent.no_of_hours
          if no_of_hours > 0
            decline_attempts = gateway.decline_attempts.select {|d| d.to_datetime > no_of_hours.to_i.hours.ago }
            transaction_attempts = gateway.transaction_attempts.select {|d| d.to_datetime > no_of_hours.to_i.hours.ago }
            if decline_attempts.count > 0 && transaction_attempts.count > 0
              calculated_percent = (decline_attempts.count.to_f/transaction_attempts.count.to_f)*100
              if processor_decline_percent.equal_to?
                operator = calculated_percent == processor_decline_percent.percentage
              elsif processor_decline_percent.greater_than?
                operator = calculated_percent > processor_decline_percent.percentage
              elsif processor_decline_percent.less_than?
                operator = calculated_percent < processor_decline_percent.percentage
              end

              if operator
                body = "The Payment Gateway '#{gateway.name}' of bank type '#{gateway.key}' has declined percentage #{number_with_precision(calculated_percent, precision: 2)} in last #{no_of_hours} hours."
                p "-------body---------"
                p body
                p "-------body---------"
                SlackService.notify(body, ENV["NOTIFY_BANK_DECLINES"])
              end
            end
          end
        end
      end
    end

    # ----------------------
    # db_misc_changes starts
    # ----------------------

    # for priority rule 6
    def update_card_transaction_attempts(card,gateway)
      transaction_attempts = card.transaction_attempts
      transaction_attempts = {} if transaction_attempts.blank?

      if transaction_attempts["#{gateway.id}"].present?
        transaction_attempts["#{gateway.id}"] << Time.zone.now
        transaction_attempts["#{gateway.id}"].select! {|d| d.to_datetime > 24.hours.ago }
      else
        transaction_attempts["#{gateway.id}"] = [Time.zone.now]
      end
      card.transaction_attempts = transaction_attempts
    end

    #for priority rule 9
    def update_card_decline_transaction_attempts(card)
      group_id = @load_balancer.id
      decline_transaction_attempts = card.decline_transaction_attempts
      decline_transaction_attempts = {} if decline_transaction_attempts.blank?

      if decline_transaction_attempts["#{group_id}"].present?
        decline_transaction_attempts["#{group_id}"] << Time.zone.now
        decline_transaction_attempts["#{group_id}"].select! {|d| d.to_datetime > 24.hours.ago }
      else
        decline_transaction_attempts["#{group_id}"] = [Time.zone.now]
      end
      card.decline_transaction_attempts = decline_transaction_attempts
    end


    def update_gateway_decline_attempts(gateway,message = nil)
      decline_transaction_attempts = gateway.decline_attempts
      decline_transaction_attempts = [] if decline_transaction_attempts.blank?
      decline_transaction_attempts.present? ? decline_transaction_attempts << Time.zone.now : decline_transaction_attempts = [Time.zone.now]
      decline_transaction_attempts.select! {|d| d.to_datetime > 24.hours.ago }
      gateway.decline_attempts = decline_transaction_attempts
      unless @test_case
        gateway.total_decline_count.present? ? gateway.total_decline_count += 1 : gateway.total_decline_count = 1
      end
      gateway.error_message.present? ? gateway.error_message << message : gateway.error_message = [message] if message.present?
      gateway.error_message = gateway.error_message.last(50) if message.present?

      gateway.last_status = I18n.t("api.load_balancers.last_status.decline")
      gateway.decline_count += 1
    end

    #for priority rule 2
    def update_card_group_transaction_attempts(card)
      group_id = @load_balancer.id
      group_transaction_attempts = card.group_transaction_attempts
      group_transaction_attempts = {} if group_transaction_attempts.blank?
      group_transaction_attempts["#{group_id}"].present? ? group_transaction_attempts["#{group_id}"] << Time.zone.now : group_transaction_attempts["#{group_id}"] = [Time.zone.now]
      group_transaction_attempts["#{group_id}"].select! {|d| d.to_datetime > 24.hours.ago }

      card.group_transaction_attempts = group_transaction_attempts
    end

    # for priority_no_7 update_gateway
    def update_gateway_transaction_count(gateway)
      transaction_attempts = gateway.transaction_attempts
      transaction_attempts = [] if transaction_attempts.blank?
      transaction_attempts.present? ? transaction_attempts << Time.zone.now : transaction_attempts = [Time.zone.now]
      transaction_attempts.select! {|d| d.to_datetime > 24.hours.ago }

      gateway.transaction_attempts = transaction_attempts
      gateway.transaction_count += 1
      unless @test_case
        gateway.total_transaction_count.present? ? gateway.total_transaction_count += 1 : gateway.total_transaction_count = 1
      end
    end

    def reset_gateways
      if @payment_gateways.present?
        d_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_day
        @payment_gateways.each do |payment_gateway|
          if payment_gateway.last_daily_updated.present? &&
              d_date != payment_gateway.last_daily_updated.in_time_zone(PSTTIMEZONE).beginning_of_day
            payment_gateway.update(amount_count:0,cc_brand_count:0,cc_number_count:0,rotation_count: 0)
          end
        end
      end
    end

    # ----------------------
    # db_misc_changes end
    # ----------------------

    def do_calculation_for_daily_limit(payment_gateway,new_amount)
      d_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_day

      if payment_gateway.last_daily_updated.present? &&
          d_date != payment_gateway.last_daily_updated.in_time_zone(PSTTIMEZONE).beginning_of_day ||
          payment_gateway.last_daily_updated.blank?

        payment_gateway.daily_volume_achived = 0
      end
      payment_gateway.daily_volume_achived = payment_gateway.daily_volume_achived + new_amount
      payment_gateway.last_daily_updated = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_day
      # daily_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",d_date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
      # return payment_gateway.daily_volume_achived
    end

    def do_calculation_for_monthly_limit(payment_gateway,new_amount)
      d_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_month

      if payment_gateway.last_monthly_updated.present? &&
          d_date != payment_gateway.last_monthly_updated.in_time_zone(PSTTIMEZONE).beginning_of_month ||
          payment_gateway.last_monthly_updated.blank?
        payment_gateway.monthly_volume_achived = 0
      end
      payment_gateway.monthly_volume_achived = payment_gateway.monthly_volume_achived + new_amount
      payment_gateway.last_monthly_updated = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_month

      # monthly_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
      return payment_gateway.monthly_volume_achived
    end
  end

end