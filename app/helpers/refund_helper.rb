module RefundHelper
  class RefundTransaction
    include ApplicationHelper
    include Payment
    include PosMerchantHelper

    def initialize(seq_transaction_id, db_transaction, merchant = nil, partial_amount=nil, reason = nil, current_user = nil, p2c_refund_fee = nil, qr_request = nil)
      @partial_amount = partial_amount
      @current_user = current_user
      @reason = reason
      @seq_transaction_id = seq_transaction_id
      @db_transaction = db_transaction
      @merchant = merchant
      @p2c_refund_fee = p2c_refund_fee
      @qr_request = qr_request
    end

    def process
      if @db_transaction.present?
        tx = SequenceLib.get_transaction_id(@seq_transaction_id)
        # return {success: false, message: "Invalid Transaction: Can't be refunded!"} if tx.blank?
        tx = parse_single_transaction(tx)
        # tx = JSON.parse(tx.to_json).first["actions"].first
        reason_detail = @reason.present? ? @reason : "NA"
        partial_amount = nil
        previousAmount = nil
        refund_fee = 0
        fee_splits = nil
        if @db_transaction.sender_id != @merchant.id && @db_transaction.receiver_id != @merchant.id
          return {success: false, message: "Transaction does not belong to this merchant"}
        end
        if @db_transaction.status != "refunded"
          transaction_id = 0
          if @db_transaction.tags["previous_issue"].present?
            transaction_id = @db_transaction.tags["previous_issue"]["transaction_id"] if @db_transaction.tags["previous_issue"]["transaction_id"].present?
            transaction_id = @db_transaction.tags["previous_issue"]["fluid_transaction_id"] if @db_transaction.tags["previous_issue"]["fluid_transaction_id"].present?
          else
            transaction_id = @db_transaction.charge_id
          end
          card = @db_transaction.card
          card = @db_transaction.try(:[],:reference).try(:[],"previous_issue").try(:[],"tags").try(:[],"card") if card.blank?
          card = @db_transaction.try(:[], :reference).try(:[], "card") if card.blank?
          user_wallet = @db_transaction.sender_wallet
          user = @db_transaction.sender
          # -----------------New BuyRate Fee------------------
          merchant_wallet = @db_transaction.receiver_wallet
          merchant = @db_transaction.receiver if @merchant.blank?
          merchant_location = merchant_wallet.location
          merchant_fee = merchant_location.fees.buy_rate.first
          pin_debit_refund = @db_transaction.main_type.try(:downcase) == TypesEnumLib::TransactionViewTypes::QrDebitCard.try(:downcase) ? true : false
          fee_class = Payment::FeeCommission.new(@merchant, nil, merchant_fee, merchant_wallet, merchant_location,TypesEnumLib::TransactionType::RefundFee,nil,pin_debit_refund)
          # -----------------End New Fee------------------
          t_amount = @db_transaction.total_amount.to_f

          payment_gateway = @db_transaction.payment_gateway
          payment_gateway = PaymentGateway.find_by_key(@db_transaction.tags["source"] || @db_transaction.tags["source2"]) if payment_gateway.blank?
          return {success: false, message: I18n.t("errors.refund_handler.blocked_refund_for_gateway")} if payment_gateway.present? && !payment_gateway.enable_refund?
          if payment_gateway.try(:type) == TypesEnumLib::GatewayType::FluidPay && @partial_amount.present?
            fluid_pay = FluidPayPayment.new
            fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
            if !([I18n.t("statuses.settled"),I18n.t("statuses.refunded"),I18n.t("statuses.partially_refunded")].include?(fluid_transaction_status)) && @partial_amount.to_f < t_amount
              return {success: false, message: I18n.t("errors.refund_handler.partial_refund_not_allowed")}
            end
          end
          if @partial_amount.present?
            if @partial_amount != "0"
              partial_amount = @partial_amount.to_f
              event = Hash.new
              temp = []
              tamount = t_amount.to_f - partial_amount.to_f
              if tamount >= 0
                if @db_transaction.refund_log.nil?
                  event = {:amount => partial_amount, :date => DateTime.now.utc, :sequence_id => @db_transaction.seq_transaction_id}
                  @db_transaction.refund_log = event.to_json
                else
                  if JSON.parse(@db_transaction.refund_log).kind_of?(Array)
                    temp = JSON.parse(@db_transaction.refund_log)
                  else
                    temp << JSON.parse(@db_transaction.refund_log)
                  end
                  previousAmount = temp.sum{|h| h["amount"]}
                  tamount = previousAmount + partial_amount
                  if tamount > t_amount.to_f
                    return {success: false, message: "Amount Cannot be refunded"}
                  end
                  if partial_amount <= @db_transaction.total_amount.to_f
                    event = {:amount => partial_amount, :date => DateTime.now.utc, :sequence_id => @db_transaction.seq_transaction_id}
                    temp << event
                    @db_transaction.refund_log = temp.to_json
                  else
                    return {success: false, message: "Amount Cannot be Refunded"}
                  end
                end
              else
                return {success: false, message: "Amount Cannot be Refunded"}
              end
            end
          end
          if @db_transaction.refund_log.nil?
            if partial_amount.to_f > @db_transaction.total_amount.to_f
              return {success: false, message: "Partial Amount is greater then actual amount"}
            end
          elsif @db_transaction.refund_log.present?
            if previousAmount.to_f + partial_amount.to_f > @db_transaction.total_amount.to_f
              return {success: false, message: "Already Refunded"}
            end
          end
          if @db_transaction.sender_wallet_id == user_wallet.try(:id) || (@db_transaction.main_type == TypesEnumLib::TransactionViewTypes::CreditCard && @db_transaction.receiver_wallet_id == merchant_wallet.try(:id))
            # ----------------------------------- Refunds Sequence---------------------------------------
            sequence_refund = false
            if @db_transaction.tags["sub_type"].nil? && @db_transaction.tags["api_type"].nil? && (@db_transaction.receiver_wallet_id.present? && @db_transaction.sender_wallet_id.present?)
              sequence_refund = true
            elsif @db_transaction.tags["sub_type"]!='sale_tip' && @db_transaction.tags["api_type"].nil? &&  @db_transaction.tags["sub_type"]!='sale-tip' && (@db_transaction.receiver_wallet_id.present? && @db_transaction.sender_wallet_id.present?)
              sequence_refund = true
            end
            if @db_transaction.tags["type"].present? && @db_transaction.tags["previous_issue"].nil?
              if @db_transaction.tags["type"] == "sale" && sequence_refund
                # ------------------Check Merchant Balance------------------------
                seq_transaction_amount = t_amount
                sequence_balance = show_balance(@db_transaction.receiver_wallet_id).to_f

                # -----------------New BuyRate Refund Fee-------------------------
                if @partial_amount.nil?
                  fee_object = fee_class.apply(seq_transaction_amount)
                  if fee_object.present?
                    fee_splits = fee_object["splits"].present? ? fee_object["splits"] : nil
                    refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                  end
                elsif @partial_amount.present?
                  if @partial_amount.to_f > 0
                    fee_object = fee_class.apply(@partial_amount.to_f)
                    if fee_object.present?
                      fee_splits = fee_object["splits"].present? ? fee_object["splits"] : nil
                      refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                    end
                  else
                    fee_object = fee_class.apply(seq_transaction_amount)
                    if fee_object.present?
                      fee_splits = fee_object["splits"].present? ? fee_object["splits"] : nil
                      refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                    end
                  end
                end
                # -----------------------End New BuyRate Refund Fee-------------------
                if !partial_amount.nil?
                  if !previousAmount.nil?
                    return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if partial_amount.present? && previousAmount.present? && partial_amount > previousAmount
                  else
                    return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if partial_amount.present? && previousAmount.present? && partial_amount + refund_fee > sequence_balance
                  end
                else
                  return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if seq_transaction_amount.present? && seq_transaction_amount + refund_fee > sequence_balance
                end
                # ------------------Check Merchant Balance------------------------
                sequence_response = refund_sequence_transaction(tx,reason_detail,partial_amount,previousAmount,fee_splits,refund_fee, payment_gateway)
                if sequence_response[0]
                  message_amount = partial_amount || t_amount
                  if merchant_location.email_receipt && merchant.try('company_id') != 2
                    refund_receipt(merchant_location.try(:id), message_amount, user.try(:id),Date.today.to_json, card.try(:[],"brand").try(:capitalize), card.try(:[],"last4"), sequence_response[2], @db_transaction)
                  end
                  if merchant_location.sms_receipt && merchant.try('company_id') != 2
                    message = refund_text_message(message_amount, merchant_location.business_name,card.try(:[], "brand").try(:capitalize), card.try(:[], "last4"), user.try(:name), merchant_location.cs_number || merchant_location.phone_number)
                    send_text_message(message, user.try(:phone_number))
                  end
                  return {success: true, message: sequence_response[1] , descriptor_id: payment_gateway.try(:name) || payment_gateway_name(@db_transaction.tags["source"])}
                else
                  return {success: false, message: sequence_response[1]}
                end
              else
                return {success: false, message: "Refund Unsuccessful"}
              end
            end
            # ------------------------Refund Issue Transaction--------------------------------
            if @db_transaction.tags["api_type"].present?
              if @db_transaction.tags["api_type"].downcase == "credit" || (@db_transaction.tags["api_type"].downcase == "debit" && @db_transaction.main_type == TypesEnumLib::TransactionType::DebitCard )
                # ------------------Check Merchant Balance------------------------
                seq_credit_transaction_amount = @db_transaction.tags["previous_issue"]["issue_amount"].to_f + @db_transaction.tip.to_f
                sequence_credit_balance = show_balance(@db_transaction.receiver_wallet_id).to_f

                # -----------------New BuyRate Refund Fee-------------------------
                if @partial_amount.nil?
                  if @db_transaction.refund_log.present?
                    refund_hash = JSON.parse(@db_transaction.refund_log)
                    if refund_hash.class == Hash
                      seq_credit_transaction_amount = refund_hash["amount"]
                    else
                      seq_credit_transaction_amount = seq_credit_transaction_amount - refund_hash.sum{|h| h["amount"]}
                    end
                  end
                  fee_object = fee_class.apply(seq_credit_transaction_amount)
                  if fee_object.present?
                    fee_splits= fee_object["splits"].present? ? fee_object["splits"] : nil
                    refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                  end
                elsif @partial_amount.present?
                  if @partial_amount.to_f > 0
                    fee_object = fee_class.apply(@partial_amount.to_f)
                    if fee_object.present?
                      fee_splits= fee_object["splits"].present? ? fee_object["splits"] : nil
                      refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                    end
                  else
                    fee_object = fee_class.apply(seq_credit_transaction_amount)
                    if fee_object.present?
                      fee_splits= fee_object["splits"].present? ? fee_object["splits"] : nil
                      refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                    end
                  end
                end
                # -----------------------End New BuyRate Refund Fee-------------------
                if !partial_amount.nil?
                  if !previousAmount.nil?
                    return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if partial_amount.present? && previousAmount.present? && partial_amount + previousAmount > seq_credit_transaction_amount
                  else
                    return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if partial_amount.present? && previousAmount.present? && partial_amount + refund_fee > sequence_balance
                  end
                else
                  return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if seq_credit_transaction_amount.present? && seq_credit_transaction_amount + refund_fee > sequence_credit_balance
                end
                # ------------------Check Merchant Balance------------------------
                issue_response = refund_issue_transaction(tx, transaction_id, reason_detail, partial_amount, previousAmount, fee_splits, refund_fee, payment_gateway)
                if issue_response[0]
                  return {success: true, message: issue_response[1], descriptor_id: payment_gateway.try(:name) || payment_gateway_name(@db_transaction.tags["source"]),bank_response: issue_response[3]}
                else
                  return {success: false, message: issue_response[1],bank_response: issue_response[3]}
                end
              else
                return {success: false, message: "Cannot Refund This Type"}
              end
            end
            # -----------------------Refund Transfer Transaction-----------------------------
            # ------------------Check Merchant Balance------------------------
            seq_transfer_transaction_amount = @db_transaction.total_amount.to_f
            if tx.present?
              sequence_transfer_balance = show_balance(tx[:destination]).to_f if sequence_balance.nil? && sequence_credit_balance.nil?
            else
              sequence_transfer_balance = show_balance(@db_transaction.try(:receiver_wallet_id)).to_f if sequence_balance.nil? && sequence_credit_balance.nil?
            end
            # -----------------New BuyRate Refund Fee-------------------------
            if @partial_amount.nil?
              if @db_transaction.refund_log.present?
                refund_hash = JSON.parse(@db_transaction.refund_log)
                if refund_hash.class == Hash
                  seq_transfer_transaction_amount = refund_hash["amount"]
                else
                  seq_transfer_transaction_amount = @db_transaction.total_amount.to_f - refund_hash.sum{|h| h["amount"]}
                end
              end
              fee_object = fee_class.apply(seq_transfer_transaction_amount)
              if fee_object.present?
                fee_splits= fee_object["splits"].present? ? fee_object["splits"] : nil
                refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
              end
            elsif @partial_amount.present?
              if @partial_amount.to_f > 0
                fee_object = fee_class.apply(@partial_amount.to_f)
                if fee_object.present?
                  fee_splits= fee_object["splits"].present? ? fee_object["splits"] : nil
                  refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                end
              else
                fee_object = fee_class.apply(seq_transfer_transaction_amount)
                if fee_object.present?
                  fee_splits= fee_object["splits"].present? ? fee_object["splits"] : nil
                  refund_fee = fee_object["fee"].present? ? fee_object["fee"].to_f : 0
                end
              end
            end
            # -----------------------End New BuyRate Refund Fee-------------------

            if !partial_amount.nil?
              if !previousAmount.nil?
                return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if partial_amount.present? && previousAmount.present? && partial_amount + previousAmount > @db_transaction.total_amount.to_f
              else
                return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if partial_amount.present? && previousAmount.present? && partial_amount + refund_fee > sequence_balance
              end
            else
              return {success: false, message: I18n.t('merchant.controller.insufficient_balance')} if seq_transfer_transaction_amount.present? && seq_transfer_transaction_amount + refund_fee > sequence_transfer_balance
            end
            # ------------------Refund Transaction------------------------
              refund_response = refund_transaction(tx,transaction_id,reason_detail,partial_amount,previousAmount,fee_splits,refund_fee, payment_gateway)
            if refund_response[0]
              message_amount = partial_amount || @db_transaction.total_amount.to_f
              if merchant_location.email_receipt && @merchant.company_id != 2
                refund_receipt(merchant_location.try(:id), message_amount, user.try(:id),Date.today.to_json, card.try(:[],"brand").try(:capitalize), card.try(:[],"last4"), refund_response[2] , @db_transaction)
                message = refund_text_message(message_amount, merchant_location.business_name, card.try(:[], "brand").try(:capitalize), card.try(:[], "last4"), user.try(:name), merchant_location.cs_number || merchant_location.phone_number)
              end
              if merchant_location.sms_receipt && @merchant.company_id != 2
                send_text_message(message, user.try(:phone_number))
              end
              return {success: true, message: refund_response[1],descriptor_id: payment_gateway.try(:name) || payment_gateway_name(@db_transaction.tags["source"]), bank_response: refund_response[3]}
            else
              return {success: false, message: refund_response[1], bank_response: refund_response[3]}
            end
          else
            return {success: false, message: "Wallet does not belongs to user."}
          end
        else
          return {success: false, message: "Already Refunded."}
        end
        # else
        #   return {success: false, message: "No Transaction found."}
        # end
      else
        {success: false, message: "Please provide Transaction Id."}
      end
    end

    def refund_sale_issue
      #this function is for refunding qr scanned sale issue  transactions
      if @db_transaction.present?
        if @db_transaction.status != "refunded"
          if @partial_amount.present?
            if @partial_amount != "0"
              partial_amount = @partial_amount.to_f
              tamount = @db_transaction.total_amount.to_f - partial_amount.to_f
              if tamount >= 0
                event = {:amount => partial_amount, :date => DateTime.now.utc, :sequence_id => @db_transaction.seq_transaction_id}
                @db_transaction.refund_log = event.to_json
              else
                return {success: false, message: "Amount Cannot be Refunded"}
              end
            end
          end
          transaction_id = @db_transaction.charge_id
          card = @db_transaction.card
          payment_gateway = @db_transaction.payment_gateway
          payment_gateway = PaymentGateway.find_by_key(@db_transaction.tags["source"] || @db_transaction.tags["source2"]) if payment_gateway.blank?
          payment_gateway_type = payment_gateway.type if payment_gateway.present?
          if payment_gateway.try(:type) == "fluid_pay"
            fluid_pay = FluidPayPayment.new
            fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
            if (fluid_transaction_status != "settled" && fluid_transaction_status != "refunded")
              return {success: false, message: "Partial refund cannot be completed at this time. You must wait 24 hours from the original transaction before issuing a partial refund."}
            end
          end
          card_token = CardToken.new
          if @partial_amount == nil
            if @db_transaction.refund_log.present?
              refund_hash = JSON.parse @db_transaction.refund_log
              if refund_hash.class == Hash
                transaction_amount = @db_transaction.total_amount.to_f - refund_hash["amount"].to_f
              else
                transaction_amount = @db_transaction.total_amount.to_f - refund_hash.sum{|h| h["amount"]}
              end
            else
              transaction_amount = @db_transaction.total_amount.to_f
            end
          else
            transaction_amount = @partial_amount.to_f
          end
          bank_amount = total_issue_amount = @db_transaction.total_amount.to_f
          p "---------------- #{transaction_amount} ----------------- #{bank_amount}"
          if transaction_amount <= bank_amount
            bank_amount = transaction_amount
          end
          if payment_gateway_type == TypesEnumLib::GatewayType::Stripe
            # -----------------------------Stripe Refund-----------------------------------
            refund = card_token.stripe_refund(transaction_id, bank_amount, payment_gateway)
            # ------------------------------If Refund Failed------------------------------------
            if refund["status"] != "succeeded"
              return {success: false,message: "Refund Unsuccessful", bank_response: refund}

            end
          elsif payment_gateway_type == TypesEnumLib::GatewayType::Converge
            elavon = Payment::ElavonGateway.new
            refund = elavon.refund(transaction_id, bank_amount, payment_gateway)
            if refund["message"].present?
              return {success: false,message: "Refund Unsuccessful", bank_response: refund}
            end
          elsif payment_gateway_type == TypesEnumLib::GatewayType::ICanPay || payment_gateway_type == TypesEnumLib::GatewayType::PaymentTechnologies
            iCanPay = Payment::ICanPay.new(payment_gateway)
            refund = iCanPay.refund(transaction_id, bank_amount)
            if refund["message"].present?
              return {success: false,message: "Refund Unsuccessful", bank_response: refund}
            end
          elsif payment_gateway_type == TypesEnumLib::GatewayType::BoltPay && bank_amount > 0
            # -----------------------------Bolt Pay Refund-----------------------------------
            bolt_pay = Payment::BoltPayGateway.new
            refund = bolt_pay.refund(transaction_id, bank_amount, payment_gateway)
            if refund[:message].present?
              return {success: false,message: "Refund Unsuccessful", bank_response: refund}
            end
          elsif payment_gateway_type == TypesEnumLib::GatewayType::FluidPay
            # -----------------------------Fluid Pay Refund-----------------------------------
            fluid_pay = FluidPayPayment.new
            fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
            if !([I18n.t("statuses.settled"),I18n.t("statuses.refunded"),I18n.t("statuses.partially_refunded")].include?(fluid_transaction_status))
              if bank_amount.to_f == total_issue_amount.to_f
                refund = fluid_pay.voidFluidPay(transaction_id,payment_gateway)
              else
                return {success: false,message: TypesEnumLib::ErrorMessage::PartialRefund, bank_response: refund}
              end
            else
              refund = fluid_pay.refundFluidAmount(transaction_id,dollars_to_cents(bank_amount),payment_gateway)
            end
            # ------------------------------If Refund Failed------------------------------------
            puts "==============fluid-pay-refund=================="
            puts refund
            puts "==============fluid-pay-refund=================="
            # ------------------------------If Refund Failed------------------------------------
            if refund.present? && refund["data"].present? &&
                refund["data"]["status"].present? &&
                refund["data"]["response"].try(:downcase) != I18n.t("statuses.approved")
              return {success: false, message: "Refund Unsuccessful", bank_response: refund}
            end

            if refund["status"] != "success"
              if refund["msg"].present?
                return {success: false,message: "#{refund['msg']}"}
              else
                return {success: false,message: "Refund Unsuccessful", bank_response: refund}
              end
            end
            refund[:id] = refund.try(:[],"data").try(:[],"id")
          elsif payment_gateway_type == TypesEnumLib::GatewayType::KnoxPayments
            knox = Payment::KnoxGateway.new(payment_gateway)
            refund = knox.refund(transaction_id, bank_amount)
            if refund[:message].present?
              return {success: false,message: "Refund Unsuccessful", bank_response: refund}
            end
          elsif  payment_gateway_type == TypesEnumLib::GatewayType::TotalPay
            total_pay = Payment::TotalPay.new
            refund = total_pay.refund(transaction_id, bank_amount,payment_gateway)
            if refund[:message].present?
              return [false, "Refund Unsuccessful", "empty", refund]
            end
          elsif  payment_gateway_type == TypesEnumLib::GatewayType::MentomPay
            mentom_pay = Payment::MentomPay.new(payment_gateway)
            refund = mentom_pay.refund(transaction_id, bank_amount)
            if refund[:message].present?
              return {success: false,message: "Refund Unsuccessful", bank_response: refund}
            end
          else
            return {success: false, message: "Refund Unsuccessful Payment Method Not Supported", bank_response: refund}
          end
          bank_details = {
              transaction_id: transaction_id,
              amount: dollars_to_cents(@db_transaction.total_amount),
              last4: card.last4
          }
          refund_details = {
              parent_transaction_id: @seq_transaction_id,
              parent_db_transaction_id: @db_transaction.id,
              request_id: @qr_request.id,
              reason: @reason
          }
          location = Location.where(id: @db_transaction.tags.try(:[], "location").try(:[], "id")).last
          gateway_name = payment_gateway.try(:name)
          if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
            gateway_name = @db_transaction.tags.try(:[], "source").present? ? @db_transaction.tags["source"] : gateway_name
          end
          retire_tags = {
              "bank_details" => bank_details,
              "refund_details" => refund_details,
              "location" => @db_transaction.tags.try(:[], "location"),
              "merchant" => @db_transaction.tags.try(:[], "merchant"),
              "descriptor" => gateway_name
          }
          total_fee = 0
          if @p2c_refund_fee
            fee = location.fees.buy_rate.first
            if @db_transaction.sub_type == TypesEnumLib::TransactionSubType::QRDebitCard
              if (fee.debit_unredeemed_fee.present? && fee.debit_unredeemed_fee > 0) || (fee.debit_unredeemed_perc_fee.present? && fee.debit_unredeemed_perc_fee > 0)
                fee_dollar = fee.debit_unredeemed_fee
                fee_percentage = get_amount_to_percentage(@db_transaction.total_amount,fee.debit_unredeemed_perc_fee)
                total_fee = fee_dollar + fee_percentage
              end
            else
              if (fee.credit_unredeemed_fee.present? && fee.credit_unredeemed_fee > 0) || (fee.credit_unredeemed_perc_fee.present? && fee.credit_unredeemed_perc_fee > 0)
                fee_dollar = fee.credit_unredeemed_fee
                fee_percentage = get_amount_to_percentage(@db_transaction.total_amount,fee.credit_unredeemed_perc_fee)
                total_fee = fee_dollar + fee_percentage
              end
            end
          end
          descriptor = payment_gateway.try(:name) if payment_gateway.present?
          if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
            descriptor = @db_transaction.tags.try(:[],'descriptor').present? ? @db_transaction.tags["descriptor"] : descriptor
          end
          retire_refund = create_refund_transaction(@db_transaction.receiver_wallet_id, nil, bank_amount, total_fee, retire_tags, 'retire', 'refund_bank', payment_gateway, @db_transaction.sub_type, @current_user.try(:id), card.try(:id),card.try(:last4),card.try(:first6), refund.try(:id),total_fee)
          seq_retire = SequenceLib.retire(bank_amount,@db_transaction.receiver_wallet_id, 'refund_bank',total_fee,@db_transaction.sub_type,gateway_name,nil,nil,location,card.try(:last4),nil,{"gbox"=>{"amount"=>total_fee.to_f, "wallet"=>Wallet.qc_support.first.id}},nil,nil,nil,nil,bank_details,refund_details, nil, descriptor, nil, nil,nil, payment_gateway)
          return {succes: false,message: "Cannot Refund From Sequence"} if seq_retire.nil?
          if seq_retire.present?
            update_refund_transaction(retire_refund, seq_retire)
            create_refund_fee_tx(location,retire_refund) if @p2c_refund_fee
            #= creating block transaction
            transactions = parse_block_transactions(seq_retire.actions, seq_retire.timestamp)
            save_block_trans(transactions)
          end
          if @db_transaction.update(status: 'refunded',refund_reason: @reason)
            return {success: true, message: "Refunded successfully"}
          end
        else
          return {success: false, message: "Already Refunded."}
        end
      else
        {success: false, message: "No Transaction found"}
      end
    end

    def refund_debit_issue
      # fucntion for refunding debit issue transaction from qr scanner
      begin
        if @db_transaction.present?
          if @db_transaction.status != "refunded"
            user_balance = SequenceLib.balance(@db_transaction.receiver_wallet_id)
            if user_balance.to_f >= @db_transaction.total_amount.to_f
              card = @db_transaction.card
              if card.present? && card.card_type == "debit"
                if @partial_amount.present?
                  if @partial_amount != "0"
                    partial_amount = @partial_amount.to_f
                    event = Hash.new
                    temp = []
                    tamount = @db_transaction.total_amount.to_f - partial_amount.to_f
                    if tamount >= 0
                      if @db_transaction.refund_log.nil?
                        event = {:amount => partial_amount, :date => DateTime.now.utc, :sequence_id => @db_transaction.seq_transaction_id}
                        @db_transaction.refund_log = event.to_json
                      else
                        if JSON.parse(@db_transaction.refund_log).kind_of?(Array)
                          temp = JSON.parse(@db_transaction.refund_log)
                        else
                          temp << JSON.parse(@db_transaction.refund_log)
                        end
                        previousAmount = temp.sum{|h| h["amount"]}
                        tamount = previousAmount + partial_amount
                        if tamount > @db_transaction.total_amount.to_f.to_f
                          return {success: false, message: "Amount Cannot be refunded"}
                        end
                        if partial_amount <= @db_transaction.total_amount.to_f
                          event = {:amount => partial_amount, :date => DateTime.now.utc, :sequence_id => @db_transaction.seq_transaction_id}
                          temp << event
                          @db_transaction.refund_log = temp.to_json
                        else
                          return {success: false, message: "Amount Cannot be Refunded"}
                        end
                      end
                    else
                      return {success: false, message: "Amount Cannot be Refunded"}
                    end
                  end
                end
                if @db_transaction.main_type == TypesEnumLib::TransactionViewTypes::QrDebitCard
                  @current_user = @db_transaction.sender
                end
                card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, @current_user.id)
                last_4 = card.last4
                first_6 = card.first6
                card_detail = {
                    number: card_info.number,
                    month: card_info.month,
                    year: card_info.year
                }
                url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/instant")
                http = Net::HTTP.new(url.host,url.port)
                http.use_ssl = true
                http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                request = Net::HTTP::Post.new(url)
                card_number= card_detail[:number]
                expiry_date = "#{card_detail[:year]}-#{card_detail[:month]}"
                if expiry_date.to_s.size > 7
                  exp_year = expiry_date.split('-').first
                  exp_month = expiry_date.split('-').last
                  if exp_year.size > 4
                    expiry_date = "#{exp_year.last(4)}-#{exp_month}"
                  end
                end
                request.body = {name: @current_user.name, recipient: @current_user.email, amount: @db_transaction.total_amount, description: "auto refund of debit transaction", card_number: card_number, expiration_date: expiry_date, zip: @current_user.zip_code}.to_json
                request["Authorization"] = "#{ENV['CHECKBOOK_KEY']}:#{ENV['CHECKBOOK_SECRET']}"
                request["Content-Type"] ='application/json'
                response = http.request(request)
                case response
                when Net::HTTPSuccess
                  digital_check = JSON.parse(response.body)
                  if response.message == "CREATED"
                    send_check_user_info = {
                        name: @current_user.name,
                        check_email: @current_user.email,
                        amount: @db_transaction.amount.to_f,
                        description: "auto refund of debit transaction",
                        fee: 0,
                        main_transaction_id: @db_transaction.id,
                        checkbook_info: {
                            checkId: digital_check["id"],
                            check_number: digital_check["number"],
                            check_catalog_image: digital_check["image_uri"],
                            check_status: digital_check["status"]
                        }
                    }

                    refund_details = {
                        parent_transaction_id: @db_transaction.seq_transaction_id,
                        parent_db_transaction_id: @db_transaction.seq_transaction_id,
                        request_id: @qr_request.id,
                        reason: "auto refund of debit transaction"
                    }
                    tags = {
                        "location" => @db_transaction.tags.try(:[], "location"),
                        "merchant" => @db_transaction.tags.try(:[], "merchant"),
                        "send_check_user_info" => send_check_user_info,
                        "send_via" => "instant_pay",
                        "refund_details" => refund_details
                    }
                    location = Location.where(id: @db_transaction.tags.try(:[], "location").try(:[], "id")).last
                    total_fee = 0
                    if @p2c_refund_fee
                      fee = location.fees.buy_rate.first
                      if (fee.debit_unredeemed_fee.present? && fee.debit_unredeemed_fee > 0) || (fee.debit_unredeemed_perc_fee.present? && fee.debit_unredeemed_perc_fee > 0)
                        fee_dollar = fee.debit_unredeemed_fee
                        fee_percentage = get_amount_to_percentage(@db_transaction.total_amount,fee.debit_unredeemed_perc_fee)
                        total_fee = fee_dollar + fee_percentage
                      end
                    end
                    transaction = create_refund_transaction(@db_transaction.receiver_wallet_id, nil, @db_transaction.total_amount.to_f, total_fee.to_f, tags, 'retire', 'refund_bank', @db_transaction.payment_gateway_id, 'qr_debit_card', @current_user.try(:id), card.try(:id),card.try(:last4),card.try(:first6), nil,total_fee)
                    withdraw = SequenceLib.retire(@db_transaction.total_amount.to_f,@db_transaction.receiver_wallet_id, "refund_bank",total_fee.to_f,"qr_debit_card",TypesEnumLib::GatewayType::Checkbook,nil,location.merchant,location,last_4,"instant_pay",{"gbox"=>{"amount"=>total_fee.to_f, "wallet"=>Wallet.qc_support.first.id}},send_check_user_info,nil,nil,nil,nil,refund_details, nil, TypesEnumLib::GatewayType::Checkbook)
                    if withdraw.present? && withdraw["actions"].present?
                      transaction.seq_transaction_id = withdraw["actions"].first["id"]
                      transaction.status = "approved"
                      transaction.tags = withdraw["actions"].first.tags
                      transaction.sub_type = withdraw["actions"].first.tags["sub_type"]
                      transaction.timestamp = withdraw["timestamp"]
                      transaction.save
                      create_refund_fee_tx(location,transaction) if @p2c_refund_fee
                      send_check_user_info = send_check_user_info.merge({refund_transaction_id: transaction.id})
                      db_tags = @db_transaction.tags.merge({"send_check_user_info" => send_check_user_info})
                      @db_transaction.update(status: 'refunded', tags: db_tags,refund_reason: "auto refund of debit transaction")
                      #= creating block transaction
                      parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
                      save_block_trans(parsed_transactions) if parsed_transactions.present?
                      return {success: true, message: "Successfully sent a Push to Card to #{ last_4 }.", check_info: send_check_user_info.to_json, refund_transaction_id: transaction.id}
                    else
                      return {success: false, message: I18n.t('merchant.controller.unaval_feature')}
                    end
                  else
                    return {success: false, message: "Something went wrong, please contact your administrator."}
                  end
                when Net::HTTPUnauthorized
                  error = JSON.parse(response.body)
                  return {success: false, message: error["error"]}
                when Net::HTTPNotFound
                  error = JSON.parse(response.body)
                  return {success: false, message: error["error"]}
                when Net::HTTPServerError
                  error = JSON.parse(response.body)
                  return {success: false, message: error["error"]}
                else
                  error = JSON.parse(response.body)
                  return {success: false, message: error["error"]}
                end
              else
                return {success: false, message: "Card is not debit"}
              end
            else
              return {success: false, message: I18n.t('merchant.controller.insufficient_balance')}
            end
          else
            return {success: false, message: "Already Refunded."}
          end
        else
          {success: false, message: "No Transaction found"}
        end
      rescue => e
        {success: false, message: e.message}
      end
    end

    def refund_issue_transaction(transaction, transaction_id, reason_detail, partial_amount = nil, previousAmount = nil, fee_splits = nil, refund_fee = nil, payment_gateway  = nil)
      if partial_amount == nil
        if @db_transaction.refund_log.present?
          refund_hash = JSON.parse(@db_transaction.refund_log)
          if refund_hash.class == Hash
            amount = @db_transaction.tags["previous_issue"]["issue_amount"].to_f - refund_hash["amount"]
          else
            amount = @db_transaction.tags["previous_issue"]["issue_amount"].to_f - refund_hash.sum{|h| h["amount"]}
          end
        else
          amount = @db_transaction.tags["previous_issue"]["issue_amount"].to_f if @db_transaction.tags["api_type"].downcase=="credit" || @db_transaction.tags["api_type"].downcase=="debit"
        end
      else
        amount = partial_amount
      end
      card_token = CardToken.new
      payment_gateway = PaymentGateway.find_by(key: @db_transaction.tags["source2"]) if payment_gateway.blank?
      payment_gateway_type = payment_gateway.type if payment_gateway.present?
      # TODO only one check for each gateway type should be here
      if  payment_gateway_type == TypesEnumLib::GatewayType::Stripe
        # -----------------------------Stripe Refund-----------------------------------
        refund = card_token.stripe_refund(transaction_id, amount, payment_gateway)
        # ------------------------------If Refund Failed------------------------------------
        if refund["status"] != "succeeded"
          return [false,'Refund Unsuccessful',"empty",refund]

        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::Converge
        elavon = Payment::ElavonGateway.new
        refund = elavon.refund(transaction_id, amount, payment_gateway)
        if refund["message"].present?
          return [false, "Refund Unsuccessful", "empty",refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::ICanPay || payment_gateway_type == TypesEnumLib::GatewayType::PaymentTechnologies
        iCanPay = Payment::ICanPay.new(payment_gateway)
        refund = iCanPay.refund(transaction_id, amount)
        if refund["message"].present?
          return [false, "Refund Unsuccessful", "empty",refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::BoltPay && amount > 0
        # -----------------------------Bolt Pay Refund-----------------------------------
        bolt_pay = Payment::BoltPayGateway.new
        refund = bolt_pay.refund(transaction_id, amount, payment_gateway)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty",refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::FluidPay
        # -----------------------------Fluid Pay Refund-----------------------------------
        fluid_pay = FluidPayPayment.new
        fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
        if !([I18n.t("statuses.settled"),I18n.t("statuses.refunded"),I18n.t("statuses.partially_refunded")].include?(fluid_transaction_status))
          if amount.to_f == @db_transaction.tags["previous_issue"]["issue_amount"].to_f
            refund = fluid_pay.voidFluidPay(transaction_id,payment_gateway)
          else
            return [false,TypesEnumLib::ErrorMessage::PartialRefund,"empty",refund]
          end
        else
          refund = fluid_pay.refundFluidAmount(transaction_id,dollars_to_cents(amount),payment_gateway)
        end
        puts "==============fluid-pay-refund=================="
        puts refund
        puts "==============fluid-pay-refund=================="
        # ------------------------------If Refund Failed------------------------------------
        if refund.present? && refund["data"].present? &&
            refund["data"]["status"].present? &&
            refund["data"]["response"].try(:downcase) != I18n.t("statuses.approved")
          return [false,"Refund Unsuccessful","empty",refund]
        end
        if refund["status"] != "success"
          if refund["msg"].present?
            return [false,"#{refund['msg']}"]
          else
            return [false,"Refund Unsuccessful", "empty",refund]
          end
        end
        refund[:id] = refund.try(:[],"data").try(:[],"id")
      elsif payment_gateway_type == TypesEnumLib::GatewayType::KnoxPayments
        knox = Payment::KnoxGateway.new(payment_gateway)
        refund = knox.refund(transaction_id, amount)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty",refund]
        end
      elsif  payment_gateway_type == TypesEnumLib::GatewayType::TotalPay
        total_pay = Payment::TotalPay.new
        refund = total_pay.refund(transaction_id, amount,payment_gateway)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty",refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::MentomPay
        mentom_pay = Payment::MentomPay.new(payment_gateway)
        refund = mentom_pay.refund(transaction_id, amount)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      else
        return [false,"Refund Unsuccessful Payment Method Not Supported"]
      end
      lastFour = @db_transaction.tags["last4"] ? @db_transaction.tags["last4"] : "NA"
      bank_details = {
          transaction_id: transaction_id,
          amount: dollars_to_cents(amount),
          last4: lastFour
      }
      refund_details = {
          parent_transaction_id: transaction[:id],
          reason: reason_detail
      }
      gateway_name=payment_gateway.try(:name)
      if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
        gateway_name = @db_transaction.tags.try(:[], "source").present? ? @db_transaction.tags["source"] : gateway_name
      end
      tags = {
          "bank_details" => bank_details,
          "refund_details" => refund_details,
          "fee_perc" => fee_splits,
          "descriptor" => gateway_name,
          "location" => @db_transaction.tags.try(:[], "location"),
          "merchant" => @db_transaction.tags.try(:[], "merchant")
      }
      gbox_fee = save_gbox_fee(tags["fee_perc"], refund_fee, "refund")
      total_fee = save_total_fee(tags["fee_perc"], refund_fee, "refund")
      retire_refund = create_refund_transaction(@db_transaction.receiver_wallet_id, nil, amount, total_fee.to_f, tags, 'retire', 'refund', payment_gateway,nil,@current_user.try(:id), @db_transaction.try(:card).try(:id), @db_transaction.try(:card).try(:last4), @db_transaction.try(:card).try(:first6), refund.try(:id), gbox_fee)
      source = @db_transaction.tags["source"] ? @db_transaction.tags["source"].downcase : "NA"
      #----------------------------Perform Sequence Retire Refund--------------------------------
      descriptor = payment_gateway.try(:name)
      if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
        descriptor = @db_transaction.tags.try(:[],'descriptor').present? ? @db_transaction.tags["descriptor"] : descriptor
      end
      seq_transaction = SequenceLib.retire(amount,@db_transaction.receiver_wallet_id, 'refund',refund_fee,0,source,nil,@db_transaction.receiver,@db_transaction.receiver_wallet.try(:location),lastFour,nil,fee_splits,nil,nil,nil,nil,bank_details,refund_details, nil, descriptor, nil, nil,nil, payment_gateway)
      return [false,"Cannot Refund From Sequence","empty",refund] if seq_transaction.nil?
      if seq_transaction.present?
        update_refund_transaction(retire_refund, seq_transaction)
        update_log(@db_transaction,seq_transaction["actions"].first["id"])
        #= creating block transaction
        transactions = parse_block_transactions(seq_transaction.actions, seq_transaction.timestamp)
        save_block_trans(transactions)
        if partial_amount.nil? || (!previousAmount.nil?) && (previousAmount + partial_amount == @db_transaction.total_amount.to_f)
          if @db_transaction.update(status: 'refunded',refund_reason: reason_detail, refunded_by: @current_user.try(:id))
            return [true,"Successfully Refunded", seq_transaction.try(:actions).try(:first).try(:id),refund]
          end
        elsif previousAmount.nil? && @db_transaction.total_amount.to_f - partial_amount == 0
          if @db_transaction.update(status: 'refunded',refund_reason: reason_detail, refunded_by: @current_user.try(:id))
            return [true,"Successfully Refunded", seq_transaction.try(:actions).try(:first).try(:id),refund]
          end
        else
          @db_transaction.update(status: 'complete',refund_reason: reason_detail, refunded_by: @current_user.try(:id))
          return [true,"successfully partially refunded", seq_transaction.try(:actions).try(:first).try(:id),refund]
        end
      end
    end

    # ----------------------------- Refund Sequence --------------------------------------
    def refund_sequence_transaction(transaction,reason_detail,partial_amount = nil,previousAmount= nil,fee_splits=nil,refund_fee=nil, payment_gateway = nil)
      if partial_amount.present?
        transaction_amount = partial_amount.to_f
      else
        transaction_amount = @db_transaction.total_amount.to_f
      end
      refund_details = {
          parent_transaction_id: transaction[:id],
          reason: reason_detail
      }
      if transaction_amount > 0
        tags = {
            "refund_details" => refund_details,
            "fee_perc" => fee_splits,
            "location" => @db_transaction.tags.try(:[], "location"),
            "merchant" => @db_transaction.tags.try(:[], "merchant")
        }
        gbox_fee = save_gbox_fee(tags["fee_perc"], refund_fee)
        total_fee = save_total_fee(tags["fee_perc"], refund_fee)
        transfer_refund = create_refund_transaction(@db_transaction.receiver_wallet_id, @db_transaction.sender_wallet_id,transaction_amount, total_fee.to_f, tags, "transfer", 'refund', payment_gateway, nil, @current_user.try(:id), @db_transaction.try(:card).try(:id), @db_transaction.try(:card).try(:last4), @db_transaction.try(:card).try(:first6), nil, gbox_fee)
        seq_transaction = SequenceLib.transfer(transaction_amount,@db_transaction.receiver_wallet_id, @db_transaction.sender_wallet_id,'refund',refund_fee,nil,TypesEnumLib::GatewayType::Quickcard,nil,@db_transaction.receiver,@db_transaction.receiver_wallet.try(:location),fee_splits,nil,nil,nil,nil,nil,nil,nil,nil,nil,refund_details,nil,nil,nil, payment_gateway)
        return [false,"Cannot Refund From Sequence"] if seq_transaction.nil?
        if seq_transaction.present? && seq_transaction["actions"].first["id"].present?
          update_log(@db_transaction,seq_transaction["actions"].first["id"])
          update_refund_transaction(transfer_refund, seq_transaction)
          #= creating block transaction
          transactions = parse_block_transactions(seq_transaction.actions, seq_transaction.timestamp)
          save_block_trans(transactions)
        end
        user = @current_user
        # transaction[:amount_updated] = transaction[:amount_updated].to_f + db_transaction.tip.to_f
        if partial_amount.nil? || (!previousAmount.nil?) && (previousAmount + partial_amount == @db_transaction.total_amount.to_f)
          if @db_transaction.update(status: 'refunded',refund_reason: reason_detail, refunded_by: user.try(:id))
            return [true,"Successfully Refunded", seq_transaction.try(:actions).try(:first).try(:id)]
          end
        elsif previousAmount.nil? && @db_transaction.total_amount.to_f - partial_amount == 0
          if @db_transaction.update(status: 'refunded',refund_reason: reason_detail, refunded_by: user.try(:id))
            return [true,"Successfully Refunded", seq_transaction.try(:actions).try(:first).try(:id)]
          end
        else
          @db_transaction.update(status: 'complete',refund_reason: reason_detail, refunded_by: user.try(:id))
          return [true,"successfully partially refunded", seq_transaction.try(:actions).try(:first).try(:id)]
        end
      else
        return [false,"Refund Unsuccessful"]
      end
    end

    def refund_transaction(transaction,transaction_id,reason_detail,partial_amount = nil , previousAmount = nil,fee_splits = nil, refund_fee = nil, payment_gateway = nil)
      card_token = CardToken.new
      if partial_amount == nil
        if @db_transaction.refund_log.present?
          refund_hash = JSON.parse @db_transaction.refund_log
          if refund_hash.class == Hash
            transaction_amount = @db_transaction.total_amount.to_f - refund_hash["amount"].to_f
          else
            transaction_amount = @db_transaction.total_amount.to_f - refund_hash.sum{|h| h["amount"]}
          end
        else
          transaction_amount = @db_transaction.total_amount.to_f
        end
      else
        transaction_amount = partial_amount.to_f
      end
      bank_amount = total_issue_amount = @db_transaction.tags["previous_issue"]["issue_amount"].to_f
      p "---------------- #{transaction_amount} ----------------- #{bank_amount}"
      if transaction_amount <= bank_amount
        bank_amount = transaction_amount
      end
      payment_gateway = PaymentGateway.find_by(key: @db_transaction.tags["source2"]) if payment_gateway.blank?
      payment_gateway_type = payment_gateway.type if payment_gateway.present?
      # TODO only one check for each gateway type should be here
      if [TypesEnumLib::TransactionType::InvoiceRTP.try(:downcase),TypesEnumLib::TransactionType::RTP.try(:downcase)].include?(@db_transaction.main_type.try(:downcase))
        refund = refund_rtp(bank_amount)
        unless refund[:success]
          return [false, refund[:message],"empty", refund]
        end
      elsif @db_transaction.main_type.try(:downcase) == TypesEnumLib::TransactionViewTypes::QrDebitCard.try(:downcase)
        @current_user = @db_transaction.sender
        refund = refund_debit(bank_amount)
        unless refund[:success]
          return [false,"Refund Unsuccessful","empty", refund]
        end
      elsif (payment_gateway_type == TypesEnumLib::GatewayType::Stripe)  && bank_amount > 0
        # -----------------------------Stripe Refund-----------------------------------
        refund = card_token.stripe_refund(transaction_id, bank_amount, payment_gateway)
        # ------------------------------If Refund Failed------------------------------------
        if refund["status"] != "succeeded"
          return [false,"Refund Unsuccessful","empty", refund]
        end
      elsif (payment_gateway_type == TypesEnumLib::GatewayType::Converge )  && bank_amount > 0
        elavon = Payment::ElavonGateway.new
        refund = elavon.refund(transaction_id, bank_amount,payment_gateway)
        if refund["message"].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::BoltPay && bank_amount > 0
        # -----------------------------Bolt Pay Refund-----------------------------------
        bolt_pay = Payment::BoltPayGateway.new
        refund = bolt_pay.refund(transaction_id, bank_amount, payment_gateway)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      elsif (payment_gateway_type == TypesEnumLib::GatewayType::ICanPay || payment_gateway_type == TypesEnumLib::GatewayType::PaymentTechnologies ) && bank_amount > 0
        # -----------------------------I Can Pay Refund-----------------------------------
        iCanPay = Payment::ICanPay.new(payment_gateway)
        refund = iCanPay.refund(transaction_id, bank_amount)
        if refund["message"].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::FluidPay  && bank_amount > 0
        # -----------------------------Fluid Pay Refund-----------------------------------
        fluid_pay = FluidPayPayment.new
        fluid_transaction_status = fluid_pay.check_fluid_pay_api_status(transaction_id,payment_gateway)
        if !([I18n.t("statuses.settled"),I18n.t("statuses.refunded"),I18n.t("statuses.partially_refunded")].include?(fluid_transaction_status))
          if bank_amount.to_f == total_issue_amount.to_f
            refund = fluid_pay.voidFluidPay(transaction_id,payment_gateway)
          else
            return [false,TypesEnumLib::ErrorMessage::PartialRefund,"empty", refund]
          end
        else
          refund = fluid_pay.refundFluidAmount(transaction_id,dollars_to_cents(bank_amount),payment_gateway)
        end
        # ------------------------------If Refund Failed------------------------------------
        puts "==============fluid-pay-refund=================="
        puts refund
        puts "==============fluid-pay-refund=================="
        # ------------------------------If Refund Failed------------------------------------
        if refund.present? && refund["data"].present? &&
            refund["data"]["status"].present? &&
            refund["data"]["response"].try(:downcase) != I18n.t("statuses.approved")
          return [false,"Refund Unsuccessful","empty", refund]
        end
        if refund["status"] != "success"
          if refund["msg"].present?
            return [false,"#{refund['msg']}","empty", refund]
          else
            return [false,"Refund Unsuccessful","empty", refund]
          end
        end
        refund[:id] = refund.try(:[],"data").try(:[],"id")
      elsif payment_gateway_type == TypesEnumLib::GatewayType::KnoxPayments  && bank_amount > 0
        knox = Payment::KnoxGateway.new(payment_gateway)
        refund = knox.refund(transaction_id, bank_amount)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      elsif  payment_gateway_type == TypesEnumLib::GatewayType::TotalPay && bank_amount > 0
        total_pay = Payment::TotalPay.new
        refund = total_pay.refund(transaction_id, bank_amount,payment_gateway)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      elsif payment_gateway_type == TypesEnumLib::GatewayType::MentomPay && bank_amount > 0
        mentom_pay = Payment::MentomPay.new(payment_gateway)
        refund = mentom_pay.refund(transaction_id, bank_amount)
        if refund[:message].present?
          return [false, "Refund Unsuccessful", "empty", refund]
        end
      elsif bank_amount > 0
        return [false,"Refund Unsuccessful Payment Method Not Supported"]
      end
      #----------------------------Perform Sequence Refund--------------------------------
      if @db_transaction.total_amount.to_f > 0
        lastFour = @db_transaction.tags["last4"].present? ? @db_transaction.tags["last4"] : "NA"
        if @db_transaction.tags["previous_issue"].present? && lastFour == "NA"
          lastFour = @db_transaction.tags["previous_issue"]["last4"].present? ? @db_transaction.tags["previous_issue"]["last4"] : "NA"
        end
        bank_details = {
            transaction_id: transaction_id,
            amount: dollars_to_cents(@db_transaction.tags["previous_issue"]["issue_amount"].to_f),
            last4: lastFour
        }
        refund_details = {
            parent_transaction_id: transaction.present? ? transaction[:id] : @db_transaction.try(:seq_transaction_id),
            reason: reason_detail
        }
        source = @db_transaction.tags["source"] ? @db_transaction.tags["source"] : "NA"
        if previousAmount.present? && partial_amount.to_f.present? && (previousAmount.to_f == 0)
          @db_transaction.status = "refunded"
          @db_transaction.save
          return [true,"already refunded","empty", refund]
        end
        gateway_name = payment_gateway.try(:name)
        if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
          gateway_name = @db_transaction.tags["source"].present? ? @db_transaction.tags["source"] : gateway_name
        end
        tags = {
            "bank_details" => bank_details,
            "refund_details" => refund_details,
            "fee_perc" => fee_splits,
            "descriptor" => gateway_name,
            "location" => @db_transaction.tags.try(:[], "location"),
            "merchant" => @db_transaction.tags.try(:[], "merchant")
        }
        gbox_fee = save_gbox_fee(tags["fee_perc"], refund_fee, "refund")
        total_fee = save_total_fee(tags["fee_perc"], refund_fee, "refund")
        transfer_refund = create_refund_transaction(@db_transaction.receiver_wallet_id, @db_transaction.sender_wallet_id,transaction_amount, total_fee.to_f, tags, "transfer", 'refund', payment_gateway,nil, @current_user.try(:id), @db_transaction.try(:card).try(:id),@db_transaction.try(:card).try(:last4), @db_transaction.try(:card).try(:first6), refund.try(:id), gbox_fee)
        seq_transaction = SequenceLib.transfer(transaction_amount,@db_transaction.receiver_wallet_id, @db_transaction.sender_wallet_id,'refund',refund_fee,nil,TypesEnumLib::GatewayType::Quickcard,nil,@db_transaction.receiver,@db_transaction.receiver_wallet.try(:location),fee_splits,nil,nil,nil,nil,nil,nil,nil,nil,bank_details,refund_details,nil,nil,nil,payment_gateway)
        if seq_transaction.present?
          update_refund_transaction(transfer_refund, seq_transaction)
          update_log(@db_transaction,seq_transaction["actions"].first["id"])
          #= creating block transaction
          transactions = parse_block_transactions(seq_transaction.actions, seq_transaction.timestamp)
          save_block_trans(transactions)
          retire_tags = {
              "bank_details" => bank_details,
              "refund_details" => refund_details,
              "descriptor" => gateway_name,
              "location" => @db_transaction.tags.try(:[], "location"),
              "merchant" => @db_transaction.tags.try(:[], "merchant")
          }
          descriptor = payment_gateway.try(:name) if payment_gateway.present?
          if payment_gateway.present? && (payment_gateway.knox_payments? || payment_gateway.payment_technologies? || payment_gateway.total_pay?)
            descriptor = @db_transaction.tags["descriptor"].present? ? @db_transaction.tags["descriptor"] : descriptor
          end
          if @db_transaction.main_type == "Qr Credit Card" || @db_transaction.main_type == "Qr Debit Card"
            issue_tx_seq_id = @db_transaction.tags.try(:[], "previous_issue").try(:[], "id")
            if issue_tx_seq_id.present?
              issue_tx_id = Transaction.find_by(seq_transaction_id: issue_tx_seq_id).try(:id)
              if issue_tx_id.present?
                request_id = QrCard.find_by(transaction_id: issue_tx_id).try(:request_id)
                refund_details[:request_id] = request_id if request_id.present?
              end
            end
          end
          retire_refund = create_refund_transaction(@db_transaction.sender_wallet_id, nil, bank_amount, 0, retire_tags, 'retire', 'refund_bank', payment_gateway, nil, @current_user.try(:id), @db_transaction.try(:card).try(:id), @db_transaction.try(:card).try(:last4), @db_transaction.try(:card).try(:first6), refund.try(:id))
          seq_retire = SequenceLib.retire(bank_amount,@db_transaction.sender_wallet_id, 'refund_bank',0,0,source,nil,@db_transaction.receiver,@db_transaction.receiver_wallet.try(:location),lastFour,nil,nil,nil,nil,nil,nil,bank_details,refund_details, nil, descriptor, nil, nil,nil, payment_gateway)
          return [false,"Cannot Refund From Sequence", "empty", refund] if seq_retire.nil?
          if seq_retire.present?
            update_refund_transaction(retire_refund, seq_retire)
            #= creating block transaction
            transactions = parse_block_transactions(seq_retire.actions, seq_retire.timestamp)
            save_block_trans(transactions)
          end
          if partial_amount.nil? || (!previousAmount.nil?) && (previousAmount + partial_amount == @db_transaction.total_amount.to_f)
            if @db_transaction.update(status: 'refunded',refund_reason: reason_detail, refunded_by: @current_user.try(:id),refund_id: transfer_refund.id)
              return [true,"Successfully Refunded", seq_retire.try(:actions).try(:first).try(:id), refund]
            end
          elsif previousAmount.nil? && @db_transaction.total_amount.to_f - partial_amount ==0
            if @db_transaction.update(status: 'refunded',refund_reason: reason_detail, refunded_by:  @current_user.try(:id),refund_id: transfer_refund.id)
              return [true,"Successfully Refunded", seq_retire.try(:actions).try(:first).try(:id), refund]
            end
          else
            @db_transaction.update(status: 'complete',refund_reason: reason_detail, refunded_by: @current_user.try(:id),refund_id: transfer_refund.id)
            return [true,"successfully partially refunded", seq_retire.try(:actions).try(:first).try(:id), refund]
          end
        else
          return [false,"Cannot Refund From Sequence","empty", refund]
        end
      else
        return [false,"Refund Unsuccessful","empty", refund]
      end
      #----------------------------Perform Sequence Refund End----------------------------
    end

    def refund_debit(bank_amount)
      # return {success: true, message: "success",id: "bilal_refund"}
      begin
        card = @db_transaction.card
        card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, card.user_id)
        last_4 = card.last4
        first_6 = card.first6
        card_detail = {
            number: card_info.number,
            month: card_info.month,
            year: card_info.year
        }
        url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/instant")
        http = Net::HTTP.new(url.host,url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
        request = Net::HTTP::Post.new(url)
        card_number= card_detail[:number]
        expiry_date = "#{card_detail[:year]}-#{card_detail[:month]}"
        if expiry_date.to_s.size > 7
          exp_year = expiry_date.split('-').first
          exp_month = expiry_date.split('-').last
          if exp_year.size > 4
            expiry_date = "#{exp_year.last(4)}-#{exp_month}"
          end
        end
        request.body = {name: @current_user.name.present? ? @current_user.name : @merchant.name, recipient: @current_user.email.present? ? @current_user.email : @merchant.email, amount: bank_amount, description: "refund from quickcard", card_number: card_number, expiration_date: expiry_date, zip: card.user.present? ? card.user.zip_code : @current_user.zip_code}.to_json
        request["Authorization"] = "#{ENV['CHECKBOOK_KEY']}:#{ENV['CHECKBOOK_SECRET']}"
        request["Content-Type"] ='application/json'
        response = http.request(request)
        case response
        when Net::HTTPSuccess
          digital_check = JSON.parse(response.body)
          if response.message == "CREATED"
            # checkbook_info = {
            #     checkId: digital_check["id"],
            #     check_number: digital_check["number"],
            #     check_catalog_image: digital_check["image_uri"],
            #     check_status: digital_check["status"]
            # }
            return {success: true, message: "success",id: digital_check["id"]}
          else
            return {success: false, message: "Something went wrong, please contact your administrator."}
          end
        when Net::HTTPUnauthorized
          error = JSON.parse(response.body)
          return {success: false, message: error["error"]}
        when Net::HTTPNotFound
          error = JSON.parse(response.body)
          return {success: false, message: error["error"]}
        when Net::HTTPServerError
          error = JSON.parse(response.body)
          return {success: false, message: error["error"]}
        else
          error = JSON.parse(response.body)
          return {success: false, message: error["error"]}
        end
      rescue => e
        return {success: false, message: e.message}
      end
    end

    def refund_rtp(bank_amount)
      url = "https://www.debitway.com/integration/index.php"

      request_body = {
          "identifier" => "XJY0657627ny082",
          "action" => "refund",
          "transaction_id" => @db_transaction.charge_id,
          "comments" => @reason,
          "vericode"=> "Veriyg0neIHGez",
          "amount" => bank_amount
      }
      @response = RestClient.post(url,request_body,{content_type: :json, accept: :json})
      result = to_hash(@response.body)
      # result = {"result"=>"success", "transaction_id"=>"3512765871263"}
      if result["result"] == "success"
        return {success: true, id: result["transaction"], message: "Successfully Refunded"}
      elsif result["result"] == "failed"
        return {success: false, message: result["errors_meaning"]}
      else
        return {success: false, message: "Refunded Failed"}
      end

    end

    def to_hash(string)
      arr_sep=' '
      key_sep='='
      array = string.split(arr_sep)
      hash = {}

      array.each do |e|
        key_value = e.split(key_sep)
        hash[key_value[0]] = key_value[1]
      end

      error_meaning = hash["errors_meaning"]
      hash.each do|k,v|
        hash["#{k}"] = v.gsub("\"","") if v.present?
        error_meaning = "#{error_meaning} #{k}" if v.nil?
      end
      hash["errors_meaning"] = error_meaning.gsub("\"","") if error_meaning.present?


      return hash
    end

    def create_refund_fee_tx(location,retire_refund)
      refund_fee_tx = retire_refund.dup
      receiver_wallet = Wallet.escrow.first
      sender_wallet = location.wallets.primary.first
      source_user = location.merchant
      destination_user = receiver_wallet.users.first

      refund_fee_tx.to = receiver_wallet.id
      refund_fee_tx.from = sender_wallet.id
      refund_fee_tx.amount = refund_fee_tx.fee
      refund_fee_tx.sender_id = source_user.id
      refund_fee_tx.sender_name = source_user.name
      refund_fee_tx.sender_wallet_id = sender_wallet.id
      refund_fee_tx.action = "transfer"
      refund_fee_tx.receiver_id = destination_user.id
      refund_fee_tx.receiver_wallet_id = receiver_wallet.id
      refund_fee_tx.receiver_balance = SequenceLib.balance(receiver_wallet.id)
      refund_fee_tx.sender_balance = SequenceLib.balance(sender_wallet.id)
      refund_fee_tx.total_amount = refund_fee_tx.fee
      refund_fee_tx.net_amount = refund_fee_tx.fee
      refund_fee_tx.main_type = "refund_fee"
      refund_fee_tx.tags["refund_details"]["parent_db_transaction_id"] = retire_refund.id
      refund_fee_tx.save
    end
  end
end