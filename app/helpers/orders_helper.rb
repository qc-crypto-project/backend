module OrdersHelper
  include ApplicationHelper

  def tracker_dates(detail)
    if detail.present?
      shipment_date = nil
      delivered_date = nil
      ship_status = nil
      detail.each do |obj|
        if obj["status"] == "pre_transit" && ship_status.blank?
          ship_status = obj["status"]
          shipment_date = obj["datetime"]
        elsif obj["status"] == "delivered"
          delivered_date = obj["datetime"]
        end
      end
      return [shipment_date,delivered_date]
    else
      return nil
    end
  end

  def tracker_status_view(value)
    if value == "cancel"
      return "Cancelled"
    else
      return value.titleize
    end
  end
  # def my_method
  #   Tracker.where.not(tracking_details: nil).where(shipment_date: nil , delivered_date: nil).find_each do |tracker|
  #     dates = tracker_dates(tracker.tracking_details)
  #     tracker.shipment_date = dates.first.to_datetime if dates.present? && dates.first.present?
  #     tracker.delivered_date = dates.second.to_datetime if dates.present? && dates.second.present?
  #     tracker.save
  #   end
  # end

  def create_fee_transaction(label=false)
    @transaction = Transaction.eager_load(receiver_wallet: :location).eager_load(:receiver).find(params[:t_id])
    from = @transaction.receiver_wallet_id
    to = Wallet.qc_support.first
    location = @transaction.receiver_wallet.location
    merchant = location.merchant
    location_fee = location.fees.buy_rate.first
    generate_label_fee = location_fee.generate_label_fee
    tracker_fee = location_fee.tracking_fee
    if tracker_fee.to_f > 0 || generate_label_fee.to_f > 0
      return if tracker_fee <= 0 && !label
      tr_type = get_transaction_type(TypesEnumLib::TransactionType::TrackingFee,nil)
      amount = tracker_fee
      data = {}
      data = {amount: generate_label_fee, type: get_transaction_type(TypesEnumLib::TransactionType::GenerateLabelFee,nil)} if generate_label_fee.to_f > 0
      if tracker_fee.to_f <= 0
        amount = generate_label_fee
        tr_type = data[:type]
        data = {}
      end
      db_amount = amount + data.try(:[], :amount).to_f
      db_tx = create_transaction_helper(merchant, to.id, from, merchant, to, "transfer", @transaction.card, 0, 0, 0, 0, db_amount, db_amount, {}, tr_type.first, tr_type.second, {}, {},nil, "Quickcard", get_ip)
      tx = SequenceLib.transfer(amount, from, to.id, tr_type.first, 0, nil, "Quickcard", nil, merchant, location, nil, nil,get_ip, nil, merchant.ledger, data,tr_type.second,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,data)
      if tx.present?
        parsed_transactions = parse_block_transactions(tx.actions, tx.timestamp)
        save_block_trans(parsed_transactions) if parsed_transactions.present?
        db_tx.update(seq_transaction_id: tx.actions.first.id,status: "approved", tags: tx.actions.first.tags, timestamp: tx[:timestamp])
      end
    end
  end

end
