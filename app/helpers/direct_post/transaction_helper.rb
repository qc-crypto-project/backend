module DirectPost::TransactionHelper
	
	class TransactionACH
		
		def initialize
			@login = {}
			@transaction_ach = {}
		end
		
		def setLogin(username,password)
			@login = {
				'username' => username,#*	Username assigned to merchant account. Also requires 'password'. Using the 'security_key' variable in the same request will result in an error
				'password' => password,#*	Password for the specified username. Also requires 'username'. Using the 'security_key' variable in the same request will result in an error
			}
		end

		def setAchTransaction(type,security_key,checkname,checkaba,checkaccount,payment,amount)
			@transaction_ach = {
				'type' => type,#*	The type of transaction to be processed. Values: 'sale', 'auth', 'credit', 'validate', or 'offline'
				'security_key' => security_key,#*	API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
				'checkname'=>  checkname,#***	The name on the customer's ACH account.
				'checkaba'=>  checkaba,#***	The customer's bank routing number.
				'checkaccount'=>  checkaccount,#***	The customer's bank account number.
				'payment' => payment, #The type of payment. Default: 'creditcard' Values: 'creditcard' or 'check'
				'amount' => amount
			}
		end

		def setAchTransactionWithVaultId(type,security_key,customer_vault_id,amount)
			@transaction_ach = {
				'type' => type,#*	The type of transaction to be processed. Values: 'sale', 'auth', 'credit', 'validate', or 'offline'
				'security_key' => security_key,#*	API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
				'customer_vault_id'	=> customer_vault_id, #Specifies a customer vault id. If not set, the payment gateway will randomly generate a customer vault id.
				'amount' => amount
			}
		end

		def setAchTransactionWithVaultIdAndOptional(type,security_key,customer_vault_id,account_holder_type,account_type,sec_code,amount)
			@transaction_ach = {
				'type' => type,#*	The type of transaction to be processed. Values: 'sale', 'auth', 'credit', 'validate', or 'offline'
				'security_key' => security_key,#*	API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
				'customer_vault_id'	=> customer_vault_id, #Specifies a customer vault id. If not set, the payment gateway will randomly generate a customer vault id.
				'account_holder_type' => account_holder_type,#	The type of ACH account the customer has. Values: 'business' or 'personal'
				'account_type' => account_type,#	The ACH account entity of the customer. Values: 'checking' or 'savings'
				'sec_code' => sec_code,#	The Standard Entry Class code of the ACH transaction. Values: 'PPD', 'WEB', 'TEL', or 'CCD'
				'amount' => amount
			}
		end

		def setActTransactionWithOptional(type,security_key,checkname,checkaba,checkaccount,account_holder_type,account_type,sec_code)
			@transaction_ach = {
				'type' => type,#*	The type of transaction to be processed. Values: 'sale', 'auth', 'credit', 'validate', or 'offline'
				'security_key' => security_key,#*	API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
				'checkname'=>  checkname,#***	The name on the customer's ACH account.
				'checkaba'=>  checkaba,#***	The customer's bank routing number.
				'checkaccount'=>  checkaccount,#***	The customer's bank account number.
				'account_holder_type' => account_holder_type,#	The type of ACH account the customer has. Values: 'business' or 'personal'
				'account_type' => account_type,#	The ACH account entity of the customer. Values: 'checking' or 'savings'
				'sec_code' => sec_code,#	The Standard Entry Class code of the ACH transaction. Values: 'PPD', 'WEB', 'TEL', or 'CCD'
				'amount' => amount
			}
		end

		def do_transaction()
			query  = ""
			query = query + "username=" + URI.escape(@login['username']) + "&"
			query += "password=" + URI.escape(@login['password']) + "&"
			@transaction_ach.each do | key,value|
				query += key +"=" + URI.escape(value) + "&"
			end
			return doPost(query)
		end

		def doPost(query)
			curlObj = Curl::Easy.new("https://secure.networkmerchants.com/api/transact.php")
			curlObj.connect_timeout = 30
			curlObj.timeout = 30
			curlObj.header_in_body = false
			curlObj.ssl_verify_peer = false
			curlObj.post_body = query
			curlObj.perform()
			data = curlObj.body_str

			data = 'https://secure.networkmerchants.com/api/transact.php?' + data
			uri = Addressable::URI.parse(data)
			@responses = uri.query_values
			return @responses
		end

		def getResponses()
			return @responses
		end

	end

end