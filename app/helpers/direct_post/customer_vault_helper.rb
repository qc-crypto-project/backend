module DirectPost::CustomerVaultHelper
	# "customer_vault_id"=>"347736111"
	class CustomerVaultACH
		def initialize
			@login = {}
			@customer_vault_ach = {}
		end

		def setLogin(username,password)
			@login = {
				'username' => username,#*Username assigned to merchant account. Also requires 'password'. Using the 'security_key' variable in the same request will result in an error
				'password' => password#*Password for the specified username. Also requires 'username'. Using the 'security_key' variable in the same request will result in an error
			}
		end

		def add_customer(security_key,checkname,checkaba,checkaccount,account_holder_type,account_type,sec_code,payment)
			@customer_vault_ach = {
				'customer_vault' => 'add_customer',# Add/Update a secure customer vault record. Values: 'add_customer' or 'update_customer'
				'security_key' => security_key,#API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
				'checkname' => checkname,#The name on the customer's ACH account.
				'checkaba' => checkaba,#The customer's bank routing number.
				'checkaccount' => checkaccount,#The customer's bank account number.
				'account_holder_type' => account_holder_type, #The customer's ACH account entity. Values: 'personal' or 'business'
				'account_type' => account_type, #The customer's ACH account type. Values: 'checking' or 'savings'
				'sec_code' => sec_code,# ACH standard entry class codes. Values: 'PPD', 'WEB', 'TEL', or 'CCD'
				'payment' => payment # Set payment type to ACH or credit card. Values: 'creditcard' or 'check'
			}
		end

		def update_customer(customer_vault_id,security_key,checkname,checkaba,checkaccount,account_holder_type,account_type,sec_code,payment)
			@customer_vault_ach = {
				'customer_vault' => 'update_customer',# Add/Update a secure customer vault record. Values: 'add_customer' or 'update_customer'
				'customer_vault_id' => customer_vault_id,#Specifies a customer vault id. If not set, the payment gateway will randomly generate a customer vault id.
				'security_key' => security_key,#API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
				'checkname' => checkname,#The name on the customer's ACH account.
				'checkaba' => checkaba,#The customer's bank routing number.
				'checkaccount' => checkaccount,#The customer's bank account number.
				'account_holder_type' => account_holder_type, #The customer's ACH account entity. Values: 'personal' or 'business'
				'account_type' => account_type, #The customer's ACH account type. Values: 'checking' or 'savings'
				'sec_code' => sec_code,# ACH standard entry class codes. Values: 'PPD', 'WEB', 'TEL', or 'CCD'
				'payment' => payment # Set payment type to ACH or credit card. Values: 'creditcard' or 'check'
			}
		end

		def delete_customer(customer_vault_id,security_key)
			@customer_vault_ach = {
				'customer_vault' => 'delete_customer', #*	Deletes a secure customer vault record. Values: 'delete_customer'
				'customer_vault_id' => customer_vault_id, #*	Specifies a customer vault id.
				'security_key' => security_key  #*	API Security Key assigned to a merchant account. Using the 'username' or 'password' variables in the same request will result in an error
			}
		end

		def do_sale 
			query  = ""
			query = query + "username=" + URI.escape(@login['username']) + "&"
			query += "password=" + URI.escape(@login['password']) + "&"
			@customer_vault_ach.each do | key,value|
				query += key +"=" + URI.escape(value) + "&"
			end
			return doPost(query)
		end

		def doPost(query)
			curlObj = Curl::Easy.new("https://secure.networkmerchants.com/api/transact.php")
			curlObj.connect_timeout = 30
			curlObj.timeout = 30
			curlObj.header_in_body = false
			curlObj.ssl_verify_peer = false
			curlObj.post_body = query
			curlObj.perform()
			data = curlObj.body_str

			data = 'https://secure.networkmerchants.com/api/transact.php?' + data
			uri = Addressable::URI.parse(data)
			@responses = uri.query_values
			return @responses
		end

		def getResponses()
			return @responses
		end
	end
end