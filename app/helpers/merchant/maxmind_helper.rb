module Merchant::MaxmindHelper

  def maxmind_minfraud_detail_status(minfraud_result)
    if minfraud_result.present? &&
        minfraud_result.qc_action.present? &&
        minfraud_result.qc_reason.present?
      #qc_disposition = minfraud_result.qc_disposition
      qc_reason = minfraud_result.qc_reason
      qc_action = minfraud_result.qc_action
      case qc_reason
      when TypesEnumLib::RiskReason::Default
        if qc_action == TypesEnumLib::RiskType::Accept
          '<div class="mt-left risk-evaluation risk-evaluation1 a-green"> A </div>'
        else
          '<div class="risk-evaluation r-red"> R </div>'
        end
      when TypesEnumLib::RiskReason::ManualReview
        if qc_action == TypesEnumLib::RiskType::PendingReview
          '<div class="risk-evaluation cr-p-manual-review"> MR </div>'
        elsif qc_action == TypesEnumLib::RiskType::Accept
          '<div class="risk-evaluation mr-green"> MR </div>'
        elsif qc_action == TypesEnumLib::RiskType::Reject
          '<div class="risk-evaluation mr-red"> MR </div>'
        elsif qc_action == TypesEnumLib::RiskType::ExpiredReview
          '<div class="risk-evaluation mr-red"> ER </div>'
        else
          '<div class="risk-evaluation">New</div>'
        end
      else
        '<div class="risk-evaluation">New</div>'
      end
    else
      '<span class="risk-evaluation no-disposition">&nbsp;</span>'
    end
  end
  def maxmind_minfraud_website_url(minfraud_result)
    if minfraud_result.present? && minfraud_result.website_url.present?
      minfraud_result.website_url
    else
      "--"
    end
  end

  def maxmind_minfraud_description(qc_action)
    qc_action.try(:titleize)
  end

  def parsing_insight_for_detail(insight)
    custom_hash = {}
    if insight["ip_address"].present?
      custom_hash["ip_risk_score"] = insight.try(:[],"ip_address").try(:[],"risk")
      custom_hash["ip_address_minfraud"] = insight.try(:[],"ip_address").try(:[],"traits").try(:[],"ip_address")
      custom_hash["high_risk_country"] = insight.try(:[],"ip_address").try(:[],"country").try(:[],"is_high_risk")
      custom_hash["time_zone"] = insight.try(:[],"ip_address").try(:[],"location").try(:[],"time_zone")
      custom_hash["city"] = insight.try(:[],"ip_address").try(:[],"city").try(:[],"names").try(:[],"en")
      custom_hash["country"] = insight.try(:[],"ip_address").try(:[],"country").try(:[],"names").try(:[],"en")
      custom_hash["user_type"] = insight.try(:[],"ip_address").try(:[],"traits").try(:[],"user_type")
      custom_hash["local_date_time"] = insight.try(:[],"ip_address").try(:[],"location").try(:[],"local_time")
    end
    if insight["credit_card"].present?
      custom_hash["credit_card_type"] = insight["credit_card"]["type"]
      custom_hash["credit_card_brand"] = insight["credit_card"]["brand"]
      custom_hash["iin_country"] = insight["credit_card"]["country"]
      custom_hash["iin_country_matches_billing_country"] = insight["credit_card"]["is_issued_in_billing_address_country"]
      custom_hash["is_prepaid_card"] = insight["credit_card"]["is_prepaid"]
      custom_hash["is_virtual_card"] = insight["credit_card"]["is_virtual"]
      custom_hash["issuer_phone"] = insight["credit_card"]["issuer"].try(:[],"phone_number")
      custom_hash["issuer_phone_matches"] = ""
      custom_hash["credit_card_name"] = ""
      custom_hash["credit_card_name_matches"] = ""
    end

    if insight["billing_address"].present?
      custom_hash["billing_postal_address_is_in_billing_city"] = insight["billing_address"]["is_postal_in_city"]
      custom_hash["billing_postal_code_latitude"] = insight["billing_address"]["latitude"]
      custom_hash["billing_postal_code_longitude"] = insight["billing_address"]["longitude"]
      custom_hash["billing_postal_code_to_IP_address_distance"] = insight["billing_address"]["distance_to_ip_location"] # add km
      custom_hash["billing_address_is_in_IP_country"] = insight["billing_address"]["is_in_ip_country"]
    end

    if insight["shipping_address"].present?
      custom_hash["shipping_postal_code_is_in_the_shipping_city"] = insight["shipping_address"]["is_postal_in_city"]
      custom_hash["shipping_postal_code_latitude"] = insight["shipping_address"]["latitude"]
      custom_hash["shipping_postal_code_longitude"] = insight["shipping_address"]["longitude"]
      custom_hash["shipping_postal_code_to_IP_address_distance"] = insight["shipping_address"]["distance_to_ip_location"] # add km
      custom_hash["shipping_address_is_in_IP_country"] = insight["shipping_address"]["is_in_ip_country"]
      custom_hash["shipping_address_distance_to_billing_address"] = insight["shipping_address"]["distance_to_billing_address"]
      custom_hash["high_risk_shipping_address"] = "N/A"
    end

    if insight["email"].present?
      custom_hash["free_email"] = insight["email"]["is_free"]
      custom_hash["email_first_seen"] = insight["email"]["first_seen"]
      custom_hash["carder_email"] = insight["email"]["is_high_risk"]
    end
    return custom_hash
  end

  def transactions_search_query_maxmind_merchant(params=nil)
    reason = []
    action = []
    conditions = []
    parameters = []
    if params["risk_status"].present?
      array = params["risk_status"]
      if array.class == String
        array = JSON.parse(array)
      end
    end

    date_range = params["query"].present? && params["query"]["date"].present? ? parse_daterange(params["query"]["date"]) : [nil,nil]

    start_date = date_range.first
    end_date = date_range.last

    if params[:offset].present?
      unless params[:offset].include?(':')
        params[:offset].insert(3,':')
      end
    end

    if start_date.present? && start_date[:day].present? && start_date[:month].present? && start_date[:year].present?
      start_date = Time.new(start_date[:year],start_date[:month], start_date[:day],00,00,00, params[:offset]).to_datetime
      conditions << "minfraud_results.created_at >= ?"
      parameters << start_date
    end

    if end_date.present? && end_date[:day].present? && end_date[:month].present? && end_date[:year].present?
      end_date =  Time.new(end_date[:year],end_date[:month], end_date[:day],23,59,59, params[:offset]).to_datetime
      conditions << "minfraud_results.created_at <= ?"
      parameters << end_date
    end
    if array.present?

      if array.any? { |s| s.include?('manual_review') }
        reason << TypesEnumLib::RiskReason::ManualReview
      end
      if array.any? { |s| s.include?('default') }
        reason << TypesEnumLib::RiskReason::Default
      end
      if array.any? {|s| s.include?("accept") }
        action << TypesEnumLib::RiskType::Accept
      end
      if array.any? {|s| s.include?("reject") }
        action << TypesEnumLib::RiskType::Reject
      end
      if array.any? {|s| s.include?("pending") }
        action << TypesEnumLib::RiskType::PendingReview
      end
      if array.any? {|s| s.include?("expired") }
        action << TypesEnumLib::RiskType::ExpiredReview
      end

    end

    if action.present? && reason.present?
      conditions << "(minfraud_results.qc_action IN (?) AND minfraud_results.qc_reason IN (?))"
      parameters << action
      parameters << reason
    end
    conditions = [conditions.join(" AND "), *parameters]
    return conditions
  end

  def parse_daterange(date)
    date = date.split(' - ')
    first_date = date.first.split('/')
    second_date = date.second ? date.second.split('/') : date.first.split('/')
    first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
    second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
    return [first_date, second_date]
  end

end