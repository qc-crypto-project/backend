module Merchant::SalesHelper

  def maintain_batch(user_id, wallet_id, sale = nil, type = nil, release_days = nil, risk = nil , fee = nil, iso_id = nil, merchant = nil, iso_fee=nil, agent_fee=nil, affiliate_fee=nil,bonus=nil)
    batch = retrieve_batch(user_id, wallet_id, type, risk, iso_id, merchant)
    if batch.present?
      total_fee = 0
      total_fee = batch.total_fee.to_f + fee.to_f if batch.total_fee.present?
      total_sales = batch.total_sales.to_f + sale.to_f
      iso_profit = batch.iso_profit.to_f + iso_fee.to_f
      agent_profit = batch.agent_profit.to_f + agent_fee.to_f
      affiliate_profit = batch.affiliate_profit.to_f + affiliate_fee.to_f
      total_transactions = batch.total_transactions.to_i + 1
      total_bonus=batch.total_bonus.to_f + bonus.to_f
      batch.update(total_transactions: total_transactions, total_sales: total_sales, total_fee: total_fee, days: release_days,iso_profit: iso_profit,agent_profit: agent_profit,affiliate_profit: affiliate_profit,total_bonus: total_bonus)
    else
      create_batch(user_id, wallet_id, type, sale, risk, release_days, fee, nil, iso_id, merchant,iso_fee,agent_fee,affiliate_fee,bonus)
    end
  end

  def get_batch(user_id, wallet_id, type, sale = nil, release_days = nil, current_tier = nil, risk = nil, iso_id = nil, merchant = nil)
    batch = retrieve_batch( user_id, wallet_id, type, risk, iso_id, merchant)
    if batch.present?
      batch
    else
      create_batch(user_id, wallet_id, type, sale, risk, release_days, nil, current_tier, iso_id, merchant)
    end
  end

  def close_batches(user_id, wallet_id, type, risk = nil, iso_id = nil, merchant = nil)
    if type == TypesEnumLib::Batch::Primary
      batch = retrieve_batch( user_id, wallet_id, type)
    elsif type == TypesEnumLib::Batch::Reserve
      batch = retrieve_batch( user_id, wallet_id, type)
    elsif type == TypesEnumLib::Batch::BuyRateCommission
      batch = retrieve_batch( user_id, wallet_id, type, risk, iso_id, merchant)
    end
    if batch.present?
      # g = ((DateTime.now.utc-batch.created_at) * 24 * 60).to_i
      # if (g/1.minute).round >= 1
      if batch.created_at.strftime("%B") != DateTime.now.utc.strftime("%B")
        batch.update(is_closed: true, close_date: Date.today, close_time: Time.now.utc.strftime("%H:%M"))
      end
    end
  end

  def retrieve_batch(user_id, wallet_id, type, risk = nil, iso_id = nil, merchant = nil)
    case type
      when TypesEnumLib::Batch::Primary
        Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).primary.first
      when TypesEnumLib::Batch::Reserve
        Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).reserve.first
    when TypesEnumLib::Batch::BuyRateCommission
        if risk == TypesEnumLib::Batch::High
          if merchant == true
            Batch.where(user_id: user_id, wallet_id: wallet_id, iso_id: iso_id, is_closed: false).buy_rate_commission.high.first
          else
            Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).buy_rate_commission.high.first
          end
        elsif risk == TypesEnumLib::Batch::Low
          if merchant == true
            Batch.where(user_id: user_id, wallet_id: wallet_id, iso_id: iso_id, is_closed: false).buy_rate_commission.low.first
          else
            Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).buy_rate_commission.low.first
          end
        end
    end
  end

  def create_batch(user_id, wallet_id, type, sale, risk = nil, release_days = nil, fee = nil, current_tier = nil, iso_id = nil, merchant = nil,iso_profit=nil,agent_profit=nil,affiliate_profit=nil,bonus=nil)
    case type
      when  TypesEnumLib::Batch::Primary
        batch = Batch.new(total_transactions: 1, total_sales: sale, user_id: user_id, wallet_id: wallet_id)
        batch.primary!
        batch.save
        batch
      when TypesEnumLib::Batch::Reserve
        batch = Batch.new(total_transactions: 1, total_sales: sale, user_id: user_id, wallet_id: wallet_id, days: release_days)
        batch.reserve!
        batch.save
        batch
    when TypesEnumLib::Batch::BuyRateCommission
        if merchant == true
          batch = Batch.new(total_transactions: 1, total_sales: sale, total_fee: fee.to_f, iso_id: iso_id, user_id: user_id, wallet_id: wallet_id, current_tier: current_tier || 1,iso_profit: iso_profit.to_f,agent_profit: agent_profit.to_f,affiliate_profit: affiliate_profit.to_f)
        else
          batch = Batch.new(total_transactions: 1, total_sales: sale, total_fee: fee.to_f, user_id: user_id, wallet_id: wallet_id, current_tier: current_tier || 1, total_bonus: bonus.to_f)
        end
        batch.buy_rate_commission!
        if risk == TypesEnumLib::Batch::High
          batch.high!
        elsif risk == TypesEnumLib::Batch::Low
          batch.low!
        end
        batch.save
        batch
    end
  end

end
