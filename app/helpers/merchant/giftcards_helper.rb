module Merchant::GiftcardsHelper


  def get_all_giftcards(cards)
    cards = parse_giftcards(cards)
    return cards
  end

  def parse_giftcards(cards)
    cards.brands.map do |obj|
      if obj.present?
        # t = obj.actions.first
        # obj.actions.each do |t|
        hash = {
            brand_key: obj.brand_key,
            brand_name: obj.brand_name,
            timestamp: obj.created_date,
            disclaimer: obj.disclaimer,
            image_urls: parse_image_urls(obj.image_urls),
            items: parse_items(obj.items),
            last_update_date: obj.last_update_date,
            short_description: obj.short_description,
            status: obj.status,
            terms: obj.terms

        }
        hash.merge({})
        # end
      end
    end
  end

  def parse_image_urls(images)
    images.each do |k,v|
      hash = {images: v}
      return hash.merge({})
    end
  end

  def parse_items(items)
    items.each do |item|
      hash = {
          countries: item.countries,
          created_date: item.created_date,
          currency_code: item.currency_code,
          face_value: item.face_value,
          last_update_date: item.last_update_date,
          max_value: item.max_value,
          min_value: item.min_value,
          reward_name: item.reward_name,
          reward_type: item.reward_type,
          status: item.status,
          utid: item.utid,
          value_type: item.value_type
      }
    end
  end
end
