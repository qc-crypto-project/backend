module Merchant::BaseHelper

  def merchant_allowed
    if current_user.MERCHANT?
      merchant = current_user.parent_merchant
      merchant = merchant.nil? ? current_user : merchant
      locations = []
      if current_user.merchant_id.nil?
        locations = Location.find(current_user.wallets.pluck(:location_id).uniq).uniq
      elsif current_user.merchant_id.present? && current_user.try(:permission).try(:admin?)
        locations = Location.find(current_user.parent_merchant.wallets.pluck(:location_id).uniq).uniq
      elsif current_user.merchant_id.present? && current_user.submerchant_type == 'regular_user'
        locations = current_user.locations
      end
      block_ach = locations.uniq.map {|loc| loc.block_ach}.all?
      block_withdrawal = locations.uniq.map {|loc| loc.block_withdrawal}.all?
      block_giftcard = locations.uniq.map {|loc| loc.block_giftcard}.all?
      push_to_card = locations.uniq.map {|loc| loc.push_to_card}.all?
      oauth_app = merchant.oauth_apps.first
      if oauth_app.present?
        if oauth_app.is_block
          return {
              sales: true,
              virtual_terminal: true,
              close_batch: true,
              giftcard: false,
              checks: false,
              ach: false,
              p2c: false
          }
        end
      end

      if locations.count > 0
        return {
          sales: locations.uniq.map{|loc| loc.sales}.all?,
          virtual_terminal: locations.uniq.map {|loc| loc.virtual_terminal}.all?,
          close_batch: locations.uniq.map {|loc| loc.close_batch}.all?,
          giftcard: false,
          checks: false,
          ach: false,
          p2c: false
        }
      end
    end
    return {
      sales: false,
      virtual_terminal: false,
      close_batch: false,
      giftcard: false,
      checks: false,
      ach: false,
      p2c: false
    }
  end

  def location_bank_account(location)
    begin
      data = `****#{AESCrypt.decrypt("#{location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", location.bank_account).last(4)}`
    rescue
      data= "No Bank Account"
    end
    [data,location.id]
  end

  def verified_tos
    if current_user.merchant_id.blank? && current_user.tos_checking == true
      return true
    elsif current_user.merchant_id.present?
      user = User.find(current_user.merchant_id )
      if user.tos_checking == true
        return true
      end
    end
    return false
  end

  def have_user_change_password #after user login with auto generated password user must set its own password
    if !current_user.is_password_set?
      return false
    end
    return true
  end

end
