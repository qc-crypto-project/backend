module Merchant::ReportsHelper
  def get_report_source(t)
    if t.main_type == TypesEnumLib::TransactionViewTypes::CreditCard
      "Credit"
    else
      t.sender_wallet_id
    end
  end

  def get_check_user_name(check)
    check.tags["send_check_user_info"].try(:[], "name")
  end

  def get_check_user_email(check)
    check.tags["send_check_user_info"].try(:[], "check_email")
  end

  def get_main_transaction_type(transaction)
    if transaction.tags.try(:[], "main_transaction_info").present?
      tags = transaction.tags.try(:[], "main_transaction_info")
      if tags["transaction_type"] == "sale"
        if tags["transaction_sub_type"] == "credit_card"
          return "Credit Card"
        elsif tags["transaction_sub_type"] == "virtual_terminal_sale"
          return "Virtual Terminal"
        elsif tags["transaction_sub_type"] == "virtual_terminal_sale_api"
          return "eCommerce"
        end
        return tags["transaction_sub_type"]
      else
        return tags["transaction_type"].humanize.titleize
      end
    end
  end

  def get_main_transaction_amount(tx)
    main_amount = nil
    main_amount = tx.main_amount
    main_amount = tx.tags.try(:[],"main_transaction_info").try(:[],"amount") if main_amount.blank? || main_amount.to_f == 0
    main_amount = SequenceLib.dollars(tx.parent_transaction.try(:amount_in_cents)) if main_amount.blank? || main_amount.to_f == 0
    main_amount = SequenceLib.dollars(BlockTransaction.where(seq_parent_id: tx.seq_block_id, position: '0').first.try(:amount_in_cents)) || 0 if tx.seq_block_id.present? && (main_amount.blank? || main_amount.to_f == 0)
    main_amount
    # if transaction.main_amount.present? && transaction.main_amount != 0
    #   transaction.main_amount
    # elsif transaction.tags.try(:[], "main_transaction_info").present?
    #   tags = transaction.tags.try(:[], "main_transaction_info")
    #   if tags["amount"].present?
    #     return tags["amount"]
    #   end
    # elsif transaction.parent_transaction.present?
    #   return SequenceLib.dollars(transaction.parent_transaction.try(:amount_in_cents))
    # elsif transaction.seq_block_id.present?
    #   SequenceLib.dollars(BlockTransaction.where(seq_block_id: transaction.seq_block_id, position: '0').first.try(:amount_in_cents))
    # else
    #   0
    # end
  end

end
