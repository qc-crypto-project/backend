module Merchant::TransactionsHelper

  def parse_date_with_time(date, offset = nil)
    first_date = date[0..9]
    first_date = Date.strptime(first_date, '%m/%d/%Y')
    firstdate = Time.new(first_date.year,first_date.month,first_date.day,00,00,00, offset).utc
    firsttime = firstdate.strftime("%H:%M:%S.00Z")
    firstdate = firstdate.strftime("%Y-%m-%dT#{firsttime}")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, '%m/%d/%Y')
    seconddate = Time.new(second_date.year,second_date.month,second_date.day,23,59,59, offset).utc
    secondtime = seconddate.strftime("%H:%M:%S.59Z")
    seconddate = seconddate.strftime("%Y-%m-%dT#{secondtime}")
    return {first_date: firstdate, second_date: seconddate}
  end

  def parse_export_time(t1,t2)
    if t1.present?
      time1 = t1+":00"+"+05:00"
    else
      time1 = "00:00"+":00"+"+05:00"
    end
    if t2.present?
      time2 = t2+":00"+"+05:00"
    else
      time2 = "23:59"+":00"+"+05:00"
    end
    return [time1,time2]
  end

  def parse_refund_date(start_date = nil, end_date = nil)
    if start_date.present?
      first_date = Date.strptime(start_date, '%d-%m-%Y') - 1.day
      first_date = first_date.strftime("%Y-%m-%dT%H:%M:%S.00Z")
    end
    if end_date.present?
      second_date = Date.strptime(end_date, '%d-%m-%Y') + 1.day
      second_date = second_date.strftime("%Y-%m-%dT23:59:59.59Z")
    end
    return {first_date: first_date, second_date: second_date}
  end

  def show_type(object)
    if object[:reference]["type"] == 'charge_back'
      return 'CBK Won' if object[:reference]["source"] == 'charge_back_won' || object[:reference]["source"] == 'cbk_won' || object[:reference]["sub_type"] == 'cbk_won'
      return 'CBK Lost' if object[:reference]["source"] == 'charge_back_lost' || object[:reference]["source"] == 'cbk_lost'
    elsif object[:reference]["type"] == 'charge_back_fee' || object[:reference]["type"] == 'cbk_fee'
      return 'CBK Fee'
    elsif object[:reference]["type"] == 'charge_back_hold' || object[:reference]["type"] == 'cbk_hold'
      return 'CBK Hold'
    else
      object[:type]
    end
  end

  def show_local_type(obj)
    if obj.main_type == TypesEnumLib::TransactionType::SaleType
      if obj.sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtualApi
        return "eCommerce"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtual
        return "Virtual Terminal"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::DebitCharge
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssueApi
        return "Sale Issue"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssue
        return "Sale Issue"
      end
    elsif obj.main_type == "ACH" || obj.main_type == "Void ACH"
      return obj.main_type
    elsif obj.main_type == "Void_Ach"
      return "Void ACH"
    elsif obj.main_type == "instant_pay" || obj.main_type == "Instant Pay"
      return "Push to Card"
    elsif obj.main_type == "ACH_deposit"
      return "ACH Deposit"
    elsif obj.main_type == "Giftcard Purchase"
      return "Giftcard"
    elsif obj.main_type == "cbk_hold"
      return "CBK Hold"
    elsif obj.main_type == "cbk_lost"
      return "CBK Lost"
    elsif obj.main_type == "cbk_fee"
      return "CBK Fee"
    elsif obj.main_type == "cbk_dispute_accepted"
      return "CBK Dispute Accepted"
    elsif obj.main_type == "cbk_won"
      return "CBK Won"
    elsif obj.main_type == "Void_Ach"
      return "Void ACH"
    else
      obj.main_type.try(:humanize).try(:titleize) || obj.sub_type.try(:humanize).try(:titleize)
    end
  end

  def show_iso_local_type(obj)
    if obj.present?
      if obj.tags.try(:[],"main_transaction_info").present?
        if obj.tags["main_transaction_info"]["transaction_type"] == TypesEnumLib::TransactionType::SaleType
          if obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
            return obj.tags["main_transaction_info"]["transaction_sub_type"].humanize.titleize
          elsif obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::SaleVirtualApi
            return "eCommerce"
          elsif obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::SaleVirtual
            return "Virtual Terminal"
          elsif obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::DebitCharge
            return TypesEnumLib::TransactionViewTypes::PinDebit
          elsif obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::SaleIssueApi
            return "Sale Issue"
          elsif obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::SaleIssue
            return "Sale Issue"
          elsif obj.tags["main_transaction_info"]["transaction_sub_type"] == TypesEnumLib::TransactionSubType::DebitCard
            return "Debit Card"
          end
        elsif obj.tags["main_transaction_info"]["transaction_type"] == "ACH" || obj.tags["main_transaction_info"]["transaction_type"] == "Void ACH"
          return obj.tags["main_transaction_info"]["transaction_type"]
        elsif obj.tags["main_transaction_info"]["transaction_type"] == "instant_pay"
          return "Push to Card"
        elsif obj.tags["main_transaction_info"]["transaction_type"]  == "ACH_deposit"
          return "ACH Deposit"
        elsif obj.tags["main_transaction_info"]["transaction_type"]  == "qr_credit_card"
          return "QR Credit Card"
        elsif obj.tags["main_transaction_info"]["transaction_type"]  == "qr_debit_card"
          return "QR Debit Card"
        else
          if obj.tags["main_transaction_info"]["transaction_type"]=="cbk_fee"
            return "CBK Fee"
          end
          return obj.tags["main_transaction_info"]["transaction_type"].try(:humanize).try(:titleize) || obj.sub_type.try(:humanize).try(:titleize)
        end
      elsif obj.parent_transaction.present?
        return obj.parent_transaction.try(:main_type).try(:humanize).try(:titleize)
      else
        if obj.main_type == "Instant Pay" || obj.main_type == "instant_pay"
          return "Push to Card"
        elsif obj.main_type == "Void Ach" || obj.main_type == "void_ach"
          return "Void ACH"
        else
          return obj.main_type
        end
        # BlockTransaction.where(seq_parent_id: obj.seq_block_id, position: '0').first.try(:main_type).try(:humanize).try(:titleize)
      end
    end
  end


  def show_descriptor(obj)
    if obj.main_type == TypesEnumLib::TransactionViewTypes::SendCheck || obj.main_type == TypesEnumLib::TransactionViewTypes::BulkCheck
      "Checkbook Io"
    elsif obj.payment_gateway.present? && (obj.payment_gateway.knox_payments? || obj.payment_gateway.payment_technologies? || obj.payment_gateway.total_pay?) && obj.main_type != I18n.t("types.TipTransfer")
      obj.tags.try(:[],"source")
    elsif obj.try(:tags).try(:[], "descriptor").present? && obj.main_type != I18n.t("types.TipTransfer")
      obj.tags["descriptor"]
    else
      "Quickcard"
    end
  end

  def show_other_descriptor(obj)
    if obj.main_type == TypesEnumLib::TransactionViewTypes::SendCheck || obj.main_type == TypesEnumLib::TransactionViewTypes::BulkCheck
      "Checkbook Io"
    elsif obj.gateway.present? && obj.main_type != I18n.t("types.TipTransfer")
      obj.gateway
    else
      "Quickcard"
    end
  end

  def get_local_sender(object)
    if object.sender_wallet_id != 0
      object.sender_wallet_id
    end
  end

  def get_sub_type(sub_type)
    if sub_type == "instant_pay" || sub_type == "instant_pay_fee"
      "Push to Card"
    elsif sub_type == "send_check_fee"
      "Send Check"
    else
      sub_type.try(:humanize)
    end
  end

  def show_source(object)
    if object[:reference]["type"] == "Giftcard" || object[:reference]["type"] == "sales_giftcard" || object[:reference]["type"] == TypesEnumLib::TransactionType::GiftCard
      if object[:source].present?
        return "***#{object[:source][-4..-1]}"
      end
    else
      object[:source]
    end
  end

  def show_destination(object)
    if object.present?
      object[:destination]
    end
  end

  def show_destination_name(object)
    if object.present?
      if object[:destination].present?
        Wallet.find_by_id(object[:destination]).try(:name)
      end
    end
  end

  def get_commission(object, user)
      if user.wallets.primary.ids.include?(object[:source].to_i)
        if object[:type] == "Send Check" || object[:type] == "Giftcard Purchase"
          data = {css_class: "text-danger mr-9", amount: number_with_precision(number_to_currency(object[:fee_without_hold_money].to_f), precision: 2, delimiter: ',')}
        else
          data = {css_class: "text-danger mr-9", amount: number_with_precision(number_to_currency(object[:amount]), precision: 2, delimiter: ',')}
        end
      elsif user.wallets.primary.ids.include?(object[:destination].to_i)
        if object[:type] == "Send Check" || object[:type] == "Giftcard Purchase"
          data = {css_class: "text-success mr-9", amount: number_with_precision(number_to_currency(object[:fee_without_hold_money].to_f), precision: 2, delimiter: ',')}
        else
          data = {css_class: "text-success mr-9", amount: number_with_precision(number_to_currency(object[:amount]), precision: 2, delimiter: ',')}
        end
      else
        data = {css_class: "text-success mr-9", amount: number_with_precision(number_to_currency(object[:amount]), precision: 2, delimiter: ',')}
      end
      data
  end

  def new_show_amount(object, user)
    transfer_type = ""
    if object[:reference]["type"].present?
      transfer_type = object[:reference]["type"] if object[:reference]["type"] == "account_transfer"
    end
    if transfer_type=="account_transfer"
      {css_class: "text-success mr-9", amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
    elsif user.merchant? && !user.merchant_id.nil? && user.try(:permission).admin?
      #for submerchant
      main_user = User.find(user.merchant_id)
      if object[:type] == 'retire' || main_user.wallets.primary.ids.include?(object[:source].to_i)
        css_class = "text-danger"
      else
        css_class = "text-success mr-9"
      end
      if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
        if object[:reference]["fee"].present?
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
        else
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
        end
      else
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), :precision => 2, delimiter: ',')}
      end
    else
      #for other users
      if object[:type] == 'retire' || user.wallets.primary.ids.include?(object[:source].to_i)
        css_class = "text-danger"
      else
        css_class = "text-success mr-9"
      end
      if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck && object[:reference]["fee_perc"].present?
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
      elsif object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
        if object[:reference]["fee"].present?
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
        else
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
        end
      else
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount].to_f), precision: 2, delimiter: ',')}
      end
    end
  end

  def show_amount(transaction, user)
    object = transaction.clone
    transfer_type = ""
    privacy = object[:privacy_fee].to_f if object[:privacy_fee].present?
    object[:amount_updated] = object[:amount_updated].to_f - object[:tip].to_f if object[:type] == "eCommerce"
    if object[:reference]["type"].present?
      transfer_type = object[:reference]["type"] if object[:reference]["type"] == "account_transfer"
    end
    if transfer_type=="account_transfer"
      {css_class: "text-success mr-9", amount: number_with_precision(number_to_currency(object[:amount_updated].to_f), precision: 2, delimiter: ',')}
    elsif user.merchant? && !user.merchant_id.nil? && user.submerchant_type == "admin_user"
      #for submerchant
      main_user = User.find(user.merchant_id)
      if object[:type] == 'retire' || main_user.wallets.primary.or(main_user.wallets.primary).ids.include?(object[:source].to_i)
        css_class = "text-danger"
      else
         css_class = "text-success mr-9"
      end
      if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
        if object[:reference]["fee"].present?
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f - object[:reference]["fee"].to_f), precision: 2, delimiter: ',')}
        else
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f), precision: 2, delimiter: ',')}
        end
      else
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f - privacy.to_f), :precision => 2, delimiter: ',')}
      end
    else
      #for other users
      if object[:type] == 'retire' || user.wallets.primary.or(user.wallets.primary).ids.include?(object[:source].to_i)
        css_class = "text-danger"
      else
        css_class = "text-success mr-9"
      end
      if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck && object[:reference]["fee_perc"].present?
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f - object[:reference]["fee"].to_f), precision: 2, delimiter: ',')}
      elsif object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
        if object[:reference]["fee"].present?
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f - object[:reference]["fee"].to_f), precision: 2, delimiter: ',')}
        else
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f), precision: 2, delimiter: ',')}
        end
      else
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:amount_updated].to_f - privacy.to_f), precision: 2, delimiter: ',')}
      end
    end
  end

  def show_total_amount(object, user)
    object[:total_amount] = object[:total_amount].to_f - object[:tip].to_f if object[:type] == "eCommerce"
    transfer_type = ""
    if object[:reference]["type"].present?
      transfer_type = object[:reference]["type"] if object[:reference]["type"] == "account_transfer"
    end
    if transfer_type=="account_transfer"
      {css_class: "text-success mr-9", amount: number_with_precision(number_to_currency(object[:total_amount].to_f), precision: 2, delimiter: ',')}
    elsif user.merchant? && !user.merchant_id.nil? && user.try(:permission).admin?
      #for submerchant
      main_user = User.find(user.merchant_id)
      if object[:type] == 'retire' || main_user.wallets.primary.ids.uniq.include?(object[:source].to_i)
        css_class = "text-danger"
      else
        css_class = "text-success mr-9"
      end
      if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
        if object[:reference]["fee"].present?
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f + object[:fee_updated].to_f), precision: 2, delimiter: ',')}
        else
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f), precision: 2, delimiter: ',')}
        end
      else
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f), :precision => 2, delimiter: ',')}
      end
    else
      #for other users
      locations_ids = user.attached_locations
      locations = Location.where(id: locations_ids)
      ids = []
      ids << user.wallets.primary.pluck(:id)
      if locations.present?
        locations.each do |location|
          ids << location.try(:wallets).try(:primary).try(:first).try(:id)
        end
      end
      if object[:type] == 'retire' || ids.flatten.include?(object[:source].to_i)
        css_class = "text-danger"
      else
        css_class = "text-success mr-9"
      end
      if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck && object[:reference]["fee_perc"].present?
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f + object[:fee_updated].to_f), precision: 2, delimiter: ',')}
      elsif object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
        if object[:reference]["fee"].present?
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f + object[:fee_updated].to_f), precision: 2, delimiter: ',')}
        else
          return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f), precision: 2, delimiter: ',')}
        end
      else
        return {css_class: css_class, amount: number_with_precision(number_to_currency(object[:total_amount].to_f ), precision: 2, delimiter: ',')}
      end
    end
  end
  def show_fee(object, user)
    if object[:reference]["type"] == TypesEnumLib::TransactionType::B2b
      if user.wallets.primary.ids.include?(object[:source].to_i)
        return "$0.00"
      else
        return number_with_precision(number_to_currency(object[:fee_updated]), precision: 2, delimiter: ',')
      end
    else
      number_with_precision(number_to_currency(object[:fee_updated]), precision: 2, delimiter: ',')
    end
  end

  def show_net(object, user)
    if object[:net].present?
      if object[:reference]["type"] == TypesEnumLib::TransactionType::B2b
        if user.wallets.primary.ids.include?(object[:source].to_i)
          return number_with_precision(number_to_currency(object[:amount_updated].to_f), precision: 2, delimiter: ',')
        else
          return number_with_precision(number_to_currency(object[:net]), precision: 2, delimiter: ',')
        end
      else
        number_with_precision(number_to_currency(object[:net]), precision: 2, delimiter: ',')
      end
    else
      "$0.00"
    end
  end

  def show_reserve(object, user)
    if object[:hold_money].present?
      if object[:reference]["type"] == TypesEnumLib::TransactionType::B2b
        if user.wallets.primary.ids.include?(object[:source].to_i)
          return "$0.00"
        else
          return number_with_precision(number_to_currency(object[:hold_money]), precision: 2, delimiter: ',')
        end
      else
        number_with_precision(number_to_currency(object[:hold_money]), precision: 2, delimiter: ',')
      end
    else
      "$0.00"
    end
  end

  # def show_hold_money_status(object,date)
  #   wallet_id = object[:destination]
  #   HoldInRear.check_wallet_status(wallet_id,date)
  # end

  def filter_transaction_type(object, user)
    if object.present?
      if object[:type] == "Send Check"
        if user.wallets.primary.ids.include?(object[:source].to_i)
          "---"
        else
          object[:transaction_type]
        end
      else
        object[:transaction_type]
      end
    else
      "---"
    end
  end
end
