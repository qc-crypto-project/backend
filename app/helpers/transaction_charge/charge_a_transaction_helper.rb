module TransactionCharge::ChargeATransactionHelper
	class IssueRawTransaction
		include ApplicationHelper
		include Admins::BlockTransactionsHelper
		include PosMerchantHelper

		attr_accessor :params

		def initialize(email,params,card_cvv=nil)
			@email = email
			@params = params
			@card_cvv = card_cvv
			@terminal_id = params.try(:[],"terminal_id")
			@tokenization = CardToken.new
			@fee_lib = FeeLib.new
		end

		# def get_location_fees(to, merchant, amount, reserve_amount = nil, user = nil, type)
		def get_location_fees(to, user, amount, reserve_amount = nil, type = nil,gateway_key = nil, load_fee = nil)
			@to = Wallet.find(to)
			location = @to.location
			if location.present?
				fee_object = location.fees.buy_rate.first
				fee_class = Payment::FeeCommission.new(nil, user	, fee_object, @to, location,nil,gateway_key,nil, load_fee)
				if user.present? && user.MERCHANT? && @to.wallet_type != 'tip'
					if fee_object.present?
						#------- get fee with respect to type
						if type.downcase == "credit"
							fee = fee_class.apply(amount, 'credit', nil)
						elsif type.downcase == "debit"
							fee = fee_class.apply(amount, 'debit', nil)
						end
						reserve_money = fee_object.reserve_fee
						reserve_detail = {amount: deduct_fee(reserve_money,reserve_amount), days: fee_object.days, wallet: location.wallets.reserve.first.id} if reserve_money > 0
						if fee.present?
							user_info = fee["splits"]
							if reserve_detail.present?
								reserve = {"reserve" => reserve_detail}
								user_info = user_info.merge(reserve)
							end
							return [reserve_detail,user_info, fee["fee"].to_f]
						else
							return [nil,nil,0]
						end
					end
				end
			else
				return [nil,nil,0]
			end
		end

		def get_fee_by_type(location, type=nil)
			if type == "debit"
				dcard_fee = location.fees.transaction_fee_dcard.first.transaction_fee_dcard.to_f
				dcard_fee_dollar = location.fees.transaction_fee_dcard_dollar.first.transaction_fee_dcard_dollar.to_f
				dcard_fee_object = location.fees.transaction_fee_dcard.first
				dcard_fee_dollar_object = location.fees.transaction_fee_dcard_dollar.first
				return [dcard_fee,dcard_fee_dollar,dcard_fee_object,dcard_fee_dollar_object]
			else
				ccard_fee = location.fees.transaction_fee_ccard.first.transaction_fee_ccard.to_f
				ccard_fee_dollar = location.fees.transaction_fee_ccard_dollar.first.transaction_fee_ccard_dollar.to_f
				ccard_fee_object = location.fees.transaction_fee_ccard.first
				ccard_fee_dollar_objetc = location.fees.transaction_fee_ccard_dollar.first
				return [ccard_fee,ccard_fee_dollar,ccard_fee_object,ccard_fee_dollar_objetc]
			end
		end

		def issue_with_fluid_pay(card,merchant,qc_wallet,request, api_type = nil, tip = nil,user=nil,card_number=nil,type=nil,email=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_info=nil, sales_info = nil,flag=nil, can_apply_load_balancer=nil)
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			billing_address = billing_address(user, payment_gateway)
			fluid_pay = FluidPayPayment.new
			#if tip is present
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = @params[:issue_amount].to_f + tip[:sale_tip].to_f
				else
					# tip is not added when tip is not present
					charge_amount = @params[:issue_amount].to_f
				end
			else
				# tip is not added when tip is not present
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# if fluid_pay_payment.present?
			location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount, nil,card.card_type.try(:downcase) || 'credit',payment_gateway) if email.present?
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount],card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?

			@params[:hold_money_location_fee] = location_fee

			card_info_decrypted = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
			number = card_info_decrypted.number
			exp_date = "#{card_info_decrypted.month}#{card_info_decrypted.year.last(2)}"
			#cvv = card_info_decrypted.cvv

			#= building Hash for decline
			transaction_info = {amount: charge_amount,last4: card_number.last(4), first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> card.exp_date})
			transaction_info = transaction_info.merge({:userinfo=> user_info})
			#= end buildingprocess

			api_key = payment_gateway.present? ? AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret) : ENV['FLUID_PAY_ID']
			fluid_pay_payment = fluid_pay.chargeAmountRaw(api_key,dollars_to_cents(charge_amount),number,exp_date,@card_cvv,@params[:first_name],@params[:last_name],merchant,card,user,transaction_info,payment_gateway,nil,billing_address)
			@params[:order_bank] = fluid_pay_payment[:order_bank]
			return {response: fluid_pay_payment[:response], message: fluid_pay_payment[:message]} if @params[:virtual_transaction_debit]
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			receiver = email.present? ? merchant : user
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]
			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => payment_gateway.try(:name),
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info,
					"payment_gateway_id" => payment_gateway.try(:id)

			}
			to_wallet = @params[:wallet_id]
			transaction = create_transaction_issue(to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,fluid_pay_payment, payment_gateway, request, api_type)
			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			fluid_pay_payment[:order_bank].transaction_id = transaction.id
			fluid_pay_payment[:order_bank].save
			@params[:charge_id] = fluid_pay_payment.try(:[], :response)
			#= failed from bank
			return {response: nil, message: fluid_pay_payment[:message]} if fluid_pay_payment[:response].nil?

			previous_issue = {
					:issue_amount => charge_amount,
					:transaction_id =>  fluid_pay_payment.try(:[], :response),
					:merchant_name => user.name
			}
			previous_issue = previous_issue.merge(card_info)
			location_fee = nil if email.nil?
			previous_issue = nil if email.nil?
			if transaction.id.present?
				transaction.charge_id = fluid_pay_payment.try(:[], :response)
				if tip.present? && email.present?
					if tip[:sale_tip].present?
						# tip will removed from sequence transaction to amount
						charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
					end
				end
				if merchant.present? && merchant.MERCHANT? && !merchant.issue_fee
					if location_fee.present?
						# issue from sequence
						issue = merchant.issue_with_fluid_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,merchant.name,fluid_pay_payment.first,get_ip_with_request(request),nil,location_fee.first,location_fee.second, nil,api_type, tip,previous_issue,sub_type,card, fluid_pay_payment.try(:[], :response), payment_gateway, privacy_fee, sales_info,flag)
					else
						# issue from sequence
						issue = merchant.issue_with_fluid_pay(charge_amount, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  merchant.name,  fluid_pay_payment.first,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,card, fluid_pay_payment.try(:[], :response), payment_gateway, privacy_fee, sales_info,flag)
					end
					if issue.present?
						transaction_update(transaction,issue)
						parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
						# Transaction for tip transfer
						if tip.present? && (api_type == "credit" ||  api_type == "debit")
							tip_block_txn = parsed_transactions.last
							tip_block_txn[:tags]["sequence_id"] = issue[:id]
							create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
						end
						if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
							reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
							if reserve_tx.present?
								create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
							end
						end
						save_block_trans(parsed_transactions) if parsed_transactions.present?
					end
				else
					if location_fee.present?
						# issue from sequence
						issue = merchant.issue_with_fluid_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,merchant.name,fluid_pay_payment.first,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,card, fluid_pay_payment.try(:[], :response), payment_gateway, privacy_fee, sales_info,flag)
					else
						# issue from sequence
						if fee_for_issue.present?
							issue = merchant.issue_with_fluid_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,merchant.name,fluid_pay_payment.first,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,card, fluid_pay_payment.try(:[], :response), payment_gateway, privacy_fee, sales_info,flag)
						else
							issue = merchant.issue_with_fluid_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,merchant.name,fluid_pay_payment.first,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,card, fluid_pay_payment.try(:[], :response), payment_gateway, privacy_fee, sales_info,flag)
						end
					end
					if issue.present?
						transaction_update(transaction,issue)
						parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
						# Transaction for tip transfer
						if tip.present? && (api_type == "credit" ||  api_type == "debit")
							tip_block_txn = parsed_transactions.last
							tip_block_txn[:tags]["sequence_id"] = issue[:id]
							create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
						end
						if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
							reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
							if reserve_tx.present?
								create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
							end
						end
						save_block_trans(parsed_transactions) if parsed_transactions.present?
					end
				end
			end
			# end
			if issue.present?
				return issue
			end
		end

		# need to update this later for fix
		def issue_with_bolt_pay(card, merchant, qc_wallet, request, api_type = nil, tip = nil, user = nil,email = nil, type=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_bin_info = nil, sales_info = nil,flag=nil, can_apply_load_balancer=nil)
			#if tip is present
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			billing_address = billing_address(user, payment_gateway)
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = (@params[:issue_amount].to_f + tip[:sale_tip].to_f).round(2)
				else
					charge_amount = @params[:issue_amount].to_f
				end
			else
				charge_amount = @params[:issue_amount].to_f
			end

			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end

			charge_amount = charge_amount.round(2)

			# decrypting card
			card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
			exp_date = "#{card_info.month}#{card_info.year.last(2)}"
			number = card_info.number

			# transaction from bolt_pay
			bolt_pay = Payment::BoltPayGateway.new

			bolt_pay.virtual_terminal_transaction = @params[:action] == 'virtual_terminal_transaction'
			charge = bolt_pay.charge(charge_amount,card_info,user,payment_gateway, card.name, card, @card_cvv, billing_address)

			if charge[:bolt_token].present? && charge[:virtual_terminal_transaction].present? && charge[:virtual_terminal_transaction]
				card.bolt_token = charge[:bolt_token]
				card.bolt_bank = payment_gateway.try(:name)
			end
			@params[:bolt_pay_load] = charge[:data]
			# getting location fee
			if email.present?
				location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway)
			end
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			@params[:hold_money_location_fee] = location_fee

			#= building hash for Decline
			transaction_info = {amount: charge_amount, last4: card.last4, first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]
			receiver = email.present? ? merchant : user
			# if success from gateway
			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => payment_gateway.try(:name),
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info,
					"payment_gateway_id" => payment_gateway.try(:id)
			}
			card.save if card.try(:id).blank?
			to_wallet = @params[:wallet_id]
			transaction = Transaction.new
			transaction = create_transaction_issue(to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type) unless @params[:virtual_transaction_debit]
			@params[:transaction_id] = transaction.try(:id)
			@params[:order_bank] = OrderBank.new(user_id: user.id, merchant_id: merchant.id, bank_type: payment_gateway.present? ? payment_gateway.key : TypesEnumLib::GatewayType::BoltPay,card_type: card.brand,card_sub_type:card.card_type, card_id: card.try(:id),amount: charge_amount, transaction_info: transaction_info.to_json, transaction_id: transaction.try(:id), payment_gateway_id:payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id))
			@params[:slug] = transaction.try(:slug)
			@params[:charge_id] = charge.try(:[], :response)
			if charge[:response].present?
				@params[:bolt_pay_txn_id] = charge[:response]
				@params[:order_bank].status = "approve"
				@params[:order_bank].save
				return {response:  charge.try(:[], :response), message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				previous_issue = {
						:issue_amount => charge_amount,
						:transaction_id => charge[:response],
						:merchant_name => user.name
				}
				previous_issue = previous_issue.merge(card_bin_info)
				# location_fee = get_location_fees(transaction_to,user,charge_amount,@params[:amount], card.card_type.try(:downcase) || 'credit',payment_gateway.try(:key))
				location_fee = nil if email.nil?
				previous_issue = nil if email.nil?

				card.decline_attempts = 0
				card.last_decline = nil
				card.save

				if transaction.id.present?
					if tip.present? && email.present?
						if tip[:sale_tip].present?
							# tip will removed from sequence transaction to amount
							charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
						end
					end
					if user.present? && user.MERCHANT? && !user.issue_fee
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_bolt_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee, sales_info, flag)
						else
							# issue from sequence
							issue = user.issue_with_bolt_pay(charge_amount, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  user.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee, sales_info, flag)
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					else
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_bolt_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee, sales_info, flag)
						else
							# issue from sequence
							if fee_for_issue.present?
								issue = user.issue_with_bolt_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee, sales_info, flag)
							else
								issue = user.issue_with_bolt_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee, sales_info, flag)
							end
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					end
				else
					raise StandardError.new("Transaction is not saved, Please Contact Administrator")
				end
				if issue.present?
					return issue
				end
			else
				transaction_info = (transaction_info.merge(charge[:message])).to_json
				@params[:order_bank].status = "decline"
				@params[:order_bank].transaction_info = transaction_info
				@params[:order_bank].save
				card.decline_attempts = card.decline_attempts.to_i + 1
				card.last_decline = DateTime.now.utc
				card.save
				return {response:  nil, message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				return {success: false, response: nil, message: charge[:message]}
			end
		end

		def issue_with_total_pay(card, merchant, qc_wallet, request, api_type = nil, tip = nil, user = nil,email = nil, type=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_bin_info = nil, sales_info = nil,flag=nil,can_apply_load_balancer=nil,card_number=nil)
			#if tip is present
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			# billing_address = billing_address(user, payment_gateway)
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = (@params[:issue_amount].to_f + tip[:sale_tip].to_f).round(2)
				else
					charge_amount = @params[:issue_amount].to_f
				end
			else
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# decrypting card
			card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
			number = card_info.number
			exp_date = "#{card_info.month}#{card_info.year.last(2)}"
			#cvv = card_info.cvv

			# transaction from total pay
			total_pay = Payment::TotalPay.new
			charge = total_pay.charge(charge_amount,number,exp_date,@card_cvv, @params,request,payment_gateway,card_bin_info,merchant)
			# getting location fee
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]
			if email.present?
				location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway)
			end
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			@params[:hold_money_location_fee] = location_fee
			# if failure
			# return {response: nil, message: charge[:message]} if charge[:response].nil?
			@params[:total_pay_txn_id] = charge[:response]
			@params[:bank_descriptor] = charge[:descriptor]
			#= building hash for Decline
			transaction_info = {amount: charge_amount, last4: card.last4, first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			receiver = email.present? ? merchant : user
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process
			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			gateway_name=payment_gateway.try(:name)
			if payment_gateway.present?
				gateway_name=charge[:descriptor].present? ? charge[:descriptor] : gateway_name
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => gateway_name,
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info

			}
			to_wallet = @params[:wallet_id]
			card.save if card.try(:id).blank?
			transaction = Transaction.new
			transaction = create_transaction_issue( to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type) unless @params[:virtual_transaction_debit]
			@params[:order_bank] = OrderBank.new(user_id: user.id, merchant_id: merchant.id, bank_type: payment_gateway.present? ? payment_gateway.key : "converge",card_type: card.brand, card_id: card.try(:id),card_sub_type:card.card_type,amount: charge_amount, transaction_info: transaction_info.to_json, transaction_id: transaction.id, payment_gateway_id:payment_gateway.try(:id),load_balancer_id:payment_gateway.try(:load_balancer_id))
			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			@params[:charge_id] = charge.try(:[], :response)
			if charge[:response].present?
				@params[:order_bank].status = "approve"
				@params[:order_bank].save
				return {response:  charge.try(:[], :response), message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				previous_issue = {
						:issue_amount => charge_amount,
						:transaction_id => charge[:response],
						:merchant_name => user.name
				}
				previous_issue = previous_issue.merge(card_bin_info)
				location_fee = nil if email.nil?
				previous_issue = nil if email.nil?

				card.decline_attempts = 0
				card.last_decline = nil
				card.save

				if transaction.id.present?
					if tip.present? && email.present?
						if tip[:sale_tip].present?
							# tip will removed from sequence transaction to amount
							charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
						end
					end
					if user.present? && user.MERCHANT? && !user.issue_fee
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_totalpay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
						else
							# issue from sequence
							issue = user.issue_with_totalpay(charge_amount, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  user.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					else
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_totalpay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
						else
							# issue from sequence
							if fee_for_issue.present?
								issue = user.issue_with_totalpay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
							else
								issue = user.issue_with_totalpay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
							end
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					end
				else
					raise StandardError.new("Transaction is not saved, Please Contact Administrator")
				end
				if issue.present?
					return issue
				end
			else
				transaction_info = (transaction_info.merge(charge[:message])).to_json
				@params[:order_bank].status = "decline"
				@params[:order_bank].transaction_info = transaction_info
				@params[:order_bank].save
				card.decline_attempts = card.decline_attempts.to_i + 1
				card.last_decline = DateTime.now.utc
				card.save
				return {response:  nil, message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				return {success: false, response: nil, message: charge[:message]}
			end
		end

		def issue_with_converge(card, merchant, qc_wallet, request, api_type = nil, tip = nil, user = nil,email = nil, type=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_bin_info = nil, sales_info = nil,flag=nil,can_apply_load_balancer=nil)
			#if tip is present
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			billing_address = billing_address(user, payment_gateway)
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = (@params[:issue_amount].to_f + tip[:sale_tip].to_f).round(2)
				else
					charge_amount = @params[:issue_amount].to_f
				end
			else
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# decrypting card
			card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
			number = card_info.number
			exp_date = "#{card_info.month}#{card_info.year.last(2)}"
			#cvv = card_info.cvv

			# transaction from elavon
			elavon = Payment::ElavonGateway.new
			charge = elavon.charge(charge_amount,number,exp_date,@card_cvv,payment_gateway, billing_address)
			# getting location fee
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]

			if email.present?
				location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway)
			end
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			@params[:hold_money_location_fee] = location_fee
			# if failure
			# return {response: nil, message: charge[:message]} if charge[:response].nil?
			@params[:total_pay_txn_id] = charge[:response]

			#= building hash for Decline
			transaction_info = {amount: charge_amount, last4: card.last4, first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			receiver = email.present? ? merchant : user
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process

			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => payment_gateway.try(:name),
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info,
					"payment_gateway_id" => payment_gateway.try(:id)

			}
			to_wallet = @params[:wallet_id]
			card.save if card.try(:id).blank?
			transaction = Transaction.new
			transaction = create_transaction_issue( to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type) unless @params[:virtual_transaction_debit]
			@params[:order_bank] = OrderBank.new(user_id: user.id, merchant_id: merchant.id, bank_type: payment_gateway.present? ? payment_gateway.key : "converge",card_type: card.brand, card_id: card.try(:id),card_sub_type:card.card_type,amount: charge_amount, transaction_info: transaction_info.to_json, transaction_id: transaction.id, payment_gateway_id:payment_gateway.try(:id),load_balancer_id:payment_gateway.try(:load_balancer_id))
			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			@params[:charge_id] = charge.try(:[], :response)
			if charge[:response].present?
				@params[:order_bank].status = "approve"
				@params[:order_bank].save
				return {response:  charge.try(:[], :response), message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				previous_issue = {
						:issue_amount => charge_amount,
						:transaction_id => charge[:response],
						:merchant_name => user.name
				}
				previous_issue = previous_issue.merge(card_bin_info)
				location_fee = nil if email.nil?
				previous_issue = nil if email.nil?

				card.decline_attempts = 0
				card.last_decline = nil
				card.save

				if transaction.id.present?
					if tip.present? && email.present?
						if tip[:sale_tip].present?
							# tip will removed from sequence transaction to amount
							charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
						end
					end
					if user.present? && user.MERCHANT? && !user.issue_fee
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_elavon(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
						else
							# issue from sequence
							issue = user.issue_with_elavon(charge_amount, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  user.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					else
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_elavon(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
						else
							# issue from sequence
							if fee_for_issue.present?
								issue = user.issue_with_elavon(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
							else
								issue = user.issue_with_elavon(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
							end
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					end
				else
					raise StandardError.new("Transaction is not saved, Please Contact Administrator")
				end
				if issue.present?
					return issue
				end
			else
				transaction_info = (transaction_info.merge(charge[:message])).to_json
				@params[:order_bank].status = "decline"
				@params[:order_bank].transaction_info = transaction_info
				@params[:order_bank].save
				card.decline_attempts = card.decline_attempts.to_i + 1
				card.last_decline = DateTime.now.utc
				card.save
				return {response:  nil, message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				return {success: false, response: nil, message: charge[:message]}
			end
		end

		def issue_with_knox(card, merchant, qc_wallet, request, api_type = nil, tip = nil, user = nil,email = nil, type=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_bin_info = nil, sales_info = nil,flag=nil,can_apply_load_balancer=nil)
			#if tip is present
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			billing_address = billing_address(user, payment_gateway)
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = (@params[:issue_amount].to_f + tip[:sale_tip].to_f).round(2)
				else
					charge_amount = @params[:issue_amount].to_f
				end
			else
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# decrypting card
			card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
			number = card_info.number
			exp_date = "#{card_info.month}#{card_info.year.last(2)}"
			#cvv = card_info.cvv

			# transaction from knox
			knox = Payment::KnoxGateway.new(payment_gateway)
			charge = knox.charge(charge_amount,number,exp_date,@card_cvv, @params,request,card_bin_info,merchant)
			# charge = knox.charge_token(charge_amount,number,exp_date,@card_cvv, @params,request,payment_gateway,card_bin_info)
			# getting location fee
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]

			if email.present?
				location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway)
			end
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			@params[:hold_money_location_fee] = location_fee
			# if failure
			# return {response: nil, message: charge[:message]} if charge[:response].nil?
			@params[:knox_txn_id] = charge[:response]
			@params[:bank_descriptor] = charge[:descriptor]
			#= building hash for Decline
			transaction_info = {amount: charge_amount, last4: card.last4, first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			receiver = email.present? ? merchant : user
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process

			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			gateway_name=payment_gateway.try(:name)
			if payment_gateway.present?
				gateway_name=charge[:descriptor].present? ? charge[:descriptor] : gateway_name
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => gateway_name,
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info

			}
			to_wallet = @params[:wallet_id]
			card.save if card.try(:id).blank?
			transaction = Transaction.new
			transaction = create_transaction_issue( to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type) unless @params[:virtual_transaction_debit]
			@params[:order_bank] = OrderBank.new(user_id: user.id, merchant_id: merchant.id, bank_type: payment_gateway.present? ? payment_gateway.key : "knox_payments",card_type: card.brand, card_id: card.try(:id),card_sub_type:card.card_type,amount: charge_amount, transaction_info: transaction_info.to_json, transaction_id: transaction.id, payment_gateway_id:payment_gateway.try(:id),load_balancer_id:payment_gateway.try(:load_balancer_id))
			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			@params[:charge_id] = charge.try(:[], :response)
			if charge[:response].present?
				@params[:order_bank].status = "approve"
				@params[:order_bank].save
				return {response:  charge.try(:[], :response), message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				previous_issue = {
						:issue_amount => charge_amount,
						:transaction_id => charge[:response],
						:merchant_name => user.name
				}
				previous_issue = previous_issue.merge(card_bin_info)
				location_fee = nil if email.nil?
				previous_issue = nil if email.nil?

				card.decline_attempts = 0
				card.last_decline = nil
				card.save

				if transaction.id.present?
					if tip.present? && email.present?
						if tip[:sale_tip].present?
							# tip will removed from sequence transaction to amount
							charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
						end
					end
					if user.present? && user.MERCHANT? && !user.issue_fee
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_knox(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
						else
							# issue from sequence
							issue = user.issue_with_knox(charge_amount, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  user.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					else
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_knox(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
						else
							# issue from sequence
							if fee_for_issue.present?
								issue = user.issue_with_knox(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
							else
								issue = user.issue_with_knox(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag,charge[:descriptor])
							end
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					end
				else
					raise StandardError.new("Transaction is not saved, Please Contact Administrator")
				end
				if issue.present?
					return issue
				end
			else
				transaction_info = (transaction_info.merge(charge[:message])).to_json
				@params[:order_bank].status = "decline"
				@params[:order_bank].transaction_info = transaction_info
				@params[:order_bank].save
				card.decline_attempts = card.decline_attempts.to_i + 1
				card.last_decline = DateTime.now.utc
				card.save
				return {response:  nil, message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				return {success: false, response: nil, message: charge[:message]}
			end
		end


		def issue_with_i_can_pay(card, merchant, qc_wallet, request, api_type = nil, tip = nil, user = nil,email = nil, type=nil,fee_for_issue=nil,sub_type=nil,secure_i_can_pay=nil,payment_gateway=nil,gateway_number=nil,sales_info = nil,flag=nil,can_apply_load_balancer=nil)
			payment = payment_gateway
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			#if tip is present
			if tip.present?
				if tip[:sale_tip].present?
					charge_amount = @params[:issue_amount].to_f + tip[:sale_tip].to_f
				else
					charge_amount = @params[:issue_amount].to_f
				end
			else
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# transaction from I can pay
			i_can_pay = Payment::ICanPay.new(payment_gateway)
				# decrypting card
				card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
				number = card_info.number
				exp_date = "#{card_info.month}#{card_info.year.last(2)}"
				#cvv = card_info.cvv
				charge = i_can_pay.charge(charge_amount,number,exp_date,@card_cvv,@params,request)

			# getting location fee
			if email.present?
				location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway)
			end
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			@params[:hold_money_location_fee] = location_fee

			#= building hash for Decline
			transaction_info = {amount: charge_amount, last4: card.last4, first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process

			# if success
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]
			receiver = email.present? ? merchant : user
			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			gateway_name=payment_gateway.try(:name)
			if payment_gateway.present?
				gateway_name=charge[:descriptor].present? ? charge[:descriptor] : gateway_name
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => gateway_name,
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info,
					"payment_gateway_id" => payment_gateway.try(:id)

			}
			to_wallet = @params[:wallet_id]
			card.save if card.try(:id).blank?
			transaction = Transaction.new
			transaction = create_transaction_issue(to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type) unless @params[:virtual_transaction_debit]
			@params[:order_bank] = OrderBank.new(user_id: user.id, merchant_id: merchant.id, bank_type: payment_gateway.try(:key) ,card_type: card.brand, card_id: card.try(:id),card_sub_type:card.card_type,amount: charge_amount, transaction_info: transaction_info.to_json, transaction_id: transaction.id, payment_gateway_id: payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id))
			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			if charge[:response].present?
				@params[:i_can_pay_txn_id] = charge[:response]
				@params[:bank_descriptor] = charge[:descriptor]
				@params[:i_can_pay_txn_amount] = charge_amount
				@params[:order_bank].status = "approve"
				@params[:order_bank].save
				return {response:  charge.try(:[], :response), message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				previous_issue = {
						:issue_amount => charge_amount,
						:transaction_id => charge[:response],
						:merchant_name => user.name
				}
				previous_issue = previous_issue.merge(get_card_info(number))
				location_fee = nil if email.nil?
				previous_issue = nil if email.nil?

				card.decline_attempts = 0
				card.last_decline = nil
				card.save

				if transaction.id.present?
					if tip.present? && email.present?
						if tip[:sale_tip].present?
							# tip will removed from sequence transaction to amount
							charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
						end
					end
					if merchant.present? && merchant.MERCHANT? && !merchant.issue_fee
						if location_fee.present?
							# issue from sequence
							issue = merchant.issue_with_iCanPay(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee,sales_info,flag,charge.try(:[],:descriptor))
						else
							# issue from sequence
							issue = merchant.issue_with_iCanPay(charge_amount.to_f, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  user.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee,sales_info,flag,charge.try(:[],:descriptor))
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					else
						if location_fee.present?
							# issue from sequence
							issue = merchant.issue_with_iCanPay(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee,sales_info,flag,charge.try(:[],:descriptor))
						else
							# issue from sequence
							if fee_for_issue.present?
								issue = merchant.issue_with_iCanPay(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee,sales_info,flag,charge.try(:[],:descriptor))
							else
								issue = merchant.issue_with_iCanPay(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:response],payment_gateway,privacy_fee,sales_info,flag,charge.try(:[],:descriptor))
							end
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					end
				else
					raise StandardError.new("Transaction is not saved, Please Contact Administrator")
				end
				if issue.present?
					return issue
				end
			else
				transaction_info = (transaction_info.merge(charge[:message])).to_json
				@params[:order_bank].status = "decline"
				@params[:order_bank].transaction_info = transaction_info
				@params[:order_bank].save
				card.decline_attempts = card.decline_attempts.to_i + 1
				card.last_decline = DateTime.now.utc
				card.save
				return {response:  nil, message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				return {response: nil, message: charge[:message]}
			end
		end

		def issue_with_stripe(card, merchant, qc_wallet, request, api_type = nil, tip = nil,card_number=nil ,gateway = nil,user=nil,email = nil,type=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_info = nil, sales_info = nil,flag=nil,can_apply_load_balancer=nil)
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			billing_address = billing_address(user, payment_gateway)
			@current_stripe_type = nil
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = @params[:issue_amount].to_f + tip[:sale_tip].to_f
				else
					# amount when tip is not present
					charge_amount = @params[:issue_amount].to_f
				end
			else
				# amount when tip is not present
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# transaction from stripe
			if payment_gateway.present?
				key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
			else
				key = ENV["#{gateway}_secret_key"]
			end
			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			stripe = Payment::StripeGateway.new
			#TODO remove other checks of strip only check 1
			if gateway == "stripe" || (payment_gateway.present? && payment_gateway.type == "stripe")
				customer = stripe.stripe_customer(user,user.stripe_customer_id,key,email) unless email.present?
				customer = stripe.stripe_customer(user,nil,key,email) if customer.nil?
				# card_stripe_data = @tokenization.stripe(@params[:card_holder_name], @params[:card_number], @params[:card_exp_date], @params[:card_cvv], customer, nil,key)
				# card.stripe = card_stripe_data[:card_id]
				user.stripe_customer_id = customer.id if customer.present? && user.present?
				#TODO should be removed after removal of previous method
			elsif gateway == "stripe_pop" || (payment_gateway.present? && payment_gateway.type == "stripe_pop")
				customer = stripe.stripe_customer(user,user.second_stripe_id,key,email) unless email.present?
				customer = stripe.stripe_customer(user,nil,key,email) if customer.nil?
				user.second_stripe_id = customer.id if customer.present? && user.present?
				#TODO should be removed after removal of previous method
			elsif gateway == "stripe_pop_2" || (payment_gateway.present? && payment_gateway.type == "stripe_pop_2")
				customer = stripe.stripe_customer(user,user.third_stripe_id,key,email) unless email.present?
				customer = stripe.stripe_customer(user,nil,key,email) if customer.nil?
				user.third_stripe_id = customer.id if customer.present? && user.present?
			end
			user.save unless email.present?
			location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.present?
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			# location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,nil, user) if email.present?
			@params[:hold_money_location_fee] = location_fee
			#= building hash for Decline
			transaction_info = {amount: charge_amount.to_f, last4: card.last4, first6: card_number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> card.exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process
			charge = issue_from_stripe(charge_amount,  customer.id, user.id, card_number, key,merchant.id,card,transaction_info,gateway,payment_gateway,nil,@card_cvv,@params[:virtual_transaction_debit], billing_address)
			@params[:order_bank] = charge[:order_bank]
			return {response:  charge.try(:[], :response), message:  charge[:message]} if @params[:virtual_transaction_debit]
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]
			receiver = email.present? ? merchant : user
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => payment_gateway.try(:name),
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info,
					"payment_gateway_id" => payment_gateway.try(:id)
			}
			to_wallet = @params[:wallet_id]
			transaction = create_transaction_issue(to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type)

			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			charge[:order_bank].transaction_id = transaction.id
			charge[:order_bank].save

			#= failed from bank
			return {success: false,response: nil, message: charge[:message]} if charge[:response].nil?

			previous_issue = {
					:issue_amount => charge_amount,
					:transaction_id => charge[:response],
					:merchant_name => merchant.name
			}
			previous_issue = previous_issue.merge(card_info)

			# location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount]) if email.present?

			# intializing location_fee & previous_issue with nil (when user & merchant both present )
			location_fee = nil if email.nil?
			previous_issue = nil if email.nil?
			key = nil # for fee that will direct transacfer to qc wallet instead of escrow
			@params[:charge_id] = charge[:charge][:charge].id unless charge[:response].nil?
			if transaction.id.present?
				if tip.present? && email.present?
					if tip[:sale_tip].present?
						# tip will removed from sequence transaction to amount
						charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
					end
				end
				if merchant.present? && merchant.MERCHANT? && !merchant.issue_fee
					if location_fee.present?
						# issue from sequence
						issue = merchant.issue_with_stripe_new(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,merchant.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,gateway,key,charge,payment_gateway, privacy_fee, sales_info,flag)
					else
						# issue from sequence
						issue = merchant.issue_with_stripe_new(charge_amount.to_f, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  merchant.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,gateway,key,charge,payment_gateway, privacy_fee, sales_info,flag)
					end
					if issue.present?
						transaction_update(transaction,issue)
						parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
						# Transaction for tip transfer
						if tip.present? && (api_type == "credit" ||  api_type == "debit")
							tip_block_txn = parsed_transactions.last
							tip_block_txn[:tags]["sequence_id"] = issue[:id]
							create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], get_ip_with_request(request), nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
						end
						if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
							reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
							if reserve_tx.present?
								create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
							end
						end
						save_block_trans(parsed_transactions) if parsed_transactions.present?
					end
				else
					if location_fee.present?
						# issue from sequence
						issue = merchant.issue_with_stripe_new(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,merchant.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,gateway,key,charge,payment_gateway, privacy_fee, sales_info,flag)
					else
						# issue from sequence
						if fee_for_issue.present?
							issue = merchant.issue_with_stripe_new(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,merchant.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,gateway,key,charge,payment_gateway, privacy_fee, sales_info,flag)
						else
							issue = merchant.issue_with_stripe_new(charge_amount.to_f,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,merchant.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,gateway,key,charge,payment_gateway, privacy_fee, sales_info,flag)
						end
					end
					if issue.present?
						transaction_update(transaction,issue)
						parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
						# Transaction for tip transfer
						if tip.present? && (api_type == "credit" ||  api_type == "debit")
							tip_block_txn = parsed_transactions.last
							tip_block_txn[:tags]["sequence_id"] = issue[:id]
							create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], get_ip_with_request(request), nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
						end
						if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
							reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
							if reserve_tx.present?
								create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
							end
						end
						save_block_trans(parsed_transactions) if parsed_transactions.present?
					end
				end
			end
			# return false
			if issue.present?
				return issue
			end
		end

		def issue_with_rtp(merchant, request, api_type = nil, user=nil, email = nil, type=nil, fee_for_issue=nil,sub_type=nil, charge)
			charge_amount = @params[:issue_amount]
			transaction_from = user.wallets.primary.first.id
			transaction_to = @params[:wallet_id]
			receiver =  user
			wallet = Wallet.where(id: transaction_to).first
			tags = {
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
			}
			to_wallet = @params[:wallet_id]
			transaction = create_transaction_issue(to_wallet, transaction_from, receiver, nil, 0, nil, 0, nil, charge_amount, nil, sub_type, tags, nil, {response: charge}, nil, request, api_type)

			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug

			previous_issue = {
					:issue_amount => charge_amount,
					:transaction_id => charge,
					:merchant_name => merchant.name
			}

			@params[:charge_id] = charge
			if transaction.id.present?
				issue = SequenceLib.issue(to_wallet, charge_amount, wallet, type, 0,nil, "Quickcard", nil,nil, merchant.name,nil, nil, get_ip_with_request(request), nil,nil,previous_issue,nil,nil,nil, nil, nil, sub_type)
				issue = {id: issue.actions.first.id,transaction_id: charge ,timestamp: issue.timestamp, asset: issue.actions.first.flavor_id, type: issue.actions.first.tags["type"], sub_type: issue.actions.first.tags["sub_type"] , destination: issue.actions.first.destination_account_id, issue_amount: issue.actions.first.amount.to_f/100, tags: issue.actions.first.tags, complete_trans: issue.actions}
				if issue.present?
					transaction_update(transaction,issue)
					parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
					save_block_trans(parsed_transactions) if parsed_transactions.present?
				end
			end
			return issue
		end

		def issue_with_mentom_pay(card, merchant, qc_wallet, request, api_type = nil, tip = nil, user = nil,email = nil, type=nil,fee_for_issue=nil,sub_type=nil,payment_gateway=nil,card_bin_info = nil, sales_info = nil,flag=nil,can_apply_load_balancer=nil)
			#if tip is present
			if can_apply_load_balancer.present? && can_apply_load_balancer
			else
				issue = check_mids_limit(payment_gateway)
				if issue.present?
					return {success: false,response: nil, message: {message: issue}}
				end
			end
			billing_address = billing_address(user, payment_gateway)
			if tip.present?
				if tip[:sale_tip].present?
					# tip will added to amount
					charge_amount = (@params[:issue_amount].to_f + tip[:sale_tip].to_f).round(2)
				else
					charge_amount = @params[:issue_amount].to_f
				end
			else
				charge_amount = @params[:issue_amount].to_f
			end
			if @params[:privacy_fee].present?
				privacy_fee = @params[:privacy_fee].to_f
				charge_amount = charge_amount.to_f + privacy_fee
			end
			charge_amount = charge_amount.round(2)
			# decrypting card
			card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
			number = card_info.number
			exp_date = "#{card_info.month}#{card_info.year.last(2)}"
			#cvv = card_info.cvv
			# transaction from Mentom
			mentom = Payment::MentomPay.new(payment_gateway)
			charge = mentom.charge(charge_amount,number,exp_date,@card_cvv, user.name)
			# getting location fee
			if api_type == 'debit'
				from_user = "Debit"
			elsif api_type == "credit"
				from_user = "Credit"
			end
			transaction_from = email.present? ? from_user : user.wallets.primary.first.id
			transaction_to = @params[:merchant_wallet_id] || @params[:wallet_id]

			if email.present?
				location_fee = get_location_fees(@params[:wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway)
			end
			location_fee = get_location_fees(@params[:merchant_wallet_id],merchant,charge_amount,@params[:issue_amount], card.card_type.try(:downcase) || 'credit',payment_gateway) if email.nil? && location_fee.nil?
			@params[:hold_money_location_fee] = location_fee
			# if failure
			# return {response: nil, message: charge[:message]} if charge[:response].nil?
			@params[:mentom_pay_txn_id] = charge[:response]

			#= building hash for Decline
			transaction_info = {amount: charge_amount, last4: card.last4, first6: number.first(6), ip: get_ip_with_request(request), type:type}
			reserve_detail = location_fee.first if location_fee.present?
			user_info = location_fee.second if location_fee.present?
			fee = location_fee.third if location_fee.present?
			fee = fee_for_issue.to_f if fee.nil? && fee_for_issue.present?
			reciever_wallett_id = email.present? ? @params["wallet_id"] : @params["merchant_wallet_id"]
			sender_wallett_id = email.present? ? "Credit" : @params["wallet_id"]
			receiver = email.present? ? merchant : user
			transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:sender_wallet_id => sender_wallett_id, :reciever_wallet_id => reciever_wallett_id,:request_url => request.host,:expiry_date=> exp_date})
			transaction_info = transaction_info.merge({:user_info => user_info})
			#= end building process

			gateway_fees = User.get_gateway_details(payment_gateway) if payment_gateway.present?
			wallet = Wallet.where(id: transaction_to).first
			if email.blank?
				location_fee = nil
				user_info = nil
				fee = nil
				reserve_detail = nil
			end
			gateway_name=payment_gateway.try(:name)
			if payment_gateway.present?
				gateway_name=charge[:descriptor].present? ? charge[:descriptor] : gateway_name
			end
			tags = {
					"fee_perc" => user_info,
					"gateway_fee_details" => gateway_fees,
					"descriptor" => gateway_name,
					"location" => location_to_parse(wallet.try(:location)),
					"merchant" => merchant_to_parse(wallet.try(:location).try(:merchant)),
					"sales_info" => sales_info,
					"payment_gateway_id" => payment_gateway.try(:id)

			}
			to_wallet = @params[:wallet_id]
			card.save if card.try(:id).blank?
			transaction = Transaction.new
			transaction = create_transaction_issue( to_wallet, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type) unless @params[:virtual_transaction_debit]
			@params[:order_bank] = OrderBank.new(user_id: user.id, merchant_id: merchant.id, bank_type: payment_gateway.present? ? payment_gateway.key : "mentom_pay",card_type: card.brand, card_id: card.try(:id),card_sub_type:card.card_type,amount: charge_amount, transaction_info: transaction_info.to_json, transaction_id: transaction.id, payment_gateway_id:payment_gateway.try(:id),load_balancer_id:payment_gateway.try(:load_balancer_id))
			@params[:transaction_id] = transaction.id
			@params[:slug] = transaction.slug
			@params[:charge_id] = charge.try(:[], :response)
			if charge[:response].present?
				@params[:order_bank].status = "approve"
				@params[:order_bank].save
				return {response:  charge.try(:[], :response), message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				previous_issue = {
						:issue_amount => charge_amount,
						:transaction_id => charge[:response],
						:merchant_name => user.name
				}
				previous_issue = previous_issue.merge(card_bin_info)
				location_fee = nil if email.nil?
				previous_issue = nil if email.nil?

				card.decline_attempts = 0
				card.last_decline = nil
				card.save

				if transaction.id.present?
					if tip.present? && email.present?
						if tip[:sale_tip].present?
							# tip will removed from sequence transaction to amount
							charge_amount = charge_amount.to_f - tip[:sale_tip].to_f
						end
					end
					if user.present? && user.MERCHANT? && !user.issue_fee
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_mentom_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,location_fee.third,card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
						else
							# issue from sequence
							issue = user.issue_with_mentom_pay(charge_amount, @params[:wallet_id],transaction_to, TypesEnumLib::TransactionType::SaleType, 0,  card.last4,  user.name,  get_ip_with_request(request), nil, nil, nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					else
						if location_fee.present?
							# issue from sequence
							issue = user.issue_with_mentom_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,@fee_lib.get_card_fee(charge_amount, 'card'),card.last4,user.name,get_ip_with_request(request),nil,location_fee.first,location_fee.second, location_fee.third, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
						else
							# issue from sequence
							if fee_for_issue.present?
								issue = user.issue_with_mentom_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,fee_for_issue,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
							else
								issue = user.issue_with_mentom_pay(charge_amount,@params[:wallet_id],transaction_to,TypesEnumLib::TransactionType::SaleType,0,card.last4,user.name,get_ip_with_request(request),nil,nil,nil,nil, api_type, tip,previous_issue,sub_type,nil,card,nil,nil,charge[:approval_code],charge[:response],payment_gateway,privacy_fee, sales_info,flag)
							end
						end
						if issue.present?
							transaction_update(transaction,issue)
							parsed_transactions = parse_block_transactions(issue[:complete_trans], issue[:timestamp])
							# Transaction for tip transfer
							if tip.present? && (api_type == "credit" ||  api_type == "debit")
								tip_block_txn = parsed_transactions.last
								tip_block_txn[:tags]["sequence_id"] = issue[:id]
								create_transaction_helper(merchant, tip[:wallet_id], to_wallet, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
							end
							if reserve_detail.present? && (api_type == "credit" ||  api_type == "debit")
								reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
								if reserve_tx.present?
									create_transaction_helper(merchant, reserve_detail["wallet"], to_wallet, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip_with_request(request), nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
								end
							end
							save_block_trans(parsed_transactions) if parsed_transactions.present?
						end
					end
				else
					raise StandardError.new("Transaction is not saved, Please Contact Administrator")
				end
				if issue.present?
					return issue
				end
			else
				transaction_info = (transaction_info.merge(charge[:message])).to_json
				@params[:order_bank].status = "decline"
				@params[:order_bank].transaction_info = transaction_info
				@params[:order_bank].save
				card.decline_attempts = card.decline_attempts.to_i + 1
				card.last_decline = DateTime.now.utc
				card.save
				return {response:  nil, message:  charge[:message].try(:[], :message)} if @params[:virtual_transaction_debit]
				return {success: false, response: nil, message: charge[:message]}
			end
		end


		def create_transaction_issue(transaction_to, transaction_from, receiver, card, fee, tip, privacy_fee, reserve_detail, charge_amount, user_info, sub_type, tags, gateway_fees,charge, payment_gateway, request, api_type)
			main_type = updated_type(TypesEnumLib::TransactionType::SaleType, sub_type)
			gbox_fee = save_gbox_fee(tags.try(:[],"fee_perc"), fee) || 0
			total_fee = save_total_fee(tags.try(:[],"fee_perc"), fee) || 0
			sub_type = TypesEnumLib::TransactionSubType::QRCreditCard if api_type.present? && api_type == "qr_code"
			if api_type.present? && api_type == "qr_code"
				net_fee = 0
				net_amount = charge_amount
			else
				net_fee = number_with_precision(total_fee.to_f - privacy_fee.to_f, precision: 2)
				net_amount = number_with_precision(charge_amount.to_f - total_fee.to_f - reserve_detail.try(:[],:amount).to_f - tip.try(:[], :sale_tip).to_f, precision: 2)
      end
      dba_name=get_business_name(Wallet.find_by(id:transaction_to))
			transaction = Transaction.create(
					to: transaction_to,
					from: transaction_from,
					status: "pending",
					amount: @params[:issue_amount],
					receiver_id: receiver.try(:id),
					receiver_name: dba_name,
					receiver_wallet_id: transaction_to,
					receiver_balance: SequenceLib.balance(transaction_to).to_f,
					action: 'issue',
					last4: card.try(:last4),
					first6: card.try(:first6),
					fee: total_fee,
					tip: tip.try(:[],:sale_tip).to_f,
					privacy_fee: privacy_fee.to_f,
					reserve_money: reserve_detail.try(:[],:amount).to_f,
					net_fee: net_fee.to_f,
					net_amount: net_amount.to_f,
					total_amount: charge_amount.to_f,
					gbox_fee: gbox_fee,
					iso_fee: save_iso_fee(tags.try(:[],"fee_perc")),
					agent_fee: user_info.try(:[],"agent").try(:[],"amount").to_f,
					affiliate_fee: user_info.try(:[],"affiliate").try(:[],"amount").to_f,
					ip: get_ip_with_request(request),
					main_type: main_type,
					sub_type: sub_type,
					tags: tags,
					card_id: card.try(:id),
					account_processing_limit: gateway_fees.try(:[],:account_processing_limit),
					transaction_fee: gateway_fees.try(:[],:transaction_fee),
					charge_back_fee: gateway_fees.try(:[],:charge_back_fee),
					retrieval_fee: gateway_fees.try(:[],:retrieval_fee),
					per_transaction_fee: gateway_fees.try(:[],:per_transaction_fee),
					reserve_money_fee: gateway_fees.try(:[],:reserve_money_fee),
					reserve_money_days: gateway_fees.try(:[],:reserve_money_days),
					monthly_service_fee: gateway_fees.try(:[],:monthly_service_fee),
					misc_fee: gateway_fees.try(:[],:misc_fee),
					misc_fee_dollars: gateway_fees.try(:[],:misc_fee_dollars),
					payment_gateway_id: payment_gateway.try(:id),
					load_balancer_id: payment_gateway.try(:load_balancer_id),
					charge_id: charge.try(:[], :response),
					auth_code: charge.try(:[],:approval_code),
					ref_id: @params[:ref_id],
					bank_reference: charge.try(:[], :reference),
					terminal_id: @terminal_id,
					clerk_id: @params[:clerk_id]
			)
			@params[:issue_quickcard_id] = transaction.quickcard_id
			transaction
		end

		def transaction_update(transaction,issue)
			transaction.update(seq_transaction_id: issue[:id],
												 status: "approved",
												 tags: issue[:tags],
												 timestamp: issue[:timestamp]
			)
		end

		def check_mids_limit(payment_gateway)
			app_config = AppConfig.where(key: AppConfig::Key::MidDisable).first
			if app_config.present? && app_config.boolValue
				if payment_gateway.present?
					date = Date.today.at_beginning_of_month
		    	monthly_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
					monthly_amount_limit = monthly_amount_limit.to_f + @params[:issue_amount] if @params[:issue_amount].present?
					d_date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day
	    		daily_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",d_date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
	 				daily_amount_limit = daily_amount_limit.to_f + @params[:issue_amount] if @params[:issue_amount].present?
					if payment_gateway.high_ticket.present?
						if @params[:issue_amount] > payment_gateway.high_ticket
							SlackService.notify(I18n.t('mid_limits.ticket_limit_exceed'), ENV["NOTIFY_BANK_DECLINES"])
							return I18n.t('mid_limits.ticket_limit_exceed')
						end
					end
					if payment_gateway.monthly_limit.present?
						if monthly_amount_limit > payment_gateway.monthly_limit
							SlackService.notify(I18n.t('mid_limits.monthly_limit_volume_exceed'), ENV["NOTIFY_BANK_DECLINES"])
							return I18n.t('mid_limits.monthly_limit_volume_exceed')
						end
					end
					if payment_gateway.daily_limit.present?
						if daily_amount_limit > payment_gateway.daily_limit
							SlackService.notify(I18n.t('mid_limits.daily_limit_volume_exceed'), ENV["NOTIFY_BANK_DECLINES"])
							return I18n.t('mid_limits.daily_limit_volume_exceed')
						end
					end
					if payment_gateway.present?
						eighty_percent_monthly = (payment_gateway.monthly_limit * 0.8 ).to_f if payment_gateway.monthly_limit.present?
						eighty_percent_daily = (payment_gateway.daily_limit * 0.8 ).to_f if payment_gateway.daily_limit.present?
						flag = false
						if payment_gateway.monthly_limit.present?
							if monthly_amount_limit > eighty_percent_monthly && monthly_amount_limit < payment_gateway.monthly_limit
								SlackService.notify(I18n.t('mid_limits.monthly_eighty_percent_exceed', descriptor: payment_gateway.name, id: payment_gateway.id), ENV["NOTIFY_BANK_DECLINES"])
								flag = true
							end
						end
						if payment_gateway.daily_limit.present?
							if daily_amount_limit > eighty_percent_daily && daily_amount_limit < payment_gateway.daily_limit
								SlackService.notify(I18n.t('mid_limits.daily_eighty_percent_exceed', descriptor: payment_gateway.name, id: payment_gateway.id), ENV["NOTIFY_BANK_DECLINES"])
								flag = true
							end
						end
						return  "" if flag == true
					end
				end
			end
		end

		def billing_address(user, payment_gateway)
			result = {require_address: false}
			if payment_gateway.virtual_tx
				result.merge!({require_address: true, country: params[:country], city: params[:city], state: params[:state], zip: params[:zip_code], street: params[:street] || params[:postal_address], postalcode: params[:zip_code], phone:params[:phone_number]})
				# addresses = user.addresses.select{|a|a.address_type == "Billing"}
				# if addresses.present?
				# 	address = addresses.last
				# 	result.merge!({require_address: true, country: address.country, city: address.city, state: address.state, zip: address.zip, street: address.street, postalcode: address.zip})
				# else
				# 	result.merge!({require_address: true, country: user.country, city: user.city, state: user.state, zip: user.zip_code, street: user.street, postalcode: user.zip_code})
				# end
			end
			return result
		end

	end

	class DisputeCaseTransaction
		include ActionView::Helpers::NumberHelper
		include Admins::BlockTransactionsHelper
		include PosMerchantHelper
		include ApplicationHelper
		def initialize
		end
		# def get_location_fee(from_id,fee_type,when_charge_fee_off=nil, amount = nil)
		# 	unless when_charge_fee_off
		# 		return {fee_amount: 0, fee_object: nil, fee_location: 0}
		# 	end
		#
		# 	@to = Wallet.find(from_id)
		# 	if @to.location.nil?
		# 		return {fee_amount: 0, fee_object: nil, fee_location: nil}
		# 	end
		# 	location = @to.location
		# 	location_fee = location.fees if location.present?
		# 	if fee_type.present?
		# 		if location_fee.present?
		# 			fee_object = location_fee.buy_rate.first
		# 			if fee_type == 16
		# 				fee = fee_object.charge_back_fee
		# 				# fee = get_charge_back_location_fee(location_fee)
		# 				return {fee_amount: fee, fee_object: fee_object, fee_location: location}
		# 			elsif fee_type == 17
		# 				fee = fee_object.retrievel_fee
		# 				# fee = get_retrivel_location_fee(location_fee)
		# 				return {fee_amount: fee, fee_object: fee_object, fee_location: location}
		# 			end
		# 		end
		# 	else
		# 		return {fee_amount: 0, fee_object: nil, fee_location: nil}
		# 	end
		# end

		def get_new_location_fee(wallet, fee_type, amount, merchant = nil, charge_fee = nil)
			if charge_fee.present?
				@to = Wallet.find(wallet)
				location = @to.location
				if merchant.blank?
					merchant = location.merchant
				end
				buy_rate = location.fees.buy_rate.first
				fee_class = Payment::FeeCommission.new(merchant, nil, buy_rate, @to, location,fee_type)
				return fee_object = fee_class.apply(amount.to_f)
			else
				return {"fee" => 0, "splits" => {}}
			end
		end

		def get_charge_back_location_fee(location_fee)
			dispute_charge_back_fee = location_fee.dispute_chargeback_fee.last
			if dispute_charge_back_fee.present?
				dispute_charge_back_fee = dispute_charge_back_fee.charge_back_fee
			else
				dispute_charge_back_fee = 0
			end
			return dispute_charge_back_fee
		end

		def get_retrivel_location_fee(location_fee)
			dispute_retrievel_fee = location_fee.dispute_retrivel_fee.last
			if dispute_retrievel_fee.present?
				dispute_retrievel_fee = dispute_retrievel_fee.retrievel_fee
			else
				dispute_retrievel_fee = 0
			end
			return dispute_retrievel_fee
		end

		# this is for charge back process
		def transfer_charge_back_sent(dispute_case,type=nil,check_for_fee=nil, cbk_wallet = nil)
			if cbk_wallet.blank?
				charge_back_wallet = Wallet.charge_back.last
			else
				charge_back_wallet = cbk_wallet
			end
			dispute_case_body = {
					case_number: dispute_case.case_number,
					amount: dispute_case.amount,
					merchant_wallet_id:dispute_case.merchant_wallet_id,
					last4: dispute_case.card_number.present? ? dispute_case.card_number : "NA"
			}
			merchant = dispute_case.merchant_wallet.location.merchant
			from_id = dispute_case.merchant_wallet_id
			to_id = charge_back_wallet.id
			charge_back_fee = get_new_location_fee(from_id, TypesEnumLib::CommissionType::ChargeBack, dispute_case.amount.to_f, merchant,check_for_fee)
			# charge_back_fee = get_location_fee(from_id,16,check_for_fee, dispute_case.amount.to_f)
			total_fee = save_total_fee(charge_back_fee["splits"], charge_back_fee["fee"].to_f)
			charge_back_amount = dispute_case.amount.to_f
			if charge_back_amount > 0
				sender_dba=get_business_name(Wallet.find_by(id:from_id))
				transaction = merchant.transactions.build(to: to_id, from: from_id, status: "Chargeback", sender_name:sender_dba, receiver_name:charge_back_wallet.try(:name), sender_wallet_id:from_id, receiver_wallet_id:to_id, amount: dispute_case.amount, total_amount: dispute_case.amount , net_amount: dispute_case.amount, sender: merchant, receiver: charge_back_wallet.try(:users).try(:first), action: 'transfer', main_type: "cbk_hold", fee: total_fee.to_f)
				cbkSent_transfer1 = SequenceLib.transfer(number_with_precision(charge_back_amount,precision:2),from_id,to_id,"cbk_hold",0,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,dispute_case_body)
				#= creating block transaction
				if cbkSent_transfer1.present?
					transaction.tags = cbkSent_transfer1.actions.first.tags
					transaction.seq_transaction_id = cbkSent_transfer1.actions.first.id
					transaction.timestamp = cbkSent_transfer1.timestamp
					parsed_transactions = parse_block_transactions(cbkSent_transfer1.actions, cbkSent_transfer1.timestamp)
					save_block_trans(parsed_transactions) if parsed_transactions.present?
				end
				transaction.save
				dispute_case.chargeback_transaction_id=transaction.id
				dispute_case.save
			end
			qc_wallet = Wallet.qc_support.first
			destination_id = qc_wallet.present? ? qc_wallet.id : "NA"
			if charge_back_fee["fee"].to_f > 0
				# -----------------New BuyRate Fee------------------
				# fee_class = Payment::FeeCommission.new(merchant, nil, charge_back_fee[:fee_object], @to, charge_back_fee[:fee_location],TypesEnumLib::CommissionType::ChargeBack)
				# fee_object = fee_class.apply(dispute_case.amount.to_f)
				# -----------------End New Fee------------------
				dispute_case_body = {
						case_number: dispute_case.case_number,
						amount: number_with_precision(charge_back_fee["fee"],precision:2),
						merchant_wallet_id:dispute_case.merchant_wallet_id,
						last4: dispute_case.card_number.present? ? dispute_case.card_number : "NA"
				}
				total_fee = charge_back_fee["fee"]
				if charge_back_fee.try(:[], "splits").try(:[], "iso").try(:[], "iso_buyrate")
					wallet = Wallet.where(id: charge_back_fee["splits"]["iso"]["wallet"].to_i).first
						iso = wallet.try(:users).try(:first)
						iso_name = get_business_name(wallet)
						iso_fee_transaction = merchant.transactions.build(
								to: destination_id,
								from: wallet.id,
								sender_wallet_id: wallet.id,
								receiver_wallet_id: destination_id,
								sender_balance: SequenceLib.balance(wallet.id),
								receiver_balance: SequenceLib.balance(destination_id),
								status: "cbk_fee",
								amount: charge_back_fee["splits"]["iso"]["amount"],
								total_amount: charge_back_fee["splits"]["iso"]["amount"].to_f,
								net_amount: charge_back_fee["splits"]["iso"]["amount"].to_f,
								sender_name: iso_name,
								receiver_name: qc_wallet.try(:users).try(:first).try(:name),
								sender: iso,
								receiver: qc_wallet.try(:users).try(:first),
								action: 'transfer',
								main_type: "Buy Rate Fee"
								)
						cbkSent_transfer3 = SequenceLib.transfer(0,wallet.id,destination_id,"buy_rate_fee",charge_back_fee["splits"]["iso"]["amount"],nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,dispute_case_body)
						#= creating block transaction
						if cbkSent_transfer3.present?
							parsed_transactions2 = parse_block_transactions(cbkSent_transfer3.actions, cbkSent_transfer3.timestamp)
							save_block_trans(parsed_transactions2) if parsed_transactions2.present?
							iso_fee_transaction.tags = cbkSent_transfer3.actions.first.tags
							iso_fee_transaction.seq_transaction_id = cbkSent_transfer3.actions.first.id
							iso_fee_transaction.timestamp = cbkSent_transfer3.timestamp
						end
						iso_fee_transaction.save
				end
				sender_dba=get_business_name(Wallet.find_by(id:from_id))
				fee_transaction = merchant.transactions.build(
						to: destination_id,
						from: from_id,
						sender_wallet_id:from_id,
						receiver_wallet_id:destination_id,
						sender_balance: SequenceLib.balance(from_id),
						receiver_balance: SequenceLib.balance(destination_id),
						status: "cbk_fee",
						charge_id: nil,
						amount: total_fee,
						total_amount: total_fee,
						net_amount: total_fee,
						sender_name: sender_dba,
						receiver_name: qc_wallet.try(:users).try(:first).try(:name),
						sender: merchant,
						receiver: qc_wallet.try(:users).try(:first),
						action: 'transfer',
						main_type: "cbk_fee",
						gbox_fee: total_fee,
						iso_fee: save_iso_fee(charge_back_fee["splits"]),
						agent_fee: charge_back_fee["splits"].try(:[],"agent").try(:[],"amount").to_f,
						affiliate_fee: charge_back_fee["splits"].try(:[],"affiliate").try(:[],"amount").to_f,
						)
				cbkSent_transfer2 = SequenceLib.transfer(0,from_id,destination_id,"cbk_fee",charge_back_fee["fee"],nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,charge_back_fee["splits"],nil,nil,nil,nil,nil,nil,nil,dispute_case_body)
				#= creating block transaction
				if cbkSent_transfer2.present?
					parsed_transactions2 = parse_block_transactions(cbkSent_transfer2.actions, cbkSent_transfer2.timestamp)
					save_block_trans(parsed_transactions2) if parsed_transactions2.present?
					fee_transaction.tags = cbkSent_transfer2.actions.first.tags
					fee_transaction.seq_transaction_id = cbkSent_transfer2.actions.first.id
					fee_transaction.timestamp = cbkSent_transfer2.timestamp
				end
				fee_transaction.save
				dispute_case.cbk_fee_transaction_id=fee_transaction.id
				dispute_case.save
			elsif charge_back_fee.try(:[], "splits").try(:[], "iso").try(:[], "iso_buyrate")
				wallet = Wallet.where(id: charge_back_fee["splits"]["iso"]["wallet"].to_i).first
				cbkSent_transfer3 = SequenceLib.transfer(0,wallet.id,destination_id,"buy_rate_fee",charge_back_fee["splits"]["iso"]["amount"],nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,dispute_case_body)
				if cbkSent_transfer3.present?
					parsed_transactions2 = parse_block_transactions(cbkSent_transfer3.actions, cbkSent_transfer3.timestamp)
					save_block_trans(parsed_transactions2) if parsed_transactions2.present?
				end
			end
		end

		def transfer_charge_back_won(dispute_case, cbk_wallet = nil)
			merchant = dispute_case.merchant_wallet.location.merchant
			if cbk_wallet.blank?
				charge_back_wallet = Wallet.charge_back.last
			else
				charge_back_wallet = cbk_wallet
			end
			from_id = charge_back_wallet.id
			to_id = dispute_case.merchant_wallet_id
			dispute_case_body = {
					case_number: dispute_case.case_number,
					amount: dispute_case.amount,
					merchant_wallet_id:dispute_case.merchant_wallet_id,
					last4: dispute_case.card_number.present? ? dispute_case.card_number : "NA"
			}
			charge_back_amount = dispute_case.amount.to_f
			if charge_back_amount > 0
				receiver_dba=get_business_name(Wallet.find_by(id:to_id))
				transaction = merchant.transactions.build(from: from_id, to: to_id,sender_wallet_id:from_id,receiver_wallet_id:to_id,sender_balance: SequenceLib.balance(from_id), receiver_balance: SequenceLib.balance(to_id),receiver_name:receiver_dba,sender_name:charge_back_wallet.try(:name), status: "CBK Won", charge_id: nil, amount: dispute_case.amount, total_amount: dispute_case.amount, net_amount: dispute_case.amount, sender: charge_back_wallet.try(:users).try(:first),receiver: merchant, action: 'withdraw',main_type: "cbk_won")
				cbkWon = SequenceLib.transfer(number_with_precision(charge_back_amount,precision:2),from_id,to_id,"charge_back",0,"cbk_won",TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,"cbk_won",nil,dispute_case_body)
				#= creating block transaction
				if cbkWon.present?
					transaction.tags = cbkWon.actions.first.tags
					transaction.seq_transaction_id = cbkWon.actions.first.id
					transaction.timestamp = cbkWon.timestamp
					parsed_transactions = parse_block_transactions(cbkWon.actions, cbkWon.timestamp)
					save_block_trans(parsed_transactions) if parsed_transactions.present?
				end
			end
			transaction.save
			dispute_case.update(dispute_result_transaction_id: transaction.id)

		end

		def transfer_charge_back_lost(dispute_case,request=nil,merchant_request=nil, cbk_wallet = nil)
			merchant = dispute_case.merchant_wallet.location.merchant
			if cbk_wallet.blank?
				from_id = Wallet.charge_back.last.id
			else
				from_id = cbk_wallet.id
			end
			to_id = dispute_case.merchant_wallet_id
			dispute_case_body = {
					case_number: dispute_case.case_number,
					amount: dispute_case.amount,
					merchant_wallet_id:dispute_case.merchant_wallet_id,
					last4: dispute_case.card_number.present? ? dispute_case.card_number : "NA"
			}
			charge_back_amount = dispute_case.amount.to_f
			cbk_lost = nil
			if charge_back_amount > 0
				transaction = Transaction.create(to: to_id, from: from_id,sender_balance: SequenceLib.balance(from_id), sender_wallet_id: from_id, status: "CBK Lost", charge_id: nil, amount: charge_back_amount, total_amount: charge_back_amount, net_amount: charge_back_amount, sender_id: Wallet.charge_back.try(:last).try(:users).try(:first).try(:id),receiver: nil, action: 'retire')
				if dispute_case.lost?
					cbk_lost = SequenceLib.retire(number_with_precision(charge_back_amount,precision:2),from_id,"charge_back",0,"cbk_lost",TypesEnumLib::GatewayType::Quickcard,nil, merchant.id,nil,nil,nil,nil,nil,request,nil,"cbk_lost",nil,nil,nil,nil,dispute_case_body)
							# SequenceLib.transfer(number_with_precision(charge_back_amount,precision:2),from_id,to_id,"charge_back",0,"cbk_lost",TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,"cbk_lost",nil,dispute_case_body)
					transaction.main_type = "cbk_lost"
				elsif dispute_case.dispute_accepted?
					cbk_lost = SequenceLib.retire(number_with_precision(charge_back_amount,precision:2),from_id,"charge_back",0,"cbk_dispute_accepted",TypesEnumLib::GatewayType::Quickcard,nil, merchant.id,nil,nil,nil,nil,nil,request,nil,"cbk_dispute_accepted",nil,nil,nil,nil,dispute_case_body)
							# SequenceLib.transfer(number_with_precision(charge_back_amount,precision:2),from_id,to_id,"charge_back",0,"cbk_dispute_accepted",TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,"cbk_dispute_accepted",nil,dispute_case_body)
					transaction.main_type = "cbk_dispute_accepted"
				end
				if cbk_lost.present?
					transaction.update(tags: cbk_lost.actions.first.tags, seq_transaction_id: cbk_lost.actions.first.id, timestamp: cbk_lost.timestamp)
					parsed_transactions = parse_block_transactions(cbk_lost.actions, cbk_lost.timestamp)
					save_block_trans(parsed_transactions) if parsed_transactions.present?
					dispute_case.update(dispute_result_transaction_id: transaction.id)
				end
			end
			# lost_retire_transaction = merchant.transactions.build(to: to_id, from: '', status: "CBK Lost", charge_id: nil, amount: charge_back_amount,receiver: Wallet.qc_support.try(:first).try(:users).try(:first), action: 'withdraw',main_type: "CBK Lost Retire")
			# if cbk_lost.present?
			# 	#= creating block transaction
			# 	parsed_transactions = parse_block_transactions(cbk_lost.actions, cbk_lost.timestamp)
			# 	save_block_trans(parsed_transactions) if parsed_transactions.present?
			# 	cbk_retire = SequenceLib.retire(number_with_precision(charge_back_amount,precision:2), to_id ,TypesEnumLib::TransactionType::Cbk_retire_gbox, 0, nil, TypesEnumLib::GatewayType::Quickcard, nil, merchant.id, nil,nil, nil, nil, nil, request, nil, TypesEnumLib::TransactionType::Cbk_retire_gbox,nil,nil,nil,nil,dispute_case_body)
			# 	if cbk_retire.present?
			# 		lost_retire_transaction.tags = cbk_retire.actions.first.tags
			# 		lost_retire_transaction.seq_transaction_id = cbk_retire.actions.first.id
			# 		cbk_retire_transactions = parse_block_transactions(cbk_retire.actions, cbk_retire.timestamp)
			# 		save_block_trans(cbk_retire_transactions) if cbk_retire_transactions.present?
			# 	end
			# end
			# lost_retire_transaction.save
		end

		# this is for retrievel process
		def transfer_retreivel_fee(dispute_case,check_for_fee=nil)
			merchant = dispute_case.merchant_wallet.location.merchant
			from_id = dispute_case.merchant_wallet_id
			to_id = Wallet.charge_back.last.id
			dispute_case_body = {
					case_number: dispute_case.case_number,
					amount: dispute_case.amount,
					merchant_wallet_id:dispute_case.merchant_wallet_id,
					last4: dispute_case.card_number.present? ? dispute_case.card_number : "NA"
			}
			retreivel_fee = get_new_location_fee(from_id, TypesEnumLib::CommissionType::Retrieval, dispute_case.amount.to_f, merchant,check_for_fee)
			if retreivel_fee["fee"].present?
				transaction = merchant.transactions.build(to: to_id, from: from_id,sender_balance: SequenceLib.balance(from_id), receiver_balance: SequenceLib.balance(to_id), status: "Retrievel", charge_id: nil, amount: retreivel_fee["fee"], total_amount: retreivel_fee["fee"], net_amount: retreivel_fee["fee"], sender: merchant,receiver: Wallet.charge_back.try(:last).try(:users).try(:first), action: 'transfer',main_type: "retrievel_fee")
				# -----------------New BuyRate Fee------------------
				# fee_class = Payment::FeeCommission.new(merchant, nil, retreivel_fee[:fee_object], @to, retreivel_fee[:fee_location],TypesEnumLib::CommissionType::Retrieval)
				# fee_object = fee_class.apply(dispute_case.amount.to_f)
				# -----------------End New Fee------------------
				retreivel = SequenceLib.transfer(0,from_id,to_id,"retrievel_fee",number_with_precision(retreivel_fee["fee"],precision:2),nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,retreivel_fee["splits"],nil,nil,nil,nil,nil,nil,nil,dispute_case_body)
				if retreivel.present?
					transaction.tags = retreivel.actions.first.tags
					transaction.seq_transaction_id = retreivel.actions.first.id
					transaction.timestamp = retreivel.timestamp
					parsed_transactions = parse_block_transactions(retreivel.actions, retreivel.timestamp)
					save_block_trans(parsed_transactions) if parsed_transactions.present?
				end
				transaction.save
				dispute_case.chargeback_transaction_id=transaction.id
				dispute_case.save
			end
		end
	end
end