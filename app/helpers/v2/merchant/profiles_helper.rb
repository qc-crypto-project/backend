module V2::Merchant::ProfilesHelper
  def first_letter_name
    if current_user.affiliate_program?
      if current_user.profile.present?
        name =  current_user.profile.company_name.present? ? current_user.profile.company_name : 'n/a'
      else 
         name = current_user.company_name.present? ? current_user.company_name : '--'
      end
    else
      name = current_user.try(:name)
    end
    first = name.blank? ? "" : name.split(" ").tap{|a| a.pop if a.length > 1 }.join(" ")
    last = name.blank? ? "" : (name.split(" ").tap{|a| a.shift }.last || "")
    cap_latter = first[0..0] + last[0..0]
    cap_latter.upcase
    end
  def first_letter_name_merchant(name)
    first = name.blank? ? "" : name.split(" ").tap{|a| a.pop if a.length > 1 }.join(" ")
    last = name.blank? ? "" : (name.split(" ").tap{|a| a.shift }.last || "")
    cap_latter = first[0..0] + last[0..0]
    cap_latter.upcase
  end
end
