module V2::Merchant::MaxmindHelper
  def merch_maxmind_minfraud_status(minfraud_result)
    if minfraud_result.present? &&
        minfraud_result.qc_action.present? &&
        minfraud_result.qc_reason.present?
      qc_action = minfraud_result.qc_action
      qc_reason = minfraud_result.qc_reason

      case qc_reason
      when TypesEnumLib::RiskReason::Default
        if qc_action == TypesEnumLib::RiskType::Accept
          {class: "cr_approved", title: "Accepted by default", text: "A"}
        else
          {class: "cr_rejected", title: "Rejected by custom rule", text: "CR"}
        end
      when TypesEnumLib::RiskReason::ManualReview
        if qc_action == TypesEnumLib::RiskType::PendingReview
          {class: "cr_default", title: "Pending manual review", text: "MR"}
        elsif qc_action == TypesEnumLib::RiskType::Accept
          {class: "cr_approved", title: "Approved manual review", text: "MR"}
        elsif qc_action == TypesEnumLib::RiskType::Reject
          {class: "cr_rejected", title: "Rejected manual review", text: "MR"}
        elsif qc_action == TypesEnumLib::RiskType::ExpiredReview
          {class: "cr_expired", title: "Manual review expired", text: "EX"}
        else
          {class: "cr_approved", title: "Accepted by default", text: "CR"}
        end
      else
        {class: "cr_approved", title: "Accepted by default", text: "CR"}
        '<a href="#" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-toggle="modal" data-target="#deleteEmployeeModal" data-original-title="Approved by manual review" class="badge cr_approved">
              New
            </a>'
      end
    else
      {class: "cr_approved", title: "Accepted by default", text: "CR"}
    end
  end

  def get_user_info(user)
    #user = User.find id
    user_email = user.email
    user_name = user.name
    return user_info = { email: user_email , name: user_name }
  end


end