module V2::Merchant::ChecksHelper
  @@check_book_key=ENV['CHECKBOOK_KEY']
  @@check_book_secret=ENV['CHECKBOOK_SECRET']

 def get_cheque(cheque)
   begin
       url =URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{cheque}")
       http = Net::HTTP.new(url.host,url.port)
       http.use_ssl =true
       http.verify_mode =OpenSSL::SSL::VERIFY_PEER
       request= Net::HTTP::Get.new(url)
       request["Authorization"]=@@check_book_key+":"+@@check_book_secret
       request["Content-Type"]='application/json'
       response =http.request(request)
       case response
         when Net::HTTPSuccess
         cheque = JSON.parse(response.body)
         return cheque
         when Net::HTTPUnauthorized
           return false
         when Net::HTTPNotFound
           return false
         when Net::HTTPServerError
           return false
         else
           return false
       end
   rescue => ex
     p"-----------EXCEPTION HANDLED #{ex.message}"
   end
 end
  def create_with_checkbook(digital_check, wallet)
    check= Check.new(cheque_params)
    check.user_id= @user.id
    check.name=digital_check["name"]
    check.recipient=digital_check["recipient"]
    check.checkId=digital_check["id"]
    check.number=digital_check["number"]
    check.description=digital_check["description"]
    check.image_uri=digital_check["image_uri"]
    check.status=digital_check["status"]
    check.wallet_id=wallet.id
    check.save
  end

  def get_bulk_status(bulk)
    checks_count = bulk.bulk_checks.count
    process_count = bulk.bulk_checks.where(is_processed: true).count
    done_count = bulk.bulk_checks.where(status: ["Success","PENDING"]).count

    if checks_count == done_count
      return "Done"
    elsif checks_count > process_count && checks_count != done_count
      return "Processing"
    elsif checks_count == process_count && checks_count != done_count
      return "Incomplete"
    end
  end

  def mask_account_number(account_number)
    account_number.present? ? "********#{account_number.last(4)}" : ""
  end

  def show_bulk_wallet(w_id)
    wallet = Wallet.find_by_id(w_id)
    wallet_name = wallet.name if wallet.present?
  end

end
