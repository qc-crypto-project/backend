module V2::Merchant::ChargebacksHelper

  def  filter_chargebacks
    if params[:custom].present? || params[:first_date].present?
      # params[:first_date] = DateTime.parse(params[:first_date])
      # params[:second_date] = DateTime.parse(params[:second_date])
      #
      date = parse_date("#{params[:first_date].to_datetime.try(:strftime, "%m/%d/%Y")} - #{params[:second_date].try(:strftime, "%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,00, params[:offset]).utc
      }
    end

    time = nil
    search_params = {}
    time = Date.today.beginning_of_month..Date.today.end_of_day
    if params[:all].present? && params[:search].blank?
      chargebacks = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @wallet_ids).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).order(id: "desc")
    elsif params[:checkbox].present? && params[:search].blank?
      search_params[:q]={status_eq_any:params[:checkbox]}
       @q = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @wallet_ids).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).ransack(search_params[:q].try(:merge, m: 'or'))
      chargebacks = @q.result().order(id: "desc")
    end
    if params[:search].present? #search field
      search_params[:q]={}
      val=params[:search].try(:strip)
      search_params[:q]={amount_eq:val}
      search_params[:q][:case_number_cont]=val
      search_params[:q]["merchant_wallet_location_business_name_cont"]=val
      search_params[:q]["reason_title_cont"]=val
      if params[:checkbox].present?
        @q = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(status: params[:checkbox], merchant_wallet_id: @wallet_ids).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(created_at: time).ransack(search_params[:q].try(:merge, m: 'or'))
      else
        @q = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @wallet_ids).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(created_at: time).ransack(search_params[:q].try(:merge, m: 'or'))
      end
      chargebacks = @q.result().order(id: "desc")
    end
    if params[:search].blank? && params[:checkbox].blank? && params[:all].blank?
      # chargebacks = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @wallet_ids).where(time).order(id: "desc")
      chargebacks = []
    end
    chargebacks= chargebacks.created_between(date[:first_date],date[:second_date]) if chargebacks.present? && date.present?
    return chargebacks
  end
end