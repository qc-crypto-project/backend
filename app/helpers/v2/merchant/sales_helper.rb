module V2::Merchant::SalesHelper
  include CardHandler
  def maintain_batch(user_id, wallet_id, sale = nil, type = nil, release_days = nil, risk = nil , fee = nil, iso_id = nil, merchant = nil, iso_fee=nil, agent_fee=nil, affiliate_fee=nil,bonus=nil)
    batch = retrieve_batch(user_id, wallet_id, type, risk, iso_id, merchant)
    if batch.present?
      total_fee = 0
      total_fee = batch.total_fee.to_f + fee.to_f if batch.total_fee.present?
      total_sales = batch.total_sales.to_f + sale.to_f
      iso_profit = batch.iso_profit.to_f + iso_fee.to_f
      agent_profit = batch.agent_profit.to_f + agent_fee.to_f
      affiliate_profit = batch.affiliate_profit.to_f + affiliate_fee.to_f
      total_transactions = batch.total_transactions.to_i + 1
      total_bonus=batch.total_bonus.to_f + bonus.to_f
      batch.update(total_transactions: total_transactions, total_sales: total_sales, total_fee: total_fee, days: release_days,iso_profit: iso_profit,agent_profit: agent_profit,affiliate_profit: affiliate_profit,total_bonus: total_bonus)
    else
      create_batch(user_id, wallet_id, type, sale, risk, release_days, fee, nil, iso_id, merchant,iso_fee,agent_fee,affiliate_fee,bonus)
    end
  end

  def get_batch(user_id, wallet_id, type, sale = nil, release_days = nil, current_tier = nil, risk = nil, iso_id = nil, merchant = nil)
    batch = retrieve_batch( user_id, wallet_id, type, risk, iso_id, merchant)
    if batch.present?
      batch
    else
      create_batch(user_id, wallet_id, type, sale, risk, release_days, nil, current_tier, iso_id, merchant)
    end
  end

  def close_batches(user_id, wallet_id, type, risk = nil, iso_id = nil, merchant = nil)
    if type == TypesEnumLib::Batch::Primary
      batch = retrieve_batch( user_id, wallet_id, type)
    elsif type == TypesEnumLib::Batch::Reserve
      batch = retrieve_batch( user_id, wallet_id, type)
    elsif type == TypesEnumLib::Batch::BuyRateCommission
      batch = retrieve_batch( user_id, wallet_id, type, risk, iso_id, merchant)
    end
    if batch.present?
      # g = ((DateTime.now.utc-batch.created_at) * 24 * 60).to_i
      # if (g/1.minute).round >= 1
      if batch.created_at.strftime("%B") != DateTime.now.utc.strftime("%B")
        batch.update(is_closed: true, close_date: Date.today, close_time: Time.now.utc.strftime("%H:%M"))
      end
    end
  end

  def retrieve_batch(user_id, wallet_id, type, risk = nil, iso_id = nil, merchant = nil)
    case type
      when TypesEnumLib::Batch::Primary
        Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).primary.first
      when TypesEnumLib::Batch::Reserve
        Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).reserve.first
    when TypesEnumLib::Batch::BuyRateCommission
        if risk == TypesEnumLib::Batch::High
          if merchant == true
            Batch.where(user_id: user_id, wallet_id: wallet_id, iso_id: iso_id, is_closed: false).buy_rate_commission.high.first
          else
            Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).buy_rate_commission.high.first
          end
        elsif risk == TypesEnumLib::Batch::Low
          if merchant == true
            Batch.where(user_id: user_id, wallet_id: wallet_id, iso_id: iso_id, is_closed: false).buy_rate_commission.low.first
          else
            Batch.where(user_id: user_id, wallet_id: wallet_id, is_closed: false).buy_rate_commission.low.first
          end
        end
    end
  end

  def create_batch(user_id, wallet_id, type, sale, risk = nil, release_days = nil, fee = nil, current_tier = nil, iso_id = nil, merchant = nil,iso_profit=nil,agent_profit=nil,affiliate_profit=nil,bonus=nil)
    case type
      when  TypesEnumLib::Batch::Primary
        batch = Batch.new(total_transactions: 1, total_sales: sale, user_id: user_id, wallet_id: wallet_id)
        batch.primary!
        batch.save
        batch
      when TypesEnumLib::Batch::Reserve
        batch = Batch.new(total_transactions: 1, total_sales: sale, user_id: user_id, wallet_id: wallet_id, days: release_days)
        batch.reserve!
        batch.save
        batch
    when TypesEnumLib::Batch::BuyRateCommission
        if merchant == true
          batch = Batch.new(total_transactions: 1, total_sales: sale, total_fee: fee.to_f, iso_id: iso_id, user_id: user_id, wallet_id: wallet_id, current_tier: current_tier || 1,iso_profit: iso_profit.to_f,agent_profit: agent_profit.to_f,affiliate_profit: affiliate_profit.to_f)
        else
          batch = Batch.new(total_transactions: 1, total_sales: sale, total_fee: fee.to_f, user_id: user_id, wallet_id: wallet_id, current_tier: current_tier || 1, total_bonus: bonus.to_f)
        end
        batch.buy_rate_commission!
        if risk == TypesEnumLib::Batch::High
          batch.high!
        elsif risk == TypesEnumLib::Batch::Low
          batch.low!
        end
        batch.save
        batch
    end
  end


  def getting_or_creating_card
    card_name = params[:card_holder_name]
    user_id = @card_customer.present? ? @card_customer.id : @user.id
    current_user = @card_customer || @user
    merchant_id = @card_customer.present? ? @user.id : nil
    #create qc card object with card info , qc token and fingerprint
    bin_result=check_bin_list(current_user,merchant_id,user_id)
    card=bin_result[:card]
    card_bin_list=bin_result[:card_bin_list]
    #check all required paramters exist
    validate_parameters(
        :card_cvv => params[:card_cvv]
    )
    card_block = check_card_blocked(params[:card_number],"#{params[:card_exp_date].first(2)}/#{params[:card_exp_date].last(2)}", @card_info)

    #raise error if card blocked
    if card_block
      msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: #{I18n.t('api.errors.blocked_card')}"
      SlackService.notify(msg, "#qc-risk-alert")
      raise StandardError.new(I18n.t('api.errors.blocked_card'))
    end
    #raise error if card not whitelisted
    if @location.vip_card && !Card.check_vip_card?(params[:card_number],"#{params[:card_exp_date].first(2)}/#{params[:card_exp_date].last(2)}")
      msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: #{I18n.t("api.errors.non_vip_card")}"
      SlackService.notify(msg, "#qc-risk-alert")
      raise StandardError.new(I18n.t('api.errors.non_vip_card'))
    end
    #return false if card decline more than or equal to CARD_DECLINE_ATTEMPTS or 6
    attempts = ENV["CARD_DECLINE_ATTEMPTS"]
    if attempts.to_i <= 0
      attempts = 6
    end
    if card.decline_attempts.to_i >= attempts.to_i
      card.update(decline_attempts: card.decline_attempts.to_i + 1, last_decline: DateTime.now.utc)
      @card_number = params[:card_number]
      return {status: false}
    end
    card.save
    return {status: true,card: card,card_info: @card_info, card_bin_list: card_bin_list,card_bank: card.try(:bank)}
  end


  def check_fee_amount(location, receiver, user, wallet, buyrate, amount, balance, fee_type = nil, type = nil, load_fee=nil)
    feeCommission = Payment::FeeCommission.new(user, receiver, buyrate, wallet, location, fee_type,nil,nil,load_fee)
    #amount calculate for each user merchant gbox iso agent and affiliate
    feeCalc = feeCommission.apply(amount.to_f, type, nil)
    if feeCalc.present?
      totalFee = number_with_precision(feeCalc["fee"].to_f, precision: 2).to_f
      minimumAmountRequired = amount.to_f + balance.to_f
      #status true if fee is less than total balance
      if minimumAmountRequired >= totalFee.to_f
        return {status: true, fee: totalFee, splits: feeCalc["splits"]}
      else
        return {status: false, fee: totalFee}
      end
    end
  end

  def authenticate_transaction
    catch_duplicate_time = ENV["DUPLICATE_TRANSACTION_TIME"] || 300
    params_card_number = "#{(params[:card_number].to_s.first(4))}********#{(params[:card_number].to_s.last(4))}"
    activities = @user.activity_logs.where("params->'amount' ? :amount AND created_at >= :time", time: Time.now - catch_duplicate_time.to_i.seconds, amount: params["amount"].to_s)
    duplicate = activities.detect {|activity| valid_number_amount?(activity) && activity.params["card_number"].last(4) == params_card_number.last(4) && (activity.api_success? || activity.vt_success?)}
    if duplicate.present?
      params[:duplicate_tx] = I18n.t('api.errors.duplicate_transaction')
      raise DuplicationError.new I18n.t('api.errors.duplicate_transaction')
    end
  end
  
  def valid_number_amount?(activity)
    if activity.params["card_number"].present? && activity.params["amount"].present?
      true
    else
      false
    end
  end
end
