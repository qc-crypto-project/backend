module V2::Merchant::AccountsHelper
  include ApplicationHelper

  def reserve_fund(fee , wallet_id, next_day = nil)
    total_amount = 0
    # fee = location.fees.buy_rate.first
    reserve_amount = fee.try(:reserve_fee)
    reserve_days = fee.try(:days)
    if reserve_amount.to_f > 0 && reserve_days > 0
      release = true
    elsif reserve_amount.to_f == 0 && reserve_days == 0
      release = true
    end
    if release == true
      if next_day.present?
        if reserve_days == 0
          previous_date = DateTime.now + 1.days
        else
          previous_date = (DateTime.now + 1.days) - reserve_days.days
        end
      else
        if reserve_days == 0
          previous_date = DateTime.now - 1.days
        else
          previous_date = DateTime.now - reserve_days.days
        end
      end
      first_date = Time.new(previous_date.year, previous_date.month, previous_date.day,00,00,00)
      second_date = Time.new(previous_date.year, previous_date.month, previous_date.day,23,59,59)
      received_amount = BlockTransaction.where(receiver_wallet_id: wallet_id, main_type: "Reserve Money Deposit").where("timestamp BETWEEN :first AND :second",first: first_date,second: second_date).pluck(:amount_in_cents).sum
      total_amount = SequenceLib.dollars(received_amount)
    end
    total_amount.to_f
  end

  def percentage_calculate(original_number, new_number)
    if original_number.to_f > 0
      new_value = new_number.to_f - original_number.to_f
      number_with_precision((new_value/original_number) * 100, precision: 2, delimiter: ',')
    else
      number_with_precision(0, precision: 2)
    end
  end

  def batch_tx_search(params, batch_id, wallets = nil, trans_type=nil,first_date=nil,second_date=nil,user=nil,offset=nil,from_worker=nil,host_name=nil,filter=nil, page_params = nil, email=nil)
    amount = nil
    per_page = filter || 10
    conditions = []
    parameters = []
    @wallet = @wallet || Wallet.find_by(id:wallets) if wallets.present?
    if params[:amount].present?
      new_var=params[:amount]
      new_var=number_with_precision(params[:amount], precision: 2)
      var=new_var.to_s.split('.').last
      storing_param=new_var
      if var == "00"
        storing_param=new_var
        storing_param=new_var.to_s.split('.').first
        storing_param=storing_param.to_i.abs
      else
        storing_param=number_with_precision(params[:amount], precision: 2).to_f.abs
      end
      if !@wallet.try(:primary?)
        storing_param=(storing_param*100).to_i
        if params[:amount].include?('-')
          conditions << "block_transactions.action=? and block_transactions.amount_in_cents::VARCHAR LIKE ?"
          parameters << BlockTransaction.actions[:retire]
          parameters << "#{storing_param}".to_s
        else
          conditions << "block_transactions.action !=? and block_transactions.amount_in_cents::VARCHAR LIKE ?"
          parameters << BlockTransaction.actions[:retire]
          parameters << "#{storing_param}".to_s
        end
      else
        if params[:amount].include?('-')
          conditions << "transactions.action=? and transactions.total_amount::VARCHAR LIKE ?"
          parameters << "retire"
          parameters << "#{storing_param}%".to_s
        else
          conditions << "transactions.action !=? and transactions.total_amount::VARCHAR LIKE ?"
          parameters << "retire"
          parameters << "#{storing_param}%".to_s
        end
      end
    end
    if params[:offset].present? && !params[:offset].include?(':')
      params[:offset].insert(3,':')
    end
    params[:date] = params[:hidden_date]

    date = parse_daterange(params[:date]) if params[:date].present?
    date = parse_daterange(params[:export_date]) if date.blank? && params[:export_date].present?
    time = parse_time(params)

    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
      }
    elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
      date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    end

    if date.present?
      if date[:first_date].present?
        if !@wallet.try(:primary?)
          conditions << "block_transactions.created_at >= ?"
        else
          conditions << "transactions.created_at >= ?"
        end
        parameters << date[:first_date]
      end
      if date[:second_date].present?
        if !@wallet.try(:primary?)
          conditions << "block_transactions.created_at <= ?"
        else
          conditions << "transactions.created_at <= ?"
        end
        parameters << date[:second_date]
      end
    end

    if (params[:time1].present? || params[:time2].present?) && params[:date].present?
      time_first = time_conversion(time.first + params[:offset]) if time.first.present?
      time_second = time_conversion(time.second + params[:offset]) if time.second.present?
      if time_first > time_second
        conditions << "(transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ? OR transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ?)"
        parameters << time_first
        parameters << "23:59:59"
        parameters << "00:00:00"
        parameters << time_second
      else
        if time_first.present?
          conditions << "transactions.created_at::TIME >= ?"
          parameters << time_first
        end
        if time_second.present?
          conditions << "transactions.created_at::TIME <= ?"
          parameters << time_second
        end
      end
    end

    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if type.present?
      if type.include?("invoice-credit") && type.include?("invoice-qc")
        if type.count == 2
          conditions << "((main_type IN (?) AND sub_type IN (?)) OR (main_type IN (?) AND sub_type IN (?) ) )"
          parameters << "QCP Secure"
          parameters << "invoice"
          parameters << "B2B Transfer"
          parameters << "invoice"
        else
          if type.include?("Void Check")
            type.push("Void_Check")
          elsif type.include?("Void_Check")
            type.push("Void Check")
          end
          conditions << "((main_type IN (?) AND sub_type IN (?) ) OR (main_type IN (?) AND sub_type IN (?) ) OR (main_type IN (?) OR sub_type IN (?) ))"
          parameters << ["QCP Secure"]
          parameters << ["invoice"]
          parameters << ["B2B Transfer"]
          parameters << ["invoice"]
          parameters << type
          parameters << type
        end
      elsif type.include?("invoice-credit")
        if type.count == 1
          conditions << "(main_type IN (?) AND sub_type IN (?) )"
          parameters << "QCP Secure"
          parameters << "invoice"
        else
          if type.include?("Void Check")
            type.push("Void_Check")
          elsif type.include?("Void_Check")
            type.push("Void Check")
          end
          conditions << "((main_type IN (?) AND sub_type IN (?) )  OR (main_type IN (?) OR sub_type IN (?) ))"
          parameters << ["QCP Secure"]
          parameters << ["invoice"]
          parameters << type
          parameters << type
        end
      elsif  type.include?("invoice-qc")
        if type.count == 1
          conditions << "(main_type IN (?) AND sub_type IN (?) )"
          parameters << "B2B Transfer"
          parameters << "invoice"
        else
          if type.include?("Void Check")
            type.push("Void_Check")
          elsif type.include?("Void_Check")
            type.push("Void Check")
          end
          conditions << "((main_type IN (?) AND sub_type IN (?)  ) OR (main_type IN (?) OR sub_type IN (?) ))"
          parameters << ["B2B Transfer"]
          parameters << ["invoice"]
          parameters << type
          parameters << type
        end
      else
      if type == ["Void ACH"]
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << ["Void_Ach", "Void ACH"]
        parameters << ["Void_Ach", "Void ACH"]
      elsif type== ["Void Check"]
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << ["Void_Check", "Void Check"]
        parameters << type
      else
        if type.include?("Void ACH")
          type.push("Void_Ach")
        end
        if type.include?("Void Check")
          type.push("Void_Check")
        end
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << type
        parameters << type
      end
      end
    end
    if params[:transaction_id].present?
      if !@wallet.try(:primary?)
        conditions << "block_transactions.sequence_id  LIKE ?"
      else
        conditions << "transactions.seq_transaction_id  LIKE ? OR transactions.quickcard_id::VARCHAR LIKE ?"
      end
      parameters << "%#{params[:transaction_id]}%"
      parameters << "#{params[:transaction_id].split.join}%"
    end
    if params[:phone_number].present?
      conditions << "users.phone_number ILIKE ?"
      parameters << "%#{params[:phone_number]}%"
    end

    if params[:email].present?
      conditions << "users.email LIKE ?"
      parameters << params[:email]
    end

    if params[:name].present?
      conditions << "users.name ILIKE ?"
      parameters << "%#{escape_like(params[:name].try(:strip))}%"
    end

    last4 = params[:last4].present? ? params[:last4].split.join : nil
    if last4.present?
      if !@wallet.try(:primary?)
        conditions << "block_transactions.last4 LIKE ?"
      else
        conditions << "transactions.last4 LIKE ?"
      end
      parameters << "#{last4}%"
    end
    first6 = params[:first6].present? ? params[:first6].split.join : nil
    if first6.present?
      conditions << "transactions.first6 LIKE ?"
      parameters << "#{first6}%"
    end
    unless conditions.empty?
      conditions = [conditions.join(" AND "), *parameters]
    end
    result = {no_transaction: true}
    batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
    if from_worker
      if !@wallet.try(:primary?)
        BlockTransaction.joins(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) AND block_transactions.main_type IN(?) ",wallets, wallets,["Reserve Money Return", "Reserve Money Deposit","Payed Tip","Account Transfer","Sale Tip"]).where(conditions).find_in_batches(batch_size: batch_size).with_index do |transactions, index|
          count = index + 1
          result.delete(:no_transaction)
          response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,'not_primary')
          result.merge!({"#{index}"=>response})
        end
      else
        if trans_type == "decline"
          Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where(status: "pending").where.not(main_type: "Sale Issue").where(conditions).or(Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("tags->'location'->>'id' = ? AND transactions.status LIKE ?","#{location_id}", 'pending').where(conditions)).find_in_batches(batch_size: batch_size).with_index do |transactions, index|
            count = index + 1
            result.delete(:no_transaction)
            response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,nil)
            result.merge!({"#{index}"=>response})
          end
        elsif trans_type == "refund"
          Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where(main_type: "refund", status: "approved").where(conditions).find_in_batches(batch_size: batch_size).with_index do |transactions,index|
            count = index + 1
            result.delete(:no_transaction)
            response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,nil)
            result.merge!({"#{index}"=>response})
          end
        else
          Transaction.includes(:sender,:receiver).references(:sender,:receiver).where.not(main_type: "refund_fee").where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where.not(main_type: "Sale Issue", status: "pending").where(conditions).find_in_batches(batch_size: batch_size).with_index do |transactions,index|
            count = index + 1
            result.delete(:no_transaction)
            response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,nil)
            result.merge!({"#{index}"=>response})
          end
        end
      end
      return result
    else
      t = @batch.batch_transactions.includes(:sender,:receiver).references(:sender,:receiver).where.not(main_type: "refund_fee").where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where.not(main_type: "Sale Issue", status: "pending").where(conditions).order(created_at: :desc).per_page_kaminari(page_params).per(per_page)
    end
    return t
  end


  def get_card_image(card)
    if card.brand.present?
      if card.brand == "visa"
        {brand: "visa",image:"v2/merchant/icons/visa_PNG30.jpg"}
      elsif card.brand == "mastercard"
        {brand: "mastercard",image:"v2/merchant/icons/p2cmaster.svg"}
      elsif card.brand == "amex"
        {brand: "amex",image:"v2/merchant/icons/american_express.png"}
      elsif card.brand == "discover"
        {brand: "discover",image:"v2/merchant/icons/discover.svg"}
      end
    else
      card_info = get_card_info(card.first6)
      if card_info.present?
        if card_info["scheme"] == "visa"
          {brand: "visa",image:"v2/merchant/icons/visa_PNG30.jpg"}
        elsif card_info["scheme"] == "mastercard"
          {brand: "mastercard",image:"v2/merchant/icons/p2cmaster.svg"}
        elsif card_info["scheme"] == "amex"
          {brand: "amex",image:"v2/merchant/icons/american_express.png"}
        elsif card_info["scheme"] == "discover"
          {brand: "discover",image:"v2/merchant/icons/discover.svg"}
        end
      end
    end

  end

  def get_hold_in_rear_days(hold_rear, days)
    if hold_rear
      days
    else
      1
    end
  end

  def manage_batches(wallet_id, user_id, date,type, starting_balance = nil)
    TransactionBatch.create(
        total_amount: 0,
        total_fee: 0,
        total_net: 0,
        total_trxs: 0,
        expenses: 0,
        starting_balance: starting_balance.to_f,
        batch_date: date,
        merchant_wallet_id: wallet_id,
        merchant_id: user_id,
        batch_type: type,
        invoice_amount: 0,
        invoice_count: 0
    )
  end

  def get_net_earnings(amount, balance)
    amount.to_f != 0 ? amount.to_f : balance.to_f
  end

  def show_view_total_net(tags, amount, tip, reserve_money, fee)
    #show total net on detail modal admin
    if tags.present?
      if tags.try(:[],"iso").present? && tags.try(:[],"iso").try(:[], "iso_buyrate").present?
        return amount.to_f - tip.to_f - reserve_money.to_f - tags.try(:[], "gbox").try(:[], "amount").to_f
      else
        return amount.to_f - tip.to_f - reserve_money.to_f - fee.to_f
      end
    else
      amount.to_f - tip.to_f - reserve_money.to_f - fee.to_f
    end
  end

  def show_view_fee(tags, fee)
    #show fee on detail modal admin
    if tags.present?
      if tags.try(:[],"iso").present? && tags.try(:[],"iso").try(:[], "iso_buyrate").present?
        if tags["gbox"]["amount"].to_f == 0
          return 0
        else
          return tags["gbox"]["amount"].to_f
        end
      else
        return fee
      end
    else
      fee.to_f
    end
  end
  def calculate_remaing_refund(total_amount, logs)
    logs = JSON.parse(logs)
    total_refunded_amount = logs.class == Hash ? logs['amount'] : logs.pluck("amount").sum
    return remaining_refundable_amount = total_amount - total_refunded_amount
  end

end