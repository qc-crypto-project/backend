module V2::Merchant::InvoicesHelper
  def calculation_for_fees(params, txn_type=nil)
    sub_total = 0
    discount_minus = 0
    tax_plus = 0
    fee_plus = 0
    shipping_handling_plus = 0

    params[:product].each do |k,v|
      quantity = v[:qty].to_i
      unit_price = v[:unit_price].to_f
      amount = quantity * unit_price
      params[:product]["#{k}"]["amount"] = amount
      sub_total += amount.to_f
    end

    discount_minus = params[:discount].to_f
    tax_plus = params[:tax].to_f
    fee_plus = params[:fee].to_f
    late_fee_plus = params[:late_fee].to_f
    shipping_handling_plus = params[:shipping_handling_fee].to_f
    if params[:discount_method] == "percent"
      if discount_minus > 100
        return {success: false, message: "Discount can't be greater than 100%"}
      else
        discount_minus = txn_type == TypesEnumLib::TransactionViewTypes::VirtualTerminal ? discount_minus : (sub_total * discount_minus/100)
      end
    end
    if params[:tax_method] == "percent"
      if tax_plus > 100
        return {success: false, message: "Tax can't be greater than 100%"}
      else
        if params[:apply_tax].present?
          new_sub_total = shipping_handling_plus + sub_total
          tax_plus = (new_sub_total * tax_plus/100)
        else
          tax_plus = txn_type == TypesEnumLib::TransactionViewTypes::VirtualTerminal ? tax_plus : (sub_total * tax_plus/100)
        end
      end
    end
    if params[:fee_method] == "percent"
      if fee_plus > 100
        return {success: false, message: "Fee can't be greater than 100%"}
      else
        fee_plus = (sub_total * fee_plus/100)
      end
    end
    if params[:shipping_handling_method] == "percent"
      if shipping_handling_plus > 100
        return {success: false, message: "Fee can't be greater than 100%"}
      else
        shipping_handling_plus = (sub_total * shipping_handling_plus/100)
      end
    end
    late_fee_plus = (sub_total * late_fee_plus/100) if params[:late_fee_method] == "percent"
    grand_total = (sub_total + tax_plus + fee_plus + late_fee_plus + shipping_handling_plus) - discount_minus
    params[:sub_total] = sub_total
    params[:grand_total] = grand_total
  end

  def get_percentage_value(new_value, old_value)
    if old_value == 0 && new_value == 0
      return 0
    elsif old_value == 0
      return new_value
    elsif new_value == 0
      return "-#{old_value}"
    else
      value=new_value-old_value
      result=(value/old_value)*100
      return result
    end
  end

  def amount_with_symbol(amount, symbol=nil)
    if symbol.present?
      if symbol == "percent"
        return number_to_percentage(amount, format: "%%n", precision: 2)
      else
        return number_with_precision(number_to_currency(amount), precision: 2)
      end
    else
      return number_with_precision(number_to_currency(amount), precision: 2)
    end

  end

  def setting_for_request(request,params,user)
    if params[:commit] == "Save Draft"
      status = "saved"
    elsif @schedule_date.present? && @schedule_date > @current_time
      status = "pending"
    else
      status = "in_progress"
    end
    request.request_type = "invoice"
    request.amount = params[:sub_total]
    request.total_amount = params[:grand_total]
    request.status = status
    request.sender_id = current_user.id
    request.reciever_id = user.id
    request.wallet_id = params[:wallet_id]
    request.sender_name = current_user.name
    request.reciever_name = user.name
    request.description = params[:memo]
    request.from_wallet_id = user.wallets.primary.try(:first).try(:id)
    request.invoice_number = params[:invoice_id]
    request.payment_option = payment_option(params[:payment_option])
    request.discount = params[:discount]
    request.discount_method = params[:discount_method]
    request.tax = params[:tax]
    request.tax_method = params[:tax_method]
    request.fee = params[:fee]
    request.fee_method = params[:fee_method]
    request.due_days = params[:due_days]
    request.due_date = params[:due_date]
    request.late_fee = params[:late_fee]
    request.late_fee_method = params[:late_fee_method]
    request.shipping_handling_fee = params[:shipping_handling_fee]
    request.shipping_handling_method = params[:shipping_handling_method]
    request.apply_tax = params[:apply_tax] == "on" ? true : false
    request.schedule_date = params[:schedule_date]
    unless params[:commit].blank?
      request.save
      reason = request.status == "saved" ? "Invoice Drafted" : "Invoice Created"
      message = request.status == "saved" ? "Drafted Invoice" : "Created Invoice"
      if params[:type].present?
        reason = params[:type] == "reopen" ? "Invoice Reopened" : "Invoice Resent"
        message = params[:type] == "reopen" ? "Reopened Invoice" : "Resent Invoice"
      end
      ActivityLog.log(nil,reason,current_user,params,response,"#{message} #{request.invoice_number} for $#{number_with_precision(request.total_amount, precision: 2)}","invoices")
    end
    return request
  end

  def check_schedule(request)
    if request.present?
      if request.schedule_date.present? && request.schedule_date >= DateTime.now.utc
        Sidekiq::Cron::Job.destroy "Invoice#{request.id}"
        request.schedule_date=nil
      end
    end
  end

  def setting_products(params,request=nil,transaction=nil)
    products = []
    new_products_id = []
    params[:product].values.each do |p|
      if p["product_id"].present?
        product = Product.find_by(id: p["product_id"])
        new_products_id.push(p["product_id"].to_i)
      else
        product = Product.new
      end
      product.name = p["description"]
      product.description = p["description"]
      product.quantity = p["qty"]
      product.amount = p["unit_price"]
      product.total_amount = p["amount"]
      product.request_id = request.id if request.present?
      product.transaction_id = transaction.id if transaction.present?
      products.push(product)
    end
    if request.present?
      request_products = request.products.pluck(:id)
      delete_products = request_products - new_products_id
      Product.where(id: delete_products).delete_all if delete_products.present? && params[:commit].present?
      Product.import products, on_duplicate_key_update: {conflict_target: [:id], columns: [:name, :description, :quantity, :amount, :total_amount, :request_id]} unless params[:commit].blank?
    elsif transaction.present?
      Product.import products, on_duplicate_key_update: {conflict_target: [:id], columns: [:name, :description, :quantity, :amount, :total_amount, :request_id]}
    end
    return products
  end

  def payment_option(option)
    option["quickcard"] = option["quickcard"].present? && option["quickcard"] == "on" ? true : false
    option["ach"] = option["ach"].present? && option["ach"] == "on" ? true : false
    option["card"] = option["card"].present? && option["card"] == "on" ? true : false
    return option
  end

  def load_wallets
    wallets=Array.new
    location_status = false
    if @user.merchant_id.nil? || @user.iso? || @user.agent?
      wallets = @user.wallets.primary.order('id ASC')
    elsif !@user.merchant_id.nil? && @user.MERCHANT?
      wallets = @user.wallets.eager_load(:location).primary.reject{|v| v.location_id.nil?}
    else
      main_user = current_user.parent_merchant
      wallets = main_user.wallets.eager_load(:location).primary.order('id ASC')
    end
    list=[]
    wallets.each do|w|
      location = w.location
      if location.present?
        location_status = location.is_block
        if location.virtual_terminal == false || location.virtual_terminal == nil
        list <<
            {
                wallet_id: w.id,
                wallet_name: w.name,
                balance: w.balance.to_f - HoldInRear.calculate_pending(w.id),
                location_status: location_status,
                name: location.business_name,
                email: location.email,
                phone_number: location.phone_number,
                cs_number: location.cs_number
            }
        @new = 0
        end
      else
        list <<
            {
                wallet_id: w.id,
                wallet_name: w.name,
                balance: show_balance(w.id),
                location_status: false,
                name: w.name,
                email: @user.email,
                phone_number: @user.phone_number,
                cs_number: location.try(:cs_number)


            }
      end
      new = 0
    end
    return {list: list,location_status: location_status}
  end

  def get_total_spending(customer_id, wallet_id)
    return Transaction.where(sender_id: customer_id, receiver_wallet_id: wallet_id).sum(&:total_amount)
  end

  def set_customer_detail_date
    # if params
  end

  def settings_search_params
    ["users.name ILIKE :value OR
      users.address ILIKE :value OR
      users.state ILIKE :value OR
      users.city ILIKE :value OR
      users.zip_code ILIKE :value OR
      users.email ILIKE :value OR
      users.phone_number ILIKE :value OR
      locations.business_name ILIKE :value",value: "%#{params[:search].strip}%"]
  end

  def set_user
    if params[:existing_user] == "true" && params[:verified_user_id].present?
      user = User.user.find_by(id: params[:verified_user_id])
      user.update(update_params) if params[:commit].present?
      user.wallets.primary.update_all(name: params[:name]) if params[:commit].present?
      wallet = Wallet.eager_load(:location).find_by(id: params[:wallet_id])
      CustomerMerchant.where(customer_id: user.id, merchant_id: current_user.merchant_id.present? ? current_user.merchant_id : current_user.id, wallet_id: params[:wallet_id], location_id: wallet.try(:location).try(:id)).first_or_create
    else
      user = get_or_create_user(params)
    end
    return user
  end

  def get_or_create_user(params)
    user = User.user.where(phone_number: params[:phone_number]).last
    if user.present? && !params[:commit].blank?
      user.update(update_params)
      user.wallets.primary.update_all(name: params[:name]) if params[:commit].present?
    else
      params[:source] = current_user.name
      params[:role] = "user"
      user = User.new(new_params)
      user.password = random_password
      user.regenerate_token
      user.save(:validate => false) unless params[:commit].blank?
    end
    unless params[:commit].blank?
      wallet = Wallet.eager_load(:location).find_by(id: params[:wallet_id])
      ActivityLog.log(nil,"Customer Created", current_user, params, response,"Created customer(#{user.name}) for #{wallet.try(:location).present? ? wallet.try(:location).try(:business_name) : wallet.name}","customers")
      CustomerMerchant.where(customer_id: user.id, merchant_id: current_user.merchant_id.present? ? current_user.merchant_id : current_user.id, wallet_id: params[:wallet_id], location_id: wallet.try(:location).try(:id)).first_or_create
    end
    return user
  end

  def address(address, user, type, request_id=nil)
    new_address = Address.where(id: address[:id]).first_or_initialize
    new_address.name = user.name
    new_address.user_id = user.id
    new_address.email = user.email
    new_address.phone_number = user.phone_number
    new_address.street = address[:street]
    new_address.suit = address[:suit]
    new_address.city = address[:city]
    new_address.state = address[:state]
    new_address.country = address[:country]
    new_address.zip = address[:zip]
    new_address.address_type = type
    new_address.request_id = request_id
    new_address.save unless params[:commit].blank?
    return new_address
  end

  def merchants_customer
    if current_user.merchant?
      merchant_id = current_user.id
      @users = User.joins(:merchant_customers).where('customer_merchants.merchant_id = ?',merchant_id).uniq
    else
    end
  end

  def   filter_chargebacks
    time = nil
    search_params = {}
    if params[:all].present? && params[:search].blank?
      chargebacks = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @client.wallet_id, user_wallet_id: @customer_wallet_id).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(time).order(id: "desc").per_page_kaminari(params[:page]).per(@per_page)
    elsif params[:checkbox].present? && params[:search].blank?
      search_params[:q]={status_eq_any:params[:checkbox]}
      @q = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @client.wallet_id, user_wallet_id: @customer_wallet_id).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(time).ransack(search_params[:q].try(:merge, m: 'or'))
      chargebacks = @q.result().order(id: "desc")
    end
    if params[:search].present? #search field
      search_params[:q]={}
      val=params[:search].try(:strip)
      search_params[:q]={amount_eq:val}
      search_params[:q][:case_number_cont]=val
      search_params[:q]["merchant_wallet_location_business_name_cont"]=val
      search_params[:q]["reason_title_cont"]=val
      if params[:checkbox].present?
        @q = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(status: params[:checkbox], merchant_wallet_id: @client.wallet_id, user_wallet_id: @customer_wallet_id).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(time).ransack(search_params[:q].try(:merge, m: 'or'))
      else
        @q = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @client.wallet_id, user_wallet_id: @customer_wallet_id).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(time).ransack(search_params[:q].try(:merge, m: 'or'))
      end
      chargebacks = @q.result().order(id: "desc")
    end
    if params[:search].blank? && params[:checkbox].blank? && params[:all].blank?
      chargebacks = DisputeCase.eager_load(merchant_wallet: :location).eager_load(:reason).where(merchant_wallet_id: @client.wallet_id, user_wallet_id: @customer_wallet_id).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime).where(time).order(id: "desc")
    end
    return chargebacks
  end

  def set_params
    params[:address] = params[:billing][:street]
    params[:street] = params[:billing][:suit]
    params[:city] = params[:billing][:city]
    params[:state] = params[:billing][:state]
    params[:country] = params[:billing][:country]
    params[:zip_code] = params[:billing][:zip]
  end

  def update_params
    params[:first_name]= params[:name]
    params.permit(:email, :first_name, :name, :country, :address, :state, :zip_code, :city, :street)
  end

  def new_params
    params.permit(:email, :first_name, :name, :country, :address, :state, :zip_code, :city, :street, :phone_number, :role, :source)
  end
  def billing_info_for_old_users(user)
    billing = Address.new
    billing.suit = user.try(:street)
    billing.street = user.try(:address)
    billing.city = user.try(:city)
    billing.country = user.try(:country)
    billing.zip = user.try(:zip_code)
    billing.state = user.try(:state)
    return billing
  end
end