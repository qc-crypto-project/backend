module V2::Merchant::BaseHelper

  def parse_date_with_time(date, offset = nil)
    first_date = date[0..9]
    first_date = Date.strptime(first_date, '%m/%d/%Y')
    firstdate = Time.new(first_date.year,first_date.month,first_date.day,00,00,00, offset).utc
    firsttime = firstdate.strftime("%H:%M:%S.00Z")
    firstdate = firstdate.strftime("%Y-%m-%dT#{firsttime}")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, '%m/%d/%Y')
    seconddate = Time.new(second_date.year,second_date.month,second_date.day,23,59,59, offset).utc
    secondtime = seconddate.strftime("%H:%M:%S.59Z")
    seconddate = seconddate.strftime("%Y-%m-%dT#{secondtime}")
    return {first_date: firstdate, second_date: seconddate}
  end

  def parse_export_time(t1,t2)
    if t1.present?
      time1 = t1+":00"+"+05:00"
    else
      time1 = "00:00"+":00"+"+05:00"
    end
    if t2.present?
      time2 = t2+":00"+"+05:00"
    else
      time2 = "23:59"+":00"+"+05:00"
    end
    return [time1,time2]
  end

  def parse_refund_date(start_date = nil, end_date = nil)
    if start_date.present?
      first_date = Date.strptime(start_date, '%d-%m-%Y') - 1.day
      first_date = first_date.strftime("%Y-%m-%dT%H:%M:%S.00Z")
    end
    if end_date.present?
      second_date = Date.strptime(end_date, '%d-%m-%Y') + 1.day
      second_date = second_date.strftime("%Y-%m-%dT23:59:59.59Z")
    end
    return {first_date: first_date, second_date: second_date}
  end

  def getting_old_report(date,params)
    return_hash = {find: false, report_file_id: nil, status: ""}
    return_hash = {find: true, report_file_id: nil, status: "pending"} if Report.exists?(status: "pending", user_id: current_user.id)
    if date.present?
      if params[:type] == "batch_transactions"
        reports = Report.where(first_date: date[:first_date], second_date: date[:second_date], transaction_type: params[:trans], user_id: current_user.id).where.not(status: ["crashed"]).select(:report_type,:status,:file_id,:transaction_type,:params)
      else
        reports = Report.where(first_date: date[:first_date], second_date: date[:second_date], user_id: current_user.id).where.not(status: ["crashed"]).select(:report_type,:status,:file_id,:transaction_type,:params)
      end
    end
    if reports.present?
      report_statuses = reports.pluck(:status).uniq
      if params[:type] == TypesEnumLib::WorkerType::Transactions
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type], transaction_type: params[:trans], status: "crashed").last
        end
        if report.blank?
          if params[:search_query].present?
            if current_user.merchant?
              report = reports.where.not(params: nil).where("report_type = ? AND transaction_type = ? AND params->>'date' = ? AND params->>'name' = ? AND params->>'email' = ? AND params->>'last4' = ? AND params->>'phone' = ? AND params->>'amount' = ? AND params->>'type' = ?" , params[:type], params[:trans], params[:search_query][:date], params[:search_query][:name], params[:search_query][:email], params[:search_query][:last4], params[:search_query][:phone],params[:search_query][:amount],["#{params[:search_query][:type]}"]).last
            elsif current_user.iso?
              report = reports.where.not(params: nil).where("report_type = ? AND params->>'date' = ? AND params->>'amount' = ? AND params->>'wallets_id' = ? AND params->>'DBA_name' = ? AND params->>'Receiver_name' = ? AND params->>'type' = ? AND params->>'time1' = ? AND params->>'time2' = ?", params[:type], params[:search_query][:date],params[:search_query][:amount],params[:search_query][:wallets_id], params[:search_query][:DBA_name], params[:search_query][:Receiver_name], ["#{params[:search_query][:type]}"], params[:search_query][:time1], params[:search_query][:time2]).last
            else
              report = reports.where.not(params: nil).where("report_type = ? AND params->>'date' = ? AND params->>'amount' = ? AND params->>'DBA_name' = ? AND params->>'Receiver_name' = ? AND params->>'type' = ? AND params->>'time1' = ? AND params->>'time2' = ?", params[:type], params[:search_query][:date],params[:search_query][:amount], params[:search_query][:DBA_name], params[:search_query][:Receiver_name], ["#{params[:search_query][:type]}"], params[:search_query][:time1], params[:search_query][:time2]).last
            end
          else
            report = reports.where(report_type: params[:type], transaction_type: params[:trans]).last
          end
        end
      else
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type],status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        report = reports.where(report_type: params[:type]).last if report.blank?
      end

      if report.present?
        report.update(read: false)
        return_hash[:find] = true
        return_hash[:status] = report.try(:status)
        return_hash[:report_file_id] = report.try(:file_id)
      end
    end
    return return_hash
  end

end