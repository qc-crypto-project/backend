
module V2::ApplicationHelper
  # include Admins::BlockTransactionsHelper
  # include HelpsHelper
  # include ActionView::Helpers::NumberHelper
  # include FluidPayHelper
  # include Merchant::TransactionsHelper
  # include Payment
  # include PosMerchantHelper

  def create_transaction_helper(merchant, transaction_to, transaction_from, user, receiver, action, card, fee, tip, privacy_fee, reserve_amount, amount, total_amount, user_info, type, sub_type, tags, gateway_fees,charge, payment_gateway, ip, previous_issue = nil,status = nil, seq_tx_id=nil, timestamp=nil,seq_tags=nil,request_id=nil,params=nil)
    main_type = updated_type(type, sub_type)
    gbox_fee = save_gbox_fee(tags.try(:[],"fee_perc"), fee) || 0
    total_fee = save_total_fee(tags.try(:[],"fee_perc"), fee) || 0
    net_fee = save_net_fee(total_fee,privacy_fee,main_type)
    net_amount = save_net_amount(total_amount,total_fee,reserve_amount,tip,privacy_fee,main_type)
    if params.try(:[],:discount).present? && params.try(:[],:discount).to_f > 0
      discount = params.try(:[],:discount).to_f
    elsif previous_issue.try(:[],"discount").present? && previous_issue.try(:[],"discount").to_f > 0
      discount = previous_issue.try(:[],"discount").to_f
    end
    payment_gateway = payment_gateway.present? ? payment_gateway : @payment_gateway
    sender_dba=get_business_name(Wallet.find_by(id:transaction_from))
    receiver_dba=get_business_name(Wallet.find_by(id:transaction_to))
    merchant.transactions.create(
        to: transaction_to,
        from: transaction_from,
        status: status || "pending",
        amount: amount,
        sender: user,
        sender_name: sender_dba,
        receiver_id: receiver.try(:id),
        receiver_name: receiver_dba,
        receiver_wallet_id: transaction_to,
        sender_wallet_id: transaction_from,
        action: action,
        last4: card.try(:last4) || previous_issue.try(:[],:tags).try(:[],"card").try(:[], "last4"),
        first6: card.try(:first6) || previous_issue.try(:[],:tags).try(:[],"card").try(:[], "first6"),
        fee: total_fee.to_f,
        tip: tip.to_f,
        privacy_fee: privacy_fee.to_f,
        reserve_money: reserve_amount.to_f,
        net_fee: number_with_precision(net_fee.to_f, precision: 2),
        net_amount: number_with_precision(net_amount, precision: 2),
        total_amount: total_amount.to_f,
        gbox_fee: gbox_fee.to_f,
        iso_fee: save_iso_fee(tags.try(:[],"fee_perc")),
        agent_fee: user_info.try(:[],"agent").try(:[],"amount").to_f,
        affiliate_fee: user_info.try(:[],"affiliate").try(:[],"amount").to_f,
        ip: ip,
        main_type: main_type,
        sub_type: sub_type,
        tags: seq_tags.present? ? seq_tags : tags,
        card_id: card.try(:id) ||  previous_issue.try(:[],:tags).try(:[],"card").try(:[], "id"),
        account_processing_limit: gateway_fees.try(:[],:account_processing_limit),
        transaction_fee: gateway_fees.try(:[],:transaction_fee),
        charge_back_fee: gateway_fees.try(:[],:charge_back_fee),
        retrieval_fee: gateway_fees.try(:[],:retrieval_fee),
        per_transaction_fee: gateway_fees.try(:[],:per_transaction_fee),
        reserve_money_fee: gateway_fees.try(:[],:reserve_money_fee),
        reserve_money_days: gateway_fees.try(:[],:reserve_money_days),
        monthly_service_fee: gateway_fees.try(:[],:monthly_service_fee),
        misc_fee: gateway_fees.try(:[],:misc_fee),
        misc_fee_dollars: gateway_fees.try(:[],:misc_fee_dollars),
        payment_gateway_id: payment_gateway.try(:id),
        load_balancer_id: payment_gateway.try(:load_balancer_id),
        charge_id: charge.try(:[], :response),
        invoice_id: request_id,
        seq_transaction_id: seq_tx_id,
        timestamp: timestamp,
        sender_balance: SequenceLib.balance(transaction_from).to_f,
        receiver_balance: SequenceLib.balance(transaction_to).to_f,
        discount: discount.to_f,
        tax: params.try(:[],:tax).to_f,
        product_fee: params.try(:[],:fee).to_f,
        fee_method: params.try(:[],:fee_method),
        discount_method: params.try(:[],:discount_method),
        shipping_amount: params.try(:[],:shipping_handling_fee).to_f,
        billing_id: params.try(:[],:billing_id)
    )
  end

  def transaction_between(to, from, amount, reference, fee, fee_type = nil, data = nil , ledger = nil, source = nil,sub_reference=nil,credit_check=nil,card=nil,tip_in_qr_sale=nil,trans_id=nil,payment_gateway=nil,card_type=nil, privacy_fee = nil,flag=nil,qr_scan_tx_amount=nil,load_fee=nil)
    reserveAmount = 0
    fee = 0
    @to = Wallet.find(to)
    @from = Wallet.find_by_id(from)
    to_user = @to.users.first
    from_user = @from.users.first if @from.present?
    fee_lib = FeeLib.new
    pay_gat = payment_gateway
    if source.present?
      app_config = source
    else
      app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first.stringValue
    end
    if to_user.present? && to_user.MERCHANT?
      # qc_wallet = Wallet.where(wallet_type: 3).first
      location = @to.location
      location_fee = location.fees if location.present?
    end
    if to_user.present? && from_user.present? && to_user.MERCHANT? && @to.wallet_type != 'tip' && !from_user.MERCHANT? && !from_user.qc? && reference != TypesEnumLib::TransactionType::InvoiceQC
      # case of to_user is merchant and from_user not merchant---------------
      if location_fee.present?
        #--------- Check if tip present ----------
        if tip_in_qr_sale.present?
          tip_amount = amount.to_f + tip_in_qr_sale[:sale_tip].to_f
        else
          tip_amount = amount.to_f
        end
        if privacy_fee.present? && qr_scan_tx_amount.blank?
          tip_amount = tip_amount.to_f + privacy_fee.to_f
        end
        # ---------- BuyRAte calculation start --------------
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location,nil,payment_gateway, nil, load_fee )
        #---------------get fee transaction Fee Mobile App
        if credit_check == TypesEnumLib::TransactionType::CreditTransaction #=----- Credit Card Fee----------------
          # C2B Case
          fee = fee_class.apply(qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : tip_amount.to_f, @invoice_request.present? ? 'invoice_credit' : 'credit')
        elsif credit_check == TypesEnumLib::TransactionType::DebitTransaction  #=---- Debit Card fee----------
          fee = fee_class.apply(qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : tip_amount.to_f, @invoice_request.present? ? 'invoice_debit' : 'debit')
        else                        #=---- Mobile App fee----------
          fee = fee_class.apply(tip_amount.to_f)
        end
        fee["amount"] = tip_amount if qr_scan_tx_amount.present?
        # ------ buyrate calculation end  --------------
        reserve_money = fee_object.reserve_fee.to_f
        reserveAmount = deduct_fee(reserve_money,amount)
        reserve_detail = {amount: reserveAmount, days: fee_object.days, wallet: location.wallets.reserve.first.id} if reserve_money > 0
        #= Sales with tip
        tr_type = get_transaction_type(reference,sub_reference)
        feeExists = false
        if fee.present?
          feeExists = true
          user_info = fee["splits"]
          if reserve_detail.present?
            reserve = {"reserve" => reserve_detail}
            user_info = user_info.merge(reserve)
          end
          pay_gat = payment_gateway
          pay_gat = PaymentGateway.find_by(key: source) if pay_gat.blank?

          gateway_fees = User.get_gateway_details(pay_gat) if pay_gat.present?
          if pay_gat.knox_payments? || pay_gat.payment_technologies? || pay_gat.total_pay?
            gateway_name=params[:bank_descriptor].present? ? params[:bank_descriptor] : pay_gat.try(:name)
            app_config = params[:bank_descriptor].present? ? params[:bank_descriptor] : app_config
          end
          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => gateway_name,
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "invoice_id" => @request.try(:id),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          if data.present? && data[:complete_trans].present?
            # to reject whole issue transaction present in data variable
            data = data.reject{|k| k == :complete_trans}
          end
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : @virtual_terminal_products ? params[:sub_total] : amount.to_f, tip_amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data,nil, nil,nil,nil,@request.try(:id),params)
          products = setting_products(params,nil, db_transaction) if @virtual_terminal_products
          tx = SequenceLib.transfer(tip_amount, from, to, tr_type.first, number_with_precision(fee['fee'].to_f, precision: 2), nil, app_config, nil, location.merchant, location, user_info, nil,get_ip, reserve_detail, ledger, data,tr_type.second,nil,nil,nil,nil,card,tip_in_qr_sale,nil,payment_gateway,privacy_fee,card_type,gateway_fees,flag)
        else
          tx = SequenceLib.transfer(tip_amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil,nil,get_ip, nil,ledger,data,tr_type.second,nil,nil,nil,nil,card,tip_in_qr_sale,nil,payment_gateway,privacy_fee,card_type,gateway_fees,flag)
        end
        if tx.present?
          parsed_transactions = parse_block_transactions(tx.actions, tx.timestamp)
          if tip_in_qr_sale.present?
            loc_wallet = Wallet.find(to) if to.present?
            merchant = loc_wallet.try(:location).try(:merchant)
            tip_block_txn = parsed_transactions.second
            tip_block_txn[:tags]["sequence_id"] = tip_block_txn[:seq_parent_id]
            create_transaction_helper(merchant, tip_in_qr_sale[:wallet_id], to, merchant, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], get_ip, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
          end
          if reserveAmount.to_f > 0 && reserve_detail.present?
            reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
            if reserve_tx.present?
              create_transaction_helper(to_user, reserve_detail[:wallet], to, to_user, to_user, "transfer", nil, nil,nil,nil, nil, reserveAmount.to_f, reserveAmount.to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip, nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
            end
          end
          save_block_trans(parsed_transactions) if parsed_transactions.present?
        end
      end
    elsif to_user.present? && from_user.present? && to_user.MERCHANT? && @to.wallet_type != 'tip' && reference == TypesEnumLib::TransactionType::InvoiceQC  #&& from_user.MERCHANT?
      # if to_user and from_user are both merchants
      #=-------------- B2b Transfer
      if location_fee.present?
        #get fee object
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location, TypesEnumLib::CommissionType::B2B)
        #get b2b fee value
        fee = fee_class.apply(amount.to_f)
        tr_type = get_transaction_type(reference,sub_reference)
        tr_type[1] = "invoice" if @request.try(:invoice?)
        if fee.present?
          # get users information
          user_info = fee["splits"]
          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => pay_gat.try(:name),
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, amount.to_f, amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data)
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee["fee"], nil, app_config, nil, location.merchant, location, user_info,nil,get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        end
      end
    elsif to_user.present? && from_user.present? && to_user.merchant? && from_user.qc?
      #= request money case if sender is qc and receiver is merchant
      #=-------------- B2b Transfer
      if location_fee.present?
        #get fee object
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location, TypesEnumLib::CommissionType::B2B)
        #get b2b fee value
        fee = fee_class.apply(amount.to_f)
        tr_type = get_transaction_type(reference,sub_reference)
        if fee.present?
          # get users information
          user_info = fee["splits"]
          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => pay_gat.try(:name),
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, amount.to_f, amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data)
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee["fee"], nil, app_config, nil, location.merchant, location, user_info,nil,get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
          # divide fee into iso, agent, partner
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        end
      end
    elsif to_user.present? && to_user.MERCHANT? && from_user.nil?
      # if to_user is merchant and from_user is not present
      #= QRCard Case
      if location_fee.present?
        #----------------get object of Mobile app fee
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location)
        #---------------get fee transaction Fee Mobile App
        if credit_check == TypesEnumLib::TransactionType::CreditTransaction #=----- Credit Card Fee
          fee = fee_class.apply(amount.to_f, 'credit')
        else                           #=---- Mobile App fee
          fee = fee_class.apply(amount.to_f)
        end
        tr_type = get_transaction_type(reference,sub_reference)
        if fee[:fee].present?
          # get users information
          # user_info = fee_lib.get_merchant_users_info(location, fee_object, company, company_wallet, cus_fee_ded, actual_fee, reserve_fee,fee_dollar_object)
          user_info = nil
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee[:fee], nil, app_config, nil, location.merchant, location, user_info, nil, get_ip, fee_object.days,ledger,data,tr_type.second,nil,nil,nil,nil,card)
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data, tr_type.second)
        end
      end

    elsif (!to_user.MERCHANT? && from_user.MERCHANT?) || from_user.PARTNER?
      tx = SequenceLib.transfer(amount, from, to, reference, fee, nil, app_config, nil, nil,nil, nil,nil, get_ip, nil, ledger, data)
    else
      #========== for sale with paytip process
      #========== if both to_user and from_user are not merchants
      tr_type =  get_transaction_type(reference, sub_reference)
      spending_limit = fee_lib.get_limit(amount,'transfer')
      raise StandardError.new "Your weekly spending limit is: #{fee_lib.show_limit('transfer')}" if !spending_limit
      tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee, nil, app_config, nil,nil,nil,nil,nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
    end
    if @to.present?
      #updating balance of @to wallet
      current_balance = @to.balance.to_f
      total_balance = current_balance + amount.to_f
      @to.update(balance: total_balance)
      to_user = @to.users.first
    end
    if from_user.present?
      # if from_user is present then parse transaction
      if @from.present?
        #updating balance of @from wallet
        current_balance = @from.balance.to_f
        total_balance = current_balance - amount.to_f
        @from.update(balance: total_balance)
      end
      if tx.blank?
        return nil
      else
      end
      # raise StandardError.new 'Transaction Failed!'
      if tx.present? && tx.actions.first.tags["fee_perc"].present?
        if tx.actions.first.tags["fee_perc"]["iso"].present? && tx.actions.first.tags["fee_perc"]["iso"]["amount"].to_f > 0
          iso_fee = tx.actions.first.tags["fee_perc"]["iso"]["amount"].to_f
          if tx.actions.first.tags["fee_perc"]["agent"].present? && tx.actions.first.tags["fee_perc"]["agent"]["amount"].to_f > 0
            iso_fee = iso_fee - tx.actions.first.tags["fee_perc"]["agent"]["amount"].to_f
            agent_fee=tx.actions.first.tags["fee_perc"]["agent"]["amount"].to_f
          end
          if tx.actions.first.tags["fee_perc"]["affiliate"].present? && tx.actions.first.tags["fee_perc"]["affiliate"]["amount"].to_f > 0
            agent_fee = agent_fee - tx.actions.first.tags["fee_perc"]["affiliate"]["amount"].to_f
            affiliate_fee=tx.actions.first.tags["fee_perc"]["affiliate"]["amount"].to_f
          end
        end
        total_bonus=tx.actions.first.tags["fee_perc"]["total_bonus"] if tx.actions.first.tags["fee_perc"]["total_bonus"].present?
      end
      transaction_type = ""
      if tx.present?
        transaction_type = tx.actions.first.tags["type"] if tx.actions.first.present?
        gateway_fee_details = tx.actions.first.tags["gateway_fee_details"] if tx.actions.first.present?
        tx = {id: tx.id,timestamp: tx.timestamp, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] ,transaction_id: tx.actions.first.id, destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount , source: tx.actions.first.source_account_id, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), tags: tx.actions.first.tags, iso_fee: iso_fee || 0, agent_fee: agent_fee || 0, affiliate_fee: affiliate_fee || 0, bonus: total_bonus || 0, card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"), charge_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"transaction_id"),gateway_fee_details: gateway_fee_details, seq_tx:tx}
      end
      if tx.present? && @to.present? && @from.present?
        to_user = @to.users.first
        if to_user.nil?
          to_user = @to.oauth_app.user
        end
        from_user = @from.users.first
        if from_user.nil?
          from_user = @from.oauth_app.user
        end
        if to_user.present?
          db_transaction.update(status: "approved",tags: tx[:tags], charge_id: tx[:charge_id],seq_transaction_id: tx[:transaction_id], timestamp: tx[:timestamp])
          if db_transaction.present?
            params[:db_trans_id] = db_transaction.id
          end
          hold_amount = amount.to_f
          if fee.present? && (fee.try(:[], :fee).present? || fee.try(:[], "fee").present?)
            # fee_amount = fee.try(:[], :fee).to_f || fee.try(:[], "fee").to_f
            fee_amount = fee.try(:[], :fee).to_f
            fee_amount = fee.try(:[], "fee").to_f if fee_amount == 0
            hold_amount = hold_amount - fee_amount.to_f
          end

          if reserveAmount.present? && reserveAmount > 0
            hold_amount = hold_amount - reserveAmount
          end

          if tx[:privacy_fee].to_f > 0
            hold_amount = hold_amount + tx[:privacy_fee].to_f
          end
          HoldInRear.hold_money_in_my_wallet(to,hold_amount.to_f, reserveAmount) if transaction_type.present? && [TypesEnumLib::TransactionType::QCSecure,TypesEnumLib::TransactionType::Transfer3DS,"sale"].include?(transaction_type)
        end
      end
      return tx
    else
      if tx.present?
        tx = {id: tx.id,timestamp: tx.timestamp, type: tx.actions.first.type ,transaction_id: tx.actions.first.id, destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount , source: tx.actions.first.source_account_id, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), tags: tx.actions.first.tags,db_trans_id: nil, card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id"), charge_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"transaction_id"), seq_tx:tx}
      end
      return tx
    end
  end

  def show_local_type(obj)
    if obj.main_type == TypesEnumLib::TransactionType::SaleType
      if obj.sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtualApi
        return "eCommerce"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtual
        return "Virtual Terminal"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::DebitCharge
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssueApi
        return "Sale Issue"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssue
        return "Sale Issue"
      end
    elsif obj.main_type == "Qr Credit Card"
      return "QR Credit Card"
    elsif obj.main_type == "Qr Debit Card"
      return "QR Debit Card"
    elsif obj.main_type == "Sale Issue" && obj.sub_type == "qr_debit_card"
      return "Sale Issue Debit"
    elsif obj.main_type == TypesEnumLib::TransactionType::Issue3DS
      return "Sale Issue"
    elsif obj.main_type == "QCP Secure" && obj.sub_type == "invoice"
      return "Invoice - Credit Card"
    elsif obj.main_type == "QCP Secure"
      return "QCP Secure"
    elsif obj.main_type == "QC Secure"
      return "QC Secure"
    elsif obj.main_type == "B2B Transfer" && obj.sub_type == "invoice"
      return "Invoice - QC"
    elsif obj.main_type == "Invoice Qc"
      return "Invoice - QC"
    elsif obj.main_type == "Invoice - Credit Card"
      return "Invoice - Credit Card"
    else
      obj.main_type.try(:humanize).try(:titleize) || obj.sub_type.try(:humanize).try(:titleize)
    end
  end

  def show_other_descriptor(obj)
    if obj.main_type == TypesEnumLib::TransactionViewTypes::SendCheck || obj.main_type == TypesEnumLib::TransactionViewTypes::BulkCheck
      "Checkbook Io"
    elsif obj.payment_gateway.present? && obj.main_type != I18n.t("types.TipTransfer")
      obj.payment_gateway.name
    else
      "Quickcard"
    end
  end

  def tip_sender_name(wallet_name)
    if wallet_name.include?("Tip Wallet")
      return "Tip Account"
    else
      return wallet_name
    end
  end

  def show_balance(wallet_id, ledger = nil)
    if wallet_id.present?
      SequenceLib.balance(wallet_id, ledger)
    end
  end

  def sale_text_message(amount = nil, location_name=nil,gateway=nil,card_number=nil,user = nil,location_number=nil)
    number = location_number || "4259546595"
    return "Dear #{user}, your card (x-#{card_number}) has been successfully charged #{amount} USD for your purchase from #{location_name}. This transaction will appear on your statement as #{gateway}. For questions or assistance, please contact #{number_to_phone(number, area_code: true) }."
    # return "Thank you for your $#{amount} purchase at #{merchant_name}. Please note: Your credit or debit card statement will reflect this transaction as a charge from: #{bank_name}. For questions or assistance, please contact us at: (425) 954-6595"
  end

  def send_text_message(body,to)
    begin
      TextsmsWorker.perform_async(to,body)
      return true
    rescue Exception => exc
      puts "=======EXCEPTION - SENDING TEXT MESSAGE======", exc.message
      return false
    end
  end

  def push_notification(message, user)
    if user.setting.push_notification
      unless user.registeration_id.nil?
        NotificationWorker.perform_async(message,user.registeration_id, user.device_type )
      end
    end
  end

  def make_elavon_payment_void(transaction_id, payment_gateway = nil)
    if transaction_id.present? #Void from Converge in case of seq failure
      elavon = Payment::ElavonGateway.new
      elavon.void(transaction_id,payment_gateway)
    end
  end

  def make_i_can_preauth_void(transaction_id, request, amount = nil, payment_gateway = nil)
    if transaction_id.present? #Void from ICanPay in case of seq failure
      i_can_pay = Payment::ICanPay.new(payment_gateway)
      i_can_pay.refund(transaction_id,amount, request)
    end
  end

  def make_bolt_pay_void(transaction_id, amount = nil, payment_gateway = nil)
    if transaction_id.present?
      bolt_pay = Payment::BoltPayGateway.new
      bolt_pay.refund(transaction_id, amount, payment_gateway)
    end
  end

  def refund_status_check(tran_id, parent_id)
    transaction = Transaction.where(seq_transaction_id: tran_id).first if tran_id.present?
    transaction = Transaction.where(seq_transaction_id: parent_id).first if transaction.blank? && parent_id.present?
    if !transaction.nil? && transaction.status == "refunded"
      return true
    else
      return false
    end
  end

  def set_profile_id(user)
    if user.present?
      if user.iso?
        "ISO-#{user.id}"
      elsif user.agent?
        "A-#{user.id}"
      elsif user.affiliate?
        "AF-#{user.id}"
      elsif user.partner?
        "CO-#{user.id}"
      elsif user.merchant?
        "M-#{user.id}"
      elsif user.affiliate_program?
        "AP-#{user.id}"
      end
    end
  end
end
