module V2::Partner::AccountsHelper
  def manage_partner_batches(iso_wallet_id, agent_wallet_id, affiliate_wallet_id, date,type, iso_starting_balance = nil, agent_starting_balance = nil, affiliate_starting_balance = nil)
    TransactionBatch.create(
        iso_wallet_id: iso_wallet_id,
        agent_wallet_id: agent_wallet_id,
        affiliate_wallet_id: affiliate_wallet_id,
        iso_balance: iso_starting_balance.to_f,
        agent_balance: agent_starting_balance.to_f,
        affiliate_balance: affiliate_starting_balance.to_f,
        batch_date: date,
        batch_type: type,
    )
  end

  def partner_percentage(original_number, new_number)
    if original_number.to_f > 0
      new_value = new_number.to_f - original_number.to_f
      number_with_precision((new_value/original_number) * 100, precision: 2, delimiter: ',')
    else
      number_with_precision(0, precision: 2)
    end
  end
end