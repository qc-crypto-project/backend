module RegistrationsHelper

  def register_in_stripe(user)
    customer = Stripe::Customer.create(:email => user.email)
    user.update(:stripe_customer_id => customer.id) if customer.present?
    user
  end
  #user = merchant
  def load_balancer(helper, gateway, card, merchant, qc_wallet, request, api_type=nil, tip_details=nil, user=nil, card_number=nil,email=nil,type=nil,fee_for_issue=nil,sub_type=nil,next_gateway=nil,card_info=nil, sales_info = nil, secure_i_can_pay=nil, gateway_number=nil,flag=nil,can_apply_load_balancer = nil)
    card_response_message = "#{card_info["scheme"].to_s.try(:capitalize) +' '+card_info["type"].to_s}" if card_info.present? && card_info["scheme"].present?
    gateway_block = ENV.fetch("GATEWAY_BLOCK") { '' }.to_s.split(',')
    unless can_apply_load_balancer.present? && can_apply_load_balancer
      #= TODO remove it
      return {blocked: true} if gateway_block.include?(gateway) && next_gateway.blank?
      return {response: nil, decline_message: "Decline please try new card"} if check_card_decline(card) && params.try(:[], "action") != "virtual_terminal_transaction_debit"
    end
    if (@payment_gateway.present? && @payment_gateway.fluid_pay?) || (gateway.present? && gateway ==  TypesEnumLib::GatewayType::FluidPay)
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_fluid_pay(card,merchant, qc_wallet,request, api_type, tip_details,user,card_number,type,email,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer)
      end
    elsif @payment_gateway.present? && (@payment_gateway.i_can_pay? || @payment_gateway.payment_technologies?)
      if gateway_block.include?(gateway)
        return {blocked: true}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_i_can_pay(card, merchant, qc_wallet, request, api_type, tip_details,user,email,type,fee_for_issue,sub_type,nil,@payment_gateway.present? ? @payment_gateway : nil,nil,sales_info,flag,can_apply_load_balancer)
      end
    elsif @payment_gateway.present? && @payment_gateway.converge?
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_converge(card, merchant, qc_wallet, request, api_type, tip_details,user,email,type,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer)
      end
    elsif @payment_gateway.present? && @payment_gateway.bolt_pay?
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "Gateway you trying is Blocked at this moment. Please contact to administration."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_bolt_pay(card, merchant, qc_wallet, request, api_type, tip_details,user,email,type,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer)
      end
    elsif @payment_gateway.present? && @payment_gateway.stripe?
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_stripe(card,merchant,qc_wallet,request, api_type, tip_details,card_number,gateway,user,email,type,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer)
      end
    elsif @payment_gateway.present? && @payment_gateway.knox_payments?
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_knox(card, merchant, qc_wallet, request, api_type, tip_details,user,email,type,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer)
      end
    elsif @payment_gateway.present? && @payment_gateway.total_pay?
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_total_pay(card, merchant, qc_wallet, request, api_type, tip_details,user,email,type,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer,card_number)
      end
    elsif @payment_gateway.present? && @payment_gateway.mentom_pay?
      if gateway_block.include?(gateway)
        return {blocked: true,response: nil,id:nil}
      else
        return {response: nil, message: {message: card_decline_message(card_response_message)}} if gateway_card_status(@payment_gateway,card_info)
        return {response: nil, message: {message: "The gateway you are trying is Blocked at this moment. Please contact your administrator."}} if @payment_gateway.present? && @payment_gateway.is_block?
        helper.issue_with_mentom_pay(card, merchant, qc_wallet, request, api_type, tip_details,user,email,type,fee_for_issue,sub_type,@payment_gateway.present? ? @payment_gateway : nil,card_info, sales_info,flag,can_apply_load_balancer)
      end
    end

  end


  def gateway_card_status(gateway,card_detail=nil) # getting card info. is it block or not in gateway setting
    temp = true
    return false if card_detail["scheme"].blank?
    card_selected = "#{card_detail["scheme"]}_#{card_detail["type"] || 'credit'}".try(:downcase)
    payment_cards = gateway.card_selection
    if payment_cards.present? && card_selected.present?
      if payment_cards[card_selected].present?
        temp = false if payment_cards[card_selected] == "on"
      end
    end
    return temp
  end

  def check_card_decline(card)
    attempts = ENV["CARD_DECLINE_ATTEMPTS"]
    if attempts.to_i <= 0
      attempts = 2
    end
    if card.present? && card.decline_attempts.to_i >= attempts.to_i
      true
    else
      false
    end
  end

  def card_decline_message(card_name)
    # if card_name.try(:downcase).include? "mastercard"
    #   return "MasterCard Connection is being updated at the moment. Please use a Visa card!"
    # end
    return "The #{card_name} you are trying is not supported. Please try a different card."
  end

end
