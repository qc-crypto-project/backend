module FluidPayHelper
	include FluidPay::CustomerVaultHelper
	include FluidPay::PaymentGatewayHelper
	include FluidPay::AddressHelper
	include FluidPay::ChargeTransactionHelper
	class FluidPayPayment
		include ApplicationHelper

		def initialize
		end

		def check_fluid_pay_api_status(transaction_id,payment_gateway = nil)
			if payment_gateway.present?
				api_key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
			else
				api_key = ENV['FLUID_PAY_ID']
			end
			if api_key.present?
				url = "#{ENV['FLUID_PAY_API_URL']}/transaction/#{transaction_id}"
				curlObj = Curl::Easy.new(url)
				curlObj.connect_timeout = 3000
				curlObj.timeout = 3000
				curlObj.headers = ["Authorization: #{api_key}",'Content-Type:application/json']
				curlObj.perform()
				if curlObj.body_str.present?
					data = JSON(curlObj.body_str)
				else
					data = {"data"=>{"status" => "unable to reach bank"}}
				end
				# settled

				required_status = data["data"]["status"]
				return required_status
			else
				return "Please Provide Api Key by setLogin('API_KEY')"
			end
		end

		def get_token_id(api_key,number,expiration_date,cvc,customer_id)
			if customer_id.present?
				get_token = FluidPay::PaymentGatewayHelper::PaymentGatewayFluid.new
				get_token.setLogin(api_key)
				if expiration_date.present?
					if expiration_date.length == 4
						expiration_date = "#{expiration_date[0..1]}/#{expiration_date[2..3]}"
					end
				end
				get_token.create_card_token(number,expiration_date,cvc,'','','','')
				data = get_token.doPost(customer_id)
				if data["status"] == "success"
					return [customer_id,data["data"]["card"]["id"]]
				else
					raise StandardError.new(data["msg"])
				end
			else
				get_id = FluidPay::CustomerVaultHelper::CustomerVault.new
				get_id.setLogin(api_key)
				if expiration_date.present?
					if expiration_date.length == 4
						expiration_date = "#{expiration_date[0..1]}/#{expiration_date[2..3]}"
					end
				end
				get_id.add_card_payment_method(number,expiration_date,cvc,'','','','')
				get_id.add_customer('testing')
				data = get_id.doPost
				if data["status"] == "success"
					return [data["data"]["id"],data["data"]["payment_method"]["card"]["id"]]
				else
					raise StandardError.new(data["msg"])
				end
			end
		end
		#------------------Refund Fluid-----------------
		def refundFluidAmount(transaction_id,amount,gateway)
			key = ENV['FLUID_PAY_ID']
			if gateway.present?
				# gateway = PaymentGateway.find_by(key: source)
				key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],gateway.client_secret) if gateway.present?
			end
			refund_amount=FluidPay::ChargeTransactionHelper::TransactionByFluid.new
			refund_amount.setLogin(key)
			data=refund_amount.refundTransaction(transaction_id,amount)
			puts "==============fluid-pay-refund=================="
			puts data
			puts "==============fluid-pay-refund=================="
			return data
		end

		def voidFluidPay(transaction_id,payment_gateway = nil)
			key = ENV['FLUID_PAY_ID']
			if payment_gateway.present?
				key = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'],payment_gateway.client_secret)
			end
			refund_amount=FluidPay::ChargeTransactionHelper::TransactionByFluid.new
			refund_amount.setLogin(key)
			data=refund_amount.voidTransaction(transaction_id)
			puts "==============fluid-pay-refund=================="
			puts data
			puts "==============fluid-pay-refund=================="

			return data
		end

		def chargeAmountRaw(api_key,amount,card_number,expiration_date,cvc,first_name,last_name, merchant = nil, card = nil, user = nil,transaction_info=nil,payment_gateway=nil, trans_id = nil,billing=nil)
			if amount.present?
				charge_the_amount = FluidPay::ChargeTransactionHelper::TransactionByFluid.new
				charge_the_amount.setLogin(api_key)
				if expiration_date.present?
					if expiration_date.length == 4
						expiration_date = "#{expiration_date[0..1]}/#{expiration_date[2..3]}"
					end
				end
				gateway = payment_gateway.present? ? payment_gateway.key : "fluid_pay"
				processor_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'], payment_gateway.processor_id) if payment_gateway.present? && payment_gateway.processor_id.present?
				user_id = user.id if user.present?
				charge_the_amount.make_a_transaction_raw(amount,card_number,expiration_date,cvc,first_name,last_name, processor_id,billing)
				data = charge_the_amount.doPost
				card_lib = CardToken.new
				card_bin_list = get_card_info(card_number)
				puts "FluidResp:",data
				transaction_info[:request_response] = data if transaction_info.present?
				response_msg = "Card Charge Error - unknown: Decline From Bank"
				if data["status"] == I18n.t("statuses.success")
					if data["data"]["response_body"]["card"]["response"].try(:downcase) == I18n.t("statuses.approved") || data["data"]["response_body"]["card"]["response"].try(:downcase) == I18n.t("statuses.success")
						if data["data"]["response_body"]["card"]["processor_response_text"].try(:downcase) == I18n.t("statuses.success") || data["data"]["response_body"]["card"]["processor_response_text"].try(:downcase) == I18n.t("statuses.approved")
							order_bank = OrderBank.new(user_id: user_id, merchant_id: merchant.id, bank_type: gateway,card_type: card_lib.card_brand(card_number), card_id: card.try(:id),card_sub_type:card_bin_list["type"].present? ? card_bin_list["type"] : nil,status: "approve",amount: cents_to_dollars(amount), transaction_info: transaction_info.to_json, payment_gateway_id: payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id),transaction_id: trans_id)
							card.decline_attempts = 0
							card.last_decline = nil
							card.save
							return {response:data["data"]["id"], message:data["data"]["response_body"]["card"]["id"], order_bank:order_bank}
						else
							message = {message: I18n.t("mid_limits.decline_from_bank")}
							slack_message = {message: "Possible Dispute #{data.try(:[],"data").try(:[],"response_body").try(:[],"card").try(:[],"processor_response_text")}"}
							transaction_info = (transaction_info.merge(message)).to_json
							order_bank = OrderBank.new(user_id: user_id,merchant_id: merchant.id, bank_type: gateway, card_id: card.try(:id),card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type:card_bin_list["type"].present? ? card_bin_list["type"] :  card.try(:card_type),status: "decline",amount: cents_to_dollars(amount), transaction_info: transaction_info, payment_gateway_id: payment_gateway.try(:id), load_balancer_id: payment_gateway.try(:load_balancer_id),transaction_id: trans_id)
							card.decline_attempts = card.decline_attempts.to_i + 1
							card.last_decline = DateTime.now.utc
							card.save
							msg = "PaymentGateway:(#{payment_gateway.id}) #{gateway} \nMerchant ID: M-#{merchant.id} \nC.C #: #{card_number.first(6)}******#{card_number.last(4)}\nReason: #{slack_message},\n Bank Response: //#{data.to_json}//"
							SlackService.notify(msg)
							return {response:nil,message: message, order_bank:order_bank}

						end
					elsif data["data"]["response_body"]["card"] && data["data"]["response_body"]["card"]["response"] == I18n.t("statuses.declined")
						message = {message: data["data"]["response_body"]["card"]["processor_response_text"] == I18n.t("statuses.success") ? response_msg : data["data"]["response_body"]["card"]["processor_response_text"]}
						transaction_info = (transaction_info.merge(message)).to_json
						order_bank = OrderBank.new(user_id: user_id,merchant_id: merchant.id, bank_type: gateway, card_id: card.try(:id),card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type:card_bin_list["type"].present? ? card_bin_list["type"] :  card.try(:card_type),status: "decline",amount: cents_to_dollars(amount), transaction_info: transaction_info, payment_gateway_id: payment_gateway.try(:id), load_balancer_id: payment_gateway.try(:load_balancer_id),transaction_id: trans_id)
						card.decline_attempts = card.decline_attempts.to_i + 1
						card.last_decline = DateTime.now.utc
						card.save

						return {response:nil,message: message, order_bank:order_bank}
					else
						message = {message: data.try(:[], "msg").present? ? data["msg"] != "success" ? data["msg"] : response_msg : response_msg}
						transaction_info = (transaction_info.merge(message)).to_json
						order_bank = OrderBank.new(user_id: user_id,merchant_id: merchant.id, bank_type: gateway, card_id: card.try(:id),card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type:card_bin_list["type"].present? ? card_bin_list["type"] :  card.try(:card_type),status: "decline",amount: cents_to_dollars(amount), transaction_info: transaction_info, payment_gateway_id: payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id),transaction_id: trans_id)
						card.decline_attempts = card.decline_attempts.to_i + 1
						card.last_decline = DateTime.now.utc
						card.save
						return {response:nil,message: message, order_bank:order_bank}
					end
				else
					message = {message: data.try(:[], "msg").present? ? data["msg"] != "success" ? data["msg"] : response_msg : response_msg}
					transaction_info = (transaction_info.merge(message)).to_json
					order_bank = OrderBank.new(user_id: user_id,merchant_id: merchant.id, bank_type: gateway, card_id: card.try(:id),card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type:card_bin_list["type"].present? ? card_bin_list["type"] :  card.try(:card_type),status: "decline",amount: cents_to_dollars(amount), transaction_info: transaction_info, payment_gateway_id: payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id),transaction_id: trans_id)
					card.decline_attempts = card.decline_attempts.to_i + 1
					card.last_decline = DateTime.now.utc
					card.save
					return {response:nil,message: message, order_bank:order_bank}
				end
			else
				raise StandardError.new('Please Provide The Amount!')
			end
		end

    def chargeAmountRawMerchant(api_key,amount,card_number,expiration_date,cvc,first_name,last_name, merchant = nil, card = nil, user = nil)
      if amount.present?
        charge_the_amount = FluidPay::ChargeTransactionHelper::TransactionByFluid.new
        charge_the_amount.setLogin(api_key)
        if expiration_date.present?
          if expiration_date.length == 4
            expiration_date = "#{expiration_date[0..1]}/#{expiration_date[2..3]}"
          end
        end
        charge_the_amount.make_a_transaction_raw(amount,card_number,expiration_date,cvc,first_name,last_name)
        data = charge_the_amount.doPost
				card_bin_list = get_card_info(card_number)
        card_lib = CardToken.new
        if data["status"] == "success"
          if data["data"]["response_body"]["card"]["response"] == "approved" or data["data"]["response_body"]["card"]["response"] == "success"
            # if current_user_id.present?
              OrderBank.create(merchant_id: merchant.id,user_id: user.id, bank_type: "fluid_pay", card_id: card.try(:id),card_type: card_lib.card_brand(card_number),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,status: "approve",amount: cents_to_dollars(amount) )
            # end
            return {charge: data["data"]["id"],
                    card_id: data["data"]["response_body"]["card"]["id"],
                    gateway: 'fluid_pay'}
          else
            # if current_user_id.present?
              OrderBank.create(merchant_id: merchant.id,user_id: user.id, bank_type: "fluid_pay", card_id: card.try(:id),card_type: card_lib.card_brand(card_number),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,status: "decline",amount: cents_to_dollars(amount))
            # end
            return check_gateway_error(data["data"]["response_body"]["card"]["processor_response_text"], "fluid_pay", merchant, user ,cents_to_dollars(amount) ,card_number ,expiration_date ,cvc ,first_name,last_name, card)
            # raise StandardError.new(data["data"]["response_body"]["card"]["processor_response_text"])
          end
        else
          # if current_user_id.present?
            OrderBank.create(merchant_id: merchant.id,user_id: user.id, bank_type: "fluid_pay", card_id: card.try(:id),card_type: card_lib.card_brand(card_number),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,status: "decline",amount: cents_to_dollars(amount))
          # end
          raise StandardError.new(data["msg"])
        end
      else
        raise StandardError.new('Please Provide The Amount!')
      end
    end
    #------------------Refund Fluid End-------------
		def chargeAmount(api_key,amount,customer_id,payment_method_id,description)
			if customer_id.present?
				if payment_method_id.present?
					if amount.present?
						charge_the_amount = FluidPay::ChargeTransactionHelper::TransactionByFluid.new
						charge_the_amount.setLogin(api_key)
						charge_the_amount.make_a_transaction(amount,customer_id,payment_method_id,description)
						data = charge_the_amount.doPost
						if data["status"] == "success"
							if data["data"]["response_body"]["card"]["status"] == "approved"
								return [data["data"]["id"],data["data"]["response_body"]["card"]["id"]]
							else
								raise StandardError.new(data["data"]["response_body"]["card"]["processor_response_text"])
							end
						else
							raise StandardError.new(data["msg"])
						end
					else
						raise StandardError.new('Please Provide The Amount!')
					end
				else
					raise StandardError.new('Please Provide The FuildCardId!')
				end
			else
				raise StandardError.new('Please Provide The customer_id!')
			end
		end
	end
end
