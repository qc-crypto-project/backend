module AdminsHelper
  include ApplicationHelper

  class AdminCheckError < StandardError; end

  def active_li(controller, action)
    if controller === controller_name && action === action_name
      'active'
    else
      ''
    end
  end

  # def ach_approve_func(check)
  #   amount = check.actual_amount.to_f
  #   if check.account_number.present? && check.routing_number.present?
  #     wallet = Wallet.find(check.wallet_id)
  #     location = wallet.location
  #     # if location.present? && location.block_withdrawal
  #     #   flash[:error] = "Checks are blocked for this user"
  #     #   return redirect_back(fallback_location: root_path)
  #     # end
  #     if wallet.present?
  #       decrypted = AESCrypt.decrypt("#{check.user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{check.wallet_id}",  check.account_token)
  #       data = JSON(decrypted)
  #       account_number = data["account_number"]
  #       account_type = data["account_type"]
  #       routing_number = check.routing_number
  #       type = 8
  #       ach_process = Payment::AchProcessor.new
  #       response = ach_process.new_transaction(location.first_name, location.last_name, account_number, routing_number,dollars_to_cents(amount),type)
  #       if response[:success] == true
  #         check.status = response[:response][:status]
  #         check.order_type = :ach
  #         check.checkId = response[:response][:id]
  #         check.approved = true
  #         check.ach_type = type
  #         check.send_via = "ach"
  #         check.bulk_check.update(:status => "Success", :response_message => "ACH") if check.bulk_check.present?
  #         check.save
  #         return {success: true, message: "Successfully approve a check to #{ check.recipient }"}
  #       else
  #         if response.present?
  #           return {success: false, message: response[:response]}
  #         else
  #           return {success: false, message: "Something Wrong happened please contact Administrator"}
  #         end
  #       end
  #     else
  #       return {success: false, message: "Something Wrong happened please contact Administrator"}
  #     end
  #   else
  #     return {success: false, message: "Cann't be proccessed by ACH"}
  #   end
  # end

  def instant_ach_approve_func(check,ach_gateway_id = nil)
    amount = check.actual_amount.to_f
    ach_gateway = AchGateway.find(ach_gateway_id)
    if check.account_number.present? && check.routing_number.present?
      user = check.user
      wallet = Wallet.find(check.wallet_id)
      location = wallet.location
      # if location.present? && location.block_ach
      #   return {success: false, message: "ACH is blocked for Location: #{location.business_name}"}
      # end
      if wallet.present?
        if check.instant_ach?
          is_paid = true
          instant_ach_escrow_trans(check,wallet,location,user,ach_gateway_id, is_paid)
        else
          if ach_gateway.ach_type == 'manual_integration'
            check.status = "IN_PROCESS"
          elsif ach_gateway.ach_type == 'direct_integration'
            check.status = "PAID"
          end
          check.order_type = :instant_ach
          check.approved = true
          check.send_via = "ACH"
          check.ach_gateway_id = ach_gateway_id
          check.bulk_check.update(:status => "Success", :response_message => "ACH") if check.bulk_check.present?
          check.save
        end
        return {success: true, message: "Successfully approve ACH to #{ check.recipient }"}
      else
        return {success: false, message: "Wallet is missing for check #{check.id}"}
      end
    else
      return {success: false, message: "Check #{check.id} Can't be proccessed by ACH"}
    end
  end

  def instant_ach_escrow_trans(check,wallet,location,user,ach_gateway_id, ispaid=nil)
    amount = check.actual_amount.to_f

    fee_lib = FeeLib.new
    if user.MERCHANT?
      location_fee = location.fees if location.present?
      fee_object = location_fee.buy_rate.first if location_fee.present?
      fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::ACH)
      fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::ACH)
      amount_sum = amount + fee["fee"].to_f
    elsif user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
      amount_sum, fee = deduct_sendcheck_fee(user,amount)
    else
      amount_sum = amount + fee_lib.get_card_fee(amount.to_f, 'echeck').to_f
      check_fee = fee_lib.get_card_fee(amount.to_f, 'echeck').to_f
    end
    check_info = {
        name: check.name,
        check_email: check.recipient,
        check_id: check.checkId,
        check_number: check.number
    }
    wallet = Wallet.find(check.wallet_id)
    if wallet.location.present?
      location = location_to_parse(wallet.location)
      merchant = merchant_to_parse(wallet.location.merchant)
    elsif wallet.try('users').try('first').present? &&  wallet.try('users').try('first').role!='merchant'
      location = nil
      merchant = merchant_to_parse(wallet.users.first)
    end
    check_escrow = Wallet.check_escrow.first
    escrow_user = check_escrow.try(:users).try(:first)
    tags = {
        "location" => location,
        "merchant" => merchant,
        "send_check_user_info" => check_info,
    }

    check_escrow = Wallet.check_escrow.first
    escrow_user = check_escrow.try(:users).try(:first)

    check_total_amount = check.amount.to_f
    fee_perc_details = {}
    if check.amount_escrow.present?
      main_tx = check.try(:transaction)
      fee_perc_details["fee_perc"] = main_tx.tags.try(:[],"fee_perc")
      receiver_dba=get_business_name(Wallet.find_by(id:wallet.id))
      transaction = Transaction.create(
          to: nil,
          from: wallet.id,
          status: "pending",
          amount: check.actual_amount,
          sender: escrow_user,
          sender_name: escrow_user.try(:name),
          receiver_wallet_id: wallet.id,
          sender_wallet_id: check_escrow.try(:id),
          sender_balance: SequenceLib.balance(check_escrow.try(:id)),
          receiver_balance: SequenceLib.balance(wallet.try(:id)),
          receiver_id: user.try(:id),
          receiver_name: receiver_dba,
          action: 'transfer',
          net_amount: check.actual_amount.to_f,
          total_amount: check_total_amount,
          ip: get_ip,
          main_type: "ACH",
          tags: tags,
          gbox_fee: main_tx.try(:gbox_fee),
          iso_fee: main_tx.try(:gbox_fee),
          affiliate_fee: main_tx.try(:gbox_fee),
          agent_fee: main_tx.try(:gbox_fee),
          fee: main_tx.try(:fee),
          net_fee: main_tx.try(:net_fee)
      )
      check.amount_escrow = false
    end
    if user.MERCHANT?
      issue = issue_checkbook_amount(check.actual_amount, check.wallet_id ,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Checkbook, fee["fee"],fee["splits"], check_info,check,true, nil,nil, ispaid)
    elsif user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
      issue = issue_checkbook_amount(check.actual_amount, check.wallet_id ,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Checkbook, fee,nil, check_info,check,true,true)
    end
    if issue.present?
      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
      save_block_trans(parsed_transactions,"sub","PAID") if parsed_transactions.present?
      check.status = "PAID"
      check.order_type = :instant_ach
      check.approved = true
      check.send_via = "ACH"
      check.ach_gateway_id = ach_gateway_id
      check.bulk_check.update(:status => "Success", :response_message => "ACH") if check.bulk_check.present?
      check.save
    end
  end

  # def ach_status(status)
  #   if status == "captured"
  #     return "IN_PROCESS"
  #   else
  #     return status
  #   end
  # end

  def instant_pay_approve_func(check,params=nil)
    amount = check.actual_amount.to_f
    if check.first6.present? && check.last4.present?
      wallet = Wallet.find(check.wallet_id)
      location = wallet.location
      # if location.present? && location.push_to_card
      #   flash[:error] = "Instant Pay is blocked for this user"
      #   return redirect_back(fallback_location: root_path)
      # end
      if wallet.present?
        check.status = "PAID"
        check.order_type = :instant_pay
        check.approved = true
        check.send_via = "instant_pay"
        check.bulk_check.update(:status => "Success", :response_message => "instant_pay") if check.bulk_check.present?
        check.save
        return {success: true, message: "Instant Pay successfully approved to #{ check.recipient }"}
      else
        return {success: false, message: "Something went wrong, please contact your administrator."}
      end
    else
      return {success: false, message: "Can't be proccessed at the moment, please contact your administrator."}
    end
  end

  def ach_batch_status(batch_value)
    statuses = batch_value.map{|a| a.status }.uniq
    status = "Pending"
    # status = "Complete" if statuses.count == 1 && (statuses.first == "captured" || statuses.first == "PAID")
    if statuses.count == 1
      if statuses.first == "PAID"
        status = "Complete"
      elsif statuses.first == "captured"
        status = "IN_PROCESS"
      end
    end

    return status
  end

  def ach_check_number(checkId)
    return checkId.last 6 if checkId.present?
  end

  def check_account_number(account_number)
    return "*****#{account_number}"
  end

  def check_batch_status(batch_value)
    statuses = batch_value.map{|a| a.status }.uniq
    status = "Pending"
    # status = "Complete" if statuses.count == 1 && (statuses.first == "captured" || statuses.first == "PAID")
    if statuses.count == 1
      if statuses.first == "PAID" || statuses.first == "PAID_WIRE" ||  statuses.first == "VOID" ||  statuses.first == "FAILED"
        status = "Complete"
      elsif statuses.first == "captured"
        status = "IN_PROCESS"
      end
    elsif statuses.count == 2
      count = (["PAID","VOID","PAID_WIRE","FAILED"] - statuses).count
      if count == 2
        status = "Complete"
      end
    elsif statuses.count == 3
      count = (["PAID","VOID","PAID_WIRE","FAILED"] - statuses).count
      if count == 1
        status = "Complete"
      end
    elsif  statuses.count == 4
      unless (statuses - ["PAID","VOID","PAID_WIRE","FAILED"]).present?
        status = "Complete"
      end
    end

    return status
  end

  def parse_wallet(wallet, transactions)
    list = []
    admin_wallet = Wallet.find(wallet) if wallet.present?
    if transactions.present?
    transactions.each do |obj|
      if obj.present?
        transaction_amount = obj.actions.first.amount
        tip = obj.actions.first.tags["tip"]
        list << filter_actions(obj, wallet).map do |t|
          {
              id: t.id,
              timestamp: obj.timestamp,
              main_type: t.tags["type"],
              old_type: parse_type(wallet, t),
              type: updated_type(t.tags["type"],t.tags["sub_type"],t.tags),
              sub_type: new_get_inner_type(t.tags),
              amount: number_with_precision(SequenceLib.dollars(t.amount), precision: 2),
              transaction_amount: number_with_precision(get_transaction_amount(t.tags, SequenceLib.dollars(transaction_amount).to_f + tip.to_f), precision: 2),
              transaction_type: updated_type(obj.actions.first.tags["type"],obj.actions.first.tags["sub_type"]),
              total_amount: number_with_precision(get_total_amount(SequenceLib.dollars(t.amount),t.tags), precision: 2),
              adminAmount: parse_admin_amount(t),
              destination: t.destination_account_id,
              tip: tip || getTip(t.tags),
              gateway: getGateway(t.tags),
              # destination_phone: phone_number(t.destination_account_id),
              # destination_email: account_email(t.destination_account_id),
              destination_name: destination_name(t.destination_account_id),
              source: get_source(t),
              # source_phone: phone_number(t.source_account_id),
              # source_email: account_email(t.source_account_id),
              # source_name: name(t.source_account_id),
              source_id: get_source(t),
              reference: t.tags,
              last4: get_last4(t.tags),
              denominations: get_denominations(t.tags),
              fee_perc: get_fee_info(t.tags),
              partner_fee: get_partner_fee(t.tags),
              iso_fee: get_iso_fee(t.tags),
              agent_fee: get_agent_fee(t.tags),
              fee: get_fee(t.tags),
              fee_type: get_fee_type(t.tags),
              privacy_fee: number_with_precision(get_privacy_fee(t.tags), precision: 2),
              net_privacy_fee: number_with_precision(get_net_privacy_fee(t.tags), precision: 2),
              wallet: admin_wallet,
              # inner_type: get_inner_type(t.tags),
              inner_type: new_get_inner_type(t.tags),
              location: get_location(obj.actions.first.tags),
              merchant: get_merchant(obj.actions.first.tags),
              hold_money: number_with_precision(get_hold_money(t.tags), precision: 2),
              fee_without_hold_money: number_with_precision(get_fee(t.tags).to_f, precision: 2),
              # amount_with_fee: number_with_precision(get_amount(SequenceLib.dollars(t.amount),get_fee_without_hold_money(t.tags),get_fee_type(t.tags)), precision: 2),
              # net: get_net(SequenceLib.dollars(t.amount),get_fee(t.tags),get_fee_type(t.tags))
              net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),parse_admin_amount(t),parse_type(wallet, t), t.tags).to_f, precision: 2),
              amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),parse_admin_amount(t),parse_type(wallet, t), t.tags).to_f, precision: 2),
              ip_address: get_transaction_ip(t.tags)
          }
        end
      end
      end
    end
    list.reduce([], :concat).reject(&:nil?)
  end

  def parse_local_db_transactions(transaction, wallet=nil)
    admin_wallet = Wallet.find(wallet) if wallet.present?
    t = transaction.tags
    list = {
        id: transaction.seq_transaction_id,
        main_type: t["type"],
        timestamp: transaction.created_at,
        type: updated_type(t["type"],t["sub_type"],t),
        sub_type: new_get_inner_type(t),
        amount: number_with_precision(get_main_amount(transaction.amount, t), precision: 2),
        total_amount: number_with_precision(get_total_amount(transaction.amount,t), precision: 2),
        adminAmount: for_local_db_parse_admin_amount(transaction.amount,t),
        destination: transaction.user_id,
        tip:getTip(t),
        gateway: getGateway(t),
        source: local_db_get_source(transaction,t),
        source_id: local_db_get_source(transaction,t),
        reference: t,
        fee_perc: get_fee_info(t),
        partner_fee: get_partner_fee(t),
        iso_fee: get_iso_fee(t),
        affiliate_fee: get_affiliate_fee(t),
        agent_fee: get_agent_fee(t),
        fee: get_fee(t),
        fee_type: get_fee_type(t),
        privacy_fee: number_with_precision(get_privacy_fee(t), precision: 2),
        net_privacy_fee: number_with_precision(get_net_privacy_fee(t), precision: 2),
        wallet: admin_wallet,
        inner_type: new_get_inner_type(t),
        location: get_location(t),
        merchant: get_merchant(t),
        hold_money: number_with_precision(get_hold_money(t), precision: 2),
        fee_without_hold_money: number_with_precision(get_fee(t).to_f, precision: 2),
        net: number_with_precision(get_new_net(transaction.amount,for_local_db_parse_admin_amount(transaction.amount,t),'TRANSFER', t).to_f, precision: 2),
        amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(transaction.amount),for_local_db_parse_admin_amount(transaction.amount,t),'TRANSFER', t).to_f, precision: 2),
        ip_address: get_transaction_ip(t),
        refund_status: ''
    }
  end


  def destination_name(object)
    if object.present?
      name = Wallet.find_by_id(object).try(:name)
      if name.present?
        name
      else
        "---"
      end
    else
      "---"
    end
  end

  def get_transaction_amount(tags, amount)
    if tags["type"] == TypesEnumLib::TransactionType::SendCheck || tags["type"] == TypesEnumLib::TransactionType::GiftCardPurchase || tags["type"] == TypesEnumLib::TransactionType::GiftCard
      amount.to_f - tags["fee"].to_f
    else
      amount.to_f
    end
  end

  def show_admin_total_amount(object, amount)
    if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
      {amount: number_with_precision(number_to_currency(amount.to_f + object[:fee_without_hold_money].to_f), precision: 2, delimiter: ',')}
    else
      {amount: number_with_precision(number_to_currency(amount.to_f), precision: 2, delimiter: ',')}
    end
  end

  def show_admin_amount(object, amount)
    # if object[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck
    #   {amount: number_with_precision(number_to_currency(amount.to_f - object[:fee_without_hold_money].to_f), precision: 2, delimiter: ',')}
    # else
    {amount: number_with_precision(number_to_currency(amount.to_f), precision: 2, delimiter: ',')}
    # end
  end

  def get_main_amount(amount, tags)
    if tags.present?
      if tags["privacy_fee"].present?
        if tags.try(:[], "sub_type").present? && (tags.try(:[], "sub_type") == "virtual_terminal_sale_api" || tags.try(:[], "sub_type") == "sale_issue_api" )
          return amount.to_f - tags.try(:[], "privacy_fee").to_f - tags.try(:[], "tip").to_f
        else
          return amount.to_f - tags.try(:[], "privacy_fee").to_f
        end
      elsif tags.try(:[], "sub_type").present? && (tags.try(:[], "sub_type") == "virtual_terminal_sale_api" || tags.try(:[], "sub_type") == "sale_issue_api" )
        return amount.to_f - tags.try(:[], "privacy_fee").to_f - tags.try(:[], "tip").to_f
      else
        return amount.to_f
      end
    else
      amount.to_f
    end
  end

  def get_privacy_fee(tags)
    if tags.present?
      if tags["privacy_fee"].present?
        number_with_precision(tags["privacy_fee"], precision: 2)
      else
        "0.00"
      end
    else
      "0.00"
    end
  end

  def get_net_privacy_fee(tags)
    if tags["fee"].present?
      fee = tags["fee"].to_f
      if tags["fee_perc"].try(:[], "iso").present? && tags["fee_perc"].try(:[], "iso").try(:[], "iso_buyrate").present?
        fee = tags["fee_perc"].try(:[], "iso").try(:[],"amount").to_f + tags["fee_perc"].try(:[], "gbox").try(:[],"amount").to_f
      end
      if fee > 0
        if tags["privacy_fee"].present?
          fee - tags["privacy_fee"].to_f
        else
          number_with_precision(fee, precision: 2)
        end
      else
        "0.00"
      end
    else
      "0.00"
    end
  end

  def new_get_inner_type(t)
    if t["type"] == "sale"
      if t["sub_type"].present?
        if t["sub_type"] == "sale_tip"
          return "sale_tip"
        elsif t["sub_type"] == "virtual_terminal_sale"
          return "virtual_terminal"
        elsif t["sub_type"] == "sale_issue"
          return "sales_issue"
        elsif t["sub_type"] == "sale_issue_api"
          return "sale_issue_api"
        elsif t["sub_type"] == "sale_giftcard"
          return "sale_giftcard"
        elsif t["sub_type"] == "tip_vt_transaction_&_debit"
          return "sale_tip"
        else
          return "N/A"
        end
      else
        return "N/A"
      end
    elsif t["type"] == "Fee_Transfer"
      if t["sub_type"].present?
        return t["sub_type"]
      else
        return "N/A"
      end
    else
      "N/A"
    end
  end

  def get_total_amount(amount, tags)
    if tags.present?
      privacy_fee = 0
      tip = 0
      privacy_fee = tags["privacy_fee"] if tags["privacy_fee"].present?
      tip = tags["tip"] if tags["tip"].present?
      if tags["sub_type"] != "virtual_terminal_sale_api" && tags["sub_type"] != "sale_issue_api"
        total_amount = amount.to_f + tip.to_f
      else
        total_amount = amount.to_f
      end
      if privacy_fee.to_f > 0
        total_amount = total_amount
      else
        total_amount = total_amount + privacy_fee.to_f
      end
      if tags["type"] == TypesEnumLib::TransactionType::SendCheck && tags["fee"].present?
        total_amount + tags["fee"].to_f
      else
        total_amount
      end
    else
      amount
    end
  end

  def get_source(transaction)
    if transaction.source_account_id.present?
      transaction.source_account_id
    elsif transaction.source_account_id.nil? && transaction.tags["api_type"].present?
      transaction.tags["api_type"].capitalize
    end
  end

  def local_db_get_source(transaction,tags)
    if transaction.sender_id.present?
      transaction.sender_id
    elsif transaction.sender_id.nil? && tags["api_type"].present?
      tags["api_type"].capitalize
    end
  end

  def get_new_net(amount, fee, type, tags)
    if tags["type"]=="send_check" && tags["fee_perc"].present?
      amount.to_f
    elsif tags["type"] == "giftcard_purchase"
      amount.to_f - fee.to_f
    elsif tags["type"]=="send_check"
      amount.to_f - fee.to_f
    elsif tags["type"]=="refund"
      amount.to_f
    elsif type == "RETIRE"
      amount.to_f
    else
      if tags["fee_perc"].present?
        if tags["fee_perc"]["reserve"].present? && (tags["type"] != "Fee_Transfer")
          amount.to_f - fee.to_f - tags["fee_perc"]["reserve"]["amount"].to_f
        else
          amount.to_f - fee.to_f
        end
      else
        amount.to_f - fee.to_f
      end
    end
  end

  def get_new_amount(amount, fee, type, tags=nil)
    if tags.present? && tags["type"] == "send_check" && tags["fee_perc"].present?
      amount.to_f + fee.to_f
    elsif tags.present? &&  tags["type"] == "giftcard_purchase"
      amount.to_f
    elsif tags.present? && tags["type"] == "send_check"
      amount.to_f
    elsif tags.present? && tags["type"] == "refund"
      amount.to_f + fee.to_f
    elsif type == "RETIRE"
      amount.to_f + fee.to_f
    else
      amount.to_f
    end
  end

  def get_admin_commission(amount, fee, type)
    if type == "Send Check" || type == "Giftcard Purchase"
      fee.to_f
    else
      amount.to_f
    end
  end

  def get_partner_fee(tags)
    if tags["fee_perc"].present?
      if tags["fee_perc"]["partner_total_fee"].present?
        return tags["fee_perc"]["partner_total_fee"].to_f
      elsif tags["fee_perc"]["partner_fee"].present? && tags["fee_perc"]["partner_check_fee"].present?
        return tags["fee_perc"]["partner_fee"].to_f + tags["fee_perc"]["partner_check_fee"].to_f
      elsif tags["fee_perc"]["company"].present?
        return tags["fee_perc"]["company"]["amount"]
      end
    end
  end

  def get_affiliate_fee(tags)
    if tags["fee_perc"].present?
      if tags["fee_perc"]["affiliate"].present?
        if tags["fee_perc"]["affiliate"]["amount"].to_f > 0
          return tags["fee_perc"]["affiliate"]["amount"]
        end
        if tags["fee_perc"]["affiliate"]["misc"].present? && tags["fee_perc"]["affiliate"]["misc"].to_f > 0
          return cents_to_dollars(tags["fee_perc"]["affiliate"]["misc"])
        end
        if tags["fee_perc"]["affiliate"]["service"].present? && tags["fee_perc"]["affiliate"]["service"].to_f > 0
          return cents_to_dollars(tags["fee_perc"]["affiliate"]["service"])
        end
        if tags["fee_perc"]["affiliate"]["statement"].present? && tags["fee_perc"]["affiliate"]["statement"].to_f > 0
          return cents_to_dollars(tags["fee_perc"]["affiliate"]["statement"])
        end
      end
    end
  end

  def get_iso_fee(tags)
    if tags["fee_perc"].present?
      if tags["fee_perc"]["iso_total_fee"].present? && tags["fee_perc"]["iso_total_fee"].to_f > 0
        return tags["fee_perc"]["iso_total_fee"].to_f
      elsif tags["fee_perc"]["iso_fee"].present? && tags["fee_perc"]["iso_check_fee"].present? && tags["fee_perc"]["iso_total_fee"].to_f > 0 && tags["fee_perc"]["iso_check_fee"].to_f > 0
        return tags["fee_perc"]["iso_fee"].to_f + tags["fee_perc"]["iso_check_fee"].to_f
      elsif tags["fee_perc"]["iso"].present?
        if tags["fee_perc"]["agent"].present?
          if tags["fee_perc"]["agent"]["amount"].to_f > 0
            return tags["fee_perc"]["iso"]["amount"].to_f - tags["fee_perc"]["agent"]["amount"].to_f
          end
        end
        if tags["fee_perc"]["iso"]["misc"].present? && tags["fee_perc"]["iso"]["misc"].to_f > 0
          if tags["fee_perc"]["agent"].present?
            if tags["fee_perc"]["agent"]["misc"].present? && tags["fee_perc"]["agent"]["misc"].to_f > 0
              return cents_to_dollars(tags["fee_perc"]["iso"]["misc"].to_f - tags["fee_perc"]["agent"]["misc"].to_f)
            end
          end
          return cents_to_dollars(tags["fee_perc"]["iso"]["misc"])
        end
        if tags["fee_perc"]["iso"]["service"].present? && tags["fee_perc"]["iso"]["service"].to_f > 0
          if tags["fee_perc"]["agent"].present?
            if tags["fee_perc"]["agent"]["service"].present? && tags["fee_perc"]["agent"]["service"].to_f > 0
              return cents_to_dollars(tags["fee_perc"]["iso"]["service"].to_f - tags["fee_perc"]["agent"]["service"].to_f)
            end
          end
          return cents_to_dollars(tags["fee_perc"]["iso"]["service"])
        end
        if tags["fee_perc"]["iso"]["statement"].present? && tags["fee_perc"]["iso"]["statement"].to_f > 0
          if tags["fee_perc"]["agent"].present?
            if tags["fee_perc"]["agent"]["statement"].present? && tags["fee_perc"]["agent"]["statement"].to_f > 0
              return cents_to_dollars(tags["fee_perc"]["iso"]["statement"].to_f - tags["fee_perc"]["agent"]["statement"].to_f)
            end
          end
          return cents_to_dollars(tags["fee_perc"]["iso"]["statement"])
        end
        return tags["fee_perc"]["iso"]["amount"]
      end
    end
  end

  def get_agent_fee(tags)
    if tags["fee_perc"].present?
      if tags["fee_perc"]["agent_total_fee"].present?
        return tags["fee_perc"]["agent_total_fee"].to_f
      elsif tags["fee_perc"]["agent_fee"].present? && tags["fee_perc"]["agent_check_fee"].present?
        return tags["fee_perc"]["agent_fee"].to_f + tags["fee_perc"]["agent_check_fee"].to_f
      elsif tags["fee_perc"]["agent"].present?
        if tags["fee_perc"]["affiliate"].present?
          if tags["fee_perc"]["affiliate"]["amount"].to_f > 0
            return tags["fee_perc"]["agent"]["amount"].to_f - tags["fee_perc"]["affiliate"]["amount"].to_f
          end
        end
        if tags["fee_perc"]["agent"]["misc"].present? && tags["fee_perc"]["agent"]["misc"].to_f > 0
          if tags["fee_perc"]["affiliate"].present?
            if tags["fee_perc"]["affiliate"]["misc"].present? && tags["fee_perc"]["affiliate"]["misc"].to_f > 0
              return cents_to_dollars(tags["fee_perc"]["agent"]["misc"].to_f - tags["fee_perc"]["affiliate"]["misc"].to_f)
            end
          end
          return cents_to_dollars(tags["fee_perc"]["agent"]["misc"])
        end
        if tags["fee_perc"]["agent"]["service"].present? && tags["fee_perc"]["agent"]["service"].to_f > 0
          if tags["fee_perc"]["affiliate"].present?
            if tags["fee_perc"]["affiliate"]["service"].present? && tags["fee_perc"]["affiliate"]["service"].to_f > 0
              return cents_to_dollars(tags["fee_perc"]["agent"]["service"].to_f - tags["fee_perc"]["affiliate"]["service"].to_f)
            end
          end
          return cents_to_dollars(tags["fee_perc"]["agent"]["service"])
        end
        if tags["fee_perc"]["agent"]["statement"].present? && tags["fee_perc"]["agent"]["statement"].to_f > 0
          if tags["fee_perc"]["affiliate"].present?
            if tags["fee_perc"]["affiliate"]["statement"].present? && tags["fee_perc"]["affiliate"]["statement"].to_f > 0
              return cents_to_dollars(tags["fee_perc"]["agent"]["statement"].to_f - tags["fee_perc"]["affiliate"]["statement"].to_f)
            end
          end
          return cents_to_dollars(tags["fee_perc"]["agent"]["statement"])
        end
        return tags["fee_perc"]["agent"]["amount"]
      end
    end
  end

  def get_iso_fee_view(tags)
    # get iso fee in transaction detail modal
    if tags.present?
      if tags[:reference].present? && tags[:fee_perc].present?
        if tags[:fee_perc]["iso"].present? && tags[:fee_perc]["iso"]["iso_buyrate"].present?
          return " $0.00"
        elsif tags[:fee_perc]["iso"].present?
          return number_to_currency(tags[:fee_perc]["iso"]["amount"].to_f)
        else
          return " $0.00"
        end
      else
        " $0.00"
      end
    else
      " $0.00"
    end
  end

  def get_gbox_fee(tags)
    # function for getting gbox fee in transaction detail modal
    if tags.present?
      if tags[:reference].present? && tags[:fee_perc].present?
        if tags[:fee_perc]["iso"].present? && tags[:fee_perc]["iso"]["iso_buyrate"].present?
          return number_to_currency(tags[:fee_perc]["iso"]["amount"].to_f + tags[:fee_perc]["gbox"]["amount"].to_f)
        else
          return number_to_currency(calculate_fees(tags[:reference]["fee"],tags[:fee_perc], tags[:reference]))
        end
      elsif tags[:fee].to_f > 0
        return number_to_currency(tags[:fee].to_f)
      else
        "$0.00"
      end
    else
      "$0.00"
    end
  end

  def show_view_fee(tags, fee)
    #show fee on detail modal admin
    if tags.present?
      if tags.try(:[],"iso").present? && tags.try(:[],"iso").try(:[], "iso_buyrate").present?
        if tags["gbox"]["amount"].to_f == 0
          return 0
        else
          return tags["gbox"]["amount"].to_f
        end
      else
        return fee
      end
    else
      fee.to_f
    end
  end

  def show_view_net_fee(tags,privacy_fee, fee)
    #show net fee on detail modal admin
    if tags.present?
      if tags.try(:[],"iso").present? && tags.try(:[],"iso").try(:[], "iso_buyrate").present?
        if tags["gbox"]["amount"].to_f == 0
          return 0
        else
          return tags["gbox"]["amount"].to_f - privacy_fee.to_f
        end
      else
        if privacy_fee.to_f > fee.to_f
        return  privacy_fee.to_f - fee.to_f
        else
        return fee.to_f - privacy_fee.to_f
        end
      end
    else
      fee.to_f
    end
  end

  def show_view_total_net(tags, amount, tip, reserve_money, fee)
    #show total net on detail modal admin
    if tags.present?
      if tags.try(:[],"iso").present? && tags.try(:[],"iso").try(:[], "iso_buyrate").present?
        return amount.to_f - tip.to_f - reserve_money.to_f - tags.try(:[], "gbox").try(:[], "amount").to_f
      else
        return amount.to_f - tip.to_f - reserve_money.to_f - fee.to_f
      end
    else
      amount.to_f - tip.to_f - reserve_money.to_f - fee.to_f
    end
  end

  def getting_fee_from_local_trans(t,user=nil)
    if user.present?
      if t.tags.try(:[],"fee_perc").present?
        if t.tags["fee_perc"]["#{user}"].present?
          if user == "iso"
            if t.tags["fee_perc"].try(:[],"iso").try(:[],"iso_buyrate").present?
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f
            elsif t.tags["fee_perc"]["agent"].present? # minus agent fee from iso
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f - t.tags["fee_perc"]["agent"]["amount"].to_f
            else
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f
            end
          elsif user == "agent"
            if t.tags["fee_perc"].try(:[],"agent").present? # minus affiliate fee from agent
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f - t.tags["fee_perc"]["affiliate"]["amount"].to_f
            else
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f
            end
          elsif user == "gbox"
            if t.tags["fee_perc"].try(:[],"iso").try(:[],"iso_buyrate").present?
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f + t.tags["fee_perc"]["iso"]["amount"].to_f
            else
              return t.tags["fee_perc"]["#{user}"]["amount"].to_f
            end
          else
            return t.tags["fee_perc"]["#{user}"]["amount"].to_f
          end
        elsif t.main_type == TypesEnumLib::TransactionViewTypes::GiftCardPurchase && user == "gbox"
          return t.try(:fee)
        else
          return "0.00"
        end
      else
        if user == "gbox"
          return t.try(:fee)
        else
          return "0.00"
        end
      end
    else
      return "0.00"
    end
  end

  def getting_fee_wallet_from_local_trans(t,user=nil)
    if user.present?
      if t.tags.present? && t.tags["fee_perc"].present?
        if t.try(:tags).try(:[], "fee_perc").try(:[], "#{user}").present?
          if t.tags["fee_perc"].try(:[],"iso").try(:[],"iso_buyrate").present? && user == "gbox" && t.tags["fee_perc"].try(:[],"iso").try(:[],"amount").present? &&  t.tags["fee_perc"].try(:[],"iso").try(:[],"amount") > 0
            user_wallet = Wallet.find_by(id: t.tags["fee_perc"]["#{user}"]["wallet"])
            if user_wallet.present?
              if user == 'iso' || user == 'agent' || user == 'affiliate'
                actual_user = user_wallet.users.first
                return "#{link_to 'wallet',profile_isos_path(format: actual_user.try(:slug),iso_id: actual_user.try(:id)), class: 'blue-color'}"
              else
                return "#{link_to 'wallet',user_transactions_path(user_wallet.slug), class: 'blue-color'}"
              end
            end
          elsif t.tags["fee_perc"]["#{user}"]["amount"].present? && t.tags["fee_perc"]["#{user}"]["amount"].to_f > 0
            user_wallet = Wallet.find_by(id: t.tags["fee_perc"]["#{user}"]["wallet"])
            if user_wallet.present?
              if user == 'iso' || user == 'agent' || user == 'affiliate'
                actual_user = user_wallet.users.first
                return "#{link_to 'wallet',profile_isos_path(format: actual_user.try(:slug),iso_id: actual_user.try(:id)), class: 'blue-color'}"
              else
                return "#{link_to 'wallet',user_transactions_path(user_wallet.slug), class: 'blue-color'}"
              end
            end
          end
        end
      end
    end
  end

  def get_inner_type(tags)
    if tags["type"].present?
      if tags["type"] == "ATM Cash Deposit"
        return "KIOSK Cash Deposit"
      elsif tags["type"] == "p2p_payment"
        return "P2P Payment"
      elsif tags["type"] == "send_check"
        return "Send Check"
      else
        return tags["type"]
      end
    end
  end

  def get_fee_type(tags)
    if tags["type"].present?
      return tags["type"].upcase
    end
  end

  def get_location(tags)
    if tags["location"].present?
      tags["location"]
    end
  end

  def get_merchant(tags)
    if tags["merchant"].present?
      if tags["merchant"].class != Integer
        tags["merchant"]["name"]
      else
        tags["merchant"].to_s
      end
    else
      "---"
    end
  end

  def get_denominations(tags)
    if tags["denominations"].present?
      tags["denominations"]
    else
      ''
    end
  end

  def get_last4(tags)
    if tags["last4"].present?
      tags["last4"]
    else
      ''
    end
  end


  # def get_first6(ta)
  #
  #   if ta.snapshot.present?
  #     if ta.snapshot["action_tags"].present?
  #       if ta.snapshot["action_tags"]["previous_issue"].present?
  #         if ta.snapshot["action_tags"]["previous_issue"]["tags"].present?
  #           if ta.snapshot["action_tags"]["previous_issue"]["tags"]["card"].present?
  #             ta.snapshot["action_tags"]["previous_issue"]["tags"]["card"]["first6"]
  #           end
  #         end
  #       end
  #     end
  #   else
  #     ''
  #   end
  # end


  def get_fee_without_hold_money(tags)
    if tags["fee"].present?
      tags["fee"].to_f
    end
  end

  def get_fee(tags)
    if tags["type"]=="retrievel_fee"
      return 0
    end
    if tags["fee"].present?
      if tags["location_fee"].present?
        fee = tags["fee"].to_f + tags["location_fee"].to_f
      else
        fee = tags["fee"].to_f
      end
      if tags["fee_perc"].present?
        if tags["fee_perc"]["iso"].present? && tags["fee_perc"]["iso"]["iso_buyrate"].present?
          fee = tags["fee_perc"].try(:[], "iso").try(:[],"amount").to_f + tags["fee_perc"].try(:[], "gbox").try(:[],"amount").to_f
        end
        if tags["fee_perc"]["hold_money"].present?
          if fee > 0
            return fee - tags["fee_perc"]["hold_money"].to_f
          else
            return fee
          end
        else
          return fee
        end
      else
        return fee
      end
    end
  end

  def get_fee_info(tags)
    if tags["fee_perc"].present?
      tags["fee_perc"]
    end
  end

  def parse_admin_amount(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    if transaction.tags['location_fee'].present?
      fee = transaction.tags['fee'].to_f + transaction.tags['location_fee'].to_f
    else
      fee = transaction.tags['fee'].to_f
    end
    # fee_perc = transaction.tags['fee_perc']
    if fee.present? && fee > 0 && amount != fee
      number_with_precision(fee, precision: 2)
    else
      number_with_precision(0, precision: 2)
    end
  end

  def for_local_db_parse_admin_amount(amount,tags)
    amount = SequenceLib.dollars(amount)
    if tags['location_fee'].present?
      fee = tags['fee'].to_f + tags['location_fee'].to_f
    else
      fee = tags['fee'].to_f
    end
    # fee_perc = transaction.tags['fee_perc']
    if fee.present? && fee > 0 && amount != fee
      number_with_precision(fee, precision: 2)
    else
      number_with_precision(0, precision: 2)
    end
  end

  def parse_type(wallet, action)
    # return action.type if wallet.nil?
    if wallet.present?
      if action.type == 'issue' && action.destination_account_id == wallet.to_s
        'DEPOSIT'
      elsif action.type == 'retire'
        "RETIRE"
      else
        if action.tags["type"] == 'fee' || action.tags["type"] == 'Fee_Transfer'
          'FEE'
        else
          'TRANSFER'
        end
      end
    else
      if action.type == 'issue'
        'DEPOSIT'
      elsif action.type == 'retire'
        "RETIRE"
      else
        if action.tags["type"] == 'fee' || action.tags["type"] == 'Fee_Transfer'
          'FEE'
        else
          'TRANSFER'
        end
      end
    end
  end

  def parse_wallet_type(t)
    if t.actions.first.type == 'issue'
      'DEPOSIT'
    elsif t.actions.first.type == 'retire'
      'RETIRE'
    else
      if t.actions.first.tags["type"] == 'fee'
        'FEE'
      else
        'TRANSFER'
      end
    end
  end

  def filter_actions(obj, wallet)
    return obj.actions.select{|o| o.source_account_id == wallet.to_s || o.destination_account_id == wallet.to_s} if wallet.present?
    obj.actions
  end

  def parse_date(date)
    date = date.split('-')
    if date.present? && date.first.present? && date.second.present?
      first_date = date.first.split('/')
      second_date = date.second ? date.second.split('/') : date.first.split('/')
      first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2].try(:strip)}
      second_date = {:day => second_date[1], :month => second_date[0].try(:strip), :year => second_date[2]}
      [first_date, second_date]
    else
      [nil, nil]
    end
  end

  def parse_date_time_new(date,offset=nil)
    date = date.split(' - ')
    if date.present? && date.first.present? && date.second.present?
      first_date = date.first.split('/')
      second_date = date.second ? date.second.split('/') : date.first.split('/')
      first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
      second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
      date1 = Time.new(first_date[:year], first_date[:month], first_date[:day], 00,00,00,params["offset"])
      date2 = Time.new(second_date[:year], second_date[:month], second_date[:day], 23,59,59,params["offset"])
      new_date={:first_date=>date1,:second_date=>date2}
      new_date
    else
      [nil, nil]
    end
  end

  def parse_time(params)
    if params[:time1].present?
      time1 = params[:time1]
    else
      time1 = "00:00"
    end
    if params[:time2].present?
      time2 = params[:time2]
    else
      time2 = "23:59"
    end
    return [time1,time2]
  end

  def get_wallet(object)
    if object[:source].present? && (object[:source] != "Credit" && object[:source] != "Debit" && object[:source] != "Sales_issue")
      object[:source]
    elsif object[:destination].present?
      object[:destination]
    end
  end

  def show_local_type(obj)
    if obj.main_type == TypesEnumLib::TransactionType::SaleType
      if obj.sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtualApi
        return "eCommerce"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtual
        return "Virtual Terminal"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::DebitCharge
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssueApi
        return "Sale Issue"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssue
        return "Sale Issue"
      end
    elsif obj.main_type == "Qr Credit Card"
      return "QR Credit Card"
    elsif obj.main_type == "Qr Debit Card"
      return "QR Debit Card"
    elsif obj.main_type == "Sale Issue" && obj.sub_type == "qr_debit_card"
      return "Sale Issue Debit"
    elsif obj.main_type == TypesEnumLib::TransactionType::Issue3DS
      return "3DS Issue"
    elsif obj.main_type == "3DS"
      return "3DS"
    else
      obj.main_type.try(:humanize).try(:titleize) || obj.sub_type.try(:humanize).try(:titleize)
    end
  end

  def show_local_type_for_decline(obj)
    if obj.main_type.nil? && obj.tags.present? && obj.tags["sub_type"].present?
      if obj.tags["sub_type"] == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        return obj.tags["sub_type"].humanize.titleize
      elsif obj.tags["sub_type"] == TypesEnumLib::TransactionSubType::SaleVirtualApi
        return "eCommerce"
      elsif obj.tags["sub_type"] == TypesEnumLib::TransactionSubType::SaleVirtual
        return "Virtual Terminal"
      elsif obj.tags["sub_type"] == TypesEnumLib::TransactionSubType::DebitCharge
        return obj.tags["sub_type"].humanize.titleize
      elsif obj.tags["sub_type"] == TypesEnumLib::TransactionSubType::SaleIssueApi
        return "Sale Issue"
      elsif obj.tags["sub_type"] == TypesEnumLib::TransactionSubType::SaleIssue
        return "Sale Issue"
      end
    else
      return "--"
    end
  end


  def withdrawal_bulk_status_update
    return_hash = {}
    begin
      if params[:id].present?
        check = TangoOrder.find(params[:id])
        user = check.user
        if check.present?
          check_info = {
              name: check.name,
              check_email: check.recipient,
              check_id: check.checkId,
              check_number: check.number,
              notes: params[:notes]
          }
          wallet = Wallet.eager_load(:location).find(check.wallet_id)
          if wallet.location.present?
            location = location_to_parse(wallet.location)
            merchant = merchant_to_parse(wallet.location.merchant)
          elsif wallet.try('users').try('first').present? &&  wallet.try('users').try('first').role!='merchant'
            location=nil
            merchant = merchant_to_parse(wallet.users.first)
          end
          check_escrow = Wallet.check_escrow.first
          escrow_user = check_escrow.try(:users).try(:first)
          tags = {
              "location" => location,
              "merchant" => merchant,
              "send_check_user_info" => check_info,
          }
          if params[:type].present? && params[:type]=='FAILED'
            ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::FailedAch : check.instant_pay? ?  TypesEnumLib::TransactionType::FailedPushtoCard : TypesEnumLib::TransactionType::FailedCheck)
          else
            ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
          end
          check_total_amount = check.amount.to_f
          if check.status == "PENDING" || check.status=='VOID' || check.status=='PAID_WIRE' || check.status=='IN_PROGRESS' || check.status == "IN_PROCESS" || ( (check.status=='FAILED' || check.status=='PAID') && check.instant_ach? )
            fee_perc_details = {}
            if (params[:type].present? && params[:type] == "VOID")
              if check.amount_escrow.present?
                main_tx = check.try(:transaction)
                params[:type2] = "VOID"
                fee_perc_details["splits"] = main_tx.tags.try(:[],"fee_perc")
                receiver_dba=get_business_name(Wallet.find_by(id:wallet.id))
                transaction = create_withdrawal_transaction(wallet, check, check_total_amount, user, ach_check_type, tags, 'transfer', escrow_user, check_escrow, receiver_dba, main_tx)
                issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, 0,fee_perc_details, check_info,check) if user.MERCHANT? || user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
                check.amount_escrow = false
              else
                transaction = create_withdrawal_transaction(wallet, check, check.actual_amount.to_f, user, ach_check_type, tags, 'issue')
                issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info, check)
              end
              if issue.present?
                transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
                #= creating block transaction
                check.void_transaction_id = transaction.id
                parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                save_block_trans(parsed_transactions) if parsed_transactions.present?
              end
            elsif params[:type].present? && params[:type]=='FAILED' && (check.PENDING? || (check.IN_PROGRESS? && check.instant_ach?) || (check.instant_ach? && (check.PAID? || check.PAID_WIRE?) && check.ach_gateway.try(:manual_integration?)) || (check.IN_PROCESS? && check.check?))
              if check.instant_pay?
                check = create_p2c_failed_withdrawal_transaction(check,user,wallet,tags,check_info)
              else
                check = create_failed_withdrawal_transaction(check,user,wallet,tags,check_info)
              end
              newstatus = "FAILED"
            end
            if params[:type].present? && (params[:type] == "paid_wire" || params[:type] == "PAID_WIRE") && params[:charge].present? && params[:charge] == 'charge'
              newstatus = 'PAID_WIRE'
              user= wallet.users.where(merchant_id:nil).first
              withdraw_void_ach_check(wallet,user,check)
            elsif params[:type].present? && params[:type] == "PAID" && params[:charge].present? && params[:charge]=='charge'
              newstatus="PAID"
              # if check.void_transaction_id.present?
              user= wallet.users.where(merchant_id:nil).first
              withdraw_void_ach_check(wallet,user,check)
              # end
            elsif (params[:type].present? && params[:type] == "PAID" || params[:type] == "paid_wire") && check.status=='IN_PROGRESS' && check.instant_ach?
              if params[:type] == "paid_wire"
                newstatus="PAID_WIRE"
              else
                newstatus="PAID"
              end
              is_paid = check.status=='IN_PROGRESS' && check.instant_ach? ? true : nil
              if check.amount_escrow.present?
                check_escrow_transactions(check,params,check_info,newstatus, check_escrow, escrow_user, nil, is_paid)
                check.amount_escrow = false
              end
            elsif params[:type].present? && (params[:type].downcase == "in_progress" || params[:type].downcase == "in_process")
              newstatus="IN_PROCESS"
              if check.instant_ach?
                check = create_in_process_withdrawal_transaction(check,user,wallet,tags,check_info)
                newstatus="IN_PROGRESS"
                check.order_type = :instant_ach
                check.send_via = "ACH"
                check.ach_gateway_id = params[:ach_gateway_id].to_i if params[:ach_gateway_id].present?
                check.bulk_check.update(:status => "Success", :response_message => "ACH") if check.bulk_check.present?
              end
            elsif params[:type].present? && params[:type] == "VOID"
              newstatus=params[:type]
              if check.amount_escrow.present?
                check_escrow_transactions(check,params,check_info,newstatus, check_escrow, escrow_user)
                check.amount_escrow = false
              end
            elsif params[:type].present? && params[:type] == "PAID" && !check.instant_ach?
              newstatus="PAID"
              check.settled=true
              if check.amount_escrow.present?
                check_escrow_transactions(check,params,check_info,newstatus, check_escrow, escrow_user)
                check.amount_escrow = false
              end
            else
              if params[:type].present? && (params[:type]=='paid_wire' || params[:type] == "PAID_WIRE")
                newstatus="PAID_WIRE"
                is_paid = check.status=='IN_PROGRESS' && check.instant_ach? ? true : nil
                if check.amount_escrow.present?
                  check_escrow_transactions(check,params,check_info,newstatus, check_escrow, escrow_user, nil, is_paid)
                  check.amount_escrow = false
                end
              else
                newstatus = params[:type].present? ? params[:type] : check.status if newstatus.blank?
                # if check.amount_escrow.present?
                #   check_escrow_transactions(check,params,check_info,newstatus, check_escrow, escrow_user)
                #   check.amount_escrow = false
                # end
              end
            end
            check.status = newstatus
            check.approved = true
            check.settled = true
            notes = {
                customer_note: params[:notes].try(:[],:customer_note).try(:strip),
                internal_note: params[:notes].try(:[],:internal_note).try(:strip)
            }
            check.notes = notes.to_json
            if check.save
              if check.ach? || check.instant_ach?
                return_hash =  {success: true, message: "Successfully updated ACH"}
              elsif check.instant_pay?
                return_hash = {success: true, message: "Successfully updated Push to Card"}
              else
                return_hash = {success: true, message: "Successfully updated Check"}
              end
            else
              return_hash = {success: false, message: "Error Occurred"}
            end
          end
        end
      end
    rescue AdminCheckError => exc
      error = JSON.parse(response.body)
      msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
      return_hash = {success: false, message: "Sorry for Inconvinence"}
    end
    return return_hash
  end

  def void_check(check)
    url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
    http = Net::HTTP.new(url.host,url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Delete.new(url)
    request["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
    request["Content-Type"] ='application/json'
    response = http.request(request)
    case response
    when Net::HTTPSuccess
      check_info = {
          name: check.name,
          check_email: check.recipient,
          check_id: check.checkId,
          check_number: check.number,
          notes: params[:notes]
      }
      wallet = Wallet.find(check.wallet_id)
      user = wallet.try(:users).try(:first)
      transaction = Transaction.create(
          to:  wallet.id,
          status: "pending",
          amount: check.actual_amount,
          receiver_wallet_id: wallet.id,
          receiver_id: user.try(:id),
          receiver_name: user.try(:name),
          action: 'issue',
          net_amount: check.actual_amount.to_f,
          total_amount: check.actual_amount.to_f,
          ip: get_ip,
          main_type: ach_check_type,
          tags: tags
      )
      if params[:type].present? && params[:type] == "issue"
        trs = issue_checkbook_amount(check.actual_amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
        if trs.present?
          transaction.update(
              status: "approved",
              seq_transaction_id: trs.actions.first.id,
              timestamp: trs.timestamp,
              tags: trs.actions.first.tags
          )
          check.void_transaction_id = transaction.id
          #= creating block transaction
          parsed_transactions = parse_block_transactions(trs.actions, trs.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
        end
      end
      if params[:type].present? && params[:type] == "paid_wire"
        newstatus = 'PAID_WIRE'
      elsif params[:type].present? && params[:type] == "PAID"
        newstatus = 'PAID'
      else
        newstatus = params[:status]
      end
      check.status = newstatus
      check.settled = true
      notes = {
          customer_note: params[:notes].try(:[],:customer_note).try(:strip),
          internal_note: params[:notes].try(:[],:internal_note).try(:strip)
      }
      check.notes = notes.to_json
      if check.save
        if check.ach? || check.instant_ach?
          flash[:error] = "Successfully updated ACH"
        elsif check.instant_pay?
          flash[:error] = "Successfully updated push to card"
          result = {success: true, message: "Successfully updated Push to Card"}
        else
          flash[:error] = "Successfully updated check"
          result = {success: true, message: "Successfully updated Check"}
        end
      else
        flash[:error] = "Error Occurred"
        result = {success: false, message: "Error Occurred"}
      end
    when Net::HTTPUnauthorized
      error = JSON.parse(response.body)
      msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
      return_hash = {success: false, message: I18n.t('merchant.controller.unauthorize_access')}
    when Net::HTTPNotFound
      flash[:error] = I18n.t('merchant.controller.record_notfound')
      error = JSON.parse(response.body)
      msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
      return_hash = {success: false, message: I18n.t('merchant.controller.unauthorize_access')}
    when Net::HTTPServerError
      flash[:error] = I18n.t('merchant.controller.record_notfound')
      error = JSON.parse(response.body)
      msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
      return_hash = {success: false, message: I18n.t('merchant.controller.unauthorize_access')}
    else
      error = JSON.parse(response.body)
      msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
      return_hash = {success: false, message: "#{error["error"]}"}
    end
  end

  def get_plugin_file(plugins, plugin_type)
    if plugin_type=='magento_1'
      plugins.magento_1.last
    elsif plugin_type=='magento_2'
      plugins.magento_2.last
    elsif plugin_type=='wordpress'
      plugins.wordpress.last
    elsif plugin_type=='woocommerece'
      plugins.woocommerece.last
    else
      plugins.open_cart.last
    end
  end

  def javascript_exists?(script)
    script = "#{Rails.root}/app/assets/javascripts/#{script}.js"
    extensions = %w(.coffee .erb .coffee.erb) + [""]
    extensions.inject(false) do |truth, extension|
      truth || File.exists?("#{script}#{extension}")
    end
  end

  def iso_profit_split_locations(location)
    if location.present? && location.profit_split == true
      if location.profit_split_detail.present?
        split_detail=JSON.parse(location.profit_split_detail)
        if split_detail["splits"]["profit_split"].present?
          split_detail["splits"]["profit_split"].except('from').each do |key, array|
            if array["role"].try(:downcase) == "merchants"
              # if key_name.include?('merchant') && !key_name.include?("ez")
                wallets = Location.find_by_id(array["wallet"]).try(:merchant_primary_locations)
                return wallets
              # end
            end
          end
        end
      end
    end
  end

  def net_profit_split_locations(location)
    if location.present? && location.net_profit_split == true
      if location.profit_split_detail.present?
        split_detail=JSON.parse(location.profit_split_detail)
        split_detail["splits"].each do |key, array|
          if key == "net"
            key = array.keys.first
            array = array.values.first
            key_name = key.tr("0-9", "")
            unless key_name.blank?
              if key_name.include?('merchant')
                wallets = Location.find_by_id(array["wallet"]).try(:merchant_primary_locations)
                return wallets
              end
            end
          end
        end
      end
    end
  end

  def ez_merchant_locations(location)
    if location.present? && location.sub_merchant_split == true
      if location.profit_split_detail.present?
        split_detail=JSON.parse(location.profit_split_detail)
        split_detail["splits"].try(:[],"ez_merchant").try(:each) do |key, array|
          array = array.values.first
          wallets = Location.find_by_id(array["wallet"])
          if wallets.present?
            locations = wallets.merchant_primary_locations
            return locations
          end
        end
      end
    end
  end

  def create_withdrawal_transaction(wallet, check, check_total_amount, user, ach_check_type, tags, action, escrow_user=nil, check_escrow=nil, receiver_dba=nil, main_tx=nil, fee_info=nil, from_task=nil, escrow_wallet=nil, failed_ach=false)
    gbox_fee = fee_info.present? ? fee_info["splits"].try(:[],"gbox").try(:[],"amount") : main_tx.try(:gbox_fee)
    iso_fee = fee_info.present? ? fee_info["splits"].try(:[],"iso").try(:[],"amount") : main_tx.try(:iso_fee)
    affiliate_fee = fee_info.present? ? fee_info["splits"].try(:[],"affiliate").try(:[],"amount") : main_tx.try(:affiliate_fee)
    agent_fee = fee_info.present? ? fee_info["splits"].try(:[], "agent").try(:[],"amount") : main_tx.try(:agent_fee)
    fee = fee_info.present? ? fee_info["fee"] : main_tx.try(:fee)
    net_fee = fee_info.present? ? fee_info["fee"] : main_tx.try(:net_fee)
    net_amount = fee_info.present? ? check.amount.to_f - fee_info["fee"].to_f : check.actual_amount.to_f
    tx_amount = check.actual_amount
    sender_wallet_id = check_escrow.try(:id)
    receiver_wallet_id = wallet.id
    if failed_ach
      sender = ach_check_type == "Fee_Transfer" ? escrow_user : user
      sender_name = ach_check_type == "Fee_Transfer" || ach_check_type == "ach" ? check_escrow.try(:name) : receiver_dba   # merchant dba name in case of failed ach fee transfer from merchant to escrow or withdrawl escrow to escrow
      receiver_id = escrow_wallet.try(:id)
      receiver_name = escrow_wallet.try(:name)
      from = ach_check_type == "Fee_Transfer" || ach_check_type == "ach" ? check_escrow.try(:id) : wallet.try(:id)
      to = escrow_wallet.try(:id)
      tx_amount = (ach_check_type == "ach" || ach_check_type == "Failed_Ach_Fee") ? fee_info.try(:[], "fee") : tags.try(:[], "fee") # in case of failed ach fee transfer to show fee as tx amount
      check_total_amount = fee_info.try(:[], "fee") if (ach_check_type == "ach" || ach_check_type == "Failed_Ach_Fee") # in case of failed ach fee transfer to show fee as tx amount
      fee = ach_check_type == "Fee_Transfer" ? nil : fee
      sender_wallet_id =  ach_check_type == "Fee_Transfer" || ach_check_type == "ach" ? check_escrow.try(:id) : wallet.try(:id)
      receiver_wallet_id = escrow_wallet.try(:id)
    else
      sender = action == "issue" ? ach_check_type == "Failed ACH" ? nil : user : escrow_user
      sender_name = action == "issue" ? ach_check_type == "Failed ACH" ? nil :  user.try(:name) : escrow_user.try(:name)
      receiver_id = action == "issue" ? ach_check_type == "Failed ACH" ? user.try(:id) : nil : user.try(:id)
      receiver_name = action == "issue" ? ach_check_type == "Failed ACH" ? receiver_dba :  nil : receiver_dba
      from = (ach_check_type == "Failed ACH" || ach_check_type == "Failed Push To Card") ? action == "issue" ? nil : check_escrow.try(:id) : wallet.try(:id)
      to = (ach_check_type == "Failed ACH" || ach_check_type == "Failed Push To Card") ? wallet.try(:id) : nil
    end
    transaction = Transaction.create(
        to: to,
        from: from,
        status: "pending",
        amount: tx_amount,
        sender: sender,
        sender_name: sender_name,
        receiver_wallet_id: receiver_wallet_id,
        sender_wallet_id: sender_wallet_id,
        sender_balance: SequenceLib.balance(sender_wallet_id),
        receiver_balance: SequenceLib.balance(receiver_wallet_id),
        receiver_id: receiver_id,
        receiver_name: receiver_name,
        action: action,
        net_amount: net_amount,
        total_amount: check_total_amount,
        ip: from_task ? nil : get_ip,
        main_type: ach_check_type,
        tags: tags,
        gbox_fee: gbox_fee,
        iso_fee: iso_fee,
        affiliate_fee: affiliate_fee,
        agent_fee: agent_fee,
        fee: fee,
        net_fee: net_fee
    )
  end

  def create_failed_withdrawal_transaction(check,user,wallet,tags=nil,check_info=nil)
    check_escrow = Wallet.check_escrow.first
    escrow_wallet = Wallet.escrow.first
    escrow_user = check_escrow.try(:users).try(:first)
    ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::FailedAch : check.instant_pay? ?  TypesEnumLib::TransactionType::FailedPushtoCard : TypesEnumLib::TransactionType::FailedCheck)
    check_total_amount = check.amount.to_f
    if user.merchant?
      check_location = wallet.location
      location_fee = check_location.fees
      fee_object = location_fee.buy_rate.first if location_fee.present?
      fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, check_location,TypesEnumLib::CommissionType.const_get("Failed#{check.order_type}"))
      fee = fee_class.apply(check.actual_amount.to_f, TypesEnumLib::CommissionType.const_get("Failed#{check.order_type}"))
      total_fee = fee["fee"].to_f
    else
      if user.affiliate_program?
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee if user.system_fee.present? && user.system_fee != "{}"
      end
      db_fee_perc = 0
      if fee_object.present?
        dollar_fee = check.instant_pay? ? fee_object["failed_push_to_card_fee"] : check.instant_ach? ? fee_object["failed_ach_fee"] : fee_object["failed_check_fee"]
        db_check_fee = dollar_fee.present? ? dollar_fee.to_f : 0
      else
        db_check_fee = 0
      end
      total_fee = db_fee_perc + db_check_fee
      fee = {"amount" => check.actual_amount.to_f,"splits" =>{"gbox" => {"amount" => total_fee, "wallet" => Wallet.qc_support.first.id}}, "fee" => total_fee, "iso_agent" => true}
    end
    params[:type2] = "FAILED"
    receiver_dba=get_business_name(Wallet.find_by(id:wallet.id))
    if check.amount_escrow.present?
      main_tx = check.try(:transaction)
      transaction = create_withdrawal_transaction(wallet, check, check.actual_amount, user, ach_check_type, tags, 'transfer', escrow_user, check_escrow, receiver_dba)
      issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Quickcard, total_fee,fee, check_info,check, nil,nil,nil,nil) if user.MERCHANT? || user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
      check.amount_escrow = false
    else
      transaction = create_withdrawal_transaction(wallet, check, check.actual_amount.to_f, user, ach_check_type, tags, 'issue', nil, nil, receiver_dba)
      if check.instant_ach? && check.ach_gateway.manual_integration? && (check.PAID? || check.PAID_WIRE?) && params[:type] == "FAILED"
        check_obj = nil
        failed_fee = fee["fee"].to_f
        user_info = fee.try(:[], "splits")
      else
        check_obj = check
        failed_fee = 0
        user_info = nil
      end
      issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Quickcard, failed_fee,user_info, check_info, check_obj)
    end
    if issue.present?
      if check.instant_ach? && (check.ach_gateway.nil? || check.ach_gateway.manual_integration?) && params[:type] == "FAILED" && (check.IN_PROGRESS? || check.PAID? || check.PAID_WIRE?)
        action = check.status == "IN_PROGRESS" ? "transfer" : "issue"
        transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
        void_txn_id = transaction.id
        create_failed_ach_transactions(wallet, check, user, escrow_user, check_escrow, receiver_dba, main_tx, fee, escrow_wallet, issue["actions"].first.tags, issue, action, total_fee,void_txn_id)
      else
        transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
      end
      #= creating block transaction
      check.void_transaction_id = transaction.id
      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
    end
    return check
  end

  def batch_query_search_params
    if params[:q].present?
      if params[:batch_date].present?
        batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
        batch_start_date = batch_start_date_int + 24.hours
        batch_end_date = batch_start_date + 24.hours
      elsif params[:q].try(:[], "batch_date").present?
        date = parse_date(params[:q][:batch_date])
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
        second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
        batch_start_date_int = (first_date.to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
        batch_end_date_int = (second_date.to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
        batch_start_date = batch_start_date_int + 24.hours
        batch_end_date = batch_end_date_int + 24.hours
      end
      params[:q][:batch_date_gteq] = batch_start_date
      params[:q][:batch_date_lteq] = batch_end_date
      if params[:q][:status].present?
        if params[:q][:status] == I18n.t("withdraw.inprocess")
          params[:q][:status_in] = [I18n.t("withdraw.inprogress"), I18n.t("withdraw.unpaid")]
        else
          params[:q][:status_cont] = params[:q][:status]
        end
      end
      if params[:q][:option].present? && params[:q][:find_by].present?
        if params[:q][:option] == "id"
          params[:q][:id_eq] = params[:q]["find_by"]
        elsif params[:q][:option] == "number"
          params[:q][:number_eq] = params[:q]["find_by"]
        elsif params[:q][:option] == "business_name"
          params[:q][:wallet_location_business_name_cont] = params[:q]["find_by"]
        elsif params[:q][:option] == "name"
          params[:q][:name_cont] = params[:q]["find_by"]
        elsif params[:q][:option] == "recipient"
          params[:q][:recipient_cont] = params[:q]["find_by"]
        elsif params[:q][:option] == "amount"
          params[:q][:amount_eq] = params[:q]["find_by"]
        elsif params[:q][:option] == "merchant_id"
          params[:q][:user_id_eq] = params[:q]["find_by"]
        elsif params[:q][:option] == "account_number"
          params[:q][:account_number_eq] = params[:q]["find_by"]
        end
      end
      if params[:q][:category].present?
        category =  params[:q][:category]
      elsif params[:batch_ach_id].present?
        category = params[:batch_ach_id].split("-").first
      end
      if category.present?
        if category == "DI"
          wallets = Wallet.dispensary_wallets
          order_type = I18n.t("withdraw.ACH")
        elsif category == "CB"
          wallets = Wallet.cbd_wallets
          order_type = I18n.t("withdraw.ACH")
        elsif category == "AI"
          order_type = I18n.t("withdraw.ACH")
        elsif category == "OT"
          wallets = Wallet.other_wallets
          order_type = I18n.t("withdraw.ACH")
        elsif category == I18n.t("withdraw.p2c_afp")
          order_type = I18n.t("withdraw.p2c")
        elsif category == I18n.t("withdraw.ach_afp") || category == I18n.t("withdraw.AFP")
          order_type = I18n.t("withdraw.ACH")
        elsif category == I18n.t("withdraw.send_check") || params[:q][:category] == I18n.t("withdraw.p2c")
          order_type = params[:q][:category]
        end
      end
    end
    order_type = [I18n.t("withdraw.ACH"), I18n.t("withdraw.send_check"), I18n.t("withdraw.p2c")].map(&:to_sym) if order_type.nil?
    if category == "AI"
      batches = TangoOrder.batch_list_with_ransack(order_type, true, params[:q], [false, nil])
    elsif category == I18n.t("withdraw.p2c_afp") || category == I18n.t("withdraw.ach_afp") || category == I18n.t("withdraw.AFP")
      batches = TangoOrder.batch_list_with_ransack(order_type, [false, nil], params[:q], true)
    elsif category == "OT"
      batches = TangoOrder.batch_list_with_ransack(order_type, [false, nil], params[:q], [false, nil])
      batches = batches.select{|ach| !wallets.include?(ach.wallet_id)}
    elsif order_type == I18n.t("withdraw.p2c") && category == I18n.t("withdraw.p2c")
      batches = TangoOrder.batch_list_with_ransack(order_type, [false,nil], params[:q], [false,nil])
    elsif category == "CB" || category == "DI"
      batches = TangoOrder.batch_list_with_ransack(order_type, [false, nil], params[:q], [false,nil])
      batches = batches.select{|ach| wallets.include?(ach.wallet_id)}
    else
      batches = TangoOrder.order_type(order_type).order(batch_date: :desc).ransack(params[:q]).result
    end
    batches.select{|ach| ach.batch_date.present?}
  end

  def get_parent_merchant_name(id)
    user = User.find(id)
    merchant_id = user.merchant_id
    parent_merchant = User.find(merchant_id) if merchant_id.present?
    parent_merchant_info = { id: parent_merchant.id, ref_no: parent_merchant.ref_no, name: parent_merchant.name} if parent_merchant.present?
  end

 def create_failed_ach_transactions(wallet, check, user, escrow_user, check_escrow, receiver_dba, main_tx, fee, escrow_wallet, failed_fee_tags, issue, action, failed_fee, void_txn_id=nil)
    failed_ach = true
    #Transaction for Failed ACH Fee
    if failed_fee.to_f > 0
      ach_check_type = TypesEnumLib::TransactionType::FailedAchFee
      failed_fee_obj = issue["actions"].select{|v| v.try(:[], "tags").try(:[], "type") == "Failed_Ach_Fee"}.first
      failed_fee_tags = failed_fee_obj.try(:[], "tags")
      seq_txn_id = failed_fee_obj.try(:id)
      failed_fee_txn_amount = failed_fee_tags.try(:[], "fee")
      failed_fee_tags["void_txn_id"] = void_txn_id
      failed_fee_transaction = create_withdrawal_transaction(wallet, check, failed_fee_txn_amount, user, ach_check_type, failed_fee_tags, action, escrow_user, check_escrow, receiver_dba, main_tx, fee, nil, escrow_wallet, failed_ach)
      failed_fee_transaction.update(seq_transaction_id: seq_txn_id, status: "approved", timestamp: issue["timestamp"])
    end
  end

  def create_in_process_withdrawal_transaction(check,user,wallet,tags=nil,check_info=nil)
    check_escrow = Wallet.check_escrow.first
    escrow_wallet = Wallet.escrow.first
    escrow_user = check_escrow.try(:users).try(:first)
    ach_check_type= TypesEnumLib::TransactionType::ACH
    check_total_amount = check.amount.to_f
    if user.merchant?
      check_location = wallet.location
      location_fee = check_location.fees
      fee_object = location_fee.buy_rate.first if location_fee.present?
      fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, check_location,TypesEnumLib::CommissionType.const_get("ACH"))
      fee = fee_class.apply(check.actual_amount.to_f, TypesEnumLib::CommissionType.const_get("ACH"))
      total_fee = fee["fee"].to_f
    else

      if user.affiliate_program?
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee if user.system_fee.present? && user.system_fee != "{}"
      end
      if fee_object.present?
        total_fee = fee_object["ach_dollar"].to_f + (check.actual_amount.to_f * (fee_object["ach_percent"].to_f)/100)
      end
      fee = {"amount" => check.actual_amount.to_f,"splits" =>{"gbox" => {"amount" => total_fee, "wallet" => Wallet.qc_support.first.id}}, "fee" => total_fee, "iso_agent" => true}
    end
    params[:type2] = "IN_PROCESS"
    receiver_dba=get_business_name(Wallet.find_by(id:wallet.id))
    if check.amount_escrow.present?
      main_tx = check.try(:transaction)
      if main_tx.tags.try(:[],"fee_perc").try(:[], "gbox").try(:[] , "amount").to_f  > 0 || (!user.merchant? && main_tx.tags.try(:[],"fee").to_f > 0)
        transaction = create_withdrawal_transaction(wallet, check, check_total_amount, user, ach_check_type, tags, 'transfer', escrow_user, check_escrow, receiver_dba, main_tx, fee, nil, escrow_wallet, true)
      end
      issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Quickcard, total_fee,fee, check_info,check, nil,nil,nil,nil) if user.MERCHANT? || user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
    end
    if issue.present?
      if main_tx.tags.try(:[],"fee_perc").try(:[], "gbox").try(:[] , "amount").to_f > 0 || (!user.merchant? && main_tx.tags.try(:[],"fee").to_f > 0)
        transaction.update(seq_transaction_id: issue["actions"].first["id"], status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
        check.void_transaction_id = transaction.id
      end
        #= creating block transaction
      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
    end
    return check
  end

  def create_p2c_failed_withdrawal_transaction(check,user,wallet,tags=nil,check_info=nil)
    check_escrow = Wallet.check_escrow.first
    escrow_wallet = Wallet.escrow.first
    escrow_user = check_escrow.try(:users).try(:first)
    ach_check_type=updated_type(TypesEnumLib::TransactionType::FailedPushtoCard)
    if user.merchant?
      # for p2c create fee splits(to distribute this fee according to splits in Failed case)
      fee = get_p2c_create_fee_splits(user, wallet, check)
      total_fee = fee["fee"].to_f
    else
      fee = get_p2c_create_fee_splits(user, wallet, check)
      total_fee = fee["fee"].to_f
    end
    params[:type2] = "FAILED"
    receiver_dba=get_business_name(Wallet.find_by(id:wallet.id))
    if check.amount_escrow.present?
      main_tx = check.try(:transaction)
      transaction = create_withdrawal_transaction(wallet, check, check.actual_amount, user, ach_check_type, tags, 'transfer', escrow_user, check_escrow, receiver_dba)
      issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, total_fee,fee, check_info,check, nil,nil,nil,nil) if user.MERCHANT? || user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
      check.amount_escrow = false
    else
      transaction = create_withdrawal_transaction(wallet, check, check.actual_amount.to_f, user, ach_check_type, tags, 'issue', nil, nil, receiver_dba)
      if check.instant_ach? && check.ach_gateway.manual_integration? && (check.PAID? || check.PAID_WIRE?) && params[:type] == "FAILED"
        check_obj = nil
        failed_fee = fee["fee"].to_f
        user_info = fee.try(:[], "splits")
      else
        check_obj = check
        failed_fee = 0
        user_info = nil
      end
      issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, failed_fee,user_info, check_info, check_obj)
    end
    if issue.present?
      if check.instant_ach? && check.ach_gateway.manual_integration? && params[:type] == "FAILED" && (check.IN_PROGRESS? || check.PAID? || check.PAID_WIRE?)
        action = check.status == "IN_PROGRESS" ? "transfer" : "issue"
        transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
        create_failed_ach_transactions(wallet, check, user, escrow_user, check_escrow, receiver_dba, main_tx, fee, escrow_wallet, issue["actions"].first.tags, issue, action, total_fee)
      else
        transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
      end
      #= creating block transaction
      check.void_transaction_id = transaction.id
      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
    end
    return check
  end

  def get_p2c_create_fee_splits(user, wallet, check)
    fee = {}
    if user.merchant?
      check_location = wallet.location
      location_fee = check_location.fees
      fee_object = location_fee.buy_rate.first if location_fee.present?
      p2c_create_fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, check_location,TypesEnumLib::CommissionType::DebitCardDeposit)
      fee = p2c_create_fee_class.apply(check.actual_amount.to_f, TypesEnumLib::CommissionType::DebitCardDeposit)
    else
      if user.affiliate_program?
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee if user.system_fee.present? && user.system_fee != "{}"
      end
      if fee_object.present?
        total_fee = fee_object["push_to_card_dollar"].to_f + (check.actual_amount.to_f * (fee_object["push_to_card_percent"].to_f)/100)
      end
      fee = {"amount" => check.actual_amount.to_f,"splits" =>{"gbox" => {"amount" => total_fee, "wallet" => Wallet.qc_support.first.id}}, "fee" => total_fee, "iso_agent" => true}
    end
    return fee
  end

end