module CheckbookHelper
  include Admins::BlockTransactionsHelper
  include AdminsHelper
  def update_check_order(check, checkbook_status)
    raise "Invalid Check/Status!" if check.nil? || checkbook_status.nil?
    puts "-------->> #{check.class}"
    # app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
    user = check.user
    wallet = Wallet.eager_load(:location).find(check.wallet_id) if check.wallet_id.present?
    # raise "Invalid Check Object" if check.class == TangoOrder
    if wallet.present?
      if wallet.location.present?
        location = location_to_parse(wallet.location)
        merchant = merchant_to_parse(wallet.location.merchant)
      elsif wallet.try('users').try('first').present? &&  !wallet.try('users').try('first').try(:merchant?)
        location=nil
        merchant = merchant_to_parse(wallet.users.first)
      end
      check_info = {
          name: check.name,
          check_email: check.recipient,
          check_id: check.checkId,
          check_number: check.number
      }
      tags = {
          "location" => location,
          "merchant" => merchant,
          "send_check_user_info" => check_info,
      }
      notes = {}
      notes = JSON check.notes if check.notes.present?
      notes["checkbook_status"] = checkbook_status
      notes = notes.to_json
      if check.status == "UNPAID"
        if checkbook_status == "PAID"
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        elsif checkbook_status == "FAILED"
          trs = issue_checkbook_amount(check.actual_amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0, nil, check_info)
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        elsif checkbook_status == "VOID"
          trs = issue_checkbook_amount(check.actual_amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        elsif checkbook_status == "IN_PROCESS"
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = false if checkbook_status.present? && check.present?
        elsif checkbook_status == 'UNPAID'
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = false if checkbook_status.present? && check.present?
        elsif checkbook_status == "PRINTED"
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        end
      elsif check.status == "IN_PROCESS"
        if checkbook_status == "PAID"
          check.status = checkbook_status
          check.settled = true if checkbook_status.present? && check.present?
        elsif checkbook_status == "FAILED" || checkbook_status == "REFUNDED"
          if check.instant_pay?
            check = create_p2c_failed_withdrawal_transaction(check,user,wallet,tags,check_info)
          else
            check = create_failed_withdrawal_transaction(check,user,wallet,tags,check_info)
          end
          check.status = "FAILED" if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        elsif checkbook_status == "VOID"
          type = check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck
          trs = issue_checkbook_amount(check.actual_amount, wallet ,type,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        elsif checkbook_status == "IN_PROCESS"
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = false if checkbook_status.present? && check.present?
        elsif checkbook_status == 'UNPAID'
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = false if checkbook_status.present? && check.present?
        elsif checkbook_status == "PRINTED"
          check.status = checkbook_status if checkbook_status.present? && check.present?
          check.settled = true if checkbook_status.present? && check.present?
        end
      elsif checkbook_status == "PRINTED"
        check.status = checkbook_status if checkbook_status.present? && check.present?
        check.settled = true if checkbook_status.present? && check.present?
      end
      check.notes = notes
      check.save
      if trs.present?
        #= creating block transaction
        parsed_transactions = parse_block_transactions(trs.actions, trs.timestamp)
        save_block_trans(parsed_transactions) if parsed_transactions.present?
      end
    end
  end

  # def issue_checkbook_amount(amount, wallet, type, source_name, total_fee, user_info = nil, check_info = nil,check=nil,while_approving=nil,iso_agent_aff_case=nil)
  #   qc_wallet = Wallet.escrow.first
  #   tr_type = get_transaction_type(type)
  #   ip=request.remote_ip if request.remote_ip.present?
  #   if check.present?
  #     @wallet  = Wallet.where(id: check.wallet_id).first
  #     location = nil
  #     merchant = nil
  #     location = location_to_parse(@wallet.location) if @wallet.location.present?
  #     merchant = merchant_to_parse(@wallet.location.merchant) if @wallet.location.present?
  #
  #     to = Wallet.check_escrow.first.id
  #     if while_approving
  #       tx = SequenceLib.retire(amount, to ,tr_type.first, total_fee, nil, source_name, nil, merchant, location,nil, check.send_via, user_info, check_info, ip, nil, tr_type.second,nil,nil,iso_agent_aff_case,nil)
  #     else
  #       tx = SequenceLib.transfer(check.amount, to, check.wallet_id, tr_type.first, nil, nil, source_name, nil, merchant, location, nil, nil,ip, nil, nil, nil,tr_type.second,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,to,check_info)
  #     end
  #   else
  #     tx = SequenceLib.issue(wallet.id, amount, qc_wallet, tr_type.first, total_fee, nil, source_name, nil, nil,nil,nil, user_info,ip,check_info)
  #   end
  #   # tx = SequenceLib.issue(wallet.id, amount, qc_wallet, tr_type.first, total_fee, nil, source_name, nil, nil,nil,nil, user_info,ip,check_info)
  # end
end
