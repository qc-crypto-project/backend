module Admins::PaymentGatewaysHelper

  def get_status(gateway)
    if gateway.is_block
      {status: "Inactive", color: "inactive_div"}
    else
      {status: "Active", color: "active_div"}
    end
  end

  def get_location_status(location)
    if location.is_block
      {status: "Inactive", color: "inactive_div"}
    else
      {status: "Active", color: "active_div"}
    end
  end

  def mtds_total_processed(gateway,transactions)
    date = Date.today.at_beginning_of_month
    if transactions[gateway.name].present?
      mtd_transactions = transactions[gateway.name].group_by {|mt| mt.created_at >= date}
      return mtd_transactions[true].try(:pluck, :total_amount).try(:sum).to_f
    else
      return 0
    end
    # gateway.transactions.where("transactions.created_at >=? and transactions.main_type IN (?)",date,[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
  end

  def mtds_daily_total_processed(gateway,transactions)
    date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day
    if transactions[gateway.name].present?
      mtd_daily_transactions = transactions[gateway.name].group_by {|mt| mt.created_at >= date}
      return mtd_daily_transactions[true].try(:pluck, :total_amount).try(:sum).to_f
    else
      return 0
    end
    # gateway.transactions.where("transactions.created_at >=? and transactions.main_type IN (?)",date,[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
  end

  def total_mid_processed(type,transactions)
    date = Date.today.at_beginning_of_month
    if transactions[PaymentGateway.types[type]].present?
      mtd_transactions = transactions[PaymentGateway.types[type]].group_by {|mt| mt.created_at >= date}
      return mtd_transactions[true].try(:pluck, :total_amount).try(:sum).to_f
    else
      0
    end
    #Transaction.joins(:payment_gateway).where('transactions.created_at >=? and payment_gateways.type=? and payment_gateways.is_deleted=? and transactions.main_type IN (?)',date,PaymentGateway.types[type],false,[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
  end

  def total_daily_processed(type,transactions)
    date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day
    if transactions[PaymentGateway.types[type]].present?
      mtd_daily_transactions = transactions[PaymentGateway.types[type]].group_by {|mt| mt.created_at >= date}
      return mtd_daily_transactions[true].try(:pluck, :total_amount).try(:sum).to_f
    else
      return 0
    end
    #Transaction.joins(:payment_gateway).where('transactions.created_at >=? and payment_gateways.type=? and payment_gateways.is_deleted=? and transactions.main_type IN (?)',date,PaymentGateway.types[type],false,[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
  end

  def total_cbk(gateway)
    date = Date.today.at_beginning_of_month
    gateway.transactions.joins(:dispute_case).where('transactions.created_at >=?',date).count
  end
  
  def total_cbk_percentage(gateway)
    date = Date.today.at_beginning_of_month
    transaction = gateway.transactions.where("transactions.created_at >=?",date)
    if transaction.present?
      dispute = transaction.joins(:dispute_case).count    
      total = transaction.count
      (dispute.to_f/total)*100
    else
      0
    end
  end

  def total_mid_assigned(gateway)
    Location.where('primary_gateway_id =? or secondary_gateway_id =?  or ternary_gateway_id=?', gateway.id,gateway.id,gateway.id).count
  end

  def total_gatyeways_assigned(type)
    total_count = 0
    ids = PaymentGateway.where('type=? and is_deleted=? and name IS Not NULL', PaymentGateway.types[type],false).pluck(:id)
    ids.each do |id|
      l = Location.where('primary_gateway_id =? or secondary_gateway_id =?  or ternary_gateway_id =?', id,id,id).count
      total_count = total_count + l 
    end
    total_count
  end

  def total_cards_trans(type,card_type,transactions)
    if transactions[PaymentGateway.types[type]].present? 
      trans = transactions[PaymentGateway.types[type]].group_by {|tran| tran.card_type == card_type }
      total = trans[true].try(:pluck, :total_amount).try(:sum)
      if total.present?
        total
      else
        0
      end
    else
      0
    end
    #Transaction.joins(order_bank: :payment_gateway).where('payment_gateways.type=? and lower(order_banks.card_type) =?  and transactions.created_at >=? and payment_gateways.is_deleted=? and transactions.main_type IN (?)',PaymentGateway.types[type],card_type,date,false,[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard]).select(:id,:total_amount).try(:pluck,:total_amount).try(:sum)
  end
  def total_sub_cards_trans(type,card_type,sub_type,transactions)
    if transactions[PaymentGateway.types[type]].present?
      trans = transactions[PaymentGateway.types[type]].group_by {|tran| tran.card_type == card_type && tran.card_sub_type == sub_type  }
      total = trans[true].try(:pluck, :total_amount).try(:sum)
      if total.present?
        total
      else
        0
      end
    else
      0
    end
    # Transaction.joins(order_bank: :payment_gateway).where('payment_gateways.type=? and lower(order_banks.card_type)=? and lower(order_banks.card_sub_type) =?  and transactions.created_at >=? and payment_gateways.is_deleted=? and transactions.main_type IN (?)',PaymentGateway.types[type],card_type,sub_type,date,false,[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard]).select(:id,:total_amount).try(:pluck,:total_amount).try(:sum)
  end
end
