module Admins::BuyRateHelper

  def count_merchants(program)
    merchant = []
    locations = program.location_ids
    locations.each do |location|
      merchant << Location.find(location).merchant
    end
    merchant.uniq.count
  end

  def transaction_update(transaction,issue)
    transaction.update(seq_transaction_id: issue[:id],
                       status: "approved",
                       tags: issue[:tags],
                       timestamp: issue[:timestamp]
    )
  end

  def create_transaction_helper_for_monthly_fee(transaction_to, transaction_from, sender, receiver, action,amount,type,sub_type,gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
    Transaction.create(
        to: transaction_to,
        from: transaction_from,
        status: "pending",
        amount: amount,
        sender: sender,
        user_id: sender,
        receiver_id: receiver,
        receiver_wallet_id: transaction_to,
        sender_wallet_id: transaction_from,
        sender_balance: SequenceLib.balance(transaction_from),
        receiver_balance: SequenceLib.balance(transaction_to),
        action: action,
        net_amount: number_with_precision(amount.to_f, precision: 2),
        total_amount: amount.to_f,
        gbox_fee: gbox_fee,
        iso_fee: iso_fee,
        agent_fee: agent_fee,
        affiliate_fee: affiliate_fee,
        main_type: type,
        sub_type: sub_type,
        tags: tags,
        )
  end

end
