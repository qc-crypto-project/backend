module Admins::ReportsHelper

  def baked_gbox_fee(splits, qc_wallet, super_iso = nil)
    sum = 0
    if splits.present?
      splits.each do |split|
        data = split["user_1_details"]
        if data.present?
          if data["from_wallet"] == qc_wallet
            sum = sum + (data["dollar"].to_f + data["percent"].to_f)
          end
        end
      end
    elsif super_iso.present?
      if qc_wallet == super_iso.try(:[], "from_wallet")
        sum = super_iso.try(:[], "dollar").to_f + super_iso.try(:[], "percent").to_f
        if sum < 0
          sum = 0
        end
      end
    end
    sum
  end

  def baked_user_name(user_details, super_iso = nil, super_agent = nil, super_affiliate = nil)
    if user_details.present?
      user1_name=user_details.try(:[],'user_1_details').try(:[],'name')
      user2_name=user_details.try(:[],'user_2_details').try(:[],'name')
      user3_name=user_details.try(:[],'user_3_details').try(:[],'name')
      user1_amount=user_details.try(:[],'user_1_details').try(:[],'dollar').to_f + user_details.try(:[],'user_1_details').try(:[],'percent').to_f
      user2_amount=user_details.try(:[],'user_2_details').try(:[],'dollar').to_f + user_details.try(:[],'user_2_details').try(:[],'percent').to_f
      user3_amount=user_details.try(:[],'user_3_details').try(:[],'dollar').to_f + user_details.try(:[],'user_3_details').try(:[],'percent').to_f
      return{'user1_name'=>user1_name,'user1_amount'=>user1_amount,'user2_name'=>user2_name,'user2_amount'=>user2_amount,'user3_name'=>user3_name,'user3_amount'=>user3_amount}
    elsif super_iso.present? || super_affiliate.present? || super_agent.present?
      user1_name=super_iso.try(:[], "name")
      user2_name= super_agent.try(:[], "name")
      user3_name=super_affiliate.try(:[], "name")
      user1_amount=super_iso.try(:[], "dollar").to_f + super_iso.try(:[], "percent").to_f
      user2_amount=super_agent.try(:[], "dollar").to_f + super_agent.try(:[], "percent").to_f
      user3_amount=super_affiliate.try(:[], "dollar").to_f + super_affiliate.try(:[], "percent").to_f
      if user1_amount.to_f <= 0
        user1_name = nil
        user1_amount = 0
      end
      if user2_amount.to_f <= 0
        user2_name = nil
        user2_amount = 0
      end
      if user3_amount.to_f <= 0
        user3_name = nil
        user3_amount = 0
      end
      return{'user1_name'=>user1_name,'user1_amount'=>user1_amount,'user2_name'=>user2_name,'user2_amount'=>user2_amount,'user3_name'=>user3_name,'user3_amount'=>user3_amount}
    end
  end

  def super_company_split(splits)
    return 0 if splits.blank?
    if splits.include?("super_company")
      splits["super_company"]["amount"]
    end
  end

  def profit_split_gbox(splits, qc_wallet)
    return 0 if splits.blank?
    amount = 0
    if splits["from_wallet"] == qc_wallet
      amount = splits["amount"]
    end
    amount
  end

  def net_profit_gbox(splits, per_transaction_fee)
    return 0 if splits.blank?
    amount = 0
    # pgateway = PaymentGateway.where(name: gateway).last
    # if pgateway.present?
    #   per_transaction_fee = pgateway.per_transaction_fee
    if per_transaction_fee.to_f > 0
      amount = splits["amount"].to_f * per_transaction_fee.to_f/100
    end
    # end
    amount
  end

  def baked_iso_fee(splits, wallet, super_iso = nil)
    sum = 0
    if splits.present?
      splits.each do |split|
        data = split["user_1_details"]
        if data.present?
          if data["from_wallet"] == wallet
            sum = sum + (data["dollar"].to_f + data["percent"].to_f)
          end
        end
      end
    elsif super_iso.present?
      if wallet == super_iso.try(:[], "from_wallet")
        sum = super_iso.try(:[], "dollar").to_f + super_iso.try(:[], "percent").to_f
        if sum < 0
          sum = 0
        end
      end
    end
    sum
  end

  def show_report_total_net(tags, amount, tip, reserve_money, fee)
    #show total net in audit report
    if tags.present?
      if tags.try(:[],"iso").present? && tags.try(:[],"iso").try(:[], "iso_buyrate").present?
        return amount.to_f - tip.to_f - reserve_money.to_f - tags.try(:[], "gbox").try(:[], "amount").to_f
      else
        return amount.to_f - tip.to_f - reserve_money.to_f - fee.to_f
      end
    else
      amount.to_f - tip.to_f - reserve_money.to_f - fee.to_f
    end
  end

  def getting_merchant_report(params,first_date, second_date, params_page=nil, from_worker=nil, tx_filter=nil, from_filter=nil)
    if from_filter == "true"
      if params[:category].present? && params[:category].class == String
        params[:category] = JSON.parse(params[:category])
        if params[:category] == "null"
          params[:category] = ""
        end
      end
      if params[:dba_name].present? && params[:dba_name].class == String
        params[:dba_name] = JSON.parse(params[:dba_name])
        if params[:dba_name] == "null"
          params[:dba_name] = ""
        end
      end
    end
    is_block_loc = params[:status] == "Active" ? false : true
    total_location_size = 0
    dba_checker = false
    details = []

    if params[:dba_name].present? || params[:select_all].present? || params[:category].present?
      wallets = {}
      location_params_data = {}
      if params[:dba_name].present? &&  !params[:dba_name].include?("all")
        dba_checker = true
        params[:dba_name].each do |param|
          temp = JSON.parse(param)
          wallets[temp["id"]] = [temp["name"],temp["category"]]
          location_params_data[temp["id"]] = temp
        end
      elsif params[:select_all].present? || (params[:dba_name].present? && params[:dba_name].include?("all")) || params[:all_location_name].present?
        JSON(params[:all_location_name]).each do |param|
          temp = JSON.parse(param)
          if params[:status].present?
            wallets[temp["id"]] = [temp["name"],temp["category"]] if params[:status] == temp["is_block"]
          else
            wallets[temp["id"]] = [temp["name"],temp["category"]]
          end
          location_params_data[temp["id"]] = temp
        end
      end
      if params[:not_0].present? && params[:not_0] == "true"
        category = []
        if params[:category].present?
          category = ['locations.category_id IN (?)', params[:category]]
        end
        wallets = {}
        puts "*********************************"
        transactions = Transaction.where(created_at: first_date..second_date, main_type: I18n.t('all_sales_types'), status: ["approved", "refunded","complete"]).group_by{|e| e.receiver_wallet_id}.reject{|k,v| v.blank? }
        puts "*********************************",transactions.keys.count
        keys = transactions.keys
        # total_location_size = keys.count
        keys.each do |wallet_id|
          # break if !from_worker && wallets.keys.count == 10
          location = location_params_data[wallet_id]
          if location.present?
            if params[:category].present? && params[:select_all].present?
              if params[:category].include?(location.try(:[], "category_id"))
                wallets[wallet_id] = [location["name"], location["category"]]
              end
            else
              wallets[wallet_id] = [location["name"], location["category"]]
            end
          end
        end
        unless from_worker
          total_location_size = wallets.keys.count
          tx_filter = tx_filter.to_i > 0 ? tx_filter.to_i : 10
          params_page = params_page.to_i > 0 ? params_page.to_i : 1
          limit = params_page * tx_filter
          offset = limit - tx_filter
          new_keys = wallets.keys[offset, limit]
          new_wallets = {}
          new_keys.each do|id|
            break if new_wallets.keys.count == 10
            new_wallets[id] = wallets[id]
          end
          wallets = new_wallets
        end
        wallets_keys = wallets.keys
        start_month = first_date.to_datetime.beginning_of_month
        start_month = DateTime.new(start_month.year, start_month.month, start_month.day, 8,00,00)
        cbk = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: start_month, second: second_date).where(main_type: ["CBK Hold"]).where("sender_wallet_id IN (:id)",id:  wallets_keys).select(:amount_in_cents, :sender_wallet_id, :main_type).group_by{|e|  e.sender_wallet_id}
        puts "###################",cbk.keys.count
        # if from_worker
        #   locations = Location.joins(:wallets, :receiver_transactions).includes(:category).where(category).where("wallets.wallet_type = ?", 0).where("transactions.timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where("transactions.main_type IN (?)",I18n.t('all_sales_types')).select("locations.id,locations.business_name,wallets.id as wallet_id,category_id")
        #   total_location_size = locations.count
        # else
        #   locations = Location.joins(:wallets, :receiver_transactions).includes(:category).where(category).where("wallets.wallet_type = ?", 0).where("transactions.timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where("transactions.main_type IN (?)",I18n.t('all_sales_types')).select("locations.id,locations.business_name,wallets.id as wallet_id,category_id").distinct.per_page_kaminari(params_page).per(tx_filter)
        #   total_location_size = locations.total_count
        # end
        # locations.each do |location|
        #   wallets[location.wallet_id] = [location.business_name, location.category.try(:name)]
        # end
        # wallets_keys = wallets.keys
        dba_checker = true
      else
        total_location_size = wallets.keys.count
        wallets_keys = from_worker ? wallets.keys : Kaminari.paginate_array(wallets.keys).page(params_page).per(tx_filter)
        start_month = first_date.to_datetime.beginning_of_month
        start_month = DateTime.new(start_month.year, start_month.month, start_month.day, 8,00,00)
        cbk = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: start_month, second: second_date).where(main_type: ["CBK Hold"]).where("sender_wallet_id IN (:id)",id:  wallets_keys).select(:amount_in_cents, :sender_wallet_id, :main_type).group_by{|e|  e.sender_wallet_id}
        # cbk = DisputeCase.charge_back.where(merchant_wallet_id: wallets.keys, created_at: start_month..second_date, status: "sent_pending").select(:id,:merchant_wallet_id,:amount).group_by{|e| e.merchant_wallet_id}
        transactions = Transaction.where(receiver_wallet_id: wallets_keys, created_at: first_date..second_date, main_type: I18n.t('all_sales_types'), status: ["approved", "refunded","complete"]).select(:id,:total_amount,:fee,:gbox_fee,:iso_fee,:status,:receiver_wallet_id, :receiver_id, :transaction_fee, :per_transaction_fee,:tags).group_by{|e| e.receiver_wallet_id}  
      end

      wallets.each do |key, value|
        next unless wallets_keys.include?(key)
        fee = gbox_fee = net_gbox = 0
        cbk_count = cbk[key].count if cbk[key].present?
        cbks = cbk[key]
        cbk_total_amount = SequenceLib.dollars(cbks.pluck(:amount_in_cents).try(:sum, &:to_f)) if cbks.present?
        location_name = value.first
        wallet_transactions = transactions[key]

        if wallet_transactions.present?
          gbox_split = 0
          tags = wallet_transactions.pluck(:tags).pluck("fee_perc")
          if tags.present?
            tags.each do |tag|
              if tag["super_company"].present?
                if tag["super_company"]["0"].present?
                  if tag["super_company"]["0"]["from"] == 2
                    gbox_split = gbox_split.to_f + tag["super_company"].values.pluck("amount").try(:sum, &:to_f).to_f
                  end
                elsif tag["super_company"]["from"].present? && tag["super_company"]["from"] == 2
                  gbox_split = gbox_split.to_f + tag["super_company"]["amount"].to_f
                end
              end
              if tag["net_super_company"].present?
                gbox_split = gbox_split.to_f + tag["net_super_company"]["amount"].to_f
              end
              if tag["super_users_details"].present?
                tag["super_users_details"].each do |obj|
                  if obj["user_1_details"]["from_wallet"] == 2
                    gbox_split = gbox_split.to_f + obj["user_1_details"]["dollar"].to_f + obj["user_1_details"]["percent"].to_f
                  end
                end
              end
            end
          end
          tx_count = wallet_transactions.count
          merchant = wallet_transactions.pluck(:receiver_id).uniq.compact.first
          refund_transactions = wallet_transactions.select{|e| e.status == "refunded" || e.status == "complete"}
          refund_count = refund_transactions.count if refund_transactions.present?
          refund_amount = refund_transactions.pluck(:total_amount).try(:sum, &:to_f) if refund_transactions.present?
          total_volume =  wallet_transactions.pluck(:total_amount)
          total_volume = total_volume.try(:sum) if total_volume.present?
          fee = wallet_transactions.pluck(:fee)
          fee = (fee.present? && fee.compact.present?) ? fee.compact.try(:sum) : 0
          gbox_fee = wallet_transactions.pluck(:gbox_fee)
          gbox_fee = gbox_fee.try(:sum) if gbox_fee.present?
          # net_gbox = gbox_fee.to_f - transaction_fee.to_f - per_transaction_fee.to_f
        end
        wallet = Wallet.includes(:location,:users).where(id: key).last
        if merchant.blank?
          merchant = wallet.try(:users).try(:first).try(:id)
        end
        loc_status = wallet.location.is_block
        tx_detail = {
            dba_name: location_name,
            date_range: params[:date],
            m_id: "M-#{merchant}",
            status: loc_status == true ? "Inactive" : "Active",
            category: value.try(:second),
            total_volume: total_volume.to_f,
            total_txn: tx_count.to_i,
            avg_per_txn: tx_count.to_f > 0 ? total_volume.to_f / tx_count.to_f : 0,
            cbk_perc: tx_count.to_f > 0 ? cbk_count.to_f * 100 / tx_count.to_f : 0,
            cbk_count: cbk_count,
            cbk_total_amount: cbk_total_amount,
            refund_perc: tx_count.to_f > 0 ? refund_count.to_f * 100 / tx_count.to_f : 0,
            refund_amount: refund_amount,
            refund_count: refund_count,
            fee: fee,
            gbox_fee: gbox_fee,
            net_gbox: gbox_split,
        }
        details.push(tx_detail)
      end
    end
    if from_worker == true
      {details: details, location: details}
    else
      det = Kaminari.paginate_array(details, total_count: total_location_size).page(params_page).per(tx_filter)
      {details: det, location: det}
    end
  end

  def getting_affiliate_program_report(params , first_date , second_date)
    if params[:affiliate_program].include?"all"
        admin_user = []
        params[:dba_name].each do |param|
          if param != "all"
            temp = JSON.parse(param)
            admin_user.push(temp["id"])
          end
        end
        affiliate_program_ids = User.affiliate_program.where(admin_user_id: admin_user)
    else
      affiliate_program_ids = params[:affiliate_program]
    end
    wallets = Wallet.where(user_id:  affiliate_program_ids)
    transactions = Transaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where(receiver_wallet_id: wallets.ids,main_type: [TypesEnumLib::TransactionType::AFP], status: ["approved", "refunded","complete"]).select(:receiver_wallet_id,:receiver_name,:receiver_id,:total_amount).group_by{|x| x.receiver_wallet_id}
    if params[:aff_not_0].present? 
      params["aff_not_0"] = params[:aff_not_0]
    end
    transaction_group = []
    wallets.each do |wallet|
      tx_details = transactions[wallet.id]
      if tx_details.present?
        tx_detail = {}
        receiver_id = tx_details.pluck(:receiver_id).uniq.first
        receiver_name = tx_details.pluck(:receiver_name).uniq.first
        total_amount = total_count = 0
        affiliate_program = User.affiliate_program.find_by_id(wallet.user_id)
        admin_name = User.admin_user.find_by_id(affiliate_program.admin_user_id).name
        total_count = tx_details.count
        date_range = "#{first_date.to_date.strftime("%m/%d/%Y")} - #{second_date.to_date.strftime("%m/%d/%Y")}"
        total_amount = tx_details.map(&:total_amount).sum
        tx_detail = {
          date_range: date_range,
          admin_name: admin_name,
          affiliate_id: receiver_id,
          affiliate_name: receiver_name,
          total_amount: total_amount,
          total_count: total_count
        }
        transaction_group << tx_detail
      elsif tx_details.blank? && ( params["aff_not_0"].blank? )
        tx_detail = {}
        receiver_id =  wallet.users.first.id
        receiver_name =  wallet.users.first.name
        total_amount = total_count = 0
        affiliate_program = User.affiliate_program.find_by_id(wallet.user_id)
        admin_name = User.admin_user.find_by_id(affiliate_program.admin_user_id).name
        date_range = "#{first_date.to_date.strftime("%m/%d/%Y")} - #{second_date.to_date.strftime("%m/%d/%Y")}"
        tx_detail = {
          date_range: date_range,
          admin_name: admin_name,
          affiliate_id: receiver_id,
          affiliate_name: receiver_name,
          total_amount: total_amount,
          total_count: total_count
        }
        transaction_group << tx_detail
      end
    end
    transaction_group
  end

  def getting_wallet_report(params)
    wallets = {}
    # tx_filter = tx_filter.present? ? tx_filter.to_i : 10
    if params[:dba_name].present?
      if !params[:dba_name].include?("all")
        params[:dba_name].each do |param|
          temp = JSON.parse(param)
          wallets[temp["id"]] = [temp["name"]]
        end
      elsif params[:dba_name].include?("all")
        @locations = Location.select("locations.id,locations.business_name")
        @locations.each do |location|
          wallets[location.id] = [location.business_name]
        end
      end
    elsif params[:partner_name].present?
      if !params[:partner_name].include?("all")
        params[:partner_name].each do |param|
          temp = JSON.parse(param)
          wallets[temp["id"]] = [temp["name"]]
        end
      elsif params[:partner_name].include?("all")
        @locations = User.where(role: [:iso, :agent, :affiliate]).select("users.id,users.name")
        @locations.each do |location|
          wallets[location.id] = [location.name]
        end
      end
    end
    details = []
    wallets.each do |key,value|
      if params[:partner_name].present?
        user = User.joins(:wallets).where(id: key).last
        main_wallet = user.wallets.last
        main_balance = main_wallet.try(:balance).to_f
        merchant = main_wallet.try(:users).try(:first)
        total_balance = main_balance
      elsif params[:dba_name].present?
        location = Location.joins(:wallets,:merchant).where(id: key).last
        main_wallet = location.try(:wallets).try(:primary).try(:last)
        main_balance = main_wallet.try(:balance).to_f
        reserve_wallet = location.try(:wallets).try(:reserve).try(:last).try(:balance).to_f || 0
        tip_wallet_balance = location.try(:wallets).try(:tip).try(:last).try(:balance).to_f
        total_balance = main_balance.to_f + reserve_wallet.to_f + tip_wallet_balance.to_f
        merchant = location.try(:merchant)
      end
      ref_no = set_ref_no(merchant)
      if (params["not_0"].present? && params["not_0"] == "true") && (main_balance.to_f == 0 && reserve_wallet.to_f == 0 && tip_wallet_balance.to_f == 0 )
      else
        tx_detail = {
            m_id: ref_no,
            dba_name: value.first,
            main_wallet_funds: main_balance,
            reserve_wallet_funds: reserve_wallet,
            total: total_balance,
            tip_balance: tip_wallet_balance
        }
        details.push(tx_detail)
      end
    end
    details
  end

  def getting_sale_report(params)
    if params["dba_name"].present? || params["types"].present? || params["select_all"].present? || params["categories"].present?
      wallets = {}
      if params["dba_name"].present? && !params["dba_name"].include?("all")
        params["dba_name"].each do |param|
          temp = JSON.parse(param)
          wallets[temp["id"]] = temp["name"]
        end
      elsif params["dba_name"].present? && params["dba_name"].include?("all")
        locations = Location.joins(:wallets).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id")
        # locations = Location.all.select(:id, :business_name).map{|e| [e.wallets.primary.first.id, e.business_name]}
        locations.each do |location|
          wallets[location.wallet_id] = location.business_name
        end
      elsif params["categories"].present?
        locations = Location.joins(:wallets).where(category_id: params["categories"]).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id")
        # locations = Location.where(category_id: params["categories"]).select(:id, :business_name).map{|e| [e.wallets.primary.try(:first).try(:id), e.business_name] }
        locations.each do |location|
          wallets[location.wallet_id] = location.business_name
        end
      end
      wallets
    end
  end


  def get_fee_type_humanized(key)
    fee_types = { "ACH_deposit_fee" => "ACH",
                  "b2b_transfer" => "B2B",
                  "cbk_fee" => "CBK Fee",
                  "send_check_fee" => "Check",
                  "giftcard_purchase_fee" => "Giftcard",
                  "Misc Fee"=> "Misc Fee",
                  "instant_pay_fee" => "Push To Card",
                  "Subscription fee" => "Subscription fee",
                  "Service fee" => "Monthly fee",
                  "retrievel_fee" => "Retrievel Fee",
                  "refund_fee" => "Refund Fee"}
    my_value = fee_types[key]
    return my_value.present? ? my_value : key
  end

  def getting_withdrawal_report(params, first_date, second_date, parmas_page, tx_filter=nil, run_report_type=nil)
    wallets = {}
    transactions = []
    if params["dba_name"].present?
      params["dba_name"].each do |param|
        temp = JSON.parse(param)
        wallets[temp["id"]] = temp["name"]
      end
    elsif params["select_all"].present? && params[:ach_category].blank?
      locations = Location.joins(:wallets).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id")
      locations.each do |location|
        wallets[location.wallet_id] = location.business_name
      end
    end
    status_types = [t("withdraw.paid"),t("withdraw.unpaid"),t("withdraw.inprocess"), t("withdraw.inprogress"), t("withdraw.void"), t("withdraw.failed"), t("withdraw.pending"), t("withdraw.paid_by_wire")]
    if params[:fee_categories].present?
      if params[:fee_categories].class == String
        params[:fee_categories] = JSON.parse(params[:fee_categories])
      end
      fee_categories = params[:fee_categories]
    else
      fee_categories = ['check', 'instant_ach','instant_pay']
    end
    if params[:status_types].present?
      if params[:status_types].class == String
        params[:status_types] = JSON.parse(params[:status_types])
      end
      status_types = params[:status_types]
    end
    if params[:ach_category].present?
      if params[:ach_category].class == String
        params[:ach_category] = JSON.parse(params[:ach_category])
      end
      ach_category = params[:ach_category]
    end
    cbd_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
    dispensary_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
    if run_report_type == "summary_report"
      if params[:ach_category].present?
        is_ach_international = false
        is_other_present = false
        category_wallet = []
        cbd_dis_wallets = []
        if params[:ach_category].include?(t("withdraw.ACH_international"))
          params[:ach_category] -= %w{ACH_International}
          is_ach_international = true
        end
        if params[:ach_category].include?(t("withdraw.CBD"))
          category_wallet = cbd_wallets
        end
        if params[:ach_category].include?(t("withdraw.dispensary"))
          category_wallet = category_wallet + dispensary_wallets
        end
        if params[:ach_category].include?(t("withdraw.other"))
          cbd_dis_wallets = cbd_wallets + dispensary_wallets
          is_other_present = true
        end
        category_wallet_check = nil
        if is_ach_international
          params[:ach_category] =  params[:ach_category] + ["ACH_International"]
        end
        if category_wallet.present?
          category_wallet_check = true
        else
          category_wallet_check = false
        end
      end
      condition = wallets.present? ? {wallet_id: wallets.keys, order_type: fee_categories, status: status_types} : params[:ach_category].present? || is_ach_international ? {wallet_id: category_wallet, order_type: fee_categories, status: status_types} : {order_type: fee_categories, status: status_types}

      if is_other_present
        if is_ach_international && category_wallet_check == true
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where("wallet_id IN (?) OR ach_international = ? OR NOT wallet_id IN (?)", category_wallet, true,cbd_dis_wallets).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil)
                        .order(batch_date: :desc)
        elsif is_ach_international && category_wallet_check == false
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where("ach_international = ? OR NOT wallet_id IN (?)", true, cbd_dis_wallets).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil)
                        .order(batch_date: :desc)
        elsif is_ach_international == false && category_wallet_check == true
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where("wallet_id IN (?) OR NOT wallet_id IN (?)",category_wallet, cbd_dis_wallets ).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil, ach_international: true)
                        .order(batch_date: :desc)
        else
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where.not(wallet_id: cbd_dis_wallets).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil, ach_international: true)
                        .order(batch_date: :desc)
        end
      else
        if is_ach_international && category_wallet_check == true
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where("wallet_id IN (?) OR ach_international = ?", category_wallet, true).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil)
                        .order(batch_date: :desc)
        elsif is_ach_international && category_wallet_check == false
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where(ach_international: true).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil)
                        .order(batch_date: :desc)
        elsif is_ach_international == false
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where(wallet_id: category_wallet).where(order_type: "instant_ach", status: status_types)
                        .where.not(transaction_id:nil, ach_international: true)
                        .order(batch_date: :desc)
        else
          batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                        .where(condition)
                        .where.not(transaction_id:nil)
                        .order(batch_date: :desc)
        end
      end
      ach_batches = batches.group_by_day(day_start: 22) {|u| u.batch_date }
      ach_batches = ach_batches.sort.reverse.to_h
      total_records=[]
      if ach_batches.present?
        ach_batches.each do |batch_key, batch_value|
          grouped_batch = batch_value.group_by(&:order_type)
          checks_total_amount = 0
          ach_total_amount = 0
          instant_pay_total_amount = 0
          ach_cbd_total_amount = 0
          ach_dispensary_total_amount = 0
          ach_international_amount = 0
          ach_international_wallets = []
          grouped_batch.each do |nested_batch_key, nested_batch_value|
            if nested_batch_key == "check"
              checks_total_amount = nested_batch_value.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
            elsif nested_batch_key == "instant_ach"
              international_achs = nested_batch_value.select{|c| c.ach_international == true}
              ach_international_amount = international_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
              nested_batch_value = nested_batch_value.reject { |p| p.ach_international == true } if nested_batch_value.present?
              dispensary_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
              cbd_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
              dispensary_achs = nested_batch_value.select{|c| dispensary_wallets.include?(c.wallet_id)}
              ach_dispensary_ids = dispensary_achs.map{|c| c.id }
              ach_dispensary_total_amount = dispensary_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
              cbd_achs = nested_batch_value.select{|c| cbd_wallets.include?(c.wallet_id)}
              ach_cbd_ids = cbd_achs.map{|c| c.id }
              ach_cbd_funding_amount = cbd_achs.map{|c| c.actual_amount if c.pending? }.compact.inject(0){|sum,x| sum + x }
              ach_cbd_total_amount = cbd_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
              wallets = dispensary_wallets + cbd_wallets
              non_dispensary_achs = nested_batch_value.select{|c| !wallets.include?(c.wallet_id)}
              instant_ach_ids = non_dispensary_achs.map{|c| c.id }
              ach_total_amount = non_dispensary_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
            elsif nested_batch_key == "instant_pay"
              ach_ids = nested_batch_value.map{|c| [c.id, c.status] }
              instant_pay_total_amount = nested_batch_value.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
            end
          end
          total_of_row=ach_cbd_total_amount.to_f + ach_total_amount.to_f + ach_dispensary_total_amount.to_f + instant_pay_total_amount.to_f + checks_total_amount.to_f + ach_international_amount.to_f

          total_records.push(['Date'=>batch_key,'cbd_total'=>ach_cbd_total_amount,'other_ach'=>ach_total_amount,'ach_international'=>ach_international_amount,'dispensary_total'=>ach_dispensary_total_amount,'instant_pay'=>instant_pay_total_amount,'checks_total'=>checks_total_amount,'row_total'=>total_of_row])
        end
      end
      total_records=total_records.flatten if total_records.present?
      cbd_total= total_records.pluck(:[],"cbd_total").flatten.compact.sum
      other_ach= total_records.pluck(:[],"other_ach").flatten.compact.sum
      ach_international= total_records.pluck(:[],"ach_international").flatten.compact.sum
      dispensary_total= total_records.pluck(:[],"dispensary_total").flatten.compact.sum
      instant_pay= total_records.pluck(:[],"instant_pay").flatten.compact.sum
      checks_total= total_records.pluck(:[],"checks_total").flatten.compact.sum
      row_total= total_records.pluck(:[],"row_total").flatten.compact.sum
      total_records.push('Date'=>"Total", 'cbd_total'=>number_with_precision(number_to_currency(cbd_total), precision: 2), 'other_ach'=>number_with_precision(number_to_currency(other_ach), precision: 2), 'ach_international'=>number_with_precision(number_to_currency(ach_international), precision: 2), 'dispensary_total'=>number_with_precision(number_to_currency(dispensary_total), precision: 2), 'instant_pay'=>number_with_precision(number_to_currency(instant_pay), precision: 2), 'checks_total'=>number_with_precision(number_to_currency(checks_total), precision: 2) , 'row_total'=>number_with_precision(number_to_currency(row_total), precision: 2))
      det = Kaminari.paginate_array(total_records).page(parmas_page).per(tx_filter)
      {transactions: det,original: det}
    else
      if wallets.present?
        tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                          .where(wallet_id: wallets.keys, order_type: fee_categories, status: status_types)
                          .where.not(transaction_id:nil).per_page_kaminari(parmas_page).per(tx_filter)
        tango_orders = tango_order.group_by(&:user_id)

      elsif params[:ach_category].present?
        is_ach_international = false
        is_other_present = false
        category_wallet = []
        cbd_dis_wallets = []
        if params[:ach_category].include?(t("withdraw.ACH_international"))
          params[:ach_category] -= %w{ACH_International}
          is_ach_international = true
        end
        if params[:ach_category].include?(t("withdraw.CBD"))
          category_wallet = cbd_wallets
        end
        if params[:ach_category].include?(t("withdraw.dispensary"))
          category_wallet = category_wallet + dispensary_wallets
        end
        if params[:ach_category].include?(t("withdraw.other"))
          cbd_dis_wallets = cbd_wallets + dispensary_wallets
          is_other_present = true
        end
        category_wallet_check = nil
        if is_ach_international
          params[:ach_category] =  params[:ach_category] + ["ACH_International"]
        end
        if category_wallet.present?
          category_wallet_check = true
        else
          category_wallet_check = false
        end
        if is_other_present
          if is_ach_international && category_wallet_check == true
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where("wallet_id IN (?) OR ach_international = ? OR NOT wallet_id IN (?)", category_wallet, true,cbd_dis_wallets).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil).per_page_kaminari(parmas_page).per(tx_filter)
          elsif is_ach_international && category_wallet_check == false
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where("ach_international = ? OR NOT wallet_id IN (?)", true, cbd_dis_wallets).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil).per_page_kaminari(parmas_page).per(tx_filter)
          elsif is_ach_international == false && category_wallet_check == true
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where("wallet_id IN (?) OR NOT wallet_id IN (?)",category_wallet, cbd_dis_wallets ).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil, ach_international: true).per_page_kaminari(parmas_page).per(tx_filter)
          else
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where.not(wallet_id: cbd_dis_wallets).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil, ach_international: true).per_page_kaminari(parmas_page).per(tx_filter)
          end
        else
          if is_ach_international && category_wallet_check == true
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where("wallet_id IN (?) OR ach_international = ?", category_wallet, true).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil).per_page_kaminari(parmas_page).per(tx_filter)
          elsif is_ach_international && category_wallet_check == false
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where(ach_international: true).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil).per_page_kaminari(parmas_page).per(tx_filter)
          else
            tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                              .where(wallet_id: category_wallet).where(order_type: "instant_ach", status: status_types)
                              .where.not(transaction_id:nil, ach_international: true).per_page_kaminari(parmas_page).per(tx_filter)
          end
        end
        tango_orders = tango_order.group_by(&:user_id)
      else
        tango_order = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                          .where(order_type: fee_categories, status: status_types)
                          .where.not(transaction_id:nil).per_page_kaminari(parmas_page).per(tx_filter)
        tango_orders = tango_order.group_by(&:user_id)
      end
      dispensary_wallets_ca = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
      cbd_wallets_ca = Wallet.joins(location: :category).where("categories.name ILIKE '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
      actual_result = []
      if tango_orders.present?
        tango_orders.each do |key, value|
          data = {}
          merchant = User.find_by(id: key)
          ref_no = set_ref_no(merchant)
          data[:merchant] = "#{merchant&.parent_merchant&.name || merchant&.name}"
          data[:user_id] = "#{ref_no}"
          # batches_with_location = value.group_by(&:location_id)
          batches_with_location = value.group_by(&:wallet_id)

          batches_with_location.each do |location_id, location_tango_orders|
            total_amount = 0
            fee = 0
            amount_sent = 0
            gbox_fee = 0
            iso_fee = 0
            agent_fee = 0
            affiliate_fee = 0
            location_wallet = Wallet.find_by(id: location_id)
            location = location_wallet.try(:location)
            data[:wallet_id] = location_wallet.try(:id)
            data[:location_name] = location.present? ? location.try(:business_name) : location_wallet.try(:name)
            data[:location_category] = location.category[:name] if location.present? && location.category.present?
            location_tango_orders.each do|tango|
              if tango.order_type == "check"
                data[:withdrawal_category] = "Check"
              elsif tango.order_type == "instant_pay"
                data[:withdrawal_category] = "Push To Card"
              elsif tango.order_type == "instant_ach"
                if tango.ach_international == true
                  data[:withdrawal_category] = "ACH International"
                elsif dispensary_wallets_ca.include?(tango.wallet_id)
                  data[:withdrawal_category] = "ACH Dispensary"
                elsif cbd_wallets_ca.include?(tango.wallet_id)
                  data[:withdrawal_category] = "ACH CBD"
                else
                  data[:withdrawal_category] = "ACH Other"
                end
              end
              data[:tango_id] = tango.id
              data[:type] = tango.instant_pay? ? "Push To Card" : tango.instant_ach? ?  "ACH" : tango.order_type.try(:capitalize)
              data[:bank] = AchGateway.find(tango.ach_gateway_id).try(:bank_name) if tango.ach_gateway_id.present?
              data[:date] = tango.created_at.strftime("%m/%d/%Y")
              data[:completed_date] = tango.batch_date.strftime("%m/%d/%Y")
              data[:status] = tango.status
              data[:note] = tango.description
              transaction = tango.transaction
              if transaction.present?
                data[:total_amount] = transaction.present? ? transaction.total_amount : 0
                data[:fee] = transaction.present? ? transaction.net_fee : 0
                data[:amount_sent] = transaction.present? ? transaction.net_amount : 0
                data[:gbox_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"gbox").to_f : 0
                data[:iso_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"iso").to_f : 0
                data[:agent_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"agent").to_f : 0
                data[:affiliate_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"affiliate").to_f : 0

                total_amount = data[:total_amount] + total_amount
                fee = (data[:fee].present? ? data[:fee] : 0) + fee
                amount_sent = data[:amount_sent] + amount_sent
                gbox_fee = data[:gbox_fee] + gbox_fee
                iso_fee = data[:iso_fee] + iso_fee
                agent_fee = data[:agent_fee] + agent_fee
                affiliate_fee = data[:affiliate_fee] + affiliate_fee
                actual_result.push(data)
              end
              data = {}
            end
            data[:total] = "Total"
            data[:total_amount] = total_amount
            data[:fee] = fee
            data[:amount_sent] = amount_sent
            data[:gbox_fee] = gbox_fee
            data[:iso_fee] = iso_fee
            data[:agent_fee] = agent_fee
            data[:affiliate_fee] = affiliate_fee
            actual_result.push(data)
            data = {}
          end
        end

        withdraw_details = []
        if actual_result.present?
          actual_result.each do |report|
            if report.try(:[],:merchant).present?
              details = {
                  merchant: report[:merchant],
                  user_id: report[:user_id],
                  DBA_name: report[:location_name],
                  Category: report[:location_category],
                  wallet_id: report[:wallet_id],
                  tango_id: report[:tango_id],
                  type: report[:type],
                  withdrawal_category: report[:withdrawal_category],
                  bank: report[:bank],
                  created_date: report[:date],
                  completed_date: report[:completed_date],
                  status: report[:status],
                  total_amount: number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  fee: number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  amount_sent: number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  gbox_fee: number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  iso_fee: number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  agent_fee: number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  aff_fee: number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  notes: report[:note]
              }
            elsif report.try(:[],:merchant).nil? && report.try(:[],:location_name).present?
              details = {
                  merchant: "",
                  user_id: "",
                  DBA_name: report[:location_name],
                  Category: report[:location_category],
                  wallet_id: report[:wallet_id],
                  tango_id: report[:tango_id],
                  type: report[:type],
                  withdrawal_category: report[:withdrawal_category],
                  bank: report[:bank],
                  created_date: report[:date],
                  completed_date: report[:completed_date],
                  status: report[:status],
                  total_amount: number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  fee: number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  amount_sent: number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  gbox_fee: number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  iso_fee: number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  agent_fee: number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  aff_fee: number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  notes: report[:note]
              }
            elsif report.try(:[],:merchant).nil? && report.try(:[],:location_name).nil? && report.try(:[],:total).nil?
              details = {
                  merchant: "",
                  user_id: "",
                  DBA_name: "",
                  Category: "",
                  wallet_id: report[:wallet_id],
                  tango_id: report[:tango_id],
                  type: report[:type],
                  withdrawal_category: report[:withdrawal_category],
                  bank: report[:bank],
                  created_date: report[:date],
                  completed_date: report[:completed_date],
                  status: report[:status],
                  total_amount: number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  fee: number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  amount_sent: number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  gbox_fee: number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  iso_fee: number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  agent_fee: number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  aff_fee: number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  notes: report[:note]
              }
            elsif report.try(:[],:total).present?
              details = {
                  merchant: "",
                  user_id: "",
                  DBA_name: "",
                  Category: "",
                  wallet_id: "",
                  tango_id: "",
                  type: "",
                  withdrawal_category: "",
                  bank: "",
                  created_date: "",
                  completed_date: "",
                  total: report[:total],
                  total_amount: number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  fee: number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  amount_sent: number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  gbox_fee: number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  iso_fee: number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  agent_fee: number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  aff_fee: number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  notes: ""
              }
            end
            withdraw_details.push(details)
          end
          # end
        end
      end
      {transactions: withdraw_details,original: tango_order}
    end
  end

  def set_ref_no(user)
    if user.present?
      if user.user?
        return "C-#{user.id}"
      elsif user.agent?
        return "A-#{user.id}"
      elsif user.affiliate?
        return "AF-#{user.id}"
      elsif user.iso?
        return "ISO-#{user.id}"
      elsif user.merchant? && user.merchant_id.present?
        return "M-#{user.merchant_id}"
      elsif user.merchant? && user.merchant_id.nil?
        return "M-#{user.id}"
      end
    end
  end
end