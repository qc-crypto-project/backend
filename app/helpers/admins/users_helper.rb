module Admins::UsersHelper

  def set_params
    params[:user] = I18n.t("role.type.customer") if current_user.support_mtrac? && [I18n.t("role.type.companies"), I18n.t("role.type.ledger_wallets"), I18n.t("role.type.greenbox_user")].include?(params[:user])
    params[:user] = I18n.t("role.type.customer") if params[:user].blank?
    params[:user] = I18n.t("role.type.customer") unless [I18n.t("role.type.companies"), I18n.t("role.type.ledger_wallets"), I18n.t("role.type.greenbox_user"), I18n.t("role.type.customer"), I18n.t("role.type.isos"), I18n.t("role.type.agents"), I18n.t("role.type.affiliates") , I18n.t("role.type.admin_users") , I18n.t("role.type.affiliate_programs")].include? params[:user]
  end

  def get_users
    case params[:user]
    when "companies"
      @company = Company.new
      @companies = Company.all.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10) if !current_user.support_mtrac?
    when "affiliate_programs"
      if params[:admin_user].present?
        @users = User.search_users_with_role(params[:user]).where(admin_user_id: params[:admin_user]).unarchived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      else
        @users = User.search_users_with_role(params[:user]).unarchived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      end
    else
      @users = User.search_users_with_role(params[:user]).unarchived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
    end
  end

  def get_users_with_advance_search
    if params[:admin_user].present?
      @users = search(params).where(admin_user_id: params[:admin_user]).where.not(role:[I18n.t("role.name.merchant"), I18n.t("role.name.gift_card"), I18n.t("role.name.qc"), I18n.t("role.name.support")]).unarchived_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
    elsif params[:user] == "ledger_wallets"
      @users = search(params).unarchived_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
    else
      @users = search(params).unarchived_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
    end
  end

  def search_users(params)
    params[:q] = params[:q].try(:strip) if params[:q].present?
    if params["archived"] == "true"
      @users = User.search_users_with_role(params[:user]).where("id = :id OR ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value OR phone_number ILIKE :value OR company_name ILIKE :value",  value: "%#{params[:q]}%", id: params[:q].to_i).archived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
    else
      if [I18n.t("role.type.all"), I18n.t("role.type.pending"), I18n.t("role.type.approved")].include? (params[:req])
        user_status = params[:req] == "pending" ? {is_block: true} : params[:req] == "pending" ? {is_block: false} : {}
        @reqs = User.where("id = :id OR name ILIKE :value OR email ILIKE :value OR phone_number ILIKE :value",  value: "%#{params[:q]}%", id: params[:q].to_i).where(user_status).order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:user] == I18n.t("role.type.companies")
        @company = Company.new
        @companies = Company.where("id = :id OR ref_no ILIKE :value OR name ILIKE :value OR category ILIKE :value OR phone_number ILIKE :value",  value: "%#{params[:q]}%", id: params[:q].to_i).order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      else
        if params[:admin_user].present?
          @users = User.search_users_with_role(params[:user]).where(admin_user_id: params[:admin_user]).where("id = :id OR ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value OR phone_number ILIKE :value OR company_name ILIKE :value",  value: "%#{params[:q]}%", id: params[:q].to_i).unarchived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
        else
          @users = User.search_users_with_role(params[:user]).where("id = :id OR ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value OR phone_number ILIKE :value OR company_name ILIKE :value",  value: "%#{params[:q]}%", id: params[:q].to_i).unarchived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
        end
      end
    end
  end


  def get_title(params)
    params[:user] == I18n.t("role.type.isos") ? "All ISOs" : params[:user] == I18n.t("role.type.affiliate_programs") ? "All Affiliates" : "All #{params[:user].try(:titleize).try(:pluralize)}" 
  end

  def get_button(params)
    arr = []
    if [I18n.t("role.type.customer"), I18n.t("role.type.isos"), I18n.t("role.type.agents"), I18n.t("role.type.affiliates"),I18n.t("role.type.greenbox_user"),I18n.t("role.type.admin_users")].include?(params[:user])
      arr.push(link_to("Archived Users", archived_users_users_path(user: I18n.t("role.type.#{params[:user]}"), archive_user: I18n.t("role.type.#{params[:user]}")), :class => "btn btn-warning"))
    end
    if I18n.t("role.type.isos") == params[:user]
      arr.push(link_to('New ISO', new_iso_path(:count=>1,:id=>"#{I18n.t("role.type.#{params[:user]}").singularize}_signup"), class:'btn btn-primary')) unless current_user.support_mtrac?
    end
    if I18n.t("role.type.agents") == params[:user]
      arr.push(link_to('New Agent', new_agent_path(:count=>1,:id=>"#{I18n.t("role.type.#{params[:user]}").singularize}_signup"), class:'btn btn-primary')) unless current_user.support_mtrac?
    end
    if I18n.t("role.type.affiliates") == params[:user]
      arr.push(link_to('New Affiliate', new_affiliate_path(:count=>1,:id=>"#{I18n.t("role.type.#{params[:user]}").singularize}_signup"), class:'btn btn-primary')) unless current_user.support_mtrac?
    end
    if I18n.t("role.type.admin_users") == params[:user]
      arr.push(link_to('New Adim User', new_admins_admin_user_path(count: 1, id: "admin_user_signup") , class:'btn btn-primary')) unless current_user.support_mtrac?
    end
    if I18n.t("role.type.affiliate_programs") == params[:user]
        arr.push(link_to("Archived Users", archived_users_users_path(user: I18n.t("role.type.#{params[:user]}"), archive_user: I18n.t("role.type.#{params[:user]}") , admin_user: params[:admin_user]), :class => "btn btn-warning"))
      arr.push(link_to('New Affiliate', new_admins_affiliate_program_path(admin_user: params[:admin_user],count: 1, id: "affiliate_program_signup") , class:'btn btn-primary')) unless current_user.support_mtrac?
    end

    if params[:user] == I18n.t("role.type.greenbox_user")
          arr.push(link_to('Add User', "#", :class => 'btn btn btn-primary show-cover-spin show-dynamic-modal', "data-modal_container_selector" => ".in", "data-modal_selector" => "#greenbox_user",  "data-url" => new_user_path, "data-method" => "GET" ))
          # arr.push(link_to'Add User', "#" , class: 'btn btn-sm btn-primary show-cover-spin show-dynamic-modal',data: { "modal_selector" => "#greenbox_user", "modal_container_selector" => ".in", method: "GET", "url" =>  new_user_path }, remote: true )
    elsif params[:user] == I18n.t("role.type.companies")
      arr.push('<button type="button" class="btn btn-primary pull-right addcompany" data-toggle="modal" data-target=".myModal text-capitalize">New Company</button>')
    end
    arr.join('&nbsp').html_safe
  end

  def search(params)
    @key = nil
    if params[:query].present?
      if params[:query]["name"].present?
        @key = "name ILIKE :name"
      end
      if params[:query]["email"].present?
        if @key.nil?
          @key = "email ILIKE :email"
        else
          @key += " Or email ILIKE :email"
        end
      end
      if params[:query]["phone_number"].present?
        if @key.nil?
          @key = "phone_number ILIKE :phone_number"
        else
          @key += " Or phone_number ILIKE :phone_number"
        end
      end
      if params[:query]["ref_no"].present?
        if @key.nil?
          @key = "ref_no ILIKE :ref_no"
        else
          @key += " Or ref_no ILIKE :ref_no"
        end
      end
      if params[:from_tickets].present?
        @key = nil
        if params[:query]["name"].present?
          @key = "name ILIKE :name"
        end
        if params[:query]["ref_no"].present?
          if @key.nil?
            @key = "ref_no ILIKE :ref_no"
          else
            @key += " AND ref_no ILIKE :ref_no"
          end
        end
      end
      if params[:query]["wallet_id"].present?
        if @key.nil?
          @key = "wallets.id = :wallet_id"
        else
          @key += " OR wallets.id = :wallet_id"
        end
      end
      if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present? || params[:query]["last4"].present? || params[:query]["wallet_id"].present?
        if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present? || params[:query]["wallet_id"].present?
          if params[:user] == "customer" || params[:user] == "isos" || params[:user] == "agents" || params[:user] == "affiliates" || params[:user] == "ledger_wallets"
            @users = User.all_users.eager_load(:users_wallets).search_all_users(@key, params[:query])
          else
            @users = User.search_users_with_role(params[:user]).eager_load(:users_wallets).search_all_users(@key,params[:query])
          end
        end
        if params[:query]["last4"].present?
          # @users = User.joins(:cards).where("cards.last4 ILIKE ? and cards.archived =?","%#{params[:query]["last4"].strip}%",false)
          @cards = Card.where("last4 ILIKE ? and archived = ?","%#{params[:query]['last4']}%",false).pluck(:user_id).uniq
          if @users.blank?
            @users = User.search_users_with_role(params[:user]).where(id: @cards)
          end
        end
      end
    end
    return @users
  end

end
