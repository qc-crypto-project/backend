module Admins::AppConfigsHelper
  def merchant_checks_enabled_emails
    users = User.notify_user
    users.each do |merchant|
      text = I18n.t('notifications.sys_maintenance_notif')
      if !merchant.parent_merchant
        if merchant.merchant? && merchant.oauth_apps.first.present? && merchant.oauth_apps.first.is_block == false
          Notification.notify_user(merchant,merchant,merchant,"system maintenance",nil,text)
          UserMailer.checks_enabled(merchant).deliver_later
        elsif merchant.is_block.present? && merchant.is_block == false
          Notification.notify_user(merchant,merchant,merchant,"system maintenance",nil,text)
          UserMailer.checks_enabled(merchant).deliver_later
        end
      end
    end
  end
  def system_maintenance_on_email
    users = User.notify_user
    users.each do |merchant|
      if !merchant.parent_merchant
        # text = I18n.t('notifications.sys_maintenance_on', name: merchant.name)
        # Notification.notify_user(merchant,merchant,merchant,"system maintenance",nil,text)
        if merchant.merchant? && merchant.oauth_apps.first.present? && merchant.oauth_apps.first.is_block == false
          UserMailer.system_maintenance_on(merchant).deliver_later
        elsif merchant.is_block.present? && merchant.is_block == false
          UserMailer.system_maintenance_on(merchant).deliver_later
        end
      end
    end
  end


  def get_load_balancer_status(load_balancer)
    if load_balancer.active
      {status: "Active", color: "active_div"}
    else
      {status: "Inactive", color: "inactive_div"}
    end
  end
end
