module Admins::EmailsHelper
  require 'sendgrid-ruby'
  include SendGrid

  def get_sender_email(key)
    sender_info = EmailNotification.sender_info[key.to_sym]
    return sender_info[:signature]
  end

  def notification_email(batch_id,emails, attachments=nil,search_option=nil)
    emails = emails.uniq{ |s| s.second }
    email_notification = EmailNotification.find_by(id: batch_id)
    subject = email_notification.subject
    sender = email_notification.sender
    sender_info = EmailNotification.sender_info[sender.to_sym]
    from = sender_info[:email]
    no_reply_from = from.split("@")
    no_reply_from[1],no_reply_from[2] = ".noreply@",no_reply_from[1]
    no_reply_from = no_reply_from.join
    signature = sender_info[:signature]
    message_ids = []
    sent = {success: false}
    if emails.present?
      if ENV['EMAIL_BATCH_SIZE'].present?
        if ENV['EMAIL_BATCH_SIZE'].to_i < 999 && ENV['EMAIL_BATCH_SIZE'].to_i > 0
          batch_size = ENV['EMAIL_BATCH_SIZE'].to_i
        else
          batch_size = 998
        end
      else
        batch_size = 998
      end
      emails.each_slice(batch_size) do |emails|
        ahoy_emails = []
        personalization = Personalization.new
        personalization.subject = subject
        emails.each do |e|
          if e.fourth == "merchant"
            user_name = "M-#{e.third}"
          elsif e.fourth == "iso"
            user_name = "ISO-#{e.third}"
          elsif e.fourth == "agent"
            user_name = "A-#{e.third}"
          elsif e.fourth == "affiliate"
            user_name = "AF-#{e.third}"
          elsif e.fourth == "user"
            user_name = "C-#{e.third}"
          else
            user_name = "#{e.third}"
          end
          ahoy_emails.push(EmailMessage.new(user_id: e.first, mailer: "UserMailer#notification_email", to: e.second, email_notification_id:email_notification.id, status:'processed',user_name: user_name))
          personalization.add_bcc(Email.new(email: e.second))  if e.second.present? && e.second.include?('@')
        end
        sent = sending_email_batches(email_notification, personalization, attachments, subject, from, no_reply_from, signature)
        unless sent[:success]
          return sent
        else
          message_ids.push(sent[:message_id])
          ahoy_emails.select! { |e| e.message_id = sent[:message_id] }
          EmailMessage.import ahoy_emails
        end
      end
      if sent[:success]
        email_notification.update(message_id: message_ids.join(','))
      else
        email_notification.images.delete_all
        email_notification.email_messages.delete_all
        email_notification.delete
      end
    end
    sent
  end

  def sending_email_batches(email_notification, personalization, attachments, subject, from, no_reply_from, signature)
    mail = SendGrid::Mail.new
    mail.from = Email.new(email: no_reply_from)

    mail.subject = subject

    signature_body=template_design
    mail.add_content(Content.new(type: 'text/html', value: "<html><body>#{signature_body.first.html_safe}<br><br>#{email_notification.body.html_safe}<br><br><h4>Sincerely,</h4><p>#{signature}</p><p>www.GreenBoxPos.com</p><h4>Contact Us:</h4><p>Email: #{from}</p><p>Phone: 619-631-4838 9am-5pm (Monday - Friday) Just ask to speak with someone in Support!</p>#{signature_body.last.html_safe}</body></html>"))

    if attachments.present?
      attachments.each do |image|
        attachment = Attachment.new
        # attachment.content = "https:#{image.add_image.url}"
        attachment.content = Base64.strict_encode64(open("https:#{image[:url]}").read)
        attachment.type = image[:type]
        attachment.filename = image[:name]
        attachment.disposition = 'attachment'
        mail.add_attachment(attachment)
      end
    end


    personalization.add_to(Email.new(email: from))
    mail.add_personalization(personalization)

    sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'], host: ENV['SENDGIRD_URL'])
    response = sg.client.mail._('send').post(request_body: mail.to_json)
    if response.body.present?
      message = "Something went wrong. Please try again later!"
      begin
        message = JSON(response.try(:body)).try(:[],"errors").try(:first).try(:[],"message")
      rescue => ex
        message = ex.message
      end
      return {success: false,message: message}
    else
      if response.try(:headers).try(:[],"x-message-id").try(:first).present?
        return {success:true, message: "Email notification sent successfully!", message_id: response.try(:headers).try(:[],"x-message-id").try(:first)}
      else
        return {success: false, message: message}
      end
    end
  end

  def template_design
    footer_body="<p>
              © Copyright, 2019, GreenBox 8880 Rios San Diego Drive, San Diego, CA 92108</p><br>
              <p style='text-align: center;'> This notification is sent to you because you are a customer or subscriber of Quickcard</p>"
    header_body = '
    <style>
      .padding-top-15{
        padding-top: 15% !important;
      }
      .padding-top-15{
        padding-top: 1% !important;
      }
      .large-font {
        font-size: 2.0rem!important;
      }
      .text-center{
        text-align:center;
      }
      .heading-font{
        color: #fff !important;
        font-weight: normal !important;
        font-size: 1.8rem !important;
      }
      .heading-font-head{
        color: #fff !important;
        font-weight: normal !important;
        font-size: 2.0rem !important;
      }
      .medium-font{
        font-size: 2.0rem!important;
      }
      </style>
        <tr>
        <td>
          <div style="background-image: url(http://54.185.13.30/qcdesign/img/email_header_bg_px.png) !important; background-repeat:repeat-x; ">
            <div style="background-image: url(http://54.185.13.30/qcdesign/img/email_header_bg_sub.png) !important; background-repeat: no-repeat;">
              <div class="padding-top-15 text-center large-font" style="color: white"><b>GreenBox POS</b></div>
              <div class="heading-font padding-top-2 padding-bottom-15 text-center medium-font"><b></b></div>
            </div>
          </div>
          <div email_header_bg_bottom_shadow.png style="background-image: url(http://54.185.13.30/qcdesign/img/email_header_bg_bottom_shadow.png) !important; background-repeat:repeat-x;">
            <div style="background-image: url(http://54.185.13.30/qcdesign/img/email_header_bg_bottom.png) !important; height: 23px;background-repeat: no-repeat;"></div>
          </div>
        </td>
      </tr>'
    return [header_body,footer_body]
  end

  def call_worker(params,schedule_date=nil)
    notification = EmailNotification.create(subject: params[:subject], body: params[:body], schedule_date: schedule_date, search_option: params[:users], sender: params[:sender],category_id: params[:category],status: "proccessing")
    message = ""
    if params[:attachments].present? && notification.present?
      params[:attachments].each do|v|
        result = save_image(v,notification.id)
        unless result[:success]
          return {success: false, message: "Invalid attachment! #{v.original_filename}"}
        end
      end
    end

    if schedule_date.present?
      if Time.now.utc >= schedule_date
        EmailSendingWorker.perform_async(notification.id, schedule_date)
        message = "Please wait! Emails are being sending."
      else
        notification.update(status: "pending")
        Sidekiq::Cron::Job.create(name: "EmailNotification#{notification.id}", cron: "#{schedule_date.to_datetime.minute} #{schedule_date.hour} #{schedule_date.day} #{schedule_date.month} *", class: EmailNotificationWorker)
        message = "Email notification sent successful"
      end
    else
      EmailSendingWorker.perform_async(notification.id, schedule_date)
      message = "Please wait! Emails are being sending."
    end
    return {success: false, message: message}
  end

  def reschedule_call_worker(params,id,schedule_date=nil)
    return_result = {success: false, message: "Something went wrong!"}
    notification = EmailNotification.find_by(id: id)
    if notification.present?
      attachments_url = []
      if schedule_date.present?
        if Time.now.utc >= schedule_date
          EmailSendingWorker.perform_async(notification.id, schedule_date)
          notification.update(subject: params[:subject], body: params[:body], schedule_date: schedule_date, sender: params[:sender],status: "proccessing")
          Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}"
        else
          notification.update(subject: params[:subject], body: params[:body], schedule_date: schedule_date, sender: params[:sender],status: "pending")
          # EmailNotificationWorker.perform_at(@schedule_date,notification.id)
          Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}" if notification.present?
          Sidekiq::Cron::Job.create(name: "EmailNotification#{notification.id}", cron: "#{schedule_date.to_datetime.minute} #{schedule_date.hour} #{schedule_date.day} #{schedule_date.month} *", class: EmailNotificationWorker)
          message = "Email notification sent successful"
        end
      else
        EmailSendingWorker.perform_async(notification.id, schedule_date)
        notification.update(subject: params[:subject], body: params[:body], schedule_date: schedule_date, sender: params[:sender],status: "proccessing")
        Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}"
      end
      return_result = {success: true, message: message}
    end
    return_result
  end

  def save_image(image, notification_id)
    old_doc= Image.where(email_notification_id: notification_id, add_image_file_name: image.original_filename,add_image_content_type: image.content_type).last if notification_id.present?
    old_doc.delete if old_doc.present?
    doc= Image.new(add_image: image, email_notification_id: notification_id)
    if doc.validate
      doc.save
      {success: true, id:doc.id,url: doc.add_image.url, type: doc.add_image_content_type, name: doc.add_image_file_name}
    else
      {success: false}
    end
  end
end
