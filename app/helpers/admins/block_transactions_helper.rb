module Admins::BlockTransactionsHelper

  def parse_block_transactions(transactions, timestamp = nil)
    if transactions.present?
      list = []
      last4 =  retrieve_last4(transactions.first["tags"])
      card_id = retrieve_card_id(transactions.first["tags"])
      first6 = retrieve_first6(transactions.first["tags"])
      main_tip = transactions.first["tags"].try(:[], "tip").to_f
      transaction_type = transactions.first["tags"].try(:[],"api_type")
      if transaction_type.present? && transaction_type == "credit"
        main_amount = SequenceLib.dollars(transactions.first["amount"])
      else
        main_amount = SequenceLib.dollars(transactions.first["amount"])
      end
      location_id = transactions.first["tags"].try(:[], "location").try(:[], "id")
      iso_id = Wallet.where(id: transactions.first["tags"].try(:[],"fee_perc").try(:[], "iso").try(:[], "wallet")).try(:last).try(:users).try(:first).try(:id) if transactions.first["tags"].try(:[],"fee_perc").try(:[], "iso").try(:[], "wallet").present?
      agent_id = Wallet.where(id: transactions.first["tags"].try(:[],"fee_perc").try(:[], "agent").try(:[], "wallet")).try(:last).try(:users).try(:first).try(:id) if transactions.first["tags"].try(:[],"fee_perc").try(:[], "agent").try(:[], "wallet").present?
      affiliate_id = Wallet.where(id: transactions.first["tags"].try(:[],"fee_perc").try(:[], "affiliate").try(:[], "wallet")).try(:last).try(:users).try(:first).try(:id) if transactions.first["tags"].try(:[],"fee_perc").try(:[], "affiliate").try(:[], "wallet").present?
      transactions.each do |obj|
        seq_parent_id = transactions.first["id"] if obj["id"] != transactions.first["id"]
        total_fee = SequenceLib.cents(get_block_fee(obj["tags"]))
        if obj.present?
          list <<
            {
                id: obj["id"],
                seq_parent_id: seq_parent_id,
                parent_id: obj["transaction_id"],
                timestamp: obj["timestamp"] || timestamp,
                action: obj["type"],
                type: updated_block_type(obj["tags"]["type"],obj["tags"]["sub_type"],obj["tags"]),
                tags: obj["tags"],
                last4: retrieve_last4(obj["tags"]) || last4,
                first6: retrieve_first6(obj["tags"]) || first6,
                card_id: retrieve_card_id(obj["tags"]) || card_id,
                charge_id: retrieve_charge_id(obj["tags"]),
                card: obj["tags"].try(:[], "card") || obj["tags"].try(:[], "previous_issue").try(:[], "tags").try(:[], "card"),
                sub_type: new_get_inner_type(obj["tags"]),
                gateway: get_block_gateway(obj["tags"]),
                amount: SequenceLib.cents(get_total_block_amount(SequenceLib.dollars(obj["amount"]), obj["tags"], SequenceLib.dollars(total_fee))),
                transaction_amount: SequenceLib.cents(get_main_block_amount(SequenceLib.dollars(obj["amount"]), obj["tags"])),
                fee: total_fee,
                tip: SequenceLib.cents(get_block_tip(obj["tags"])),
                hold_money: SequenceLib.cents(get_block_hold_money(obj["tags"])),
                sender: get_block_source(obj),
                receiver: obj["destination_account_id"],
                api_type: retrieve_api_type(obj["tags"]),
                ip: obj["tags"]["ip"],
                iso_fee: sync_fees(obj["tags"].try(:[], "fee_perc"), TypesEnumLib::Users::Iso),
                agent_fee: sync_fees(obj["tags"].try(:[], "fee_perc"), TypesEnumLib::Users::Agent),
                partner_fee: sync_fees(obj["tags"].try(:[], "fee_perc"), TypesEnumLib::Users::Partner),
                affiliate_fee: sync_fees(obj["tags"].try(:[], "fee_perc"), TypesEnumLib::Users::Affiliate),
                gbox_fee: sync_fees(obj["tags"].try(:[], "fee_perc"), TypesEnumLib::Users::Gbox, get_block_fee(obj["tags"])),
                privacy_fee: obj["tags"].try(:[],"privacy_fee").to_f,
                net_fee: number_with_precision(get_net_block_privacy_fee(obj["tags"], SequenceLib.dollars(total_fee)), precision: 2),
                net_amount: number_with_precision(get_new_block_net(SequenceLib.dollars(obj["amount"]),SequenceLib.dollars(total_fee),parse_block_type(nil, obj), obj["tags"], obj["tags"].try(:[], "tip")).to_f, precision: 2),
                location_dba_name: get_dba_name(obj["tags"]),
                merchant_name: get_merchant_name(obj["tags"]),
                tx_type: obj["tags"].try(:[], "type"),
                tx_sub_type: obj["tags"].try(:[],"sub_type"),
                payment_gateway_id: get_gateway_id(obj["tags"]),
                main_amount: main_amount,
                transaction_fee: obj.tags["gateway_fee_details"].try(:[], "transaction_fee"),
                account_processing_limit: obj.tags["gateway_fee_details"].try(:[], "account_processing_limit"),
                charge_back_fee: obj.tags["gateway_fee_details"].try(:[], "charge_back_fee"),
                retrieval_fee: obj.tags["gateway_fee_details"].try(:[], "retrieval_fee"),
                per_transaction_fee: obj.tags["gateway_fee_details"].try(:[], "per_transaction_fee"),
                reserve_money_fee: obj.tags["gateway_fee_details"].try(:[], "reserve_money_fee"),
                reserve_money_days: obj.tags["gateway_fee_details"].try(:[], "reserve_money_days"),
                monthly_service_fee: obj.tags["gateway_fee_details"].try(:[], "monthly_service_fee"),
                misc_fee: obj.tags["gateway_fee_details"].try(:[], "misc_fee"),
                misc_fee_dollars: obj.tags["gateway_fee_details"].try(:[], "misc_fee_dollars"),
                clerk_id: obj.tags.try(:[], "sales_info").try(:[], "clerk_id"),
                terminal_id: obj.tags.try(:[], "sales_info").try(:[], "terminal_id"),
                ref_id: obj.tags.try(:[], "sales_info").try(:[], "ref_id"),
                location_id: location_id,
                iso_id: iso_id,
                agent_id: agent_id,
                affiliate_id: affiliate_id,
                baked: obj.tags.try(:[], "baked_type")
            }
        end
      end
      list
    end
  end

  def save_block_trans(transactions,check_escrow = nil,tango_status=nil)
    if transactions.present? && transactions.count > 0
      block_transactions = []
      position = 0
      transactions.each do |transaction|
        sender=receiver=nil
        sender = Wallet.where(id: transaction[:sender]).try(:first) if transaction[:sender].present?
        sender_user = sender.try(:users).try(:first) if sender.present?
        receiver = Wallet.where(id: transaction[:receiver]).try(:first) if transaction[:receiver].present?
        receiver_user = receiver.try(:users).try(:first) if receiver.present?
        transaction[:tags]["check_escrow"] = check_escrow if check_escrow.present?
        transaction[:tags]["check_escrow_status"] = tango_status if tango_status.present?
        sender_dba=get_dba_name(nil,sender)
        receiver_dba=get_dba_name(nil,receiver)
        block_tx = BlockTransaction.new(
            receiver_id: receiver_user.try(:id),
            sender_id: sender_user.try(:id),
            sender_name: sender_dba,
            receiver_name: receiver_dba,
            receiver_wallet_id: transaction[:receiver],
            sender_wallet_id: transaction[:sender],
            amount_in_cents: transaction[:amount],
            tx_amount: transaction[:transaction_amount],
            fee_in_cents: transaction[:fee],
            tags: transaction[:tags],
            timestamp: transaction[:timestamp],
            sequence_id: transaction[:id],
            seq_parent_id: transaction[:seq_parent_id],
            tip_cents: transaction[:tip],
            hold_money_cents: transaction[:hold_money],
            gateway: transaction[:gateway],
            card_id: transaction[:card_id],
            last4: transaction[:last4],
            first6: transaction[:first6],
            charge_id: transaction[:charge_id],
            main_type: transaction[:type],
            sub_type: transaction[:sub_type],
            api_type: transaction[:api_type],
            ip: transaction[:ip],
            card_type: transaction[:card].try(:[], "card_type"),
            card_brand: transaction[:card].try(:[], "brand"),
            action: transaction[:action].parameterize.underscore.to_sym,
            status: :approved,
            iso_fee: transaction[:iso_fee],
            partner_fee: transaction[:partner_fee],
            affiliate_fee: transaction[:affiliate_fee],
            agent_fee: transaction[:agent_fee],
            gbox_fee: transaction[:gbox_fee],
            privacy_fee: transaction[:privacy_fee],
            net_fee: transaction[:net_fee],
            total_net: transaction[:net_amount],
            location_dba_name: transaction[:location_dba_name],
            merchant_name: transaction[:merchant_name],
            tx_type: transaction[:tx_type],
            tx_sub_type: transaction[:tx_sub_type],
            category_id: receiver.try(:location).try(:category_id),
            payment_gateway_id: transaction[:payment_gateway_id],
            main_amount: transaction[:main_amount],
            position: position,
            clerk_id: transaction[:clerk_id],
            terminal_id: transaction[:terminal_id],
            ref_id: transaction[:ref_id],
            location_id: transaction[:location_id],
            iso_id: transaction[:iso_id],
            agent_id: transaction[:agent_id],
            affiliate_id: transaction[:affiliate_id],
            transaction_fee: transaction[:transaction_fee],
            baked: transaction[:baked],
            # account_processing_limit: transaction[:account_processing_limit],
            # charge_back_fee: transaction[:charge_back_fee],
            # retrieval_fee: transaction[:retrieval_fee],
            per_transaction_fee: transaction[:per_transaction_fee],
            # reserve_money: transaction[:reserve_money_fee],
            # reserve_money_days: transaction[:reserve_money_days],
            # monthly_service_fee: transaction[:monthly_service_fee],
            # misc_fee: transaction[:misc_fee],
            # misc_fee_dollars: transaction[:misc_fee_dollars],
        )
        block_tx.generate_quickcard_id if block_tx.main_type == "Invoice Qc"
        block_transactions << block_tx
        position = position + 1
      end
      if block_transactions.present? && block_transactions.count > 0
        if block_transactions.count > 1  #= saving parent id to child transaction
          new_block_transaction = []
          @parent_id = nil
          @seq_parent = nil

          block_transactions.each_with_index do|trans,index|
            if index == 0
              trans.save
              @seq_parent = trans.sequence_id
              @parent_id = trans.id
            else
              trans.parent_id = @parent_id
              trans.seq_parent_id = @seq_parent
              new_block_transaction << trans
            end
          end
          block_transactions = new_block_transaction
        end
        BlockTransaction.import block_transactions, on_duplicate_key_update: {conflict_target: [:sequence_id], columns: [:updated_at, :tx_amount, :net_fee, :total_net, :privacy_fee, :fee_in_cents, :amount_in_cents]}
      end
    end
  end

  def parse_single_block(obj)
      if obj.present?
        tags = JSON.parse(obj.tags)
        source_tags = JSON.parse(obj.src_account_tags)
        destination_tags = JSON.parse(obj.dst_account_tags)
        location_id = tags.try(:[], "location").try(:[], "id")
        iso_id = Wallet.where(id: tags.try(:[],"fee_perc").try(:[], "iso").try(:[], "wallet")).try(:last).try(:users).try(:first).try(:id) if tags.try(:[],"fee_perc").try(:[], "iso").try(:[], "wallet").present?
        agent_id = Wallet.where(id: tags.try(:[],"fee_perc").try(:[], "agent").try(:[], "wallet")).try(:last).try(:users).try(:first).try(:id) if tags.try(:[],"fee_perc").try(:[], "agent").try(:[], "wallet").present?
        affiliate_id = Wallet.where(id: tags.try(:[],"fee_perc").try(:[], "affiliate").try(:[], "wallet")).try(:last).try(:users).try(:first).try(:id) if tags.try(:[],"fee_perc").try(:[], "affiliate").try(:[], "wallet").present?
        total_fee = SequenceLib.cents(get_block_fee(tags))
         {
                sequence_id: obj.id.gsub('\\x',''),
                seq_block_id: obj.transaction_id.gsub('\\x',''),
                timestamp: obj.timestamp,
                action: obj.type,
                type: updated_block_type(tags["type"],tags["sub_type"],tags),
                tx_type: tags["type"],
                tx_sub_type: tags["sub_type"],
                tags: tags,
                last4: retrieve_last4(tags),
                first6: retrieve_first6(tags),
                card_id: retrieve_card_id(tags),
                card: tags.try(:[], "card") || tags.try(:[], "previous_issue").try(:[], "tags").try(:[], "card"),
                charge_id: retrieve_charge_id(tags),
                sub_type: new_get_inner_type(tags),
                gateway: get_block_gateway(tags),
                payment_gateway_id: get_gateway_id(tags),
                amount: SequenceLib.cents(get_total_block_amount(SequenceLib.dollars(obj.amount.to_i), tags, SequenceLib.dollars(total_fee))),
                transaction_amount: SequenceLib.cents(get_main_block_amount(SequenceLib.dollars(obj.amount), tags)),
                fee: total_fee,
                tip: SequenceLib.cents(get_block_tip(tags)),
                hold_money: SequenceLib.cents(get_block_hold_money(tags)),
                sender: obj.src_account_id,
                receiver: obj.dst_account_id,
                api_type: retrieve_api_type(tags),
                ip: tags["ip"],
                iso_fee: sync_fees(tags.try(:[], "fee_perc"), TypesEnumLib::Users::Iso),
                agent_fee: sync_fees(tags.try(:[], "fee_perc"), TypesEnumLib::Users::Agent),
                partner_fee: sync_fees(tags.try(:[], "fee_perc"), TypesEnumLib::Users::Partner),
                affiliate_fee: sync_fees(tags.try(:[], "fee_perc"), TypesEnumLib::Users::Affiliate),
                gbox_fee: sync_fees(tags.try(:[], "fee_perc"), TypesEnumLib::Users::Gbox, get_block_fee(obj["tags"])),
                privacy_fee: tags.try(:[],"privacy_fee").to_f,
                net_fee: number_with_precision(get_net_block_privacy_fee(tags, SequenceLib.dollars(total_fee)), precision: 2),
                net_amount: number_with_precision(get_new_block_net(SequenceLib.dollars(obj.amount),SequenceLib.dollars(total_fee),parse_block_type(nil, obj), tags, tags.try(:[], "tip")).to_f, precision: 2),
                position: obj.position,
                location_dba_name: get_dba_name(tags),
                merchant_name: get_merchant_name(tags),
                source_name: source_tags.try(:[],"name"),
                destination_name: destination_tags.try(:[],"name"),
                location_id: location_id,
                agent_id: agent_id,
                iso_id: iso_id,
                affiliate_id: affiliate_id,
                clerk_id: tags.try(:[], "sales_info").try(:[], "clerk_id"),
                terminal_id: tags.try(:[], "sales_info").try(:[], "terminal_id"),
                ref_id: tags.try(:[], "sales_info").try(:[], "ref_id")

            }
      end
  end

  def get_dba_name(tags, wallet = nil)
    if tags.present? && tags.try(:[], "location").present?
      tags["location"].try(:[], "business_name")
    elsif wallet.present? && wallet.primary?
      wallet.try(:location).try(:business_name) || wallet.try(:name)
    elsif wallet.present?
      "#{wallet.try(:location).try(:business_name) || wallet.try(:users).try(:first).try(:name)} #{wallet.try(:wallet_type).try(:capitalize)} Wallet "
    end
  end

  def get_merchant_name(tags)
    if tags.try(:[],"merchant").present?
      tags.try(:[],"merchant").try(:[],"name") if tags.try(:[],"merchant").class != 1.class
    end
  end

  def get_sale_type(type, sub_type)
    if type == "sale" && sub_type.present?
      if sub_type == "debit_charge" || sub_type == "virtual_terminal_sale" || sub_type == "virtual_terminal_sale_api" || sub_type == "QR_Scan" || sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction || sub_type == "sale_giftcard"
        return "sale"
      end
    end
  end

  def get_block_source(transaction)
    if transaction["source_account_id"].present?
      transaction["source_account_id"]
    elsif transaction["source_account_id"].nil? && transaction["tags"]["api_type"].present?
      transaction["tags"]["api_type"].capitalize
    end
  end

  def get_sender(object)
    if object.present?
      if object.sender_wallet_id.present?
        if object.sender_wallet_id == 0
          if object.tags["api_type"].present?
            return object.tags["api_type"].capitalize
          end
        else
          return object.sender_wallet_id
        end
      end
    end
  end

  def retrieve_last4(object)
    if object["last4"].present?
      object["last4"]
    elsif object.try(:[],"previous_issue").try(:[],"last4").present?
      object["previous_issue"]["last4"]
    else
      nil
    end
  end

  def retrieve_first6(object)
    if object["first6"].present?
      object["first6"]
    elsif object.try(:[],"previous_issue").try(:[],"first6").present?
      object["previous_issue"]["first6"]
    else
      nil
    end
  end

  def retrieve_card_id(object)
    if object["card"].present?
      object["card"]["id"]
    elsif object.try(:[],"previous_issue").try(:[],"tags").try(:[],"card").present?
      object["previous_issue"].try(:[],"tags").try(:[],"card").try(:[], "id")
    elsif object.try(:[],"previous_issue").try(:[], "card").present?
      object["previous_issue"].try(:[], "card").try(:[], "id")
    else
      nil
    end
  end

  def retrieve_charge_id(object)
    if object.try(:[],"previous_issue").present?
      object["previous_issue"]["transaction_id"]
    else
      nil
    end
  end

  def retrieve_api_type(object)
    if object["api_type"].present?
      object["api_type"]
    else
      nil
    end
  end

  def sync_fees(tags,type, fee = nil)
    if tags.present?
      case type
      when TypesEnumLib::Users::Iso
        if tags.try(:[], "iso").present?
          if tags["iso"]["iso_buyrate"].present?
            return 0.0
          else
            return tags["iso"]["amount"].to_f
          end
        elsif tags.try(:[], "iso_fee").present?
          return tags["iso_fee"].to_f
        elsif tags.try(:[], "iso_total_fee").present?
          return tags["iso_total_fee"].to_f
        else
          return 0.0
        end
      when TypesEnumLib::Users::Agent
        if tags.try(:[], "agent").present?
          return tags["agent"]["amount"].to_f
        elsif tags.try(:[], "agent_fee").present?
          return tags["agent_fee"].to_f
        elsif tags.try(:[], "agent_total_fee").present?
          return tags["agent_total_fee"].to_f
        else
          return 0.0
        end
      when TypesEnumLib::Users::Affiliate
        if tags.try(:[], "affiliate").present?
         return tags["affiliate"]["amount"].to_f
        else
          return 0.0
        end
      when TypesEnumLib::Users::Partner
        if tags.try(:[], "partner").present?
          return tags["partner"]["amount"].to_f
        elsif tags.try(:[], "partner_fee").present?
          return tags["partner_fee"].to_f
        elsif tags.try(:[], "partner_total_fee").present?
          return tags["partner_total_fee"].to_f
        else
          return 0.0
        end
      when TypesEnumLib::Users::Gbox
        if tags.try(:[], "gbox").present?
          if tags["iso"].try(:[],"iso_buyrate").present?
            return tags["gbox"].try(:[],"amount").to_f + tags["iso"].try(:[],"amount").to_f
          else
            return tags["gbox"].try(:[],"amount").to_f
          end
        elsif tags.try(:[], "partner_total_fee").present? || tags.try(:[], "agent_total_fee").present? || tags.try(:[], "iso_total_fee").present? || tags.try(:[], "partner_fee").present? || tags.try(:[], "agent_fee").present? || tags.try(:[], "iso_fee").present?
          return fee.to_f - tags.try(:[], "partner_total_fee").to_f - tags.try(:[], "agent_total_fee").to_f - tags.try(:[], "iso_total_fee").to_f - tags.try(:[], "partner_fee").to_f - tags.try(:[], "agent_fee").to_f - tags.try(:[], "iso_fee").to_f
        else
          return 0.0
        end
      else
        0.0
      end
    else
      0.0
    end
  end

  def parse_block_admin_amount(transaction)
    # need to remove this once task is run on live
    amount = SequenceLib.dollars(transaction.amount_in_cents)
    if transaction.tags['location_fee'].present?
      fee = transaction.tags['fee'].to_f + transaction.tags['location_fee'].to_f
    else
      fee = transaction.tags['fee'].to_f
    end
    # fee_perc = transaction.tags['fee_perc']
    if fee.present? && fee > 0 && amount != fee
      number_with_precision(fee, precision: 2)
    else
      number_with_precision(0, precision: 2)
    end
  end

  #= For transaction types
  def updated_block_type(type, sub_type=nil,t=nil)
    return "CBK Won" if sub_type == 'cbk_won'
    if type == "Fee_Transfer"
      return "CBK Fee" if sub_type == 'cbk_fee' || sub_type == 'charge_back_fee' || sub_type == 'cbk_fee_fee'
      return "Retrieval Fee" if sub_type == 'retrievel_fee' || sub_type == 'retrievel_fee_fee'
    end
    return "CBK Lost" if sub_type =='cbk_lost'
    return "Dispute Accepted" if sub_type == 'cbk_dispute_accepted'
    return "CBK Hold" if type=='cbk_hold' || type=='charge_back_hold'
    if t.present?
      if t["source"].present?
        return "CBK Won" if t["sub_type"] == 'cbk_won' || t["source"]=='charge_back_won' || t["source"]=='cbk_won' || t["fee_type"]=='cbk_won'
        return "CBK Lost" if t["sub_type"] =='cbk_lost' || t["source"]=='charge_back_lost' || t["source"]=='cbk_lost' || t["fee_type"]=='cbk_lost'
      end
    end
    return 'Unknown' if type.nil?
    case type
      when "bulk_check"
        return "Bulk Check"
      when "send_check"
        return "Send Check"
      when "giftcard_purchase"
        return "Giftcard Purchase"
      when "giftcard"
        return "Giftcard Purchase"
      when "ACH_deposit"
        return "ACH Deposit"
      when "sale"
        if sub_type.present?
          if sub_type == "sale_tip" || sub_type == "tip_vt_transaction_&_debit"
            return "Sale Tip"
          end
          if sub_type == "sale_giftcard"
            return "Sale Giftcard"
          end
          if sub_type == "sale_issue"
            return "Sale Issue"
          end
          if sub_type == "sale_issue_api"
            return "Sale Issue"
          end
          if sub_type == "debit_charge"
            return "PIN Debit"
          end
          if sub_type == "virtual_terminal_sale"
            return "Virtual Terminal"
          end
          if sub_type == "virtual_terminal_sale_api"
            return "eCommerce"
          end
          if sub_type == "QR_Scan"
            return "Sale"
          end
          if sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
            return "Credit Card"
          end
          if sub_type == TypesEnumLib::TransactionSubType::DebitCard
            return TypesEnumLib::TransactionType::DebitCard
          end
          if sub_type == TypesEnumLib::TransactionSubType::Issue3DS
            return TypesEnumLib::TransactionSubType::Issue3DS
          end
        end
        return "Sale"
      when TypesEnumLib::TransactionType::Transfer3DS
        return TypesEnumLib::TransactionType::Transfer3DS
      when "approve_request_issue"
        return "Approve Request Issue"
      when "b2b_transfer"
        return "B2B Transfer"
      when "Reserve_Money_Deposit"
        return "Reserve Money Deposit"
      when "Reserve_Money_Return"
        return "Reserve Money Return"
      when "Void_Check"
        return "Void Check"
      when "send_check_void"
        return "Void Check"
      when "payed_tip"
        return "Payed Tip"
      when "Fee_Transfer"  || "fee"
        return "Fee Transfer"
      when "charge" || "Charge"
        return "Mobile Charge"
      when "B2B Fee"
        return "B2B Fee"
    when "Service Fee"
      return "Monthly Fee"
    when TypesEnumLib::TransactionType::Issue3DS
      return TypesEnumLib::TransactionType::Issue3DS
    else
        return type.present? ? "#{type}".split('_').map{|m| m.capitalize}.join(' ') : nil
    end
  end

  def get_block_gateway(tags)
    if tags["descriptor"].present?
      return tags["descriptor"].try(:humanize)
    elsif tags["source"].present?
      if tags["source"] == "stripe"
        return "Stripe GBox"
      end
      if tags["type"].downcase == "void_check"
        return "Checkbook io"
      end
      if !tags["source"].is_a?(Integer) && tags["source"].downcase == "checkbook.io"
        return "Checkbook io"
      end
      tags["source"]#.humanize
    else
      ""
    end
  end

  def get_gateway_id(tags)
    # gateway = tags["descriptor"]
    # gateway = tags["source"] if gateway.blank?
    tags["payment_gateway_id"]
    # PaymentGateway.where("key = :key OR name = :key", key: gateway).try(:first).try(:id) if payment_gateway_id.blank?
  end

  def new_get_inner_type(t)
    if t["type"] == "sale"
      if t["sub_type"].present?
        if t["sub_type"] == "sale_tip"
          return "sale_tip"
        elsif t["sub_type"] == "virtual_terminal_sale"
          return "virtual_terminal"
        elsif t["sub_type"] == "sale_issue"
          return "sales_issue"
        elsif t["sub_type"] == "sale_issue_api"
          return "sale_issue_api"
        elsif t["sub_type"] == "sale_giftcard"
          return "sale_giftcard"
        elsif t["sub_type"] == "tip_vt_transaction_&_debit"
          return "sale_tip"
        elsif t["sub_type"] == "qr_credit_card"
          return "qr_credit_card"
        elsif t["sub_type"] == "qr_debit_card"
          return "qr_debit_card"
        else
          return "N/A"
        end
      else
        return "N/A"
      end
    elsif t["type"] == "Fee_Transfer" || t["type"] == "fee"
      if t["sub_type"].present?
        return t["sub_type"]
      else
        return "N/A"
      end
    else
      "N/A"
    end
  end

  def get_total_block_amount(amount, tags, total_fee)
    if tags.present?
      privacy_fee = 0
      tip = 0
      privacy_fee = tags["privacy_fee"] if tags["privacy_fee"].present?
      tip = tags["tip"] if tags["tip"].present?
      if tags["sub_type"] != "virtual_terminal_sale_api" && tags["sub_type"] != "sale_issue_api"
        total_amount = amount.to_f
      else
        total_amount = amount.to_f
      end
      if privacy_fee.to_f > 0
        total_amount = total_amount
      else
        total_amount = total_amount + privacy_fee.to_f
      end
      if (tags["type"] == TypesEnumLib::TransactionType::SendCheck || tags["type"] == "giftcard_purchase" || tags["type"] == TypesEnumLib::TransactionType::Refund || tags["type"] == TypesEnumLib::TransactionType::ACHdeposit || tags["type"] == TypesEnumLib::TransactionType::DebitCardDeposit ) && tags["fee"].present?
        total_amount = total_amount + tags["fee"].to_f
      else
        total_amount
      end
    else
      amount
    end
  end

  def get_block_tip(tags)
    if tags["tip"].present?
      number_with_precision(tags["tip"], precision: 2)
    else
      "0.00"
    end
  end

  def get_main_block_amount(amount, tags)
    if tags.present?
      if tags["privacy_fee"].present?
        if tags.try(:[], "sub_type").present? && (tags.try(:[], "sub_type") == "virtual_terminal_sale_api" || tags.try(:[], "sub_type") == "sale_issue_api" )
          return amount.to_f - tags.try(:[], "privacy_fee").to_f - tags.try(:[], "tip").to_f
        else
          return amount.to_f - tags.try(:[], "privacy_fee").to_f
        end
      elsif tags.try(:[], "sub_type").present? && (tags.try(:[], "sub_type") == "virtual_terminal_sale_api" || tags.try(:[], "sub_type") == "sale_issue_api" )
        return amount.to_f - tags.try(:[], "privacy_fee").to_f - tags.try(:[], "tip").to_f
      else
        return amount.to_f
      end
    else
      amount.to_f
    end
  end

  def get_block_fee(tags)
    if tags["type"]=="retrievel_fee"
      return 0
    end
    if tags["fee"].present?
      if tags["location_fee"].present?
        fee = tags["fee"].to_f + tags["location_fee"].to_f
      else
        fee = tags["fee"].to_f
      end
      if tags["fee_perc"].present?
        if tags["fee_perc"]["iso"].present? && tags["fee_perc"]["iso"]["iso_buyrate"].present?
          fee = tags["fee_perc"].try(:[], "iso").try(:[],"amount").to_f + tags["fee_perc"].try(:[], "gbox").try(:[],"amount").to_f
        end
        if tags["fee_perc"]["hold_money"].present?
          if fee > 0
            return fee - tags["fee_perc"]["hold_money"].to_f
          else
            return fee
          end
        else
          return fee
        end
      else
        return fee
      end
    end
  end

  def get_block_hold_money(tags)
    if tags["fee_perc"].present?
      if tags["fee_perc"]["hold_money"].present?
        return number_with_precision(tags["fee_perc"]["hold_money"], precision: 2)
      elsif tags["fee_perc"]["reserve"].present? && tags["fee_perc"]["reserve"]["amount"].present?
        return number_with_precision(tags["fee_perc"]["reserve"]["amount"], precision: 2)
      else
        return number_with_precision(0, precision: 2)
      end
    else
      return number_with_precision(0, precision: 2)
    end
  end

  def get_net_block_privacy_fee(tags, total_fee)
    if total_fee.present?
      fee = total_fee.to_f
      if tags["fee_perc"].try(:[], "iso").present? && tags["fee_perc"].try(:[], "iso").try(:[], "iso_buyrate").present?
        fee = tags["fee_perc"].try(:[], "iso").try(:[],"amount").to_f + tags["fee_perc"].try(:[], "gbox").try(:[],"amount").to_f
      end
      if fee > 0
        if tags["privacy_fee"].present?
          fee = fee - tags["privacy_fee"].to_f
        else
          fee = number_with_precision(fee, precision: 2)
        end
        fee
      else
        "0.00"
      end
    else
      "0.00"
    end
  end

  def get_new_block_net(amount, fee, type, tags, tip)
    amount = amount.to_f + tip.to_f
    if tags["type"]=="send_check" || tags["type"] == "giftcard_purchase" || tags["type"]=="refund" || type == "RETIRE" || tags["type"] == TypesEnumLib::TransactionType::ACHdeposit || tags["type"] == TypesEnumLib::TransactionType::DebitCardDeposit
      amount.to_f
    else
      if tags["fee_perc"].present?
        if tags["fee_perc"]["reserve"].present? && (tags["type"] != "Fee_Transfer")
          amount.to_f - fee.to_f - tags["fee_perc"]["reserve"]["amount"].to_f - tip.to_f
        else
          amount.to_f - fee.to_f - tip.to_f
        end
      else
        amount.to_f - fee.to_f - tip.to_f
      end
    end
  end

  def parse_admin_block_amount(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    if transaction.tags['location_fee'].present?
      fee = transaction.tags['fee'].to_f + transaction.tags['location_fee'].to_f
    else
      fee = transaction.tags['fee'].to_f
    end
    # fee_perc = transaction.tags['fee_perc']
    if fee.present? && fee > 0 && amount != fee
      number_with_precision(fee, precision: 2)
    else
      number_with_precision(0, precision: 2)
    end
  end

  def parse_block_type(wallet, action)
    # return action.type if wallet.nil?
    if wallet.present?
      if action.type == 'issue' && action.destination_account_id == wallet.to_s
        'DEPOSIT'
      elsif action.type == 'retire'
        "RETIRE"
      else
        if action.tags["type"] == 'fee' || action.tags["type"] == 'Fee_Transfer'
          'FEE'
        else
          'TRANSFER'
        end
      end
    else
      if action.type == 'issue'
        'DEPOSIT'
      elsif action.type == 'retire'
        "RETIRE"
      else
        if action.tags["type"] == 'fee' || action.tags["type"] == 'Fee_Transfer'
          'FEE'
        else
          'TRANSFER'
        end
      end
    end
  end

end
