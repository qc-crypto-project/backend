module Admins::MaxmindHelper
  def maxmind_minfraud_status(minfraud_result)
    if minfraud_result.present? &&
        minfraud_result.qc_action.present? &&
        minfraud_result.qc_reason.present?
      qc_action = minfraud_result.qc_action
      qc_reason = minfraud_result.qc_reason

      case qc_reason
      when TypesEnumLib::RiskReason::Default
        if qc_action == TypesEnumLib::RiskType::Accept
          '<span class="risk-evaluation a-green"> A </span>'
        else
          '<span class="risk-evaluation r-red"> R </span>'
        end
      when TypesEnumLib::RiskReason::ManualReview
        if qc_action == TypesEnumLib::RiskType::PendingReview
          '<span class="risk-evaluation cr-p-manual-review"> MR </span>'
        elsif qc_action == TypesEnumLib::RiskType::Accept
          '<span class="risk-evaluation mr-green"> MR </span>'
        elsif qc_action == TypesEnumLib::RiskType::Reject
          '<span class="risk-evaluation mr-red"> MR </span>'
        elsif qc_action == TypesEnumLib::RiskType::ExpiredReview
          '<span class="risk-evaluation mr-red"> ER </span>'
        else
          '<span class="risk-evaluation">New</span>'
        end
      else
        '<span class="risk-evaluation">New</span>'
      end
    else
      '<span class="risk-evaluation no-disposition">&nbsp;</span>'
    end
  end

  def get_user_info(user)
    #user = User.find id
    user_email = user.email
    user_name = user.name
    return user_info = { email: user_email , name: user_name }
  end


end