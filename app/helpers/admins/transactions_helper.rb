module Admins::TransactionsHelper
  #using this helper to get information in transactions detail modal
  
  def url_path(source, destination)
    #getting id of source/destination to show information on modal
    if source.present? && (source != "Credit" && source != "Debit" && source != "Sales_issue")
      wallet_id = Wallet.find_by_id(source).try(:id)
    elsif destination.present?
      wallet_id = Wallet.find_by_id(destination).try(:id)
    end
    wallet_id
  end

  def return_user(users, type, t=nil)
    if type == "DEPOSIT" || type == "FEE"
      {users: users[:destination], wallet: users[:destination_wallet]}
    elsif type == 'RETIRE' || type == "TRANSFER"
      return {users: users[:source], wallet: users[:source_wallet]}
    end
  end

  def transaction_heading(transaction)
    transaction[:type]
  end

  def bulk_checks_number(obj)
    if obj.try(:[],"bulk_checks").present?
      if obj["bulk_checks"]["bulk_check_instance"].present?
        bulk = BulkCheckInstance.find_by(id: obj["bulk_checks"]["bulk_check_instance"]).try(:bulk_checks)
        if bulk.present?
          check_number = []
          bulk.each do |check|
            number = check.tango_order.try(:number)
            check_number << number
          end
          check_number = check_number.compact
          return "#{check_number.first}-#{check_number.last}"
        end
      end
    end
  end

  def bulk_total_checks(obj)
    if obj.try(:[],"bulk_checks").present?
      if obj["bulk_checks"]["bulk_check_instance"].present?
        bulk = BulkCheckInstance.find_by(id: obj["bulk_checks"]["bulk_check_instance"]).try(:bulk_checks)
        return bulk.try(:count)
      end
    end
  end

  def bulk_via_email(obj)
    if obj.try(:[],"bulk_checks").present?
      if obj["bulk_checks"]["checks_ids"].present?
        return TangoOrder.where(id: obj["bulk_checks"]["checks_ids"],send_via: "email").try(:count)
      end
    end
  end

  def bulk_via_deposit(obj)
    if obj.try(:[],"bulk_checks").present?
      if obj["bulk_checks"]["checks_ids"].present?
        return TangoOrder.where(id: obj["bulk_checks"]["checks_ids"],send_via: "direct_deposit").try(:count)
      end
    end
  end

  def report_type(type)
    return_type = type
    case type
    when TypesEnumLib::WorkerType::Admin
      return_type = "Transactions"
    when TypesEnumLib::WorkerType::AdminUsers
      return_type = "User Transactions"
    when TypesEnumLib::WorkerType::AdminMerchant
      return_type = "Merchant Transactions"
    when TypesEnumLib::WorkerType::AdminMerchantProfile

    end
    return return_type
  end

end
