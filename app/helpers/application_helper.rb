module ApplicationHelper
  include Admins::BlockTransactionsHelper
  include HelpsHelper
  include ActionView::Helpers::NumberHelper
  include FluidPayHelper
  include Merchant::TransactionsHelper
  include Payment
  include PosMerchantHelper

  # @@app = Rpush::Gcm::App.new(name: "QuickCard",auth_key: ENV['FIRE_BASE_SERVER_KEY'], connections: 1)
  @@ach_client = Plaid::Client.new( env: :sandbox, client_id: ENV['PLAID_CLIENT_ID'], secret: ENV['PLAID_SECRET'], public_key: ENV['PLAID_PUBLIC_KEY'] )
  @@check_book = ENV['CHECKBOOK_API_KEY']
  @@check_book_seceret= ENV['CHEKCBOOK_API_SECRET']

  class SaleValidationError < StandardError; end

  def active_class(link_path)
    current_page?(link_path) ? "active" : ""
  end

  def mtrac_admin
    offset = Time.now.in_time_zone(cookies[:timezone]).strftime("%z")
    if current_user.present? && current_user.support_mtrac?
      unless controller_name == "error_message" && action_name == "index"
        flash[:notice] = "Page not available!"
        return redirect_to transactions_admins_path(trans: "success", offset: offset)
      end
    end
  end

  def support_customer
    offset = Time.now.in_time_zone(cookies[:timezone]).strftime("%z")
    if current_user&.support_customer?
      unless (controller_name == "transactions" && action_name == "local_transaction_details") || (controller_name == "admins" && action_name == "transactions") || (controller_name == "error_message" && action_name == "index")
        flash[:notice] = "Page not available!"
        return redirect_to transactions_admins_path(trans: "success", offset: offset)
      end
    end
  end

  def ach_status(status)
    if status == "captured" || status == "IN_PROGRESS"
      return "IN_PROCESS"
    else
      return status
    end
  end

  def random_password
    return "#{rand(1..10)}a#{Devise.friendly_token.first(8)}A"
  end

  def recurring_flaged_trans(card,params)
    success = true
    message = ''
    if params[:flag].present?
      flaged = params[:flag]
      if flaged == "straight"
      elsif flaged == "recurring"
        card.update(flag: flaged) if card.flag != "rebill" && card.flag != "initial"
      elsif flaged == "initial"
        if params[:amount].to_f > 15.to_f
          success = false
          message = TypesEnumLib::ErrorMessage::Initial
        else
          flaged = "initial"
          card.update(flag: flaged) if card.flag != "rebill"
        end
      elsif flaged == "rebill"
        if card.flag == "initial" || card.flag == "rebill"
          flaged = "rebill"
          card.update(flag: flaged)
        else
          success = false
          message = TypesEnumLib::ErrorMessage::Rebill
        end
      end
    end
    return {success: success,flag: flaged,message: message}
  end

  def save_image
    @old_doc= Image.where(location_id: params[:image].try(:[], :location_id)).last if params[:image].try(:[], :location_id).present?
    @old_doc.delete if @old_doc.present?
    @doc= Image.new(doc_params)
    if @doc.validate
      puts 'added image================================================='
      @doc.save
    else
      @error = @doc.errors.full_messages.first
      puts "============================================================",@error
    end
    # respond_to do |f|
    #   f.js{}
    #   f.json{render status: 200 , json: {success: true , doc: @doc,image_url:@doc.add_image.url}}
    #   f.html{}
    # end
  end

  def save_bank_image
    @old_doc= Image.where(location_id: params[:image].try(:[], :location_id)).last if params[:image].try(:[], :location_id).present?
    @old_doc.delete if @old_doc.present?
    @doc= Image.new(doc_params)
    if @doc.validate
      puts 'added image================================================='
      @doc.save
    else
      @error = @doc.errors.full_messages.first
      puts "============================================================",@error
    end
  end

  def save_image_bank_account
    @doc= Image.new(doc_params)
    if @doc.validate
      puts 'added image================================================='
      @doc.save
    else
      @error = @doc.errors.full_messages.first
      puts "============================================================",@error
    end
  end

  def save_image_with_params(params)
    # @old_doc= Image.where(location_id: params["image"].try(:[], "location_id")).last if params["image"].try(:[], "location_id").present?
    # @old_doc.delete if @old_doc.present?
    @doc= Image.new(doc_with_params(params))
    if @doc.validate
      puts 'added image================================================='
      @doc.save
    else
      @error = @doc.errors.full_messages.first
      puts "============================================================",@error
    end
    respond_to do |f|
      f.js{}
      f.json{render status: 200 , json: {success: true , doc: @doc,image_url:@doc.add_image.url}}
      f.html{}
    end
  end

  def check_directory(dir_path)
    unless File.directory?(dir_path)
      Dir.mkdir(dir_path)
    end
  end

  def get_ip
    @ip = request.remote_ip
  end

  def get_ip_with_request(request)
    @ip = request.remote_ip || request.ip
  end

  def all_transactions_of_wallet(wallet_id)
    transactions = SequenceLib.transactions(wallet_id, wallet_id, '', 0)
  end

  def do_transaction(gateway, issue_amount, original_amount, merchant, sender_wallet, qc_wallet, card, sender, charge_id, fee_lib, sub_type = nil )
    # transctions from gateway for virtual terminal
    if merchant.present? && merchant.MERCHANT? && !merchant.issue_fee
      fee = 0
    else
      fee = fee_lib.get_card_fee(original_amount.to_f, 'card')
    end
    if gateway == 'fluid_pay'
      issue = sender.issue_with_fluid_pay(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name, charge_id, get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type,card)
    elsif gateway == 'bridge_pay'
      issue = sender.issue_with_bridge_pay(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name, charge_id, get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type,card)
    elsif gateway == 'stripe'
      issue = sender.issue_with_stripe(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name, get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type, charge_id)
    elsif gateway == 'stripe_pop'
      issue = sender.issue_with_stripe_pop(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name, get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type, charge_id)
    elsif gateway == 'stripe_pop_2'
      issue = sender.issue_with_stripe_pop_2(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name, get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type, charge_id)
    elsif gateway == 'converge'
      issue = sender.issue_with_elvano(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name,  get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type, charge_id)
    elsif gateway == 'payeezy'
      issue = sender.issue_with_payeezy(issue_amount, sender_wallet.id,qc_wallet, TypesEnumLib::TransactionType::SaleType, fee, card.last4, merchant.name, get_ip, sender.ledger,nil,nil,nil,nil,nil,nil,sub_type, charge_id,card)
    end
    return {
        :issue_transaction_id => issue[:transaction_id],
        :issue_amount => issue_amount,
        :transaction_id => charge_id,
        :merchant_name => merchant.name,
        last4: card.last4,
        brand: card.brand.present? ? card.brand : "",
        exp_date: card.exp_date.present? ? card.exp_date : ""
    }
  end

  def check_gateway_error(failure, gateway, merchant, user ,amount = nil,card_number = nil,expiration_date = nil,cvc = nil,first_name = nil,last_name = nil, card = nil)
    # trying different gateways if one gateway fails
    if merchant.present?
      issue = nil
      new_gateway = nil
      if gateway == merchant.payment_gateway
        # charge from secondary and third gateways
        if merchant.secondary_payment_gateway.present?
          if merchant.secondary_payment_gateway == 'fluid_pay' && gateway != merchant.secondary_payment_gateway
            fluid_pay = FluidPayPayment.new
            issue = fluid_pay.chargeAmountRawMerchant("#{ENV['FLUID_PAY_ID']}",dollars_to_cents(amount),card_number,expiration_date,cvc,first_name,last_name, merchant, card, user)
            if issue.present?
              if issue[:charge].nil?
                issue = nil
              end
            end
            new_gateway = merchant.secondary_payment_gateway
          elsif merchant.secondary_payment_gateway == 'stripe' && gateway != merchant.secondary_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.secondary_payment_gateway, card_number)
            new_gateway = merchant.secondary_payment_gateway
          elsif merchant.secondary_payment_gateway == 'stripe_pop' && gateway != merchant.secondary_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.secondary_payment_gateway, card_number)
            new_gateway = merchant.secondary_payment_gateway
          elsif merchant.secondary_payment_gateway == 'stripe_pop_2' && gateway != merchant.secondary_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.secondary_payment_gateway, card_number)
            new_gateway = merchant.secondary_payment_gateway
          elsif merchant.secondary_payment_gateway == 'payeezy' && gateway != merchant.secondary_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.secondary_payment_gateway, card_number)
            new_gateway = merchant.secondary_payment_gateway
          elsif merchant.secondary_payment_gateway == 'bridge_pay' && gateway != merchant.secondary_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.secondary_payment_gateway, card_number)
            new_gateway = merchant.secondary_payment_gateway
          end
        end
        if merchant.third_payment_gateway.present? && issue == nil
          if merchant.third_payment_gateway == 'fluid_pay' && gateway != merchant.third_payment_gateway
            fluid_pay = FluidPayPayment.new
            issue = fluid_pay.chargeAmountRawMerchant("#{ENV['FLUID_PAY_ID']}",dollars_to_cents(amount),card_number,expiration_date,cvc,first_name,last_name, merchant, card, user)
            if issue.present?
              if issue[:charge].nil?
                issue = nil
              end
            end
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'stripe' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'stripe_pop' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'stripe_pop_2' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'payeezy' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'bridge_pay' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          end
        end
      elsif gateway == merchant.secondary_payment_gateway
        # charge from first and third gateway
        if merchant.third_payment_gateway.present?
          if merchant.third_payment_gateway == 'fluid_pay' && gateway != merchant.third_payment_gateway
            fluid_pay = FluidPayPayment.new
            issue = fluid_pay.chargeAmountRawMerchant("#{ENV['FLUID_PAY_ID']}",dollars_to_cents(amount),card_number,expiration_date,cvc,first_name,last_name, merchant, card, user)
            if issue.present?
              if issue[:charge].nil?
                issue = nil
              end
            end
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'stripe' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'stripe_pop' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'stripe_pop_2' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'payeezy' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          elsif merchant.third_payment_gateway == 'bridge_pay' && gateway != merchant.third_payment_gateway
            issue = issue_with_gateways(user, amount, card, merchant, merchant.third_payment_gateway, card_number)
            new_gateway = merchant.third_payment_gateway
          end
        end
      elsif gateway == merchant.third_payment_gateway
        issue = nil
        new_gateway = merchant.third_payment_gateway
      end
      return {charge: issue,
              gateway: new_gateway}
    end
  end
  # for parsing date in merchant transaction search
  def parse_daterange(date)
    spliter = date[10..12].present? ? date[10..12] : ' - '
    date = date.split(spliter)
    first_date = date.first.split('/')
    second_date = date.second ? date.second.split('/') : date.first.split('/')
    first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
    second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
    return [first_date, second_date]
  end

  def parse_time(params)
    if params[:time1].present?
      time1 = params[:time1]
    else
      time1 = "00:00"
    end
    if params[:time2].present?
      time2 = params[:time2]
    else
      time2 = "23:59"
    end
    return [time1,time2]
  end

  def time_parse(time_1,time_2)
    if time_1.present?
      time1 = time_1
    else
      time1 = "00:00"
    end
    if time_2.present?
      time2 = time_2
    else
      time2 = "23:59"
    end
    return [time1,time2]
  end

  def time_conversion(date_time, zone=nil)

    if zone.present?
      time = Time.parse("#{date_time}")
      time = time.in_time_zone(zone).utc.strftime("%H:%M:%S")
    else
      time = Time.parse("#{date_time}")
      time = time.in_time_zone("Pacific Time (US & Canada)").utc.strftime("%H:%M:%S")
    end
    return time
  end

  def number_valid(phone_number)
    begin
      puts "============= validating ================="
      lookup_client = Twilio::REST::LookupsClient.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN'])
      response = lookup_client.phone_numbers.get("+#{phone_number}")
      response.phone_number
      return true
    rescue => e

      puts "============= Invalid ================="
      return false
    end
  end


  def issue_with_stripe(amount, wallet_id, last_4)
    app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    tx = SequenceLib.issue(wallet_id, amount, wallet_id, "issue", 0, nil, app_config.stringValue, nil, last_4,nil, nil, nil, get_ip)
    @wallet  = Wallet.where(id: wallet_id).first
    if @wallet.present?
      current_balance = @wallet.balance.to_f
      total_balance = current_balance + amount.to_f
      @wallet.update(balance: total_balance)
    end
    tx = {id: tx.id,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.type , destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id}
    return tx
  end

  def check_send_via(send_via)
    if send_via == "ach"
      return "ACH"
    elsif send_via == "email"
      return "Email"
    else
      return send_via
    end
  end

  def withdraw(amount, wallet_id, fee, type = nil, last4=nil, send_via=nil,user_info=nil, send_check_user_info= nil, ledger = nil, sub_type=nil,source=nil,other=nil,pending_state=nil, card = nil)
    @wallet  = Wallet.where(id: wallet_id).first
    location = nil
    merchant = nil

    location = location_to_parse(@wallet.location) if @wallet.location.present?
    merchant = merchant_to_parse(@wallet.location.merchant) if @wallet.location.present?
    if source.present?
      app_config = source
    else
      app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first.stringValue
    end
    tr_type=get_transaction_type(type,sub_type)

    if pending_state
      to = Wallet.check_escrow.first.id
      tx = SequenceLib.transfer(amount, wallet_id, to, tr_type.first, number_with_precision(fee.to_f, precision: 2), nil, app_config, nil, merchant, location, user_info, nil,nil, nil, ledger, nil,tr_type.second,nil,nil,nil,nil,card,nil,nil,nil,nil,nil,nil,nil,to, send_check_user_info)
    else
      if other.present?
        tx = SequenceLib.retire(amount, wallet_id ,tr_type.first, fee, nil, app_config, nil, merchant, location,last4, send_via, user_info, send_check_user_info, get_ip, ledger, tr_type.second,nil,nil,"giftcard")
      else
        tx = SequenceLib.retire(amount, wallet_id ,tr_type.first, fee, nil, app_config, nil, merchant, location,last4, send_via, user_info, send_check_user_info, get_ip, ledger, tr_type.second)
      end
    end


    if @wallet.present?
      current_balance = @wallet.balance.to_f
      total_balance = current_balance - amount.to_f
      @wallet.update(balance: total_balance)
    end
    return tx
  end

  def custom_withdraw(amount, wallet_id, fee, type = nil, last4=nil, send_via=nil,user_info=nil, send_check_user_info= nil, ledger = nil, sub_type=nil,source=nil,pending_state=nil)
    @wallet  = Wallet.where(id: wallet_id).first

    location = nil
    merchant = nil

    location = location_to_parse(@wallet.location) if @wallet.location.present?
    merchant = merchant_to_parse(@wallet.location.merchant) if @wallet.location.present?
    if source.present?
      app_config = source
    else
      app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first.stringValue
    end
    tr_type=get_transaction_type(type,sub_type)
    if pending_state
      to = Wallet.check_escrow.first.id
      tx = SequenceLib.transfer(amount, wallet_id, to, tr_type.first, number_with_precision(fee.to_f, precision: 2), nil, app_config, nil, merchant, location, user_info, nil,nil, nil, ledger, nil,tr_type.second,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,to)
    else
      tx = SequenceLib.custom_retire(amount, wallet_id ,tr_type.first, fee, nil, app_config, nil, merchant, location,last4, send_via, user_info, send_check_user_info, get_ip, ledger, tr_type.second)
    end
    if @wallet.present?
      current_balance = @wallet.balance.to_f
      total_balance = current_balance - amount.to_f
      @wallet.update(balance: total_balance)
    end
    return tx
  end

  def dollars_to_cents(dollars)
    return number_with_precision(dollars.to_f * 100, precision: 2).to_i
  end

  def cents_to_dollars(cents)
    return number_with_precision(cents.to_f / 100, precision: 2)
  end

  def location_to_parse(location)
    if location.present?
      if location.class == Hash
        loc = location
      else
        loc = {}
        loc= {id: location.try(:id),name: location.try(:first_name), email: location.try(:email), business_name: location.try(:business_name)}
      end
      return loc
    end
  end

  def merchant_to_parse(merchant)
    if merchant.present?
      if merchant.class == Hash
        merch= merchant
      else
        merch= {}
        merchant = User.admin_user.find_by_id(merchant.admin_user_id) if merchant.affiliate_program?
        merch= {id: merchant.try(:id),name: merchant.try(:name) ,email: merchant.try(:email), phone_number: merchant.try(:phone_number), owner_name: merchant.try(:owners).try(:first).try(:name)}
      end
      return merch
    end
  end

  def format_dollar_amount(amount)
    return number_with_precision(amount, precision: 2)
  end

  def ach_stripe(token, account_id)
    client = @@ach_client
    exchange_token_response = client.item.public_token.exchange(token)
    access_token = exchange_token_response['access_token']
    item_id = exchange_token_response['item_id']
    puts "item ID: #{item_id}"
    puts stripe_response = client.processor.stripe.bank_account_token.create(access_token, account_id)
    bank_account_token = stripe_response['stripe_bank_account_token']
    return bank_account_token
  end

  def get_business_name(wallet)
    return nil if wallet.blank?
    if wallet.primary?
      dba_name= wallet.try(:location).try(:business_name) || wallet.try(:name)
    elsif wallet.try(:qc_support?)
      dba_name = "QC Support"
    else
      dba_name="#{wallet.try(:location).try(:business_name) || wallet.try(:user).try(:name)} #{wallet.try(:wallet_type).try(:capitalize)} Wallet "
    end
    return dba_name
  end

  def create_transaction_helper(merchant, transaction_to, transaction_from, user, receiver, action, card, fee, tip, privacy_fee, reserve_amount, amount, total_amount, user_info, type, sub_type, tags, gateway_fees,charge, payment_gateway, ip, previous_issue = nil,status = nil, seq_tx_id=nil, timestamp=nil,seq_tags=nil)
    main_type = updated_type(type, sub_type)
    gbox_fee = save_gbox_fee(tags.try(:[],"fee_perc"), fee) || 0
    total_fee = save_total_fee(tags.try(:[],"fee_perc"), fee) || 0
    net_fee = save_net_fee(total_fee,privacy_fee,main_type)
    net_amount = save_net_amount(total_amount,total_fee,reserve_amount,tip,privacy_fee,main_type)
    payment_gateway = payment_gateway.present? ? payment_gateway : @payment_gateway
    sender_dba=get_business_name(Wallet.find_by(id:transaction_from))
    receiver_dba=get_business_name(Wallet.find_by(id:transaction_to))
    if @transaction.present? && [TypesEnumLib::TransactionType::QCSecure, TypesEnumLib::TransactionType::Transfer3DS, TypesEnumLib::TransactionType::InvoiceDebitCard, TypesEnumLib::TransactionType::InvoiceCreditCard, TypesEnumLib::TransactionType::InvoiceRTP, TypesEnumLib::TransactionType::RTP].include?(type)
      @transaction.update(
          last4: card.try(:last4) || previous_issue.try(:[],:tags).try(:[],"card").try(:[], "last4"),
          first6: card.try(:first6) || previous_issue.try(:[],:tags).try(:[],"card").try(:[], "first6"),
          fee: total_fee.to_f,
          tip: tip.to_f,
          privacy_fee: privacy_fee.to_f,
          reserve_money: reserve_amount.to_f,
          net_fee: number_with_precision(net_fee.to_f, precision: 2),
          net_amount: number_with_precision(net_amount, precision: 2),
          total_amount: total_amount.to_f,
          discount: previous_issue.try(:[],"discount").to_f,
          gbox_fee: gbox_fee.to_f,
          iso_fee: save_iso_fee(tags.try(:[],"fee_perc")),
          agent_fee: user_info.try(:[],"agent").try(:[],"amount").to_f,
          affiliate_fee: user_info.try(:[],"affiliate").try(:[],"amount").to_f,
          ip: ip,
          main_type: type,
          tags: seq_tags.present? ? seq_tags : tags,
          card_id: card.try(:id) ||  previous_issue.try(:[],:tags).try(:[],"card").try(:[], "id"),
          account_processing_limit: gateway_fees.try(:[],:account_processing_limit),
          transaction_fee: gateway_fees.try(:[],:transaction_fee),
          charge_back_fee: gateway_fees.try(:[],:charge_back_fee),
          retrieval_fee: gateway_fees.try(:[],:retrieval_fee),
          per_transaction_fee: gateway_fees.try(:[],:per_transaction_fee),
          reserve_money_fee: gateway_fees.try(:[],:reserve_money_fee),
          reserve_money_days: gateway_fees.try(:[],:reserve_money_days),
          monthly_service_fee: gateway_fees.try(:[],:monthly_service_fee),
          misc_fee: gateway_fees.try(:[],:misc_fee),
          misc_fee_dollars: gateway_fees.try(:[],:misc_fee_dollars),
          payment_gateway_id: payment_gateway.try(:id),
          load_balancer_id: payment_gateway.try(:load_balancer_id),
          charge_id: charge.try(:[], :response),
          seq_transaction_id: seq_tx_id,
          timestamp: timestamp,
          sender_balance: SequenceLib.balance(transaction_from).to_f,
          receiver_balance: SequenceLib.balance(transaction_to).to_f
      )
      return @transaction
    else
      merchant.transactions.create(
          to: transaction_to,
          from: transaction_from,
          status: status || "pending",
          amount: amount,
          sender: user,
          sender_name: sender_dba,
          receiver_id: receiver.try(:id),
          receiver_name: receiver_dba,
          receiver_wallet_id: transaction_to,
          sender_wallet_id: transaction_from,
          action: action,
          last4: card.try(:last4) || previous_issue.try(:[],:tags).try(:[],"card").try(:[], "last4"),
          first6: card.try(:first6) || previous_issue.try(:[],:tags).try(:[],"card").try(:[], "first6"),
          fee: total_fee.to_f,
          tip: tip.to_f,
          privacy_fee: privacy_fee.to_f,
          reserve_money: reserve_amount.to_f,
          net_fee: number_with_precision(net_fee.to_f, precision: 2),
          net_amount: number_with_precision(net_amount, precision: 2),
          total_amount: total_amount.to_f,
          discount: previous_issue.try(:[],"discount").to_f,
          gbox_fee: gbox_fee.to_f,
          iso_fee: save_iso_fee(tags.try(:[],"fee_perc")),
          agent_fee: user_info.try(:[],"agent").try(:[],"amount").to_f,
          affiliate_fee: user_info.try(:[],"affiliate").try(:[],"amount").to_f,
          ip: ip,
          main_type: main_type,
          sub_type: sub_type,
          tags: seq_tags.present? ? seq_tags : tags,
          card_id: card.try(:id) ||  previous_issue.try(:[],:tags).try(:[],"card").try(:[], "id"),
          account_processing_limit: gateway_fees.try(:[],:account_processing_limit),
          transaction_fee: gateway_fees.try(:[],:transaction_fee),
          charge_back_fee: gateway_fees.try(:[],:charge_back_fee),
          retrieval_fee: gateway_fees.try(:[],:retrieval_fee),
          per_transaction_fee: gateway_fees.try(:[],:per_transaction_fee),
          reserve_money_fee: gateway_fees.try(:[],:reserve_money_fee),
          reserve_money_days: gateway_fees.try(:[],:reserve_money_days),
          monthly_service_fee: gateway_fees.try(:[],:monthly_service_fee),
          misc_fee: gateway_fees.try(:[],:misc_fee),
          misc_fee_dollars: gateway_fees.try(:[],:misc_fee_dollars),
          payment_gateway_id: payment_gateway.try(:id),
          load_balancer_id: payment_gateway.try(:load_balancer_id),
          charge_id: charge.try(:[], :response),
          seq_transaction_id: seq_tx_id,
          timestamp: timestamp,
          sender_balance: SequenceLib.balance(transaction_from).to_f,
          receiver_balance: SequenceLib.balance(transaction_to).to_f
      )
    end
  end

  def transaction_between(to, from, amount, reference, fee, fee_type = nil, data = nil , ledger = nil, source = nil,sub_reference=nil,credit_check=nil,card=nil,tip_in_qr_sale=nil,trans_id=nil,payment_gateway=nil,card_type=nil, privacy_fee = nil,flag=nil,qr_scan_tx_amount=nil,load_fee=nil,b2b_case=nil)
    reserveAmount = 0
    fee = 0
    @to = Wallet.find(to)
    @from = Wallet.find_by_id(from)
    to_user = @to.users.first
    from_user = @from.users.first if @from.present?
    fee_lib = FeeLib.new
    pay_gat = payment_gateway
    if source.present?
      app_config = source
    else
      app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first.stringValue
    end
    if to_user.present? && to_user.MERCHANT?
      # qc_wallet = Wallet.where(wallet_type: 3).first
      location = @to.location
      location_fee = location.fees if location.present?
    end
    if to_user.present? && from_user.present? && to_user.MERCHANT? && @to.wallet_type != 'tip' && !from_user.MERCHANT? && !from_user.qc? && !b2b_case
      # case of to_user is merchant and from_user not merchant---------------
      if location_fee.present?
        #--------- Check if tip present ----------
        if tip_in_qr_sale.present?
          tip_amount = amount.to_f + tip_in_qr_sale[:sale_tip].to_f
        else
          tip_amount = amount.to_f
        end
        if privacy_fee.present? && qr_scan_tx_amount.blank?
          tip_amount = tip_amount.to_f + privacy_fee.to_f
        end
        # ---------- BuyRAte calculation start --------------
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location,nil,payment_gateway, nil, load_fee )
        #---------------get fee transaction Fee Mobile App
        if credit_check == TypesEnumLib::TransactionType::CreditTransaction #=----- Credit Card Fee----------------
          # C2B Case
          fee = fee_class.apply(qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : tip_amount.to_f, @invoice_request.present? ? 'invoice_credit' : 'credit')
        elsif credit_check == TypesEnumLib::TransactionType::DebitTransaction  #=---- Debit Card fee----------
          fee = fee_class.apply(qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : tip_amount.to_f,@invoice_request.present? ? 'invoice_debit' : 'debit')
        elsif ["rtp","invoice_rtp"].include?(credit_check)
          fee = fee_class.apply(qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : tip_amount.to_f,credit_check)
        else                           #=---- Mobile App fee----------
          fee = fee_class.apply(tip_amount.to_f)
        end
        fee["amount"] = tip_amount if qr_scan_tx_amount.present?
        # ------ buyrate calculation end  --------------
        reserve_money = fee_object.reserve_fee.to_f
        reserveAmount = deduct_fee(reserve_money,amount)
        reserve_detail = {amount: reserveAmount, days: fee_object.days, wallet: location.wallets.reserve.first.id} if reserve_money > 0
        #= Sales with tip
        tr_type = get_transaction_type(reference,sub_reference)
        feeExists = false
        if fee.present?
          feeExists = true
          user_info = fee["splits"]
          if reserve_detail.present?
            reserve = {"reserve" => reserve_detail}
            user_info = user_info.merge(reserve)
          end
          pay_gat = PaymentGateway.find_by(key: source) if pay_gat.blank?
          gateway_fees = User.get_gateway_details(pay_gat) if pay_gat.present?

          gateway_name = pay_gat.try(:name)
          if pay_gat.try(:knox_payments?) || pay_gat.try(:payment_technologies?) || pay_gat.try(:total_pay?)
            gateway_name = params[:bank_descriptor].present? ? params[:bank_descriptor] : pay_gat.try(:name)
            app_config = params[:bank_descriptor].present? ? params[:bank_descriptor] : app_config
          end

          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => gateway_name,
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          if data.present? && data[:complete_trans].present?
            # to reject whole issue transaction present in data variable
            data = data.reject{|k| k == :complete_trans}
          end
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, qr_scan_tx_amount.present? ? qr_scan_tx_amount.to_f : amount.to_f, tip_amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data)
          tx = SequenceLib.transfer(tip_amount, from, to, tr_type.first, number_with_precision(fee['fee'].to_f, precision: 2), nil, app_config, nil, location.merchant, location, user_info, nil,get_ip, reserve_detail, ledger, data,tr_type.second,nil,nil,nil,nil,card,tip_in_qr_sale,nil,payment_gateway,privacy_fee,card_type,gateway_fees,flag)
        else
          tx = SequenceLib.transfer(tip_amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil,nil,get_ip, nil,ledger,data,tr_type.second,nil,nil,nil,nil,card,tip_in_qr_sale,nil,payment_gateway,privacy_fee,card_type,gateway_fees,flag)
        end
        if tx.present?
          parsed_transactions = parse_block_transactions(tx.actions, tx.timestamp)
          if tip_in_qr_sale.present?
            loc_wallet = Wallet.find(to) if to.present?
            merchant = loc_wallet.try(:location).try(:merchant)
            tip_block_txn = parsed_transactions.second
            tip_block_txn[:tags]["sequence_id"] = tip_block_txn[:seq_parent_id]
            create_transaction_helper(merchant, tip_in_qr_sale[:wallet_id], to, merchant, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
          end
          if reserveAmount.to_f > 0 && reserve_detail.present?
            reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
            if reserve_tx.present?
              create_transaction_helper(to_user, reserve_detail[:wallet], to, to_user, to_user, "transfer", nil, nil,nil,nil, nil, reserveAmount.to_f, reserveAmount.to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip, nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
            end
          end
          save_block_trans(parsed_transactions) if parsed_transactions.present?
        end
      end
    elsif to_user.present? && from_user.present? && to_user.MERCHANT? && from_user.MERCHANT? && @to.wallet_type != 'tip'
      # if to_user and from_user are both merchants
      #=-------------- B2b Transfer
      if location_fee.present?
        #get fee object
        fee_object = location_fee.buy_rate.first
        type = b2b_case ? TypesEnumLib::CommissionType::B2B : nil
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location, type)
        #get b2b fee value
        fee = fee_class.apply(amount.to_f)
        tr_type = get_transaction_type(reference,sub_reference)
        if fee.present?
          # get users information
          user_info = fee["splits"]
          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => pay_gat.try(:name),
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, amount.to_f, amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data)
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee["fee"], nil, app_config, nil, location.merchant, location, user_info,nil,get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        end
        if tx.present? && fee.try(:[], "fee").to_f > 0 && b2b_case
          parsed_transactions = parse_block_transactions(tx.actions, tx.timestamp)
          loc_wallet = Wallet.find(to) if to.present?
          merchant = loc_wallet.try(:location).try(:merchant)
          fee_block_txn = parsed_transactions.select{|v| v[:type] == "B2B Fee"}.first
          fee_block_txn[:tags]["sequence_id"] = fee_block_txn[:seq_parent_id]
          fee = SequenceLib.dollars(fee_block_txn[:fee].to_f)
          create_transaction_helper(merchant, fee_block_txn[:receiver], to, merchant, fee_block_txn[:receiver], fee_block_txn[:action], nil, fee, nil, nil, nil, fee, fee, nil, fee_block_txn[:type], fee_block_txn[:sub_type], fee_block_txn[:tags], nil, nil, fee_block_txn[:gateway], nil, nil,"approved", fee_block_txn[:id], fee_block_txn[:timestamp])
        end
      end
    elsif to_user.present? && from_user.present? && to_user.merchant? && from_user.qc?
      #= request money case if sender is qc and receiver is merchant
      #=-------------- B2b Transfer
      if location_fee.present?
        #get fee object
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location, TypesEnumLib::CommissionType::B2B)
        #get b2b fee value
        fee = fee_class.apply(amount.to_f)
        tr_type = get_transaction_type(reference,sub_reference)
        if fee.present?
          # get users information
          user_info = fee["splits"]
          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => pay_gat.try(:name),
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, amount.to_f, amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data)
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee["fee"], nil, app_config, nil, location.merchant, location, user_info,nil,get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
          # divide fee into iso, agent, partner
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        end
      end
    elsif to_user.present? && to_user.MERCHANT? && from_user.nil?
      # if to_user is merchant and from_user is not present
      #= QRCard Case
      if location_fee.present?
        #----------------get object of Mobile app fee
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location)
        #---------------get fee transaction Fee Mobile App
        if credit_check == TypesEnumLib::TransactionType::CreditTransaction #=----- Credit Card Fee
          fee = fee_class.apply(amount.to_f, 'credit')
        else                           #=---- Mobile App fee
          fee = fee_class.apply(amount.to_f)
        end
        tr_type = get_transaction_type(reference,sub_reference)
        if fee[:fee].present?
          # get users information
          # user_info = fee_lib.get_merchant_users_info(location, fee_object, company, company_wallet, cus_fee_ded, actual_fee, reserve_fee,fee_dollar_object)
          user_info = nil
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee[:fee], nil, app_config, nil, location.merchant, location, user_info, nil, get_ip, fee_object.days,ledger,data,tr_type.second,nil,nil,nil,nil,card)
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data, tr_type.second)
        end
      end

    elsif (!to_user.MERCHANT? && from_user.MERCHANT?) || from_user.PARTNER?
      tx = SequenceLib.transfer(amount, from, to, reference, fee, nil, app_config, nil, nil,nil, nil,nil, get_ip, nil, ledger, data)
    elsif to_user.present? && from_user.present? && to_user.MERCHANT? && !from_user.MERCHANT? && b2b_case
      # if to_user is merchant and from_user are iso,agent,affi
      #=-------------- B2b Transfer
      if location_fee.present?
        #get fee object
        fee_object = location_fee.buy_rate.first
        fee_class = Payment::FeeCommission.new(from_user, to_user, fee_object, @to, location,TypesEnumLib::CommissionType::B2B)
        #get b2b fee value
        fee = fee_class.apply(amount.to_f)
        tr_type = get_transaction_type(reference,sub_reference)
        if fee.present?
          # get users information
          user_info = fee["splits"]
          tags = {
              "fee_perc" => user_info,
              "gateway_fee_details" => gateway_fees,
              "previous_issue" => data,
              "descriptor" => pay_gat.try(:name),
              "location" => location_to_parse(location),
              "merchant" => merchant_to_parse(location.try(:merchant)),
              "payment_gateway_id" => pay_gat.try(:id)

          }
          db_transaction = create_transaction_helper(to_user, to, from, from_user, to_user,"transfer", card, fee.try(:[], "fee").to_f, tip_in_qr_sale.try(:[], :sale_tip).to_f,privacy_fee, reserveAmount, amount.to_f, amount.to_f, user_info, tr_type.first, tr_type.second, tags, gateway_fees, nil, pay_gat,get_ip, data)
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee["fee"], nil, app_config, nil, location.merchant, location, user_info,nil,get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        else
          tx = SequenceLib.transfer(amount, from, to, tr_type.first, 0, nil, app_config, nil,location.merchant, location,nil, nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
        end
        parsed_transactions = parse_block_transactions(tx.actions, tx.timestamp) if tx.present?
        if tx.present? && to_user.merchant? && (from_user.iso? || from_user.agent? || from_user.affiliate?) && fee.try(:[], "fee").to_f > 0 && b2b_case
          loc_wallet = Wallet.find(to) if to.present?
          merchant = loc_wallet.try(:location).try(:merchant)
          fee_block_txn = parsed_transactions.select{|v| v[:type] == "B2B Fee"}.first
          fee_block_txn[:tags]["sequence_id"] = fee_block_txn[:seq_parent_id]
          fee = SequenceLib.dollars(fee_block_txn[:fee].to_f)
          create_transaction_helper(merchant, fee_block_txn[:receiver], to, merchant, fee_block_txn[:receiver], fee_block_txn[:action], nil, fee, nil, nil, nil, fee, fee, nil, fee_block_txn[:type], fee_block_txn[:sub_type], fee_block_txn[:tags], nil, nil, fee_block_txn[:gateway], nil, nil,"approved", fee_block_txn[:id], fee_block_txn[:timestamp])
        end
        save_block_trans(parsed_transactions) if parsed_transactions.present?
      end
    else
      #========== for sale with paytip process
      #========== if both to_user and from_user are not merchants
      tr_type =  get_transaction_type(reference, sub_reference)
      spending_limit = fee_lib.get_limit(amount,'transfer')
      raise StandardError.new "Your weekly spending limit is: #{fee_lib.show_limit('transfer')}" if !spending_limit
      tx = SequenceLib.transfer(amount, from, to, tr_type.first, fee, nil, app_config, nil,nil,nil,nil,nil, get_ip,nil,nil,data,tr_type.second,nil,nil,nil,nil,card)
    end
    if @to.present?
      #updating balance of @to wallet
      current_balance = @to.balance.to_f
      total_balance = current_balance + amount.to_f
      @to.update(balance: total_balance)
      to_user = @to.users.first
    end
    if from_user.present?
      # if from_user is present then parse transaction
      if @from.present?
        #updating balance of @from wallet
        current_balance = @from.balance.to_f
        total_balance = current_balance - amount.to_f
        @from.update(balance: total_balance)
      end
      if tx.blank?
        return nil
      else
      end
      # raise StandardError.new 'Transaction Failed!'
      if tx.present? && tx.actions.first.tags["fee_perc"].present?
        if tx.actions.first.tags["fee_perc"]["iso"].present? && tx.actions.first.tags["fee_perc"]["iso"]["amount"].to_f > 0
          iso_fee = tx.actions.first.tags["fee_perc"]["iso"]["amount"].to_f
          if tx.actions.first.tags["fee_perc"]["agent"].present? && tx.actions.first.tags["fee_perc"]["agent"]["amount"].to_f > 0
            iso_fee = iso_fee - tx.actions.first.tags["fee_perc"]["agent"]["amount"].to_f
            agent_fee=tx.actions.first.tags["fee_perc"]["agent"]["amount"].to_f
          end
          if tx.actions.first.tags["fee_perc"]["affiliate"].present? && tx.actions.first.tags["fee_perc"]["affiliate"]["amount"].to_f > 0
            agent_fee = agent_fee - tx.actions.first.tags["fee_perc"]["affiliate"]["amount"].to_f
            affiliate_fee=tx.actions.first.tags["fee_perc"]["affiliate"]["amount"].to_f
          end
        end
        total_bonus=tx.actions.first.tags["fee_perc"]["total_bonus"] if tx.actions.first.tags["fee_perc"]["total_bonus"].present?
      end
      transaction_type = ""
      if tx.present?
        transaction_type = tx.actions.first.tags["type"] if tx.actions.first.present?
        gateway_fee_details = tx.actions.first.tags["gateway_fee_details"] if tx.actions.first.present?
        tx = {id: tx.id,timestamp: tx.timestamp, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] ,transaction_id: tx.actions.first.id, destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount , source: tx.actions.first.source_account_id, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), tags: tx.actions.first.tags, iso_fee: iso_fee || 0, agent_fee: agent_fee || 0, affiliate_fee: affiliate_fee || 0, bonus: total_bonus || 0, card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"), charge_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"transaction_id"),gateway_fee_details: gateway_fee_details, seq_tx:tx}
      end
      if tx.present? && @to.present? && @from.present?
        to_user = @to.users.first
        if to_user.nil?
          to_user = @to.oauth_app.user
        end
        from_user = @from.users.first
        if from_user.nil?
          from_user = @from.oauth_app.user
        end
        if to_user.present?
          db_transaction.update(status: "approved",tags: tx[:tags], charge_id: tx[:charge_id],seq_transaction_id: tx[:transaction_id], timestamp: tx[:timestamp])
          if db_transaction.present?
            params[:db_trans_id] = db_transaction.id
            params[:db_quickcard_id] = db_transaction.quickcard_id
          end
          hold_amount = amount.to_f
          if fee.present? && (fee.try(:[], :fee).present? || fee.try(:[], "fee").present?)
            # fee_amount = fee.try(:[], :fee).to_f || fee.try(:[], "fee").to_f
            fee_amount = fee.try(:[], :fee).to_f
            fee_amount = fee.try(:[], "fee").to_f if fee_amount == 0
            hold_amount = hold_amount - fee_amount.to_f
          end

          if reserveAmount.present? && reserveAmount > 0
            hold_amount = hold_amount - reserveAmount
          end

          if tx[:privacy_fee].to_f > 0
            hold_amount = hold_amount + tx[:privacy_fee].to_f
          end
          HoldInRear.hold_money_in_my_wallet(to,hold_amount.to_f,reserveAmount.to_f) if transaction_type.present? && [TypesEnumLib::TransactionType::QCSecure,TypesEnumLib::TransactionType::Transfer3DS,"sale",TypesEnumLib::TransactionType::InvoiceDebitCard, TypesEnumLib::TransactionType::InvoiceCreditCard, TypesEnumLib::TransactionType::RTP, TypesEnumLib::TransactionType::InvoiceRTP].include?(transaction_type)
        end
      end
      return tx
    else
      if tx.present?
        tx = {id: tx.id,timestamp: tx.timestamp, type: tx.actions.first.type ,transaction_id: tx.actions.first.id, destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount , source: tx.actions.first.source_account_id, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), tags: tx.actions.first.tags,db_trans_id: nil, card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id"), charge_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"transaction_id"), seq_tx:tx}
      end
      return tx
    end
  end
  # removed card_token from params it may create problem .. TODO Need to fix
  def issue_from_stripe(amount, customer,user_id = nil,card_number=nil, key = nil,merchant_id=nil,card=nil,transaction_hash=nil,gateway=nil,payment_gateway=nil,trans_id=nil,card_cvv=nil,virtual_transaction_debit=nil, billing_address=nil)
    payment_gateway_id = payment_gateway.id if payment_gateway.present?
    gateway = payment_gateway.present? ? payment_gateway.key : gateway
    card_lib = CardToken.new
    stripe = Payment::StripeGateway.new
    card_bin_list = get_card_info(card_number)
    if amount.to_f > 0
      charge = stripe.stripe_charge(amount, card, key,user_id,customer,card_cvv,virtual_transaction_debit, billing_address)
      # charge = card_lib.stripe_charge(amount, customer, key)
      if not charge[:charge].nil?
        if charge[:charge][:error].present? #= if when charge is failed or blocked
          message = {message: "Card Charge Error - #{charge[:error_code]}: Decline From Bank"}
          transaction_hash = (transaction_hash.merge(message)).to_json
          order_bank = OrderBank.new(merchant_id: merchant_id,user_id: user_id,bank_type: gateway,card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : card.try(:card_type), card_id: card.try(:id),status: "decline",amount: amount,transaction_info: transaction_hash, payment_gateway_id: payment_gateway_id ,transaction_id: trans_id)
          card.decline_attempts = card.decline_attempts.to_i + 1
          card.last_decline= DateTime.now.utc
          card.save
          return {response: nil, message: charge[:message], order_bank: order_bank}
        else
          order_bank = OrderBank.new(merchant_id: merchant_id,user_id: user_id,bank_type: gateway,card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : card.try(:card_type), card_id: card.try(:id),status: "approve",amount: amount,transaction_info: transaction_hash.to_json, payment_gateway_id:payment_gateway_id ,transaction_id: trans_id)
          card.decline_attempts = 0
          card.last_decline = nil
          card.save
        end
        return {response: charge[:charge][:id], message: message,charge: charge, order_bank:order_bank}
      else
        message = {message: "Card Charge Error - #{charge[:error_code]}: #{charge[:message]}"}

        transaction_hash = (transaction_hash.merge(message)).to_json
        order_bank = OrderBank.new(merchant_id: merchant_id,user_id: user_id,bank_type: gateway,card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : card.try(:card_type), card_id: card.try(:id),status: "decline",amount: amount,transaction_info: transaction_hash, payment_gateway_id:payment_gateway_id,transaction_id: trans_id )
        # order_bank = OrderBank.create(merchant_id: merchant_id,user_id: user_id,bank_type: gateway,card_type: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card.try(:brand),card_sub_type: card_bin_list["type"].present? ? card_bin_list["type"] : card.try(:card_type),status: "decline",amount: amount,transaction_info: transaction_hash, payment_gateway_id:payment_gateway_id )
        card.decline_attempts = card.decline_attempts.to_i + 1
        card.last_decline= DateTime.now.utc
        card.save
        return {response: nil, message: message, order_bank:order_bank}
      end
    end
  end

  def get_transaction_type(type, sub_type=nil)
    t = TypesEnumLib::TransactionType
    st = TypesEnumLib::TransactionSubType
    return 'Unknown' if type.nil?
    case type
    when type.downcase == t::SendCheck     #== Send Check
      return [t::SendCheck, nil]
    when type.downcase == t::GiftCard     #== Giftcard buy
      return [t::GiftCard,nil]
    when t::ACHdeposit      #== pay invoice from admin
      return [t::ACHdeposit,nil]
    when t::SaleType         #== Sales
      if sub_type == st::ICanPay3ds       #== Sale with Tip
        return [t::SaleType, st::ICanPay3ds]
      end
      if sub_type == st::SaleTip       #== Sale with Tip
        return [t::SaleType, st::SaleTip]
      end
      if sub_type == st::SaleGiftcard     #== Sale with QRCard
        return [t::SaleType, st::SaleGiftcard]
      end
      if sub_type == st::SaleIssue     #== Sale Issue
        return [t::SaleType, st::SaleIssue]
      end
      if sub_type == st::DebitCharge
        return [t::SaleType, st::DebitCharge]
      end
      if sub_type == st::SaleVirtual      #=-- Virtual Sale
        return [t::SaleType, st::SaleVirtual]
      end
      if sub_type == st::SaleVirtualApi
        return [t::SaleType, st::SaleVirtualApi]
      end
      if sub_type == st::QRCreditCard
        return [t::SaleType, st::QRCreditCard]
      end
      if sub_type == st::VirtualTerminalTransaction
        return [t::SaleType, st::VirtualTerminalTransaction]
      end
      if sub_type == st::DebitCard
        return [t::SaleType, st::DebitCard]
      end
      if sub_type == st::SaleIssueApi
        return [t::SaleType, st::SaleIssueApi]
      end
      if sub_type == st::Issue3DS
        return [t::Issue3DS, st::Issue3DS]
      end
      return [t::SaleType, nil]
    when t::B2b      #== Request money from merchant
      return [t::B2b,nil]
    when t::Reserve      #== Reserve Deposit
      return [t::Reserve,nil]
    when t::ReserveTransfer      #== Reserve Money Return
      return [t::ReserveTransfer,nil]
    when t::VoidCheck      #== Void check by admin
      return [t::VoidCheck,nil]
    when t::PayedTip      #== Tip Paid from merchant wallets to employee wallets
      return [t::PayedTip,nil]
    when t::Fee      #== Fee Transfer
      return [t::Fee,nil]
    when t::ApproveRequest
      return [t::ApproveRequest,nil]
    when t::ACH
      return [t::ACH,nil]
    else
      return ["#{type}",nil]
    end

  end
  #= For transaction types
  def updated_type(type, sub_type=nil,t=nil)
    return "CBK Won" if sub_type == 'cbk_won'
    if type == "Fee_Transfer"
      return "CBK Fee" if sub_type == 'cbk_fee' || sub_type == 'charge_back_fee' || sub_type == 'cbk_fee_fee'
      return "Retrieval Fee" if sub_type == 'retrievel_fee' || sub_type == 'retrievel_fee_fee'
    end
    return "CBK Lost" if sub_type =='cbk_lost'
    return "Dispute Accepted" if sub_type == 'cbk_dispute_accepted'
    return "CBK Hold" if type=='cbk_hold' || type=='charge_back_hold'
    if t.present?
      if t["source"].present?
        return "CBK Won" if t["sub_type"] == 'cbk_won' || t["source"]=='charge_back_won' || t["source"]=='cbk_won' || t["fee_type"]=='cbk_won'
        return "CBK Lost" if t["sub_type"] =='cbk_lost' || t["source"]=='charge_back_lost' || t["source"]=='cbk_lost' || t["fee_type"]=='cbk_lost'
      end
    end
    return 'Unknown' if type.nil?
    case type
    when "bulk_check"
      return "Bulk Check"
    when "send_check"
      return "Send Check"
    when "giftcard_purchase"
      return "Giftcard Purchase"
    when "giftcard"
      return "Giftcard Purchase"
    when "ACH_deposit"
      return "ACH Deposit"
    when "sale"
      if sub_type.present?
        if sub_type == "sale_tip" || sub_type == "tip_vt_transaction_&_debit"
          return "Sale Tip"
        end
        if sub_type == "sale_giftcard"
          return "Sale Giftcard"
        end
        if sub_type == "sale_issue"
          return "Sale Issue"
        end
        if sub_type == "sale_issue_api"
          return "Sale Issue"
        end
        if sub_type == "debit_charge" || sub_type == "Pin Gateway"
          return "PIN Debit"
        end
        if sub_type == "virtual_terminal_sale"
          return "Virtual Terminal"
        end
        if sub_type == "virtual_terminal_sale_api"
          return "eCommerce"
        end
        if sub_type == "QR_Scan" || sub_type == "qr_credit_card"
          return "Sale Issue"
        end
        if sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
          return "Credit Card"
        end
        if sub_type == TypesEnumLib::TransactionSubType::Issue3DS
          return TypesEnumLib::TransactionSubType::Issue3DS
        end
        if sub_type == TypesEnumLib::TransactionSubType::IssueQCSecure
          return TypesEnumLib::TransactionSubType::IssueQCSecure
        end
        if sub_type == TypesEnumLib::TransactionSubType::DebitCard
          return TypesEnumLib::TransactionType::DebitCard
        end
      end
      return "Sale"
    when "approve_request_issue"
      return "Approve Request Issue"
    when "b2b_transfer"
      return "B2B Transfer"
    when "Reserve_Money_Deposit"
      return "Reserve Money Deposit"
    when "Reserve_Money_Return"
      return "Reserve Money Return"
    when "Void_Check"
      return "Void Check"
    when "Void_Ach"
      return "Void ACH"
    when "Failed_Ach"
      return "Failed ACH"
    when "send_check_void"
      return "Void Check"
    when "payed_tip"
      return "Payed Tip"
    when "Fee_Transfer"  || "fee"
      return "Fee Transfer"
    when "charge" || "Charge"
      return "Mobile Charge"
    when "B2B Fee"
      return "B2B Fee"
    else
      return type.present? ? "#{type}".split('_').map{|m| m.capitalize}.join(' ') : nil
    end
  end

  def sale_text_message(amount = nil, location_name=nil,gateway=nil,card_number=nil,user = nil,location_number=nil)
    number = location_number || "4259546595"
    return "Dear #{user}, your card (x-#{card_number}) has been successfully charged #{amount} USD for your purchase from #{location_name}. This transaction will appear on your statement as #{gateway}. For questions or assistance, please contact #{number_to_phone(number, area_code: true) }."
    # return "Thank you for your $#{amount} purchase at #{merchant_name}. Please note: Your credit or debit card statement will reflect this transaction as a charge from: #{bank_name}. For questions or assistance, please contact us at: (425) 954-6595"
  end


  def refund_receipt(location_id, amount, user_id, date, card_brand, card_number, transaction_id = nil , txn)
    if txn.present?
      UserMailer.refund_email(location_id, amount, user_id, date, card_brand, card_number, transaction_id).deliver_later unless txn.try(:minfraud_result).present?
    end
  end

  def refund_text_message(amount, location_name, card_brand, card_number, user, location_number = nil)
    number = location_number || "4259546595"
    return "Dear #{user}, a refund has been posted back to your card (#{card_brand}-#{card_number}) for the amount of $#{number_with_precision(amount, precision: 2)} for your purchase from #{location_name}. For questions or assistance, please contact #{number_to_phone(number, area_code: true)}"
  end

  def all_transactions(source_id, destination_id, page_number, ledger = nil)
    transactions = SequenceLib.transactions(source_id,destination_id, '', page_number, nil,ledger)
    unless  transactions.nil?
      transactions = parse_transactions(transactions, source_id)
    end
    transactions.compact
  end

  def all_company_transactions(source_id, destination_id, page_number = nil, ledger = nil, first_date = nil, second_date = nil)
    #this is for getting transactions of agent, iso, partner/company
    wallets = [source_id,destination_id]
    transactions = SequenceLib.query_transactions(wallets, '', ledger,first_date,second_date) #get transactions from sequence against each merchant wallet
    # transactions = SequenceLib.transactions(source_id,destination_id, '', page_number, nil,ledger)
    if  transactions.present?
      list = parse_company_transactions(source_id,transactions[:items])
      return [list.compact, transactions[:cursor],transactions[:last_page]]
    else
      return [nil, nil, nil]
    end
  end

  def all_company_export(source_id, destination_id, ledger = nil, first_date = nil, second_date = nil,count=nil,cursor=nil)
    #this is for getting transactions of agent, iso, partner/company in export
    wallets = [source_id,destination_id]
    transactions = SequenceLib.query_export_transactions(wallets, '', ledger,first_date,second_date, count, cursor) #get transactions from sequence against each merchant wallet
    # transactions = SequenceLib.transactions(source_id,destination_id, '', page_number, nil,ledger)
    if transactions.present?
      if transactions[:items].present?
        list = parse_company_transactions(source_id,transactions[:items])
      else
        list = parse_company_transactions(source_id,transactions)
      end
      return [list.flatten.compact, transactions[:cursor],transactions[:last_page]]
    else
      return []
    end
  end

  def all_transactions_with_time(account_id, time, time2, type = nil, sub_type = nil,from_export_worker=nil,count=nil, cursor= nil)
    list = []
    qc_wallet = Wallet.qc_support.first
    transactions = SequenceLib.transaction_with_time(account_id, time, time2, qc_wallet.id, type, sub_type,from_export_worker ,count, cursor)
    return list if transactions.blank?
    unless transactions.blank?
      if from_export_worker
        parse_transactions = parse_report_transactions(transactions[:items], account_id)
      else
        transactions = parse_report_transactions(transactions, account_id)
      end
    end
    if from_export_worker
      return [parse_transactions.compact.flatten, transactions[:cursor],transactions[:last_page]]
    else
      transactions.compact
    end
  end

  def reports_transactions(account_id, time, time2, type = nil, sub_type = nil,from_export_worker=nil,count=nil, cursor = nil)
    # this function is for getting transactions for transactions for tip and reserve wallet
    # qc_wallet = Wallet.qc_support.first
    list = []
    transactions = SequenceLib.transaction_with_time(account_id, time, time2, nil, type, sub_type,from_export_worker,count,cursor)
    return list if transactions.blank?
    unless transactions.blank?
      if from_export_worker
        parse_transactions = parse_reports(transactions[:items], account_id)
      else
        transactions = parse_reports(transactions, account_id)
      end
    end
    if from_export_worker
      return [parse_transactions.flatten.compact, transactions[:cursor],transactions[:last_page]]
    else
      transactions.compact
    end
  end

  def all_merchant_wallets_transactions(wallets,type, ledger = nil,start_date=nil,end_date=nil)
    list = []
    transactions = SequenceLib.user_array_transactions(wallets, type, ledger,start_date,end_date) #get transactions from sequence against each merchant wallet
    unless transactions.nil?
      list.push(parse_merchant_transactions(transactions, nil)) # parsing transactions for showing
    end
    list.flatten.compact
  end

  def all_merchant_transactions(wallets,type, ledger = nil,start_date=nil,end_date=nil,count=nil, cursor=nil)
    list = []
    transactions = SequenceLib.query_transactions(wallets, type, ledger,start_date,end_date,count,cursor) #get transactions from sequence against each merchant wallet
    return list if transactions.blank?
    unless transactions.nil?
      list.push(parse_merchants_transactions(transactions, nil)) # parsing transactions for showing
    end
    return [list.flatten.compact, transactions[:cursor],transactions[:last_page]]
  end

  def single_page_transactions(source_id, destination_id, next_query)
    data = {}
    page = SequenceLib.single_transactions(source_id, destination_id, '', 20, next_query)
    if page.present?
      transactions = page.items
      if transactions.present?
        transactions = parse_mobile_transactions(transactions, source_id).compact
        data = {
            transactions: transactions,
            next: page.cursor,
            last_page: page.last_page
        }
      end
    end
    data
  end

  def all_transactions_except(source_id, destination_id, type)
    transactions = SequenceLib.transactions(source_id, destination_id, type, 0)
    unless  transactions.nil?
      transactions = parse_transactions(transactions, source_id)
    end
    transactions
  end

  def between_us_transactions(source_id, destination_id, page_number)
    transactions = SequenceLib.between_transactions(source_id,destination_id, '', 0)
    unless  transactions.nil?
      transactions = parse_transactions(transactions, source_id)
    end
    transactions
  end

  def show_balance(wallet_id, ledger = nil)
    if wallet_id.present?
      SequenceLib.balance(wallet_id, ledger)
    end
  end

  def push_notification(message, user)
    if user.setting.push_notification
      unless user.registeration_id.nil?
        NotificationWorker.perform_async(message,user.registeration_id, user.device_type )
      end
    end
  end

  def send_text_message(body,to)
    begin
      TextsmsWorker.perform_async(to,body)
      return true
    rescue Exception => exc
      puts "=======EXCEPTION - SENDING TEXT MESSAGE======", exc.message
      return false
    end
  end

  def calculate_fees(allFee, divide, location_fee = nil)
    agent = 0
    iso = 0
    partner = 0
    if divide["partner_fee"].present? || divide["partner_check_fee"].present?
      partner =divide["partner_fee"].present? ? divide["partner_fee"].to_f : 0
      partner1 =divide["partner_check_fee"].present? ? divide["partner_check_fee"].to_f : 0
      partner = partner + partner1
    elsif divide["partner_total_fee"].present?
      partner =divide["partner_total_fee"].present? ? divide["partner_total_fee"].to_f : 0
    else
      partner = 0
    end

    if divide["agent_fee"].present? || divide["agent_check_fee"].present?
      agent =divide["agent_fee"].present? ? divide["agent_fee"].to_f : 0
      agent1 =divide["agent_check_fee"].present? ? divide["agent_check_fee"].to_f : 0
      agent = agent1 + agent
    elsif divide["agent_total_fee"].present?
      agent =divide["agent_total_fee"].present? ? divide["agent_total_fee"].to_f : 0
    end

    if divide["iso_fee"].present? || divide["iso_check_fee"].present?
      iso =divide["iso_fee"].present? ? divide["iso_fee"].to_f : 0
      iso1 =divide["iso_check_fee"].present? ? divide["iso_check_fee"].to_f : 0
      iso = iso + iso1
    elsif divide["iso_total_fee"].present?
      iso =divide["iso_total_fee"].present? ? divide["iso_total_fee"].to_f : 0
    end
    if divide["hold_money"].to_f > 0
      total = partner + agent + iso + divide["hold_money"].to_f
    else
      total = partner + agent + iso
    end
    if divide["gbox"].present?
      if divide["gbox"]["misc"].present? && divide["gbox"]["misc"].to_f > 0
        if divide["iso"].present?
          if divide["iso"]["misc"].present? && divide["gbox"]["misc"].to_f > 0
            # return cents_to_dollars(divide["gbox"]["misc"].to_f - divide["iso"]["misc"].to_f)
            return cents_to_dollars(divide["gbox"]["misc"].to_f )
          end
        end
        return cents_to_dollars(divide["gbox"]["misc"].to_f)
      end
      if divide["gbox"]["service"].present? && divide["gbox"]["service"].to_f > 0
        if divide["iso"].present?
          if divide["iso"]["service"].present? && divide["gbox"]["service"].to_f > 0
            # return cents_to_dollars(divide["gbox"]["service"].to_f - divide["iso"]["service"].to_f)
            return cents_to_dollars(divide["gbox"]["service"].to_f )
          end
        end
        return cents_to_dollars(divide["gbox"]["service"].to_f)
      end
      if divide["gbox"]["statement"].present? && divide["gbox"]["statement"].to_f > 0
        if divide["iso"].present?
          if divide["iso"]["statement"].present? && divide["gbox"]["statement"].to_f > 0
            # return cents_to_dollars(divide["gbox"]["statement"].to_f - divide["iso"]["statement"].to_f)
            return cents_to_dollars(divide["gbox"]["statement"].to_f)
          end
        end
        return cents_to_dollars(divide["gbox"]["statement"].to_f)
      end
      return divide["gbox"]["amount"]
    end
    if location_fee["location_fee"].present?
      fee = allFee.to_f + location_fee["location_fee"].to_f
    else
      fee = allFee.to_f
    end
    return fee - total
  end

  def approve_request_wihtout_payment(user, request_amount, card, request, ledger = nil)
    t=TypesEnumLib::TransactionType
    wallet = user.wallets.first
    wallet_id = wallet.id
    # wallet_name = wallet.name
    qc_wallet = Wallet.escrow.first
    # amount = dollars_to_cents(request_amount)
    merchant_wallet = Wallet.where(id: request.wallet_id).first
    merchant_user = merchant_wallet.users.first
    issue = nil
    charge = nil
    card_fee = FeeLib.new
    if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
      amount = request_amount.to_f
    else
      amount = request_amount.to_f + card_fee.get_card_fee(request_amount, 'card').to_f
    end
    if merchant_user.payment_gateway.present?
      if merchant_user.payment_gateway == 'stripe'
        transaction = user.transactions.build(to: wallet_id, from: "stripe", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_stripe(amount, wallet_id, qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_stripe(amount, wallet_id,qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount,'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
            # transaction.update(seq_transaction_id: issue.id)
          end
        end
      elsif merchant_user.payment_gateway == 'bridge_pay'
        transaction = user.transactions.build(to: wallet_id, from: "bridge_pay", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_bridge_pay(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, charge, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_bridge_pay(amount, wallet_id, qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, charge, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      elsif merchant_user.payment_gateway == 'payeezy'
        transaction = user.transactions.build(to: wallet_id, from: "payeezy", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name,get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet, t::ApproveRequest,card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      elsif merchant_user.payment_gateway == 'converge'
        transaction = user.transactions.build(to: wallet_id, from: "elavon", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_elvano(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_elvano(amount, wallet_id,qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      end
      return [issue,charge]
    end
  end

  def approve_request_issue(user, request_amount, card, request, ledger = nil)
    t = TypesEnumLib::TransactionType
    wallet = user.wallets.first
    wallet_id = wallet.id
    # wallet_name = wallet.name
    qc_wallet = Wallet.qc_support.first
    # amount = dollars_to_cents(request_amount)
    merchant_wallet = Wallet.where(id: request.wallet_id).first
    merchant_user = merchant_wallet.users.first
    issue = nil
    charge = nil
    card_fee = FeeLib.new
    tokenization = CardToken.new
    app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
      amount = request_amount.to_f
    else
      amount = request_amount.to_f + card_fee.get_card_fee(request_amount, 'card').to_f
    end
    if merchant_user.payment_gateway.present?
      if merchant_user.payment_gateway == 'stripe'
        charge = issue_from_stripe(amount, card.stripe, user.stripe_customer_id)
        transaction = user.transactions.build(to: wallet_id, from: "stripe", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_stripe(amount, wallet_id, qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_stripe(amount, wallet_id,qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount,'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
            # transaction.update(seq_transaction_id: issue.id)
          end
        end
      elsif merchant_user.payment_gateway == 'converge'
        charge = tokenization.charge_elavon(card.elavon, amount, params[:exp_date], user.email)
        transaction = user.transactions.build(to: wallet_id, from: "elavon", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_elvano(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_elvano(amount, wallet_id,qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      elsif merchant_user.payment_gateway == 'payeezy'
        charge = tokenization.charge_payeezy(card, amount)
        transaction = user.transactions.build(to: wallet_id, from: "payeezy", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet, t::ApproveRequest,card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      elsif merchant_user.payment_gateway == "bridge_pay"
        # if card.type.present? && card.type == 'Credit'
        #   transaction_from_credit(user, card, amount, wallet_id, qc_wallet, card_fee, request_amount, app_config, merchant_user)
        # else
        charge = tokenization.charge_bridge_pay(card.bridge_pay, amount, card.name, card.exp_date)
        transaction = user.transactions.build(to: wallet_id, from: "bridge_pay", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_bridge_pay(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, charge, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_bridge_pay(amount, wallet_id, qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, charge, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
        # end
      end
    else
      # bank with admin config
      if app_config.stringValue == "stripe"
        charge = issue_from_stripe(amount, card.stripe, user.stripe_customer_id)
        transaction = user.transactions.build(to: wallet_id, from: "stripe", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_stripe(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_stripe(amount, wallet_id,qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount,'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
            # transaction.update(seq_transaction_id: issue.id)
          end
        end
      elsif app_config.stringValue == "converge"
        charge = tokenization.charge_elavon(card.elavon, amount, params[:exp_date], user.email)
        transaction = user.transactions.build(to: wallet_id, from: "elavon", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_elvano(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_elvano(amount, wallet_id,qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      elsif app_config.stringValue == "payeezy"
        charge = tokenization.charge_payeezy(card, amount)
        transaction = user.transactions.build(to: wallet_id, from: "payeezy", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name,get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet, t::ApproveRequest,card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
      elsif app_config.stringValue == "bridge_pay"
        # if card.type.present? && card.type == 'Credit'
        #   transaction_from_credit(user, card, amount, wallet_id, qc_wallet, card_fee, request_amount, app_config, merchant_user)
        # else
        charge = tokenization.charge_bridge_pay(card.bridge_pay, amount, card.name, card.exp_date)
        transaction = user.transactions.build(to: wallet_id, from: "bridge_pay", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
        # end
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_bridge_pay(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, charge, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_bridge_pay(amount, wallet_id, qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, charge, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
        # end
      elsif app_config.stringValue == "fluid_pay"
        fluid_pay = FluidPayPayment.new
        fluid_pay_payment = fluid_pay.chargeAmount('19VpZDXlUKuLPmkjXLhdIEIOFQi',(number_with_precision(amount,precision:2)*100).to_i,user.fluid_pay_customer_id,card.fluid_pay,'making a transactions')
        # card.fluid_pay = fluid_pay_payment.second
        transaction = user.transactions.build(to: wallet_id, from: "fluid_pay", status: "approved", charge_id: fluid_pay_payment.first, amount: amount, sender: user, action: 'issue')
        if transaction.save!
          if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
            issue = user.issue_with_fluid_pay(amount, wallet_id,qc_wallet, t::ApproveRequest, 0, card.last4, merchant_user.name, fluid_pay_payment.first, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          else
            issue = user.issue_with_fluid_pay(amount, wallet_id, qc_wallet, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, merchant_user.name, fluid_pay_payment.first, get_ip, ledger)
            transaction.update(seq_transaction_id: issue[:transaction_id])
          end
        end
        # end
      end
    end
    return [issue,charge]
  end

  def issue_with_gateways(user, request_amount, card, merchant, gateway, card_number = nil)
    # process for try different gateway if one gateway fails except fluid pay for merchant
    @current_stripe_type = nil
    charge = nil
    card_fee = FeeLib.new
    tokenization = CardToken.new
    # if merchant.present? && merchant.MERCHANT? && !merchant.issue_fee
    #   amount = request_amount.to_f
    # else
    #   amount = request_amount.to_f + card_fee.get_card_fee(request_amount, 'card').to_f
    # end
    if gateway == 'stripe'
      @current_stripe_type = "stripe"
      charge = issue_from_stripe(request_amount.to_f, user.stripe_customer_id, user.id,card_number, ENV["stripe_secret_key"],card)
    elsif gateway == 'stripe_pop'
      @current_stripe_type = "stripe_pop"
      charge = issue_from_stripe(request_amount.to_f, user.second_stripe_id, user.id,card_number, ENV["stripe_pop_secret_key"],card)
    elsif gateway == 'stripe_pop_2'
      @current_stripe_type = "stripe_pop_2"
      charge = issue_from_stripe(request_amount.to_f, user.third_stripe_id, user.id,card_number, ENV["stripe_pop_2_secret_key"],card)
    elsif gateway == 'converge'
      charge = tokenization.charge_elavon(card.elavon, request_amount.to_f, card.exp_date, user.email)
    elsif gateway == 'payeezy'
      charge = tokenization.charge_payeezy_merchant(card, request_amount.to_f)
    elsif gateway == "bridge_pay"
      charge = tokenization.charge_bridge_pay_merchant(card.bridge_pay, request_amount.to_f, card.name, card.exp_date)
    end
    return charge
  end

  def transaction_from_credit(user, card, amount, wallet_id, qc_wallet, card_fee, request_amount, app_config, merchant_user)
    # not yet in use will use for credit card transcation from brigde_pay
    t = TypesEnumLib::TransactionType
    if app_config.secondary_option == 'stripe'
      charge = issue_from_stripe(amount, card.stripe, user.stripe_customer_id)
      transaction = user.transactions.build(to: wallet_id, from: "stripe", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
      if transaction.save!
        if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
          issue = user.issue_with_stripe(amount, wallet_id,qc_wallet.id, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip)
        else
          issue = user.issue_with_stripe(amount, wallet_id,qc_wallet.id, t::ApproveRequest, card_fee.get_card_fee(request_amount,'card'), card.last4, nil,get_ip)
        end
      end
    elsif app_config.secondary_option == 'converge'
      charge = tokenization.charge_elavon(card.elavon, amount, params[:exp_date], user.email)
      transaction = user.transactions.build(to: wallet_id, from: "elavon", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
      if transaction.save!
        if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
          issue = user.issue_with_elvano(amount, wallet_id,qc_wallet.id, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip)
        else
          issue = user.issue_with_elvano(amount, wallet_id,qc_wallet.id, t::ApproveRequest, card_fee.get_card_fee(request_amount, 'card'), card.last4, nil,get_ip)
        end
      end
    elsif app_config.secondary_option == 'payeezy'
      charge = tokenization.charge_payeezy(card, amount)
      transaction = user.transactions.build(to: wallet_id, from: "payeezy", status: "approved", charge_id: charge, amount: amount, sender: user, action: 'issue')
      if transaction.save!
        if merchant_user.present? && merchant_user.MERCHANT? && !merchant_user.issue_fee
          issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet.id, t::ApproveRequest, 0, card.last4, merchant_user.name, get_ip)
        else
          issue = user.issue_with_payeezy(amount, wallet_id,qc_wallet.id, t::ApproveRequest,card_fee.get_card_fee(request_amount, 'card'), card.last4, nil, get_ip)
        end
      end
    end
  end

  def change_wallet_name(name)
    return_name = "--"
    if name.present?
      if name == "Check Escrow" || name == " Check_escrow Wallet "
        return_name = "Withdrawal Escrow"
      elsif name == "Escrow Wallet"
        return_name = "QC Escrow"
      elsif name == "CBK Wallet"
        return_name = "CBK Escrow"
      elsif ["Qc_support", "Qc_support Wallet"].include?(name)
        return_name = "QC Support"
      else
        return_name = name
      end
    end
    return_name
  end

  def change_cbk_rec_name(rec_name)
    return_name = "--"
    if rec_name.present? && rec_name == "QC Support"
      return_name = "Escrow Support"
    end

    return  return_name
  end


  def change_wallets_name(name)
    return_name = "--"
    if name.present?
      if name == "Check Escrow" || name == " Check_escrow Wallet "
        return_name = "Withdrawal Escrow"
      elsif name == "Escrow Wallet"
        return_name = "QC Escrow"
      elsif name == "CBK Wallet"
        return_name = "CBK Escrow"
      elsif ["Qc_support", "Qc_support Wallet"].include?(name)
        return_name= "QC Support"
      elsif name.include?('Qc_support') || name.include?('Qc_support Wallet')
        return_name= "QC Support"
      elsif name.include?("CBK Escrow") || name.include?("Charge_back")
        return_name ="CBK Wallet"
      elsif name.include?('Escrow Wallet') || name.include?('Escrow Support')
        return_name = "Escrow Wallet"
      elsif name.include?('Withdrawal Escrow') || name.include?('Check_escrow Wallet') || name.include?('Check Escrow')
        return_name = "Withdrawal Escrow"
      else
        return_name = name
      end
    end
    return return_name
  end

  def location_name
    merchant_wallets = @sub_merchant.wallets.primary.pluck(:location_id)
    Location.where(id: merchant_wallets).pluck(:business_name).join(",")
  end

  def permission_format
    @sub_merchant.permission.try(:name)
  end

  private

  def wallet_name(id)
    wallet = Wallet.find_by_id(id) if id.present?
    if wallet.present?
      return wallet.name
    else
      return 'N/A'
    end
  end

  # def name(id)
  #   wallet = Wallet.find_by_id(id) if id.present?
  #   if wallet.present?
  #     return wallet.name
  #   else
  #     return ""
  #   end
  # end

  def wallet_last_name(id)
    wallet = Wallet.where(id: id).first
    user = wallet.users.first.last_name if wallet.present?
    if user.present?
      return user
    else
      return "N/A"
    end
  end

  def doc_params
    params.require(:image).permit(:add_image,:usage_id,:location_id)
  end

  def doc_with_params(params)
    params.require(:image).permit(:add_image,:usage_id,:location_id,:bank_detail_id)
  end

  def wallet_phone_number(id)
    wallet = Wallet.find_by_id(id)
    if wallet.present?
      if wallet.users.present?
        return wallet.users.first.phone_number
      else
        return "N/A"
      end
    end
  end

  def get_net(amount, fee, type)
    if type == "DEPOSIT"
      return number_with_precision((amount.to_f - fee.to_f).abs, precision: 2)
    else
      return number_with_precision(amount.to_f, precision: 2)
    end
  end

  def get_amount(amount, fee, type)
    if type == "DEPOSIT"
      return (amount.to_f).abs
    else
      return (amount.to_f + fee.to_f).abs
    end
  end

  def random_token
    Digest::SHA1.hexdigest([Time.now, rand].join)
  end

  def get_parse_check_user_data(tags, value)
    if tags["send_check_user_info"].present?
      if value == "name"
        return tags["send_check_user_info"]["name"]
      elsif value == "email"
        return tags["send_check_user_info"]["check_email"]
      end
    else
      return "N/A"
    end
  end

  def get_parse_send_via(tags)
    if tags["send_via"] == "email"
      return "Email"
    elsif tags["send_via"] == "direct_deposit"
      return "Direct Deposit"
    else
      return "N/A"
    end
  end

  def parse_transactions(transactions, source_id = nil)
    transactions.map do |obj|
      if obj.present?
        t = obj.actions.first
        # obj.actions.each do |t|
        hash = {
            id: t.id,
            reference: t.tags,
            timestamp: obj.timestamp,
            main_type: t.type,
            type: updated_type(t.tags["type"],t.tags["sub_type"]),
            fee: 0,
            last4: get_last4(t.tags),
            source_name: wallet_name(t.source_account_id),
            destination_name: wallet_name(t.destination_account_id),
            actualAmount: number_with_precision(parse_merchant_amount(t, source_id), precision: 2),
            amount: number_with_precision(parse_amount(t), precision: 2).to_f,
            destination: t.destination_account_id,
            source: t.source_account_id,
            send_check_email: send_check_email(t.tags),
            hold_money: get_hold_money(t.tags),
            ip_address: get_transaction_ip(t.tags),
            fee_updated: (get_fee_updated(t.tags)).abs,
            net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
            amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2)
        }
        if source_id.present?
          if hash[:main_type] == "transfer"
            if hash[:destination] == "#{source_id}"
              hash.merge({recieved_from: hash[:source]})
            elsif hash[:source] == "#{source_id}"
              hash.merge({sent_to: hash[:destination]})
            else
              hash.merge({})
            end
          else
            hash.merge({})
          end
        else
          hash.merge({})
        end
      end
    end
  end

  def parse_mobile_transactions(transactions, source_id = nil)
    transactions.map do |obj|
      if obj.present?
        t = obj.actions.first
        # obj.actions.each do |t|
        hash = {
            id: t.id,
            reference: t.tags,
            timestamp: obj.timestamp,
            type: t.type,
            fee: 0,
            last4: get_last4(t.tags),
            source_name: wallet_name(t.source_account_id),
            destination_name: wallet_name(t.destination_account_id),
            actualAmount: number_with_precision(parse_merchant_amount(t, source_id), precision: 2),
            amount: number_with_precision(parse_amount(t), precision: 2).to_f,
            destination: t.destination_account_id,
            source: t.source_account_id,
            send_check_email: send_check_email(t.tags),
            hold_money: get_hold_money(t.tags),
            ip_address: get_transaction_ip(t.tags),
            fee_updated: (t.tags["fee"].to_f).abs,
            net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
            amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type,t.tags).to_f, precision: 2)
        }
        if source_id.present?
          if hash[:type] == "transfer"
            if hash[:destination] == "#{source_id}"
              hash.merge({recieved_from: hash[:source]})
            elsif hash[:source] == "#{source_id}"
              hash.merge({sent_to: hash[:destination]})
            else
              hash.merge({})
            end
          else
            hash.merge({})
          end
        else
          hash.merge({})
        end
      end
    end
  end

  def parse_merchant_transactions(transactions, source_id = nil)
    # parse transactions for merchant side
    transactions.map do |obj|
      if obj.present?
        t = obj.actions.first
        hash = {
            id: t.id,
            parent_id: obj.id,
            reference: t.tags,
            tip: getTip(t.tags),
            timestamp: obj.timestamp,
            main_type: t.type,
            type: updated_type(t.tags["type"],t.tags["sub_type"]),
            last4: get_last4(t.tags),
            gateway: getGateway(t.tags),
            actualAmount: number_with_precision(parse_merchant_amount(t, source_id), precision: 2),
            amount: number_with_precision(get_main_amount_merchant(SequenceLib.dollars(t.amount), t.tags), precision: 2).to_f,
            total_amount: number_with_precision(get_total_amount_merchant(SequenceLib.dollars(t.amount),t.tags), precision: 2),
            destination: t.destination_account_id,
            source: t.source_account_id,
            send_check_email: send_check_email(t.tags),
            hold_money: get_hold_money(t.tags),
            ip_address: get_transaction_ip(t.tags),
            fee_updated: get_fee_updated(t.tags).abs,
            net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
            amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags["type"]).to_f, precision: 2),
            privacy_fee: number_with_precision(get_privacy_fee_merchant(t.tags), precision: 2).to_f,
            net_privacy_fee: number_with_precision(get_net_privacy_fee_merchant(t.tags), precision: 2).to_f
        }
        if source_id.present?
          if hash[:main_type] == "transfer"
            if hash[:destination] == "#{source_id}"
              hash.merge({recieved_from: hash[:source]})
            elsif hash[:source] == "#{source_id}"
              hash.merge({sent_to: hash[:destination]})
            else
              hash.merge({})
            end
          else
            hash.merge({})
          end
        else
          hash.merge({})
        end
      end
    end
  end

  def parse_merchants_transactions(transactions, source_id = nil)
    # parse transactions for merchant side
    return [] if transactions.blank?
    transactions = transactions[:items]
    transactions.map do |obj|
      if obj.present?
        t = obj.actions.first
        hash = {
            id: t.id,
            parent_id: obj.id,
            reference: t.tags,
            tip: getTip(t.tags),
            timestamp: obj.timestamp,
            main_type: t.type,
            type: updated_type(t.tags["type"],t.tags["sub_type"],t.tags),
            gateway: getGateway(t.tags),
            last4: get_last4(t.tags),
            actualAmount: number_with_precision(parse_merchant_amount(t, source_id), precision: 2),
            amount: number_with_precision(get_main_amount_merchant(SequenceLib.dollars(t.amount), t.tags), precision: 2).to_f,
            total_amount: number_with_precision(get_total_amount_merchant(SequenceLib.dollars(t.amount),t.tags), precision: 2),
            destination: t.destination_account_id,
            source: t.source_account_id,
            send_check_email: send_check_email(t.tags),
            hold_money: get_hold_money(t.tags),
            ip_address: get_transaction_ip(t.tags),
            fee_updated: get_fee_updated(t.tags).abs,
            net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
            amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags["type"]).to_f, precision: 2),
            privacy_fee: number_with_precision(get_privacy_fee_merchant(t.tags), precision: 2).to_f,
            net_privacy_fee: number_with_precision(get_net_privacy_fee_merchant(t.tags), precision: 2).to_f,
            merchant: get_merchant_of_transaction(t.tags),
            location: get_location_of_transaction(t.tags),
            sub_merchant_detail: t.tags["fee_perc"].try("sub_merchant_details")
        }
        if source_id.present?
          if hash[:main_type] == "transfer"
            if hash[:destination] == "#{source_id}"
              hash.merge({recieved_from: hash[:source]})
            elsif hash[:source] == "#{source_id}"
              hash.merge({sent_to: hash[:destination]})
            else
              hash.merge({})
            end
          else
            hash.merge({})
          end
        else
          hash.merge({})
        end
      end
    end
  end

  def parse_single_transaction(transaction, source_id = nil)
    # parse transactions for merchant side
    return [] if transaction.blank?
    t = transaction.first.actions.first
    hash = {
        id: t.id,
        parent_id: transaction.first.id,
        reference: t.tags,
        tip: getTip(t.tags),
        timestamp: transaction.first.timestamp,
        main_type: t.type,
        type: updated_type(t.tags["type"],t.tags["sub_type"]),
        gateway: getGateway(t.tags),
        last4: get_last4(t.tags),
        actualAmount: number_with_precision(parse_merchant_amount(t, source_id), precision: 2),
        amount: number_with_precision(get_main_amount_merchant(SequenceLib.dollars(t.amount), t.tags), precision: 2).to_f,
        total_amount: number_with_precision(get_total_amount_merchant(SequenceLib.dollars(t.amount),t.tags), precision: 2),
        destination: t.destination_account_id,
        source: t.source_account_id,
        send_check_email: send_check_email(t.tags),
        hold_money: get_hold_money(t.tags),
        ip_address: get_transaction_ip(t.tags),
        fee_updated: get_fee_updated(t.tags).abs,
        net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
        amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags["type"]).to_f, precision: 2),
        privacy_fee: number_with_precision(get_privacy_fee_merchant(t.tags), precision: 2).to_f,
        net_privacy_fee: number_with_precision(get_net_privacy_fee_merchant(t.tags), precision: 2).to_f,
        merchant: get_merchant_of_transaction(t.tags),
        location: get_location_of_transaction(t.tags)
    }
  end


  def get_merchant_of_transaction(tags)
    if tags["merchant"].present?
      tags["merchant"]
    end
  end

  def get_location_of_transaction(tags)
    if tags["location"].present?
      tags["location"]
    end
  end

  def get_total_amount_merchant(amount, tags)
    if tags.present?
      privacy_fee = 0
      tip = 0
      privacy_fee = tags["privacy_fee"] if tags["privacy_fee"].present?
      tip = tags["tip"] if tags["tip"].present?
      if privacy_fee.to_f > 0
        amount.to_f + tip.to_f
      else
        amount.to_f + tip.to_f + privacy_fee.to_f
      end
    else
      amount
    end
  end

  def get_net_privacy_fee_merchant(tags)
    if tags["fee"].present?
      if tags["fee"].to_f > 0
        if tags["privacy_fee"].present?
          tags["fee"].to_f - tags["privacy_fee"].to_f
        else
          number_with_precision(tags["fee"], precision: 2)
        end
      else
        "0.00"
      end
    else
      "0.00"
    end
  end

  def get_privacy_fee_merchant(tags)
    if tags.present?
      if tags["privacy_fee"].present?
        number_with_precision(tags["privacy_fee"], precision: 2)
      else
        "0.00"
      end
    else
      "0.00"
    end
  end

  def getGateway(tags)
    if tags["descriptor"].present?
      return tags["descriptor"].try(:humanize)
    elsif tags["source"].present?
      if tags["source"] == "stripe"
        return "Stripe GBox"
      end
      if tags["type"].downcase == "void_check"
        return "Checkbook io"
      end
      if !tags["source"].is_a?(Integer) && tags["source"].downcase == "checkbook.io"
        return "Checkbook io"
      end
      tags["source"]#.humanize
    else
      ""
    end
  end
  #========== for Ach_gateway_name used to show ACH Deposit transaction info
  def getGateway_name(t)
    get_gateway = AchGateway.find_by(id: t)
    return get_gateway.try(:bank_name)
  end

  def getTip(tags)
    if tags["tip"].present?
      number_with_precision(tags["tip"], precision: 2)
    else
      "0.00"
    end
  end

  def parse_report_transactions(transactions, source_id)
    qc_id = Wallet.qc_support.first.id
    transactions.map do |obj|
      if obj.present?
        t = obj.actions.first
        hash = {
            id: t.id,
            timestamp: obj.timestamp,
            reference: t.tags,
            type: t.type,
            new_type: parse_report_type(t, source_id, qc_id),
            inner_type: get_inner_type(t.tags),
            sub_type: updated_type(t.tags["type"]),
            tip: t.tags["tip"].present? ? t.tags["tip"] : 0,
            fee: number_with_precision(t.tags["fee"].to_f, precision: 2).to_f,
            privacy_fee: number_with_precision(get_privacy_fee_merchant(t.tags), precision: 2).to_f,
            after_fee: number_with_precision(after_fee_amount(t),precision: 2).to_f,
            after_retire_fee: number_with_precision(after_fee_retire(t), precision: 2).to_f,
            amount: number_with_precision(SequenceLib.dollars(t.amount), precision: 2).to_f,
            destination: t.destination_account_id,
            source: t.source_account_id,
            last4: get_last4(t.tags),
            send_via: get_parse_send_via(t.tags),
            check_user_name: get_parse_check_user_data(t.tags, "name"),
            check_user_email: get_parse_check_user_data(t.tags, "email"),
            hold_money: get_hold_money(t.tags).to_f,
            fee_without_hold_money: get_fee(t.tags).to_f,
            net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
            amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type,t.tags).to_f, precision: 2)
        }
        if hash[:type] == "transfer"
          if hash[:destination] == "#{source_id}"
            hash.merge({recieved_from: hash[:source]})
          elsif hash[:source] == "#{source_id}"
            hash.merge({sent_to: hash[:destination]})
          else
            hash.merge({})
          end
        else
          hash.merge({})
        end
      end
    end
  end

  def parse_iso_report_transactions(transactions)
    # qc_id = Wallet.qc_support.first.id
    transactions.map do |obj|
      if obj.present?
        t = obj.actions.first
        hash = {
            id: t.id,
            timestamp: obj.timestamp,
            reference: t.tags,
            type: t.type,
            # new_type: parse_report_type(t, source_id, qc_id),
            inner_type: get_inner_type(t.tags),
            sub_type: updated_type(t.tags["type"]),
            tip: t.tags["tip"].present? ? t.tags["tip"] : 0,
            fee: number_with_precision(t.tags["fee"].to_f, precision: 2).to_f,
            after_fee: number_with_precision(after_fee_amount(t),precision: 2).to_f,
            after_retire_fee: number_with_precision(after_fee_retire(t), precision: 2).to_f,
            amount: number_with_precision(SequenceLib.dollars(t.amount), precision: 2).to_f,
            destination: t.destination_account_id,
            source: t.source_account_id,
            last4: get_last4(t.tags),
            send_via: get_parse_send_via(t.tags),
            check_user_name: get_parse_check_user_data(t.tags, "name"),
            check_user_email: get_parse_check_user_data(t.tags, "email"),
            hold_money: get_hold_money(t.tags).to_f,
            fee_without_hold_money: get_fee(t.tags).to_f,
            net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
            amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type,t.tags).to_f, precision: 2)
        }
        hash.merge({})
        # if hash[:type] == "transfer"
        #   if hash[:destination] == "#{source_id}"
        #     hash.merge({recieved_from: hash[:source]})
        #   elsif hash[:source] == "#{source_id}"
        #     hash.merge({sent_to: hash[:destination]})
        #   else
        #     hash.merge({})
        #   end
        # else
        #   hash.merge({})
        # end
      end
    end
  end

  def parse_reports(transactions, source_id)
    # function for parsing transctions for tip and reserve wallet
    qc_wallet = Wallet.qc_support.first.id
    list = []
    transactions.each do |obj|
      if obj.present?
        merchant_amount = obj.actions.first.amount
        list << SequenceLib.filter_actions(obj, source_id).map do |t|
          {
              id: t.id,
              timestamp: obj.timestamp,
              reference: t.tags,
              type: t.type,
              tip: t.tags["tip"].present? ? t.tags["tip"] : 0,
              new_type: parse_report_type(t, source_id, qc_wallet),
              inner_type: get_inner_type(t.tags),
              sub_type: updated_type(t.tags["type"]),
              fee: number_with_precision(t.tags["fee"].to_f, precision: 2).to_f,
              after_fee: number_with_precision(after_fee_amount(t),precision: 2).to_f,
              after_retire_fee: number_with_precision(after_fee_retire(t), precision: 2).to_f,
              amount: number_with_precision(SequenceLib.dollars(t.amount), precision: 2).to_f,
              merchant_amount: SequenceLib.dollars(merchant_amount),
              destination: t.destination_account_id,
              source: t.source_account_id,
              last4: get_last4(t.tags),
              send_via: get_parse_send_via(t.tags),
              check_user_name: get_parse_check_user_data(t.tags, "name"),
              check_user_email: get_parse_check_user_data(t.tags, "email"),
              hold_money: get_hold_money(t.tags).to_f,
              fee_without_hold_money: number_with_precision(t.tags["fee"].to_f, precision: 2).to_f - get_hold_money(t.tags).to_f,
              net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
              amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type,t.tags).to_f, precision: 2)
          }
        end
      end
    end
    list.reduce([], :concat).reject(&:nil?)
  end

  def parse_company_transactions(wallet, transactions)
    #function for parsing iso,agent, company transactions
    list = []
    transactions.each do |obj|
      if obj.present?
        merchant_transaction_amount = obj.actions.first.amount
        tip = obj.actions.first.tags["tip"]
        list << SequenceLib.filter_actions(obj, wallet).map do |t|
          {
              id: t.id,
              parent_id: obj.id,
              reference: t.tags,
              timestamp: obj.timestamp,
              main_type: t.type,
              tip: tip || getTip(t.tags),
              merchant_transaction_amount: number_with_precision(get_merchant_transaction_amount(t.tags, SequenceLib.dollars(merchant_transaction_amount).to_f + tip.to_f, wallet,t.source_account_id), precision: 2),
              transaction_type: updated_type(obj.actions.first.tags["type"],obj.actions.first.tags["sub_type"]),
              type: updated_type(t.tags["type"],t.tags["sub_type"]),
              fee: 0,
              last4: get_last4(t.tags),
              actualAmount: number_with_precision(parse_merchant_amount(t, wallet), precision: 2),
              amount: number_with_precision(parse_amount(t), precision: 2).to_f,
              destination: t.destination_account_id,
              source: t.source_account_id,
              send_check_email: send_check_email(t.tags),
              hold_money: get_hold_money(t.tags),
              ip_address: get_transaction_ip(t.tags),
              fee_updated: (get_fee_updated(t.tags)).abs,
              net: number_with_precision(get_new_net(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type, t.tags).to_f, precision: 2),
              amount_updated: number_with_precision(get_new_amount(SequenceLib.dollars(t.amount),t.tags["fee"].to_f,t.type,t.tags).to_f, precision: 2),
              merchant: get_merchant_of_transaction(t.tags),
              location: get_location_of_transaction(t.tags),
              fee_without_hold_money: number_with_precision(t.tags["fee"].to_f, precision: 2).to_f - get_hold_money(t.tags).to_f,
              privacy_fee: number_with_precision(get_privacy_fee_merchant(t.tags), precision: 2).to_f,
              net_privacy_fee: number_with_precision(get_net_privacy_fee_merchant(t.tags), precision: 2).to_f,
              total_amount: number_with_precision(get_total_amount_merchant(SequenceLib.dollars(t.amount),t.tags), precision: 2),
          }
        end
      end
    end
    list.reduce([], :concat).reject(&:nil?)
  end

  def get_merchant_transaction_amount(tags, amount, wallet,source)
    if tags["type"] == TypesEnumLib::TransactionType::SendCheck || tags["type"] == TypesEnumLib::TransactionType::GiftCardPurchase || tags["type"] == TypesEnumLib::TransactionType::GiftCard
      if source == wallet.to_s
        amount.to_f
      else
        amount.to_f - tags["fee"].to_f
      end
    else
      amount.to_f
    end
  end


  def get_main_amount_merchant(amount, tags)
    if tags.present?
      if tags["privacy_fee"].present? || tags["tip"].present?
        tip = tags["tip"].to_f if tags["tip"].present?
        privacy = tags["privacy_fee"].to_f if tags["privacy_fee"].present?
        return amount.to_f - tip.to_f - privacy.to_f
      else
        return amount.to_f
      end
    else
      amount.to_f
    end
  end

  def get_fee(tags)
    if tags["type"]=="retrievel_fee"
      return 0
    end
    if tags["fee"].present?
      if tags["location_fee"].present?
        fee = tags["fee"].to_f + tags["location_fee"].to_f
      else
        fee = tags["fee"].to_f
      end
      if tags["fee_perc"].present?

        if tags["fee_perc"]["iso"].present? && tags["fee_perc"]["iso"]["iso_buyrate"] == true
          fee = tags["fee_perc"]["iso"]["amount"].to_f + tags["fee_perc"]["gbox"]["amount"].to_f
        end

        if tags["fee_perc"]["hold_money"].present?
          if fee > 0
            return fee - tags["fee_perc"]["hold_money"].to_f
          else
            return fee
          end
        else
          return fee
        end
      else
        return fee
      end
    end
  end

  def get_new_net(amount, fee, type, tags)
    if tags["type"]=="send_check" && tags["fee_perc"].present?
      amount.to_f
    elsif tags["type"] == "giftcard_purchase"
      amount.to_f - fee.to_f
    elsif tags["type"] == "send_check"
      amount.to_f - fee.to_f
    elsif tags["type"] == "refund"
      amount.to_f
    elsif type == "retire"
      amount.to_f
    else
      if tags["fee_perc"].present?
        if tags["fee_perc"]["reserve"].present?
          amount.to_f - fee.to_f - tags["fee_perc"]["reserve"]["amount"].to_f
        else
          amount.to_f - fee.to_f
        end
      else
        amount.to_f - fee.to_f
      end
    end
  end

  def get_new_amount(amount,fee,type,inner_type=nil)
    if inner_type == "refund"
      amount = amount.to_f + fee.to_f
    elsif inner_type.present? && inner_type == "refund_bank"
      amount = amount
    elsif inner_type.present? && inner_type["type"] == "giftcard_purchase"
      amount.to_f
    elsif type == "retire"
      amount.to_f + fee.to_f
    else
      amount.to_f
    end
  end

  def get_fee_updated(tags)
    if tags["fee"].present?
      if tags["fee_perc"].present?
        if tags["fee_perc"]["iso"].present? && tags["fee_perc"]["iso"]["iso_buyrate"] == true
          return tags["fee_perc"]["iso"]["amount"].to_f + tags["fee_perc"].try(:[],'gbox').try(:[],'amount').to_f
        end

        if tags["fee_perc"]["hold_money"].present?
          return tags["fee"].to_f - tags["fee_perc"]["hold_money"].to_f
        else
          return tags["fee"].to_f
        end
      else
        return tags["fee"].to_f
      end
    else
      0
    end
  end

  def get_inner_type(tags)
    if tags["type"].present?
      if tags["type"] == "ATM Cash Deposit"
        return "KIOSK Cash Deposit"
      elsif tags["type"] == "p2p_payment"
        return "P2P Payment"
      elsif tags["type"] == "send_check"
        return "Send Check"
      elsif tags["type"] == "ach"
        return "ACH"
      elsif tags["type"] == "instant_pay"
        return "Instant Pay"
      else
        return tags["type"]
      end
    end
  end

  def parse_report_type(transaction, source, qc_id)
    if transaction.destination_account_id.to_i == source.to_i && transaction.type != 'issue'
      return "Sale"
    elsif transaction.destination_account_id.nil?
      return "Withdraw"
    elsif transaction.source_account_id.to_i == source.to_i && transaction.destination_account_id.to_i != qc_id.to_i
      return "Transfer"
    elsif transaction.type == 'issue'
      return "Deposit"
    elsif transaction.destination_account_id.to_i == qc_id.to_i
      return "Fee"
    end
  end

  def get_transaction_ip(tags)
    if tags["ip"].present?
      return tags["ip"]
    end
  end

  def get_hold_money(tags)
    if tags["fee_perc"].present?
      if tags["fee_perc"]["hold_money"].present?
        return number_with_precision(tags["fee_perc"]["hold_money"], precision: 2)
      elsif tags["fee_perc"]["reserve"].present? && tags["fee_perc"]["reserve"]["amount"].present?
        return number_with_precision(tags["fee_perc"]["reserve"]["amount"], precision: 2)
      else
        return number_with_precision(0, precision: 2)
      end
    else
      return number_with_precision(0, precision: 2)
    end
  end

  def last4(last4)
    if last4.present?
      return last4
    else
      return "N/A"
    end
  end

  def account_email(source)
    wallet = Wallet.where(id: source).first
    user=nil
    if wallet.present? && wallet.users.present?
      user = wallet.users.first.email if wallet.present?
      return user if user.present?
    else
      return "N/A"
    end

  end

  def after_fee_retire(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    fee = transaction.tags["fee"].to_f
    return fee + amount
  end

  def after_fee_amount(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    fee = transaction.tags["fee"].to_f
    if transaction.type == "retire"
      return 0.00
    else
      if fee > amount
        return fee - amount
      else
        return amount - fee
      end
    end
  end

  def parse_amount(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    fee = transaction.tags['fee'].to_f
    # type = transaction.tags['type']
    if fee.present? && fee > 0 && amount != fee
      if transaction.type == 'issue'
        if fee > amount
          return fee - amount
        else
          return amount - fee
        end

      end
    end
    return amount
  end

  def parse_merchant_amount(transaction, source= nil)
    if source.present?
      amount = SequenceLib.dollars(transaction.amount)
      fee = transaction.tags['fee'].to_f
      if fee.present? && fee > 0 && amount != fee
        if transaction.type == 'retire'
          return amount + fee
        elsif transaction.tags['type'] == 'invoice' && transaction.source_account_id.to_i == source
          return amount
        elsif transaction.tags['type'] == 'invoice' && transaction.destination_account_id.to_i == source
          return amount - fee
        else
          if fee > amount
            return fee - amount
          else
            return amount - fee
          end
        end
      end
      return amount
    end
  end

  def issue_amount(amount, wallet, type, source_name, total_fee, user_info = nil, check_info = nil,check_number = nil, key = nil)
    qc_wallet = Wallet.escrow.first
    tr_type = get_transaction_type(type)
    to_user = wallet.users.first
    if to_user.nil?
      to_user = @to.oauth_app.user
    end
    tags =  {
        "fee_perc" => user_info,
        "send_check_user_info" => check_info
    }
    transaction = to_user.transactions.create!(
        to: wallet.id.to_s,
        from: source_name,
        status: "pending",
        action: 'issue',
        receiver_id: to_user.id,
        receiver_name: to_user.try(:name),
        receiver_wallet_id: wallet.id,
        amount: "#{amount.to_f}",
        main_type: updated_type(tr_type.first),
        fee: total_fee,
        total_amount: amount,
        net_fee: total_fee,
        net_amount: amount.to_f - total_fee.to_f,
        tags: tags
    )
    tx = SequenceLib.issue(wallet.id, amount, qc_wallet, tr_type.first, total_fee, nil, source_name, nil, nil,nil,nil, user_info, get_ip, check_info,nil,nil,nil,nil,nil,nil,nil,nil,check_number, nil, key)
    if tx.present?
      transaction.update(seq_transaction_id: tx.id, status: "approved", tags: tx.actions.first.tags, timestamp: tx.timestamp)
    end
    return tx
  end


  def issue_checkbook_amount(amount, wallet, type, source_name, total_fee, user_info = nil, check_info = nil,check=nil,while_approving=nil,iso_agent_aff_case=nil, from_task = false, ispaid=nil)
    tr_type = get_transaction_type(type)
    ip=request.remote_ip if !from_task && request.present? && request.remote_ip.present?
    if check.present?
      @wallet  = Wallet.where(id: check.wallet_id).first
      location = nil
      merchant = nil
      location = location_to_parse(@wallet.location) if @wallet.location.present?
      merchant = merchant_to_parse(@wallet.location.merchant) if @wallet.location.present?
      to = Wallet.check_escrow.first.id
      if while_approving
        tx = SequenceLib.retire(amount, to ,tr_type.first, total_fee, nil, source_name, nil, merchant, location,nil, check.send_via, user_info, check_info, ip, nil, tr_type.second,nil,nil,iso_agent_aff_case,nil,nil, ispaid)
      else
        check_transaction = check.try(:transaction).try(:tags)
        fee = (from_task ? true : (params[:type2] == "FAILED" || params[:type2] == "IN_PROCESS")) && (from_task || check.PENDING? || (check.IN_PROGRESS? && check.instant_ach?) || (check.IN_PROCESS? && check.check?)) ? total_fee : 0
        void_withdrawal = nil
        failed_fee = 0
        amount = check.amount
        if user_info.try(:[], "fee").to_f > 0
          if user_info["splits"].present? && user_info["splits"].try(:[], "iso").present? && user_info["splits"].try(:[], "iso").try(:[], "iso_buyrate")
            if params[:type2] == "IN_PROCESS" && check.instant_ach?
              fee = total_fee
              ach_total_fee = fee.to_f + user_info["splits"].try(:[], "iso").try(:[], "amount").to_f
            elsif params[:type2] == "FAILED" && check.instant_ach?
              fee = total_fee
            else
              failed_fee = fee
              fee = user_info["splits"].try(:[], "iso").try(:[], "amount")
              void_withdrawal = user_info["splits"].try(:[], "iso").try(:[], "wallet")
            end
          end
        elsif check_transaction.try(:[], "fee").to_f > 0 && !((params[:type2] == "FAILED" || params[:type2] == "IN_PROCESS") && check.instant_ach?)
          if check_transaction["fee_perc"].present? && check_transaction["fee_perc"].try(:[], "iso").present? && check_transaction["fee_perc"].try(:[], "iso").try(:[], "iso_buyrate")
            failed_fee = fee
            fee = check_transaction["fee_perc"].try(:[], "iso").try(:[], "amount")
            void_withdrawal = check_transaction["fee_perc"].try(:[], "iso").try(:[], "wallet")
          end
        elsif user_info.try(:[], "splits").try(:[], "iso").try(:[], "iso_buyrate") && params[:type2] == "VOID"
          fee = user_info.try(:[], "splits").try(:[], "gbox").try(:[], "amount")
          buyrate_fee = user_info.try(:[], "splits").try(:[], "iso").try(:[], "amount")
          void_withdrawal = user_info["splits"].try(:[], "iso").try(:[], "wallet")
        elsif user_info.try(:[], "fee").to_f == 0 && user_info.try(:[], "splits").try(:[], "iso").try(:[], "iso_buyrate") && (params[:type2] == "FAILED" || params[:type2] == "IN_PROCESS")
          iso_buyrate_case = true if check.try(:PENDING?) # when merchant fee is 0 and iso buyrate is true
        end
        user_info = user_info.try(:[], "splits")
        amount = get_transfer_amount(check, amount)
        send_check_case = ((params[:type2] == "IN_PROCESS" || params[:type2] == "FAILED") && check.instant_ach?) || (params[:type2] == "FAILED" && check.instant_pay?) ? nil : to
        tx = SequenceLib.transfer(amount, to, check.wallet_id, tr_type.first, fee, nil, source_name, nil, merchant, location, user_info, nil,ip, nil, nil, nil,tr_type.second,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil, send_check_case, check_info, void_withdrawal, failed_fee, ach_total_fee, nil, nil,buyrate_fee,iso_buyrate_case)
      end
    else
      qc_wallet = Wallet.escrow.first
      tx = SequenceLib.issue(wallet.id, amount, qc_wallet, tr_type.first, total_fee, nil, source_name, nil, nil,nil,nil, user_info,ip,check_info)
      # tx = SequenceLib.issue(wallet.id, amount, qc_wallet, tr_type.first, total_fee, nil, source_name, nil, nil,nil,nil, user_info,nil,check_info)
    end
    return tx
  end

  def withdraw_void_ach_check(wallet,user,check, escrow_wallet = nil, escrow_user = nil)
    location=wallet.location
    amount=check.amount
    recipient=check.recipient
    name=check.name
    send_via=check.instant_ach? ? 'ACH' : 'email'
    main_type = check.instant_ach? ? 'ACH' : updated_type(TypesEnumLib::TransactionType::SendCheck)
    trans_type=check.instant_ach? ? TypesEnumLib::TransactionType::ACHdeposit : TypesEnumLib::TransactionType::SendCheck
    enum_type=check.instant_ach? ? TypesEnumLib::GatewayType::ACH : TypesEnumLib::GatewayType::Checkbook
    if user.merchant?
      location_fee = location.fees if location.present?
      fee_object = location_fee.buy_rate.first if location_fee.present?
      commision_type=check.instant_ach? ? TypesEnumLib::CommissionType::ACH : TypesEnumLib::CommissionType::SendCheck
      fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,commision_type)
      fee = fee_class.apply(amount.to_f, commision_type)
      splits = fee["splits"]
      if splits.present?
        agent_fee = splits["agent"]["amount"]
        iso_fee = splits["iso"]["amount"]
        iso_balance = show_balance(location.iso.wallets.primary.first.id)
        if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
          if iso_balance < iso_fee
            raise AdminCheckError.new(I18n.t("errors.iso.iso_0005"))
          end
        else
          if iso_balance + iso_fee < agent_fee.to_f
            raise AdminCheckError.new(I18n.t("errors.iso.iso_0006"))
          end
        end
      end
      amount_sum = amount + fee["fee"].to_f
      balance = show_balance(wallet.id).to_f
      pending_amount = HoldInRear.calculate_pending(wallet.id)
      raise 'Error Code 2039 Insuficient balance!' if balance - pending_amount < amount_sum
      account_number=check.account_number
      location1 = location_to_parse(location)
      merchant1 = merchant_to_parse(user)
      send_check_user_info=check.instant_ach? ? nil : { name: name, check_email: recipient }
      tags = {
          "fee_perc" => splits,
          "location" => location1,
          "merchant" => merchant1,
          "send_check_user_info" => send_check_user_info,
          "send_via" => send_via
      }
      gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
      total_fee = save_total_fee(splits, fee["fee"].to_f)
      transaction = user.transactions.build(
          to: recipient,
          from: nil,
          status: "pending",
          charge_id: nil,
          amount: amount,
          sender: user,
          sender_name: user.try(:name),
          sender_wallet_id: wallet.id,
          action: 'retire',
          fee: total_fee.to_f,
          net_amount: amount.to_f,
          net_fee: total_fee.to_f,
          total_amount: amount_sum.to_f,
          gbox_fee: gbox_fee.to_f,
          iso_fee: save_iso_fee(splits),
          agent_fee: splits["agent"]["amount"].to_f,
          affiliate_fee: splits["affiliate"]["amount"].to_f,
          ip: get_ip,
          main_type: main_type,
          tags: tags
      )

      user_info = fee.try(:[], "splits") || {}
      withdraw = withdraw(amount,wallet.id, fee.try(:[],"fee"), trans_type,account_number,send_via,user_info, send_check_user_info, nil, nil,enum_type)
      # withdraw = withdraw(amount,@wallet.id, fee["fee"].to_f || 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
    else
      #other users
      fee_object = user.system_fee if user.system_fee.present? && user.system_fee != "{}"
      if check.instant_ach?
        if fee_object.present?
          send_check_fee = fee_object["ach_dollar"].present? ? fee_object["ach_dollar"].to_f : 0
          fee_perc_fee = fee_object["ach_percent"].present? ? deduct_fee(fee_object["ach_percent"].to_f, amount) : 0
        else
          send_check_fee = 0
          fee_perc_fee = 0
        end
      else
        if fee_object.present?
          send_check_fee = fee_object["send_check"].present? ? fee_object["send_check"].to_f : 0
          fee_perc_fee = fee_object["redeem_fee"].present? ? deduct_fee(fee_object["redeem_fee"].to_f, amount) : 0
        else
          send_check_fee = 0
          fee_perc_fee = 0
        end
      end
      fee = send_check_fee + fee_perc_fee
      amount_sum = amount + fee
      balance = show_balance(wallet.id)
      raise 'Error Code 2039 Insuficient balance!' if balance < amount_sum
      if check.instant_ach?
        tags=nil
      else
        location1 = location_to_parse(location)
        merchant1 = merchant_to_parse(user)
        send_check_user_info=check.instant_ach? ? nil : { name: name, check_email: recipient }
        tags = {
            "send_check_user_info" => send_check_user_info,
            "send_via" => send_via,
            "location" => location1,
            "merchant" => merchant1
        }

      end
      transaction = user.transactions.build(
          to: recipient,
          from: nil,
          status: "pending",
          charge_id: nil,
          amount: amount,
          sender: user,
          sender_name: user.try(:name),
          sender_wallet_id: wallet.id,
          action: 'retire',
          fee: fee.to_f,
          net_amount: amount_sum.to_f - fee.to_f,
          net_fee: fee.to_f,
          total_amount: amount_sum.to_f,
          ip: get_ip,
          main_type: main_type,
          tags: tags
      )
      if check.instant_ach?
        withdraw = withdraw(amount,wallet.id, fee.to_f, TypesEnumLib::TransactionType::ACHdeposit,account_number,send_via,nil, nil, nil, nil,TypesEnumLib::GatewayType::ACH)
      else
        if fee > 0
          user_info = fee.try(:[], "splits") || {}
          withdraw = custom_withdraw(amount,wallet.id, fee, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, nil, nil,TypesEnumLib::GatewayType::Checkbook)
        else
          withdraw = withdraw(amount,wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, nil, nil,TypesEnumLib::GatewayType::Checkbook)
        end
      end

    end
    if withdraw.present? && withdraw["actions"].present?
      transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
      save_block_trans(transactions)
      check.transaction_id = transaction.id
      transaction.seq_transaction_id= withdraw["actions"].first["id"]
      transaction.status= "approved"
      transaction.privacy_fee= 0
      transaction.tags= withdraw["actions"].first.tags
      transaction.sub_type= withdraw["actions"].first.tags["sub_type"]
      transaction.timestamp= withdraw["timestamp"]
      transaction.save
    else
      flash[:error] =  I18n.t('merchant.controller.unaval_feature')
    end
  end

  def check_escrow_transactions(check, params, check_info, status=nil, escrow_wallet = nil, escrow_user = nil, from_task = false, ispaid=nil)
    if check.check?
      type = updated_type(params["type"]== "VOID" ? TypesEnumLib::TransactionType::VoidCheck : TypesEnumLib::TransactionType::CheckDeposit)
    elsif check.instant_pay?
      type = updated_type(params["type"]== "VOID" ? TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::PushtoCardDeposit)
    elsif check.instant_ach?
      type = updated_type(params["type"]== "VOID" ? TypesEnumLib::TransactionType::VoidAch : TypesEnumLib::TransactionType::ACHdeposit)
    else
      type = TypesEnumLib::TransactionType::SendCheck
    end
    wallet = Wallet.find(check.wallet_id)
    user = check.user
    location = wallet.location
    amount = check.actual_amount.to_f
    if user.present? && wallet.present?
      fee_lib = FeeLib.new
      if user.MERCHANT?
        location_fee = location.fees if location.present?
        fee_object = location_fee.buy_rate.first if location_fee.present?
        if check.check?
          fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::SendCheck)
          fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
        elsif check.instant_pay?
          # type = TypesEnumLib::TransactionType::DebitCardDeposit
          fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::DebitCardDeposit)
          fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::DebitCardDeposit)
        elsif check.instant_ach?
          # type = params["type"]== "VOID" ? TypesEnumLib::TransactionType::VoidAch : TypesEnumLib::TransactionType::ACHdeposit
          fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::ACH)
          fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::ACH)
        else
          fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::SendCheck)
          fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
        end

      elsif user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
        if user.affiliate_program?
          admin_user = User.find_by_id(user.admin_user_id)
          fee_object = admin_user.try(:system_fee)
        else
          fee_object = user.system_fee if user.system_fee.present? && user.system_fee != "{}"
        end
        if fee_object.present? && check.check?
          fee_dollar = fee_object["send_check"].present? ? fee_object["send_check"].to_f : 0
          fee_perc = fee_object["redeem_fee"].present? ? deduct_fee(fee_object["redeem_fee"].to_f, amount) : 0
        elsif fee_object.present? && check.instant_ach?
          fee_dollar = fee_object["ach_dollar"].present? ? fee_object["ach_dollar"].to_f : 0
          fee_perc = fee_object["ach_percent"].present? ? deduct_fee(fee_object["ach_percent"].to_f, amount) : 0
        elsif fee_object.present? && check.instant_pay?
          fee_dollar = fee_object["push_to_card_dollar"].present? ? fee_object["push_to_card_dollar"].to_f : 0
          fee_perc = fee_object["push_to_card_percent"].present? ? deduct_fee(fee_object["push_to_card_percent"].to_f, amount) : 0
        else
          fee_dollar = 0
          fee_perc = 0
        end
        fee = {fee: fee_dollar.to_f + fee_perc.to_f}
      else
        fee = {fee: fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f}
      end
    end
    gbox_fee = save_gbox_fee(fee["splits"], fee.symbolize_keys[:fee].to_f)
    total_fee = save_total_fee(fee["splits"], fee.symbolize_keys[:fee].to_f)
    ip = get_ip if !from_task
    if check.instant_ach? && check.IN_PROGRESS? && (check.ach_gateway.try(:manual_integration?) || check.ach_gateway.nil?)
      tx_fee = nil
      tx_fee_splits = nil
      total_amount = check.actual_amount.to_f
    else
      tx_fee = fee.symbolize_keys[:fee]
      tx_fee_splits = fee["splits"]
      total_amount = check.amount.to_f
    end
    transaction = Transaction.create(
        from: escrow_wallet.try(:id),
        status: "pending",
        amount: amount,
        ach_gateway_id: check.ach_gateway_id,
        sender: escrow_user,
        sender_name: escrow_user.try(:name),
        sender_wallet_id: escrow_wallet.try(:id),
        sender_balance: SequenceLib.balance(escrow_wallet.try(:id)),
        action: 'retire',
        fee: total_fee.to_f,
        net_amount: check.actual_amount.to_f,
        net_fee: total_fee.to_f,
        total_amount: total_amount,
        gbox_fee: gbox_fee.to_f,
        iso_fee: save_iso_fee(fee["splits"]),
        agent_fee: fee["splits"].try(:[],"agent").try(:[],"amount").to_f,
        affiliate_fee: fee["splits"].try(:[],"affiliate").try(:[],"amount").to_f,
        ip: ip,
        main_type: type,
        )
    if user.MERCHANT?
      issue = issue_checkbook_amount(check.actual_amount, check.wallet_id , type, check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, tx_fee,tx_fee_splits, check_info,check,true, nil, from_task, ispaid)
    elsif user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
      issue = issue_checkbook_amount(check.actual_amount, check.wallet_id , type, check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, tx_fee,nil, check_info,check,true,true, from_task)
    end
    if issue.present?
      transaction.update(
          timestamp: issue.timestamp,
          status: "approved",
          tags: issue.actions.first.tags,
          seq_transaction_id: issue.actions.first.id
      )
      check.update(void_transaction_id: transaction.id)
      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
      save_block_trans(parsed_transactions,"sub",status) if parsed_transactions.present?
    end

  end
  # def issue_checkbook_amount(amount, wallet, type, source_name, total_fee, user_info = nil, check_info = nil,void_ach=nil)
  #   qc_wallet = Wallet.escrow.first
  #   tr_type = get_transaction_type(type)
  #   tx = SequenceLib.issue(wallet.id, amount, qc_wallet, tr_type.first, total_fee, nil, source_name, nil, nil,nil,nil, user_info,nil,check_info,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,void_ach)
  # end

  def get_last4(tags)
    if tags["last4"].present?
      return tags["last4"]
    elsif tags["bank_details"].present? && tags["bank_details"]["last4"].present?
      return tags["bank_details"]["last4"]
    elsif tags["card_type"].present? && tags["card_type"]["last4"].present?
      return tags["card_type"]["last4"]
    elsif tags["check_number"].present? && tags["check_number"]["last4"].present?
      return tags["check_number"]["last4"]
    elsif tags["previous_issue"].present? && tags["previous_issue"]["last4"].present?
      return tags["previous_issue"]["last4"]
    else
      return ''
    end
  end

  def get_denominations(tags)
    if tags["denominations"].present?
      return tags["denominations"]
    else
      return ''
    end
  end

  def send_check_email(tags)
    if tags["send_check_user_info"].present?
      return tags["send_check_user_info"]["check_email"]
    else
      return ''
    end
  end

  def validate_parameters(args)
    args.each do |name, value|
      if value.blank?
        raise ArgumentError.new "Missing required parameter: #{name}"
      end
      if name.present? && (name==:email || name.to_s.include?('email'))
        value = value.gsub(" ","") if name == "billing[email]"
        raise ArgumentError.new "Invalid required parameter: #{name}" if value.include?(' ')
      end
      if name.present? && name==:country
        country = ISO3166::Country.new(params[:country])
        raise ArgumentError.new "Invalid country code. i,e United States = US" if country.nil?
      end
    end
  end

  def check_string(string)
    return true if string.class == Integer
    #= function to check if there are alphanumeric characters in numeric string
    string.gsub!(/[^0-9A-Za-z]/, '')
    if string.start_with?("+")
      new = string.strip.gsub("+",'')
      new.scan(/\D/).empty?
    else
      string.strip.scan(/\D/).empty?
    end
  end

  def make_elavon_payment_void(transaction_id, payment_gateway = nil)
    if transaction_id.present? #Void from Converge in case of seq failure
      elavon = Payment::ElavonGateway.new
      elavon.void(transaction_id,payment_gateway)
    end
  end

  def make_i_can_preauth_void(transaction_id, request, amount = nil, payment_gateway = nil)
    if transaction_id.present? #Void from ICanPay in case of seq failure
      i_can_pay = Payment::ICanPay.new(payment_gateway)
      i_can_pay.refund(transaction_id,amount, request)
    end
  end

  def make_bolt_pay_void(transaction_id, amount = nil, payment_gateway = nil)
    if transaction_id.present?
      bolt_pay = Payment::BoltPayGateway.new
      bolt_pay.refund(transaction_id, amount, payment_gateway)
    end
  end
  # ------------------------ Refund Debit Card Transaction------------------------------
  def refunds_debit_transaction(db_transaction,transaction)
    transaction_amount=transaction[:amount].to_f
    name="NA"
    name=@user.name if @user.present?
    name=@current_user.name if @current_user.present?
    lastFour=transaction[:reference]["last4"] ? transaction[:reference]["last4"] : "NA"
    source=transaction[:reference]["source"] ? transaction[:reference]["source"].downcase : "NA"
    #----------------------------Perform Sequence Retire Refund--------------------------------
    seq_transaction = SequenceLib.retire(transaction_amount,transaction[:destination], 'refund',0,0,source,nil,name,nil,lastFour)
    if seq_transaction.present?
      if db_transaction.update(status: 'refunded')
        return [true,"Successfully Refunded"]
      end
    else
      return [false,"Sequence Refund Unsuccessful"]
    end
  end
  #------- Create local refund transaction----------------
  def create_refund_transaction(source_wallet, destination_wallet, amount, fee, tags, action, type, payment_gateway = nil, sub_type = nil, user_id = nil, card_id = nil, last4 = nil, first6 = nil, refund_id = nil, gbox_fee = nil)
    fee_perc = tags.try(:[], "fee_perc")
    if type == 'refund'
      source_user = Wallet.find_by(id: source_wallet).users.merchant.where(merchant_id: nil).first if source_wallet.present?
      destination_user = Wallet.find_by(id: destination_wallet).users.first if destination_wallet.present?
      receiver_wallet = destination_wallet
      sender_wallet = source_wallet
    elsif type == "refund_bank"
      source_user = Wallet.find_by(id: source_wallet).users.first if source_wallet.present?
      sender_wallet = source_wallet
      receiver_wallet = nil
    end
    sender_balance = SequenceLib.balance(sender_wallet) if sender_wallet.present?
    receiver_balance = SequenceLib.balance(receiver_wallet) if receiver_wallet.present?
    sender_dba=get_business_name(Wallet.find_by(id:sender_wallet))
    receiver_dba=get_business_name(Wallet.find_by(id:receiver_wallet))
    Transaction.create(
        to: "#{destination_wallet}",
        from: "#{source_wallet}",
        amount: "#{amount}",
        fee: fee.to_f,
        net_amount: amount.to_f,
        total_amount: amount.to_f + fee.to_f,
        net_fee: fee.to_f,
        gbox_fee: gbox_fee.to_f,
        agent_fee: fee_perc.try(:[], "agent").try(:[], "amount"),
        iso_fee: save_iso_fee(fee_perc),
        affiliate_fee: fee_perc.try(:[], "affiliate").try(:[], "amount"),
        tags: tags,
        action: action,
        main_type: type,
        sub_type: sub_type,
        payment_gateway_id: payment_gateway.try(:id),
        receiver_wallet_id: receiver_wallet,
        sender_wallet_id: sender_wallet,
        sender_balance: sender_balance,
        receiver_balance: receiver_balance,
        sender_id: source_user.try(:id),
        receiver_id: destination_user.try(:id),
        sender_name: sender_dba,
        receiver_name: receiver_dba,
        status: "pending",
        refunded_by: user_id,
        card_id: card_id,
        last4: last4,
        first6: first6,
        charge_id: refund_id
    )
  end

  def update_refund_transaction(tx, seq_tx)
    tx.update(
        status: "approved",
        seq_transaction_id: seq_tx["actions"].first["id"],
        timestamp: seq_tx["timestamp"],
        tags: seq_tx["actions"].first["tags"]
    )
  end

  def refund_status_check_loc(transaction)
    if transaction.status == "refunded"
      return "refunded"
    elsif transaction.status != "refunded" && transaction.refund_log != nil
      return "partial refunded"
    else
      return "Success"
    end
  end

  def refund_status_check(tran_id, parent_id)
    transaction = Transaction.where(seq_transaction_id: tran_id).first if tran_id.present?
    transaction = Transaction.where(seq_transaction_id: parent_id).first if transaction.blank? && parent_id.present?
    if !transaction.nil? && transaction.status == "refunded"
      return true
    else
      return false
    end
  end

  def record_refund_transaction(refund_params, from_worker = nil)
    ## Activity Log saved.
    # from_worker is used because we are calling this function from worker
    activity = ActivityLog.new
    if from_worker.blank?
      activity.url = request.url
      activity.host = request.host
      activity.browser = request.env["HTTP_USER_AGENT"]
      if request.remote_ip.present?
        activity.ip_address = request.remote_ip
      end
      activity.controller = controller_name
      activity.action = action_name
      if params[:user_image].present?
        if params[:user_image].original_filename.present?
          params[:user_image] = params[:user_image].original_filename
        else
          params[:user_image] = 'unknown image format'
        end
      end
      params[:ip_details][:request_ip] = request.ip if params[:ip_details].try(:[],:request_ip).present?
      params[:ip_details][:request_remote_ip] = request.remote_ip if params[:ip_details].try(:[],:request_remote_ip).present?
      params[:issue_amount] = params[:issue_amount].to_f if params.try(:[],:issue_amount).present?
      activity.params = params.except(:card_cvv)
    end
    if from_worker.present?
      activity.action = "refund_transaction"
      activity.controller = "admins"
    end
    activity.transaction_id = refund_params[:seq_transaction_id]
    activity.respond_with = refund_params[:response]
    activity.user = refund_params[:user]
    activity.save
    if refund_params[:type] == "admin"
      activity.admin_refund!
    elsif refund_params[:type] == "merchant"
      activity.merchant_refund!
    end
    return activity.id
  end

  def payment_gateway_name(key)
    payment_gateway = PaymentGateway.find_by(key: key)
    if payment_gateway.present?
      payment_gateway.name
    else
      key
    end
  end

  def get_state(state)
    states=CS.states(:us)
    symbol = state.try(:to_sym)
    if states[symbol].present?
      return states[symbol]
    else
      return state
    end
  end
  # date parsing for searches on admin side transaction_list & merchant e-wallet search
  def parse_date(date)
    date = date.split(' - ')
    first_date = date.first.split('/')
    second_date = date.second ? date.second.split('/') : date.first.split('/')
    first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
    second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
    return [first_date, second_date]
  end

  def get_card_info(number)
    first_digits = number.to_s.first(6).to_i
    # url = "https://lookup.binlist.net/#{first_digits}"
    # url = "https://#{ENV['CARD_BIN_LIST_KEY']}:@lookup.binlist.net/#{first_digits}"
    begin
      response = Card.neutrino_post(first_digits)
    rescue => ex
      response = {}
    end
    response = {} if response.blank?
    response = Card.convert_hash(response)
    response = response.merge({:first6 => number.to_s.first(6).to_s, :last4 => number.to_s.last(4).to_s}) if number.to_s.length > 10
    return response
  end

  def update_log(transaction,log)
    if transaction.refund_log.nil?
      false
    else
      if JSON.parse(transaction.refund_log).kind_of?(Array)
        temp = JSON.parse(transaction.refund_log)
        temp.last["sequence_id"] = log
      else
        temp = JSON.parse(transaction.refund_log)
        temp["sequence_id"] = log
      end
      transaction.refund_log = temp.to_json
      true
    end
  end

  def valid_json?(json)
    return false if json.blank?
    JSON.parse(json)
    return true
  rescue JSON::ParserError => e
    return false
  end

  def valid_jsons?(json)
    begin
      return false if json.blank?
      JSON.parse(json)
      return true
    rescue => e
      return false
      end
  end

  def clean_parameters(parameters = {})
    parameters = params if parameters == {}
    parameters.except(:card_cvv, :cvv) if parameters.try(:[], :card_cvv).present? || parameters.try(:[], :cvv).present?
    if parameters.try(:[],:card_number).present?
      parameters[:card_number] = "XXXX-XXXX-XXXX-#{params[:card_number].last(4)}"
    end
    if parameters.try(:[],:card_no).present?
      parameters[:card_no] = "XXXX-XXXX-XXXX-#{params[:card_no].last(4)}"
    end
    if parameters.try(:[],:cardNoSafe).present?
      parameters[:cardNoSafe] = "XXXX-XXXX-XXXX-#{params[:cardNoSafe].last(4)}"
    end
  end

  def cultivate_api(status,message,transaction_id,base_url,eaze=nil)
    user = @user
    auth_app = user.oauth_apps.last
    key = auth_app.key
    secret = auth_app.secret
    if params[:location_id].present?
      location_id = params[:location_id]
    end
    if params[:wallet_id].present?
      wallet = Wallet.find_by_id(params[:wallet_id])
      if wallet
        location = wallet.location
        location_id = location.location_secure_token
      end
    end
    card_holder_name = params[:card_holder_name]
    exp_date = params[:card_exp_date]
    card_number = @card_number
    amount = params[:amount]
    tip =  params[:tip].present? ? params[:tip] : 0
    privacy_fee =  params[:privacy_fee].present? ? params[:privacy_fee] : 0
    total_amount = params[:amount].to_i
    if params[:privacy_fee].present?
      total_amount = total_amount+params[:privacy_fee].to_i
    end
    if params[:tip].present? && params[:tip].to_f > 0
      total_amount = total_amount+params[:tip].to_i
    end
    card_info = get_card_info(card_number)
    if card_info.try(:[], "type").blank? && card_info.try(:[], "scheme").blank? && params[:transaction_id].present?
      transaction = Transaction.find(params[:transaction_id])
      if transaction.present?
        card = transaction.card
        if card.present?
          type = card.card_type
          payment_type = card.brand
        end
      end
    else
      type = card_info['type']
      payment_type = card_info['scheme']
    end
    last4 = card_info[:last4]
    Rails.logger.debug "************API Called*************"
    Rails.logger.debug "Client-Key"+key
    Rails.logger.debug "Client-Secret"+secret
    Rails.logger.debug base_url
    Rails.logger.debug "*************************"

    begin
      request_body = {
          "card_holder_name" => card_holder_name,
          "exp_date" => exp_date,
          "last4" => last4,
          "amount" => amount,
          "tip" => tip,
          "privacy_fee" => privacy_fee,
          "total_amount" => total_amount,
          "payment_type" => type,
          "card_type" => payment_type,
          "status" => status,
          "message" => message,
          "quickcard_transaction_id" => transaction_id,
          "terminal_id" => params[:terminal_id]
      }.to_json
      # Posting Data to Cultivate API
      cultivate_response = HTTParty.post(base_url,
                                         :body => request_body,
                                         :headers => {
                                             "Content-Type" => "application/json",
                                             "Client-Id" => key,
                                             "Client-Secret" => secret,
                                             "Client-Location" => location_id
                                         }
      )
    rescue Net::ReadTimeout => exc
      back_trace = exc.backtrace.select { |x| x.match(Rails.application.class.parent_name.downcase)}.first(5)
      SlackService.notify("*Cultivate API Timed Out - #{exc.class.name}* \n App: \`\`\` #{{URL: request.base_url, HOST: request.host, ACTION: "#{params[:controller]}/#{params[:action]}"}.to_json} \`\`\` Error: \`\`\` #{exc.to_json} \`\`\` Params: \`\`\`#{request.params.except(:card_number).to_json}\`\`\` User: \`\`\`#{@user.present? ? {id: @user.id, email: @user.email, role: @user.role }.to_json : 'No User Present!'} \`\`\` BACKTRACE: \`\`\` #{back_trace.to_json} \`\`\`")
      cultivate_response = {message: "API Timed out: #{base_url}", params: request_body, transaction: transaction.id}
    rescue => exc
      cultivate_response = {message: "No Reponse returned!", params: request_body}
    ensure
      @eaze_response = cultivate_response if eaze.present?
      return cultivate_response
    end
    Rails.logger.debug cultivate_response
  end

  def transactions_search_query(params,trans=nil,offset=nil,url=nil)
    params[:offset] = offset if offset.present?
    conditions = []
    parameters = []

    #= last4 & first6
    # if trans == 'refund' || trans == "undefined" || trans == "success"
    #   if params[:last4].present?
    #     conditions << "cards.last4::VARCHAR LIKE ?"
    #     parameters << "#{params[:last4].split.join.to_s}%"
    #   end
    #   if params[:first6].present?
    #     conditions << "cards.first6::VARCHAR LIKE ?"
    #     parameters << "#{params[:first6].split.join.to_s}%"
    #   end
    # else
      if params[:last4].present?
        conditions << "last4::VARCHAR LIKE ?"
        parameters << "#{params[:last4].split.join}%"
      end
      if params[:first6].present?
        conditions << "first6::VARCHAR LIKE ?"
        parameters << "#{params[:first6].split.join}%"
      end
    # end
    if offset.present?
      unless offset.include?(':')
        offset.insert(3,':')
      end
    end

    #= datetime
    time = parse_time(params)

    date = params[:date].present? ? parse_daterange_transaction(params[:date]) : nil
    date = parse_daterange_transaction(params[:datie]) if params[:datie].present? && date.blank?

    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
      }
    elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
      date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    end

    if date.present? && date.try(:[],:first_date).present?
      conditions << "transactions.created_at >= ?"
      parameters << date[:first_date]
    end
    if date.present? && date.try(:[],:second_date).present?
      conditions << "transactions.created_at <= ?"
      parameters << date[:second_date]
    end
    zone = ActiveSupport::TimeZone[params[:offset].to_i].name if params[:offset].present?
    # If Time and Date is Selected
    if (params[:time1].present? || params[:time2].present?) && params[:date].present?
      time_first = time_conversion(time.first + params[:offset], zone) if time.first.present?
      time_second = time_conversion(time.second + params[:offset], zone) if time.second.present?
      if time_first > time_second
        conditions << "(transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ? OR transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ?)"
        parameters << time_first
        parameters << "23:59:59"
        parameters << "00:00:00"
        parameters << time_second
      else
        if time_first.present?
          conditions << "transactions.created_at::TIME >= ?"
          parameters << time_first
        end
        if time_second.present?
          conditions << "transactions.created_at::TIME <= ?"
          parameters << time_second
        end
      end
    end

    #= Block ID
    if params[:block_id].present?
      if trans == "decline"
        conditions << "transactions.id::VARCHAR LIKE ?"
        parameters << "#{params[:block_id].split.join}%"
      else
        conditions << "transactions.seq_transaction_id LIKE ? OR transactions.quickcard_id::VARCHAR LIKE ?"
        parameters << "#{params[:block_id].split.join}%"
        parameters << "#{params[:block_id].split.join}%"
      end
    end

    #= sender & receiver ID
    if params[:sender_id].present?
      conditions << "(transactions.from LIKE ? OR transactions.sender_wallet_id = ?)"
      parameters << "#{params[:sender_id].split.join}%".to_s
      parameters <<  params[:sender_id]
    end
    if params[:receiver_id].present?
      conditions << "(transactions.to LIKE ? OR transactions.receiver_wallet_id = ?)"
      parameters << "#{params[:receiver_id].split.join}%".to_s
      parameters <<  params[:receiver_id]
    end
    #= sender & receiver Name
    if params[:sender].present?
      conditions << "(transactions.sender_name ILIKE ?)"
      parameters << "#{params[:sender].downcase}%".to_s
    end
    if params[:receiver].present?
      conditions << "(transactions.receiver_name ILIKE ?)"
      parameters << "#{params[:receiver].downcase}%".to_s
    end

    #= amount
    is_decimal = params[:amount].gsub(/[^. ]/, '')
    amount = params[:amount].split.join.to_s
    if amount.split('.').last == "00"
      amount=number_with_precision(amount, precision: 0)
    end
    if is_decimal == '.'
      after_decimal = amount.split('.')
      if after_decimal.last.length == 2
        if after_decimal.last.last == '0'
          amount=number_with_precision(amount, precision: 1)
        end
      end
      if trans=='checks'
        if params[:amount].present?
          conditions << "total_amount::VARCHAR LIKE ?"
          parameters << "#{amount}%"
        end
      else
        if params[:amount].present?
          conditions << "total_amount::VARCHAR LIKE ?"
          parameters << "#{amount}"
        end
      end
    else
      if trans=='checks'
        if params[:amount].present?
          conditions << "total_amount::VARCHAR LIKE ?"
          parameters << "#{amount}.%"
        end
      else
        if params[:amount].present?
          conditions << "total_amount::VARCHAR LIKE ?"
          parameters << "#{amount}"
        end
      end
    end

    #= type
    type = ''
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if type.present?
     if  type.include?("Invoice Qc")
        if type.count == 1
          conditions << "(main_type IN (?) AND sub_type IN (?) )"
          parameters << ["B2B Transfer","Invoice Qc"]
          parameters << "invoice"
        else
          if type.include?("Void Check")
            type.push("Void_Check")
          elsif type.include?("Void_Check")
            type.push("Void Check")
          end
          conditions << "((main_type IN (?) AND sub_type IN (?)  ) OR (main_type IN (?) OR sub_type IN (?) ))"
          parameters << ["B2B Transfer","Invoice Qc"]
          parameters << ["invoice"]
          parameters << type
          parameters << type
        end
      else
        if type.include?("Void Check")
          type.push("Void_Check")
        elsif type.include?("Void_Check")
          type.push("Void Check")
        end
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << type
        parameters << type
      end
    end

    #= gateway
    gateway = ''
    if params[:gateway].present?
      if params[:gateway].kind_of?(Array)
        gateway = params[:gateway]
      else
        gateway = JSON.parse(params[:gateway])
      end
    end
    if gateway.present?
      if trans=="decline"
        conditions << "payment_gateway_id IN(?)"
        parameters << gateway
      elsif  (type.include? "refund" ) || (type.include? "refund_bank" ) || trans== "refund"
        conditions << "(tags ->> 'source' IN  (?) OR payment_gateway_id IN (?))"
        parameters << gateway
        parameters << gateway
      else
        if gateway.present? && (gateway.include?("Checkbox.io") || gateway.include?("Tango")|| gateway.include?("Quickcard"))
          conditions << "(tags ->> 'source' IN(?) OR payment_gateway_id IN (?))"
          custom_option=[]
          custom_option.push('Checkbox.io') if gateway.include?("Checkbox.io")
          custom_option.push('Tango') if gateway.include?("Tango")
          custom_option.push('Quickcard')  if gateway.include?("Quickcard")
          parameters << custom_option
          gateway=gateway.reject{|e| e=='Checkbox.io'}
          gateway=gateway.reject{|e| e=='Tango'}
          gateway=gateway.reject{|e| e=='Quickcard'}
          parameters << gateway
        else
          conditions << "payment_gateway_id IN(?)"
          parameters << gateway
        end
      end
    end


    #= city

    if params[:city].present?
      location_cities = Location.joins(:wallets).where("lower(locations.city) ILIKE ?" ,"%#{params[:city].strip.downcase}").where("wallets.wallet_type = ?", 0).select('wallets.id')
      conditions << "(transactions.sender_wallet_id IN (?) OR transactions.receiver_wallet_id IN (?))"
      parameters << location_cities
      parameters << location_cities
    end

    #= state

    if params[:state].present?
      location_state_id = Location.joins(:wallets).where("lower(locations.state) LIKE ?" ,"%#{params[:state].strip.downcase}").where("wallets.wallet_type = ?", 0).select('wallets.id')
      conditions << "(transactions.sender_wallet_id IN (?) OR transactions.receiver_wallet_id IN (?))"
      parameters << location_state_id
      parameters << location_state_id
    end

    conditions = [conditions.join(" AND "), *parameters]
    return conditions
  end

  def maxmind_transactions_search_query(array=nil)
    conditions = []
    parameters = []
    reason = []
    action = []
    if array.class == String
      array = JSON.parse(array)
    end
    if array.any? { |s| s.include?('manual_review') }
      reason << TypesEnumLib::RiskReason::ManualReview
    end
    if array.any? { |s| s.include?('default') }
      reason << TypesEnumLib::RiskReason::Default
    end
    if array.any? {|s| s.include?("accept") }
      action << TypesEnumLib::RiskType::Accept
    end
    if array.any? {|s| s.include?("reject") }
      action << TypesEnumLib::RiskType::Reject
    end
    if array.any? {|s| s.include?("pending") }
      action << TypesEnumLib::RiskType::PendingReview
    end
    if array.any? {|s| s.include?("expired_manual_review") }
      action << TypesEnumLib::RiskType::ExpiredReview
    end
    if action.present? && reason.present?
      conditions << "(minfraud_results.qc_action IN (?) AND minfraud_results.qc_reason IN (?))"
      parameters << action
      parameters << reason

      conditions = [conditions.join(" AND "), *parameters]
      return conditions
    end
  end

  def shipment_search_query(params,offset = nil)
    params[:offset] = offset if offset.present?

    conditions = []
    parameters = []


    #TXN ID
    if params[:trans_id].present?
      conditions << "transactions.id = ?"
      parameters << "#{params[:trans_id]}"
    end
    #DBA NAME
    if params[:dba_name].present?
      conditions << "transactions.tags -> 'location' ->> 'business_name' ILIKE ?"
      parameters << "#{params[:dba_name]}"
    end
    #CUSTOMER NAME
    if params[:cust_name].present?
      conditions << "transactions.sender_name ILIKE ?"
      parameters << "%#{params[:cust_name]}%"
    end
    #TXN TOTAL AMOUNT
    if params[:total_amount].present?
      #is_decimal = params[:total_amount].gsub(/[^. ]/, '')
      #amount = params[:total_amount].split.join.to_s
      #if amount.split('.').last == "00"
      #  amount=number_with_precision(amount, precision: 1)
      #end
      #if is_decimal == '.'
      #  after_decimal = amount.split('.')
      #  if after_decimal.last.length == 2
      #    if after_decimal.last.last == '0'
      #      amount=number_with_precision(amount, precision: 1)
      #    end
      #  end
      #end

      conditions << "transactions.total_amount::VARCHAR LIKE ?"
      parameters << "#{params[:total_amount]}%"
    end
    #TRACKER CARRIER
    if params[:carrier].present?
      conditions << "trackers.carrier ILIKE ?"
      parameters << "#{params[:carrier]}"
    end
    #TRACKER STATUS
    if params[:status].present?
      conditions << "trackers.status ILIKE ?"
      parameters << "#{params[:status]}"
    end
    #LAST4
    if params[:last4].present?
      conditions << "transactions.last4 LIKE ?"
      parameters << "#{params[:last4]}"
    end
    #TRACKER CODE
    if params[:track].present?
      conditions << "trackers.tracker_code LIKE ?"
      parameters << "#{params[:track]}"
    end

    if offset.present?
      unless offset.include?(':')
        offset.insert(3,':')
      end
    end

    #ORDER DATE
    if params[:order_date_time].present? || (params[:time1].present? && params[:time2].present?)
      time = parse_time(params)
      date = params[:order_date_time].present? ? parse_daterange(params[:order_date_time]) : nil
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end
      if date.present? && date.try(:[],:first_date).present?
        conditions << "transactions.created_at >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "transactions.created_at <= ?"
        parameters << date[:second_date]
      end
      zone = ActiveSupport::TimeZone[params[:offset].to_i].name if params[:offset].present?

      # If Time and Date is Selected
      if (params[:time1].present? || params[:time2].present?) && params[:order_date_time].present?
        time_first = time_conversion(time.first + params[:offset], zone) if time.first.present?
        time_second = time_conversion(time.second + params[:offset], zone) if time.second.present?
        if time_first > time_second
          conditions << "(transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ? OR transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ?)"
          parameters << time_first
          parameters << "23:59:59"
          parameters << "00:00:00"
          parameters << time_second
        else
          if time_first.present?
            conditions << "transactions.created_at::TIME >= ?"
            parameters << time_first
          end
          if time_second.present?
            conditions << "transactions.created_at::TIME <= ?"
            parameters << time_second
          end
        end
      end
    end
    #TRACKER DATE
    if params[:shipment_date_time].present? || (params[:time3].present? && params[:time4].present?)
      time = time_parse(params[:time3],params[:time4])
      date = params[:shipment_date_time].present? ? parse_daterange(params[:shipment_date_time]) : nil
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      elsif params[:time3].present? || params[:time4].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end

      if date.present? && date.try(:[],:first_date).present?
        conditions << "trackers.shipment_date >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "trackers.shipment_date <= ?"
        parameters << date[:second_date]
      end

      zone = ActiveSupport::TimeZone[params[:offset].to_i].name if params[:offset].present?

      # If Time and Date is Selected
      if (params[:time3].present? || params[:time4].present?) && params[:shipment_date_time].present?
        time_first = time_conversion(time.first + params[:offset], zone) if time.first.present?
        time_second = time_conversion(time.second + params[:offset], zone) if time.second.present?
        if time_first > time_second
          conditions << "(trackers.shipment_date::TIME >= ? AND trackers.shipment_date::TIME <= ? OR trackers.shipment_date::TIME >= ? AND trackers.shipment_date::TIME <= ?)"
          parameters << time_first
          parameters << "23:59:59"
          parameters << "00:00:00"
          parameters << time_second
        else
          if time_first.present?
            conditions << "trackers.shipment_date::TIME >= ?"
            parameters << time_first
          end
          if time_second.present?
            conditions << "trackers.shipment_date::TIME <= ?"
            parameters << time_second
          end
        end
      end
    end

    if params[:delivery_date_time].present? || (params[:time5].present? && params[:time6].present?)
      time = time_parse(params[:time5],params[:time6])
      date = params[:delivery_date_time].present? ? parse_daterange(params[:delivery_date_time]) : nil
      if date.present?

        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      elsif params[:time5].present? || params[:time6].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end

      if date.present? && date.try(:[],:first_date).present?
        conditions << "trackers.delivered_date >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "trackers.delivered_date <= ?"
        parameters << date[:second_date]
      end
      zone = ActiveSupport::TimeZone[params[:offset].to_i].name if params[:offset].present?

      # If Time and Date is Selected
      if (params[:time5].present? || params[:time6].present?) && params[:shipment_date_time].present?
        time_first = time_conversion(time.first + params[:offset], zone) if time.first.present?
        time_second = time_conversion(time.second + params[:offset], zone) if time.second.present?
        if time_first > time_second
          conditions << "(trackers.delivered_date::TIME >= ? AND trackers.delivered_date::TIME <= ? OR trackers.delivered_date::TIME >= ? AND trackers.delivered_date::TIME <= ?)"
          parameters << time_first
          parameters << "23:59:59"
          parameters << "00:00:00"
          parameters << time_second
        else
          if time_first.present?
            conditions << "trackers.delivered_date::TIME >= ?"
            parameters << time_first
          end
          if time_second.present?
            conditions << "trackers.delivered_date::TIME <= ?"
            parameters << time_second
          end
        end
      end
    end
    #Risk Evaluation
    if params[:risk_eval].present?
      array = params[:risk_eval]
      reason = []
      action = []

      if array.any? { |s| s.include?('manual_review') }
        reason << TypesEnumLib::RiskReason::ManualReview
      end
      if array.any? { |s| s.include?('default') }
        reason << TypesEnumLib::RiskReason::Default
      end
      if array.any? {|s| s.include?("accept") }
        action << TypesEnumLib::RiskType::Accept
      end
      if array.any? {|s| s.include?("reject") }
        action << TypesEnumLib::RiskType::Reject
      end
      if array.any? {|s| s.include?("pending") }
        action << TypesEnumLib::RiskType::PendingReview
      end
      if array.any? {|s| s.include?("expired_manual_review") }
        action << TypesEnumLib::RiskType::ExpiredReview
      end
      if action.present? && reason.present?
        conditions << "(minfraud_results.qc_action IN (?) AND minfraud_results.qc_reason IN (?))"
        parameters << action
        parameters << reason

      end
    end
    conditions = [conditions.join(" AND "), *parameters]
    return conditions
  end

  def chat_search_query(params,offset = nil)
    params[:offset] = offset if offset.present?

    conditions = []
    parameters = []

    #Order ID
    if params[:order_id].present?
      conditions << "transactions.order_id = ?"
      parameters << "#{params[:order_id]}"
    end
    #Merchant NAME
    if params[:merchant_name].present?
      conditions << "transactions.receiver_name ILIKE ?"
      parameters << "#{params[:merchant_name]}"
    end
    #CUSTOMER NAME
    if params[:cust_name].present?
      conditions << "transactions.sender_name ILIKE ?"
      parameters << "%#{params[:cust_name]}%"
    end
    #Order AMOUNT
    if params[:order_amount].present?

      conditions << "transactions.total_amount::VARCHAR LIKE ?"
      parameters << "#{params[:order_amount]}%"
    end

    if offset.present?
      unless offset.include?(':')
        offset.insert(3,':')
      end
    end

    #ORDER DATE
    if params[:order_date_time].present? || (params[:time1].present? && params[:time2].present?)
      time = parse_time(params)
      date = params[:order_date_time].present? ? parse_daterange(params[:order_date_time]) : nil
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end
      if date.present? && date.try(:[],:first_date).present?
        conditions << "transactions.created_at >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "transactions.created_at <= ?"
        parameters << date[:second_date]
      end
      zone = ActiveSupport::TimeZone[params[:offset].to_i].name if params[:offset].present?

      # If Time and Date is Selected
      if (params[:time1].present? || params[:time2].present?) && params[:order_date_time].present?
        time_first = time_conversion(time.first + params[:offset], zone) if time.first.present?
        time_second = time_conversion(time.second + params[:offset], zone) if time.second.present?
        if time_first > time_second
          conditions << "(transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ? OR transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ?)"
          parameters << time_first
          parameters << "23:59:59"
          parameters << "00:00:00"
          parameters << time_second
        else
          if time_first.present?
            conditions << "transactions.created_at::TIME >= ?"
            parameters << time_first
          end
          if time_second.present?
            conditions << "transactions.created_at::TIME <= ?"
            parameters << time_second
          end
        end
      end
    end
    conditions = [conditions.join(" AND "), *parameters]
    return conditions
  end

  def dispute_search_query(params,offset = nil)
    params[:offset] = offset if offset.present?

    conditions = []
    parameters = []

    #Order ID
    if params[:order_id].present?
      conditions << "transactions.order_id = ?"
      parameters << "#{params[:order_id]}"
    end
    #Transaction ID
    if params[:trans_id].present?
      conditions << "transactions.id = ?"
      parameters << "#{params[:trans_id]}"
    end
    #Merchant NAME
    if params[:merchant_name].present?
      conditions << "transactions.receiver_name ILIKE ?"
      parameters << "%#{params[:merchant_name]}%"
    end
    #CUSTOMER NAME
    if params[:cust_name].present?
      conditions << "transactions.sender_name ILIKE ?"
      parameters << "%#{params[:cust_name]}%"
    end
    #Refund Case No
    if params[:case_no].present?
      conditions << "customer_disputes.id = ?"
      parameters << "#{params[:case_no]}"
    end
    #Status
    if params[:status].present?
      conditions << "customer_disputes.status = ?"
      parameters << "#{params[:status]}"
    end
    #Last4
    if params[:last4].present?
      conditions << "transactions.last4 = ?"
      parameters << "#{params[:last4]}"
    end

    # #Order AMOUNT
    # if params[:order_amount].present?
    #
    #   conditions << "transactions.total_amount::VARCHAR LIKE ?"
    #   parameters << "#{params[:order_amount]}%"
    # end

    if offset.present?
      unless offset.include?(':')
        offset.insert(3,':')
      end
    end

    #Creation DATE
    if params[:dispute_date].present?
      time = parse_time(params)
      date = params[:dispute_date].present? ? parse_daterange(params[:dispute_date]) : nil
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end
      if date.present? && date.try(:[],:first_date).present?
        conditions << "customer_disputes.created_at >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "customer_disputes.created_at <= ?"
        parameters << date[:second_date]
      end

    end
    conditions = [conditions.join(" AND "), *parameters]
    return conditions
  end


  def account_transactions_status_search(params)
    conditions = []
    parameters = []

    if params[:approved].present? && params[:approved] == "1"
      conditions << "transactions.status = ?"
      parameters << "approved"
    end

    if params[:refunded].present? && params[:refunded] == "1"
      conditions << "transactions.status = ?"
      parameters << "refunded"
    end

    if params[:partial_refund].present? && params[:partial_refund] == "1"
      conditions << "transactions.status = ?"
      parameters << "complete"
    end
    if params[:all].present? && params[:all] == "1"
      conditions = []
      parameters = []
    end

    conditions = [conditions.join(" OR "), *parameters]
    return conditions

  end

  def merchant_transactions_search_query(params,offset = nil)
    params[:offset] = offset if offset.present?

    conditions = []
    parameters = []

    #TXN ID
    if params[:transaction_id].present?
      conditions << "block_transactions.sequence_id ILIKE ?"
      parameters << "#{params[:transaction_id]}%"
    end
    #SENDER NAME
    if params[:name].present?
      conditions << "block_transactions.sender_name ILIKE ?"
      parameters << "%#{params[:name]}%"
    end
    #SENDER EMAIL
    if params[:email].present?
      conditions << "users.email ILIKE ?"
      parameters << "%#{params[:email]}%"
    end
    #TXN NET AMOUNT
    if params[:amount].present?
      conditions << "block_transactions.total_net::VARCHAR LIKE ?"
      parameters << "#{params[:amount]}%"
    end
    #TRANSACTION TYPE
    if params[:type].present?
      conditions << "block_transactions.main_type ILIKE ?"
      parameters << "#{params[:type]}"
    end
    #SENDER PHONE NUMBER
    if params[:phone_number].present?
      conditions << "users.phone_number ILIKE ?"
      parameters << "#{params[:phone_number]}"
    end
    #LAST4
    if params[:last4].present?
      conditions << "block_transactions.last4 LIKE ?"
      parameters << "#{params[:last4]}"
    end

    if params[:first6].present?
      conditions << "block_transactions.first6 LIKE ?"
      parameters << "#{params[:first6]}"
    end

    if offset.present?
      unless offset.include?(':')
        offset.insert(3,':')
      end
    end
    #TRANSACTION DATE
    if params[:date].present? || (params[:time1].present? && params[:time2].present?)
      time = parse_time(params)
      date = params[:date].present? ? parse_daterange(params[:date]) : nil
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end
      if date.present? && date.try(:[],:first_date).present?
        conditions << "block_transactions.timestamp >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "block_transactions.timestamp <= ?"
        parameters << date[:second_date]
      end
      # zone = ActiveSupport::TimeZone[params[:offset].to_i].name if params[:offset].present?

      # If Time and Date is Selected
      # if (params[:time1].present? || params[:time2].present?) && params[:order_date_time].present?
      #   time_first = time_conversion(time.first + params[:offset], zone) if time.first.present?
      #   time_second = time_conversion(time.second + params[:offset], zone) if time.second.present?
      #   if time_first > time_second
      #     conditions << "(transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ? OR transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ?)"
      #     parameters << time_first
      #     parameters << "23:59:59"
      #     parameters << "00:00:00"
      #     parameters << time_second
      #   else
      #     if time_first.present?
      #       conditions << "transactions.created_at::TIME >= ?"
      #       parameters << time_first
      #     end
      #     if time_second.present?
      #       conditions << "transactions.created_at::TIME <= ?"
      #       parameters << time_second
      #     end
      #   end
      # end
    end

    conditions = [conditions.join(" AND "), *parameters]
    return conditions
  end

  def accounts_search_query(query)
    conditions = []
    parameters = []

    if query.present?
      conditions << "locations.business_name ILIKE ?"
      parameters << "%#{query}%"
    end

    conditions = [conditions.join(" AND "), *parameters]
    return conditions

  end


  def save_merchant(merchant_data)
    result = nil
    @merchant = nil
    # merchant_data = merchant_data.with_indifferent_access
    params[:merchant] = merchant_data
    if params[:merchant].present?
      params[:merchant][:phone_number] = params[:merchant][:phone_code] + params[:merchant][:phone_number]
      if params[:merchant][:merchant_id].present?
        @merchant = User.merchant.friendly.find params[:merchant][:merchant_id]
        if edit_merchant_params[:password].present?
          ex=edit_merchant_params.except(:profile_attributes)
          @merchant.update(ex)
        else
          ex=edit_merchant_params.except(:password_confirmation)
          ex = ex.except(:password)
          ex = ex.except(:profile_attributes)
          if @merchant.update(ex)
            @merchant.profile.update(edit_profile_params)
          end
        end
        if @merchant.errors.blank?
          @merchant.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
          result = {success:true, merchant:@merchant.id}
        else
          result = {success:false, message:@merchant.errors.full_messages.first}
        end
      else
        params[:merchant] = params[:merchant].except(:user_id, :merchant_id)
        company = Company.find params[:merchant][:company_id]
        @merchant = User.new(new_merchant_params)
        @merchant.ledger = company.ledger if company.ledger.present?
        password = random_password
        @merchant.password = password
        @merchant.is_password_set
        @merchant.issue_fee = false
        @merchant.regenerate_token
        @merchant.merchant!
        if !@merchant.valid?
          result = {success:false, message:@merchant.errors.full_messages.first}
          # flash[:error] = @merchant.errors.full_messages.first
        elsif @merchant.merchant?
          result = {success:true, merchant_id:@merchant.id}
          # @merchant.send_welcome_email('Merchant')
        else
          result = {success:false, message:@merchant.errors.full_messages.first}
        end
      end
    end
    return result
  end

  def save_owners(owners_data)
    result = {}
    if owners_data
      # owners = owners.with_indifferent_access
      merchant_id = params[:merchant_id] if params[:merchant_id].present?
      @merchant = User.find merchant_id if merchant_id.present?
      owners_data.each do |key, value|
        if value[:phone_number].present? && value[:phone_code].present?
          value[:phone_number] = "#{value[:phone_code]}#{value[:phone_number]}"
          value[:phone_number]='' if value[:phone_number]==value[:phone_code]
        else
          value[:phone_number] = "#{value["phone_code"]}#{value["phone_number"]}"
        end
        params[:owner] = value
        if params[:owner][:phone_code].present?
          params[:owner] = params[:owner].except(:phone_code)
        end
        params[:owner][:user_id] = merchant_id if merchant_id.present?
        if params[:owner][:owner_id].present?
          owner = Owner.find params[:owner][:owner_id]

          if owner.update(new_owner_params)
            result = {success:true, message:"Owners updated Successfully"}
          else
            result = {success:false, message:"Error Occured"}
            break
          end
        else
          owner = Owner.new(new_owner_params)
          if owner.save
            result = {success:true, message:"Owners saved Successfully"}
          else
            result = {success:false, message:owner.errors.full_messages.first}
            break
          end
        end
      end
    end
    return result
  end

  def save_location(location_data)
    params[:location] = location_data
    old_location_id = nil
    result = {}
    if params[:location].present?
      params[:location][:merchant_id] = params[:merchant_id]
      if params[:old_location_id].present?
        old_location_id = params[:old_location_id]
      else
        old_location_id = params[:location][:location_id] if params[:location][:location_id].present?
      end
      if params[:merchant_id].present?
        if params[:location][:location_id].present? && params[:location][:loc] != "duplicate"
          @location = Location.find params[:location][:location_id]
          wallet = @location.wallets.primary.first
          wallet.update(name: params[:location][:business_name])
          reserve_wallet = @location.wallets.reserve.first
          if reserve_wallet.present?
            reserve_wallet.update(name: params[:location][:business_name]+" reserve")
          end
          if @location.update(new_location_params)
            result = {success:true,location_id:@location.id}
          else
            result = {success:false, message:@location.errors.full_messages.first}
          end
        elsif params[:location][:location_id].present? && params[:location][:loc] == "duplicate" && params[:duplicate] == "false"
          @location = Location.find params[:location][:location_id]
          wallet = @location.wallets.primary.first
          wallet.update(name: params[:location][:business_name])
          reserve_wallet = @location.wallets.reserve.first
          if reserve_wallet.present?
            reserve_wallet.update(name: params[:location][:business_name]+" reserve")
          end
          if @location.update(new_location_params)
            result = {success:true,old_location_id:old_location_id, location_id:@location.id, duplicate:"true"}
          else
            result = {success:false, message:@location.errors.full_messages.first}
          end
        else
          @merchant = User.find params[:merchant_id]
          @location = Location.new(new_location_params)
          @location.ach_international = params[:location][:ach_international]
          @location.ledger = @merchant.ledger if @merchant.ledger.present?
          @location.location_secure_token = SecureRandom.urlsafe_base64(10)
          # if params[:allowed].present?
          #   params[:allowed].reject {|v, k| k != "true"}.each do |key, value|
          #     if key == "1" && value == "true"
          #       @location.iso_allowed= true
          #     elsif key == "2" && value == "true"
          #       @location.agent_allowed = true
          #     end
          #   end
          # end
          # @location.web_site = params[:location][:web_site].to_json
          if params[:location][:ecom_platform].present?
            @location.ecom_platform = params[:location][:ecom_platform].to_json
          end
          if params[:location][:ecomm_platform_id].present?
            @location.ecomm_platform_id = params[:location][:ecomm_platform_id]
          end
          if @location.save
            @location.bank_account= AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @location.bank_account) if @location.bank_account.present? && @location.bank_account.length < 25
            @location.save
            if params[:location][:loc] == "duplicate"
              result = {success:true, old_location_id:old_location_id, location_id:@location.id, duplicate:"false"}
            else
              result = {success:true, location_id:@location.id}
            end
          else
            result = {success:false, message:@location.errors.full_messages.first}
          end
        end
      else
        result = {success:false, message:"You can not create without merchant"}
      end
    end
    return result
  end

  def save_iso_agent_data(iso_agent_data)
    params[:location] = iso_agent_data
    location = Location.find params[:location_id] if params[:location_id].present?
    result = {}
    if location.present?
      if params[:location][:profit_split] == "1" || params[:location][:baked_iso] == "1" || params[:location][:net_profit_split] == "1" || params[:location][:sub_merchant_split] == "1"
        profit_splits = {}
        if params[:location][:baked_iso] == "1"
          profit_splits[:share_split] = params[:location][:profit_splits]["share_split"]
        end
        profit_splits[:splits] = params[:location][:profit_splits][:splits]
        location.profit_split_detail = profit_splits.to_json
      else
        location.profit_split_detail = nil
      end
      location.update(new_location_params)
      users = params[:location][:ids].reject{|v|v.empty?}
      existing_users = location.users.pluck(:id)
      existing_users.each do|t|
        del = User.find t
        location.users.delete(del)
      end
      if params[:location][:profit_splits][:splits][:ez_merchant].present?
        splits_ez = params[:location][:profit_splits][:splits][:ez_merchant]
        splits_ez.each do |key, value|
          location_new = Location.find_by(id: value[:ez_merchant_1][:wallet]) if  value[:ez_merchant_1][:wallet].present?
          user = location_new.try(:merchant)
          if user.present?
            if value[:ez_merchant_1][:attach].present? && value[:ez_merchant_1][:attach] == "true"
              LocationsUsers.create(user_id: user.id, location_id: location.id, attach: true).secondary!
            else
              LocationsUsers.create(user_id: user.id, location_id: location.id).secondary!
            end
          end
        end
      end
      unless users.empty?
        params[:location][:ids].reject(&:blank?).each do |user|
          u = User.find(user.to_i)
          location.users <<  u
        end
      end
      if location.errors.blank?
        result ={success: true, message:"Location Iso and Agent data saved successfully."}
      else
        result = {success: false, message: location.errors.full_messages.first}
      end
    end
    return result
  end

  def save_fee(fee_data)
    params[:fee] = fee_data
    merchant_id = params[:merchant_id]
    location = Location.find params[:location_id] if params[:location_id].present?
    result = {}
    if location.present?
      params[:fee] = fee_data
      if location.fees.present?
        location.fees.update(new_fee_params)
      else
        fee = Fee.new(new_fee_params)
        fee.user_id = merchant_id if merchant_id.present?
        fee.location_id = location.id
        location.fees << fee
        location.fees.first.buy_rate!
        location.fees.first.high!
      end
      if location.fees
        result ={success: true, message:"Location Fee saved successfully."}
      else
        result = {success: false, message: location.errors.full_messages.first}
      end
    end
    return result
  end

  def save_setting(setting_data)
    params[:location] = setting_data
    params[:fee] = setting_data["fee"]
    result = {}
    location = Location.find params[:location_id] if params[:location_id].present?
    if location.present?
      params[:location][:web_site] = params[:location][:web_site].to_json
      if location.update(new_location_params)
        location.update(block_ach: params[:location][:block_withdrawal],push_to_card: params[:location][:block_withdrawal]) if params[:location][:block_withdrawal].present?
        location.fees.update(new_fee_params)
        result ={success: true, message:"Location Settings saved successfully."}
      else
        result = {success: false, message: location.errors.full_messages.first}
      end
    end
    return result
  end

  def save_bank_details(bank_account)
    result = {}
    location = Location.find params[:location_id] if params[:location_id].present?
    @bank_account_image = Image.where(bank_detail_id: params[:merchant_data_id])
    if location.present?
      bank_account.each do |key, bank|
        if bank["bank_account_type"].present?
          @bank = location.bank_details.new(
              bank_account_type: bank["bank_account_type"],
              routing_no: bank["routing_no"],
              bank_name: bank["bank_name"],
              account_no: bank["account_no"])
          @bank.account_no = AESCrypt.encrypt("#{location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @bank.account_no) if @bank.account_no.present?
          @bank.save
          if bank["saved_image_id"].present?
            img = Image.find_by(id: bank["saved_image_id"])
            img.update(bank_detail_id: @bank.id)
          end
        end
      end
      result ={success: true, message:"Bank account saved successfully."}
    else
      result = {success: false, message: location.errors.full_messages.first}
    end
    return result
  end

  def upload_docs(image, id)
    bank = BankDetail.find(id) if id.present?
    if bank.present? && bank.image.present?
      @old_img = Image.where(bank_detail_id: id).last
      @old_img.delete
    end
    @img = Image.new(image)
    if @img.validate
      @img.save
    else
      @error = @img.errors.full_messages.first
    end
    @img.id
  end

  def save_documentation(documentation_data, duplicate_document= nil)
    old_location_id = nil
    location_id = params[:location_id] if params[:location_id].present?
    location = Location.find location_id if location_id.present?
    location = Location.where(merchant_id: params[:user][:form_id] ).first if params[:user][:form_id].present?
    location = @location  if @location.present? && !params[:merchant_id].present?
    location = @new_location  if @new_location.present?
    result = {}
    duplicate = false
    if !duplicate_document
      @old_location = Location.find old_location_id if old_location_id.present?
      @old_documents = @old_location.documentations if @old_location.present?
    end
    if @old_documents.present? && location.present?
      @old_documents.each do |doc|
        doc_url = "https:"+doc.document.url
        doc_file = open(doc_url)
        doc_file.read.force_encoding(Encoding::UTF_8)
        @documentation = Documentation.new(:document => doc_file, :image_type => doc.try(:image_type), :imageable_id => location.id, :imageable_type => "Location")
        if @documentation.save
          duplicate = true
          result = {success: true, message:"Location Documentation saved successfully."}
        else
          result = {success: true, message:@documentation.errors.full_messages.first}
        end
      end
    end
    if location.present?
      # if params[:user].present?
      if documentation_data.present?
        documentation_data.clone.each do |key, value|
          if value.count > 1
            value.each do |v|
              document = v
              type = Documentation.types[key]
              document.read.force_encoding(Encoding::UTF_8) if document.present?
              @documentation = Documentation.new(:document => document, :image_type => type, :imageable_id => location.id, :imageable_type => "Location")
              if @documentation.save
                result ={success: true, message:"Location Documentation saved successfully."}
              else
                result = {success: false, message: @documentation.errors.full_messages.first}
              end
            end
          else
            document = value.first
            type = Documentation.types[key]
            document.read.force_encoding(Encoding::UTF_8) if document.present?
            @documentation = Documentation.new(:document => document, :image_type => type, :imageable_id => location.id, :imageable_type => "Location")
            if @documentation.save
              result ={success: true, message:"Location Documentation saved successfully."}
            else
              result = {success: false, message: @documentation.errors.full_messages.first}
            end
          end
        end
      end
      # end
    end
    if duplicate
      result[:loc] = "duplicate"
    end
    return result
  end

  #= for iso, agent & affiliate transaction search
  def searching_iso_local_transaction(params, wallets,first_date=nil,second_date=nil,user=nil,offset=nil,from_worker=nil,hostname=nil,email=nil, txn_type=nil)
    conditions = []
    parameters = []
    if params[:name].present?
      conditions << "(lower(merchant_name) LIKE ?)"
      parameters << "%#{params[:name].try(:downcase)}%"
    end
    if params[:Receiver_name].present?
      params[:Receiver_name]=params[:Receiver_name].strip
      conditions <<  "(lower(receiver_name) LIKE ?)"
      parameters << "%#{params[:Receiver_name].try(:downcase)}%"
    end
    if params[:sender_name].present?
      params[:sender_name]=params[:sender_name].strip
      conditions <<  "(lower(sender_name) LIKE ?)"
      parameters << "%#{params[:sender_name].try(:downcase)}%"
    end
    dba_name = ''
    if params[:DBA_name].present?
      if params[:DBA_name].kind_of?(Array)
        dba_name = params[:DBA_name]
      else
        dba_name = JSON.parse(params[:DBA_name])
      end
    end
    if dba_name.present?
      conditions<< "(location_dba_name IN (?) OR tags->'location'->>'business_name' IN (?))"
      parameters<< dba_name
      parameters << dba_name
    end

    if params[:dba_name].present?
      params[:dba_name] = params[:dba_name].strip
      conditions << "(location_dba_name ILIKE (?) OR tags->'location'->>'business_name' ILIKE (?))"
      parameters << "%#{params[:dba_name]}%"
      parameters << "%#{params[:dba_name]}%"
    end
    if params[:amount].present?
      if @user.try(:merchant?)
        conditions << "(amount = ?)"
        parameters << params[:amount]
      elsif (@user.try(:iso?) || @user.try(:agent?) || @user.try(:affiliate?)) && params[:amount].to_f > 0 && txn_type != "commission"
          conditions << "(amount_in_cents/100.0 = ?)"
          parameters << params[:amount]
      else
        new_var=number_with_precision(params[:amount], precision: 2)
        var=new_var.to_s.split('.').last
        storing_param=new_var.to_f
        if var=="00"
          storing_param=new_var
          storing_param=number_with_precision(storing_param, precision: 1).to_f
        end
        conditions << "(block_transactions.main_amount = ? OR tags->'main_transaction_info'->>'amount' = ?)"
        parameters << params[:amount].to_f
        parameters << "#{storing_param}"
      end
    end
    if params[:offset].present?
      unless params[:offset].include?(':')
        params[:offset].insert(3,':')
      end
    end
    time = parse_time(params)
    date = parse_daterange(params[:date]) if params[:date].present?
    date = parse_daterange(params[:export_date]) if date.blank? && params[:export_date].present?
    date = parse_daterange(params[:datie]) if params[:datie].present? && date.blank?
    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
      }
    elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
      date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    end
    if date.present? && date.try(:[],:first_date).present?
      conditions << "(block_transactions.timestamp >= ?)"
      parameters << date[:first_date]
    end
    if date.present? && date.try(:[],:second_date).present?
      conditions << "(block_transactions.timestamp <= ?)"
      parameters << date[:second_date]
    end

    # If Time and Date is Selected
    if (params[:time1].present? || params[:time2].present?) && params[:date].present?
      time_first = time_conversion(time.first + params[:offset]) if time.first.present?
      time_second = time_conversion(time.second + params[:offset]) if time.second.present?
      if time_first > time_second
        conditions << "(block_transactions.created_at::TIME >= ? AND block_transactions.created_at::TIME <= ? OR block_transactions.created_at::TIME >= ? AND block_transactions.created_at::TIME <= ?)"
        parameters << time_first
        parameters << "23:59:59"
        parameters << "00:00:00"
        parameters << time_second
      else
        if time_first.present?
          conditions << "(block_transactions.created_at::TIME >= ?)"
          parameters << time_first
        end
        if time_second.present?
          conditions << "(block_transactions.created_at::TIME <= ?)"
          parameters << time_second
        end
      end
    end
    type = ''
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if params[:type1].present?
      if params[:type1].kind_of?(Array)
        type1 = params[:type1]
      else
        type1 = JSON.parse(params[:type1])
      end
    end
    if type.present?
      if type.include?"Void Ach"
        type << "Void ach"
      end
      conditions << "(main_type IN(?) OR tags ->> 'sub_type' IN(?) OR sub_type IN (?))"
      parameters << type
      parameters << type
      parameters << type
    end
    if type1.present?
      if type1.include?"debit_card"
        type1 << "debit_charge"
      end
      conditions << "(tags->'main_transaction_info'->>'transaction_type' IN(?) OR tags->'main_transaction_info'->>'transaction_sub_type' IN(?) OR tx_type IN (?) OR tx_sub_type IN (?))"
      parameters << type1
      parameters << type1
      parameters << type1
      parameters << type1
    end
    name = ''
    if params[:wallet_ids].present?
      if params[:wallet_ids].kind_of?(Array)
        name = params[:wallet_ids]
      else
        name = JSON.parse(params[:wallet_ids])
      end
    end
    if params[:wallets_id].present?
      if params[:wallets_id].kind_of?(Array)
        name = params[:wallets_id]
      else
        name = JSON.parse(params[:wallets_id])
      end
    end
    if name.present?
      conditions<< "(tags->'merchant'->>'id' IN (?)) "
      parameters<< name
    end
    if params[:block_id].present?
      conditions << "(sequence_id LIKE ? OR block_transactions.quickcard_id::VARCHAR LIKE ?)"
      parameters << "#{params[:block_id].split.join}%"
      parameters << "#{params[:block_id].split.join}%"
    end
    result = {no_transaction: true}
    batch_size = ENV['BATCH_SIZE'].try(:to_i)|| 20000
    conditions = [conditions.join(" AND "), *parameters]
    if from_worker
      BlockTransaction.where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where(conditions).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check"]).find_in_batches(batch_size: batch_size).with_index do |transactions,index|
        count = index + 1
        result.delete(:no_transaction)
        response = creating_export_file(transactions,user,first_date,second_date,offset,hostname,email,count,nil)
        result.merge!({"#{index}":response})
      end
      return result
    else
      t = BlockTransaction.where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where(conditions).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check"])
    end
    return t
  end

  #= for merchant transaction search
  def searching_merchant_local_transaction(params, wallets = nil, trans_type=nil,first_date=nil,second_date=nil,user=nil,offset=nil,from_worker=nil,host_name=nil,filter=nil, page_params = nil, email=nil)
    amount = nil
    per_page = filter || 10
    conditions = []
    parameters = []
    @wallet = @wallet || Wallet.find_by(id: wallets) if wallets.present?
    if trans_type == "decline"
      location_id = @location.try(:id)
      location_id= Wallet.find_by(id:wallets).try(:location_id) if location_id.blank?
    end
    if params[:chargeback_transaction_id].present?
      conditions << "id IN (?)"
      parameters << params[:chargeback_transaction_id]
    end
    if params[:amount].present?
      new_var=params[:amount]
      new_var=number_with_precision(params[:amount], precision: 2)
      var=new_var.to_s.split('.').last
      storing_param=new_var
      if var == "00"
        storing_param=new_var
        storing_param=new_var.to_s.split('.').first
        storing_param=storing_param.to_i.abs
      else
        storing_param=number_with_precision(params[:amount], precision: 2).to_f.abs
      end

      if !@wallet.try(:primary?)
        storing_param=(storing_param*100).to_i
        if params[:amount].include?('-')
          conditions << "block_transactions.action=? and block_transactions.amount_in_cents::VARCHAR LIKE ?"
          parameters << BlockTransaction.actions[:retire]
          parameters << "#{storing_param}".to_s
        else
          conditions << "block_transactions.action !=? and block_transactions.amount_in_cents::VARCHAR LIKE ?"
          parameters << BlockTransaction.actions[:retire]
          parameters << "#{storing_param}".to_s
        end
      else
        if params[:amount].include?('-')
          conditions << "transactions.action=? and transactions.total_amount::VARCHAR LIKE ?"
          parameters << "retire"
          parameters << "#{storing_param}".to_s
        else
          conditions << "transactions.action !=? and transactions.total_amount::VARCHAR LIKE ?"
          parameters << "retire"
          parameters << "#{storing_param}".to_s
        end
      end
      # if !@wallet.primary?
      #   conditions << "block_transactions.tx_amount::VARCHAR LIKE ?"
      # else
      #   conditions << "transactions.total_amount::VARCHAR LIKE ?"
      # end
      # parameters << "#{storing_param}%".to_s
    end
    if params[:offset].present? && !params[:offset].include?(':')
      params[:offset].insert(3,':')
    end
    date = parse_daterange(params[:date]) if params[:date].present?
    date = parse_daterange(params[:export_date]) if date.blank? && params[:export_date].present?
    time = parse_time(params)
    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
      }
    elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
      date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    end

    if date.present?
      if date[:first_date].present?
        if !@wallet.try(:primary?)
          conditions << "block_transactions.created_at >= ?"
        else
          conditions << "transactions.created_at >= ?"
        end
        parameters << date[:first_date]
      end
      if date[:second_date].present?
        if !@wallet.try(:primary?)
          conditions << "block_transactions.created_at <= ?"
        else
          conditions << "transactions.created_at <= ?"
        end
        parameters << date[:second_date]
      end
    end

    if (params[:time1].present? || params[:time2].present?) && params[:date].present?
      time_first = time_conversion(time.first + params[:offset]) if time.first.present?
      time_second = time_conversion(time.second + params[:offset]) if time.second.present?
      if time_first > time_second
        conditions << "(transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ? OR transactions.created_at::TIME >= ? AND transactions.created_at::TIME <= ?)"
        parameters << time_first
        parameters << "23:59:59"
        parameters << "00:00:00"
        parameters << time_second
      else
        if time_first.present?
          conditions << "transactions.created_at::TIME >= ?"
          parameters << time_first
        end
        if time_second.present?
          conditions << "transactions.created_at::TIME <= ?"
          parameters << time_second
        end
      end
    end

    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if type.present?
      if  type.include?("invoice-qc")
        if type.count == 1
          conditions << "(main_type IN (?) AND sub_type IN (?) )"
          parameters << "B2B Transfer"
          parameters << "invoice"
        else
          if type.include?("Void Check")
            type.push("Void_Check")
          elsif type.include?("Void_Check")
            type.push("Void Check")
          end
          conditions << "((main_type IN (?) AND sub_type IN (?)  ) OR (main_type IN (?) OR sub_type IN (?) ))"
          parameters << ["B2B Transfer"]
          parameters << ["invoice"]
          parameters << type
          parameters << type
        end
      else
      if type == ["Void ACH"]
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << ["Void_Ach", "Void ACH"]
        parameters << ["Void_Ach", "Void ACH"]
      elsif type== ["Void Check"]
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << ["Void_Check", "Void Check"]
        parameters << type
      else
        if type.include?("Void ACH")
          type.push("Void_Ach")
        end
        if type.include?("Void Check")
          type.push("Void_Check")
        end
        conditions << "(main_type IN (?) OR sub_type IN (?) )"
        parameters << type
        parameters << type
      end
      end
    end
    if params[:transaction_id].present?
      if !@wallet.try(:primary?)
        conditions << "block_transactions.sequence_id  LIKE ?"
      else
        conditions << "transactions.seq_transaction_id  LIKE ? OR transactions.quickcard_id::VARCHAR LIKE ?"
      end
      parameters << "%#{params[:transaction_id]}%"
      parameters << "#{params[:transaction_id]}%"
    end
    if params[:phone_number].present?
      conditions << "users.phone_number ILIKE ?"
      parameters << "%#{params[:phone_number]}%"
    end

    if params[:email].present?
      conditions << "users.email LIKE ?"
      parameters << params[:email]
    end

    if params[:name].present?
      conditions << "sender_name ILIKE ?"
      parameters << "%#{escape_like(params[:name].try(:strip))}%"
    end

    last4 = params[:last4].present? ? params[:last4].split.join : nil
    if last4.present?
      if !@wallet.try(:primary?)
        conditions << "block_transactions.last4 LIKE ?"
      else
        conditions << "transactions.last4 LIKE ?"
      end
      parameters << "#{last4}%"
    end
    first6 = params[:first6].present? ? params[:first6].split.join : nil
    if first6.present?
      conditions << "transactions.first6 LIKE ?"
      parameters << "#{first6}%"
    end
    unless conditions.empty?
      conditions = [conditions.join(" AND "), *parameters]
    end
    result = {no_transaction: true}
    batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
    if from_worker
      if !@wallet.try(:primary?)
        BlockTransaction.joins(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) AND block_transactions.main_type IN(?) ",wallets, wallets,["Reserve Money Return", "Reserve Money Deposit","Payed Tip","Account Transfer","Sale Tip"]).where(conditions).find_in_batches(batch_size: batch_size).with_index do |transactions, index|
          count = index + 1
          result.delete(:no_transaction)
          response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,'not_primary')
          result.merge!({"#{index}"=>response})
        end
      else
        if trans_type == "decline"
          Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where(status: "pending").where.not(main_type: "Sale Issue").where(conditions).or(Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("tags->'location'->>'id' = ? AND transactions.status LIKE ?","#{location_id}", 'pending').where(conditions)).find_in_batches(batch_size: batch_size).with_index do |transactions, index|
            count = index + 1
            result.delete(:no_transaction)
            response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,nil)
            result.merge!({"#{index}"=>response})
          end
        elsif trans_type == "refund"
          Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where(main_type: "refund", status: "approved").where(conditions).find_in_batches(batch_size: batch_size).with_index do |transactions,index|
            count = index + 1
            result.delete(:no_transaction)
            response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,nil)
            result.merge!({"#{index}"=>response})
          end
        else
          Transaction.includes(:sender,:receiver).references(:sender,:receiver).where.not(main_type: "refund_fee").where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where.not(main_type: "Sale Issue", status: "pending").where(conditions).find_in_batches(batch_size: batch_size).with_index do |transactions,index|
            count = index + 1
            result.delete(:no_transaction)
            response = creating_export_file(transactions,user,first_date,second_date,offset,host_name,email,count,nil)
            result.merge!({"#{index}"=>response})
          end
        end
      end
      return result
    else
      if @wallet.present?
        if !@wallet.try(:primary?)
          t = BlockTransaction.joins(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) AND block_transactions.main_type IN(?) ",wallets, wallets,["Reserve Money Return", "Reserve Money Deposit","Account Transfer","Sale Tip"]).where(conditions).per_page_kaminari(page_params).per(per_page)
          return t
        end
      end

      if trans_type == "decline"
        t = Transaction.includes(:sender, :receiver).references(:sender, :receiver).where("(receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR tags->'location'->>'id' = ?) AND transactions.status = ? AND transactions.main_type != ?",wallets, wallets,"#{location_id}","pending", "Sale Issue").where(conditions).order(created_at: :desc).per_page_kaminari(page_params).per(per_page)
        # t = Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where(status: "pending").where.not(main_type: "Sale Issue").where(conditions).or(Transaction.includes(:sender,:receiver).references(:sender,:receiver).where("tags->'location'->>'id' = ? AND transactions.status LIKE ?","#{location_id}", 'pending').where(conditions)).order(created_at: :desc).per_page_kaminari(page_params).per(per_page)
      elsif trans_type == "refund"
        t = Transaction.includes(:sender, :receiver).references(:sender, :receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where(main_type: "refund", status: "approved").where(conditions).order(created_at: :desc).per_page_kaminari(page_params).per(per_page)
      else
        t = Transaction.includes(:sender, :receiver).references(:sender, :receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?) OR transactions.from LIKE ? OR transactions.to LIKE ?",wallets, wallets, "#{wallets}", "#{wallets}").where.not(main_type: "Sale Issue", status: "pending").where(conditions).order(created_at: :desc).per_page_kaminari(page_params).per(per_page)
      end
    end
    return t
  end

  #for ledger wallets transaction page search
  def ledger_wallets_search(params, wallet_id, page_size, page, offset)
    conditions = []
    parameters = []
    if params["id"].present?
      conditions << "sequence_id ILIKE ?"
      parameters << "#{params["id"]}%"
    end
    if params["timestamp"].present?
      date = parse_date(params["timestamp"])
      if date.present?
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, offset).to_datetime
        second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, offset).to_datetime
        if first_date.present?
          conditions << "timestamp >= ?"
          parameters << first_date
        end
        if second_date.present?
          conditions << "timestamp <= ?"
          parameters << second_date
        end
      end
    end
    if params["type"].present?
      type = (valid_json?(params.try(:[], "type").to_s) ? JSON.parse(params.try(:[], "type").to_s) : params.try(:[], "type"))
      types = []
      type.each do |single_type|
        types << "main_type ILIKE ? OR "
        parameters << "%#{single_type}%"
      end
      conditions << types.join[0...-3]
    end
    if params["merchant_type"].present?
      merchant_type = (valid_json?(params.try(:[], "merchant_type").to_s) ? JSON.parse(params.try(:[], "merchant_type").to_s) : params.try(:[], "merchant_type"))
      merchant_type.push('credit_card') if merchant_type.include?('Credit Card')
      merchant_type.push('debit_card') if merchant_type.include?('Debit Card')
      if @user.qc?
        conditions << "(tags->'main_transaction_info'->>'transaction_type' IN (?) OR tx_type IN (?) OR sub_type IN (?))"
        parameters << merchant_type
        parameters << merchant_type
        parameters << merchant_type
      else
        conditions << "(tags->'main_transaction_info'->>'transaction_type' IN (?) OR tx_type IN (?))"
        parameters << merchant_type
        parameters << merchant_type
      end
    end
    if params["merchant_name"].present?
      conditions << "merchant_name ILIKE ?"
      parameters << "%#{params["merchant_name"]}%"
    end
    if params["amount"].present?
      conditions << 'main_amount = ?'
      parameters << params["amount"]
    end
    if params["sender_name"].present?
      conditions << "sender_name ILIKE ?"
      parameters << "%#{params["sender_name"]}%"
    end
    if params["receiver_name"].present?
      conditions << "receiver_name ILIKE ?"
      parameters << "%#{params["receiver_name"]}%"
    end
    if params["business_name"].present?
      conditions << "location_dba_name ILIKE ?"
      parameters << "%#{params["business_name"]}%"
    end
    if params["gateway"].present?
      conditions << "gateway ILIKE ?"
      parameters << "%#{params["gateway"]}%"
    end
    if params["commission"].present?
      conditions << "amount_in_cents = ?"
      parameters << SequenceLib.cents(params["commission"])
    end
    unless conditions.empty?
      conditions = [conditions.join(" AND "), *parameters]
    end
    if params["commission"].present?
      BlockTransaction.where("sender_wallet_id = :wallet OR receiver_wallet_id = :wallet",wallet: wallet_id).where(conditions).where.not(main_type:["Send Check", "ACH Deposit", "Instant Pay"]).order(timestamp: :desc).per_page_kaminari(page).per(page_size)
    else
      BlockTransaction.where("sender_wallet_id = :wallet OR receiver_wallet_id = :wallet",wallet: wallet_id).where(conditions).order(timestamp: :desc).per_page_kaminari(page).per(page_size)
    end
  end

  def escape_like(field)
    field.gsub(/[_%]/) { |x| "\\#{x}" }
  end

  def creating_export_file(transactions,user,first_date,second_date,offset,host_name=nil,email=nil,count=nil,wallet=nil,type=nil)
    transactions_count = transactions.try(:count)
    ActionCable.server.broadcast "report_channel_#{host_name}", channel_name: "report_channel_#{host_name}", action: 'merchantexport' , message: {count: transactions_count}
    transactions_csv = CSV.generate do |csv|
      if user.MERCHANT?
        if wallet == "not_primary"
          csv << ["Txn ID","Date/Time", "Sender Name", "Reciever Name", "Type", "Descriptor", "C.C #", "Total Amount"]

          transactions.each.with_index(1) do |transaction, idx|
            if transaction.last4.present?
              last4 = "****#{transaction.last4}"
            elsif transaction.parent_id.present?
              card_details =BlockTransaction.where(id: transaction.parent_id).first
              if card_details.present?
                last4 = "****#{card_details[:tags].try(:[],'previous_issue').try(:[],'tags').try(:[],'card').try(:[],'last4')}"
              else
                last4 = "--"
              end
            else
              last4 = "---"
            end
            if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
              amount = number_with_precision(number_to_currency(transaction.tx_amount.to_f), precision: 2)
            else
              if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
                amount = "+#{number_with_precision(number_to_currency(transaction.total_net.to_f), precision: 2)}"
              elsif transaction.main_type=='refund'
                amount = "-#{transaction.total_net > 0 ? number_with_precision(number_to_currency(transaction.total_net.to_f), precision: 2) : number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
              else
                amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{transaction.total_net > 0 ? number_with_precision(number_to_currency(transaction.total_net.to_f), precision: 2) : number_with_precision(number_to_currency(transaction.tx_amount.to_f), precision: 2) }"
              end
            end
            csv << [
                transaction.sequence_id.present? ? "#{transaction.sequence_id}" : nil,
                transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                transaction.sender.try(:name).present? ? transaction.sender.try(:name) :  '',
                transaction.receiver.try(:name).present? ? transaction.receiver.try(:name) :  '',
                show_local_type_for_worker(transaction),
                show_other_descriptor_for_worker(transaction),
                last4,
                amount
            ]
          end
        else
          csv << ["Txn ID","Date/Time", "Sender Name", "Reciever Name", "Type", "Descriptor", "C.C #", "C.C Type", "Total Amount","Tx Amount","Privacy Amount","Tip","Fee","Net Fee","Total Net","Reserve Amount","Discount","Clerk ID"]
          transactions.each.with_index(1) do |transaction, idx|
            total_amount = transaction.total_amount
            if transaction.main_type == "cbk_fee"
              total_amount = transaction.total_amount.present? && transaction.total_amount > 0 ? transaction.total_amount : transaction.amount
            end
            if transaction.main_type == "cbk_hold"
              total_amount = transaction.total_amount.present? && transaction.total_amount > 0 ? transaction.total_amount : transaction.amount.to_f + transaction.fee.to_f
            end
            symbol = transaction.retire? ? "" : ""
            descriptor1 = show_descriptor(transaction)
            first6 = transaction.first6.present? ? "#{transaction.first6}*****" : transaction.card.present? ? "#{transaction.card.first6}*****" : ""
            if first6.present?
              last4 = transaction.last4.present? ? "#{transaction.last4}" : transaction.card.present? ? "#{transaction.card.last4}" : ""
            else
              last4 = transaction.last4.present? ? "*****#{transaction.last4}" : transaction.card.present? ? "*****#{transaction.card.last4}" : ""
            end

            if transaction.main_type == "Send Check"
              net_fee = fee = transaction.gbox_fee.to_f + transaction.iso_fee.to_f + transaction.agent_fee.to_f + transaction.affiliate_fee.to_f
            else
              if transaction.try(:tags).try(:[], "fee_perc").try(:[], "iso").present? && transaction.try(:tags).try(:[], "fee_perc").try(:[], "iso").try(:[], "iso_buyrate").present?
                if transaction.try(:main_type) == "refund"
                  fee = transaction.try(:tags).try(:[], "fee_perc").try(:[], "gbox").try(:[], "amount")
                  net_fee = fee
                  total_amount = transaction.try(:amount).to_f + fee.to_f
                else
                  net_fee = transaction.net_fee
                  fee = transaction.fee
                end
              else
                net_fee = transaction.net_fee
                fee = transaction.fee
              end
            end

            card_number = "#{first6}#{last4}"
            csv << [
                transaction.status == "pending" ? transaction.seq_transaction_id : transaction.seq_transaction_id.present? ? "#{transaction.seq_transaction_id}" : "#{transaction.id}",
                transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                transaction.from.present? && transaction.from == 'Credit'? "Credit" : transaction.from.present? && transaction.from == 'Debit'? "Debit" : transaction.sender.try(:name).present? ? transaction.sender.try(:name) : transaction.from.present? && transaction.from.gsub(/[^0-9]/, '').present?  ? Wallet.find_by(id: transaction.from).try(:name) : '',
                transaction.main_type == "Sale Issue"? Wallet.find_by(id: user.try(:wallet_id)).try(:name) : transaction.tags.try(:[],"send_check_user_info").try(:[],"name").present? ? transaction.tags["send_check_user_info"].try(:[],"name") : transaction.to.present? ? Wallet.find_by(id:transaction.to).try(:name) : transaction.receiver.present? ? transaction.receiver.try(:name) : '',
                main_type_format(transaction.main_type),
                transaction.payment_gateway.present? ? transaction.payment_gateway.try(:name): "#{descriptor1}",
                card_number,
                transaction.card.present? ? transaction.card.try(:card_type) : "-",
                "#{symbol}" + number_to_currency(number_with_precision(total_amount), :precision => 2, delimiter: ','),
                transaction.amount.present? ? "$#{number_with_precision(transaction.amount.to_f, :precision => 2, delimiter: ',')}": "0.00",
                "$#{number_with_precision(transaction.privacy_fee.to_f, :precision => 2, delimiter: ',')}",
                "$#{number_with_precision(transaction.tip.to_f, :precision => 2, delimiter: ',')}",
                "$#{number_with_precision(fee.to_f, :precision => 2, delimiter: ',')}",
                "$#{number_with_precision(net_fee.to_f, :precision => 2, delimiter: ',')}",
                "$#{number_with_precision(transaction.net_amount.to_f, :precision => 2, delimiter: ',')}",
                "$#{number_with_precision(transaction.reserve_money.to_f, :precision => 2, delimiter: ',')}",
                "$#{number_with_precision(transaction.discount.to_f, :precision => 2, delimiter: ',')}",
                transaction.clerk_id.present? ? transaction.clerk_id : "-"
            ]
          end
        end
      elsif user.affiliate_program?
        if type == "personal"
          csv << ["Txn ID","Date/Time","Sender", "Receiver","Type","Total Amount"]
          transactions.each.with_index(1) do |transaction, idx|
            if ["Send Check","ACH Deposit","Instant Pay", "Account Transfer","Void Check", "Void Ach","Void ACH","Void Push To Card"].include? transaction.main_type
              amount = number_with_precision(number_to_currency(SequenceLib.dollars(transaction.amount_in_cents)), precision: 2)
            elsif transaction.main_type == "Ach"
              amount = number_with_precision(number_to_currency(SequenceLib.dollars(transaction.amount_in_cents.to_f) + SequenceLib.dollars(transaction.fee_in_cents.to_f)), precision: 2)
            else
              amount = number_with_precision(number_to_currency(get_main_transaction_amount(transaction)), precision: 2)
            end
            merchant_txn_type = show_iso_local_type(transaction)
            csv << [
              transaction.sequence_id.present? ? "#{transaction.sequence_id}" : '' ,
              transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
              change_wallet_name(transaction.sender_name || transaction.try(:sender).try(:name) || transaction.sender_wallet.try(:users).try(:first).try(:name) || transaction.sender_wallet.try(:name) || ""),
              change_wallet_name(transaction.receiver_name || transaction.try(:receiver).try(:name) || transaction.receiver_wallet.try(:users).try(:first).try(:name) || transaction.receiver_wallet.try(:name) || ""),
              main_type_format(transaction.main_type),
              amount
            ]
          end
        else
          csv << ["Txn ID","Date/Time", "Partner","Type","Transaction Amount"]
          transactions.each.with_index(1) do |transaction, idx|
            if ["Send Check","ACH Deposit","Instant Pay", "Account Transfer","Void Check", "Void Ach","Void ACH","Void Push To Card"].include? transaction.main_type
              amount = number_with_precision(number_to_currency(SequenceLib.dollars(transaction.amount_in_cents)), precision: 2)
            else
              amount = number_with_precision(number_to_currency(get_main_transaction_amount(transaction)), precision: 2)
            end
            admin_user = User.find_by_id(user.admin_user_id)
            if admin_user.try(:profile).present? 
              partner_name = admin_user.profile.company_name.present? ? admin_user.profile.company_name : 'n/a'
            else 
              partner_name = admin_user.try(:company_name).present? ? admin_user.try(:company_name)  : '--'
            end 
            merchant_txn_type = show_iso_local_type(transaction)
            csv << [
                transaction.sequence_id.present? ? "#{transaction.sequence_id}" : '' ,
                transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                partner_name,
                main_type_format(transaction.main_type),
                amount
            ]
          end
        end
      else
        csv << ["Txn ID","Date/Time", "Main Type", "Merchant Name", "DBA Name", "Merchant Txn Type", "Transaction Amount", "Commission", "Sender", "Receiver"]
        transactions.each.with_index(1) do |transaction, idx|
          # symbol = transaction.retire? ? "" : ""
          if ["Send Check", "ACH Deposit", "Instant Pay"].include? transaction.main_type
            comission = "--"
          else
            comission = number_to_currency(number_with_precision(SequenceLib.dollars(transaction.amount_in_cents), :precision => 2, delimiter: ','))
          end
          if ["Send Check","ACH Deposit","Instant Pay", "Account Transfer","Void Check", "Void Ach","Void ACH","Void Push To Card"].include? transaction.main_type
            amount = number_with_precision(number_to_currency(SequenceLib.dollars(transaction.amount_in_cents)), precision: 2)
          else
            amount = number_with_precision(number_to_currency(get_main_transaction_amount(transaction)), precision: 2)
          end
          merchant_txn_type = show_iso_local_type(transaction)
          csv << [
              transaction.sequence_id.present? ? "#{transaction.sequence_id}" : '' ,
              transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
              main_type_format(transaction.main_type),
              transaction.merchant_name || transaction.tags.try(:[], "merchant").try(:[], "name") || "--",
              transaction.location_dba_name || transaction.tags.try(:[],"location").try(:[], "business_name") || "--",
              merchant_txn_type.try(:downcase) == "ach deposit" ? "ACH Deposit" : merchant_txn_type,
              amount,
              comission,
              change_wallet_name(transaction.sender_name || transaction.try(:sender).try(:name) || transaction.sender_wallet.try(:users).try(:first).try(:name) || transaction.sender_wallet.try(:name) || ""),
              change_wallet_name(transaction.receiver_name || transaction.try(:receiver).try(:name) || transaction.receiver_wallet.try(:users).try(:first).try(:name) || transaction.receiver_wallet.try(:name) || "")
          ]
        end
      end
    end
    file = StringIO.new(transactions_csv)
    qc_file = QcFile.new(name: "transactions_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
    qc_file.image = file
    qc_file.image.instance_write(:content_type, 'text/csv')
    qc_file.image.instance_write(:file_name, "transactions_export.csv")
    qc_file.save
    return {file_url: qc_file.image.url, qc_file_id: qc_file.id}
  end

  # def get_fee(user)
  # duplicate code if someone needs it please change its name then use it
  #   l = Location.where(merchant_id: user)
  #   if l.present?
  #     l = l.last.fees
  #     return number_with_precision(l, precision: 2).to_f
  #   end
  # end

  def creating_funding_export_file(transactions,user,first_date,second_date,offset,host_name=nil,email=nil,count=nil,wallet=nil,reserve=nil)
    transactions_count = transactions.try(:count)
    # ActionCable.server.broadcast "report_channel_#{host_name}", channel_name: "report_channel_#{host_name}", action: 'merchantexport' , message: {count: transactions_count}
    transactions_csv = CSV.generate do |csv|
      if user.MERCHANT?
        if reserve.present? && reserve == "reserve"
          csv << ["Txn ID","Date/Time", "Sender Name", "Reciever Name", "Type", "Descriptor", "C.C #", "Total Amount","Reserve Amount"]
          transactions.each.with_index(1) do |trans, idx|
            transaction=Transaction.where(seq_transaction_id:trans.try(:seq_parent_id))
            if transaction.present?
              transaction=transaction.first
              total_amount = transaction.total_amount
              symbol = transaction.retire? ? "" : ""
              descriptor1 = show_descriptor(transaction)
              first6 = transaction.first6.present? ? "#{transaction.first6}*****" : transaction.card.present? ? "#{transaction.card.first6}*****" : ""
              if first6.present?
                last4 = transaction.last4.present? ? "#{transaction.last4}" : transaction.card.present? ? "#{transaction.card.last4}" : ""
              else
                last4 = transaction.last4.present? ? "*****#{transaction.last4}" : transaction.card.present? ? "*****#{transaction.card.last4}" : ""
              end
              card_number = "#{first6}#{last4}"
              csv << [
                  transaction.status == "pending" ? transaction.seq_transaction_id : transaction.seq_transaction_id.present? ? "#{transaction.seq_transaction_id}" : "#{transaction.id}",
                  transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                  transaction.from.present? && transaction.from == 'Credit'? "Credit" : transaction.from.present? && transaction.from == 'Debit'? "Debit" : transaction.sender.try(:name).present? ? transaction.sender.try(:name) : transaction.from.present? && transaction.from.gsub(/[^0-9]/, '').present?  ? Wallet.find_by(id: transaction.from).try(:name) : '',
                  transaction.main_type == "Sale Issue"? Wallet.find_by(id: user.try(:wallet_id)).try(:name) : transaction.tags.try(:[],"send_check_user_info").try(:[],"name").present? ? transaction.tags["send_check_user_info"].try(:[],"name") : transaction.to.present? ? Wallet.find_by(id:transaction.to).try(:name) : transaction.receiver.present? ? transaction.receiver.try(:name) : '',
                  main_type_format(transaction.main_type),
                  transaction.payment_gateway.present? ? transaction.payment_gateway.try(:name): "#{descriptor1}",
                  card_number,
                  "#{symbol}" + number_to_currency(number_with_precision(total_amount), :precision => 2, delimiter: ','),
                  "$#{number_with_precision(transaction.reserve_money.to_f, :precision => 2, delimiter: ',')}",
              ]
            end
          end
        else
          csv << ["Txn ID","Date/Time", "Sender Name", "Reciever Name", "Type", "Descriptor", "C.C #", "Total Amount"]
          transactions.each.with_index(1) do |transaction, idx|
            total_amount = transaction.total_amount
            symbol = transaction.retire? ? "" : ""
            descriptor1 = show_descriptor(transaction)
            first6 = transaction.first6.present? ? "#{transaction.first6}*****" : transaction.card.present? ? "#{transaction.card.first6}*****" : ""
            if first6.present?
              last4 = transaction.last4.present? ? "#{transaction.last4}" : transaction.card.present? ? "#{transaction.card.last4}" : ""
            else
              last4 = transaction.last4.present? ? "*****#{transaction.last4}" : transaction.card.present? ? "*****#{transaction.card.last4}" : ""
            end
            card_number = "#{first6}#{last4}"
            csv << [
                transaction.status == "pending" ? transaction.seq_transaction_id : transaction.seq_transaction_id.present? ? "#{transaction.seq_transaction_id}" : "#{transaction.id}",
                transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                transaction.from.present? && transaction.from == 'Credit'? "Credit" : transaction.from.present? && transaction.from == 'Debit'? "Debit" : transaction.sender.try(:name).present? ? transaction.sender.try(:name) : transaction.from.present? && transaction.from.gsub(/[^0-9]/, '').present?  ? Wallet.find_by(id: transaction.from).try(:name) : '',
                transaction.main_type == "Sale Issue"? Wallet.find_by(id: user.try(:wallet_id)).try(:name) : transaction.tags.try(:[],"send_check_user_info").try(:[],"name").present? ? transaction.tags["send_check_user_info"].try(:[],"name") : transaction.to.present? ? Wallet.find_by(id:transaction.to).try(:name) : transaction.receiver.present? ? transaction.receiver.try(:name) : '',
                main_type_format(transaction.main_type),
                transaction.payment_gateway.present? ? transaction.payment_gateway.try(:name): "#{descriptor1}",
                card_number,
                "#{symbol}" + number_to_currency(number_with_precision(total_amount), :precision => 2, delimiter: ','),
            ]
          end
        end
      end
    end
    file = StringIO.new(transactions_csv)
    qc_file = QcFile.new(name: "funding_transactions_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
    qc_file.image = file
    qc_file.image.instance_write(:content_type, 'text/csv')
    qc_file.image.instance_write(:file_name, "funding_transactions_export.csv")
    qc_file.save
    return {file_url: qc_file.image.url, qc_file_id: qc_file.id}
  end


  def show_local_type_for_worker(obj)
    if obj.main_type == TypesEnumLib::TransactionType::SaleType
      if obj.sub_type == TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtualApi
        return "eCommerce"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleVirtual
        return "Virtual Terminal"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::DebitCharge
        return obj.sub_type.humanize.titleize
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssueApi
        return "Sale Issue"
      elsif obj.sub_type == TypesEnumLib::TransactionSubType::SaleIssue
        return "Sale Issue"
      end
    else
      obj.main_type.try(:humanize).try(:titleize) || obj.sub_type.try(:humanize).try(:titleize)
    end
  end

  def show_other_descriptor_for_worker(obj)
    if obj.main_type == TypesEnumLib::TransactionViewTypes::SendCheck || obj.main_type == TypesEnumLib::TransactionViewTypes::BulkCheck
      "Checkbook Io"
    elsif obj.gateway.present?
      obj.gateway
    else
      "Quickcard"
    end
  end

  def ach_check_number(checkId)
    return checkId.last 6 if checkId.present?
  end

  def deduct_fee(fee, amount)
    deduct = amount.to_f * (fee.to_f/100)
    return number_with_precision(deduct, precision: 2).to_f
  end

  def check_card_blocked(card_number,exp_date,card_info)
    if card_info.present?
      card1 = Payment::QcCard.new(card_info,nil,1)
      card = Card.blocked_cards_with_fingerprint(card1.fingerprint,exp_date).first
      return card.present? ? true : false
    elsif card_number.present? && exp_date.present?
      card = Card.blocked_cards_with_number(card_number.first(6),card_number.last(4)).first
      return card.present? ? true : false
    else
      return false
    end
  end

  def gateway_type
    if params[:gateway_type].present?
      return params[:gateway_type] == "push_to_card" ? 'Push to Card' : params[:gateway_type] == "check" ? 'Check' : 'ACH'
    end
    if params[:type].present?
      return params[:type] == "push_to_card" ? 'Push to Card' : params[:type] == "check" ? 'Check' : 'ACH'
    end
  end

  def save_user_documentation(user, documentation_data)
    result = {}
    if user.present?
      if documentation_data.present?
        params[:documentation] = documentation_data
        params[:documentation].clone.each do |key, value|
          if value.count > 1
            value.each do |v|
              params[:documentation][:document] = v
              params[:documentation][:type] = Documentation.types[key]
              params[:documentation][:document].read.force_encoding(Encoding::UTF_8) if params[:documentation][:document].present?
              @documentation = Documentation.new(:document => params[:documentation][:document], :image_type => Documentation.types[key], :imageable_id => user.id, :imageable_type => "User")
              if @documentation.save
                result ={success: true }
              else
                result = {success: false, message: @documentation.errors.full_messages.first}
              end
            end
          else
            params[:documentation][:document] = value.first
            params[:documentation][:type] = Documentation.types[key]
            params[:documentation][:document].read.force_encoding(Encoding::UTF_8) if params[:documentation][:document].present?
            @documentation = Documentation.new(:document => params[:documentation][:document], :image_type => Documentation.types[key], :imageable_id => user.id, :imageable_type => "User")
            if @documentation.save
              result ={success: true }
            else
              result = {success: false, message: @documentation.errors.full_messages.first}
            end
          end
        end
      end
    end
    return result
  end

  def main_type_format(main_type)
    if main_type.present?
      if (main_type == "instant_pay" || main_type.downcase == "instant pay")
        return "Push to Card"
      elsif (main_type == "send_check" || main_type.downcase == "send Check")
        return "Send Check"
      elsif (main_type.downcase == "void_ach" || main_type.downcase == "void ach")
        return "Void ACH"
      elsif (main_type.downcase == "ach")
        return "ACH"
      elsif (main_type.downcase == "afp")
        return "AFP"
      elsif (main_type.downcase == "cbk_won" || main_type.downcase == "cbk won")
        return "CBK Won"
      elsif (main_type.downcase == "cbk_lost" || main_type.downcase == "cbk lost")
        return "CBK Lost"
      elsif (main_type.downcase == "cbk_fee" || main_type.downcase == "cbk fee")
        return "CBK Fee"
      elsif (main_type.downcase == "cbk_hold" || main_type.downcase == "cbk hold")
        return "CBK Hold"
      elsif (main_type.downcase == "Cbk Lost Retire" || main_type.downcase == "cbk lost retire")
        return "CBK Lost Retire"
      elsif (main_type.downcase == "CBK Dispute Accepted" || main_type.downcase == "cbk dispute accepted")
        return "CBK Dispute Accepted"
      elsif main_type.downcase == "ACH Deposit" || main_type.downcase == "Ach Deposit"
        return "ACH Deposit"
      elsif (main_type.downcase == "b2b transfer" || main_type.downcase == "b2b_transfer")
        return "B2B Transfer"
      elsif main_type.downcase == "qr credit card"
        return "QR Credit Card"
      elsif main_type.downcase == "qr debit card"
        return "QR Debit Card"
      elsif main_type == "QCP Secure"
        return "QCP Secure"
      elsif main_type == "QC Secure"
        return "QC Secure"
      elsif main_type == "Sale Issue"
        return "Sale Issue"
      elsif main_type == "Ecommerce" || main_type == "eCommerce"
        return "eCommerce"
      elsif main_type == "Service Fee"
        return "Monthly Fee"
      elsif main_type.downcase == "b2b fee"
        return "B2B Fee"
      elsif main_type.downcase == "failed ach"
        return "Failed ACH"
      elsif main_type.downcase == "failed ach fee"
        return "Failed ACH Fee"
      elsif main_type == "Invoice - QC"
        return "Invoice - QC"
      elsif main_type == "Invoice - Credit Card"
        return "Invoice - Credit Card"
      elsif main_type == "Invoice   Debit Card" || main_type == "Invoice - Debit Card"
        return "Invoice - Debit Card"
      elsif main_type.try(:downcase) == "rtp"
        return "RTP"
      elsif main_type.try(:downcase) == "invoice - rtp" || main_type.try(:downcase) == "invoice   rtp"
        return "Invoice - RTP"
      elsif main_type.downcase == "virtual_terminal_sale"
        return "Virtual Terminal"
      else
        return main_type.try(:titleize)
      end
    else
      ""
    end
  end

  def get_total_fee(check_fee,fee_perc,actual_amount,tango_order = nil)
    if tango_order.present?
      number_with_precision(check_fee.to_f + fee_perc.to_f, precision: 2, delimiter: ',')
    else
      number_with_precision(check_fee.to_f + (fee_perc.to_f*actual_amount.to_f/100), precision: 2, delimiter: ',')
    end
  end

  def total_amount(actual_amount, total_fee) #because of total amount in database are higher than real total ammount in float case
    number_with_precision(actual_amount.to_f + total_fee.to_f, precision: 2, delimiter: ',')
  end

  def reset_password_errors(errors)
    if errors[:current_password].present?
      if errors[:current_password][0] == "is invalid"
        errors[:current_password][0] = "Invalid Password"
      end
    end

    if errors[:password].present?
      if errors[:password][0] == "is invalid"
        errors[:password][0] = ""
      end
    end

    if errors[:password_confirmation].present?
      if errors[:password_confirmation][0] == "is invalid"
        errors[:password_confirmation][0] = ""
      end
    end
    return errors
  end

  def get_amount_to_percentage(amount,percentage)
    if percentage.present?
      (percentage/100 * amount)
    else
      0
    end
  end
  def get_tango_order_for_block_transaction(id)
    txn = Transaction.find_by(seq_transaction_id: id)
    if txn.present?
      tango_order = txn.tango_order
      if tango_order.present?
        tango = {id: tango_order.id, amount: tango_order.actual_amount, name: tango_order.name, description: tango_order.description , recipient: tango_order.recipient }
      else
        tango = {id: nil, amount: txn.amount, name: nil, description: nil , recipient: nil }
      end
    end
  end
  def block_txn_parent_transaction_info(id)
    txn = Transaction.find_by(seq_transaction_id: id)
    txn_detail = { amount: txn.amount, fee: txn.fee, total_amount: txn.total_amount, net_amount: txn.net_amount, t: txn} if txn.present?
    # fee_detail = {gbox_fee: txn.gbox_fee, iso_fee: txn.iso_fee, agent_fee: txn.agent_fee, affiliate_fee: txn.affiliate_fee } if txn.present?
  end

  def round_5(n)
    return n if n % 5 == 0
    rounded = n.round(-1)
    rounded > n ? rounded : rounded + 5
  end

  def javascript_exists?(script)
    script = "#{Rails.root}/app/assets/javascripts/#{script}.js"
    extensions = %w(.coffee .erb .coffee.erb) + [""]
    extensions.inject(false) do |truth, extension|
      truth || File.exists?("#{script}#{extension}")
    end
  end

  def transaction_main_types
    if request.present?
      if request.controller_class.name.split("::").first.include? "Admins"
        if params[:trans] == "refund"
          @types = [{value:"refund",view:"Refund"},{value:"refund_bank",view:"Refund Bank"}]
        elsif params[:trans] == "checks"
          @types = [{value:"Send Check",view:"Send Check"},{value:"Void Check",view:"Void Check"},{value:"Bulk Check",view:"Bulk Check"}]
        elsif params[:trans] == "ach"
          @types = [{value:"ACH_deposit",view:"ACH Deposit"},{value:"ACH",view:"ACH"},{value:"Void ACH",view:"Void ACH"}]
        elsif params[:trans] == "push_to_card"
          @types = [{value:"void_push_to_card",view:"Void P2c"}]
        elsif params[:trans] == "decline"
          @types = [{value:"Credit Card",view:"Credit Card"},{value:"eCommerce",view:"eCommerce"},{value:"Sale Issue",view:"Sale Issue"},{value:"Virtual Terminal",view:"Virtual Terminal"}]
        else
          @types = [{value:"ACH",view:"ACH"},{value:"ACH Deposit",view:"ACH Deposit"},{value:"account_transfer",view:"Account Transfer"},{value: "Buy rate fee", view: TypesEnumLib::TransactionType::BuyRateFee},{value: "cbk_dispute_accepted", view: "CBK Dispute Accepted"},{value: "cbk_fee", view: "CBK Fee"},{value: "cbk_hold", view: "CBK Hold"},{value: "cbk_lost", view: "CBK Lost"},{value: "CBK Lost Retire", view: "CBK Lost Retire"},{value: "cbk_won", view: "CBK Won"},{value:"Credit Card",view:"Credit Card"},{value:"eCommerce",view:"eCommerce"},{value:"Giftcard Purchase",view:"Giftcard Purchase"},{value: "Misc Fee", view: "Misc Fee"},{value:"PIN Debit",view:"PIN Debit"},{value:"instant_pay",view:"Push To Card"},{value:"payed_tip",view:"Paid Tip"},{value:"QR_Scan",view:"QR Code"},{value:"refund",view:"Refund"},{value:"Reserve_Money_Return",view:"Reserve Money Return"},{value: "retrievel_fee", view: "Retrieval Fee"},{value:"sale_giftcard",view:"Sale Giftcard"},{value:"Sale Issue",view:"Sale Issue"},{value:"Sales tip",view:"Sale Tip"},{value:"Send Check",view:"Send Check"},{value: "Service Fee", view: "Service Fee"},{value: "Subscription Fee", view: "Subscription Fee"},{value:"Virtual Terminal",view:"Virtual Terminal"},{value:"Void ACH",view:"Void ACH"},{value:"Void_Check",view:"Void Check"},{value:"void_push_to_card",view:"Void P2C"},{value:"Qr Credit Card",view:"QR Credit Card"},{value:"Qr Debit Card",view:"QR Debit Card"}]
        end
      elsif request.controller_class.name.split("::").first.include? "Merchant"
        @types = [{value:"ACH",view:"ACH"},{value:"account_transfer",view:"Account Transfer"},{value: "cbk_dispute_accepted", view: "CBK Dispute Accepted"},{value: "cbk_fee", view: "CBK Fee"},{value: "cbk_hold", view: "CBK Hold"},{value: "cbk_lost", view: "CBK Lost"},{value: "cbk_won", view: "CBK Won"},{value:"credit_card",view:"Credit Card"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"giftcard_purchase",view:"Giftcard Purchase"},{value: "Misc Fee", view: "Misc Fee"},{value: "Service Fee", view: "Monthly Fee"},{value:"debit_charge",view:"PIN Debit"},{value:"instant_pay",view:"Push to Card"},{value:"refund",view:"Refund"},{value:"Reserve_Money_Return",view:"Reserve Money Return"},{value: "retrievel_fee", view: "Retrieval Fee"},{value:"Send Check",view:"Send Check"},{value: "Subscription Fee", view: "Subscription Fee"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"Void ACH",view:"Void ACH"},{value:"Void Check",view:"Void Check"},{value:"Void Push To Card",view:"Void P2C"},{value:"Failed Check",view:"Failed Check"},{value:"Failed ACH",view:"Failed ACH"},{value:"Failed Push To Card",view:"Failed Push To Card"}]
        @decline_types = [{value:"Sale Issue",view:"Sale Issue"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"credit_card",view:"Credit Card"}]
        @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
      end
    end
  end
  def get_transaction_amount_from_main_transaction(id)
    txn =  Transaction.find_by(seq_transaction_id: id)
    amount = txn.present? ? txn.total_amount : 0.0
  end

  def record_it(type = nil, reason = nil, request = nil, params = nil, user_id = nil, action = nil, controller=nil)
    clean_parameters(params)
    activity = ActivityLog.new
    activity.url = request.try(:url)
    activity.host = request.try(:host)
    activity.browser = request.env["HTTP_USER_AGENT"] if request.present?
    activity.ip_address = request.remote_ip if request.try(:remote_ip).present?
    activity.controller = controller || controller_name
    activity.action = action || action_name
    activity.respond_with = reason
    activity.activity_type = type unless type.nil?
    if params.try(:[],:user_image).present?
      if params[:user_image].original_filename.present?
        params[:user_image] = params[:user_image].original_filename
      else
        params[:user_image] = 'unknown image format'
      end
    end
    activity.transaction_id= params.try(:[],:transaction_id)
    activity.params = params.except(:card_cvv) if params.present?
    activity.user_id = user_id
    activity.save
  end


  def get_user_type_for_ids(id)
    user = User.find id
    if user.merchant?
      return "M-#{id}"
    elsif user.iso?
      return "ISO-#{id}"
    elsif user.agent?
      return "A-#{id}"
    elsif user.affiliate?
      return "AF-#{id}"
    else
      return id
    end
  end

  def get_user_type(id)
    user = User.find id
    if user.merchant?
      return "merchant"
    elsif user.iso?
      return "iso"
    elsif user.agent?
      return "agent"
    elsif user.affiliate?
      return "affiliate"
    else
      return " "
    end
  end

  def save_activity(merchant,date,admin_id,note,location)
    activity_type='user_activity'
    action_type=nil
    if note.present? && (note=='Archived' || note=='Un-Archived' || note=='Inactivated' || note=='Activated')
      activity={user_id:merchant.try(:id),date:date,admin_id:admin_id}
      note = merchant.iso? ? "ISO is #{note}"  : "#{merchant.try(:role)} is #{note}"
      user_id=merchant.try(:id)
      action_type='affiliate_program' if merchant.admin_user? || merchant.affiliate_program?
    elsif note.present? && note=='system_fee'
      note={}
      activity={user_id:merchant.try(:id),date:date,admin_id:admin_id}
      # iso_fee=merchant.fees.try(:first).audits.where(created_at: (Time.now-50)..Time.now).or(merchant.fees.try(:last).audits.where(created_at: (Time.now-50)..Time.now)) if merchant.try(:fees).try(:first).present? && merchant.try(:fees).try(:last).present?
      iso_fee=merchant.audits.where(created_at: (Time.now-50)..Time.now).or(merchant.audits.where(created_at: (Time.now-50)..Time.now)) if merchant.try(:fees).try(:first).present? && merchant.try(:fees).try(:last).present?
      iso_fee=merchant.audits.where(created_at: (Time.now-50)..Time.now) if merchant.admin_user?
      action_type='affiliate_program' if merchant.admin_user? || merchant.affiliate_program?
      if iso_fee.present?
        note.merge!({'fee'=>iso_fee.first.audited_changes}) if iso_fee.try(:first).present?
        note.merge!({'fee'=>iso_fee.last.audited_changes}) if iso_fee.try(:last).present?
        note=note.values.flatten if note.present?
        note=note.first if note.present?
        b={}
        note=note.values.flatten
        if note.present?
          if note.first=='{}'
            note[0]=note.last
          end
          note.first.each do |k,v|
            if note[0].try(:[],"#{k}").present? || note[1].try(:[],"#{k}").present?
              b.merge!("#{k}"=>[note[0].try(:[],"#{k}"),note[1].try(:[],"#{k}")]) unless note[0].try(:[],"#{k}")==note[1].try(:[],"#{k}")
            end
          end
        end
        note=b
        note=note.to_json if note.present?
        user_id=merchant.try(:id)
        activity_type='user_fee'
      end
    elsif note.present? && note=='buy_rate'
      note={}
      activity={user_id:merchant.try(:id),date:date,admin_id:admin_id}
      buyrate=merchant.fees.try(:first).audits.where(created_at: (Time.now-50)..Time.now).or(merchant.fees.try(:last).audits.where(created_at: (Time.now-50)..Time.now)) if merchant.try(:fees).try(:first).present? && merchant.try(:fees).try(:last).present?
      note={}
      if buyrate.present?
        note.merge!({'buyrate'=>buyrate.first.audited_changes})
      end
      note=note.values.flatten if note.present?
      note=note.to_json if note.present?
      user_id=merchant.try(:id)
      # activity_type='user_activity'
    elsif note.present? && note=='iso_buyrate'
      note={}
      activity={user_id:merchant.try(:id),date:date,admin_id:admin_id}
      buyrate=merchant.fees.try(:first).audits.where(created_at: (Time.now-50)..Time.now).or(merchant.fees.try(:last).audits.where(created_at: (Time.now-50)..Time.now)) if merchant.try(:fees).try(:first).present? && merchant.try(:fees).try(:last).present?
      note={}
      if buyrate.present? && buyrate.count > 1
        if buyrate.try(:first).present?
          note.merge!({'buyrate1'=>buyrate.first.audited_changes})
        end
        if buyrate.try(:last).present?
          note.merge!({'buyrate'=>buyrate.last.audited_changes})
        end
        note=note.values.flatten if note.present?
        note=note.to_json if note.present?
        user_id=merchant.try(:id)
      else
        note.merge!({'buyrate'=>buyrate.first.audited_changes}) if buyrate.present?
        note=note.values.flatten if note.present?
        note=note.to_json if note.present?
        user_id=merchant.try(:id)
      end
    elsif note.present? && note=='user'
      activity={user_id:merchant.try(:id),date:date}
      note={}
      audit=location #here in location variable audit is coming
      if audit.present?
        note.merge!({'user'=>audit.first.audited_changes})
      end
      note=note.values.flatten if note.present?
      note=note.to_json if note.present?
      user_id=merchant.try(:id)
    elsif note.present? && note=='location_users'
      activity={user_id:merchant,date:date}
      loc_users=location # here location users ids are in location param
      new_users=loc_users.try(:first)
      old_users=loc_users.try(:last)
      note={}
      if loc_users.present?
        second_user=first_user=last_user={}
        first_user={'iso'=>[old_users.try(:first).to_i,new_users.try(:first).to_i]} unless new_users.try(:first)==old_users.try(:first)
        second_user={'agent'=>[old_users.try(:second).to_i,new_users.try(:second).to_i]} unless new_users.try(:second)==old_users.try(:second)
        last_user={'affiliate'=>[old_users.try(:third).to_i,new_users.try(:third).to_i]} unless new_users.try(:third)==old_users.try(:third)
        note.merge!({'iso_id'=>first_user}) if first_user.present?
        note.merge!({'agent_id'=>second_user}) if second_user.present?
        note.merge!({'affiliate_id'=>last_user}) if last_user.present?
      end
      note=note.values.flatten if note.present?
      note=note.to_json if note.present?
      user_id=merchant
    else
      # activity_type='user_activity'
      activity={user_id:merchant,date:date,admin_id:admin_id}
      note={}
      location_audit=location.audits.where(created_at: (Time.now-50)..Time.now) if location.try(:audits).present?
      fee_audit=location.try(:fees).try(:first).audits.where(created_at: (Time.now-50)..Time.now) if location.try(:fees).try(:first).present?
      if location_audit.present?
        note.merge!({'location'=>location_audit.first.audited_changes})
      end
      if fee_audit.present?
        note.merge!({'fee'=>fee_audit.first.audited_changes})
      end
      note=note.values.flatten if note.present?
      note=note.to_json if note.present?
      user_id=merchant
    end
    ip_address = request.remote_ip.present? ? request.remote_ip : request.env['REMOTE_ADDR']
    ActivityLog.create(params:activity,activity_type:activity_type,note:note,user_id:user_id,host:request.env["SERVER_NAME"],url: request.url,browser: request.env['HTTP_USER_AGENT'],ip_address: ip_address, action:action_name,action_type:action_type ) if note.present?
  end

  def get_flatten_hash_for_logs(hash)
    hash.each_with_object({}) do |(k, v), h|
      if v.is_a? Hash
        get_flatten_hash_for_logs(v).map do |h_k, h_v|
          h["#{k}.#{h_k}".to_sym] = h_v
        end
      else
        h[k] = v
      end
    end
  end
  def parse_daterange_transaction(date)
    date = date.split(' - ')
    first_date = date.first.split('/')
    second_date = date.second ? date.second.split('/') : date.first.split('/')
    first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
    second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
    return [first_date, second_date]
  end
  def parse_daterange_activity(date)
    date = date.split(' / ')
    first_date = date.first.split('-')
    second_date = date.second ? date.second.split('-') : date.first.split('-')
    first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
    second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
    return [first_date, second_date]
  end

  def searching_iso_local_transaction_for_batch_export(params, wallets,first_date=nil,second_date=nil,user=nil,offset=nil,from_worker=nil,hostname=nil,email=nil,trans=nil)
    conditions = []
    parameters = []
    if params[:name].present?
      conditions << "(lower(merchant_name) LIKE ?)"
      parameters << "%#{params[:name].try(:downcase)}%"
    end
    if params[:Receiver_name].present?
      params[:Receiver_name]=params[:Receiver_name].strip
      conditions <<  "(lower(receiver_name) LIKE ?)"
      parameters << "%#{params[:Receiver_name].try(:downcase)}%"
    end
    if params[:sender_name].present?
      params[:sender_name]=params[:sender_name].strip
      conditions <<  "(lower(sender_name) LIKE ?)"
      parameters << "%#{params[:sender_name].try(:downcase)}%"
    end
    dba_name = ''
    if params[:DBA_name].present?
      if params[:DBA_name].kind_of?(Array)
        dba_name = params[:DBA_name]
      else
        dba_name = JSON.parse(params[:DBA_name])
      end
    end
    if dba_name.present?
      conditions<< "(location_dba_name IN (?) OR tags->'location'->>'business_name' IN (?))"
      parameters<< dba_name
      parameters << dba_name
    end

    if params[:dba_name].present?
      params[:dba_name] = params[:dba_name].strip
      conditions << "(location_dba_name ILIKE (?) OR tags->'location'->>'business_name' ILIKE (?))"
      parameters << "%#{params[:dba_name]}%"
      parameters << "%#{params[:dba_name]}%"
    end

    if params[:amount].present?
      if @user.try(:merchant?)
        conditions << "(amount = ?)"
        parameters << params[:amount]
      else
        new_var=number_with_precision(params[:amount], precision: 2)
        var=new_var.to_s.split('.').last
        storing_param=new_var.to_f
        if var=="00"
          storing_param=new_var
          storing_param=number_with_precision(storing_param, precision: 1).to_f
        end
        if trans == "commission"
          conditions << "(block_transactions.amount_in_cents = ? OR tags->'main_transaction_info'->>'amount' = ?)"
          parameters << params[:amount].to_f
          parameters << "#{storing_param}"
        else
          conditions << "(block_transactions.main_amount = ? OR tags->'main_transaction_info'->>'amount' = ?)"
          parameters << params[:amount].to_f
          parameters << "#{storing_param}"
        end
      end
    end
    if params[:offset].present?
      unless params[:offset].include?(':')
        params[:offset].insert(3,':')
      end
    end
    time = parse_time(params)
    date = parse_daterange(params[:date]) if params[:date].present?
    date = parse_daterange(params[:export_date]) if date.blank? && params[:export_date].present?
    date = parse_daterange(params[:datie]) if params[:datie].present? && date.blank?
    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
      }
    elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
      date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    elsif date.blank? && params[:batch_date].present?
      date = "#{params[:batch_date].to_date.strftime("%m/%d/%Y")} - #{params[:batch_date].to_date.strftime("%m/%d/%Y")}"
      date = parse_date(date)
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    end
    if date.present? && date.try(:[],:first_date).present?
      conditions << "(block_transactions.timestamp >= ?)"
      parameters << date[:first_date]
    end
    if date.present? && date.try(:[],:second_date).present?
      conditions << "(block_transactions.timestamp <= ?)"
      parameters << date[:second_date]
    end
    # If Time and Date is Selected
    if (params[:time1].present? || params[:time2].present?) && params[:date].present?
      time_first = time_conversion(time.first + params[:offset]) if time.first.present?
      time_second = time_conversion(time.second + params[:offset]) if time.second.present?
      if time_first > time_second
        conditions << "(block_transactions.created_at::TIME >= ? AND block_transactions.created_at::TIME <= ? OR block_transactions.created_at::TIME >= ? AND block_transactions.created_at::TIME <= ?)"
        parameters << time_first
        parameters << "23:59:59"
        parameters << "00:00:00"
        parameters << time_second
      else
        if time_first.present?
          conditions << "(block_transactions.created_at::TIME >= ?)"
          parameters << time_first
        end
        if time_second.present?
          conditions << "(block_transactions.created_at::TIME <= ?)"
          parameters << time_second
        end
      end
    end
    type = ''
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if params[:type1].present?
      if params[:type1].kind_of?(Array)
        type1 = params[:type1]
        if type1.include?(I18n.t('search_types.debit_charger.value'))
          type1 << I18n.t('search_types.debit_card_.value')
        end
      else
        type1 = JSON.parse(params[:type1])
      end
    end
    if type.present?
        if type.include?"Void Ach"
          type << "Void ach"
        end
        conditions << "(main_type IN(?) OR tags ->> 'sub_type' IN(?) OR sub_type IN (?))"
        parameters << type
        parameters << type
        parameters << type
    end
    if type1.present?
      conditions << "(tags->'main_transaction_info'->>'transaction_type' IN(?) OR tags->'main_transaction_info'->>'transaction_sub_type' IN(?) OR tx_type IN (?) OR tx_sub_type IN (?))"
      parameters << type1
      parameters << type1
      parameters << type1
      parameters << type1
    end
    name = ''
    if params[:wallet_ids].present?
      if params[:wallet_ids].kind_of?(Array)
        name = params[:wallet_ids]
      else
        name = JSON.parse(params[:wallet_ids])
      end
    end
    if params[:wallets_id].present?
      if params[:wallets_id].kind_of?(Array)
        name = params[:wallets_id]
      else
        name = JSON.parse(params[:wallets_id])
      end
    end
    if name.present?
      conditions<< "(tags->'merchant'->>'id' IN (?)) "
      parameters<< name
    end
    if params[:block_id].present?
      conditions << "(sequence_id LIKE ? OR block_transactions.quickcard_id::VARCHAR LIKE ?)"
      parameters << "#{params[:block_id].split.join}%"
      parameters << "#{params[:block_id].split.join}%"
    end
    if params[:first6].present?
      conditions << "(first6 ILIKE ? OR tags->'card'->>'first6' ILIKE ?)"
      parameters << "#{params[:first6].to_s}%"
      parameters << "#{params[:first6].to_s}%"
    end
    if params[:last4].present?
      conditions << "(last4 ILIKE ? OR tags->'card'->>'last4' ILIKE ?)"
      parameters << "%#{params[:last4].to_s}"
      parameters << "%#{params[:last4].to_s}"
    end
    if params[:sender_email].present?
      conditions << "(users.email ILIKE ?)"
      parameters << params[:sender_email]
    end
    if params[:sender_phone_number].present?
      conditions << "(users.phone_number ILIKE ?)"
      parameters << params[:sender_phone_number]
    end
    conditions = [conditions.join(" AND "), *parameters]
    if params[:Receiver_name].present?
      t = BlockTransaction.joins(:receiver).where(conditions).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check"])
    elsif params[:sender_email].present? || params[:sender_phone_number].present?
      t = BlockTransaction.joins(:sender).where(conditions).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check"])
    else
      t = BlockTransaction.where(conditions).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check"])
    end
    return t
  end

  def get_transfer_amount(check, amount)
    if check.instant_ach?
      if params[:type2] == "IN_PROCESS"
        amount = nil
      elsif params[:type2] == "FAILED"
        amount = check.actual_amount
      end
    elsif check.instant_pay? && (params[:type2] == "FAILED" || params[:type2] == "VOID")
      amount = check.actual_amount
    end
    return amount
  end

  def get_source_account_id(tr_fee_type, type, source_id, destination_id, send_check_case)
    source_account_id = nil
    if (tr_fee_type.first == TypesEnumLib::TransactionType::ACH || type == "Failed Push To Card") && send_check_case.blank?
      source_account_id = source_id
    elsif type == TypesEnumLib::TransactionType::ACH && tr_fee_type.first == TypesEnumLib::TransactionType::FeeTransfer && send_check_case.blank?
      source_account_id = source_id
    else
      source_account_id = destination_id
    end
    return source_account_id
  end
  #Date split with dash ('-')
  def parse_daterange_dash(date)
    date = date.split(' - ')
    first_date = date.first.split('/')
    second_date = date.second ? date.second.split('/') : date.first.split('/')
    first_date = {:day => first_date[1], :month => first_date[0], :year => first_date[2]}
    second_date = {:day => second_date[1], :month => second_date[0], :year => second_date[2]}
    return [first_date, second_date]
  end
end
