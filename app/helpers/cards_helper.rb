module CardsHelper

  def get_card_brand(obj)
    if obj[:card].present?
      obj[:card]["brand"].capitalize
    elsif obj["previous_issue"].present?
      if obj["previous_issue"]["tags"].present?
        if obj["previous_issue"]["tags"]["card"].present?
          obj["previous_issue"]["tags"]["card"]["brand"].capitalize
        end
      end
    end
  end
end