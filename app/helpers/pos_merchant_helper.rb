module PosMerchantHelper

  def save_gbox_fee(tags, total_fee, tx_type = nil)
    #function for getting gbox fee & total fee to save in transaction
    if tags.present?
      if tags["iso"].present? && tags["iso"]["iso_buyrate"].present?
        if tx_type == "refund"
          return tags["gbox"]["amount"].to_f
        else
          return tags["iso"]["amount"].to_f + tags["gbox"]["amount"].to_f
        end
      else
        return calculate_fees(total_fee,tags)
      end
    elsif total_fee.to_f > 0
      return total_fee.to_f
    else
      nil
    end
  end

  def save_total_fee(tags, total_fee, tx_type = nil)
    #function for getting total fee to save in transaction
    if tags.present?
      if tags["iso"].present? && tags["iso"]["iso_buyrate"].present?
        if tx_type == "refund"
          return tags["gbox"]["amount"].to_f
        else
          return tags["iso"]["amount"].to_f + tags["gbox"]["amount"].to_f
        end
      else
        return total_fee.to_f
      end
    elsif total_fee.to_f > 0
      return total_fee.to_f
    else
      nil
    end
  end

  def save_net_fee(total_fee,privacy_fee,main_type)
    if main_type.present? && main_type=='Qr Debit Card'
      net_fee=total_fee.to_f
    else
      net_fee=total_fee.to_f - privacy_fee.to_f
    end
    return net_fee
  end

  def save_net_amount(total_amount,total_fee,reserve_amount,tip,privacy_fee,main_type)
    if main_type.present? && main_type=='Qr Debit Card'
      net_amount= total_amount.to_f - total_fee.to_f - reserve_amount.to_f - tip.to_f - privacy_fee.to_f
    else
     net_amount= total_amount.to_f - total_fee.to_f - reserve_amount.to_f - tip.to_f
    end
    return net_amount
  end

  def save_iso_fee(tags)
    #function for getting iso fee to save in transaction
    if tags.present?
      if tags["iso"].present? && tags["iso"]["iso_buyrate"].present?
        return "0.00"
      else
        return tags["iso"]["amount"].to_f
      end
    else
      "0.00"
    end
  end

end
