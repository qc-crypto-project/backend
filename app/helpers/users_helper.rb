module UsersHelper
  def deduct_sendcheck_fee(user,amount)

    if (user.system_fee.present? && user.system_fee != "{}") || user.affiliate_program?
      if user.affiliate_program? 
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee
      end
      check_fee = 0
      check_fee = fee_object["send_check"].try(:to_f) if fee_object.present?
      fee_perc = deduct_fee(fee_object["redeem_fee"].try(:to_f), amount) if fee_object.present?
      fee = check_fee.to_f + fee_perc.to_f
      amount_sum = amount.to_f + check_fee.to_f + fee_perc.to_f
      [amount_sum,fee]
    else
      [amount,0]
    end
  end

  def deduct_giftcard_fee(user,amount)
    if ( user.system_fee.present? && user.system_fee != "{}" ) || user.affiliate_program?
      if user.affiliate_program? 
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee
      end
      check_fee = fee_object["giftcard_fee"].try(:to_f) if fee_object.present?
      fee = check_fee.to_f
      amount = amount + check_fee.to_f
      [amount,fee]
    else
      [amount,0]
    end
  end

  def deduct_add_money_fee(user, amount)
    if (user.system_fee.present? && user.system_fee != "{}") || user.affiliate_program?
      if user.affiliate_program? 
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee
      end
      check_fee = 0
      check_fee = fee_object["add_money_dollar"].try(:to_f) if fee_object.present?
      fee_perc = deduct_fee(fee_object["add_money_percent"].try(:to_f), amount.to_f) if fee_object.present?
      fee = check_fee.to_f + fee_perc.to_f
      amount_sum = amount.to_f + check_fee.to_f + fee_perc.to_f
      [amount_sum,fee]
    else
      [amount,0]
    end
  end

  def deduct_b2b_fee(user, amount)
    if user.system_fee.present? && user.system_fee != "{}"
      fee_object = user.system_fee
      check_fee = 0
      check_fee = fee_object["b2b_dollar"].try(:to_f) if fee_object.present?
      fee_perc = deduct_fee(fee_object["b2b_percent"].try(:to_f), amount.to_f) if fee_object.present?
      fee = check_fee.to_f + fee_perc.to_f
      amount_sum = amount.to_f + check_fee.to_f + fee_perc.to_f
      [amount_sum,fee]
    else
      [amount,0]
    end
  end

  def deduct_p2_fee(user,amount)
    if ( user.system_fee.present? && user.system_fee != "{}") || user.affiliate_program?
      if user.affiliate_program? 
        admin_user = User.find_by_id(user.admin_user_id)
        fee_object = admin_user.try(:system_fee)
      else
        fee_object = user.system_fee
      end
      p2c_fee_dollar = fee_object["push_to_card_dollar"].try(:to_f) if fee_object.present?
      p2c_fee_perc = deduct_fee(fee_object["push_to_card_percent"].try(:to_f), amount) if fee_object.present?
      fee = p2c_fee_dollar.to_f + p2c_fee_perc.to_f
      amount_sum = amount.to_f + fee
      [amount_sum,fee]
    else
      [amount,0]
    end
  end
end
