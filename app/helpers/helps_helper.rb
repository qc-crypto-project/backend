module HelpsHelper
  def notes_plus(user_id, hash)
    @hash = eval(hash)
    @user = User.find(user_id)
    @denominations = eval(@user.denominations)
    @denominations[1] = @denominations[1] + (@hash[1].nil? ? 0 : @hash[1])
    @denominations[2] = @denominations[2] + (@hash[2].nil? ? 0 : @hash[2])
    @denominations[5] = @denominations[5] + (@hash[5].nil? ? 0 : @hash[5])
    @denominations[10] = @denominations[10] + (@hash[10].nil? ? 0 : @hash[10])
    @denominations[20] = @denominations[20] + (@hash[20].nil? ? 0 : @hash[20])
    @denominations[50] = @denominations[50] + (@hash[50].nil? ? 0 : @hash[50])
    @denominations[100] = @denominations[100] + (@hash[100].nil? ? 0 : @hash[100])
    @user.update!(denominations: @denominations)
  end

  def notes_minus(user_id, hash)
    @hash = eval(hash)
    @user = User.find(user_id)
    @denominations = eval(@user.denominations)
    @denominations[1] = @denominations[1] - (@hash[1].nil? ? 0 : @hash[1])
    @denominations[2] = @denominations[2] - (@hash[2].nil? ? 0 : @hash[2])
    @denominations[5] = @denominations[5] - (@hash[5].nil? ? 0 : @hash[5])
    @denominations[10] = @denominations[10] - (@hash[10].nil? ? 0 : @hash[10])
    @denominations[20] = @denominations[20] - (@hash[20].nil? ? 0 : @hash[20])
    @denominations[50] = @denominations[50] - (@hash[50].nil? ? 0 : @hash[50])
    @denominations[100] = @denominations[100] - (@hash[100].nil? ? 0 : @hash[100])
    @user.update(denominations: @denominations)
  end
end
