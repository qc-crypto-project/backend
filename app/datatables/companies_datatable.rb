class CompaniesDatatable
  ApplicationHelper
  delegate :params, :h,:span,:content_tag, :link_to, :span,:edit_admins_company_path,:admins_company_path , :current_user,:content_tag,:options_for_select,:update_valid_giftcards_path,:url_for,:select_tag,:cents_to_dollars,:image_tag, :number_to_currency, to: :@view

  require 'rqrcode'

  def initialize(view)
    @view = view
    # @companies = list
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Company.all.size,
      iTotalDisplayRecords: Company.all.size,
      aaData: data
    }
  end

private

  def data
    companies.map do |gc|
      [
          gc.id,
          gc.name,
          gc.category,
          gc.phone_number,
          [
          link_to("Edit", edit_admins_company_path(gc.slug),:remote => true  ,:class => "btn btn-sm btn-primary editModel", "data-toggle" => "modal" ),
          link_to("View", admins_company_path(gc.slug),:class => "btn btn-sm btn-info ")
        ]
      ]
    end
  end

  def companies
    @companies ||= fetch_companies
  end

  def sort_column
    columns = %w[id name category phone_number]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_companies
    companies = Company.all.order("#{sort_column} #{sort_direction}")
    companies = companies.page(page).per_page(per_page)
    if params[:search][:value].present?
      companies = companies.where("lower(name)  like lower(:search) or lower(category) like lower(:search) or phone_number like :search", search: "%#{params[:search][:value]}%")
    end
    companies
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end
