class IsosDatatable
  ApplicationHelper
  delegate :params, :h, :link_to, :span,:iso_path ,:edit_iso_path, :current_user,:content_tag,:options_for_select,:update_valid_giftcards_path,:url_for,:select_tag,:cents_to_dollars,:image_tag,:details_iso_path, :number_to_currency, to: :@view

  require 'rqrcode'

  def initialize(view)
    @view = view

  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: isos.count,
        iTotalDisplayRecords: isos.count,
        aaData: data
    }
  end

  private

  def data
    isos.map do |gc|
      [
          (gc.ref_no.nil? ? gc.id : gc.ref_no ),
          # gc.name = "#{gc.first_name} #{gc.last_name}",
          gc.name,
          gc.phone_number,
          [
              # link_to("Edit", edit_iso_path(gc),:remote => true  ,:class => "btn btn-sm btn-primary editModel", "data-toggle" => "modal" ),
              link_to("Edit", edit_iso_path(gc ,iso_id: gc),:class => "btn btn-sm btn-primary"),
              link_to("View", iso_path(gc,iso_id: gc.id),:class => "btn btn-sm btn-info"),
              link_to("Archive",gc, controller: "admins/users",:method => :delete, class: 'btn btn-sm btn-warning', :data => { :confirm => 'Are you sure you want to archive this iso?'}),
              link_to("Details", details_iso_path(gc.id,iso_id: gc.id)  ,:class => "btn btn-sm btn-default")
          ]
      ]
    end
  end

  def isos
    @isos ||= fetch_isos
  end

  def sort_column
    columns = %w[id name  phone_number]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_isos
    isos = User.iso.where(archived: false).order("#{sort_column} #{sort_direction}")
    isos = isos.page(page).per_page(per_page)
    if params[:search][:value].present?
      isos=isos.where("lower(name)  like lower(:search) or lower(last_name) like lower(:search)  or phone_number like :search", search: "%#{params[:search][:value]}%")
    end
    isos
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end