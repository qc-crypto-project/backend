class UsersReportDatatable
  delegate :params, :h, :link_to, :span, :current_user,:content_tag,:user_report_admins_audits_path,:image_tag, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: User.where.not(email: current_user.email).count,
        iTotalDisplayRecords: users.total_entries,
        aaData: data
    }
  end

  private

  def data
    users.map do |user|
      [
          user.id ,
          user.name,
          user.email,
          user.phone_number,
          (user.is_block == false ? content_tag('span','Active' , class: 'label label-success') : content_tag('span','Inactive' , class: 'label label-danger')),
          user.user_image.present? ? image_tag(user.user_image) : '' ,
          user.front_card_image.present? ? image_tag(user.front_card_image) : '',
          user.back_card_image.present? ? image_tag(user.back_card_image) : '',
          user.postal_address,
          [link_to('Reports', user_report_admins_audits_path(user), controller: "admins/audits", class: 'btn btn-sm btn-success')]
      #link_to('sss',user)
      ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def sort_column
    columns = %w[id name email phone_number is_block postal_address, front_card_image ]

    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_users
    users = User.where.not(email: current_user.email).order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    if params[:search][:value].present?
      users = users.where("phone_number like :search or lower(name) like lower(:search) or lower(email) like lower(:search)", search: "%#{params[:search][:value]}%")
    end
    users
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end