class MerchantsDatatable
  ApplicationHelper
  delegate :params, :h, :link_to, :span,:merchant_id_locations_path, :location_path,:edit_merchant_path, :current_user,:content_tag,:options_for_select,:update_valid_giftcards_path,:url_for,:select_tag,:cents_to_dollars,:image_tag, :number_to_currency, to: :@view



  require 'rqrcode'

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: merchants.count,
      iTotalDisplayRecords: merchants.count,
      aaData: data
    }
  end

private

  def data
    merchants.map do |gc|
      [
        (gc.ref_no.nil? ? gc.id : gc.ref_no ),
        gc.name,
        gc.phone_number,
      # if params[:id].present?
        [
          link_to("Edit", edit_merchant_path(gc.slug),:remote => true  ,:class => "btn btn-sm btn-primary editModel",data: { disable_with: "<i class='fa fa-refresh fa-spin'> </i> Loading..."}, "data-toggle" => "modal" ),
          link_to('Locations',merchant_id_locations_path(user_id: gc.slug),class: 'btn btn-sm btn-success')
        ]
      # {:controller => "admins/locations", :action => "index", :user => gc.id }
      # else
      #   [
      #     link_to("Edit", edit_merchant_path(gc.id),:remote => true  ,:class => "btn btn-sm btn-primary editModel", "data-toggle" => "modal" )
      #   ]
      # end
      ]
    end
  end

  def merchants
    @merchants ||= fetch_merchants
  end

  def sort_column
    columns = %w[id name  phone_number]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_merchants
    if params[:id]
      @company = Company.friendly.find params[:id]
      merchants = User.merchant.where(company_id: @company.id)
    else
      merchants = User.merchant.where(merchant_id: nil)
    end
    merchants = merchants.order("#{sort_column} #{sort_direction}")
    if params[:search][:value].present?
      merchants = merchants.where("lower(name)  like lower(:search)  or phone_number like :search", search: "%#{params[:search][:value]}%")
    end
    merchants = merchants.page(page).per_page(per_page)
    merchants
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end
