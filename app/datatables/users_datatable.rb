class UsersDatatable
  delegate :params, :h, :link_to, :span,:verification_admins_path,:edit_user_path, :current_user,:content_tag,:image_tag, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: users.count,
      iTotalDisplayRecords: users.count,
      aaData: data
    }
  end

private

  def data
    users.map do |user|
      [
        (user.ref_no.nil? ? user.id : user.ref_no ),
        user.name,
        user.email,
        user.role || 'No Role',
        user.phone_number,
        user.source,
        (user.is_block == false ? link_to("<i class='fa fa-check'> Approved</i>".html_safe,verification_admins_path(id: user.id) , remote: true ,:data => {:toggle => 'modal', :id => user.id},class: 'btn btn-success btn-sm verify_status') : link_to("<i class='fa fa-cog'> Disapproved</i>".html_safe,verification_admins_path(id: user.id) , remote: true ,:data => {:toggle => 'modal', :id => user.id},class: 'btn btn-danger btn-sm verify_status')),
        user.user_image.present? ? image_tag(user.user_image) : '' ,
        user.front_card_image.present? ? image_tag(user.front_card_image) : '',
        user.back_card_image.present? ? image_tag(user.back_card_image) : '',
        user.postal_address,
        [link_to('Wallets',user, class: 'btn btn-sm btn-success'),(user.archived == false ? (link_to 'Archive',user, controller: "admins/users",:method => :delete, class: 'btn btn-sm btn-warning', :data => { :confirm => 'Are you sure you want to archive this user?'}) : (link_to 'Unarchive',user, controller: "admins/users",:method => :delete, class: 'btn btn-sm btn-warning', :data => { :confirm => 'Are you sure you want to unarchive this user?'}))]
      ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def sort_column
    columns = %w[id name email phone_number is_block postal_address front_card_image ]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_users
    @key = nil
    if params[:query].present?
      if params[:query]["name"].present?
        @key = "name ILIKE :name"
      end
      if params[:query]["email"].present?
        if @key.nil?
          @key = "email ILIKE :email"
        else
          @key += " Or email ILIKE :email"
        end
      end
      if params[:query]["phone_number"].present?
        if @key.nil?
          @key = "phone_number ILIKE :phone_number"
        else
          @key += " Or phone_number ILIKE :phone_number"
        end
      end
      if params[:query]["ref_no"].present?
        if @key.nil?
          @key = "ref_no ILIKE :ref_no"
        else
          @key += " Or ref_no ILIKE :ref_no"
        end
      end
      if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present? || params[:query]["role"].present? || params[:query]["last4"].present? || params[:query]["wallet_id"].present?
        if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present? || params[:query]["ref_no"].present?
          @users = User.where(@key,  name: "%#{params[:query]["name"]}%", email: "%#{params[:query]["email"]}%",phone_number: "%#{params[:query]["phone_number"]}%",ref_no: "%#{params[:query]["ref_no"]}%").unarchived_users
        end
        if params[:query]["role"].present?
         # all = []
          b = User.where(role: params[:query]["role"].to_i,archived: false).order("#{sort_column} #{sort_direction}")
          if @users.present?
            @users = b.merge(@users)
          else
            @users = b
          end
         #@users =  @users.merge(b)
        end
        if params[:query]["wallet_id"].present?
          w = Wallet.find(params[:query]["wallet_id"]).users.unarchived_users.order("#{sort_column} #{sort_direction}")
          if @users.blank?
            @users = w
          end
        end

        if params[:query]["last4"].present?
          # @cards = Card.find_by_last4(params[:query]["last4"])
          @cards = Card.where(last4: "#{params[:query]["last4"]}", archived: false)#.page(page).per_page(per_page)#.order("#{sort_column} #{sort_direction}")
          if @cards.present?
            user_ids = []
            @cards.each do |card|
              if card.user.present?
                  user_ids.push card.user.id
              end
            end
            @users = User.where(id: user_ids).unarchived_users
            @users
          end
        end
      end
      if @users.present?
        @users = @users.page(page).per_page(per_page)
      end
      users =@users
    else
      users = User.where.not(email: current_user.email, archived: true).order("#{sort_column} #{sort_direction}")
      if users.present?
        users = users.page(page).per_page(per_page)
      end
      users
    end
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end