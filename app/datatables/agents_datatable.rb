class AgentsDatatable
  ApplicationHelper
  delegate :params, :h, :link_to, :agent_path, :span,:edit_agent_path, :current_user,:content_tag,:options_for_select,:update_valid_giftcards_path,:url_for,:select_tag,:cents_to_dollars,:image_tag, :number_to_currency, to: :@view

  require 'rqrcode'

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: agents.count,
        iTotalDisplayRecords: agents.count,
        aaData: data
    }
  end

  private

  def data
    agents.map do |gc|
      [
          (gc.ref_no.nil? ? gc.id : gc.ref_no ),
          "#{gc.first_name} #{gc.last_name}",
          gc.phone_number,[
              link_to("Edit", edit_agent_path(gc)  ,:class => "btn btn-sm btn-primary" ),
              link_to("View", agent_path(gc)  ,:class => "btn btn-sm btn-info"),
              link_to("Archive",gc, controller: "admins/users",:method => :delete, class: 'btn btn-sm btn-warning', :data => { :confirm => 'Are you sure you want to archive this agent?'})
          ]
      ]
    end
  end

  def agents
    @agents ||= fetch_agents
  end

  def sort_column
    columns = %w[id name  phone_number]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_agents
    agents = User.agent.where(archived: false).order("#{sort_column} #{sort_direction}")
    agents = agents.page(page).per_page(per_page)
    if params[:search][:value].present?
      agents=agents.where("lower(name)  like lower(:search) or lower(last_name)  like lower(:search)  or phone_number like :search", search: "%#{params[:search][:value]}%")
    end
    agents
  end


  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end