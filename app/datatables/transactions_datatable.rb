class TransactionsDatatable
  ApplicationHelper
  delegate :params, :h, :link_to, :span, :current_user,:content_tag,:options_for_select,:url_for,:select_tag, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: transactions.present? ? transactions.count : 0,
      iTotalDisplayRecords: transactions.present? ? transactions.count : 0,
      aaData: data
    }
  end

private

  def data
    transactions.map do |record|
      if record['ssl_txn_id'].present? && record['ssl_trans_status'].present?
        [
            record['ssl_txn_id'],
            "#{record['ssl_first_name'] || 'No Name'} #{record['ssl_last_name'] || ''}",
            content_tag('span', record['ssl_trans_status'] , class: "label label-default text-capitalize"),
            record['ssl_card_number'] || 'No Card Number',
            record['ssl_card_type'] || 'Not Specified',
            record['ssl_approval_code'] || 0,
            record['ssl_amount'] || 0,
            record['ssl_txn_time'] || 'No Time Provided'
        ]
      end
    end
  end

  def transactions
    @transactions ||= fetch_transactions
  end

  def sort_column
    columns = %w[id name status card_number card_type approval_code amount created_at]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_transactions
    begin
      url = "https://demo.myvirtualmerchant.com/VirtualMerchantDemo/processxml.do?xmldata=<txn><ssl_merchant_id>002544</ssl_merchant_id><ssl_user_id>webpage</ssl_user_id><ssl_pin>6FCEZY</ssl_pin><ssl_transaction_type>txnquery</ssl_transaction_type><ssl_search_start_date>#{(Date.today-31).strftime("%m/%d/%Y")}</ssl_search_start_date><ssl_search_end_date>#{Time.now.strftime("%m/%d/%Y")}</ssl_search_end_date></txn>"
      response = HTTParty.get(url)
      p "datatatatatat", response.parsed_response
      if response.parsed_response['txnlist']['txn'].present?
        response.parsed_response['txnlist']['txn']
      else
        []
      end
    rescue => exc
      p "TRANSACTIONS EXCEPTION", exc
      return []
    end
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end