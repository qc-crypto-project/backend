class VerificationUsersDatatable
  delegate :params, :h, :link_to, :span, :verification_admins_path, :content_tag, :icon, :image_tag, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: User.where.not(email: 'admin@admin.com').count,
      iTotalDisplayRecords: users.total_entries,
      aaData: data
    }
  end

private

  def data
    users.map do |user|
      [
        user.id ,
        user.name,
        user.email,
        user.phone_number,
        user.created_at,
        (user.is_block == false ? content_tag('span','Approved' , class: 'label icon-btn label-success glyphicon  fa fa-check img-circle status_class') : content_tag('span',' Disapproved' , class: 'label icon-btn label-danger glyphicon  fa fa-times img-circle status_class')),
        if user.merchant? && user.is_block == true && Location.where(merchant_id: user.id).size == 0 && user.merchant_id.nil?
          link_to('Locations', {:controller => "admins/locations", :action => "index", :user => user.id } ,class: 'btn btn-sm btn-success')
        else
          link_to("Verify",verification_admins_path(id: user.id) , remote: true ,:data => {:toggle => 'modal', :id => user.id},class: 'btn btn-primary btn-sm')
        end

      ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def sort_column
    columns = %w[id name email phone_number is_block postal_address,created_at ]

    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_users
    users = User.where.not(email: 'admin@admin.com').order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    if params[:search][:value].present?
      users = users.where("lower(name) like lower(:search) or lower(email) like lower(:search) or phone_number like :search", search: "%#{params[:search][:value]}%")
    end
    users
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end