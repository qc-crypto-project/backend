class LocationsDatatable
  ApplicationHelper
  delegate :params, :h,:location_in_valid_location_path, :update_location_locations_path, :link_to,:edit_location_path, :span, :current_user,:content_tag,:options_for_select,:update_valid_giftcards_path,:url_for,:select_tag,:cents_to_dollars,:image_tag, :number_to_currency, to: :@view

  require 'rqrcode'

  def initialize(view,list)
    @view = view
     @locations = list
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: locations.count,
      iTotalDisplayRecords: locations.count,
      aaData: data

    }
  end

private

  def data
    if params[:user_id].present?
      locations.map do |gc|
        [
            gc.id,
            gc.business_name,
            gc.email,
            gc.web_site,
            gc.category,
            [
                link_to('Wallets',gc, class: 'btn btn-sm btn-success'),
                link_to('Edit',edit_location_path(gc),class: 'btn btn-sm btn-info'),
                link_to('Update',update_location_locations_path(gc.id),class: 'btn btn-sm btn-warning', data: { method: "get", confirm: "Are you sure?" }),
                select_tag('is_block', options_for_select([["block", true],["un blocked", false]], selected:gc.is_block ),
                           include_blank: "Change status",
                           class: 'form-control',
                           style: 'padding:0px;height:23px',
                           id: 'status',
                           data: {
                               remote: true,
                               url:  location_in_valid_location_path(gc.id),
                               method: 'post',
                               confirm: 'Are you sure?'
                           })
            ]
        ]
      end
    else
      locations.map do |gc|
        [
            gc.try(:merchant).try(:id),
            gc.try(:merchant).try(:name),
            gc.business_name,
            gc.try(:merchant).try(:email),
            gc.ecom_platform,
            gc.category,
            [
                link_to('Wallets',gc, class: 'btn btn-sm btn-success'),
                link_to('Edit',edit_location_path(gc),class: 'btn btn-sm btn-info'),
                link_to('Update',update_location_locations_path(gc.id),class: 'btn btn-sm btn-warning', data: { method: "get", confirm: "Are you sure?" }),
                select_tag('is_block', options_for_select([["block", true],["un blocked", false]], selected:gc.is_block ),
                           include_blank: "Change status",
                           class: 'form-control',
                           style: 'padding:0px;height:23px',
                           id: 'status',
                           data: {
                               remote: true,
                               url:  location_in_valid_location_path(gc.id),
                               method: 'post',
                               confirm: 'Are you sure?'
                           })
            ]
        ]
      end

    end

  end

  def locations
    fetch_locations
  end

  def sort_column
    columns = %w[id business_name  email web_site]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end
  def fetch_locations
     # @locations = @locations.sort_by(&:"#{sort_column}")
     # @locations = @locations.reverse if sort_direction == 'DESC'
     # locations = @locations.page(page).per_page(per_page)
     locations = @locations.order("#{sort_column} #{sort_direction}")
     locations = locations.page(page).per_page(per_page)
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end
