class PartnersDatatable
  ApplicationHelper
  delegate :params, :h, :link_to, :span,:edit_partner_path, :current_user,:content_tag,:options_for_select,:update_valid_giftcards_path,:url_for,:select_tag,:cents_to_dollars,:image_tag, :number_to_currency, to: :@view

  require 'rqrcode'

  def initialize(view, list)
    @view = view
    @partners = list
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: partners.count,
      iTotalDisplayRecords: partners.count,
      aaData: data
    }
  end

private

  def data
    partners.map do |gc|
      [
          gc.id,
          gc.name,
          gc.phone_number,
          link_to("Edit", edit_partner_path(gc.id),:remote => true  ,:class => "btn btn-sm btn-primary editModel", "data-toggle" => "modal" )
      ]
    end
  end

  def partners
    fetch_partners
  end

  def sort_column
    columns = %w[id name  phone_number]
    columns[params[:order]["0"][:column].to_i]
  end

  def sort_direction
    params[:order]["0"][:dir] == "desc" ? "desc" : "asc"
  end

  def fetch_partners
    partners = @partners.order("#{sort_column} #{sort_direction}")
    partners = partners.page(page).per_page(per_page)
    if params[:search][:value].present?
      # companies = companies.where("token like :search or wallet_id=#{params[:search][:value].to_i} ", search: "%#{params[:search][:value]}%")
    end
    partners
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end
