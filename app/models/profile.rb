# == Schema Information
#
# Table name: profiles
#
#  id                :bigint(8)        not null, primary key
#  user_id           :integer
#  company_name      :string
#  business_type     :integer
#  ein               :string
#  business_location :string
#  street_address    :string
#  city              :string
#  state             :string
#  zip_code          :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  years_in_business :string
#  tax_id            :string
#

class Profile < ApplicationRecord
  audited
  belongs_to :user
  enum business_type: [:sole_proprietorship,:individual ,:s_corp,:c_corp,:limited_liablity_company, :general_partnership, :limited_partnership ,:non_profit ,:professional_association,:limited_liability_partnership]
end

