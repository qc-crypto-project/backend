# == Schema Information
#
# Table name: fees
#
#  id                           :bigint(8)        not null, primary key
#  customer_fee                 :string
#  b2b_fee                      :string
#  redeem_fee                   :string
#  send_check                   :string
#  gbox                         :string
#  partner                      :string
#  iso                          :string
#  agent                        :string
#  fee_id                       :integer
#  fee_status                   :integer
#  location_id                  :integer
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  buy_qc_card_cash             :float
#  buy_qc_card_cc               :float
#  atm_processing_fee           :float
#  user_id                      :integer
#  add_money_check              :string
#  redeem_check                 :string
#  giftcard_fee                 :string
#  reserve_fee                  :float            default(0.0)
#  days                         :integer
#  customer_fee_dollar          :string
#  transaction_fee_app          :string
#  transaction_fee_app_dollar   :string
#  transaction_fee_dcard        :string
#  transaction_fee_dcard_dollar :string
#  transaction_fee_ccard        :string
#  transaction_fee_ccard_dollar :string
#  charge_back_fee              :float            default(0.0)
#  retrievel_fee                :float            default(0.0)
#

class Fee < ApplicationRecord
  audited
  enum fee_status: [:processing_fee, :customer_fee, :b2b_fee, :redeem_fee, :send_check, :add_check, :redeem_check, :giftcard_fee, :reserve_fee, :customer_fee_dollar, :transaction_fee_app, :transaction_fee_app_dollar,:transaction_fee_dcard, :transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :dispute_chargeback_fee,:dispute_retrivel_fee, :buy_rate, :buy_program, :user_fee]
  enum risk: [:high, :low]

  # has_one :fee_type, class_name: "Fee", foreign_key: "fee_id"
  has_and_belongs_to_many :location , optional: true
  belongs_to :user , optional: true
  belongs_to :payment_gateway, optional: true

end
