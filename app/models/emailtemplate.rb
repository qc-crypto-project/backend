# == Schema Information
#
# Table name: emailtemplates
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  body       :text
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Emailtemplate < ApplicationRecord
end
