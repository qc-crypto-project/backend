# == Schema Information
#
# Table name: companies
#
#  id            :bigint(8)        not null, primary key
#  name          :string
#  category      :string
#  phone_number  :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  archived      :boolean          default(FALSE)
#  company_email :string
#  slug          :string
#  ledger        :string
#

class Company < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :giftcards
  validates :name, :presence => true
  has_many :users
  after_create :set_ref_no
  # after_create :create_company_admin

  def create_company_admin
    password = "#{rand(1..10)}#{Devise.friendly_token.first(8)}"
    user = User.new(name: self.name, email: self.company_email.downcase, company_id: self.id, phone_number: self.phone_number, ledger: self.ledger)
    user.password = password
    if user.partner!
      user.update(ref_no: "CP-#{user.id}")
      # UserMailer.welcome_merchant(user, password, "Company Signup").deliver
    end
  end

  def set_ref_no
    self.update(ref_no: "CO-#{self.id}")
  end
end
