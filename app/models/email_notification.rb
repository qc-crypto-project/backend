class EmailNotification < ApplicationRecord
  STATUS = {
      "accounting": {id: 1, email: "accounting@greenboxpos.com", signature: "GreenBox Accounting Team"},
      "technical_support": {id: 2, email: "technical_support@greenboxpos.com", signature: "GreenBox Technical Support Team"},
      "merchant_services": {id: 3, email: "merchant_services@greenboxpos.com", signature: "GreenBox Merchant Services Team"},
      "integration": {id: 4, email: "integration@greenboxpos.com", signature: "GreenBox Integration Team"},
      "legal": {id: 5, email: "legal@greenboxpos.com", signature: "GreenBox Legal Team"}
  }
  enum sender: {"accounting": "accounting",
                 "technical_support": "technical_support",
                 "merchant_services": "merchant_services",
                 "integration": "integration",
                 "legal": "legal"}
  validates :sender, inclusion: { in: senders.values }

  has_many :email_messages
  has_many :images

  def self.sender_info
    STATUS
  end

end
