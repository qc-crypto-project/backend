class CronJob < ApplicationRecord
  enum log_type: [:checks, :release_batch, :monthly_fee, :rake_task, :update_transfer,:gateway_update,:refund_update, :refund_revert,:user_archive,:location,:usersfee,:locationusers,:cardupdate,
                  :ach_settled, :ach_settled_revert ,:batchdate_task, :sync, :reports,:user_profile_task,:rake_task_revert, :add_owner, :users_ref_no, :ach_ftp_process, :refund_task, :update_company_name,
                  :update_txn_action_for_achs, :txn_fee_missing, :sequence_ids, :update_attached_locations, :create_user_profile,:sub_merchant_name,:sub_merchant_auth,:vt_country_code,:maxmind_review,:cancel_review_expire,
                  :counter_offer_review_expire, :refund_review_expire , :return_status_review_expire, :transaction_batch, :failed_ach_txn_seq_id, :update_ach_limit]
  enum status: {:pending => "pending", :processing => "processing", :failed => "failed", :completed => "completed", :bypass => "bypassed"}
end
