# == Schema Information
#
# Table name: requests
#
#  id                  :bigint(8)        not null, primary key
#  amount              :string
#  status              :string
#  sender_id           :integer
#  reciever_id         :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  wallet_id           :string
#  sender_name         :string
#  reciever_name       :string
#  sender_access_token :string
#  pin_code            :string
#  description         :text
#

class Request < ApplicationRecord
  belongs_to :sender, :class_name => 'User'
  belongs_to :reciever, :class_name => 'User'
  after_create :notify_user
  has_many   :images
  has_many   :products
  has_one :qr_card
  has_one :invoice_transaction, class_name: 'Transaction', foreign_key: 'order_id'
  belongs_to :account_wallet, class_name: 'Wallet', foreign_key: "wallet_id"
  has_many :addresses
  accepts_nested_attributes_for :images
  after_update :notify_user
  # after_update :notify_user_update

  enum status: {"in_progress" => "in_progress", "pending" => "pending", "saved" => "saved", "cancel" => "cancel", "past_due" => "past_due", "approved" => "approved", "rejected" => "rejected", "refunded" => "refunded"}
  enum request_type: {"b2b" => "b2b", "qr_code" => "qr_code", "invoice" => "invoice"}

  def notify_user
    Notification.notify_user(self.reciever, self.sender, self,nil,self.status) if self.b2b?
  end

  # def notify_user_update
  #   Notification.notify_user(self.sender, self.reciever, self)
  # end

  def regenerate_token
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    self.sender_access_token = token
  end

  def create_qr_batch(type,old_type=nil)
    batch = Batch.qr_scan.created_today.first
    if batch.present?
      batch_detail = batch.batch_detail.with_indifferent_access
      batch_detail["#{type}"]["id"].push(self.id) unless batch_detail["#{type}"]["id"].include? self.id
      batch_detail["#{type}"]["amount"] = batch_detail["#{type}"]["amount"].to_f + self.amount.to_f if self.status == "in_progress"
      batch_detail["#{type}"]["in_progress"] +=  1 if self.status == "in_progress"
      batch_detail["#{type}"]["approved"] +=  1 if self.status == "approved"
      batch_detail["#{type}"]["refunded"] +=  1 if self.status == "refunded"
      batch_detail["#{type}"]["#{old_type}"] -=  1 if old_type.present?
      batch.batch_detail = batch_detail
    else
      batch_detail = {}
      if type == "credit"
        batch_detail["credit"] = { id: [self.id], amount: self.amount.to_f, in_progress: 1, approved: 0, refunded: 0 }
        batch_detail["debit"] = { id: [], amount: 0, in_progress: 0, approved: 0, refunded: 0 }
      elsif type == "debit"
        batch_detail["credit"] = { id: [], amount: 0, in_progress: 0, approved: 0, refunded: 0 }
        batch_detail["debit"] = { id: [self.id], amount: amount, in_progress: 1, approved: 0, refunded: 0 }
      end
      batch = Batch.new(user_id: self.reciever_id,batch_type: "qr_scan",batch_detail: batch_detail)
    end
    batch.save!
  end

  def approve_request_status
    self.status = "approved"
    self.save
  end

  def self.to_csv(timezone)
    CSV.generate do |csv|
      csv << ['Date/Time', 'Sender', 'Receiver', 'Amount', 'Status']
      all.each do |request|
        csv << [request.created_at.in_time_zone(timezone).strftime("%m-%d-%Y %I:%M:%S %p"), request.sender.try(:name), request.reciever.try(:name), request.amount, request.status == "in_progress" ? "pending" : request.status]
      end
    end
  end

end
