class PaymentGateway < ApplicationRecord
  # audited max_audits: 5, except: [:client_secret,:client_id,:authentication_id,:processor_id]
  attr_accessor :current_user,:request_ip
  # has_many :user
  has_many :transactions
  has_many :order_banks
  has_many :dispute_cases
  has_many :misc_fees
  has_many :transaction_types
  has_many :vertical_types
  has_one :fee
  belongs_to :descriptor, optional: true
  # load balancer relations
  belongs_to :load_balancer, optional: true
  after_save :save_logs
  LOGS_FIELDS=['name','key','account_processing_limit','transaction_fee','charge_back_fee','retrieval_fee','per_transaction_fee','reserve_money','reserve_money_days',
                'misc_fee','misc_fee_dollars','cbk_max_percent','cbk_max_count','average_ticket','high_ticket','daily_limit','monthly_limit','virtual_tx','status',
                'card_selection','monthly_service_fee','enable_refund','is_block']
  # belongs_to :load_balancer_rule, optional: true

  accepts_nested_attributes_for :transaction_types, :allow_destroy => true
  accepts_nested_attributes_for :misc_fees, :allow_destroy => true
  accepts_nested_attributes_for :vertical_types, :allow_destroy => true
  accepts_nested_attributes_for :fee, allow_destroy: true

  self.inheritance_column = :_type_disabled

  validates :name, presence: true
  validates :key, presence: true
  validates :slot_name, uniqueness: true, :if => Proc.new { |a| a.slot? &&  a.new_record? }

  enum type: [:stripe, :converge, :fluid_pay, :i_can_pay, :non_bank, :bolt_pay, :slot, :ficer, :knox_payments, :total_pay, :payment_technologies, :mentom_pay]

  enum status: {active: "active", in_active: "in_active", terminated: "terminated", archived: "archived"}
  CARDTYPES = {"visa_credit"=>"on", "visa_debit"=>"on", "mastercard_credit"=>"on", "mastercard_debit"=>"on","amex_credit"=>"on","discover_credit"=>"on","jcb_credit"=>"on"}

  scope :active_gateways, -> { where(is_deleted: false) }
  # scope :active_load_gateways, -> (brand_type){ self.where(blocked_by_load_balancer: false).where("card_selection ->> '#{brand_type}' = ?","on") }
  scope :blocked, -> { where(blocked_by_load_balancer: true) }
  scope :archived_gateways, -> { where(is_deleted: true) }
  scope :main_gateways, -> { where.not(type: "slot") }

  scope :load_balancer_gateway, -> {
    Rails.cache.fetch("load_balancer_gateway", expires_in: 10.minutes) do
      where("type != :type AND is_deleted = false AND is_block = false AND load_balancer_id IS NULL AND descriptor_id IS NOT NULL",type:  6)
    end
  }

  scope :all_active, -> (value) {
    if value.present?
      Rails.cache.fetch("specific_active_#{value}", expires_in: 10.minutes) do
        where("type != :type AND is_block = false AND (key ILIKE :value OR name ILIKE :value)",type:  6, value: value)
      end
    else
      Rails.cache.fetch("all_active", expires_in: 10.minutes) do
        where("type != :type AND is_block = false",type:  6)
      end
    end
  }

  scope :all_inactive, -> (value) {
    if value.present?
      Rails.cache.fetch("specific_inactive_#{value}", expires_in: 10.minutes) do
        where("type != :type AND is_deleted = false AND is_block = true AND (key ILIKE :value OR name ILIKE :value)",type:  6, value: value)
      end
    else
      Rails.cache.fetch("all_inactive", expires_in: 10.minutes) do
        where("type != :type AND is_block = true AND is_deleted = false",type:  6)
      end
    end
  }

  def self.active_load_gateways(brand_type = nil, issue_amount = nil)
    if brand_type.present? && PaymentGateway.card_types.keys.include?(brand_type)
      pgs = self.where(blocked_by_load_balancer: false).where("card_selection ->> '#{brand_type}' = ?","on").order(name: :asc)
    else
      pgs = self.where(blocked_by_load_balancer: false).order(name: :asc)
    end
    my_pgs = []
    if issue_amount.present?
      pgs.each do |pg|
        if (pg.daily_volume_achived.to_f + issue_amount) >= pg.daily_limit.to_f ||
            (pg.monthly_volume_achived.to_f + issue_amount) >= pg.monthly_limit.to_f || pg.high_ticket.to_f < issue_amount
        else
          my_pgs << pg
        end
      end
    end
    return my_pgs
  end

  def update_daily_monthly_limits(issue_amount)
    d_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_day
    m_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_month
    if self.present?
      if self.last_daily_updated.present? &&
          d_date != self.last_daily_updated.in_time_zone(PSTTIMEZONE).beginning_of_day ||
          self.last_daily_updated.blank?
        self.daily_volume_achived = 0
      end
      if self.last_monthly_updated.present? &&
          m_date != self.last_monthly_updated.in_time_zone(PSTTIMEZONE).beginning_of_month ||
          self.last_monthly_updated.blank?
        self.monthly_volume_achived = 0
      end
      self.daily_volume_achived = self.daily_volume_achived + issue_amount
      self.last_daily_updated = d_date
      self.monthly_volume_achived = self.monthly_volume_achived + issue_amount
      self.last_monthly_updated = m_date
      self.save
    end
  end

  def self.card_types
    return CARDTYPES
  end

  def self.get_report(gateways_key,first_date, second_date, date_range, params)
    details = []
    if params["gateway"].present? && params["gateway"].include?("all")
      gateways = PaymentGateway.all.select(:id, :name, :type, :key)
    else
      gateways = PaymentGateway.where(id: gateways_key).select(:id, :name, :type, :key)
    end
    transactions = Transaction.where("timestamp BETWEEN :first AND :second",first: first_date,second: second_date).where("(payment_gateway_id IN (:gateway_ids) AND main_type IN (:main_types)) OR (main_type IN (:secondary_type) AND transactions.from IN (:credit))",gateway_ids: gateways.pluck(:id), main_types: [TypesEnumLib::TransactionViewTypes::DebitCard,
                                                                                          TypesEnumLib::TransactionViewTypes::PinDebit,
                                                                                          TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard], secondary_type: ['Sale'],credit: "Credit").select(:id,:amount, :total_amount, :status, :payment_gateway_id).group_by{|e| e.payment_gateway_id}
    pending_transactions = Transaction.where("created_at BETWEEN :first AND :second",first: first_date,second: second_date).where(main_type: [TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::DebitCard,TypesEnumLib::TransactionViewTypes::PinDebit], payment_gateway_id: gateways.pluck(:id), status: "pending").select(:id,:amount, :total_amount, :status, :payment_gateway_id).group_by{|e| e.payment_gateway_id}
    cbk_second_date = second_date
    start_month = first_date.to_datetime.beginning_of_month
    start_month = DateTime.new(start_month.year, start_month.month, start_month.day, 07,00,00)
    cbks = DisputeCase.charge_back.where(payment_gateway_id: gateways.pluck(:id)).where("recieved_date BETWEEN :first AND :second",first: start_month,second: cbk_second_date).select(:amount, :payment_gateway_id, :id).group_by{|e| e.payment_gateway_id}
    refund_retire = Transaction.where(payment_gateway_id: gateways.pluck(:id), main_type: ["refund","refund_bank"], action: 'retire').where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).select(:total_amount, :payment_gateway_id).group_by{|e| e.payment_gateway_id}
    # refund_bank = Transaction.where(payment_gateway_id: gateways.pluck(:id), main_type: ["refund_bank"], action: 'retire').where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).select(:total_amount, :payment_gateway_id).group_by{|e| e.payment_gateway_id}
    gateways.each do |g|
      if transactions[g.id].present?
        refund_tx = refund_retire[g.id]
        decline_tx = pending_transactions[g.id]
        total_tx = transactions[g.id].try(:count)
        cbk_tx = cbks[g.id].try(:count)
        gateway_transactions = transactions[g.id].group_by{|e| e.status}
        approved_transactions = gateway_transactions["approved"] || []
        decline_transactions = decline_tx || []
        refunds_transactions = gateway_transactions["refunded"] || []
        # manual_refund = gateway_transactions.try(:[], "manual-refund") || []
        complete_transactions = gateway_transactions.try(:[],"complete")
        # refund_total_tx = refunds_transactions + complete_transactions if refunds_transactions.present? && complete_transactions.present?
        # sale_transactions = gateway_transactions.try(:[],"sale")
        # approved_transactions = approved_transactions + manual_refund if manual_refund.present?
        approved_transactions = approved_transactions + refunds_transactions if refunds_transactions.present?
        approved_transactions = approved_transactions + complete_transactions if complete_transactions.present?
        # approved_transactions = approved_transactions + sale_transactions if sale_transactions.present?
        approved_count = approved_transactions.try(:count).to_i
        cbk_percent = cbk_tx.to_f/approved_count.to_f * 100 if approved_count.to_i > 0 && cbk_tx.to_i > 0
        total_volume = approved_transactions.try(:pluck,:total_amount).try(:sum,&:to_f)
        decline_volume = decline_transactions.pluck(:total_amount).sum(&:to_f) if decline_transactions.present?
        decline_volume = decline_transactions.pluck(:amount).sum(&:to_f) if decline_transactions.present? && decline_volume.to_f == 0
        decline_count = decline_transactions.try(:count).to_i
        refund_volume = refund_tx.try(:pluck,:total_amount).try(:sum,&:to_f)
        refund_count = refund_tx.try(:count).to_i
      end
      if (params["not_0"].present? && params["not_0"] == "true") && ( refund_volume.to_f == 0 && cbk_tx.to_i == 0 && decline_volume.to_f == 0 && total_volume.to_f == 0 && approved_count.to_i == 0 && cbk_percent.to_f == 0 && total_tx.to_i == 0 && decline_count.to_i == 0 && refund_count.to_i == 0)
      else
        g_detail = {
            date_range: date_range,
            name: g.key,
            descriptor: g.name,
            total_approved: total_volume || 0,
            total_approved_count: approved_count,
            avg_per_txn: approved_count.to_f > 0 ? total_volume.to_f / approved_count.to_f : 0,
            decline_vol: decline_volume,
            total_decline: decline_count || 0,
            decline_perc: total_tx.to_f > 0 ? decline_count.to_f * 100 / total_tx.to_f : 0,
            refund_vol: refund_volume.to_f,
            total_refund: refund_count || 0,
            refund_perc: (refund_count.to_f/approved_count.to_f * 100).to_f,
            total_cbk: cbk_tx || 0,
            cbk_percent: cbk_percent || 0
        }
        details.push(g_detail)
      end
    end
    details
  end

  def self.get_audit_report(first_date, second_date)
    details = []
    gateways = PaymentGateway.all.select(:id, :name, :type,:reserve_money,:transaction_fee,:per_transaction_fee)
    transactions = Transaction.where(payment_gateway_id: gateways.pluck(:id), main_type: [TypesEnumLib::TransactionViewTypes::SaleIssue, TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::PinDebit], created_at: first_date..second_date).select(:id,:amount, :total_amount, :status, :payment_gateway_id,:gbox_fee,:fee,:agent_fee,:affiliate_fee,:iso_fee,:reserve_money,:per_transaction_fee,:transaction_fee).group_by{|e| e.payment_gateway_id}
    cbk_first_date = first_date.utc.beginning_of_day
    cbk_second_date = second_date.utc.beginning_of_day - 1.day
    cbks = DisputeCase.charge_back.where(payment_gateway_id: gateways.pluck(:id), recieved_date: cbk_first_date..cbk_second_date).select(:amount, :payment_gateway_id, :id, :status,:transaction_id).group_by{|e| e.payment_gateway_id}
    gateways.each do |g|
      if transactions[g.id].present?
        total_tx = transactions[g.id].try(:count)
        gateway_cbks = cbks[g.id]
        cbk_amount = gateway_cbks.try(:pluck, :amount).try(:sum,&:to_f).to_f
        cbk_tx = gateway_cbks.try(:count)
        group_cbk = gateway_cbks.group_by{|e| e.status} if gateway_cbks.present?
        pend_volume = group_cbk["sent_pending"].try(:pluck, :amount).try(:sum,&:to_f).to_f if group_cbk.present?
        pend_count = group_cbk["sent_pending"].try(:count).to_i if group_cbk.present?
        won_volume = group_cbk["won_reversed"].try(:pluck, :amount).try(:sum,&:to_f).to_f if group_cbk.present?
        won_count =  group_cbk["won_reversed"].try(:count).to_i if group_cbk.present?
        lost_count = group_cbk["lost"].try(:count).to_i if group_cbk.present?
        lost_volume = group_cbk["lost"].try(:pluck, :amount).try(:sum,&:to_f).to_f if group_cbk.present?
        if gateway_cbks.present?
          cbk_trx = Transaction.where(id: gateway_cbks.pluck(:transaction_id)).select(:fee,:gbox_fee, :iso_fee,:transaction_fee, :per_transaction_fee)
          if cbk_trx.present?
            total_cbk_fee = cbk_trx.pluck(:fee).try(:sum, &:to_f)
            gbox_cbk_fee = cbk_trx.pluck(:gbox_fee).try(:sum, &:to_f)
            iso_cbk_fee = cbk_trx.pluck(:iso_fee).try(:sum, &:to_f)
            # per_transaction_fee = 0
            # cbk_trx.map do |t|
            #   perc_cbk = g.per_transaction_fee.to_f/100 * cbk_amount.to_f
            #   per_transaction_fee = per_transaction_fee.to_f + perc_cbk
            # end
            # cbk_bank_fee = g.transaction_fee.to_f + per_transaction_fee.to_f
            gbox_cbk_net = gbox_cbk_fee.to_f
          end
        end
        # bank_charge_back = g.read_attribute(:charge_back_fee)

        gateway_transactions = transactions[g.id].group_by{|e| e.status}
        approved_transactions = gateway_transactions["approved"] || []
        decline_transactions = gateway_transactions["pending"]
        refund_transactions = gateway_transactions["refunded"] || []
        # manual_refund = gateway_transactions.try(:[], "manual-refund") || []
        complete_transactions = gateway_transactions.try(:[],"complete")
        sale_transactions = gateway_transactions.try(:[],"sale")
        # approved_transactions = approved_transactions + manual_refund if manual_refund.present?

        approved_transactions = approved_transactions + refund_transactions if refund_transactions.present?
        approved_transactions = approved_transactions + complete_transactions if complete_transactions.present?
        approved_transactions = approved_transactions + sale_transactions if sale_transactions.present?
        approved_count = approved_transactions.count

        cbk_percent = cbk_tx.to_f/approved_count.to_f * 100 if approved_count.to_i > 0 && cbk_tx.to_i > 0

        total_volume = approved_transactions.try(:pluck,:total_amount).try(:sum,&:to_f)
        decline_volume = decline_transactions.pluck(:total_amount).sum(&:to_f) if decline_transactions.present?
        decline_volume = decline_transactions.pluck(:amount).sum(&:to_f) if decline_transactions.present? && decline_volume.to_f == 0
        gbox_sales_profit = approved_transactions.try(:pluck,:gbox_fee).try(:sum,&:to_f)
        iso_sales_profit = approved_transactions.try(:pluck,:iso_fee).try(:sum,&:to_f)
        total_reserve_bank = approved_transactions.try(:pluck,:reserve_money).try(:sum,&:to_f)
        per_transaction_fee = 0
        approved_transactions.try(:map) do |tx|
          perc = tx.transaction_fee.to_f/100 * tx.total_amount.to_f
          per_transaction_fee = per_transaction_fee.to_f + perc.to_f
        end
        transaction_fee = approved_transactions.try(:pluck,:per_transaction_fee).try(:sum,&:to_f)
        bank_sales_fee = transaction_fee.to_f + per_transaction_fee.to_f
        gbox_sales_net_profit = gbox_sales_profit.to_f - bank_sales_fee.to_f
        total_sales_fee = approved_transactions.try(:pluck,:fee).try(:sum,&:to_f)

        total_refund_volume = refund_transactions.try(:pluck,:total_amount).try(:sum,&:to_f)
        # per_refund_transaction_fee = 0
        # refund_transactions.try(:map) do |tx|
        #   perc_refund = tx.per_transaction_fee.to_f/100 * total_refund_volume.to_f
        #   per_refund_transaction_fee = per_refund_transaction_fee.to_f + perc_refund.to_f
        # end
        total_refund_fee = refund_transactions.try(:pluck,:fee).try(:sum,&:to_f)
        # refund_transaction_fee = refund_transactions.try(:pluck,:transaction_fee).try(:sum,&:to_f)
        # bank_refund = per_refund_transaction_fee.to_f + refund_transaction_fee.to_f
        gbox_refund_profit = refund_transactions.try(:pluck,:gbox_fee).try(:sum,&:to_f)
        iso_refund_profit = refund_transactions.try(:pluck,:iso_fee).try(:sum,&:to_f)
        gbox_refund_net_profit = gbox_refund_profit.to_f

      end
      g_detail = {
          name: g.type,
          descriptor: g.name,
          total_approved: total_volume || 0,
          total_approved_count: approved_transactions.present? ? approved_transactions.try(:count) : 0,
          avg_per_txn: total_tx.to_f > 0 ? total_volume.to_f / total_tx : 0,
          decline_vol: decline_volume,
          total_decline: decline_transactions.try(:count) || 0,
          decline_perc: total_tx.to_f > 0 ? decline_transactions.try(:count).to_f * 100 / total_tx : 0,
          refund_vol: refund_transactions.present? ? refund_transactions.pluck(:total_amount).sum(&:to_f) : 0,
          total_refund: refund_transactions.try(:count) || 0,
          refund_perc: refund_transactions.present? ? total_tx.to_f > 0 ? refund_transactions.try(:count).to_f * 100 / total_tx : 0 : 0,

          cbk_amount: cbk_amount || 0,
          total_cbk: cbk_tx || 0,
          cbk_percent: cbk_percent || 0,
          pend_volume: pend_volume || 0,
          pend_count: pend_count || 0,
          won_volume: won_volume || 0,
          won_count: won_count || 0,
          lost_count: lost_count || 0,
          lost_volume: lost_volume || 0,
          gbox_cbk_charge_back: gbox_cbk_fee || 0,
          iso_cbk_charge_back: iso_cbk_fee || 0,
          gbox_cbk_charge_back_net: gbox_cbk_net || 0,
          cbk_charge_back: 0,
          total_cbk_fee: total_cbk_fee || 0,

          gbox_profit: gbox_sales_profit || 0,
          gbox_net_profit: gbox_sales_net_profit || 0,
          total_reserve_bank: total_reserve_bank || 0,
          iso_sales_profit: iso_sales_profit || 0,
          bank_sales_fee: bank_sales_fee || 0,
          total_sales_fee: total_sales_fee || 0,
          total_refund_volume: total_refund_volume || 0,
          bank_refund: 0,
          gbox_refund_net_profit: gbox_refund_net_profit || 0,
          iso_refund_profit: iso_refund_profit || 0,
          gbox_refund_profit: gbox_refund_profit || 0,
          total_refund_fee: total_refund_fee || 0

      }
      details.push(g_detail)
    end
    details
  end

  def save_logs
    if self.previous_changes.present?
      hash=self.previous_changes.select {|k,v| LOGS_FIELDS.include?(k) }
      ActivityLog.create(params:{'gateway_id'=>self.try(:id),'name'=>self.try(:key)},note:hash.to_json,activity_type: "user_activity",user_id:self.try(:current_user).try(:id),action:'update',ip_address:self.try(:request_ip)) if hash.present?
    end
  end

  def assigned_mids
    Location.where('primary_gateway_id = :id or secondary_gateway_id = :id  or ternary_gateway_id = :id', id: self.id).select(:id)
  end
end
