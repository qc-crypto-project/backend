class LocationsUsers < ApplicationRecord
  belongs_to :user
  belongs_to :location
  enum relation_type: {primary: "primary", secondary: "secondary"}
end
