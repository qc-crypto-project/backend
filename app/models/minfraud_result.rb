class MinfraudResult < ApplicationRecord
  audited max_audits: 5, on: [:update]
  #risk_score:float
  #insight_detail:jsonb
  belongs_to :main_transaction, class_name: 'Transaction', foreign_key: 'transaction_id', optional: true
  validates :transaction_id, uniqueness: true, presence:true
end
