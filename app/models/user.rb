# == Schema Information
#
# Table name: users
#
#  id                            :bigint(8)        not null, primary key
#  email                         :string           default("")
#  encrypted_password            :string           default(""), not null
#  reset_password_token          :string
#  reset_password_sent_at        :datetime
#  remember_created_at           :datetime
#  sign_in_count                 :integer          default(0), not null
#  current_sign_in_at            :datetime
#  last_sign_in_at               :datetime
#  current_sign_in_ip            :inet
#  last_sign_in_ip               :inet
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  name                          :string
#  postal_address                :string
#  phone_number                  :string
#  status                        :boolean
#  user_image_file_name          :string
#  user_image_content_type       :string
#  user_image_file_size          :integer
#  user_image_updated_at         :datetime
#  front_card_image_file_name    :string
#  front_card_image_content_type :string
#  front_card_image_file_size    :integer
#  front_card_image_updated_at   :datetime
#  back_card_image_file_name     :string
#  back_card_image_content_type  :string
#  back_card_image_file_size     :integer
#  back_card_image_updated_at    :datetime
#  authentication_token          :string
#  admin                         :boolean          default(FALSE)
#  is_block                      :boolean          default(TRUE)
#  confirmation_code             :string
#  date_of_birth                 :string
#  issue_date                    :string
#  expire_date                   :string
#  stripe_customer_id            :string
#  pin_code                      :string
#  device_token                  :string
#  device_type                   :string
#  archived                      :boolean          default(FALSE)
#  registeration_id              :string
#  username                      :string
#  client_id                     :string
#  client_secret                 :string
#  app_id                        :integer
#  redirect_url                  :string
#  denominations                 :text             default("{1 => 0, 2 => 0, 5 => 0, 10 => 0, 20 => 0, 50 => 0, 100 => 0}")
#  oauth_code                    :string
#  first_name                    :string
#  last_name                     :string
#  card_json                     :json
#  company_id                    :integer
#  role                          :integer
#  fee                           :float
#  plain_password_text           :string
#  zip_code                      :string
#  merchant_id                   :integer
#  merchant_id                   :integer
#  address                       :string
#  city                          :string
#  state                         :string
#  street                        :string
#  location_id                   :integer
#  issue_fee                     :boolean          default(TRUE)
#  retail_sales                  :boolean          default(FALSE)
#  owner_name                    :string
#  confirmation_code_enc         :text
#  slug                          :string
#  ref_no                        :string
#  source                        :string
#  payment_gateway               :integer
#  fluid_pay_customer_id         :string           default("")
#  ledger                        :string
#  submerchant_type              :string
#  area_code                     :string           default("")
#

class User < ApplicationRecord
  audited only: [:email,:name,:first_name,:permission_id, :phone_number, :is_block, :merchant_ach_status, :system_fee, :last_name, :phone_number, :encrypted_password, :company_name,:admin_user_id], on: [:update, :destroy]


  include ApplicationHelper
  extend FriendlyId
  require 'rqrcode'
  require 'barby'
  require 'barby/barcode'
  require 'barby/barcode/qr_code'
  require 'barby/outputter/png_outputter'

  enum role: [:admin, :user, :atm, :gift_card, :merchant, :app, :qc, :iso, :partner, :agent, :affiliate, :support, :support_mtrac, :employee, :support_customer, :admin_user, :affiliate_program]
  # enum payment_gateway: [:stripe, :stripe_pop, :converge, :payeezy, :bridge_pay, :fluid_pay, :stripe_pop_2]
  # enum secondary_payment_gateway: [:stripe, :stripe_pop, :converge, :payeezy, :bridge_pay, :fluid_pay, :stripe_pop_2], _prefix: :secondary_payment
  # enum third_payment_gateway: [:stripe, :stripe_pop, :converge, :payeezy, :bridge_pay, :fluid_pay, :stripe_pop_2], _prefix: :third_payment
  enum payment_gateway: [:stripe, :stripe_pop, :fluid_pay, :stripe_pop_2, :converge, TypesEnumLib::GatewayType::ICanPay.to_sym]
  enum secondary_payment_gateway: [:stripe, :stripe_pop, :fluid_pay, :stripe_pop_2, :converge, TypesEnumLib::GatewayType::ICanPay.to_sym], _prefix: :secondary_payment
  enum third_payment_gateway: [:stripe, :stripe_pop, :fluid_pay, :stripe_pop_2, :converge, TypesEnumLib::GatewayType::ICanPay.to_sym], _prefix: :third_payment
  enum risk: [:high, :low]
  enum merchant_ach_status: [:initial,:pending,:complete]

  attr_accessor :skip_password_validation  # virtual attribute to skip password validation while saving

  attr_accessor :login

  friendly_id :generate_custom_slug, use: :slugged
  def generate_custom_slug
    "#{name}-#{rand(1000..9999)}-#{Time.now.to_i}".parameterize
  end
  ########################## RELATIONS ################################
  has_many :trackers
  belongs_to :permission, optional: true
  belongs_to :company, optional: true
  has_one :profile
  has_many :config_logs
  belongs_to :primary_gateway, class_name: "PaymentGateway", foreign_key: "primary_gateway_id", optional: true
  belongs_to :secondary_gateway, class_name: "PaymentGateway", foreign_key: "secondary_gateway_id", optional: true
  belongs_to :ternary_gateway, class_name: "PaymentGateway", foreign_key: "ternary_gateway_id", optional: true
  has_many :activity_logs
  has_many :transfers
  has_many :messages, foreign_key: :recipient_id

  # self Inheritence concept
  has_many :disputes_as_customer, class_name: 'CustomerDispute', foreign_key: 'customer_id'
  has_many :disputes_as_merchant, class_name: 'CustomerDispute', foreign_key: 'merchant_id'

  has_many :merchant_customers, class_name: 'CustomerMerchant', foreign_key: 'customer_id'
  has_many :location_customers, class_name: 'CustomerMerchant', foreign_key: 'location_id'

  has_many :sub_merchants, class_name: 'User', foreign_key: 'merchant_id'
  has_one :parent_merchant, class_name: 'User', primary_key: 'merchant_id', foreign_key: 'id'
  has_one :iso, class_name: 'User', foreign_key: 'merchant_id'
  has_one :partner, class_name: 'User', foreign_key: 'merchant_id'
  has_one :agent, class_name: 'User', foreign_key: 'merchant_id'
  has_many :addresses
  has_many :batches
  accepts_nested_attributes_for :addresses
  has_many  :fees
  accepts_nested_attributes_for :fees
  has_many :checks
  # ------Above Locations Added ----
  has_many :my_locations , class_name: 'Location', foreign_key: "merchant_id"
  has_many :bulk_check_instances
  has_and_belongs_to_many :wallets, dependent: :destroy
  # has_many :locations, :through => :wallets, :foreign_key => :location_id
  has_and_belongs_to_many :locations, optional: true
  has_many :cards, dependent: :destroy
  has_many :qr_cards, dependent: :destroy
  has_many :tickets, dependent: :destroy
  has_one :setting, dependent: :destroy
  has_many :send_money_requests, :class_name => 'Request', :foreign_key => "sender_id"
  has_many :recieve_money_requests, :class_name => 'Request', :foreign_key => "reciever_id"
  devise :password_expirable, :secure_validatable,:password_archivable,:two_factor_authenticatable, :database_authenticatable, :registerable, :lockable,
         :recoverable, :trackable, :timeoutable, :rememberable, authentication_keys: [:login]
  self._validators[:email].reject!{|v| v.class == ActiveRecord::Validations::UniquenessValidator}
  self._validate_callbacks.delete self._validate_callbacks.find{|v| v.filter.class == ActiveRecord::Validations::UniquenessValidator && v.filter.attributes == [:email]}
  has_one_time_password(encrypted: true)
  has_many :transactions, dependent: :destroy
  after_create :qr_create_wallet, :set_ref_no, :create_setting, :create_oauth_app, :tos_update
  after_save :update_wallets_name, if: :saved_change_to_name?
  after_update :update_bank_account, if: :saved_change_to_system_fee?

  has_many :oauth_apps, dependent: :delete_all
  has_many :comments, dependent: :destroy
  has_many :tango_orders, dependent: :destroy
  has_many :notifications, foreign_key: :recipient_id
  has_many :sender_block_transactions, class_name: "BlockTransaction", foreign_key: :sender_id
  has_many :receiver_block_transactions, class_name: "BlockTransaction", foreign_key: :receiver_id
  has_many :sender_transactions, class_name: "Transaction", foreign_key: :sender_id
  has_many :receiver_transactions, class_name: "Transaction", foreign_key: :receiver_id
  has_many :owners
  has_many :documentations, as: :imageable
  has_many :baked_users
  has_many :baked_locations, :through => :baked_users, foreign_key: :user_id
  has_many :transaction_batches, :foreign_key => :merchant_id

  has_many :affiliate_programs , class_name: 'User' ,foreign_key: :admin_user_id

  has_many :messages, class_name: "EmailMessage", as: :user

  has_one :profile_image  , as: :imageable , class_name: "Documentation"  , :dependent => :destroy
  accepts_nested_attributes_for :profile_image , allow_destroy: true

  ########################## /RELATIONS ################################

  accepts_nested_attributes_for :profile
  accepts_nested_attributes_for :addresses

  ########################## VALIDATIONS ################################
  validates :name, presence: true
  validates :phone_number, presence: true, :if => Proc.new { |a|  !a.merchant? }
  #head
  validates :phone_number, uniqueness: true, :if => Proc.new { |a|  a.user? &&  a.new_record? }
  #master
  #validates :phone_number, uniqueness: true, if: Proc.new{|obj| obj.new_record?}

  # validates :phone_number, phone: true

  validates :password, confirmation: true
  validates :password, length: { maximum: 20 }
  # validates :email, uniqueness: true, :if => Proc.new { |a|  a.check_user_validation }
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
	has_attached_file :user_image, styles: { medium: "300x300" }, default_url: "<%= asset_path('world.jpg') %>"
	validates_attachment_content_type :user_image, content_type: /\Aimage\/.*\z/
	has_attached_file :front_card_image, styles: { medium: "300x300", thumb: "100x100" }, default_url: "<%= asset_path('world.jpg') %>"
	validates_attachment_content_type :front_card_image, content_type: /\Aimage\/.*\z/
	has_attached_file :back_card_image, styles: { medium: "300x300", thumb: "100x100" }, default_url: "<%= asset_path('world.jpg') %>"
	validates_attachment_content_type :back_card_image, content_type: /\Aimage\/.*\z/
  ############################ /VALIDATIONS ##################################

  scope :friend_with, ->( other ) do
    other = other.id if other.is_a?( User )
    where('(friendships.user_id = users.id AND friendships.friend_id = ?) OR (friendships.user_id = ? AND friendships.friend_id = users.id)', other, other ).includes( :frienships )
  end

  scope :have_gateway, ->(id, key){ where('primary_gateway_id = :id', {id: id, key: User.payment_gateways[key]}) }
  # scope :have_gateway, ->(id, key){ where('primary_gateway_id = :id OR secondary_gateway_id = :id OR ternary_gateway_id = :id OR payment_gateway = :key OR secondary_payment_gateway = :key OR third_payment_gateway = :key', {id: id, key: User.payment_gateways[key]}) }

  scope :wallet_id , ->( other ) do
    wallets.first.id
  end

  scope :authenticate_by_token, -> (token) do
    where(authentication_token: token)
  end

  scope :search_users_with_role, ->(role_params) {
    case role_params
    when I18n.t("role.type.greenbox_user")
      roles = [I18n.t("role.name.admin"), I18n.t("role.name.support_mtrac"), I18n.t("role.name.employee"), I18n.t("role.name.support_customer") ]
    when I18n.t("role.type.ledger_wallets")
      roles = [I18n.t("role.name.gift_card"), I18n.t("role.name.qc"), I18n.t("role.name.support"),nil]
    when I18n.t("role.type.customer")
      roles = I18n.t("role.name.user")
    else
      roles = role_params.singularize
    end
    where(role: roles)
  }

  scope :unarchived_users, -> {where(archived: false)}
  scope :archived_users, -> {where(archived: true)}
  scope :pending_users, -> {where(is_block: true)}
  scope :approved_users, -> {where(is_block: false)}
  scope :active_users, -> {where(is_block: false, archived: false)}
  scope :active_merchant, -> {where(archived: false)}
  scope :complete_profile_users, -> {where(is_profile_completed: [true, false])}

  scope :customers, -> {where(role: I18n.t("role.name.user"))}
  scope :not_customers, -> {where.not(role: I18n.t("role.name.user"))}

  scope :notify_user, -> {where(:is_block => false, role: ["iso", "agent", "affiliate", "merchant"])}
  scope :all_users,  -> {where(role: [:iso, :agent, :affiliate, :user, :gift_card, :qc, :support, nil])}
  scope :search_all_users, -> (key, query) {
    where(key,  wallet_id: "#{query[:wallet_id]}%",name: "%#{query[:name].try(:strip)}%", email: "#{query[:email].try(:strip)}",phone_number: "%#{query[:phone_number].try(:strip)}%",ref_no: "%#{query[:ref_no].try(:strip)}%")
  }

  def send_two_factor_authentication_code(code)
    if self.two_step_verification
      # TextsmsWorker.perform_async(self.phone_number, code)
    end
  end

  def attached_locations
    LocationsUsers.where(user_id: self.id, attach: true).pluck(:location_id)
  end

  def attached_other_location
    LocationsUsers.where(user_id: self.id, attach: false).pluck(:location_id)
  end

  def authenticate_otp(code)
    if self.direct_otp == code
      return true
    else
      return false
    end
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      # active_users.where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      active_users.where(["lower(username) = :value OR lower(email) = :value OR phone_number = :value" , { :value => login.downcase }]).where.not(role: [nil, 'user']).first
    elsif conditions.has_key?(:phone_number) 
      active_users.where(["phone_number = :value", { :value => conditions[:phone_number] }]).where.not(role: [nil, 'user']).first
    elsif email = conditions.delete(:email)
      active_users.where(["lower(email) = :value", { :value => email.downcase }]).where.not(role: [nil, 'user']).first
    elsif conditions.has_key?(:username)
      active_users.where.not(role:'user').first
    end
  end

  def check_for_archive
    checking_user = User.where(phone_number: self.phone_number)
    return true if checking_user.blank? # for new user
    if checking_user.count == 1         # creating user with archived user phone
      if checking_user.first.archived?
        return false
      else
        return true
      end
    elsif checking_user.count > 1       # updating user with archived user phone
      arch = []
      checking_user.each do |a|
        if a.archived
          arch << true
        end
      end
      count = checking_user.count - arch.count
      if count == 1
        return false
      else
        return true
      end

    end
  end

  def set_ref_no
    user = self
    if user.user?
      user.update(ref_no: "C-#{user.id}")
    elsif user.agent?
      user.update(ref_no: "A-#{user.id}")
    # elsif role == "partner"
    #   user.update(ref_no: "CP-#{id}")
    elsif user.affiliate?
      user.update(ref_no: "AF-#{user.id}")
    elsif user.iso?
      user.update(ref_no: "ISO-#{user.id}")
    elsif user.merchant?
      user.update(ref_no: "M-#{user.id}")
    end
  end

  def send_notification_to_password_change
    # Email will send to user after 5 month to change your password
    UserMailer.warning_email_to_change_password(self).deliver_later
  end

  def friend_with?(other)
    User.where( id: id ).friend_with( other ).any?
  end

  def get_role_name
    if parent_merchant.present?
      "Sub Merchant"
    else
      role.capitalize
    end
  end

  def wallet_id
    wallets = self.wallets
    wallets.count > 0 ? wallets.first.id : nil
  end

  def regenerate_token
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    self.authentication_token = token
  end

  def regenerate_oauth_code
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    self.oauth_code = token
  end

  def have_admin_access?
    self.admin? || self.support? || self.support_mtrac? || self.employee? || self.support_customer?
  end

  def MERCHANT?
    self.merchant?
  end

  def ISO?
    self.iso?
  end

  def AGENT?
    self.agent?
  end

  def PARTNER?
    self.partner?
  end

  def AFFILIATE?
    self.affiliate?
  end

  def SUBMERCHANT?
    parent_merchant.present?
  end

  def alias
    return self.role.blank? ? "#{self.id}" : "#{I18n.t("role.alias.#{self.try(:role)}")}#{self.id}"
  end

  def update_wallets_name
    if self.iso? || self.affiliate? || self.agent?
      self.wallets.primary.update(name: self.name) if self.name.present?
    end
  end

  def update_bank_account
    if self.iso? || self.affiliate? || self.agent?
      if self.try(:[],'system_fee').try(:[],'bank_account').present? && self['system_fee']['bank_account'].length < 10
        bank_acc=AESCrypt.encrypt("#{self.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", self['system_fee']['bank_account'])
        self.system_fee['bank_account']= bank_acc
        self.save
      end
    end
  end

  def bank_account
    return self['system_fee']['bank_account']
  end

  def get_locations
    if self.merchant_id.present?
      #if self.try(:permission).admin?
        #parent_user = User.find_by(id: self.merchant_id)
        Location.where(id: self.wallets.primary.pluck(:location_id).compact)
        # Location.where(merchant_id: self.merchant_id)
      #end
    else
      Location.where(merchant_id: self.id)
    end
  end

  def self.get_gateway_details(payment_gateway)
    return {account_processing_limit: payment_gateway.try(:account_processing_limit), transaction_fee: payment_gateway.try(:transaction_fee), charge_back_fee: payment_gateway.try(:charge_back_fee), retrieval_fee: payment_gateway.try(:retrieval_fee), per_transaction_fee: payment_gateway.try(:per_transaction_fee), reserve_money_fee: payment_gateway.try(:reserve_money), reserve_money_days: payment_gateway.try(:reserve_money_days), monthly_service_fee: payment_gateway.try(:monthly_service_fee), misc_fee: payment_gateway.try(:misc_fee), misc_fee_dollars: payment_gateway.try(:misc_fee_dollars)}
  end

  #after using this there would be no need of making seperate methods for each stripe method
  def issue_with_stripe_new(amount, wallet_id,merchant_wallet, type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type = nil, bpay_reference = nil,card = nil,gateway=nil,key=nil,charge_id=nil,payment_gateway=nil, privacy_fee = nil, sales_info = nil,flag=nil)
    tr_type = get_transaction_type(type, sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    tx = SequenceLib.issue(wallet_id, amount, merchant_wallet,type, fee,nil, payment_gateway.present? ? payment_gateway.key : gateway, nil,last4, merchant_name,bpay_reference, user_info, ip, nil,ledger,previous_issue,release_days,tip,sales_info, location_fee, api_type, tr_type.second,nil,card,key,payment_gateway.present? ? payment_gateway : nil, privacy_fee, payment_gateway.present? ? payment_gateway.name : nil, gateway_fee_details,flag)
    if tx.present? && tx.id.present? || tx.size > 0
      order_bank = charge_id[:order_bank]
      # OrderBank.create(merchant_id: order_bank.merchant_id,user_id: order_bank.user_id,bank_type: "Quickcard",card_type: nil,card_sub_type:nil,status: "approve",amount: order_bank.amount,transaction_info: order_bank.transaction_info)
      charge = charge_id[:charge][:charge]
      charge.capture unless charge.captured #charge capture going to true on stripe
      if charge.captured
        tx = {id: tx.actions.first.id,transaction_id: charge_id[:response] ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id") ,gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
      end
    else
      tx = nil
    end
    return tx
  end

  def issue_with_elavon(amount, wallet_id, qc_id,type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type=nil, bpay_reference = nil,card_type = nil,gateway=nil,key=nil,approval_code = nil, charge_id=nil,payment_gateway=nil, privacy_fee = nil, sales_info = nil,flag=nil)
    tr_type = get_transaction_type(type,sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = payment_gateway.present? ? payment_gateway.key : "converge"
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card_type,nil,payment_gateway.present? ? payment_gateway : nil, privacy_fee, payment_gateway.present? ? payment_gateway.name : nil, gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, approval_code: approval_code, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"),card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
    end
    return tx
  end
  def issue_with_mentom_pay(amount, wallet_id, qc_id,type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type=nil, bpay_reference = nil,card_type = nil,gateway=nil,key=nil,approval_code = nil, charge_id=nil,payment_gateway=nil, privacy_fee = nil, sales_info = nil,flag=nil)
    tr_type = get_transaction_type(type,sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = payment_gateway.present? ? payment_gateway.key : "mentom_pay"
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card_type,nil,payment_gateway.present? ? payment_gateway : nil, privacy_fee, payment_gateway.present? ? payment_gateway.name : nil, gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, approval_code: approval_code, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"),card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
    end
    return tx
  end

  def issue_with_totalpay(amount, wallet_id, qc_id,type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type=nil, bpay_reference = nil,card_type = nil,gateway=nil,key=nil,approval_code = nil, charge_id=nil,payment_gateway=nil, privacy_fee = nil, sales_info = nil,flag=nil,descriptor=nil)
    tr_type = get_transaction_type(type,sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = descriptor.present? ? descriptor : "Total Pay"
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card_type,nil,payment_gateway.present? ? payment_gateway : nil, privacy_fee, descriptor.present? ? descriptor : payment_gateway.try(:key), gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, approval_code: approval_code, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"),card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
    end
    return tx
  end

  def issue_with_knox(amount, wallet_id, qc_id,type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type=nil, bpay_reference = nil,card_type = nil,gateway=nil,key=nil,approval_code = nil, charge_id=nil,payment_gateway=nil, privacy_fee = nil, sales_info = nil,flag=nil,descriptor=nil)
    tr_type = get_transaction_type(type,sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = descriptor.present? ? descriptor : "knox_payments"
    # gateway = payment_gateway.present? ? payment_gateway.key : "knox_payments"
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card_type,nil,payment_gateway.present? ? payment_gateway : nil, privacy_fee, descriptor.present? ? descriptor : payment_gateway.try(:key), gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, approval_code: approval_code, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"),card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
    end
    return tx
  end

  def issue_with_bolt_pay(amount, wallet_id, qc_id,type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type=nil, bpay_reference = nil,card_type = nil,gateway=nil,key=nil, charge_id=nil,payment_gateway=nil, privacy_fee = nil, sales_info = nil, flag=nil)
    tr_type = get_transaction_type(type,sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = payment_gateway.present? ? payment_gateway.key : TypesEnumLib::GatewayType::BoltPay
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card_type,nil, payment_gateway, privacy_fee, payment_gateway.try(:name), gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
    end
    return tx
  end

  def issue_with_iCanPay(amount, wallet_id, qc_id,type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue = nil, sub_type=nil, bpay_reference = nil,card_type = nil,gateway=nil,key=nil, charge_id=nil,payment_gateway = nil,privacy_fee = nil,sales_info = nil,flag=nil,descriptor=nil)
    tr_type = get_transaction_type(type,sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = descriptor.present? ? descriptor : payment_gateway.present? ? payment_gateway.key : TypesEnumLib::GatewayType::ICanPay
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card_type,nil,payment_gateway,privacy_fee, descriptor.present? ? descriptor : payment_gateway.try(:name), gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details ,complete_trans: tx.actions} # , approval_code: approval_code
    end
    return tx
  end

  def issue_with_payeezy(amount, wallet_id,qc_id, type, fee, last4 = nil, merchant_name = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue=nil, sub_type=nil, bpay_reference = nil, card = nil)
    tr_type = get_transaction_type(type, sub_type)
    tx = SequenceLib.issue(wallet_id, amount, qc_id,type, fee,nil,'payeezy', nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days,tip, nil, location_fee, api_type ,tr_type.second,nil, card)
    if tx.present?
      tx = {id: tx.id,transaction_id: tx.actions.first.id ,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.type , destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id}
    end
    return tx
  end

  def issue_with_bridge_pay(amount, wallet_id,qc_id, type, fee, last4 = nil, merchant_name = nil, bpay_reference = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue=nil,sub_type=nil, card=nil)
    tr_type = get_transaction_type(type, sub_type)
    tx = SequenceLib.issue(wallet_id, amount, qc_id,tr_type.first, fee,nil,'Bridge Pay', nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip,nil,location_fee, api_type, tr_type.second,nil,card)
    if tx.present?
      tx = {id: tx.id,transaction_id: tx.actions.first.id , timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.type , destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id}
    end
    return tx
  end

  def issue_with_fluid_pay(amount, wallet_id,qc_id, type, fee, last4 = nil, merchant_name = nil, bpay_reference = nil, ip = nil, ledger = nil, release_days = nil,user_info = nil, location_fee = nil, api_type = nil, tip = nil,previous_issue=nil, sub_type=nil,card = nil,charge_id=nil, payment_gateway = nil, privacy_fee = nil, sales_info = nil,flag=nil)
    tr_type = get_transaction_type(type, sub_type)
    gateway_fee_details = User.get_gateway_details(payment_gateway)
    gateway = payment_gateway.present? ? payment_gateway.key : "Fluid Pay"
    tx = SequenceLib.issue(wallet_id, amount, qc_id,tr_type.first, fee,nil,gateway, nil, last4, merchant_name, bpay_reference,user_info, ip, nil,ledger,previous_issue,release_days, tip, sales_info, location_fee, api_type, tr_type.second,nil,card, nil, payment_gateway, privacy_fee, payment_gateway.try(:name), gateway_fee_details,flag)
    if tx.present?
      tx = {id: tx.actions.first.id,transaction_id: charge_id , timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.tags["type"], sub_type: tx.actions.first.tags["sub_type"] , destination: tx.actions.first.destination_account_id, issue_amount: tx.actions.first.amount.to_f/100 , source: tx.actions.first.source_account_id, tags: tx.actions.first.tags, fee: tx.actions.first.tags["fee"], privacy_fee: tx.actions.first.tags.try(:[], "privacy_fee"), tip: tx.actions.first.tags.try(:[], "tip"), reserve_money: tx.actions.first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"),card_id: tx.actions.first.tags.try(:[], "previous_issue").try(:[],"tags").try(:[], "card").try(:[], "id") || tx.actions.first.tags.try(:[], "card").try(:[], "id"),gateway_fee_details: gateway_fee_details,complete_trans: tx.actions}
    end
    return tx
  end

  def issue(amount, wallet_id)
    tx = SequenceLib.issue(wallet_id, amount, wallet_id,'Issue', 0, nil,nil,nil)
    if tx.present?
      tx = {id: tx.id,timestamp: tx.timestamp, asset: tx.actions.first.flavor_id, type: tx.actions.first.type , destination: tx.actions.first.destination_account_id, amount: tx.actions.first.amount , source: tx.actions.first.source_account_id}
    end
    return tx
  end

  def withdraw(amount, wallet_id)
    tx = SequenceLib.retire(amount,wallet_id,'withdraw', nil, nil, nil, nil)
    return tx
  end

  def create_wallet(name, id ,type, user = nil)
    begin
      creating_user_for_merchant_iso_agent_aff = User.where(phone_number: self.phone_number)
      user_id = self.merchant? && self.merchant_id.nil? ? nil : self.id
      if creating_user_for_merchant_iso_agent_aff.count > 1
        wallet = Wallet.create(name: name, wallet_type: type,user_id: user_id)
        self.wallets << wallet
      else
        wallet = Wallet.create(name: name, wallet_type: type,user_id: user_id)
        self.wallets << wallet
      end
      return wallet
    rescue Exception => exc
      p "---Exception Occured While Generating QR Image---", exc
      SlackService.notify("*Wallet Creation ERROR * \n Error: \`\`\` #{exc} \`\`\` User: \`\`\` name: #{user.try(:name)} id: #{user.try(:id)} email: #{user.try(:email)} \`\`\`")
    end
  end

  def send_welcome_email(role, from=nil)
    user = User.find_by(id:self.id)
    role = role
    password = random_password
    if from != "resend"
      text = I18n.t('notifications.welcome_notification', name: self.name)
      Notification.notify_user(self,self,self,"welcome notification",nil,text)
    end
    if self.merchant?
      UserMailer.welcome_merchant(self.id, password, "Signup-MERCHANT").deliver_later
      unless from == "resend"
        @location_details=[]
        @iso=[]
        if self.wallets.present?
          wallets=self.wallets.primary
          wallets.each do |wallet|
            wallet_location=wallet.location
            if wallet_location.present? && wallet_location.is_block==false
              @location_details.push(wallet_location.business_name)
              if wallet_location.isos.present?
                iso=wallet_location.isos.first
                @iso.push(iso)
              end
            end
          end
        end
        @iso.each do |iso|
          UserMailer.iso_email(iso,iso.name,self.id,@location_details).deliver_later if iso.present? && iso.email.present?
        end
      end
    elsif self.affiliate_program?
      UserMailer.welcome_affiliate(user,"Welcome to Quick Card",role,password).deliver_later
    else
      UserMailer.welcome_user(user,"Welcome to Quick Card",role,password).deliver_later
    end
    self.update(:password => password,:welcome_user_date => Time.now)
  end
  
  def password_changed(check)
    if check == true
      def send_devise_notification(notification, *args)
        unless(notification == :password_change)
          super
        end
      end
    end
  end

  def clear_denominations(hash)
    @hash = eval(hash)
    @denominations = eval(self.denominations)
    @denominations[1] = @denominations[1] - (@hash[1].nil? ? 0 : @hash[1])
    @denominations[2] = @denominations[2] - (@hash[2].nil? ? 0 : @hash[2])
    @denominations[5] = @denominations[5] - (@hash[5].nil? ? 0 : @hash[5])
    @denominations[10] = @denominations[10] - (@hash[10].nil? ? 0 : @hash[10])
    @denominations[20] = @denominations[20] - (@hash[20].nil? ? 0 : @hash[20])
    @denominations[50] = @denominations[50] - (@hash[50].nil? ? 0 : @hash[50])
    @denominations[100] = @denominations[100] - (@hash[100].nil? ? 0 : @hash[100])
    self.update(denominations: @denominations)
  end

  def update_denominations(hash)
    @hash = eval(hash)
    @denominations = eval(self.denominations)
    @denominations[1] = @denominations[1] + (@hash[1].nil? ? 0 : @hash[1])
    @denominations[2] = @denominations[2] + (@hash[2].nil? ? 0 : @hash[2])
    @denominations[5] = @denominations[5] + (@hash[5].nil? ? 0 : @hash[5])
    @denominations[10] = @denominations[10] + (@hash[10].nil? ? 0 : @hash[10])
    @denominations[20] = @denominations[20] + (@hash[20].nil? ? 0 : @hash[20])
    @denominations[50] = @denominations[50] + (@hash[50].nil? ? 0 : @hash[50])
    @denominations[100] = @denominations[100] + (@hash[100].nil? ? 0 : @hash[100])
    self.update(denominations: @denominations)
  end
#
  # this is overide for password expirable controller
  def need_change_password?
    return false if self.admin?
    super
  end
 # To get Merchant parent status
  def get_merchant_ach_status
    current_merchant_id = self.merchant_id
    b = User.find current_merchant_id
    ach_status_of_parent= b.merchant_ach_status
     ach_status_of_parent
  end

  def update_with_password(params, *options)
    current_password = params.delete(:current_password)

    new_password = params[:password]
    new_password_confirmation = params[:password_confirmation]

    result = if valid_password?(current_password) && new_password.present? && new_password_confirmation.present?
               update(params, *options)
             else
               self.assign_attributes(params, *options)
               self.valid?
               self.errors.add(:current_password, current_password.blank? ? :blank : :equal_to_current_password)
               self.errors.add(:password, new_password.blank? ? :blank : :invalid)
               self.errors.add(:password_confirmation, new_password_confirmation.blank? ? :blank : :invalid)
               false
             end

    clean_up_passwords
    result
  end

  def attributes_changed?
    attrs = ["name", "email", "last_name", "phone_number", "permission_id", "password", "password_confirmation", "first_name", "encrypted_password", "password_changed_at"]
    if (self.saved_changes.keys & attrs).any?
      return true
    else
      return false
    end
  end

  private



  def create_oauth_app
    if ( self.merchant? && self.merchant_id.nil? ) || self.admin_user? 
      app=OauthApp.new
      app.user_id = self.id
      app.key = 'ID' + Digest::SHA1.hexdigest([Time.now, rand].join)
      app.secret = 'SE' + Digest::SHA1.hexdigest([Time.now, rand].join)
      app.token =Digest::SHA1.hexdigest([Time.now, rand].join)
      app.save
    elsif self.affiliate_program?
      app=OauthApp.new
      app.user_id = self.id
      app.save
    end
  end

  def tos_update
    # if self.iso? || self.affiliate? || self.agent?
    #   self.update(tos_checking:true)
    # end
  end

  def qr_create_wallet
    if self.merchant? && self.merchant_id != nil
       wallet= create_wallet(self.name , self.id , 0, self)
    elsif self.email == 'support@quickcard.me'
      wallet = create_wallet(self.name, self.id, 3, self)
      self.update(username: "#{self.id}#{wallet.id}")
    elsif self.email == 'check_escrow@quickcard.me'
      wallet = create_wallet(self.name, self.id, 7)
      self.update(username: "#{self.id}#{wallet.id}")
    elsif !self.merchant? && !self.admin_user?
      wallet = create_wallet(self.name, self.id, 0, self)
      if self.user?
        create_wallet(self.name+" Reward Wallet", self.id, 8, self)
      end
      # self.update(username: "#{self.id}#{wallet.id}")
    else
    end
  end

  def wallet_name(id)
    wallet = Wallet.find_by_id(id) if id.present?
    return wallet.present? ? wallet.name : ''
  end

  def create_setting
    Setting.create(user_id: self.id)
  end

  def send_unlock_instructions
    self.set_reset_password_token
    Thread.current[:unlock_instruction]='true'
    super
  end

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

end
