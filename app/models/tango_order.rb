# == Schema Information
#
# Table name: tango_orders
#
#  id                 :bigint(8)        not null, primary key
#  user_id            :bigint(8)
#  utid               :string
#  amount             :float
#  order_id           :string
#  account_identifier :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  catalog_image      :string
#  checkId            :string
#  description        :text
#  direction          :string
#  name               :string
#  number             :string
#  recipient          :string
#  sender             :string
#  status             :string
#  wallet_id          :integer
#  order_type         :integer
#  account_number     :string
#  routing_number     :string
#  actual_amount      :float
#  check_fee          :float
#  fee_perc           :float
#  gift_card_fee      :float
#  pincode            :string
#  settled            :boolean          default(TRUE)
#  notes              :string
#

class TangoOrder < ApplicationRecord

  include ActionView::Helpers::NumberHelper

  enum order_type: [:check, :invoice, :direct_deposit, :bulk_check, :debit_card_deposit, :ach,:instant_ach, :instant_pay]
  enum status: {"UNPAID" =>  "UNPAID", "IN_PROGRESS" => "IN_PROGRESS", "IN_PROCESS" => "IN_PROCESS", "PAID" => "PAID", "VOID" => "VOID", "FAILED" => "FAILED", "EXPIRED" => "EXPIRED", "PRINTED" => "PRINTED", "CANCELED" => "CANCELED", "PAID_WIRE" => "PAID_WIRE", "PENDING" => "PENDING", "COMPLETE" => "COMPLETE"}
  belongs_to :ach_gateway, optional: true
  belongs_to :user
  belongs_to :wallet
  belongs_to :location, optional: true
  has_one :bulk_check
  belongs_to :tango_transaction, class_name: "Transaction", foreign_key: 'transaction_id', optional: true
  after_create :notify_user
  after_update :status_change_email

  audited
  has_many :activity_logs
  after_save :activ_log

  def activ_log
    audits = self.audits.last

    if audits.present? && audits.audited_changes.present?
        audits.audited_changes.delete("account_number") if audits.audited_changes["account_number"].present?
        audits.audited_changes.delete("card_cvv") if audits.audited_changes["card_cvv"].present?
      if audits.audited_changes["order_type"].present? && audits.audited_changes["order_type"] == "instant_pay"
        audits.audited_changes["order_type"] = "push_to_card"
        audits.audited_changes["send_via"] = "push_to_card"
      end
    end
    activity = self.activity_logs.new
    #modifying_user = User.find(self.user_id)
    activity.activity_type = :tango_order
    #activity.transaction_id = self.id
    activity.user_id = audits.user_id
    message = audits.audited_changes
    activity.note = message
    #activity.tango_order_id = self.id
    activity.respond_with = audits
    activity.ip_address = audits.remote_address
    activity.action = audits.action
    activity.action_type = audits.try(:user).try(:affiliate_program?) ? 'affiliate_program' : nil
    if audits.audited_changes.present? && audits.audited_changes["order_type"].present?
      activity.controller = audits.audited_changes["order_type"]
    else
      if self.order_type == 'instant_pay'
        activity.controller = "push_to_card"
      else
        activity.controller = self.order_type
      end
    end
    activity.save
  end

  scope :pending_checks, ->{where(status: "PENDING")}
  scope :status, -> (status) {where(status: status)}
  scope :order_type, -> (order_type) { where(order_type: order_type) }
  scope :batch_date, -> (batch_date) { where(batch_date: batch_date) }
  scope :ach_international, -> (ach_international) {where(ach_international:ach_international)}
  scope :ach_afp, -> (ach_afp) {where(ach_afp:ach_afp)}
  scope :wallet_id, -> (wallet_id) {where(wallet_id: wallet_id)}
  scope :not_wallet_id, -> (wallet_id) {where.not(wallet_id: wallet_id)}
  scope :withdraw_batches, -> {
    Rails.cache.fetch("withdraw_batches", expires_in: 10.minutes) do
      where.not(order_type: nil,batch_date: nil).order(batch_date: :desc).group_by_day(day_start: 22) {|u| u.batch_date }
    end
  }
  scope :batch_list, -> (order_type, batch_date, ach_international, ach_afp) {
    includes(:ach_gateway,:user).order_type(order_type).batch_date(batch_date).ach_international(ach_international).ach_afp(ach_afp)
  }
  scope :batch_list_with_ransack, -> (order_type, ach_international, ransack_params, ach_afp) {
    includes(:ach_gateway).order_type(order_type).ach_international(ach_international).ach_afp(ach_afp).order(batch_date: :desc).ransack(ransack_params).result
  }
  scope :checks_list, -> (order_types) {
    includes(:user).where(order_type: order_types).where.not(batch_date: nil).group_by_day(time_zone: "Pacific Time (US & Canada)", day_start: 14) {|u| u.batch_date }
  }
  scope :checks_list_with_ransack, -> (order_types) {
    includes(:user).where(order_type: order_types).where.not(batch_date: nil).ransack(params[:q].try(:merge, m: 'or')).result.group_by_day(time_zone: "Pacific Time (US & Canada)", day_start: 14) {|u| u.batch_date }
  }
  scope :order_by, -> () {
    order('tango_orders.created_at DESC')
  }
  scope :search_by_status, lambda {|search|
    status_array = search.select{|k,v| v=="1"}.keys.map{|a| a.upcase} if search.present?
    if status_array.present?
      if status_array.include?("ALL") && status_array.count == 1
        where(id: TangoOrder.all)
      else
        status_array << "IN_PROGRESS" if status_array.include? "IN_PROCESS"
        search.present? ? where("status IN (?)",status_array) : where(id: TangoOrder.all)
      end
    end
  }
  scope :batch_date_not_nil, -> {where.not(batch_date: nil)}
  scope :orders_for_today, -> (wallet_id, user_id, order_type) {
    where(wallet_id: wallet_id,user_id: user_id,order_type: order_type,created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
  }
  def notify_user
    if self.order_type=="check" || self.order_type=="invoice"
      user = User.where(email: "admin@quickcard.me").first
      Notification.notify_user(user, self.user, self, self.order_type)
    elsif self.order_type=='instant_ach'
      user = User.where(email: "admin@quickcard.me").first
      Notification.notify_user(user, self.user, self, self.order_type)
    end
  end

  def transaction
    Transaction.find_by_id(self.transaction_id)
  end

  def pending?
    self.status.downcase == 'pending'
  end

  def void?
    self.status.downcase == 'void'
  end

  def status_change_email
    current_user = User.try(:current)
    if self.saved_changes[:status].present?
      if self.status== "PAID_WIRE" || self.status== "PENDING" || self.status== "pending" || self.status == "IN_PROGRESS" || self.status == "IN_PROCESS" || self.status == "FAILED" || self.status == "VOID" || (self.status == "UNPAID" && self.check? ) || (self.status == "PAID" && self.instant_pay? )
        # if current_user.try(:role) != "merchant"
          sleep 0.5
          if self.order_type == "instant_pay"
            check_user = self.user
            noti_amount = number_to_currency(number_with_precision(self.actual_amount, :precision => 2, delimiter: ','))
            if self.status == "VOID"
              text = I18n.t('notifications.p2c_voided_notification', amount: noti_amount)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user,check_user,self,"P2C status voided",nil,text)
                Notification.notify_user(main_merchant,main_merchant,self,"P2C status voided",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user,check_user,self,"P2C status voided",nil,text)
              end
            elsif self.status == "FAILED"
              text = I18n.t('notifications.p2c_failed_notification', amount: noti_amount)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user,check_user,self,"P2C status Failed",nil,text)
                Notification.notify_user(main_merchant,main_merchant,self,"P2C status Failed",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user,check_user,self,"P2C status Failed",nil,text)
              end
            elsif self.status == "PAID" || self.status == "PAID_WIRE"
              text = I18n.t('notifications.p2c_paid_notification', amount: noti_amount)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user ,check_user ,self,"ACH Status In-process",nil,text)
                Notification.notify_user(main_merchant ,main_merchant ,self,"ACH Status In-process",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user ,check_user ,self,"ACH Status In-process",nil,text)
              end
            else
              text = I18n.t('notifications.withdraw_status', name: check_user.name, status: self.status)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user ,check_user ,self,"Withdraw Status Changed",nil,text)
                Notification.notify_user(main_merchant ,main_merchant ,self,"Withdraw Status Changed",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user ,check_user ,self,"Withdraw Status Changed",nil,text)
              end
            end
            UserMailer.p2c_status_change_email(self.id,self.status, self.notes).deliver_later if self.oauth_blocked?
          elsif self.order_type == "check"
            check_user = self.user
            text = I18n.t('notifications.withdraw_status', name: check_user.name, status: self.status)
            if check_user.merchant_id.present?
              main_merchant = User.find_by(id: check_user.merchant_id)
              Notification.notify_user(check_user ,check_user ,self,"Withdraw Status Changed",nil,text)
              Notification.notify_user(main_merchant ,main_merchant ,self,"Withdraw Status Changed",nil,text)
            elsif check_user.merchant_id == nil
              Notification.notify_user(check_user ,check_user ,self,"Withdraw Status Changed",nil,text)
            end
            UserMailer.check_status_change_email(self.id,self.status, self.notes).deliver_later
          else
            check_user = self.user
            noti_amount = number_to_currency(number_with_precision(self.actual_amount, :precision => 2, delimiter: ','))
            if self.status == "Void" || self.status == "VOID"
              text = I18n.t('notifications.ach_voided_notification', id: noti_amount)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user ,check_user ,self,"ACH Status Void",nil,text)
                Notification.notify_user(main_merchant ,main_merchant ,self,"ACH Status Void",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user ,check_user ,self,"ACH Status Void",nil,text)
              end
            elsif self.status == "Failed" || self.status == "FAILED"
              text = I18n.t('notifications.ach_failed_notification', id: noti_amount)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user ,check_user ,self,"ACH Status Failed",nil,text)
                Notification.notify_user(main_merchant ,main_merchant ,self,"ACH Status Failed",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user ,check_user ,self,"ACH Status Failed",nil,text)
              end
            elsif self.status == "IN_PROGRESS" || self.status == "IN_PROCESS"
              text = I18n.t('notifications.ach_in_progress_notification', id: noti_amount)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user ,check_user ,self,"ACH Status In-process",nil,text)
                Notification.notify_user(main_merchant ,main_merchant ,self,"ACH Status In-process",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user ,check_user ,self,"ACH Status In-process",nil,text)
              end
            else
              text = I18n.t('notifications.withdraw_status', name: check_user.name, status: self.status)
              if check_user.merchant_id.present?
                main_merchant = User.find_by(id: check_user.merchant_id)
                Notification.notify_user(check_user ,check_user ,self,"Withdraw Status Changed",nil,text)
                Notification.notify_user(main_merchant ,main_merchant ,self,"Withdraw Status Changed",nil,text)
              elsif check_user.merchant_id == nil
                Notification.notify_user(check_user ,check_user ,self,"ACH Status In-process",nil,text)
              end
            end
            UserMailer.ach_status_change_email(self.id,self.status, self.notes).deliver_later if self.oauth_blocked?
          end
        # end
      end
    end
  end

  def oauth_blocked?
    !!self.user&.oauth_apps&.first&.is_block == false
  end

  ransacker :total_fee do |r|
    Arel::Nodes::SqlLiteral.new("(check_fee+fee_perc)")
  end

  def deduct_fee(fee, amount)
    deduct = amount.to_f * (fee.to_f/100)
    return number_with_precision(deduct, precision: 2).to_f
  end

  def get_location_fee_for_bulk_checks(amount, wallet, checks = nil)
    location = wallet.location
    location_fee = location.fees.buy_rate.first if location.present?
    redeem_fee = location_fee.redeem_fee.to_f if location_fee.present?
    check_fee = location_fee.send_check.to_f if location_fee.present?
    check_fee = check_fee * checks if check_fee.present?
    fee_perc = deduct_fee(redeem_fee.to_f, amount)
    amount_sum = check_fee + fee_perc.to_f
    number_with_precision(amount_sum, :precision => 2, :delimiter => ',')
  end

  # validates :name, presence: true


  # def self.update_all_check_status
  #   @checks = TangoOrder.where(order_type: "check", settled: false).or(TangoOrder.where(order_type: "check", settled: false))
  #   if @checks.present?
  #     @checks.each do |check|
  #       result = get_cheq(check.checkId)
  #
  #       if result != false
  #         if result["status"] != check.status
  #           # if result["status"] != "PAID"
  #           validation(check,result)
  #           # end
  #         end
  #       end
  #     end
  #   end
  #   puts "1"
  # end
  #
  # def validation(check, result)
  #   check_status = result["status"]
  #   app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
  #   wallet = Wallet.where(id: check.wallet_id).first if check.wallet_id.present?
  #   if wallet.present?
  #     check_info = {
  #         name: check.name,
  #         check_email: check.recipient,
  #         check_id: check.checkId,
  #         check_number: check.number
  #     }
  #     if check.status == "UNPAID"
  #       if check_status == "PAID" && check.status == "UNPAID"
  #         check.update(status: result["status"], settled: true)
  #       elsif check_status == "FAILED" && check.status == "UNPAID"
  #         issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
  #         check.update(status: result["status"], settled: true)
  #       elsif check_status == "VOID" && check.status == "UNPAID"
  #         issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
  #         check.update(status: result["status"], settled: true)
  #       elsif check_status == "IN_PROCESS" && check.status == "UNPAID"
  #         check.update(status: result["status"], settled: false)
  #       elsif check_status == 'UNPAID' && check.status == 'UNPAID'
  #         check.update(status: result["status"], settled: false)
  #       end
  #     elsif check.status == "IN_PROCESS"
  #       if check_status == "PAID" && check.status == "IN_PROCESS"
  #         check.update(status: result["status"], settled: true)
  #       elsif check_status == "FAILED" && check.status == "IN_PROCESS"
  #         issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
  #         check.update(status: result["status"], settled: true)
  #       elsif check_status == "VOID" && check.status == "IN_PROCESS"
  #         issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
  #         check.update(status: result["status"], settled: true)
  #       elsif check_status == "IN_PROCESS" && check.status == "IN_PROCESS"
  #         check.update(status: result["status"], settled: false)
  #       elsif check_status == 'UNPAID' && check.status == 'IN_PROCESS'
  #         check.update(status: result["status"], settled: false)
  #       end
  #     end
  #     # if check_status == "RETURNED"
  #     #   issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
  #     # elsif check_status == "FAILED"
  #     #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
  #     # elsif check_status == "VOID"
  #     #   # if check.status != 'VOID'
  #     #   issue_checkbook_amount(check.amount, wallet,'send_check',app_config.stringValue, 0, nil, check_info)
  #     #   # end
  #     # elsif check_status == "EXPIRED"
  #     #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0, nil, check_info)
  #     # else
  #     #   puts "no if"
  #     # end
  #     puts "2"
  #   end
  #   puts "3"
  # end
  #
  #
  # def get_cheq(cheque)
  #   puts "4"
  #   begin
  #     url =URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{cheque}")
  #     http = Net::HTTP.new(url.host,url.port)
  #     http.use_ssl =true
  #     http.verify_mode =OpenSSL::SSL::VERIFY_PEER
  #     request= Net::HTTP::Get.new(url)
  #     request["Authorization"]=ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
  #     request["Content-Type"]='application/json'
  #     response =http.request(request)
  #     case response
  #       when Net::HTTPSuccess
  #         cheque = JSON.parse(response.body)
  #         return cheque
  #       when Net::HTTPUnauthorized
  #         return false
  #       when Net::HTTPNotFound
  #         return false
  #       when Net::HTTPServerError
  #         return false
  #       else
  #         return false
  #     end
  #   rescue => ex
  #     p"-----------EXCEPTION HANDLED #{ex.message}"
  #   end
  #   puts "5"
  # end



end
