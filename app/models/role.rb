# == Schema Information
#
# Table name: roles
#
#  id                :bigint(8)        not null, primary key
#  title             :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  charge_percentage :string
#

class Role < ApplicationRecord
  validates :title , uniqueness: true
  validates :charge_percentage,presence:true, numericality: {only_float: true}
end
