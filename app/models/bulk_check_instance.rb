class BulkCheckInstance < ApplicationRecord
  has_many :bulk_checks
  belongs_to :user
  belongs_to :user_wallet ,:class_name => "Wallet" , foreign_key: :wallet , optional: true
  extend V2::Merchant::ChecksHelper

  	has_many :tango_orders , through: :bulk_checks

    scope :search_by_status, lambda {|search|
	    status_array = search.select{|k,v| v=="1"}.keys.map{|a| a.upcase} if search.present?
	    if status_array.present?
				if !status_array.include?("NO_SELECT")
					# if status_array.include?("ALL") && status_array.count == 1
					#   where(id: BulkCheckInstance.all)
					if status_array.include?("DONE")
						# select{|a| BulkCheckInstance.get_bulk_status(a) == "Done" }
						joins(:bulk_checks).where("bulk_checks.is_processed = ? ",true).where( " bulk_checks.status IN (?) " ,["Success","PENDING"] )
					elsif status_array.include?("PROCESSING")
						# select{|a| BulkCheckInstance.get_bulk_status(a) == "Processing" }
						where(start_process: true).joins(:bulk_checks).where( " bulk_checks.status  = ? " ,"Ready" )
					elsif status_array.include?("INCOMPLETE")
						# select{|a| BulkCheckInstance.get_bulk_status(a) == "Incomplete" }
						where(start_process: true).joins(:bulk_checks).where( " bulk_checks.status  = ? " ,"Unauthorized" )
          end
				else
					where.not(id: BulkCheckInstance.all)
				end
	    end
	}
end