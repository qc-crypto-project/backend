class AchGateway < ApplicationRecord
  has_many :transactions
  has_many :tango_orders
  scope :unarchived_gateways, ->{where(archive: false)}
  scope :archive_gateways, ->{where(archive: true)}
  scope :not_ftp, ->{where.not(ach_type: "ftp")}
  validates :bank_name, presence: true, :uniqueness => {:scope => :gateway_type}
  validates :descriptor, presence: true, :uniqueness => {:scope => :gateway_type}
  enum gateway_type: [:ach, :push_to_card,:check]
  enum status: [:active, :inactive]
  enum ach_type: {'direct_integration' => "direct_integration", 'manual_integration' => "manual_integration"}

end
