# == Schema Information
#
# Table name: oauth_apps
#
#  id                :bigint(8)        not null, primary key
#  name              :string
#  description       :text
#  website           :string
#  tos               :string
#  privacy_url       :string
#  redirect_uri      :string
#  is_block          :boolean          default(FALSE)
#  key               :string
#  secret            :string
#  token             :string
#  code              :string
#  organization_name :string
#  organization_web  :string
#  user_id           :bigint(8)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  location_id       :integer
#

class OauthApp < ApplicationRecord
  audited max_audits: 10
  belongs_to :user
  has_many :wallets, dependent: :destroy
  belongs_to :location , optional: true
end
