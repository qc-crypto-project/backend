# == Schema Information
#
# Table name: checks
#
#  id          :bigint(8)        not null, primary key
#  description :text
#  amount      :float
#  direction   :string
#  checkId     :string
#  status      :string
#  recipient   :string
#  sender      :string
#  name        :string
#  number      :integer
#  image_uri   :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  wallet_id   :integer
#

class Check < ApplicationRecord
  belongs_to :user , optional: true

  validates :name, presence: true,  length: { minimum: 3 }
  validates :recipient, email: true
  validates :amount, numericality: true, presence: true
  validates :description, presence: true
  validates :routing_number, length: { is: 9 }, allow_blank: true
  validates :account_number, length: { in: 10..12 }, allow_blank: true
end

