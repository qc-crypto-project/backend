# == Schema Information
#
# Table name: comments
#
#  id         :bigint(8)        not null, primary key
#  body       :text
#  user_id    :bigint(8)
#  ticket_id  :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :ticket
  has_many :images
  after_create :notify_user

  def notify_user
    Notification.notify_user(self.ticket.user, self.user, self)
  end
end
