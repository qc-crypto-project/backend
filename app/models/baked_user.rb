class BakedUser < ApplicationRecord

  belongs_to :location
  belongs_to :user
  belongs_to :wallet
  belongs_to :baked_location, class_name: "Location", foreign_key: :location_id

  enum split_type: {'baked_profit_split' => "baked_profit_split", 'gbox_profit_split' => "gbox_profit_split", 'net_profit_split' => "net_profit_split", 'sub_merchant_split' => "sub_merchant_split", 'dispensary_credit_split' => "dispensary_credit_split", "dispensary_debit_split" => "dispensary_debit_split", "locations_users" => "locations_users"}

  validates :location_id, presence: true

  # audited





end
