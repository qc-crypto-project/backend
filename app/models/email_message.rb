class EmailMessage < ApplicationRecord
   self.table_name = "ahoy_messages"
    # serialize :properties, JSON
  belongs_to :user, polymorphic: true, optional: true
  belongs_to :email_notification, optional: true
   enum status: {:processed => "processed",:dropped => "dropped",:deferred => "deferred",:delivered => "delivered",:bounce => "bounce"}
end