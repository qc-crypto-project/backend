class Chat < ApplicationRecord
  has_many :messages
  belongs_to :customer,class_name: "User",foreign_key: "customer_id"
  belongs_to :merchant,class_name: "User",foreign_key: "merchant_id"
  belongs_to :chat_transaction,class_name: "Transaction",foreign_key: "transaction_id"


end
