class OrderBank < ApplicationRecord
  belongs_to :payment_gateway, optional: true
  belongs_to :load_balancer, optional: true
  after_create :notify_declined_bank
  belongs_to :charge_transactions, class_name: "Transaction", foreign_key: :transaction_id, optional: true

  def notify_declined_bank
    # if self.status == "decline"
    #   bank_ty = self.bank_type
    #   gateway_id = self.payment_gateway_id
    #   tr = OrderBank.where(bank_type: bank_ty).last(10)
    #   count_dec = tr.select{|u| u.status == "decline"}.count
    #   count_apr = tr.select{|u| u.status == "approve"}.count
    #   if count_dec == 10 && count_apr == 0
    #     payment_gateway = PaymentGateway.find_by_id(gateway_id)
    #     payment_gateway = PaymentGateway.where(key: bank_ty).first if payment_gateway.blank?
    #     pg_name = payment_gateway.try(:name)
    #     if payment_gateway.present? && payment_gateway.try(:type) != "bolt_pay"
    #       body = "Warning! The Payment Gateway '#{pg_name}' of bank type '#{self.bank_type}' has declined last #{count_dec} consecutive transactions. Merchant IDs: #{tr.pluck(:merchant_id).uniq.compact.map{|i| "M-#{i}"}}"
    #
    #       SlackService.notify(body, ENV["NOTIFY_BANK_DECLINES"])
    #
    #       TextsmsWorker.perform_async("1#{ENV["PHONE_NUMBER_FOR_PAY_INVOICE2"]}", body) if ENV["PHONE_NUMBER_FOR_PAY_INVOICE2"].present?
    #       TextsmsWorker.perform_async("1#{ENV["PHONE_NUMBER_FOR_PAY_INVOICE1"]}", body) if ENV["PHONE_NUMBER_FOR_PAY_INVOICE1"].present?
    #     end
    #   end
    # end
  end
end