class QcFile < ApplicationRecord
  has_attached_file :image,{ validate_media_type: false }
  validates_attachment_content_type :image, :content_type => ["text/csv"]

  enum status: [:report, :export, :charge_back, :gateway, :sale, :merchant, :wallet,:bulk_cbk, :fee_details, :fee_summary, :merchant_audit, :bulk_cards, :withdrawal, :iso_profit_split, :vip_cards, :lost_bulk_cbk, :cbk_detail]
end
