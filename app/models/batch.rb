# == Schema Information
#
# Table name: batches
#
#  id                 :bigint(8)        not null, primary key
#  close_date         :date
#  close_time         :time
#  total_transactions :integer
#  total_sales        :float
#  is_closed          :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :bigint(8)
#  wallet_id          :bigint(8)
#  batch_type         :integer
#  days               :integer          default(0)
#

class Batch < ApplicationRecord
  enum batch_type: [:primary, :reserve, :buy_rate_commission,:qr_scan]
  enum batch_risk: [:high, :low]

  scope :created_today, -> {where(created_at: DateTime.now.get_beginning_of_day("Pacific Time (US & Canada)")..DateTime.now.get_end_of_day("Pacific Time (US & Canada)"))}

  belongs_to :user
  belongs_to :wallet, optional: true
  belongs_to :location, optional: true

end
