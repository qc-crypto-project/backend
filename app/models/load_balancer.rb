class LoadBalancer < ApplicationRecord
  has_many :load_balancer_rules
  has_many :payment_gateways
  has_many :transactions
  has_many :block_transactions
  has_many :order_banks
  # Scopes
  scope :active_with_name_like, ->(name){ where("active = ? and name ILIKE ?", true,  "%#{name.strip}%").order('created_at DESC') }
  scope :inactive_with_name_like, ->(name){ where("active = ? and name ILIKE ?", false,  "%#{name.strip}%").order('created_at DESC') }
  after_save :loadbalancer_audit
  attr_accessor :current_id, :request, :action_name
  def loadbalancer_audit
    if  self.previous_changes.present?
      note={}
      request = self.request
      user_id = self.current_id
      action_name = self.action_name
      activity={load_balancer_id:self.id,date:Time.now}
      activity_type = "load_balancer"
      note= self.previous_changes.first
      ActivityLog.create(params:activity,activity_type:activity_type,note:note,user_id:user_id,host:request.env["SERVER_NAME"],url: request.url,browser: request.env['HTTP_USER_AGENT'],ip_address: request.ip, action:action_name ) if note.present? && request.present?
    end
  end

end
