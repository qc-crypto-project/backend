# == Schema Information
#
# Table name: reasons
#
#  id          :bigint(8)        not null, primary key
#  title       :string           default("")
#  reason_type :string           default("")
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Reason < ApplicationRecord
end
