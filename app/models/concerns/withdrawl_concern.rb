module WithdrawlConcern
  extend ActiveSupport::Concern

  class TransactionError < StandardError; end
  class ActivityLogSuccess < StandardError; end
  class ActivityLogFailure < StandardError; end

  included do
    # Define custom
    def withdrawl?
      withdrawl_types = [I18n.t('types.withdrawl.ach'),I18n.t('types.withdrawl.ach_deposit'), I18n.t('types.withdrawl.void_ach'),"instant_pay", I18n.t('types.withdrawl.failed_push_to_card'), I18n.t('types.withdrawl.failed_check'), I18n.t('types.withdrawl.failed_ach'), I18n.t('types.withdrawl.instant_pay') , I18n.t('types.withdrawl.send_check') , I18n.t('types.withdrawl.push_to_card') , I18n.t('types.withdrawl.check_deposit') , I18n.t('types.withdrawl.push_to_card_deposit') ]
      withdrawl_types.include?(self.main_type&.downcase) || withdrawl_types.include?(self.tags.try(:[],"type")&.downcase)
    end
  end

end