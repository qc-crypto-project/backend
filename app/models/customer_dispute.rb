class CustomerDispute < ApplicationRecord
  has_many :dispute_requests , dependent: :destroy
  has_many :products
  belongs_to :location,optional: true
  enum reason: {by_mistake: "Order Created By Mistake", time_issue: 'Item(s) Would Not Arrive on Time', high_cost_shipping: 'Shipping Cost Too High', item_price_high: 'Item Price Too High', cheaper_somewhere_else: 'Found Cheaper Somewhere Else', item_sold_by_third_party: 'Item Sold by Third Party', other: 'other'}
  validates :transaction_id , uniqueness: true
  belongs_to :dispute_transaction, class_name: 'Transaction', foreign_key: 'transaction_id'
  belongs_to :customer, class_name: "User",foreign_key: "customer_id"
  belongs_to :merchant, class_name: "User",foreign_key: "merchant_id"
  has_many :evidences , as: :imageable , class_name: "Documentation" , dependent: :destroy
  accepts_nested_attributes_for :evidences

  scope :open_disputes , lambda {|open_disputes|
    open_disputes.present? ? where(status: "pending") : []
  }

  scope :close_disputes , lambda {|close_disputes|
    close_disputes.present? ? where.not(status: "pending") : []
  }

  scope :all_disputes , lambda {|all_disputes,open_disputes,close_disputes|
    all_disputes.present? || (open_disputes.blank? && close_disputes.blank?) ? all : []
  }

end
