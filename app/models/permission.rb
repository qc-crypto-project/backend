class Permission < ApplicationRecord
  belongs_to :merchant, class_name: "User", foreign_key: "merchant_id", optional: true
  has_many :users
  validates :name, presence: true
  validates_uniqueness_of :name
  enum permission_type: [:admin, :regular, :custom]

  def merchant_users_count(user_id)
		self.users.where("users.merchant_id = ?",user_id).unarchived_users.try(:count)
	end
end






