class BlockTransaction < ApplicationRecord
  #schema
  # sequence_id :string
  # sender_id :integer
  # receiver_id :integer
  # sender_wallet :integer
  # receiver_wallet :integer
  # amountInCents :integer
  # feeInCents :integer
  # action :string enum e.g issue, transfer, retire
  # type :string enum
  # sub_type :string enum
  # gateway :string
  # tags :jsonb
  # refunded :boolean
  # refund_reason :string
  # refund_log :jsonb
  # ip :string
  # last4 :string
  # charge_id :string
  # card_id :string
  # status :integer enum
  # api_type :integer enum
  # parent_id :integer

  include WithdrawlConcern

  belongs_to :sender, class_name: "User", foreign_key: :sender_id, optional: true
  belongs_to :receiver, class_name: "User", foreign_key: :receiver_id, optional: true
  belongs_to :sender_wallet, class_name: "Wallet", foreign_key: :sender_wallet_id, optional: true
  belongs_to :receiver_wallet, class_name: "Wallet", foreign_key: :receiver_wallet_id, optional: true
  belongs_to :location, optional: true
  belongs_to :payment_gateway, optional: true
  belongs_to :load_balancer, optional: true
  belongs_to :category, optional: true
  belongs_to :main_transaction, class_name: "Transaction", foreign_key: :transaction_id, optional: true

  enum action: [:issue, :transfer, :retire]
  enum status: [:approved, :failed, :pending]
  enum baked: {baked_split: "baked_split",profit_split: "profit_split", net_profit_split: "net_profit_split", iso_profit: "iso_profit", agent_profit: "agent_profit", affiliate_profit: "affiliate_profit"}
  belongs_to :card, optional: true

  validates_uniqueness_of :sequence_id

  scope :all_transactions, -> (page_size, wallet_id){
    Rails.cache.fetch("all_trx_#{page_size}_#{wallet_id}", expires_in: 10.minutes) do
      where("sender_wallet_id = :wallet OR receiver_wallet_id = :wallet",wallet: wallet_id)
          .order(timestamp: :desc)
          .limit(page_size)
    end
  }

  scope :transactions_types, -> (page_size, transaction_type, current_user){
    if transaction_type == "commission"
      if current_user.affiliate_program?
        Rails.cache.fetch("transactions_types_#{page_size}_affiliate", expires_in: 10.minutes) do
          where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).where(main_type:["Monthly Fee","CBK Fee","Misc fee","Subscription fee","Afp"]).limit(page_size)
          end
      else
        Rails.cache.fetch("transactions_types_#{page_size}_non_affiliate", expires_in: 10.minutes) do
          where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee","ach","giftcard_fee"]).where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "Buy rate fee"]).limit(page_size)
        end
      end
    else
      Rails.cache.fetch("transactions_types_#{page_size}_non_commission", expires_in: 10.minutes) do
        where.not(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "B2B Fee", "Buy rate fee","Afp"]).limit(page_size)
      end
    end
  }

  scope :by_wallet, -> (user_wallet){
    Rails.cache.fetch("by_wallet_#{user_wallet}", expires_in: 10.minutes) do
      where(receiver_wallet_id: user_wallet)
    end
  }

  scope :by_type, -> (type){
    Rails.cache.fetch("by_type_#{type}", expires_in: 10.minutes) do
      where(main_type: type)
    end
  }

  scope :by_time, -> (first_date, second_date){
    Rails.cache.fetch("by_time_#{first_date}_#{second_date}", expires_in: 10.minutes) do
      where(timestamp: first_date..second_date)
    end
  }

  scope :batch_by_commission, -> (wallet_id, first_date, second_date, page_size, transaction_type){
    if transaction_type == "commission"
      Rails.cache.fetch("batch_by_commission_#{first_date}_#{second_date}", expires_in: 10.minutes) do
        where('(receiver_wallet_id = :wallet OR sender_wallet_id = :wallet) AND timestamp BETWEEN :first AND :second', wallet: wallet_id, first: first_date, second: second_date).where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee","ach","giftcard_fee"]).where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "Buy rate fee"]).limit(page_size)
      end
    else
      Rails.cache.fetch("batch_by_commission_#{first_date}_#{second_date}", expires_in: 10.minutes) do
        where("(receiver_wallet_id = :wallets OR sender_wallet_id = :wallets) AND timestamp BETWEEN :first AND :second", wallets: wallet_id, first: first_date, second: second_date).where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).where.not(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "B2B Fee", "Buy rate fee"]).limit(page_size)
      end
    end
  }

  def parent_amount
    BlockTransaction.where(seq_parent_id: self.seq_parent_id, main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit]).try(:first).try(:amount_in_cents)
  end

  def parent_transaction
    BlockTransaction.where("id = ? OR sequence_id = ?",self.parent_id, self.seq_parent_id).first
  end
  
  def child_transactions
    BlockTransaction.where("parent_id = ? OR seq_parent_id = ?", self.id, self.sequence_id)
  end

  def get_location_wallets(location_id)
    location = Location.find(location_id)
    location.wallets.primary.first
  end

  def update_on_local
    puts "Invalid Sequence Transaction ID" && return if self.sequence_id.blank?
    transaction = Transaction.where(seq_transaction_id: self.sequence_id).first
    if transaction.present?
      gateway = PaymentGateway.where(key: self.gateway).first
      transaction.sender_wallet_id = self.sender_wallet_id
      transaction.receiver_wallet_id = self.receiver_wallet_id
      transaction.receiver_id = self.receiver_id
      transaction.sender_id = self.sender_id
      transaction.privacy_fee = self.privacy_fee
      transaction.reserve_money = SequenceLib.dollars(self.hold_money_cents)
      transaction.tip = SequenceLib.dollars(self.tip_cents)
      transaction.net_fee = self.net_fee
      transaction.net_amount = self.total_net
      transaction.total_amount = SequenceLib.dollars(self.amount_in_cents)
      transaction.amount = SequenceLib.dollars(self.tx_amount).to_s
      transaction.card_id = self.card_id
      transaction.charge_id = self.charge_id
      transaction.tags = self.tags
      transaction.ip = self.ip || 'No IP Found!'
      transaction.gbox_fee = self.gbox_fee
      transaction.iso_fee = self.iso_fee
      transaction.agent_fee = self.agent_fee
      transaction.affiliate_fee = self.affiliate_fee
      transaction.payment_gateway_id = gateway.try(:id)
      transaction.load_balancer_id = gateway.try(:load_balancer_id)
      transaction.timestamp = self.timestamp
      transaction.last4 = self.last4
      transaction.main_type = self.main_type
      transaction.sub_type = self.sub_type
      transaction.save!
    else
      puts "Transaction not present in Local Table!"
    end
  end

  def generate_quickcard_id
    loop do
      self.quickcard_id = "QC#{SecureRandom.urlsafe_base64(21)}"
      break unless BlockTransaction.exists?(quickcard_id: self.quickcard_id)
    end
  end
end
