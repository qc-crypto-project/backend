# == Schema Information
#
# Table name: activity_logs
#
#  id              :bigint(8)        not null, primary key
#  user_id         :integer
#  browser         :string
#  ip_address      :string
#  action          :string
#  params          :jsonb
#  controller      :string
#  note            :text
#  respond_with    :jsonb
#  host            :string
#  url             :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  response_status :string
#  activity_type   :integer
#  transaction_id  :integer
#

class ActivityLog < ApplicationRecord

  enum activity_type: [:authorized, :unauthorized, :message, :sequence, :api_success, :api_error, :vt_success, :vt_error, :server_error, :bad_request, :not_found, :tango_order, :admin_refund, :merchant_refund, :user_activity, :user_fee, :success, :error , :affiliate_activity , :load_balancer, :rtp, :rtp_hook ]
  enum action_type: {"invoices"=>"invoices", "customers"=>"customers", "employees"=>"employees", "chargebacks"=>"chargebacks", "permissions"=>"permissions" ,  "ach"=>"ach","check"=>"checks","bulk_checks"=>"bulk_checks","push_to_card"=>"push_to_card","accounts"=>"accounts", "giftcard"=>"giftcard" ,"affiliate_program"=>"affiliate_program","transaction_amount_limit"=>"transaction_amount_limit" }

  belongs_to :user, optional: true
  belongs_to :tango_order, optional: true
  # belongs_to :transaction
  after_save :slack_notify
  before_save :update_url_respond_params

  def slack_notify
    if self.sequence?
      back_trace = self.respond_with["backtrace"]
      SlackService.notify("App: \`\`\` #{{URL: ENV["APP_ROOT"], ACTION: "#{self.controller}/#{self.action}", CrashFrom: self.respond_with.try(:[],"crash_from"), ActivityLogId: self.id}} \`\`\` Error: \`\`\` #{self.respond_with["error"]} \`\`\` SEQ_ERROR_DETAILS: \`\`\` #{self.respond_with["seq_error_detail"].to_json} \`\`\` User: \`\`\` #{self.user.present? ? {id: self.user.id, email: self.user.email, role: self.user.role }.to_json : 'No User Present!'} \`\`\` BACKTRACE: \`\`\` #{back_trace.to_json} \`\`\` ")
    end
  end

  def update_url_respond_params
    self.url = self.url.filtered if self.url.present?
    if self.respond_with.present?
      self.respond_with = self.respond_with.filter_json
    end
    if self.params.present?
      self.params = self.params.filter_json
    end
  end

  def self.log(type = nil, reason = nil,user = nil,params = nil,response = nil, message = nil, action_type = nil, response_status_rtp=nil, activity_log = nil)
    activity = ActivityLog.new
    activity = activity_log if activity_log.present?
    activity.url = RequestInfo.request_url
    activity.host = RequestInfo.req_host
    activity.browser = RequestInfo.browser_req
    if RequestInfo.host_name.present?
      activity.ip_address = RequestInfo.host_name
    end
    activity.controller = RequestInfo.controller
    activity.action = RequestInfo.action
    activity.ref_id = RequestInfo.ref_id
    activity.respond_with = reason.present? ? {response: reason} : response.try(:body)
    activity.response_status = response_status_rtp
    activity.response_status = response.status if response.present? && response_status_rtp.blank?
    unless type.nil?
      activity.activity_type = type
    end
    #if params[:user_image].present?
    #  if params[:user_image].original_filename.present?
    #    params[:user_image] = params[:user_image].original_filename
    #  else
    #    params[:user_image] = 'unknown image format'
    #  end
    #end
    activity.action_type = action_type
    activity.note = message
    if params.present? && response_status_rtp.blank?
      activity.transaction_id = params[:transaction_id]
    elsif response_status_rtp.present?
      activity.transaction_id = params[:merchant_transaction_id].try(:split,'_').try(:first)
    end
    #params[:ip_details][:request_ip] = request.ip if params[:ip_details].try(:[],:request_ip).present?
    #params[:ip_details][:request_remote_ip] = RequestInfo.host_name if params[:ip_details].try(:[],:request_remote_ip).present?
    #params[:issue_amount] = params[:issue_amount].to_f if params.try(:[],:issue_amount).present?
    # clean_parameters(params[:registration]) if params[:registration].present?
    activity.params = params.except(:card_cvv) if params.present?
    activity.user = user
    activity.save
    return activity if response_status_rtp.present?
  end
  scope :this_month, -> { where(:created_at => DateTime.now.beginning_of_month..Time.now) }
end
