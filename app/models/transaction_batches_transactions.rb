class TransactionBatchesTransactions < ApplicationRecord
  belongs_to :transaction_batch
  belongs_to :batch_transaction, :class_name => "Transaction", foreign_key: 'transaction_id'
end
