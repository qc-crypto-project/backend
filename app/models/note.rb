# == Schema Information
#
# Table name: notes
#
#  id           :bigint(8)        not null, primary key
#  notable_type :string
#  notable_id   :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  title        :string           default("")
#  name         :string           default("")
#

class Note < ActiveRecord::Base
	belongs_to :notable, polymorphic: true
end
