# == Schema Information
#
# Table name: app_configs
#
#  id               :bigint(8)        not null, primary key
#  key              :string           not null
#  stringValue      :string
#  boolValue        :boolean          default(TRUE)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  merchant_fee     :float
#  user_fee         :float
#  atm_fee          :float
#  secondary_option :string
#  client_id        :string           default("")
#  client_secret    :string           default("")
#

class AppConfig < ApplicationRecord
  audited max_audits: 5
  include RequestInfo

  enum key: {stripeAch: "stripeAch", payment_processor: "payment_processor", atm_payment_processor: "atm_payment_processor", merchant_api_keys: "merchant_api_keys",
             gift_cards_config: "gift_cards_config",issue_check_config: "issue_check_config", debit_card_deposit: "debit_card_deposit", apis_authorization: "apis_authorization",
             instant_daily_limit: "instant_daily_limit", instant_per_tx_limit: "instant_per_tx_limit", slot_over: "slot_over", slot_under: "slot_under", mid_disable: "mid_disable",
             user_paypal_card_fee: "user_paypal_card_fee",user_gift_card_fee: "user_gift_card_fee", user_visa_card_fee: "user_visa_card_fee",user_spending_limit: "user_spending_limit",
             deposit_from_credit_debit_card: "deposit_from_credit_debit_card", user_check_fee: "user_check_fee",user_deposit_limit: "user_deposit_limit",issue_dispute_config: "issue_dispute_config" , sequence_down: "sequence_down", from_signature: "from_signature",version: "version",
             high_risk_max_mind: "high_risk_max_mind",moderate_risk_max_mind: "moderate_risk_max_mind",approve_manual_review_days: "approve_manual_review_days",merchant_performance: "merchant_performance",agent_performance: "agent_performance",
             merchant_graph: "merchant_graph",affiliate_performance: "affiliate_performance",iso_performance: "iso_performance",iso_agent_performance: "iso_agent_performance",agent_affiliate_performance: "agent_affiliate_performance",ach: "ach",giftcard: "giftcard",p2c: "p2c",checks: "checks"}

  validates :key, inclusion: { in: keys.values }

  after_save :update_app_config_cache
  after_update :merchant_api_keys_configuration

  module Key
    Ach = 'stripeAch'
    PaymentProcessor = 'payment_processor'
    ATMPaymentProcessor = 'atm_payment_processor'
    MerchantApiKey = 'merchant_api_keys'
    GiftCardConfig = 'gift_cards_config'
    IssueCheckConfig = 'issue_check_config'
    IssueDisputeConfig = 'issue_dispute_config'
    DebitCardDeposit = 'debit_card_deposit'
    APIsAuthorization = 'apis_authorization'
    InstantDailyLimit = 'instant_daily_limit'
    InstantPerTxLimit = 'instant_per_tx_limit'
    SlotOver = 'slot_over'
    SlotUnder = 'slot_under'
    MidDisable = 'mid_disable'
    SequenceDown = 'sequence_down'
    HighRiskMaxMind = 'high_risk_max_mind'
    ModerateRiskMaxMind = 'moderate_risk_max_mind'
    ApproveManualReviewDays = 'approve_manual_review_days'
  end

  module Fee
    MerchantVisaCardFee = 'merchant_visa_card_fee'
    MerchantCheckFee = 'merchant_check_fee'
    MerchantPaypalCardFee = 'merchant_paypal_card_fee'
    MerchantGiftCardFee = 'merchant_gift_card_fee'

    UserVisaCardFee = 'user_visa_card_fee'
    UserCheckFee = 'user_check_fee'
    UserPaypalCardFee = 'user_paypal_card_fee'
    UserGiftCardFee = 'user_gift_card_fee'
    UserDepositLimit = 'user_deposit_limit'
    UserSpendingLimit = 'user_spending_limit'
    DepositCard = 'deposit_from_credit_debit_card'
    InstantDailyLimit = 'instant_daily_limit'
    InstantPerTxLimit = 'instant_per_tx_limit'

    AtmVisaCardFee = 'atm_visa_card_fee'
    AtmCheckFee = 'atm_check_fee'
    AtmPaypalCardFee = 'atm_paypal_card_fee'
    AtmGiftCardFee = 'atm_gift_card_fee'
  end

  def self.find_by(params)
    where(params).first
  end

  def self.where(params)
    params= params.present? ? params.try(:with_indifferent_access) : ''
    if params.keys.size === 1 && params['key'].present?
     result = AppConfigCache.getAppConfig(params[:key])
     result.present? ? [result] : []
    end
    super
  end

  def update_app_config_cache
    AppConfigCache.updateAppConfigByKey(self.key , self)
  end

  def merchant_api_keys_configuration
    if self.slot_detail.blank? && Rails.env.production? && ENV['HEROKU_APP'].blank?
      SlackService.notify("* #{self.key.humanize.titleize}.*  \`\`\`#{self.key.humanize} switch has been updated to \" #{self.boolValue.to_s.upcase} \" from HOST: #{RequestInfo.host_name} logged in as ADMIN user from IP: #{RequestInfo.request_ip_address}.\`\`\`", "#quickcard")
    end
  end
end
