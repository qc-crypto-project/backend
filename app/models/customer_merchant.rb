class CustomerMerchant < ApplicationRecord
  belongs_to :merchant_customer, class_name: "User",foreign_key: "customer_id", optional: true
  belongs_to :merchant, class_name: "User",foreign_key: "merchant_id", optional: true
  belongs_to :location_customer, class_name: "Location",foreign_key: "location_id", optional: true
  belongs_to :wallet_customer, class_name: "Wallet",foreign_key: "wallet_id", optional: true
end
