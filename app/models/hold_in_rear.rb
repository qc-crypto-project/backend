class HoldInRear < ApplicationRecord
  include ActionView::Helpers::NumberHelper

  belongs_to :location

  scope :created_today, -> {where(created_at: HoldInRear.pst_beginning_of_day(Time.zone.now)..HoldInRear.pst_end_of_day(Time.zone.now))}
  scope :created_on_date, -> (date){where(created_at: HoldInRear.pst_beginning_of_day(date)..HoldInRear.pst_end_of_day(date))}

  scope :by_location_ids, -> (location_ids){where(location_id: location_ids)}

  scope :in_pending, -> {where("release_date >= ?", HoldInRear.pst_end_of_day(Time.zone.now.end_of_day))}
  # scope :is_released, -> {where("release_date < ?", Date.today + 1.day )}
  scope :is_released, -> {where("release_date < ?", Time.zone.now.end_of_day + 1.day)}
  scope :is_created, -> { where("DATE(created_at) <= ?", Time.zone.now.end_of_day)}
  scope :release_on_date, -> (date){where(release_date: HoldInRear.pst_beginning_of_day(date)..HoldInRear.pst_end_of_day(date))}

  scope :release_on_date_new, -> (date){where(release_date: HoldInRear.pst_beginning_of_day(date))}

  def self.hold_money_in_my_wallet(wallet_id,amount,reserve_amount)
    wallet = Wallet.find_by(id: wallet_id)
    location = wallet.location if wallet.present?
    amount = 0 if amount < 0
    raise "Wallets location should must be present" if location.blank?
    # if location.ecommerce? # processing type is ecommerce
      #hold_money = HoldInRear.where(location_id: location.id).first
      #if hold_money.present?
      #  hold_money.amount = hold_money.ecommerce_amount.to_f + amount.to_f
      #else
      #  hold_money = HoldInRear.new(location_id: location.id, release_date: HoldInRear.pst_beginning_of_day(Time.zone.now))
      #  hold_money.ecommerce_amount = amount.to_f
      #end

    # else # processing type is retail
      hold_money = HoldInRear.created_today.where(location_id: location.id).first
      if hold_money.present?
        reserve_days=0
        hold_money.amount = hold_money.amount.to_f + amount.to_f
        hold_money.reserve_amount = hold_money.reserve_amount.to_f + reserve_amount.to_f
        fees = location.fees.buy_rate.first if location.present?
        reserve_days = fees.days if fees.present?
        reserve_release_date = Time.zone.now + reserve_days.days
        hold_money.reserve_release = HoldInRear.pst_beginning_of_day(reserve_release_date)
      else
        reserve_days=0
        fees = location.fees.buy_rate.first if location.present?
        reserve_days = fees.days if fees.present?
        reserve_release_date = Time.zone.now + reserve_days.days
        release_date = Time.zone.now
        no_of_days = 1
        if location.hold_in_rear?
          no_of_days = location.hold_in_rear_days
          no_of_days = 1 if no_of_days.blank? || no_of_days < 1
          # no_of_days = no_of_days + 1
          # release_date = no_of_days.business_days.from_now
          # else
        end
        no_of_days = no_of_days + 1
        release_date = Time.zone.now + no_of_days.days
        hold_money = HoldInRear.new(location_id: location.id, release_date: HoldInRear.pst_beginning_of_day(release_date) ,reserve_release: HoldInRear.pst_beginning_of_day(reserve_release_date))
        hold_money.amount = amount.to_f
        hold_money.reserve_amount = reserve_amount.to_f

      end
      hold_money.save!
    # end

  end

  #def self.release_money_from_my_wallet(loc_id,amount)
  #  loc = Location.find(loc_id)
  #  if loc.ecommerce?
  #    holded_amount = HoldInRear.where(location_id: loc_id).first
  #    if holded_amount.ecommerce_amount > 0 && (holded_amount.ecommerce_amount - amount.to_f) >= 0
  #      holded_amount.ecommerce_amount = holded_amount.ecommerce_amount - amount.to_f
  #      holded_amount.save!
  #      return true
  #    else
  #      return false
  #    end
  #  else
  #    return false
  #  end
  #end


  def self.calculate_pending(wallet_id)
    wallet = Wallet.find_by(id: wallet_id)
    return 0 if wallet.blank?
    return 0 unless wallet.primary?
    location_id = wallet.location_id if wallet.present?
    if location_id.present?
      location = wallet.location
      if location.ecommerce?
        pending_amount = Transaction.joins(:minfraud_result,:tracker).where("minfraud_results.qc_action": TypesEnumLib::RiskType::PendingReview,main_type: "3DS" ,receiver_wallet_id: wallet_id,status: "approved").where.not("trackers.status": [:delivered, :return_to_sender, :failure]).pluck(:net_amount)
      else
        pending_amount = HoldInRear.in_pending.by_location_ids(location_id).pluck(:amount)
      end
      pending_amount.compact!
      pending_amount = pending_amount.inject(0){|sum,x| sum + x }
    else
      pending_amount = 0
    end
    return pending_amount.to_f
  end

  def self.calculate_pending_new(location_id)
    #using this in new design
    if location_id.present?
      pending_amount = HoldInRear.in_pending.by_location_ids(location_id).pluck(:amount)
      pending_amount.compact!

      pending_amount = pending_amount.inject(0){|sum,x| sum + x }
    else
      pending_amount = 0
    end
    return pending_amount.to_f
  end

  def self.calculate_pending_by_loc_id(location_id)
    if location_id.present?
      if Location.find_by(id: location_id).ecommerce?
        pending_amount = Transaction.joins(:receiver_wallet,:minfraud_result,:tracker).where("wallets.location_id": location_id ,"minfraud_results.qc_action": TypesEnumLib::RiskType::PendingReview ,status: "approved").where.not("trackers.status": [:delivered, :return_to_sender, :failure]).pluck(:net_amount)
      else
        pending_amount = HoldInRear.in_pending.by_location_ids(location_id).pluck(:amount)
      end
      pending_amount.compact!
      pending_amount = pending_amount.inject(0){|sum,x| sum + x }
    else
      pending_amount = 0
    end
    return pending_amount.to_f
  end


  def self.calculate_released_on_date_custom(loc_id,date)
    if loc_id.present?
      released_amount = HoldInRear.release_on_date_new(date + 1.day).by_location_ids(loc_id).pluck(:amount)
      released_amount.compact!

      released_amount = released_amount.inject(0){|sum,x| sum + x }
      released_amount = 0 if released_amount.to_f < 0
    else
      released_amount = 0
    end
    return released_amount.to_f
  end

  def self.pst_beginning_of_day(date_time)
    time = Time.parse("#{date_time}")
    time = time.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc
    return time
  end

  def self.pst_end_of_day(date_time)
    time = Time.parse("#{date_time}")
    time = time.in_time_zone("Pacific Time (US & Canada)").end_of_day.utc
    return time
  end

end
