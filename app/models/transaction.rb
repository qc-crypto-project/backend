# == Schema Information
#
# Table name: transactions
#
#  id                 :bigint(8)        not null, primary key
#  to                 :string
#  from               :string
#  amount             :string
#  status             :string
#  charge_id          :string
#  user_id            :bigint(8)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  sender_id          :integer
#  action             :string
#  seq_transaction_id :text
#  slug               :string
#  card_id            :bigint(8)
#

class Transaction < ApplicationRecord
  extend FriendlyId
  include ActionView::Helpers::NumberHelper
  include WithdrawlConcern
  friendly_id :action, use: :slugged
  # validates_uniqueness_of :quickcard_id, :if => Proc.new { |a| a.new_record? }
  before_create :set_quickcard_id

  belongs_to :load_balancer, optional: true
  has_one :minfraud_result
  has_one :tracker
  belongs_to :payment_gateway, optional: true
  belongs_to :invoice, class_name: 'Request', foreign_key: 'order_id', optional: true
  belongs_to :ach_gateway, optional: true
  has_and_belongs_to_many :transaction_batches
  has_one :dispute_case
  has_many :activity_logs, :dependent => :destroy
  has_many :messages
  belongs_to :user, optional: true
  belongs_to :card, optional: true
  has_one :tango_order
  has_many :notes, as: :notable
  has_many :event_logs, dependent: :destroy
  has_many :products
  has_one :block_transaction
  has_many :attachments, :class_name => "Image", foreign_key: 'usage_id', :dependent => :destroy
  has_one :order_bank
  has_one :customer_dispute
  belongs_to :sender, class_name: "User", foreign_key: :sender_id, optional: true
  belongs_to :receiver, class_name: "User", foreign_key: :receiver_id, optional: true
  belongs_to :sender_wallet, class_name: "Wallet", foreign_key: :sender_wallet_id, optional: true
  belongs_to :receiver_wallet, class_name: "Wallet", foreign_key: :receiver_wallet_id, optional: true
  belongs_to :shipping_address, class_name: "Address", foreign_key: :shipping_id, optional: true
  belongs_to :billing_address, class_name: "Address", foreign_key: :billing_id, optional: true
  ransacker :terminal_transaction do
    Arel.sql("transactions.tags -> 'sales_info' ->> 'terminal_id'")
  end
  scope :all_approved, -> (page_size, wallet_id){
    if wallet_id.present?
      Rails.cache.fetch("approved_trx_#{wallet_id}_#{page_size}", expires_in: 10.minutes) do
        includes(:minfraud_result).references(:minfraud_result).where('(sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string) AND transactions.status IN (:status)', wallets: wallet_id, string: "#{wallet_id}", status: ["approved", "complete", "refunded","Chargeback","CBK Won","CBK Lost","cbk_fee", "Retrievel"])
            .order('transactions.created_at DESC').limit(page_size)
      end
    else
      Rails.cache.fetch("approved_trx_#{page_size}", expires_in: 10.minutes) do
        includes(:minfraud_result).references(:minfraud_result).where(status: ["approved", "complete", "refunded","Chargeback","CBK Won","CBK Lost","cbk_fee", "Retrievel"])
            .order('transactions.created_at DESC').limit(page_size)
      end
    end
  }
  scope :all_refund, -> (page_size){
    Rails.cache.fetch("refund_trx_#{page_size}", expires_in: 10.minutes) do
      where(main_type: ["refund_bank", "refund"])
      .order('transactions.created_at DESC').limit(page_size)
    end
  }
  scope :all_b2b, -> (page_size){
    Rails.cache.fetch("b2b_trx_#{page_size}", expires_in: 10.minutes) do
      where(main_type: ["B2B Transfer"]).order('created_at DESC').limit(page_size)
    end
  }

  scope :all_sale_b2b, -> (wallet_id){
    Rails.cache.fetch("sale_b2b_trx_#{wallet_id}", expires_in: 10.minutes) do
      where(receiver_wallet_id: wallet_id ,main_type: ["B2B Transfer","eCommerce", "Virtual Terminal", "Credit Card","Debit Card","QCP Secure","QC Secure","Invoice - Credit Card","Invoice - Debit Card","Invoice Qc"] ,status: ["approved", "complete", "refunded"])
    end
  }

  scope :all_declines, -> (page_size){
    Rails.cache.fetch("declines_trx_#{page_size}", expires_in: 10.minutes) do
      where(status: "pending")
      .where(main_type:["eCommerce", "Virtual Terminal", "Credit Card", "PIN Debit", "Sale Issue", "Debit Card","QCP Secure","QC Secure", "Invoice - RTP", "RTP"])
      .order('created_at DESC').limit(page_size)
    end
  }

  scope :specific_declines, -> (page_size, wallet_id, location_id) {
    Rails.cache.fetch("specific_declines_trx_#{wallet_id}_#{page_size}", expires_in: 10.minutes) do
      where("(sender_wallet_id = :wallet OR receiver_wallet_id = :wallet OR tags->'location'->>'id' = :location_id) AND status = 'pending' AND main_type != :main_type", main_type: "", wallet: wallet_id,location_id: "#{location_id}")
          .distinct
          .order(created_at: :desc).limit(page_size)
    end
  }

  scope :specific_approved, -> (page_size, wallet_id) {
    Rails.cache.fetch("specific_approved_trx_#{wallet_id}_#{page_size}", expires_in: 10.minutes) do
      where('(sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string) AND main_type != :main_type',main_type: "Sale Issue", wallets: wallet_id, string: "#{wallet_id}")
          .where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee", "CBK Lost"])
          .order(created_at: :desc).limit(page_size)
    end
  }


  scope :parent_refund_transaction, -> (refund_transaction) {
    Rails.cache.fetch("parent_refund_transaction_#{refund_transaction.try(:id)}", expires_in: 10.minutes) do
      includes(:sender_wallet,:sender).where(seq_transaction_id:  refund_transaction.try(:[],"tags").try(:[],"refund_details").try(:[],"parent_transaction_id")).last
    end
  }

  scope :refund_parent_failed_transaction, -> (refund_failed_transaction){
    Rails.cache.fetch("refund_parent_failed_transaction#{refund_failed_transaction.try(:id)}", expires_in: 10.minutes) do
      where(seq_transaction_id: refund_failed_transaction.try("tags").try(:[], 'refund_failed_details').try(:[], 'parent_refund_transaction_id')).last
    end
  }

  scope :exclude_qr_refund_fee, ->{ where.not(main_type: "refund_fee") }

  scope :dispute_transaction_count, -> (wallet_id) {
    where(receiver_wallet_id: wallet_id,main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionType::Transfer3DS], status: ["approved", "refunded", "complete"]).where('transactions.created_at > ?','01/01/2020'.to_datetime).count
  }
  scope :wallet_transactions, -> (wallet_id) {
    where('(sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string) AND transactions.status IN (:status)', wallets: wallet_id, string: "#{wallet_id}", status: ["approved", "complete", "refunded","Chargeback","CBK Won","CBK Lost","cbk_fee", "Retrievel"])
  }

  scope :refund_failed_count, ->(parent_id){
    where("tags -> 'refund_failed_details' ->> 'parent_transaction_id' = ? AND main_type = ? " , parent_id, "Refund Failed").count
  }

  scope :by_date, -> (first_date, second_date){
    includes(:card).references(:card).where('transactions.created_at BETWEEN :first AND :second', first: first_date, second: second_date).select("total_amount as total_amount")
  }

  scope :by_transactions_types, -> (gateways){
    where('(main_type IN (:types) AND payment_gateway_id IN (:gateways)) OR (lower(main_type) IN (:rtp))', types: [TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::DebitCard,TypesEnumLib::TransactionViewTypes::PinDebit],gateways: gateways, rtp: [TypesEnumLib::TransactionType::RTP.downcase, TypesEnumLib::TransactionType::InvoiceRTP.downcase]).group_by{ |e| e.main_type.downcase == TypesEnumLib::TransactionType::RTP.downcase ? "#{e.main_type.downcase}_#{e.status}" : e.status == "refunded" || e.status == "complete" ? "#{e.try(:card).try(:brand).try(:downcase)}_approved_#{e.try(:card).try(:card_type).try(:downcase)}" : "#{e.try(:card).try(:brand).try(:downcase)}_#{e.status}_#{e.try(:card).try(:card_type).try(:downcase)}"}
  }

  scope :created_between, -> (first_date, second_date){
    where("created_at BETWEEN :first AND :second", first: first_date, second: second_date)
  }

  scope :not_pending_or_refund, -> (client){
    where.not(status: "pending").where(sender_id: client.customer_id, receiver_wallet_id: client.wallet_id).or(Transaction.where.not(status: "pending").where(receiver_id: client.customer_id, sender_wallet_id: client.wallet_id, main_type: "refund"))
  }
  
  # after_create :notify_user, :record_transaction
  after_create :notify_user
  after_update :notify_user_update
  after_create :create_events
  after_save :transaction_events
  after_save :update_sequence, :maintain_sender_batch, :maintain_receiver_batch

  def notify_user
    return if self.try(:sender).blank? && self.try(:receiver).blank?
    return if self.try(:user).try(:id) === self.sender.try(:id)
    Notification.notify_user(self.sender || self.receiver, self.sender || self.receiver, self)
  end

  def notify_user_update
    return if self.try(:sender).blank? && self.try(:receiver).blank?
    if status == "complete"
      Notification.notify_user(self.sender || self.receiver, self.sender || self.receiver, self, 'update')
    elsif self.status == "approved" && self.main_type == "refund"
      noti_amount = number_to_currency(number_with_precision(self.amount, :precision => 2, delimiter: ','))
      txn_id = self.try(:seq_transaction_id)[0..5]
      if self.tags.present? && self.tags["refund_details"].present? && self.tags["refund_details"]["parent_transaction_id"].present?
        txn_id = self.tags["refund_details"]["parent_transaction_id"][0..5]
      end
      text = I18n.t('notifications.approved_refund',dba_name: self.sender_name, amount: noti_amount, trans_id: txn_id)
      merchant = User.find(self.refunded_by)
      if merchant.merchant_id == nil
        Notification.notify_user(self.sender, self.sender, self, 'approve refund',nil,text)
      elsif merchant.merchant_id != nil
        Notification.notify_user(merchant, merchant, self, 'approve refund',nil,text)
        Notification.notify_user(self.sender , self.sender, self, 'approve refund',nil,text)
      end
    end
  end

  def get_location_wallets(location_id)
    location = Location.find_by(id: location_id)
    location&.wallets&.primary&.first
  end

  def update_from_block
    puts "Invlid Sequence Transaction ID." && return if self.seq_transaction_id.blank?
    block_transaction = BlockTransaction.where(sequence_id: self.seq_transaction_id).first
    if block_transaction.present?
      gateway = PaymentGateway.where(key: block_transaction.gateway).first
      self.sender_wallet_id = block_transaction.sender_wallet_id
      self.receiver_wallet_id = block_transaction.receiver_wallet_id
      self.receiver_id = block_transaction.receiver_id
      self.sender_id = block_transaction.sender_id
      self.privacy_fee = block_transaction.privacy_fee
      self.reserve_money = SequenceLib.dollars(block_transaction.hold_money_cents)
      self.tip = SequenceLib.dollars(block_transaction.tip_cents)
      self.net_fee = block_transaction.net_fee
      self.net_amount = block_transaction.total_net
      self.total_amount = SequenceLib.dollars(block_transaction.amount_in_cents)
      self.amount = SequenceLib.dollars(block_transaction.tx_amount).to_s
      self.card_id = block_transaction.card_id
      self.charge_id = block_transaction.charge_id
      self.tags = block_transaction.tags
      self.ip = block_transaction.ip || 'No IP Found!'
      self.gbox_fee = block_transaction.gbox_fee
      self.iso_fee = block_transaction.iso_fee
      self.agent_fee = block_transaction.agent_fee
      self.affiliate_fee = block_transaction.affiliate_fee
      self.payment_gateway_id = gateway.try(:id)
      self.load_balancer_id = gateway.try(:id)
      self.timestamp = block_transaction.timestamp
      self.last4 = block_transaction.last4
      self.main_type = block_transaction.main_type
      self.sub_type = block_transaction.sub_type
      self.save!
    else
      puts "Transaction not present in Block Table!"
    end
  end

  def retire?
    if self.action == "retire"
      true
    else
      false
    end
  end

  def rtp_webhook(response)
    location = self.receiver_wallet.try(:location)
    if location.present? && location.rtp_hook.present?
      Thread.new do
        curlObj = Curl::Easy.new(location.rtp_hook)
        curlObj.post_body = response.to_json
        curlObj.headers = ['Content-Type:application/json']
        curlObj.perform()
      end
    end
  end

  private

  def update_sequence
    if self.seq_transaction_id.blank?
      self.update(seq_transaction_id: self.slug)
    end
  end

  def maintain_sender_batch
    #using this function to maintain batches to show in view in new design
    sender = self.sender
    if self.timestamp.present? && ((sender.present? && sender.merchant?) && (self.sender_wallet.present? && !self.sender_wallet.reserve?)) && (self.status == "approved" || self.status == "Chargeback" || self.status == "cbk_fee")
      deduct_partner_fee = true
      tags = self.tags.try(:[],"fee_perc")
      if ([TypesEnumLib::TransactionViewTypes::B2B.humanize.downcase, TypesEnumLib::TransactionType::InvoiceQC.humanize.downcase].include?(self.main_type.humanize.downcase) && self.sub_type == "invoice") || (self.main_type == TypesEnumLib::TransactionType::Transfer3DS && self.sub_type == "invoice") || ([TypesEnumLib::TransactionType::InvoiceRTP].include?(self.main_type))
        invoice_amount = self.total_amount.to_f
        invoice_count = 1
        if [TypesEnumLib::TransactionViewTypes::B2B.humanize.downcase, TypesEnumLib::TransactionType::InvoiceQC.humanize.downcase].include?(self.main_type.humanize.downcase) && self.sub_type == "invoice"
          deduct_partner_fee = false
        end
        receiver = self.receiver
        if receiver.iso?
          iso_commission = self.total_amount.to_f
        elsif receiver.agent?
          agent_commission = self.total_amount.to_f
        elsif receiver.affiliate?
          affiliate_commission = self.total_amount.to_f
        end
      elsif self.main_type == TypesEnumLib::TransactionType::Refund
        refund_count = 1
        refund_amount = self.total_amount.to_f
      elsif self.main_type == TypesEnumLib::TransactionType::ReserveMoneyDeposit
        reserve_amount = self.total_amount.to_f
      end
      batch_type = "sale"
      fees = 0
      total_amount = 0
      merchant_wallet_id = self.sender_wallet_id
      iso = tags.try(:[], "iso")
      if self.main_type != TypesEnumLib::TransactionType::ReserveMoneyDeposit
        if iso.try(:[],"iso_buyrate")
          if tags.try(:[], "fee").to_f > 0
            expenses = self.amount.to_f + tags.try(:[], "fee").to_f
          else
            expenses = self.amount.to_f
          end
        else
          expenses = self.total_amount.to_f
        end
      end
      if tags.present? && deduct_partner_fee && (self.main_type != "Send Check" && self.main_type != "ACH" && self.main_type != "instant_pay" && self.main_type != "Failed ACH")
        if tags["splits"].present?
          tags = tags["splits"]
        end
        Thread.new do
          curlObj = Curl::Easy.new(ENV["BATCH_URL"])
          curlObj.post_body = {tags: tags,date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date}.to_json
          curlObj.perform()
        end
        agent = tags["agent"]
        affiliate = tags["affiliate"]
        if iso.present? && iso["wallet"].present? && (iso["amount"].to_f > 0 || iso["misc"].to_f > 0 || iso["statement"].to_f > 0 || iso["service"].to_f > 0)
          iso_count = 1
          iso_balance = iso["starting_balance"].to_f
          iso_split = get_commission_split(tags, iso["wallet"], "iso")
          earn_commission = get_commission_earn(tags, iso["wallet"])
          monthly_iso_fee = iso["misc"].to_f + iso["service"].to_f + iso["statement"].to_f
          if iso["iso_buyrate"].present? && iso["iso_buyrate"] == true
            iso_amount = earn_commission.to_f
            iso_expense = iso["amount"].to_f
            if iso["amount"].to_f == fees.to_f
              fees = 0
            elsif iso["amount"].to_f != fees.to_f
              fees = fees.to_f.try(:abs) - iso["amount"].to_f.try(:abs) unless [TypesEnumLib::TransactionType::CbkFee, TypesEnumLib::TransactionType::Refund].include?(self.main_type)
              fees = fees.to_f.try(:abs)
            end
          else
            iso_amount = iso["amount"].to_f + earn_commission.to_f + monthly_iso_fee.to_f
          end
          iso_wallet = iso["wallet"]
        end
        if agent.present? && agent["wallet"].present? && (agent["amount"].to_f > 0 || agent["misc"].to_f > 0 || agent["statement"].to_f > 0 || agent["service"].to_f > 0)
          agent_count = 1
          iso_count = iso_count.to_i + 1
          agent_balance = agent["starting_balance"].to_f
          agent_split = get_commission_split(tags, agent["wallet"], "agent")
          agent_earn_commission = get_commission_earn(tags, agent["wallet"])
          monthly_agent_fee = agent["misc"].to_f + agent["service"].to_f + agent["statement"].to_f
          agent_amount = agent["amount"].to_f + agent_earn_commission.to_f + monthly_agent_fee.to_f
          agent_wallet = agent["wallet"]
        end
        if affiliate.present? && affiliate["wallet"].present? && (affiliate["amount"].to_f > 0 || affiliate["misc"].to_f > 0 || affiliate["statement"].to_f > 0 || affiliate["service"].to_f > 0)
          affiliate_count = 1
          agent_count = agent_count.to_i + 1
          affiliate_balance = affiliate["starting_balance"].to_f
          affiliate_split = get_commission_split(tags, affiliate["wallet"], "affiliate")
          affiliate_earn_commission = get_commission_earn(tags, affiliate["wallet"])
          monthly_affiliate_fee = affiliate["misc"].to_f + affiliate["service"].to_f + affiliate["statement"].to_f
          affiliate_amount = affiliate["amount"].to_f + affiliate_earn_commission.to_f + monthly_affiliate_fee.to_f
          affiliate_wallet = affiliate["wallet"]
        end
      end
      iso_amount = iso_amount.to_f + iso_commission.to_f
      agent_amount = agent_amount.to_f + agent_commission.to_f
      affiliate_amount = affiliate_amount.to_f + affiliate_commission.to_f
      batch = TransactionBatch.where(merchant_wallet_id: merchant_wallet_id, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: batch_type).last
      if batch.present?
        reserve = batch.reserve.to_f + reserve_amount.to_f
        expense = batch.expenses.to_f + expenses.to_f
        fee = batch.total_fee.to_f + fees.to_f
        amount = (batch.total_amount.to_f + total_amount.to_f).to_f
        total_net = batch.starting_balance.to_f + amount.to_f - fee.to_f - expense.to_f - reserve.to_f
        update_batch(batch,total_amount, fee, total_net, expense, invoice_amount, invoice_count, iso_amount, agent_amount, affiliate_amount, iso_split, agent_split, affiliate_split, iso_expense, 0, 0, iso_wallet, agent_wallet, affiliate_wallet, refund_count, refund_amount, iso_count ,agent_count, affiliate_count, reserve_amount)
      elsif batch.blank?
        batch = TransactionBatch.where(merchant_wallet_id: merchant_wallet_id, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day, batch_type: batch_type).last
        starting_balance = SequenceLib.balance(merchant_wallet_id)
        if self.main_type == TypesEnumLib::TransactionType::ReserveMoneyDeposit
          starting_balance = (starting_balance.to_f + self.tags["main_transaction_info"].try(:[], "fee").to_f + self.total_amount) - self.tags["main_transaction_info"].try(:[] ,"amount").to_f
        else
          starting_balance = starting_balance.to_f + expenses.to_f
        end
        if batch.present?
          net = number_with_precision(batch.try(:total_net).to_f, precision: 2).to_f
          if net.to_f != starting_balance.to_f
            send_batch_error(sender, merchant_wallet_id, Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, net, starting_balance)
          end
        end
        total_net = starting_balance.to_f + total_amount.to_f - fees.to_f - expenses.to_f - reserve_amount.to_f
        batch = create_batch(total_amount, fees, total_net, expenses, starting_balance, merchant_wallet_id, nil, batch_type, invoice_amount,invoice_count, iso_wallet, iso_amount, iso_split,0, agent_wallet, agent_amount, agent_split,0, affiliate_wallet, affiliate_amount, affiliate_split,0, iso_balance, agent_balance, affiliate_balance,0,0, iso_count ,agent_count, affiliate_count, reserve_amount)
      end
      TransactionBatchesTransactions.create(transaction_batch_id: batch.id, transaction_id: self.id) if batch.present?
    elsif self.timestamp.present? && (self.sender_wallet.present? && self.sender_wallet.check_escrow? && self.main_type == "Push to card deposit")
      batch_type = "sale"
      tags = self.tags.try(:[],"fee_perc")
      if tags.present? && (self.main_type != "ACH" && self.main_type != "instant_pay")
        if tags["splits"].present?
          tags = tags["splits"]
        end
        Thread.new do
          curlObj = Curl::Easy.new(ENV["BATCH_URL"])
          curlObj.post_body = {tags: tags,date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date}.to_json
          curlObj.perform()
        end
        iso = tags["iso"]
        agent = tags["agent"]
        affiliate = tags["affiliate"]
        if iso.present? && iso["wallet"].present? && (iso["amount"].to_f > 0 || iso["misc"].to_f > 0 || iso["statement"].to_f > 0 || iso["service"].to_f > 0)
          iso_count = 1
          iso_balance = iso["starting_balance"].to_f
          iso_split = get_commission_split(tags, iso["wallet"], "iso")
          earn_commission = get_commission_earn(tags, iso["wallet"])
          monthly_iso_fee = iso["misc"].to_f + iso["service"].to_f + iso["statement"].to_f
          if iso["iso_buyrate"].present? && iso["iso_buyrate"] == true
            iso_amount = earn_commission.to_f
            iso_expense = iso["amount"].to_f
            if iso["amount"].to_f == fees.to_f
              fees = 0
            elsif iso["amount"].to_f != fees.to_f
              fees = fees.to_f - iso["amount"].to_f
            end
          else
            iso_amount = iso["amount"].to_f + earn_commission.to_f + monthly_iso_fee.to_f
          end
          iso_wallet = iso["wallet"]
        end
        if agent.present? && agent["wallet"].present? && (agent["amount"].to_f > 0 || agent["misc"].to_f > 0 || agent["statement"].to_f > 0 || agent["service"].to_f > 0)
          agent_count = 1
          iso_count = iso_count.to_i + 1
          agent_balance = agent["starting_balance"].to_f
          agent_split = get_commission_split(tags, agent["wallet"], "agent")
          agent_earn_commission = get_commission_earn(tags, agent["wallet"])
          monthly_agent_fee = agent["misc"].to_f + agent["service"].to_f + agent["statement"].to_f
          agent_amount = agent["amount"].to_f + agent_earn_commission.to_f + monthly_agent_fee.to_f
          agent_wallet = agent["wallet"]
        end
        if affiliate.present? && affiliate["wallet"].present? && (affiliate["amount"].to_f > 0 || affiliate["misc"].to_f > 0 || affiliate["statement"].to_f > 0 || affiliate["service"].to_f > 0)
          affiliate_count = 1
          agent_count = agent_count.to_i + 1
          affiliate_balance = affiliate["starting_balance"].to_f
          affiliate_split = get_commission_split(tags, affiliate["wallet"], "affiliate")
          affiliate_earn_commission = get_commission_earn(tags, affiliate["wallet"])
          monthly_affiliate_fee = affiliate["misc"].to_f + affiliate["service"].to_f + affiliate["statement"].to_f
          affiliate_amount = affiliate["amount"].to_f + affiliate_earn_commission.to_f + monthly_affiliate_fee.to_f
          affiliate_wallet = affiliate["wallet"]
        end
        if tags["location"].present?
          location = Location.where(id: tags["location"]["id"]).last
          merchant_wallet_id = location.try(:wallets).try(:primary).try(:first).try(:id)
        end
        batch = TransactionBatch.where(merchant_wallet_id: merchant_wallet_id, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: batch_type).last
        if batch.present?
          reserve = batch.reserve.to_f + reserve_amount.to_f
          expense = batch.expenses.to_f + expenses.to_f
          fee = batch.total_fee.to_f + fees.to_f
          amount = (batch.total_amount.to_f + total_amount.to_f).to_f
          total_net = batch.starting_balance.to_f + amount.to_f - fee.to_f - expense.to_f - reserve.to_f
          update_batch(batch,total_amount, fee, total_net, expense, invoice_amount, invoice_count, iso_amount, agent_amount, affiliate_amount, iso_split, agent_split, affiliate_split, iso_expense, 0, 0, iso_wallet, agent_wallet, affiliate_wallet, refund_count, refund_amount, iso_count ,agent_count, affiliate_count, reserve_amount)
        elsif batch.blank?
          batch = TransactionBatch.where(merchant_wallet_id: merchant_wallet_id, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day, batch_type: batch_type).last
          starting_balance = SequenceLib.balance(merchant_wallet_id)
          starting_balance = number_with_precision(starting_balance.to_f + expenses.to_f, precision: 2).to_f
          if batch.present?
            net = number_with_precision(batch.try(:total_net).to_f, precision: 2).to_f
            if net.to_f != starting_balance.to_f
              send_batch_error(sender, merchant_wallet_id, Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, net, starting_balance)
            end
          end
          total_net = starting_balance.to_f + total_amount.to_f - fees.to_f - expenses.to_f - reserve_amount.to_f
          batch = create_batch(total_amount, fees, total_net, expenses, starting_balance, merchant_wallet_id, nil, batch_type, invoice_amount,invoice_count, iso_wallet, iso_amount, iso_split,0, agent_wallet, agent_amount, agent_split,0, affiliate_wallet, affiliate_amount, affiliate_split,0, iso_balance, agent_balance, affiliate_balance,0,0, iso_count ,agent_count, affiliate_count, reserve_amount)
        end
        TransactionBatchesTransactions.create(transaction_batch_id: batch.id, transaction_id: self.id) if batch.present?
      end
    elsif self.timestamp.present? && ((sender.present? && (sender.agent? || sender.iso? || sender.affiliate?))) && (self.status == "approved" || self.status == "cbk_fee")
      #for partner batches
      batch_type = "sale"
      if (self.main_type == TypesEnumLib::TransactionViewTypes::B2B && self.sub_type == "invoice") || (self.main_type == TypesEnumLib::TransactionType::Transfer3DS && self.sub_type == "invoice")
        invoice_amount = self.total_amount.to_f
        invoice_count = 1
      end
      if sender.iso?
        iso_expense = self.total_amount.to_f
        iso_wallet = self.sender_wallet_id
        iso_count = 1
      elsif sender.agent?
        agent_expense = self.total_amount.to_f
        agent_wallet = self.sender_wallet_id
        agent_count = 1
      elsif sender.affiliate?
        affiliate_expense = self.total_amount.to_f
        affiliate_wallet = self.sender_wallet_id
        affiliate_count = 1
      end
      batch = TransactionBatch.where(iso_wallet_id: iso_wallet, agent_wallet_id: agent_wallet, affiliate_wallet_id: affiliate_wallet, merchant_wallet_id: nil, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: batch_type).last
      if batch.present?
        update_batch(batch,0, 0, 0, 0, invoice_amount, invoice_count, iso_amount, agent_amount, affiliate_amount, iso_split, agent_split, affiliate_split, iso_expense, agent_expense, affiliate_expense,iso_wallet,agent_wallet,affiliate_wallet, nil,0, iso_count ,agent_count, affiliate_count, 0)
      else
        if sender.iso?
          iso_balance = SequenceLib.balance(iso_wallet)
          iso_balance = iso_balance.to_f + iso_expense.to_f
          get_partner_balance(iso_wallet, "iso", iso_balance, sender)
        elsif sender.agent?
          agent_balance = SequenceLib.balance(agent_wallet)
          agent_balance = agent_balance.to_f + agent_expense.to_f
          get_partner_balance(agent_wallet, "agent", agent_balance, sender)
        elsif sender.affiliate?
          affiliate_balance = SequenceLib.balance(affiliate_wallet)
          affiliate_balance = affiliate_balance.to_f + affiliate_expense.to_f
          get_partner_balance(affiliate_wallet, "affiliate", affiliate_balance, sender)
        end
        batch = create_batch(0, 0, 0, 0, 0, nil, nil, batch_type, invoice_amount,invoice_count, iso_wallet, iso_amount, iso_split,iso_expense, agent_wallet, agent_amount, agent_split,agent_expense, affiliate_wallet, affiliate_amount, affiliate_split,affiliate_expense, iso_balance, agent_balance, affiliate_balance,nil,nil, iso_count ,agent_count, affiliate_count, 0)
      end
      TransactionBatchesTransactions.create(transaction_batch_id: batch.id, transaction_id: self.id) if batch.present?
    end
  end

  def maintain_receiver_batch
    #using this function to maintain batches to show in view in new design
    receiver = self.receiver
    if self.timestamp.present? && ((receiver.present? && receiver.merchant?)) && (self.status == "approved" || self.status == "CBK Won")
      if self.main_type == TypesEnumLib::TransactionType::ReserveMoneyDeposit
        return
      end
      batch_type = "sale"
      fees = self.fee.to_f
      if self.main_type == TypesEnumLib::TransactionViewTypes::B2B || self.main_type == TypesEnumLib::TransactionViewTypes::Ecommerce || self.main_type == TypesEnumLib::TransactionViewTypes::CreditCard || self.main_type == TypesEnumLib::TransactionViewTypes::PinDebit || self.main_type == TypesEnumLib::TransactionType::QCSecure || self.main_type == TypesEnumLib::TransactionType::Transfer3DS || self.main_type == TypesEnumLib::TransactionType::DebitCard || self.main_type == TypesEnumLib::TransactionViewTypes::VirtualTerminal || self.main_type == TypesEnumLib::TransactionViewTypes::QrDebitCard || self.main_type == TypesEnumLib::TransactionViewTypes::QrCreditCard || self.main_type == "account_transfer" || (self.main_type == TypesEnumLib::TransactionType::Transfer3DS && self.sub_type == "invoice") || ([TypesEnumLib::TransactionType::InvoiceRTP, TypesEnumLib::TransactionType::RTP].include?(self.main_type))
        expenses = 0
        invoice_amount = 0
        invoice_count = 0
        if ([TypesEnumLib::TransactionViewTypes::B2B.humanize.downcase, TypesEnumLib::TransactionType::InvoiceQC.humanize.downcase].include?(self.main_type.humanize.downcase) && self.sub_type == "invoice") || (self.main_type == TypesEnumLib::TransactionType::Transfer3DS && self.sub_type == "invoice") || (self.main_type == TypesEnumLib::TransactionType::Transfer3DS && self.sub_type == "invoice") || ([TypesEnumLib::TransactionType::InvoiceRTP].include?(self.main_type))
          invoice_amount = self.total_amount.to_f
          invoice_count = 1
          sender = self.sender
          if sender.iso?
            iso_expense = self.total_amount.to_f
          elsif sender.agent?
            agent_expense = self.total_amount.to_f
          elsif sender.affiliate?
            affiliate_expense = self.total_amount.to_f
          end
        end
      end
      total_amount = self.total_amount.to_f
      merchant_wallet_id = self.receiver_wallet_id
      tags = self.tags.try(:[],"fee_perc")
      if tags.present? && (self.main_type != "Void ACH" && self.main_type != "Void Push To Card" && self.main_type != "Void Check" && self.main_type != "Failed ACH")
        if tags["splits"].present?
          tags = tags["splits"]
        end
        Thread.new do
          curlObj = Curl::Easy.new(ENV["BATCH_URL"])
          curlObj.post_body = {tags: tags,date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date}.to_json
          curlObj.perform()
        end
        iso = tags["iso"]
        agent = tags["agent"]
        affiliate = tags["affiliate"]
        if iso.present? && iso["wallet"].present? && (iso["amount"].to_f > 0 || iso["misc"].to_f > 0 || iso["statement"].to_f > 0 || iso["service"].to_f > 0)
          iso_count = 1
          iso_balance = iso["starting_balance"].to_f
          iso_split = get_commission_split(tags, iso["wallet"], "iso")
          earn_commission = get_commission_earn(tags, iso["wallet"])
          monthly_iso_fee = iso["misc"].to_f + iso["service"].to_f + iso["statement"].to_f
          if iso["iso_buyrate"].present? && iso["iso_buyrate"] == true
            iso_amount = earn_commission.to_f
            iso_expense = iso["amount"].to_f
            if iso["amount"].to_f == fees.to_f
              fees = 0
            elsif iso["amount"].to_f != fees.to_f
              fees = fees.to_f.try(:abs) - iso["amount"].to_f.try(:abs)
              fees = fees.to_f.try(:abs)
            end
          else
            iso_amount = iso["amount"].to_f + earn_commission.to_f + monthly_iso_fee.to_f
          end
          iso_wallet = iso["wallet"]
        end
        if agent.present? && agent["wallet"].present? && (agent["amount"].to_f > 0 || agent["misc"].to_f > 0 || agent["statement"].to_f > 0 || agent["service"].to_f > 0)
          agent_count = 1
          iso_count = iso_count.to_i + 1
          agent_balance = agent["starting_balance"].to_f
          agent_split = get_commission_split(tags, agent["wallet"], "agent")
          agent_earn_commission = get_commission_earn(tags, agent["wallet"])
          monthly_agent_fee = agent["misc"].to_f + agent["service"].to_f + agent["statement"].to_f
          agent_amount = agent["amount"].to_f + agent_earn_commission.to_f + monthly_agent_fee.to_f
          agent_wallet = agent["wallet"]
        end
        if affiliate.present? && affiliate["wallet"].present? && (affiliate["amount"].to_f > 0 || affiliate["misc"].to_f > 0 || affiliate["statement"].to_f > 0 || affiliate["service"].to_f > 0)
          affiliate_count = 1
          agent_count = agent_count.to_i + 1
          affiliate_balance = affiliate["starting_balance"].to_f
          affiliate_split = get_commission_split(tags, affiliate["wallet"], "affiliate")
          affiliate_earn_commission = get_commission_earn(tags, affiliate["wallet"])
          monthly_affiliate_fee = affiliate["misc"].to_f + affiliate["service"].to_f + affiliate["statement"].to_f
          affiliate_amount = affiliate["amount"].to_f + affiliate_earn_commission.to_f + monthly_affiliate_fee.to_f
          affiliate_wallet = affiliate["wallet"]
        end
      end
      batch = TransactionBatch.where(merchant_wallet_id: merchant_wallet_id, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: batch_type).last
      if batch.present?
        reserve = batch.reserve.to_f
        expense = batch.expenses.to_f + expenses.to_f
        fee = batch.total_fee.to_f + fees.to_f
        amount = (batch.total_amount.to_f + total_amount.to_f).to_f
        total_net = batch.starting_balance.to_f + amount.to_f - fee.to_f - expense.to_f - reserve.to_f
        update_batch(batch,total_amount, fee, total_net, expense, invoice_amount, invoice_count, iso_amount, agent_amount, affiliate_amount, iso_split, agent_split, affiliate_split, iso_expense, agent_expense, affiliate_expense, iso_wallet, agent_wallet, affiliate_wallet, nil, 0, iso_count ,agent_count, affiliate_count, 0)
      elsif batch.blank?
        batch = TransactionBatch.where(merchant_wallet_id: merchant_wallet_id, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day, batch_type: batch_type).last
        starting_balance = SequenceLib.balance(merchant_wallet_id)
        starting_balance = number_with_precision((starting_balance.to_f + fees.to_f + self.reserve_money.to_f) - total_amount.to_f, precision: 2).to_f
        if batch.present?
          net = number_with_precision(batch.try(:total_net).to_f, precision: 2).to_f
          if net.to_f != starting_balance.to_f
            send_batch_error(receiver, merchant_wallet_id, Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, net, starting_balance)
          end
        end
        total_net = starting_balance.to_f + total_amount.to_f - fees.to_f - expenses.to_f
        batch = create_batch(total_amount, fees, total_net, expenses, starting_balance, merchant_wallet_id, nil, batch_type, invoice_amount,invoice_count, iso_wallet, iso_amount, iso_split,iso_expense, agent_wallet, agent_amount, agent_split,agent_expense, affiliate_wallet, affiliate_amount, affiliate_split,affiliate_expense, iso_balance, agent_balance, affiliate_balance, nil, 0, iso_count ,agent_count, affiliate_count, 0 )
      end
      TransactionBatchesTransactions.create(transaction_batch_id: batch.id, transaction_id: self.id) if batch.present?
    elsif self.timestamp.present? && (receiver.present? && (receiver.iso? || receiver.agent? || receiver.affiliate?)) && (self.status == "approved")
      #for partner batches
      batch_type = "sale"
      if (self.main_type == TypesEnumLib::TransactionViewTypes::B2B && self.sub_type == "invoice") || (self.main_type == TypesEnumLib::TransactionType::Transfer3DS && self.sub_type == "invoice")
        invoice_amount = self.total_amount.to_f
        invoice_count = 1
      end
      if receiver.iso?
        iso_amount = self.total_amount.to_f
        iso_wallet = self.receiver_wallet_id
        iso_count = 1
      elsif receiver.agent?
        agent_amount = self.total_amount.to_f
        agent_wallet = self.receiver_wallet_id
        agent_count = 1
      elsif receiver.affiliate?
        affiliate_amount = self.total_amount.to_f
        affiliate_wallet = self.receiver_wallet_id
        affiliate_count = 1
      end
      batch = TransactionBatch.where(iso_wallet_id: iso_wallet, agent_wallet_id: agent_wallet, affiliate_wallet_id: affiliate_wallet, merchant_wallet_id: nil, batch_date: self.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: batch_type).last
      if batch.present?
        update_batch(batch,0, 0, 0, 0, invoice_amount, invoice_count, iso_amount, agent_amount, affiliate_amount, 0, 0, 0, 0, 0, 0,iso_wallet,agent_wallet,affiliate_wallet,nil,0, iso_count ,agent_count, affiliate_count, 0)
      else
        if receiver.iso?
          iso_balance = SequenceLib.balance(iso_wallet)
          iso_balance = iso_balance.to_f - iso_amount.to_f
          get_partner_balance(iso_wallet, "iso", iso_balance, receiver)
        elsif receiver.agent?
          agent_balance = SequenceLib.balance(agent_wallet)
          agent_balance = agent_balance.to_f - agent_amount.to_f
          get_partner_balance(agent_wallet, "agent", agent_balance, receiver)
        elsif receiver.affiliate?
          affiliate_balance = SequenceLib.balance(affiliate_wallet)
          affiliate_balance = affiliate_balance.to_f - affiliate_amount.to_f
          get_partner_balance(affiliate_wallet, "affiliate", affiliate_balance, receiver)
        end
        batch = create_batch(0, 0, 0, 0, 0, nil, nil, batch_type, invoice_amount,invoice_count, iso_wallet, iso_amount, iso_split,0, agent_wallet, agent_amount, 0,0, affiliate_wallet, affiliate_amount, 0,0, iso_balance, agent_balance, affiliate_balance,nil,0, iso_count ,agent_count, affiliate_count, 0)
      end
      TransactionBatchesTransactions.create(transaction_batch_id: batch.id, transaction_id: self.id) if batch.present?
    end
  end

  def record_transaction
    return unless self.user_id.present? && self.amount.present?

    if self.action == 'issue'
      activity = ActivityLog.new
      receiver = User.find(self.user_id)
      message = "$ #{number_with_precision(self.amount.to_f, :precision => 2)} has been deposited into your account at"
      activity.note = message
      activity.transaction_id = self.id
      activity.user = receiver
      activity.save
    elsif self.action == 'transfer'
      activity = ActivityLog.new
      receiver = User.find(self.user_id).name
      sender = User.find(self.sender_id)
      message = "#{sender.name} has transfered $ #{number_with_precision(self.amount.to_f, :precision => 2) } to #{receiver} at"
      activity.note = message
      activity.transaction_id = self.id
      activity.user = sender
      activity.save
    elsif self.action == 'retire'
      activity = ActivityLog.new
      message = nil
      if self.user_id.present?
        # receiver = User.find(self.user_id).name
        sender = User.find(self.sender_id).name
        message = "#{self.amount} has been withdraw from #{sender} at #{self.created_at} "
        # message = "#{sender} has with $ #{self.amount} to #{receiver} at #{self.created_at} "
      end
      activity.note = message
      activity.transaction_id = self.id
      activity.save
    end
  end

  def create_events
    if self.seq_transaction_id.present?
      transaction = self
      if transaction.present?
        actions = transaction.tags
        if actions.present?
          tags = actions
          if tags.present?
            if tags["fee_perc"].present? && tags["fee_perc"]["iso"].present? && tags["fee_perc"]["iso"]["iso_buyrate"].present? && tags["fee_perc"]["iso"]["amount"].to_f > 0
              event = EventLog.new
              event.message = "Transfered $#{tags["fee_perc"]["iso"]["amount"]} from #{tags["fee_perc"]["iso"]["name"]} to Escrow Wallet."
              event.transaction_id = self.id
              event.save
              event = EventLog.new
              event.message = "Transfered $#{tags["fee_perc"]["iso"]["amount"]} from Escrow to Gbox"
              event.transaction_id = self.id
              event.save
            end
            activities = []
            if tags['fee'].present? && tags['fee'].to_f > 0 && tags["merchant"].present?
              event = EventLog.new
              event.message = "Transfered $#{tags["fee"]} from #{tags["merchant"]["name"]} to Escrow Wallet."
              event.transaction_id = self.id
              activities << event
            end
            if tags["fee_perc"].present? && tags["fee_perc"]["gbox"].present? && tags["fee_perc"]["gbox"]["amount"].present? && tags["fee_perc"]["gbox"]["amount"].to_f > 0
              event = EventLog.new
              event.message = "Transfered $#{tags["fee_perc"]["gbox"]["amount"]} from Escrow Wallet to Gbox."
              event.transaction_id = self.id
              activities << event
            end
            if tags["fee_perc"].present? && tags["fee_perc"]["iso"].present? && tags["fee_perc"]["iso"]["amount"].present? && tags["fee_perc"]["iso"]["amount"].to_f > 0
              event = EventLog.new
              event.message = "Transfered $#{tags["fee_perc"]["iso"]["amount"]} from Escrow Wallet to #{tags["fee_perc"]["iso"]["name"]}."
              event.transaction_id = self.id
              activities << event

              if tags["fee_perc"]["agent"].present? && tags["fee_perc"]["agent"]["amount"].present? && tags["fee_perc"]["agent"]["amount"].to_f > 0
                event = EventLog.new
                event.message = "Transfered $#{tags["fee_perc"]["agent"]["amount"]} from #{tags["fee_perc"]["iso"]["name"]} to #{tags["fee_perc"]["agent"]["name"]}."
                event.transaction_id = self.id
                activities << event

              end
              if tags["fee_perc"]["affiliate"].present? && tags["fee_perc"]["affiliate"]["amount"].present? && tags["fee_perc"]["affiliate"]["amount"].to_f > 0
                event = EventLog.new
                event.message = "Transfered $#{tags["fee_perc"]["affiliate"]["amount"]} from #{tags["fee_perc"]["agent"]["name"]} to #{tags["fee_perc"]["affiliate"]["name"]}."
                event.transaction_id = self.id
                activities << event
              end
            end
            if activities.present? && activities.count > 0
              EventLog.import activities
            end
          end
        end
      end
    end
  end

  def transaction_events
    if self.timestamp.present?
      transaction = self
      if transaction.status == "approved"
        existing_event = EventLog.where(transaction_id: self.id,event_type: "transaction_created")
        if existing_event.empty?
          event = EventLog.new(transaction_id: self.id,message: "Transaction Created",event_type: "transaction_created")
          event.save
        end
      end
      if transaction.status == "refunded"
        existing_event = EventLog.where(transaction_id: self.id,event_type: "transaction_refunded")
        if existing_event.empty?
          event = EventLog.new(transaction_id: self.id,message: "Transaction Refunded",event_type: "transaction_refunded")
          event.save
        end
      end
      if transaction.status == "complete"
        existing_event = EventLog.where(transaction_id: self.id,event_type: "transaction_partial_refunded")
        if existing_event.empty?
          event = EventLog.new(transaction_id: self.id,message: "Partial Refunded",event_type: "transaction_partial_refunded")
          event.save
        end
      end
    end
  end

  def get_commission_split(tag, wallet, role)
    split = 0
    if tag["super_company"].present?
      if tag["super_company"]["0"].present?
        if tag["super_company"]["0"]["from"] == wallet
          split = split.to_f + tag["super_company"].values.pluck("amount").try(:sum, &:to_f).to_f
        end
      elsif tag["super_company"]["from"].present? && tag["super_company"]["from"] == wallet
        split = split.to_f + tag["super_company"]["amount"].to_f
      end
    end
    if tag["super_users_details"].present?
      tag["super_users_details"].each do |obj|
        if role == "iso"
          if obj["user_1_details"].present? && obj["user_1_details"]["from_wallet"] == wallet
            split = split.to_f + obj["user_1_details"]["dollar"].to_f + obj["user_1_details"]["percent"].to_f
          end
        else
          if obj["user_2_details"].present? && obj["user_2_details"]["from_wallet"] == wallet
            split = split.to_f + obj["user_2_details"]["dollar"].to_f + obj["user_2_details"]["percent"].to_f
          end
          if obj["user_3_details"].present? && obj["user_3_details"]["from_wallet"] == wallet
            split = split.to_f + obj["user_3_details"]["dollar"].to_f + obj["user_3_details"]["percent"].to_f
          end
        end
      end
    end
    if role == "iso"
      if tag["agent"].try(:[], "amount").present?
        split = split.to_f + tag["agent"]["amount"].to_f
      end
      if tag["agent"].try(:[], "misc").present? && tag["agent"].try(:[], "misc").to_f > 0
        split = split.to_f + tag["agent"]["misc"].to_f
      end
      if tag["agent"].try(:[], "service").present? && tag["agent"].try(:[], "service").to_f > 0
        split = split.to_f + tag["agent"]["service"].to_f
      end
      if tag["agent"].try(:[], "statement").present? && tag["agent"].try(:[], "statement").to_f > 0
        split = split.to_f + tag["agent"]["statement"].to_f
      end
    elsif role == "agent"
      if tag["affiliate"].try(:[], "amount").present?
        split = split.to_f + tag["affiliate"]["amount"].to_f
      end
      if tag["affiliate"].try(:[], "misc").present? && tag["affiliate"].try(:[], "misc").to_f > 0
        split = split.to_f + tag["affiliate"]["misc"].to_f
      end
      if tag["affiliate"].try(:[], "service").present? && tag["affiliate"].try(:[], "service").to_f > 0
        split = split.to_f + tag["affiliate"]["service"].to_f
      end
      if tag["affiliate"].try(:[], "statement").present? && tag["affiliate"].try(:[], "statement").to_f > 0
        split = split.to_f + tag["affiliate"]["statement"].to_f
      end
    end
    split
  end

  def get_commission_earn(tag, wallet)
    split = 0
    if tag["super_company"].present?
      if tag["super_company"]["0"].present?
        tag["super_company"].each do |key,obj|
          if obj["wallet"] == wallet
            split = split.to_f + obj["amount"].to_f
          end
        end
      elsif tag["super_company"]["wallet"].present? && tag["super_company"]["wallet"] == wallet
        split = split.to_f + tag["super_company"]["amount"].to_f
      end
    end
    if tag["super_users_details"].present?
      tag["super_users_details"].each do |obj|
        if obj["user_1_details"].present? && obj["user_1_details"]["wallet"] == wallet
          split = split.to_f + obj["user_1_details"]["dollar"].to_f + obj["user_1_details"]["percent"].to_f
        end
        if obj["user_2_details"].present? && obj["user_2_details"]["wallet"] == wallet
          split = split.to_f + obj["user_2_details"]["dollar"].to_f + obj["user_2_details"]["percent"].to_f
        end
        if obj["user_3_details"].present? && obj["user_3_details"]["wallet"] == wallet
          split = split.to_f + obj["user_3_details"]["dollar"].to_f + obj["user_3_details"]["percent"].to_f
        end
      end
    end
    if tag["net_super_company"].present? && tag["net_super_company"]["wallet"] == wallet
      split = split + tag["net_super_company"]["amount"].to_f
    end
    split
  end

  def create_batch(total_amount, fees, total_net, expenses, starting_balance, merchant_wallet_id, merchant_id, batch_type, invoice_amount,invoice_count, iso_wallet, iso_amount, iso_split,iso_expense, agent_wallet, agent_amount, agent_split,agent_expense, affiliate_wallet, affiliate_amount, affiliate_split,affiliate_expense, iso_balance, agent_balance, affiliate_balance, refund_count = nil, refund_amount = nil, iso_count = nil, agent_count = nil, affiliate_count = nil, reserve_amount = nil)
    split_iso = iso_split.to_f
    split_agent = agent_split.to_f
    split_affiliate = affiliate_split.to_f
    TransactionBatch.create(
        total_amount: total_amount.to_f,
        total_fee: fees.to_f,
        total_net: total_net.to_f,
        total_trxs: 1,
        expenses: expenses.to_f,
        starting_balance: starting_balance.to_f,
        batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date,
        merchant_wallet_id: merchant_wallet_id,
        merchant_id: merchant_id,
        batch_type: batch_type,
        invoice_amount: invoice_amount.to_f,
        invoice_count: invoice_count.to_i,
        iso_wallet_id: iso_wallet,
        iso_commission: iso_amount.to_f,
        agent_wallet_id: agent_wallet,
        agent_commission: agent_amount.to_f,
        affiliate_wallet_id: affiliate_wallet,
        affiliate_commission: affiliate_amount.to_f,
        iso_split: split_iso.to_f,
        agent_split: split_agent.to_f,
        affiliate_split: split_affiliate.to_f,
        iso_expense: iso_expense.to_f,
        agent_expense: agent_expense.to_f,
        affiliate_expense: affiliate_expense.to_f,
        iso_balance: iso_balance.to_f,
        agent_balance: agent_balance.to_f,
        affiliate_balance: affiliate_balance.to_f,
        refund_count: refund_count.to_i ,
        refund_amount: refund_amount.to_f,
        iso_count: iso_count.to_i,
        agent_count: agent_count.to_i,
        affiliate_count: affiliate_count.to_i,
        reserve: reserve_amount.to_f
    )
  end

  def update_batch(batch,total_amount, fee, total_net, expense, invoice_amount, invoice_count, iso_amount, agent_amount, affiliate_amount, iso_split, agent_split, affiliate_split, iso_expense, agent_expense, affiliate_expense, iso_wallet_id, agent_wallet_id, affiliate_wallet_id, refund_count = nil, refund_amount = nil, iso_count = nil, agent_count = nil, affiliate_count = nil, reserve_amount = nil)
    split_iso = iso_split.to_f + batch.iso_split.to_f
    split_agent = agent_split.to_f + batch.agent_split.to_f
    split_affiliate = affiliate_split.to_f.to_f + batch.affiliate_split.to_f
    if iso_wallet_id.present?
      iso_wallet = iso_wallet_id
    elsif batch.iso_wallet_id.present? && iso_wallet_id.blank?
      iso_wallet = batch.iso_wallet_id
    end
    if agent_wallet_id.present?
      agent_wallet = agent_wallet_id
    elsif batch.agent_wallet_id.present? && agent_wallet_id.blank?
      agent_wallet = batch.agent_wallet_id
    end
    if affiliate_wallet_id.present?
      affiliate_wallet = affiliate_wallet_id
    elsif batch.affiliate_wallet_id.present? && affiliate_wallet_id.blank?
      affiliate_wallet = batch.affiliate_wallet_id
    end
    batch.update(
        total_amount: batch.total_amount.to_f + total_amount.to_f,
        total_fee: fee.to_f,
        total_net: total_net,
        total_trxs: batch.total_trxs.to_i + 1,
        expenses: expense.to_f,
        invoice_amount: batch.invoice_amount.to_f + invoice_amount.to_f,
        invoice_count: batch.invoice_count.to_i + invoice_count.to_i,
        iso_commission: iso_amount.to_f + batch.iso_commission.to_f,
        agent_commission: agent_amount.to_f + batch.agent_commission.to_f,
        affiliate_commission: affiliate_amount.to_f + batch.affiliate_commission.to_f,
        iso_split: split_iso.to_f,
        agent_split: split_agent.to_f,
        affiliate_split: split_affiliate.to_f,
        iso_expense: iso_expense.to_f + batch.iso_expense.to_f,
        agent_expense: agent_expense.to_f + batch.agent_expense.to_f,
        affiliate_expense: affiliate_expense.to_f + batch.affiliate_expense.to_f,
        iso_wallet_id: iso_wallet,
        agent_wallet_id: agent_wallet,
        affiliate_wallet_id: affiliate_wallet,
        refund_count: refund_count.to_i + batch.refund_count.to_i,
        refund_amount: refund_amount.to_f + batch.refund_amount.to_f,
        iso_count: batch.iso_count.to_i + iso_count.to_i,
        agent_count: batch.agent_count.to_i + agent_count.to_i,
        affiliate_count: batch.affiliate_count.to_i + affiliate_count.to_i,
        reserve: batch.reserve.to_f + reserve_amount.to_f
    )
  end

  def get_partner_balance(wallet_id, role, today_balance, user)
    if role == "iso"
      batches = TransactionBatch.where(iso_wallet_id: wallet_id, batch_date: Date.today.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).order(created_at: :asc)
      if batches.present?
        earnings = batches.pluck(:iso_commission).try(:compact).try(:sum).to_f
        split = batches.pluck(:iso_split).try(:compact).try(:sum).to_f
        expense = batches.pluck(:iso_expense).try(:compact).try(:sum).to_f
        balance = batches.select{|e| e.iso_balance != 0}.try(:first).try(:iso_balance).to_f
        net = number_with_precision((balance.to_f + earnings).to_f - expense.to_f - split.to_f, precision: 2).to_f
        if net.to_f != number_with_precision(today_balance.to_f, precision: 2).to_f
          send_batch_error(user, wallet_id, Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, net, today_balance)
        end
      else
        net = 0
      end
    elsif role == "agent"
      batches = TransactionBatch.where(agent_wallet_id: wallet_id, batch_date: Date.today.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).order(created_at: :asc)
      if batches.present?
        earnings = batches.pluck(:agent_commission).try(:compact).try(:sum).to_f
        split = batches.pluck(:agent_split).try(:compact).try(:sum).to_f
        expense = batches.pluck(:agent_expense).try(:compact).try(:sum).to_f
        balance = batches.select{|e| e.agent_balance != 0}.try(:first).try(:agent_balance).to_f
        net = number_with_precision((balance.to_f + earnings).to_f - expense.to_f - split.to_f, precision: 2).to_f
        if net.to_f != number_with_precision(today_balance.to_f, precision: 2).to_f
          send_batch_error(user, wallet_id, Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, net, today_balance)
        end
      else
        net = 0
      end
    elsif role == "affiliate"
      batches = TransactionBatch.where(affiliate_wallet_id: wallet_id, batch_date: Date.today.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).order(created_at: :asc)
      if batches.present?
        earnings = batches.pluck(:affiliate_commission).try(:compact).try(:sum).to_f
        split = batches.pluck(:affiliate_split).try(:compact).try(:sum).to_f
        expense = batches.pluck(:affiliate_expense).try(:compact).try(:sum).to_f
        balance = batches.select{|e| e.affiliate_balance != 0}.try(:first).try(:affiliate_balance).to_f
        net = number_with_precision((balance.to_f + earnings).to_f - expense.to_f - split.to_f, precision: 2).to_f
        if net.to_f != number_with_precision(today_balance.to_f, precision: 2).to_f
          send_batch_error(user, wallet_id, Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, net, today_balance)
        end
      else
        net = 0
      end
    end
    net
  end

  def send_batch_error(user, wallet, date, net, today_balance)
    msg = "*Balance and net does not match* \n url: \`\`\`#{ENV["APP_ROOT"]}\`\`\` user_id: \`\`\`#{user.id}\`\`\`  role: \`\`\`#{user.role}\`\`\`  wallet_id: \`\`\`#{wallet}\`\`\` previous_net: \`\`\` #{net.to_f} \`\`\` today_balance: \`\`\` #{today_balance.to_f} \`\`\` batch_date: \`\`\` #{date} \`\`\`"
    SlackService.notify(msg, "#qcbatches", true)
  end

  def set_quickcard_id
    self.quickcard_id = generate_quickcard_id
  end

  def generate_quickcard_id
    loop do
      token = "QC#{SecureRandom.urlsafe_base64(21)}"
      break token unless Transaction.exists?(quickcard_id: token)
    end
  end

  SEARCH_TYPES = {
    refund: [I18n.t('search_types.refund'),I18n.t('search_types.refund_bank')],
    checks: [I18n.t('search_types.send_check'),I18n.t('search_types.void_check'),I18n.t('search_types.failed_check'),I18n.t('search_types.bulk_check')],
    ach: [I18n.t('search_types.ach_deposit'),I18n.t('search_types.ach'),I18n.t('search_types.failed_ach'),I18n.t('search_types.void_ach')],
    push_to_card: [I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.push_to_card'),I18n.t('search_types.void_p2c')],
    decline: [I18n.t('search_types.credit_card'),I18n.t('search_types.ecommerce'),I18n.t('search_types.sale_issue'),I18n.t('search_types.virtual_terminal'),I18n.t('search_types.rtp'),I18n.t('search_types.invoice_rtp')],
    success: [I18n.t('search_types.ach'),I18n.t('search_types.ach_deposit'),I18n.t('search_types.account_transfer'),I18n.t('search_types.afp'),I18n.t('search_types.buy_rate_fee'),I18n.t('search_types.cbk_dispute_accepted'),I18n.t('search_types.cbk_fee'),I18n.t('search_types.cbk_hold'),I18n.t('search_types.cbk_lost'),I18n.t('search_types.cbk_lost_retire'),I18n.t('search_types.cbk_won'),I18n.t('search_types.credit_card'),I18n.t('search_types.debit_card'),I18n.t('search_types.ecommerce'),I18n.t('search_types.failed_check'),I18n.t('search_types.failed_ach'),I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.giftcard_purchase'),I18n.t('search_types.invoice_credit_card'),I18n.t('search_types.invoice_debit_card'),I18n.t('search_types.invoice_qc'),I18n.t('search_types.rtp'),I18n.t('search_types.invoice_rtp'),I18n.t('search_types.misc_fee'),I18n.t('search_types.misc_fee'),I18n.t('search_types.service_fee'),I18n.t('search_types.pin_debit'),I18n.t('search_types.push_to_card'),I18n.t('search_types.paid_tip'),I18n.t('search_types.qr_scan'),I18n.t('search_types.qcp_secure'),I18n.t('search_types.qc_secure'),I18n.t('search_types.refund'),I18n.t('search_types.refund_failed'),I18n.t('search_types.reserve_money_deposit'),I18n.t('search_types.reserve_money_return'),I18n.t('search_types.retrievel_fee'),I18n.t('search_types.slae_giftcard'),I18n.t('search_types.sale_issue'),I18n.t('search_types.sale_tip'),I18n.t('search_types.send_check'),I18n.t('search_types.subscription_fee'),I18n.t('search_types.virtual_terminal'),I18n.t('search_types.void_ach'),I18n.t('search_types.void_check'),I18n.t('search_types.void_p2c')]
  }

  EVENTS = [I18n.t('event_types.account_transfer'),I18n.t('event_types.token'),I18n.t('event_types.retrieve_token'),I18n.t('event_types.virtual_transaction'),I18n.t('event_types.virtual_terminal_transaction'),I18n.t('event_types.refund_money'),I18n.t('event_types.virtual_debit_charge'),I18n.t('event_types.tango_order'),I18n.t('event_types.deposit'),I18n.t('event_types.affiliate'),I18n.t('event_types.admin_user_create'),I18n.t('event_types.get_transaction_info'),I18n.t('event_types.get_product_info')]


  ISO_SEARCH_TYPES = {
      refund: [I18n.t('search_types.refund'),I18n.t('search_types.refund_bank')],
      checks: [I18n.t('search_types.send_check'),I18n.t('search_types.void_check'),I18n.t('search_types.bulk_check')],
      reserve: [I18n.t('search_types.ach'),I18n.t('search_types.ach_deposit'),I18n.t('search_types.account_transfer'),I18n.t('search_types.buy_rate_fee'),I18n.t('search_types.failed_check'),I18n.t('search_types.failed_ach'),I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.fee_transfer'),I18n.t('search_types.giftcard_only'),I18n.t('search_types.qcp_secure_'),I18n.t('search_types.invoice_qc'),I18n.t('search_types.misc_fee'),I18n.t('search_types.service_fee'),I18n.t('search_types.push_to_card_'),I18n.t('search_types.send_check'),I18n.t('search_types.subscription_fee'),I18n.t('search_types.void_ach'),I18n.t('search_types.void_push_to_card'),I18n.t('search_types.void_check')],
      partner_types: [I18n.t('search_types.ach'),I18n.t('search_types.ach_deposit'),I18n.t('search_types.account_transfer'),I18n.t('search_types.buy_rate_fee'),I18n.t('search_types.failed_check'),I18n.t('search_types.failed_ach'),I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.fee_transfer'),I18n.t('search_types.giftcard_purchase'),I18n.t('search_types.qcp_secure_'),I18n.t('search_types.invoice_qc'),I18n.t('search_types.misc_fee'),I18n.t('search_types.push_to_card_'),I18n.t('search_types.send_check'),I18n.t('search_types.subscription_fee'),I18n.t('search_types.void_ach'),I18n.t('search_types.void_push_to_card'),I18n.t('search_types.void_check')],
      partner_types1: [I18n.t('search_types.ach_'),I18n.t('search_types.ach_deposit'),I18n.t('search_types.cbk_fee'),I18n.t('search_types.credit_card_'),I18n.t('search_types.debit_card_'),I18n.t('search_types.eCommerce_'),I18n.t('search_types.failed_check'),I18n.t('search_types.failed_ach'),I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.giftcard_purchase_'),I18n.t('search_types.invoice_rtp'),I18n.t('search_types.misc_fee'),I18n.t('search_types.monthly_fee'),I18n.t('search_types.push_to_card'),I18n.t('search_types.rtp'),I18n.t('search_types.send_check_'),I18n.t('search_types.subscription_fee'),I18n.t('search_types.virtual_terminal_sale')],
      partner_types_for_batch: [I18n.t('search_types.ach_deposit'),I18n.t('search_types.cbk_fee'),I18n.t('search_types.credit_card_'),I18n.t('search_types.debit_charger'),I18n.t('search_types.eCommerce_'),I18n.t('search_types.failed_check'),I18n.t('search_types.failed_ach'),I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.giftcard_purchase_'),I18n.t('search_types.invoice_rtp'),I18n.t('search_types.misc_fee'),I18n.t('search_types.monthly_fee'),I18n.t('search_types.push_to_card'),I18n.t('search_types.rtp'),I18n.t('search_types.send_check_'),I18n.t('search_types.subscription_fee'),I18n.t('search_types.virtual_terminal_sale')],
      merchant_txn_type: [I18n.t('search_types.ach_'),I18n.t('search_types.ach_deposit'),I18n.t('search_types.cbk_fee'),I18n.t('search_types.credit_card_'),I18n.t('search_types.debit_card'),I18n.t('search_types.invoice_rtp'),I18n.t('search_types.eCommerce_'),I18n.t('search_types.failed_check'),I18n.t('search_types.failed_ach'),I18n.t('search_types.failed_push_to_card'),I18n.t('search_types.giftcard_only'),I18n.t('search_types.misc_fee'),I18n.t('search_types.monthly_fee'),I18n.t('search_types.push_to_card'),I18n.t('search_types.rtp'),I18n.t('search_types.send_check'),I18n.t('search_types.subscription_fee'),I18n.t('search_types.virtual_terminal_sale')],
      merchant_txn_type_others: [I18n.t('search_types.ach_small'),I18n.t('search_types.void_ach_'),I18n.t('search_types.ach_deposit'),I18n.t('search_types.push_to_card'),I18n.t('search_types.send_check_'),I18n.t('search_types.void_check'),I18n.t('search_types.giftcard_purchase')],
      ledger_user_types: [I18n.t('search_types.credit_card'),I18n.t('search_types.debit_card'),I18n.t('search_types.ecommerce'),I18n.t('search_types.service_fee'),I18n.t('search_types.sale_issue'),I18n.t('search_types.virtual_terminal'),I18n.t('search_types.refund_bank'),I18n.t('search_types.debit_charge'),I18n.t('search_types.qr_scan_'),I18n.t('search_types.fee_transfer'), I18n.t('search_types.cbk_hold_'), I18n.t('search_types.cbk_lost_'),I18n.t('search_types.cbk_won_'),I18n.t('search_types.subscription_fee')],
      merchant_txn_types_qc: [I18n.t('search_types.credit_card'),I18n.t('search_types.debit_card'),I18n.t('search_types.eCommerce_'),I18n.t('search_types.invoice_rtp'),I18n.t('search_types.service_fee'),I18n.t('search_types.debit_charge'),I18n.t('search_types.qr_scan_3ds'),I18n.t('search_types.refund_bank'),I18n.t('search_types.rtp'),I18n.t('search_types.sale_issue'),I18n.t('search_types.virtual_terminal_sale'),I18n.t('search_types.subscription_fee')]
  }

end
