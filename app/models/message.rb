class Message < ApplicationRecord
  belongs_to :recipient, class_name: "User",foreign_key: "recipient_id"
  belongs_to :actor, class_name: "User",foreign_key: "actor_id"
  belongs_to :message_transaction,class_name: "Transaction",foreign_key: "transaction_id"
  has_many :attachments ,as: :imageable, class_name: "Documentation" , dependent: :destroy
  accepts_nested_attributes_for :attachments

end
