class BulkCheck < ApplicationRecord
  belongs_to :bulk_check_instance
  belongs_to :tango_order, optional: true, dependent: :destroy

     scope :search_by_status, lambda {|search|
	    status_array = search.select{|k,v| v=="1"}.keys.map{|a| a.upcase} if search.present?
	    if status_array.present?
	      if status_array.include?("ALL") && status_array.count == 1
	        where(id: BulkCheck.all)
	      else
	      	joins(:tango_order).where("tango_orders.status IN (?) " ,status_array )
				end
	    end
	}

	scope :before_search_by_status, lambda {|search|
	    status_array = search.select{|k,v| v=="1"}.keys.map{|a| a.upcase} if search.present?
	    if status_array.present?
				if !status_array.include?("NO_SELECT")
					if status_array.include?("READY")
						where(status: "Ready" )
					elsif status_array.include?("PENDING")
						where(status: "Pending" )
					end
				else
					where.not(id: BulkCheck.all)
				end
	    end
	}

end
