class Product < ApplicationRecord
  belongs_to :product_transaction, class_name: "Transaction", foreign_key: :transaction_id, optional: true
  belongs_to :request, optional: true
  belongs_to :customer_dispute , optional: true
end
