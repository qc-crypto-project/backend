# == Schema Information
#
# Table name: releases
#
#  id         :bigint(8)        not null, primary key
#  version    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  note       :string
#

class Release < ApplicationRecord
end
