class LoadBalancerRule < ApplicationRecord
  include ActionView::Helpers::NumberHelper
  #relations
  belongs_to :load_balancer
  # has_many :payment_gateways

  #enum arrays
  enum operator: [
      :equal_to,
      :not_equal_to,
      :greater_than,
      :less_than
  ]
  enum rule_type: [
      :notification,
      :automation
  ]

  enum identifier: {
      :rotate_processor => 0,
      :rotate_processor_with_amount => 1,
      :rotate_processor_with_cc => 2,
      :block_card_for_decline_count => 3,
      :processor_daily_volume => 4,
      :processor_monthly_volume => 5,
      :group_daily_volume => 6,
      :group_monthly_volume => 7,
      :block_processor_daily_volume => 8,
      :processor_decline_percent => 9,
      :processor_decline_times => 10,
      :decline_on_cc_country => 11,
      :block_card_for_reason => 12,
      :rotate_processor_on_cc_brand => 13,
      :block_card_for_processed_count => 14
  }

  enum status: [:active, :inactive]

  # validations
  validates :identifier, presence: true
  validates :rule_type, presence: true

  attr_accessor :rules

  def rules
    @rules = [
        {rule_type: "automation", identifier: "rotate_processor", text: "When transaction come in rotate every <span class='blue-color underline'>number</span> transaction/s between all processor."},
        {rule_type: "automation", identifier: "rotate_processor_with_amount", text: "If transaction amount <span class='blue-color underline'> <=> </span><span class='blue-color underline'>$</span>
            send transaction to <span class='blue-color underline'>descriptor/s</span> and rotate
            every <span class='blue-color underline'>number</span> transaction/s between processor/s."},
        {rule_type: "automation", identifier: "rotate_processor_with_cc", text: "If a C.C # been process <span class='blue-color underline'>number</span>
            time/s on the same processor, rotate to the next process."},
        {rule_type: "automation", identifier: "block_card_for_decline_count", text: "If a C.C # been Decline <span class='blue-color underline'>number</span> time/s on the same group block the C.C for <span class='blue-color underline'>number</span> Hour/s."},
        {rule_type: "automation", identifier: "block_processor_daily_volume", text: "When a processor get to <span class='blue-color underline'>number</span> percentage of total Daily volume, block processor until the end of the day (PST) from receiving transaction."},
        {rule_type: "automation", identifier: "decline_on_cc_country", text: "If Credit card country <span class='blue-color underline'> = ⍯ </span><span class='blue-color underline'> country</span> Decline the transaction."},
        {rule_type: "automation", identifier: "block_card_for_reason", text: "If C.C # got decline for <span class='blue-color underline'> reason </span>, block the card for<span class='blue-color underline'> number</span> hour/s or Move the card to blacklist."},
        {rule_type: "automation", identifier: "rotate_processor_on_cc_brand", text: "If Credit card brand <span class='blue-color underline'> = ⍯ </span><span class='blue-color underline'>brand</span>
            send transaction to <span class='blue-color underline'>descriptor/s</span> and rotate
            every <span class='blue-color underline'>number</span> transaction/s between processor/s."},
        {rule_type: "automation", identifier: "block_card_for_processed_count", text: "If a C.C # been process <span class='blue-color underline'>number</span> time/s on the same group block him for <span class='blue-color underline'>number</span> Hour/s."},
        {rule_type: "notification", identifier: "processor_daily_volume", text: "If a processor get to <span class='blue-color underline'>number</span> percentage of total Daily volume send Slack alert."},
        {rule_type: "notification", identifier: "processor_monthly_volume", text: "If a processor get to <span class='blue-color underline'>number</span> percentage of total Monthly volume send Slack alert."},
        {rule_type: "notification", identifier: "group_daily_volume", text: "If a group get to <span class='blue-color underline'>number</span> percentage of total Daily volume send Slack alert."},
        {rule_type: "notification", identifier: "group_monthly_volume", text: "If a group get to <span class='blue-color underline'>number</span> percentage of total Monthly volume send Slack alert."},
        {rule_type: "notification", identifier: "processor_decline_times", text: "If a processor decline <span class='blue-color underline'>number</span> time/s  in a row, send slack alert with decline messages."},
        {rule_type: "notification", identifier: "processor_decline_percent", text: "If a processor decline percentage is <span class='blue-color underline'><=></span> <span class='blue-color underline'>number</span> % in <span class='blue-color underline'>number</span> hour/s send alert to Slack."},

    ]
  end

  def get_rule_message
    case self.identifier
    when "rotate_processor"
      "<div class='rotate-every'>When transaction come in rotate every <span class='blue-color underline'>#{self.count}</span> transaction/s between all processor.</div>"
    when "rotate_processor_with_amount"
      "<div class='rotate-every'>If transaction amount <span class='blue-color underline'> #{self.operator} </span><span class='blue-color underline'>$ #{ number_with_precision(self.amount,precision: 2, delimiter: ',') }</span>
            send transaction to <span class='blue-color underline'>#{self.payment_gateways.pluck(:name).join(', ')}</span> and rotate
            every <span class='blue-color underline'>#{self.count}</span> transaction/s between processor/s.</div>"
    when "rotate_processor_with_cc"
      "<div class='rotate-every'>If a C.C # been process <span class='blue-color underline'>#{self.count}</span>
            time/s on the same processor, rotate to the next process.</div>"
    when "block_card_for_decline_count"
      "<div class='rotate-every'>If a C.C # been Decline <span class='blue-color underline'>#{self.count}</span> time/s on the same group block the C.C for <span class='blue-color underline'>#{self.no_of_hours}</span> Hour/s.</div>"
    when "processor_daily_volume"
      "<div class='rotate-every'>If a processor get to <span class='blue-color underline'>#{self.percentage}</span> percentage of total Daily volume send Slack alert.</div>"
    when "processor_monthly_volume"
      "<div class='rotate-every'>If a processor get to <span class='blue-color underline'>#{self.percentage}</span> percentage of total Monthly volume send Slack alert.</div>"
    when "group_daily_volume"
      "<div class='rotate-every'>If a group get to <span class='blue-color underline'>#{self.percentage}</span> percentage of total Daily volume send Slack alert.</div>"
    when "group_monthly_volume"
      "<div class='rotate-every'>If a group get to <span class='blue-color underline'>#{self.percentage}</span> percentage of total Monthly volume send Slack alert.</div>"
    when "block_processor_daily_volume"
      "<div class='rotate-every'>When a processor get to <span class='blue-color underline'>#{self.percentage}</span> percentage of total Daily volume, block processor until the end of the day (PST) from receiving transaction.</div>"
    when "decline_on_cc_country"
      country = ""
      self.country.split(',').each do|id|
        country = country + ISO3166::Country["#{id}"].name + ", "
      end
      "<div class='rotate-every'>If Credit card country <span class='blue-color underline'> #{self.operator} </span><span class='blue-color underline'> #{country[0...-2]} </span> Decline the transaction.</div>"
    when "block_card_for_reason"
      reason = ""
      self.decline_reason.split(',').each do|id|
        reason = reason + Card.new.decline_reason["#{id}"] + ", "
      end
      "<div class='rotate-every'>If C.C # got decline for <span class='blue-color underline'> #{reason[0...-2]} </span>, block the card for<span class='blue-color underline'> #{self.no_of_hours}</span> hour/s or Move the card to blacklist.</div>"
    when "rotate_processor_on_cc_brand"
      "<div class='rotate-every'>If Credit card brand <span class='blue-color underline'> #{self.operator} </span><span class='blue-color underline'> #{self.card_brand}</span>
            send transaction to <span class='blue-color underline'>#{self.payment_gateways.pluck(:name).join(', ')}</span> and rotate
            every <span class='blue-color underline'>#{self.count}</span> transaction/s between processor/s.</div>"
    when "block_card_for_processed_count"
      "<div class='rotate-every'>If a C.C # been process <span class='blue-color underline'>#{self.count}</span> time/s on the same group block him for <span class='blue-color underline'>#{self.no_of_hours}</span> Hour/s.</div>"
    when "processor_decline_times"
      "<div class='rotate-every'>If a processor decline <span class='blue-color underline'>#{self.count}</span> time/s  in a row, send slack alert with decline messages.</div>"
    when "processor_decline_percent"
      "<div class='rotate-every'>If a processor decline percentage is <span class='blue-color underline'>#{self.operator}</span> <span class='blue-color underline'>#{self.percentage}</span> % in <span class='blue-color underline'>#{self.no_of_hours}</span> hour/s send alert to Slack.</div>"

    end
  end

  def payment_gateways
    ids = self.descriptor
    PaymentGateway.where(id: ids.try(:split,','))
  end

end
