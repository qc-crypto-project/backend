# == Schema Information
#
# Table name: config_log_details
#
#  id            :bigint(8)        not null, primary key
#  user_id       :bigint(8)
#  config_log_id :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class ConfigLogDetail < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :location, optional: true
  belongs_to :config_log
end
