# == Schema Information
#
# Table name: giftcards
#
#  id                :bigint(8)        not null, primary key
#  token             :string
#  exp_year          :string
#  exp_month         :string
#  wallet_id         :bigint(8)
#  is_valid          :boolean          default(TRUE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  balance           :string
#  category          :string
#  remaining_balance :string
#  pincode           :string
#  company_id        :integer
#  slug              :string
#

class Giftcard < ApplicationRecord
  include ApplicationHelper
  extend FriendlyId
  friendly_id SecureRandom.hex(3), use: :slugged

  belongs_to :company
  # after_create :create_wallet
  # before_save :generate_token

  validates :token, uniqueness: true, presence: true
  validates :company, presence: true
  validates :pincode, presence: true

  def generate_token
    self.token = loop do
      random_token = 16.times.map{rand(10)}.join # rand(1_000_00_00_00_00_00_00..9_999_99_99_99_99_99_99)
      break random_token unless Giftcard.exists?(token: random_token)
    end
    self.pincode = loop do
      random_token = 6.times.map{rand(10)}.join # rand(0_000_01..9_999_99)
      break random_token unless Giftcard.exists?(pincode: random_token)
    end
    self.exp_year = (Time.now.year + 1) if self.exp_year.nil?
    self.exp_month = Time.now.month if self.exp_month.nil?
    SequenceLib.create(self.token, 'Giftcard')
  end
end
