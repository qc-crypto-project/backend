class Asset < ApplicationRecord
  has_attached_file :file , default_url: "<%= asset_path('images/world.jpg') %>"
  validates_attachment_content_type :file, content_type: ["application/zip"]
  validates :name,:file, presence: true
  validates :name, uniqueness: true
end
