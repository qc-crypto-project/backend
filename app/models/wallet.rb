# == Schema Information
#
# Table name: wallets
#
#  id                    :bigint(8)        not null, primary key
#  name                  :string
#  type                  :string
#  user_id               :bigint(8)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  qr_image_file_name    :string
#  qr_image_content_type :string
#  qr_image_file_size    :integer
#  qr_image_updated_at   :datetime
#  string_file_name      :string
#  string_content_type   :string
#  string_file_size      :integer
#  string_updated_at     :datetime
#  token                 :string
#  balance               :string           default("0")
#  oauth_app_id          :bigint(8)
#  wallet_type           :integer
#  total_amount          :float
#  total_sales           :float
#  total_transactions    :integer
#  location_id           :integer
#  slug                  :string
#

class Wallet < ApplicationRecord
  has_many :transfers
  extend FriendlyId
  friendly_id :generate_custom_slug, use: :slugged
  def generate_custom_slug
    "#{name}-#{rand(1000..9999)}-#{Time.now.to_i}".parameterize
  end
  enum wallet_type: [:primary, :tip, :secondary, :qc_support, :reserve, :escrow, :charge_back,:check_escrow, :reward]

  # include SequenceExceptionHandler
  has_and_belongs_to_many :users, dependent: :destroy
  belongs_to :oauth_app, dependent: :destroy, optional: true
  validates :name, :presence => true
  has_many :giftcards, dependent: :destroy
  has_many :qr_cards, dependent: :destroy
  has_many :batches
  has_many :tango_orders
  belongs_to :location , optional: true
  has_many :transaction_batches, foreign_key: :merchant_wallet_id
  has_many :customers, class_name: "CustomerMerchant", foreign_key: :wallet_id

  has_many :sender_wallet_transactions, class_name: "BlockTransaction", foreign_key: :sender_wallet_id
  has_many :receiver_wallet_transactions, class_name: "BlockTransaction", foreign_key: :receiver_wallet_id

  has_many :sender_transactions, class_name: "Transaction", foreign_key: :sender_wallet_id
  has_many :receiver_transactions, class_name: "Transaction", foreign_key: :receiver_wallet_id




  has_many :baked_users




  has_attached_file :qr_image
  validates_attachment_content_type :qr_image, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  after_create :create_seq_wallet, :add_qr

  scope :dispensary_wallets, -> {
    Rails.cache.fetch("dispensary_wallets", expires_in: 30.minutes) do
      joins(location: :category).where("categories.name ILIKE  '%Dispensary%'").pluck(:id)
    end
  }
  scope :cbd_wallets, -> {
    Rails.cache.fetch("cbd_wallets", expires_in: 30.minutes) do
      joins(location: :category).where("categories.name ILIKE  '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
    end
  }
  scope :other_wallets, -> {
    Rails.cache.fetch("other_wallets", expires_in: 30.minutes) do
      joins(location: :category).where("categories.name ILIKE  '%CBD%' or categories.name ILIKE  '%Dispensary%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
    end
  }

  def generate_token
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    self.token = token
  end

  def render_json
    {wallet_id: self.id, wallet_name: self.name, wallet_qr: "https:#{self.qr_image}"}
  end

  def add_qr
    unless self.oauth_app_id.present?
      generate_token
      req = {"wallet_qr" => self.token}
      req_into_json = req.to_json
      barcode = Barby::QrCode.new(req_into_json, level: :q, size: 10)
      base64_output = Base64.encode64(barcode.to_png({xdim: 5}))
      data = "data:image/png;base64,#{base64_output}"
      image = Paperclip.io_adapters.for(data)
      image.original_filename = "qr_image.png"
      self.update(qr_image: image)
    end
  end

  def create_seq_wallet
    begin
      wallet = Wallet.find(self.id)
      user = wallet.users.first
      user = User.find_by(id: wallet.user_id) if user.blank?
      location = wallet.location
      if user.present?
        if user.ledger.present? && user.ledger != ENV["LEDGER_NAME"]
          qc_wallet = Wallet.qc_support.first
          qc_user = qc_wallet.users.first
          SequenceLib.create(self.id, self.wallet_type, self.name, user.phone_number, user.email, user.ledger)
          SequenceLib.create(qc_wallet.id, qc_wallet.wallet_type, qc_wallet.name, qc_user.phone_number, qc_user.email, user.ledger)
        else
          SequenceLib.create(self.id, self.wallet_type, self.name, user.phone_number, user.email)
        end
      elsif location.present?
        SequenceLib.create(self.id, self.wallet_type, self.name, location.phone_number, location.email)
      else
        SequenceLib.create(self.id, self.wallet_type)
      end
    rescue => exc
      p "---Exception Handled---", exc
      return false
    end
  end

  def update_wallet_balance?(amount,operation)
    return false if !(amount.to_f > 0)
    return false if !["add","sub"].include?(operation)
    if operation == "add"
      self.balance = self.balance.to_f + amount.to_f
      self.save!
      return true
    elsif operation == "sub"
      return false if !(self.balance.to_f > 0 && (self.balance.to_f - amount.to_f) > 0)
      self.balance = self.balance.to_f - amount.to_f
      self.save!
      return true
    end
  end

  def merchant
    return users.merchant.where(merchant_id: nil).first
  end
end
