class DisputeRequest < ApplicationRecord
  belongs_to :customer_dispute
  enum request_type: {initial_request: "initial_request",merchant_offer: "merchant_offer",merchant_final_offer: "merchant_final_offer",counter_offer: "counter_offer",return_to_sender: "return_to_sender"}
end
