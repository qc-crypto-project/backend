class Descriptor < ApplicationRecord
  has_many :payment_gateways#, -> { where.not(type: ["slot","ficer","non_bank"]) }, class_name: "PaymentGateway"
  validates :name, presence: true, uniqueness: true
end
