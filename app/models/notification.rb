# == Schema Information
#
# Table name: notifications
#
#  id              :bigint(8)        not null, primary key
#  recipient_id    :integer
#  actor_id        :integer
#  read_at         :datetime
#  action          :string
#  notifiable_id   :integer
#  notifiable_type :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  archived        :boolean          default(FALSE)
#

class Notification < ApplicationRecord
  belongs_to :recipient, class_name: "User"
  belongs_to :actor, class_name: "User"
  belongs_to :notifiable, polymorphic: true

  scope :unread, ->{ where read_at: nil }
  scope :recent, ->{ order(created_at: :desc).limit(10) }
  enum status: [:in_progress, :approved, :rejected,:cancel,:read, :refunded]

  # after_create_commit { BroadcastMessageJob.perform_later self}

  after_create_commit {NotificationRelayJob.perform_now self if self.text.present?}

  TYPE = ActiveSupport::HashWithIndifferentAccess.new(
      :transfer => 'transferred',
      :received => 'received',
      :request => 'requested',
      :generate => 'generated',
      :complete => 'completed',
      :withdraw => 'withdrawn',
      :deposit => 'deposited',
      :cancel => 'cancelled',
      :start => 'started',
      :ticket => 'created a ticket',
      :comment => 'commented',
      :transaction => 'transaction',
      :issue => 'added'
  )

  def self.notify_user(recipient, actor, notifier, action = nil, status = "in_progress",text = nil)
    status = Notification.statuses["saved"].present? ? status : "in_progress"
    Notification.create(recipient: recipient, actor: actor, action: action.present? ? action : TYPE[notifier.class.name.humanize.downcase], notifiable: notifier, status: status,text: text)
    # notifications_count = Notification.where(read_at: nil,recipient_id: actor.id, notifiable_type: "Request" ).count
    ActionCable.server.broadcast "notification_channel_#{recipient.try(:id)}", channel_name: "notification_channel_#{recipient.try(:id)}", action: 'add' , message: 1
  end
end
