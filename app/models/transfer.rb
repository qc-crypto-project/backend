class Transfer < ApplicationRecord
  extend ActionView::Helpers::NumberHelper
  extend Admins::BlockTransactionsHelper
  enum status: [:pending,:completed]

  belongs_to :user
  belongs_to :wallet, optional:true
  belongs_to :sender, class_name: "Wallet",foreign_key: "from_wallet"
  belongs_to :receiver, class_name: "Wallet",foreign_key: "to_wallet", optional: true

  def self.send_transfer
    Transfer.where(status: "pending").find_each(batch_size: 25) do |transfer|
      begin
        timezone = TIMEZONE
        timezone = transfer.transfer_timezone if transfer.transfer_timezone.present?
        today_date = Time.now.in_time_zone(timezone).to_date.strftime("%m/%d/%Y")
        transfer_date = transfer.transfer_date.to_date.strftime("%m/%d/%Y")
        if transfer_date == today_date
          transfer_details = {
            amount: transfer.amount,
            memo: transfer.memo.present? ? transfer.memo : '',
            date: transfer.transfer_date,
            transfer_timezone: timezone
          }
          user = User.find(transfer.user_id)
          if user.admin?
            user = nil
          end
          balance = SequenceLib.balance(transfer.from_wallet)
          if balance < transfer.amount.to_f
            description = {name: "update_transfer", description: "Cannot transfer Insufficient Balance"}
            cron_job = CronJob.create(name: 'update_transfer', description: description.to_json)
            cron_job.update_transfer!
          else
            db_transaction = user.transactions.create(
              to: transfer.to_wallet,
              from: transfer.from_wallet,
              status: "pending",
              action: 'transfer',
              sender: user,
              receiver_id: user.try(:id),
              sender_wallet_id: transfer.from_wallet,
              receiver_wallet_id: transfer.to_wallet,
              sender_balance: balance,
              receiver_balance: SequenceLib.balance(transfer.to_wallet),
              amount: transfer.amount.to_f,
              net_amount: transfer.amount.to_f,
              total_amount:  transfer.amount.to_f,
              main_type: "account_transfer",
              tags: transfer_details
            )
            transaction = SequenceLib.transfer(transfer.amount,transfer.from_wallet,transfer.to_wallet,"account_transfer",0,nil,TypesEnumLib::GatewayType::Quickcard,nil,user,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
            if transaction.present?
              # creating block transaction
              parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              transfer.update(status: 'completed',sequence_id: transaction.id)
              db_transaction.update(seq_transaction_id: transaction.id, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
              description = {from_wallet: transfer.from_wallet,to_wallet: transfer.to_wallet, amount: transfer.amount, memo: transfer.memo, status: transfer.status,sequence_id: transfer.sequence_id,transfer_date: transfer.transfer_date}
              cron_job = CronJob.create(name: 'update_transfer', description: description.to_json)
              cron_job.update_transfer!
            else
              description = {name: "update_transfer",description: "Cannot transfer sequence error"}
              cron_job = CronJob.create(name: 'update_transfer', description: description.to_json)
              cron_job.update_transfer!
            end
          end
        end
      rescue StandardError => exc
        cron_job = CronJob.create(name: "update_transfer", description: exc.message)
        cron_job.update_transfer!
      end
    end
  end
end
