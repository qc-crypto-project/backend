# == Schema Information
#
# Table name: cards
#
#  id         :bigint(8)        not null, primary key
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  brand      :string
#  last4      :string
#  customer   :string
#  name       :string
#  stripe     :string
#  elavon     :string
#  payeezy    :string
#  exp_date   :string
#  bridge_pay :string
#  cvv        :string
#  card_type  :string
#  fluid_pay  :string           default("")
#  archived   :boolean          default(FALSE)
#

class Card < ApplicationRecord
  belongs_to :user
  scope :instant_pay_cards, -> {where(instant_pay: true)}
  enum flag: {:straight=>"straight",:recurring=>"recurring", :initial=>"initial", :rebill=>"rebill"}
  has_many :transactions
  has_many :block_transactions

  scope :blocked_by_load_balancer_cards, -> (first6, last4) {where(first6: first6 , last4: last4, blocked_by_load_balancer: true).all}
  scope :blocked_cards_with_fingerprint, -> (fingerprint,exp_date) {where(fingerprint: fingerprint, is_blocked: true,exp_date:exp_date).all}
  scope :blocked_cards_with_number, -> (first6, last4) {where(first6: first6 , last4: last4, is_blocked: true).all}
  scope :cards_with_fingerprint_merchant_id, -> (fingerprint, merchant_id,exp_date) {where(fingerprint: fingerprint , merchant_id: merchant_id,exp_date:exp_date).all}

  audited except: [:group_transaction_attempts, :transaction_attempts]

  def self.check_vip_card?(card_number,exp_date)
    Card.exists?(first6: card_number.first(6) , last4: card_number.last(4), is_vip: true)
  end

  attr_accessor :decline_reason
  attr_accessor :card_brands

  def decline_reason
    @decline_reason = {
        "0" => "Transaction Approved Successfully",
        "1" => "Refer To Card Issuer",
        "3" => "Invalid Merchant Number",
        "4" => "Pick Up Card",
        "5" => "Do Not Honour",
        "6" => "Error",
        "12" => "Invalid Transaction",
        "13" => "Invalid Amount",
        "14" => "Invalid Card Number",
        "15" => "No Issuer",
        "22" => "Suspected Malfunction",
        "30" => "Format Error",
        "31" => "Bank Not Supported By Switch",
        "34" => "Suspected Fraud, Retain Card",
        "37" => "Contact Acquirer Security Department",
        "41" => "Lost Card",
        "42" => "No Universal Account",
        "43" => "Stolen Card",
        "49" => "The Card Was Declined",
        "51" => "Insufficient Funds",
        "54" => "Expired Card",
        "56" => "No Card Record",
        "57" => "Function Not Permitted To Cardholder",
        "58" => "Function Not Permitted To Terminal",
        "59" => "Suspected Fraud",
        "61" => "Withdrawal Limit Exceeded",
        "62" => "Restricted Card",
        "67" => "Capture Card",
        "91" => "Card Issuer Unavailable",
        "92" => "Unable To Route Transaction",
    }
  end

  def card_brands
    @card_brands = ["visa", "amex", "mastercard", "discover" , "jcb", "diners club", "world", "maestro", "cirrus"]
  end

  def check_bin
    if !self.country.present? || !self.brand.present? || !self.card_type.present?
      begin
        response = Card.neutrino_post(self.first6)
      rescue => ex
        SlackService.notify("*Error in BinList Retreivel  => #{ex.message} First6:#{self.first6} last4:#{self.last4} User id: #{self.user_id}","#quickcard-exceptions")
        response = {}
      end
      if response.present?
        response = Card.convert_hash(response)
        self.brand = response["scheme"].present? ? response["scheme"] : self.brand
        self.card_type = response["type"].present? ? response["type"] : nil
        self.country = response["country"] if self.country.blank?
        self.bank = response&.[]("bank")&.[]('name') if self.bank.blank?
      end
    end
  end

  def self.convert_hash(result)
    response = {}
    response["scheme"] = result["card-brand"].try(:downcase)
    response["type"] = result["card-type"].try(:downcase)
    response["brand"] = result["card-category"].try(:titleize)
    response["country"] = {"alpha2"=>result["country-code"],"name"=>result["country"].try(:titleize),"currency"=>result["currency-code"]}
    response["bank"] = {"name"=>result["issuer"], "phone"=>result["issuer-phone"]}
    return response
  end

  def self.neutrino_post(number)
    url = ENV["NEUTRINO_API"]
    parameters = {
      "user-id" => ENV["CARD_BIN_USER"],
      "api-key" => ENV["CARD_BIN_LIST_KEY"],
      "bin-number" => number
    }
    response = RestClient.post(url,parameters, {content_type: :json, accept: :json})
    response = JSON.parse(response)
  end

end