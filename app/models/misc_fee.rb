class MiscFee < ApplicationRecord
	belongs_to :payment_gateway
	enum misc_type: [:mid, :others]
end
