# == Schema Information
#
# Table name: tickets
#
#  id            :bigint(8)        not null, primary key
#  message       :string
#  user_id       :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  status        :string           default("untreated")
#  title         :string
#  slug          :string
#  device_type   :string
#  time_stamp    :string
#  get_attention :boolean          default(TRUE)
#

class Ticket < ApplicationRecord
  audited

  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :user
  has_many :comments
  has_many :images

  enum assigned_to: [:liron_nusinovich, :dan_nusinovich, :tom_whitley, :alyse_knight, :fredi_nisan, :ken_haller]
  enum flag: [:no_flag, :red, :blue]

  after_create :notify_user
  after_update :notify_user_update

  def notify_user
    Notification.notify_user(self.user, self.user, self)
  end

  def notify_user_update
    Notification.notify_user(self.user, self.user, self, 'update')
  end
end
