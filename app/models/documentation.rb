# == Schema Information
#
# Table name: owners
#
#  id                 :bigint(8)        not null, primary key
#  name               :string
#  ownership          :string
#  address            :string
#  city               :string
#  state              :string
#  phone_number       :string
#  zip_code           :string
#  ssn                :string
#  date_of_birth      :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :bigint(8)
#  wallet_id          :bigint(8)
#  batch_type         :integer
#  days               :integer          default(0)
#

class Documentation < ApplicationRecord

  belongs_to :imageable, polymorphic: true

  enum type: [:user_image, :w9_form_image, :schedule_a_image, :voided_check, :owner_id_card, :bank_statements, :credit_card_statements, :corporate_info, :signed_application, :domain_register_website, :w9, :ein, :ss4, :cannabis_license,:shipping_fulfillment_agreement]

  has_attached_file :document, styles:  lambda { |a| a.content_type =~ %r(image) ? {:small => "x200>", :medium => "x300>", :large => "x400>", :thumb => "100x100"}  : {} }, default_url: "/images/:style/missing.png"
  process_in_background :document, queue: "default", processing_image_url: :processing_image_fallback
  validates_attachment :document, :content_type => { :content_type => %w(image/jpeg image/jpg image/png text/plain application/pdf application/csv application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document application/zip)}

  def processing_image_fallback
    options = document.options
    options[:interpolator].interpolate(options[:url], document, :original)
  end
end
