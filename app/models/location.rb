# == Schema Information
#
# Table name: locations
#
#  id                    :bigint(8)        not null, primary key
#  business_name         :string
#  first_name            :string
#  last_name             :string
#  email                 :string
#  web_site              :string
#  address               :text
#  business_type         :string
#  years_in_business     :string
#  category              :string
#  city                  :string
#  state                 :string
#  zip                   :string
#  social_security_no    :string
#  tax_id                :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  phone_number          :string
#  merchant_id           :integer
#  location_secure_token :string
#  iso_allowed           :boolean          default(FALSE)
#  agent_allowed         :boolean          default(FALSE)
#  is_block              :boolean          default(FALSE)
#  slug                  :string
#  sale_limit            :float
#  sale_limit_percentage :float
#  ledger                :string
#  block_withdrawal      :boolean          default(FALSE)
#

class Location < ApplicationRecord
  extend FriendlyId

  audited
  friendly_id :first_name, use: :slugged

  # Model Hooks
  after_create :qr_create_wallet , :create_setting, :email_notification
  before_save :update_account_number, if: :will_save_change_to_bank_account?

  # Validations
  validates :business_name, :presence => true

  # Data Relations
  has_many :baked_users
  has_and_belongs_to_many :users
  has_many :hold_in_rears
  has_many :wallets
  has_many :bank_details
  has_one :slot
  has_many :oauth_apps
  has_many :batches
  has_many :config_logs
  has_and_belongs_to_many :fees
  has_many :documentations, as: :imageable
  has_many :trackers
  has_many :customer_disputes

  has_many :receiver_transactions , through: :wallets
  # accepts_nested_attributes_for :fees, :reject_if => proc { |attributes| attributes['fee_status']== 0 }
  accepts_nested_attributes_for :fees

  belongs_to :merchant, class_name: "User", foreign_key: "merchant_id", optional: true
  belongs_to :primary_gateway, class_name: "PaymentGateway", foreign_key: "primary_gateway_id", optional: true
  belongs_to :secondary_gateway, class_name: "PaymentGateway", foreign_key: "secondary_gateway_id", optional: true
  belongs_to :ternary_gateway, class_name: "PaymentGateway", foreign_key: "ternary_gateway_id", optional: true
  belongs_to :category, :optional => true
  belongs_to :load_balancer, :optional => true
  belongs_to :ecomm_platform, :optional => true
  # Scopes
  scope :have_gateway, ->(id){ where(primary_gateway_id: id) }
  scope :asg_pay, ->(payment_gateway_id){where("primary_gateway_id = ? or secondary_gateway_id = ? or ternary_gateway_id = ?",  payment_gateway_id, payment_gateway_id,  payment_gateway_id )}
  scope :active_with_load_balancer, ->(lb_id){ where(load_balancer_id: lb_id, is_block: false) }
  scope :inactive_with_load_balancer, ->(lb_id){ where(load_balancer_id: lb_id, is_block: true) }

  # ENUMS
  enum risk: [:high, :dispensary]
  enum bank_account_type: [:checking, :savings, :business]

  enum processing_type:  {retail: "Retail", ecommerce: "eCommerce (Products)", ecommerce_service: "eCommerce (Services)"}
  enum standalone_type: {:cultivate => 'cultivate', :charge_savy => 'charge_savy'}

  before_save :remove_blank_amounts_limit

  def remove_blank_amounts_limit
    transaction_limit_offset.reject!(&:blank?)
    transaction_limit_offset.reject!(&:zero?)
  end

  STANDALONE_URLS = HashWithIndifferentAccess.new({
    "#{Location.standalone_types[:cultivate]}": ENV.fetch("CULTIVATE_WEBHOOK_URL") { '' },
    "#{Location.standalone_types[:charge_savy]}": ENV.fetch("CHARGE_SAVY_WEBHOOK_URL") { '' }
  })

    
  # Class Methods
  def self.ransackable_scopes(auth_object = nil)
    if auth_object.try(:admin?)
      # allow admin users access to all three methods
      %i(have_gateway)
    else
      # allow other users to search on `activated` and `hired_since` only
      %i(have_gateway)
    end
  end

  # Instance Methods
  def update_account_number
    self.bank_account = AESCrypt.encrypt("#{self.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", self.bank_account) if self.bank_account.present? && self.bank_account.length < 25 && self.id.present?
  end

  def create_wallet(name, id ,type,location_id)
    begin
      wallet = Wallet.new(name: name, user_ids: id ,wallet_type: type,location_id: location_id)
      wallet.save!
      return wallet
    rescue Exception => exc
      p "---Exception Occured While Generating QR Image---", exc
    end
  end

  def ui_bank_account
    begin
      return  "****#{(AESCrypt.decrypt("#{self.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", self.bank_account)).last(4)}"
    rescue
      begin
      return "****#{(AESCrypt.decrypt("#{self.id}-", self.bank_account)).last(4)}"
      rescue
        # return self.bank_account.last(4)
      end
    end
  end

  def merchant_primary_locations
    User.find(self.merchant_id).wallets.primary
  end

  def update_token
    token = SecureRandom.urlsafe_base64(10)
    self.update(location_secure_token: token)
  end

  def merchants
    self.users.merchant
  end

  def affiliates
    self.users.affiliate
  end

  def isos
    self.users.iso
  end

  def iso
    iso = nil
    self.users.iso.each do |user|
      if user.iso?
        if self.profit_split
          profit_detail = JSON.parse(self.profit_split_detail)
          if profit_detail.present?
            if profit_detail["splits"]["super_iso"].present?
              if profit_detail["splits"]["super_iso"]["wallet"].present?
                if user.id == profit_detail["splits"]["super_iso"]["wallet"].to_i
                elsif profit_detail["splits"].keys[1].include? TypesEnumLib::Users::Iso
                  if user.id != profit_detail["splits"].values[1]["wallet"].to_i
                    iso = user
                  end
                else
                  iso = user
                end
              else
                iso = user
              end
            else
              iso = user
            end
          end
        else
          iso = user
        end
      end
    end
    iso
  end

  def partners
    self.users.partner
  end

  def agents
    self.users.agent
  end

  def standalone_url
    return STANDALONE_URLS[self.standalone_type]
  end

  def self.create_baked_users(location_id, wallet_id, user_id, split_type, name, dollar, percent, user_role, attach, gbox_on_file, iso_on_file, position)
    BakedUser.create(
        user_id: user_id,
        location_id: location_id,
        wallet_id: wallet_id,
        name: name,
        active: true,
        split_type: split_type,
        position: position,
        gbox: gbox_on_file.present? ? gbox_on_file : false,
        iso_on_file: iso_on_file.present? ? iso_on_file : false,
        attach: attach,
        dollar: dollar,
        percent: percent,
        user_role: user_role
    )
  end

  private

  def qr_create_wallet
    user=User.find(self.merchant_id)
    if user.merchant? && !user.merchant_id.present?
      wallet1 = create_wallet(self.business_name, user.id , 0,self.id)
      wallet2 = create_wallet("Tip Wallet", user.id, 1,self.id )
      wallet3 = create_wallet(self.business_name+" reserve", user.id, 4, self.id)
    end
  end

  def wallet_name(id)
    wallet = Wallet.find_by_id(id) if id.present?
    wallet.present? ? wallet.name : ''
  end

  def create_setting
    Setting.create(user_id: self.id)
  end

  def email_notification
    merchant = self.merchant
    UserMailer.new_location_email(merchant.id, self.id).deliver_later if merchant.is_block == false && merchant.company_id!=2
  end

end
