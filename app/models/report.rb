class Report < ApplicationRecord
  enum status: {pending: "pending", crashed: "crashed", completed: "completed", crashed_archived: "crashed_archived"}
end
