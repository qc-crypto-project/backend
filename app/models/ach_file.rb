class AchFile < ApplicationRecord

  # has_attached_file :file
  # validates_attachment_content_type :file, :content_type => ["text/csv"]
  has_attached_file :file, { validate_media_type: false }
  validates_attachment :file, {
                                      # tweak as desired
                                      content_type: { content_type: ["text/csv", Paperclip::ContentTypeDetector::SENSIBLE_DEFAULT] }
                                  }
end
