# == Schema Information
#
# Table name: config_logs
#
#  id              :bigint(8)        not null, primary key
#  user_id         :integer
#  to              :string
#  from            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  total_merchants :integer
#

class ConfigLog < ApplicationRecord
  belongs_to :user,  optional: true
  belongs_to :location, optional: true
  has_many :config_log_details
end
