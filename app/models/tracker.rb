class Tracker < ApplicationRecord

  belongs_to :user
  belongs_to :tracking_transaction, class_name: 'Transaction', foreign_key: 'transaction_id'
  has_one :customer_dispute , through: :tracking_transaction
  belongs_to :location
  has_one :merchant , through: :location
  validates :transaction_id, uniqueness: true, presence:true
  #enum carrier: [amazon_mws: 'amazon_mws', apc: 'apc', aramex: 'aramex', arrow_xl: 'arrow_xl', asendia: 'asendia', australia_post: 'australia_post', axlehire_v3: 'axlehire_v3', border_guru: 'border_guru', cainiao: 'cainiao', canada_post: 'canada_post', canpar: 'canpar', columbus_last_mile: 'columbus_last_mile', chronopost: 'chronopost', colis_prive: 'colis_prive', colissimo: 'colissimo', couriers_please: 'couriers_please', dai_post: 'dai_post', deliv: 'deliv', deutsche_post: 'deutsche_post', dhl_ecommerce_asia: 'dhl_ecommerce_asia', dhl_express: 'dhl_express', dhl_freight: 'dhl_freight', dhl_germany: 'dhl_germany', dhl_global_mail: 'dhl_global_mail', dhl_globalmail_international: 'dhl_globalmail_international', dicom: 'dicom', direct_link: 'direct_link', doorman: 'doorman', dpd: 'dpd', dpduk: 'dpduk', china_ems: 'china_ems' , estafeta: 'estafeta', estes: 'estes', fastway: 'fastway', fed_ex: 'fed_ex', fed_ex_mailview: 'fed_ex_mailview', fed_ex_same_day_city: 'fed_ex_same_day_city', fed_ex_uk: 'fed_ex_uk', fedex_smart_post: 'fedex_smart_post', first_mile: 'first_mile', globegistics: 'globegistics', gso: 'gso', hermes: 'hermes', hong_kong_post: 'hong_kong_post', interlink_express: 'interlink_express', janco_freight: 'janco_freight', jp_post: 'jp_post', kuroneko_yamato: 'kuroneko_yamato', la_poste: 'la_poste', laser_ship_v2: 'laser_ship_v2', latvijas_pasts: 'latvijas_pasts', liefery: 'liefery', loomis_express: 'loomis_express', lso: 'lso', network_4: 'network_4', newgistics: 'newgistics', norco: 'norco', on_trac: 'on_trac', on_trac_direct_post: 'on_trac_direct_post', orange_ds: 'orange_ds', osm_worldwide: 'osm_worldwide', parcelforce: 'parcelforce', passport: 'passport', pilot: 'pilot', post_nl: 'post_nl', posten: 'posten', post_nord: 'post_nord', purolator: 'purolator', royal_mail: 'royal_mail', rr_donnelley: 'rr_donnelley', seko: 'seko', singapore_post: 'singapore_post', spee_dee: 'spee_dee', sprint_ship: 'sprint_ship', star_track: 'star_track', toll: 'toll', t_force: 't_force', uds: 'uds', ukrposhta: 'ukrposhta', ups: 'ups', ups_iparcel: 'ups_iparcel', ups_mail_innovations: 'ups_mail_innovations', usps: 'usps', veho: 'veho', yanwen: 'yanwen', yodel: 'yodel']
  enum status: {new_order: "new_order", pre_transit: 'pre_transit', in_transit: 'in_transit', out_for_delivery: 'out_for_delivery', delivered: 'delivered', return_to_sender: 'return_to_sender', failure: 'failure', unknown: 'unknown',cancel: 'cancel', refund: "refunded", overdue: "overdue"}

  after_update :make_refund_confirmation, if: proc { |obj| obj.previous_changes.include?('status')}
  after_update :status_change_email, if: proc { |obj| obj.previous_changes.include?('status')}
  after_update :send_notification, if: proc { |obj| obj.previous_changes.include?('status')}
  after_update :refund_return_sender, if: proc { |obj| obj.previous_changes.include?('return_status')}
  after_update :return_status_change_email, if: proc { |obj| obj.previous_changes.include?('return_status')}
  after_update :return_send_notification, if: proc { |obj| obj.previous_changes.include?('return_status')}

  # scope :by_transaction_id , lambda {|transaction_id| where(transaction_id: transaction_id)}

  scope :by_order_id , lambda { |order_id|
    unless order_id.blank?
      joins(:tracking_transaction).where("transactions.order_id = ? " ,order_id)
    end
  }

  scope :by_transaction_id,lambda { |transaction_id|
    unless transaction_id.blank?
       where(transaction_id: transaction_id) 
    end
  }

  scope :by_status,lambda {|status|
    unless status.blank?
      where(status: status)
    end
  }

  scope :by_merchant_name,lambda {|name|
    unless name.blank?
      joins(:merchant).where("name LIKE (?)","%#{name}%")
    end
  }

  scope :by_refund_case,lambda {|refund_no|
    unless refund_no.blank?
      joins(:customer_dispute).where("customer_disputes.id = ?  ", refund_no).where("customer_disputes.dispute_type = ? " , "refund")
    end
  }

  scope :by_cancel_case , lambda {|cancel_no|
    unless cancel_no.blank?
      joins(:customer_dispute).where("customer_disputes.id = ?  " , cancel_no ).where("customer_disputes.dispute_type = ?" , "cancel")
    end
  }

  scope :by_order_date , lambda {|order_date|
    unless order_date.blank?
      where("Date(created_at) = ? " , order_date)
    end
  }

  scope :by_delivered_date , lambda {|deliver_date|
    unless deliver_date.blank?
      where("Date(delivered_date) = ? " , deliver_date)
    end
  }

  scope :order_search , lambda {|order_search|
    unless order_search.blank?
      joins(:merchant,:tracking_transaction).where("users.name LIKE (?) or transactions.order_id = ?","%#{order_search}%",order_search)
    end
  }

  def refund_return_sender
    if self.is_return == true && self.return_status == "in_transit"
      transaction = self.tracking_transaction
      if transaction.status != "refunded"
        merch = transaction.receiver
        refund      =  RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Return to Sender",self.user)
        response    = refund.process
        dispute = transaction.customer_dispute
        if dispute
          dispute.update(status: "approved")
        end
      end
    end
  end

  private

  def make_refund_confirmation
    if self.cancel? || self.refund?
    # if self.cancel? || self.refund? || self.failure?
      if self.tracking_transaction.status != "refunded" && (self.tracking_transaction.status != "complete" && self.tracking_transaction.refund_log.blank?)
        transaction = self.tracking_transaction
        merch       = transaction.receiver
        refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Tracker Got Cancelled", self.user)
        min_result = self.tracking_transaction.minfraud_result
        if min_result.present? && min_result.qc_action == TypesEnumLib::RiskType::PendingReview
          min_result.qc_action = TypesEnumLib::RiskType::Reject
          min_result.save
        end
      end

    end
  end

  def status_change_email
    if self.status == "pre_transit"
      UserMailer.pre_transit_status(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.payment_gateway.name,self.tracker_code).deliver_later
    elsif self.status == "in_transit"
      UserMailer.in_transit_status(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.payment_gateway.name,self.tracker_code,self.carrier).deliver_later
    elsif self.status == "delivered"
      UserMailer.delivered_status(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.payment_gateway.name,self).deliver_later
    elsif self.status == "cancel"
      UserMailer.order_cancel_email(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.try(:card).try(:brand),self.tracking_transaction.last4,self.tracking_transaction.total_amount).deliver_later
    elsif self.status == "return_to_sender"
      UserMailer.return_to_sender(self.tracking_transaction.receiver.id,self.location.id,self.tracking_transaction.order_id,self.carrier).deliver_later
      UserMailer.return_to_sender_customer(self.user.id,self.location.id,self.tracking_transaction.order_id,self.carrier).deliver_later
    end
  end
  def return_status_change_email
    if self.return_status == "pre_transit"
      UserMailer.pre_transit_status(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.payment_gateway.name,self.tracker_code).deliver_later
    elsif self.return_status == "in_transit"
      UserMailer.in_transit_status(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.payment_gateway.name,self.tracker_code,self.carrier).deliver_later
    elsif self.return_status == "delivered"
      UserMailer.delivered_status(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.payment_gateway.name,self).deliver_later
    elsif self.return_status == "cancel"
      UserMailer.order_cancel_email(self.user.id,self.tracking_transaction.order_id,self.location.id,self.tracking_transaction.try(:card).try(:brand),self.tracking_transaction.last4,self.tracking_transaction.total_amount).deliver_later
    elsif self.return_status == "return_to_sender"
      UserMailer.return_to_sender(self.tracking_transaction.receiver.id,self.location.id,self.tracking_transaction.order_id,self.carrier).deliver_later
      UserMailer.return_to_sender_customer(self.user.id,self.location.id,self.tracking_transaction.order_id,self.carrier).deliver_later
    end
  end

  def send_notification
    if self.status == "pre_transit"
      text = I18n.t('notifications.pre_transit', order_id: self.tracking_transaction.order_id,tracking_no: self.tracker_code)
      Notification.notify_user(self.user,self.user,self,"pre_transit",nil,text)
    elsif self.status == "in_transit"
      text = I18n.t('notifications.in_transit', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.user,self.user,self,"in_transit",nil,text)
    elsif self.status == "delivered"
      text = I18n.t('notifications.delivered', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.user,self.user,self,"delivered",nil,text)
    elsif self.status == "cancel"
      text = I18n.t('notifications.cancelled', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.user,self.user,self,"cancelled",nil,text)
    elsif self.status == "return_to_sender"
      text = I18n.t('notifications.undeliverable_order', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.tracking_transaction.receiver,self.tracking_transaction.receiver,self,"return to sender",nil,text)
      Notification.notify_user(self.user,self.user,self,"return to sender",nil,text)
    end
  end

  def return_send_notification
    if self.return_status == "pre_transit"
      text = I18n.t('notifications.pre_transit', order_id: self.tracking_transaction.order_id,tracking_no: self.tracker_code)
      Notification.notify_user(self.user,self.user,self,"pre_transit",nil,text)
    elsif self.return_status == "in_transit"
      text = I18n.t('notifications.in_transit', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.user,self.user,self,"in_transit",nil,text)
    elsif self.return_status == "delivered"
      text = I18n.t('notifications.delivered', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.user,self.user,self,"delivered",nil,text)
    elsif self.return_status == "cancel"
      text = I18n.t('notifications.cancelled', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.user,self.user,self,"cancelled",nil,text)
    elsif self.return_status == "return_to_sender"
      text = I18n.t('notifications.undeliverable_order', order_id: self.tracking_transaction.order_id)
      Notification.notify_user(self.tracking_transaction.receiver,self.tracking_transaction.receiver,self,"return to sender",nil,text)
      Notification.notify_user(self.user,self.user,self,"return to sender",nil,text)
    end
  end

end
