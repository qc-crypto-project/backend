class ErrorMessage < ApplicationRecord
  validates_uniqueness_of :error_code
  validates :error_code, :error_message, :error_reason, presence: true

  after_save :update_error_message_cache


  def self.find_by(params)
    where(params).first
  end

  def self.where(params)
    params[:error_code] = params[:error_code].to_s if params[:error_code].present?
    return ErrorMessageCache.get(params)
  end


  def update_error_message_cache
    ErrorMessageCache.update(self.error_code , self)
  end
end
