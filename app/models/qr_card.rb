# == Schema Information
#
# Table name: qr_cards
#
#  id                 :bigint(8)        not null, primary key
#  is_valid           :boolean          default(TRUE)
#  user_id            :bigint(8)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  price              :string
#  wallet_id          :bigint(8)
#  token              :string
#  message            :string
#  category           :string
#  from_id            :integer
#  type_of            :string
#  tip                :float
#

class QrCard < ApplicationRecord
  belongs_to :wallet
  belongs_to :user
  belongs_to :request, optional: true
  belongs_to :card_transaction, class_name: 'Transaction', foreign_key: 'transaction_id', optional: true
  has_attached_file :image
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif", "application/pdf"]

  def generate_token
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    self.token = token
  end

  def render_map(id)
  end
end
