# == Schema Information
#
# Table name: owners
#
#  id                 :bigint(8)        not null, primary key
#  name               :string
#  ownership          :string
#  address            :string
#  city               :string
#  state              :string
#  phone_number       :string
#  zip_code           :string
#  ssn                :string
#  date_of_birth      :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  user_id            :bigint(8)
#  wallet_id          :bigint(8)
#  batch_type         :integer
#  days               :integer          default(0)
#

class Owner < ApplicationRecord

  belongs_to :user
end
