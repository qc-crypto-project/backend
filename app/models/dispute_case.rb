# == Schema Information
#
# Table name: dispute_cases
#
#  id                 :bigint(8)        not null, primary key
#  case_number        :string
#  user_id            :bigint(8)
#  transaction_id     :bigint(8)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  dispute_type       :string           default("")
#  recieved_date      :datetime
#  due_date           :datetime
#  amount             :float            default(0.0)
#  card_type          :string           default("")
#  status             :string           default("")
#  reason_id          :bigint(8)
#  merchant_wallet_id :integer
#  user_wallet_id     :integer
#  card_number        :string           default("")
#

class DisputeCase < ApplicationRecord
	extend ApplicationHelper
	extend Merchant::TransactionsHelper
	enum dispute_type: [:charge_back,:retrievel]
	enum status: [:sent_pending,:won_reversed,:lost,:dispute_accepted, :under_review]


	belongs_to :user
	belongs_to :reason
	belongs_to :merchant_wallet, class_name: "Wallet",foreign_key: "merchant_wallet_id"
	belongs_to :user_wallet, class_name: "Wallet",foreign_key: "user_wallet_id", optional: true
	belongs_to :charge_transaction, class_name: 'Transaction', foreign_key: 'transaction_id'
	belongs_to :cbk_fee_transaction, class_name: 'Transaction', foreign_key: 'cbk_fee_transaction_id',optional: true
	belongs_to :charge_back_transaction, class_name: 'Transaction', foreign_key: 'chargeback_transaction_id' ,optional: true
	belongs_to :dispute_result_transaction, class_name: 'Transaction', foreign_key: 'dispute_result_transaction_id' ,optional: true
	has_many :attachments, :class_name => "Image", foreign_key: 'usage_id', :dependent => :destroy
	accepts_nested_attributes_for :attachments, :allow_destroy => true
	has_many :notes, as: :notable, :dependent => :destroy
	belongs_to :payment_gateway, optional: true
	has_one :evidence,foreign_key: "dispute_id", dependent: :destroy
	# after_create :dispute_email
	validates :case_number, uniqueness: true, presence:true
	after_save :transaction_events

	# ransacker :dispute_type, formatter: proc {|v| statuses[v]} do |parent|
	# 	parent.table[:status]
	# end
	scope :created_between, -> (first_date, second_date) {
			where( created_at: first_date..second_date).where('dispute_cases.created_at > ?','01/01/2020'.to_datetime)
	}

	scope :wallet_disputes, -> (wallet_id,page_size) {
		Rails.cache.fetch("merchant_or_customer_disputes_#{wallet_id}", expires_in: 10.minutes) do
			where('merchant_wallet_id = :wallet OR user_wallet_id = :wallet',wallet: wallet_id)
					.limit(page_size)
		end
	}

	scope :total_wallet_disputes, -> (wallet_id) {
		Rails.cache.fetch("merchant_or_customer_disputes_total_amount#{wallet_id}", expires_in: 10.minutes) do
			where('merchant_wallet_id = :wallet OR user_wallet_id = :wallet',wallet: wallet_id)
		end
	}

	def transaction_events
		if self.charge_transaction.present?
			dispute_case = self
			transaction = self.charge_transaction
			if transaction.status == "approved"
				if dispute_case.status == "sent_pending" && dispute_case.dispute_type =="charge_back"
					existing_event = EventLog.where(transaction_id: transaction.id,event_type: "chargeback_created")
					if existing_event.empty?
						event = EventLog.new(transaction_id: transaction.id,message: "Chargeback Created",event_type: "chargeback_created")
						event.save
					end
				end
				if dispute_case.status == "won_reversed" && dispute_case.dispute_type =="charge_back"
					existing_event = EventLog.where(transaction_id: transaction.id,event_type: "chargeback_created")
					if existing_event.empty?
						event = EventLog.new(transaction_id: transaction.id,message: "Chargeback Created",event_type: "chargeback_created")
						event.save
						event_won = EventLog.new(transaction_id: transaction.id,message: "Chargeback Won",event_type: "chargeback_won")
						event_won.save
					elsif existing_event.count == 1
						event_won = EventLog.new(transaction_id: transaction.id,message: "Chargeback Won",event_type: "chargeback_won")
						event_won.save
					end
				end
				if dispute_case.status == "lost" && dispute_case.dispute_type =="charge_back"
					existing_event = EventLog.where(transaction_id: transaction.id,event_type: "chargeback_created")
					if existing_event.empty?
						event = EventLog.new(transaction_id: transaction.id,message: "Chargeback Created",event_type: "chargeback_created")
						event.save
						event_won = EventLog.new(transaction_id: transaction.id,message: "Chargeback Lost",event_type: "chargeback_lost")
						event_won.save
					elsif existing_event.count == 1
						event_won = EventLog.new(transaction_id: transaction.id,message: "Chargeback Lost",event_type: "chargeback_lost")
						event_won.save
					end
				end
				if dispute_case.status == "dispute_accepted" && dispute_case.dispute_type =="charge_back"
					existing_event = EventLog.where(transaction_id: transaction.id,event_type: "chargeback_created")
					if existing_event.empty?
						event = EventLog.new(transaction_id: transaction.id,message: "Chargeback Created",event_type: "chargeback_created")
						event.save
						event_won = EventLog.new(transaction_id: transaction.id,message: "Dispute Accepted",event_type: "dispute_accepted")
						event_won.save
					elsif existing_event.count == 1
						event_won = EventLog.new(transaction_id: transaction.id,message: "Dispute Accepted",event_type: "dispute_accepted")
						event_won.save
					end
				end
			end

		end
	end

	def self.to_csv
		wallets = Wallet.joins(:users).where(id: all.pluck(:merchant_wallet_id).uniq).select("wallets.id AS id", "wallets.location_id AS location_id", "users.id AS user_id", "users.name As user_name").group_by{|e| e.id}
		locations = Location.joins(:wallets,:fees).where("wallets.wallet_type = ? AND fees.fee_status = ? AND fees.risk = ?",0,18, 0).where(id: wallets.values.flatten.pluck(:location_id)).select("locations.id AS id", "locations.business_name AS business_name","wallets.id AS wallet_id","fees.charge_back_fee AS charge_back_fee").group_by{|e| e.wallet_id}
		reasons = Reason.where(id: all.pluck(:reason_id).uniq).select("id AS id", "title AS title").group_by{|e| e.id}
		transactions = Transaction.where(id: all.pluck(:chargeback_transaction_id, :transaction_id).flatten.compact.uniq).select("id AS id", "seq_transaction_id AS seq_transaction_id","total_amount AS total_amount").group_by{|e| e.id}
		CSV.generate do |csv|
			csv << ['Txn ID', 'Merchant ID', 'Merchant Name',	'DBA Name',	'Main Wallet ID', 'Case Number', 'Dispute Type', 'CBK Reason', 'Received Date', 'Due Date', 'Amount', 'Original Transaction ID', 'Original Transaction Amount', 'CBK Fee Amount', 'Last 4', 'Status']
			all.each do |dispute_case|
				if dispute_case.charge_fee == true
					charge_back_fee = locations[dispute_case.merchant_wallet_id].try(:first).try(:charge_back_fee)
				end
				if dispute_case.try(:status) == 'won_reversed'
					status = "Won/Reversed"
				elsif dispute_case.try(:status) == 'sent_pending'
					status = "Sent/Pending"
				elsif dispute_case.try(:status) == 'lost'
					status = "Lost"
				elsif dispute_case.try(:status)=='dispute_accepted'
					status = "Dispute Accepted"
				end
				csv << [transactions[dispute_case.chargeback_transaction_id].try(:first).try(:seq_transaction_id), "M-#{wallets[dispute_case.merchant_wallet_id].try(:first).try(:user_id)}", wallets[dispute_case.merchant_wallet_id].try(:first).try(:user_name), locations[dispute_case.merchant_wallet_id].try(:first).try(:business_name), dispute_case.merchant_wallet_id,dispute_case.try(:case_number), dispute_case.try(:dispute_type).humanize.titleize, reasons[dispute_case.reason_id].try(:first).try(:title), dispute_case[:recieved_date].nil? ? 'NA' : dispute_case[:recieved_date].strftime("%m-%d-%Y"), dispute_case[:due_date].nil? ? 'NA' : dispute_case[:due_date].strftime("%m-%d-%Y"), number_to_currency(number_with_precision(dispute_case.try(:amount).to_f,precision: 2, :delimiter => ',')), transactions[dispute_case.transaction_id].try(:first).try(:seq_transaction_id), number_to_currency(number_with_precision(transactions[dispute_case.transaction_id].try(:first).try(:total_amount),precision: 2, :delimiter => ',')),number_to_currency(number_with_precision(charge_back_fee.to_f,precision: 2, :delimiter => ',')), dispute_case.try(:card_number), status]

			end
		end
	end

	def self.run_chargeback(params, first_date, second_date)
		reports = {}
		params["parameters"].insert(0,"Date Range")
		if params["dba_name"].present? || params["m_names"].present? || params["m_ids"].present?
			params["parameters"].insert(1, "Merchant Id")
			params["parameters"].insert(2, "Merchant Name")
			params["parameters"].insert(3,"DBA Name")
			dba_params = {}
			if params["dba_name"].present?
				if !params["dba_name"].include?("all")
					params["dba_name"].each do |param|
						temp = JSON.parse(param)
						dba_params["#{temp["id"]}"] = {name: temp["name"], merchant_id: temp["merchant_id"],merchant_name: temp["merchant_name"]}
					end
				elsif params["dba_name"].include?("all")
					locations = Location.includes(:merchant).joins(:wallets).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id,locations.merchant_id")
					locations.each do |location|
						dba_params["#{location.wallet_id}"] = {name: location.business_name, merchant_id: location.merchant_id,merchant_name: location.merchant.try(:name)}
					end
				end
			elsif params["m_names"].present?
				if params["m_names"].include?("all")
					locations = Location.includes(:merchant).joins(:wallets).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id,locations.merchant_id")
				else
					ids = params["m_names"].map {|n| n.split(',').first} if params["m_names"].present?
					locations = Location.includes(:merchant).joins(:wallets).where("wallets.wallet_type = ? AND locations.merchant_id IN (?)", 0,ids).select("locations.id,locations.business_name,wallets.id as wallet_id,locations.merchant_id")
				end
				locations.each do |location|
					dba_params["#{location.wallet_id}"] = {name: location.business_name, merchant_id: location.merchant_id,merchant_name: location.merchant.try(:name)}
				end
			elsif params["m_ids"].present?
				if params["m_ids"].include?("all")
					locations = Location.includes(:merchant).joins(:wallets).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id,locations.merchant_id")
				else
					ids = params["m_ids"]
					locations = Location.includes(:merchant).joins(:wallets).where("wallets.wallet_type = ? AND locations.merchant_id IN (?)", 0,ids).select("locations.id,locations.business_name,wallets.id as wallet_id,locations.merchant_id")
				end
				locations.each do |location|
					dba_params["#{location.wallet_id}"] = {name: location.business_name, merchant_id: location.merchant_id,merchant_name: location.merchant.try(:name)}
				end
			end
			# cbk_first_date = first_date.utc.beginning_of_day if first_date.present?
			# cbk_second_date = second_date.utc.beginning_of_day - 1.day if second_date.present?
			# start_month = first_date.try(:to_date).try(:beginning_of_month)
			# start_month = "#{start_month.year}-#{start_month.month}-#{start_month.day} 07:00:00"
			hold_first_date = "#{first_date.to_date.year}-#{first_date.to_date.month}-#{first_date.to_date.day} 00:00:00"
			hold_second_date = "#{second_date.to_date.year}-#{second_date.to_date.month}-#{second_date.to_date.day} 00:00:00"
			if params[:date].present?
				date = parse_date(params[:date]) if params[:date].present?
				first_date1 = date.first
				second_date1 = date.last
				hold_first_date = "#{first_date1[:year]}-#{first_date1[:month]}-#{first_date1[:day]} 00:00:00"
				hold_second_date = "#{second_date1[:year]}-#{second_date1[:month]}-#{second_date1[:day]} 00:00:00"
			end

			# start_month = DateTime.new(start_month.year, start_month.month, start_month.day, 7,00,00)
			# cbk_txs = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: start_month, second: second_date).where(main_type: ["CBK Won"]).where("sender_wallet_id IN (:id) OR receiver_wallet_id IN (:id)",id:  dba_params.keys).select(:amount_in_cents, :sender_wallet_id, :receiver_wallet_id, :main_type).group_by{|e| [e.main_type, "#{e.receiver_wallet_id}"]}
			# cbk_lost = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: start_month, second: second_date).where(main_type: "CBK Lost").where("tags->'dispute_case'->>'merchant_wallet_id' IN (?)",dba_params.keys).group_by{|e| "#{e.tags["dispute_case"]["merchant_wallet_id"]}"}
			# cbk_hold = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where(main_type: ["CBK Hold"]).where("sender_wallet_id IN (:id)",id:  dba_params.keys).select(:amount_in_cents, :sender_wallet_id, :main_type).group_by{|e|  "#{e.sender_wallet_id}"}
			cbk_hold = DisputeCase.charge_back.where(merchant_wallet_id: dba_params.keys, recieved_date: hold_first_date..hold_second_date).select(:amount, :status, :merchant_wallet_id, :id).group_by{|e| "#{e.merchant_wallet_id}"}
			# charge_backs = DisputeCase.charge_back.where(merchant_wallet_id: dba_params.keys, created_at: start_month..second_date, status: "sent_pending").select(:amount, :status, :merchant_wallet_id, :id).group_by{|e| "#{e.merchant_wallet_id}"}

			transactions = Transaction.where(receiver_wallet_id: dba_params.keys,main_type: I18n.t('all_sales_types'), timestamp: first_date..second_date, status: ["approved", "refunded", "complete"]).select(:id,:total_amount, :receiver_wallet_id).group_by{|e| "#{e.receiver_wallet_id}"}
			dba_params.each do |key, name_hash|
				name = name_hash[:name]
				merchant_id = name_hash[:merchant_id]
				merchant_name = name_hash[:merchant_name]
				# lost = cbk_lost[key]
				# won = cbk_txs[["CBK Won", key]] if cbk_txs.present?
				# hold = charge_backs[key] if charge_backs.present?
				# total_cbk = lost.try(:count).to_i  + won.try(:count).to_i + hold.try(:count).to_i
				cbk_count =  cbk_hold[key].try(:count).to_i
				if transactions[key].present?
					tx_count = transactions[key].count
					total_processed = transactions[key].pluck(:total_amount).try(:sum)
				end
				if tx_count.to_i > 0
					cbk_percent = cbk_count.to_f/tx_count.to_f * 100
				elsif cbk_count.to_i > 0 && tx_count.to_i == 0
					cbk_percent = 100
				end
				# won_volume = SequenceLib.dollars(won.try(:pluck, :amount_in_cents).try(:sum).to_f)
				# lost_volume = SequenceLib.dollars(lost.try(:pluck, :amount_in_cents).try(:sum).to_f)
				pend_volume = cbk_hold[key].try(:pluck, :amount).try(:sum).to_f
				# dis_volume = charge_backs[["dispute_accepted", key]].try(:pluck, :amount).try(:sum).to_f
				# volume = hold.try(:pluck, :amount).try(:sum).to_f

				if (params["not_0"].present? && params["not_0"] == "true") && (tx_count.to_i == 0 && total_processed.to_f == 0 && cbk_percent.to_f == 0 && cbk_count.to_i == 0)
				else
					reports["#{key}-#{name}"] = {
							date_range: "#{params['date']}",
							merchant_id: merchant_id,
							merchant_name: merchant_name,
							dba_name: name,
							total_tx: tx_count || 0,
							total_processed: number_with_precision(number_to_currency(total_processed || 0, precision: 2)),
							received_count: cbk_count,
							received_amount: number_with_precision(number_to_currency(pend_volume), precision: 2),
							cbk_perc: "#{number_with_precision( cbk_percent.to_f || 0, precision: 2)}%",
							# cbk_volume: number_with_precision(number_to_currency(pend_volume), precision: 2),
							# in_progress: hold.try(:count).to_i || 0,
							# in_progress_amount: number_with_precision(number_to_currency(volume),precision: 2),
							# cbk_won: won.try(:count).to_i || 0,
							# cbk_won_amount: number_with_precision(number_to_currency(won_volume),precision: 2),
							# cbk_lost: lost.try(:count).to_i || 0,
							# cbk_lost_amount: number_with_precision(number_to_currency(lost_volume),precision: 2),
					}
				end
			end
		elsif params["gateway"].present?
			params["parameters"].insert(1, "Descriptor")
			gateways = PaymentGateway.where(id: params["gateway"]).select(:id, :name)
			cbk_first_date = first_date
			cbk_second_date = second_date

			charge_backs = DisputeCase.charge_back.where(payment_gateway_id: gateways.pluck(:id), created_at: cbk_first_date..cbk_second_date).select(:amount, :status, :payment_gateway_id, :id,:merchant_wallet_id).group_by{|e| [e.status, e.payment_gateway_id]}
			transactions = Transaction.where(payment_gateway_id: gateways.pluck(:id),main_type: I18n.t('all_sales_types'), timestamp: first_date..second_date, status: ["approved", "refunded","complete"]).select(:id,:total_amount, :payment_gateway_id).group_by{|e| e.payment_gateway_id}
			gateways.each do |gateways|
				cbk_count = charge_backs[["won_reversed", gateways.id]].try(:count).to_i + charge_backs[["lost", gateways.id]].try(:count).to_i + charge_backs[["dispute_accepted", gateways.id]].try(:count).to_i + charge_backs[["sent_pending", gateways.id]].try(:count).to_i
				# won_volume = charge_backs[["won_reversed", gateways.id]].try(:pluck, :amount).try(:sum).to_f
				# lost_volume = charge_backs[["lost", gateways.id]].try(:pluck, :amount).try(:sum).to_f
				pend_volume = charge_backs[["sent_pending", gateways.id]].try(:pluck, :amount).try(:sum).to_f
				# dis_volume = charge_backs[["dispute_accepted", gateways.id]].try(:pluck, :amount).try(:sum).to_f
				# volume = won_volume.to_f + lost_volume.to_f + pend_volume.to_f + dis_volume.to_f
				if transactions[gateways.id].present?
					tx_count = transactions[gateways.id].count
					cbk_percent = cbk_count.to_f/tx_count.to_f * 100 if tx_count.to_i > 0
					total_processed = transactions[gateways.id].pluck(:total_amount).try(:sum)
				end

				if (params["not_0"].present? && params["not_0"] == "true") && (tx_count.to_i == 0 && total_processed.to_f == 0 && cbk_percent.to_f == 0 && cbk_count.to_i == 0)
				else
					reports["#{gateways.id}-#{gateways.name}"] = {
							date_range: "#{params['date']}",
							descriptor: gateways.name,
							total_tx: tx_count || 0,
							total_processed: number_with_precision(number_to_currency(total_processed || 0, precision: 2)),
							received_count: cbk_count,
							received_amount: number_with_precision(number_to_currency(pend_volume), precision: 2),
							cbk_perc: "#{number_with_precision(cbk_percent || 0, precision: 2)}%",
							# cbk_volume: number_with_precision(number_to_currency(volume || 0), precision: 2),
							# in_progress: charge_backs[["sent_pending", gateways.id]].try(:count).to_i || 0,
							# in_progress_amount: number_with_precision(number_to_currency(pend_volume),precision: 2),
							# cbk_won: charge_backs[["won_reversed", gateways.id]].try(:count).to_i || 0,
							# cbk_won_amount: number_with_precision(number_to_currency(won_volume),precision: 2),
							# cbk_lost: charge_backs[["lost", gateways.id]].try(:count).to_i || 0,
							# cbk_lost_amount: number_with_precision(number_to_currency(lost_volume),precision: 2),
							# total_cbk: cbk_count || 0
					}
				end
			end
		end
		header = []
		params["parameters"].each do |v|
			header.push(v.humanize.capitalize)
		end
		{reports: reports, params: header}
	end

end
