class ApplicationRecord < ActiveRecord::Base
  before_save :save_versions, if:-> {['Wallet','Transaction','BlockTransaction'].include?(self.class.to_s)}
  self.abstract_class = true

  def save_versions
    self.block_version=Sequence::VERSION
  end

  def self.key_rotation(old_key,column_name)
  	self.all.where.not("#{column_name} = ? " ,'').each do |table|
  		decrypted_value = AESCrypt.decrypt("", table.try("#{column_name}") , nil , old_key)
  		table.send("#{column_name}=", AESCrypt.encrypt(nil,decrypted_value,nil,ENV['AES_KEY']))
  		table.save
  	end
  end
end
