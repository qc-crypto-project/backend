class TransactionBatch < ApplicationRecord
  belongs_to :user, class_name: "User", foreign_key: :merchant_id, optional: true
  belongs_to :wallet, class_name: "Wallet", foreign_key: :merchant_wallet_id, optional: true
  has_and_belongs_to_many :batch_transactions, class_name: "Transaction", foreign_key: :transaction_batch_id


  enum batch_type: {"sale" => "sale", "refund" => "refund"}
  
end
