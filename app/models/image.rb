# == Schema Information
#
# Table name: images
#
#  id                     :bigint(8)        not null, primary key
#  add_image_file_name    :string
#  add_image_content_type :string
#  add_image_file_size    :integer
#  add_image_updated_at   :datetime
#  add_id                 :bigint(8)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  request_id             :integer
#  ticket_id              :integer
#  comment_id             :integer
#  usage_id               :integer
#

class Image < ApplicationRecord
  belongs_to :dispute_case, optional: true
  belongs_to :request ,optional: true
  belongs_to :ticket , optional:  true
  belongs_to :comment , optional:  true
  belongs_to :evidence, optional:  true
  belongs_to :email_notification, optional:  true
  belongs_to :bank_detail, optional:  true
  belongs_to :invoice_user, :class_name => 'User', optional: true
  has_attached_file :add_image , default_url: "<%= asset_path('images/world.jpg') %>"
  process_in_background :add_image, queue: "default", processing_image_url: :processing_image_fallback
  validates_attachment_content_type :add_image, content_type: ["application/pdf","application/vnd.ms-excel",
                                                               "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                                               "application/msword",
                                                               "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                                                "text/plain",/\Aimage\/.*\z/]
  enum key: {customer_signature: "customer_signature", receipt: "receipt", customer_communication: "customer_communication", shipping_proof: "shipping_proof"}


  def processing_image_fallback
    options = add_image.options
    options[:interpolator].interpolate(options[:url], add_image, :original)
  end
end
