class Evidence < ApplicationRecord
  has_many :images, :dependent => :destroy
  belongs_to :dispute_case, class_name: "DisputeCase", foreign_key: "dispute_id"
  # accepts_nested_attributes_for :images, :allow_destroy => true
end
