class ResetLoadBalancerWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'default', :retry => 1, backtrace: 8

  def perform(*args)
    d_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_day
    payment_gateways_daily = PaymentGateway.where.not(last_daily_updated: d_date)
    payment_gateways_daily.update_all(transaction_count: 0, cc_brand_count: 0, cc_number_count: 0, amount_count: 0,daily_volume_achived: 0, last_daily_updated: d_date)

    m_date = DateTime.now.in_time_zone(PSTTIMEZONE).beginning_of_month
    payment_gateways = PaymentGateway.where.not(last_monthly_updated: m_date)
    payment_gateways.update_all(last_monthly_updated: m_date,monthly_volume_achived: 0)
  end
end
