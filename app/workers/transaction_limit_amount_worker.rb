class TransactionLimitAmountWorker
	include Sidekiq::Worker
	include Sidekiq::Status::Worker
	include ActionView::Helpers::NumberHelper
	sidekiq_options queue: 'default', :retry => 0, backtrace: 8
	def perform(*arg)
		activity_logs = ActivityLog.transaction_amount_limit.where("created_at > ?",60.minutes.ago).select(:params,:user_id).group_by{|x| x.params["location_id"]}
		activity_logs.each do |activity_log|
			merchant = User.find_by_id(activity_log.second.first.user_id)
			location = Location.find_by_id(activity_log.first)
			total_count = activity_log.second.count
			puts("Merchant ID: M-#{merchant.try(:id)} \nMerchant Name: #{merchant.try(:name)} \nDBA Name: #{location.try(:business_name)} \nIncorrect Transaction count: #{total_count}")
    		channel = ENV["STACK"] == I18n.t("environments.live") ? I18n.t('slack.channels.qc_live_alerts') : I18n.t('slack.channels.quickcard_exception')
			SlackService.notify("Merchant ID: M-#{merchant.try(:id)} \nMerchant Name: #{merchant.try(:name)} \nDBA Name: #{location.try(:business_name)} \nIncorrect Transaction count: #{total_count}", channel)
		end
	end
end