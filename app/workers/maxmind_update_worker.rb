class MaxmindUpdateWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8
  def perform(*args)
    store new_status: 'queued'
    allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
    # date = HoldInRear.pst_beginning_of_day(DateTime.now) - allowed_days.minutes
    date = DateTime.now.in_time_zone(TIMEZONE) - allowed_days.days
    job = CronJob.new(name: 'maxmind_update_worker', description: 'running maxmind manual expiration', success: true)
    result = {updated_records: [], not_updated_records: [], error: nil}
    MinfraudResult.where("qc_action = ? AND created_at <= ?", TypesEnumLib::RiskType::PendingReview, date).find_each do |minfraud|
      begin
        #Refunding transactions start
        transaction = Transaction.find(minfraud.transaction_id)
        merch = transaction.receiver
        if transaction.status != "pending"
          refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Review Expired", merch)
          response = refund.process
          transaction_id = minfraud.transaction_id
          transaction_id_notfication =  transaction.try(:seq_transaction_id).try(:slice,0..5)
          amount = transaction.amount
          text = I18n.t('notifications.risk_review_expired', txn_id: transaction_id_notfication)
          Notification.notify_user(merch,merch,transaction,"risk review expired",nil,text)
          UserMailer.manual_review_expired(transaction_id).deliver_later
          UserMailer.manual_review_refund(transaction_id).deliver_later
          #Refunding transactions end
          if response[:success] == true || response[:message] == "Already Refunded."
            minfraud.update(qc_action: TypesEnumLib::RiskType::ExpiredReview)
            # if transaction.tracker.present?
            #   transaction.tracker.cancel!
            # end
            result[:updated_records] << {id: minfraud.id, message: I18n.t('maxmind.rejected')}
          else
            result[:not_updated_records] << {id: minfraud.id, message: response[:message]}
          end
        end
      rescue Stripe::CardError, Stripe::InvalidRequestError => e
        if e.message.include?("has already been refunded")
          minfraud.update(qc_action: TypesEnumLib::RiskType::ExpiredReview)
          result[:updated_records] << {id: minfraud.id, message: e.message}
        else
          result[:not_updated_records] << {id: minfraud.id, message: e.message}
        end
        puts " stripe error", e
      rescue Exception => e
        result[:not_updated_records] << {id: minfraud.id, message: e}
        puts " exception", e
      end
    end
    job.result = result
    job.maxmind_review!
    store new_status: 'completed'
  end
end