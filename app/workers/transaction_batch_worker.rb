class TransactionBatchWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 2, backtrace: 11

  def perform(args = nil)
    begin
      SlackService.notify("Transaction BatchWorker started on #{ENV['APP_ROOT']}", "#quickcard-internal", true)
      cron_job = CronJob.new(name: "transaction batches create", log_type: :transaction_batch, success: true)
      result = {user_ids: [], error: nil}
      data = {}
      ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential: ENV['LEDGER_CREDENTIAL'] )
      page = ledger.tokens.sum(
          group_by: ['account_id']
      )
      page.each do |bal|
        data.merge!(bal.account_id => {balance: SequenceLib.dollars(bal.amount)})
      end
      User.includes(:wallets).where(role: [:merchant, :iso, :agent, :affiliate], merchant_id: nil).find_in_batches(batch_size: 2000) do |users|
        if users.present?
          updated_batches = []
          users.each do |user|
            if user.merchant?
              wallets = user.wallets.primary.pluck(:id).try(:compact)
              batches = TransactionBatch.where(merchant_wallet_id: wallets, batch_type: "sale", batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date).group_by{|e| e.merchant_wallet_id}
              old_batch = TransactionBatch.where(merchant_wallet_id: wallets, batch_type: "sale", batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).group_by{|e| e.merchant_wallet_id}
              wallets.each do |obj|
                wallet = Wallet.where(id: obj).last
                location = wallet.location
                loc_user = location.users.joins(:wallets).where(role: [:agent, :iso, :affiliate]).select("wallets.id as wallet_id", "users.role as role").group_by{|e| e.role} if location.present?
                batch = batches[obj]
                balance = data["#{obj}"].try(:[],:balance).to_f
                if batch.blank?
                  old = old_batch[obj].try(:first)
                  if old.present?
                    msg = "*Balance and net does not match* \n url: \`\`\`#{ENV["APP_ROOT"]}\`\`\` user_id: \`\`\`#{user.id}\`\`\`  role: \`\`\`#{user.role}\`\`\`  wallet_id: \`\`\`#{obj}\`\`\` previous_net: \`\`\` #{old.try(:total_net).to_f} \`\`\` today_balance: \`\`\` #{balance.to_f} \`\`\` batch_date: \`\`\` #{Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date} \`\`\`"
                    SlackService.notify(msg, "#qcbatches", true) if number_with_precision(old.try(:total_net).to_f, precision: 2).to_f != balance.to_f
                    updated_batches << TransactionBatch.new(merchant_wallet_id: obj, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, starting_balance: balance.to_f, total_net: balance.to_f, total_trxs: 0, iso_wallet_id: loc_user.try(:[],"iso").try(:first).try(:[],"wallet_id"), agent_wallet_id: loc_user.try(:[],"agent").try(:first).try(:[],"wallet_id"), affiliate_wallet_id: loc_user.try(:[],"affiliate").try(:first).try(:[],"wallet_id"))
                    result[:user_ids] << user.try(:id)
                  else
                    net = balance.to_f
                    updated_batches << TransactionBatch.new(merchant_wallet_id: obj, total_net: net.to_f, reserve: 0,starting_balance: balance.to_f, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, total_trxs: 0, iso_wallet_id: loc_user.try(:[],"iso").try(:first).try(:[],"wallet_id"), agent_wallet_id: loc_user.try(:[],"agent").try(:first).try(:[],"wallet_id"), affiliate_wallet_id: loc_user.try(:[],"affiliate").try(:first).try(:[],"wallet_id"))
                    result[:user_ids] << user.try(:id)
                  end
                end
              end
            elsif user.iso?
              wallets = user.wallets.primary.first.try(:id)
              if wallets.present?
                batches = TransactionBatch.where(iso_wallet_id: wallets, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, merchant_wallet_id: nil).last
                if batches.blank?
                  batches = TransactionBatch.where(iso_wallet_id: wallets, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).order(created_at: :asc)
                  if batches.present?
                    today_balance = data["#{wallets}"].try(:[],:balance).to_f
                    earnings = batches.pluck(:iso_commission).try(:compact).try(:sum).to_f
                    split = batches.pluck(:iso_split).try(:compact).try(:sum).to_f
                    expense = batches.pluck(:iso_expense).try(:compact).try(:sum).to_f
                    balance = batches.select{|e| e.iso_balance != 0}.try(:first).try(:iso_balance).to_f
                    net = number_with_precision((balance.to_f + earnings).to_f - expense.to_f - split.to_f, precision: 2).to_f
                    msg = "*Balance and net does not match* \n url: \`\`\`#{ENV["APP_ROOT"]}\`\`\` user_id: \`\`\`#{user.id}\`\`\`  role: \`\`\`#{user.role}\`\`\`  wallet_id: \`\`\`#{wallets}\`\`\` previous_net: \`\`\` #{net.to_f} \`\`\` today_balance: \`\`\` #{today_balance} \`\`\` batch_date: \`\`\` #{Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date} \`\`\`"
                    SlackService.notify(msg, "#qcbatches", true) if net.to_f !=  today_balance.to_f
                    updated_batches << TransactionBatch.new(iso_wallet_id: wallets, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, iso_balance: today_balance, merchant_wallet_id: nil)
                    result[:user_ids] << user.try(:id)
                  else
                    updated_batches << TransactionBatch.new(iso_wallet_id: wallets, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, merchant_wallet_id: nil, iso_balance: data["#{wallets}"].try(:[],:balance).to_f)
                    result[:user_ids] << user.try(:id)
                  end
                end
              end
            elsif user.agent?
              wallets = user.wallets.primary.first.try(:id)
              if wallets.present?
                batches = TransactionBatch.where(agent_wallet_id: wallets, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, merchant_wallet_id: nil).last
                if batches.blank?
                  batches = TransactionBatch.where(agent_wallet_id: wallets, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).order(created_at: :asc)
                  if batches.present?
                    today_balance = data["#{wallets}"].try(:[],:balance).to_f
                    earnings = batches.pluck(:agent_commission).try(:compact).try(:sum).to_f
                    split = batches.pluck(:agent_split).try(:compact).try(:sum).to_f
                    expense = batches.pluck(:agent_expense).try(:compact).try(:sum).to_f
                    balance = batches.select{|e| e.agent_balance != 0}.try(:first).try(:agent_balance).to_f
                    net = number_with_precision((balance.to_f + earnings).to_f - expense.to_f - split.to_f, precision: 2).to_f
                    msg = "*Balance and net does not match* \n url: \`\`\`#{ENV["APP_ROOT"]}\`\`\` user_id: \`\`\`#{user.id}\`\`\`  role: \`\`\`#{user.role}\`\`\` wallet_id: \`\`\`#{wallets}\`\`\` previous_net: \`\`\` #{net.to_f} \`\`\` today_balance: \`\`\` #{today_balance} \`\`\` batch_date: \`\`\` #{Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date} \`\`\`"
                    SlackService.notify(msg, "#qcbatches", true) if net.to_f !=  today_balance.to_f
                    updated_batches << TransactionBatch.new(agent_wallet_id: wallets, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, agent_balance: today_balance.to_f, merchant_wallet_id: nil)
                    result[:user_ids] << user.try(:id)
                  else
                    updated_batches << TransactionBatch.new(agent_wallet_id: wallets, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, merchant_wallet_id: nil, agent_balance: data["#{wallets}"].try(:[],:balance).to_f)
                    result[:user_ids] << user.try(:id)
                  end
                end
              end
            elsif user.affiliate?
              wallets = user.wallets.primary.first.try(:id)
              if wallets.present?
                batches = TransactionBatch.where(affiliate_wallet_id: wallets, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, merchant_wallet_id: nil).last
                if batches.blank?
                  batches = TransactionBatch.where(affiliate_wallet_id: wallets, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date - 1.day).order(created_at: :asc)
                  if batches.present?
                    today_balance = data["#{wallets}"].try(:[],:balance).to_f
                    earnings = batches.pluck(:affiliate_commission).try(:compact).try(:sum).to_f
                    split = batches.pluck(:affiliate_split).try(:compact).try(:sum).to_f
                    expense = batches.pluck(:affiliate_expense).try(:compact).try(:sum).to_f
                    balance = batches.select{|e| e.affiliate_balance != 0}.try(:first).try(:affiliate_balance).to_f
                    net = number_with_precision((balance.to_f + earnings).to_f - expense.to_f - split.to_f, precision: 2).to_f
                    msg = "*Balance and net does not match* \n url: \`\`\`#{ENV["APP_ROOT"]}\`\`\` user_id: \`\`\`#{user.id}\`\`\`  role: \`\`\`#{user.role}\`\`\` wallet_id: \`\`\`#{wallets}\`\`\` previous_net: \`\`\` #{net.to_f} \`\`\` today_balance: \`\`\` #{today_balance} \`\`\` batch_date: \`\`\` #{Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date} \`\`\`"
                    SlackService.notify(msg, "#qcbatches", true) if net.to_f !=  today_balance.to_f
                    updated_batches << TransactionBatch.new(affiliate_wallet_id: wallets, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, affiliate_balance: today_balance.to_f, merchant_wallet_id: nil)
                    result[:user_ids] << user.try(:id)
                  else
                    updated_batches << TransactionBatch.new(affiliate_wallet_id: wallets, batch_type: "sale",batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, merchant_wallet_id: nil, affiliate_balance: data["#{wallets}"].try(:[],:balance).to_f)
                    result[:user_ids] << user.try(:id)
                  end
                end
              end
            end
          end
          if updated_batches.present?
            TransactionBatch.import updated_batches, on_duplicate_key_update: {conflict_target: [:id], columns: [:updated_at, :batch_date,:batch_type, :total_net,:total_trxs, :merchant_wallet_id, :iso_wallet_id, :agent_wallet_id, :affiliate_wallet_id, :starting_balance,:iso_balance, :agent_balance, :affiliate_balance]}
            updated_batches = []
          end
        end
      end
    rescue => exc
      puts exc.message
      SlackService.notify(exc.try(:message), "#quickcard-internal", true)
      result[:error] = exc.try(:message)
    ensure
      cron_job.result = result
      cron_job.transaction_batch!
    end
  end
end