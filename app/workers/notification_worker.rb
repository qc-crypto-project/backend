class NotificationWorker
  include Sidekiq::Worker

  def perform(message, token, type)
    if type == "android"
      android_notification(message,token)
    elsif type == "ios"
      ios_notification(message,token)
    end   
  end

  private
  
  def android_notification(message,token)
    if (!Rpush::Gcm::App.find_by_name("QuickCard"))
      app = Rpush::Gcm::App.new
      # let's name this one pushme_droid
      app.name = "QuickCard"
      # FCM auth key from firebase project
      app.auth_key = ENV['FIRE_BASE_SERVER_KEY']
      app.connections = 1
      # save our app in db
      if app.save
        Rpush.apns_feedback
      end
    end
    n = Rpush::Gcm::Notification.new
    n.app = Rpush::Gcm::App.find_by_name("QuickCard")
    n.registration_ids = ["#{token}"]
    n.notification = {
      body: "#{message}",
      title: "#{message}",
      sound: 'default'
      }
    if n.save
      Rpush.push
    end
  end

  def ios_notification(message,token)
    if(!Rpush::Apns::App.find_by_name("ios_Quickcard"))
      app = Rpush::Apns::App.new
      app.name = "ios_Quickcard"
      app.certificate = File.read("#{Rails.root}/pushcert.pem")
      app.environment = "production" # APNs environment.
      app.password = "push"
      app.connections = 1
      app.save!
    end
    n = Rpush::Apns::Notification.new
    n.app = Rpush::Apns::App.find_by_name("ios_Quickcard")
    n.device_token = "#{token}" # 64-character hex string
    n.alert = "#{message}"
    if n.save
      Rpush.push
    end
  end

  def random_token
    Digest::SHA1.hexdigest([Time.now, rand].join)
  end
end
