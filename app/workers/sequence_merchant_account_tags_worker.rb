class SequenceMerchantAccountTagsWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(ledger_name,ledger_credential)
    #"quickcard-production-test"
    # PGYXVV7M6KKRKOQSPRRZDBVP4WWGY2ZN
    ledger = Sequence::Client.new(ledger_name: ledger_name, credential:  ledger_credential)
    Wallet.find_each do |wallet|
      begin
        location = wallet.location
        user = wallet.users.first
        p "----------------------------"
        p "updating wallet #{wallet.id}"
        user = User.find_by(id: wallet.user_id) if user.blank?
        if user.present?
          ledger.accounts.update_tags(id: "#{wallet.id}", tags: {type: wallet.wallet_type, name: wallet.name, number: user.try(:phone_number), email: user.try(:email)})
        elsif location.present?
          ledger.accounts.update_tags(id: "#{wallet.id}", tags: {type: wallet.wallet_type, name: wallet.name, number: location.try(:phone_number), email: location.try(:email)})
        else
          ledger.accounts.update_tags(id: "#{wallet.id}", tags: {type: wallet.wallet_type, name: wallet.name})
        end
      rescue Sequence::APIError => exc
        p "----------------------------"
        p exc
        p "----------------------------"
      end
    end
  end
end
