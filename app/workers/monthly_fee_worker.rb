class MonthlyFeeWorker
  include Sidekiq::Worker
  include ApplicationHelper
  include Admins::BuyRateHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
      date = Date.today
      job = CronJob.new(name: 'monthly_fee_worker', description: 'running monthly fee', success: true)
      result = {merchant_blocked: [], service_fee_charged: [],statement_fee_charged: [], misc_fee_charged: [],service_fee_not_charged: [],statement_fee_not_charged: [], misc_fee_not_charged: [], error: nil}
      locations = Location.includes(:merchant).joins(:fees).where("extract(day from fees.service_date) = :date OR extract(day from fees.statement_date) = :date OR extract(day from fees.misc_date) = :date", date: date.strftime("%d").to_i)
      locations.each do |location|
        unless location.wallets.count < 3 #= only work for location with primary wallet
          merchant = location.merchant
          if merchant.oauth_apps.first.present? && merchant.oauth_apps.first.is_block?
            result[:merchant_blocked] << location.merchant_id
          else
            fee_object = location.fees.buy_rate.first if location.fees.present?
            if fee_object.present?
              escrow_wallet = Wallet.escrow.first.id
              qc_wallet = Wallet.qc_support.first.id
              fee_class = Payment::FeeCommission.new(merchant, nil, fee_object, location.wallets.primary.first, location,TypesEnumLib::CommissionType::Monthly)
              service_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Service) if fee_class.present?
              statement_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Statement) if fee_class.present?
              misc_fee = fee_class.apply(nil, TypesEnumLib::TransactionSubType::Misc) if fee_class.present?
              if fee_object.service_date.present?
                unless fee_object.try(:service_last_charged).try(:strftime, '%m').to_i == date.strftime('%m').to_i
                  if fee_object.service_date.strftime('%d').to_i == date.strftime('%d').to_i
                    if service_fee.present?
                        if service_fee["amount"] > 0
                          gbox_fee = service_fee.try(:[],"splits").try(:[],"gbox").try(:[], "service").to_f
                          iso_fee = service_fee.try(:[],"splits").try(:[],"iso").try(:[], "service").to_f
                          agent_fee = service_fee.try(:[],"splits").try(:[],"agent").try(:[], "service").to_f
                          affiliate_fee = service_fee.try(:[],"splits").try(:[],"affiliate").try(:[], "service").to_f
                          tags = {"fee_perc" => service_fee["splits"]}
                          db_transaction = create_transaction_helper_for_monthly_fee(escrow_wallet, location.wallets.primary.first.id, merchant,Wallet.escrow.first.users.first,"transfer",service_fee["amount"],TypesEnumLib::TransactionSubType::Service,TypesEnumLib::CommissionType::Monthly, gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
                          trans = SequenceLib.transfer_monthly_fees(service_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Service, escrow_wallet, qc_wallet, service_fee["splits"], merchant, location)
                          if trans.present?
                            fee_object.update(service_last_charged: DateTime.current)
                            result[:service_fee_charged] << location.id
                            trans = {id: trans.actions.first.id ,timestamp: trans.timestamp, asset: trans.actions.first.flavor_id, type: trans.actions.first.tags["type"], sub_type: trans.actions.first.tags["sub_type"] , destination: trans.actions.first.destination_account_id, issue_amount: trans.actions.first.amount.to_f/100 , source: trans.actions.first.source_account_id, tags: trans.actions.first.tags, complete_trans: trans.actions}
                            transaction_update(db_transaction,trans)
                            parsed_transactions = parse_block_transactions(trans[:complete_trans], trans[:timestamp])
                            save_block_trans(parsed_transactions) if parsed_transactions.present?
                          else
                            result[:service_fee_not_charged] << location.id
                          end
                        end
                    end
                  end
                end
              end
              if fee_object.statement_date.present?
                unless fee_object.try(:statement_last_charged).try(:strftime, '%m').to_i == date.strftime('%m').to_i
                  if fee_object.statement_date.strftime('%d').to_i == date.strftime('%d').to_i
                    if statement_fee.present?
                        if statement_fee["amount"] > 0
                          gbox_fee = statement_fee.try(:[],"splits").try(:[],"gbox").try(:[], "statement").to_f
                          iso_fee = statement_fee.try(:[],"splits").try(:[],"iso").try(:[], "statement").to_f
                          agent_fee = statement_fee.try(:[],"splits").try(:[],"agent").try(:[], "statement").to_f
                          affiliate_fee = statement_fee.try(:[],"splits").try(:[],"affiliate").try(:[], "statement").to_f
                          tags = {"fee_perc" => statement_fee["splits"]}
                          db_transaction = create_transaction_helper_for_monthly_fee(escrow_wallet, location.wallets.primary.first.id, merchant,Wallet.escrow.first.users.first,"transfer",statement_fee["amount"],TypesEnumLib::TransactionSubType::Statement,TypesEnumLib::CommissionType::Monthly, gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
                          trans = SequenceLib.transfer_monthly_fees(statement_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Statement, escrow_wallet, qc_wallet, statement_fee["splits"], merchant, location)
                          if trans.present?
                            fee_object.update(statement_last_charged: DateTime.current)
                            result[:statement_fee_charged] << location.id
                            trans = {id: trans.actions.first.id ,timestamp: trans.timestamp, asset: trans.actions.first.flavor_id, type: trans.actions.first.tags["type"], sub_type: trans.actions.first.tags["sub_type"] , destination: trans.actions.first.destination_account_id, issue_amount: trans.actions.first.amount.to_f/100 , source: trans.actions.first.source_account_id, tags: trans.actions.first.tags, complete_trans: trans.actions}
                            transaction_update(db_transaction,trans)
                            parsed_transactions = parse_block_transactions(trans[:complete_trans], trans[:timestamp])
                            save_block_trans(parsed_transactions) if parsed_transactions.present?
                          else
                            result[:statement_fee_not_charged] << location.id
                          end
                        end
                    end
                  end
                end
              end
              if fee_object.misc_date.present?
                unless fee_object.try(:misc_last_charged).try(:strftime, '%m').to_i == date.strftime('%m').to_i
                  if fee_object.misc_date.strftime('%d').to_i == date.strftime('%d').to_i
                    if misc_fee.present?
                        if misc_fee["amount"] > 0
                          gbox_fee = misc_fee.try(:[],"splits").try(:[],"gbox").try(:[], "misc").to_f
                          iso_fee = misc_fee.try(:[],"splits").try(:[],"iso").try(:[], "misc").to_f
                          agent_fee = misc_fee.try(:[],"splits").try(:[],"agent").try(:[], "misc").to_f
                          affiliate_fee = misc_fee.try(:[],"splits").try(:[],"affiliate").try(:[], "misc").to_f
                          tags = {"fee_perc" => misc_fee["splits"]}
                          db_transaction = create_transaction_helper_for_monthly_fee(escrow_wallet, location.wallets.primary.first.id, merchant,Wallet.escrow.first.users.first,"transfer",misc_fee["amount"],TypesEnumLib::TransactionSubType::Misc,TypesEnumLib::CommissionType::Monthly, gbox_fee,iso_fee,agent_fee,affiliate_fee,tags)
                          trans = SequenceLib.transfer_monthly_fees(misc_fee["amount"], location.wallets.primary.first.id, TypesEnumLib::TransactionSubType::Misc, escrow_wallet, qc_wallet, misc_fee["splits"], merchant, location)
                          if trans.present?
                            fee_object.update(misc_last_charged: DateTime.current)
                            result[:misc_fee_charged] << location.id
                            trans = {id: trans.actions.first.id ,timestamp: trans.timestamp, asset: trans.actions.first.flavor_id, type: trans.actions.first.tags["type"], sub_type: trans.actions.first.tags["sub_type"] , destination: trans.actions.first.destination_account_id, issue_amount: trans.actions.first.amount.to_f/100 , source: trans.actions.first.source_account_id, tags: trans.actions.first.tags, complete_trans: trans.actions}
                            transaction_update(db_transaction,trans)
                            parsed_transactions = parse_block_transactions(trans[:complete_trans], trans[:timestamp])
                            save_block_trans(parsed_transactions) if parsed_transactions.present?
                          else
                            result[:misc_fee_not_charged] << location.id
                          end
                        end
                    end
                  end
                end
              end
            end
          end
        end
      end
      job.result = result
      job.monthly_fee!
      # end
  end
end
