class PartialRefundReqMerchant
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default',:retry => 0

  def perform(*args)
    job = CronJob.new(name: "counter_offer_review_expire",description: "counter review expire , system auto refund")
    result = {updated_records: [] , not_updated_records: [],error: nil}
    begin
      allowed_days = CUSTOMER_RESPOND_ALLOWED || 12
      reminder_days = REMINDER_DAYS || 2
      date = HoldInRear.pst_beginning_of_day(DateTime.now) - allowed_days.days
      reminder_date = HoldInRear.pst_beginning_of_day(DateTime.now) - reminder_days.days
      customer_disputes = CustomerDispute.where(dispute_type: "refund").where(status: "pending")
      customer_disputes.each do |customer_dispute|
        dispute_request = customer_dispute.dispute_requests.where(user_id: customer_dispute.merchant_id).where(status: "pending").where(request_type: "merchant_offer").where("updated_at <= ? " , date).last
        reminder_dispute_request = customer_dispute.dispute_requests.where(user_id: customer_dispute.merchant_id).where(status: "pending").where(request_type: "merchant_offer").where("date(updated_at) <= ? " , reminder_date.to_date).last
        if reminder_dispute_request #&& Date.today != reminder_dispute_request.created_at.to_date
          if (Date.today - reminder_dispute_request.created_at.to_date).to_i % 2 == 0
            times = ((Date.today - reminder_dispute_request.created_at.to_date).to_i / 2).ordinalize
            website = customer_dispute.dispute_transaction.website
            subject = "#{times} notice for refund request for #{customer_dispute.id} from #{website}."
            UserMailer.partial_refund_request_merchant(customer_dispute.id,customer_dispute.amount,subject).deliver_later
          end
        end
        if dispute_request
          transaction = customer_dispute.dispute_transaction
          if transaction.tracker.present?
            transaction.tracker.update(return_status: "cancel")
          end
          customer_dispute.update(status: "declined")
          customer_dispute.dispute_requests.where(user_id: customer_dispute.merchant_id).update(status: "declined")
          result[:updated_records] << {id: customer_dispute.id, message: "after refund dispute close"}
        end
      end
      job.result = result
      job.return_status_review_expire!
    rescue Exception => e
      result[:error] = e
      job.counter_offer_review_expire!
    end
  end
end