# class BulkcheckWorker
#   include Sidekiq::Worker
#   sidekiq_options queue: 'critical', :retry => true
#
#   def perform(params)
#     checks = BulkCheckInstance.find(params["bulk_instance_id"].to_i).bulk_checks
#     checks.each do |check|
#       create_single_check_from_bulk(check, params[:wallet_id].to_i)
#     end
#   end
#
#   private
#
#   def create_single_check_from_bulk(bulk_check, wallet_id)
#     #creating bulk checks for both direct and digital checks
#     app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
#     if bulk_check.account_number.present? && bulk_check.routing_number.present?
#       dd = 1
#     else
#       dd = 0
#     end
#     type = 0
#     amount = bulk_check.amount.to_f
#     recipient = bulk_check.email
#     name = bulk_check.name+" "+bulk_check.last_name
#     sender = current_user.email
#     description = "Bulk Check"
#
#     if !wallet_id.blank?
#       begin
#         @user = current_user
#         @wallet = Wallet.find(wallet_id)
#         balance = show_balance(@wallet.id, @user.ledger).to_f
#         if @user.present? && @wallet.present?
#           location = @wallet.location
#           amount_sum = 0
#           fee_lib = FeeLib.new
#           location_fee = location.fees.send_check.first if location.present?
#           redeem_fee = location.fees.redeem_fee.first if location.present?
#           check_fee = location_fee.send_check.to_f if location_fee.present?
#           fee_perc = deduct_fee(redeem_fee.redeem_fee.to_f, amount)
#           amount_sum = amount + check_fee + fee_perc.to_f
#
#           if dd.present? && dd.to_i == 1
#             url = URI("https://checkbook.io/v3/check/direct")
#             raise "Account Type is missing " unless "CHECKING".present?
#             raise "Unmatched Account and Routing number !" unless bulk_check.account_number == bulk_check.confirm_account
#           else
#             url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/digital")
#           end
#
#           if  balance < amount_sum
#             text = "Insufficient balance"
#           end
#
#
#           @recipient= recipient
#           http = Net::HTTP.new(url.host,url.port)
#           http.use_ssl = true
#           http.verify_mode = OpenSSL::SSL::VERIFY_PEER
#           request = Net::HTTP::Post.new(url)
#           if dd.present? && dd.to_i == 1
#             account_number = bulk_check.account_number
#             account_type = "CHECKING"
#             routing_number = bulk_check.routing_number
#             request.body = {name: name, recipient: recipient, amount: amount,description: description ,account_number: account_number , routing_number: routing_number ,account_type: account_type }.to_json
#           else
#             if type == 1
#               request.body = {name: name, recipient: recipient, amount: amount_sum,description: description}.to_json
#             else
#               request.body = {name: name, recipient: recipient, amount: amount,description: description}.to_json
#             end
#           end
#           request["Authorization"] = @@check_book_key+":"+@@check_book_secret
#           request["Content-Type"] ='application/json'
#           response = http.request(request)
#
#           case response
#             when Net::HTTPSuccess
#               digital_check = JSON.parse(response.body)
#               if response.message == "CREATED"
#                 check= TangoOrder.new
#                 check.user_id = @user.id
#                 check.name = digital_check["name"]
#                 check.amount = digital_check["amount"]
#                 if type == 1
#                   check.actual_amount = amount
#                   check.check_fee = check_fee
#                   check.fee_perc = fee_perc
#                 end
#                 check.recipient = digital_check["recipient"]
#                 check.checkId = digital_check["id"]
#                 check.number = digital_check["number"]
#                 check.description = digital_check["description"]
#                 check.catalog_image = digital_check["image_uri"]
#                 check.status = digital_check["status"]
#                 check.order_type = type
#                 check.settled = false
#                 if dd.present?
#                   check.account_number = bulk_check.account_number.last(4)
#                   # check.routing_number= params[:check][:routing_number]
#                 end
#                 check.wallet_id = @wallet.id
#                 if account_number.nil?
#                   account_number = nil
#                   send_via = 'email'
#                 else
#                   account_number = account_number.last(4)
#                   send_via = 'direct_deposit'
#                 end
#                 send_check_user_info = {
#                     name: check.name,
#                     check_email: check.recipient,
#                     check_id: check.checkId,
#                     check_number: check.number
#                 }
#                 if @user.MERCHANT?
#
#                   qc_wallet = Wallet.where(wallet_type: 3).first
#                   company = @user.company
#                   company_user =  company.users.where(email: company.company_email).first
#                   company_wallet = company_user.wallets.first.id
#                   location_fee = location.fees.send_check.first if location.present?
#                   redeem_fee = location.fees.redeem_fee.first if location.present?
#                   check_fee = location_fee.send_check.to_f if location_fee.present?
#                   fee_perc = deduct_fee(redeem_fee.redeem_fee.to_f, amount)
#                   fee_sum = amount + check_fee + fee_perc.to_f
#                   if show_balance(@wallet.id, @user.ledger).to_f >= fee_sum
#                     total_fee_deduct = check_fee.to_f + fee_perc.to_f
#                     if total_fee_deduct > 0
#                       lib= FeeLib.new
#                       user_info = lib.divide_send_check_fee(location,location_fee,company,redeem_fee, fee_perc ,check_fee, company_wallet )
#                       withdraw = withdraw(amount,@wallet.id, total_fee_deduct, 'send_check',account_number,send_via,user_info, send_check_user_info, @user.ledger)
#                     else
#                       withdraw = withdraw(amount, @wallet.id, 0, 'send_check',account_number,send_via,user_info, send_check_user_info,@user.ledger)
#                     end
#
#                   else
#                     raise StandardError 'Error Code 2041 Insuficient balance You can not create check!'
#                   end
#
#                 else
#                   withdraw(digital_check["amount"],@wallet.id, fee_lib.get_card_fee(params[:amount].to_f, 'echeck'), 'send_check', account_number, send_via,nil,send_check_user_info, @user.ledger)
#                 end
#                 check.save
#                 text = "Success"
#               else
#                 text = "Something Wrong"
#               end
#
#             when Net::HTTPUnauthorized
#               text = "Unauthorized"
#             when Net::HTTPNotFound
#               text = "Record Not found"
#             when Net::HTTPServerError
#               text = "Not responding"
#             else
#               error = JSON.parse(response.body)
#               text = "#{error["error"]}"
#           end
#         else
#           text = "Something Wrong"
#         end
#       rescue => ex
#         p"-----------EXCEPTION HANDLED #{ex.message}"
#         text = "Something Wrong"
#       end
#     else
#       text = "Try again"
#     end
#     bulk_check.update(status: text, response: response, is_processed: true)
#   end
# end
#
#
