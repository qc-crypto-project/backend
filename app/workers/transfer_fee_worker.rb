class TransferFeeWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  def perform(*args)
    Transfer.send_transfer
  end
end