class AdminReportWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  include Admins::ReportsHelper
  include AdminsHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(first_date=nil, second_date=nil, type=nil, params=nil, offset=nil, session_id=nil, user=nil)
    @host = session_id
    params = params.class == String && params.valid_json? ? eval(params) : params
    case type
    when TypesEnumLib::WorkerType::Sale
      sale_report(first_date, second_date, params, offset)
    when TypesEnumLib::WorkerType::Merchant
      merchant_report(first_date, second_date, params, user)
    when TypesEnumLib::WorkerType::Wallet
      wallet_report(params)
    when TypesEnumLib::WorkerType::FeeSummary
      fee_summary_report(first_date, second_date, params)
    when TypesEnumLib::WorkerType::Withdrawal
      withdrawal_report(first_date, second_date, params, user)
    when TypesEnumLib::WorkerType::IsoProfitSplit
      iso_profit_split_report(first_date, second_date, params)
    when TypesEnumLib::WorkerType::TerminalID
      terminal_id_report(first_date,second_date,params)
    when TypesEnumLib::WorkerType::BlockVIPExport
      block_vip_export(params, user)
    when TypesEnumLib::WorkerType::AffiliateProgram
      affiliate_program_report(first_date,second_date,params,user)
    else
      return
    end
  end

  def sale_report(first_date, second_date, params, offset)

    is_types_present = params[:types].first.present?
    is_categories_present = params[:categories].first.present?

    if is_types_present && params[:types].first.class == String
      types = JSON.parse(params[:types].first)
      params[:types] = types
    end
    if params[:dba_name].present? || is_categories_present
      wallets = {}
      if params[:dba_name].present? && params[:dba_name].first != "null"
        params[:dba_name] = JSON.parse(params[:dba_name].first)
        if !params[:dba_name].include?("all")
          params[:dba_name].each do |param|
            temp = JSON.parse(param)
            wallets[temp["id"]] = temp["name"]
          end
        elsif params[:dba_name].include?("all")
          locations = Location.all.select(:id, :business_name).map{|e| [e.wallets.primary.first.id, e.business_name]}
          locations.each do |location|
            wallets[location.first] = location.second
          end
        end
      elsif is_categories_present && params[:categories].first != "null"
        params[:categories] = JSON.parse(params[:categories].first)
        locations = Location.where(category_id: params[:categories]).select(:id, :business_name).map{|e| [e.wallets.primary.first.id, e.business_name]}
        locations.each do |location|
          wallets[location.first] = location.second
        end
      end
    end
    qc_wallet = Wallet.qc_support.first.id
    data = []
    if wallets.present? && is_types_present
      Transaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets.keys)
          .where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date)
          .where(main_type: params[:types], status: ["approved", "refunded","complete"])
          .select('transactions.id AS id', 'transactions.seq_transaction_id AS seq_transaction_id','transactions.sender_name AS sender_name', 'transactions.receiver_name AS receiver_name','transactions.timestamp AS timestamp','transactions.main_type AS main_type',
                  'transactions.total_amount AS total_amount','transactions.payment_gateway_id AS payment_gateway_id', 'transactions.amount AS amount','transactions.privacy_fee As privacy_fee',
                  'transactions.tip AS tip','transactions.fee AS fee','transactions.net_amount AS net_amount','transactions.reserve_money AS reserve_money',
                  'transactions.gbox_fee AS gbox_fee','transactions.tags AS tags','transactions.iso_fee AS iso_fee',
                  'transactions.agent_fee AS agent_fee','transactions.affiliate_fee AS affiliate_fee')
          .find_each(batch_size: 1000) do |report|
        data << {
            id: report.seq_transaction_id.first(6),
            timestamp: report.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
            type: report.main_type.try(:titleize),
            gateway: report.payment_gateway.try(:name),
            sender: report.sender_name,
            receiver: report.receiver_name,
            total_amount: number_with_precision(number_to_currency(report.total_amount.to_f), precision: 2),
            amount: number_with_precision(number_to_currency(report.amount.to_f), precision: 2),
            privacy_fee: number_with_precision(number_to_currency(report.privacy_fee.to_f), precision: 2),
            tip: number_with_precision(number_to_currency(report.tip.to_f), precision: 2),
            fee: number_with_precision(number_to_currency(report.fee.to_f), precision: 2),
            net_amount: number_with_precision(number_to_currency(report.net_amount.to_f), precision: 2),
            reserve_money: number_with_precision(number_to_currency(report.reserve_money.to_f), precision: 2),
            gbox_fee: number_with_precision(number_to_currency(report.gbox_fee.to_f), precision: 2),
            baked_gbox: number_with_precision(number_to_currency(baked_gbox_fee(report.tags.try(:[], "fee_perc").try(:[], "super_users_details"), qc_wallet)), precision: 2),
            iso_fee: number_with_precision(number_to_currency(report.iso_fee.to_f), precision: 2),
            baked_iso: number_with_precision(number_to_currency(baked_iso_fee(report.tags.try(:[], "fee_perc").try(:[], "super_users_details"), report.tags.try(:[], "fee_perc").try(:[], "iso").try(:[],"wallet"))), precision: 2),
            agent_fee: number_with_precision(number_to_currency(report.agent_fee.to_f), precision: 2),
            affiliate_fee: number_with_precision(number_to_currency(report.affiliate_fee.to_f), precision: 2)
        }

      end
    elsif wallets.present? && (!is_types_present || !is_categories_present)
      Transaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets.keys)
          .where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date)
          .where(main_type: [TypesEnumLib::TransactionViewTypes::Sale,TypesEnumLib::TransactionViewTypes::SaleTip,TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::CreditCard], status: ["approved", "refunded","complete"])
          .select('transactions.id AS id', 'transactions.seq_transaction_id AS seq_transaction_id','transactions.sender_name AS sender_name', 'transactions.receiver_name AS receiver_name','transactions.timestamp AS timestamp','transactions.main_type AS main_type',
                  'transactions.total_amount AS total_amount','transactions.payment_gateway_id AS payment_gateway_id', 'transactions.amount AS amount','transactions.privacy_fee As privacy_fee',
                  'transactions.tip AS tip','transactions.fee AS fee','transactions.net_amount AS net_amount','transactions.reserve_money AS reserve_money',
                  'transactions.gbox_fee AS gbox_fee','transactions.tags AS tags','transactions.iso_fee AS iso_fee',
                  'transactions.agent_fee AS agent_fee','transactions.affiliate_fee AS affiliate_fee')
          .find_each(batch_size: 1000) do |report|
        data << {
            id: report.seq_transaction_id.first(6),
            timestamp: report.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
            type: report.main_type.try(:titleize),
            gateway: report.payment_gateway.try(:name),
            sender: report.sender_name,
            receiver: report.receiver_name,
            total_amount: number_with_precision(number_to_currency(report.total_amount.to_f), precision: 2),
            amount: number_with_precision(number_to_currency(report.amount.to_f), precision: 2),
            privacy_fee: number_with_precision(number_to_currency(report.privacy_fee.to_f), precision: 2),
            tip: number_with_precision(number_to_currency(report.tip.to_f), precision: 2),
            fee: number_with_precision(number_to_currency(report.fee.to_f), precision: 2),
            net_amount: number_with_precision(number_to_currency(report.net_amount.to_f), precision: 2),
            reserve_money: number_with_precision(number_to_currency(report.reserve_money.to_f), precision: 2),
            gbox_fee: number_with_precision(number_to_currency(report.gbox_fee.to_f), precision: 2),
            baked_gbox: number_with_precision(number_to_currency(baked_gbox_fee(report.tags.try(:[], "fee_perc").try(:[], "super_users_details"), qc_wallet)), precision: 2),
            iso_fee: number_with_precision(number_to_currency(report.iso_fee.to_f), precision: 2),
            baked_iso: number_with_precision(number_to_currency(baked_iso_fee(report.tags.try(:[], "fee_perc").try(:[], "super_users_details"), report.tags.try(:[], "fee_perc").try(:[], "iso").try(:[],"wallet"))), precision: 2),
            agent_fee: number_with_precision(number_to_currency(report.agent_fee.to_f), precision: 2),
            affiliate_fee: number_with_precision(number_to_currency(report.affiliate_fee.to_f), precision: 2)
        }

      end
    elsif is_types_present && wallets.blank?
      Transaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date)
          .where(main_type: params[:types], status: ["approved", "refunded","complete"])
          .select('transactions.id AS id', 'transactions.seq_transaction_id AS seq_transaction_id','transactions.sender_name AS sender_name', 'transactions.receiver_name AS receiver_name','transactions.timestamp AS timestamp','transactions.main_type AS main_type',
                  'transactions.total_amount AS total_amount','transactions.payment_gateway_id AS payment_gateway_id', 'transactions.amount AS amount','transactions.privacy_fee As privacy_fee',
                  'transactions.tip AS tip','transactions.fee AS fee','transactions.net_amount AS net_amount','transactions.reserve_money AS reserve_money',
                  'transactions.gbox_fee AS gbox_fee','transactions.tags AS tags','transactions.iso_fee AS iso_fee',
                  'transactions.agent_fee AS agent_fee','transactions.affiliate_fee AS affiliate_fee')
          .find_each(batch_size: 1000) do |report|
        data << {
            id: report.seq_transaction_id.first(6),
            timestamp: report.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
            type: report.main_type.try(:titleize),
            gateway: report.payment_gateway.try(:name),
            sender: report.sender_name,
            receiver: report.receiver_name,
            total_amount: number_with_precision(number_to_currency(report.total_amount.to_f), precision: 2),
            amount: number_with_precision(number_to_currency(report.amount.to_f), precision: 2),
            privacy_fee: number_with_precision(number_to_currency(report.privacy_fee.to_f), precision: 2),
            tip: number_with_precision(number_to_currency(report.tip.to_f), precision: 2),
            fee: number_with_precision(number_to_currency(report.fee.to_f), precision: 2),
            net_amount: number_with_precision(number_to_currency(report.net_amount.to_f), precision: 2),
            reserve_money: number_with_precision(number_to_currency(report.reserve_money.to_f), precision: 2),
            gbox_fee: number_with_precision(number_to_currency(report.gbox_fee.to_f), precision: 2),
            baked_gbox: number_with_precision(number_to_currency(baked_gbox_fee(report.tags.try(:[], "fee_perc").try(:[], "super_users_details"), qc_wallet)), precision: 2),
            iso_fee: number_with_precision(number_to_currency(report.iso_fee.to_f), precision: 2),
            baked_iso: number_with_precision(number_to_currency(baked_iso_fee(report.tags.try(:[], "fee_perc").try(:[], "super_users_details"), report.tags.try(:[], "fee_perc").try(:[], "iso").try(:[],"wallet"))), precision: 2),
            agent_fee: number_with_precision(number_to_currency(report.agent_fee.to_f), precision: 2),
            affiliate_fee: number_with_precision(number_to_currency(report.affiliate_fee.to_f), precision: 2)
        }

      end
    end
    data = data.reverse if data.present?
    report_csv = CSV.generate do |csv|
      csv << ["Block Id", "Timestamp", "Type", "Gateway", "Sender Name", "Receiver Name",
              "Total Amount", "TXN Amount", "Privacy Fee", "Tip", "Fee", "Net Amount",
              "Reserve Amount", "GBOx Fee", "GBOX Baked Split", "ISO Fee", "ISO Baked Split", "Agent Fee", "Affiliate Fee"]
      data.each do |report|
        csv << [
            report[:id],
            report[:timestamp],
            report[:type],
            report[:gateway],
            report[:sender],
            report[:receiver],
            report[:total_amount],
            report[:amount],
            report[:privacy_fee],
            report[:tip],
            report[:fee],
            report[:net_amount],
            report[:reserve_money],
            report[:gbox_fee],
            report[:baked_gbox],
            report[:iso_fee],
            report[:baked_iso],
            report[:agent_fee],
            report[:affiliate_fee]
        ]
      end
    end
    file = StringIO.new(report_csv)
    qc_file = QcFile.new(name: "sale report", start_date: first_date.to_date, end_date: second_date.to_date, status: :sale)
    qc_file.image = file
    qc_file.image.instance_write(:content_type, 'text/csv')
    qc_file.image.instance_write(:file_name, "sale.csv")
    qc_file.save
    ActionCable.server.broadcast "report_channel_#{@host}", channel_name: "report_channel_#{@host}", action: 'adminreport' , message: qc_file.id
  end

  def merchant_report(first_date, second_date, params, user=nil)
    if params[:status] == "null"
      params[:status] = ""
    end
    params[:category] = JSON.parse(params[:category].first)
    if params[:category].present?
      if params[:category] == "null"
        params[:category] = ""
      end
    end
    params[:dba_name] = JSON.parse(params[:dba_name].first)
    if params[:dba_name].present?
      if params[:dba_name] == "null"
        params[:dba_name] = ""
      end
    end
    from_worker = true
    begin
      report_first_date = first_date.to_datetime.utc
      report_second_time = second_date.to_datetime.utc
      report = Report.new(user_id: user, first_date: report_first_date, second_date:report_second_time, offset: nil, report_type: "Merchant report", status: "pending",transaction_type: nil,read: false)
      report.params = params
      report.save
      result = getting_merchant_report(params, first_date, second_date, nil, from_worker)
      @details = result.try(:[],:details)
      report_csv = CSV.generate do |csv|
        csv << ["Date", "M-ID","DBA Name", "Status","Category","Total Volume", "Total Txn", "Avg Per Txn", "CBK %", "CBK Total Amount", "Total CBK TXN","Refund %", "Refund Total Amount", "Total Refund TRX","Total Fee","Gbox Fee","Gbox Net Amount"]
        @details.each do |report|
          csv << [report[:date_range], report[:m_id], report[:dba_name], report[:status], report[:category],number_with_precision(number_to_currency(report[:total_volume]), precision: 2),
                  report[:total_txn],number_with_precision(number_to_currency(report[:avg_per_txn]), precision: 2),number_with_precision(report[:cbk_perc]),
                  number_with_precision(number_to_currency(report[:cbk_total_amount]), precision: 2), report[:cbk_count],
                  number_with_precision(report[:refund_perc], precision: 1), number_with_precision(number_to_currency(report[:refund_amount]), precision: 2),
                  report[:refund_count], number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),number_with_precision(number_to_currency(report[:net_gbox]), precision: 2)]
        end
      end
      file = StringIO.new(report_csv)
      qc_file = QcFile.new(name: "Merchant report", start_date: first_date, end_date: second_date, status: :merchant)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "merchant.csv")
      qc_file.save
      report.file_id = {id: qc_file.id}
      report.status = "completed"
      report.message = "done"
      report.save
      ActionCable.server.broadcast "report_channel_#{@host}", channel_name: "report_channel_#{@host}", action: 'merchant_report' , message: qc_file.id
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
    ensure
      report.save
    end

  end


  def affiliate_program_report(first_date, second_date, params,user = nil)
    begin
      report_first_date = first_date.to_datetime.utc
      report_second_time = second_date.to_datetime.utc
      params[:dba_name] =  JSON.parse params[:dba_name].first
      params[:affiliate_program] = JSON.parse params[:affiliate_program].first
      report = Report.new(user_id: user, first_date: report_first_date, second_date:report_second_time, offset: nil, report_type: "Affiliate Program report", status: "pending",transaction_type: nil,read: false)
      report.params = params
      report.save
      result = getting_affiliate_program_report(params, first_date, second_date)
      @details = result
      report_csv = CSV.generate do |csv|
        csv << ["Date Range", "Admin Name","Affiliate ID", "Affiliate Name","Total Amount Receiver","Total TXN Count"]
        @details.each do |report|
          csv << [report[:date_range], report[:admin_name], report[:affiliate_id], report[:affiliate_name], number_with_precision(number_to_currency(report[:total_amount]), precision: 2),report[:total_count]]
        end
      end
      file = StringIO.new(report_csv)
      qc_file = QcFile.new(name: "Affiliate Program report", start_date: first_date, end_date: second_date, status: :merchant)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "affiliate_program.csv")
      qc_file.save
      report.file_id = {id: qc_file.id}
      report.status = "completed"
      report.message = "done"
      report.save
      ActionCable.server.broadcast "report_channel_#{@host}", channel_name: "report_channel_#{@host}", action: 'affiliate_program_report' , message: qc_file.id
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
    ensure
      report.save
    end

  end

  def wallet_report(params)
    begin
      report = Report.new(user_id: 1, offset: nil, report_type: "Wallet report", status: "pending",transaction_type: nil,read: false)
      report.params = params
      report.save
      params = params.symbolize_keys!
      #@details = getting_wallet_report(locations)
      if params[:partner_name].present? && params[:partner_name].first != ""
        params[:partner_name] = JSON.parse(params[:partner_name].first)
        if params[:partner_name].present?
          if params[:partner_name] == "null"
            params[:partner_name] = ""
          end
        end
      end

      params[:dba_name] = JSON.parse(params[:dba_name].first)
      if params[:dba_name].present?
        if params[:dba_name] == "null"
          params[:dba_name] = ""
        end
      end
      wallets = {}
      if params[:dba_name].present?  && params[:dba_name].first != ""
        if !params[:dba_name].include?("all")
          params[:dba_name].each do |param|
            temp = JSON.parse(param)
            wallets[temp["id"]] = [temp["name"]]
          end
        elsif params[:dba_name].include?("all")
          locations = Location.select("locations.id,locations.business_name")
          locations.each do |location|
            wallets[location.id] = [location.business_name]
          end
        end
      elsif params[:partner_name].present?  && params[:partner_name].first != ""
        if !params[:partner_name].include?("all")
          params[:partner_name].each do |param|
            temp = JSON.parse(param)
            wallets[temp["id"]] = [temp["name"]]
          end
        elsif params[:partner_name].include?("all")
          locations = User.where(role: [:iso, :agent, :affiliate]).select("users.id,users.name")
          locations.each do |location|
            wallets[location.id] = [location.name]
          end
        end
      end
      details = []
      wallets.each do |key,value|
        if params[:partner_name].present? && params[:partner_name].first != ""
          user = User.joins(:wallets).where(id: key).last
          main_wallet = user.wallets.last
          main_balance = main_wallet.try(:balance).to_f
          merchant = user.try(:id)
          total_balance = main_balance
        elsif params[:dba_name].present? && params[:dba_name].first != ""
          location = Location.joins(:wallets,:merchant).where(id: key).last
          main_wallet = location.wallets.primary.last
          main_balance = main_wallet.try(:balance).to_f
          reserve_wallet = location.wallets.reserve.last.balance.to_f || 0
          tip_wallet_balance = location.wallets.tip.last.balance.to_f
          total_balance = main_balance.to_f + reserve_wallet.to_f + tip_wallet_balance.to_f
          merchant = location.try(:merchant).try(:id)
        end
        if (params[:not_0].present? && params[:not_0] == "true") && (main_balance.to_f == 0 && reserve_wallet.to_f == 0 && tip_wallet_balance.to_f == 0 )
        else
          tx_detail = {
              m_id: "M-#{merchant}",
              dba_name: value.first,
              main_wallet_funds: main_balance,
              reserve_wallet_funds: reserve_wallet,
              total: total_balance,
              tip_balance: tip_wallet_balance
          }
          details.push(tx_detail)
        end
      end
      report_csv = CSV.generate do |csv|
        csv << ["Date","MID","DBA Name", "Main Wallet Funds","Tip Wallet Funds","Reserve Wallet Funds","Total Amount"]
        details.each do |report|
          csv << [Date.today.strftime("%m/%d/%Y"),report[:m_id],report[:dba_name], number_with_precision(number_to_currency(report[:main_wallet_funds].to_f), precision: 2),number_with_precision(number_to_currency(report[:tip_balance].to_f), precision: 2), number_with_precision(number_to_currency(report[:reserve_wallet_funds].to_f), precision: 2),number_with_precision(number_to_currency(report[:total]), precision: 2)]
        end
      end
      file = StringIO.new(report_csv)
      qc_file = QcFile.new(name: "Wallet report", start_date: Time.now, end_date: Time.now, status: :wallet)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "wallet.csv")
      qc_file.save
      report.file_id = {id: qc_file.id}
      report.status = "completed"
      report.message = "done"
      report.save
      ActionCable.server.broadcast "report_channel_#{@host}", channel_name: "report_channel_#{@host}", action: 'adminreport' , message: qc_file.id
    rescue => exc
      puts exc.message
      report.status = "crashed"
      report.message = exc.message
    ensure
      report.save
    end
  end

  def withdrawal_report(first_date, second_date, params, user=nil)
    if params[:sub_type] == "detail_withdrawal"
      report_name = "Withdrawal Detail"
    elsif params[:sub_type] == "summary_withdrawal"
      report_name = "Withdrawal Summary"
    end
    report = Report.new(user_id: user, first_date: first_date, second_date: second_date, offset: nil, report_type: "#{report_name} report", status: "pending",transaction_type: nil,read: false)
    report.params = params
    report.save
    dba_name = []
    params = params.with_indifferent_access
    params[:dba_name].each do |dba|
      dba_name.push(JSON.parse(dba))
    end

    params[:fee_categories] = JSON.parse(params[:fee_categories].first)
    if params[:fee_categories].present?
      if params[:fee_categories] == "null"
        params[:fee_categories] = ""
      end
    end
    params[:status_types] = JSON.parse(params[:status_types].first)
    if params[:status_types].present?
      if params[:status_types] == "null"
        params[:status_types] = ""
      end
    end
    params[:ach_category] = JSON.parse(params[:ach_category].first) if params[:ach_category].present?
    if params[:ach_category].present?
      if params[:ach_category] == "null"
        params[:ach_category] = ""
      end
    end
    params[:dba_name] = dba_name.first
    wallets = {}
    transactions = []
    if params["dba_name"].present?
      params["dba_name"].each do |param|
        temp = JSON.parse(param)
        wallets[temp["id"]] = temp["name"]
      end
    elsif params["select_all"].present?
      locations = Location.joins(:wallets).where("wallets.wallet_type = ?", 0).select("locations.id,locations.business_name,wallets.id as wallet_id")
      locations.each do |location|
        wallets[location.wallet_id] = location.business_name
      end
    end
    if params[:fee_categories].present?
      params[:fee_categories].each do |order_type|
        order_type.gsub!('ACH', 'instant_ach')
        order_type.gsub!('Send Check', 'check')
      end
      order_type = params[:fee_categories]
    else
      order_type = ["check", "instant_ach","instant_pay"]
    end
    if params[:status_types].present?
      statuses = params[:status_types]
    else
      statuses = ["PAID","UNPAID","IN_PROCESS", "IN_PROGRESS","VOID", "FAILED", "PENDING", I18n.t("withdraw.paid_by_wire")]
    end
    cbd_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
    dispensary_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
    if params[:ach_category].present?
      is_ach_international = false
      is_other_present = false
      category_wallet = []
      cbd_dis_wallets = []
      if params[:ach_category].include?(I18n.t("withdraw.ACH_international"))
        params[:ach_category] -= %w{ACH_International}
        is_ach_international = true
      end
      if params[:ach_category].include?(I18n.t("withdraw.CBD"))
        category_wallet = cbd_wallets
      end
      if params[:ach_category].include?(I18n.t("withdraw.dispensary"))
        category_wallet = category_wallet + dispensary_wallets
      end
      if params[:ach_category].include?(I18n.t("withdraw.other"))
        cbd_dis_wallets = cbd_wallets + dispensary_wallets
        is_other_present = true
      end
      category_wallet_check = nil
      if is_ach_international
        params[:ach_category] =  params[:ach_category] + ["ACH_International"]
      end
      if category_wallet.present?
        category_wallet_check = true
      else
        category_wallet_check = false
      end
    end
    condition = wallets.present? ? {wallet_id: wallets.keys, order_type: order_type, status: statuses} : category_wallet_check ? {wallet_id: category_wallet, order_type: order_type, status: statuses} : {order_type: order_type, status: statuses}
    if params[:sub_type] == "detail_withdrawal"
      report_csv = withdrawal_detail_report(first_date, second_date, params, condition, is_ach_international, category_wallet_check, is_other_present, cbd_dis_wallets)
    elsif params[:sub_type] == "summary_withdrawal"
      report_csv = withdrawal_summary_report(first_date, second_date, params, condition, is_ach_international, category_wallet_check, is_other_present, cbd_dis_wallets)
    end


    file = StringIO.new(report_csv)
    qc_file = QcFile.new(name: "#{report_name} report", start_date: first_date, end_date: second_date, status: "export")
    qc_file.image = file
    qc_file.image.instance_write(:content_type, 'text/csv')
    qc_file.image.instance_write(:file_name, "#{params[:sub_type]}.csv")
    qc_file.save
    report.file_id = {id: qc_file.id}
    report.status = "completed"
    report.message = "done"
    report.save
  end

  def withdrawal_summary_report(first_date, second_date, params,condition, is_ach_international=nil, category_wallet_check=nil, is_other_present=nil, cbd_dis_wallets=nil)
    first_date = first_date.to_datetime.utc
    second_date = second_date.to_datetime.utc
    if is_other_present
      if is_ach_international && category_wallet_check == true
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where("wallet_id IN (?) OR ach_international = ? OR NOT wallet_id IN (?)", condition[:wallet_id], true,cbd_dis_wallets).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil)
                      .order(batch_date: :desc)
      elsif is_ach_international && category_wallet_check == false
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where("ach_international = ? OR NOT wallet_id IN (?)", true, cbd_dis_wallets).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil)
                      .order(batch_date: :desc)
      elsif is_ach_international == false && category_wallet_check == true
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where("wallet_id IN (?) OR NOT wallet_id IN (?)", condition[:wallet_id], cbd_dis_wallets ).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil, ach_international: true)
                      .order(batch_date: :desc)
      else
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where.not(wallet_id: cbd_dis_wallets).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil, ach_international: true)
                      .order(batch_date: :desc)
      end
    else
      if is_ach_international && category_wallet_check == true
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where("wallet_id IN (?) OR ach_international = ?", condition[:wallet_id], true).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil)
                      .order(batch_date: :desc)
      elsif is_ach_international && category_wallet_check == false
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where(ach_international: true).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil)
                      .order(batch_date: :desc)

      elsif is_ach_international == false
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where(wallet_id: condition[:wallet_id]).where(order_type: "instant_ach", status: condition[:status])
                      .where.not(transaction_id:nil, ach_international: true)
                      .order(batch_date: :desc)
      else
        batches = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                      .where(condition)
                      .where.not(transaction_id:nil)
                      .order(batch_date: :desc)
      end
    end
    ach_batches = batches.group_by_day(time_zone: "Pacific Time (US & Canada)", day_start: 14) {|u| u.batch_date }
    ach_batches = ach_batches.sort.reverse.to_h
    total_records=[]
    if ach_batches.present?
      ach_batches.each do |batch_key, batch_value|
        # Batch date : batch_key
        grouped_batch = batch_value.group_by(&:order_type)
        checks_total_amount = 0
        ach_total_amount = 0
        instant_pay_total_amount = 0
        ach_cbd_total_amount = 0
        ach_dispensary_total_amount = 0
        ach_international_amount = 0
        ach_international_wallets = []
        grouped_batch.each do |nested_batch_key, nested_batch_value|
          if nested_batch_key == "check"
            check_ids = nested_batch_value.map{|c| c.id }
            # checks_funding_amount = nested_batch_value.map{|c| c.actual_amount if c.pending? }.compact.inject(0){|sum,x| sum + x }
            checks_total_amount = nested_batch_value.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
          elsif nested_batch_key == "instant_ach"
            international_achs = nested_batch_value.select{|c| c.ach_international == true} if nested_batch_value.present?
            ach_international_amount = international_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
            nested_batch_value = nested_batch_value.reject { |p| p.ach_international == true } if nested_batch_value.present?
            dispensary_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
            cbd_wallets = Wallet.joins(location: :category).where("categories.name ILIKE '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
            dispensary_achs = nested_batch_value.select{|c| dispensary_wallets.include?(c.wallet_id)}
            ach_dispensary_ids = dispensary_achs.map{|c| c.id }
            ach_dispensary_total_amount = dispensary_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }

            cbd_achs = nested_batch_value.select{|c| cbd_wallets.include?(c.wallet_id)}
            ach_cbd_ids = cbd_achs.map{|c| c.id }
            ach_cbd_funding_amount = cbd_achs.map{|c| c.actual_amount if c.pending? }.compact.inject(0){|sum,x| sum + x }
            ach_cbd_total_amount = cbd_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
            wallets = dispensary_wallets + cbd_wallets
            non_dispensary_achs = nested_batch_value.select{|c| !wallets.include?(c.wallet_id)}
            instant_ach_ids = non_dispensary_achs.map{|c| c.id }
            ach_total_amount = non_dispensary_achs.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
          elsif nested_batch_key == "instant_pay"
            ach_ids = nested_batch_value.map{|c| [c.id, c.status] }
            instant_pay_total_amount = nested_batch_value.map{|c| c.actual_amount }.compact.inject(0){|sum,x| sum + x }
          end
        end
        total_of_row=ach_cbd_total_amount.to_f + ach_total_amount.to_f + ach_dispensary_total_amount.to_f + instant_pay_total_amount.to_f + checks_total_amount.to_f + ach_international_amount.to_f

        total_records.push(['Date'=>batch_key,'cbd_total'=>ach_cbd_total_amount,'other_ach'=>ach_total_amount,'ach_international'=>ach_international_amount,'dispensary_total'=>ach_dispensary_total_amount,'instant_pay'=>instant_pay_total_amount,'checks_total'=>checks_total_amount,'row_total'=>total_of_row])
      end
    end

    total_records=total_records.flatten if total_records.present?
    cbd_total= total_records.pluck(:[],"cbd_total").flatten.compact.sum
    other_ach= total_records.pluck(:[],"other_ach").flatten.compact.sum
    ach_international= total_records.pluck(:[],"ach_international").flatten.compact.sum
    dispensary_total= total_records.pluck(:[],"dispensary_total").flatten.compact.sum
    instant_pay= total_records.pluck(:[],"instant_pay").flatten.compact.sum
    checks_total= total_records.pluck(:[],"checks_total").flatten.compact.sum
    row_total= total_records.pluck(:[],"row_total").flatten.compact.sum
    report_csv = CSV.generate do |csv|
      csv << ["Date", "CBD","Other","ACH International","Dispensary", "Push To Card", "Checks", "Total"]
      total_records.each do |report|
        csv << [
            report['Date'],
            number_with_precision(number_to_currency(report['cbd_total']), precision: 2),
            number_with_precision(number_to_currency(report['other_ach']), precision: 2),
            number_with_precision(number_to_currency(report['ach_international']), precision: 2),
            number_with_precision(number_to_currency(report['dispensary_total']), precision: 2),
            number_with_precision(number_to_currency(report['instant_pay']), precision: 2),
            number_with_precision(number_to_currency(report['checks_total']), precision: 2),
            number_with_precision(number_to_currency(report['row_total']), precision: 2),
        ]
      end
      csv << ["Total", number_with_precision(number_to_currency(cbd_total), precision: 2),number_with_precision(number_to_currency(other_ach), precision: 2),number_with_precision(number_to_currency(ach_international), precision: 2),number_with_precision(number_to_currency(dispensary_total), precision: 2), number_with_precision(number_to_currency(instant_pay), precision: 2),number_with_precision(number_to_currency(checks_total), precision: 2) , number_with_precision(number_to_currency(row_total), precision: 2)]
    end
    return report_csv
  end

  def withdrawal_detail_report(first_date, second_date, params, condition, is_ach_international=nil, category_wallet_check=nil, is_other_present=nil, cbd_dis_wallets=nil)
    first_date = first_date.to_datetime.utc
    second_date = second_date.to_datetime.utc
    if is_other_present
      if is_ach_international && category_wallet_check == true
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where("wallet_id IN (?) OR ach_international = ? OR NOT wallet_id IN (?)", condition[:wallet_id], true,cbd_dis_wallets).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil)
                                 .group_by(&:user_id)
      elsif is_ach_international && category_wallet_check == false
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where("ach_international = ? OR NOT wallet_id IN (?)", true, cbd_dis_wallets).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil)
                                 .group_by(&:user_id)
      elsif is_ach_international == false && category_wallet_check == true
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where("wallet_id IN (?) OR NOT wallet_id IN (?)", condition[:wallet_id], cbd_dis_wallets ).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil, ach_international: true)
                                 .group_by(&:user_id)
      else
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where.not(wallet_id: cbd_dis_wallets).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil, ach_international: true)
                                 .group_by(&:user_id)
      end
    else
      if is_ach_international && category_wallet_check == true
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where("wallet_id IN (?) OR ach_international = ?", condition[:wallet_id], true).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil)
                                 .group_by(&:user_id)
      elsif is_ach_international && category_wallet_check == false
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where(ach_international: true).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil)
                                 .group_by(&:user_id)
      elsif is_ach_international == false
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where(wallet_id: condition[:wallet_id]).where(order_type: "instant_ach", status: condition[:status])
                                 .where.not(transaction_id:nil, ach_international: true)
                                 .group_by(&:user_id)
      else
        batches_with_users = TangoOrder.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
                                 .where(condition)
                                 .where.not(transaction_id:nil)
                                 .group_by(&:user_id)
      end
    end

    actual_result = []
    dispensary_wallets_ca = Wallet.joins(location: :category).where("categories.name ILIKE '%Dispensary%'").pluck(:id)
    cbd_wallets_ca = Wallet.joins(location: :category).where("categories.name ILIKE '%CBD%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
    if batches_with_users.present?
      batches_with_users.each do |key, value|
        data = {}
        merchant = User.find_by(id: key)
        ref_no = set_ref_no(merchant)
        data[:merchant] = "#{merchant&.parent_merchant&.name || merchant&.name}"
        data[:user_id] = "#{ref_no}"
        # batches_with_location = value.group_by(&:location_id)
        batches_with_location = value.group_by(&:wallet_id)

        batches_with_location.each do |location_id, location_tango_orders|
          total_amount = 0
          fee = 0
          amount_sent = 0
          gbox_fee = 0
          iso_fee = 0
          agent_fee = 0
          affiliate_fee = 0
          location_wallet = Wallet.find_by(id: location_id)
          location = location_wallet.try(:location)
          data[:wallet_id] = location_wallet.try(:id)
          flag_text = location_wallet.try(:location).try(:ach_flag_text) if location_wallet.try(:location).try(:ach_flag)
          data[:location_name] = location.present? ? location.try(:business_name) : location_wallet.try(:name)
          data[:flag_text] =  flag_text.present? ? flag_text : ""
          data[:location_category] = location.category[:name] if location.present? && location.category.present?
          location_tango_orders.each do|tango|
            if tango.order_type == "check"
              data[:withdrawal_category] = "Check"
            elsif tango.order_type == "instant_pay"
              data[:withdrawal_category] = "Push To Card"
            elsif tango.order_type == "instant_ach"
              if tango.ach_international == true
                data[:withdrawal_category] = "ACH International"
              elsif dispensary_wallets_ca.include?(tango.wallet_id)
                data[:withdrawal_category] = "ACH Dispensary"
              elsif cbd_wallets_ca.include?(tango.wallet_id)
                data[:withdrawal_category] = "ACH CBD"
              else
                data[:withdrawal_category] = "ACH Other"
              end
            end
            data[:tango_id] = tango.id
            data[:type] = tango.instant_pay? ? "Push To Card" : tango.instant_ach? ?  "ACH" : tango.order_type.try(:capitalize)
            data[:bank] = AchGateway.find(tango.ach_gateway_id).try(:bank_name) if tango.ach_gateway_id.present?
            data[:date] = tango.created_at.strftime("%m/%d/%Y")
            data[:completed_date] = tango.batch_date.strftime("%m/%d/%Y")
            data[:status] = tango.status
            data[:note] = tango.description
            transaction = tango.transaction
            if transaction.present?
              data[:total_amount] = transaction.present? ? transaction.total_amount : 0
              data[:fee] = transaction.present? ? transaction.net_fee : 0
              data[:amount_sent] = transaction.present? ? transaction.net_amount : 0
              data[:gbox_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"gbox").to_f : 0
              data[:iso_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"iso").to_f : 0
              data[:agent_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"agent").to_f : 0
              data[:affiliate_fee] = transaction.present? ? getting_fee_from_local_trans(transaction,"affiliate").to_f : 0
              total_amount = data[:total_amount] + total_amount
              fee = (data[:fee].present? ? data[:fee] : 0) + fee
              amount_sent = data[:amount_sent] + amount_sent
              gbox_fee = data[:gbox_fee] + gbox_fee
              iso_fee = data[:iso_fee] + iso_fee
              agent_fee = data[:agent_fee] + agent_fee
              affiliate_fee = data[:affiliate_fee] + affiliate_fee
              actual_result.push(data)
            end
            data = {}
          end
          data[:total] = "Total"
          data[:total_amount] = total_amount
          data[:fee] = fee
          data[:amount_sent] = amount_sent
          data[:gbox_fee] = gbox_fee
          data[:iso_fee] = iso_fee
          data[:agent_fee] = agent_fee
          data[:affiliate_fee] = affiliate_fee
          actual_result.push(data)
          data = {}
        end
      end

      report_csv = []
      if actual_result.present?
        report_csv = CSV.generate do |csv|
          csv << ["Merchant", "User ID", "DBA Name", "Category", "Wallet ID","Withdrawal ID","Type", "Withdrawal Category", "Bank", "Created Date", "Completed Date", "Status", "Total Amount", "Fee", "Amount Sent", "GreenBox Fee", "ISO Fee", "Agent Fee", "Affiliate Fee", "Notes", "DBA Flag Text"]
          actual_result.each do |report|
            if report.try(:[],:merchant).present?
              csv << [
                  report[:merchant],
                  report[:user_id],
                  report[:location_name],
                  report[:location_category],
                  report[:wallet_id],
                  report[:tango_id],
                  report[:type],
                  report[:withdrawal_category],
                  report[:bank],
                  report[:date],
                  report[:completed_date],
                  report[:status],
                  number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  report[:note],
                  report[:flag_text]
              ]
            elsif report.try(:[],:merchant).nil? && report.try(:[],:location_name).present?
              csv << [
                  "",
                  "",
                  report[:location_name],
                  report[:location_category],
                  report[:wallet_id],
                  report[:tango_id],
                  report[:type],
                  report[:withdrawal_category],
                  report[:bank],
                  report[:date],
                  report[:completed_date],
                  report[:status],
                  number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  report[:note],
                  report[:flag_text]
              ]
            elsif report.try(:[],:merchant).nil? && report.try(:[],:location_name).nil? && report.try(:[],:total).nil?
              csv << [
                  "",
                  "",
                  "",
                  "",
                  report[:wallet_id],
                  report[:tango_id],
                  report[:type],
                  report[:withdrawal_category],
                  report[:bank],
                  report[:date],
                  report[:completed_date],
                  report[:status],
                  number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  report[:note],
                  ""
              ]
            elsif report.try(:[],:total).present?
              csv << [
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  report[:total],
                  number_with_precision(number_to_currency(report[:total_amount]), precision: 2),
                  number_with_precision(number_to_currency(report[:fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:amount_sent]), precision: 2),
                  number_with_precision(number_to_currency(report[:gbox_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:iso_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:agent_fee]), precision: 2),
                  number_with_precision(number_to_currency(report[:affiliate_fee]), precision: 2),
                  "",
                  ""
              ]
              # csv << ["", "", "", "", "", "", "", "", "", "", "", "", "", ""]
            end
          end
        end
      end
      return report_csv
    end
  end


  def terminal_id_report(first_date,second_date,params)
    transactions = []
    if params[:select_all].present? && params[:select_all] == "true"
      transactions = Transaction.where(main_type: ["Credit Card", "PIN Debit"]).where.not(terminal_id: nil).group_by{|t| t.receiver_wallet_id}
    else
      wallet_ids = []
      if params[:dba_name].present?
        params[:dba_name].each do |loca_info|
          if loca_info.present?
            p = JSON.parse(loca_info)
            if p['id'].present?
              wallet_ids.push(p['id'])
            end
          end
        end
        if params[:date].present?
          transactions = Transaction.where(receiver_wallet_id: wallet_ids, main_type: ["Credit Card", "PIN Debit"], created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where.not(terminal_id: nil).group_by{|t| t.receiver_wallet_id}
        else
          transactions = Transaction.where(receiver_wallet_id: wallet_ids, main_type: ["Credit Card", "PIN Debit"]).where.not(terminal_id: nil).group_by{|t| t.receiver_wallet_id}
        end
      else
        transactions = Transaction.where(main_type: ["Credit Card", "PIN Debit"], created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where.not(terminal_id: nil).group_by{|t| t.receiver_wallet_id}
      end
    end
    details = []
    if transactions.present?
      transactions.each do |k,v|
        dba_name = v.try(:pluck, :tags).try(:compact).try(:pluck, :[], "location").try(:flatten).try(:compact).try(:uniq).try(:first).try(:[], "business_name")
        details.push({
                         date: params[:date],
                         merchant_name: v.first.tags.try(:[],"merchant").try(:[],"name").present? ? v.first.tags.try(:[],"merchant").try(:[],"name") : v.first.receiver.try(:name),
                         dba_name: dba_name ? dba_name : v.first.receiver_wallet.try(:location).try(:business_name),
                         wallet_id: k,
                         terminal_id: v.pluck(:terminal_id).uniq,
                         status: "Active"
                     })
      end

      report_csv = CSV.generate do |csv|
        csv << ["Date", "Merchant Name","DBA Name","Wallet ID", "Terminal", "status"]
        details.each do |report|
          csv << [report[:date], report[:merchant_name], report[:dba_name],report[:wallet_id],report[:terminal_id].try(:uniq),report[:status]]
        end
      end
      file = StringIO.new(report_csv)
      qc_file = QcFile.new(name: "Terminal Id report", start_date: Time.now, end_date: Time.now, status: :wallet)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "terminal_id.csv")
      qc_file.save
      ActionCable.server.broadcast "report_channel_#{@host}", channel_name: "report_channel_#{@host}", action: 'adminreport' , message: qc_file.id
    else
      ActionCable.server.broadcast "report_channel_#{@host}", channel_name: "report_channel_#{@host}", action: 'no_reports' , message: "No reports found!"
    end
  end

  def set_ref_no(user)
    if user.user?
      return "C-#{user.id}"
    elsif user.agent?
      return "A-#{user.id}"
    elsif user.affiliate?
      return "AF-#{user.id}"
    elsif user.iso?
      return "ISO-#{user.id}"
    elsif user.merchant? && user.merchant_id.present?
      return "M-#{user.merchant_id}"
    elsif user.merchant? && user.merchant_id.nil?
      return "M-#{user.id}"
    end
  end
  def block_vip_export(card_type=nil, user=nil)
    begin
      report = Report.new(user_id: user, first_date: nil, second_date:nil, offset: nil, report_type: "#{card_type} report", status: "pending",transaction_type: nil,read: false)
      report.params = card_type
      report.save
      if card_type == "vip"
        cards = Card.where(is_vip: true)
      else
        cards = Card.where(is_blocked: true)
      end
      report_csv = CSV.generate do |csv|
        csv << ["Cardholder Name", "First 6","Last 4", "Expiry Date"]
        cards.find_each do |card|
          csv << [card.try(:name), card.try(:first6), card.try(:last4), card.try(:exp_date)]
        end
      end
      file = StringIO.new(report_csv)
      qc_file = QcFile.new(name: "#{card_type} report", start_date: nil, end_date: nil, status: "export")
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "#{card_type}.csv")
      qc_file.save
      report.file_id = {id: qc_file.id}
      report.status = "completed"
      report.message = "done"
      report.save
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'export_completed' , message: "done"
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'export_completed' , message: "crash"
    ensure
      report.save
    end
  end
end