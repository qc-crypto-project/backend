class AuditExportWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  include Admins::ReportsHelper
  include AdminsHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(first_date = nil, second_date = nil ,folder_name = nil, role = nil)
    Dir.mkdir("#{Rails.root}/tmp/#{folder_name}")
    count = 0
    user_wallets = User.user.joins(:wallets).where("lower(state) = ?", "ca").pluck("wallets.id as wallet_id").uniq
    merchant_wallets = Location.joins(:wallets).where("lower(state) = ?", "ca").where("wallets.wallet_type = ?", 0).pluck('wallets.id').uniq
    wallets = user_wallets + merchant_wallets
    BlockTransaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where("sender_wallet_id IN (?) OR receiver_wallet_id IN (?)", wallets, wallets).where(main_type: ['Credit Card','Debit Card','PIN Debit','Sale Issue','eCommerce','Virtual Terminal']).find_in_batches(batch_size: 50000) do |block|
      if block.present?
        file = File.new("#{Rails.root}/tmp/#{folder_name}/sales-#{count}.csv", "w+")
        file << CSV.generate do |csv|
          csv << ["Block ID", "Date/Time", "Main Type","Sender Name", "Receiver Name","Sender wallet", "Receiver Wallet", "Tx Amount", "Total Amount", "Fee", "Privacy Fee", "tip", "Reserve Money" ,"Net Fee" , "Net Amount"]
          block.each do |tx|
            csv <<
                [
                    tx.sequence_id,
                    tx.timestamp.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                    tx.main_type,
                    tx.sender_name,
                    tx.receiver_name,
                    tx.sender_wallet_id,
                    tx.receiver_wallet_id,
                    number_to_currency(number_with_precision(SequenceLib.dollars(tx.tx_amount), precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(SequenceLib.dollars(tx.amount_in_cents), precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(SequenceLib.dollars(tx.fee_in_cents), precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(tx.privacy_fee, precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(SequenceLib.dollars(tx.tip_cents), precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(SequenceLib.dollars(tx.hold_money_cents), precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(tx.net_fee, precision: 2, delimeter: ',')),
                    number_to_currency(number_with_precision(tx.total_net, precision: 2, delimeter: ',')),
                ]
          end
        end
        file.close
        SlackService.upload_files("sales-#{count}", file, "#qc-files", "sales-#{count}.csv")
        count = count + 1
      end
    end
    # if role == "card"
    #   count = 0
    #   Audited::Audit.where(auditable_type: "Card", action: "update").where("created_at BETWEEN :first AND :second", first: first_date, second: second_date).find_in_batches(batch_size: 20000) do |batches|
    #     if batches.present?
    #       count = count + 1
    #       file = File.new("#{Rails.root}/tmp/#{folder_name}/card-#{count}.csv", "w+")
    #       file << CSV.generate do |csv|
    #         csv << ["Card Id", "Old Name", "Changed Name", "MID", "Date"]
    #         batches.each do |batch|
    #           if batch.audited_changes.include?("name")
    #             name = batch.audited_changes["name"]
    #             csv << [batch.auditable_id, name.first, name.second, batch.user_id, batch.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p")]
    #           end
    #         end
    #       end
    #     end
    #       file.close
    #       SlackService.upload_files("card-#{count}", file, "#qc-files", "card-#{count}.csv")
    #     end
    # elsif role == "user"
    #   count = 0
    #   ActivityLog.where(action: ["virtual_transaction", "virtual_terminal_transaction_debit","virtual_transaction_card","submit_virtual_terminal","virtual_terminal_transaction"], activity_type: ["api_success","vt_success"]).where("created_at BETWEEN :first AND :second", first: first_date, second: second_date).find_in_batches(batch_size: 20000) do |batches|
    #     if batches.present?
    #       count = count + 1
    #       file = File.new("#{Rails.root}/tmp/#{folder_name}/user-#{count}.csv", "w+")
    #       file << CSV.generate do |csv|
    #         csv << ["User Id", "Name", "MID", "Date"]
    #         batches.each do |batch|
    #           params = batch.params
    #           name = params["card_holder_name"]
    #           name = params["name"] if name.blank?
    #           order_bank = params["order_bank"]
    #           csv << [order_bank.try(:[],"user_id"), name, batch.user_id, batch.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p")]
    #         end
    #       end
    #       file.close
    #       SlackService.upload_files("user-#{count}", file, "#qc-files", "user-#{count}.csv")
    #     end
    #   end
    # elsif role == "decline"
    #   ActivityLog.includes(:user).where("created_at >= :first", first: first_date).where(activity_type: [:api_error,:vt_error]).find_in_batches(batch_size: 5000) do |activities|
    #     if activities.present?
    #       file = File.new("#{Rails.root}/tmp/#{folder_name}/declines-#{count}.csv", "w+")
    #       file << CSV.generate do |csv|
    #         csv << ["Activity id","Action", "MID", "Merchant name","Location key", "Wallet Id", "Ref Id", "Amount", "Error"]
    #         activities.each do |activity|
    #           if activity.respond_with.is_a?(Hash)
    #             parse = activity.respond_with
    #           else
    #             parse = JSON.parse(activity.respond_with)
    #           end
    #           csv << [
    #               activity.id,
    #               activity.action,
    #               activity.user.try(:id),
    #               activity.user.try(:name),
    #               activity.params.try(:[], "location_id"),
    #               activity.params.try(:[], "wallet_id"),
    #               activity.params.try(:[], "ref_id"),
    #               activity.params.try(:[], "amount"),
    #               parse.try(:[],"message")
    #           ]
    #         end
    #       end
    #       file.close
    #       SlackService.upload_files("declines-#{count}", file, "#qc-files", "declines-#{count}.csv")
    #       count = count + 1
    #     end
    #   end
    # end

    # category = Category.where(name: "Forex").last
    # locations = Location.joins(:wallets).where(category_id: category.id).where("wallets.wallet_type = ?",0).select("locations.id as id, locations.business_name as business_name, locations.merchant_id as merchant_id, wallets.id as wallet_id").group_by{|e| e.wallet_id}
    # transactions = Transaction.where(receiver_wallet_id: locations.keys,main_type: [TypesEnumLib::TransactionViewTypes::DebitCard,
    #                                                                                TypesEnumLib::TransactionViewTypes::PinDebit,
    #                                                                                TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard], status:  ["approved","refunded","complete"]).group_by{|e| e.receiver_wallet_id}
    # file = File.new("#{Rails.root}/tmp/#{folder_name}/forex-#{count}.csv", "w+")
    # file << CSV.generate do |csv|
    #   csv << ["MID", "DBA Name", "Debit","Credit"]
    #   transactions.each do |key,transaction|
    #     tx = transaction.group_by{|e| e.try(:card).try(:card_type).try(:downcase)}
    #     location = locations[key].try(:first)
    #     merchant_id = location.try(:merchant_id)
    #     debit = tx["debit"].pluck(:total_amount).sum if tx["debit"].present?
    #     credit = tx["credit"].pluck(:total_amount).sum if tx["credit"].present?
    #       csv << ["M-#{merchant_id}", location.try(:business_name),number_with_precision(number_to_currency(debit.to_f),precision: 2),number_with_precision(number_to_currency(credit.to_f),precision: 2)]
    #   end
    # end
    # file.close
    # SlackService.upload_files("forex-#{count}", file, "#qc-files", "forex-#{count}.csv")
    # TangoOrder.includes(:user,:wallet).includes(wallet: :location).where.not(order_type: nil).find_in_batches(batch_size: 5000) do |batches|
    #   if batches.present?
    #     file = File.new("#{Rails.root}/tmp/#{folder_name}/withdraws-#{count}.csv", "w+")
    #     file << CSV.generate do |csv|
    #       csv << ['Date', 'Withdraw Type','Id', 'Check #', 'Merchant Name', 'User ID', 'Location Name', 'Total Amount','Fee', 'Funding Amount', 'Status']
    #       batches.each do |batch|
    #         if batch.ach? || batch.instant_ach?
    #           type = "ACH"
    #         elsif batch.check?
    #           type = "Send Check"
    #         elsif batch.invoice?
    #           type = "Invoice"
    #         elsif batch.instant_pay?
    #           type = "P2C"
    #         elsif batch.bulk_check?
    #           type = "Bulk Check"
    #         end
    #         user = batch.user
    #         if user.present?
    #           if user.iso?
    #             ref_id = "ISO-#{user.id}"
    #           elsif user.agent?
    #             ref_id  = "A-#{user.id}"
    #           elsif user.affiliate?
    #             ref_id = "AF-#{user.id}"
    #           elsif user.partner?
    #             ref_id = "CO-#{user.id}"
    #           elsif user.merchant?
    #             ref_id = "M-#{user.id}"
    #           end
    #         end
    #         if batch.actual_amount.present?
    #           actual_amount = batch.actual_amount.to_f
    #         else
    #           actual_amount = batch.amount.to_f
    #         end
    #         csv << [
    #             batch.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
    #             type,
    #             batch.id,
    #             batch.number,
    #             user.try(:name),
    #             ref_id,
    #             batch.wallet.try(:location).try(:business_name),
    #             number_to_currency(number_with_precision(batch.amount, precision: 2, delimiter: ',')),
    #             number_to_currency(number_with_precision(batch.amount.to_f - batch.actual_amount.to_f,precision:2, delimiter: ',')),
    #             number_to_currency(number_with_precision(actual_amount.to_f,precision:2, delimiter: ',')),
    #             batch.status
    #         ]
    #       end
    #     end
    #     file.close
    #     SlackService.upload_files("withdraws-#{count}", file, "#qc-files", "withdraws-#{count}.csv")
    #     count = count + 1
    #   end
    # end

    #dont remove these comments
    # tx = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).order(timestamp: :asc)
    # puts "first: ", tx.first.timestamp
    # puts "last: ", tx.last.timestamp
    # puts "count: ", tx.count
    # puts "total amount: ",tx.pluck(:amount_in_cents).sum
    #s3 = Aws::S3::Resource.new(
    #    credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],ENV['AWS_SECRET_ACCESS_KEY']),
    #    region: ENV['AWS_REGION']
    #)
    #count = 0
    #limit = 50000
    #offset = 0
    #qc_wallet = Wallet.qc_support.first.try(:id)
    #Dir.mkdir("#{Rails.root}/tmp/#{folder_name}")
    #while offset >= 0
    #  block = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where.not(main_type: ["Fee Transfer","Fee transfer","Fee"]).order(timestamp: :asc).offset(offset).limit(limit)
    #  if block.present?
    #    offset = offset + limit
    #    # File.delete("#{Rails.root}/tmp/#{folder_name}/transactions-#{count}.csv") if File.exist?("#{Rails.root}/tmp/#{folder_name}/transactions-#{count}.csv")
    #    file = File.new("#{Rails.root}/tmp/#{folder_name}/transactions-#{count}.csv", "w+")
    #    file << CSV.generate do |csv|
    #      csv << ["Block Id", "Date/Time","Main Type", "Sub Type","Action","Gateway","Descriptor", "MID #","Merchant Name", "Sender Name", "Receiver Name", "Sender Wallet", "Receiver Wallet","DBA Name", "Total Amount", "Tx Amount","Privacy Fee","Fee","Tip","Net Fee", "Net Amount","Reserve Money",
    #              "Bank Fee","Gbox Fee", "Gbox Net Fee","Iso Name","Iso Fee", "Agent Name","Agent Fee", "Affiliate Name", "Affiliate Fee", "Baked Profit Split (Gbox)","Baked Profit Split (ISO)",
    #              "Baked 1A Name","Baked 1A Amount","Baked 1B Name","Baked 1B Amount","Baked 1C Name","Baked 1C Amount",
    #              "Baked 2A Name","Baked 2A Amount","Baked 2B Name","Baked 2B Amount","Baked 2C Name","Baked 2C Amount",
    #              "Baked 3A Name","Baked 3A Amount","Baked 3B Name","Baked 3B Amount","Baked 3C Name","Baked 3C Amount",
    #              "Baked 4A Name","Baked 4A Amount","Baked 4B Name","Baked 4B Amount","Baked 4C Name","Baked 4C Amount",
    #              "Baked 5A Name","Baked 5A Amount","Baked 5B Name","Baked 5B Amount","Baked 5C Name","Baked 5C Amount",
    #              "Net Profit Split(Gbox)","Profit Split(with Gbox)","Terminal ID", "Ref #", "Clerk ID", "C.C Type", "C.C Brand ", "Card Holder Name", "First 6", "Last 4","Exp Date",
    #              "Bank Charge ID"]
    #      block.each do |transaction|
    #        payment_gateway = transaction.payment_gateway
    #        if payment_gateway.blank?
    #          payment_gateway = PaymentGateway.where("lower(name) LIKE ? OR lower(key) LIKE ?", transaction.gateway.downcase, transaction.gateway.downcase).first
    #          if payment_gateway.present?
    #            gateway_key = payment_gateway.key
    #            gateway_name = payment_gateway.name
    #          end
    #        end
    #        bank_fee = 0
    #        if transaction.main_type == TypesEnumLib::TransactionViewTypes::Ecommerce || transaction.main_type == TypesEnumLib::TransactionViewTypes::VirtualTerminal || transaction.main_type == TypesEnumLib::TransactionViewTypes::CreditCard || transaction.main_type == TypesEnumLib::TransactionViewTypes::PinDebit
    #          percent = transaction.per_transaction_fee.to_f
    #          perc = percent.to_f/100 * SequenceLib.dollars(transaction.amount_in_cents)
    #          bank_fee = transaction.transaction_fee.to_f + perc.to_f
    #          if bank_fee <= 0 && payment_gateway.present?
    #            percent = payment_gateway.per_transaction_fee
    #            perc = percent.to_f/100 * SequenceLib.dollars(transaction.amount_in_cents)
    #            bank_fee = payment_gateway.transaction_fee.to_f + perc.to_f
    #          end
    #        end
    #        company_split = super_company_split(transaction.tags.try(:[],"fee_perc"))
    #        user1=baked_user_name(transaction.try(:tags).try(:[],'fee_perc').try(:[],'super_users_details').try(:first), transaction.try(:tags).try(:[], "fee_perc").try(:[], "super_iso"), transaction.try(:tags).try(:[], "fee_perc").try(:[], "super_agent"), transaction.try(:tags).try(:[], "fee_perc").try(:[], "super_affiliate"))
    #        user2=baked_user_name(transaction.try(:tags).try(:[],'fee_perc').try(:[],'super_users_details').try(:second))
    #        user3=baked_user_name(transaction.try(:tags).try(:[],'fee_perc').try(:[],'super_users_details').try(:third))
    #        user4=baked_user_name(transaction.try(:tags).try(:[],'fee_perc').try(:[],'super_users_details').try(:fourth))
    #        user5=baked_user_name(transaction.try(:tags).try(:[],'fee_perc').try(:[],'super_users_details').try(:fifth))
    #        sender = transaction.sender
    #        receiver = transaction.receiver
    #        if sender.present?
    #          sender_wallet = transaction.sender_wallet
    #          if sender_wallet.present? && sender.merchant?
    #            if sender_wallet.primary?
    #              sender_name = transaction.location_dba_name || transaction.tags.try(:[],"location").try(:[],"business_name") || sender_wallet.try(:location).try(:business_name) || sender.name
    #            elsif sender_wallet.tip?
    #              sender_name = transaction.location_dba_name.present? ? "#{transaction.location_dba_name} Tip" : transaction.tags.try(:[], "location").try(:[],"business_name").present? ? "#{transaction.tags.try(:[],"location").try(:[],"business_name")} Tip" : sender_wallet.try(:location).try(:business_name).present? ? "#{sender_wallet.try(:location).try(:business_name)} Tip" : "#{transaction.sender_name} Tip"
    #            elsif sender_wallet.reserve?
    #              sender_name = transaction.location_dba_name.present? ? "#{transaction.location_dba_name} Reserve" : transaction.tags.try(:[], "location").try(:[],"business_name").present? ? "#{transaction.tags.try(:[],"location").try(:[],"business_name")} Reserve" : sender_wallet.try(:location).try(:business_name).present? ? "#{sender_wallet.try(:location).try(:business_name)} Reserve" : "#{transaction.sender_name} Reserve"
    #            end
    #          else
    #            sender_name = transaction.sender_name || sender.name
    #          end
    #        end
    #        if receiver.present?
    #          receiver_wallet = transaction.receiver_wallet
    #          if receiver_wallet.present? && receiver.merchant?
    #            if receiver_wallet.primary?
    #              receiver_name = transaction.location_dba_name || transaction.tags.try(:[],"location").try(:[],"business_name") || receiver_wallet.try(:location).try(:business_name) || receiver.name
    #            elsif receiver_wallet.tip?
    #              receiver_name = transaction.location_dba_name.present? ? "#{transaction.location_dba_name} Tip" : transaction.tags.try(:[], "location").try(:[],"business_name").present? ? "#{transaction.tags.try(:[],"location").try(:[],"business_name")} Tip" : receiver_wallet.try(:location).try(:business_name).present? ? "#{receiver_wallet.try(:location).try(:business_name)} Tip" : "#{transaction.receiver_name} Tip"
    #            elsif receiver_wallet.reserve?
    #              receiver_name = transaction.location_dba_name.present? ? "#{transaction.location_dba_name} Reserve" : transaction.tags.try(:[], "location").try(:[],"business_name").present? ? "#{transaction.tags.try(:[],"location").try(:[],"business_name")} Reserve" : receiver_wallet.try(:location).try(:business_name).present? ? "#{receiver_wallet.try(:location).try(:business_name)} Reserve" : "#{transaction.receiver_name} Reserve"
    #            end
    #          else
    #            receiver_name = transaction.receiver_name || receiver.name
    #          end
    #        end
    #        card = transaction.card
    #        #per_transaction_fee = transaction.per_transaction_fee
    #        #per_transaction_fee = payment_gateway.try(:per_transaction_fee).to_f if per_transaction_fee.to_f == 0
    #        csv << [
    #            transaction.sequence_id,
    #            transaction.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
    #            transaction.main_type,
    #            transaction.sub_type,
    #            transaction.action,
    #            payment_gateway.try(:key) || gateway_key,
    #            payment_gateway.try(:name) || gateway_name,
    #            receiver.present? && receiver.merchant? ? "M-#{transaction.receiver_id}" : sender.present? && sender.merchant? ? "M-#{transaction.sender_id}" : "",
    #            receiver.present? && receiver.merchant? ?  transaction.receiver_name || receiver.try(:name) : sender.present? && sender.merchant? ? transaction.sender_name || sender.try(:name) :  "",
    #            sender_name || transaction.sender_name,
    #            receiver_name || transaction.receiver_name,
    #            transaction.sender_wallet_id != 0 ? transaction.sender_wallet_id : "",
    #            transaction.receiver_wallet_id,
    #            transaction.location_dba_name || transaction.tags.try(:[],"location").try(:[],"busines_name") || transaction.receiver_wallet.try(:location).try(:business_name),
    #            number_with_precision(number_to_currency(SequenceLib.dollars(transaction.amount_in_cents)), precision: 2),
    #            number_with_precision(number_to_currency(SequenceLib.dollars(transaction.tx_amount)), precision: 2),
    #            number_with_precision(number_to_currency(transaction.privacy_fee), precision: 2),
    #            number_to_currency(number_with_precision(show_view_fee(transaction.tags.try(:[],"fee_perc"),SequenceLib.dollars(transaction.try(:fee_in_cents))), precision: 2)),
    #            number_with_precision(number_to_currency(SequenceLib.dollars(transaction.tip_cents)), precision: 2),
    #            number_to_currency(number_with_precision(show_view_net_fee(transaction.tags.try(:[],"fee_perc"), transaction.try(:privacy_fee), SequenceLib.dollars(transaction.try(:fee_in_cents))), precision: 2)),
    #            number_to_currency(number_with_precision(show_report_total_net(transaction.tags.try(:[],"fee_perc"),  SequenceLib.dollars(transaction.try(:amount_in_cents)),SequenceLib.dollars(transaction.try(:tip_cents)), SequenceLib.dollars(transaction.try(:hold_money_cents)),SequenceLib.dollars(transaction.try(:fee_in_cents))) , precision: 2)),
    #            number_with_precision(number_to_currency(SequenceLib.dollars(transaction.hold_money_cents)), precision: 2),
    #            number_with_precision(number_to_currency(bank_fee.to_f), precision: 2),
    #            number_with_precision(number_to_currency(getting_fee_from_local_trans(transaction,"gbox")), precision: 2),
    #            number_with_precision(number_to_currency(getting_fee_from_local_trans(transaction,"gbox").to_f - bank_fee.to_f), precision: 2),
    #            transaction.tags.try(:[],"fee_perc").try(:[],"iso").try(:[], "name"),
    #            transaction.tags.try(:[],"fee_perc").try(:[], "iso").try(:[], "iso_buyrate").present? ? "-"+ number_with_precision(number_to_currency(getting_fee_from_local_trans(transaction,"iso")), precision: 2)  : number_with_precision(number_to_currency(getting_fee_from_local_trans(transaction,"iso")), precision: 2),
    #            transaction.tags.try(:[],"fee_perc").try(:[],"agent").try(:[], "name"),
    #            number_with_precision(number_to_currency(getting_fee_from_local_trans(transaction,"agent")), precision: 2),
    #            transaction.tags.try(:[],"fee_perc").try(:[],"affiliate").try(:[], "name"),
    #            number_with_precision(number_to_currency(getting_fee_from_local_trans(transaction,"affiliate")), precision: 2),
    #            number_with_precision(number_to_currency(baked_gbox_fee(transaction.tags.try(:[], "fee_perc").try(:[], "super_users_details"), qc_wallet, transaction.tags.try(:[], "fee_perc").try(:[], "super_iso"))), precision: 2),
    #            number_with_precision(number_to_currency(baked_iso_fee(transaction.tags.try(:[], "fee_perc").try(:[], "super_users_details"), transaction.tags.try(:[], "fee_perc").try(:[], "iso").try(:[],"wallet"), transaction.tags.try(:[], "fee_perc").try(:[], "super_iso"))), precision: 2),
    #            user1.present? ? user1.try(:[],'user1_name') : '',
    #            user1.present? ? number_with_precision(number_to_currency(user1.try(:[],'user1_amount')), precision: 2) : '$0.00',
    #            user1.present? ? user1.try(:[],'user2_name') : '',
    #            user1.present? ? number_with_precision(number_to_currency(user1.try(:[],'user2_amount')), precision: 2) : '$0.00',
    #            user1.present? ? user1.try(:[],'user3_name') : '',
    #            user1.present? ? number_with_precision(number_to_currency(user1.try(:[],'user3_amount')), precision: 2) : '$0.00',
    #            user2.present? ? user2.try(:[],'user1_name') : '',
    #            user2.present? ? number_with_precision(number_to_currency(user2.try(:[],'user1_amount')), precision: 2) : '$0.00',
    #            user2.present? ? user2.try(:[],'user2_name') : '',
    #            user2.present? ? number_with_precision(number_to_currency(user2.try(:[],'user2_amount')), precision: 2) : '$0.00',
    #            user2.present? ? user2.try(:[],'user3_name') : '',
    #            user2.present? ? number_with_precision(number_to_currency(user2.try(:[],'user3_amount')), precision: 2) : '$0.00',
    #            user3.present? ? user3.try(:[],'user1_name') : '',
    #            user3.present? ? number_with_precision(number_to_currency(user3.try(:[],'user1_amount')), precision: 2) : '$0.00',
    #            user3.present? ? user3.try(:[],'user2_name') : '',
    #            user3.present? ? number_with_precision(number_to_currency(user3.try(:[],'user2_amount')), precision: 2) : '$0.00',
    #            user3.present? ? user3.try(:[],'user3_name') : '',
    #            user3.present? ? number_with_precision(number_to_currency(user3.try(:[],'user3_amount')), precision: 2) : '$0.00',
    #            user4.present? ? user4.try(:[],'user1_name') : '',
    #            user4.present? ? number_with_precision(number_to_currency(user4.try(:[],'user1_amount')), precision: 2) : '$0.00',
    #            user4.present? ? user4.try(:[],'user2_name') : '',
    #            user4.present? ? number_with_precision(number_to_currency(user4.try(:[],'user2_amount')), precision: 2) : '$0.00',
    #            user4.present? ? user4.try(:[],'user3_name') : '',
    #            user4.present? ? number_with_precision(number_to_currency(user4.try(:[],'user3_amount')), precision: 2) : '$0.00',
    #            user5.present? ? user5.try(:[],'user1_name') : '',
    #            user5.present? ? number_with_precision(number_to_currency(user5.try(:[],'user1_amount')), precision: 2) : '$0.00',
    #            user5.present? ? user5.try(:[],'user2_name') : '',
    #            user5.present? ? number_with_precision(number_to_currency(user5.try(:[],'user2_amount')), precision: 2) : '$0.00',
    #            user5.present? ? user5.try(:[],'user3_name') : '',
    #            user5.present? ? number_with_precision(number_to_currency(user5.try(:[],'user3_amount')), precision: 2) : '$0.00',
    #            transaction.tags.try(:[], "fee_perc").try(:[], "net_super_company").try(:[], "amount").present? && transaction.tags.try(:[], "fee_perc").try(:[], "net_super_company").try(:[], "amount").to_f > 0 ? number_with_precision(number_to_currency(transaction.tags.try(:[], "fee_perc").try(:[], "net_super_company").try(:[], "amount")), precision: 2) : "$0.00",
    #            #number_with_precision(number_to_currency(net_profit_gbox(transaction.tags.try(:[], "fee_perc").try(:[], "net_super_company"), per_transaction_fee)), precision: 2),
    #            number_with_precision(number_to_currency(company_split.to_f), precision: 2),
    #            transaction.terminal_id || transaction.tags.try(:[], "sales_info").try(:[], "terminal_id"),
    #            transaction.ref_id || transaction.tags.try(:[], "sales_info").try(:[], "ref_id"),
    #            transaction.clerk_id || transaction.tags.try(:[], "sales_info").try(:[], "clerk_id"),
    #            card.try(:card_type).try(:capitalize) || transaction.tags.try(:[], "card").try(:[], "card_type"),
    #            card.try(:brand).try(:capitalize) || transaction.tags.try(:[], "card").try(:[], "brand"),
    #            card.try(:name).try(:capitalize) || transaction.tags.try(:[], "card").try(:[], "name"),
    #            transaction.first6 || card.try(:first6) || transaction.tags.try(:[], "card").try(:[], "first6"),
    #            transaction.last4 || card.try(:last4) || transaction.tags.try(:[], "card").try(:[], "last4"),
    #            card.try(:exp_date) || transaction.tags.try(:[], "card").try(:[], "exp_date"),
    #            transaction.charge_id
    #        ]
    #      end
    #    end
    #    file.close
    #    obj = s3.bucket('audit-exports').object("#{folder_name}/transactions-#{count}.csv")
    #    obj.upload_file("#{Rails.root}/tmp/#{folder_name}/transactions-#{count}.csv")
    #    count = count + 1
    #    puts "#{offset} transactions exported!"
    #  else
    #    puts "No Transactions found!"
    #    offset = -1
    #  end
    #end
  end
end