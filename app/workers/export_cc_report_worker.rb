class ExportCCReportWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'critical', :retry => 0, backtrace: 8
  def perform(gateway,offset,first_date,second_date,user_id,host_name,bankstatus=nil)
    begin
      store new_status: 'queued'
      reports_cron = CronJob.new(name: "export_cc_report",description: "Export CC Report for dates: #{first_date} - #{second_date}")
      reports_cron.processing!
      result = { start_date: first_date, end_date: second_date,  offset: offset }
      # bankstatus = ActiveModel::Type::Boolean.new.cast(bankstatus)
      # if gateway.present? && bankstatus.present?
      #   decline = OrderBank.includes(:payment_gateway).references(:payment_gateway).where(bank_type: gateway, created_at: first_date..second_date).where.not(status: ["depricated","duplicate"]).where("payment_gateways.is_deleted = :bankstatus",bankstatus: bankstatus)
      # elsif bankstatus.present?
      #   decline = OrderBank.includes(:payment_gateway).references(:payment_gateway).where(created_at: first_date..second_date).where.not(status: ["depricated","duplicate"]).where("payment_gateways.is_deleted = :bankstatus",bankstatus: bankstatus)
      # elsif gateway.present?
      #   decline = OrderBank.includes(:payment_gateway).references(:payment_gateway).where(bank_type: gateway, created_at: first_date..second_date).where.not(status: ["depricated","duplicate"])
      # else
      #   decline = OrderBank.includes(:payment_gateway).references(:payment_gateway).where(created_at: first_date..second_date)
      # end
      unless gateway.present?
        decline = OrderBank.where( bank_type: gateway ,created_at: first_date..second_date).where.not(status: ["depricated","duplicate"])
      else
        gateway = JSON.parse(gateway)
        if gateway.include?("all")
          decline = OrderBank.where(created_at: first_date..second_date).where.not(status: ["depricated","duplicate"])
        else
          decline = OrderBank.where(bank_type: gateway, created_at: first_date..second_date).where.not(status: ["depricated","duplicate"])
        end
      end
      # ActionCable.server.broadcast "report_channel_#{user_id}", channel_name: "report_channel_#{user_id}", action: 'cc_report_export' , message: {count: decline.count}
      csv_file = generate_csv(decline,bankstatus)
      file = StringIO.new(csv_file)
      qc_file = QcFile.new(name: "cc_report_export", start_date: first_date, end_date: second_date, status: :export, user_id: user_id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "cc_report_export: #{first_date.to_date} - #{second_date.to_date}.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      # UserMailer.send_export_file("", qc_file.image.url).deliver_later
      ActionCable.server.broadcast "report_channel_#{host_name}", channel_name: "report_channel_#{host_name}", action: 'cc_report_export', message: qc_file.id
      reports_cron.completed!
      # end
    rescue => exc
      puts "Export Failed for CC Report of dates: #{first_date} - #{second_date}", exc
      ActionCable.server.broadcast "report_channel_#{host_name}", channel_name: "report_channel_#{host_name}", action: 'error' , message: "Server is busy right now! You will get an email once export is ready."
      reports_cron.failed! if reports_cron.present?
    ensure
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end
  end

  def generate_csv(orders, bankstatus=nil)
    bankstatus = ActiveModel::Type::Boolean.new.cast(bankstatus)
    CSV.generate do |csv|
      csv << %w{ id bank_type bank_status status card_type card_sub_type amount created_at user merchant }
      # orders.in_batches(of: 500) do |batch|
        orders.each do |order|
          bank_status = order.payment_gateway.present? ? order.payment_gateway.try(:is_deleted) == true ? 'Inactive' : order.payment_gateway.try(:is_deleted)  == false ? 'Active' : '-' : '-'
          if bankstatus == true && order.payment_gateway.try(:is_deleted) == true && order[:status] != "seq_crash"
            csv << [order[:id], order[:bank_type],bank_status, order[:status], order[:card_type],order[:card_sub_type].try(:titleize), number_to_currency(number_with_precision(order[:amount], :precision => 2, delimiter: ',')), order[:created_at].in_time_zone("Pacific Time (US & Canada)").strftime("%Y-%m-%d %I:%M:%S %p"), order[:user_id], order[:merchant_id]]
          elsif bankstatus == false && order.payment_gateway.try(:is_deleted) == false && order[:status] != "seq_crash"
            csv << [order[:id], order[:bank_type],bank_status, order[:status], order[:card_type],order[:card_sub_type].try(:titleize), number_to_currency(number_with_precision(order[:amount], :precision => 2, delimiter: ',')), order[:created_at].in_time_zone("Pacific Time (US & Canada)").strftime("%Y-%m-%d %I:%M:%S %p"), order[:user_id], order[:merchant_id]]
          elsif bankstatus != true && bankstatus != false && order[:status] != "seq_crash"
            csv << [order[:id], order[:bank_type],bank_status, order[:status], order[:card_type],order[:card_sub_type].try(:titleize), number_to_currency(number_with_precision(order[:amount], :precision => 2, delimiter: ',')), order[:created_at].in_time_zone("Pacific Time (US & Canada)").strftime("%Y-%m-%d %I:%M:%S %p"), order[:user_id], order[:merchant_id]]
          end
        end
      # end
    end
  end
end