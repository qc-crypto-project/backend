class PretransitEmailWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
    store new_status: 'queued'

    Tracker.where(status: "new_order").find_each do |order|
      location = order.location
      if location.on_hold_pending_delivery == true && location.on_hold_pending_delivery_days > 0
        send_email = order.created_at + location.on_hold_pending_delivery_days.days - 24.hours
        if send_email >= DateTime.now.utc.beginning_of_day && send_email < DateTime.now.utc.end_of_day
          merchant = order.tracking_transaction.receiver
          order_id = order.tracking_transaction.order_id
          customer = order.tracking_transaction.sender
          website = JSON(order.location.web_site).first
          # Email to Merchant
          text = I18n.t('notifications.pre_transit_deadline', order_id: order_id)
          Notification.notify_user(merchant,merchant,order,"shipment deadline",nil,text)
          UserMailer.pre_transit_confirmation(merchant.id,order_id).deliver_later
          # Email to Customer
          text = I18n.t('notifications.shipment_delay', order_id: order_id,website: website)
          Notification.notify_user(customer,customer,order,"shipment deadline",nil,text)
          UserMailer.shipment_delay(customer.id,location.id,order_id).deliver_later
        end
      end
    end
    store new_status: 'completed'
  end
end



