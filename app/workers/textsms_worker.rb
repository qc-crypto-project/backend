class TextsmsWorker
  # require 'twilio-ruby'
  include Sidekiq::Worker
  sidekiq_options queue: 'critical', :retry => 3, backtrace: 8

  def perform(to,body)
    account_sid = ENV['TWILIO_SID']
    auth_token = ENV['TWILIO_TOKEN']
    begin
      from = '+18442345430'
      client = Twilio::REST::Client.new account_sid, auth_token
      client.messages.create(:from => from, :to => "+#{to}", :body => body)
      record_it(to,from,body)
    rescue => ex
      puts "==========>>>><<<< Invalid request"
    end
  end

  private

  def number_valid(phone_number)
    begin
      lookup_client = Twilio::REST::LookupsClient.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN'])
      response = lookup_client.phone_numbers.get("+#{phone_number}")
      response.phone_number
      return true
    rescue => e
      puts "============= Invalid ================="
      return false
    end
  end

  def record_it(to, from, body)
    ActivityLog.create(
      :respond_with => body,
      :params => {to: to, from: from}.to_json,
      :note => "Message sent to #{to} at #{Time.now} from #{from} with body: #{body}",
      :action => 'sending-sms',
      :activity_type => 2
    )
  end
end
