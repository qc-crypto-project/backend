class ImportWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker

  def perform(file_name, chunck = nil)
    SequenceLib.import_transactions(file_name, chunck)
  end
end
