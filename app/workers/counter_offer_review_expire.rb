class CounterOfferReviewExpire
	include Sidekiq::Worker
	include Sidekiq::Status::Worker
	sidekiq_options queue: 'default'

	def perform(*args)	

		# allowed_hours = ALLOWED_HOURS || 72
		job = CronJob.new(name: "counter_offer_review_expire",description: "counter review expire , system auto refund")
		result = {updated_records: [] , not_updated_records: [],error: nil}
		begin
			allowed_days = REFUND_ALLOWED_DAYS || 6
			date = HoldInRear.pst_beginning_of_day(DateTime.now) - allowed_days.days
			customer_disputes = CustomerDispute.where(dispute_type: "refund").where(status: "pending")
			customer_disputes.each do |customer_dispute|
				dispute_request = customer_dispute.dispute_requests.where(user_id: customer_dispute.customer_id).where(status: "pending").where(request_type: "counter_offer").where("updated_at <= ? " , date).last
				reminder_date = HoldInRear.pst_beginning_of_day(allowed_days.days.to_date.ago)
				reminder_dispute_request = customer_dispute.dispute_requests.where(user_id: customer_dispute.customer_id).where(status: "pending").where(request_type: "counter_offer").where("updated_at >= ? " , reminder_date).last
				if reminder_dispute_request && !reminder_dispute_request.last.updated_at.today?
					times = ((Date.today - reminder_dispute_request.created_at.to_date).to_i).ordinalize
					customer_name = User.find(customer_dispute.id).name
					subject = "#{times} notice for Refund request for # #{customer_dispute.id} from #{customer_name}."
					UserMailer.partial_refund_request_merchant(customer_dispute.id,customer_dispute.amount,subject).deliver_later
				end
				if dispute_request
					transaction = customer_dispute.dispute_transaction
					if transaction.status != "refunded"
						merch = transaction.receiver
						refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Cancel review expired", merch)
			      		response = refund.process
			      		if response[:success] == true
					        customer_dispute.update(status: "approved",refund_amount: customer_dispute.amount)
					        customer_dispute.dispute_requests.where(user_id: customer_dispute.merchant_id).update(status: "declined")
					        customer_dispute.dispute_requests.last.update(status: "approved",title: "Refunded (By Merchant)",refund_amount: customer_dispute.amount)
					        result[:updated_records] << {id: customer_dispute.id, message: "after refund dispute close"}
					    else
					        result[:not_updated_records] << {id: customer_dispute.id, message: response[:message]}
					    end
					end
				end
			end
			job.result = result
			job.counter_offer_review_expire!				
		rescue Exception => e
			result[:error] = e
			job.counter_offer_review_expire!
		end
	end
end