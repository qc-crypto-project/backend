class CheckbookWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper
  sidekiq_options queue: 'critical', :retry => 3, backtrace: 8

  def perform(check = nil)
    store check_status: 'queued'
    begin
      check = TangoOrder.find_by(id: check.to_i)
      if check.present?
        # check = JSON.parse(check, object_class: OpenStruct)
        unless check.checkId.blank?
          result = get_cheq(check.checkId)
          if result != false
            if result["status"] != check.status
              # if result["status"] != "PAID"
              validation(check,result)
              # end
            end
          end
        end
      end
      store check_status: 'completed'
    rescue
      store check_status: 'completed'
    end
  end


  def validation(check, result)
    check_status = result["status"]
    app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
    wallet = Wallet.where(id: check.wallet_id).first if check.wallet_id.present?
    if wallet.present?
      check_info = {
          name: check.name,
          check_email: check.recipient,
          check_id: check.checkId,
          check_number: check.number
      }
      if check.status == "UNPAID"
        if check_status == "PAID"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "FAILED"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "VOID"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "IN_PROCESS"
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == 'UNPAID'
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == "PRINTED"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        end
      elsif check.status == "IN_PROCESS"
        if check_status == "PAID"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "FAILED"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "VOID"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "IN_PROCESS"
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == 'UNPAID'
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == "PRINTED"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        end
      elsif check_status == "PRINTED"
        check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
      end
      # if check_status == "RETURNED"
      #   issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
      # elsif check_status == "FAILED"
      #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
      # elsif check_status == "VOID"
      #   # if check.status != 'VOID'
      #   issue_checkbook_amount(check.amount, wallet,'send_check',app_config.stringValue, 0, nil, check_info)
      #   # end
      # elsif check_status == "EXPIRED"
      #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0, nil, check_info)
      # else
      #   puts "no if"
      # end
    end
  end


  def get_cheq(cheque)
    begin
      url =URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{cheque}")
      http = Net::HTTP.new(url.host,url.port)
      http.use_ssl =true
      http.verify_mode =OpenSSL::SSL::VERIFY_PEER
      request= Net::HTTP::Get.new(url)
      request["Authorization"]=ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
      request["Content-Type"]='application/json'
      response =http.request(request)
      p "#{cheque} ------ #{response}"
      case response
        when Net::HTTPSuccess
          cheque = JSON.parse(response.body)
          return cheque
        when Net::HTTPUnauthorized
          return false
        when Net::HTTPNotFound
          return false
        when Net::HTTPServerError
          return false
        else
          return false
      end
    rescue => ex
      p"-----------EXCEPTION HANDLED #{ex.message}"
      return false
    end
  end

end
