class SentryWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'default', :retry => 1, backtrace: 8

  def perform(event)
    Raven.send_event(event)
  end
end

