class InvoiceWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform()
    Request.where(status: "pending").where('schedule_date <= ?', Time.now.utc).each do |request|
      begin
        @schedule_date =request.schedule_date
        if @schedule_date.present?
          if Time.now.utc >= @schedule_date
            # request.schedule_date=nil
            request.status = "in_progress"
            request.save
            Sidekiq::Cron::Job.destroy "Invoice#{request.id}"
            UserMailer.invoice_pdf_email("",request.id).deliver_later
          end
        end
      rescue => ex
      end
    end
  end
end