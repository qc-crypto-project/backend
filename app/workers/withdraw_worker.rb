class WithdrawWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(wallets)
    begin
      count = 1
      TangoOrder.where(wallet_id: wallets).select(:id, :wallet_id, :created_at, :status, :order_type, :checkId, :actual_amount, :amount, :check_fee, :fee_perc, :ach_gateway_id, :location_id, :updated_at).find_in_batches(batch_size: 1000) do |tango_orders|
        if tango_orders.present?
          locations = Location.where(id: tango_orders.pluck(:location_id).uniq.compact).select('id,business_name').group_by{|e| e.id}
          file = File.new("#{Rails.root}/tmp/withdraw_report-#{count}.csv", "w")
          file << CSV.generate do |csv|
            csv << ['DBA Name','Wallet ID', 'Withdraw ID', 'Created Date', 'Paid Date', 'Status', 'Type','Total Amount','Transaction Amount','Fee','Bank Descriptor']
            tango_orders.each do |order|
              if order.location_id.blank?
                location = order.wallet.location
              else
                location = locations[order.location_id].first
              end
              if order.status == "PAID" || order.status == "PAID_WIRE"
                if order.instant_ach? || order.ach?
                  tx = Transaction.where("tags -> 'send_check_user_info' ->> 'check_id' = ?", order.checkId).where(main_type: ["ACH Deposit","ACH_deposit"], status: "approved").try(:first)
                  descriptor = order.ach_gateway.try(:descriptor)
                elsif order.instant_pay?
                  tx = Transaction.where("tags -> 'send_check_user_info' ->> 'check_id' = ?", order.checkId).where(main_type: ["push to card deposit","Push to card deposit"], status: "approved").try(:first)
                  descriptor = "Checkbook.io"
                end
              end
              csv << [
                  location.try(:business_name),
                  order.wallet_id,
                  order.id,
                  order.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                  tx.try(:timestamp).present? ? tx.try(:timestamp).to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : order.updated_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                  order.status,
                  withdraw_type(order),
                  number_to_currency(number_with_precision(order.amount.to_f, precision: 2, delimiter: ',')),
                  number_to_currency(number_with_precision(order.actual_amount.to_f, precision: 2, delimiter: ',')),
                  number_to_currency(number_with_precision(order.check_fee.to_f + order.fee_perc.to_f, precision: 2, delimiter: ',')),
                  descriptor
              ]
            end
          end
          file.close
          SlackService.upload_files("withdraw_report-#{count}", file, "#qc-files", "Withdraw report")
          count = count + 1
        end
      end
    rescue => exc
      puts 'Dispute Worker Crashed: ', exc.message
      puts "backtrace: ", exc.backtrace.first(8)
    end
  end

  private

  def withdraw_type(order)
    if order.instant_ach? || order.ach?
      "ACH"
    elsif order.instant_pay?
      "Push to Card"
    elsif order.check?
      "Send Check"
    end
  end
end