class SequenceBalanceCheckWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  #include Admins::ReportsHelper
  #include AdminsHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(folder_name,role )
    s3 = Aws::S3::Resource.new(
        credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],ENV['AWS_SECRET_ACCESS_KEY']),
        region: ENV['AWS_REGION']
    )
    if role != "custom"
      data = {}
      ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential:  ENV['LEDGER_CREDENTIAL'] )
      balance = ledger.tokens.sum(
          group_by: ['account_id']
      )
      balance.each do |bal|
        data.merge!(bal.account_id => {balance: SequenceLib.dollars(bal.amount)})
      end
    end
    Dir.mkdir("#{Rails.root}/tmp/#{folder_name}")
    file = File.new("#{Rails.root}/tmp/#{folder_name}/wallet_balance.csv", "w+")
    if role == "user"
      users = User.includes(:wallets).where.not(role: [:user,:merchant]).order("wallets.created_at asc").uniq
      file << CSV.generate do |csv|
        csv << ["Ref Id","user name", "primary wallet balance"]
        users.each do |user|
          wallets = user.wallets.primary.first.try(:id)
          if user.ref_no.present?
            ref_no = user.ref_no
          elsif user.ref_no.blank?
            if user.merchant?
              ref_no = "M-#{user.id}"
            elsif user.affiliate?
              ref_no = "AF-#{user.id}"
            elsif user.agent?
              ref_no = "A-#{user.id}"
            elsif user.iso?
              ref_no = "ISO-#{user.id}"
            else
              ref_no = "#{user.role.try(:first).try(:capitalize)}-#{user.id}"
            end
          end
          csv << [ref_no,user.name, data[wallets.to_s].try(:[],:balance).to_f]
        end
      end
      file.close
    elsif role == "merchant"
      locations = Location.includes(:wallets,:merchant).references(:wallets).where("wallets.id IN (?)",data.keys)
      file << CSV.generate do |csv|
        csv << ["Ref Id","user name", "DBA", "primary wallet balance", "reserve balance","tip balance"]
        locations.each do |location|
          user = location.merchant || User.where(id: location.merchant_id).last
          wallets = location.wallets.group_by{|e| e.wallet_type}
          csv << [user.ref_no,user.name,location.business_name, data[wallets["primary"].try(:first).try(:id).to_s].try(:[],:balance).to_f, data[wallets["reserve"].try(:first).try(:id).to_s].try(:[],:balance).to_f, data[wallets["tip"].try(:first).try(:id).to_s].try(:[],:balance).to_f]
        end
      end
      file.close
    elsif role == "wallets"
      wallets = Wallet.where(wallet_type: [:qc_support, :escrow, :charge_back,:check_escrow])
      file << CSV.generate do |csv|
        csv << ["Wallet name", "primary wallet balance"]
        # wallets.each do |location|
        #   csv << [location.name, data[wallets[location.id.to_s].try(:first).try(:id).to_s].try(:[],:balance).to_f]
        # end
        wallets.each do |wallet|
          csv << [wallet.name, data[wallet.try(:id).to_s].try(:[],:balance).to_f]
        end
      end
      file.close
    elsif role == "custom"
      locations = Location.includes(:wallets,:merchant).all
      file << CSV.generate do |csv|
        csv << ["Ref Id","user name", "DBA", "wallet balance","TimeStamp", "Seq id"]
        locations.each do |location|
          user = location.merchant || User.where(id: location.merchant_id).last
          wallet = location.wallets.primary.first.id
          first_date = '2020-01-01 07:00:00'
          second_date = '2020-04-01 07:59:59'
          tx = Transaction.where('sender_wallet_id = ? OR receiver_wallet_id = ?', wallet, wallet).where("timestamp BETWEEN :first AND :second",first: first_date, second: second_date).order(timestamp: :desc).first
          balance = 0
          if tx.present?
            if tx.sender_wallet_id == wallet
              balance = tx.sender_balance.to_f - tx.total_amount.to_f
            elsif tx.receiver_wallet_id == wallet
              balance = tx.receiver_balance.to_f + tx.net_amount.to_f
            end
          end
          csv << [user.try(:ref_no),user.try(:name),location.business_name, number_to_currency(number_with_precision(balance, precision: 2, delimiter: ',')), tx.try(:timestamp), tx.try(:seq_transaction_id)]
        end
      end
      file.close
    end
    obj = s3.bucket('audit-exports').object("#{folder_name}/wallet_balance.csv")
    obj.upload_file("#{Rails.root}/tmp/#{folder_name}/wallet_balance.csv")
  end
end
