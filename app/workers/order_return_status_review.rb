class OrderReturnStatusReview
	include Sidekiq::Worker
	include Sidekiq::Status::Worker
	sidekiq_options queue: 'default'

	def perform(*args)	
		job = CronJob.new(name: "return_review_expire",description: "return review expire , system auto refund")
		result = {updated_records: [] , not_updated_records: [],error: nil}
		begin
			allowed_days = ALLOWED_RETURN_DAYS || 8
			date = HoldInRear.pst_beginning_of_day(DateTime.now) - allowed_days.days
			customer_disputes = CustomerDispute.where(dispute_type: "refund").where(status: "pending")

			customer_disputes.each do |customer_dispute|
				dispute_request = customer_dispute.dispute_requests.where(user_id: customer_dispute.merchant_id).where(status: "pending").where(request_type: "return_to_sender").where("updated_at <= ? " , date).last
				if dispute_request
					transaction = customer_dispute.dispute_transaction
					if transaction.tracker.present?
						transaction.tracker.update(return_status: "cancel")
					end
			        customer_dispute.update(status: "declined")
			        customer_dispute.dispute_requests.where(user_id: customer_dispute.merchant_id).update(status: "declined")
			        result[:updated_records] << {id: customer_dispute.id, message: "after refund dispute close"}
				end
			end
			job.result = result
			job.return_status_review_expire!			
		rescue Exception => e
			result[:error] = e
			job.return_status_review_expire!
		end
	end
end