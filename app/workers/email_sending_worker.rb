class EmailSendingWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  include Admins::EmailsHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(notification_id, schedule_date=nil, for_testing=nil)
    begin
      notification = EmailNotification.find_by(id: notification_id, status: "proccessing")
      reports_cron = CronJob.new(name: "EmailSending",description: "Sending emails to Customers", log_type: "rake_task", status: "processing")
      result = {}
      @schedule_date = schedule_date
      emails = []
      if notification.search_option != "merchant"
        emails = User.active_users.where(role: notification.search_option).pluck(:id,:email,:name,:role)
      elsif notification.search_option == "merchant" && notification.category_id.blank?
        emails = User.active_users.where(role: notification.search_option).joins(:oauth_apps).where("oauth_apps.is_block = ?",false).pluck(:id,:email,:name,:role)
      elsif notification.search_option == "merchant" && notification.category_id.present?
        sql = "SELECT users.id AS id, users.email AS email, users.name AS name, users.role AS role FROM locations INNER JOIN users ON locations.merchant_id=users.id WHERE locations.category_id = #{notification.category_id} AND users.is_block = false AND users.archived = false"
        emails = ActiveRecord::Base.connection.execute(sql).values
      end
      if emails.present?
        attachments_url = []
        if notification.images.present?
          notification.images.each do|v|
            attachments_url.push({id: v.id,url: v.add_image.url, type: v.add_image_content_type, name: v.add_image_file_name})
          end
        end
        if @schedule_date.present?
          if Time.now.utc >= @schedule_date
            response_result = notification_email(notification.id,emails,attachments_url)
            Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}"
            notification.update(status: "processed") if response_result[:success]
            result["message"] = response_result[:message]
          else
            result["message"] = "Future Date"
          end
        else
          response_result = notification_email(notification.id,emails,attachments_url)
          Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}"
          notification.update(status: "processed") if response_result[:success]
          result["message"] = response_result[:message]
        end
      else
        result["message"] = "Users not exist!"
      end

    rescue => ex
      result["message"] = ex.message
    ensure
      reports_cron.result = result
      reports_cron.save
    end
  end
end
