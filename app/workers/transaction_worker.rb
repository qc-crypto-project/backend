class TransactionWorker
  include Sidekiq::Worker
  @@ledger = ENV['LEDGER_CREDENTIAL']

  sidekiq_options queue: 'transactionss'
  def perform(to, from,amount,reference, data)
    ledger = @@ledger
    tx = ledger.transactions.transact do |builder|
           builder.transfer(
            asset_alias: 'usd',
            amount:"#{amount}".to_i,
            source_account_alias: "#{from}",
            destination_account_alias: "#{to}",
            reference_data: {
              type: "#{reference}",
              source_name: "#{wallet_name(from)}",
              destination_name: "#{wallet_name(to)}",
              data: data })
      end
  end



  private

  def wallet_name(id)
    wallet = Wallet.find_by_id(id) if id.present?
    if wallet.present?
      return wallet.name
    else
      return ''
    end
  end

end
