class MerchantTransactionsWorker
  include Sidekiq::Worker
  include Merchant::TransactionsHelper
  include Merchant::ReportsHelper
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(merchant_id)
    begin

      user = User.includes(:wallets).where(id: merchant_id).first
      wallets = user.wallets
      primary_wallet = wallets.select{|w| w.primary? }
      reserve_wallet = wallets.select{|w| w.reserve? }
      tip_wallet = wallets.select{|w| w.tip? }
      headers = [
          "Block ID",
          "date/time",
          "Type",
          "Sender Name",
          "Receiver Name",
          "Sender Wallet ID",
          "Receiver Wallet ID",
          "first6",
          "last4",
          "IP",
          "Total Amount",
          "TXN Amount",
          "Fee",
          "Reserve Money",
          "Total Net"]
      file = File.new("#{Rails.root}/tmp/#{merchant_id}-primary-#{DateTime.now.to_i}.csv", "w")
      file << CSV.generate do |csv|
        csv << headers
        BlockTransaction.where("sender_wallet_id IN (:wallet_id) OR receiver_wallet_id IN (:wallet_id)",wallet_id: primary_wallet.pluck(:id)).find_in_batches(batch_size: 5000).each do |batch|
          batch.each do |tx|
            csv << [
                tx.sequence_id,
                tx.timestamp.present? ? tx.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : tx.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                tx.main_type,
                tx.sender_name,
                tx.receiver_name,
                tx.sender_wallet_id,
                tx.receiver_wallet_id,
                tx.first6,
                tx.last4,
                tx.ip,
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.amount_in_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.tx_amount), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.fee_in_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.hold_money_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(tx.total_net.to_f, precision: 2,delimiter: ','))}"
            ]
          end
        end
      end
      file.close
      SlackService.upload_files("#{merchant_id}-primary-#{DateTime.now.to_i}", file, I18n.t("slack.channels.qc_files"), "#{merchant_id}-primary-#{DateTime.now.to_i}")

      file1 = File.new("#{Rails.root}/tmp/#{merchant_id}-reserve-#{DateTime.now.to_i}.csv", "w")
      file1 << CSV.generate do |csv|
        csv << headers
        BlockTransaction.where("sender_wallet_id IN (:wallet_id) OR receiver_wallet_id IN (:wallet_id)",wallet_id: reserve_wallet.pluck(:id)).find_in_batches(batch_size: 5000).each do |batch|
          batch.each do |tx|
            csv << [
                tx.sequence_id,
                tx.timestamp.present? ? tx.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : tx.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                tx.main_type,
                tx.sender_name,
                tx.receiver_name,
                tx.sender_wallet_id,
                tx.receiver_wallet_id,
                tx.first6,
                tx.last4,
                tx.ip,
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.amount_in_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.tx_amount), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.fee_in_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.hold_money_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(tx.total_net.to_f, precision: 2,delimiter: ','))}"
            ]
          end
        end
      end
      file1.close
      SlackService.upload_files("#{merchant_id}-reserve-#{DateTime.now.to_i}", file1, I18n.t("slack.channels.qc_files"), "#{merchant_id}-reserve-#{DateTime.now.to_i}")

      file2 = File.new("#{Rails.root}/tmp/#{merchant_id}-tip-#{DateTime.now.to_i}.csv", "w")
      file2 << CSV.generate do |csv|
        csv << headers
        BlockTransaction.where("sender_wallet_id IN (:wallet_id) OR receiver_wallet_id IN (:wallet_id)",wallet_id: tip_wallet.pluck(:id)).find_in_batches(batch_size: 5000).each do |batch|
          batch.each do |tx|
            csv << [
                tx.sequence_id,
                tx.timestamp.present? ? tx.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : tx.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                tx.main_type,
                tx.sender_name,
                tx.receiver_name,
                tx.sender_wallet_id,
                tx.receiver_wallet_id,
                tx.first6,
                tx.last4,
                tx.ip,
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.amount_in_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.tx_amount), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.fee_in_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.hold_money_cents), precision: 2,delimiter: ','))}",
                "#{number_to_currency(number_with_precision(tx.total_net.to_f, precision: 2,delimiter: ','))}"
            ]
          end
        end
      end
      file2.close
      SlackService.upload_files("#{merchant_id}-tip-#{DateTime.now.to_i}", file1, I18n.t("slack.channels.qc_files"), "#{merchant_id}-tip-#{DateTime.now.to_i}")

    rescue => exc
      SlackService.notify(exc.message.to_s,  '#test-channel')
      SlackService.notify(exc.backtrace.to_json, '#test-channel')
    end
  end
end
