class TrackerEmailWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
    store new_status: 'queued'

    Tracker.where(status: ["new_order","pre-transit"]).find_each do |order|
      location = order.location
      if location.on_hold_pending_delivery == true && location.on_hold_pending_delivery_days > 0
        send_email = order.created_at + location.on_hold_pending_delivery_days.days
        if send_email >= DateTime.now.utc.beginning_of_day && send_email < DateTime.now.utc.end_of_day
          order.overdue!
          merchant = order.tracking_transaction.receiver
          order_id = order.tracking_transaction.order_id
          customer = order.tracking_transaction.sender
          website = JSON(order.location.web_site).first
          # Email to Merchant
          text = I18n.t('notifications.in_transit_deadline', order_id: order_id)
          Notification.notify_user(merchant,merchant,order,"shipment overdue",nil,text)
          UserMailer.overdue_shipment(merchant.id,order_id).deliver_later
          # Email to Customer
          text = I18n.t('notifications.shipment_delay', order_id: order_id,website: website)
          Notification.notify_user(customer,customer,order,"shipment delay",nil,text)
          UserMailer.overdue_shipment_confirmation(customer.id,location.id,order_id).deliver_later
        end
      end
    end
    store new_status: 'completed'
  end
end


