class CardBinListWorker
	include Sidekiq::Worker
	include Sidekiq::Status::Worker
	sidekiq_options queue: 'default', :retry => 0, backtrace: 8
	def perform(*arg)
		# cards = Card.where(brand: ["mastercard", "visa", nil, "Visa", "amex", "discover", "Mastercard", "American Express", "Discover", "unknown", "jcb", "unionpay", "Diners Club", "JCB", "Traditional", "Platinium", "Debit Other 2 Embossed", "Standard", "Unembossed Prepaid Student", "Prepaid Debit Government", "Business", "World Debit Embossed", "World", "Debit", "american express", "Prepaid Debit Gift", "Credit", "Debit Business", "Gold", "Standard Immediate Debit", "Electron", "Prepaid Debit Consumer Incentive", "World Elite", "Prepaid Debit Payroll", "Signature", "Infinite", "Prepaid Platinum", "Cirrus", "Commercial/Business", "Corporate", "Standard Prepaid", "Prepaid", "Credit Business Prepaid", "Prepaid Debit Flex Benefit", "Debit Platinum"] ).last(100)
		cards =  Card.find_by_sql("SELECT * FROM (SELECT ROW_NUMBER() OVER (PARTITION BY brand ORDER BY brand DESC) AS r, cards.* FROM cards) x WHERE x.r <= 3 ORDER BY brand")
		bin_list_responses = {responses: []}
		new_api_responses = {responses: []}
		cards.each do |card|
			sleep 6
			bin_list_responses[:responses] << check_bin_list_response(card)
			new_api_responses[:responses] << check_new_api_response(card)
		end
		upload_to_slack("bin_list_response",bin_list_responses[:responses])
		upload_to_slack("new_api_response",new_api_responses[:responses])
	end


	def upload_to_slack(name,files)
    	file = File.new("#{Rails.root}/tmp/#{name}.csv", "w")
    	file << CSV.generate do |csv|
      		csv << ["ID","Number","Scheme","Type","Brand","Country","Code","Currency","bank","phone"]
      		files.each do |file|
        		csv << [file[:id] ,file[:first6], file[:scheme] , file[:type], file[:brand] , file[:country] ,file[:code],file[:currency],file[:bank],file[:phone]]
      		end
    	end
    	file.close
    	SlackService.upload_files(name, file, "#qc-files", "#{name} Card Detail")
  end


	def check_bin_list_response(card)
		url = "https://3f29e97d-d429-4559-a710-12b310080fe8:@lookup.binlist.net/#{card.first6}"
		response = {}
		begin
	        response = RestClient.post(url,{}, {content_type: :json, accept: :json})
	        response = JSON.parse(response)
	        return response = {id: card.id,first6: card.first6,scheme: response["scheme"].try(:downcase),type: response["type"],brand: response["brand"] , country: response["country"].try(:[],"name"),code: response["country"].try(:[],"alpha2"), currency: response["country"].try(:[],"currency"),bank: response["bank"].try(:[],"name"),phone: response["bank"].try(:[],"phone")}
	    rescue => ex
	    	return response
	    end		
	end

	def check_new_api_response(card)
		url = "https://neutrinoapi.net/bin-lookup"
		response = {}
      	begin
	        parameters = {
	          "user-id" => "Danusinovich",
	          "api-key" => "XOfKXWI91rJY0k5CAxxWkvO7MdFIwwZuC01P4mxN4KNLeRPP",
	          "bin-number" => card.first6
	        }
	        response = RestClient.post(url,parameters, {content_type: :json, accept: :json})
	        response = JSON.parse(response)
	        response = {id: card.id,first6: card.first6,scheme: response["card-brand"].try(:downcase),type: response["card-type"].try(:downcase),brand: response["card-category"].try(:titleize) , country: response["country"].try(:titleize),code: response["country-code"], currency: response["currency-code"],bank: response["issuer"],phone: response["issuer-phone"]}
	        return response
      	rescue => ex
      		return response
      	end
	end
end