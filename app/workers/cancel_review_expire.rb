class CancelReviewExpire
	include Sidekiq::Worker
	include Sidekiq::Status::Worker
	sidekiq_options queue: 'default'

	def perform(*args)	
		job = CronJob.new(name: "cancel_review_expire",description: "cancel review expire , system auto refund")
		result = {updated_records: [] , not_updated_records: [],error: nil}
		begin
			allowed_hours = ALLOWED_HOURS || 72
			date = HoldInRear.pst_beginning_of_day(DateTime.now) - allowed_hours.hours
			customer_disputes = CustomerDispute.where(dispute_type: "cancel").where(status: "pending").where('updated_at <= ?',date)
			
		
			customer_disputes.each do |customer_dispute|
				transaction = customer_dispute.dispute_transaction
				if transaction.status != "refunded"
					merch = transaction.receiver
					refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Cancel review expired", merch)
		      		response = refund.process
		      		if response[:success] == true
				        customer_dispute.update(status: "full_refund")
				        customer_dispute.dispute_requests.update(status: "full_refund",title: "Full Refund")

				        if transaction.tracker.present?
				          transaction.tracker.cancel!
				        end
				        result[:updated_records] << {id: customer_dispute.id, message: "after refund dispute close"}
				    else
				        result[:not_updated_records] << {id: customer_dispute.id, message: response[:message]}
				    end
				end
			end
			job.result = result
			job.cancel_review_expire!	
		rescue Exception => e
			result[:error] = e
			job.cancel_review_expire!	
		end		
	end
end