class DisputeStatusWorker
  include TransactionCharge::ChargeATransactionHelper
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
    # Do something
      job = CronJob.new(name: 'dispute_status:update_staus', description: 'update disputes status', success: true)
    result = {updates: [], error: nil}
    updated = []
    begin
      puts "Dispute Update Started..."
      dispute_case=DisputeCase.charge_back.where(due_date:Date.today,status:['under_review','sent_pending'])
      if dispute_case.present?
        dispute_case.find_each(batch_size: 50) do |cas|
          dispute_case_transaction = DisputeCaseTransaction.new
          puts "Updating #{cas.id}"
          if cas.present?
            if cas.charge_back? and (cas.sent_pending? || cas.under_review?)
              cas.status='lost'
              dispute_case_transaction.transfer_charge_back_lost(cas,nil)
              cas.save
            end
          end
          updated << cas.id
          puts "#{updated} disputes updated..."
        end
      end
    rescue StandardError => exc
      job.success = false
      result[:error] = "Error: #{exc.message}"
      puts 'Error occured during the task process: ', exc
    ensure
      puts "successfully updated #{updated} disputes!"
      result[:updates] << updated
      job.result = result if job.present?
      job.rake_task! if job.present?
    end



  end
end
