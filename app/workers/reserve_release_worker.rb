class ReserveReleaseWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper
  include Admins::BlockTransactionsHelper
  sidekiq_options queue: 'default', :retry => 1, backtrace: 8

  def perform(*args)
    begin
      description = []
      cron_job = CronJob.create(name: 'release_batch')
      Wallet.includes(:users, :location).reserve.find_each(batch_size: 25) do |wallet|
        @wallet_id = wallet.id
        if @wallet_id != 47129 #this wallet is not letting us transfer from sequence
          release = false
          location = wallet.location
          fees = location.fees.buy_rate.first if location.present?
          reserve_days = fees.days if fees.present?
          reserve_amount = fees.reserve_fee if fees.present?
          if reserve_amount.to_f > 0 && reserve_days > 0
            release = true
          elsif reserve_amount.to_f == 0 && reserve_days == 0
            release = true
          end
          if release == true
            if reserve_days == 0
              previous_date = DateTime.now - 1.days
            else
              previous_date = DateTime.now - reserve_days.days
            end
            first_date = Time.new(previous_date.year, previous_date.month, previous_date.day,00,00,00)
            second_date = Time.new(previous_date.year, previous_date.month, previous_date.day,23,59,59)
            transactions = BlockTransaction.where(receiver_wallet_id: @wallet_id, main_type: "Reserve Money Deposit").where("timestamp BETWEEN :first AND :second",first: first_date,second: second_date)
            transactions_count = transactions.try(:count)
            received_amount = transactions.pluck(:amount_in_cents).sum
            total_amount = SequenceLib.dollars(received_amount)
            balance = SequenceLib.balance(@wallet_id)
            batch = HoldInRear.where(created_at: first_date..second_date,location_id: location.id).first
            batch_info = {batch_id: batch.id,batch_created_at: batch.created_at,transactions_count: transactions_count} if batch.present?
            if total_amount.to_f > 0
              user = wallet.users.first
              if balance.to_f >= total_amount.to_f
                primary_wallet = location.wallets.primary.first.try(:id) if location.present?
                tr_type = get_transaction_type(TypesEnumLib::TransactionType::ReserveTransfer)
                sender_dba=get_business_name(Wallet.find_by(id:@wallet_id))
                receiver_dba=get_business_name(Wallet.find_by(id:primary_wallet))
                transaction = Transaction.create(
                    sender_wallet_id: @wallet_id,
                    receiver_wallet_id: primary_wallet,
                    sender_name: sender_dba,
                    receiver_name: receiver_dba,
                    sender_balance: balance,
                    receiver_balance: SequenceLib.balance(primary_wallet),
                    amount: total_amount,
                    total_amount: total_amount,
                    net_amount: total_amount,
                    status: "pending",
                    sender_id: user.id,
                    receiver_id: user.id,
                    main_type: tr_type.first,
                    sub_type: tr_type.second,
                    action: "transfer"
                )
                transfer = SequenceLib.transfer(total_amount, @wallet_id, primary_wallet, tr_type.first, 0 ,nil, "Quickcard", nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,batch_info)
                if transfer.present?
                  description = description.push({amount_in_released: total_amount,wallet_id: @wallet_id, user_id: user.id, status: "success"})
                  transaction.update(seq_transaction_id: transfer.actions.first.id,
                                     timestamp: transfer.timestamp,
                                     status: "approved",
                                     tags: transfer.actions.first.tags
                                     )
                  parsed_transactions = parse_block_transactions(transfer.actions, transfer.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                  noti_amount = number_to_currency(number_with_precision(total_amount, :precision => 2, delimiter: ','))
                  text = I18n.t('notifications.reserve_release_notif', amount: noti_amount)
                  Notification.notify_user(user,user,transaction,"Reserve Release",nil,text)
                  # UserMailer.reserve_release(user,total_amount,'reserve').deliver_later if total_amount.to_f > 0
                else
                  description = description.push({amount_not_released: total_amount,wallet_id: @wallet_id, user_id: user.id, status: "false"})
                end
              else
                description = description.push({amount_not_released: total_amount,wallet_id: @wallet_id, user_id: user.id, status: "false",error: "Insufficient balance"})
              end
            end
          end
        end
        sleep(1)
      end
      cron_job.description = description
      cron_job.release_batch!
    rescue StandardError => exc
      cron_job.description = {error: exc.message, backtrace: exc.backtrace.first(8), wallet_id: @wallet_id}
      cron_job.release_batch!
    end
  end
end
