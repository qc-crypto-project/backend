class MaxmindEmailWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
    store new_status: 'queued'
    allowed_days = ALLOWED_DAYS - 1

    MinfraudResult.where("qc_action = ?", TypesEnumLib::RiskType::PendingReview).find_each do |minfraud|
      send_email = minfraud.created_at + allowed_days.days
      if send_email >= DateTime.now.utc.beginning_of_day && send_email < DateTime.now.utc.end_of_day
      transaction_id = minfraud.transaction_id
      # UserMailer.manual_review_3ds(transaction_id).deliver_later
      end
    end

    store new_status: 'completed'
  end
end

