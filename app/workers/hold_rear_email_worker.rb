class HoldRearEmailWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
    store new_status: 'queued'
    User.merchant.where(merchant_id:nil).each do |user|
      if user.present? && user.get_locations.present?
        locations=user.get_locations
        total_amount=0
        locations.each do |loc|
           amount=HoldInRear.calculate_released_on_date_custom(loc.id, Time.zone.now - 1.day )
           total_amount =amount + total_amount.to_f
        end
        if total_amount > 0
          noti_amount = number_to_currency(number_with_precision(total_amount, :precision => 2, delimiter: ','))
          text = I18n.t('notifications.hold_in_arrear_notif', amount: noti_amount)
          return if Notification.today.where(text: text,recipient_id: user.id).present?
          Notification.notify_user(user,user,user,"Hold in arrear",nil,text)
        end
      end
      # UserMailer.reserve_release(user,total_amount,nil).deliver_later if total_amount.to_f > 0
    end
    store new_status: 'completed'
  end
end

