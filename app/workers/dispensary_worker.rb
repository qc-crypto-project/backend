class DispensaryWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(file_name, role, category = nil, pending = nil)
    return SlackService.notify("*Invalid command.*\ntype: \`\`\`Report\`\`\` command: \`\`\`#{role}\`\`\` Valid commands: \`\`\`merchant, user, wallets, dispute_case\`\`\`", "#qc-files") if role != "merchant" && role != "user" && role != "wallets" && role != "dispute_case" && role != "customer" && role != "locations"
    begin
      data = {}
      ledger = Sequence::Client.new(ledger_name: ENV['LEDGER_NAME'], credential: ENV['LEDGER_CREDENTIAL'] )
      page = ledger.tokens.sum(
          group_by: ['account_id']
      )
      page.each do |bal|
        data.merge!(bal.account_id => {balance: SequenceLib.dollars(bal.amount)})
      end
      if role == "merchant"
        if category.present?
          if category == "Dispensary"
            locations = Location.joins(:category, :wallets).where("categories.name ILIKE '%Dispensary%'").uniq
          elsif category == "CBD"
            locations = Location.joins(:category, :wallets).where("categories.name ILIKE '%CBD%' or categories.name ILIKE '%Hemp%'").uniq
          elsif category == "others"
            locations = Location.joins(:category, :wallets).where.not("categories.name ILIKE '%CBD%' or categories.name ILIKE '%Dispensary%' or categories.name ILIKE '%Hemp%'").uniq
          end
        else
          locations = Location.all.uniq
        end
        file = File.new("#{Rails.root}/tmp/#{file_name}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["MID", "Merchant Name", "DBA Name", "Main Wallet Amount", "Tip Wallet Amount", "Reserve Wallet Amount"]
          locations.each do |location|
            merchant = location.merchant || User.where(id: location.merchant_id).last
            wallets = location.wallets.group_by{|e| e.wallet_type}
            # if pending.present?
            #   tango_orders = merchant.tango_orders.group_by{|e| e.order_type} if merchant.present?
            #   ach = tango_orders["instant_ach"].group_by{|e| e.status} if tango_orders.present? && tango_orders["instant_ach"].present?
            #   ach_count = ach["PENDING"].count if ach.present? && ach["PENDING"].present?
            #   ach_amount = ach["PENDING"].pluck(:amount).sum if ach.present? && ach["PENDING"].present?
            #   p2c = tango_orders["instant_pay"].group_by{|e| e.status} if tango_orders.present? && tango_orders["instant_pay"].present?
            #   p2c_count = p2c["PENDING"].count if p2c.present? && p2c["PENDING"].present?
            #   p2c_amount = p2c["PENDING"].pluck(:amount).sum if p2c.present? && p2c["PENDING"].present?
            #   check = tango_orders["check"].group_by{|e| e.status} if tango_orders.present? && tango_orders["check"].present?
            #   check_count = check["PENDING"].count if check.present? && check["PENDING"].present?
            #   check_amount = check["PENDING"].pluck(:amount).sum if check.present? && check["PENDING"].present?
            # end
            if merchant.present?
              csv << ["M-#{merchant.id}", merchant.name, location.business_name, number_to_currency(data["#{wallets["primary"].first.id}"].try(:[],:balance).to_f), number_to_currency(data["#{wallets["tip"].first.id}"].try(:[],:balance).to_f), number_to_currency(data["#{wallets["reserve"].first.id}"].try(:[],:balance).to_f)]
            end
          end
        end
        file.close
      elsif role == "user"
        users = User.where(role: [:iso, :agent, :affiliate])
        file = File.new("#{Rails.root}/tmp/#{file_name}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["ID", "Name", "Main Wallet Amount", "Pending ACH Count", "Pending ACH Amount",
                  "Pending P2C Count", "Pending P2C Amount", "Pending Checks Count", "Pending Checks Amount"]
          users.each do |user|
            wallets = user.wallets.primary.first
            # if pending.present?
            #   tango_orders = user.tango_orders.group_by{|e| e.order_type}
            #   ach = tango_orders["instant_ach"].group_by{|e| e.status} if tango_orders.present? && tango_orders["instant_ach"].present?
            #   ach_count = ach["PENDING"].count if ach.present? && ach["PENDING"].present?
            #   ach_amount = ach["PENDING"].pluck(:amount).sum if ach.present? && ach["PENDING"].present?
            #   p2c = tango_orders["instant_pay"].group_by{|e| e.status} if tango_orders.present? && tango_orders["instant_pay"].present?
            #   p2c_count = p2c["PENDING"].count if p2c.present? && p2c["PENDING"].present?
            #   p2c_amount = p2c["PENDING"].pluck(:amount).sum if p2c.present? && p2c["PENDING"].present?
            #   check = tango_orders["check"].group_by{|e| e.status} if tango_orders.present? && tango_orders["check"].present?
            #   check_count = check["PENDING"].count if check.present? && check["PENDING"].present?
            #   check_amount = check["PENDING"].pluck(:amount).sum if check.present? && check["PENDING"].present?
            # end
            if user.iso?
              id = "ISO-#{user.id}"
            elsif user.agent?
              id = "A-#{user.id}"
            elsif user.affiliate?
              id = "AF-#{user.id}"
            end
            csv << [id, user.name, number_with_precision(number_to_currency(data["#{wallets.id}"].try(:[],:balance).to_f), precision: 2)]
          end
        end
        file.close
      elsif role == "customer"
        file = File.new("#{Rails.root}/tmp/#{file_name}.csv", "w")
        ids = data.keys.map(&:to_i)
        User.joins(:wallets).where(role: :user).where("wallets.id IN (?)", ids).find_in_batches(batch_size: 20000) do |users|
          file << CSV.generate do |csv|
            csv << ["ID", "Name", "Main Wallet Amount"]
            users.each do |user|
              wallets = user.wallets.primary.first
              csv << ["C-#{user.id}", user.name, number_with_precision(number_to_currency(data["#{wallets.id}"].try(:[],:balance).to_f), precision: 2)]
            end
          end
        end
        file.close
      elsif role == "wallets"
        wallets =  Wallet.where(wallet_type: [:qc_support, :escrow, :charge_back, :check_escrow])
        file = File.new("#{Rails.root}/tmp/#{file_name}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["ID", "Name", "Main Wallet Amount"]
          wallets.each do |wallet|
            csv << [wallet.id, wallet.name, number_with_precision(number_to_currency(data["#{wallet.id}"].try(:[],:balance).to_f), precision: 2)]
          end
        end
        file.close
      elsif role == "dispute_case"
        disputes = DisputeCase.sent_pending
        file = File.new("#{Rails.root}/tmp/#{file_name}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["Merchant Name", "DBA Name", "Amount", "Transaction ID", "Case Number"]
          disputes.each do |dispute|
            location = dispute.merchant_wallet.location
            business_name = location.business_name if location.present?
            merchant_name = location.merchant.try(:name) || User.where(id: location.merchant_id).last.try(:name) if location.present?
            csv << [merchant_name, business_name, number_with_precision(number_to_currency(dispute.amount), precision: 2), dispute.charge_transaction.try(:seq_transaction_id), dispute.case_number]
          end
        end
        file.close
      elsif role == "locations"
        locations = Location.joins(:wallets).all.uniq
        file = File.new("#{Rails.root}/tmp/#{file_name}.csv", "w")
        file << CSV.generate do |csv|
          csv << ["Merchant ID", "Created Day", "Merchant Name", "DBA", "DBA Created Day", "Main Wallet Funds", "Tip Wallet Funds", "Reserve Wallet Funds",
                  "Status", "Owner's Name", "Email"]
          locations.each do |location|
            merchant = location.merchant || User.where(id: location.merchant_id).last
            wallets = location.wallets.group_by{|e| e.wallet_type}
            if location.is_block
              status = "Inactive"
            else
              status = "Active"
            end
            owners = merchant.try(:owners)
            if owners.present?
              owner_name = owners.pluck(:name).join(",")
            end
            if merchant.present?
              csv << ["M-#{merchant.id}", merchant.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"), merchant.name, location.business_name,location.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),number_to_currency(data["#{wallets["primary"].first.id}"].try(:[],:balance).to_f), number_to_currency(data["#{wallets["tip"].first.id}"].try(:[],:balance).to_f), number_to_currency(data["#{wallets["reserve"].first.id}"].try(:[],:balance).to_f),
                      status, owner_name, location.email]
            end
          end
        end
        file.close
      end
      SlackService.upload_files("#{file_name}", file, "#qc-files", "#{file_name}") if file.present?
    end
  rescue => exc
    puts 'Mtrac Worker Crashed: ', exc.message
    puts "backtrace: ", exc.backtrace.first(8)
  end
end
