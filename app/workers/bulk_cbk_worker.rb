class BulkCbkWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include TransactionCharge::ChargeATransactionHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(cbks,request=nil,user_id=nil, session_id = nil, type = nil)
    host_name = session_id
    if type == "charge_id"
      create_with_charge_id(cbks, request, user_id, host_name)
    elsif type == "card"
      create_with_card(cbks, request, user_id, host_name)
    elsif type == "lost_bulk_cbks"
      lost_bulk_cbk(cbks, request, user_id, host_name)
    end
  end
  def lost_bulk_cbk(cbks, request, user_id, host_name)
    # to lost cbk bulk
    results = CustomLib.lost_ckb(cbks) if cbks.present?
    cbks_count = results['updates'].count
    if results['updates'].present?
       # generating CSV cbk
      result_csv = CSV.generate do |csv|
        csv << ['Case','Dispute Id', 'Sequence Id']
        results['updates'].each do |cbk|
          csv << [cbk["case_number"],cbk["dispute_id"],cbk["sequence_id"] ]
        end
      end

      file = StringIO.new(result_csv)
      qc_file = QcFile.new(name: "admin_lost_bulk_cbk",start_date: Time.now, status: :lost_bulk_cbk, user_id: user_id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "lost_bulk_cbk.csv")
      qc_file.info = cbks_count
      qc_file.save
      results[:file_url] = qc_file.image.url
      results[:qc_file_id] = qc_file.id
    end
  end

  def create_with_charge_id(cbks, request, user_id, host_name)
    begin
      id = nil
      new_cbks = []
      cron_job = CronJob.new(name: "bulk_cbk",description: "Importing Bulk ChargeBack", success: true)
      result = {updates: [],  error: nil}
      cbks_count = cbks.try(:count)
      ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "0/#{cbks_count}"
      cbks.each_with_index do|d,i|
        i = i + 1
        d = d.symbolize_keys
        new_cbks_single = d.dup
        id = d[:charge_id]
        d[:amount] = d[:amount].gsub(/[^\d\.]/, '').to_f
        transactions = Transaction.where('charge_id ILIKE ?', "%#{d[:charge_id]}")
        if transactions.present?
          if transactions.count == 1 && transactions.first.action == "issue" && transactions.first.from == "Credit"
            transaction = transactions.last
          elsif transactions.count == 2
            transaction = transactions.where(action: "transfer").last
          elsif transactions.first.action == "transfer"
            transaction = transactions.where(action: "transfer").last
          end
          if transaction.present?
            if transaction.dispute_case.present?
              new_cbks_single[:result] = "Dispute Already Exist"
              new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
              new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
              new_cbks_single[:customer_name] = transaction.sender.try(:name)
              new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
              result[:updates].push({charge_id: d[:charge_id], result: "Dispute Already Exist"})
              ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
            else
              dispute_case_transaction = DisputeCaseTransaction.new
              merchant_sequence_balance = SequenceLib.balance(transaction.receiver_wallet_id)
              charge_back_fee = dispute_case_transaction.get_new_location_fee(transaction.receiver_wallet_id,TypesEnumLib::CommissionType::ChargeBack,d[:amount].to_f, nil, d[:fee] == "yes" ? true : false)
              retrievel_fee=dispute_case_transaction.get_new_location_fee(transaction.receiver_wallet_id,TypesEnumLib::CommissionType::Retrieval,d[:amount].to_f, nil, d[:fee] == "yes" ? true : false)

              if d[:amount].present? && charge_back_fee["fee"].to_f == 0
                charge_back_total_amount = d[:amount].to_f
              elsif d[:amount].present? && charge_back_fee["fee"].present?
                charge_back_total_amount = (d[:amount].to_f + charge_back_fee["fee"].to_f)
              else
                charge_back_total_amount = 0
              end
              retrievel_total_amount = retrievel_fee["fee"].to_f

              if (charge_back_total_amount < merchant_sequence_balance and d[:type].downcase == "chargeback") or ( retrievel_total_amount < merchant_sequence_balance and d[:type].downcase == "retrievel" )
                received_date = d[:received_date].split('/')
                received_date[0],received_date[1] = received_date[1],received_date[0]
                received_date[2] = "20#{received_date[2]}"
                received_date = received_date.join('/')
                due_date = d[:due_date].split('/')
                due_date[0],due_date[1] = due_date[1],due_date[0]
                due_date[2] = "20#{due_date[2]}"
                due_date = due_date.join('/')
                dispute_case = DisputeCase.create(
                    case_number: d[:charge_id],
                    user_id: 1,
                    transaction_id: transaction.id,
                    recieved_date: received_date.to_datetime,
                    due_date: due_date.to_datetime,
                    amount: d[:amount],
                    reason_id: d[:reason],
                    merchant_wallet_id: transaction.receiver_wallet_id,
                    user_wallet_id: transaction.sender_wallet_id.present? ? transaction.sender_wallet_id : transaction.receiver_wallet_id,
                    card_number: transaction.last4,
                    status: d[:status].tr('/','_').downcase,
                    dispute_type: d[:type].downcase == "chargeback" ? "charge_back" : "retrievel",
                    charge_fee: d[:fee] == "yes" ? true : false,
                    payment_gateway_id: transaction.payment_gateway_id
                )
                if dispute_case.save
                  Note.create(notable_type: "Transaction", notable_id: dispute_case.id, title: d[:notes])
                  if dispute_case.dispute_type == "charge_back" and (dispute_case.status == "won_reversed" or dispute_case.status == "lost" or dispute_case.status == "dispute_accepted")
                    dispute_type_charge_back_won_or_lost_on_create(dispute_case,d[:fee] == "yes" ? true : false,request)
                  else
                    dispute_type_process(dispute_case,d[:fee] == "yes" ? true : false,request)
                  end
                  new_cbks_single[:result] = "Dispute Successfully Saved!"
                  new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                  new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                  new_cbks_single[:customer_name] = transaction.sender.try(:name)
                  new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                  result[:updates].push({charge_id: d[:charge_id], result: "Dispute Successfully Saved!"})
                  ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
                else
                  new_cbks_single[:result] = "#{dispute_case.errors.full_messages}"
                  new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                  new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                  new_cbks_single[:customer_name] = transaction.sender.try(:name)
                  new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                  result[:updates].push({charge_id: d[:charge_id], result: "#{dispute_case.errors.full_messages}"})
                  ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
                end
              else
                new_cbks_single[:result] = "Merchant has no sufficient Balance in wallet!"
                new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                new_cbks_single[:customer_name] = transaction.sender.try(:name)
                new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                result[:updates].push({charge_id: d[:charge_id], result: "Merchant has no sufficient Balance in wallet!"})
                ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
              end
            end
          else
            new_cbks_single[:result] = "Transaction Not Found"
            result[:updates].push({charge_id: d[:charge_id], result: "Transaction Not Found"})
            ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
          end
        else
          new_cbks_single[:result] = "Transaction Not Found"
          result[:updates].push({charge_id: d[:charge_id], result: "Transaction Not Found"})
          ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
        end
        new_cbks.push(new_cbks_single)
      end
      result_csv = CSV.generate do |csv|
        csv << ["Dispute case","Dispute type","Received date", "Due date", "Reason", "Amount", "Status", "Notes", "CBK fee","Result", "Merchant Name", "DBA", "Customer Name", "First6/Last4"]
        new_cbks.each do |c|
          csv << [c[:charge_id], c[:type],c[:received_date],c[:due_date],c[:reason],c[:amount],c[:status],c[:notes],c[:fee],c[:result],c[:merchant_name], c[:dba_name], c[:customer_name], c[:first6_last4]]
        end
      end
      file = StringIO.new(result_csv)
      qc_file = QcFile.new(name: "admin_import_bulk_cbk",start_date: Time.now, status: :bulk_cbk, user_id: user_id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "bulk_cbk.csv")
      qc_file.info = cbks_count
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'adminreport' , message: qc_file.id

    rescue => ex
      cron_job.success = false
      result[:error] = "Error: #{ex.message} for charge_id: #{id}"
      puts 'Error occured during Batch Date Update: ', ex
      ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'crash_worker' , message: "#{ex.message}"
    ensure
      cron_job.result = result
      cron_job.rake_task!
    end
  end

  def create_with_card(cbks, request, user_id, host_name)
    begin
      id = nil
      new_cbks = []
      cron_job = CronJob.new(name: "bulk_cbk",description: "Importing Bulk ChargeBack", success: true)
      result = {updates: [],  error: nil}
      cbks_count = cbks.try(:count)
      ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "0/#{cbks_count}"
      cbks.each_with_index do|d,i|
        i = i + 1
        d = d.symbolize_keys
        new_cbks_single = d.dup
        id = d[:charge_id]
        d[:amount] = d[:amount].gsub(/[^\d\.]/, '').to_f
        transactions = Transaction.where(main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard],total_amount: d[:amount], last4: d[:last4], first6: d[:first6], status: ["approved", "complete", "refunded"])
        if transactions.present? && transactions.count == 1
          transaction = transactions.last
          if transaction.present?
            if transaction.dispute_case.present?
              new_cbks_single[:result] = "Dispute Already Exist"
              new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
              new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
              new_cbks_single[:customer_name] = transaction.sender.try(:name)
              new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
              result[:updates].push({charge_id: d[:charge_id], result: "Dispute Already Exist"})
              ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
            else
              if transaction.status == "refunded"
                new_cbks_single[:result] = "Already Refunded"
                new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                new_cbks_single[:customer_name] = transaction.sender.try(:name)
                new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                result[:updates].push({charge_id: d[:charge_id], result: "Already Refunded"})
                ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
              else
                dispute_case_transaction = DisputeCaseTransaction.new
                merchant_sequence_balance = SequenceLib.balance(transaction.receiver_wallet_id)
                charge_back_fee = dispute_case_transaction.get_new_location_fee(transaction.receiver_wallet_id,TypesEnumLib::CommissionType::ChargeBack,d[:amount].to_f, nil, d[:fee] == "yes" ? true : false)
                retrievel_fee=dispute_case_transaction.get_new_location_fee(transaction.receiver_wallet_id,TypesEnumLib::CommissionType::Retrieval,d[:amount].to_f, nil, d[:fee] == "yes" ? true : false)

                if d[:amount].present? && charge_back_fee["fee"].to_f == 0
                  charge_back_total_amount = d[:amount].to_f
                elsif d[:amount].present? && charge_back_fee["fee"].present?
                  charge_back_total_amount = (d[:amount].to_f + charge_back_fee["fee"].to_f)
                else
                  charge_back_total_amount = 0
                end
                retrievel_total_amount = retrievel_fee["fee"].to_f
                if (charge_back_total_amount < merchant_sequence_balance and d[:type].downcase == "chargeback") or ( retrievel_total_amount < merchant_sequence_balance and d[:type].downcase == "retrievel" )
                  received_date = d[:received_date].split('/')
                  received_date[0],received_date[1] = received_date[1],received_date[0]
                  received_date[2] = "20#{received_date[2]}"
                  received_date = received_date.join('/')
                  due_date = d[:due_date].split('/')
                  due_date[0],due_date[1] = due_date[1],due_date[0]
                  due_date[2] = "20#{due_date[2]}"
                  due_date = due_date.join('/')
                  dispute_case = DisputeCase.create(
                      case_number: d[:charge_id],
                      user_id: 1,
                      transaction_id: transaction.id,
                      recieved_date: received_date.to_datetime,
                      due_date: due_date.to_datetime,
                      amount: d[:amount],
                      reason_id: d[:reason],
                      merchant_wallet_id: transaction.receiver_wallet_id,
                      user_wallet_id: transaction.sender_wallet_id.present? ? transaction.sender_wallet_id : transaction.receiver_wallet_id,
                      card_number: transaction.last4,
                      status: d[:status].tr('/','_').downcase,
                      dispute_type: d[:type].downcase == "chargeback" ? "charge_back" : "retrievel",
                      charge_fee: d[:fee] == "yes" ? true : false,
                      payment_gateway_id: transaction.payment_gateway_id
                  )
                  if dispute_case.save
                    Note.create(notable_type: "Transaction", notable_id: dispute_case.id, title: d[:notes])
                    if dispute_case.dispute_type == "charge_back" and (dispute_case.status == "won_reversed" or dispute_case.status == "lost" or dispute_case.status == "dispute_accepted")
                      dispute_type_charge_back_won_or_lost_on_create(dispute_case,d[:fee] == "yes" ? true : false,request)
                    else
                      dispute_type_process(dispute_case,d[:fee] == "yes" ? true : false,request)
                    end
                    new_cbks_single[:result] = "Dispute Successfully Saved!"
                    new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                    new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                    new_cbks_single[:customer_name] = transaction.sender.try(:name)
                    new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                    result[:updates].push({charge_id: d[:charge_id], result: "Dispute Successfully Saved!"})
                    ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
                  else
                    new_cbks_single[:result] = "#{dispute_case.errors.full_messages}"
                    new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                    new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                    new_cbks_single[:customer_name] = transaction.sender.try(:name)
                    new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                    result[:updates].push({charge_id: d[:charge_id], result: "#{dispute_case.errors.full_messages}"})
                    ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
                  end
                else
                  new_cbks_single[:result] = "Merchant has no sufficient Balance in wallet!"
                  new_cbks_single[:dba_name] = transaction.receiver_wallet.try(:location).try(:business_name)
                  new_cbks_single[:merchant_name] = transaction.receiver.try(:name)
                  new_cbks_single[:customer_name] = transaction.sender.try(:name)
                  new_cbks_single[:first6_last4] = "#{transaction.try(:first6) || transaction.tags.try(:[],"card").try(:[], "first6") || transaction.try(:card).try(:first6)}/#{transaction.try(:last4) || transaction.tags.try(:[],"card").try(:[], "last4") || transaction.try(:card).try(:last4)}"
                  result[:updates].push({charge_id: d[:charge_id], result: "Merchant has no sufficient Balance in wallet!"})
                  ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
                end
              end
            end
          else
            new_cbks_single[:result] = "Transaction Not Found"
            result[:updates].push({charge_id: d[:charge_id], result: "Transaction Not Found"})
            ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
          end
        elsif transactions.present? && transactions.count > 1
          new_cbks_single[:result] = "Multiple Transactions Found"
          result[:updates].push({charge_id: d[:charge_id], result: "Transaction Not Found"})
          ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
        else
          new_cbks_single[:result] = "Transaction Not Found"
          result[:updates].push({charge_id: d[:charge_id], result: "Transaction Not Found"})
          ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'circulation' , message: "#{i}/#{cbks_count}"
        end
        new_cbks.push(new_cbks_single)
      end
      result_csv = CSV.generate do |csv|
        csv << ["Dispute case","Dispute type","Received date", "Due date", "Reason", "First6", "Last4", "Amount", "Status", "Notes", "CBK fee","Result", "Merchant Name", "DBA", "Customer Name", "First6/Last4"]
        new_cbks.each do |c|
          csv << [c[:charge_id], c[:type],c[:received_date],c[:due_date],c[:reason],c[:first6], c[:last4],c[:amount],c[:status],c[:notes],c[:fee],c[:result], c[:merchant_name], c[:dba_name], c[:customer_name], c[:first6_last4]]
        end
      end
      file = StringIO.new(result_csv)
      qc_file = QcFile.new(name: "admin_import_bulk_cbk",start_date: Time.now, status: :bulk_cbk, user_id: user_id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "bulk_cbk.csv")
      qc_file.info = cbks_count
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'adminreport' , message: qc_file.id

    rescue => ex
      cron_job.success = false
      result[:error] = "Error: #{ex.message} for charge_id: #{id}"
      puts 'Error occured during Batch Date Update: ', ex
      ActionCable.server.broadcast "cbk_channel_#{host_name}", channel_name: "cbk_channel_#{host_name}", action: 'crash_worker' , message: "#{ex.message}"
    ensure
      cron_job.result = result
      cron_job.rake_task!
    end
  end

  def dispute_type_process(dispute_case,check_for_fee=nil,request=nil)
    dispute_case_transaction = DisputeCaseTransaction.new
    charge_back_wallet = Wallet.charge_back.last
    if dispute_case.present? and charge_back_wallet.present?
      if dispute_case.charge_back? and dispute_case.sent_pending?
        dispute_case_transaction.transfer_charge_back_sent(dispute_case,nil,check_for_fee)
      elsif dispute_case.retrievel?
        dispute_case_transaction.transfer_retreivel_fee(dispute_case,check_for_fee)
      elsif dispute_case.charge_back? and dispute_case.won_reversed?
        dispute_case_transaction.transfer_charge_back_won(dispute_case)
      elsif dispute_case.charge_back? and (dispute_case.lost? || dispute_case.dispute_accepted?)
        dispute_case_transaction.transfer_charge_back_lost(dispute_case,request)
      end
    end
  end

  def dispute_type_charge_back_won_or_lost_on_create(dispute_case,check_for_fee=nil,request=nil)
    dispute_case_transaction = DisputeCaseTransaction.new
    if dispute_case.present?
      if dispute_case.charge_back? and dispute_case.won_reversed?
        dispute_case_transaction.transfer_charge_back_sent(dispute_case,"charge_back_sent_plus_fee",check_for_fee)
        dispute_case_transaction.transfer_charge_back_won(dispute_case)
      elsif dispute_case.charge_back? and (dispute_case.lost? || dispute_case.dispute_accepted?)
        dispute_case_transaction.transfer_charge_back_sent(dispute_case,nil,check_for_fee)
        dispute_case_transaction.transfer_charge_back_lost(dispute_case,request)
      end
    end
  end

end
