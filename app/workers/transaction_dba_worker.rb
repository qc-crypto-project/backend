class TransactionDbaWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(*args)
    store new_status: 'queued'
    puts "Start updating"
    begin
      job = CronJob.new(name: 'TransactionReceiverName', description: 'Updating Transaction receiver Name', success: true)
      object = {updates: [], error: nil}
      first_date = "2020-6-1"
      second_date = "2020-10-1"
      Transaction.includes(:receiver_wallet).where(created_at: first_date..second_date, main_type: ["eCommerce","Virtual Terminal","PIN Debit","Credit Card","Debit Card", "QCP Secure", "QC Secure"], status: ["approved", "refunded","complete"]).each_slice(1000) do |batch|
        transaction_update=[]
        batch.each do |transaction|
          if transaction.receiver_wallet_id.present?
            location_dba_name = transaction.receiver_wallet.try(:location).try(:business_name)
            if location_dba_name.present? && transaction.receiver_name != location_dba_name
              transaction.receiver_name = location_dba_name
              transaction_update.push(transaction)
              object[:updates].push(transaction.id)
            end
          end
          if transaction_update.present?
            Transaction.import transaction_update, on_duplicate_key_update: {conflict_target: [:id], columns: [:receiver_name]}
          end
          job.result = object
          job.rake_task!
          sleep(1)
        end
      end
      puts 'Updated Successfully'
    rescue StandardError => exc
      job.success = false
      object[:error] = "Error: #{exc.message}"
      puts 'Error occured during transfer Update: ', exc
    end
    job.result = object
    job.rake_task!
  end
end


