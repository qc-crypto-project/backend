class BalanceUpdateWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'critical', :retry => 3, backtrace: 8

  def perform(*args)
    store new_status: 'queued'
    wallets = Wallet.all
    wallets.each do |wallet|
      puts "wallet---> #{wallet.id}"
      puts "old balance----> #{wallet.balance}"
      balance = SequenceLib.balance(wallet.id)
      puts "sequence balalnce----> #{balance}"
      wallet.update(balance: balance)
      puts "new_balance -----> #{wallet.balance}"
    end
    store new_status: 'completed'
  end
end
