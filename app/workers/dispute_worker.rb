class DisputeWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include Merchant::TransactionsHelper
  include Merchant::ReportsHelper
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8
  
  def perform(first_date, second_date, user_id = nil)

    begin
      count=1
      DisputeCase.charge_back.where("recieved_date BETWEEN :first AND :second",first: first_date.to_date, second: second_date.to_date).order(recieved_date: :desc).find_in_batches(batch_size: 5000) do |disputes|
      puts "Batch# ", count
      if disputes.present?
        report_first_date = first_date.to_date
        report_second_date = second_date.to_date
        @report = Report.new(user_id: user_id, first_date: report_first_date, second_date: report_second_date, offset: nil, report_type: "CBK Detail report", status: "pending",read: false)
        @report.save
        wallets = Wallet.where(id: disputes.pluck(:merchant_wallet_id).uniq).select("wallets.id AS id","wallets.location_id AS location_id").group_by{|e| e.id}
        locations = Location.joins(:wallets,:merchant).where("wallets.wallet_type = ?",0).where(id: wallets.values.flatten.pluck(:location_id).uniq.compact).select("locations.id AS id", "locations.business_name AS business_name","wallets.id AS wallet_id", "locations.category_id as category_id","users.name as name", "users.id as user_id").group_by{|e| e.wallet_id}
        categories = Category.where(id: locations.values.flatten.pluck(:category_id).uniq.compact).select("id as id, name as name").group_by{|e| e.id}
        reasons = Reason.where(id: disputes.pluck(:reason_id).uniq).select("id AS id", "title AS title").group_by{|e| e.id}
        transactions = Transaction.where(id: disputes.pluck(:transaction_id).flatten.compact.uniq).select("id AS id", "seq_transaction_id AS seq_transaction_id","total_amount AS total_amount","fee AS fee","created_at as created_at", "timestamp as timestamp", "card_id as card_id", "tags as tags").group_by{|e| e.id}
        gateways = PaymentGateway.where(id: disputes.pluck(:payment_gateway_id).uniq.compact).select("id as id", "name as name").group_by{|e| e.id}
        # file = File.new("#{Rails.root}/tmp/dispute_report#{count}.csv", "w")
        report_csv = CSV.generate do |csv|
          csv << ['Case Number','Customer Name','Merchant ID', 'Merchant Name', 'DBA Name','DBA Category', 'Received Date','Created Date', 'CBK Amount','Due Date', 'Description', 'Original Transaction Date','Original Txn Amount', 'Original Txn ID', 'Bank Descriptor', 'Status', 'Card Brand']

          disputes.each do |d|
            # if d.charge_fee == true
            #   charge_back_fee = locations[d.merchant_wallet_id].try(:first).try(:charge_back_fee)
            # end
            # merchant = wallets[d.merchant_wallet_id].try(:first)
            user = Wallet.where(id: d.user_wallet_id).try(:first).try(:users).try(:first)
            transaction = transactions[d.transaction_id].try(:first)
            # if d.try(:card_number).present?
            #   last4 = d.try(:card_number)
            # else
            #   last4 = transaction.try(:last4)
            # end

            # if transactions[d.chargeback_transaction_id].try(:first).try(:seq_transaction_id).present?
            #   tx_id = transactions[d.chargeback_transaction_id].try(:first).try(:seq_transaction_id)
            # else
            #   transaction = Transaction.where("transactions.tags -> 'dispute_case' ->> 'case_number' = ? and  transactions.status = ?", "#{d.try(:case_number)}","Chargeback").try(:first)
            #   tx_id = transaction.try(:seq_transaction_id)
            #   if last4.blank?
            #     last4 = transaction.try(:tags).try(:[],"dispute_case").try(:[],"last4")
            #   end
            # end
            location = locations[d.merchant_wallet_id].try(:first)
            if location.present?
              dba_name = location.try(:business_name).gsub(",", " ")
            end
            if transaction.card.present?
              brand = transaction.card.brand
            else
              brand = transaction.tags["card"].present? ? transaction.tags["card"].try(:[], "brand") : transaction.tags["previous_issue"].try(:[], "tags").try(:[], "card").try(:[], "brand")
            end
            csv << [
                    "#{d.try(:case_number)}",
                    user.try(:name).present? ? user.try(:name) : user.try(:first_name),
                    location.try(:user_id),
                    location.try(:name),
                    dba_name,
                    categories[location.try(:category_id)].try(:first).try(:name),
                    d[:recieved_date].nil? ? 'NA' : d[:recieved_date].strftime("%m-%d-%Y"),
                    d[:created_at].nil? ? 'NA' : d[:created_at].to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                    number_to_currency(number_with_precision(d.try(:amount).to_f,precision: 2, :delimiter => ',')),
                    d[:due_date].nil? ? 'NA' : d[:due_date].strftime("%m-%d-%Y"),
                    reasons[d.try(:reason_id)].try(:first).try(:title)  ,
                    transaction.try(:timestamp).present? ? transaction.try(:timestamp).to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : "N/A",
                    number_to_currency(number_with_precision(transaction.try(:total_amount).to_f,precision: 2, :delimiter => ',')),
                    transaction.try(:seq_transaction_id),
                    gateways[d.payment_gateway_id].try(:first).try(:name),
                    d.status,
                    brand
            ]

          end
        end
        file = StringIO.new(report_csv)
        qc_file = QcFile.new(name: "cbk detail report #{count}", start_date: first_date.to_date, end_date: second_date.to_date, status: :cbk_detail)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "cbk_detail_#{count}.csv")
        qc_file.save
        @report.file_id = {id: qc_file.id}
        @report.status = "completed"
        @report.message = "done"
        @report.save
        # file.close
        # SlackService.upload_files("Disputes-#{count}", file, "#qc-files", "Dispute export")
      end
      count = count + 1
    end
    rescue => exc
      puts 'Dispute Worker Crashed: ', exc.message
      puts "backtrace: ", exc.backtrace.first(8)
      @report.status = "crashed"
      @report.message = exc.message
    ensure
      @report.save
    end
  end

end
