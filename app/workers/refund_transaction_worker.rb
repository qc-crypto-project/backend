class RefundTransactionWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(qr_id, fee=true)
    description = []
    begin
      cron_job = CronJob.new(name: "refund transaction",log_type: "refund_task")
      QrCard.where(id: qr_id).each do |q|
        puts "--------------->",q.id
        qr_card = q
        if qr_card.present? && qr_card.request.status == "in_progress"
          issue_transaction = qr_card.card_transaction
          if issue_transaction.present?
            refund = RefundHelper::RefundTransaction.new(issue_transaction.seq_transaction_id,issue_transaction,nil,issue_transaction.total_amount,"auto refund after 60 mins", issue_transaction.receiver,fee,qr_card.request)
            if issue_transaction.tags.try(:[], "api_type").present? && issue_transaction.tags.try(:[], "api_type") == "qr_code"
              type = "credit"
              response = refund.refund_sale_issue
            elsif issue_transaction.sub_type == TypesEnumLib::TransactionSubType::QRDebitCard
              type = "debit"
              response = refund.refund_debit_issue
            end
            refund_params = {}
            refund_params[:seq_transaction_id] = issue_transaction.id
            refund_params[:user] = issue_transaction.receiver
            refund_params[:type] = "admin"
            refund_params[:response] = response
            refund_params[:qr_id] = qr_card.id
            refund_params[:request_id] = qr_card.request.id
            activity_id = record_refund_transaction(refund_params, true)
            request = qr_card.request
            if response[:success] == true
              old_type = request.status.dup
              request.update(status: "refunded", activity_id: activity_id)
              request.create_qr_batch(type,old_type)
              qr_card.update(is_valid: false)
              description.push({message: response[:message], qr_id: qr_id})
            else
              request.update(activity_id: activity_id)
              description.push({message: response[:message], qr_id: qr_id})
            end
          else
            description.push({message: "no transaction found", qr_id: qr_id})
          end
        else
          description.push({message: "no qr code found/already refunded", qr_id: qr_id})
        end
      end
    rescue StandardError => exc
      description.push({message: exc.message, backtrace: exc.backtrace.first(8), qr_id: qr_id})
    ensure
      cron_job.description = description
      cron_job.refund_task!
    end
  end
end
