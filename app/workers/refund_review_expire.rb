class RefundReviewExpire
	include Sidekiq::Worker
	include Sidekiq::Status::Worker
	sidekiq_options queue: 'default'

	def perform(*args)	
		job = CronJob.new(name: "refund_review_expire",description: "refund review expire , system auto refund")
		result = {updated_records: [] , not_updated_records: [],error: nil}
		begin
			allowed_days = REFUND_ALLOWED_DAYS || 6
			date = HoldInRear.pst_beginning_of_day(DateTime.now) - allowed_days.days
			customer_disputes = CustomerDispute.where(dispute_type: "refund").where(status: "pending")
			customer_disputes.each do |customer_dispute|
				dispute_request = customer_dispute.dispute_requests.where(user_id: customer_dispute.customer_id).where(status: "pending").where(request_type: "initial_request").where("updated_at <= ? " , date).last
				if dispute_request
					transaction = customer_dispute.dispute_transaction
					if transaction.status != "refunded"
						merch = transaction.receiver
						refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Cancel review expired", merch)
			      		response = refund.process
			      		if response[:success] == true
					        customer_dispute.update(status: "approved")
					        customer_dispute.dispute_requests.update(status: "refunded",title: "Refunded (By Merchant)")
					        result[:updated_records] << {id: customer_dispute.id, message: "after refund dispute close"}
					    else
					        result[:not_updated_records] << {id: customer_dispute.id, message: response[:message]}
					    end
					end
				end
			end
			job.result = result
			job.refund_review_expire!		
		rescue Exception => e
			result[:error] = e
			job.refund_review_expire!	
		end
	end
end