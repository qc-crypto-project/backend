class PaymentGatewayVolumeWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'default', :retry => 1, backtrace: 8

  def perform(*args)
    puts '================>>  Updating...'
    PaymentGateway.main_gateways.each do |payment_gateway|
      date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").at_beginning_of_month
      d_date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day
      monthly_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
      daily_amount_limit = payment_gateway.transactions.where("transactions.created_at >=? and transactions.status IN (?) and transactions.main_type IN (?)",d_date,["complete","approved","refunded"],[TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::Issue3DS,TypesEnumLib::TransactionViewTypes::CreditCard]).sum(:total_amount)
      puts "monthly_amount_limit"
      puts monthly_amount_limit
      puts "daily_amount_limit"
      puts daily_amount_limit
      payment_gateway.daily_volume_achived = daily_amount_limit
      payment_gateway.monthly_volume_achived = monthly_amount_limit
      payment_gateway.total_transaction_count = payment_gateway.transactions.count
      payment_gateway.total_cbk_count = DisputeCase.where(payment_gateway_id: payment_gateway.id).where('created_at >= :date', date: date).count
      payment_gateway.save

      puts "saved---monthly--vol"
      puts payment_gateway.monthly_volume_achived
      puts "saved---daily--vol"
      puts payment_gateway.daily_volume_achived


    end
    puts '================>>  Updated Merchants gateway...'
  end
end
