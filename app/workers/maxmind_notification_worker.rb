class MaxmindNotificationWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8
  include ActionView::Helpers::NumberHelper

  def perform(*args)
    store new_status: 'queued'
    allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5

    MinfraudResult.joins(:main_transaction).where("minfraud_results.qc_action = ? AND transactions.status = ?", TypesEnumLib::RiskType::PendingReview,"approved").find_each do |minfraud|
      expiry = minfraud.created_at + allowed_days.days
      if minfraud.created_at <= DateTime.now.utc && expiry >= DateTime.now.utc
        transaction_id = minfraud.transaction_id
        transaction = Transaction.find(transaction_id)
        days_left = (expiry.to_date - DateTime.now.to_date).to_i
        txn_id = transaction.try(:seq_transaction_id)[0..5]
        merchant = transaction.receiver
        amount= "#{'%.02f' % transaction.try(:amount)}"
        text = I18n.t('notifications.risk_review', txn_id: txn_id, amount: amount, days: days_left)
        Notification.notify_user(merchant,merchant,transaction,"manual review transaction",nil,text) if (days_left > 0 && days_left != allowed_days)
      end

    end

    store new_status: 'completed'
  end
end


