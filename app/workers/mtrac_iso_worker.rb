class MtracIsoWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include Merchant::TransactionsHelper
  include Merchant::ReportsHelper
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(wallet_id, first_date, second_date, folder_name, count_number = nil)
    begin
      s3 = Aws::S3::Resource.new(
          credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],ENV['AWS_SECRET_ACCESS_KEY']),
          region: ENV['AWS_REGION']
      )
      if count_number.present?
        count = count_number
      else
        count = 0
      end
      Dir.mkdir("#{Rails.root}/tmp/#{folder_name}")
      #user = Wallet.where(id: wallet_id).first.try(:users).try(:first)
      # if first_date.blank? && second_date.blank?
      #   first_date = Time.new(2020,01,01,0,0,0).in_time_zone("Pacific Time (US & Canada)")
      #   second_date = Time.new(2020,04,15,23,59,59).in_time_zone("Pacific Time (US & Canada)")
      # end
      limit = 20000
      offset = 0
      while offset >= 0
        block = BlockTransaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date).where(main_type: ["Refund","Refund Bank"]).order(timestamp: :asc).offset(offset).limit(limit)
        if block.present?
          offset = offset + limit
          file = File.new("#{Rails.root}/tmp/#{folder_name}/report#{count}.csv", "w")
          file << CSV.generate do |csv|
            csv << ["Transaction ID", "date/time", "main type", "sender name", "receiver name", "sender wallet", "receiver wallet", "total amount", "TXn amount","fee", "net fee", "total net", "first 6", "last4","descriptor"]

            block.each do |tx|
              # if tx.receiver_name.blank?
              #   receiver_user = tx.receiver.try(:name)
              # end
              #
              # main_amount = 0
              # main_amount = tx.main_amount
              # main_amount = tx.tags.try(:[],"main_transaction_info").try(:[],"amount") if main_amount.blank? || main_amount.to_f == 0
              # # main_amount = SequenceLib.dollars(tx.parent_transaction.try(:amount_in_cents)) if main_amount.blank? || main_amount.to_f == 0
              # # main_amount_amount = SequenceLib.dollars(BlockTransaction.where(seq_parent_id: tx.seq_block_id, position: '0').first.try(:amount_in_cents)) || 0 if tx.seq_block_id.present? && (main_amount.blank? || main_amount.to_f == 0)
              #
              # if tx.location_dba_name.blank?
              #   locationName = tx.tags.try(:[],"location").try(:[],"business_name")
              # else
              #   locationName = tx.location_dba_name
              # end
              # if tx.merchant_name.blank?
              #   merchantName = tx.tags.try(:[],"merchant").try(:[],"name")
              # else
              #   merchantName = tx.merchant_name
              # end
              # if tx.sender_wallet_id == wallet_id
              #   sign = "-"
              # elsif tx.receiver_wallet_id == wallet_id
              #   sign = "+"
              # end
              # first6 = tx.first6
              # last4 = tx.last4
              # if first6.blank? || last4.blank?
              #   card = tx.card
              #   first6 = card.try(:first6)
              #   last4 = card.try(:last4)
              # end
              # main_amount = tx.main_amount.to_f > 0 ? tx.main_amount : tx.tags.try(:[],"main_transaction_info").try(:[], "amount").to_f
              csv << [tx.sequence_id,
                      tx.timestamp.present? ? tx.timestamp.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : tx.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                      tx.main_type,
                      tx.sender_name,
                      tx.receiver_name,
                      tx.sender_wallet_id,
                      tx.receiver_wallet_id,
                      "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.amount_in_cents), precision: 2,delimiter: ','))}",
                      "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.tx_amount), precision: 2,delimiter: ','))}",
                      "#{number_to_currency(number_with_precision(SequenceLib.dollars(tx.fee_in_cents), precision: 2,delimiter: ','))}",
                      "#{number_to_currency(number_with_precision(tx.net_fee.to_f, precision: 2,delimiter: ','))}",
                      "#{number_to_currency(number_with_precision(tx.total_net.to_f, precision: 2,delimiter: ','))}",
                      tx.first6,
                      tx.last4,
                      tx.gateway.present? ? tx.gateway : tx.payment_gateway.try(:name)

              ]
            end
          end
          file.close
          obj = s3.bucket('audit-exports').object("#{folder_name}/report#{count}.csv")
          obj.upload_file("#{Rails.root}/tmp/#{folder_name}/report#{count}.csv")
        else
          puts "No Transaction found"
          offset = -1
        end
        count = count + 1
      end
    end
    rescue => exc
      puts 'Mtrac Worker Crashed: ', exc.message
      puts "backtrace: ", exc.backtrace.first(8)
    end
end
