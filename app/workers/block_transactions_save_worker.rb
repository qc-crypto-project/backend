class BlockTransactionsSaveWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper
  sidekiq_options queue: 'default', :retry => 3, backtrace: 8

  def perform(id,timestamp=nil)
    begin
      seq_transaction = SequenceLib.get_transaction_id(id)
      transactions = parse_block_transactions(seq_transaction.first.actions, seq_transaction.first.timestamp)
      save_block_trans(transactions) if transactions.present?
    rescue => ex
      SlackService.notify("*Error in BlockTransactionsSaveWorker => #{ex.message}*")
    end
  end

end
