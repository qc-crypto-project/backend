class ExportFileWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper
  include Merchant::ChecksHelper
  include Merchant::TransactionsHelper
  include Merchant::ReportsHelper
  include AdminsHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8
  def perform(user_id, first_date, second_date, type, offset, wallet, send_via, search_query,date=nil, status = nil, trans = nil, admin_user_id=nil, specific_wallet_id=nil, session_id = nil,email=nil,batch_id=nil, params = nil )
    # start if need to disable exports uncomment this code
    # ActionCable.server.broadcast "report_channel_#{user_id}", channel_name: "report_channel_#{user_id}", action: 'error' , message: "Sorry for inconvenience exports are currently disabled"
    # return false
    # end
    @host_name = session_id
    user = User.find(user_id)
    if user.present? && !user.admin?
      if type != TypesEnumLib::WorkerType::SendCheck && type != TypesEnumLib::WorkerType::InstantPAY && type != TypesEnumLib::WorkerType::InstantACH && type !=TypesEnumLib::WorkerType::PermissionExport && type !=TypesEnumLib::WorkerType::EmployeeExport
        if (first_date.present? && second_date.present?) && (second_date.to_datetime - first_date.to_datetime).to_i > 90
          return ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Date range search exceeds export limit, please search for 1-90 days before trying to export again. Thank you!"
        end
      end
    elsif user.present? && user.admin?
      if (first_date.present? && second_date.present?) && (second_date.to_datetime - first_date.to_datetime).to_i > 90 && type != TypesEnumLib::WorkerType::IsoSummaryReport
        return ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "You cannot run export more than 90 days."
      end
    end
    case type
    when TypesEnumLib::WorkerType::BatchTransactions
      batch_transactions_export(user,first_date,second_date,wallet, offset,batch_id, trans, params, email)
    when TypesEnumLib::WorkerType::SaleBatches
      sale_batches_export(user,first_date,second_date,wallet, offset)
    when TypesEnumLib::WorkerType::Transactions
      export_transactions(user, first_date, second_date, offset, search_query,wallet,trans,type,email,params)
    when TypesEnumLib::WorkerType::Refund
      export_refunds(user, first_date, second_date, offset)
    when TypesEnumLib::WorkerType::PermissionExport
      export_permission(user, first_date, second_date, offset)
    when TypesEnumLib::WorkerType::AddMoney
      export_add_money(user, first_date, second_date, offset)
    when TypesEnumLib::WorkerType::RequestMoney
      export_request_money(user, first_date, second_date, offset,type)
    when TypesEnumLib::WorkerType::Transfer
      #for sales report
      if user.present? && user.role.present? &&(user.iso? || user.agent? || user.affiliate? || user.partner? || user.qc?)
        export_iso_agent_report(user, first_date, second_date, wallet, offset)
      else
        export_transfer_report(user, first_date, second_date, wallet, offset, status)
      end
    when TypesEnumLib::WorkerType::Retire
      #for sendcheck reports
      export_checks_report(user, first_date, second_date, wallet, send_via, offset)
    when TypesEnumLib::WorkerType::Giftcard
      export_giftcard_report(user, first_date, second_date, wallet, offset)
    when TypesEnumLib::WorkerType::SendCheck, TypesEnumLib::WorkerType::InstantACH, TypesEnumLib::WorkerType::InstantPAY
      export_send_check(user, first_date, second_date, offset, type, email,params)
    when TypesEnumLib::WorkerType::BulkCheck
      export_bulk_check(user, first_date, second_date, offset)
    when TypesEnumLib::WorkerType::Dispute
      export_dispute(user, first_date, second_date, offset, search_query,type)
    when TypesEnumLib::WorkerType::MerchantDispute
      export_merchant_dispute(user, first_date, second_date, offset, search_query,type)
    when TypesEnumLib::WorkerType::MerchantExport
      export_merchant(user,search_query,type)
    when TypesEnumLib::WorkerType::MerchantUsersExport
      export_merchant_user(user,search_query,type)
    when TypesEnumLib::WorkerType::LocationExport
      export_location(user,search_query,type)
    when TypesEnumLib::WorkerType::WalletExport
      wallet_export(user,wallet,send_via,first_date,second_date,offset)
    when TypesEnumLib::WorkerType::EmployeeExport
      employee_export(user,wallet,send_via,first_date,second_date,offset)
    when TypesEnumLib::WorkerType::IsoSummaryReport
      export_iso_summary(user, first_date, second_date, offset, search_query,type,@host_name)
    when TypesEnumLib::WorkerType::HoldRearExport
      export_funding_transactions(user, first_date, second_date, offset, search_query,type,wallet)
    when TypesEnumLib::WorkerType::RserveReleaseExport
      export_funding_transactions(user, first_date, second_date, offset, search_query,type,wallet)
    when TypesEnumLib::WorkerType::Admin, TypesEnumLib::WorkerType::AdminUsers, TypesEnumLib::WorkerType::AdminMerchant, TypesEnumLib::WorkerType::AdminMerchantProfile
      export_admin_transactions(user, first_date, second_date, wallet, type, offset, search_query, date, trans, admin_user_id, specific_wallet_id,send_via)
    when TypesEnumLib::WorkerType::MerchantLocationDispute
      export_location_dispute(user, first_date, second_date, offset, search_query,type,wallet)
    end
  end

  private
  def batch_transactions_export(user,first_date,second_date,wallet,offset,batch_id, trans, params, email)
    begin
      if params.present?
        params = eval(params)
      end
      reports_cron = CronJob.new(name: "batch_transactions_export",description: "Export batch transactions for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}")
      reports_cron.processing!
      result = { user_id: user.try(:id), start_date: params[:batch_date].to_date, end_date: params[:batch_date].to_date,  offset: offset, wallet_id: wallet,batch_id: batch_id}
      report = Report.create(name: "Batch Transactions",user_id: user.try(:id), first_date: params[:batch_date].to_date, second_date: params[:batch_date].to_date, offset: offset, report_type: TypesEnumLib::WorkerType::BatchTransactions, transaction_type: trans, status: "pending",read: false)
      if user.merchant?
        batch = TransactionBatch.where(id: batch_id).first
        transactions = batch.batch_transactions.where(timestamp: first_date.to_datetime.utc..second_date.to_datetime.utc)
        if transactions.present?
          report.user_id = user.merchant_id if user.merchant_id.present?
          report.save
          transactions_csv = CSV.generate do |csv|
            csv << ['Txn ID', 'Date / Time', 'Receiver Name', 'Sender Name', 'Type', 'Descriptor', 'Card Number','Total Amount']
            transactions.each do |t|
              csv << [t.seq_transaction_id.present? ? t.seq_transaction_id[0..5] : nil,t.timestamp.present? ? t.timestamp.strftime("%m-%d-%Y %H:%M:%S") : t.created_at.strftime("%m-%d-%Y %H:%M:%S"),t.receiver_name.present? ? t.receiver_name : t.receiver.try(:name).present? ? t.receiver.try(:name) :  '',t.sender_name.present? ? t.sender_name : t.sender.try(:name).present? ? t.sender.try(:name) :  '' ,
                      main_type_format(show_local_type(t)),show_descriptor(t),t.last4.present? ? "•••• #{t.last4}" : "---",number_with_precision(number_to_currency(t.total_amount.to_f), precision: 2, delimiter: ',')]
            end
          end
        end
      elsif user.iso? || user.agent? || user.affiliate?
        wallets = user.try(:wallets).try(:primary).pluck(:id)
        if params[:search_query].present?
          params[:search_query][:batch_date] = params[:batch_date]
          params = params[:search_query]
          report.params = params
          report.save
          params = params.reject{|key,value| value.empty?}
          params["offset"] = offset
          transactions = searching_iso_local_transaction_for_batch_export(params.symbolize_keys, wallets, first_date, second_date, user, offset, true,@host_name,email, trans) if params.present?
          transactions =  trans == "commission" ? transactions.where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "Buy rate fee"]) : transactions.where.not(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "Buy rate fee"])
        else
          if trans == "commission"
            transactions = BlockTransaction.where('(receiver_wallet_id = :wallet OR sender_wallet_id = :wallet) AND timestamp BETWEEN :first AND :second', wallet: wallet, first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee"]).order(timestamp: :desc)
          else
            transactions = BlockTransaction.where("(receiver_wallet_id = :wallets OR sender_wallet_id = :wallets) AND timestamp BETWEEN :first AND :second", wallets: wallet, first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).where.not(main_type:["Fee Transfer"]).order(timestamp: :desc)
          end
        end
        if transactions.present?
          report.user_id = user.id
          report.save
          transactions_csv = CSV.generate do |csv|
            if trans == "commission"
              csv << ['Txn ID', 'Date / Time', 'Merchant', 'DBA', 'Transaction Type', 'Transaction Amount', 'Commission','Receiver','Merchant TXN Type']
            elsif trans == "personal_transaction"
              csv << ["Txn ID", "Date / Time", "Sender", "Receiver", "Type", "Total Amount"]
            end
            transactions.each do |t|
              if t.main_type == "Subscription fee"
                txn_type = show_iso_local_type(t)
              elsif t.main_type == "Service Fee"
                "Monthly Fee"
              else
                txn_type = main_type_format(t.main_type)
                if txn_type == "Push to Card"
                  t.action == "retire"? "Push To Card Deposit" : "Push To Card"
                elsif txn_type == "Send Check"
                  t.action == "retire"? "Check Deposit" : "Send Check"
                else
                  txn_type
                end
              end
              if t.main_amount.to_f.present? && t.net_fee.to_f.present? && t.sub_type == "ACH Deposit_fee"
                tx_amount = t.main_amount.to_f + t.net_fee.to_f
              else
                tx_amount = t.main_amount.to_f > 0 ? t.main_amount.to_f : cents_to_dollars(t.amount_in_cents)
              end
              if ["Send Check", "ACH Deposit", "Instant Pay"].include? t.main_type
                commission =  "--"
              elsif user.try(:id) == t.receiver_id
                commission = "+$#{number_with_precision(cents_to_dollars(t.amount_in_cents), precision: 2, delimiter: ",")}"
              else
                commission = "-$#{number_with_precision(cents_to_dollars(t.amount_in_cents), precision: 2, delimiter: ",")}"
              end
              if t.try(:receiver_name).present?
                receiver = change_wallets_name(t.receiver_name.truncate(24) || t.try(:receiver).try(:name).truncate(24) || t.receiver_wallet.try(:users).try(:first).try(:name).truncate(24) || t.receiver_wallet.try(:name).truncate(24) || "---" )
              else
                receiver = change_wallets_name(t.receiver_name || t.try(:receiver).try(:name) || t.receiver_wallet.try(:users).try(:first).try(:name) || t.receiver_wallet.try(:name) || "" )
              end
              if trans == "personal_transaction"
                if t.try(:sender_name).present?
                  sender = change_wallets_name(t.sender_name.truncate(24) || t.try(:sender).try(:name).truncate(24) || t.sender_wallet.try(:users).try(:first).try(:name).truncate(24) || t.sender_wallet.try(:name).truncate(24) || "---" )
                else
                  sender = change_wallets_name(t.sender_name || t.try(:sender).try(:name) || t.sender_wallet.try(:users).try(:first).try(:name) || t.sender_wallet.try(:name) || "" )
                end
              end
              if trans == "commission"
                merchant_txn_main_type = ''
                if t.tags.present? && t.tags["main_transaction_info"].present? && t.tags["main_transaction_info"]["transaction_type"] == "sale" && t.tags["main_transaction_info"]["transaction_sub_type"] == "virtual_terminal_sale_api"
                  merchant_txn_main_type = 'eCommerce'
                elsif t.tags.present? && t.tags["main_transaction_info"].present?
                  merchant_txn_main_type = t.tags["main_transaction_info"]["transaction_sub_type"].present? ? main_type_format(t.tags["main_transaction_info"]["transaction_sub_type"]) : t.tags["main_transaction_info"]["transaction_type"].present? ? main_type_format(t.tags["main_transaction_info"]["transaction_type"]) : "--"
                else
                  merchant_txn_main_type = t.tx_type.present? ?  main_type_format(t.tx_type) : t.tx_sub_type.present? ?  main_type_format(t.tx_sub_type) : "--"
                end

                csv << [
                    t.sequence_id.present? ? t.sequence_id[0..5] : nil,
                    t.timestamp.present? ? t.timestamp.to_datetime.new_offset(offset).strftime("%m-%d-%Y %H:%M:%S %p") : t.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                    t.try(:[],:tags).try(:[],"merchant").try(:[],"name").present? ? t.try(:[],:tags).try(:[],"merchant").try(:[],"name") : "--",
                    t.location_dba_name || t.tags.try(:[], "location").try(:[], "business_name") || "--",
                    txn_type,
                    number_with_precision(number_to_currency(tx_amount.to_f), precision: 2, delimiter: ','),
                    commission,
                    receiver, merchant_txn_main_type]
              elsif trans == "personal_transaction"
                csv << [
                    t.sequence_id.present? ? t.sequence_id[0..5] : nil,
                    t.timestamp.present? ? t.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : t.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                    sender,
                    receiver,
                    txn_type,
                    number_with_precision(number_to_currency(SequenceLib.dollars(t.amount_in_cents.to_f)), precision: 2, delimiter: ','),
                ]
              end
            end
          end
        end
      end
      if transactions.present?
        report.status = "completed"
        report.message = "done"
        file = StringIO.new(transactions_csv)
        qc_file = QcFile.new(name: "batch_transactions_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "batch_transactions_export.csv")
        qc_file.save
        report.file_id = qc_file.id
        UserMailer.send_export_notification(user).deliver_later
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end
  end


  def sale_batches_export(user,first_date,second_date,wallet,offset)
    begin
      reports_cron = CronJob.new(name: "export_sale_batches",description: "Export sale batches for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}")
      reports_cron.processing!
      result = { user_id: user.try(:id), start_date: first_date, end_date: second_date,  offset: offset, wallet_id: wallet}
      if offset.present?
        zone = ActiveSupport::TimeZone[offset.to_i].name
        actual_first_date = first_date.in_time_zone(zone).strftime('%m/%d/%Y') if first_date.present?
        actual_first_date = Date.strptime(actual_first_date, '%m/%d/%Y')
        actual_second_date = second_date.in_time_zone(zone).strftime('%m/%d/%Y') if second_date.present?
        actual_second_date = Date.strptime(actual_second_date, '%m/%d/%Y')
      else
        actual_first_date = first_date.in_time_zone("Pacific Time (US & Canada)").strftime("%m-%d-%Y") if first_date.present?
        actual_first_date = Date.strptime(actual_first_date, '%m/%d/%Y')
        actual_second_date = second_date.in_time_zone("Pacific Time (US & Canada)").strftime("%m-%d-%Y") if second_date.present?
        actual_second_date = Date.strptime(actual_second_date, '%m/%d/%Y')
      end
      report = Report.create(name: "Sale Batches",user_id: user.try(:id), first_date: first_date, second_date: second_date, offset: offset, report_type: TypesEnumLib::WorkerType::SaleBatches, status: "pending",read: false)
      if user.merchant?
        batches = TransactionBatch.where(merchant_wallet_id: wallet).where("batch_date BETWEEN :first AND :second", first: actual_first_date, second: actual_second_date).where(batch_type: "sale")
        if batches.present?
          report.user_id = user.merchant_id if user.merchant_id.present?
          report.save
          transactions_csv = CSV.generate do |csv|
            csv << ['Date', 'Starting Balance', 'Transactions', 'Gross Earnings', 'Processing Fee', 'Reserve', 'Expenses', 'Net Earnings']
            batches.each do |batch|
              csv << [batch.batch_date.strftime("%m/%d/%Y"), number_to_currency(number_with_precision(batch.starting_balance.to_f,precision: 2, delimiter: ',')), batch.total_trxs.to_i,number_to_currency(number_with_precision(batch.total_amount.to_f,precision: 2, delimiter: ',')),
                      number_to_currency(number_with_precision(batch.total_fee.to_f,precision: 2, delimiter: ',')),"-#{number_to_currency(number_with_precision(batch.reserve.to_f,precision: 2, delimiter: ','))}",number_to_currency(number_with_precision(batch.expenses.to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch.total_net.to_f,precision: 2, delimiter: ','))]
            end
          end
        end
      elsif user.iso?
        batch_data = TransactionBatch.where(iso_wallet_id: wallet).sale.where("batch_date BETWEEN :first AND :second", first: actual_first_date, second: actual_second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
        if batch_data.present?
          batches = []
          report.user_id = user.id
          report.save
          batch_data.try(:each) do |key,batch|
            balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:iso_balance).to_f
            gross_earnings = batch.pluck(:iso_commission).try(:compact).try(:sum).to_f
            commission_split = batch.pluck(:iso_split).try(:compact).try(:sum).to_f
            expenses = batch.pluck(:iso_expense).try(:compact).try(:sum).to_f
            net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
            batches.push({
                             date: key,
                             gross_earnings: gross_earnings.to_f,
                             commission_split: commission_split.to_f,
                             expenses: expenses.to_f,
                             net: net.to_f,
                             balance: balance.to_f,
                             total_trxs: batch.pluck(:total_trxs).try(:compact).try(:sum).to_i
                         })
          end
          if batches.present?
            transactions_csv = CSV.generate do |csv|
              csv << ['Date', 'Starting Balance', 'Transactions', 'Gross Earnings', 'Commission Split', 'Expenses', 'Net Earnings']
              batches.each do |batch|
                csv << [batch[:date].strftime("%m/%d/%Y"), number_to_currency(number_with_precision(batch[:balance].to_f,precision: 2, delimiter: ',')), batch[:total_trxs].to_i,number_to_currency(number_with_precision(batch[:gross_earnings].to_f,precision: 2, delimiter: ',')),
                        number_to_currency(number_with_precision(batch[:commission_split].to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch[:expenses].to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch[:net].to_f,precision: 2, delimiter: ','))]
              end
            end
          end
        end
      elsif user.agent?
        batch_data = TransactionBatch.where(agent_wallet_id: wallet).sale.where("batch_date BETWEEN :first AND :second", first: actual_first_date, second: actual_second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
        if batch_data.present?
          batches = []
          report.user_id = user.id
          report.save
          batch_data.try(:each) do |key,batch|
            balance = batch.select{|e| e.agent_balance != 0}.try(:first).try(:agent_balance).to_f
            gross_earnings = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f
            commission_split = batch.pluck(:agent_split).try(:compact).try(:sum).to_f
            expenses = batch.pluck(:agent_expense).try(:compact).try(:sum).to_f
            net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
            batches.push({
                             date: key,
                             gross_earnings: gross_earnings.to_f,
                             commission_split: commission_split.to_f,
                             expenses: expenses.to_f,
                             net: net.to_f,
                             balance: balance.to_f,
                             total_trxs: batch.pluck(:total_trxs).try(:compact).try(:sum).to_i
                         })
          end
        end
        if batches.present?
          transactions_csv = CSV.generate do |csv|
            csv << ['Date', 'Starting Balance', 'Transactions', 'Gross Earnings', 'Commission Split', 'Expenses', 'Net Earnings']
            batches.each do |batch|
              csv << [batch[:date].strftime("%m/%d/%Y"), number_to_currency(number_with_precision(batch[:balance].to_f,precision: 2, delimiter: ',')), batch[:total_trxs].to_i,number_to_currency(number_with_precision(batch[:gross_earnings].to_f,precision: 2, delimiter: ',')),
                      number_to_currency(number_with_precision(batch[:commission_split].to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch[:expenses].to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch[:net].to_f,precision: 2, delimiter: ','))]
            end
          end
        end
      elsif user.affiliate?
        batch_data = TransactionBatch.where(affiliate_wallet_id: wallet).sale.where("batch_date BETWEEN :first AND :second", first: actual_first_date, second: actual_second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
        if batch_data.present?
          batches = []
          report.user_id = user.id
          report.save
          batch_data.try(:each) do |key,batch|
            balance = batch.select{|e| e.affiliate_balance != 0}.try(:first).try(:affiliate_balance).to_f
            gross_earnings = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f
            commission_split = batch.pluck(:affiliate_split).try(:compact).try(:sum).to_f
            expenses = batch.pluck(:affiliate_expense).try(:compact).try(:sum).to_f
            net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
            batches.push({
                             date: key,
                             gross_earnings: gross_earnings.to_f,
                             commission_split: commission_split.to_f,
                             expenses: expenses.to_f,
                             net: net.to_f,
                             balance: balance.to_f,
                             total_trxs: batch.pluck(:total_trxs).try(:compact).try(:sum).to_i
                         })
          end
        end
        if batches.present?
          transactions_csv = CSV.generate do |csv|
            csv << ['Date', 'Starting Balance', 'Transactions', 'Gross Earnings', 'Commission Split', 'Expenses', 'Net Earnings']
            batches.each do |batch|
              csv << [batch[:date].strftime("%m/%d/%Y"), number_to_currency(number_with_precision(batch[:balance].to_f,precision: 2, delimiter: ',')), batch[:total_trxs].to_i,number_to_currency(number_with_precision(batch[:gross_earnings].to_f,precision: 2, delimiter: ',')),
                      number_to_currency(number_with_precision(batch[:commission_split].to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch[:expenses].to_f,precision: 2, delimiter: ',')),number_to_currency(number_with_precision(batch[:net].to_f,precision: 2, delimiter: ','))]
            end
          end
        end
      end
      if batches.present?
        report.status = "completed"
        report.message = "done"
        file = StringIO.new(transactions_csv)
        qc_file = QcFile.new(name: "export_sale_batches", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "export_sale_batches.csv")
        qc_file.save
        report.file_id = qc_file.id
        UserMailer.send_export_notification(user).deliver_later
      else
        report.status = "completed"
        report.message = "done"
      end

    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end
  end

  def export_transactions(user, first_date, second_date, offset, search_query,wallet_id,trans=nil,type=nil,email=nil,param=nil)
    begin
      store new_status: 'queued'
      transactions = nil
      no_transaction = true
      reports_cron = CronJob.new(name: "export_transactions",description: "Export transaction for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}")
      reports_cron.processing!
      param=param.present? ? JSON.parse(param) : ''
      actual_date=param.try(:[],'date')
      if actual_date.present?
        date_first=actual_date[0..9]
        date_first=Date.strptime(date_first, '%m/%d/%Y')
        date_second=actual_date[13..22]
        date_second=Date.strptime(date_second, '%m/%d/%Y')
      else
        date_first=first_date
        date_second=second_date
      end
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchant_email', message: ""
      report = Report.create(user_id: user.try(:id), first_date: date_first, second_date: date_second, offset: offset, report_type: type, status: "pending",transaction_type: trans,read: false)
      result = { user_id: user.try(:id), start_date: first_date, end_date: second_date,  offset: offset, search_query: search_query, qc_file_id: {}}
      if user.merchant?
        report.user_id = user.merchant_id if user.merchant_id.present?
        wallet_object = Wallet.find_by(id: wallet_id)
        report.name = wallet_object.name
        wallets = wallet_object.try(:id)
        if search_query.present?
          params = JSON.parse(search_query)
          report.params = params
          report.save
          params = params.reject{|key,value| value.empty?}
          params["offset"] = offset
          return_reponse = searching_merchant_local_transaction(params.symbolize_keys, wallets,trans,first_date,second_date,user,offset,true,@host_name,nil,nil,email) if params.present?
          if return_reponse[:no_transaction]
          else
            result[:qc_file_id] = return_reponse
            no_transaction = false
          end
          # transactions = apply_search_filter(search_query, user, offset,wallets,trans)
        else
          report.save
          batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
          unless wallet_object.primary?
            BlockTransaction.where('(sender_wallet_id = ? OR receiver_wallet_id = ?) AND block_transactions.main_type IN (?)', wallets, wallets, ["Reserve Money Return", "Reserve Money Deposit"]).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              count = index + 1
              no_transaction = false
              res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,"not_primary")
              result[:qc_file_id].merge!({"#{index}": res})
            end
          else
            if trans == "success"
              Transaction.where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets) OR transactions.from LIKE (:wallets_string) OR transactions.to LIKE (:wallets_string)', wallets: wallets, wallets_string: "#{wallets}").where.not(main_type: "Sale Issue", status: "pending").where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                # where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets) OR transactions.from LIKE (:wallets_string) OR transactions.to LIKE (:wallets_string)', wallets: wallets, wallets_string: "#{wallets}")
                count = index + 1
                no_transaction = false
                res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            elsif trans == "refund"
              Transaction.where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets) OR transactions.from LIKE (:wallets_string) OR transactions.to LIKE (:wallets_string)', wallets: wallets, wallets_string: "#{wallets}").where(main_type: "refund", status: "approved").where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                count = index + 1
                no_transaction = false
                res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            elsif trans == "decline"
              if wallets.class == Integer
                my_wallet = wallets
              else
                my_wallet = wallets.first
              end
              # Transaction.where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets)', wallets: wallets).where(status: "pending",created_at: first_date.to_datetime.utc..second_date.to_datetime.utc)
              location_id=Wallet.find_by(id: my_wallet).try(:location_id)
              Transaction.where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets)', wallets: wallets).where(status: "pending",created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where.not(main_type: "Sale Issue").or(Transaction.where("tags->'location'->>'id' = ? AND status LIKE ?","#{location_id}", 'pending').where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc)).distinct.order(created_at: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                count = index + 1
                no_transaction = false
                res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            else
              Transaction.where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets)', wallets: wallets).where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee"]).where.not(main_type: "Sale Issue", status: "pending").where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                count = index + 1
                no_transaction = false
                res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            end
          end
          # transactions = Transaction.where(receiver_wallet_id: wallets,created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).or(Transaction.where(sender_wallet_id: wallets,created_at: first_date.to_datetime.utc..second_date.to_datetime.utc)).where.not(status: "pending",main_type: ["Fee Transfer", TypesEnumLib::TransactionViewTypes::ReserveMoneyDeposit, TypesEnumLib::TransactionViewTypes::SaleTip]).order(created_at: :desc)
          result[:wallets_id] = wallets
        end
      else
        # for iso,agent,affilaite
        wallets = user.try(:wallets).try(:primary).pluck(:id)
        report.name = user.try(:wallets).try(:primary).try(:first).try(:name)
        if search_query.present?
          params = JSON.parse(search_query)
          report.params = params
          report.save
          params = params.reject{|key,value| value.empty?}
          params["offset"] = offset
          return_reponse = searching_iso_local_transaction(params.symbolize_keys, wallets, first_date, second_date, user, offset, true,@host_name,email) if params.present?
          if return_reponse[:no_transaction]
          else
            result[:qc_file_id] = return_reponse
            no_transaction = false
          end
        else
          report.save
          batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
          if trans == "personal_transaction"
            BlockTransaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets).where("timestamp BETWEEN :first AND :second", first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).where.not("sub_type IN (?) OR main_type IN (?)",["instant_pay","ACH_deposit","send_check"],["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee","Afp"]).order(timestamp: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              count = index + 1
              no_transaction = false
              res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil,"personal")
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "commission"
            if user.affiliate_program?
              BlockTransaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets).where("timestamp BETWEEN :first AND :second", first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).where(main_type:["Monthly Fee","CBK Fee","Misc fee","Subscription fee","Afp"]).order(timestamp: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|              
                count = index + 1
                no_transaction = false
                res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            else
              BlockTransaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets).where("timestamp BETWEEN :first AND :second", first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).where.not("sub_type IN (?)",["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee"]).order(timestamp: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                count = index + 1
                no_transaction = false
                res = creating_export_file(transactions,user,first_date,second_date,offset,@host_name,email,count,nil)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            end
          end

          result[:wallets_id] = wallets
        end
      end
      reports_cron_again = CronJob.find(reports_cron.id)
      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      elsif result[:qc_file_id].present?
        report.file_id = {id: result[:qc_file_id].values.pluck(:qc_file_id)}
        text = I18n.t('notifications.exports_noti', name: user.name)
        Notification.notify_user(user ,user ,report,"Export",nil,text)
        UserMailer.send_export_notification(user).deliver_later
        report_user_id = user.merchant_id.present? ? user.merchant_id : user.id
        reports_count = Report.where(read: false, user_id: report_user_id).count
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        report.status = "completed"
        report.message = "done"
      else
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
        report.status = "crashed"
        report.message = "file_ids missing"
        msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
        SlackService.notify(msg)
        if reports_cron_again.try(:email) == true && report.status == "crashed"
          UserMailer.send_error_message(user).deliver_later
        end
      end
      reports_cron.completed!
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      p "Export Failed for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}", exc
      p "BACKTRACE: ", exc.backtrace
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      if reports_cron_again.try(:email) == true && report.status == "crashed"
        UserMailer.send_error_message(user).deliver_later
      end
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end

  end

  def export_refunds(user, first_date, second_date, offset)
    begin
      store new_status: 'queued'
      last_page = false
      cursor = nil
      transactions_count = 0
      tx_page_count = 1000
      transactions = []
      wallets_ids = user.wallets.primary.or(user.wallets.tip).pluck(:id)
      reports_cron = CronJob.new(name: "export_refunds",description: "Export Refunds from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,wallets_id: wallets_ids}

      transactions = BlockTransaction.where(receiver_id: user.id, main_type: TypesEnumLib::TransactionType::Refund.capitalize,timestamp: first_date.to_datetime.utc..second_date.to_datetime.utc).or(BlockTransaction.where(main_type: TypesEnumLib::TransactionType::Refund.capitalize,sender_id: user.id,timestamp: first_date.to_datetime.utc..second_date.to_datetime.utc)).order(timestamp: :desc)

      reports_cron.status = "processing"
      transactions_count = transactions.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: transactions_count}
      # store total_transactions: "#{transactions_count}"

      # transactions = transactions.sort_by{|i| i[:timestamp]}.reverse
      transactions_csv = CSV.generate do |csv|
        csv << ['TransID','Date-Time','RefundAmount', 'Name', 'Last4']
        transactions.find_each.with_index(1) do |transaction,count|
          csv << [count, transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), number_to_currency(number_with_precision(SequenceLib.dollars(transaction.amount_in_cents), :precision => 2, delimiter: ',')), wallet_name(transaction.receiver_wallet_id), "'#{transaction.last4}" || transaction.tags["last4"] || transaction.tags.try(:[],"bank_details").try(:[],"last4") || transaction.tags.try(:[],"card_type").try(:[],"last4") || transaction.tags.try(:[],"check_number").try(:[],"last4")]
        end
      end
      file = StringIO.new(transactions_csv)
      qc_file = QcFile.new(name: "merchant_refund_list", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "merchant_refund_list.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      mailer = UserMailer.send_export_notification(user)
      mailer.deliver_later
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      # store id: "#{qc_file.id}"
      # store new_status: 'completed'
      reports_cron.status = "completed"
    rescue => exc
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      # store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_add_money(user, first_date, second_date, offset)
    begin
      store new_status: 'queued'
      reports_cron = CronJob.new(name: "export_add_money",description: "Export Add Money from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,wallets_id: nil}
      invoices = TangoOrder.where(order_type: 1 , user_id: user.id, created_at: first_date..second_date).order("created_at DESC")
      reports_cron.status = "processing"
      transactions_count = invoices.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: transactions_count}
      # store total_transactions: "#{transactions_count}"

      invoices_csv = CSV.generate do |csv|
        csv << ['ID','Number','Recipient', 'Amount', 'Status', 'Name', 'Date']
        invoices.find_each.with_index(1) do |transaction|
          csv << [transaction.id, transaction.number,transaction.recipient, number_to_currency(number_with_precision(transaction[:amount].to_f, :precision => 2, delimiter: ',')), transaction.status, transaction.name, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p")]
        end
      end
      file = StringIO.new(invoices_csv)
      qc_file = QcFile.new(name: "add_money_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "add_money_export.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      mailer = UserMailer.send_export_notification(user)
      mailer.deliver_later
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      # store id: "#{qc_file.id}"
      # store new_status: 'completed'
      reports_cron.status = "completed"
    rescue => exc
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_permission(user, first_date, second_date, offset)
    begin
      store new_status: 'queued'
      reports_cron = CronJob.new(name: "export_permission",description: "Export permission",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,wallets_id: nil}

      report = Report.new(user_id: user.try(:id), first_date: first_date, second_date: second_date, offset: offset, report_type: "permission", status: "pending", read: false)
      report.name = "Permission List"
      report.save
      permissions = Permission.where('merchant_id IN (?) OR permission_type IN (?)',[user.id], [0,1]).order("created_at DESC")
      reports_cron.status = "processing"
      permissions_csv = CSV.generate do |csv|
        # csv << ['ID','Name','Type', 'Date']
        csv << ['ID','Creation Date','User Permission Types', '#of users Assigned']
        permissions.find_each.with_index(1) do |permission|
          # csv << [permission.id, permission.name,permission.permission_type,permission.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p")]
          # csv << [permission.id,permission.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),  permission.name, permission.merchant_id.present? ? permission.merchant_users_count(permission.merchant_id) : permission.merchant_users_count(user_id) ]
          csv << [permission.id,permission.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),  permission.name, permission.merchant_users_count(permission.merchant_id) ]
        end
      end
      file = StringIO.new(permissions_csv)
      qc_file = QcFile.new(name: "permission_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "permission_export.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      mailer = UserMailer.send_export_notification(user)
      mailer.deliver_later
      reports_cron.status = "completed"
      report.file_id = {id: [qc_file.id]}
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      report.status = "completed"
      report.message = "done"
    rescue => exc
      puts "export worker error occured", exc
      store new_status: 'completed'
      reports_cron.status = "failed"
      reports_cron.status = "failed"
    ensure
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end
  end


  def export_request_money(user, first_date, second_date, offset,type)
    begin
      store new_status: 'queued'
      reports_cron = CronJob.new(name: "export_request_money",description: "Export Request Money from merchant",success: true,status: "pending")
      report = Report.create(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: type, status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,wallets_id: nil}
      if user.merchant_id.present? && user.try(:permission).admin?
        invoices = Request.where("sender_id = ? or reciever_id = ?", user.merchant_id, user.merchant_id).where(created_at: first_date..second_date).order(created_at: :desc)
      else
        invoices = Request.where("sender_id = ? or reciever_id = ?", user.id, user.id).where(created_at: first_date..second_date).order(created_at: :desc)
      end
      reports_cron.status = "processing"
      transactions_count = invoices.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: transactions_count}
      # store total_transactions: "#{transactions_count}"

      invoices_csv = CSV.generate do |csv|
        csv << ['ID','Date','Sender','Recipient', 'Amount', 'Status']
        invoices.find_each.with_index(1) do |transaction|
          csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), User.where(id: transaction.sender_id).first.name, User.where(id: transaction.reciever_id).first.name, number_to_currency(number_with_precision(transaction.amount.to_f , :precision => 2, delimiter: ',')), transaction.status]
        end
      end
      file = StringIO.new(invoices_csv)
      qc_file = QcFile.new(name: "request_money_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "request_money_export.csv")
      qc_file.save
      report.file_id = {id: [qc_file.id]}
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      # mailer = UserMailer.send_export_file(user, qc_file.image.url)
      # mailer.deliver_later
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      # store id: "#{qc_file.id}"
      # store new_status: 'completed'
      reports_cron.status = "completed"
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      report.status = "completed"
      report.message = "done"
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_transfer_report(user, first_date, second_date, wallet, offset, status)
    begin
      store new_status: 'queued'
      user_wallet = Wallet.find(wallet)
      reports_cron = CronJob.new(name: "export_transfer_report",description: "Export Transfer Report from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,wallets_id: wallet}

      reports_cron.status = "processing"
      # if ENV["SYNC_FROM"] == "sequence"
      #   myCsv = get_sequence_sales_report(wallet,user_wallet, first_date, second_date, offset)
      # elsif ENV["SYNC_FROM"] == "local"
      myCsv = get_local_sales_report(wallet,user_wallet, first_date, second_date, offset, status)
      # end
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: myCsv[:reports].count}
      file = StringIO.new(myCsv[:csv])
      qc_file = QcFile.new(name: "sales_report", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "sales_report.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      mailer = UserMailer.send_export_notification(user)
      mailer.deliver_later
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      reports_cron.status = "completed"
    rescue => exc
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      reports_cron.status = "failed"
    ensure
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def get_local_sales_report(wallet,user_wallet, first_date, second_date, offset, status)
    if user_wallet.reserve? || user_wallet.tip?
      if user_wallet.reserve?
        reports = BlockTransaction.where(receiver_wallet_id: wallet, main_type:[TypesEnumLib::TransactionViewTypes::ReserveMoneyDeposit], timestamp: first_date.to_datetime.utc..second_date.to_datetime.utc).order(timestamp: :desc)
      elsif user_wallet.tip?
        reports = BlockTransaction.where(receiver_wallet_id: wallet, main_type:[TypesEnumLib::TransactionViewTypes::SaleTip], timestamp: first_date.to_datetime.utc..second_date.to_datetime.utc).order(timestamp: :desc)
      end
    else
      if status == "Approved"
        reports = Transaction.where(receiver_wallet_id: wallet, created_at: first_date.to_datetime.utc..second_date.to_datetime.utc, main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce, TypesEnumLib::TransactionViewTypes::VirtualTerminal, TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::PinDebit] ,status: ["approved", "refunded", "complete"]).order(created_at: :desc)
      elsif status == "Decline"
        reports = Transaction.where(receiver_wallet_id: wallet, created_at: first_date.to_datetime.utc..second_date.to_datetime.utc, main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce, TypesEnumLib::TransactionViewTypes::VirtualTerminal, TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::PinDebit] ,status: ["pending"]).order(created_at: :desc)
      end
    end
    reports_count = reports.count
    store total_transactions: "#{reports_count}"

    if user_wallet.primary?
      total_fee = reports.pluck(:fee)
      privacy_fee = reports.pluck(:privacy_fee)
      total_privacy_fee = privacy_fee.inject(0){|sum,x| sum+x.to_f}
      total_fee = total_fee.inject(0){|sum,x| sum + x.to_f}
      total_tip = reports.pluck(:tip)
      total_tip = total_tip.inject(0){|sum,x| sum + x.to_f.to_f}
      hold_money = reports.pluck(:reserve_money)
      hold_money = hold_money.inject(0){|sum,x| sum + x.to_f}
      # total_after_fee = reports.pluck(:net_amount)
      # total_after_fee = total_after_fee.inject(0){|sum,x| sum + x.to_f}
      net_fee = reports.pluck(:net_fee)
      net_fee = net_fee.inject(0){|sum,x| sum + x.to_f}
      tx_amount = reports.pluck(:amount)
      tx_amount = tx_amount.inject(0){|sum,x| sum + x.to_f}
      total_amount = reports.pluck(:total_amount)
      total_amount = total_amount.inject(0){|sum,x| sum + x.to_f}
      total_after_fee = total_amount - total_fee - total_tip
    else
      tx_amount = reports.pluck(:tx_amount)
      tx_amount = tx_amount.inject(0){|sum,x| sum + SequenceLib.dollars(x).to_f}
      total_amount = reports.pluck(:amount_in_cents)
      total_amount = total_amount.inject(0){|sum,x| sum + SequenceLib.dollars(x).to_f}
    end
    csv = CSV.generate do |csv|
      if user_wallet.primary?
        csv << ["Date/Time","Type","Sender","Receiver","Last4","Terminal Id", "Card Type", "Card Brand","Total Amount", "Tx Amount","Privacy Fee","Tip", "Fee","Net Fee", "Total Net", "Reserve"]
      else
        if user_wallet.reserve?
          csv << ["Date/Time","Type", "Sender", "Receiver", "Last4", "Total Amount", "Reserve"]
          # worksheet.add_row %w(Date/Time Sender Name Phone# Amount Reserve), style: highlight_cell
        elsif user_wallet.tip?
          csv << ["Date/Time", "Type", "Sender", "Receiver", "Last4", "Total Amount", "Reserve"]
          # worksheet.add_row %w(Date/Time Sender Name Phone# Amount Tip), style: highlight_cell
        end
      end
      count = 0
      if user_wallet.primary?
        reports.find_each.with_index(1) do |transaction, idx|
          csv << [transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),transaction.main_type, get_report_source(transaction), transaction.receiver_wallet_id, "'#{transaction.last4}", transaction.tags.try(:[], "sales_info").try(:[], "terminal_id") || "---",transaction.try(:card).try(:card_type).try(:capitalize) || "---", transaction.try(:card).try(:brand).try(:capitalize) || "---" , number_to_currency(number_with_precision(transaction.total_amount, :precision => 2, delimiter: ',')),number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),number_with_precision(number_to_currency(transaction.privacy_fee), precision: 2, delimiter: ','), number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ',')),number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ',')), number_to_currency(number_with_precision((transaction.total_amount - transaction.tip - transaction.fee), :precision => 2, delimiter: ',')),number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))]
          # csv << [transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),transaction.main_type, get_report_source(transaction), transaction.receiver_wallet_id, "#{transaction.last4}", number_to_currency(number_with_precision(transaction.total_amount, :precision => 2, delimiter: ',')),number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),number_with_precision(number_to_currency(transaction.privacy_fee), precision: 2, delimiter: ','), number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ',')),number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction.net_amount, :precision => 2, delimiter: ',')),number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))]
          total count+=1
          at idx
        end
      else
        if user_wallet.reserve?
          reports.find_each.with_index(1) do |transaction, idx|
            csv << [transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.main_type, get_report_source(transaction), transaction.receiver_wallet_id, "'#{transaction.last4}", number_to_currency(number_with_precision(SequenceLib.dollars(transaction.parent_amount), :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(SequenceLib.dollars(transaction.amount_in_cents), :precision => 2, delimiter: ','))]
            total count+=1
            at idx
          end
        elsif user_wallet.tip?
          reports.find_each.with_index(1) do |transaction, idx|
            csv << [transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.main_type,get_report_source(transaction), transaction.receiver_wallet_id, "'#{transaction.last4}", number_to_currency(number_with_precision(SequenceLib.dollars(transaction.parent_amount), :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(SequenceLib.dollars(transaction.amount_in_cents), :precision => 2, delimiter: ','))]
            total count+=1
            at idx
          end
        end
      end
      if user_wallet.primary?
        csv << ['Totals','','','','','','','', number_with_precision(number_to_currency(total_amount), precision: 2, delimiter: ','),number_to_currency(number_with_precision(tx_amount, precision: 2, delimiter: ',')),number_to_currency(number_with_precision(total_privacy_fee, precision: 2, delimiter: ',')),number_to_currency(number_with_precision(total_tip, precision: 2, delimiter: ',')), number_to_currency(number_with_precision(total_fee, precision: 2, delimiter: ',')), number_to_currency(number_with_precision(net_fee, precision: 2, delimiter: ',')), number_with_precision(number_to_currency(total_after_fee), precision: 2, delimiter: ','), number_to_currency(number_with_precision(hold_money, precision: 2, delimiter: ',')) ]
      else
        if user_wallet.reserve?
          csv << ['Totals','','','','', number_with_precision(number_to_currency(total_amount), precision: 2, delimiter: ','), number_with_precision(number_to_currency(total_amount.to_f), precision: 2, delimiter: ',') ]
        elsif user_wallet.tip?
          csv << ['Totals','','','','', number_with_precision(number_to_currency(total_amount), precision: 2, delimiter: ','), number_with_precision(number_to_currency(total_amount.to_f ), precision: 2, delimiter: ',') ]
        end
      end
    end
    return {csv: csv, reports: reports}
  end

  def get_sequence_sales_report(wallet,user_wallet, first_date, second_date, offset)
    cursor = nil
    tx_page_count = 1000
    # reports = []
    last_page = false
    while last_page == false do
      begin
        if user_wallet.primary?
          query_result = all_transactions_with_time(wallet, first_date, second_date, "b2b_transfer","sale",true,tx_page_count, cursor)
        elsif user_wallet.reserve? || user_wallet.tip?
          query_result = reports_transactions(wallet, first_date, second_date, "transfer",nil,true,tx_page_count,cursor)
        end
        raise StandardError.new("Seq error") if query_result.blank?
        reports = query_result.first
        cursor = query_result.second
        last_page = query_result.third
      rescue StandardError => exc
        tx_page_count = (tx_page_count/2).try(:ceil)
      end
    end
    # if user_wallet.primary?
    #   reports = all_transactions_with_time(wallet, first_date, second_date, "b2b_transfer","sale").to_a
    # elsif user_wallet.reserve? || user_wallet.tip?
    #   reports = reports_transactions(wallet, first_date, second_date, "transfer")
    # end

    reports_count = reports.count

    store total_transactions: "#{reports_count}"

    if user_wallet.primary?
      hold_money = reports.select{|v| v[:new_type] != "Fee"}.pluck(:hold_money)
      hold_money = hold_money.inject(0){|sum,x| sum+x.to_f}

      total_fee_amount = reports.select{|v| v[:fee] != 0.0}.pluck(:fee_without_hold_money)
      privacy_fee = reports.pluck(:privacy_fee)
      total_privacy_fee = privacy_fee.inject(0){|sum,x| sum+x.to_f}
      total_fee = total_fee_amount.inject(0){|sum,x| sum+x.to_f}

      tip = reports.select{|v| v[:new_type] != "Fee"}.pluck(:tip)
      tip_amount = 0
      tip.each do |x|
        tip_amount = x.to_f + tip_amount.to_f
      end

      after_fee = reports.select{|v| v[:sub_type] == "Sale" || v[:new_type] == "Sale"}.pluck(:net)
      total_after_fee = 0
      after_fee.each do |x|
        total_after_fee = x.to_f + total_after_fee.to_f
      end

    end

    amount = reports.select{|v| v[:new_type] != "Fee"}.pluck(:amount_updated)
    total_amount = 0
    amount.each do |x|
      total_amount = x.to_f + total_amount.to_f
    end

    merchant_amount = reports.pluck(:merchant_amount)
    merchant_amount = merchant_amount.inject(0){|sum,x| sum + x.to_f}

    reports = reports.sort_by{|i| i[:timestamp]}.reverse
    csv = CSV.generate do |csv|
      if user_wallet.primary?
        csv << %w{ Date/Time Sender Name Phone# Amount Fee PrivacyFee Reserve Tip Net}
      else
        if user_wallet.reserve?
          csv << %w{ Date/Time Sender Name Phone# Amount Reserve}
          # worksheet.add_row %w(Date/Time Sender Name Phone# Amount Reserve), style: highlight_cell
        elsif user_wallet.tip?
          csv << %w{ Date/Time Sender Name Phone# Amount Tip}
          # worksheet.add_row %w(Date/Time Sender Name Phone# Amount Tip), style: highlight_cell
        end
      end
      count = 0
      if user_wallet.primary?
        reports.find_each.with_index(1) do |transaction, idx|
          csv << [transaction[:timestamp].to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction[:source], wallet_name(transaction[:source]), number_to_phone(wallet_phone_number(transaction[:source]), area_code: true), number_to_currency(number_with_precision(transaction[:amount_updated].to_f, :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction[:fee_without_hold_money], :precision => 2, delimiter: ',')), number_with_precision(number_to_currency(transaction[:privacy_fee]), precision: 2, delimiter: ','), number_to_currency(number_with_precision(transaction[:hold_money], :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction[:tip], :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction[:net], :precision => 2, delimiter: ','))]
          total count+=1
          at idx
        end
      else
        if user_wallet.reserve?
          reports.find_each.with_index(1) do |transaction, idx|
            csv << [transaction[:timestamp].to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction[:source], wallet_name(transaction[:source]), wallet_phone_number(transaction[:source]), number_to_currency(number_with_precision(transaction[:merchant_amount], :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction[:amount_updated], :precision => 2, delimiter: ','))]
            total count+=1
            at idx
          end
        elsif user_wallet.tip?
          reports.find_each.with_index(1) do |transaction, idx|
            csv << [transaction[:timestamp].to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction[:source], wallet_name(transaction[:source]), wallet_phone_number(transaction[:source]), number_to_currency(number_with_precision(transaction[:merchant_amount], :precision => 2, delimiter: ',')), number_to_currency(number_with_precision(transaction[:amount_updated], :precision => 2, delimiter: ','))]
            total count+=1
            at idx
          end
        end
      end
      if user_wallet.primary?
        csv << ['Totals','','','', number_with_precision(number_to_currency(total_amount), precision: 2, delimiter: ','), number_to_currency(number_with_precision(total_fee, precision: 2, delimiter: ',')), number_to_currency(number_with_precision(total_privacy_fee, precision: 2, delimiter: ',')), number_to_currency(number_with_precision(hold_money, precision: 2, delimiter: ',')), number_with_precision(number_to_currency(tip_amount.to_f), precision: 2, delimiter: ',') , number_with_precision(number_to_currency(total_amount.to_f - total_fee.to_f - hold_money.to_f), precision: 2, delimiter: ',') ]
      else
        if user_wallet.reserve?
          csv << ['Totals','','','', number_with_precision(number_to_currency(merchant_amount), precision: 2, delimiter: ','), number_with_precision(number_to_currency(total_amount.to_f), precision: 2, delimiter: ',') ]
        elsif user_wallet.tip?
          csv << ['Totals','','','', number_with_precision(number_to_currency(merchant_amount), precision: 2, delimiter: ','), number_with_precision(number_to_currency(total_amount.to_f ), precision: 2, delimiter: ',') ]
        end
      end

    end
    return {csv: csv, reports: reports}
  end

  def export_checks_report(user, first_date, second_date, wallet, send_via, offset)
    begin
      store new_status: 'queued'
      # last_page = false
      # cursor = nil
      # reports_count = 0
      # tx_page_count = 1000
      reports = nil
      reports_cron = CronJob.new(name: "export_checks_report",description: "Export Checks Report from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,wallets_id: wallet}

      reports = Transaction.where(sender_wallet_id: wallet, main_type: [TypesEnumLib::TransactionViewTypes::SendCheck,TypesEnumLib::TransactionViewTypes::BulkCheck],created_at: first_date.to_datetime.utc..second_date.to_datetime.utc, status: ["approved", "refunded", "complete"]).order(created_at: :desc)

      # reports = reports.select{|v| v[:reference]["type"] == TypesEnumLib::TransactionType::SendCheck || v[:reference]["type"] == TypesEnumLib::TransactionType::BulkCheck}
      if send_via == "all"
        reports = reports
      elsif send_via == "direct_deposit"
        reports = reports.select{|v| v.tags["send_via"] == "Direct Deposit"}
      elsif send_via == "email"
        reports = reports.select{|v| v.tags["send_via"] == "Email" }
      end
      reports_cron.status = "processing"

      reports_count = reports.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: reports_count}
      # store total_transactions: "#{reports_count}"

      total_fee = reports.pluck(:fee)
      total_fee = total_fee.inject(0){|sum,x| sum + x.to_f}
      total_amount = reports.pluck(:amount)
      total_amount = total_amount.inject(0){|sum,x| sum + x.to_f}
      total_net = reports.pluck(:net_amount)
      total_after_fee = total_net.inject(0){|sum,x| sum.to_f+x.to_f}

      # reports = reports.sort_by{|i| i[:timestamp]}.reverse
      user_wallet = Wallet.find(wallet)
      report_csv = CSV.generate do |csv|
        count = 0
        csv << ['Wallet Name', user_wallet.name]
        csv << ['Date/Time','Send Via','Name', 'Email', 'A/C Last 4','Total Send', 'Fee','Total With Fee']
        reports.find_each do |transaction|
          if transaction.main_type == TypesEnumLib::TransactionViewTypes::BulkCheck
            send_via = "BulkCheck"
          else
            send_via = transaction.tags["send_via"]
          end
          csv << [transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), send_via, get_check_user_name(transaction), get_check_user_email(transaction), "'#{transaction.last4}", number_to_currency(transaction.net_amount), number_to_currency(transaction.fee), number_to_currency(transaction.amount)]
          total count+=1
        end
        csv << ['Totals','','','','', number_with_precision(number_to_currency(total_after_fee), precision: 2, delimiter: ','), number_to_currency(number_with_precision(total_fee, precision: 2, delimiter: ',')), number_with_precision(total_amount, precision: 2, delimiter: ',') ]
      end
      file = StringIO.new(report_csv)
      qc_file = QcFile.new(name: "transactions_list", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "checks_report.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      mailer = UserMailer.send_export_notification(user)
      mailer.deliver_later
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      store id: "#{qc_file.id}"
      store new_status: 'completed'
      reports_cron.status = "completed"
    rescue => exc
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_giftcard_report(user, first_date, second_date, wallet, offset)
    begin
      store new_status: 'queued'
      reports_cron = CronJob.new(name: "export_giftcard_report",description: "Export Giftcard Report from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date}
      result[:wallets_id]= wallet

      if user.merchant_id.present? && user.submerchant_type == "admin_user"
        giftcards = TangoOrder.where(user_id: user.merchant_id,wallet_id: wallet, account_identifier: "quickcard", created_at: first_date..second_date)
      else
        giftcards = TangoOrder.where(user_id: user.id,wallet_id: wallet, account_identifier: "quickcard", created_at: first_date..second_date)
      end
      reports_cron.status = "processing"
      giftcards_count = giftcards.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: giftcards_count}
      # store total_transactions: "#{giftcards_count}"

      total_amount = giftcards.pluck(:amount)
      total_amount = total_amount.compact.inject(0){|sum,x| sum+x}
      total_fee = giftcards.pluck(:gift_card_fee)
      total_fee = total_fee.compact.inject(0){|sum,x| sum+x}
      total_with_fee = total_fee + total_amount

      # user_wallet = Wallet.find(wallet)

      giftcards_csv = CSV.generate do |csv|
        csv << ['Date','Gift Card Name','Send to','UTID', 'Total Send', 'Fee', 'Total With Fee']
        giftcards.find_each.with_index(1) do |transaction,count|
          csv << [transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.name , transaction.recipient, transaction.utid, number_with_precision(number_to_currency(transaction.amount), precision: 2), number_with_precision(number_to_currency(transaction.gift_card_fee), precision: 2), number_with_precision(number_to_currency(transaction.amount.to_f + transaction.gift_card_fee.to_f), precision: 2)]
        end
        csv << ['Totals','','','',number_to_currency(total_amount),number_to_currency(total_fee), number_to_currency(total_with_fee)]
      end
      file = StringIO.new(giftcards_csv)
      qc_file = QcFile.new(name: "giftcard_report_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "giftcard_report_export.csv")
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      mailer = UserMailer.send_export_notification(user)
      mailer.deliver_later
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      store id: "#{qc_file.id}"
      store new_status: 'completed'
      reports_cron.status = "completed"
    rescue => exc
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      reports_cron.result = result
      reports_cron.reports!
    end
  end


  def export_merchant(user=nil,search_query=nil,type)
    begin
      search_query = JSON.parse(search_query) if search_query.present? && search_query.class == String
      if search_query["type"].blank?
        merchants=User.merchant.includes(:oauth_apps).includes(:owners).where(:merchant_id => nil,low_risk: [nil,false]).complete_profile_users.order(id: :desc)
      else
        params = {"query" => search_query["type"]}
        merchants = search(params.with_indifferent_access)
      end
      if search_query.try(:[],'archive').present?
        merchants=merchants.archived_users
      else
        merchants=merchants.active_merchant
      end
      report = Report.create(user_id: user.try(:id), report_type: type.try(:humanize), status: "pending", read: false)
      if merchants.present?
        locations = Location.where(merchant_id: merchants.pluck(:id)).select("id AS id", "merchant_id AS merchant_id").group_by{|e| e.merchant_id}
        merchants_csv = CSV.generate do |csv|
          csv << ['Merchant ID', 'Created Day', 'Merchant Name', 'Total Locations', 'Owner\'s Name', 'Phone Number', 'Email', 'Status']
          merchants.each do |merchant|
            if merchant.oauth_apps.first.present?
              status = merchant.oauth_apps.first.is_block ? 'Inactive' : 'Active'
            else
              status = "Incomplete Profile"
            end
            csv << ["M-#{merchant.id}", merchant.created_at.to_date, merchant.name, locations[merchant.id].try(:count), merchant.owners.present? ? merchant.owners.last.try(:name) : merchant.owner_name, merchant.phone_number, merchant.email, status]
          end
        end

        file = StringIO.new(merchants_csv)
        qc_file = QcFile.new(name: "merchants_export", start_date: Time.now, end_date: Time.now, status: :export)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "merchants_export.csv")
        qc_file.save
        report.file_id = {id: qc_file.id}
        report.status = "completed"
        report.message = "done"
        report.params=search_query
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'export' , message: qc_file.id
      else
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx",message: "No Merchants found!"}
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
    ensure
      report.save
    end
  end
  def export_merchant_user(user=nil,search_query=nil,type)
    begin
      search_query = JSON.parse(search_query) if search_query.present? && search_query.class == String
      if search_query["type"].blank?
        merchants=User.where.not(merchant_id:nil).where(archived: "false").order(id: :desc)
      else
        params = {"query" => search_query["type"]}
        merchants = search_merchant_users(params.with_indifferent_access)
      end
      report = Report.create(user_id: user.try(:id), report_type: type.try(:humanize), status: "pending", read: false)
      if merchants.present?
        merchants_csv = CSV.generate do |csv|
          csv << ['Created Day', ' Merchant ID', 'Merchant Name', 'Merchant User\'s Name', 'Merchant User\'s ID', 'Merchant User\'s Phone Number', 'Merchant User\'s Email', 'User Type']
          merchants.each do |merchant|
            if merchant.oauth_apps.first.present?
              status = merchant.oauth_apps.first.is_block ? 'Inactive' : 'Active'
            else
              status = "Incomplete Profile"
            end
            csv << [ merchant.created_at.to_date, "M-#{merchant.parent_merchant.try(:id)}",  merchant.parent_merchant.try(:name).present? ?  merchant.parent_merchant.try(:name) : '', merchant.name, "M-#{merchant.id}", merchant.phone_number, merchant.email, merchant.try(:permission).try(:name)]
          end
        end

        file = StringIO.new(merchants_csv)
        qc_file = QcFile.new(name: "merchants_export", start_date: Time.now, end_date: Time.now, status: :export)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "merchants_export.csv")
        qc_file.save
        report.file_id = {id: qc_file.id}
        report.status = "completed"
        report.message = "done"
        report.params=search_query
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'export' , message: qc_file.id
      else
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx",message: "No Merchants Users found!"}
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
    ensure
      report.save
    end

  end

  def export_location(user,search_query=nil,type)
    begin
      merchants_locations = false
      search_query = JSON.parse(search_query) if search_query.present? && search_query.class == String
      report = Report.create(user_id: user.try(:id), report_type: type.try(:humanize), status: "pending", read: false)
      if search_query["type"].blank?
        locations = Location.joins(:merchant).includes(:ecomm_platform).all
        merchants_locations = true
        @merchant = search_query["merchant_location"].present? ? User.friendly.find(search_query["merchant_location"]) : nil
      else
        loc = search_query["type"]
        if loc.present?
          loc["find_by"] = loc["find_by"].try(:strip)
          locations=[]
          search_operation = 'like'
          if loc["option"]=='merchant_name' || loc["option"]=='merchant_email'
            search_by='lower(name)' if loc["option"]=='merchant_name'
            search_by='lower(email)' if loc["option"]=='merchant_email'
            merchant_name = User.where("#{search_by} #{search_operation} ?", "%#{loc["find_by"].try(:downcase)}%").pluck(:id)
            if merchant_name.present?
              location_name  = Location.joins(:merchant).includes(:ecomm_platform).where(merchant_id: merchant_name)
              locations=location_name if location_name.present?
            end
          end
          if loc["option"]=='merchant_id'
            merchant_id = User.where(id: loc["find_by"]).pluck(:id)
            location_id  = Location.joins(:merchant).includes(:ecomm_platform).where(merchant_id: merchant_id) if merchant_id.present?
            locations=location_id if location_id.present?
          end
          if loc["option"]=='main_wallet_id'
            location_id= Wallet.primary.where(id: loc["find_by"]).pluck(:location_id)
            locations  = Location.joins(:merchant).includes(:ecomm_platform).where(id: location_id) if location_id.present?
          end
          if loc["option"]=='business_name'
            locations= Location.joins(:merchant).includes(:ecomm_platform).where("lower(business_name) #{search_operation} ?", "%#{loc["find_by"].try(:downcase)}%")
          end
          if loc["option"]=='web_site'
            locations= Location.joins(:merchant).where("lower(web_site) #{search_operation} ?", "%#{loc["find_by"].try(:downcase)}%")
          end
          if loc["option"]=='category'
            @locations = Location.joins(:merchant).includes(:ecomm_platform).joins(:category).where("categories.name ILIKE ?","%#{loc["find_by"].try(:downcase)}%")
          end
          if loc["option"]=='token'
            locations = Location.joins(:merchant).joins(:ecomm_platform).where("ecomm_platforms.name ILIKE ?","%#{loc["find_by"].try(:downcase)}%")
            # @locations= Location.where("lower(ecom_platform) #{search_operation}?", "%#{params[:query]["find_by"].try(:downcase)}%")
          end
          if loc["option"]=='city'
            locations= Location.joins(:merchant).includes(:ecomm_platform).where("lower(city) #{search_operation}?", "%#{loc["find_by"].try(:downcase)}%")
          end
          if loc["option"]=='state'
            locations= Location.joins(:merchant).includes(:ecomm_platform).where("lower(state) #{search_operation}?", "%#{loc["find_by"].try(:downcase)}%")
          end
        end
      end
      if locations.present?
        if merchants_locations
          conditions = {}
          conditions = {merchant_id: @merchant.try(:id)} if @merchant.present?
          locations = Location.joins(:merchant).where(conditions)
          location_csv = CSV.generate do |csv|
            csv << %w{ Id Business_name Email Website Category }
            locations.each do |l|
              website = ""
              begin
                website = JSON(l.web_site) if l.web_site.present?
              rescue
              end
              csv << [l.id, l.business_name, l.email, website, l.category.present? ? l.category.try(:name) : ""]
            end
          end
        else
          gateway_ids = locations.pluck(:primary_gateway_id,:secondary_gateway_id, :ternary_gateway_id).flatten.compact.uniq
          gateways = PaymentGateway.where(id: gateway_ids).select("id AS id", "name AS name").group_by{|e| e.id}
          merchants = User.where(id: locations.pluck(:merchant_id).uniq).select("id AS id","name AS name", "email AS email").group_by{|e| e.id}
          wallets = Wallet.primary.where.not(location_id: nil).select("id AS id, location_id AS location_id").group_by{|e| e.location_id}
          categories = Category.select("id AS id, name AS name").group_by{|e| e.id}
          location_users = LocationsUsers.joins(:user).where("users.role IN (?)", [7,9,10]).where(location_id:locations.pluck(:id)).select("location_id", "users.role as user_role","users.name as users_name").group_by{|e| e.location_id}
          my_loc = {}
          location_users.each do |loc_id,loc_users|
            my_loc[loc_id] = {} if my_loc[loc_id].blank?
            loc_users.each do |loc_user|
              my_loc[loc_id]["iso"] = loc_user.users_name if loc_user.user_role == 7
              my_loc[loc_id]["agent"] = loc_user.users_name if loc_user.user_role == 9
              my_loc[loc_id]["affiliate"] = loc_user.users_name if loc_user.user_role == 10
            end
          end
          location_csv = CSV.generate do |csv|
            csv << ['Merchant ID', 'Merchant Name', 'Merchant Email',  'DBA Name', 'Main Wallet ID', 'E-Commerce Platform', 'Category', 'Website', 'ISO Name', 'Agent Name', 'Affiliate Name', 'Risk', 'Bank 1', 'Bank 2', 'Bank 3']
            locations.find_each do |l|
              merchant = merchants[l.merchant_id].try(:first)
              if merchant.present?
                csv << [l.merchant_id, merchant.name, merchant.email, l['business_name'], wallets[l.id].try(:first).try(:id), l.ecomm_platform.try(:name), categories[l.category_id].try(:first).try(:name), l.web_site, my_loc.try(:[],l.id).try(:[],"iso"), my_loc.try(:[],l.id).try(:[],"agent"), my_loc.try(:[],l.id).try(:[],"affiliate"), l.risk, gateways[l.primary_gateway_id].try(:first).try(:name), gateways[l.secondary_gateway_id].try(:first).try(:name), gateways[l.ternary_gateway_id].try(:first).try(:name)]
              end
            end
          end
        end

        file = StringIO.new(location_csv)
        qc_file = QcFile.new(name: "locations_export", start_date: Time.now, end_date: Time.now, status: :export)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "locations_export.csv")
        qc_file.save
        report.file_id = {id: qc_file.id}
        report.status = "completed"
        report.message = "done"
        report.params=search_query
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'export' , message: qc_file.id
      else
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx",message: "No Merchants found!"}
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
    ensure
      report.save
    end
  end

  def export_dispute(user, first_date, second_date, offset, search_query=nil,type=nil,host_name=nil)
    begin
      store new_status: 'queued'
      no_transaction = true
      reports_cron = CronJob.new(name: "export_#{type}",description: "Export #{type}",success: true,status: "pending")
      reports_cron.processing!
      report = Report.create(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: type, status: "pending", read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date, qc_file_id: {}}
      result[:wallets_id] = nil

      params = {}
      search_query = JSON.parse(search_query) if search_query.present? && search_query.class == String
      params[:q] = search_query.try(:[],"q") if search_query.present?
      report.params = params[:q]
      if params[:q].present?
        params[:q]["id_cont"] = params[:q]["id_cont"].try(:strip)
        params[:q]["case_number_cont"] = params[:q]["case_number_cont"].try(:strip)
        params[:q]["dispute_type_eq"] = params[:q]["dispute_type_eq"].try(:strip)
        params[:q]["amount_eq"] = params[:q]["amount_eq"].try(:strip)
        params[:q]["card_number_cont"] = params[:q]["card_number_cont"].try(:strip)
        params[:q]["merchant_wallet_location_business_name_cont"] = params[:q]["merchant_wallet_location_business_name_cont"].try(:strip)
        params[:q][:recieved_date_gteq] = first_date
        params[:q][:recieved_date_lteq] = second_date
      end
      if params[:q].present?
        if params[:q]["recieved_date"].present?
          date = parse_date(params[:q]["recieved_date"])
          first_date = Date.new(date.first[:year].to_i, date.first[:month].to_i, date.first[:day].to_i)
          second_date = Date.new(date.second[:year].to_i, date.second[:month].to_i, date.second[:day].to_i)
          params[:q][:recieved_date_gteq] = first_date
          params[:q][:recieved_date_lteq] = second_date
        end
      end
      if search_query.present? && search_query["case"].present? && search_query["case"] != "all"
        case_status = DisputeCase.statuses[params[:case]]
      else
        case_status = ""
      end
      if case_status.present? && params[:q].present?
        params[:q][:status_eq] = case_status
      elsif case_status.present? && params[:q].blank?
        params[:q] = {}
        params[:q]["status_eq"] = case_status
        params[:q][:recieved_date_gteq] = first_date
        params[:q][:recieved_date_lteq] = second_date
      elsif !params[:q].present?
        params[:q] = {}
        params[:q][:recieved_date_gteq] = first_date
        params[:q][:recieved_date_lteq] = second_date
      end
      # @q = DisputeCase.ransack(params[:q])
      # @dispute_cases = @q.result.includes(:charge_transaction, :merchant_wallet).order(due_date: :desc)
      #
      url = "/admins/disputes/cases"
      batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
      reports_cron_again = reports_cron
      DisputeCase.ransack(params[:q]).result.order(recieved_date: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |disputeCases,index|
        transactions_csv = create_dispute_csv(disputeCases)
        count = index + 1
        no_transaction = false
        file = StringIO.new(transactions_csv)
        qc_file = QcFile.new(name: "dispute_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "dispute_export.csv")
        qc_file.save
        reports_cron_again = CronJob.find_by(id: reports_cron.id)
        if reports_cron_again.try(:email) != true
          # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sendemail', message: {user_id: user.id, qc_file: qc_file.id,report: "admin",count: count, url: url}
        end
        # 5 sec
        sleep(4)
        result[:qc_file_id].merge!({"#{index}":  {file_url: qc_file.image.url, qc_file_id: qc_file.id}})
      end
      if no_transaction
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      elsif result[:qc_file_id].present?
        report.file_id = {id: result[:qc_file_id].values.pluck(:qc_file_id)}
        reports_count = Report.where(read: false, user_id: user.id).count
        text = I18n.t('notifications.exports_noti', name: user.name)
        Notification.notify_user(user ,user ,report,"Export",nil,text)
        UserMailer.send_export_notification(user).deliver_later
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        # if reports_cron_again.email == true
        #   UserMailer.send_export_files(user, report.file_id).deliver_later
        # else
        #   # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: ""
        # end
        report.status = "completed"
        report.message = "done"
      else
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
        report.status = "crashed"
        report.message = "file_ids missing"
        msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
        SlackService.notify(msg)
        # if reports_cron_again.try(:email) == true && report.status == "crashed"
        #   UserMailer.send_error_message(user).deliver_later
        # end
      end
      reports_cron.completed!
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      p "Export Failed for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}", exc
      p "BACKTRACE: ", exc.backtrace
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      # if reports_cron_again.try(:email) == true && report.status == "crashed"
      #   UserMailer.send_error_message(user).deliver_later
      # end
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end
  end

  def export_merchant_dispute(user, first_date, second_date, offset, search_query=nil,type=nil)
    begin
      store new_status: 'queued'
      no_transaction = true
      reports_cron = CronJob.new(name: "export_#{type}",description: "Export #{type}",success: true,status: "pending")
      reports_cron.processing!
      report = Report.new(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: "Chargeback List", status: "pending",read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date, qc_file_id: {}}
      result[:wallets_id] = nil

      params = JSON.parse(search_query) if search_query.present? && search_query.class == String
      if user.merchant? && user.merchant_id.nil?
        wallet_ids=user.wallets.primary.pluck(:id)
      elsif user.merchant_id.present? && user.try(:permission).admin?
        parent = User.find_by(id: user.merchant_id)
        report.user_id = user.merchant_id
        wallet_ids = parent.wallets.primary.pluck(:id)
      end
      report.save
      wallet_ids= params.try(:[],"wallets_id").present? ? JSON.parse(params['wallets_id']) : wallet_ids
      if params.try(:[],"status").present?
        params['status'].include?('1') ||  params['status'].include?('1')
        status=params["status"]=='Lost' ? 'lost' : params["status"]=='Dispute Accepted' ? 'dispute_accepted' : params["status"]=='Sent/Pending' ? 'sent_pending' : params["status"]=='Under Review'? 'under_review' : params["status"]=='Won/Reversed'?  'won_reversed' : ''
        status=status.present? ? status : params['status'].split(",")
      else
        status= ["sent_pending","won_reversed","lost","dispute_accepted", "under_review"]
      end
      url = "/merchant/disputes"
      batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
      reports_cron_again = reports_cron
      if params.try(:[],"q").present?
        dispute_type_keys=DisputeCase.dispute_types.keys
        dispute_type_keys.select!{|a| a.include?(params[:q].try(:downcase).tr(" ","_"))}
        if dispute_type_keys.present?
          dispute_case_type=DisputeCase.dispute_types[dispute_type_keys.first]
        end
        status_keys = DisputeCase.statuses.keys
        status_keys.select!{|a| a.include?(params[:q].try(:downcase).tr(" ","_"))}
        if status_keys.present?
          dispute_case_status = DisputeCase.statuses[status_keys.first]
        end
        params[:q]={case_number_or_card_number_or_amount_eq:params[:q]}
        if dispute_case_status.present?
          params[:q]={status_eq:dispute_case_status}
        elsif dispute_case_type.present?
          params[:q]={dispute_type_eq:dispute_case_type}
        else
          params[:q]['merchant_wallet_location_business_name_cont']=  params[:q]['case_number_or_card_number_or_amount_eq']
        end
        DisputeCase.where(merchant_wallet_id:wallet_ids,status:status).where(recieved_date: first_date.to_date..second_date.to_date).ransack(params[:q].try(:merge, m: 'or')).result().order(id: "desc").find_in_batches(batch_size: batch_size.to_i).with_index do |disputeCases,index|
          transactions_csv = create_dispute_csv(disputeCases)
          count = index + 1
          no_transaction = false
          file = StringIO.new(transactions_csv)
          qc_file = QcFile.new(name: "Chargeback_list_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
          qc_file.image = file
          qc_file.image.instance_write(:content_type, 'text/csv')
          qc_file.image.instance_write(:file_name, "Chargeback_list_export.csv")
          qc_file.save
          result[:qc_file_id].merge!({"#{index}":  {file_url: qc_file.image.url, qc_file_id: qc_file.id}})
        end
      else
        DisputeCase.where(merchant_wallet_id:wallet_ids,status:status).where(recieved_date: first_date.to_date..second_date.to_date).order(id: "desc").find_in_batches(batch_size: batch_size.to_i).with_index do |disputeCases,index|
          transactions_csv = create_dispute_csv(disputeCases)
          count = index + 1
          no_transaction = false
          file = StringIO.new(transactions_csv)
          qc_file = QcFile.new(name: "Chargeback_list_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
          qc_file.image = file
          qc_file.image.instance_write(:content_type, 'text/csv')
          qc_file.image.instance_write(:file_name, "Chargeback_list_export.csv")
          qc_file.save
          result[:qc_file_id].merge!({"#{index}":  {file_url: qc_file.image.url, qc_file_id: qc_file.id}})
        end
      end

      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      elsif result[:qc_file_id].present?
        report.file_id = {id: result[:qc_file_id].values.pluck(:qc_file_id)}
        text = I18n.t('notifications.exports_noti', name: user.name)
        Notification.notify_user(user ,user ,report,"Export",nil,text)
        UserMailer.send_export_notification(user).deliver_later
        report_user_id = user.merchant_id.present? ? user.merchant_id : user.id
        reports_count = Report.where(read: false, user_id: report_user_id).count
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        report.status = "completed"
        report.message = "done"
      else
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
        report.status = "crashed"
        report.message = "file_ids missing"
        msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
        SlackService.notify(msg)
        if reports_cron_again.try(:email) == true && report.status == "crashed"
          UserMailer.send_error_message(user).deliver_later
        end
      end
      reports_cron.completed!
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      p "Export Failed for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}", exc
      p "BACKTRACE: ", exc.backtrace
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      if reports_cron_again.try(:email) == true && report.status == "crashed"
        UserMailer.send_error_message(user).deliver_later
      end
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end
  end

  def export_location_dispute(user, first_date, second_date, offset, search_query=nil,type=nil, wallet=nil)
    begin
      no_transaction = true
      reports_cron = CronJob.new(name: "export_#{type}",description: "Export #{type}",success: true,status: "pending")
      reports_cron.processing!
      report = Report.create(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: "Merchant Chargebacks", status: "pending",read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date, qc_file_id: {}}
      result[:wallets_id] = nil
      params = JSON.parse(search_query) if search_query.present? && search_query.class == String
      user=user
      if user.present? && user.merchant? && user.merchant_id.nil?
        wallets = user.wallets.primary.order('id ASC')
      elsif user.present? && user.merchant_id.present?
        wallets = user.wallets.primary.where.not(location_id: nil).order('id ASC')
      end
      wallet_ids = wallets.pluck(:id)
      chargebacks=DisputeCase.where(merchant_wallet_id: wallet_ids).where(created_at: first_date.to_date..second_date.to_date).group_by(&:merchant_wallet_id)
      file_csv = CSV.generate do |csv|
        csv << ['Account', 'Cases Recieved', ' Cases Pending',	'Cases Won',	'Cases Lost', ' Draft Cases', 'Chargeback %']
        chargebacks.each do |dispute|
          case_received= dispute.try(:second).try(:count)
          sent_pending = dispute.try(:second).select{|i| i.status=='sent_pending'}
          won_case = dispute.try(:second).select{|i| i.status=='won_reversed'}
          lost = dispute.try(:second).select{|i| i.status=='lost'}
          draft = dispute.try(:second).select{|i| i.status=='draft'}
          draft = dispute.try(:second).select{|i| i.status=='draft'}
          total_transactions = Transaction.where(receiver_wallet_id: dispute.try(:first),main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionType::Transfer3DS], status: ["approved", "refunded", "complete"]).count
          disputes_count = dispute.try(:second).count
          no_transaction=false
          if disputes_count > 0 && total_transactions > 0
            cbk_percentage=((disputes_count.to_f/total_transactions.to_f)*100).to_i
          else
            cbk_percentage = "0.00"
          end
          csv << [dispute.second.try(:first).try(:merchant_wallet).try(:location).try(:business_name),
                  case_received,
                  sent_pending.try(:count),
                  won_case.try(:count),
                  lost.try(:count),
                  draft.try(:count),
                  cbk_percentage,
          ]
        end

      end

      file = StringIO.new(file_csv)
      qc_file = QcFile.new(name: "dispute_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      download_type = type
      qc_file.image.instance_write(:file_name, "dispute_export.csv")
      qc_file.save
      result[:qc_file_id].merge!({'0':  {file_url: qc_file.image.url, qc_file_id: qc_file.id}})
      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      elsif result[:qc_file_id].present?
        report.file_id = {id: result[:qc_file_id].values.pluck(:qc_file_id)}
        report_user_id = user.merchant_id.present? ? user.merchant_id : user.id
        reports_count = Report.where(read: false, user_id: report_user_id).count
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        report.status = "completed"
        report.message = "done"
      else
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
        report.status = "crashed"
        report.message = "file_ids missing"
        msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."

      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end

  end

  def create_dispute_csv(all)
    wallets = Wallet.joins(:users).where(id: all.pluck(:merchant_wallet_id).uniq).where("users.role = ? AND users.merchant_id IS NULL", User.roles["merchant"]).select("wallets.id AS id", "wallets.location_id AS location_id", "users.id AS user_id", "users.name As user_name").group_by{|e| e.id}
    locations = Location.joins(:wallets,:fees).where("wallets.wallet_type = ? AND fees.fee_status = ? AND fees.risk = ?",0,18, 0).where(id: wallets.values.flatten.pluck(:location_id)).select("locations.id AS id", "locations.business_name AS business_name","wallets.id AS wallet_id","fees.charge_back_fee AS charge_back_fee","locations.primary_gateway_id AS payment_gateway_id").group_by{|e| e.wallet_id}
    reasons = Reason.where(id: all.pluck(:reason_id).uniq).select("id AS id", "title AS title").group_by{|e| e.id}
    transactions = Transaction.where(id: all.pluck(:chargeback_transaction_id, :transaction_id).flatten.compact.uniq).select("id AS id", "seq_transaction_id AS seq_transaction_id","total_amount AS total_amount","fee AS fee","payment_gateway_id AS payment_gateway_id", "tags as tags").group_by{|e| e.id}
    file_csv = CSV.generate do |csv|
      csv << ['Case ID', 'Account', 'Reason', 'CBK Amount', 'Original TXN Amount',  'Received Date', 'Due Date','Original TXN Date','Original TXN ID', 'Customer Name','Customer Email','Status',]
      all.each do |dispute_case|
        if dispute_case.charge_fee == true
          charge_back_fee = locations[dispute_case.merchant_wallet_id].try(:first).try(:charge_back_fee)
        end
        last4 = ""
        if dispute_case.try(:card_number).present?
          last4 = dispute_case.try(:card_number)
        else
          last4 = transactions[dispute_case.chargeback_transaction_id].try(:first).try(:last4)
        end
        tx_id = ""
        if transactions[dispute_case.chargeback_transaction_id].try(:first).try(:seq_transaction_id).present?
          tx_id = transactions[dispute_case.chargeback_transaction_id].try(:first).try(:seq_transaction_id)
        else
          transaction = Transaction.where("transactions.tags -> 'dispute_case' ->> 'case_number' = ? and  transactions.status = ?", "#{dispute_case.try(:case_number)}","Chargeback").try(:first)
          tx_id = transaction.try(:seq_transaction_id)
          if last4.blank?
            last4 = transaction.try(:tags).try(:[],"dispute_case").try(:[],"last4")
          end
        end
        if dispute_case.try(:status) == 'won_reversed'
          status = "Won/Reversed"
        elsif dispute_case.try(:status) == 'sent_pending'
          status = "Sent/Pending"
        elsif dispute_case.try(:status) == 'lost'
          status = "Lost"
        elsif dispute_case.try(:status)=='dispute_accepted'
          status = "Dispute Accepted"
        end
        merchant = User.where(id: wallets[dispute_case.merchant_wallet_id].try(:pluck, :user_id), role: "merchant", merchant_id: nil).select(:id, :name).first
        csv << [
            "#{dispute_case.try(:case_number)}",
            locations[dispute_case.merchant_wallet_id].try(:first).try(:business_name),
            reasons[dispute_case.reason_id].try(:first).try(:title),
            number_to_currency(number_with_precision(dispute_case.try(:amount).to_f,precision: 2, :delimiter => ',')),
            number_to_currency(number_with_precision(transactions[dispute_case.transaction_id].try(:first).try(:total_amount),precision: 2, :delimiter => ',')),
            dispute_case[:recieved_date].nil? ? 'NA' : dispute_case[:recieved_date].strftime("%m-%d-%Y"),
            dispute_case[:due_date].nil? ? 'NA' : dispute_case[:due_date].strftime("%m-%d-%Y"),
            dispute_case[:created_at].nil? ? 'NA' : dispute_case[:created_at].strftime("%m-%d-%Y"),
            transactions[dispute_case.transaction_id].try(:first).try(:seq_transaction_id),
            dispute_case.try(:charge_transaction).try(:sender).try(:name),
            dispute_case.try(:charge_transaction).try(:sender).try(:email),
            status
        ]

      end
    end
    return file_csv
  end

  def dispute_payment_gateway_descriptor(dispute_case,transactions,locations)
    if dispute_case.payment_gateway_id.present?
      dispute_case.payment_gateway.try(:name)
    elsif transactions[dispute_case.chargeback_transaction_id].try(:first).try(:payment_gateway_id).present?
      transactions[dispute_case.chargeback_transaction_id].try(:first).try(:payment_gateway).try(:name)
    elsif transactions[dispute_case.transaction_id].try(:first).try(:payment_gateway_id).present?
      transactions[dispute_case.transaction_id].try(:first).try(:payment_gateway_id).try(:name)
    elsif transactions[dispute_case.chargeback_transaction_id].try(:first).try(:tags).try(:[],'descriptor').present?
      transactions[dispute_case.chargeback_transaction_id].try(:first).try(:tags).try(:[],'descriptor')
    elsif transactions[dispute_case.transaction_id].try(:first).try(:tags).try(:[],'descriptor').present?
      transactions[dispute_case.transaction_id].try(:first).try(:tags).try(:[],'descriptor')
    elsif locations[dispute_case.merchant_wallet_id].try(:first).present?
      location = locations[dispute_case.merchant_wallet_id].try(:first)
      if location.payment_gateway_id.present?
        payment_gateway = PaymentGateway.find_by(id: location.payment_gateway_id)
        payment_gateway.try(:name)
      end
    else
      ""
    end
  end

  def dispute_payment_gateway(dispute_case,transactions,locations)
    if dispute_case.payment_gateway_id.present?
      dispute_case.payment_gateway.try(:key)
    elsif transactions[dispute_case.chargeback_transaction_id].try(:first).try(:payment_gateway_id).present?
      transactions[dispute_case.chargeback_transaction_id].try(:first).try(:payment_gateway).try(:key)
    elsif transactions[dispute_case.transaction_id].try(:first).try(:payment_gateway_id).present?
      transactions[dispute_case.transaction_id].try(:first).try(:payment_gateway_id).try(:key)
    elsif transactions[dispute_case.chargeback_transaction_id].try(:first).try(:tags).try(:[],'source').present?
      transactions[dispute_case.chargeback_transaction_id].try(:first).try(:tags).try(:[],'source')
    elsif transactions[dispute_case.transaction_id].try(:first).try(:tags).try(:[],'source').present?
      transactions[dispute_case.transaction_id].try(:first).try(:tags).try(:[],'source')
    elsif locations[dispute_case.merchant_wallet_id].try(:first).present?
      location = locations[dispute_case.merchant_wallet_id].try(:first)
      if location.payment_gateway_id.present?
        payment_gateway = PaymentGateway.find_by(id: location.payment_gateway_id)
        payment_gateway.try(:key)
      end
    else
      ""
    end
  end

  def export_send_check(user, first_date, second_date, offset, type,email=nil,param=nil)
    begin
      store new_status: 'queued'
      no_transaction = true
      reports_cron = CronJob.new(name: "export_#{type}",description: "Export #{type} from merchant",success: true,status: "pending")
      reports_cron.processing!
      param=param.present? ? JSON.parse(param) : ''
      actual_date=param.try(:[],'date')
      if actual_date.present?
        date_first=actual_date[0..9]
        date_first=Date.strptime(date_first, '%m/%d/%Y')
        date_second=actual_date[13..22]
        date_second=Date.strptime(date_second, '%m/%d/%Y')
      else
        date_first=first_date
        date_second=second_date
      end
      report = Report.new(user_id: user.try(:id), first_date: date_first, second_date:date_second, offset: offset, report_type: type, status: "pending", read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date}
      result[:wallets_id] = nil
      if user.present? && !user.merchant_id.nil? && user.try(:permission).admin?
        user_id = User.find(user.merchant_id).try(:id)
      else
        user_id = user.id
      end
      report.user_id = user_id
      report.save
      url = ""
      if type == TypesEnumLib::WorkerType::SendCheck
        order_type = "check"
        name = "Checks"
      elsif type == TypesEnumLib::WorkerType::InstantACH
        order_type = "instant_ach"
        name = "ACH"
      elsif type == TypesEnumLib::WorkerType::InstantPAY
        order_type = "instant_pay"
        name = "Push To Card"
      end
      report.name = name

      checks = TangoOrder.where(order_type: order_type,user_id: user_id, created_at: first_date..second_date).sort.reverse
      # store total_transactions: "#{checks_count}"
      if order_type == "instant_ach"
        checks_csv = CSV.generate do |csv|
          if user.merchant?
            csv << ['ACH ID','Creation Date','Name on Account', 'Email', 'Account last 4','ACH Amount', 'Fee', 'Total', 'DBA Name','Status','Memo']
            checks.each.with_index(1) do |transaction,count|
              no_transaction = false
              wallet = Wallet.find_by(id: transaction.wallet_id)
              dba_name = wallet.try(:location).try(:business_name) if wallet.present?
              csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),  transaction.name, transaction.recipient, transaction.account_number, number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')), number_with_precision(transaction.check_fee + (transaction.fee_perc*transaction.actual_amount/100), precision: 2, delimiter: ','), number_with_precision(transaction.amount, precision: 2, delimiter: ','), dba_name, transaction.status, transaction.description ]
            end
          else
            csv << ['ACH ID','Creation Date','Name on Account', 'Email', 'Account last 4','ACH Amount','Fee', 'Total','Status','Memo']
            checks.each.with_index(1) do |transaction,count|
              no_transaction = false
              wallet = Wallet.find_by(id: transaction.wallet_id)
              dba_name = wallet.try(:location).try(:business_name) if wallet.present?
              csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),  transaction.name, transaction.recipient, transaction.account_number, number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')),number_with_precision(transaction.check_fee + (transaction.fee_perc*transaction.actual_amount/100), precision: 2, delimiter: ','), number_with_precision(transaction.amount, precision: 2, delimiter: ','), transaction.status, transaction.description]
            end
          end
        end
      elsif order_type == "instant_pay"
        checks_csv = CSV.generate do |csv|
          if user.merchant?
            csv << ['P2C ID','Creation Date','Name on Card','Email', 'Last 4', 'P2C Amount', 'Fee', 'Total', 'DBA Name','Status','Memo']
            checks.each.with_index(1) do |transaction,count|
              no_transaction = false
              total_fee = get_total_fee(transaction.check_fee, transaction.fee_perc, transaction.actual_amount, "tango_order")
              csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.name, transaction.recipient, "'#{transaction.last4}", number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')),total_fee, total_amount(transaction.actual_amount, total_fee) ,Wallet.find_by_id(transaction.wallet_id).name, transaction.status, transaction.description]
            end
          else
            csv << ['P2C ID','Creation Date','Name on Card','Email', 'Last 4', 'P2C Amount','Fee', 'Total','Status','Memo']
            checks.each.with_index(1) do |transaction,count|
              no_transaction = false
              total_fee = get_total_fee(transaction.check_fee, transaction.fee_perc, transaction.actual_amount, "tango_order")
              csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.name, transaction.recipient, "'#{transaction.last4}", number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')),total_fee, total_amount(transaction.actual_amount, total_fee), transaction.status, transaction.description]
            end
          end
        end
      else
        checks_csv = CSV.generate do |csv|
          csv << ['Check ID','Check Number','Creation Date','Name on Check', 'Email', 'Check Amount', 'Status','Memo']
          checks.each.with_index(1) do |transaction,count|
            no_transaction = false
            csv << [transaction.id, transaction.number, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.name, transaction.recipient, number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')), transaction.status, transaction.description]
          end
        end
      end
      reports_cron_again = CronJob.find reports_cron.id
      file = StringIO.new(checks_csv)
      qc_file = QcFile.new(name: "#{type}_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      if order_type == "instant_pay"
        download_type = "Push to Card"
        qc_file.image.instance_write(:file_name, "push_to_card_export.csv")
      elsif order_type == "instant_ach"
        download_type = "instant_ach"
        qc_file.image.instance_write(:file_name, "ach_export.csv")
      else
        download_type = type
        qc_file.image.instance_write(:file_name, "#{type}_export.csv")
      end
      qc_file.save
      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      else
        report.file_id = {id: [qc_file.id]}
        result[:file_url] = qc_file.image.url
        result[:qc_file_id] = qc_file.id
        text = I18n.t('notifications.exports_noti', name: user.name)
        Notification.notify_user(user ,user ,report,"Export",nil,text)
        UserMailer.send_export_notification(user).deliver_later
        report_user_id = user.merchant_id.present? ? user.merchant_id : user.id
        reports_count = Report.where(read: false, user_id: report_user_id).count
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        store id: "#{qc_file.id}"
        store new_status: 'completed'
        reports_cron.status = "completed"
        report.status = "completed"
        report.message = "done"
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      puts "export worker error occured", exc
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      if reports_cron_again.present? && reports_cron_again.email == true && report.try(:status) == "crashed"
        UserMailer.send_error_message(user).deliver_later
      end
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_bulk_check(user, first_date, second_date, offset)
    begin
      store new_status: 'queued'
      no_transaction = true
      reports_cron = CronJob.new(name: "export_bulk_check",description: "Export bulk check from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date}
      result[:wallets_id]= nil

      report = Report.new(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: TypesEnumLib::WorkerType::BulkCheck, status: "pending", read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date}
      result[:wallets_id] = nil
      if user.present? && !user.merchant_id.nil? && user.try(:permission).admin?
        user_id = User.find(user.merchant_id).try(:id)
      else
        user_id = user.id
      end
      report.user_id = user_id
      report.save
      url = ""
      report.name = "Bulk Check"
      bulks = user.bulk_check_instances.where.not(wallet: nil).where(created_at: first_date..second_date).order(id: "desc")

      checks_count = bulks.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: checks_count}
      # store total_transactions: "#{checks_count}"
      reports_cron.status ="processing"

      bulks_csv = CSV.generate do |csv|
        csv << ['ID','Wallet', 'Creation Date', 'Total Checks', 'Checks Amount', 'Status']
        bulks.find_each.with_index(1) do |transaction,count|
          no_transaction = false
          csv << [transaction.id, show_bulk_wallet(transaction.try(:wallet)), transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), "#{transaction.bulk_checks.where(is_processed: true, status: ["Success","PENDING"]).count}"+"/"+"#{transaction.bulk_checks.count}" , number_to_currency(number_with_precision(transaction.total_amount.to_f, :precision => 2, delimiter: ',')), get_bulk_status(transaction)]
        end
      end
      file = StringIO.new(bulks_csv)
      qc_file = QcFile.new(name: "bulk_check_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "bulk_check_export.csv")
      qc_file.save
      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      else
        report.file_id = {id: [qc_file.id]}
        result[:file_url] = qc_file.image.url
        result[:qc_file_id] = qc_file.id
        text = I18n.t('notifications.exports_noti', name: user.name)
        Notification.notify_user(user ,user ,report,"Export",nil,text)
        UserMailer.send_export_notification(user).deliver_later
        report_user_id = user.merchant_id.present? ? user.merchant_id : user.id
        reports_count = Report.where(read: false, user_id: report_user_id).count
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        store id: "#{qc_file.id}"
        store new_status: 'completed'
        reports_cron.status = "completed"
        report.status = "completed"
        report.message = "done"
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      puts "export worker error occured", exc
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      if reports_cron_again.email == true && report.status = "crashed"
        UserMailer.send_error_message(user).deliver_later
      end
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_admin_transactions(user, first_date, second_date, wallet, type, offset, search_query,date=nil,trans=nil,admin_user_id=nil, specific_wallet_id=nil,send_via=nil)
    begin
      store new_status: 'queued'
      no_transaction = true
      @iso_agent_aff = nil
      @admin_merchant = nil
      @send_via = send_via
      reports_cron = CronJob.new(name: "export_admin_transactions",description: "Export Admin Transaction from merchant",success: true,status: "pending")
      report = Report.new(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: type, status: "pending",transaction_type: trans,admin_user_id: admin_user_id,read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,type: type, qc_file_id: {}}
      result[:wallets_id]= wallet
      if search_query.present?
        query = JSON.parse(search_query)
      end
      batch_size = ENV['BATCH_SIZE'] || 20000
      if type == TypesEnumLib::WorkerType::AdminUsers
        wallet_obj = Wallet.find_by(id: wallet)
        admin_user = wallet_obj.try(:users).try(:first)
        ref_no = ""
        if admin_user.present? && admin_user.ref_no.present?
          ref_no = "(#{admin_user.ref_no})"
        elsif admin_user.present?
          if admin_user.user?
            ref_no = "(C-#{admin_user.id})"
          elsif admin_user.agent?
            ref_no = "(A-#{admin_user.id})"
          elsif admin_user.affiliate?
            ref_no = "(AF-#{admin_user.id})"
          elsif admin_user.iso?
            ref_no = "(ISO-#{admin_user.id})"
          end
        end
        report.name = "#{admin_user.try(:name)} #{ref_no}"
        report.save
        if send_via.present? && send_via == "user"
          Transaction.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet', wallet: wallet).exclude_qr_refund_fee.where("created_at BETWEEN :first AND :second", first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).order(created_at: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
            no_transaction = false
            count = index + 1
            res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
            result[:qc_file_id].merge!({"#{index}": res})
          end
        else
          @iso_agent_aff = true
          BlockTransaction.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet', wallet: wallet).where("timestamp BETWEEN :first AND :second", first: first_date.to_datetime.utc, second: second_date.to_datetime.utc).order(timestamp: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
            no_transaction = false
            count = index + 1
            res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
            result[:qc_file_id].merge!({"#{index}": res})
          end
        end
      elsif type == TypesEnumLib::WorkerType::AdminMerchant
        @admin_merchant = true
        merchant_wallet = Wallet.find(wallet)
        wallet_type = merchant_wallet.wallet_type
        wallet_user = merchant_wallet.try(:users).try(:first)
        report.name = "#{merchant_wallet.location.business_name} (M-#{wallet_user.id})"
        report.save
        Transaction.exclude_qr_refund_fee.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet OR  transactions.from = :wallet_string OR transactions.to = :wallet_string',wallet: merchant_wallet.try(:id), wallet_string: "#{merchant_wallet.try(:id)}" ).where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee"],created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).order(created_at: :desc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
          no_transaction = false
          count = index + 1
          res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count,wallet_type)
          result[:qc_file_id].merge!({"#{index}": res})
        end
      elsif type == TypesEnumLib::WorkerType::Admin || type == TypesEnumLib::WorkerType::AdminMerchantProfile
        conditions = []
        if query.present? && query.reject{|k,v| v.nil? || v == "" || v == {}}.present?
          report.params = query
          conditions = transactions_search_query(query.symbolize_keys!,trans,offset)
        end
        if type == TypesEnumLib::WorkerType::Admin
          report.save
          if trans == "success"
            Transaction.exclude_qr_refund_fee.where.not(status: "pending").where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "refund"
            if query.present? && query[:type].present?
              Transaction.joins(:card).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where("tags ->> 'type' IN(?)",['refund_bank','refund']).order('transactions.created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            else
              Transaction.joins(:card).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where("tags ->> 'type' IN(?)",['refund_bank','refund']).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            end
          elsif trans == "checks"
            Transaction.where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where("main_type IN (?)",["Send Check", I18n.t("withdraw.failed_check"),"Void_Check", "Void Check",'bulk_check']).where.not(status: "pending").where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "b2b"
            Transaction.where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where("tags ->> 'type' = 'b2b_transfer'").order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "ach"
            Transaction.where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc,main_type: ['ACH','Void_Ach','ACH_deposit','Void ACH'],status: "approved").where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "push_to_card"
            Transaction.where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc,main_type: ["instant_pay","void_push_to_card"],status: "approved").where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "decline"
            if query.present? && query[:gateway].present?
              Transaction.joins(:payment_gateway).where(status: "pending", created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(main_type:["eCommerce", "Virtual Terminal", "Credit Card", "PIN Debit"]).where(conditions).order('transactions.created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,true,count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            else
              Transaction.where(conditions).where(status: "pending", created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(main_type:["eCommerce", "Virtual Terminal", "Credit Card", "PIN Debit"]).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,"decline_transaction",count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            end
          else
            Transaction.where.not(status: "pending").exclude_qr_refund_fee.where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          end
        else
          report.wallet = specific_wallet_id
          report.save
          if trans == "success"
            Transaction.exclude_qr_refund_fee.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where.not(status: "pending").where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "refund"
            if query.present? && query[:type].present?
              Transaction.joins(:card).where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where("tags ->> 'type' IN(?)",['refund_bank','refund']).order('transactions.created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            else
              Transaction.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where("tags ->> 'type' IN(?)",['refund_bank','refund']).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            end
          elsif trans == "checks"
            Transaction.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where("tags ->> 'type' IN (?)",['send_check','Void_Check','bulk_check', 'Failed Check']).where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "b2b"
            Transaction.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where("tags ->> 'type' = 'b2b_transfer'").order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          elsif trans == "decline"
            if query.present? && query[:gateway].present?
              Transaction.joins(:payment_gateway).where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where(status: "pending", created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).where(main_type:["eCommerce", "Virtual Terminal", "Credit", "PIN Debit"]).order('transactions.created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,true,count)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            else
              Transaction.where(conditions).where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where(status: "pending", created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(main_type:["eCommerce", "Virtual Terminal", "Credit", "PIN Debit"]).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
                no_transaction = false
                count = index + 1
                res = create_admin_export_file(transactions, user, first_date, second_date,offset,"decline_transaction",count,url)
                result[:qc_file_id].merge!({"#{index}": res})
              end
            end
          else
            Transaction.exclude_qr_refund_fee.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet',wallet: specific_wallet_id).where.not(status: "pending").where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).where(conditions).order('created_at DESC').find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              no_transaction = false
              count = index + 1
              res = create_admin_export_file(transactions, user, first_date, second_date,offset,nil,count)
              result[:qc_file_id].merge!({"#{index}": res})
            end
          end
        end


        # transactions = filter_admin_transactions_export(query, offset,tx_page_count,cursor,date)
      end
      reports_cron.status = "processing"
      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      else
        unless result[:qc_file_id].blank?
          report.file_id = {id: result[:qc_file_id].values.pluck(:qc_file_id)}
          reports_count = Report.where(read: false, user_id: user.id).count
          text = I18n.t('notifications.exports_noti', name: user.name)
          Notification.notify_user(user ,user ,report,"Export",nil,text)
          UserMailer.send_export_notification(user).deliver_later
          ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
          report.status = "completed"
          report.message = "done"
          reports_cron.status = "completed"
        else
          # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
          report.status = "crashed"
          report.message = "file_id_missing"
          msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report. #{report.message}"
          SlackService.notify(msg)
        end
      end
      store new_status: 'completed'
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      puts "export worker error occured", exc
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "#{TypesEnumLib::Message::Crashed}"
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report. #{report.message}"
      SlackService.notify(msg)
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_iso_summary(user, first_date, second_date, offset, search_query,type,host_name)
    begin
      store new_status: 'queued'
      reports_cron = CronJob.new(name: "export_#{type}",description: "Export #{type} from admin",success: true,status: "pending")
      report = Report.create(user_id: user.try(:id), first_date: first_date, second_date:second_date, offset: offset, report_type: type, status: "pending",read: false)
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date}
      data = {}
      details = {}
      query = JSON(search_query)
      params_date = query["date"]

      # getting isos
      if query["all"] == "true"
        isos = User.iso.select('users.id', 'users.name', 'users.company_name', 'wallets.id AS wallet_id', 'wallets.wallet_type').includes(:locations).joins(:wallets).where("wallets.wallet_type = ?", 0)
      else
        if query["iso"].present?
          iso_array = JSON(query["iso"])
          if iso_array.present?
            iso_array.each do |param|
              temp = JSON.parse(param)
              data[temp["id"]] = {name: temp["name"], wallet: temp["wallet_id"]}
            end
            isos = User.iso.where(id: data.keys).select('users.id', 'users.name', 'users.company_name', 'wallets.id AS wallet_id', 'wallets.wallet_type').includes(:locations).joins(:wallets).where("wallets.wallet_type = ?", 0)
          end
        end
        if isos.blank?
          isos = User.iso.select('users.id', 'users.name', 'users.company_name', 'wallets.id AS wallet_id', 'wallets.wallet_type').includes(:locations).joins(:wallets).where("wallets.wallet_type = ?", 0)
        end
      end
      isos_ids = isos.pluck(:wallet_id).uniq

      # getting iso transactions
      BlockTransaction.where('sender_wallet_id IN (:wallets) OR receiver_wallet_id IN (:wallets)', wallets: isos_ids).where(main_type: "Fee Transfer", timestamp: first_date..second_date).select(:id,:amount_in_cents, :sender_wallet_id, :receiver_wallet_id, :parent_id,:seq_parent_id,:tags).find_in_batches(batch_size: 5000).with_index do |txn,index|
        tx_details = calculating_iso_summary(txn,isos, first_date, second_date,params_date)
        if details.present?
          details.each do |k,v|
            if tx_details.keys.include? k
              v[:total_mtd] = v[:total_mtd] + tx_details["#{k}"][:total_mtd]
              v[:total_txn] = v[:total_txn] + tx_details["#{k}"][:total_txn]
              v[:avg_dollar] = v[:total_mtd].present? && v[:total_txn].present? ? v[:total_mtd] / v[:total_txn] : 0
              v[:iso_commission] = v[:iso_commission] + tx_details["#{k}"][:iso_commission]
              v[:iso_commission_paid] = v[:iso_commission_paid] + tx_details["#{k}"][:iso_commission_paid]
              details["#{k}"] = v
            else
              details.merge!({"#{k}" => tx_details["#{k}"]})
            end
          end
        else
          details.merge!(tx_details)
        end
      end
      iso_reports = details.values
      reports_cron.status = "processing"
      # ActionCable.server.broadcast "report_channel_#{user.id}_#{host_name}", channel_name: "report_channel_#{user.id}_#{host_name}", action: 'merchantexport' , message: {count: checks_count}


      checks_csv = CSV.generate do |csv|
        csv << ['Date Range',
                'Company Name',
                'Total Volume MTD',
                'Total # TXN',
                'Avg $ per TXN',
                'Total Low Risk Active',
                'Total Low Risk Inactive',
                'Total High Risk Active',
                'Total Hish Risk Inactive',
                'ISO Commissions',
                'ISO Commissions Paid Out',
                'Total New MID (Account)']
        iso_reports.each do |g|
          csv << [
              g[:date_range],
              g[:iso_name],
              number_with_precision(number_to_currency(g[:total_mtd]), precision: 2),
              g[:total_txn],
              number_with_precision(number_to_currency(g[:avg_dollar]), precision: 2),
              g[:low_active],
              g[:low_inactive],
              g[:high_active],
              g[:high_inactive],
              number_with_precision(number_to_currency(g[:iso_commission]), precision: 2),
              number_with_precision(number_to_currency(g[:iso_commission_paid]), precision: 2),
              g[:total_new_mid],
          ]
        end
      end
      file = StringIO.new(checks_csv)
      qc_file = QcFile.new(name: "#{type}_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "iso_summary_report.csv")

      qc_file.save
      report.file_id = {id: [qc_file.id]}
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      ActionCable.server.broadcast "report_channel_#{host_name}", channel_name: "report_channel_#{host_name}", action: 'sendemailtango', message: {user_id: user.id, qc_file: qc_file.id}
      ActionCable.server.broadcast "report_channel_#{host_name}", channel_name: "report_channel_#{host_name}", action: 'export' , message: qc_file.id
      store id: "#{qc_file.id}"
      store new_status: 'completed'
      reports_cron.status = "completed"
      report.status = "completed"
      report.message = "done"
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{user.id}_#{host_name}", channel_name: "report_channel_#{user.id}_#{host_name}", action: 'error' , message: "#{TypesEnumLib::Message::Crashed}"
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end
  end


  def calculating_iso_summary(txn,isos,first_date,second_date,params_date)
    data = {}
    sender_txn = txn.group_by{|e| e.sender_wallet_id}
    txn = txn.group_by{|e| e.receiver_wallet_id}
    isos.each do |iso|
      wallet = iso.wallet_id
      transactions = txn[wallet]
      sender_transactions = sender_txn[wallet]
      if transactions.present?
        tags = transactions.pluck(:tags)
        total_mtd = tags.try(:pluck,"main_transaction_info").compact.try(:pluck,"amount").try(:sum,&:to_f)
        total_txn = tags.try(:pluck,"main_transaction_info").compact.try(:count)

        iso_commission = SequenceLib.dollars(transactions.try(:pluck,:amount_in_cents).try(:sum,&:to_f))
        iso_commission_paid = SequenceLib.dollars(sender_transactions.try(:pluck,:amount_in_cents).try(:sum,&:to_f))
        avg_amount = total_mtd.to_f / total_txn.to_f
        locations = iso.locations.select(:id, :merchant_id,:risk)
        oauth_apps = OauthApp.select(:id, :is_block, :user_id, :created_at).where(user_id: locations.pluck(:merchant_id))
        locations_group = locations.group_by{|e| e.risk}
        low_locations = locations_group["low"].try(:pluck, :merchant_id)
        high_locations = locations_group["high"].try(:pluck, :merchant_id)
        low_merchants = oauth_apps.select{|e| e if low_locations.include?(e.user_id)}.group_by{|e| e.is_block} if low_locations.present?
        high_merchants = oauth_apps.select{|e| e if high_locations.include?(e.user_id)}.group_by{|e| e.is_block} if high_locations.present?
        high_active = high_merchants[false].try(:count) if high_merchants.present?
        high_inactive = high_merchants[true].try(:count) if high_merchants.present?
        low_active = low_merchants[false].try(:count) if low_merchants.present?
        low_inactive = low_merchants[true].try(:count) if low_merchants.present?
        total_new_mid = oauth_apps.uniq.select{|e| e if e.created_at >= first_date && e.created_at <= second_date}.count
      end
      data.merge!({"#{iso.company_name}" => {
          date_range: params_date,
          iso_name: iso.company_name,
          total_mtd: total_mtd || 0,
          total_txn: total_txn || 0,
          avg_dollar: avg_amount || 0,
          low_active: low_active || 0,
          low_inactive: low_inactive || 0,
          high_active: high_active || 0,
          high_inactive: high_inactive || 0,
          iso_commission: iso_commission || 0,
          iso_commission_paid: iso_commission_paid || 0,
          total_new_mid: total_new_mid || 0
      }})
    end
    return data
  end

  def export_transactions_list_local(user, first_date, second_date, wallet, type, offset, search_query)
    begin
      store new_status: 'queued'
      first_date = first_date.to_time.utc
      second_date = second_date.to_time.utc
      reports_cron = CronJob.new(name: "export_transactions_list_local",description: "Export Block Transaction from merchant",success: true,status: "pending")
      result = {user_id: user.try(:id),start_date: first_date,end_date:second_date,type: type}
      result[:wallets_id]= wallet
      puts "getting block transactions"
      block = BlockTransaction.where("DATE(timestamp) >= ? AND DATE(timestamp) <= ? AND main_type != ? AND main_type != ?", first_date.to_date,second_date.to_date, "Fee_Transfer", "Reserve_Money_Deposit")
      reports_cron.status = "processing"
      if block.present?
        store total_transactions: "#{block.try(:count)}"
        report_csv = CSV.generate do |csv|
          csv << ['Block Id','Date/Time','Type','Gateway','Sender Wallet id','Receiver Wallet id','Last4','Total Amount','Tx Amount','Privacy fee','Tip','Fee','Net fee','Total fee','Reserve']
          block.find_each do |block|
            csv << [block.id, block.timestamp, block.main_type, block.gateway, block.sender_wallet_id, block.receiver_wallet_id, block.last4, SequenceLib.dollars(block.amount_in_cents), SequenceLib.dollars(block.tx_amount), block.privacy_fee, SequenceLib.dollars(block.tip_cents), SequenceLib.dollars(block.fee_in_cents), block.net_fee, block.total_net, SequenceLib.dollars(block.hold_money_cents)]
          end
        end
        file = StringIO.new(report_csv)
        qc_file = QcFile.new(name: "transactions_list", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
        qc_file.image = file
        qc_file.image.instance_write(:content_type, 'text/csv')
        qc_file.image.instance_write(:file_name, "admin_transactions_export.csv")
        qc_file.save
        result[:file_url] = qc_file.image.url
        result[:qc_file_id] = qc_file.id
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'export' , message: qc_file.id
        store id: "#{qc_file.id}"
        # store file_name: "#{qc_file.image_file_name}"
        store new_status: 'completed'
      else
        store total_transactions: "0"
        store new_status: 'completed'
      end
      reports_cron.status = "completed"
    rescue => exc
      puts "export worker error occured", exc
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error' , message: "Server is busy right now please try later."
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      reports_cron.result = result
      reports_cron.reports!
    end
  end

  def export_iso_agent_report(user, first_date, second_date, wallets, offset)
    store new_status: 'queued'
    reports=[]
    wallets_ids = []
    new = {}
    User.where(id: wallets).find_each do |merchant|
      merchant.wallets.primary.find_each do |wallet|
        wallets_ids << wallet.id
      end
      merchant=merchant.name
      if new[merchant]
        new[merchant] = [{:total_transactions => 0 , :total_recieved => 0, :total_processed_amount=> 0}]
      else
        new[merchant] = [{:total_transactions => 0 , :total_recieved => 0 , :total_processed_amount=> 0}]
      end
    end
    first_date = first_date.to_time.utc
    second_date = second_date.to_time.utc
    if user.qc?
      current_wallet = user.wallets.first.try(:id)
    else
      current_wallet = user.wallets.primary.first.try(:id)
    end
    # current_wallet = user.wallets.primary.first.try(:id)
    if user.iso?
      reports = BlockTransaction.where("tags->'fee_perc'->'iso'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: first_date..second_date)
    elsif user.qc?
      reports = BlockTransaction.where("tags->'fee_perc'->'gbox'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: first_date..second_date)
    elsif user.agent?
      reports = BlockTransaction.where("tags->'fee_perc'->'agent'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: first_date..second_date)
    elsif user.affiliate?
      reports = BlockTransaction.where("tags->'fee_perc'->'affiliate'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: first_date..second_date)
    end
    unless reports.nil?
      total_amount = 0
      total_trx = 0
      total_processed_amount = 0
      txs = reports

      count = txs.count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: {count: count}
      # store total_transactions: "#{count}"
      tx = {}

      txs.find_each do |t|
        ref = t.tags
        fee_perc = ref["fee_perc"]
        if user.iso?
          # Avoid those which does not belongs to the user
          if user.email == fee_perc["iso_email"] || ( fee_perc["iso"].present? && user.email == fee_perc["iso"]["email"])
            iso_fee = 0
            if fee_perc["iso_fee"].present?
              if fee_perc["iso_total_fee"].present?
                iso_fee = fee_perc["iso_total_fee"].to_f
              else
                iso_fee = fee_perc["iso_fee"].to_f
              end
            elsif fee_perc["iso_total_fee"].present?
              iso_fee = fee_perc["iso_total_fee"].to_f
            elsif fee_perc["iso"]["amount"].present?
              iso_fee = fee_perc["iso"]["amount"].to_f
            end
            merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
            if new.key?(merchant)
              iso_transaction = new[merchant][0][:total_transactions] + 1
              iso_recieved = new[merchant][0][:total_recieved].to_f + iso_fee.to_f
              iso_profit = new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
              new[merchant] = [{:total_transactions => iso_transaction , :total_recieved => iso_recieved, :total_processed_amount => iso_profit}]
            end
          end
        elsif user.partner?
          # Avoid those which does not belongs to the user
          if user.email == fee_perc["partner_email"] || ( fee_perc["partner"].present? && user.email == fee_perc["partner"]["email"])
            partner_fee = 0
            if fee_perc["partner_fee"].present?
              if fee_perc["partner_total_fee"].present?
                partner_fee = fee_perc["partner_total_fee"].to_f
              else
                partner_fee = fee_perc["partner_fee"].to_f
              end
            elsif fee_perc["partner_total_fee"].present?
              partner_fee = fee_perc["partner_total_fee"].to_f
            elsif fee_perc["partner"]["amount"].present?
              partner_fee = fee_perc["partner"]["amount"].to_f
            end
            merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
            if new.key?(merchant)
              partner_transaction = new[merchant][0][:total_transactions] + 1
              partner_recieved = new[merchant][0][:total_recieved].to_f + partner_fee.to_f
              partner_profit = new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
              new[merchant]=[{:total_transactions => partner_transaction , :total_recieved => partner_recieved,:total_processed_amount => partner_profit}]
            end
          end

        elsif user.qc?
          # Avoid those which does not belongs to the user
          # if user.email == fee_perc["partner_email"] || ( fee_perc["partner"].present? && user.email == fee_perc["partner"]["email"])
          gbox_fee = 0
          if fee_perc["gbox_fee"].present?
            if fee_perc["gbox_total_fee"].present?
              gbox_fee = fee_perc["gbox_total_fee"].to_f
            else
              gbox_fee = fee_perc["gbox_fee"].to_f
            end
          elsif fee_perc["gbox_total_fee"].present?
            gbox_fee = fee_perc["gbox_total_fee"].to_f
          elsif fee_perc["gbox"]["amount"].present?
            gbox_fee = fee_perc["gbox"]["amount"].to_f
          end
          merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
          if new.key?(merchant)
            partner_transaction = new[merchant][0][:total_transactions] + 1
            partner_recieved = new[merchant][0][:total_recieved].to_f + gbox_fee.to_f
            partner_profit = new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
            new[merchant]=[{:total_transactions => partner_transaction , :total_recieved => partner_recieved,:total_processed_amount => partner_profit}]
          end
          # end

        elsif user.agent?
          # Avoid those which does not belongs to the user
          if user.email == fee_perc["agent_email"] || ( fee_perc["agent"].present? && user.email == fee_perc["agent"]["email"])
            agent_fee = 0
            if fee_perc["agent_fee"].present?
              if fee_perc["agent_total_fee"].present?
                agent_fee = fee_perc["agent_total_fee"].to_f
              else
                agent_fee = fee_perc["agent_fee"].to_f
              end
            elsif fee_perc["agent_total_fee"].present?
              agent_fee = fee_perc["agent_total_fee"].to_f
            elsif fee_perc["agent"]["amount"].present?
              agent_fee = fee_perc["agent"]["amount"].to_f
            end
            merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
            if new.key?(merchant)
              agent_transaction = new[merchant][0][:total_transactions] + 1
              agent_recieved = new[merchant][0][:total_recieved].to_f + agent_fee.to_f
              agent_profit = new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
              new[merchant]=[{:total_transactions => agent_transaction , :total_recieved => agent_recieved, :total_processed_amount => agent_profit}]
            end
          end
        elsif user.affiliate?
          # Avoid those which does not belongs to the user
          if user.email == fee_perc["affiliate_email"] || ( fee_perc["affiliate"].present? && user.email == fee_perc["affiliate"]["email"])
            affiliate_fee = 0
            if fee_perc["affiliate_fee"].present?
              if fee_perc["affiliate_total_fee"].present?
                affiliate_fee = fee_perc["affiliate_total_fee"].to_f
              else
                affiliate_fee = fee_perc["affiliate_fee"].to_f
              end
            elsif fee_perc["affiliate_total_fee"].present?
              affiliate_fee = fee_perc["affiliate_total_fee"].to_f
            elsif fee_perc["affiliate"]["amount"].present?
              affiliate_fee = fee_perc["affiliate"]["amount"].to_f
            end
            merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
            if new.key?(merchant)
              affiliate_transaction = new[merchant][0][:total_transactions] + 1
              affiliate_recieved = new[merchant][0][:total_recieved].to_f + affiliate_fee.to_f
              affiliate_profit = new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
              new[merchant] = [{:total_transactions => affiliate_transaction , :total_recieved => affiliate_recieved, :total_processed_amount => affiliate_profit}]
            end
          end
        end
      end

      new.map{|v|v.last}.flatten.find_each do |ss|
        unless ss.nil?
          total_amount += ss[:total_recieved]
          total_trx += ss[:total_transactions]
          total_processed_amount = total_processed_amount.to_f + ss[:total_processed_amount]
        end
      end
      reports_csv = CSV.generate do |csv|
        csv << ['Merchant', 'Total Transactions', 'Total Processed Amount', 'Total Profit']
        new.find_each do |k,v|
          csv << [k, v.first[:total_transactions], number_with_precision(number_to_currency(v.first[:total_processed_amount]), precision: 2), number_with_precision(number_to_currency(v.first[:total_recieved]), precision: 2)]
        end
        csv << ["Totals", total_trx, number_with_precision(number_to_currency(total_processed_amount), precision: 2), number_with_precision(number_to_currency(total_amount), precision: 2)]
      end
      file = StringIO.new(reports_csv)
      qc_file = QcFile.new(name: "sales_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "sales_export.csv")
      qc_file.save
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexport' , message: qc_file.id
      store id: "#{qc_file.id}"
      store new_status: 'completed'
    end
  end

  def getting_merchant_wallet_ids(user)
    locations_ids = user.attached_locations
    locations = Location.where(id: locations_ids) if locations_ids.present?
    ids = []
    ids << user.wallets.primary.pluck(:id)
    if locations.present?
      locations.find_each do |location|
        ids << location.try(:wallets).try(:primary).try(:first).try(:id)
      end
    end
    return ids.flatten.uniq.compact
  end

  def filter_admin_transactions_export(search_query, offset,tx_count = nil, cursor=nil,date=nil)
    conditions = []
    parameters = []
    if search_query.present?
      search_query = search_query.reject{|k,v| v.nil? || v == "" || v == {}}
      #= last4 & first6
      if search_query["last4"].present?
        conditions << "last4 LIKE ?"
        parameters << "%#{search_query["last4"]}%"
      end

      # if search_query["first6"].present?
      #   conditions << "first6 LIKE ?"
      #   parameters << "%#{search_query["first6"]}%"
      # end

      #= datetime
      time = parse_time(search_query)
      date = search_query["date"].present? ? parse_daterange(search_query["date"]) : nil
      date = parse_daterange(search_query["datie"]) if search_query["datie"].present? && date.blank?
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, offset).to_datetime.rfc3339,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, offset).to_datetime.rfc3339
        }
        date[:first_date] = "#{date[:first_date].first(11)}#{time.first}#{date[:first_date].last(9)}"
        date[:second_date] = "#{date[:second_date].first(11)}#{time.second}#{date[:second_date].last(9)}"
      elsif search_query["time1"].present? || search_query["time2"].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, offset).to_datetime.rfc3339,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, offset).to_datetime.rfc3339
        }
        date[:first_date] = "#{date[:first_date].first(11)}#{time.first}#{date[:first_date].last(9)}"
        date[:second_date] = "#{date[:second_date].first(11)}#{time.second}#{date[:second_date].last(9)}"
      end
      if date.present? && date.try(:[],:first_date).present?
        conditions << "created_at >= ?"
        parameters << date[:first_date]
      end
      if date.present? && date.try(:[],:second_date).present?
        conditions << "created_at <= ?"
        parameters << date[:second_date]
      end
      #= Block ID
      if search_query["block_id"].present?
        conditions << "sequence_id LIKE ?"
        parameters << "%#{search_query["block_id"]}"
      end
      #= sender & receiver Wallet ID
      if search_query["sender"].present?
        conditions << "sender_wallet_id = ?"
        parameters << "#{search_query["sender"]}"
      end
      if search_query["receiver"].present?
        conditions << "receiver_wallet_id = ?"
        parameters << "#{search_query["receiver"]}"
      end
      #= amount
      if search_query["amount"].present?
        conditions << "amount LIKE ?"
        parameters << "%#{SequenceLib.cents(search_query["amount"])}"
      end
      #= type
      type = ''
      if search_query["type"].present?
        if search_query["type"].kind_of?(Array)
          type = search_query["type"]
        else
          type = JSON.parse(search_query["type"])
        end
      end
      if type.present?
        conditions << "(tags ->> 'type' IN(?) OR tags ->> 'sub_type' IN(?))"
        parameters << type
        parameters << type
      end

      #= gateway
      gateway = ''
      if search_query["gateway"].present?
        if search_query["gateway"].kind_of?(Array)
          gateway = search_query["gateway"]
        else
          gateway = JSON.parse(search_query["gateway"])
        end
      end
      if gateway.present?
        conditions << "tags ->> 'source' IN(?)"
        parameters << gateway
      end

      #= city & state
      location_city_id = Location.find_by(city: "#{search_query["city"].try(:downcase)}").try(:wallets).try(:primary).try(:first).try(:id) if search_query["city"].present?
      location_state_id = Location.find_by(state: "#{search_query["state"].try(:downcase)}").try(:wallets).try(:primary).try(:first).try(:id) if search_query["state"].present?
      if location_city_id.present?
        conditions << "sender_wallet_id = ? OR receiver_wallet_id = ?"
        parameters << "#{location_city_id}"
      end
      if location_state_id.present?
        conditions << "sender_wallet_id = ? OR receiver_wallet_id = ?"
        parameters << "#{location_state_id}"
      end

      conditions = [conditions.join(" AND "), *parameters] if conditions.present?
      return BlockTransaction.all.where(conditions).order('created_at DESC') #.per(page_size)
    else
      if date.present?
        parsed_date = parse_daterange(date)
        if parsed_date.present?
          date_hash = {
              first_date: Time.new(parsed_date.first[:year], parsed_date.first[:month], parsed_date.first[:day], 00,00,00, offset).to_datetime.rfc3339,
              second_date: Time.new(parsed_date.second[:year], parsed_date.second[:month], parsed_date.second[:day], 23,59,59, offset).to_datetime.rfc3339
          }
        end
        if date_hash.present? && date_hash.try(:[],:first_date).present?
          conditions << "created_at >= ?"
          parameters << date_hash[:first_date]
        end
        if date_hash.present? && date_hash.try(:[],:second_date).present?
          conditions << "created_at <= ?"
          parameters << date_hash[:second_date]
        end
        conditions = [conditions.join(" AND "), *parameters] if conditions.present?
      end
      return BlockTransaction.all.where(conditions).order('created_at DESC') #.per(page_size)
    end
  end

  def create_admin_export_file(transactions, user, first_date, second_date,offset,decline=nil,count=1,wallet_type)
    transactions_csv = CSV.generate do |csv|
      if @iso_agent_aff
        if @send_via == 'partner' || @send_via == 'qc' || @send_via == 'support' || @send_via == "iso" || @send_via == "affiliate" || @send_via == "agent"
          csv << ['Txn ID', 'Date/Time', 'Main Type', 'Merchant Name', 'DBA Name', 'Merchant Txn Type', 'Transaction Amount', 'Commission', 'Sender','Receiver']
          transactions.each.with_index(1) do |transaction, idx|
            if ["Send Check", "ACH Deposit", "Instant Pay"].include? transaction.main_type
              comission = "--"
            else
              comission = number_to_currency(number_with_precision(SequenceLib.dollars(transaction.amount_in_cents), :precision => 2, delimiter: ','))
            end
            merchant_txn_type = show_iso_local_type(transaction)
            csv << [
                "#{transaction.sequence_id}",
                transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                main_type_format(transaction.main_type),
                transaction.merchant_name || transaction.tags.try(:[], "merchant").try(:[], "name") || "--",
                transaction.location_dba_name || transaction.tags.try(:[],"location").try(:[], "business_name") || "--",
                merchant_txn_type.try(:downcase) == "ach deposit" ? "ACH Deposit" : merchant_txn_type,
                # transaction.try(:tx_type)== "instant_pay" ? "Push to Card" : transaction.main_type.tr("_", " "),
                number_to_currency(number_with_precision(get_main_transaction_amount(transaction), :precision => 2, delimiter: ',')),
                comission,
                change_wallet_name(transaction.sender_name || transaction.try(:sender).try(:name) || transaction.sender_wallet.try(:users).try(:first).try(:name) || transaction.sender_wallet.try(:name) || ""),
                change_wallet_name(transaction.receiver_name || transaction.try(:receiver).try(:name) || transaction.receiver_wallet.try(:users).try(:first).try(:name) || transaction.receiver_wallet.try(:name) || ""),
            ]
          end
        else
          csv << ['Txn ID', 'Date/Time', 'Sender name', 'Receiver name', 'DBA Category', 'Type', 'Descriptor', 'First6', 'Last4', 'C.C Type','Total Amount', 'Tx Amount', 'Privacy Fee', 'Tip', 'Fee', 'Net Fee', 'Total Net', 'Reserve Amount', 'Discount','Gbox fee',	'ISO fee', 'Agent fee',	'Affiliate fee']
          transactions.each.with_index(1) do |transaction, idx|
            if decline
              descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : (transaction.try(:tags).try(:[],"descriptor").present? ? transaction.try(:tags).try(:[],"descriptor") : "--" )
            else
              descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : "Quickcard"
            end
            if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
              total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
            else
              if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
                total_amount = "+ #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
              else
                total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_to_currency(number_with_precision(SequenceLib.dollars(transaction.amount_in_cents), precision: 2))}"
              end
            end
            symbol = transaction.retire? || user.wallets.primary.ids.include?(transaction.sender_wallet_id.to_i) ?  "-" : "+"
            g_box_fee = getting_fee_from_local_trans(transaction,"gbox")
            iso_fee = getting_fee_from_local_trans(transaction,"iso")
            agent_fee = getting_fee_from_local_trans(transaction,"agent")
            affiliate_fee = getting_fee_from_local_trans(transaction,"affiliate")
            # iso_fee = transaction.iso_fee
            # agent_fee = transaction.agent_fee
            # affiliate_fee = transaction.affiliate_fee
            # if agent_fee.present?
            #   iso_fee = iso_fee - agent_fee
            #   if affiliate_fee.present?
            #     agent_fee = agent_fee - affiliate_fee
            #   end
            # end
            csv << [
                "#{transaction.sequence_id}",
                transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                transaction.try(:from) == "Credit" ? transaction.try(:from) : (transaction.try(:sender_name) || transaction.sender.present? ? transaction.sender.try(:name) : nil),
                transaction.try(:receiver_name) || transaction.receiver.present? ? transaction.receiver.try(:name) : transaction.try(:to).present? ? Wallet.find_by(id: transaction.try(:to).try(:to_i)).try(:[],'name') : nil,
                transaction.tags.try(:[],'location').try(:[],'id').present? ? get_category_name(transaction) : nil,
                main_type_format(transaction.main_type),
                (transaction.main_type == "void_push_to_card" || transaction.main_type == "Void Push To Card") ? "Quickcard" : descriptor,
                transaction.first6,
                transaction.last4.present? ? "'#{transaction.last4}" : nil,
                transaction.card.present? ? transaction.card.card_type : nil,
                total_amount,
                number_to_currency(number_with_precision(transaction.tx_amount / 100.00, :precision => 2, delimiter: ',')),
                (number_to_currency(number_with_precision(transaction.privacy_fee, :precision => 2, delimiter: ','))) || '$0.00',
                (number_to_currency(number_with_precision(SequenceLib.dollars(transaction.tip_cents), :precision => 2, delimiter: ','))) || '$0.00',
                (number_to_currency(number_with_precision(SequenceLib.dollars(transaction.fee_in_cents), :precision => 2, delimiter: ','))) || '$0.00',
                transaction.net_fee.to_f > 0 ? "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}" : "$0.00",
                # "- #{number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}",
                number_to_currency(number_with_precision(transaction.total_net, :precision => 2, delimiter: ',')),
                (number_to_currency(number_with_precision(SequenceLib.dollars(transaction.hold_money_cents), :precision => 2, delimiter: ','))) || '$0.00',
                (number_to_currency(number_with_precision(transaction.tags.try(:discount), :precision => 2, delimiter: ',')) || '$0.00'),
                number_with_precision(number_to_currency(g_box_fee), :precision => 2, delimiter: ','),
                number_with_precision(number_to_currency(iso_fee), :precision => 2, delimiter: ','),
                number_with_precision(number_to_currency(agent_fee), :precision => 2, delimiter: ','),
                number_with_precision(number_to_currency(affiliate_fee), :precision => 2, delimiter: ',')
            ]
          end
        end
      elsif @admin_merchant
        if wallet_type == "reserve"
        csv << ['Txn ID', 'Date/Time', 'Sender name', 'Receiver name', 'DBA Category', 'Type', 'Descriptor', 'First6', 'Last4', 'C.C Type','Total Amount']
        transactions.each.with_index(1) do |transaction,count|
          if decline
            descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : (transaction.try(:tags).try(:[],"descriptor").present? ? transaction.try(:tags).try(:[],"descriptor") : "--" )
          else
            descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : "Quickcard"
          end
          if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
            total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
          else
            if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
              total_amount = "+ #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
            else
              total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
            end
          end
          # g_box_fee = getting_fee_from_local_trans(transaction,"gbox")
          # iso_fee = getting_fee_from_local_trans(transaction,"iso")
          # agent_fee = getting_fee_from_local_trans(transaction,"agent")
          # affiliate_fee = getting_fee_from_local_trans(transaction,"affiliate")
          # iso_fee = transaction.iso_fee
          # agent_fee = transaction.agent_fee
          # affiliate_fee = transaction.affiliate_fee
          # if agent_fee.present?
          #   iso_fee = iso_fee - agent_fee
          #   if affiliate_fee.present?
          #     agent_fee = agent_fee - affiliate_fee
          #   end
          # end
          csv << [
              "#{transaction.seq_transaction_id}",
              transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
              # transaction.try(:from) == "Credit" ? transaction.try(:from) : (transaction.try(:sender_name) || transaction.sender.present? ? transaction.sender.try(:name) : nil),
              # transaction.try(:receiver_name) || transaction.receiver.present? ? transaction.receiver.try(:name) : transaction.try(:to).present? ? Wallet.find_by(id: transaction.try(:to).try(:to_i)).try(:[],'name') : nil,
              transaction.try(:from) == "Credit" ? transaction.try(:from) :transaction.sender_name,
              transaction.receiver_name,
              transaction.tags.try(:[],'location').try(:[],'id').present? ? get_category_name(transaction) : nil,
              main_type_format(transaction.main_type),
              (transaction.main_type == "void_push_to_card" || transaction.main_type == "Void Push To Card") ? "Quickcard" : descriptor,
              transaction.first6,
              transaction.last4.present? ? "'#{transaction.last4}" : nil,
              transaction.card.present? ? transaction.card.card_type : nil,
              total_amount,
              # number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),
              # (number_to_currency(number_with_precision(transaction.privacy_fee, :precision => 2, delimiter: ','))) || '$0.00',
              # (number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ','))) || '$0.00',
              # (number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ','))) || '$0.00',
              # transaction.net_fee.to_f > 0 ? "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}" : "$0.00",
              # # "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}",
              # # number_to_currency(number_with_precision(transaction.net_amount, :precision => 2, delimiter: ',')),
              # # (number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))) || '$0.00',
              # # (number_to_currency(number_with_precision(transaction.discount, :precision => 2, delimiter: ',')) || '$0.00'),
              # # number_with_precision(number_to_currency(g_box_fee), :precision => 2, delimiter: ','),
              # # number_with_precision(number_to_currency(iso_fee), :precision => 2, delimiter: ','),
              # # number_with_precision(number_to_currency(agent_fee), :precision => 2, delimiter: ','),
              # # number_with_precision(number_to_currency(affiliate_fee), :precision => 2, delimiter: ',')
          ]
        end
        elsif wallet_type == "tip"
          csv << ['Txn ID', 'Date/Time', 'Sender name', 'Receiver name', 'DBA Category', 'Type', 'Descriptor', 'First6', 'Last4', 'C.C Type','Total Amount', 'Tx Amount', 'Privacy Fee', 'Tip']
          transactions.each.with_index(1) do |transaction,count|
            if decline
              descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : (transaction.try(:tags).try(:[],"descriptor").present? ? transaction.try(:tags).try(:[],"descriptor") : "--" )
            else
              descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : "Quickcard"
            end
            if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
              total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
            else
              if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
                total_amount = "+ #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
              else
                total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
              end
            end
            # g_box_fee = getting_fee_from_local_trans(transaction,"gbox")
            # iso_fee = getting_fee_from_local_trans(transaction,"iso")
            # agent_fee = getting_fee_from_local_trans(transaction,"agent")
            # affiliate_fee = getting_fee_from_local_trans(transaction,"affiliate")
            # iso_fee = transaction.iso_fee
            # agent_fee = transaction.agent_fee
            # affiliate_fee = transaction.affiliate_fee
            # if agent_fee.present?
            #   iso_fee = iso_fee - agent_fee
            #   if affiliate_fee.present?
            #     agent_fee = agent_fee - affiliate_fee
            #   end
            # end
            csv << [
                "#{transaction.seq_transaction_id}",
                transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                # transaction.try(:from) == "Credit" ? transaction.try(:from) : (transaction.try(:sender_name) || transaction.sender.present? ? transaction.sender.try(:name) : nil),
                # transaction.try(:receiver_name) || transaction.receiver.present? ? transaction.receiver.try(:name) : transaction.try(:to).present? ? Wallet.find_by(id: transaction.try(:to).try(:to_i)).try(:[],'name') : nil,
                transaction.try(:from) == "Credit" ? transaction.try(:from) :transaction.sender_name,
                transaction.receiver_name,
                transaction.tags.try(:[],'location').try(:[],'id').present? ? get_category_name(transaction) : nil,
                main_type_format(transaction.main_type),
                (transaction.main_type == "void_push_to_card" || transaction.main_type == "Void Push To Card") ? "Quickcard" : descriptor,
                transaction.first6,
                transaction.last4.present? ? "'#{transaction.last4}" : nil,
                transaction.card.present? ? transaction.card.card_type : nil,
                total_amount,
            number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),
            (number_to_currency(number_with_precision(transaction.privacy_fee, :precision => 2, delimiter: ','))) || '$0.00',
            (number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ','))) || '$0.00',
            # (number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ','))) || '$0.00',
            # transaction.net_fee.to_f > 0 ? "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}" : "$0.00",
            # # "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}",
            # # number_to_currency(number_with_precision(transaction.net_amount, :precision => 2, delimiter: ',')),
            # # (number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))) || '$0.00',
            # # (number_to_currency(number_with_precision(transaction.discount, :precision => 2, delimiter: ',')) || '$0.00'),
            # # number_with_precision(number_to_currency(g_box_fee), :precision => 2, delimiter: ','),
            # # number_with_precision(number_to_currency(iso_fee), :precision => 2, delimiter: ','),
            # # number_with_precision(number_to_currency(agent_fee), :precision => 2, delimiter: ','),
            # # number_with_precision(number_to_currency(affiliate_fee), :precision => 2, delimiter: ',')
            ]
          end
        else
          csv << ['Txn ID', 'Date/Time', 'Sender name', 'Receiver name', 'DBA Category', 'Type', 'Descriptor', 'First6', 'Last4', 'C.C Type','Total Amount', 'Tx Amount', 'Privacy Fee', 'Tip', 'Fee', 'Net Fee', 'Total Net', 'Reserve Amount', 'Discount','Gbox fee',	'ISO fee', 'Agent fee',	'Affiliate fee']
          transactions.each.with_index(1) do |transaction,count|
            if decline
              descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : (transaction.try(:tags).try(:[],"descriptor").present? ? transaction.try(:tags).try(:[],"descriptor") : "--" )
            else
              descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : "Quickcard"
            end
            if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
              total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
            else
              if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
                total_amount = "+ #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
              else
                total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
              end
            end
            g_box_fee = getting_fee_from_local_trans(transaction,"gbox")
            iso_fee = getting_fee_from_local_trans(transaction,"iso")
            agent_fee = getting_fee_from_local_trans(transaction,"agent")
            affiliate_fee = getting_fee_from_local_trans(transaction,"affiliate")
            # iso_fee = transaction.iso_fee
            # agent_fee = transaction.agent_fee
            # affiliate_fee = transaction.affiliate_fee
            # if agent_fee.present?
            #   iso_fee = iso_fee - agent_fee
            #   if affiliate_fee.present?
            #     agent_fee = agent_fee - affiliate_fee
            #   end
            # end
            csv << [
                "#{transaction.seq_transaction_id}",
                transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
                transaction.try(:from) == "Credit" ? transaction.try(:from) : (transaction.try(:sender_name) || transaction.sender.present? ? transaction.sender.try(:name) : nil),
                transaction.try(:receiver_name) || transaction.receiver.present? ? transaction.receiver.try(:name) : transaction.try(:to).present? ? Wallet.find_by(id: transaction.try(:to).try(:to_i)).try(:[],'name') : nil,
                transaction.tags.try(:[],'location').try(:[],'id').present? ? get_category_name(transaction) : nil,
                main_type_format(transaction.main_type),
                (transaction.main_type == "void_push_to_card" || transaction.main_type == "Void Push To Card") ? "Quickcard" : descriptor,
                transaction.first6,
                transaction.last4.present? ? "'#{transaction.last4}" : nil,
                transaction.card.present? ? transaction.card.card_type : nil,
                total_amount,
                number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),
                (number_to_currency(number_with_precision(transaction.privacy_fee, :precision => 2, delimiter: ','))) || '$0.00',
                (number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ','))) || '$0.00',
                (number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ','))) || '$0.00',
                transaction.net_fee.to_f > 0 ? "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}" : "$0.00",
                # "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}",
                number_to_currency(number_with_precision(transaction.net_amount, :precision => 2, delimiter: ',')),
                (number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))) || '$0.00',
                (number_to_currency(number_with_precision(transaction.discount, :precision => 2, delimiter: ',')) || '$0.00'),
                number_with_precision(number_to_currency(g_box_fee), :precision => 2, delimiter: ','),
                number_with_precision(number_to_currency(iso_fee), :precision => 2, delimiter: ','),
                number_with_precision(number_to_currency(agent_fee), :precision => 2, delimiter: ','),
                number_with_precision(number_to_currency(affiliate_fee), :precision => 2, delimiter: ',')
            ]
          end
          end
      elsif decline || decline == "decline_transaction"
        csv << ['Txn ID', 'Date/Time', 'Sender name', 'Receiver name', 'DBA Category', 'Type',"Status", 'Descriptor', 'First6', 'Last4', 'C.C Type','Total Amount', 'Tx Amount', 'Privacy Fee', 'Tip', 'Fee', 'Net Fee', 'Total Net', 'Reserve Amount', 'Discount','Gbox fee',	'ISO fee', 'Agent fee',	'Affiliate fee', 'Clerk ID']
        transactions.each.with_index(1) do |transaction,count|
          if decline
            descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : (transaction.try(:tags).try(:[],"descriptor").present? ? transaction.try(:tags).try(:[],"descriptor") : "--" )
          else
            descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : "Quickcard"
          end
          if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
            total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
          else
            if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
              total_amount = "+ #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
            else
              total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
            end
          end
          g_box_fee = getting_fee_from_local_trans(transaction ,"gbox")
          iso_fee = getting_fee_from_local_trans(transaction,"iso")
          agent_fee = getting_fee_from_local_trans(transaction,"agent")
          affiliate_fee = getting_fee_from_local_trans(transaction,"affiliate")
          # if agent_fee.present?
          #   iso_fee = iso_fee - agent_fee
          #   if affiliate_fee.present?
          #     agent_fee = agent_fee - affiliate_fee
          #   end
          # end
          csv << [
              "#{transaction.id}",
              # count,
              transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
              transaction.try(:from) == "Credit" ? transaction.try(:from) : (transaction.try(:sender_name) || transaction.sender.present? ? transaction.sender.try(:name) : nil),
              transaction.try(:receiver_name) || transaction.receiver.present? ? transaction.receiver.try(:name) : transaction.try(:to).present? ? Wallet.find_by(id: transaction.try(:to).try(:to_i)).try(:[],'name') : nil,
              transaction.tags.try(:[],'location').try(:[],'id').present? ? get_category_name(transaction) : nil,
              main_type_format(transaction.main_type),
              transaction.status = status_format(transaction.status),
              (transaction.main_type == "void_push_to_card" || transaction.main_type == "Void Push To Card") ? "Quickacrd" : descriptor,
              # get_report_source(transaction),
              # transaction.receiver_wallet_id,
              transaction.first6,
              transaction.last4.present? ? "'#{transaction.last4}" : nil,
              transaction.card.present? ? transaction.card.card_type : nil,
              total_amount,
              number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),
              (number_to_currency(number_with_precision(transaction.privacy_fee, :precision => 2, delimiter: ','))) || '$0.00',
              (number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ','))) || '$0.00',
              (number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ','))) || '$0.00',
              transaction.net_fee.to_f > 0 ? "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}" : "$0.00",
              number_to_currency(number_with_precision(transaction.net_amount, :precision => 2, delimiter: ',')),
              (number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))) || '$0.00',
              (number_to_currency(number_with_precision(transaction.discount, :precision => 2, delimiter: ',')) || '$0.00'),
              number_to_currency(number_with_precision(transaction.gbox_fee, :precision => 2, delimiter: ',')) || '$0.00',
              number_to_currency(number_with_precision(transaction.iso_fee, :precision => 2, delimiter: ',')) || '$0.00',
              number_to_currency(number_with_precision(transaction.agent_fee, :precision => 2, delimiter: ',')) || '$0.00',
              number_to_currency(number_with_precision(transaction.affiliate_fee, :precision => 2, delimiter: ',')) || '$0.00',
              transaction.clerk_id
          ]
        end
      else
        csv << ['Txn ID', 'Date/Time', 'Sender name', 'Receiver name', 'DBA Category', 'Type',"Status", 'Descriptor', 'First6', 'Last4', 'C.C Type','Total Amount', 'Tx Amount', 'Privacy Fee', 'Tip', 'Fee', 'Net Fee', 'Total Net', 'Reserve Amount', 'Discount','Gbox fee',	'ISO fee', 'Agent fee',	'Affiliate fee', 'Clerk ID']
        transactions.each.with_index(1) do |transaction,count|
          if decline
            descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : (transaction.try(:tags).try(:[],"descriptor").present? ? transaction.try(:tags).try(:[],"descriptor") : "--" )
          else
            descriptor = transaction.try(:payment_gateway).present? ? transaction.try(:payment_gateway).try(:name) : "Quickcard"
          end
          if transaction.main_type=='cbk_hold' || transaction.main_type=='cbk_won' || transaction.main_type=='retrievel_fee' || transaction.main_type=='cbk_lost' || transaction.main_type=='CBK Lost Retire' || transaction.main_type=='cbk_fee'
            total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.amount.to_f), precision: 2)}"
          else
            if (transaction.main_type=='Void Check' || transaction.main_type=='Void ACH') && (transaction.action!='retire')
              total_amount = "+ #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
            else
              total_amount = "#{transaction.action.present? && (transaction.action=='retire' || transaction.action=='withdraw') ? '-' : ''} #{number_with_precision(number_to_currency(transaction.total_amount.to_f), precision: 2)}"
            end
          end
          if transaction.try(:sender).try(:iso?) || transaction.try(:sender).try(:agent?) || transaction.try(:sender).try(:affiliate?) || (transaction.try(:sender).try(:support?) && transaction.try(:tags).try(:[], "merchant").nil?)
            g_box_fee = number_with_precision(number_to_currency(transaction.try(:fee)), precision: 2)
          else
            g_box_fee = number_to_currency(getting_fee_from_local_trans(transaction ,"gbox"))
          end
          iso_fee = getting_fee_from_local_trans(transaction,"iso")
          agent_fee = getting_fee_from_local_trans(transaction,"agent")
          affiliate_fee = getting_fee_from_local_trans(transaction,"affiliate")
          # if agent_fee.present?
          #   iso_fee = iso_fee - agent_fee
          #   if affiliate_fee.present?
          #     agent_fee = agent_fee - affiliate_fee
          #   end
          # end
          csv << [
              "#{transaction.seq_transaction_id}",
              # count,
              transaction.timestamp.present? ? transaction.timestamp.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p") : transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),
              transaction.try(:sender_wallet).try(:location).try(:business_name) || transaction.sender_wallet.try(:name) || "--",
              transaction.try(:receiver_wallet).try(:location).try(:business_name) || transaction.try(:receiver).try(:name) || "--",

              #transaction.try(:receiver_name) || transaction.receiver.present? ? transaction.receiver.try(:name) : transaction.try(:to).present? ? Wallet.find_by(id: transaction.try(:to).try(:to_i)).try(:[],'name') : nil,
              get_category_name(transaction) || "---",
              main_type_format(transaction.main_type),
              transaction.status = status_format(transaction.status),
              (transaction.main_type == "void_push_to_card" || transaction.main_type == "Void Push To Card") ? "Quickcard" :  descriptor,
              # get_report_source(transaction),
              # transaction.receiver_wallet_id,
              transaction.first6,
              transaction.last4.present? ? "'#{transaction.last4}" : nil,
              transaction.card.present? ? transaction.card.card_type : nil,
              total_amount,
              number_to_currency(number_with_precision(transaction.amount, :precision => 2, delimiter: ',')),
              (number_to_currency(number_with_precision(transaction.privacy_fee, :precision => 2, delimiter: ','))) || '$0.00',
              (number_to_currency(number_with_precision(transaction.tip, :precision => 2, delimiter: ','))) || '$0.00',
              (number_to_currency(number_with_precision(transaction.fee, :precision => 2, delimiter: ','))) || '$0.00',
              transaction.net_fee.to_f > 0 ? "- #{ number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}" : "$0.00",
              # "- #{number_to_currency(number_with_precision(transaction.net_fee, :precision => 2, delimiter: ','))}",
              number_to_currency(number_with_precision(transaction.net_amount, :precision => 2, delimiter: ',')),
              (number_to_currency(number_with_precision(transaction.reserve_money, :precision => 2, delimiter: ','))) || '$0.00',
              (number_to_currency(number_with_precision(transaction.discount, :precision => 2, delimiter: ',')) || '$0.00'),
              number_with_precision(g_box_fee, :precision => 2, delimiter: ','),
              number_with_precision(iso_fee, :precision => 2, delimiter: ','),
              number_with_precision(agent_fee, :precision => 2, delimiter: ','),
              number_with_precision(affiliate_fee, :precision => 2, delimiter: ','),
              transaction.clerk_id
          ]
        end
      end
    end
    file = StringIO.new(transactions_csv)
    qc_file = QcFile.new(name: "admin_transactions_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
    qc_file.image = file
    qc_file.image.instance_write(:content_type, 'text/csv')
    qc_file.image.instance_write(:file_name, "admin_transactions_export.csv")
    qc_file.save
    return {file_url: qc_file.image.url, qc_file_id: qc_file.id}
  end

  def get_category_name(tran=nil)
    location_id = tran.receiver_wallet.try(:location)
    if location_id.present?
      category_name = location_id.category.try(:name)
    end
    if category_name.blank? && location_id.blank? && tran.tags.try(:[], "location").try(:[], "id").present?
      location_id = tran.tags.try(:[],'location').try(:[],'id')
      category_name = Category.joins(:locations).where("locations.id = ?", location_id).pluck("categories.name").last
    end
    return category_name
  end
  def status_format(status)
    if status == "pending"
      return "declined"
    elsif status == "cbk_fee"
      return "CBK Fee"
    elsif status.present?
      return status
    else
      return nil
    end
  end

  def search(params)
    if params[:query].present? && params[:query].select{ |k,v| v.present? }.present?
      @merchants = []
      params[:query]["role"] = 'merchant'
      @key = nil
      @location_keys=nil

      if params[:query]["name"].present?
        @key = "name ILIKE :name"
      end
      if params[:query]["email"].present?
        if @key.nil?
          @key = "email ILIKE :email"
        else
          @key += " Or email ILIKE :email"
        end
      end
      if params[:query]["phone_number"].present?
        if @key.nil?
          @key = "phone_number ILIKE :phone_number"
        else
          @key += " Or phone_number ILIKE :phone_number"
        end
      end
      if params[:query]["ref_no"].present?
        params[:query]["ref_no"] = params[:query]["ref_no"].try(:strip)
        if @key.nil?
          @key = "ref_no ILIKE :ref_no"
        else
          @key += " Or ref_no ILIKE :ref_no"
        end
      end
      if params[:query]["location_id"].present?
        @location_keys = "location_secure_token ILIKE :location_secure_token"
      end
      if params[:query]["location_dba_name"].present?
        if @location_keys.nil?
          @location_keys = "business_name ILIKE :business_name"
        else
          @location_keys += " Or business_name ILIKE :business_name"
        end
      end
      if params[:from_tickets].present?
        @key = nil
        if params[:query]["name"].present?
          @key = "name ILIKE :name"
        end
        if params[:query]["ref_no"].present?
          if @key.nil?
            @key = "ref_no ILIKE :ref_no"
          else
            @key += " AND ref_no ILIKE :ref_no"
          end
        end
      end
      if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present? || params[:query]["role"].present? || params[:query]["last4"].present? || params[:query]["wallet_id"].present?
        if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present?
          @merchants = User.merchant.where(low_risk: [nil,false]).complete_profile_users.where(@key,  name: "%#{params[:query]["name"].try(:strip)}%", email: "%#{params[:query]["email"].try(:strip)}%",phone_number: "%#{params[:query]["phone_number"].try(:strip)}%",ref_no: "%#{params[:query]["ref_no"].try(:strip)}%").order(created_at: :asc)
        end
        if params[:query]["wallet_id"].present?
          if Wallet.where(id: params[:query]["wallet_id"].try(:strip).to_i).present?
            w = Wallet.find(params[:query]["wallet_id"].try(:strip).to_i).users.where(low_risk: [nil,false])
            @merchants = @merchants | w
          end
        end
        if params[:query]["location_id"].present? || params[:query]["location_dba_name"].present?
          location_merchant_ids = Location.where(@location_keys, location_secure_token: "%#{params[:query]["location_id"].try(:strip)}%", business_name: "%#{params[:query]["location_dba_name"].try(:strip)}%").pluck(:merchant_id)
          if location_merchant_ids.present?
            @user_loc=User.complete_profile_users.where(low_risk: [nil,false]).where('id IN (?)', location_merchant_ids)
            if @user_loc.present?
              @merchants = @merchants | @user_loc
            end
          end
        end
      end
      if params[:query]["gateway"].present?
        w = User.merchant.complete_profile_users.where(low_risk: [nil,false],primary_gateway_id: params[:query]["gateway"].try(:strip).to_i,merchant_id: nil)
        @merchants = @merchants | w if w.present?
      end
    end
    return @merchants
  end

  def search_merchant_users(params)

    conditions = []
    parameters = []

    if params[:query]["name"].present?
      conditions << "(parent_merchants_users.name ILIKE ?)"
      parameters << "#{params[:query]["name"].downcase}%".to_s
    end

    if params[:query]["phone_number"].present?
      conditions << "(users.phone_number ILIKE ?)"
      parameters << "#{params[:query]["phone_number"].downcase}%".to_s
    end

    if params[:query]["sub_merchant_id"].present?
      conditions << "(users.id = ?)"
      parameters << params[:query]["sub_merchant_id"].to_i
    end

    if params[:query]["email"].present?
      conditions << "(users.email ILIKE ?)"
      parameters << "#{params[:query]["email"].downcase}".to_s
    end

    if params[:query]["sub_merchant_name"].present?
      conditions << "(users.name ILIKE ?)"
      parameters << "%#{params[:query]["sub_merchant_name"].downcase}%".to_s
    end

    if params[:query]["mer_id"].present?
      conditions << "(users.merchant_id = ?)"
      parameters << params[:query]["mer_id"].to_i
    end

    conditions = [conditions.join(" AND "), *parameters] if conditions.present? && parameters.present?

    if conditions.present?
      @merchants = User.joins(:parent_merchant).where.not(merchant_id: nil).where(archived: "false").where(conditions).order(id: :desc)
    else
      @merchants = User.where.not(merchant_id:nil).where(archived: "false").order(id: :desc)
    end

  end

  def wallet_export(user,wallet,send_via,first_date,second_date,offset)
    begin
      store new_status: 'queued'
      no_transaction = true
      reports_cron = CronJob.new(name: "export_#{send_via}",description: "Export #{send_via} from merchant",success: true,status: "pending")
      reports_cron.processing!
      report = Report.new(first_date: first_date, second_date:second_date, offset: offset, report_type: send_via, status: "pending", read: false)
      @user = User.friendly.find wallet
      @primary_wallet = @user.try(:wallets).try(:primary).try(:first)
      result = {user_id: @user.try(:id),start_date: first_date,end_date:second_date}
      result[:wallets_id] = nil
      report.user_id = user.try(:id)
      report.save
      url = ""
      if send_via == 'checks'
        order_type = "check"
        name = "Checks"
      elsif send_via == 'achs'
        order_type = "instant_ach"
        name = "ACH"
      elsif send_via == 'B2B (In the system)'
        order_type = "B2B"
        name = "B2B"
      end
      report.name = name
      checks = TangoOrder.where(order_type: order_type,wallet_id: @primary_wallet.id,created_at: first_date..second_date).sort.reverse unless order_type=='B2B'
      requests = Request.where("sender_id = ? or reciever_id = ?", @user.id,@user.id).where(created_at: first_date..second_date) if order_type=='B2B'
      if order_type == "instant_ach"
        checks_csv = CSV.generate do |csv|
          csv << ['ACH ID','Creation Date','Name on Account', 'Email', 'Account last 4','ACH Amount','Fee', 'Total','Status','Memo']
          checks.each.with_index(1) do |transaction,count|
            no_transaction = false
            wallet = Wallet.find_by(id: transaction.wallet_id)
            dba_name = wallet.try(:location).try(:business_name) if wallet.present?
            csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"),  transaction.name, transaction.recipient, transaction.account_number, number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')),number_with_precision(transaction.check_fee + (transaction.fee_perc*transaction.actual_amount/100), precision: 2, delimiter: ','), number_with_precision(transaction.amount, precision: 2, delimiter: ','), transaction.status, transaction.description]
          end
          # end
        end
      elsif order_type == "B2B"
        checks_csv = CSV.generate do |csv|
          csv << ['ID','Creation Date','Sender Name','Reciever Name', 'Amount','Status']
          requests.each.with_index(1) do |transaction,count|
            no_transaction = false
            csv << [transaction.id, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.sender_name, transaction.reciever_name, number_to_currency(number_with_precision(transaction.amount.to_f, :precision => 2, delimiter: ',')), transaction.status]
          end
        end
      else
        checks_csv = CSV.generate do |csv|
          csv << ['Check ID','Check Number','Creation Date','Name on Check', 'Email', 'Check Amount', 'Status','Memo']
          checks.each.with_index(1) do |transaction,count|
            no_transaction = false
            csv << [transaction.id, transaction.number, transaction.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"), transaction.name, transaction.recipient, number_to_currency(number_with_precision(transaction.actual_amount.to_f, :precision => 2, delimiter: ',')), transaction.status, transaction.description]
          end
        end
      end
      file = StringIO.new(checks_csv)
      qc_file = QcFile.new(name: "#{send_via}_export", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      if order_type == "B2B"
        download_type = "B2B"
        qc_file.image.instance_write(:file_name, "b2b.csv")
      elsif order_type == "instant_ach"
        download_type = "instant_ach"
        qc_file.image.instance_write(:file_name, "ach_export.csv")
      else
        download_type = 'checks'
        qc_file.image.instance_write(:file_name, "#{send_via}_export.csv")
      end
      qc_file.save
      if no_transaction
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      else
        report.file_id = {id: [qc_file.id]}
        result[:file_url] = qc_file.image.url
        result[:qc_file_id] = qc_file.id
        reports_count = Report.where(read: false, user_id: @user.id).count
        ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        store id: "#{qc_file.id}"
        store new_status: 'completed'
        reports_cron.status = "completed"
        report.status = "completed"
        report.message = "done"
      end
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      puts "export worker error occured", exc
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      store new_status: 'completed'
      reports_cron.status = "failed"
    ensure
      report.save
      reports_cron.result = result
      reports_cron.reports!
    end

  end

  def export_funding_transactions(user, first_date, second_date, offset, search_query,type,wallet_id)
    begin
      if type==TypesEnumLib::WorkerType::HoldRearExport
        store new_status: 'queued'
        transactions = nil
        no_transaction = true
        reports_cron = CronJob.new(name: "export_funding_transactions",description: "Export transaction for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}")
        reports_cron.processing!
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchant_email', message: ""
        report = Report.create(user_id: user.try(:id),name: 'Funding transactions', first_date: first_date, second_date:second_date, offset: offset, report_type: type, status: "pending",read: false)
        result = { user_id: user.try(:id), start_date: first_date, end_date: second_date,  offset: offset, search_query: search_query, qc_file_id: {}}
        if user.merchant?
          report.user_id = user.merchant_id if user.merchant_id.present?
          wallet_object = Wallet.find_by(id: wallet_id)
          wallets = wallet_object.try(:id)
          if search_query.present?
            report.save
            batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
            query=JSON.parse(search_query)
            hold_in_rear = HoldInRear.find_by(id: query.try(:[],'batch_id'))
            created_at = hold_in_rear.try(:created_at)
            wallet=wallet_id
            Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["complete","approved","refunded"], main_type:["eCommerce","Virtual Terminal","Credit Card"],to: wallet_id).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              count = index + 1
              no_transaction = false
              res = creating_funding_export_file(transactions,user,first_date,second_date,offset,@host_name,user.email,count,nil,'')
              result[:qc_file_id].merge!({"#{index}": res})
            end
            result[:wallets_id] = wallets
          end
        end
      else
        store new_status: 'queued'
        transactions = nil
        no_transaction = true
        reports_cron = CronJob.new(name: "export_reserve_release_transactions",description: "Export reserve transaction for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}")
        reports_cron.processing!
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchant_email', message: ""
        report = Report.create(user_id: user.try(:id),name: "Reserve Release", first_date: first_date, second_date:second_date, offset: offset, report_type: type, status: "pending",read: false)
        result = { user_id: user.try(:id), start_date: first_date, end_date: second_date,  offset: offset, search_query: search_query, qc_file_id: {}}
        if user.merchant?
          report.user_id = user.merchant_id if user.merchant_id.present?
          wallet_object = Wallet.find_by(id: wallet_id)
          wallets = wallet_object.try(:id)
          if search_query.present?
            report.save
            batch_size = ENV['BATCH_SIZE'].try(:to_i) || 20000
            query=JSON.parse(search_query)
            hold_in_rear = HoldInRear.find_by(id: query.try(:[],'batch_id'))
            created_at = hold_in_rear.try(:created_at)
            wallet=wallet_id
            @wallet=wallet
            # @transactions = Transaction.where(created_at: HoldInRear.pst_beginning_of_day(hold_in_rear.created_at)..HoldInRear.pst_end_of_day(hold_in_rear.created_at), status: ["complete","approved"], main_type:["eCommerce","Virtual Terminal","Credit Card"],to: wallet.id).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["refunded"], to: wallet.id)).distinct.per_page_kaminari(params[:page]).per(page_size)
            BlockTransaction.where(created_at: HoldInRear.pst_beginning_of_day(hold_in_rear.created_at)..HoldInRear.pst_end_of_day(hold_in_rear.created_at), main_type:'Reserve Money Deposit',sender_wallet_id: wallet_id).where(created_at: first_date.to_datetime.utc..second_date.to_datetime.utc).find_in_batches(batch_size: batch_size.to_i).with_index do |transactions,index|
              count = index + 1
              no_transaction = false
              res = creating_funding_export_file(transactions,user,first_date,second_date,offset,@host_name,user.email,count,nil,'reserve')
              result[:qc_file_id].merge!({"#{index}": res})
            end
            result[:wallets_id] = wallets
          end
        end

      end

      reports_cron_again = CronJob.find(reports_cron.id)
      if no_transaction
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'merchantexportdone', message: {no_tx: "no_tx"}
        report.status = "completed"
        report.message = "done"
      elsif result[:qc_file_id].present?
        report.file_id = {id: result[:qc_file_id].values.pluck(:qc_file_id)}
        # UserMailer.send_export_notification(user).deliver_later
        report_user_id = user.merchant_id.present? ? user.merchant_id : user.id
        reports_count = Report.where(read: false, user_id: report_user_id).count
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
        report.status = "completed"
        report.message = "done"
      else
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
        report.status = "crashed"
        report.message = "file_ids missing"
        msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
        SlackService.notify(msg)
        if reports_cron_again.try(:email) == true && report.status == "crashed"
          UserMailer.send_error_message(user).deliver_later
        end
      end
      reports_cron.completed!
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
      p "Export Failed for user with id: #{user.try(:id)} for dates: #{first_date} - #{second_date}", exc
      p "BACKTRACE: ", exc.backtrace
      msg = "User_ID: #{user.id} Email: #{user.email} Role: #{user.role} Failed to Export a #{report.report_type} Report."
      SlackService.notify(msg)
      if reports_cron_again.try(:email) == true && report.status == "crashed"
        UserMailer.send_error_message(user).deliver_later
      end
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
      reports_cron.failed! if reports_cron.present?
    ensure
      report.save
      if reports_cron.present?
        reports_cron.result = result
        reports_cron.reports!
      end
    end
  end
  def employee_export(user,wallet,send_via,first_date,second_date,offset)
    begin
      if user.merchant_id.present?
        sub_merchants= User.where(id: user.merchant_id).first.sub_merchants.where(archived: "false").where.not(id: user.id)
      else
        sub_merchants= user.sub_merchants.where(archived: "false")
      end
      report = Report.new(first_date: first_date, second_date:second_date, offset: offset, status: "pending", read: false, report_type: 'employee_list')
      report.user_id = user.try(:id)
      report.save
      text = I18n.t('notifications.exports_noti', name: user.name)
      Notification.notify_user(user ,user ,report,"Export",nil,text)
      UserMailer.send_export_notification(user).deliver_later
      employees_csv = CSV.generate do |csv|
        csv << ['ID','Name','Last Name','Permission', 'Email','Phone#']
        sub_merchants.each.with_index(1) do |user,count|
          no_transaction = false
          csv << [user.id, user.try(:name), user.try(:last_name), user.try(:permission).try(:name),user.try(:email) , user.try(:phone_number)]
        end
      end


      file = StringIO.new(employees_csv)
      qc_file = QcFile.new(name: "employee_list", start_date: first_date, end_date: second_date, status: :export, user_id: user.id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "employee_list.csv")
      qc_file.save

      report.file_id = {id: [qc_file.id]}
      # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
      report.status = "completed"
      report.message = "done"
    rescue => exc
      report.status = "crashed"
      report.message = exc.message
        # ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'error',heading: "#{TypesEnumLib::Message::CrashedHeading}" , message: "#{TypesEnumLib::Message::Crashed}"
    ensure
      report.save
    end
  end

end
