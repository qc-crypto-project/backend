class BlockedCardsWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  include ApplicationHelper
  # include TransactionCharge::ChargeATransactionHelper
  sidekiq_options queue: 'default', :retry => 0, backtrace: 8

  def perform(cards,user_id,session_id, type)
    @host = session_id
    if type == "vip"
      vip_cards(cards, user_id)
    else
      blocked_cards(cards,user_id)
    end
  end

  def vip_cards(cards, user_id)
    begin
      id = nil
      blocked_card = []
      new_cards = []
      cron_job = CronJob.new(name: "bulk_cards", description: "Importing vip Cards", success: true)
      result = {updates: [],  error: nil}
      cards_count = cards.try(:count)
      is_all_saved = true
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "0/#{cards_count}"
      count = 0
      cards.each_slice(2500) do |batch|
        if batch.present?
          old_cards = Card.where("(first6 IN (:first6) OR last4 IN (:last4)) AND (is_vip = true AND is_blocked = false)", first6: batch.pluck("first6"), last4: batch.pluck("last4")).select("id, last4, first6, is_vip").group_by{|e| ["#{e.first6}", "#{e.last4}"]}
          batch.each_with_index do |card, i|
            # i = i + 1
            count = count + 1
            card = card.symbolize_keys
            new_card_single = card.dup
            cardholder_name = card[:CardholderName]
            first6 = card[:first6]
            last4 = card[:last4]
            last4_check = last4.present? && last4.length == 4 ? last4.tr('0-9','').present? : true
            last6_check = first6.present? && first6.length == 6 ? first6.tr('0-9','').present? : true
            exp_date = card[:exp_date]
            if last4_check || last6_check
              is_all_saved = false
              last4_result = last4_check == true ? "Last4 Error-" : ""
              last6_result = last6_check == true ? "Last6 Error-" : ""
              new_card_single[:result] = "Wrong Format Error"
              result[:updates].push({first6: first6, last4: last4, exp_date: exp_date, result: "#{last4_result}#{last6_result}"})
              ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{count}/#{cards_count}"
            else
              if old_cards[["#{first6}","#{last4}"]].present?
                is_all_saved = false
                new_card_single[:result] = "Card Already Present!"
                new_card_single[:card_id] = nil
                new_card_single[:status] = card[:is_vip]
                result[:updates].push({first6: first6, last4: last4, exp_date: exp_date, result: "Card Already Present!"})
                ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{count}/#{cards_count}"
              else
                blocked_card << Card.new(name: cardholder_name,
                                         first6: first6,
                                         last4: last4,
                                         is_blocked: card[:is_blocked],
                                         is_vip: card[:is_vip],
                                         exp_date: card[:exp_date],
                                         user_id: 1,
                                         )
                new_card_single[:result] = "Card Successfully Saved!"
                new_card_single[:card_id] = nil
                new_card_single[:status] = card[:is_vip]
                result[:updates].push({first6: first6, last4: last4, exp_date: exp_date, result: "Card Successfully Saved!"})
                ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{count}/#{cards_count}"
              end
            end
            new_cards.push(new_card_single)
          end
          if blocked_card.present?
            Card.import blocked_card, on_duplicate_key_update: {conflict_target: [:id], columns: [:updated_at, :first6, :last4, :is_blocked, :is_vip, :user_id, :name]}
            blocked_card = []
          end
        end
      end
      n = 0
      result_csv = CSV.generate do |csv|
        csv << ["Cardholder Name","First6","Last4","Expiry Date", "VIP", "Card ID", "Result"]
        new_cards.in_groups_of(2500,false) do |group|
          group.each do |c|
            csv << [c[:CardholderName], c[:first6],c[:last4],c[:exp_date], c[:status],c.try(:[],:card_id), result.first[1][n]]
            n = n + 1
          end
          n = 0
        end
      end
      file = StringIO.new(result_csv)
      qc_file = QcFile.new(name: "admin_import_vip_cards", start_date: Time.now, status: "vip_cards", user_id: user_id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "vip_cards.csv")
      qc_file.info = cards_count
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation_completed' , message: {is_all_saved: is_all_saved, is_blocked: '?card=vip'}
    rescue => ex
      cron_job.success = false
      result[:error] = "Error: #{ex.message} for charge_id: #{id}"
      puts 'Error occured: ', ex
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{ex.message}"
    ensure
      cron_job.result = result
      cron_job.rake_task!
    end
  end

  def blocked_cards(cards, user_id)
    begin
      blocked_card = []
      new_cards = []
      is_all_saved = true
      cron_job = CronJob.new(name: "bulk_cards", description: "Importing blocked Cards", success: true)
      result = {updates: [],  error: nil}
      cards_count = cards.try(:count)
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "0/#{cards_count}"
      count = 0
      cards.each_slice(2500) do |batch|
        if batch.present?
          batch.each_with_index do |card, i|
            count = count + 1
            card = card.symbolize_keys
            new_card_single = card.dup
            card_number = card[:card_number]
            expi = card[:exp_date]
            first6 = card_number.first(6)
            last4 = card_number.last(4)
            card_expiry_check =/^(0[1-9]|1[0-2])\/([0-9]{2})$/.match(expi)
            if !card_expiry_check.present?
              is_all_saved = false
              card_expiry_result = card_expiry_check.present? ? "" : "Expiry Date Error"
              new_card_single[:result] = "Wrong Format Error"
              result[:updates].push({first6: first6, last4: last4, result: "#{card_expiry_result}"})
              ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{count}/#{cards_count}"
            else
              card_info = {
                  number: card[:card_number],
                  month: card[:exp_date].first(2),
                  year: "20#{card[:exp_date].last(2)}",
                  exp_date: "#{card[:exp_date].first(2)}/#{card[:exp_date].last(2)}"
              }
              card1 = Payment::QcCard.new(card_info,nil,user_id)
              blocked_card << Card.new(exp_date: card[:exp_date],
                                       first6: first6,
                                       last4: last4,
                                       is_blocked: card[:is_blocked],
                                       is_vip: card[:is_vip],
                                       user_id: 1,
                                       fingerprint: card1.try(:fingerprint),
                                       qc_token: card1.try(:qc_token)
                                       )
              new_card_single[:result] = "Card Successfully Saved!"
              new_card_single[:card_id] = nil
              new_card_single[:status] = card[:is_blocked]
              result[:updates].push({first6: first6, last4: last4, result: "Card Successfully Saved!"})
              ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{count}/#{cards_count}"
            end
            new_cards.push(new_card_single)
          end
          if blocked_card.present?
            Card.import blocked_card, on_duplicate_key_update: {conflict_target: [:id], columns: [:updated_at, :first6,:exp_date, :last4, :is_blocked, :is_vip, :user_id, :fingerprint, :qc_token]}
            blocked_card = []
          end
        end
      end
      n = 0
      result_csv = CSV.generate do |csv|
        csv << ["First6","Last4","Expiry Date", "Blocked", "Result"]
        new_cards.in_groups_of(2500,false) do |group|
          group.each do |c|
            csv << [c[:card_number].first(6),c[:card_number].last(4), c[:exp_date], c[:status], result.first[1][n]]
            n = n + 1
          end
          n = 0
        end
      end
      file = StringIO.new(result_csv)
      qc_file = QcFile.new(name: "admin_import_blocked_cards", start_date: Time.now, status: "bulk_cards", user_id: user_id)
      qc_file.image = file
      qc_file.image.instance_write(:content_type, 'text/csv')
      qc_file.image.instance_write(:file_name, "blocked_cards.csv")
      qc_file.info = cards_count
      qc_file.save
      result[:file_url] = qc_file.image.url
      result[:qc_file_id] = qc_file.id
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation_completed' , message: {is_all_saved: is_all_saved, is_blocked: '?card=blocked'}
    rescue => ex
      cron_job.success = false
      result[:error] = "Error: #{ex.message}"
      puts 'Error occured: ', ex
      ActionCable.server.broadcast "cards_channel_#{@host}", channel_name: "cards_channel_#{@host}", action: 'circulation' , message: "#{ex.message}"
    ensure
      cron_job.result = result
      cron_job.rake_task!
    end
  end

end
