class UserMailer < ApplicationMailer
  default from: 'QuickCard <noreply@quickcard.me>'
  track utm_params: true
  include ActionView::Helpers::NumberHelper
  ALGORITHM = 'HS256'

  def welcome_merchant(user_id, password, subject = 'User SignUp')
    begin
      @user = User.find_by(id: user_id)
      @password = password
      @key=@user.oauth_apps.first.try(:key) if @user.oauth_apps.present?
      @secret=@user.oauth_apps.first.try(:secret) if @user.oauth_apps.present?
      @auth_apps=@user.oauth_apps.first if @user.oauth_apps.present?
      @descriptor=@user.primary_gateway.present? ? @user.primary_gateway.name : ""
      @url = ENV['APP_ROOT']
      @location_details=[]
      @user_info=[]
      @fee_details=[]
      type=''
      bcc=[]
      bcc.push("support@quickcard.me")
      # bcc.push("welcome@greenboxpos.com")
      if @user.wallets.present?
        wallets=@user.wallets
        # ----------------------Get Location Information------------------
        wallets.primary.each do |wallet|
          wallet_location=wallet.location
          if wallet_location.present? && wallet_location.is_block==false
            fee=wallet_location.fees.buy_rate.first
            @fee_details.push(fee: fee.reserve_fee,days: fee.days) if fee.present?
            @location_details.push(name: wallet_location.business_name, key: wallet_location.location_secure_token, hold_in_rear_days: wallet_location.hold_in_rear_days)
            if wallet_location.isos.present?
              iso=wallet_location.isos.first
              type='Iso'
              @user_info.push(name: iso.company_name,email: iso.email,phone: iso.phone_number,type: type)
              # bcc.push(iso.email)
            end
            # -----------add iso to bcc if iso is present-------------
            if type=="Agent"
              if wallet_location.isos.present?
                iso=wallet_location.isos.first
                # bcc.push(iso.email)
              end
            end
          end
        end
        # ----------------------End Get Location Inforamtion--------------
      end
      @user_info=@user_info.uniq
      @fee_details=@fee_details.uniq
      if ENV['APP_ROOT']!='http://quickcard.herokuapp.com'
        body_html= render_to_string(partial:'user_mailer/welcome_merchant.pdf.erb')
        begin
          @pdf = WickedPdf.new.pdf_from_string(body_html)
          # Attach to email as attachment.
          attachments["Congratulations.pdf"] = @pdf
        rescue => ex
          SlackService.notify(ex.message)
        end
      end
      # smpt_auth
      bcc=bcc.uniq
      mail(to: @user.email,bcc: bcc, subject: "Welcome to QuickCard")
    rescue => ex
      SlackService.notify(ex.message)
    end
  end

  def invoice_pdf_email(pdf,request_id)
    # attachments["invoice.pdf"] = pdf
    @request = Request.find(request_id)
    @products = @request.products
    @merchant = @request.sender
    @user = @request.reciever
    @billing = @request.addresses.billing
    @shipping = @request.addresses.shipping
    @shipping = @shipping.first
    @billing = @billing.first
    @location = Wallet.find_by(id: @request.wallet_id).try(:location)
    urls = get_payment_urls
    @quickcard_url = urls[:quickcard_url]
    payload = {request_id: @request.id}
    @download_invoice = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
    @download_url = download_invoice_url(request_url: @download_invoice)
    @ach_url = urls[:ach_url]
    @card_url = urls[:card_url]
    active_option = urls[:active_option]
    if @request.apply_tax == true
      tax_on_SH = (@request.tax.to_f / 100) * @request.shipping_handling_fee.to_f
      @taxes = @request.tax_method == "percent" ? ((@request.tax.to_f / 100) * @request.amount.to_f) + tax_on_SH.to_f  : @request.tax
    else
      @taxes = (@request.tax.to_f / 100) * @request.amount.to_f
    end
    # mail(to: "abbas8156@gmail.com", subject: "Invoice", :template_name => "invoice_pdf_email#{active_option}")
    mail(to: @user.email, subject: "Invoice", :template_name => "invoice_pdf_email#{active_option}")
  end

  def virtual_terminal_email(merchant,sender,location,transaction_id,amount,product,discount,tax,sub_total,grand_total,card,descriptor=nil,shipping_fee=nil)
    # attachments["invoice.pdf"] = pdf
    @user = User.find_by(id: merchant.id)
    @merchant = merchant.name
    @customer = sender.name
    @location_name = location[:business_name]
    @amount = amount
    @transaction_id = transaction_id.first(6)
    @products = JSON.parse(product) if product.present?
    @discount = discount
    @tax = tax
    @shipping_fee = shipping_fee
    @sub_total = sub_total
    @grand_total = grand_total
    @card = card
    @descriptor = descriptor
    @card_brand = card.brand
    @last4 = card.last4
    @support_number = location.phone_number
    @email = location.email
    payload = {request_id: transaction_id}
    @download_virtual_terminal = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
    @download_url = download_virtual_terminal_url(request_url: @download_virtual_terminal, products: @products, sub_total: @sub_total, descriptor: @descriptor)
    mail(to: sender.email, subject: "Order Confirmation from #{@location_name}")
  end


  def send_tos(email,user)
    @user_name = user.name
    mail(to: email, subject: "QuickCard Terms of Service")
  end


  def ach_request(data,user_id)
    begin
      @user = User.find_by(id: user_id)
      @data = JSON(data)
      if @user.merchant?
        @locations = @user.get_locations
        Image.where(location_id: @locations.pluck(:id)).each do |img|
          @url = "https:#{img.add_image.url}"
          file_data = open(@url)
          mail.attachments["#{img.add_image_file_name}"] ={  content: file_data.read}
          # mail.attachments["#{img.add_image_file_name}"] = img.add_image.url
        end
      else
        @wallet=@user.wallets.primary.first.id
        img= Image.where(location_id: @wallet).first
        @url = "https:#{img.add_image.url}"
        file_data = open(@url)
        mail.attachments["#{img.add_image_file_name}"] ={  content: file_data.read}
      end

      @urls = admins_approve_ach_request_url(id: @user.id)
      to = ENV['SEND_ACH_REQ'].present? ? ENV['SEND_ACH_REQ'] : "alyse@greenboxpos.com"
      mail(to: to, subject: "Withdraw Status Changed")
    rescue => ex
    end
  end

  def new_location_email(user_id, location_id, supportMail=nil)
    user = User.find_by(id: user_id)
    location = Location.find_by(id: location_id)
    if user.present? && location.present?
      @merchant_legal_name = location.merchant.name.try(:titleize)
      @user_name = user.name.try(:capitalize)
      @dba_name = location.business_name.try(:capitalize)
      @location_id = location.location_secure_token
      @website = JSON(location.web_site).first
      @key= location.merchant.oauth_apps.first.try(:key) if location.merchant.oauth_apps.present?
      @secret=location.merchant.oauth_apps.first.try(:secret) if location.merchant.oauth_apps.present?
      @location_details = location
      mail(to: user.email, subject: "Welcome to QuickCard")
    else
      SlackService.notify("*Error in finding email or merchant*")
    end
  end

  def refund_email(location,amount, user,date, card_brand, card_number, trx_id)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    @website = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    @header = location.try(:business_name)
    @dba_name = location.try(:business_name)
    @amount = number_with_precision(number_to_currency((amount).to_f), precision: 2)
    @date = JSON(date).to_date.strftime("%b %d, %Y")
    @location_email = location.try(:email)
    @location_phone = location.cs_number || location.phone_number
    @user_name = user.try(:name)
    @payment = "#{card_brand} - #{card_number}"
    @card_brand = card_brand
    @card_number = card_number if card_number.present?
    @trx_id = trx_id
    @email = location.email
    @phone_number = location.phone_number
    mail(to:  user.email, subject: "Refund approved")
  end

  def refund_cbk_email(dispute,trans)
    @dispute = dispute
    @trans = trans
    @transaction=Transaction.find_by(id:@dispute.dispute_result_transaction_id)
    location = Location.find_by(id: trans.tags["location"]["id"])
    @website = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    @dba_name = location.try(:business_name)
    @amount = @dispute.try(:[],"amount").present? ? number_with_precision(number_to_currency(@dispute.try(:[],"amount").to_f), precision: 2) : "$0.00"
    @user_name = @trans.sender.try(:name)
    @card_brand = @trans.card.try(:brand)
    @card_number = @trans.card.try(:last4)
    @trx_id = @transaction.try(:seq_transaction_id).first(6) if @transaction.try(:seq_transaction_id).present? && @transaction.try(:seq_transaction_id).first(6).present?
    @email = location.try(:email)
    @phone_number = location.try(:phone_number)
    @user_email = @trans.sender.try(:email)
    mail(to: @user_email, subject: "Refund approved")
  end

  def cbk_lost_email(dispute,trans)
    @dispute = dispute
    @trans = trans
    @transaction=Transaction.find_by(id:@dispute.dispute_result_transaction_id)
    location = Location.find_by(id: trans.tags["location"]["id"])
    @website = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    @dba_name = location.try(:business_name)
    @amount = @dispute.try(:[],"amount").present? ? number_with_precision(number_to_currency(@dispute.try(:[],"amount").to_f), precision: 2) : "$0.00"
    @user_name = @trans.sender.try(:name)
    @card_brand = @trans.card.try(:brand)
    @card_number = @trans.card.try(:last4)
    @trx_id = @transaction.try(:seq_transaction_id).first(6) if @transaction.try(:seq_transaction_id).present?
    @email = location.try(:email)
    @phone_number = location.try(:phone_number)
    @user_email = @trans.receiver.try(:email)
    mail(to: @user_email, subject: "Chargeback Case Lost")
  end

  def send_email_code(user, subject = 'QuickCard Verification Code')
    @random_code = rand(1_00000..9_99999)
    user.direct_otp = @random_code
    user.pin_code = @random_code
    user.direct_otp_sent_at = DateTime.now.utc
    user.save
    @user = user
    # smpt_auth
    mail(to: @user.email, subject: subject)
  end

  def send_pincode(user, subject = 'Verification Code')
    @random_code = user.pin_code
    @user = user
    mail(to: @user.email, subject: subject)
  end

  def support_email(user,subject, ticket)
    @user = user
    @ticket= ticket
    @url = ENV['APP_ROOT']
    # smpt_auth
    mail(to: "help@mtractech.com", subject: subject)
  end

  def welcome_company_admin(user)
    @user = user
    @url = ENV['APP_ROOT']
    mail(to: @user.email, subject: 'Company Admin SignUp ')
  end

  def inform_merchant(user,message )
    @user = user
    @url = ENV['APP_ROOT']
    @body = message
    mail(to: @user.email, subject: 'Information')
  end

  def welcome_user(user, message, role, passsword)
    @user = user
    @url = ENV['APP_ROOT']
    @body = message
    @password = passsword
    recipients = []
    if role.present? &&
        if role == "ISO" || role == 'iso'
          @role = 'I'
          recipients.push("arid@greenboxpos.com")
        elsif role == 'Agent' || role == 'agent'
          @role = 'A'
          recipients.push("arid@greenboxpos.com")
        elsif role == "Affiliate" || role == 'affiliate'
          @role = 'AF'
          recipients.push("arid@greenboxpos.com")
        end
    end
    text = I18n.t('notifications.welcome_notification', name: @user.name)
    Notification.notify_user(user,user,user,"welcome notification",nil,text)
    mail(to: @user.email, bcc: recipients, subject: "Welcome to QuickCard")
  end

  def welcome_affiliate(user, message, role, passsword)
    @user = user
    @url = ENV['APP_ROOT']
    @body = message
    @password = passsword
    mail(to: @user.email,subject: "Welcome to QuickCard")
  end

  def welcome_employee(user,password)
    @user = user
    first_name = user.name
    last_name = user.last_name
    @user_name = first_name +' '+ last_name
    @url = ENV['APP_ROOT']
    @password = password
    mail(to: @user.email, subject: "Welcome to QuickCard")
  end

  def return_label(user,label_url)
    @customer = user
    @label_url = label_url
    @customer_name = user.name
    mail(to: @customer.email,subject: "Return Label")
  end

  def custom_email(email)
    subject='Attention: Revision to QuickCard System Upgrade Date'
    mail(to: email,subject: subject)
  end

  def transaction_email(location,amount,gateway,user,date,card_brand,card_number,trx_id,action=nil)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      @header = location.try(:business_name)
      @dba_name = location.try(:business_name)
      @amount = amount
      @date = JSON(date).to_date.strftime("%b %d, %Y")
      @trx_id = trx_id.first(6)
      @descriptor = gateway.present? ? gateway : '---'
      @payment = "#{card_brand} - #{card_number.first(4)}"
      @location_email = location.try(:email)
      @location_phone = location.cs_number || location.phone_number
      @user_name = user.try(:name)
      @card_brand = card_brand
      @last4 = card_number.last(4)
      mail(to:  user.email, subject: "Transaction Detail")
    else
      SlackService.notify("Location Not found while sending transaction email, Action: #{action}")
    end
  end

  def checkout_order_email(user,order_id,location,card_brand,card_number,gateway,transaction,request = nil)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    if request.present?
      @discount = request.discount_method == "dollar" ? request.discount : (request.discount.to_f / 100) * request.amount.to_f
    else
      @discount = transaction.try(:discount)
    end
    @tax = transaction.try(:tax)
    @amount_due = transaction.try(:total_amount)
    @sub_total = transaction.try(:total)
    @products = transaction.products
    @merchant_name = transaction.try(:receiver_name)
    @user_name = user.try(:name)
    @order_id = transaction.try(:seq_transaction_id).first(6)
    @card_brand = card_brand
    @last4 = card_number
    @support_number = location.try(:cs_number)
    @email = location.try(:email)
    @location_name = location.try(:business_name)
    @descriptor = gateway.present? ? gateway : '---'
    @transaction_id = transaction.seq_transaction_id
    payload = {request_id: @transaction_id}
    @download_checkout = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
    @download_url = download_customer_checkout_url(request_url: @download_checkout, sub_total: @sub_total, descriptor: @descriptor)
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      mail(to: user.email, subject: "Order Confirmation from #{@location_name}")
    else
      SlackService.notify("Location Not found while sending checkout order email, Action: #{action}")
    end

  end
  def checkout_order_email_qc_sequre(user,order_id,location,card_brand,card_number,gateway,transaction)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    @discount = transaction.try(:discount)
    @tax = transaction.try(:tax)
    @amount_due = transaction.try(:total_amount)
    @sub_total = transaction.try(:total)
    @products = transaction.products
    @merchant_name = transaction.try(:receiver_name)
    @user_name = user.try(:name)
    @order_id = transaction.try(:seq_transaction_id).first(6)
    @card_brand = card_brand
    @last4 = card_number
    @support_number = location.try(:cs_number)
    @email = location.try(:email)
    @location_name = location.try(:business_name)
    @descriptor = gateway.present? ? gateway : '---'
    @transaction_id = transaction.seq_transaction_id
    payload = {request_id: @transaction_id}
    @download_checkout_qcp = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
    @download_url = download_customer_checkout_qcp_url(request_url: @download_checkout_qcp, descriptor: @descriptor)
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      mail(to: user.email, subject: "Order Confirmation from #{@location_name}")
    else
      SlackService.notify("Location Not found while sending checkout order email, Action: #{action}")
    end

  end

  def pre_transit_status(user,order_id,location,gateway,tracking_no)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    @user_name = user.try(:name)
    @order_id = order_id
    @support_number = location.try(:cs_number)
    @email = location.try(:email)
    @descriptor = gateway.present? ? gateway : '---'
    @tracking_no = tracking_no
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      mail(to: user.email, subject: "Tracking # #{@tracking_no} for Order ID #{order_id}")
    else
      SlackService.notify("Location Not found while sending pre-transit status email, Action: #{action}")
    end

  end

  def in_transit_status(user,order_id,location,gateway,tracking_no,carrier_name)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    @user_name = user.try(:name)
    @order_id = order_id
    @support_number = location.try(:cs_number)
    @email = location.try(:email)
    @descriptor = gateway.present? ? gateway : '---'
    @tracking_no = tracking_no
    @carrier_name = carrier_name
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      mail(to: user.email, subject: "Tracking # #{@tracking_no} for #{@web_site} order")
    else
      SlackService.notify("Location Not found while sending pre-transit status email, Action: #{action}")
    end

  end

  def order_cancel_email(user,order_id,location,card_brand,last4,amount)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    @user_name = user.try(:name)
    @order_id = order_id
    @support_number = location.try(:cs_number)
    @email = location.try(:email)
    @card_brand =card_brand
    @last4 = last4
    @refund_amount = number_to_currency(amount)
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      mail(to: user.email, subject: "Order ID #{@order_id} from #{@web_site} has been cancelled.")
    else
      SlackService.notify("Location Not found while sending order cancelled email, Action: #{action}")
    end

  end

  def customer_cancel_order(dispute_id) #Task 6a
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  merchant = @dispute.merchant
  @merchant_name = merchant.try(:name)
  @user_name = user.try(:name)
  @order_id = @dispute.dispute_transaction.order_id
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  @refund_amount = number_to_currency(@dispute.amount)
  mail(to: merchant.email, subject: "Order ID #{@order_id} has been cancelled by the customer.")
  end

  def order_cancel_email_to_customer(dispute_id)#Task 6b
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @order_id = @dispute.dispute_transaction.order_id
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @card_brand = @dispute.dispute_transaction.card.present? ? @dispute.dispute_transaction.card.try(:brand) : "card"
  @last4 = @dispute.dispute_transaction.last4
  @refund_amount = number_to_currency(@dispute.refund_amount)
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Order ID #{@order_id} from #{@web_site} has been cancelled.")
  else
    SlackService.notify("Location Not found while sending order cancelled email, Action: #{action}")
  end

  end

  def order_cancel_update(dispute_id)#Task 6c
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @order_id = @dispute.dispute_transaction.order_id
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Update for cancellation request for #{@order_id} from #{@web_site}.")
  else
    SlackService.notify("Location Not found while sending order cancelled update email, Action: #{action}")
  end

  end

  def request_refund_confirmation(dispute_id)#Task 7
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @order_id = @dispute.dispute_transaction.order_id
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  @refund_amount = number_to_currency(@dispute.amount)
  @reason = @dispute.reason
  @url = ENV['APP_ROOT']
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Refund Request Confirmation for #{@order_id}.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def request_refund_confirmation_merchant(dispute_id)#Task 8
  @dispute = CustomerDispute.find(dispute_id)
  merchant = @dispute.merchant
  @merchant_name = merchant.try(:name)
  @customer_name = @dispute.customer.name
  @order_id = @dispute.dispute_transaction.order_id
  @refund_amount = number_to_currency(@dispute.amount)
  @reason = @dispute.reason
  @order_date = @dispute.dispute_transaction.created_at
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  mail(to: merchant.email, subject: "Refund Request Confirmation for #{@order_id} from #{@customer_name}")
  end

  def approved_refund_request(dispute_id)#task 9
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @refund_amount = number_to_currency(@dispute.amount)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @card_brand = @dispute.dispute_transaction.card.present? ? @dispute.dispute_transaction.card.try(:brand) : "card"
  @last4 = @dispute.dispute_transaction.last4
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Refund approved.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def partial_refund_request_merchant(dispute_id,offer_amount,subject = nil)#Task 10a
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @refund_amount = number_to_currency(@dispute.amount)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @refund_amount = number_to_currency(offer_amount)
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    if subject.blank?
      subject = "Refund Request refund case #{@dispute.id} from #{@web_site}."
    end
    mail(to: user.email, subject: subject)
  else
    SlackService.notify("Location Not found while sending partial refund request offer email, Action: #{action}")
  end

  end

  def partial_refund_request_customer_offer(dispute_id,subject = nil)#Task 10b
  @dispute = CustomerDispute.find(dispute_id)
  merchant = @dispute.merchant
  user = @dispute.customer
  @merchant_name = merchant.try(:name)
  @customer_name = user.try(:name)
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  if subject.blank?
    subject = "Counter Offer for Refund Case # #{@dispute.id} from #{@customer_name}."
  end
  mail(to: merchant.email, subject: subject)
  end

  def merchant_approved_customer_offer(dispute_id,refund_amount)#Task 10c
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @refund_amount = number_to_currency(refund_amount)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @card_brand = @dispute.dispute_transaction.card.present? ? @dispute.dispute_transaction.card.try(:brand) : "card"
  @last4 = @dispute.dispute_transaction.last4
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Refund request update for Refund Case #{@dispute.id} from #{@web_site}.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def merchant_declined_customer_offer(dispute_id,refund_amount)#Task 10d
  @dispute = CustomerDispute.find(dispute_id)
  user = @dispute.customer
  location = @dispute.location
  @user_name = user.try(:name)
  @refund_amount = number_to_currency(refund_amount)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @card_brand = @dispute.dispute_transaction.card.present? ? @dispute.dispute_transaction.card.try(:brand) : "card"
  @last4 = @dispute.dispute_transaction.last4
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Refund request update for Refund Case #{@dispute.id} from #{@web_site}.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def order_return_email(user,customer_dispute_id,location,order_id)#Task 10e
  location = Location.find_by(id: location)
  @refund_request = CustomerDispute.find(customer_dispute_id)
  user = User.find_by(id: user)
  @user_name = user.try(:name)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @order_id = order_id
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Refund from #{@web_site} for Order # #{@order_id}.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def refund_cancelled_email(user,customer_dispute_id,location,order_id)#Task 10f
  location = Location.find_by(id: location)
  @refund_request = CustomerDispute.find(customer_dispute_id)
  user = User.find_by(id: user)
  @user_name = user.try(:name)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @order_id = order_id
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Refund from #{@web_site} for Order # #{@order_id}.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def counter_offer_expired(user,customer_dispute_id,location)#Task 10g
  location = Location.find_by(id: location)
  @refund_request = CustomerDispute.find(customer_dispute_id)
  user = User.find_by(id: user)
  @user_name = user.try(:name)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Acceptance deadline for refund counteroffer for Case# #{@refund_request.id} has expired.")
  else
    SlackService.notify("Location Not found while sending refund request confirmation email, Action: #{action}")
  end

  end

  def pre_transit_confirmation(merchant,order_id)#Task 11
  merchant = User.find_by(id: merchant)
  @merchant_name = merchant.try(:name)
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  @order_id = order_id
  mail(to: merchant.email, subject: "Urgent Notice - No shipment confirmation for Order # #{@order_id} ")
  end


  def shipment_delay(user,location,order_id)#Task 12
  location = Location.find_by(id: location)
  user = User.find_by(id: user)
  @user_name = user.try(:name)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @order_id = order_id
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Important info about your Order # #{@order_id} from #{@web_site}")
  else
    SlackService.notify("Location Not found while sending shipment delay email, Action: #{action}")
  end

  end

  def overdue_shipment(merchant,order_id)#Task 13
  merchant = User.find_by(id: merchant)
  @merchant_name = merchant.try(:name)
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  @order_id = order_id
  mail(to: merchant.email, subject: "Urgent Notice - Overdue shipment confirmation for order # #{@order_id}")

  end



  def overdue_shipment_confirmation(user,location,order_id)#Task 14
  location = Location.find_by(id: location)
  user = User.find_by(id: user)
  @user_name = user.try(:name)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @order_id = order_id
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Delivery update for Order # #{@order_id}")
  else
    SlackService.notify("Location Not found while sending delivery update email, Action: #{action}")
  end

  end

  def return_to_sender(merchant,location,order_id,carrier)#Task 15a
  location = Location.find_by(id: location)
  merchant = User.find_by(id: merchant)
  @merchant_name = merchant.try(:name)
  @support_number = "0800-7860100"
  @email = "zcart@zcart.com"
  @order_id = order_id
  @carrier = carrier
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: merchant.email, subject: "Urgent Notice - Order # #{@order_id} was undeliverable and returned to sender")
  else
    SlackService.notify("Location Not found while sending return to sender email, Action: #{action}")
  end

  end

  def return_to_sender_customer(user,location,order_id,carrier)#Task 15b
  location = Location.find_by(id: location)
  user = User.find_by(id: user)
  @user_name = user.try(:name)
  @support_number = location.try(:cs_number)
  @email = location.try(:email)
  @order_id = order_id
  @carrier = carrier
  if location.present?
    @web_site = ""
    website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
    if website.present?
      website.each do |w|
        url = URI.parse(w)
        if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
          @web_site = w
        end
      end
    end
    mail(to: user.email, subject: "Urgent Notice - Order # #{@order_id} was undeliverable and returned to sender")
  else
    SlackService.notify("Location Not found while sending return to sender email, Action: #{action}")
  end

  end

  def manual_review_3ds(txn_id)#Task 16
  transaction = Transaction.find_by(id: txn_id)
  location = transaction.receiver_wallet.try(:location)
  @dba_name = location.try(:business_name)
  @txn_id = transaction.try(:seq_transaction_id).try(:slice,0..5)
  @url = ENV['APP_ROOT']
                               # @amount =transaction.amount
  @amount = transaction.tags["previous_issue"]["issue_amount"] if transaction.try(:tags)["previous_issue"]["issue_amount"].present?
  @txn_date = transaction.created_at
  @allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
  mail(to: location.email, subject: "Approval required for moderate risk transaction")
  end

  def manual_review_expired(txn_id)#Task 18
  transaction = Transaction.find_by(id: txn_id)
  location = transaction.receiver_wallet.try(:location)
  @dba_name = location.try(:business_name)
  @txn_id = transaction.try(:seq_transaction_id).try(:slice,0..5)
  @allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
  @url = ENV['APP_ROOT']
  @amount = transaction.tags["previous_issue"]["issue_amount"] if transaction.try(:tags)["previous_issue"]["issue_amount"].present?
  mail(to: location.email, subject: "Expired Deadline for Transaction Approval")
  end
  def manual_review_refund(txn_id)#Task 19a
  transaction = Transaction.find_by(id: txn_id)
  location = transaction.receiver_wallet.try(:location)
  @dba_name = location.try(:business_name)
  @txn_id = transaction.try(:seq_transaction_id).try(:slice,0..5)
  @customer = transaction.try(:sender).try(:name).try(:capitalize)
  @allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
  @url = ENV['APP_ROOT']
  @amount = transaction.tags["previous_issue"]["issue_amount"] if transaction.try(:tags)["previous_issue"]["issue_amount"].present?
  if transaction.card.present?
    @card_brand = transaction.card.brand
  else
    @card_brand = transaction.tags.try(:[],"card").try(:[],"brand")
  end
  if transaction.last4.present?
    @last4 = transaction.last4
  elsif transaction.tags.try(:[],"card").try(:[],"last4")
    @last4 = transaction.tags.try(:[],"card").try(:[],"last4")
  elsif transaction.card.present?
    @last4 = transaction.card.last4
  end
  @email = location.email
  @phone_number = location.phone_number
  @email= transaction.try(:sender).try(:email)
  mail(to: @email, subject: "Refunded Transaction")
  end


  def delivered_status(user,order_id,location,gateway,tracker)
    location = Location.find_by(id: location)
    user = User.find_by(id: user)
    @user_name = user.try(:name)
    @order_id = order_id
    @support_number = location.try(:cs_number)
    @email = location.try(:email)
    @descriptor = gateway.present? ? gateway : '---'
    @delivery_time = tracker.updated_at
    if location.present?
      @web_site = ""
      website = JSON(location.web_site) if location.web_site.present? && location.web_site.kind_of?(String)
      if website.present?
        website.each do |w|
          url = URI.parse(w)
          if (url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)) && @web_site.blank?
            @web_site = w
          end
        end
      end
      mail(to: user.email, subject: "Order from #{@web_site} was DELIVERED!")
    else
      SlackService.notify("Location Not found while sending delivered status email, Action: #{action}")
    end

  end


  def warning_email_to_change_password(user)
    @user = user
    @url = ENV['APP_ROOT']
    mail(to:  @user.email, subject: "Update Your Password")
  end

  def resend_welcome_email(user)
    @user = user
    @token = user.reset_password_token
    @url = "#{ENV['APP_ROOT']}/unlock_account?id=#{@user.id}"
    mail(to:  @user.email, subject: "QuickCard Security Alert")
  end

  def send_export_file(user, url)
    @attachment_file = false
    @user = user
    @url = "https:#{url}"
    file_data = open(@url)
    if (file_data.size.to_f / 1024000) <= 30
      @attachment_file = true
      mail.attachments["SalesReport.csv"] = { mime_type: 'text/csv', content: file_data.read}
    end
    mail(to: @user.email, subject: "Export File")
  end

  def send_permission_export_file(user, url)
    @attachment_file = false
    @user = user
    @url = "https:#{url}"
    file_data = open(@url)
    if (file_data.size.to_f / 1024000) <= 30
      @attachment_file = true
      mail.attachments["PermissionsList.csv"] = { mime_type: 'text/csv', content: file_data.read}
    end
    mail(to: @user.email, subject: "Export File")
  end

  def send_error_message(user)
    @user = user
    mail(to: @user.email, subject: "Export File_Error")
  end
  def send_export_files(user, url)
    @attachment_file = true
    @user = user
    @url = []
    if url.try(:[],"id").present?
      url.try(:[],"id").each do |id|
        qc_file = QcFile.find_by(id: id)
        if qc_file.present?
          file_data = open("https:#{qc_file.image.url}")
          # if (file_data.size.to_f / 1024000) <= 30
          mail.attachments["SalesReport_#{id}.csv"] = { mime_type: 'text/csv', content: file_data.read}
          # else
          #   @attachment_file = false
          #   @url.push("https:#{qc_file.image.url}")
          # end
        end
      end
      mail(to: @user.email, subject: "Export File")
    end
  end

  def send_export_notification(user)
    @user = user
    if @user.admin?
      @url = "#{ENV['APP_ROOT']}/admins/transactions/exported_files"
    else
      @url = "#{ENV['APP_ROOT']}/v2/merchant/exports"
    end
    mail(to: @user.email, subject: "Export File is Ready")
  end

  def checks_enabled(user)
    @user = user
    mail(to: user.email, subject: "QuickCard System Maintenance Notice")
  end

  def system_maintenance_on(user)
    @user = user
    mail(to: user.email, subject: "QuickCard System Maintenance Notice")
  end

  def merchant_dispute(dispute,trans,loc_email)
    @dispute=dispute
    @reason = Reason.where(id:@dispute.reason_id).last.try(:title)
    @trans = trans
    @dba_name = trans.try(:receiver_name)
    @user_name = trans.try(:sender_name) || trans.try(:tags).try(:[],"previous_issue").try(:[],"tags").try(:[],"merchant").try(:[],"name")
    @date = @dispute.recieved_date.strftime("%m/%d/%Y")
    @due_date = @dispute.due_date.strftime("%m/%d/%Y")
    @trans_date = @trans.created_at.strftime("%m/%d/%Y")
    @id = @trans.try(:seq_transaction_id).first(6)
    @url = ENV['APP_ROOT']
    @user = @dispute.try(:merchant_wallet).try(:location).try(:merchant)
    @id = @trans.try(:seq_transaction_id).first(6)
    @phone_number = trans.try(:sender).try(:phone_number)
    @email = trans.try(:sender).try(:email)
    mail(to: loc_email, cc: @user.email, subject: "Chargeback Notice")
  end

  def admin_dispute_email(dispute,dba_name,accept_dispute)
    @dispute=dispute
    @dba_name=dba_name
    @accept_dispute=accept_dispute
    @url = ENV['APP_ROOT']
    to = ENV['SEND_DISPUTE_REQ'].present? ? ENV['SEND_DISPUTE_REQ'] : "cbk@greenboxpos.com"
    if @accept_dispute.present?
      mail(to: to, subject: "Dispute Accepted")
    else
      mail(to: to, subject: "Evidence Uploaded")
    end
  end

  def reserve_release(user,amount,reserve)
    @amount=amount
    @reserve=reserve
    mail(to: user.try(:email), subject: "Fund Released")
  end


  def ach_status_change_email(check_id,status,notes)
    check = TangoOrder.find check_id
    ach = TangoOrder.where(id:check_id).last
    trans = Transaction.where(id:check.transaction_id).last
    loc_id = trans.try(:[],:tags).try(:[],"location").try(:[],"id")
    location = Location.where(id:loc_id).last
    @bank_account_name = location.try(:bank_account_name)
    @trans_time = ach.updated_at.in_time_zone("Pacific Time (US & Canada)").try(:strftime, "%m/%d/%Y %I:%M:%S %p")
    @tango_id = check_id
    @user = check.user if check.present?
    @recipient = check.recipient
    @wallet_id = check.try(:wallet_id)
    @amount = check.actual_amount || check.amount
    @status = status if status.present?
    @sent_via = check.send_via
    @routing_number = "xxxx#{check.routing_number.last(4)}" if check.routing_number.present?
    @account_number = "xxxx#{check.account_number.last(4)}" if check.account_number.present?
    @description = check.description if check.description.present?
    @company_name = check.try(:user).try(:company).try(:name)
    @name = check.name
    @status = check.status
    @bank_name = check.ach_gateway.try(:descriptor)
    notes = notes.present? ? JSON(notes) : ''
    @customer_note = notes["customer_note"]
    @internal_note = notes["internal_note"]
    @date = check.created_at.in_time_zone("Pacific Time (US & Canada)").try(:strftime, "%m/%d/%Y %I:%M:%S %p")
    @for=check.instant_ach? ? 'ACH' : check.instant_pay? ? 'Push to Card' : 'Check' if check.present?
    if status == 'PAID_WIRE'
      @status = 'PAID'
    else
      @status = status if check.present?
    end
    if @status == "IN_PROGRESS" || @status == "IN_PROCESS"
      mail(to: @user.email, subject: "ACH has been processed")
    elsif @status == "FAILED"
      mail(to: @user.email, subject: "ACH withdrawal failed")
    elsif @status == "VOID"
      mail(to: @user.email, subject: "ACH withdraw voided")
    end
  end

  def p2c_status_change_email(check_id,status,notes)
    check = TangoOrder.find check_id
    p2c = TangoOrder.where(id:check_id).last
    trans = Transaction.where(id:check.transaction_id).last
    loc_id = trans.try(:[],:tags).try(:[],"location").try(:[],"id")
    location = Location.where(id:loc_id).last
    @role = trans.try(:sender).try(:role)
    @user = check.user if check.present?
    if @role == 'merchant'
      @user_role = 'M'
      if @user.merchant_id.present?
        @user = @user.parent_merchant
      end
    elsif @role == 'iso'
      @user_role = "I"
    else
      @user_role = 'A'
    end
    @bank_account_name = check.try(:name)
    @trans_time = p2c.updated_at.in_time_zone("Pacific Time (US & Canada)").try(:strftime, "%m/%d/%Y %I:%M:%S %p")
    @tango_id = check_id
    @recipient = check.recipient
    @wallet_id = check.try(:wallet_id)
    @amount = check.actual_amount || check.amount
    @status = status if status.present?
    @sent_via = check.send_via
    @routing_number = "xxxx#{check.routing_number.last(4)}" if check.routing_number.present?
    @account_number = "****#{check.last4}" if check.last4.present?
    @description = check.description if check.description.present?
    @company_name = check.try(:user).try(:company).try(:name)
    @name = location.try(:business_name)
    @bank_name = check.ach_gateway.try(:descriptor)
    @brand = trans.try(:tags).try(:[],"card").try(:[],"brand")
    notes = notes.present? ? JSON(notes) : ''
    @customer_note = notes["customer_note"]
    @internal_note = notes["internal_note"]
    @date = check.created_at.in_time_zone("Pacific Time (US & Canada)").try(:strftime, "%m/%d/%Y %I:%M:%S %p")
    @for=check.instant_ach? ? 'ACH' : check.instant_pay? ? 'Push to Card' : 'Check' if check.present?
    if @status == "FAILED"
      mail(to: @user.email, subject: "P2C withdrawal failed")
    else
      unless @status === "IN_PROGRESS" || @status === "IN_PROCESS"
        mail(to: @user.email, subject: "Push to Card Transaction Confirmation")
      end
    end
  end

  def check_status_change_email(check_id,status,notes)
    check = TangoOrder.find check_id
    @tango_id = check_id
    @user = check.user if check.present?
    @recipient = check.try(:recipient)
    @wallet_id = check.try(:wallet_id)
    @amount = check.actual_amount || check.amount
    @status = status if status.present?
    @sent_via = check.send_via
    @routing_number = "xxxx#{check.routing_number.last(4)}" if check.routing_number.present?
    @account_number = "xxxx#{check.account_number.last(4)}" if check.account_number.present?
    @description = check.description if check.description.present?
    @company_name = check.try(:user).try(:company).try(:name)
    @name = check.name
    @status = check.status
    @bank_name = check.ach_gateway.try(:descriptor)
    notes = notes.present? ? JSON(notes) : ''
    @customer_note = notes["customer_note"]
    @internal_note = notes["internal_note"]
    @date = check.created_at.strftime("%m/%d/%Y")
    @for=check.instant_ach? ? 'ACH' : check.instant_pay? ? 'Push to Card' : 'Check' if check.present?
    if status == 'PAID_WIRE'
      @status = 'PAID'
    else
      @status = status if check.present?
    end
    if @status == "IN_PROGRESS" || @status == "IN_PROCESS"
      mail(to: @user.email, subject: "E-Check has been processed")
    else
      mail(to: @user.email, subject: "E-#{@for} Withdraw #{@status}")
    end
  end

  def account_cancel(data,date)
    @date = date
    @user = data
    merchant = @user.user
    @merchant_name = merchant.try(:name)
    @merchant_id = merchant.try(:id)
    @support_number = "(619) 631-4838"
    @email = "support@greenboxpos.com"
    mail(to: merchant.email, subject: "Your Account is Cancelled")
  end

  def account_cancel_partner(user,date)
    @date = date
    @user = user
    @user_name = @user.try(:name)
    mail(to: @user.email, subject: "Your Account is Cancelled")
  end

  def integration(user_id)
    @user = User.find_by(id: user_id)
    mail(to: @user.email, subject: "QuickCard Integration Information")
  end

  def insufficient_balance
    @dba_name = "ARTech"
    @account_id = "12345"
    @amount = "1253"
    @case_no = "ABC-123"

    mail(to: "i140105@nu.edu.pk" , subject: "Insufficient Balance Alert")
  end

  def two_step_veification(user, subject = 'QuickCard Verification Code')
    @random_code = rand(1_00000..9_99999)
    user.direct_otp = @random_code
    user.pin_code = @random_code
    user.direct_otp_sent_at = DateTime.now.utc
    user.save
    @user = user
    # smpt_auth
    mail(to: @user.email, subject: subject)
  end

  def iso_email(user,name,user_id,location)
    @user = User.find_by(id: user_id)
    @location_details = location
    @iso_name = name
    mail(to: user.email, subject: "Merchant Onboarding Notice")
  end

  private

  def smpt_auth
    smpt = Net::SMTP.start('smtp.sendgrid.net', 587, 'heroku.com', 'app53227029@heroku.com', 'xwkeftaw5401', :login)
    smpt.set_debug_output $stderr
  end

  def get_payment_urls

    ach_url = "#"
    quickcard_url = "#"
    card_url = "#"

    if @request.payment_option.try(:[],"ach")
      products = {}
      tax = @request.tax
      @request.products.each_with_index {|p, i|
        products[i] = {product_id: p.id, name: p.name, description: p.description, amount: p.amount, quantity: p.quantity}
      }
      website_url=JSON.parse(@location.web_site).first if @location.try(:web_site).present?
      ach_url_params = { shipping: {id: @shipping.id, name: @shipping.name, first_name: @shipping.name, last_name: @shipping.name, email: @shipping.email.present? ? @shipping.email : @user.email, phone_no: @shipping.phone_number.present? ? @shipping.phone_number : @user.phone_number, street: @shipping.street, city: @shipping.city, state: @shipping.state, country: @shipping.country, zip: @shipping.zip},
                          billing: {id: @billing.id, name: @billing.name, first_name: @billing.name, last_name: @billing.name, email: @billing.email.present? ? @billing.email : @user.email, phone_no: @billing.phone_number.present? ? @billing.phone_number : @user.phone_number, street: @billing.street, city: @billing.city, state: @billing.state, country: @billing.country, zip: @billing.zip},
                          auth_token: @merchant.authentication_token,
                          products: products,
                          success_url: ENV['APP_ROOT'],
                          fail_url: ENV['APP_ROOT'],
                          request_id: @request.id,
                          total_tax: tax,
                          total_amount: @request.total_amount,
                          shipping_amount: @request.shipping_handling_fee,
                          location_id: @location.try(:location_secure_token),
                          website_url: website_url,
                          invoice_checkout: "true",
                          exp: (@request.due_date).to_i
      }
      request_url = JWT.encode ach_url_params, Rails.application.secrets.secret_key_base, ALGORITHM
      ach_url = get_invoice_rtp_info_api_checkout_index_url(request_url: request_url)
    end
    if @request.payment_option.try(:[],"quickcard")
      request_role = @request.try(:sender).try(:merchant?) ? "merchant" : "partner"
      intended_url = @request.try(:sender).try(:merchant?) ? quickcard_payment_v2_merchant_invoices_url : quickcard_payment_v2_partner_invoices_url
      payload = {intended_url: intended_url, invoice_id: @request.id, exp: (@request.due_date).to_i}
      request_url = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
      if @request.try(:sender).try(:merchant?)
        quickcard_url = quickcard_payment_v2_merchant_invoices_url(request_url: request_url, role: request_role)
      else
        quickcard_url = quickcard_payment_v2_partner_invoices_url(request_url: request_url, role: request_role)
      end
    end
    if @request.payment_option.try(:[],"card")
      products = {}
      tax = @request.tax_method == "percent" ? (@request.tax / 100)* @request.amount.to_f : @request.tax
      @request.products.each_with_index {|p, i|
        products[i] = {product_id: p.id, name: p.name, description: p.description, amount: p.amount, quantity: p.quantity}
      }
      website_url=JSON.parse(@location.web_site).first if @location.try(:web_site).present?
      card_url_params = { shipping: {id: @shipping.id, name: @shipping.name, email: @shipping.email, phone_no: @shipping.phone_number, street: @shipping.street, city: @shipping.city, state: @shipping.state, country: @shipping.country, zip: @shipping.zip},
                          billing: {id: @billing.id, name: @billing.name, email: @billing.email, phone_no: @billing.phone_number, street: @billing.street, city: @billing.city, state: @billing.state, country: @billing.country, zip: @billing.zip},
                          auth_token: @merchant.authentication_token,
                          products: products,
                          success_url: ENV['APP_ROOT'],
                          fail_url: ENV['APP_ROOT'],
                          request_id: @request.id,
                          total_tax: tax,
                          total_amount: @request.total_amount,
                          location_id: @location.try(:location_secure_token),
                          website_url: website_url,
                          invoice_checkout: "true",
                          exp: (@request.due_date).to_i,
                          percent_tax: @request.tax,
                          amount: @request.amount.to_f
      }
      request_url = JWT.encode card_url_params, Rails.application.secrets.secret_key_base, ALGORITHM
      card_url = invoice_checkout_api_checkout_index_url(request_url: request_url)
    end
    active_option = 0
    active_option = active_option + 1 if ach_url != "#"
    active_option = active_option + 1 if quickcard_url != "#"
    active_option = active_option + 1 if card_url != "#"
    return {ach_url: ach_url, quickcard_url: quickcard_url, card_url: card_url, active_option: active_option}
  end
end
