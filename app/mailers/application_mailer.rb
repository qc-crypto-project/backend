class ApplicationMailer < ActionMailer::Base
  default from: "admin@quickcard.me"
  layout 'mailer'
end
