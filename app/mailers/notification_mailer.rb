class NotificationMailer < ApplicationMailer
  default from: "QuickCard"
  
  def send_email(user)
    @user = user
    mail(to: @user, subject: 'Quick Card').deliver_later
  end
end
