class NotificationRelayJob < ApplicationJob
	queue_as :default

	def perform(notification)
	    html = ApplicationController.render partial: "v2/merchant/shared/notification_body", locals: {noti: notification}, formats: [:html]
	    ActionCable.server.broadcast "customer_notification_channel_#{notification.recipient_id}", html: html , id: notification.id
	end

end