class GiftcardChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    stream_from "giftcard_channel_#{current_session}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    ActionCable.server.broadcast 'giftcard_channel', message: data['message']
  end

end

