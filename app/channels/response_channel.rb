class ResponseChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    stream_from "response_channel_#{current_session}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    ActionCable.server.broadcast 'response_channel', message: data['message'] , error_code: data['code']
  end

end

