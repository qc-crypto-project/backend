class ReportChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    stream_from "report_channel_#{current_session}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    ActionCable.server.broadcast 'report_channel_2', message: data['message']
  end
end
