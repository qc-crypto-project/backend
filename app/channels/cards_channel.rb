class CardsChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    stream_from "cards_channel_#{current_session}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    ActionCable.server.broadcast "cards_channel_#{current_session}", message: data['message']
  end
end
