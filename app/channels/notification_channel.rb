class NotificationChannel < ApplicationCable::Channel

  def subscribed
    stream_from "notification_channel_#{self.current_notification.try(:id)}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
  	ActionCable.server.broadcast 'notification_channel_2', message: data['message']
  end
end
