module ApplicationCable
  class Connection < ActionCable::Connection::Base
  	  identified_by :current_session, :current_notification

    def connect
      self.current_session = request.session.id if request.session.present?
      self.current_notification = find_verified_user
      logger.add_tags 'ActionCable', current_notification.try(:email)
    end

    protected

    def find_verified_user
      if (current_user = User.find_by_id cookies.signed['user.id'])
        current_user
      # else
      #   reject_unauthorized_connection
      end
    end
  end
end
