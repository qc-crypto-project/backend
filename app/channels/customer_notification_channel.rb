class CustomerNotificationChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    stream_from "customer_notification_channel_#{self.current_notification.try(:id)}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

end
