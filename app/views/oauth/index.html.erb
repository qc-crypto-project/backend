<div class="container">
<div class="col-md-12">
<div id="api_main_content" class="col">
  <h1>Using OAuth 2.0</h1>
  <div class="api_doc page_oauth  card ">
    <p>OAuth 2.0 is a protocol that lets your app request authorization to private details in a user's Quickcard account without getting their password.</p>
    <p>Your app asks for specific <code>Permissions</code> and is rewarded with access tokens upon a user's approval.</p>
    <p>You'll need to <a href="/developer/apps">register your app</a> before getting started. A registered app is assigned a unique Client ID and Client Secret which will be used in the OAuth flow. The Client Secret should not be shared.</p>
    <p><strong><a href="#">Sign in/up with QuickCard</a></strong> is the best way to log individual members into your application.</p>

    <!--<ul>-->
      <!--<li><a href="#flow">Obtaining access tokens with OAuth 2.0</a></li>-->
      <!--<li><a href="#bots">User Access tokens</a></li>-->
      <!--<li><a href="#storage">Storing tokens securely</a></li>-->
      <!--<li><a href="#using_tokens">Using access tokens</a></li>-->
      <!--<li><a href="#redirect_urls">Setting up your Redirect URL</a></li>-->
      <!--<li><a href="#multiple">Handling multiple authorizations</a></li>-->
      <!--<li><a href="#revoking_tokens">Revoking tokens</a></li>-->
    <!--</ul>-->

    <p><a name="flow"></a></p>

    <a name="the_oauth_flow"></a><h2>The OAuth Flow</h2>

    <p>Quickcard uses OAuth 2.0's <a href="https://tools.ietf.org/html/rfc6749#section-4.1">authorization code grant flow</a> to issue access tokens on behalf of users.</p>

    <p>
      <img style="width: 50%;" src="https://a.slack-edge.com/bfaba/img/api/slack_oauth_flow_diagram@2x.png" alt="Negotiating tokens with Quickcard's OAuth 2.0 authorization flow" title="Negotiating tokens with Quickcard's OAuth 2.0 authorization flow" class="no_border">
    </p>

    <a name="step_1_-_sending_users_to_authorize_and_or_install"></a><h3>Step 1 - Sending users to authorize and/or install</h3>

    <p>Your web or mobile app should redirect users to the following URL:</p>

    <p><code>https://api.quickcard.me/oauth/authorize</code></p>
    <p>The following values should be passed as GET parameters:</p>

    <ul>
      <li><code>client_id</code> - issued when you created your app (required)</li>
      <li><code>redirect_uri</code> - URL to redirect back to after successfult authoentication (required)</li>
      <li><code>state</code> - unique string to be passed back upon completion (optional)</li>
    </ul>


    <p>The <code>state</code> parameter should be used to avoid forgery attacks by passing in a value that's unique to the user you're authenticating and checking it when auth completes.</p>
    <p>If the user is not signed in yet, the user will be asked to sign in OR sign up. That account will then be used as they complete the authorization flow.</p>

    <a name="step_2_-_users_are_redirected_to_your_server_with_a_verification_code"></a><h3>Step 2 - Users are redirected to your server with a verification code</h3>

    <p>If the user authorizes your app, Quickcard will redirect back to your specified <code>redirect_uri</code> with a temporary code in a <code>code</code> GET parameter, as well as a <code>state</code> parameter if you provided one in the previous step. If the states don't match, the request may have been created by a third party and you should abort the process.</p>

    <p class="alert alert_info"><i class="ts_icon ts_icon_info_circle"></i> Authorization codes may only be exchanged once and will expire 10 minutes after issuance.</p>

    <a name="step_3_-_exchanging_a_verification_code_for_an_access_token"></a><h3>Step 3 - Exchanging a verification code for an access token</h3>

    <p>If all is well, exchange the authorization code for an access token using the <a href="#"><code>oauth/token</code></a> API method.</p>

    <p><code>https://api.quickcard.me/oauth/token</code></p>
    <p>The following values should be passed as POST parameters:</p>
    <ul>
      <li><code>client_id</code> - issued when you created your app (required)</li>
      <li><code>client_secret</code> - issued when you created your app (required)</li>
      <li><code>code</code> - a temporary authorization code you got after authorize step (required)</li>
      <li><code>redirect_uri</code>  - must match the originally submitted URI (if one was sent) (optional here we store redirect_uri in first step OR use from the app settings if provided.)</li>
    </ul>

    <p>You'll receive a JSON response containing an <code>access_token</code> (among other details):</p>

<pre><code class="json hljs">{
  <span class="hljs-attr">"access_token"</span>: <span class="hljs-string">"xoxp-23984754863-2348975623103"</span>,
  <span class="hljs-attr">"scope"</span>: <span class="hljs-string">"API"</span>
  <span class="hljs-attr">"token_type"</span>: <span class="hljs-string">"Bearer"</span>
  <span class="hljs-attr">"state"</span>: <span class="hljs-string">"Value"</span>
  }
</code></pre>

    <p>These access tokens are also known as bearer tokens.</p>

    <p>You can then use this token to call <a href="#">API methods</a> on behalf of the user. The token will continue functioning until the installing user either revokes the token and/or uninstalls your application.</p>

    <p>Quickcard apps can be installed multiple times by the same user <em>and</em> additional users on the same workspace. Your app is considered "installed" as long as one of these tokens is still valid.</p>

    <p class="alert alert_info"><i class="ts_icon ts_icon_info_circle"></i> Please note that these access tokens do not expire.</p>

    <p>This is an opportunity to get users back to work by <a href="#">redirecting them to deep links</a> within Quickcard OR your Apps.</p>

    <p>If the user denies your request, Quickcard redirects back to your <code>redirect_uri</code> with an <code>error</code> parameter.</p>

<pre><code class="hljs javascript">http:<span class="hljs-comment">//yourapp.com/oauth?</span>
  error=access_denied
</code></pre>

    <p>Applications should handle this condition appropriately.</p>

    <p><a name="storage"></a></p>

    <a name="storing_tokens_and_credentials"></a><h2>Storing tokens and credentials</h2>

    <p>Store your application's credentials and user tokens with care.</p>

    <p><a name="using_tokens"></a></p>

    <a name="using_access_tokens"></a><h2>Using access tokens</h2>

    <p>The tokens awarded to your app can be used in requests to all Web API Endpoints.</p>

    <p><a name="redirect_urls"></a></p>

    <a name="redirect_uris"></a><h2>Redirect URIs</h2>

    <p>The <code>redirect_uri</code> parameter is optional. If left out, Quickcard will redirect users to the Redirect URL configured in your app's settings. If provided, the redirect URL's host and port must exactly match the callback URL. The redirect URL's path must reference a subdirectory of the callback URL.</p>

<pre><code class="hljs http"><span class="hljs-attribute">CALLBACK</span>: http://example.com/path

<span class="javascript">GOOD: https:<span class="hljs-comment">//example.com/path</span>
GOOD: http:<span class="hljs-comment">//example.com/path/subdir/other</span>
BAD:  http:<span class="hljs-comment">//example.com/bar</span>
BAD:  http:<span class="hljs-comment">//example.com/</span>
BAD:  http:<span class="hljs-comment">//example.com:8080/path</span>
BAD:  http:<span class="hljs-comment">//oauth.example.com:8080/path</span>
BAD:  http:<span class="hljs-comment">//example.org</span>
</span></code></pre>

    <!--<a name="verifying_user_identity_without_installing_anything"></a><h3>Verifying user identity without installing anything</h3>-->

    <!--<p>Use <a href="#">Sign in with Quickcard</a> instead when you just want to log members in and verify their identity without having them "install" something. If you need to ask for specific authorization scopes from a user, you can switch to the <a href="/docs/slack-button">Add to Slack</a> flow to request them. Additional scopes awarded there will be appended to the same OAuth token for that user.</p>-->

    <p><a name="revoking_tokens"></a></p>

    <a name="revoking_tokens"></a><h2>Revoking tokens</h2>

    <p>If you want to dispose of an OAuth token, use <a href="#"><code>oauth/revoke</code></a>. It works with all access tokens. Its a POST Request</p>

    <p><code>https://api.quickcard.me/oauth/revoke</code></p>
    <p>The following values should be passed as POST parameters:</p>
    <ul>
      <li><code>token</code> - which you want to revoke (required)</li>
    </ul>

    <p>By revoking the last token associated with your application will be effectively uninstalled for that user.</p>

    <p>Revoking tokens and asking a user to authenticate again is the best way to <em>start over</em>.</p>
  </div>
</div>
</div>
</div>