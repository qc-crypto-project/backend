json.extract! admins_ach_gateway, :id, :bank_name, :descriptor, :status, :archive, :bank_fee_percent, :transaction_limit ,:bank_fee_dollar, :created_at, :updated_at
json.url admins_ach_gateway_url(admins_ach_gateway, format: :json)
