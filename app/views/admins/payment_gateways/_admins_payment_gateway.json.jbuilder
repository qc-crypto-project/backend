json.extract! admins_payment_gateway, :id, :type, :name, :client_secret, :client_id, :user_id, :created_at, :updated_at
json.url admins_payment_gateway_url(admins_payment_gateway, format: :json)
