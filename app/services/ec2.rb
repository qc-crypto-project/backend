# frozen_string_literal: true

require_relative 'parameter_store'

class EC2
  def stack
    return 'heroku' if heroku_app?
    client = Aws::EC2::Client.new(
      region: region,
      credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],ENV['AWS_SECRET_ACCESS_KEY']),
    )
    resp = client.describe_tags(
      filters: [{ name: 'resource-id', values: [instance_id] }],
    )
    resp.tags.find { |t| t.key == 'Stack' }&.value
  end

  def parameter_store
    store = ParameterStore.new(region, stack)
    store.fetch_parameters
    store
  end

  private

  def region
    return ENV['AWS_REGION'] || 'us-west-2' if heroku_app?
    rurl = 'http://169.254.169.254/latest/meta-data/placement/availability-zone'
    get(rurl).chop # chop the az code
  end

  def instance_id
    return 'heroku-app' if heroku_app?
    iurl = 'http://169.254.169.254/latest/dynamic/instance-identity/document'
    JSON.parse(get(iurl))['instanceId']
  end

  def get(url)
    Net::HTTP.get_response(URI.parse(url)).body
  end

  def heroku_app?
    ENV.fetch("HEROKU_APP") { false }
  end
end
