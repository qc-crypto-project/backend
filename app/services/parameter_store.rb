# frozen_string_literal: true

class ParameterStore
  attr_accessor :params

  def initialize(region, stack)
    @client = Aws::SSM::Client.new(
      credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],ENV['AWS_SECRET_ACCESS_KEY']),
      region: region
    )
    @stack = stack
    @params = {}
  end

  def fetch_parameters
    puts "FETCHING #{@stack} server ENV vars from AWS Parameter Store..."
    next_token = nil
    opts = {path: prefix, with_decryption: true}
    loop do
      res = @client.get_parameters_by_path(opts.merge(next_token: next_token))
      res.parameters.each do |param|
        name = param.name.split("/").last
        @params[name] = param.value
      end
      next_token = String(res.next_token)
      break if next_token.empty?
    end
  end

  def key?(key)
    @params.key?(key)
  end

  def fetch(key, *fallback)
    @params.fetch(key, *fallback)
  end

  private

  def prefix
    "/quickcard/#{@stack}/"
  end
end
