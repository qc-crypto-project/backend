class V2::Partner::SettingsController < V2::Partner::BaseController
	def fee_structure
		@user = current_user
		fee = current_user.fees
		@user_data = fee.try(:high).first
		@buyrate_dispensary = fee.try(:low).first
		system_fee = current_user.system_fee
		@withdraw_fee = system_fee["gitcard_fee"].to_f + system_fee["ach_dollar"].to_f + system_fee["send_check"].to_f + system_fee["push_to_card_dollar"].to_f
		@failed_ach_check = system_fee["failed_ach_fee"].to_f + system_fee["failed_check_fee"].to_f
	end
end