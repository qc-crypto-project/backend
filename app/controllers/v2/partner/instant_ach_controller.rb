class V2::Partner::InstantAchController < V2::Partner::BaseController

	before_action :update_system_fee , only: [:new,:create]

	class CheckError < StandardError; end


	def index
		@q = TangoOrder.ransack(params[:q])

		if params[:custom].present?
			if params[:custom] == "true"
				params[:first_date] = DateTime.parse(params[:first_date])
				params[:second_date] = DateTime.parse(params[:second_date])
			end
		end

		page_size = params[:filter].present? ? params[:filter].to_i : 10
		@achIssueEnabled = true
		@filters = [10,25,50,100]
		@total_paid = @total_void = @total_pending = @total_failed = @total_in_process = @total_paid_wire = number_with_precision(0, :precision => 2, :delimiter => ',')
	params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )

	if params[:q].present?
		params[:q][:data_search] = params[:q][:name_cont] if params[:q][:data_search].blank? && params[:q][:name_cont].present?
		params[:q]["name_or_account_number_or_wallet_location_business_name_cont"] = params[:q][:data_search]
			params[:q]["id_eq"] = params[:q][:data_search]
			params[:q]["total_fee_eq"] = params[:q][:data_search] if !params[:q][:data_search].is_a? String
			params[:q]["amount_eq"] = params[:q][:data_search]
			params[:q]["actual_amount_eq"] = params[:q][:data_search]
		end
				user_ids=current_user.id
		if params[:wallet_id].present?
			@total_achs = TangoOrder.where(order_type: [5,6] ,wallet_id: params[:wallet_id])
		else
			@total_achs = TangoOrder.where(order_type: [5,6] ,user_id: user_ids)
		end
		@total_achs = @total_achs.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
		@total_achs = @total_achs.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
			@achs = @total_achs.order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
		if @total_achs.present?
			@total_paid = @total_achs.PAID.sum(:amount)
			@total_paid =  number_with_precision(@total_paid, precision: 2, delimiter: ',')
			@total_pending = @total_achs.PENDING.sum(:amount)
			@total_pending =  number_with_precision(@total_pending, precision: 2, delimiter: ',')
			@total_void = @total_achs.VOID.sum(:amount)
			@total_void =  number_with_precision(@total_void, precision: 2, delimiter: ',')
			@total_failed = @total_achs.FAILED.sum(:amount)
			@total_failed =  number_with_precision(@total_failed, precision: 2, delimiter: ',')
			@total_in_process = @total_achs.IN_PROCESS.sum(:amount)
			@total_in_process =  number_with_precision(@total_in_process, precision: 2, delimiter: ',')
			@total_paid_wire = @total_achs.PAID_WIRE.sum(:amount)
	    	@total_paid_wire =  number_with_precision(@total_paid_wire, precision: 2, delimiter: ',') 
		end
	render partial: 'v2/partner/instant_ach/ach_index_datatable' if request.xhr?
	end

	def show
		@instant_ach= TangoOrder.find params[:id]
		account = ''
		wallet = Wallet.find_by(id: @instant_ach.wallet_id)
		account = wallet.try(:name) if wallet.present?
		amount = number_with_precision(@instant_ach.actual_amount, precision: 2, delimiter: ',')
		fee = number_with_precision(@instant_ach.check_fee.to_f + @instant_ach.fee_perc.to_f, precision: 2, delimiter: ',')
		total_amount = number_with_precision(@instant_ach.amount, precision: 2, delimiter: ',')
		render json: {"ach_id"=>@instant_ach.id,"account" => account,"recipient"=>@instant_ach.try(:recipient),"memo"=>@instant_ach.description,
			"number" => @instant_ach.try(:account_number),"status"=>@instant_ach.try(:status),"amount"=>amount,"fee"=>fee,"total_amount"=>total_amount
		}
	end

	def location_data
      	if params[:location_id].present? && params[:location_id].to_i != 0
        	@balance = show_balance(params[:location_id])
      		@fee_dollar = @user.system_fee.try(:[],'ach_dollar')
      		@fee_perc = @user.system_fee.try(:[],'ach_percent')
      		if @user.system_fee.try(:[],'ach_limit_type') == "Transaction"
	            @ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
	            @ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
      		elsif @user.system_fee.try(:[],'ach_limit_type') == "Day"
          		date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          		instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").instant_ach
          		@ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
          		@total_count = instant_achs.sum(:actual_amount)
          		@ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
      		end
      	else
        	@balance = 0
      	end

	   	if @user.iso? || @user.agent? || @user.affiliate? 
	    	merchant_user = false
	    end
	    wallet_id = nil
	    system_fee = nil
	    if @user.try(:[],'system_fee').try(:[],'bank_account').present?
	    	system_fee = true
	    	d_account = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user['system_fee']['bank_account'])
            d_account = "****#{d_account.last(4)}" if d_account.present?
            wallet_id = @user.try(:wallets).try(:first).try(:primary).try(:id)
        end

        multiple_bank_details = []
        @balance =  number_with_precision(@balance, precision: 2, delimiter: ',') 
        location_bank_details = nil
	    render json: {"balance" => @balance,"fee_dollar" => @fee_dollar , "fee_perc" => @fee_perc,
	    	"merchant_user" => merchant_user ,"banks" => @banks , "d_account" => d_account , "wallet_id" => wallet_id , "system_fee" => system_fee,
	    	"location" => @location , "location_bank_details" => location_bank_details , "multiple_bank_details" => multiple_bank_details,"ach_limit" => @ach_limit , "ach_limit_type" => @ach_limit_type , "total_count" => @total_count
	    }
  	end

	def new
		flash = []
		@locations = @user.wallets.primary.try(:first)
		if @user.system_fee.try(:[],'ach_limit_type') == "Day"
			date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
			instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",@locations.id,date,"VOID").instant_ach
			# @ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
			@total_count = instant_achs.sum(:actual_amount)
			# @ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
		end
		render partial: "new"
	end

	def create
	    begin
	      	if params[:bank_routing].blank?
	        	result = setup_params
	        	raise "#{result.second}" if result.first
	      	end
	      	validate_parameters(
	          	{
	              location_id: params[:location_id],
	              bank_name: params[:bank_name],
	              amount: params[:amount],
	              bank_account_type: params[:bank_account_type],
	              bank_routing: params[:bank_routing],
	              bank_account: params[:bank_account],
	              confirm_bank_account: params[:confirm_bank_account],
	              email_address: params[:email_address],
	              description: params[:description]
	          	}
	      	)
	      	raise I18n.t("errors.withdrawl.unmatch_routing_number") if params[:bank_account] != params[:confirm_bank_account]
	      	raise I18n.t("errors.withdrawl.amount_must_greater_than_zero") if params[:amount].to_f <= 0
	      	ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'

	      	  wallet = Wallet.find_by(id: params[:location_id])
			    raise I18n.t('errors.withdrawl.wallet_not_exist') if wallet.blank?

			    amount = params[:amount].to_f

			    bank_name = params[:bank_name]
			    recipient = params[:email_address]
			    account_number = params[:bank_account]
			    routing_number = params[:bank_routing]
			    bank_account_type = params[:bank_account_type]
			    description = params[:description]
			    ach_afp = @user.affiliate_program? ? true : nil
			    send_via = "ACH"

			    fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
			    if fee_object.present?
			      send_check_fee = fee_object["ach_dollar"].present? ? fee_object["ach_dollar"].to_f : 0
			      fee_perc_fee = fee_object["ach_percent"].present? ? deduct_fee(fee_object["ach_percent"].to_f, amount) : 0
			    else
			      send_check_fee = 0
			      fee_perc_fee = 0
			    end

			    fee = send_check_fee + fee_perc_fee
			    amount_sum = amount + fee
			    balance = show_balance(params[:location_id])
			    raise I18n.t("merchant.controller.insufficient_balance") if balance < amount_sum
			    if @user.agent? || @user.affiliate? || @user.affiliate_program?
			      raise CheckError.new("Error Code 5028 Ach creation failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0)
			    elsif @user.iso?
			      raise CheckError.new("Error Code 5028 Ach creation failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0) && @user.iso?
			    end
			    rand = "#{rand(100..999)}#{Time.now.to_i.to_s}"
			    card_info = {
			        :account_number => account_number,
			        :account_type => bank_account_type
			    }
			    card_info = card_info.to_json
			    account_number = account_number.last(4)
			    encrypted_account_info = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{wallet.id}", card_info)
			    escrow_wallet = Wallet.check_escrow.first
			    escrow_user = escrow_wallet.try(:users).try(:first)
			    ach_international = @user.system_fee["ach_international"]
			    ach_international = false if ach_international.nil?
			    check = TangoOrder.new(user_id: @user.id,
			                           name: bank_name,
			                           status: "PENDING",
			                           amount: amount_sum,
			                           actual_amount: amount,
			                           check_fee: send_check_fee,
			                           fee_perc: fee_perc_fee,
			                           recipient: recipient,
			                           checkId: rand,
			                           description: description,
			                           order_type: :instant_ach,
			                           batch_date:Time.zone.now + 1.day,
			                           approved: false,
			                           account_type: bank_account_type,
			                           wallet_id: wallet.id,
			                           settled: :false,
			                           account_token: encrypted_account_info,
			                           send_via: send_via,
			                           account_number: account_number,
			                           routing_number: routing_number,
			                           amount_escrow: true,
			                           ach_international: ach_international,
			                           ach_afp: ach_afp)

			    sender_dba=get_business_name(wallet)
			    transaction = @user.transactions.create(
			        to: recipient,
			        from: nil,
			        status: "pending",
			        charge_id: nil,
			        amount: amount,
			        sender: @user,
			        sender_name: sender_dba,
			        sender_wallet_id: wallet.id,
			        receiver_id: escrow_user.try(:id),
			        receiver_wallet_id: escrow_wallet.try(:id),
			        receiver_name: escrow_user.try(:name),
			        sender_balance: balance,
			        receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
			        action: 'transfer',
			        fee: fee.to_f,
			        net_amount: amount_sum.to_f - fee.to_f,
			        net_fee: fee.to_f,
			        total_amount: amount_sum.to_f,
			        ip: get_ip,
			        main_type: "ACH"
			    )
			    withdraw = withdraw(amount,wallet.id, fee.to_f, TypesEnumLib::GatewayType::ACH,account_number,send_via,nil, nil, nil, nil,TypesEnumLib::GatewayType::Quickcard, "other",true)
			    puts "============WITHDRAW==================", withdraw
			    if withdraw.present? && withdraw["actions"].present?
			      parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
			      save_block_trans(parsed_transactions) if parsed_transactions.present?
			      check.transaction_id = transaction.id
			      check.save
			      transaction.update(seq_transaction_id: withdraw["actions"].first["id"],
			                         status: "approved",
			                         privacy_fee: 0,
			                         sub_type: withdraw["actions"].first.tags["sub_type"],
			                         timestamp: withdraw["timestamp"],
			                         tags: withdraw["actions"].first.tags
			      )
						type=current_user.affiliate_program? ? 'affiliate_activity' : nil
			      ActivityLog.log(type,"ACH Created",current_user,params,response,"ACH Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","ach")
			      flash[:success] = "Successfully sent a ACH to #{ recipient }"
			    else
			      flash[:error] = I18n.t 'merchant.controller.unaval_feature'
			    end



	    rescue => exe
	      	flash[:error] = exe.message
	    ensure
	      	if flash[:error].blank? && flash[:success].blank?
	        	flash[:error] = I18n.t 'merchant.controller.except'
	      	end
	      	if flash[:error].blank?
	      		ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: ["**** #{check.account_number}"], ach_id: [check.id]
					else
						if flash[:error].include?('Error Code')
							str = flash[:error]
							message = str[16, str.length-1]
							code = str[0,15]
						else
							message = flash[:error]
							code = " Error Code #"
						end
	      		ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error',error_code: code, message: message
	      	end
			end
			flash.discard
	end

   def setup_params
      params[:bank_name] = @user['system_fee']['bank_account_name']
      params[:bank_account_type] =  @user['system_fee']['bank_account_type']
      params[:bank_routing] = @user['system_fee']['bank_routing']
      params[:bank_account] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])
      params[:confirm_bank_account] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])
      params[:email_address] = @user.email
      return [false,nil]
  end

  	def update_system_fee
    	if @user.admin_user_id.present? && @user.affiliate_program?
      		admin_user = User.find(@user.admin_user_id)
      		user_system_fee = @user.system_fee
      		@user.system_fee = admin_user.present? && admin_user.try(:system_fee) != "{}" ? admin_user.system_fee : {}
      		@user.system_fee["bank_account"] =  user_system_fee["bank_account"]
      		@user.system_fee["bank_routing"] =  user_system_fee["bank_routing"]
      		@user.system_fee["bank_account_name"] =  user_system_fee["bank_account_name"]
			@user.system_fee["bank_account_type"] =  user_system_fee["bank_account_type"]
    	end
  	end

end