class V2::Partner::BaseController < ApplicationController
  require 'uri/http'
  require 'uri/https'
  require 'net/http'
  require 'will_paginate/array'
  include V2::ApplicationHelper
  layout "v2/partner/layouts/application"

  before_action :check_partner , :authenticate_user!,:require_partner
  before_action :verify_tos_acceptance , except: [:welcome_partner,:send_tos,:update_password_tos,:tos_approval, :accept_cookies,:update_bank_details,:update_documents]

  def require_partner
	  if current_user && (current_user.iso? || current_user.qc? || current_user.affiliate_program? )
      @user=current_user
    elsif current_user && current_user.agent?
      @user=current_user
    elsif current_user && current_user.partner?
      @user = current_user
    elsif current_user && current_user.affiliate?
      @user = current_user
    else
      if params[:request_url].present?
        flash[:error] = "Please login to continue!"
        return redirect_to new_user_session_path(request_url: params[:request_url])
      else
        flash[:error] = I18n.t 'devise.failure.page_not_exist'
        return redirect_to new_user_session_path
      end
    end
  end

  def accept_cookies
    if current_user.present? && !current_user.cookies_accept
      current_user.update(cookies_accept: true)
    end
  end

  def welcome_partner
    if current_user.affiliate_program?
      if current_user.system_fee != "{}" || current_user.documentations.present?
        render template: 'v2/partner/welcome/affiliate_programs/steps2'
      else
        render template: 'v2/partner/welcome/affiliate_programs/steps1'
      end
    else
      render template: 'v2/partner/welcome/index'
    end
  end


  def verify_tos_acceptance
    return unless  current_user.iso? || current_user.agent? || current_user.affiliate? || current_user.affiliate_program?
    if current_user.iso? || current_user.agent? || current_user.affiliate? || current_user.affiliate_program?
      if current_user.tos_checking == false || current_user.tos_acceptance_date.blank?
        return redirect_to v2_partner_welcome_path
      end
    end
  end

  def send_tos
    user = current_user
    if UserMailer.send_tos(params[:email], user).deliver_later
      render status: 200,json:{success: 'success'}
    else
      render status: 404,json:{success: 'failed'}
    end
  end

  def update_password_tos
    if current_user.valid_password?(params[:old_password])
      if params[:new_password] != params[:old_password]
        if current_user.update(password: params[:new_password])
          sign_in(current_user, :bypass => true)
          bypass_sign_in current_user
          render status: 200,json:{success: 'success'}
        else
          render status: 404,json:{success: current_user.errors.full_messages.first}
        end
      else
        render status: 200,json:{success: 'success'}
      end
    else
      render status: 404,json:{success: 'failed'}
    end
  end


  def update_bank_details
    @user.system_fee = {"bank_account_type"=>params[:bank_account_type],"bank_account_name"=>params[:bank_account_name],"bank_routing"=>params["bank_routing"],"bank_account"=>params["bank_account"]}
    @user.system_fee["bank_account"] = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user.system_fee["bank_account"])
    params["image"] = params["FileAttachment"]
    if params["FileAttachment"].present?
      wallet_id = @user.wallets.first.id
      params["usage_id"] = wallet_id
      params["location_id"] = wallet_id
      @old_doc= Image.where(location_id: params[:location_id]).last if params[:location_id].present?
      @old_doc.delete if @old_doc.present?

      @doc= Image.new(add_image: params["FileAttachment"],usage_id: wallet_id,location_id: wallet_id)
      if @doc.validate
        puts 'added image================================================='
        @doc.save
      else
        @error = @doc.errors.full_messages.first
        puts "============================================================",@error
      end
    end
    if @user.save
      return render json: {success: "true"}
    end
  end

  def update_documents
    params["documentation"] = {}
    params["documentation"]["w9_form_image"] = [params["FileAttachment2"]]
    save_user_documentation(@user, params[:documentation])
    return render json: {success: "true"}
  end


  def tos_approval
    user = current_user
    # UserMailer.integration(user.id).deliver_later
    if user.update(tos_checking: true,tos_user_id: user.id,tos_acceptance_date: Time.now.utc)
      render status: 200,json:{success: 'success'}
    else
      render status: 404,json:{success: 'failed'}
    end
  end

  def activity_logs
    Time.zone = cookies[:timezone]
    if current_user.merchant_id.present? && @user_permission.admin?
      @main_merchant =  User.find current_user.try(:merchant_id)
      @users = @main_merchant.sub_merchants.active_users
    else
    @users = current_user.sub_merchants.active_users
    end
    admin_id = User.where(role: 'admin').first.id
    if params[:search] == "true"
      activity_logs = []
      today = yesterday = week = month = year = 0
      show_more = params[:last_id].present? ? (["activity_logs.id < ?", params[:last_id]]) : []
      if params[:filter].present?
        if current_user.merchant_id.present? && @user_permission.admin?
          condition = params[:person].present? ? {action_type: params[:action_type], user_id: params[:person]} : {action_type: params[:action_type],user_id: @users.ids.push(@main_merchant.id)}
        else
          condition = params[:person].present? ? {action_type: params[:action_type], user_id: params[:person]} : {action_type: params[:action_type],user_id: @users.ids.push(current_user.id)}
        end
        case params[:filter]
        when "today_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).today.order(id: "desc").limit(10)
        when "yesterday_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).yesterday.order(id: "desc").limit(10)
        when "week_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).past_week.order(id: "desc").limit(10)
        when "month_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).past_month.order(id: "desc").limit(10)
        when "year_filter"
          date = params[:date_val]
          date = parse_daterange_activity(date)
          first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
          second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
          activity_counts = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(created_at: first_date..second_date).length
          activity_logs =   ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).where(created_at: first_date..second_date).order(id: "desc").limit(10)
        end
      elsif params[:person].present?
        activity_logs = ActivityLog.joins(:user).where(show_more).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(action_type: params[:action_type], user_id: params[:person]).order(id: "desc").limit(10)
        today = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).today.count
        yesterday = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).yesterday.count
        week = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).past_week.count
        month = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).past_month.count
        year = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).past_year.count
      end
      if params[:filter] == "year_filter"
        render status: 200, json:{activity_logs: activity_logs,activity_counts: activity_counts, today: today, yesterday: yesterday, week: week, month: month, year: year}
      else
        render status: 200, json:{activity_logs: activity_logs, today: today, yesterday: yesterday, week: week, month: month, year: year}
      end
    else
      if params[:action_type] == "employees"
        if current_user.merchant_id.present? && @user_permission.admin?
          activity_logs = ActivityLog.where("activity_logs.action_type = ? AND CAST(activity_logs.params -> 'user' ->> 'merchant_id' AS integer) IN(?)", params[:action_type], @users.ids.push(@main_merchant.id))
        else
          activity_logs = ActivityLog.where("activity_logs.action_type = ? AND CAST(activity_logs.params -> 'user' ->> 'merchant_id' AS integer) IN(?)", params[:action_type], @users.ids.push(current_user.id))
        end
      else
        activity_logs = ActivityLog.where(action_type: params[:action_type],user_id: @users.ids.push(current_user.id, admin_id))
      end
      @today = activity_logs.today.count
      @yesterday = activity_logs.yesterday.count
      @week = activity_logs.past_week.count
      @month = activity_logs.past_month.count
      @year = activity_logs.past_year.count
      @activity_logs = activity_logs.today.order("id desc").limit(10)
      render :partial => 'v2/merchant/shared/activity_log'
    end
  end



  def update_notification
    notification =  Notification.find_by_id(params[:id])
    if notification.notifiable.present?
        if params[:hide_at] == "true" 
          if notification && notification.hide_at.nil? && notification.update!(hide_at: DateTime.now.utc)
            count = 0
            if notification && notification.read_at.nil? 
              notification.update!(read_at: DateTime.now.utc) 
              count = 1
            end
          render json: {success: true , count: count} , status: 200
          end
        elsif params[:unread] == "true" 
          if notification && notification.read_at.present?
            notification.update!(read_at: nil) if notification && notification.read_at.present?
            render json: {success: true} , status: 200
          end
        else
          if notification && notification.read_at.nil? && notification.update!(read_at: DateTime.now.utc)
            render json: {success: true} , status: 200
          end
        end
    else
      render json: {success: true} , status: 200
    end
  end

  def hide_all_notification
    if params[:type] == "read_all"
      load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where(read_at: nil)
      read_count = load_more_data.count
      load_more_data.update_all(read_at: DateTime.now.utc)
      render json: {"data"=>read_count}
    else
      if params[:type] == "yesterday"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 1)
      elsif params[:type] == "older"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) < ? ",Date.today - 2)
      elsif params[:type] == "today"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).today.where(hide_at: nil)
      elsif params[:type] == "day_before"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 2)
      elsif  params[:type] == "all"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil)
      end
      read_count = load_more_data.where(read_at: nil).count
      load_more_data.where(read_at: nil).update_all(hide_at: DateTime.now.utc, read_at: DateTime.now.utc)
      load_more_data.where(hide_at: nil).update_all(hide_at: DateTime.now.utc)
      render json: {"data"=>read_count}
    end
  end


  def load_notifications
    if params[:type] == "yesterday"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 1).order("created_at DESC").limit(10)
    elsif params[:type] == "older"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) < ? ",Date.today - 2).order("created_at DESC").limit(20)
    elsif params[:type] == "today"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).order("created_at DESC").today.limit(10)
    elsif params[:type] == "day_before"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 2).order("created_at DESC").limit(10) 
    end
    if load_more_data.present?
      render partial: 'v2/merchant/shared/load_more_notification' , locals: {load_more_data: load_more_data}
    else
      render json: {"data"=>false}
    end
  end
  
  def verify_user
    params[:email]=params[:email].downcase if params[:email].present?
    params[:phone_number]=params[:phone_number].downcase if params[:phone_number].present?
    wallet=''
    if params[:user_phone].present? && !params[:user_phone].blank?
      @user = User.unarchived_users.where.not(:role => :user).where(phone_number: params[:user_phone]).first
    elsif params[:ref_no].present? && !params[:ref_no].blank?
      @user = User.unarchived_users.where(ref_no: params[:ref_no].upcase).where.not(:role => :user).first
    elsif  params[:email].present? && params[:password].present?
      @user = current_user
      if @user.valid_password?(params[:password])
        @user = @user
      else
        @user= nil
      end
    elsif  params[:email].present?
      @user =  User.unarchived_users.where.not(role: 'user').find_by_email(params[:email])
      if @user.present?
        @user= @user
      else
        @user= nil
      end
    end
    if @user.present?
      @user.wallets.present? ? (wallet= @user.wallets.first) : (wallet = "No wallet")
      render status: 200, json:{success: 'Verified', :user => @user, :wallet => wallet, qr_codes: params[:qr_codes]} #used qr codes to be used in redeem qr functionality
    else
      render status: 200, json:{success: "not found",error: 'You are not Authorized for this action.'}
    end
  end

  def verifying_user
    phone_number = params[:phone_number]
    email = params[:email]
    table = User
    if email.present?
      email = email.downcase
      user = table.where.not(role:'user').complete_profile_users.where(email: email).first if params[:not_user] != 'notuser'
      user = table.where.not(role:'user').complete_profile_users.where(email:email).first if params[:not_user]=='notuser'
      ids=user.id if user.present?
      if user.present? && ids != params[:user_id].to_i && !user.archived?
        render status: 200, json:{success: 'notverified'}
      else
        render status: 200, json:{success: 'Verified', :user => @user}
      end
    elsif phone_number.present?
      user = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).first
      ids = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      res = Phonelib.valid?(phone_number)
      message = !res ? "Invalid Phone Number" : "A user with this phone number already exists"
      if ids.present? && ids.count > 0 && !ids.include?(params[:user_id].to_i)  && !user.archived? || !res
        render status: 200, json:{success: 'notverified',message: message}
      else
        render status: 200,json:{success: 'Verified', :user => @user}
      end
    end
  end



  private

  def check_partner
    if params[:request_url].present?
      if current_user.present?
        unless current_user&.iso? || current_user&.agent? || current_user&.affiliate?
          if current_user && current_user.merchant?
            return redirect_to quickcard_payment_v2_merchant_invoices_path(request_url: params[:request_url])
          else
            return redirect_to new_user_session_path(request_url: params[:request_url]), notice: "Please sign in to continue" unless current_user&.iso? || current_user&.agent? || current_user&.affiliate? || current_user&.affiliate_program?
          end
        end
      end
    elsif current_user && current_user.merchant?
      flash[:alert] = I18n.t("devise.failure.page_not_exist")
      return redirect_to v2_merchant_accounts_path
    else
      flash[:alert] = I18n.t("devise.failure.page_not_exist") unless current_user&.iso? || current_user&.agent? || current_user&.affiliate? || current_user&.affiliate_program?
      return redirect_to new_user_session_path, notice: "Please sign in to continue" unless current_user&.iso? || current_user&.agent? || current_user&.affiliate? || current_user&.affiliate_program?
    end
  end

end