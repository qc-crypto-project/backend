class V2::Partner::SalesController < V2::Partner::BaseController
  include V2::ApplicationHelper
  include V2::Merchant::SalesHelper
  include V2::Merchant::InvoicesHelper
  include SaleHandler
  # include Merchant::BaseHelper
  include RegistrationsHelper
  # include Api::RegistrationsHelper
  # before_action :authenticate_transaction , only: [:submit_virtual_terminal]
  include TransactionCharge::ChargeATransactionHelper
  # skip_before_action :set_wallet_balance, only: [:verify_email_and_phone_number, :submit_virtual_terminal]
  # skip_before_action :verify_tos_acceptance, only: [:virtual_terminal]

  def index
    wallets = load_wallets
    @list = wallets[:list]
    render template: 'v2/merchant/virtual_terminal/index'
  end

  def submit_virtual_terminal
    params[:phone_number] = params[:phone_code] + params[:phone_number]
    params[:amount] = params[:grand_total]
    # params validation
    card_number = params[:card_number].gsub(/\s+/, "")
    params[:card_number] = card_number
    if params[:existing_user] == "true" #- existing user
      if not (params[:wallet_id].present? and params[:verified_user_id].present? and params[:card_name].present? and card_number.present? and params[:card_cvv].present? and params[:exp_date].present? and params[:amount].present?)
        raise StandardError.new "Please fill out missing fields!"
      end
    else                                 #- new user
      if not (params[:wallet_id].present? and params[:card_name].present? and card_number.present? and params[:card_cvv].present? and params[:exp_date].present? and params[:amount].present? and params[:name].present? and params[:address].present? and params[:city].present? and params[:state].present? and params[:zip_code].present? and params[:phone_number].present? and params[:email].present?)
        raise StandardError.new "Please fill out missing fields!"
      end
    end
    calculation_for_fees(params)
    raise StandardError.new "Amount must be greater then 0" if params[:amount].to_f <= 0
    params["exp_date"] = params["exp_date"].split("/").join

    merchant = current_user.parent_merchant
    merchant = merchant.nil? ? current_user : merchant
    if merchant.oauth_apps.present?
      oauth_app = merchant.oauth_apps.first
      raise StandardError.new "You are blocked by admin. Please contact to administrator." if oauth_app.is_block
    end

    # getting wallet

    wallet = Wallet.where(id: params[:wallet_id]).first
    if wallet.present?
      @location = wallet.location

      # checking for location availabilty
      if @location.present?
        raise StandardError.new I18n.t("errors.withdrawl.blocked_location") if @location.is_block
        if !@location.virtual_terminal.nil? && @location.virtual_terminal
          raise StandardError.new I18n.t("api.registrations.unavailable") if @location.is_block
        end

        # checking for location sale limit, you are not allowed if sale is greater than limit
        if @location.sale_limit.present?
          if @location.sale_limit_percentage.present?
            sale_limit = @location.sale_limit
            sale_limit_percentage = @location.sale_limit_percentage
            total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
            raise StandardError.new I18n.t("api.registrations.trans_limit") if params[:amount].to_f > total_limit
          else
            raise StandardError.new I18n.t("api.registrations.trans_limit") if params[:amount].to_f > @location.sale_limit
          end
        end
      end
    end
    sender = set_user
    sender_wallet = sender.wallets.primary.first

    #creating card
    @card_customer = sender
    params[:card_holder_name] = params[:card_name]
    params[:card_exp_date] = params[:exp_date]
    result_card = getting_or_creating_card
    raise StandardError.new I18n.t('api.registrations.invalid_card') unless result_card[:status]
    card = result_card[:card]
    card_bin_list = result_card[:card_bin_list]
    ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: '25percent'
    params[:wallet_id] = sender.try(:wallets).try(:first).try(:id)
    params[:merchant_wallet_id] = wallet.try(:id)

    data = {}
    params[:both] = false
    gateway = @location.primary_gateway.present? ? @location.primary_gateway.key : merchant.payment_gateway
    merchant_balance = show_balance(wallet.id, merchant.ledger)
    buyRate = @location.fees.buy_rate.first
    amount_fee_check = check_fee_amount(@location, merchant, sender, wallet, buyRate, params[:amount], merchant_balance, nil, 'credit')
    if amount_fee_check.present? && amount_fee_check[:status] == false
      raise StandardError.new I18n.t("api.registrations.amount")
    end
    if amount_fee_check.present? && amount_fee_check[:status] == true
      if amount_fee_check[:splits].present?
        agent_fee = amount_fee_check[:splits]["agent"]["amount"]
        iso_fee = amount_fee_check[:splits]["iso"]["amount"]
        iso_balance = show_balance(@location.iso.wallets.primary.first.id)
        if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
          raise StandardError.new I18n.t("errors.iso.iso_0007") if iso_balance < iso_fee
        else
          raise StandardError.new I18n.t("errors.iso.iso_0008") if iso_balance + iso_fee < agent_fee.to_f
        end
      end
    end
    authenticate_transaction
    # checking for sender
    if sender.present?
      sender_wallet = sender.wallets.first
      balance = show_balance(sender_wallet.id, sender.ledger)
      fee_for_issue = 0
      if balance.to_f != params[:amount].to_f
        # if balance.to_f < params[:amount].to_f  # user's wallet doesnt have enough balance to sale
        params[:both] = true
        params[:issue_amount] = params[:amount].to_f
        qc_wallet = Wallet.qc_support.first
        issue_raw_transaction = IssueRawTransaction.new(sender.email,params,params[:card_cvv])
        seq_transaction_id = {:message => {:message => I18n.t('api.errors.no_gateway', location_contact_no: @location.try(:phone_number) , location_email: @location.try(:email)) }}
        # load balancer starts for issue amount to user
        # LOAD BALANCER FIRST payment_gateway
        if @location.primary_gateway.present?
          # LOAD BALANCER FIRST payment_gateway
          @payment_gateway = @location.primary_gateway if !@location.primary_gateway.is_block
          seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, sender, card_number,nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.secondary_gateway,card_bin_list)
          gateway = @payment_gateway.key if @payment_gateway.present?
          if @location.primary_gateway.is_block && seq_transaction_id.nil?
            seq_transaction_id = {:message => {:message => "Processing: Inactive – Please contact customer support."}}
          end
          # LOAD BALANCER secondary_payment_gateway
          if seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank? && @location.secondary_gateway.present? && !@location.secondary_gateway.is_block
            record_it(7, seq_transaction_id[:message][:message].present? ? seq_transaction_id[:message][:message] : "Decline From Bank")
            @payment_gateway = @location.secondary_gateway
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, sender, card_number,nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.ternary_gateway,card_bin_list)
            gateway = @payment_gateway.key
          end
          # LOAD BALANCER third_payment_gateway
          if seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank? && @location.ternary_gateway.present? && !@location.ternary_gateway.is_block
            record_it(7, seq_transaction_id[:message][:message].present? ? seq_transaction_id[:message][:message] : "Decline From Bank")
            @payment_gateway = @location.ternary_gateway
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, sender, card_number,nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,nil,card_bin_list)
            gateway = @payment_gateway.key
          end
        end
        if seq_transaction_id.try(:[],:blocked).present?
          raise StandardError.new I18n.t('merchant.controller.unaval_feature')
        elsif seq_transaction_id.try(:[], :decline_message).present?
          raise StandardError.new I18n.t('errors.withdrawl.decline_use_new_card')
        elsif seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank?
          if params[:order_bank].present? && params[:charge_id].present?
            params[:order_bank].update(status: "seq_crash")
          end
          if seq_transaction_id.blank?
            raise StandardError.new I18n.t('merchant.controller.unaval_feature')
          else
            raise StandardError.new "#{seq_transaction_id[:message][:message]}"
          end
        end
      end
    end
    data = seq_transaction_id if seq_transaction_id.present?
    ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: '50percent'
    # creating object for previous refund
    data = data.merge(get_card_info(card_number)) unless data.empty?
    data.delete(:gateway_fee_details) if data[:gateway_fee_details].present?
    balance = show_balance(sender.wallets.first.id, sender.ledger)
    raise StandardError.new I18n.t("merchant.controller.insufficient_balance") if balance.to_f < params[:amount].to_f
    @payment_gateway = @location.primary_gateway if @payment_gateway.blank?
    gateway = @payment_gateway.key if gateway.blank?
    @virtual_terminal_products = true
    current_transaction = transaction_between(params[:merchant_wallet_id].to_i,sender.wallets.first.id,params[:amount].to_f, TypesEnumLib::TransactionType::SaleType,0, nil, data,sender.ledger, gateway,TypesEnumLib::TransactionSubType::SaleVirtual,card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction,nil,nil,nil,@payment_gateway)
    raise StandardError.new I18n.t('api.registrations.sequence') if current_transaction.nil? || current_transaction.size <=0
    ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: '75percent'
    maintain_batch(merchant.id, params[:merchant_wallet_id].to_i, params[:amount].to_f,TypesEnumLib::Batch::Primary)
    if merchant.high?
      if @location.iso.present?
        maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High,nil,nil,nil,nil,nil,nil,current_transaction[:bonus] || nil)
      end
      maintain_batch(merchant.id, params[:merchant_wallet_id].to_i, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true,current_transaction[:iso_fee] || nil,current_transaction[:agent_fee] || nil,current_transaction[:affiliate_fee] || nil)
    elsif merchant.low?
      if @location.iso.present?
        maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low,nil,nil,nil,nil,nil,nil,current_transaction[:bonus] || nil)
      end
      maintain_batch(merchant.id, params[:merchant_wallet_id].to_i, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true,current_transaction[:iso_fee] || nil,current_transaction[:agent_fee] || nil,current_transaction[:affiliate_fee] || nil)
    end
    payment_gateway = nil
    if gateway.present?
      payment_gateway = PaymentGateway.find_by(key: gateway)
      if payment_gateway.present?
        message = sale_text_message(number_with_precision(params[:amount].to_f,precision:2), @location.business_name, payment_gateway.try(:name),card_number.last(4),sender.name, @location.cs_number || @location.phone_number)
        # Email/Text Notifications
        amount = number_to_currency(number_with_precision(params[:amount], precision: 2, delimiter: ','))
        UserMailer.transaction_email(@location.id,amount,(payment_gateway.try(:name) || '---'),sender.id,Date.today.to_json,card.brand,card_number.last(4),current_transaction[:id], params[:action]).deliver_later if @location.try(:email_receipt)
        twilio_text = send_text_message(message, sender.phone_number) if @location.try(:sms_receipt) && message && sender.try(:phone_number).present?
      end
    end
    push_notification(message, sender)
    record_it(6, "Successfully Done!")
    ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: '100percent', message: {"success"=> 'success', "card_brand" => card.try(:brand), "last4" => card.try(:last4), "descriptor" => payment_gateway.try(:name)}
  end

  def verify_email_and_phone_number
    response_json = false
    response_name = ""
    response_first_name = ""
    response_last_name = ""
    response_email = ""
    response_number = ""
    response_address = ""
    response_zip_code = ""
    response_state = ""
    response_country = ""
    user_role = ""
    ref_no = ""
    response_id = 0
    if current_user.merchant?

      if params[:phone_number].present? && params[:phone_code].present?
        params[:phone_number].rstrip!
        phonenumber = params[:phone_code] + params[:phone_number]
        if User.exists?(phone_number: phonenumber) || User.exists?(phone_number: params[:phone_number])
          user = User.unarchived_users.where(phone_number: [phonenumber,params[:phone_number]])
          user = user.first
          if user.present?
            user_role = user.role
            ref_no = user.ref_no
            if params[:for_new].present? || params[:for_existing].present?
              unless user_role == "user"
                response_json = false
              else
                response_json = true
              end
            else
              response_json = true
            end
            response_name = user.name
            response_first_name = user.first_name
            response_last_name = user.last_name
            response_email = user.email
            response_address = user.postal_address
            response_zip_code = user.zip_code
            response_city = user.city
            response_state = user.state
            response_country = user.country
            response_id = user.id
          else
            response_json = false
          end

        end
        if !Phonelib.valid?(phonenumber)
          response_phone_invalid = true
        end
      elsif params[:email].present?
        params[:email].rstrip!
        if User.user.exists?(email:params[:email])
          user = User.user.unarchived_users.where(email: params[:email])
          user = user.first
          # getting_user = user.user
          # if getting_user.blank?
          #   user = user.first
          # else
          #   user = getting_user.first
          # end
          if user.present?
            user_role = user.role
            if params[:for_new].present? || params[:for_existing].present?
              unless user_role == "user"
                response_json = false
              else
                response_json = true
              end
            else
              response_json = true
            end
            response_name = user.name
            response_number = user.phone_number
            response_id = user.id
            ref_no = user.ref_no
          else
            response_json = false
          end
        end
      end
    else
      if params[:phone_number].present? && params[:phone_code].present?
        phonenumber = params[:phone_code] + params[:phone_number]
        @user=User.unarchived_users.where.not(:role => :user).find_by(phone_number:phonenumber)
        if @user.present?
          @user= @user
        else
          @user= nil
        end
      end
    end
    if current_user.merchant?
      render json:{response_json:response_json,name:response_name,first_name:response_first_name,last_name:response_last_name,email:response_email,phone_number:response_number,address: response_address,zip_code: response_zip_code,city: response_city, state: response_state, country: response_country,user_role: user_role,response_id:response_id, invalid_phone:response_phone_invalid,ref_no: ref_no},status:200
    else
      if @user.present?
        render status: 200, json:{success: 'Verified', :user => @user}
      else
        render status: 404, json:{error: 'You are not Authorized for this action.'}
      end
    end
  end

  def country_data
    if params[:country].present?
      @states = Hash[CS.states(params[:country]).sort_by{|k,v| v}]
      if request.xhr?
        respond_to do |format|
          format.json {
            render json: {states: @states}
          }
        end
      end
    end
  end

end