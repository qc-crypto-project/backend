class V2::Partner::ExportsController < V2::Partner::BaseController
  include Zipline
  def index
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    Report.where(file_id: nil).update_all(read: true)
    user_id = current_user.merchant_id.present? ? current_user.merchant_id : current_user.id
    if params[:query].present?
      conditions = []
      parameters = []
      conditions << "name ILIKE ? OR report_type ILIKE ?"
      parameters << "%#{params[:query]}%"
      parameters << "%#{params[:query].parameterize(separator: "_")}%"
      conditions = [conditions.join(" AND "), *parameters]
    end
    if params[:search].present?
      status_array = params[:search].select{|k,v| v=="1"}.keys.map{|a| a.downcase}
    elsif params["no_data"]=="no_data"
      status_array = []
    else
      status_array = ["completed","pending"]
    end
    @files = Report.where(user_id: user_id, status: status_array , read: [false,true]).where(conditions).order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
    render partial: "v2/partner/shared/exported_datatable" if request.xhr?
  end

  def export_download
    if params[:report_id].present?
      report = Report.find_by(id: params[:report_id])
      report.update(read: true) if report.present?
    end
    if params[:id].present?
      files = []
      ids = JSON(params[:id])
      if ids.present?
        if ids.class == Array
          if ids.count == 1
            foo = QcFile.find(ids.first)
            data = open("https:"+foo.image.url)
            send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
          else
            QcFile.where(id: ids).each do|foo|
              files.push([foo.image, "#{foo.id}#{foo.image_file_name}", modification_time: 1.day.ago])
            end
            zipline(files, 'export.zip')
          end
        else
          foo = QcFile.find(params[:id])
          data = open("https:"+foo.image.url)
          send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
        end
      end
    end
  end

end
