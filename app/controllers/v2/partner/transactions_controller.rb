class V2::Partner::TransactionsController < V2::Partner::BaseController
  include ApplicationHelper
  include Merchant::ChecksHelper
  include Merchant::TransactionsHelper
  include FluidPayHelper
  include Zipline

  def index
    if params[:query].present?
      params[:query][:wallet_ids]= params[:query][:wallet_ids].kind_of?(Array)? params[:query][:wallet_ids] : params[:query][:wallet_ids].present? ? JSON.parse(params[:query][:wallet_ids]) : ""
      params[:query]["DBA_name"] = params[:query]["DBA_name"].kind_of?(Array)? params[:query]["DBA_name"] : params[:query]["DBA_name"].present? ? JSON.parse(params[:query]["DBA_name"]) : ""
      params[:query]["type"] =  params[:query]["type"].kind_of?(Array)?  params[:query]["type"] :  params[:query]["type"].present? ? JSON.parse(params[:query]["type"]) : ""
      params[:query]["type1"] =  params[:query]["type1"].kind_of?(Array)?  params[:query]["type1"] : params[:query]["type1"].present? ? JSON.parse(params[:query]["type1"]) : ""
    end
    @filters = [10,25,50,100]
    page_size = params[:page]
    per_page = params[:filter].present? ? params[:filter].to_i : 10
    @heading = "Transactions"
    @wallet = current_user.wallets.primary.first
    @user = current_user
    if params[:query].present?
      @transactions = searching_iso_local_transaction(params[:query], @wallet,nil,nil,nil,nil,nil,nil, nil, params[:type]).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
      # @transactions = searching_local_transaction(params[:query], @wallets).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
      @transactions =  params[:type] == "commission" ? @transactions.where.not(sub_type:["ach","giftcard_fee"]).where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "B2B Fee", "Buy rate fee"]) : @transactions.where.not(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "B2B Fee", "Buy rate fee"])
    else
      params[:type] == I18n.t('types.transaction.commission')
      tnx_type = params[:type]
      @transactions = BlockTransaction.all_transactions(per_page, @wallet.id).transactions_types(page_size, tnx_type, @user).order(timestamp: :desc).per_page_kaminari(page_size).per(per_page)
    end
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    @types = Transaction::ISO_SEARCH_TYPES[:partner_types]
    @types1= Transaction::ISO_SEARCH_TYPES[:partner_types1]
  end

  def show
    @local = BlockTransaction.where(id: params[:id]).first
    @transaction = @local.try(:parent_transaction)
    @transaction = @local if @transaction.blank?
    @wallet_name =  Wallet.find_by_id(@transaction.try(:sender_wallet_id)).try(:name)
    @transaction = Transaction.where("seq_transaction_id = ? or seq_transaction_id = ? " , @local.sequence_id , @local.seq_parent_id).first

  end
end