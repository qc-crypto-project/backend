class V2::Partner::AccountsController < V2::Partner::BaseController
  include V2::Partner::AccountsHelper
  include Merchant::GiftcardsHelper
  include UsersHelper
  include WithdrawHandler
  require 'raas'

	class CheckError < StandardError; end

	def index
		@wallet = current_user.wallets.primary.first
		@achs = TangoOrder.instant_ach.where(wallet_id: current_user.wallets.first.id).limit(10).order(created_at: :desc) if current_user.affiliate_program?
		first_date = Date.today.beginning_of_month
		second_date = Date.today
		@batches = []
		if current_user.iso?
			batches = TransactionBatch.where(iso_wallet_id: @wallet.id).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(created_at: :desc).group_by{|e| e.batch_date}
				batches.each do |key,batch|
					balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:iso_balance).to_f
					gross_earnings = batch.pluck(:iso_commission).try(:compact).try(:sum).to_f
					commission_split = batch.pluck(:iso_split).try(:compact).try(:sum).to_f
					expenses = batch.pluck(:iso_expense).try(:compact).try(:sum).to_f
					net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
					@batches.push({
														id: batch.pluck(:id),
														date: key,
														gross_earnings: gross_earnings.to_f,
														commission_split: commission_split.to_f,
														expenses: expenses.to_f,
														net: net.to_f,
														balance: balance.to_f,
														total_trxs: batch.pluck(:iso_count).try(:compact).try(:sum).to_i
												})
				end
			# @total_net = @batches.pluck(:net).try(:compact).try(:sum).to_f - @batches.last.try(:[],:balance).to_f
			 @total_net = @batches.try(:pluck,:gross_earnings).try(:compact).try(:sum) - @batches.try(:pluck,:commission_split).try(:compact).try(:sum) - @batches.try(:pluck,:expenses).try(:compact).try(:sum)
		elsif current_user.agent?
			batches = TransactionBatch.where(agent_wallet_id: @wallet.id).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
			batches.each do |key,batch|
				balance = batch.select{|e| e.agent_balance != 0}.try(:first).try(:agent_balance).to_f
				gross_earnings = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f
				commission_split = batch.pluck(:agent_split).try(:compact).try(:sum).to_f
				expenses = batch.pluck(:agent_expense).try(:compact).try(:sum).to_f
				# net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				net = gross_earnings.to_f - commission_split.to_f - expenses.to_f
				@batches.push({
													id: batch.pluck(:id),
													date: key,
													gross_earnings: gross_earnings.to_f,
													commission_split: commission_split.to_f,
													expenses: expenses.to_f,
													net: net.to_f,
													balance: balance.to_f,
													total_trxs: batch.pluck(:agent_count).try(:compact).try(:sum).to_i
											})
			end
			# @total_net = @batches.pluck(:net).try(:compact).try(:sum).to_f - @batches.last.try(:[],:balance).to_f
			@total_net = @batches.pluck(:net).last
		elsif current_user.affiliate?
			batches = TransactionBatch.where(affiliate_wallet_id: @wallet.id).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
			batches.each do |key,batch|
				balance = batch.select{|e| e.affiliate_balance != 0}.try(:first).try(:affiliate_balance).to_f
				gross_earnings = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f
				commission_split = batch.pluck(:affiliate_split).try(:compact).try(:sum).to_f
				expenses = batch.pluck(:affiliate_expense).try(:compact).try(:sum).to_f
				net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				@batches.push({
													id: batch.pluck(:id),
													date: key,
													gross_earnings: gross_earnings.to_f,
													commission_split: commission_split.to_f,
													expenses: expenses.to_f,
													net: net.to_f,
													balance: balance.to_f,
													total_trxs: batch.pluck(:affiliate_count).try(:compact).try(:sum).to_i
											})
			end
			@total_net = @batches.pluck(:net).try(:compact).try(:sum).to_f - @batches.last.try(:[],:balance).to_f
		end
	end

  def sales_batch
		first_date = params[:first_date].to_date
		second_date = params[:second_date].to_date
		@wallet  = Wallet.where(id: params[:wallet_id]).last
		@batches = []
		if current_user.iso?
			batches = TransactionBatch.where(iso_wallet_id: params[:wallet_id]).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
			batches.each do |key,batch|
				balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:iso_balance).to_f
				gross_earnings = batch.pluck(:iso_commission).try(:compact).try(:sum).to_f
				commission_split = batch.pluck(:iso_split).try(:compact).try(:sum).to_f
				expenses = batch.pluck(:iso_expense).try(:compact).try(:sum).to_f
				net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				@batches.push({
													id: batch.pluck(:id),
													date: key,
													gross_earnings: gross_earnings.to_f,
													commission_split: commission_split.to_f,
													expenses: expenses.to_f,
													net: net.to_f,
													balance: balance.to_f,
													total_trxs: batch.pluck(:iso_count).try(:compact).try(:sum).to_i
											})
			end
			# @total_net = @batches.pluck(:net).try(:compact).try(:sum).to_f - @batches.last.try(:[],:balance).to_f
			@total_net = @batches.try(:pluck,:gross_earnings).try(:compact).try(:sum) - @batches.try(:pluck,:commission_split).try(:compact).try(:sum) - @batches.try(:pluck,:expenses).try(:compact).try(:sum)
		elsif current_user.agent?
			batches = TransactionBatch.where(agent_wallet_id: params[:wallet_id]).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
			batches.each do |key,batch|
				balance = batch.select{|e| e.agent_balance != 0}.try(:first).try(:agent_balance).to_f
				gross_earnings = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f
				commission_split = batch.pluck(:agent_split).try(:compact).try(:sum).to_f
				expenses = batch.pluck(:agent_expense).try(:compact).try(:sum).to_f
				net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				@batches.push({
													id: batch.pluck(:id),
													date: key,
													gross_earnings: gross_earnings.to_f,
													commission_split: commission_split.to_f,
													expenses: expenses.to_f,
													net: net.to_f,
													balance: balance.to_f,
													total_trxs: batch.pluck(:agent_count).try(:compact).try(:sum).to_i
											})
			end
			@total_net = @batches.pluck(:net).try(:compact).try(:sum).to_f - @batches.last.try(:[],:balance).to_f
		elsif current_user.affiliate?
			batches = TransactionBatch.where(affiliate_wallet_id: params[:wallet_id]).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :desc).group_by{|e| e.batch_date}
			batches.each do |key,batch|
				balance = batch.select{|e| e.affiliate_balance != 0}.try(:first).try(:affiliate_balance).to_f
				gross_earnings = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f
				commission_split = batch.pluck(:affiliate_split).try(:compact).try(:sum).to_f
				expenses = batch.pluck(:affiliate_expense).try(:compact).try(:sum).to_f
				net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				@batches.push({
													id: batch.pluck(:id),
													date: key,
													gross_earnings: gross_earnings.to_f,
													commission_split: commission_split.to_f,
													expenses: expenses.to_f,
													net: net.to_f,
													balance: balance.to_f,
													total_trxs: batch.pluck(:affiliate_count).try(:compact).try(:sum).to_i
											})
			end
			@total_net = @batches.pluck(:net).try(:compact).try(:sum).to_f - @batches.last.try(:[],:balance).to_f
		end
		render partial: 'v2/partner/accounts/sales_batch'
	end

  def batch_transactions
		page_size = params[:page]
		@filters = [10,25,50,100]
		per_page = params[:filter].present? ? params[:filter].to_i : 10
		@heading = "Batch Transactions"
		@time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
		@types = Transaction::ISO_SEARCH_TYPES[:partner_types]
		@types1= Transaction::ISO_SEARCH_TYPES[:partner_types_for_batch]
		date = params[:batch_date].to_date
		@wallet = current_user.wallets.primary.first
		first_date = Time.new(date.year, date.month, date.day, 00,00,00, Time.now.in_time_zone(PSTTIMEZONE).utc_offset).utc
		second_date = Time.new(date.year, date.month, date.day, 23,59,59, Time.now.in_time_zone(PSTTIMEZONE).utc_offset).utc
		params[:query] = params[:query].present? ? params[:query] : {}
		params[:query][:batch_date] = params[:batch_date] if params[:query].try(:[], "date").blank?
		params[:query][:offset] = params[:offset].present? ? params[:offset] : Time.now.in_time_zone(cookies[:timezone]).strftime("%z")
		@transactions = searching_iso_local_transaction_for_batch_export(params[:query], params[:wallet_id],nil,nil,nil,nil,nil,nil, nil, params[:type]).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
		# @transactions = searching_local_transaction(params[:query], @wallets).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
		@transactions =  params[:type] == "commission" ? @transactions.where(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "Buy rate fee"]) : @transactions.where.not(main_type:["Fee Transfer","Monthly Fee","CBK Fee","Misc fee","Subscription fee", "Buy rate fee"])
	end

	def get_ach
    	@achs = TangoOrder.instant_ach.where(wallet_id: current_user.wallets.first.id).limit(10).order(created_at: :desc)
    	render partial: "v2/partner/accounts/ach_body"
  	end

	def get_p2c
	    @p2cs = TangoOrder.instant_pay.includes(:location).where(wallet_id: current_user.wallets.first.id).limit(10).order(created_at: :desc)
	    render partial: "v2/partner/accounts/p2c_body"

	end

  def graph_data
		first_date = params[:start_date].to_date.present? ? params[:start_date].to_date : Date.today.beginning_of_month
		second_date = params[:end_date].to_date.present? ? params[:end_date].to_date : Date.today
		batches = TransactionBatch.where(agent_wallet_id: params[:wallet_id]).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :asc).group_by{|e| e.batch_date} if current_user.iso?
		batches = TransactionBatch.where(affiliate_wallet_id: params[:wallet_id]).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :asc).group_by{|e| e.batch_date} if current_user.agent?
		sale_data = []
		net_data = []
		merchant_data = []
		merchant_attached = []
		net_sum = 0
		net_earning = 0
		transactions_count = 0
		merchant_sum = 0
		wallet=Wallet.where(id:params[:wallet_id]).try(:first)
		user=wallet.try(:users).try(:first)
		baked_batches=BakedUser.where(split_type:'locations_users',active:true,user_id:user.try(:id)).where("updated_at BETWEEN :first AND :second", first: first_date.beginning_of_day, second: second_date.end_of_day)
		location_ids=baked_batches.pluck(:location_id).try(:uniq)
		# locations=Location.where(id:location_ids)
		if baked_batches.present?
			baked_batches=baked_batches.group_by{|e| e.updated_at.to_date}
			baked_batches.each do |key,batch|
				location_id=batch.pluck(:location_id).try(:uniq)
				# merchant_attached=locations.where(id:location_id).pluck(:merchant_id).try(:uniq) if location_id.present?
				merchant_data.push({y: location_id.try(:count), x: key.strftime("%b %d"), count: 0})
			end
		else
			merchant_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
		end
		# merchant_sum=Location.where(id:location_ids).pluck(:merchant_id).try(:uniq) if location_ids.present?
		# merchant_sum=locations.pluck(:merchant_id).try(:uniq) if location_ids.present?
		if batches.present?
			batches.each do |key,batch|
				agent_comminssion = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f if current_user.iso?
				agent_comminssion = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f if current_user.agent?
				net_sum= net_sum.to_f + agent_comminssion.to_f
				tx_count=batch.pluck(:total_trxs).try(:compact).try(:sum)
				transactions_count= transactions_count.to_i + tx_count.to_i
				sale_data.push({y: number_with_precision(agent_comminssion.to_f, precision: 2), x: key.strftime("%b %d"), count: tx_count})

				balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:agent_balance).to_f if current_user.iso?
				balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:affiliate_balance).to_f if current_user.agent?
				gross_earnings = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f if current_user.iso?
				gross_earnings = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f if current_user.agent?

				commission_split = batch.pluck(:agent_split).try(:compact).try(:sum).to_f if current_user.iso?
				commission_split = batch.pluck(:affiliate_split).try(:compact).try(:sum).to_f if current_user.agent?

				expenses = batch.pluck(:agent_expense).try(:compact).try(:sum).to_f if current_user.iso?
				expenses = batch.pluck(:affiliate_expense).try(:compact).try(:sum).to_f if current_user.agent?

				net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				net_earning= net_earning.to_f + net.to_f
				net_data.push({y: number_with_precision(net.to_f, precision: 2), x: key.strftime("%b %d"), count: ''})
			end
		else
			sale_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
			net_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
			# merchant_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
		end
		render json: {sale_data: sale_data,net_data: net_data,
									net_sum: number_with_precision(net_sum.to_f, precision: 2, delimiter: ','),
									transactions_count: transactions_count, net_earning: number_with_precision(net_earning.to_f, precision: 2, delimiter: ','),
									merchant_data: merchant_data,merchant_sum: location_ids.present? ? location_ids.try(:count) : 0

		}
	end


  def merchant_graph_data
		first_date = params[:start_date].to_date.present? ? params[:start_date].to_date : Date.today.beginning_of_month
		second_date = params[:end_date].to_date.present? ? params[:end_date].to_date : Date.today
		batches = TransactionBatch.where(merchant_wallet_id: params[:wallet_id],iso_wallet_id:@user.try(:wallets).try(:first).try(:id)).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :asc).group_by{|e| e.batch_date} if current_user.iso?
		batches = TransactionBatch.where(merchant_wallet_id: params[:wallet_id],agent_wallet_id:@user.try(:wallets).try(:first).try(:id)).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :asc).group_by{|e| e.batch_date} if current_user.agent?
		batches = TransactionBatch.where(merchant_wallet_id: params[:wallet_id],affiliate_wallet_id:@user.try(:wallets).try(:first).try(:id)).sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :asc).group_by{|e| e.batch_date} if current_user.affiliate?
		# .sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date)
		sale_data = []
		commission_data = []
		commission_split_data = []
		net_data = []
		net_sum = 0
		net_earning = 0
		transactions_count = 0
		merchant_sum = 0
		iso_commission_sum = 0
		total_sum = 0
		iso_split_sum = 0
		if batches.present?
			batches.each do |key,batch|
				amount_sum=batch.pluck(:total_amount).try(:compact).try(:sum).to_f
				total_sum= amount_sum.to_f + total_sum.to_f
				tx_count=batch.pluck(:total_trxs).try(:compact).try(:sum)
				transactions_count= transactions_count.to_i + tx_count.to_i
				sale_data.push({y: number_with_precision(amount_sum.to_f, precision: 2), x: key.strftime("%b %d"), count: tx_count})

				iso_comminssion = batch.pluck(:iso_commission).try(:compact).try(:sum).to_f if current_user.iso?
				iso_comminssion = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f if current_user.agent?
				iso_comminssion = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f if current_user.affiliate?
				iso_commission_sum=iso_commission_sum.to_f + iso_comminssion.to_f
				commission_data.push({y: number_with_precision(iso_comminssion.to_f, precision: 2), x: key.strftime("%b %d"), count: tx_count})

				iso_comminssion_split = batch.pluck(:iso_split).try(:compact).try(:sum).to_f if current_user.iso?
				iso_comminssion_split = batch.pluck(:agent_split).try(:compact).try(:sum).to_f if current_user.agent?
				iso_comminssion_split = batch.pluck(:affiliate_split).try(:compact).try(:sum).to_f if current_user.affiliate?
				iso_split_sum=iso_split_sum.to_f + iso_comminssion_split.to_f
				commission_split_data.push({y: number_with_precision(iso_comminssion_split.to_f, precision: 2), x: key.strftime("%b %d"), count: tx_count})

				balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:iso_balance).to_f if current_user.iso?
				balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:agent_balance).to_f if current_user.agent?
				balance = batch.select{|e| e.iso_balance != 0}.try(:first).try(:affiliate_balance).to_f if current_user.affiliate?
				gross_earnings = batch.pluck(:iso_commission).try(:compact).try(:sum).to_f if current_user.iso?
				gross_earnings = batch.pluck(:agent_commission).try(:compact).try(:sum).to_f if current_user.agent?
				gross_earnings = batch.pluck(:affiliate_commission).try(:compact).try(:sum).to_f if current_user.affiliate?

				commission_split = batch.pluck(:iso_split).try(:compact).try(:sum).to_f if current_user.iso?
				commission_split = batch.pluck(:agent_split).try(:compact).try(:sum).to_f if current_user.agent?
				commission_split = batch.pluck(:affiliate_split).try(:compact).try(:sum).to_f if current_user.affiliate?

				expenses = batch.pluck(:iso_expense).try(:compact).try(:sum).to_f if current_user.iso?
				expenses = batch.pluck(:agent_expense).try(:compact).try(:sum).to_f if current_user.agent?
				expenses = batch.pluck(:iso_expense).try(:compact).try(:sum).to_f if current_user.affiliate?
				net = (balance.to_f + gross_earnings.to_f).to_f - commission_split.to_f - expenses.to_f
				net_earning= net_earning.to_f + net.to_f
				net_data.push({y: number_with_precision(net.to_f, precision: 2), x: key.strftime("%b %d"), count: ''})
			end
		else
			sale_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
			commission_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
			commission_split_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
			net_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
		end
		render json: {sale_data: sale_data,net_data: net_data,
									total_sum: number_with_precision(total_sum.to_f, precision: 2, delimiter: ','),
									transactions_count: transactions_count, net_earning: number_with_precision(net_earning.to_f, precision: 2, delimiter: ','),
									commission_data: commission_data,commission_split_data: commission_split_data, iso_split_sum: number_with_precision(iso_split_sum.to_f, precision: 2, delimiter: ','),
									iso_commission_sum: number_with_precision(iso_commission_sum.to_f, precision: 2, delimiter: ',')

		}
	end


	def get_check
	    @checks = TangoOrder.check.includes(:location).where(wallet_id: current_user.wallets.first.id).limit(10).order(created_at: :desc)
	    render partial: "v2/partner/accounts/check_body"

	end

	def merchant_list
		@wallet = Wallet.friendly.find(current_user.wallets.first.slug)
		# merchant_id = TransactionBatch.where(iso_wallet_id: @wallet.id).where.not(merchant_wallet_id: nil).try(:pluck, :merchant_wallet_id) if current_user.iso?
		# @merchant_wallet=Wallet.where(id:merchant_id).order('id DESC').uniq if merchant_id.present?
		@merchant_wallet=current_user.locations.order('id DESC').uniq
		render partial: "v2/partner/accounts/merchant_list"
	end

  def agent_list
		@wallet = Wallet.friendly.find(current_user.wallets.first.slug)
		locations=current_user.locations.order('id DESC').uniq
		@agents=[]
		locations.each do |loc|
			@agents << loc.users.where(role:'agent') if current_user.iso?
			@agents << loc.users.where(role:'affiliate') if current_user.agent?
		end
		@agents=@agents.try(:flatten).try(:uniq)

		# agent_id = TransactionBatch.where(iso_wallet_id: @wallet.id).where.not(merchant_wallet_id: nil).try(:pluck, :agent_wallet_id)
		# @agents=User.agent.joins(:wallets).where('wallets.id IN (?)',agent_id).order('id DESC') if agent_id.present?
		render partial: "v2/partner/accounts/agent_list"
	end

	def get_giftcard
	    begin
	      client = TangoClient.instance
	      # orders = client.orders
	      customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
	      account_identifier = ENV["ACCOUNT_IDENTIFIER"]
	      collect = Hash.new
	      collect['account_identifier'] = account_identifier
	      collect['customer_identifier'] = customer_identifier
	      elements_per_block = 100
	      collect['elements_per_block'] = elements_per_block
	      page = 0
	      collect['page'] = page

	      orders = client.orders
	      tangoIntialOrders = @user.tango_orders.order(created_at: :desc).select{|v| v.utid != nil}
	      tangoIntialOrders = User.find(@user.merchant_id).tango_orders.order(created_at: :desc).select{|v| v.utid != nil} if @user.merchant_id.present? && @user.try(:permission).admin?
	      @giftcards = tangoIntialOrders.map do |order|
	        object = orders.get_order(order.order_id)
	        object.to_hash.merge(:catalog_image => order.catalog_image)
	      end
	      render partial: "v2/partner/accounts/giftcard_body"
	    rescue Exception => exc
	      p "Tango Exception Handled: ", exc
	      message = exc.message ? exc.message : 'Something went wrong!'
	      if exc.class != Raas::RaasGenericException && exc.try(:errors).present? && exc.try(:errors).try(:count).to_i > 0
	        message = exc.errors.first.message
	      end
	      flash[:danger] = message
	    end
	end

  def get_withdraw_data

		if params[:wallet_id].present?
			if @user.admin_user_id.present? && @user.affiliate_program?
		      admin_user = User.find(@user.admin_user_id)
		      @user.system_fee = admin_user.system_fee
		    end
			if @user.system_fee.present?
				date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
				if @user.system_fee.try(:[],'ach_limit_type') == "Day"
					instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:wallet_id],date,"VOID").instant_ach
					ach_total_count = instant_achs.sum(:actual_amount)
				end
				if @user.system_fee.try(:[],'push_to_card_limit_type') == "Day"
					instant_p2c = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:wallet_id],date,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil)
					p2c_total_count = instant_p2c.sum(:actual_amount)
				end
				if @user.system_fee.try(:[],'check_limit_type') == "Day"
					instant_check = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=? and tango_orders.order_type IN (?)",params[:wallet_id],date,"VOID",[0,5])
					check_total_count = instant_check.sum(:actual_amount)
				end
			end
		end
		render json: {"ach_count" => ach_total_count.to_i,"p2c_count"=>p2c_total_count.to_f, "check_count"=>check_total_count.to_i}
	end

  def create_ach
		begin
			raise I18n.t("errors.withdrawl.amount_must_greater_than_zero") if params[:amount].to_f < 1
			wallet = Wallet.where(id: params[:wallet_id]).last
			raise I18n.t("errors.withdrawl.account_not_exist") if wallet.blank?
			raise  I18n.t('errors.withdrawl.blocked_ach') if current_user.block_ach?

			amount = params[:amount].to_f
			if current_user.affiliate_program? && current_user.admin_user_id.present?
				admin_user = User.find(@user.admin_user_id)
				user_system_fee = current_user['system_fee']
     	 		system_fee = admin_user.present? && admin_user.try(:system_fee) != "{}" ? admin_user.system_fee : {}
	      		system_fee["bank_account"] =  user_system_fee["bank_account"]
	      		system_fee["bank_routing"] =  user_system_fee["bank_routing"]
	      		system_fee["bank_account_name"] =  user_system_fee["bank_account_name"]
				system_fee["bank_account_type"] =  user_system_fee["bank_account_type"]

			else
				system_fee = current_user['system_fee']
			end

			bank = system_fee['bank_account_name'] if system_fee.present?
			raise "No Bank Found." if bank.blank?

			bank_name = system_fee['bank_account_name']
			recipient = current_user.email
			account_number = AESCrypt.decrypt("#{current_user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", system_fee['bank_account'])
			routing_number = system_fee['bank_routing']
			bank_account_type = system_fee["bank_account_type"]
			description = params[:ach_memo]
			ach_afp = current_user.affiliate_program? ? true : nil
			send_via = "ACH"

			#= calculate fee start
			fee_object = system_fee if system_fee.present? && system_fee != "{}"
			if fee_object.present?
				send_check_fee = fee_object["ach_dollar"].present? ? fee_object["ach_dollar"].to_f : 0
				fee_perc_fee = fee_object["ach_percent"].present? ? deduct_fee(fee_object["ach_percent"].to_f, amount) : 0
			else
				send_check_fee = 0
				fee_perc_fee = 0
			end

			fee = send_check_fee.to_f + fee_perc_fee.to_f
			amount_sum = amount.to_f + fee.to_f
			balance = show_balance(params[:wallet_id])
			raise I18n.t("merchant.controller.insufficient_balance") if balance.to_f < amount_sum.to_f
			raise CheckError.new("Error Code 5028 Ach creation failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0)
			# raise "Error Code 7054 Ach creation failed, you must keep min $#{system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again." if balance.to_f - amount_sum.to_f < system_fee.try(:[],'check_withdraw_limit').to_f

			card_info = {
					:account_number => account_number,
					:account_type => bank_account_type
			}
			card_info = card_info.to_json
			account_number = account_number.last(4)
			encrypted_account_info = AESCrypt.encrypt("#{current_user.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{wallet.id}", card_info)
			rand = "#{rand(100..999)}#{Time.now.to_i.to_s}"
			ach_international = system_fee["ach_international"]
			ach_international = false if ach_international.nil?
			check = TangoOrder.new(user_id: current_user.id,
														 name: bank_name,
														 status: "PENDING",
														 amount: amount_sum,
														 actual_amount: amount,
														 check_fee: send_check_fee,
														 fee_perc: fee_perc_fee,
														 recipient: recipient,
														 checkId: rand,
														 description: description,
														 order_type: :instant_ach,
														 batch_date:Time.zone.now + 1.day,
														 approved: false,
														 account_type: bank_account_type,
														 wallet_id: wallet.id,
														 settled: :false,
														 account_token: encrypted_account_info,
														 send_via: send_via,
														 account_number: account_number,
														 routing_number: routing_number,
														 amount_escrow: true,
														 ach_international: ach_international,
														 ach_afp: ach_afp)
			ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'fifty', type: "ach"
			escrow_wallet = Wallet.check_escrow.first
			escrow_user = escrow_wallet.try(:users).try(:first)
			sender_dba=get_business_name(wallet)
			transaction = current_user.transactions.create(
					to: recipient,
					status: "pending",
					amount: amount,
					sender_id: current_user.id,
					sender_name: sender_dba,
					sender_wallet_id: wallet.id,
					receiver_id: escrow_user.try(:id),
					receiver_wallet_id: escrow_wallet.try(:id),
					receiver_name: escrow_user.try(:name),
					sender_balance: balance,
					receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
					action: 'transfer',
					fee: fee.to_f,
					net_amount: amount.to_f,
					net_fee: fee.to_f,
					total_amount: amount_sum.to_f,
					ip: get_ip,
					main_type: "ACH",
			)
			user_info = fee.try(:[], "splits") || {}
			withdraw = withdraw(amount,wallet.id, fee || 0, TypesEnumLib::GatewayType::ACH,account_number,send_via,user_info, nil, nil, nil,TypesEnumLib::GatewayType::Quickcard,nil,true)
			ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'finalize', type: "ach"
			if withdraw.present? && withdraw["actions"].present?
				parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
				save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
				check.transaction_id = transaction.id
				check.save
				transaction.update(seq_transaction_id: withdraw["actions"].first["id"],
													 status: "approved",
													 privacy_fee: 0,
													 tags: withdraw["actions"].first.tags,
													 sub_type: withdraw["actions"].first.tags["sub_type"],
													 timestamp: withdraw["timestamp"]
				)
				ActivityLog.log(nil,"ACH Created",current_user,params,response,"ACH Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","ach")
				# ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: [check.id], type: "ach"
				flash[:success] = "Successfully sent a ACH to #{ recipient }"
			else
				# raise I18n.t('merchant.controller.unaval_feature')
				flash[:error] = I18n.t 'merchant.controller.unaval_feature'
			end


		rescue => exc
			flash[:error] = exc.message
			# raise exc.message
		ensure
			if flash[:error].blank? && flash[:success].blank?
				flash[:error] = I18n.t 'merchant.controller.except'
			end
			if flash[:error].blank?
				ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: [check.id], type: "ach"
			else
				if flash[:error].include?('Error Code')
					str = flash[:error]
					message = str[16, str.length-1]
					code = str[0,15]
				else
					message = flash[:error]
					code = " Error Code #"
				end
				ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: code, message: message, type: "ach"
			end
		end
		flash.discard
	end

  def create_check
		amount = params[:amount].to_f
		recipient = params[:email]
		name = params[:name]
		description = params[:memo]
		send_via = 'email'
		send_check_user_info = { name: name, check_email: recipient }
		begin
			wallet = Wallet.where(id: params[:wallet_id]).last
			raise I18n.t('errors.withdrawl.wallet_not_exist') if wallet.blank?

			if @user.admin_user_id.present? && @user.affiliate_program?
      			admin_user = User.find(@user.admin_user_id)
      			fee_object = admin_user.system_fee
    		else
    			fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
    		end
			raise I18n.t('errors.withdrawl.blocked_check') if @user.block_withdrawal
			raise I18n.t('errors.withdrawl.amount_must_greater_than_zero') if params[:amount].to_f < 1
			if fee_object.present?
				send_check_fee = fee_object["send_check"].present? ? fee_object["send_check"].to_f : 0
				fee_perc_fee = fee_object["redeem_fee"].present? ? deduct_fee(fee_object["redeem_fee"].to_f, amount) : 0
			else
				send_check_fee = 0
				fee_perc_fee = 0
			end
			fee = send_check_fee + fee_perc_fee
			amount_sum = amount + fee
			balance = show_balance(wallet.id, @user.ledger).to_f
			raise I18n.t('merchant.controller.insufficient_balance')  if balance.to_f < amount_sum.to_f
			raise "Error Code 6003 Send Check failed, you must keep min $#{fee_object.try(:[],'check_withdraw_limit').to_f} available in your account. Please try again." if balance.to_f - amount_sum.to_f < fee_object.try(:[],'check_withdraw_limit').to_f
			check = TangoOrder.new(user_id: @user.id,
														 name: name,
														 status: "PENDING",
														 amount: amount_sum,
														 actual_amount: amount,
														 check_fee: send_check_fee,
														 fee_perc: fee_perc_fee,
														 recipient: recipient,
														 description: description,
														 order_type: "check",
														 approved: false,
														 settled: false,
														 wallet_id: wallet.id,
														 send_via: send_via,
														 batch_date: Time.now.to_datetime + 1.day,
														 amount_escrow: true)
			tags = {
					"send_check_user_info" => send_check_user_info,
					"send_via" => send_via
			}
			ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'fifty', type: "check"
			sender_dba=get_business_name(wallet)
			main_type = updated_type(TypesEnumLib::TransactionType::SendCheck)
			escrow_wallet = Wallet.check_escrow.first
			escrow_user = escrow_wallet.try(:users).try(:first)
			transaction = @user.transactions.create(
					to: recipient,
					from: nil,
					status: "pending",
					amount: amount,
					sender: current_user,
					sender_wallet_id: wallet.id,
					sender_name: sender_dba,
					receiver_id: escrow_user.try(:id),
					receiver_wallet_id: escrow_wallet.try(:id),
					receiver_name: escrow_user.try(:name),
					sender_balance: balance,
					receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
					action: 'transfer',
					fee: fee.to_f,
					net_amount: amount_sum.to_f - fee.to_f,
					net_fee: fee.to_f,
					total_amount: amount_sum.to_f,
					ip: get_ip,
					main_type: main_type,
					tags: tags
			)

			if fee > 0
				withdraw = custom_withdraw(amount,wallet.id, fee, TypesEnumLib::TransactionType::SendCheck,nil,send_via,nil, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,true)
			else
				withdraw = withdraw(amount,wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,nil, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
			end
			ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'finalize', type: "check"
			if withdraw.present? && withdraw["actions"].present?
				transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
				check.transaction_id = transaction.id
				#= creating block transaction
				parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
				save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
				if check.save
					@success = true
					ActivityLog.log(nil,"Check Created",current_user,params,response,"Check Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","checks")
					ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: [check.id], type: "check"
				else
					raise I18n.t('merchant.controller.chck_failed')
				end
			else
				raise message: I18n.t('merchant.controller.unaval_feature')
			end
		rescue => ex
			@error = ex.message
			if @error.include?('Error Code')
				str = @error
				message = str[16, str.length-1]
				code = str[0,15]
			else
				message = @error
				code = " Error Code #"
			end
			msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`"
			handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
			ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: code,message: message, type: "check"
		ensure
			if @error.blank? && @success.blank?
				msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\`"
				handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
				ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.unexpected_error.title'), message: I18n.t('error_codes.account.unexpected_error.description'), type: "check"
			end
		end
	end

  def create_p2c
		begin
			amount = params[:amount].to_f
			raise I18n.t('errors.withdrawl.amount_must_greater_than_zero') if amount.to_f < 1
			name = params[:name]
			raise CheckError.new("Sorry, Name should be present!") if name.blank?
			raise CheckError.new("Sorry, Name should have atleast 2 characters!") if name.try(:strip).try(:length) < 2
			recipient = params[:email]
			description = params[:p2c_memo]
			if params[:cardNoSafe].present?
				card_no = params[:cardNoSafe].gsub(' ', '')
			else
				card_no = params[:card_number]
			end

			card_bin_list = get_card_info(card_no)
			if card_no.present?
				reg = /\D/
				raise I18n.t('api.registrations.invalid_card_data') if card_no.match(reg).present?
			end
			if card_bin_list.present? && card_bin_list.try(:[],'type').present? && card_bin_list.try(:[],'type')=="credit"
				raise I18n.t('merchant.controller.debit_card_only')
			end

			## Card Expiry :-:
			if params[:expiry_date].present?
				exp_month = params[:expiry_date].split('/').first
				exp_year = params[:expiry_date].split('/').last
				current_year = Time.current.year - 2000
				if exp_year.to_i < current_year || exp_month.to_i > 12 || exp_month.to_i < 0
					raise I18n.t('api.registrations.invalid_card_data')
				end
			end
			if exp_year.present? && exp_year.to_s.size == 4
				exp_year = exp_year.to_i - 2000
			end
			exp_date = "20#{exp_year}-#{exp_month}"
			##
			zip = params[:zip_code]
			send_via = 'instant_pay'

			raise I18n.t('errors.withdrawl.blocked_p2c')if current_user.push_to_card
			wallet = Wallet.where(id: params[:wallet_id]).try(:first)

			balance = show_balance(wallet.id, current_user.ledger).to_f
			last_4 = card_no.last(4) if card_no.present?
			first_6 = card_no.first(6) if card_no.present?
			if (params[:card_id].present? && params[:other_card].blank?) || (params[:card_id].present? && params[:other_card].present? && params[:card_number].blank?) #= using exiting card
				card = Card.find_by(id: params[:card_id])
				if card.present? && card.card_type=="debit"
					card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, current_user.id)
					last_4 = card.last4
					first_6 = card.first6
					raise I18n.t('api.registrations.invalid_card_data') if card_info.number.tr("0-9","").present?
					card_detail = {
							number: card_info.number,
							month: card_info.month,
							year: card_info.year,
					}
				else
					raise I18n.t('merchant.controller.debit_card_only')
				end
			else
				if params[:save_card].present? &&  params[:save_card] == "on"
					raise I18n.t('api.registrations.invalid_card_data') if card_no.tr("0-9","").present?
					card_detail = {
							number: card_no,
							month: exp_month,
							year: "20#{exp_year}",
					}
					card_bin_list = get_card_info(card_no)
					card1 = Payment::QcCard.new(card_detail, nil, current_user.id)
					cards = current_user.cards.instant_pay_cards.where(fingerprint: card1.fingerprint,merchant_id: current_user.id)
					if cards.blank?
						card = current_user.cards.create(qc_token:card1.qc_token,exp_date: "#{card_detail[:month]}/#{card_detail[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand, last4: last_4, name: current_user.name, card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: first_6,fingerprint: card1.fingerprint,merchant_id: current_user.id,instant_pay: true)
					else
						card = cards.first
						card.exp_date = params[:expiry_date] if params[:expiry_date].present?
					end
				end
			end

			if @user.admin_user_id.present? && @user.affiliate_program?
		      admin_user = User.find(@user.admin_user_id)
		        fee_object = admin_user.system_fee
		    else
				fee_object = current_user.system_fee
			end

			if fee_object.try(:[],"push_to_card_limit").to_f != 0 && fee_object.try(:[],"push_to_card_limit_type") == "Day"
				todays_orders = TangoOrder.orders_for_today(wallet.id, current_user.id, "instant_pay")
				todays_transactions_amount = todays_orders.pluck(:actual_amount).sum(&:to_f)
				todays_void_amount = todays_orders.VOID.pluck(:actual_amount).sum(&:to_f)
				todays_failed_amount = todays_orders.FAILED.pluck(:actual_amount).sum(&:to_f)
				valid_amount = todays_transactions_amount - todays_void_amount - todays_failed_amount + amount
				raise I18n.t('errors.withdrawl.cant_proceed_daily_limit', location_limit: number_with_precision(location.push_to_card_limit, :precision => 2, delimiter: ','))  if valid_amount.to_f > fee_object.try(:[],"push_to_card_limit").to_f
			elsif fee_object.try(:[],"push_to_card_limit").to_f != 0 && amount.to_f > fee_object.try(:[],"push_to_card_limit").to_f
				raise I18n.t('errors.withdrawl.cant_proceed_daily_limit', location_limit: number_with_precision(location.push_to_card_limit, :precision => 2, delimiter: ','))
			end

			if card_detail.present?
				account_token_info =  {
						:card_number => card_detail[:number],
						:expiry_date => "#{card_detail[:year]}-#{card_detail[:month]}"
				}
			else
				account_token_info =  {
						:card_number => card_no,
						:expiry_date => exp_date
				}
			end

			account_token_info = account_token_info.to_json
			encrypted_card_info = AESCrypt.encrypt("#{current_user.id}-#{ENV['CARD_ENCRYPTER']}-#{wallet.id}}", account_token_info)

			if user_signed_in? && wallet.present?
				if fee_object.present?
					db_check_fee = fee_object["push_to_card_dollar"].present? ? fee_object["push_to_card_dollar"].to_f : 0
					db_fee_perc = fee_object["push_to_card_percent"].present? ? deduct_fee(fee_object["push_to_card_percent"].to_f, amount) : 0
				else
					db_check_fee = 0
					db_fee_perc = 0
				end
				total_fee = db_fee_perc + db_check_fee
				amount_sum = amount + total_fee

				raise  I18n.t('merchant.controller.insufficient_balance') if balance.to_f < amount_sum.to_f

				raise "Error Code 5054 P2C creation failed, you must keep min $#{fee_object.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again." if balance.to_f - amount_sum.to_f < fee_object.try(:[],'check_withdraw_limit').to_f
				ach_afp = current_user.affiliate_program? ? true : nil
				check_escrow = Wallet.check_escrow.first
				check = TangoOrder.new(user_id: current_user.id,
															 name: name,
															 status: "PENDING",
															 amount: amount_sum,
															 actual_amount: amount,
															 check_fee: db_check_fee,
															 fee_perc: db_fee_perc,
															 recipient: recipient,
															 description: description,
															 order_type: "instant_pay",
															 approved: false,
															 account_token: encrypted_card_info,
															 settled: false,
															 wallet_id: wallet.id,
															 send_via: send_via,
															 last4: last_4,
															 first6: first_6,
															 batch_date:Time.zone.now + 1.day,
															 zip_code: zip,
															 amount_escrow: true,
															 ach_afp: ach_afp)


				send_check_user_info = { name: name, check_email: recipient,amount: amount,description: description,fee: total_fee }
				tags = {
						"send_check_user_info" => { name: name, check_email: recipient,amount: amount,description: description,fee: total_fee },
						"send_via" => send_via,
						"card" => card
				}

				total_fee = db_fee_perc + db_check_fee
				ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'fifty', type: "p2c"
				sender_dba=get_business_name(wallet)
				transaction = current_user.transactions.build(
						to: recipient,
						from: nil,
						status: "pending",
						charge_id: nil,
						amount: amount,
						sender: current_user,
						sender_name: sender_dba,
						sender_wallet_id: wallet.id,
						receiver_wallet_id: check_escrow.id,
						receiver_name: check_escrow.try(:users).try(:first).try(:name),
						receiver_id: check_escrow.try(:users).try(:first).try(:id),
						sender_balance: balance,
						receiver_balance: SequenceLib.balance(check_escrow.id),
						action: 'transfer',
						fee: total_fee,
						net_amount: amount,
						net_fee: total_fee,
						total_amount: amount_sum.to_f,
						ip: get_ip,
						main_type: "instant_pay",
						tags: tags,
						last4: last_4,
						first6: first_6
				)

				withdraw = withdraw(amount,wallet.id, total_fee || 0, TypesEnumLib::TransactionType::DebitCardDeposit,last_4,send_via,nil, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true, card)
				ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'finalize', type: "p2c"
				if withdraw.present? && withdraw["actions"].present?
					transaction.seq_transaction_id = withdraw["actions"].first["id"]
					transaction.status = "approved"
					transaction.privacy_fee = 0
					transaction.sub_type = withdraw["actions"].first.tags["sub_type"]
					transaction.timestamp = withdraw["timestamp"]
					transaction.save
					check.transaction_id = transaction.id
					check.save
					#= creating block transaction
					parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
					save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
					ActivityLog.log(nil,"Push to Card Created",current_user,params,response,"Push To Card Created [##{check.id}] for [$#{number_with_precision(amount_sum, precision: 2)}]","push_to_card")
					ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: check.id, type: "p2c"
				else
					error = I18n.t('merchant.controller.unaval_feature')
					raise error
				end
			end
		rescue => ex
			message = ex.message
			if message.include?('Error Code')
				str = message
				message = str[16, str.length-1]
				code = str[0,15]
			else
				message = message
				code = " Error Code #"
			end
			ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: code, message: message, type: "p2c"
			# SlackService.handle_exception "Debit Card Deposit failed", ex
			msg = "*Push To Card Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`"
			handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
		end
	end

	def present_card_verification
		if params[:card_number].blank?
			render json:{"success":"not_available"}, status: 200
		else
			count = params[:card_number].length
			params[:card_number] = params[:card_number].gsub(' ', '')
			if params[:present].present? && params[:present] == "present"
				card=Card.find_by_id(params[:card_number])
			else
				card=Card.where(first6:params[:card_number].first(6).to_i,last4: params[:card_number].last(4)).last
			end
			card_type=card.try(:card_type)
			card_scheme = card.try(:brand)
			if card_type.blank? || card_type=='credit'
				card_info = get_card_info(params[:card_number])
				card_type = card_info['type']
				card_scheme = card_info['scheme']
				card.update(card_type: card_type) if card.present?
			end
			if card_type == 'debit'
				render json:{"success": card_scheme, count: count}, status: 200
			elsif card_type == 'credit'
				render json:{"success":"false", count: count} , status: 200
			else
				render json:{"success":"false", count: count}, status: 200
			end
		end
	end

	def buy_giftcard
		client = TangoClient.instance
		catalog = client.catalog
		@result = catalog.get_catalog()
		@giftcards = get_all_giftcards(@result)

		render partial: "v2/partner/accounts/buy_giftcard"
	end

	def giftcard_show
		@list=[]
		client = TangoClient.instance
		catalog = client.catalog
		@giftcards = catalog.get_catalog()
		@fee = params[:fee] if params.present?
		@wallet = @user.wallets.primary.first
		@giftcard={}
		@giftcards.brands.each do |b|
			br=b.items.select{|v|v.utid== params[:id]}
			if !br.empty?
				@giftcard = {gift_card: br.first,brand_name:b.brand_name,description:b.description,disclaimer:b.disclaimer, image_url: b.image_urls.map{|s| s.second}.third }
			end
		end
		render partial: "v2/partner/accounts/gift_card_buy"
	end

	def buy_card
		fee = 0
		unless params[:email].present? &&  params[:image_url].present? && params[:wallet_id].present? &&  params[:utid].present? && params[:amount].present?
			ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_missing_parameters.title') , message: I18n.t('error_codes.account.gift_card_missing_parameters.description')
		end
		@user= current_user
		# @user= User.find(current_user.merchant_id) if current_user.merchant_id.present? && current_user.try(:permission).admin?
		return ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_block.title'), message: I18n.t('error_codes.account.gift_card_block.description') if @user.block_giftcard
		begin
			validate_parameters({wallet_id: params[:wallet_id], utid:  params[:utid], amount: params[:amount] })
			balance = show_balance(params[:wallet_id])
			client = TangoClient.instance
			if params[:amount].present? && params[:utid].present?
				fee_lib = FeeLib.new
				w = Wallet.find(params[:wallet_id])
				amount = params[:amount].to_f
				actual_amount = params[:amount].to_f
				if @user.ISO? || @user.AGENT? || @user.AFFILIATE? || @user.affiliate_program?
					data = deduct_giftcard_fee(@user,amount)
					amount = data[0]
					fee = data[1]
				else
					location_fee = fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
					amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
					fee = location_fee
				end
				if balance.to_i < amount.to_i
					ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_insufficient_funds.title') ,message: I18n.t('error_codes.account.gift_card_insufficient_funds.description')
				else
					account = client.accounts
					account_info = account.get_account('quickcard')
					if account_info.present? && account_info.current_balance.present?
						tango_balance = account_info.current_balance
					end
					if tango_balance.present? && tango_balance < amount.to_f
						ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_not_process.title') , message: I18n.t('error_codes.account.gift_card_not_process.description')
					else
						orders = client.orders
						customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
						account_identifier = ENV["ACCOUNT_IDENTIFIER"]

						body = Raas::CreateOrderRequestModel.new
						body.account_identifier = account_identifier
						body.amount = params[:amount].to_f || 0
						body.customer_identifier = customer_identifier
						body.send_email = true
						body.sender = {
								"email": 'admin@quickcard.me',
								"firstName": "QuickCard",
								"lastName": ""
						}
						body.recipient = {
								"email": params[:email],
								"firstName": @user.name || 'No Name',
								"lastName": ""
						}
						body.utid = params[:utid]
						result = orders.create_order(body)
						if result
							p "TANGO ORDER: ", result
							tango_order =  @user.tango_orders.create!(
									:utid => params[:utid],
									:account_identifier => account_identifier,
									:amount => body.amount,
									:name => result.reward_name,
									:recipient => params[:email],
									:order_id => result.reference_order_id,
									:catalog_image => params[:image_url] || nil,
									:wallet_id => params[:wallet_id],
									:gift_card_fee => fee
							)
							ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'finalize'
								utid = params[:utid].present? ? params[:utid] : ''
								recipient_email = params[:email].present? ? params[:email] : ''
								brand_name = result.reward_name.present? ? result.reward_name : ''
								user_info = {utid: utid, recipient_email: recipient_email, brand_name:brand_name}
								transaction = @user.transactions.create(
										to: nil,
										from: params[:wallet_id].to_s,
										status: "pending",
										amount: actual_amount,
										sender: @user,
										sender_name: @user.try(:name),
										sender_wallet_id: w.id,
										sender_balance: balance,
										action: 'retire',
										fee: fee.to_f,
										net_amount: actual_amount.to_f,
										net_fee: fee.to_f,
										gbox_fee: fee.to_f,
										total_amount: amount.to_f,
										ip: get_ip,
										main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
										tags: user_info
								)
								transact = withdraw(amount,params[:wallet_id], fee, TypesEnumLib::TransactionType::GiftCard, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango,'other')
							if transact.present?
								tango_order.update(transaction_id: transaction.id)
								transaction.update(seq_transaction_id: transact.actions.first.id, tags: transact.actions.first.tags, status: "approved", timestamp: transact.timestamp)
								#= creating block transaction
								parsed_transactions = parse_block_transactions(transact.actions, transact.timestamp)
								if parsed_transactions.last[:seq_parent_id].nil? && parsed_transactions.last[:type] == "Giftcard Purchase"
									parsed_transactions.last[:seq_parent_id] = transact.id
								end
								save_block_trans(parsed_transactions) if parsed_transactions.present?
							end
							ActivityLog.log(nil,"Giftcard Created",current_user,params,response,"Giftcard Created [##{tango_order.id}] for [$#{number_with_precision(amount.to_f, precision: 2)}]","giftcard")
							ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'done'

						end
					end
				end
			else
				ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_missing_parameters.title') , message: I18n.t('error_codes.account.gift_card_missing_parameters.description')
			end
		rescue Exception => exc
			message = exc.message ? exc.message : 'Something went wrong!'
			if exc.class != Raas::RaasGenericException && exc.message.present?
				message = exc.message
				if message.include?('Error Code')
					str = message
					message = str[16, str.length-1]
					code = str[0,15]
				else
					message = message
					code = " Error Code #"
				end
			end
			ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: code, message: message
		end
	end

	def delete_p2c_card
		card = Card.find_by_id(params[:id])
		if card.present?
			card.delete
			result = true
		else
			result = false
		end
		render json: {result: result}
	end


end
