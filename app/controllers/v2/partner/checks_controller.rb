class V2::Partner::ChecksController < V2::Partner::BaseController

  before_action :update_system_fee , only: [:new,:create]
  include WithdrawHandler
  class CheckError < StandardError; end

  def index

    @q = TangoOrder.ransack(params[:q])

    if params[:custom].present?
        params[:first_date] = DateTime.parse(params[:first_date])
        params[:second_date] = DateTime.parse(params[:second_date])
    end
      
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @heading = "Checks"
    @check = Check.new
    @checkIssueEnabled = false
    checkConfig = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @checkIssueEnabled = checkConfig.boolValue unless checkConfig.blank?
    @total_paid = @total_void = @total_pending = @total_failed = @total_unpaid  = @total_paid_wire = number_with_precision(0, :precision => 2, :delimiter => ',')
    params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )
    if params[:q].present?
      params[:q][:data_search] = params[:q][:name_cont] if params[:q][:data_search].blank? && params[:q][:name_cont].present?
      params[:q]["name_or_recipient_or_wallet_name_cont"] = params[:q]["data_search"] 
      params[:q]["total_fee_eq"] = params[:q][:data_search] if !params[:q][:data_search].is_a? String
      params[:q]["id_eq"] = params[:q]["data_search"] if params[:q].present?
      params[:q]["amount_eq"] = params[:q]["data_search"] if params[:q].present?
      params[:q]["actual_amount_eq"] = params[:q]["data_search"] if params[:q].present?
    end
	  user_ids=current_user.id
	  if params[:wallet_id].present?
	    @total_checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids, wallet_id: params[:wallet_id])
	  else
	    @total_checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids)
	  end
	  @total_checks = @total_checks.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
	  @total_checks = @total_checks.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
	  @checks = @total_checks.order(id: :desc)
	  if @user.oauth_apps.present?
	    oauth_app = @user.oauth_apps.first
	    @checkIssueEnabled = false if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
	  end
    @checks = @checks.per_page_kaminari(params[:page]).per(page_size)
    @wallets = []

    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE? || @user.affiliate_program?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    elsif !@user.merchant_id.nil?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.primary.each do |w|
          @wallets << w
        end
      end
    end

    if @total_checks.present? 
        @total_paid = @total_checks.PAID.sum(:amount)
        @total_paid =  number_with_precision(@total_paid, precision: 2, delimiter: ',') 
        @total_pending = @total_checks.PENDING.sum(:amount)
        @total_pending =  number_with_precision(@total_pending, precision: 2, delimiter: ',') 
        @total_void = @total_checks.VOID.sum(:amount)
        @total_void =  number_with_precision(@total_void, precision: 2, delimiter: ',') 
        @total_failed = @total_checks.FAILED.sum(:amount)
        @total_failed =  number_with_precision(@total_failed, precision: 2, delimiter: ',') 
        @total_unpaid = @total_checks.UNPAID.sum(:amount)
        @total_unpaid =  number_with_precision(@total_unpaid, precision: 2, delimiter: ',') 
        @total_paid_wire = @total_checks.PAID_WIRE.sum(:amount)
        @total_paid_wire =  number_with_precision(@total_paid_wire, precision: 2, delimiter: ',') 
    end
    render partial: 'v2/partner/checks/checks_datatable' if request.xhr?
  end

  def new
    flash = []
    @check =Check.new
    @wallets = @user.wallets.primary.first
    if @user.system_fee.try(:[],'check_limit_type') == "Day"
      date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
      instant_check = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=? and tango_orders.order_type IN (?)",@wallets.id,date,"VOID",[0,5])
      # @check_limit = @user.system_fee.try(:[],'check_limit').to_f
      @total_count = instant_check.sum(:actual_amount)
      # @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
    end
    render partial: "new"
  end


  def show
    @check= TangoOrder.find params[:id]
    phone_number =  @check.try(:bulk_check).try(:phone)

    account = ''
    wallet = Wallet.find_by(id: @check.wallet_id) 
    account = wallet.try(:name) if wallet.present?
    amount = number_with_precision(@check.actual_amount, precision: 2, delimiter: ',')
    fee = number_with_precision(@check.check_fee.to_f + @check.fee_perc.to_f, precision: 2, delimiter: ',')
    total_amount = number_with_precision(@check.amount, precision: 2, delimiter: ',')
    render json: {"account" => account,"recipient"=>@check.try(:recipient),"memo"=>@check.description,
      "number" => @check.try(:number),"status"=>@check.try(:status),"amount"=>amount,"fee"=>fee,"total_amount"=>total_amount,"phone_number"=> phone_number
    }
  end

  def location_data
      if params[:location_id].present? && params[:location_id].to_i != 0
        @balance = show_balance(params[:location_id])
          @fee_dollar = @user.system_fee.try(:[],'send_check')
          @fee_perc = @user.system_fee.try(:[],'redeem_fee')
          if @user.system_fee.try(:[],'check_limit_type') == "Transaction"
            @check_limit = @user.system_fee.try(:[],'check_limit').to_f
            @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
          elsif @user.system_fee.try(:[],'check_limit_type') == "Day"
            date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
            instant_check = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=? and tango_orders.order_type IN (?)",params[:location_id],date,"VOID",[0,5])
            @check_limit = @user.system_fee.try(:[],'check_limit').to_f
            @total_count = instant_check.sum(:actual_amount)
            @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
          end
      else
        @balance = 0
      end
      fee_dollar = @fee_dollar
      fee_perc = @fee_perc 
      location_block = false
      location_block_withdraw = false
      system_tran_type = false
      system_day_type = false
      @balance =  number_with_precision(@balance, precision: 2, delimiter: ',') 
      check_limit =  @check_limit
      check_limit_type = @check_limit_type 
      if @user.system_fee.try(:[],'check_limit_type').present? && @user.system_fee.try(:[],'check_limit_type') == "Transaction" 
        system_tran_type = true
      elsif @user.system_fee.try(:[],'check_limit_type').present? && @user.system_fee.try(:[],'check_limit_type') == "Day"
        system_day_type = true
      end     
      render json: {"balance" => @balance,"fee_dollar1" => @fee_dollar , "fee_perc1" => @fee_perc,
        "location" => @location , "location_block" => location_block , "location_block_withdraw" => location_block_withdraw ,
        "check_limit" => check_limit , "check_limit_type" => check_limit_type , "total_count" => @total_count , "system_day_type" => system_day_type,
        "system_tran_type" => system_tran_type
      }
  end
  def create
    begin
    amount = params[:check][:amount].to_f
    recipient = params[:check][:recipient]
    name = params[:check][:name]
    sender = params[:check][:sender]
    description = params[:check][:description]
    send_via = 'email'
    send_check_user_info = { name: name, check_email: recipient }
    begin
      raise "Error Code 4001 Amount Must be greater then 0." if params[:check][:amount].to_f <= 0
      unless params[:location_id].blank?
        @user = User.find(params[:check][:sender_id]) if @user.blank?
      	@wallet = Wallet.find(params[:location_id])
      	location = @wallet.location
      	params[:wallet_id] = @wallet.id
        raise I18n.t("api.registrations.unavailable") if location.try(:block_withdrawal)
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
        main_type = updated_type(TypesEnumLib::TransactionType::SendCheck)
        if @user.present? && @wallet.present?
          amount_sum = 0
          fee_lib = FeeLib.new
          escrow_wallet = Wallet.check_escrow.first
          escrow_user = escrow_wallet.try(:users).try(:first)
          if @user.iso? || @user.agent? || @user.affiliate? || @user.affiliate_program?
            fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
            if fee_object.present?
              send_check_fee = fee_object["send_check"].present? ? fee_object["send_check"].to_f : 0
              fee_perc_fee = fee_object["redeem_fee"].present? ? deduct_fee(fee_object["redeem_fee"].to_f, amount) : 0
            else
              send_check_fee = 0
              fee_perc_fee = 0
            end
            fee = send_check_fee + fee_perc_fee
            amount_sum = amount + fee
            balance = show_balance(@wallet.id, @user.ledger).to_f
            raise CheckError.new(I18n.t('merchant.controller.insufficient_balance')) if balance < amount_sum
            if @user.iso?
              raise CheckError.new("Error Code 6003 Send Check failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0) && @user.iso?
            elsif @user.affiliate? || @user.agent? || @user.affiliate_program?
              raise CheckError.new("Error Code 6003 Send Check failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0)
            end
            check = TangoOrder.new(user_id: @user.id,
                                   name: name,
                                   status: "PENDING",
                                   amount: amount_sum,
                                   actual_amount: amount,
                                   check_fee: send_check_fee,
                                   fee_perc: fee_perc_fee,
                                   recipient: recipient,
                                   description: description,
                                   order_type: "check",
                                   approved: false,
                                   settled: false,
                                   wallet_id: @wallet.id,
                                   send_via: send_via,
                                   batch_date: Time.now.to_datetime + 1.day,
                                   amount_escrow: true)
            tags = {
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: current_user,
                sender_wallet_id: @wallet.id,
                sender_name: sender_dba,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: fee.to_f,
                net_amount: amount_sum.to_f - fee.to_f,
                net_fee: fee.to_f,
                total_amount: amount_sum.to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )
            if fee > 0
              withdraw = custom_withdraw(amount,@wallet.id, fee, TypesEnumLib::TransactionType::SendCheck,nil,send_via,nil, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,true)
            else
              withdraw = withdraw(amount,@wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,nil, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
            end
            if withdraw.present? && withdraw["actions"].present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
              if check.save
                flash[:success] = "Successfully sent a check to #{ recipient }"
              else
                flash[:error] = I18n.t 'merchant.controller.chck_failed'
              end
            else
              flash[:error] = I18n.t 'merchant.controller.unaval_feature'
            end
          end
        else
          flash[:error] = I18n.t 'merchant.controller.insufficient_balance'
          raise "Please select a wallet and try again "
        end
      end
    rescue => ex
      flash[:error] = I18n.t 'merchant.controller.chck_creation_faild'
      flash[:error] = ex.message if ex.class == CheckError
      msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
    ensure
      if flash[:error].blank? && flash[:success].blank?
        flash[:error] = I18n.t 'merchant.controller.except'
        msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\`"
        handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
      end
      if flash[:error].blank? and check.present?
        type=current_user.affiliate_program? ? 'affiliate_activity' : nil
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]
        ActivityLog.log(type,"Check Created",current_user,params,response,"Check Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","checks")
      else
        if flash[:error].include?('Error Code')
          str = flash[:error]
          message = str[16, str.length-1]
          code = str[0,15]
        else
          message = flash[:error]
          code = " Error Code #"
        end
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', error_code: code , message: message
      end
      # return redirect_back(fallback_location: root_path)
    end

    rescue Exception => e
      flash[:error] = e.message
      ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error]
    end
    flash.discard
  end

  def update
      begin
        if params[:id].present?
          check = TangoOrder.find(params[:id].to_i)
          user = check.user
          check_old_status = check.status
          if check_old_status == "VOID"
            flash[:success] = check.instant_ach? ? "Successfully Voided ACH" : check.instant_pay? ? "Successfully Voided Push to Card" : check.check? ? "Successfully Voided Check" : flash[:success] = "Successfully Voided"
            return ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]            
          end
          if check.present?
            check_escrow = Wallet.check_escrow.first
            escrow_user = check_escrow.try(:users).try(:first)
            if params[:status] == "VOID"
              check_info = {
                  name: check.name,
                  check_email: check.recipient,
                  check_id: check.checkId,
                  check_number: check.number,
                  notes: params[:notes]
              }
              if check.status == "PENDING"
                wallet = Wallet.find(check.wallet_id)
                if current_user.present? && current_user.role!='merchant'
                  location=nil
                  merchant = merchant_to_parse(wallet.users.first)
                else
                  location = location_to_parse(wallet.location)
                  merchant = merchant_to_parse(wallet.location.merchant)
                end
                tags = {
                    "location" => location,
                    "merchant" => merchant,
                    "send_check_user_info" => check_info,
                }
                ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
                # ach_check_type=check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard :  TypesEnumLib::TransactionType::VoidCheck
                fee_perc_details = {}
                if check.amount_escrow.present?
                  main_tx = check.try(:transaction)
                  fee_perc_details["fee_perc"] = main_tx.try(:[],'tags').try(:[],'fee_perc')
                  receiver_dba=get_business_name(wallet)
                  ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
              
                  transaction = Transaction.create(
                      to: nil,
                      from: wallet.id,
                      status: "pending",
                      amount: check.actual_amount,
                      sender: escrow_user,
                      sender_name: escrow_user.try(:name),
                      receiver_wallet_id: wallet.id,
                      sender_wallet_id: check_escrow.try(:id),
                      receiver_id: current_user.try(:id),
                      sender_balance: SequenceLib.balance(wallet.id),
                      receiver_balance: SequenceLib.balance(check_escrow.try(:id)),
                      receiver_name: receiver_dba,
                      action: 'transfer',
                      net_amount: check.actual_amount.to_f,
                      total_amount: check.amount.to_f,
                      ip: get_ip,
                      main_type: ach_check_type,
                      tags: tags,
                      gbox_fee: main_tx.try(:gbox_fee),
                      iso_fee: main_tx.try(:gbox_fee),
                      affiliate_fee: main_tx.try(:gbox_fee),
                      agent_fee: main_tx.try(:gbox_fee),
                      fee: main_tx.try(:fee),
                      net_fee: main_tx.try(:net_fee)
                  )
                   ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
             
                  issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info,check) if @user.MERCHANT? || @user.ISO? || @user.AGENT? || @user.AFFILIATE? || @user.affiliate_program?
                  check.amount_escrow = false
                else
                  receiver_dba=get_business_name(wallet)
                   ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
             
                  transaction = user.transactions.create(
                      to: nil,
                      from: wallet.id,
                      status: "pending",
                      amount: check.actual_amount,
                      receiver_wallet_id: wallet.id,
                      receiver_name: receiver_dba,
                      receiver_balance: SequenceLib.balance(wallet.id),
                      receiver_id: current_user.try(:id),
                      action: 'issue',
                      net_amount: check.actual_amount.to_f,
                      total_amount: check.actual_amount.to_f,
                      ip: get_ip,
                      main_type: ach_check_type,
                      tags: tags
                  )  
                  ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
               
                  issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info)
                end
            

               
                if issue.present?
                  transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags.merge(fee_perc_details), timestamp: issue["timestamp"])
                  #= creating block transaction
                  parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                  save_block_trans(parsed_transactions,"sub","void") if parsed_transactions.present?
                end
                check.void_transaction_id = transaction.try(:id)
                if !check.under_review?
                  check.status = params[:status]
                  check.approved = true
                  check.settled = true
                else
                  flash[:error] = "Can't Process Under Review #{check.instant_ach? ? 'Ach':'Check'}"
                end
                if check.save
                  check.instant_ach? ? flash[:success] = "Successfully Voided ACH" : check.instant_pay? ? flash[:success] = "Successfully Voided Push to Card" : flash[:success] = "Successfully Voided"
                else
                  flash[:error] = "Error Occurred"
                end
              elsif check.checkId.present?
                url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
                http = Net::HTTP.new(url.host,url.port)
                http.use_ssl = true
                http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                requeste = Net::HTTP::Get.new(url)
                requeste["Accept"] = 'application/json'
                requeste["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
                response = http.request(requeste)
                checkData = JSON.parse(response.read_body)
                if checkData["status"] == "UNPAID"
                  check.status = params[:status]
                  check.save
                  url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
                  http = Net::HTTP.new(url.host,url.port)
                  http.use_ssl = true
                  http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                  requeste = Net::HTTP::Delete.new(url)
                  requeste["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
                  requeste["Content-Type"] ='application/json'
                  response = http.request(requeste)
                  case response
                  when Net::HTTPSuccess
                    wallet = Wallet.find(check.wallet_id)
                    if current_user.present? && current_user!='merchant'
                      location=nil
                      merchant = merchant_to_parse(wallet.users.first)
                    else
                      location = location_to_parse(wallet.location)
                      merchant = merchant_to_parse(wallet.location.merchant)
                    end
                    tags = {
                        "location" => location,
                        "merchant" => merchant,
                        "send_check_user_info" => check_info,
                    }
                    ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
                    transaction = @user.transactions.create(
                        to: nil,
                        from: wallet.id,
                        status: "pending",
                        amount: check.actual_amount,
                        sender: current_user,
                        sender_name: current_user.try(:name),
                        sender_wallet_id: wallet.id,
                        sender_balance: SequenceLib.balance(wallet.id),
                        action: 'issue',
                        net_amount: check.actual_amount.to_f,
                        total_amount: check.actual_amount.to_f,
                        ip: get_ip,
                        main_type: ach_check_type,
                        tags: tags
                    )
                    ach_check_type=check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck
                    issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
                    if issue.present?
                      transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
                      #= creating block transaction
                      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                      save_block_trans(parsed_transactions) if parsed_transactions.present?
                    end
                    check.status = params[:status]
                    check.settled = true
                    check.approved = true
                    check.void_transaction_id = transaction.try(:id)
                    if check.save
                      flash[:success] = I18n.t 'merchant.controller.successful_void_check'
                    else
                      flash[:error] = I18n.t 'merchant.controller.exception2'
                    end
                  when Net::HTTPUnauthorized
                    check.status = check_old_status
                    check.save
                    flash[:error] = I18n.t 'merchant.controller.unauthorize_access'
                  when Net::HTTPNotFound
                    check.status = check_old_status
                    check.save
                    flash[:error] = I18n.t 'merchant.controller.record_notfound'
                  when Net::HTTPServerError
                    check.status = check_old_status
                    check.save
                    flash[:error] = I18n.t 'merchant.controller.server_notrespnd'
                  else
                    check.status = check_old_status
                    check.save
                    error = JSON.parse(response.body)
                    flash[:error] = "#{error["error"]}"
                  end
                else
                  if checkData["status"] == "IN_PROCESS"
                    # flash[:error] = I18n.t 'merchant.controller.ex_time_limit'  temporaily commented untill new US team create a new Error for it
                  else
                    msg = "The VOID cannot be processed at this time beacuse the check status is #{checkData["status"]}"
                    handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true) unless request.host == 'localhost'
                    flash[:error] = I18n.t 'merchant.controller.void_not_proced'
                  end
                end
              else
                flash[:error] = I18n.t 'merchant.controller.exception3'
              end
              if flash[:error].blank?
                order_type = ("ACH" if check.order_type == "instant_ach") || ("P2C" if check.order_type == "instant_pay") || "Check"
                activity_type = ("ach" if check.order_type == "instant_ach") || ("push_to_card" if check.order_type == "instant_pay") || ("checks" if check.order_type == "check")  || "bulk_checks"
                
                ActivityLog.log(nil,"#{order_type} Voided",current_user,params,response,"#{order_type} Voided [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]",activity_type)            
                ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]
              else
                ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error]   
              end
            end
          end
        end             
      rescue Exception => e
        flash[:error] = e.message
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error]   
      end
  end

  def update_system_fee
    if @user.admin_user_id.present? && @user.affiliate_program?
      admin_user = User.find(@user.admin_user_id)
      @user.system_fee = admin_user.system_fee
    end
  end

end