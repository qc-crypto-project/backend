class V2::Merchant::SettingsController < V2::Merchant::BaseController

  def index
    @heading = "Employee List"
    @user= User.new
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    if current_user.merchant_id.present?
      @sub_merchants= User.where(id: current_user.merchant_id).first.sub_merchants.where(archived: "false").where.not(id: current_user.id).order('created_at DESC').per_page_kaminari(params[:page]).per(page_size)
      @wallets = Location.where(merchant_id: current_user.merchant_id)
    else
      if params[:q].present?
        val=params[:q]
        params[:q]={name_or_email_or_phone_number_or_last_name_cont:params[:q]}
        params[:q]['id_eq']=val
        params[:q]['permission_name_cont'] = val
        @q= current_user.sub_merchants.where(archived: "false").includes(:permission).ransack(params[:q].try(:merge, m: 'or'))
        @sub_merchants=@q.result().per_page_kaminari(params[:page]).per(page_size)
      else
        @sub_merchants= current_user.sub_merchants.where(archived: "false").order('created_at DESC').per_page_kaminari(params[:page]).per(page_size)
      end
      @wallets = Location.where(merchant_id: current_user.id)
    end
    @sub_merchant=User.new
    # @wallets = current_user.wallets
    @filters = [10,25,50,100]
    if params[:search].present? && !params[:page].present?
      render partial: 'v2/merchant/shared/profile_datatable'
    else
      render template: 'v2/merchant/settings/index'
      # render partial: 'v2/merchant/shared/profile_datatable'
    end
  end

  def create
    begin
      params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number]
      params[:user][:first_name].strip!
      # params[:user][:last_name] = params[:user][:name].partition(" ").last
      params[:user][:name] = params[:user][:first_name]
      params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
      # params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
      @sub_merchant = User.new(sub_merchant_params)
      @sub_merchant.merchant_id = current_user.merchant_id || current_user.id
      @sub_merchant.is_block = false
      mail=params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
      user = User.where.not(role:'user').where(email: mail ).first
      if user.present? && user.archived == false
        flash[:successs] = I18n.t('merchant.controller.used_email')
      # elsif user.present?
      #   user.archived = false
      #   user.password_changed(true)
      #   if user.update(sub_merchant_params)
      #     UserMailer.welcome_employee(@sub_merchant, @sub_merchant.password).deliver_later
      #     flash[:successs] = I18n.t 'merchant.controller.successful_user_add'
      #     binding.pry
      #   else
      #     flash[:successs] = user.errors.first
      #   end
      else
        @sub_merchant.regenerate_token
        @sub_merchant.is_block = false
        # @sub_merchant.submerchant_type = params[:submerchant_type]
        # params[:user][:password]= "#{rand(1..10)}a#{Devise.friendly_token.first(8)}A"
        password = @sub_merchant.password
        @sub_merchant.merchant!
        if @sub_merchant.errors.blank?
          if params[:user][:wallets]
            params[:user][:wallets].each do |id|
              location = Location.find id
              @sub_merchant.wallets << location.wallets.primary.first
            end
          end
          UserMailer.welcome_employee(@sub_merchant, password).deliver_later
          flash[:successs] = I18n.t 'merchant.controller.successful_user_add'
        else
          flash[:successs] = @sub_merchant.errors.first
        end
      end
      ActivityLog.log(nil,"User Added",current_user,params,response,"Added Employee User #{params[:user][:name]} #{params[:user][:last_name]}","employees")
    rescue => ex
      flash[:successs] = ex.message
    end
    respond_to  do |format|

      format.html{ redirect_back(fallback_location: v2_merchant_settings_path) }
    end
  end

  def new
    if current_user.merchant_id.present?
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
      @permission = Permission.where('merchant_id IN (?) OR permission_type IN (?)',[current_user.merchant_id], [0,1])
    else
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
      @permission = Permission.where('merchant_id IN (?) OR permission_type IN (?)',[current_user.id], [0,1])
    end
    @user= User.new
    @user=User.find params[:id] if params[:id].present?
    render partial: 'v2/merchant/settings/add_new_employee'
    # respond_to do |format|
    #
    #   format.js
    # end
  end

  def edit
    @sub_merchant = User.find(params[:id])
    @merchant = User.find @sub_merchant.merchant_id
    if current_user.merchant_id.present?
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
    else
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
    end
    @permission = Permission.where('merchant_id IN (?) OR permission_type IN (?)',[@merchant.id], [0,1])
    # @wallets = (@sub_merchant.locations + Location.where(merchant_id: @merchant.id)).uniq
    render partial: 'v2/merchant/settings/add_new_employee'
  end

  def update
    params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number]
    @sub_merchant=User.find params[:id]
    existing_wallets = @sub_merchant.wallets.where.not(location_id: nil)
    coming_locations = params[:user][:wallets]
    name=@sub_merchant.try(:name)
    ids=@sub_merchant.wallets.pluck(:id)

    # params[:user][:email]=params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
    params[:user][:name] = params[:user][:first_name]
    # params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
    result = update_sub_merchant(@sub_merchant,params[:user],params[:id].to_i)
    if result.first
      if @sub_merchant.errors.blank?
        if coming_locations.present?
          existing_wallets.each do |wallet|
            @sub_merchant.wallets.delete(wallet)
          end
          coming_locations.each do |id|
            location = Location.find(id)
            @sub_merchant.wallets << location.wallets.primary.first if location.present?
          end
        end
        flash[:successs] = result.second
      else
        flash[:successs] = @sub_merchant.errors.full_messages.first
      end
    else
      flash[:successs] = result.second
    end
    new_ids = @sub_merchant.wallets.pluck(:id)
    wallet_changes = ids - new_ids
    save_activity_logs(ids,name) if @sub_merchant.attributes_changed? || wallet_changes.present?
    redirect_back(fallback_location: v2_merchant_settings_path)
  end

  def update_sub_merchant(submerchant,params,sub_id)
    begin
      user = User.find_by(email: params["email"]) if params["email"].present? && params["email"] != submerchant.email
      if submerchant.email.present? && params["email"].present? && submerchant.email==params["email"] && params["password"].blank? && submerchant.name==params['name'] && submerchant.last_name==params['last_name'] && submerchant.permission_id == params['permission_id']
        return  [true,"Updated Successfully"]
      elsif submerchant.email.present? && params["email"].present? && submerchant.email!=params["email"] && submerchant.email.downcase==params["email"].downcase && params["password"].blank? && submerchant.name==params['name'] && submerchant.last_name==params['last_name'] && submerchant.permission_id == params['permission_id'] && user.id==sub_id
        return  [false,"You can not edit the same email"]
      elsif user.present?
        return  [false,"A user with this email already exists"]
      else
        submerchant.email = params["email"]; submerchant.name = params["name"]; submerchant.phone_number = params["phone_number"]; submerchant.last_name = params["last_name"]; submerchant.permission_id = params["permission_id"]
        if params["password"].blank?
          if submerchant.save
            return  [true,"Employee Information Updated Successfully!"]
          else
            return [false,submerchant.errors.full_messages.first]
          end
        else
          submerchant.password = params["password"]
          if submerchant.save
            # ActivityLog.log(nil,"Changed Password",current_user,params,response,"Changed Password for #{@sub_merchant.try(:name)} ")
            return [true,"Updated Successfully"]
          else
            return [false,submerchant.errors.full_messages.first]
          end
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      return [false,invalid]
    end
  end

  def destroy
    @sub_merchant = User.where(id: params[:user_id]).first
    if @sub_merchant.archived == false
      @sub_merchant.update(archived: true,permission_id: nil)
    else
      @sub_merchant.update(archived: false)
    end
    ActivityLog.log(nil,"User Deleted",current_user,params,response,"Deleted Employee User #{@sub_merchant.try(:name)} #{@sub_merchant.try(:last_name)}","employees")
    flash[:successs] = "User Successfully Deleted!"
    redirect_back(fallback_location: v2_merchant_settings_path)
  end

  private

  def save_activity_logs(ids,name)
    changes=[]
    new_ids=@sub_merchant.wallets.pluck(:id)
    wallet_changes=ids-new_ids
    wallet_changes=wallet_changes.present? ? wallet_changes :  new_ids-ids
    user_audit=@sub_merchant.audits.where(created_at: (Time.now-50)..Time.now) if @sub_merchant.try(:audits).present?
    if user_audit.present?
      changes=user_audit.try(:last).try(:audited_changes)
      # changes=changes.map{|a| a.try(:humanize)}
      new_hash={}
      changes.to_hash.each_pair do |k,v|
        new_hash.merge!({k.humanize => v})
      end
      if new_hash['Permission'].present?
        permission=Permission.where(id:new_hash['Permission'].last).first
        new_hash['Permission']=permission.try(:name)
      end
      changes=new_hash
    end
    if changes.present? && wallet_changes.present?
      wallet=Wallet.where(id:wallet_changes).pluck(:name)
      changes=changes.merge({'Account Access'=>wallet.try("to_sentence")})
    elsif wallet_changes.present?
      wallet=Wallet.where(id:wallet_changes).pluck(:name)
      changes={'Account Access'=>wallet.try("to_sentence")}
    end
    if changes.present?
      changes.to_hash.each_pair do |k,v|
        if k=='Permission' || k=='Account Access'
          ActivityLog.log(nil,"User Update",current_user,params,response,"Modified #{name}'s #{k}  #{v}","employees")
        elsif k=='Encrypted password'
          ActivityLog.log(nil," Changed password",current_user,params,response," Changed password for #{name}","employees")
        else
          ActivityLog.log(nil,"User Update",current_user,params,response,"Changed #{name}'s #{k} from #{v.try(:first)} to #{v.try(:last)}","employees")
        end
      end
    end
  end

  def sub_merchant_params
    params.require(:user).permit(:first_name,:name, :email, :phone_number,:last_name, :merchant_id, :password, :password_confirmation, :retail_sales, :archived, :permission_id,:is_block)
  end
end
