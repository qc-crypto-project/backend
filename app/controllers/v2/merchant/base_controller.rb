class V2::Merchant::BaseController < ApplicationController
  require 'uri/http'
  require 'uri/https'
  require 'net/http'
  require 'will_paginate/array'
  include V2::ApplicationHelper
  include V2::Merchant::BaseHelper
  include StripeHandler

  # Using Merchant Layout for Merchant namespace views
  layout "v2/merchant/layouts/application"

  # all child controllers will automatically enforce access to merchants only
  before_action :check_merchant , :authenticate_user!, :set_wallet_balance, :require_merchant, :verify_tos_acceptance, :set_merchant_permission, :permitted_routes,  except: [:get_user, :welcome, :tos_approval, :send_tos, :update_password_tos, :generate_export_file, :accept_cookies]
  before_action :set_merchant_permission,:authenticate_user!, :permitted_routes, only: [:welcome, :tos_approval, :send_tos, :update_password_tos]
  skip_before_action :require_merchant, :set_wallet_balance, only: [:show_wallet_balance, :verify_user, :get_user]
  skip_before_action :set_wallet_balance, only: [:generate_export_file, :accept_cookies]
  before_action :set_merchant_permission, :permitted_routes, except: [:accept_cookies]

  def verify_user
    params[:email]=params[:email].downcase if params[:email].present?
    params[:phone_number]=params[:phone_number].downcase if params[:phone_number].present?
    wallet=''
    if params[:user_phone].present? && !params[:user_phone].blank?
      @user = User.unarchived_users.where.not(:role => :user).where(phone_number: params[:user_phone]).first
    elsif params[:ref_no].present? && !params[:ref_no].blank?
      @user = User.unarchived_users.where(ref_no: params[:ref_no].upcase).where.not(:role => :user).first
    elsif  params[:email].present? && params[:password].present?
      @user = current_user
      if @user.valid_password?(params[:password])
        @user = @user
      else
        @user= nil
      end
    elsif  params[:email].present?
      @user =  User.unarchived_users.where.not(role: 'user').find_by_email(params[:email])
      if @user.present?
        @user= @user
      else
        @user= nil
      end
    end
    if @user.present?
      @user.wallets.present? ? (wallet= @user.wallets.first) : (wallet = "No wallet")
      render status: 200, json:{success: 'Verified', :user => @user, :wallet => wallet, qr_codes: params[:qr_codes]} #used qr codes to be used in redeem qr functionality
    else
      render status: 200, json:{success: "not found",error: 'You are not Authorized for this action.'}
    end
  end

  def verifying_user
    phone_number = params[:phone_number]
    email = params[:email]
    table = User
    if email.present?
      email = email.downcase
      user = table.where.not(role:'user').complete_profile_users.where(email: email).first if params[:not_user] != 'notuser'
      user = table.where.not(role:'user').complete_profile_users.where(email:email).first if params[:not_user]=='notuser'
      ids=user.id if user.present?
      if user.present? && ids != params[:user_id].to_i && !user.archived?
        render status: 200, json:{success: 'notverified'}
      else
        render status: 200, json:{success: 'Verified', :user => @user}
      end
    elsif phone_number.present?
      user = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).first
      ids = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      res = Phonelib.valid?(phone_number)
      message = !res ? "Invalid Phone Number" : "A user with this phone number already exists"
      if ids.present? && ids.count > 0 && !ids.include?(params[:user_id].to_i)  && !user.archived? || !res
        render status: 200, json:{success: 'notverified',message: message}
      else
        render status: 200,json:{success: 'Verified', :user => @user}
      end
    end
  end

  def welcome
    render template: 'v2/merchant/welcome/index'
  end

  def tos_approval
    user = current_user
    if user.role == "merchant"
      UserMailer.integration(user.id).deliver_later
    end
    if user.merchant_id.present? && user.try(:permission).admin?
      user = User.find(user.merchant_id)
    end
    if user.update(tos_checking: true,tos_acceptance_date: Time.now.utc,tos_user_id: current_user.id)
      render status: 200,json:{success: 'success'}
    else
      render status: 404,json:{success: 'failed'}
    end
  end

  def send_tos
    user = current_user
    if UserMailer.send_tos(params[:email], user).deliver_later
      render status: 200,json:{success: 'success'}
    else
      render status: 404,json:{success: 'failed'}
    end
  end

  def update_password_tos
    if current_user.valid_password?(params[:old_password])
      if params[:new_password] != params[:old_password]
        if current_user.update(password: params[:new_password])
          sign_in(current_user, :bypass => true)
          bypass_sign_in current_user
          render status: 200,json:{success: 'success'}
        else
          render status: 404,json:{success: current_user.errors.full_messages.first}
        end
      else
        render status: 200,json:{success: 'success'}
      end
    else
      render status: 404,json:{success: 'failed'}
    end
  end

  def verifying_user_phone_email
    phone_number = params[:phone_number]
    email = params[:email]
    table = User
    if email.present? && params[:user_id].present?
      email = email.downcase
      user = table.where.not(role:'user').complete_profile_users.where(email: email).first if params[:not_user] != 'notuser'
      user = table.where.not(role:'user').complete_profile_users.where(email:email).first if params[:not_user]=='notuser'
      ids=user.id if user.present?
      if user.present? && ids != params[:user_id].to_i && !user.archived?
        render status: 200, json:{success: 'notverified'}
      else
        render status: 200, json:{success: 'Verified', :user => @user}
      end
    elsif phone_number.present? && params[:user_id].present?
      user = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).first
      ids = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      res = Phonelib.valid?(phone_number)
      message = !res ? "Invalid Phone Number" : "A user with this phone number already exists"
      if ids.present? && ids.count > 0 && !ids.include?(params[:user_id].to_i)  && !user.archived? || !res
        render status: 200, json:{success: 'notverified',message: message}
      else
        render status: 200,json:{success: 'Verified', :user => @user}
      end
    end
  end

  def get_user
    user = User.find_by(id:params[:id])
    if user.present?
      render status: 200, json:{success: 'found',user: user}
    else
      render status: 200,json:{success: 'not found'}
    end
  end

  def valid_location
    if params[:wallet_id].present?
      @wallet = Wallet.find params[:wallet_id]
      if @wallet.present?
        render status: 200, json: {success: true , location: @wallet.location}
      else
        render status: 404, json: {success: false}
      end
    end
  end

  def show_wallet_balance
    wallet_id = params[:wallet_id]
    balance = show_balance(wallet_id)
    location = Wallet.find(wallet_id).location
    render status: 200 , json:{:balance => number_with_precision(balance.to_f, precision: 2), location: location.block_withdrawal}
  end

  def verify_tos_acceptance
    return unless current_user.merchant? || current_user.iso? || current_user.agent? || current_user.affiliate?

    if current_user.merchant_id.blank? && current_user.tos_checking == false
      return redirect_to v2_welcome_path
    elsif current_user.merchant_id.present?
      user = User.find(current_user.merchant_id)
      if user.tos_checking == false
        if current_user.try(:permission).admin?
          return redirect_to v2_welcome_path
        else
          redirect_to virtual_terminal_merchant_sales_path
        end
      end
    end
    if current_user.iso? || current_user.agent? || current_user.affiliate?
      if current_user.tos_checking == false
        return redirect_to v2_welcome_path
      end
    end
  end

  def permitted_routes
    if @user.present? && @user.merchant? && session[:previous_url].present? && !@user_permission.try(:admin?)
      allowed = true
      case params["controller"]
      when "v2/merchant/accounts"
        allowed = false unless !@user_permission.try(:regular?) && @user_permission.try(:view_accounts) || @user_permission.try(:account_transfer) || @user_permission.try(:refund) || @user_permission.try(:tip_transfer) || @user_permission.try(:export_daily_batch)
      when "v2/merchant/instant_ach"
        allowed = false unless @user_permission.try(:ach_view_only) || @user_permission.try(:ach_add) || @user_permission.try(:ach_void)
      when "v2/merchant/push_to_card"
        allowed = false unless @user_permission.try(:push_to_card_view_only) || @user_permission.try(:push_to_card_add) || @user_permission.try(:push_to_card_void)
      when "v2/merchant/checks"
        allowed = false unless @user_permission.try(:check_view_only) || @user_permission.try(:check_void) || @user_permission.try(:check_add)
      when "v2/merchant/giftcards"
        allowed = false unless @user_permission.try(:gift_card_view_only) || @user_permission.try(:gift_card_add) || @user_permission.try(:gift_card_void)
      when "v2/merchant/chargebacks"
        allowed = false unless @user_permission.try(:view_chargeback_cases) || @user_permission.try(:fight_chargeback) || @user_permission.try(:accept_chargeback)
      when "v2/merchant/sales"
        allowed = false unless @user_permission.try(:virtual_terminal)
      when "v2/merchant/invoices"
        if params[:action] == "payee"
          allowed = false unless @user_permission.try(:view_invoice) || @user_permission.try(:create_invoice) || @user_permission.try(:cancel_invoice)
        elsif params[:action] == "clients"
          allowed = false unless @user_permission.try(:customer_list)
        end
      when "v2/merchant/hold_money"
        allowed = false unless @user_permission.try(:funding_schedule)
      when "v2/merchant/settings"
        allowed = false unless @user_permission.try(:user_view_only) || @user_permission.try(:user_edit) || @user_permission.try(:user_add) || @user_permission.try(:employee_list)
      when "v2/merchant/permission"
        allowed = false unless @user_permission.try(:permission_list) || @user_permission.try(:permission_view_only)  || @user_permission.try(:permission_edit) || @user_permission.try(:permission_add)
      when "v2/merchant/location_fees"
        allowed = false unless @user_permission.try(:fee_structure)
      when "v2/merchant/plugins"
        allowed = false unless @user_permission.try(:plugin)
      when "v2/merchant/apps"
        allowed = false unless @user_permission.try(:api_key)
      end
      if allowed
        session[:previous_url] = request.env["REQUEST_PATH"]
      else
        flash[:error] = "You don't have required permission!"
        redirect_back fallback_location: session[:previous_url]
      end
    else
      session[:previous_url] = request.env["REQUEST_PATH"]
    end
  end

  def generate_export_file
    if params[:search_query].present?
      if params[:search_query].try(:[],"date").present?
        if params[:type] == "merchant_dispute"
          date1 = params[:search_query].try(:[],"date")
          first_date = date1[0..9]
          first_date = Date.strptime(first_date, '%m/%d/%Y');
          second_date = date1[13..22]
          second_date = Date.strptime(second_date, '%m/%d/%Y')
          date={:first_date=>first_date,:second_date=>second_date}
        else
          date = parse_date_with_time(params[:search_query].try(:[],"date"), params[:offset])
        end
      elsif params[:search_query].try(:[],"export_date").present?
        date = parse_date_with_time(params[:search_query].try(:[],"export_date"), params[:offset])
      elsif params[:batch_date].present?
        date = "#{params[:batch_date].to_date.strftime("%m/%d/%Y")} - #{params[:batch_date].to_date.strftime("%m/%d/%Y")}"
        date = parse_date_with_time(date, params[:offset])
      end
      if params[:search_query]["time1"].present? || params[:search_query]["time2"].present?
        time=parse_export_time(params[:search_query]["time1"],params[:search_query]["time2"])
      end
      if time.present? && (params[:search_query]["date"].present? || params[:search_query]["datie"].present?)
        date[:first_date] = "#{date[:first_date].first(11)}#{time.first}"
        date[:second_date] = "#{date[:second_date].first(11)}#{time.second}"
      end
      if time.present?
        date[:first_date] = "#{date[:first_date].first(11)}#{time.first}"
        date[:second_date] = "#{date[:second_date].first(11)}#{time.second}"
      end
    elsif params[:start_date].present? || params[:end_date].present?
      date = parse_refund_date(params[:start_date], params[:end_date])
    elsif params[:type] == "permission_export"
      date = {}
      permissions = Permission.where(merchant_id: current_user.id).order(id: :desc)
      date[:second_date] = permissions.first.try(:created_at).try(:strftime, "%d/%m/%Y")
      date[:first_date] = permissions.last.try(:created_at).try(:strftime, "%d/%m/%Y")
    elsif params[:type] == "employee_export"
      date = {}
      merchant_ids = User.where(role: 'merchant', merchant_id: nil).pluck(:id)
      sub_merchants = User.where(merchant_id: merchant_ids).order(id: :desc)
      date[:second_date] = sub_merchants.first.created_at.strftime("%d/%m/%Y")
      date[:first_date] = sub_merchants.last.created_at.strftime("%d/%m/%Y")
    elsif params[:batch_date].present?
      date = "#{params[:batch_date].to_date.strftime("%m/%d/%Y")} - #{params[:batch_date].to_date.strftime("%m/%d/%Y")}"
      date = parse_date_with_time(date, params[:offset])
    else
      date = parse_date_with_time(params[:date], params[:offset]) if params[:date].present?
    end
    report = getting_old_report(date,params)
    send_via = params[:send_via] if params[:send_via]
    search_query = params[:search_query].to_json if params[:search_query].present?
    message = ""
    file_id = ""
    unless report[:find]
      if date.present?
        job_id = ExportFileWorker.perform_async(current_user.id, date[:first_date], date[:second_date], params[:type], params[:offset], params[:wallet_id], send_via, search_query, nil, params[:status],params[:trans],nil,nil,session[:session_id],params[:email],params[:batch_id], params.to_json)
      else
        job_id = ExportFileWorker.perform_async(current_user.id, nil, nil, params[:type], params[:offset], params[:wallet_id], send_via, search_query, nil, params[:status],params[:trans],nil,nil,session[:session_id],params[:email],params[:batch_id], params.to_json)
      end
    else
      job_id = nil
      message = report[:status]
    end
    respond_to do |format|
      format.json do
        render json: {
            jid: job_id,
            status: report[:status],
            message: message,
            type: params[:type],
            file_id: file_id.to_json
        }
      end
    end
  end

  def activity_logs
    Time.zone = cookies[:timezone]
    if current_user.merchant_id.present? && @user_permission.admin?
      @main_merchant =  User.find current_user.try(:merchant_id)
      @users = @main_merchant.sub_merchants.active_users
    else
      @users = current_user.sub_merchants.active_users
    end
    admin_id = User.where(role: 'admin').first.id
    if params[:search] == "true"
      activity_logs = []
      today = yesterday = week = month = year = 0
      show_more = params[:last_id].present? ? (["activity_logs.id < ?", params[:last_id]]) : []
      if params[:filter].present?
        if current_user.merchant_id.present? && @user_permission.admin?
          condition = params[:person].present? ? {action_type: params[:action_type], user_id: params[:person]} : {action_type: params[:action_type],user_id: @users.ids.push(@main_merchant.id)}
        else
          condition = params[:person].present? ? {action_type: params[:action_type], user_id: params[:person]} : {action_type: params[:action_type],user_id: @users.ids.push(current_user.id)}
        end
        case params[:filter]
        when "today_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).today.order(id: "desc").limit(10)
        when "yesterday_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).yesterday.order(id: "desc").limit(10)
        when "week_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).past_week.order(id: "desc").limit(10)
        when "month_filter"
          activity_logs = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).this_month.order(id: "desc").limit(10)
        when "year_filter"
          date = params[:date_val]
          date = parse_daterange_activity(date)
          first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
          second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
          activity_counts = ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(created_at: first_date..second_date).length
          activity_logs =   ActivityLog.joins(:user).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(condition).where(show_more).where(created_at: first_date..second_date).order(id: "desc").limit(10)
        end
      elsif params[:person].present?
        activity_logs = ActivityLog.joins(:user).where(show_more).select('users.name as user_name', 'activity_logs.note as note', 'activity_logs.created_at as created_at', 'activity_logs.id as id').where(action_type: params[:action_type], user_id: params[:person]).order(id: "desc").limit(10)
        today = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).today.count
        yesterday = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).yesterday.count
        week = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).past_week.count
        month = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).this_month.count
        year = ActivityLog.where(action_type: params[:action_type], user_id: params[:person]).past_year.count
      end
      if params[:filter] == "year_filter"
        render status: 200, json:{activity_logs: activity_logs,activity_counts: activity_counts, today: today, yesterday: yesterday, week: week, month: month, year: year}
      else
        render status: 200, json:{activity_logs: activity_logs, today: today, yesterday: yesterday, week: week, month: month, year: year}
      end
    else
      if params[:action_type] == "employees"
        if current_user.merchant_id.present? && @user_permission.admin?
          activity_logs = ActivityLog.where("activity_logs.action_type = ? AND CAST(activity_logs.params -> 'user' ->> 'merchant_id' AS integer) IN(?)", params[:action_type], @users.ids.push(@main_merchant.id))
        else
          activity_logs = ActivityLog.where("activity_logs.action_type = ? AND CAST(activity_logs.params -> 'user' ->> 'merchant_id' AS integer) IN(?)", params[:action_type], @users.ids.push(current_user.id))
                              .or(ActivityLog.where("activity_logs.action_type = ? AND activity_logs.user_id = ?", params[:action_type], current_user.id))
        end
      else
        activity_logs = ActivityLog.where(action_type: params[:action_type],user_id: @users.ids.push(current_user.id, admin_id))
      end
      @today = activity_logs.today.count
      @yesterday = activity_logs.yesterday.count
      @week = activity_logs.past_week.count
      @month = activity_logs.this_month.count
      @year = activity_logs.past_year.count
      @activity_logs = activity_logs.today.order("id desc").limit(10)
      render :partial => 'v2/merchant/shared/activity_log'
    end
  end

  def update_notification
    notification =  Notification.find_by_id(params[:id])
    if notification.notifiable.present?
      if params[:hide_at] == "true"
        if notification && notification.hide_at.nil? && notification.update!(hide_at: DateTime.now.utc)
          count = 0
          if notification && notification.read_at.nil? 
            notification.update!(read_at: DateTime.now.utc) 
            count = 1
          end
          render json: {success: true , count: count} , status: 200
        end

      elsif params[:unread] == "true"
        if notification && notification.read_at.present?
          notification.update!(read_at: nil) if notification && notification.read_at.present?
          render json: {success: true} , status: 200
        end
      else
        if notification && notification.read_at.nil? && notification.update!(read_at: DateTime.now.utc)
          render json: {success: true} , status: 200
        end
      end
    else
      render json: {success: true} , status: 200
    end
  end

  def hide_all_notification

    if params[:type] == "read_all"
      load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where(read_at: nil)
      read_count = load_more_data.count
      load_more_data.update_all(read_at: DateTime.now.utc)
      render json: {"data"=>read_count}
    else
      if params[:type] == "yesterday"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 1)
      elsif params[:type] == "older"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) < ? ",Date.today - 2)
      elsif params[:type] == "today"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).today.where(hide_at: nil)
      elsif params[:type] == "day_before"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 2)
      elsif  params[:type] == "all"
        load_more_data =  Notification.where(recipient_id: current_user,hide_at: nil).where.not(text: nil)
      end
      read_count = load_more_data.where(read_at: nil).count
      load_more_data.where(read_at: nil).update_all(hide_at: DateTime.now.utc, read_at: DateTime.now.utc)
      load_more_data.where(hide_at: nil).update_all(hide_at: DateTime.now.utc)
      render json: {"data"=>read_count}
    end
  end

  def load_notifications
    if params[:type] == "yesterday"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 1).order("created_at DESC").limit(10)
    elsif params[:type] == "older"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) < ? ",Date.today - 2).order("created_at DESC").limit(20)
    elsif params[:type] == "today"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).order("created_at DESC").today.limit(10)
    elsif params[:type] == "day_before"
      load_more_data =  Notification.where("id < ? ",params[:id]).where(recipient_id: current_user,hide_at: nil).where.not(text: nil).where("Date(created_at) = ? ",Date.today - 2).order("created_at DESC").limit(10)
    end
    if load_more_data.present?
      render partial: 'v2/merchant/shared/load_more_notification' , locals: {load_more_data: load_more_data}
    else
      render json: {"data"=>false}
    end
  end

  def set_merchant_permission
    @user_permission = current_user.try(:merchant_id).present? ? current_user.try(:permission_id).present? ? current_user.try(:permission) : nil : Permission.admin.first
  end

  def accept_cookies
    if current_user.present?
      current_user.update(cookies_accept: true)
    end
  end

  private

  def check_merchant
    if params[:request_url].present?
      if current_user.present?
        unless current_user.merchant?
          if current_user && (current_user.partner? || current_user.agent? || current_user.iso? || current_user.affiliate? || current_user.qc?)
            return redirect_to quickcard_payment_v2_partner_invoices_path(request_url: params[:request_url])
          else
            return redirect_to new_user_session_path(request_url: params[:request_url]), notice: "Please sign in to continue" unless current_user&.merchant?
          end
        end
      end
    else
      flash[:alert] = I18n.t("devise.failure.page_not_exist") unless current_user&.merchant?
      return redirect_to new_user_session_path, notice: "Please sign in to continue" unless current_user&.merchant?
    end
  end


  def validate_location
    if params[:wallet_id].present?
      @wallet = Wallet.find params[:wallet_id]
    elsif params[:from_id]
      @wallet = Wallet.where(id: params[:from_id]).first
    end
    if @wallet.present?
      @location = @wallet.location
      if @location.present? && @location.is_block
        flash[:error] = I18n.t("errors.withdrawl.blocked_location")
        redirect_back(fallback_location: root_path)
      end
    end
  end

  def page_filter
    @filters = [10,25,50,100]
    @per_page = params[:filter].present? ? params[:filter].to_i : 10
  end

  def require_merchant
    if current_user && current_user.merchant?
      @user=current_user
      # elsif current_user && (current_user.iso? || current_user.qc?)
      #   @user=current_user
      # elsif current_user && current_user.agent?
      #   @user=current_user
      # elsif current_user && current_user.partner?
      #   @user = current_user
      # elsif current_user && current_user.affiliate?
      #   @user = current_user
    elsif current_user && (current_user.partner? || current_user.agent? || current_user.iso? || current_user.affiliate? || current_user.qc?)
      if params[:request_url].present?
        return redirect_to quickcard_payment_v2_partner_invoices_path(request_url: params[:request_url])
      else
        return redirect_to v2_partner_accounts_path
      end
    else
      if params[:request_url].present?
        flash[:error] = "Please login to continue!"
        return redirect_to new_user_session_path(request_url: params[:request_url])
      else
        flash[:error] = I18n.t 'merchant.controller'
        return redirect_to new_user_session_path unless current_user&.merchant?
      end
    end
  end
  # def is_merchant?
  #   redirect_to root_path if current_user.role == "merchant"
  # end
  def set_wallet_balance
    @balance = 0
    if current_user.present? && current_user.iso? || current_user.present? && current_user.agent? || current_user.present? && current_user.affiliate?
      @balance = show_balance(current_user.wallets.primary.first.id, current_user.ledger)
    elsif current_user.present? && current_user.qc?
      @balance = show_balance(current_user.wallets.first.id, current_user.ledger)
    end
    if current_user.present? && current_user.merchant_id.present? && current_user.MERCHANT? && current_user.submerchant_type == "admin_user"
      parent_user = User.find(current_user.merchant_id)
      @notifications = Notification.where(read_at: nil,recipient_id: parent_user.id, notifiable_type: "Request" )
    elsif current_user.present?
      @notifications = Notification.where(read_at: nil,recipient_id: current_user.id, notifiable_type: "Request" ).where.not(status:['rejected','approved']).or(Notification.where(status: ['approved','rejected'],actor_id: current_user.id, notifiable_type: "Request" ))
    end
  end

end
