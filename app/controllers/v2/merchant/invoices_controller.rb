class V2::Merchant::InvoicesController < V2::Merchant::BaseController
  include V2::ApplicationHelper
  include V2::Merchant::InvoicesHelper
  include UsersHelper
  include InvoiceHandler
  skip_before_action :set_wallet_balance, only: [:new, :detail_modal, :approve_invoice, :create, :update_invoice_status, :dashboard, :payee, :reopen, :cancel, :view_invoice, :payor, :recurring_billing, :recurring_billing_post, :clinets, :logo, :add_logo, :add_logo_post, :delete_logo, :template, :edit_template, :delete_template, :get_template, :new_template, :add_template]
  before_action :validate_location
  before_action :page_filter
  before_action :set_schedule_date, :set_params, only: :recurring_billing_post
  before_action :merchants_customer, only: [:reopen, :view_invoice]
  ALGORITHM = 'HS256'

  def update_invoice_status
    begin
      status = params[:status]
      invoice = params[:invoice_id]
      from_wallet = params[:from_wallet].nil? ? nil : params[:from_wallet]
      params[:page_number] = 0
      raise Exception.new "Missing params" if status.blank? || invoice.blank?
      @request = Request.in_progress.where(id: invoice).first
      raise Exception.new "Invoice Not Found" if @request.blank?
      case status
      when "cancel"
        @request.update(status: "cancel")
        render status: 200, json:{message: 'Request Cancelled'}
      when "approved"
        @wallet = @user.wallets.where(id: from_wallet).first
        raise Exception.new "Wallet does not exist" if @wallet.blank?

        # reciever = User.where(id: @request.reciever_id).first #reciever is the one who received request
        reciever = @user
        balance = show_balance(@wallet.id)
        balance = balance - HoldInRear.calculate_pending(@wallet.id)
        if @request.invoice?
          transaction_amount =@request.total_amount
        elsif @request.b2b?
          transaction_amount =@request.amount.to_f
        end
        raise Exception.new "Insufficient Funds for this Request" if balance.to_f < transaction_amount
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: '65percent'
        sender = User.where(id: @request.sender_id).first # sender is the one who sent request and we will deduct fee from sender
        data = ''
        if sender.merchant? #&& reciever.merchant?
          current_transaction = transaction_between(@request.wallet_id,@wallet.id ,transaction_amount, TypesEnumLib::TransactionType::InvoiceQC,0, data,nil,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil)
          seq_tx = current_transaction.try(:[], :seq_tx) if current_transaction.try(:[], :seq_tx).present?
        elsif sender.iso? || reciever.iso? || sender.agent? || reciever.agent? || sender.affiliate? || reciever.affiliate?
          amount_sum, fee = deduct_b2b_fee(sender, transaction_amount)
          if (reciever.iso? || reciever.agent? || reciever.affiliate?) && reciever.system_fee.present? && reciever.system_fee != "{}"
            limit_withdraw = reciever.system_fee["check_withdraw_limit"]
            raise Exception.new "You can't make transaction this time, Withdraw limit exceed!" if (balance.to_f - transaction_amount) < limit_withdraw.to_f
          end
          transaction = create_transaction(@user,@request.wallet_id, @wallet.id, reciever, sender, transaction_amount, fee,@request.id)
          current_transaction = SequenceLib.direct_transfer(transaction_amount, @wallet.id, @request.wallet_id, TypesEnumLib::TransactionType::InvoiceQC, fee,  nil, TypesEnumLib::GatewayType::Quickcard, get_ip,nil)
          if current_transaction.present?
            seq_tx = current_transaction
            transaction.update(
                status: "approved",
                seq_transaction_id: current_transaction.actions.first.id,
                tags: current_transaction.actions.first.tags,
                timestamp: current_transaction.timestamp
            )
          end
        elsif reciever.qc? || sender.qc?
          if !sender.merchant?
            transaction = create_transaction(@user,@request.wallet_id, @wallet.id, reciever, sender, transaction_amount, fee,@request.id)
            seq_tx = current_transaction[:seq_tx] if current_transaction[:seq_tx].present?
          end
          if sender.merchant?
            current_transaction = transaction_between(@request.wallet_id,@wallet.id ,transaction_amount, TypesEnumLib::TransactionType::InvoiceQC,0, data,nil,nil,TypesEnumLib::GatewayType::Quickcard)
            seq_tx = current_transaction.try(:[], :seq_tx) if current_transaction.try(:[], :seq_tx).present?
          elsif sender.iso? || sender.agent? || sender.affiliate?
            amount_sum, fee = deduct_b2b_fee(sender, transaction_amount)
            current_transaction = SequenceLib.direct_transfer(transaction_amount, @wallet.id, @request.wallet_id, TypesEnumLib::TransactionType::InvoiceQC, fee,  nil, TypesEnumLib::GatewayType::Quickcard, get_ip,nil)
            seq_tx = current_transaction
          else
            current_transaction = SequenceLib.direct_transfer(transaction_amount, @wallet.id, @request.wallet_id, TypesEnumLib::TransactionType::InvoiceQC, fee,  nil, TypesEnumLib::GatewayType::Quickcard, get_ip,nil)
            seq_tx = current_transaction
          end
          if !sender.merchant? && transaction.present? && current_transaction.present?
            transaction.update(seq_transaction_id: current_transaction.actions.first.id,
                               status: "approved",
                               tags: current_transaction.actions.first.tags,
                               timestamp: current_transaction.timestamp)
          end
        end
        if current_transaction.nil? || [current_transaction].flatten.size <= 0
          raise Exception.new "Sequence not responding please try again later or Contact App Administrator!"
        else
          #= creating block transaction
          parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          message = "#{reciever.name} has accepted your request for #{number_to_currency(transaction_amount)} ."
          twilio_text = send_text_message(message, sender.phone_number)
          push_notification(message, sender)
          # request = Request.new(description: description,amount: params[:amount], sender_id: reciever.id, sender_name: reciever.name, reciever_id: sender.id, reciever_name: sender.name, status: 'in_progress', wallet_id: wallet.id)
          @request.update(status: 'approved', from_wallet_id: @wallet.id)
          ActivityLog.log(nil,"Invoice Paid",current_user,params,response,"Paid Invoice #{@request.invoice_number} for $#{number_with_precision(@request.total_amount, precision: 2)}","invoices")
          render status: 200, json:{message: 'Transferred Successfully'}
        end
      else
        raise Exception.new "Something wrong happened !Please try again"
      end
    rescue Exception => exc
      render status: 404, json:{message: exc.message}
    end
  end

  def detail_modal
    @user= current_user
    @wallets=[]
    @request = Request.b2b.find(params[:invoice_id])
    @invoice= @request
    @notification = Notification.where(read_at: nil,notifiable_id: @request.id ,recipient_id: current_user.id, notifiable_type: "Request" )
    unless @notification.blank?
      @notification.last.update(read_at: Time.now)
      # ActionCable.server.broadcast "notification_channel_#{current_user.id}", channel_name: "notification_channel_#{current_user.id}" , message: 1
    end
    if (@user.merchant? && @user.merchant_id.nil?) || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
      @user.wallets.primary.each do |w|
        @wallets << w
      end
    elsif @user.qc?
      @wallets = @user.wallets.qc_support
    elsif @user.merchant_id.present?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    end
    @list=[]
    if @user.qc?
      @wallets.try(:each) do |wallet|
        @list << {
            wallet_id: wallet.id,
            wallet_name: wallet.name,
            balance: show_balance(wallet.id) - HoldInRear.calculate_pending(wallet.id),
            location_status: false
        }
      end
    else
      @wallets.each do|w|
        @location = w.location
        if @location.present?
          location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
                  location_status: location_status
              }
        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: false
              }
        end
      end
    end
    # @notifications = Notification.where(read_at: nil,notifiable_id: @request.id ,recipient_id: current_user.id, notifiable_type: "Request" )
    @notifications_count = Notification.where(read_at: nil,recipient_id: current_user.id, notifiable_type: "Request" ).count
    respond_to :js
  end

  def payee #invoices that merchant created
    @user = current_user
    params[:search_only] = nil if params[:page].present?
    if current_user.merchant_id.present?
      @main_merchant =  User.find current_user.try(:merchant_id)
      @users = @main_merchant.sub_merchants.active_users
      @users = @users.ids.push(@main_merchant.id)
    else
      @users = current_user.sub_merchants.active_users
      @users = @users.ids.push(current_user.id)
    end
    if params[:checkbox].present? && !params[:all].present?
      params[:q]={status_cont_any:params[:checkbox]}
      @q = Request.where(sender_id: @users,request_type: "invoice").ransack(params[:q].try(:merge, m: 'or'))
      @requests=@q.result().order(id: "desc")
    end
    if params[:search].present?
      params[:q]={}
      val=params[:search]
      params[:q]={total_amount_eq:val}
      params[:q][:invoice_number_or_reciever_name_cont]=val
      params[:q][:account_wallet_name_cont]=val
    end
    if (!params[:search].present? && !params[:checkbox].present?) || params[:all].present?
      @requests = Request.where(sender_id: @users, request_type: "invoice")
      time_zone = cookies[:timezone]
      Request.where(sender_id: @users, request_type: "invoice", status: "in_progress").where('due_date < ?', Time.now.in_time_zone(time_zone).utc).where.not(status: "due_date").update_all(status: "past_due")
    end
    @q = @requests.ransack(params[:q].try(:merge, m: 'or'))
    @requests=@q.result().order(id: "desc").per_page_kaminari(params[:page]).per(@per_page)
    if params[:search_only] == "true"
      render :partial => 'v2/merchant/invoices/partials/datatable',locals: {requests: @requests}
    end
  end

  def reopen #merchant can reopen invoice and send it again
    if params[:id].present?
      @user = current_user
      @request = Request.eager_load(:addresses).where(sender_id: @user.id, id: params[:id],request_type: "invoice").first
      raise StandardError.new "Invoice not found!" if @request.blank?
      @receiver = @request.reciever
      @invoice_number = @request.invoice_number
      @sender = @request.sender
      @products = @request.products
      @billing = @request.addresses.billing.first
      @shipping = @request.addresses.shipping.first
      @billing_states = CS.states(@billing.country) if @billing.present?
      @shipping_states = CS.states(@shipping.country) if @shipping.present?
      @view_only = true if params[:type] == "resend"
    else
      raise StandardError.new "Invoice not found!"
    end
    @list = load_wallets[:list]
    render :template => "v2/merchant/invoices/recurring_billing"
  end

  def edit #merchant can edit invoice and send it again
    if params[:id].present?
      @user = current_user
      @request = Request.eager_load(:addresses).where(sender_id: @user.id, id: params[:id],request_type: "invoice").first
      raise StandardError.new "Invoice not found!" if @request.blank?
      @receiver = @request.reciever
      @invoice_number = @request.invoice_number
      @sender = @request.sender
      @products = @request.products
      @billing = @request.addresses.billing.first
      @shipping = @request.addresses.shipping.first
      @billing_states = CS.states(@billing.country) if @billing.present?
      @shipping_states = CS.states(@shipping.country) if @shipping.present?
    else
      raise StandardError.new "Invoice not found!"
    end
    @list = load_wallets[:list]
    render :template => "v2/merchant/invoices/edit"
  end

  def cancel
    params[:redirect_url] = payee_v2_merchant_invoices_path
    if params[:id].present?
      @user = current_user
      if @user.merchant_id.present?
        request = Request.where(sender_id: @user.merchant_id, id: params[:id],request_type: "invoice").first
      else
        @merchant_user = @user.sub_merchants.pluck(:id)
        request = Request.where(sender_id: @merchant_user, id: params[:id],request_type: "invoice").first
      end
      request = Request.where(sender_id: @user.id, id: params[:id],request_type: "invoice").first if request.blank?
      raise StandardError.new "Invoice not found!" if request.blank?
      check_schedule(request)
      request.update(status: "cancel")
      flash[:notice] = "Invoice cancelled!"
      ActivityLog.log(nil,"Invoice Cancelled",current_user,params,response,"Cancelled Invoice #{request.invoice_number} for $#{number_with_precision(request.total_amount, precision: 2)}","invoices")
    else
      raise StandardError.new "Invoice not found!"
    end
    if params[:filter].present?
    redirect_to payee_v2_merchant_invoices_path(filter:params[:filter])
    else
    redirect_to payee_v2_merchant_invoices_path
    end
  end

  def view_invoice
    if current_user.merchant_id.present?
      @main_merchant =  User.find current_user.try(:merchant_id)
      @users = @main_merchant.sub_merchants.active_users
      @users = @users.ids.push(@main_merchant.id)
    else
      @users = current_user.sub_merchants.active_users
      @users = @users.ids.push(current_user.id)
    end
    @list = load_wallets[:list]
    @view_only = true
    params[:redirect_url] = payee_v2_merchant_invoices_path
    @user = current_user
    @request=Request.eager_load(:addresses).where(sender_id: @users, id: params[:id],request_type: "invoice").first
    raise StandardError.new "Invoice not found!" if @request.blank?
    @merchant=@request.sender
    @receiver=@request.reciever
    @products=@request.products
    @billing = @request.addresses.billing.first
    @shipping = @request.addresses.shipping.first
    @same_address = @request.checked_value_address
    @billing_states = CS.states(@billing.country) if @billing.present?
    @shipping_states = CS.states(@shipping.country) if @shipping.present?
    params['view']='true'
    payload = {request_id: @request.id}
    @download_invoice = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
    render :recurring_billing
  end

  def approve_invoice
    @user= current_user
    @request=Request.where(reciever_id: @user.id, id: params[:id],request_type: "invoice").first
    raise StandardError.new "Invoice not found!" if @request.blank?
    @list = load_wallets[:list]
    respond_to :js
  end

  def recurring_billing #invoice form
    @invoice_number = Digest::SHA1.hexdigest([Time.now, rand].join)[0,12].capitalize
    loop do
      break unless Request.exists?(invoice_number: @invoice_number)
      @invoice_number = Digest::SHA1.hexdigest([Time.now, rand].join)[0,12].capitalize
    end
    wallets = load_wallets
    @list = wallets[:list]
    if @list.size == 1 && wallets[:location_status]
      flash[:danger] = I18n.t("errors.withdrawl.blocked_location")
    end
  end

  def recurring_billing_post
    ActiveRecord::Base.transaction do
      params[:phone_number] = params[:phone_code] + params[:phone_number]
      @user = current_user
      user = set_user
      raise StandardError.new "Invoice Payee not found!" if user.blank?
      if params[:request_id].present?
        request = Request.where(sender_id: @user.id,id:params[:request_id]).first
        check_schedule(request)
      else
        raise StandardError.new "Invoice ID already exist!" if Request.exists?(invoice_number: params[:invoice_id])
        request = Request.new
      end
      total_amount = calculation_for_fees(params)
      if params["payment_option"].try(:[],"card") == "on"
        if @location.transaction_limit?
          amounts = []
          unless @location.transaction_limit_offset.include? total_amount
            params[:location_id] = @location.id
            amounts = @location.transaction_limit_offset.map{|a| "#{number_with_precision(number_to_currency(a))}"} if @location.transaction_limit_offset.present?
            raise StandardError.new  I18n.t('api.registrations.account_limit_true', amount_limit: amounts.join(", "))
          end
        end
      end
      request = setting_for_request(request,params,user)
      products = setting_products(params,request)
      billing_address = address(params[:billing], user, "billing", request.id)
      shipping_address = address(params[:shipping], user, "shipping", request.id)
      user.addresses << shipping_address unless params[:commit].blank?
      user.addresses << billing_address unless params[:commit].blank?
      generate_pdf(request,user,products, billing_address, shipping_address) if params[:commit]!='Save Draft' || params[:schedule_date].present?
    end
    flash[:notice] = "Created Successfully!" unless params[:commit].blank?
    redirect_to payee_v2_merchant_invoices_path(filter:params[:filter]) unless params[:commit].blank?
  end

  def customer_search
    if current_user.merchant_id.present?
      merchant_id = current_user.merchant_id
    else
      merchant_id = current_user.id
    end
    if params[:search_with] == "phone_number"
      users = User.user.where("users.phone_number ILIKE ?","%#{params[:query].strip}%").select(:id, :email, :phone_number, :name).limit(15)
    else
      users = User.user.joins(:merchant_customers).where('customer_merchants.merchant_id = ?',merchant_id).where("users.name ILIKE ?","%#{params[:query].strip}%").select(:id, :email, :phone_number, :name).limit(15).uniq
    end
    render status: 200, json: {users: users}
  end

  def customer_address
    user = User.find_by(id: params[:id])
    billing = user.addresses.billing.last
    shipping = user.addresses.shipping.last
    render status: 200, json: {billing: billing, shipping: shipping}
  end

  def check_phone_number
    valid = Phonelib.valid?(params[:phone_number])
    if valid == true
      render status: 200, json: {valid: true}
    else
      render status: 200, json: {valid: false}
    end

  end

  def generate_pdf(request,user,products, billing, shipping)
    @request = request
    @merchant = current_user
    @shipping = shipping
    @billing = billing
    @user = user
    @location = Wallet.find_by(id: @request.wallet_id).try(:location)
    @customer_name = params[:name]
    @products=products
    @active_option = @request.payment_option
    template = @active_option.values.select{|a| a}.count
    if @request.apply_tax == true
      tax_on_SH = (@request.tax.to_f / 100) * @request.shipping_handling_fee.to_f
      @taxes = @request.tax_method == "percent" ? ((@request.tax.to_f / 100) * @request.amount.to_f) + tax_on_SH.to_f  : @request.tax
    else
      @taxes = (@request.tax.to_f / 100) * @request.amount.to_f
    end
    if @schedule_date.present? && @schedule_date > @current_time
      Sidekiq::Cron::Job.create(name: "Invoice#{@request.id}", cron: "#{@schedule_date.min} #{@schedule_date.hour} #{@schedule_date.day} #{@schedule_date.month} *", class: InvoiceWorker)  unless params[:commit].blank?
      UserMailer.invoice_pdf_email("pdf",request.id).deliver_later if ( params[:commit].present? && params[:commit] != "Save Draft" )
    else
      UserMailer.invoice_pdf_email("pdf",request.id).deliver_later if ( params[:commit].present? && params[:commit] != "Save Draft" )
    end
    if params[:commit].blank?
      pdf = WickedPdf.new.pdf_from_string(render_to_string("user_mailer/invoice_pdf#{template}.html.erb", layout: false))
      send_data pdf, :filename => "invoice.pdf", :type => "application/pdf", :disposition => "attachment"
    end
  end

  def clients
    params[:search_only] = nil if params[:page].present?
    @user = current_user
    # @clients = User.joins(:recieve_money_requests).where('requests.request_type = ?',"invoice").where('requests.sender_id = ?',@user.id).order('requests.created_at').uniq
    condition = []
    if params[:search].present?
      if params[:search].include?("-") && params[:search].include?("(")
       params[:search] = params[:search].gsub /[" "()-]/, ""
      end
      condition = settings_search_params
    end
    if current_user.merchant? && current_user.merchant_id.nil?
    @clients = CustomerMerchant.eager_load(:merchant_customer).eager_load(:location_customer).where(condition).where(merchant_id: current_user.id).order(:created_at).per_page_kaminari(params[:page]).per(@per_page || 10)
    elsif current_user.merchant? && current_user.merchant_id.present?
      @clients = CustomerMerchant.eager_load(:merchant_customer).eager_load(:location_customer).where(condition).where(merchant_id: current_user.merchant_id).order(:created_at).per_page_kaminari(params[:page]).per(@per_page || 10)
    end
    # @clients = Kaminari.paginate_array(@clients).page(params[:page]).per(@per_page)
    if params[:search_only].present?
      render :partial => "v2/merchant/invoices/partials/clients_datatable", locals: {clients: @clients}
    end
  end

  def client_details
    params[:search_only] = nil if params[:page].present?
    @client = CustomerMerchant.find_by(id: params[:id])
    @customer = @client.merchant_customer
    @customer_wallet_id = @customer.wallets.first.id
    time = set_customer_detail_date
    if params[:search_only].present?
      @chargebacks = filter_chargebacks
      @chargebacks = Kaminari.paginate_array(@chargebacks).page(params[:page]).per(@per_page)
      render :partial => 'v2/merchant/shared/chargebacks_datatable',locals: {chargebacks: @chargebacks}
    else
      if params[:first_date]
        first_date = DateTime.parse(params[:first_date])
        second_date = DateTime.parse(params[:second_date])
        @all_transactions = Transaction.not_pending_or_refund(@client).created_between(first_date, second_date).order(created_at: :desc)
      else
        @all_transactions = Transaction.not_pending_or_refund(@client).order(created_at: :desc)
      end
      chargebacks = filter_chargebacks
      @chargebacks_amount = chargebacks.sum(&:amount)
      if params[:type] == "transaction"
        @transactions = Kaminari.paginate_array(@all_transactions).page(params[:page]).per(10)
        @chargebacks = Kaminari.paginate_array(chargebacks).page("1").per(@per_page)
      else
        @transactions = Kaminari.paginate_array(@all_transactions).page("1").per(10)
        @chargebacks = Kaminari.paginate_array(chargebacks).page(params[:page]).per(@per_page)
      end
      @invoices_amount = Request.where(wallet_id: @client.wallet_id, reciever_id: @client.customer_id).where(time).sum(&:total_amount)
    end
    render partial: 'v2/merchant/shared/transaction_datatable', locals: {transactions: @transactions} if params[:first_date]
  end

  #Functions for new invoicing system
  def ach_payment # pay invoice with ach
  end

  def quickcard_payment # pay invoice with quickcard
    begin
      if params[:request_url].present?
        decode_token = JWT.decode params[:request_url], Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
        decode_token = decode_token.first
        params[:invoice_id]=decode_token.try(:[],'invoice_id')
      end
      @invoice_id = params[:invoice_id]
      @request = Request.in_progress.find_by_id(@invoice_id)
      raise StandardError.new "Pending invoice not found!" if @request.blank?

      @receiver = @request.reciever
      @sender = @request.sender
      @wallet = Wallet.find_by(id: @request.wallet_id)
      @location = @wallet.location
      @products = @request.products
      if current_user.id != @sender.id
        wallets = load_wallets
        @list = wallets[:list]
        if @list.size == 1 && wallets[:location_status]
          flash[:danger] = I18n.t("errors.withdrawl.blocked_location")
        end
      else
        raise StandardError.new "You can't pay!"
      end
    rescue StandardError => exc
      flash[:notice] = exc.message == "Signature has expired" ? "Due Date has passed" : exc.message
      redirect_to payee_v2_merchant_invoices_path
    end
  end


  private

  def set_schedule_date
    time_zone = cookies[:timezone]
    @offset = Time.now.in_time_zone(time_zone).utc_offset
    params[:due_date] = Date.strptime(params[:due_date], "%m/%d/%Y").end_of_day.to_datetime
    due_date_utc_time = params[:due_date]
    params[:due_date] = Time.new("#{due_date_utc_time.year}", "#{due_date_utc_time.month}", "#{due_date_utc_time.day}", "#{due_date_utc_time.hour}","#{due_date_utc_time.min}",00, @offset).utc
    if params[:schedule_date].present?
      params[:schedule_date] = Date.strptime(params[:schedule_date], "%m/%d/%Y").end_of_day.to_datetime
      utc_time = params[:schedule_date]
      @schedule_date = Time.new("#{utc_time.year}", "#{utc_time.month}", "#{utc_time.day}", "#{utc_time.hour}","#{utc_time.min}",00, @offset).utc
    end
    current_time = Date.today.end_of_day.to_datetime
    @current_time = Time.new("#{current_time.year}", "#{current_time.month}", "#{current_time.day}", "#{current_time.hour}","#{current_time.min}",00, @offset).utc
    # SlackService.notify("due_date: #{params[:due_date]} ...... schedule_date: #{@schedule_date}...  Time.now.end_of_day.utc: #{@current_time} .... Test_time: #{Time.now.in_time_zone(time_zone).utc}","#qc-files")
  end

  def user_name(id)
    user = User.find_by_id(id) if id.present?
    if user.present?
      return user.name
    else
      return ''
    end
  end

  def create_transaction(user, to, from, reciever, sender, amount, fee,request_id)
    sender_dba=get_business_name(Wallet.find_by(id:from))
    receiver_dba=get_business_name(Wallet.find_by(id:to))
    user.transactions.create(to: to,
                             from: from,
                             status: "pending",
                             action: 'transfer',
                             sender: reciever,
                             sender_name: sender_dba,
                             receiver: sender,
                             receiver_name: receiver_dba,
                             sender_wallet_id: from,
                             receiver_wallet_id: to,
                             amount: amount.to_f,
                             fee: fee,
                             main_type: updated_type(TypesEnumLib::TransactionType::InvoiceQC),
                             sub_type: @request.invoice? ? "invoice" : "",
                             ip: get_ip,
                             net_fee: fee,
                             net_amount: amount.to_f - fee.to_f,
                             total_amount: amount.to_f,
                             invoice_id: request_id
    )
  end

end