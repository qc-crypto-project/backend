class V2::Merchant::BulkChecksController < V2::Merchant::BaseController
	require 'csv'
  	include ApplicationHelper
  	include UsersHelper
  	include V2::Merchant::ChecksHelper

  	skip_before_action :require_merchant , only: [:policy, :bulk_check_detail, :destroy, :show, :load_updated_checks,:get_bank_by_routing_number]
  	skip_before_action  :set_wallet_balance, only: [:update_check_form, :policy, :bulk_check_detail, :get_fee_sum, :destroy, :show, :load_updated_checks,:debit_card_deposit_form,:create_bulk_checks, :update_single_bulk_check,:get_bank_by_routing_number, :check_form, :create_check]
  	skip_before_action :authenticate_user!, only: [:policy]
  	before_action :validate_location, only: [:create]
  	before_action :set_cache_headers, only: [:debit_card_deposit]
	def index

		@q = BulkCheckInstance.ransack(params[:q])

		if params[:custom].present?
        	params[:first_date] = DateTime.parse(params[:first_date])
        	params[:second_date] = DateTime.parse(params[:second_date])
    	end
      
		@total_paid = @total_void = @total_pending = @total_failed = number_with_precision(0, :precision => 2, :delimiter => ',')
		@total_bulk_checks = current_user.bulk_check_instances.where.not(wallet: nil)
		params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )
		if params[:q].present?
			params[:q]["user_wallet_name_cont"] = params[:q][:data_search]
	    	params[:q]["id_eq"] = params[:q][:data_search]
	    	params[:q]["total_amount_eq"] = params[:q][:data_search]
	    end
		page_size = params[:filter].present? ? params[:filter].to_i : 10
		@filters = [10,25,50,100]

		@total_bulk_checks = @total_bulk_checks.where("Date(bulk_check_instances.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?

	    @total_bulk_checks = @total_bulk_checks.where(id: BulkCheckInstance.search_by_status(params["search"])).ransack(params[:q].try(:merge,m: 'or')).result.order(id: "desc")
	    @bulks = Kaminari.paginate_array(@total_bulk_checks).page(params[:page]).per(page_size)
	    #@bulks = @bulks.paginate(:page => params[:page], :per_page => 10)
	    @checkIssueEnabled = false
	    checkConfig = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
	    @checkIssueEnabled = checkConfig.boolValue unless checkConfig.blank?
	    if current_user.present? && !current_user.merchant_id.nil? && current_user.submerchant_type == "admin_user"
	      parent_user = User.find(current_user.merchant_id)
	      if parent_user.oauth_apps.present?
	        oauth_app = parent_user.oauth_apps.first
	        @checkIssueEnabled = false if oauth_app.is_block && parent_user.get_locations.try(:first).try(:block_withdrawal)
	      end
	    else
	      if @user.oauth_apps.present?
	        oauth_app = @user.oauth_apps.first
	        @checkIssueEnabled = false if oauth_app.is_block && @user.get_locations.try(:first).try(:block_withdrawal)
	      end
	    end
	    total_bulk_tango = @total_bulk_checks.joins(:tango_orders)
	    if total_bulk_tango.present? 
		    @total_paid = total_bulk_tango.where("tango_orders.status = ?","PAID").distinct.sum(:total_amount)
		    @total_paid =  number_with_precision(@total_paid, precision: 2, delimiter: ',') 
			@total_pending = total_bulk_tango.where("tango_orders.status = ?","PENDING").distinct.sum(:total_amount)
			@total_pending =  number_with_precision(@total_pending, precision: 2, delimiter: ',') 
			@total_void = total_bulk_tango.where("tango_orders.status = ?","VOID").distinct.sum(:total_amount)
			@total_void =  number_with_precision(@total_void, precision: 2, delimiter: ',') 
			@total_failed = total_bulk_tango.where("tango_orders.status = ?","FAILED").distinct.sum(:total_amount)
			@total_failed =  number_with_precision(@total_failed, precision: 2, delimiter: ',') 
		end

	    render partial: 'v2/merchant/bulk_checks/bulk_checks_datatable' if request.xhr?
	end
	def new
		render partial: "new"
	end

	def edit
		@check = BulkCheck.find(params[:id])
		render partial: "edit"
	end

	def update
		@check = BulkCheck.find(params[:id])
	    @wallet_id = params[:wallet_id]
	    @check.update(name: params[:name],
	                  last_name: params[:last_name],
	                  phone: params[:phone],
	                  email: params[:email],
	                  amount: params[:amount].to_f,
	                  routing_number: params[:routing_number],
	                  account_number: params[:account_number],
	                  confirm_account: params[:confirm_account])
	    @checks = @check.bulk_check_instance.bulk_checks
	    @bulk_count = @checks.count
	    a = @checks.map {|x| x.amount.to_f}
	    @total_amount = a.sum
	    #
	    @wallets = current_user.wallets.primary if current_user.MERCHANT?
	    # @inst = @check.bulk_check_instance
	    @check.bulk_check_instance.update(total_amount: @total_amount.to_f)
	    @list=[]
	    @wallets.each do|w|
	      @list << {
	          wallet_id: w.id,
	          wallet_name: w.name,
	          balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
	          fee: get_location_fee_for_bulk_checks(@total_amount,w, @bulk_count)
	      }
	    end
	    selected_wallet_data = @list.select{ |v| v[:wallet_id] == @wallet_id.to_i}
	    render json: {"check" => @check , "selected_wallet_data" => selected_wallet_data , "total_amount" => @total_amount}
	end

	def show
		@q = TangoOrder.ransack(params[:q])
		page_size = params[:filter].present? ? params[:filter].to_i : 10

		@filters = [10,25,50,100]
		@wallets = current_user.wallets.primary if current_user.MERCHANT?
		params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )
    	# params[:q] = eval(params[:q]) if params[:q].present? && ( params[:q].is_a? String ) 
	    if params[:q].present?
	      # params[:q]["total_fee_eq"] = params[:q][:data_search] 
	      params[:q]["name_or_recipient_or_wallet_name_cont"] = params[:q]["data_search"] 
	      params[:q]["id_eq"] = params[:q]["data_search"] if params[:q].present?
	      params[:q]["amount_eq"] = params[:q]["data_search"] if params[:q].present?
	      params[:q]["actual_amount_eq"] = params[:q]["data_search"] if params[:q].present?
			end

		@bulk_check = BulkCheckInstance.find(params[:id])
	    @checks = @bulk_check.bulk_checks.where(id: BulkCheck.search_by_status(params["search"]))
	    
	    @total_tango_checks = TangoOrder.joins(:bulk_check).where("bulk_checks.id IN (?)",@checks.ids).ransack(params[:q].try(:merge, m: 'or')).result
	    @tango_checks = @total_tango_checks.per_page_kaminari(params[:page]).per(page_size)

	    @wallet = Wallet.find_by_id(@bulk_check.wallet)
	    @bulk_check_wallet_name = @wallet.try(:name)
	    @total_checks_amount = @total_amount = @total_fee_amount =   number_with_precision(0, :precision => 2, :delimiter => ',')
	    @total_checks_count = 0
	    if  request.format.html?
	    	@balance = number_with_precision((show_balance(@wallet.id) - HoldInRear.calculate_pending(@wallet.id)).to_f, :precision => 2, :delimiter => ',')
	    end
	    if @total_tango_checks.present?
	    	@total_checks_amount = @total_tango_checks.PENDING.map {|x| x.actual_amount.to_f}
	    	@total_checks_amount = number_with_precision(@total_checks_amount.try(:sum, &:to_f), :precision => 2, :delimiter => ',')
	    	@total_amount =  number_with_precision(get_location_fee_for_bulk_checks(@total_checks_amount,@wallet, @total_tango_checks.PENDING.count).to_f + @total_checks_amount.to_f, :precision => 2, :delimiter => ',') 
	    	    	
	    	@total_fee_amount  = get_location_fee_for_bulk_checks(@total_checks_amount,@wallet, @total_tango_checks.PENDING.count)

	    	@total_checks_count = @total_tango_checks.count
	    end
	    render partial: 'v2/merchant/bulk_checks/bulk_check_show_datatable' if request.xhr?
	end

	def destroy
	    @bulk_check = BulkCheck.find params[:id]
	    @check = @bulk_check
	    @wallet_id = params[:wallet_id]
	    @bulk_check.destroy
	    @checks = @bulk_check.bulk_check_instance.bulk_checks

	    @bulk_count = @checks.count
	    a = @checks.map {|x| x.amount.to_f}
	    @total_amount = a.sum

	    @wallets = current_user.wallets.primary if current_user.MERCHANT?
	    @check.bulk_check_instance.update(total_amount: @total_amount.to_f)
	    @list=[]
	    @wallets.each do|w|
	      @list << {
	          wallet_id: w.id,
	          wallet_name: w.name,
	          balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
	          fee: get_location_fee_for_bulk_checks(@total_amount,w, @bulk_count)
	      }
	    end

	    selected_wallet_data = @list.select{ |v| v[:wallet_id] == @wallet_id.to_i}
	    render json: {"selected_wallet_data" => selected_wallet_data , "total_amount" => @total_amount}
  	end

	def create
    # BulkcheckWorker.perform_async(params)
	    Thread.new do
	      bulk_check = BulkCheckInstance.find(params[:bulk_instance_id].to_i)
	      bulk_check.update(start_process: true)
	      checks = bulk_check.bulk_checks
	      bulk_check.update(wallet: params[:wallet_id].to_i)
	      checks.each do |check|
	        create_single_check_from_bulk(check, params[:wallet_id].to_i)
	      end
	        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
	      withdraw_checks = checks.where(is_processed: true, status: "PENDING", sent: false)
	      if withdraw_checks.present?
	        withdraw_amount = withdraw_checks.pluck(:amount).sum
	        wallet = Wallet.find(params[:wallet_id].to_i)
	        location = wallet.location
	        fee_object = location.fees.buy_rate.first if location.present?
	        check_fee = withdraw_checks.count * fee_object.send_check.to_f if fee_object.present?
	        fee_perc = deduct_fee(fee_object.redeem_fee.to_f, withdraw_amount)
	        fee_sum = check_fee + fee_perc.to_f
	        fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
	        fee = fee_class.apply(withdraw_amount.to_f, TypesEnumLib::CommissionType::SendCheck, withdraw_checks.count)
	        user_info = fee["splits"]
	        send_check_info = {
	            bulk_checks: {
	                checks_ids: withdraw_checks.pluck(:id),
	                bulk_check_instance: bulk_check.id,
	                total_checks: checks.count
	            }
	        }
	        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize' 
      
	        location_info = location_to_parse(wallet.location) if wallet.location.present?
	        merchant_info = merchant_to_parse(wallet.location.merchant) if wallet.location.present?
	        tags = {
	            "fee_perc" => user_info,
	            "location" => location_info,
	            "merchant" => merchant_info,
	            "send_check_user_info" => send_check_info
	        }
	        transaction = @user.transactions.create(
	            status: "pending",
	            amount: withdraw_amount,
	            sender: @user,
	            sender_wallet_id: wallet.id,
	            sender_name: @user.try(:name),
	            action: 'retire',
	            fee: fee_sum.to_f,
	            net_amount: withdraw_amount.to_f - fee_sum.to_f,
	            net_fee: fee_sum.to_f,
	            total_amount: withdraw_amount.to_f + fee_sum.to_f,
	            gbox_fee: user_info["gbox"]["amount"].to_f,
	            iso_fee: user_info["iso"]["amount"].to_f,
	            agent_fee: user_info["agent"]["amount"].to_f,
	            affiliate_fee: user_info["affiliate"]["amount"].to_f,
	            ip: get_ip,
	            main_type: updated_type(TypesEnumLib::TransactionType::BulkCheck),
	            tags: tags
	        )
	        withdraw = withdraw(withdraw_amount,wallet.id, fee_sum, TypesEnumLib::TransactionType::BulkCheck,nil,nil,user_info, send_check_info, @user.ledger,nil,TypesEnumLib::GatewayType::Checkbook)
	        if withdraw.present?
	          #= creating block transaction
	          parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
	          save_block_trans(parsed_transactions) if parsed_transactions.present?
	          transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
	          withdraw_checks.each do |check|
	            check.update(sent: true)
	          end
	        end
	         ActivityLog.log(nil,"Bulk Checks Created",current_user,params,response,"Bulk Checks Created [##{bulk_check.id}] for [$#{number_with_precision(bulk_check.total_amount, precision: 2)}]","bulk_checks")            
     
	        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [bulk_check.id]   
      
	      end
	    end
  	end

  	def load_updated_checks
	    if params[:bulk_instance_id].nil?
	      @checks = current_user.bulk_check_instances.last.bulk_checks
	      @inst_id = current_user.bulk_check_instances.last
	    else
	      @checks = BulkCheckInstance.find(params[:bulk_instance_id]).bulk_checks
	      @inst_id = BulkCheckInstance.find(params[:bulk_instance_id])
	    end
	    @in_pending = @checks.where(is_processed: true, status: "PENDING").count
	    @processed = @checks.where(is_processed: true, status: "Success").count
	    @all_processed = @checks.where(is_processed: true).count
	    respond_to do |format|
	      format.js {
	        render 'merchant/checks/show_bulk_checks.js.erb'
	      }
	    end
  end



	def import_checks
		page_size = params[:filter].present? ? params[:filter].to_i : 10
		@filters = [10,25,50,100]
	    file = params[:file]
	    parent = BulkCheckInstance.create! bulk_type: "bulk_checks", user_id: current_user.id
	    parent_id = parent.id
	    @inst_id = parent_id
	    @inst = parent
	    @valid_csv = true
	    CSV.foreach(file.path, headers: true) do |row|
	      flag = validate_checks_bulk_data?(row)
	      unless flag
	        flash[:error] = 'CSV File Contain Invalid Check(\'s).Please enter Valid Information!'
	        @valid_csv = false
	        redirect_back(fallback_location: root_path)
	        break
	      end
	    end

	    if @valid_csv
	      CSV.foreach(file.path, headers: true) do |row|
	        rec = BulkCheck.new(name: row[0], last_name: row[1], phone: row[2], email: row[3], routing_number: row[4], account_number: row[5], confirm_account: row[6], amount: row[7].to_f, bulk_check_instance_id: parent_id )
	        rec.save!
	      end
	    end

	    @wallets = current_user.wallets.primary if current_user.MERCHANT?
	    @checks = BulkCheckInstance.find(parent_id).bulk_checks
	    @total_checks = @checks
	    @total_checks_count = @total_checks.count
	    @checks = Kaminari.paginate_array(@checks).page(params[:page]).per(page_size)
	    a = @total_checks.map {|x| x.amount.to_f}
	    @total_amount = number_with_precision(a.try(:sum, &:to_f), :precision => 2, :delimiter => ',').to_f
	    parent.update(total_amount: @total_amount.to_f)
	    @bulk_check_instance = parent
	    @list=[]
	    @wallets.each do|w|
	    	block_check = w.location.block_withdrawal == true ? true : false
	      @list << {
	          wallet_id: w.id,
	          wallet_name: w.name,
	          balance:  number_with_precision((show_balance(w.id) - HoldInRear.calculate_pending(w.id)).to_f, :precision => 2, :delimiter => ','),
	          fee:  get_location_fee_for_bulk_checks(@total_amount,w, @total_checks.count),
	          fee_and_total: number_with_precision(get_location_fee_for_bulk_checks(@total_amount,w, @total_checks.count).to_f + @total_amount.to_f, :precision => 2, :delimiter => ','),
	          block_check: block_check
	      }
	    end			
  	end

  	def before_send_index

  		page_size = params[:filter].present? ? params[:filter].to_i : 10
		@filters = [10,25,50,100]
	    parent = BulkCheckInstance.find(params[:id])


	    parent_id = parent.id
	    @inst_id = parent_id
	    @inst = parent
	    @valid_csv = true
	    params[:q] = {name_or_last_name_or_email_or_account_number_or_phone_cont: params[:data_search]}
	    params[:q]["amount_eq"] = params[:data_search] if params[:q].present?
	    @checks = BulkCheckInstance.find(parent.id).bulk_checks.where(id: BulkCheck.before_search_by_status(params[:search])).ransack(params[:q].try(:merge, m: 'or')).result
	   	@checks = Kaminari.paginate_array(@checks).page(params[:page]).per(page_size)
	    @bulk_check_instance = parent
	    render partial: "before_send_bulkcheck_datatable"  if request.xhr?
  	end

  	private

	  def create_single_check_from_bulk(bulk_check, wallet_id)
	    #creating bulk checks for both direct and digital checks
	    if bulk_check.account_number.present? && bulk_check.routing_number.present?
	      dd = 1
	    else
	      dd = 0
	    end
	    amount = bulk_check.amount.to_f
	    recipient = bulk_check.email
	    name = bulk_check.name+" "+bulk_check.last_name
	    description = "Bulk Check"

	    if wallet_id.present?
	      begin
	        @user = current_user
	        @wallet = Wallet.find(wallet_id)
	        balance = show_balance(@wallet.id, @user.ledger).to_f
	        if @user.present? && @wallet.present?
	          location = @wallet.location
	          amount_sum = 0
	          location_fee = location.fees.buy_rate.first if location.present?
	          redeem_fee = location_fee.redeem_fee if location.present?
	          check_fee = location_fee.send_check.to_f if location_fee.present?
	          fee_perc = deduct_fee(redeem_fee.to_f, amount)
	          amount_sum = amount + check_fee + fee_perc.to_f
	          text = "Insufficient balance" if  balance < amount_sum
	          check = TangoOrder.new
	          check.name = name
	          check.recipient = recipient
	          check.description = description

	          if @user.merchant_id.present? && @user.try(:permission).admin?
	            user_id = @user.merchant_id
	          else
	            user_id = @user.id
	          end
	          check.user_id = user_id
	          if dd.present? && dd.to_i == 1
	            card_info =  {
	                :account_number => bulk_check.account_number,
	                :account_type => "CHECKING"
	            }
	            card_info = card_info.to_json
	            encrypted_card_info = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{@wallet.id}}", card_info)
	            check.account_token = encrypted_card_info
	            check.account_number = bulk_check.account_number.last(4)
	            check.account_type = "CHECKING"
	            check.routing_number = bulk_check.routing_number
	            check.amount = amount
	            check.actual_amount = amount
	            check.send_via = "Direct Deposit"
	          else
	            check.amount = amount_sum
	            check.actual_amount = amount
	            check.check_fee = check_fee
	            check.fee_perc = fee_perc
	            check.send_via = "email"
	          end
	          check.status = "PENDING"
	          check.order_type = 3
	          check.settled = false
	          check.wallet_id = @wallet.id
	          check.approved = false
	          check.save!
	          bulk_check.update(tango_order_id: check.id)
	          text = "PENDING"
	        else
	          text = "Something Wrong"
	        end
	      rescue => ex
	        p "-----------EXCEPTION HANDLED: #{ex.message}"
	        text = "Something Wrong"
	      end
	    else
	      text = "Try again"
	    end
	    bulk_check.update(status: text, is_processed: true)
	    # if !response.nil?
	    #   bulk_check.update(status: text, response_message: response.body, is_processed: true)
	    # else
	    #   bulk_check.update(status: text, response_message: response, is_processed: true)
	    # end
	  end

	  def get_location_fee_for_bulk_checks(amount, wallet, checks = nil)
	    location = wallet.location
	    location_fee = location.fees.buy_rate.first if location.present?
	    redeem_fee = location_fee.redeem_fee.to_f if location_fee.present?
	    check_fee = location_fee.send_check.to_f if location_fee.present?
	    check_fee = check_fee * checks if check_fee.present?
	    fee_perc = deduct_fee(redeem_fee.to_f, amount)
	    amount_sum = check_fee + fee_perc.to_f
	    number_with_precision(amount_sum, :precision => 2, :delimiter => ',')
	  end

	  def validate_checks_bulk_data?(data)
	    valid_phone_number = validate_number(data[2])
	    valid_email = validate_eamil(data[3])
	    valid_route_number = validate_number(data[4])
	    valid_account_number = validate_number(data[5])
	    valid_confirm_account_number = validate_number(data[6])
	    valid_amount = validate_amount(data[7])
	    valid_phone_number && valid_email && valid_route_number && valid_account_number && valid_confirm_account_number && valid_amount
	  end

	  def validate_eamil(email_obj)
	    valid_email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	    !!(email_obj =~ valid_email_regex)
	  end

	  def validate_number(number_obj)
	    number_obj !~ /\D/
	  end

	  def validate_amount(amount_obj)
	    valid_amount_regex = /\A[+-]?\d+(\.[\d]+)?\z/
	    !!(amount_obj =~ valid_amount_regex)
	  end


end