class V2::Merchant::ChargebacksController < V2::Merchant::BaseController
  include TransactionCharge::ChargeATransactionHelper
  include V2::Merchant::ChargebacksHelper

  def index
    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @per_page = params[:filter].present? ? params[:filter].to_i : 10
    params[:search_only] = nil if params[:page].present?
    if params[:custom].present? || params[:first_date].present?
      params[:first_date] = DateTime.parse(params[:first_date])
      params[:second_date] = DateTime.parse(params[:second_date])
      date = parse_date("#{params[:first_date].strftime("%m/%d/%Y")} - #{params[:second_date].strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,00, params[:offset]).utc
      }
    end
    if @user.merchant? && @user.merchant_id.nil?
      @wallets = current_user.wallets.primary.order('id ASC')
    elsif @user.merchant_id.present?
      @wallets = @user.wallets.primary.where.not(location_id: nil).order('id ASC')
    end
    @wallet_ids = @wallets.pluck(:id)
    @date = date
    @chargebacks=DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(Date.today.beginning_of_month,Date.today.end_of_day).group_by(&:merchant_wallet_id)
    @chargebacks=DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(date[:first_date],date[:second_date]).group_by(&:merchant_wallet_id)  if params[:first_date].present? && params[:second_date].present?
    unless params[:search_only].present?
      @dispute_case=DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(Date.today.beginning_of_month,Date.today.end_of_day).order(created_at: :desc)
      @dispute_case=DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(date[:first_date],date[:second_date]).order(created_at: :desc)  if params[:first_date].present? && params[:second_date].present?
      @sent_pending=@dispute_case.sent_pending.count
      @lost= @dispute_case.lost.count
      @under_review= @dispute_case.under_review.count
      @won_reversed= @dispute_case.won_reversed.count
      @dispute_accepted= @dispute_case.dispute_accepted.count
      total_transactions = Transaction.where(receiver_wallet_id: @wallet_ids,main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionType::Transfer3DS], status: ["approved", "refunded", "complete"]).where('transactions.created_at > ?','01/01/2020'.to_datetime).count
      disputes_count = @dispute_case.count
      if disputes_count > 0 && total_transactions > 0
        @cbk_percentage=number_with_precision(((disputes_count.to_f/total_transactions.to_f)*100).to_f,precision: 2)
      else
        @cbk_percentage = "0.00"
      end
    end
    render partial: "v2/merchant/chargebacks/chargeback_widget" if request.xhr?
    # render :partial => 'v2/merchant/shared/merchant_chargebacks',locals: {chargebacks: @chargebacks} if request.xhr?
  end

  def account_chargeback
    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @per_page = params[:filter].present? ? params[:filter].to_i : 10
    # params[:search_only] = nil if params[:page].present?
    if params[:custom].present? || params[:first_date].present? || params[:custom_filter].present?
      if params[:custom_filter].present?
        custom_filter = JSON params[:custom_filter]
        date = parse_filter_date(custom_filter["first_date"], custom_filter["second_date"])
      else
        date = parse_filter_date(params[:first_date], params[:second_date])
      end
    end

    @wallet_ids = params[:wallet_id]
    @wallet=Wallet.where(id:params[:wallet_id]).try(:first) if params[:wallet_id].present?
    if params[:q].present?
      q=params[:q]
      params[:q]={case_number_or_reason_title_or_merchant_wallet_location_business_name_cont:params[:q]}
      params[:q]["amount_eq"] = q
      @q = DisputeCase.where(merchant_wallet_id: @wallet_ids).ransack(params[:q].try(:merge, m: 'or'))
      @chargebacks=@q.result().order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    elsif params[:search_only].present? || params[:search].present?
      @chargebacks = filter_chargebacks
      # render :partial => 'v2/merchant/shared/chargebacks_datatable',locals: {chargebacks: @dispute_case}
    elsif params[:first_date].present? && params[:second_date].present?
      @chargebacks=DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(date[:first_date],date[:second_date]).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    else
      @chargebacks=DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(Date.today.beginning_of_month,Date.today.end_of_day).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    end
    if params[:search_only].present? || params[:search].present?
      chargebacks_data = @chargebacks
      @chargebacks = Kaminari.paginate_array(@chargebacks).page(params[:page]).per(@per_page)
    else
      chargebacks_data = DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(Date.today.beginning_of_month,Date.today.end_of_day).order(created_at: :desc)
      chargebacks_data = DisputeCase.where(merchant_wallet_id: @wallet_ids).created_between(date[:first_date],date[:second_date]).order(created_at: :desc)  if (params[:first_date].present? && params[:second_date].present?) || date.present?
      @chargebacks = chargebacks_data.per_page_kaminari(params[:page]).per(page_size)
    end
    if chargebacks_data.present?
      @sent_pending = chargebacks_data.sent_pending.count
      @lost = chargebacks_data.lost.count
      @under_review = chargebacks_data.under_review.count
      @won_reversed = chargebacks_data.won_reversed.count
      @dispute_accepted = chargebacks_data.dispute_accepted.count
      total_transactions = Transaction.where(receiver_wallet_id: @wallet_ids,main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionType::Transfer3DS], status: ["approved", "refunded", "complete"]).where('transactions.created_at > ?','01/01/2020'.to_datetime).count
      disputes_count = chargebacks_data.count
      if disputes_count > 0 && total_transactions > 0
        @cbk_percentage = number_with_precision(((disputes_count.to_f/total_transactions.to_f)*100).to_f,precision: 2)
      else
        @cbk_percentage = "N/A"
      end
    end
    if params[:search_only] == "true" || request.xhr?
      render partial: "v2/merchant/chargebacks/account_chargeback_widget" if request.xhr?
      # render :partial => 'v2/merchant/shared/chargebacks_datatable',locals: {variable: @chargebacks}
      # render partial: 'v2/merchant/shared/chargebacks_datatable'
    end
  end



  def show_details
    @chargebacks=DisputeCase.find_by(id:params[:dispute_id])
    @wallet=@chargebacks.merchant_wallet
    @transaction=@chargebacks.charge_transaction
    @products=@transaction.products
    @evidence=Evidence.new
  end

  def accept_dispute
    dispute=DisputeCase.find_by(id:params[:dispute_id])
    dispute_case_transaction = DisputeCaseTransaction.new
    if dispute.present?
      if dispute.charge_back? and (dispute.sent_pending?)
        dispute.status='dispute_accepted'
        dispute_case_transaction.transfer_charge_back_lost(dispute,request.try(:remote_ip),'merchant_request')
        dispute.save
        flash[:chargeback]="Chargeback accepted"
      else
        flash[:error]=I18n.t 'merchant.controller.successful_error'
      end
    end
    trans=Transaction.find_by(id:dispute.transaction_id)
    UserMailer.refund_cbk_email(dispute, trans).deliver_later
    UserMailer.cbk_lost_email(dispute, trans).deliver_later
    ActivityLog.log(nil,"Dispute Accepted",current_user,params,response,"Chargeback Accepted for Case# #{dispute.try(:case_number)} for #{number_with_precision(number_to_currency(dispute.try(:amount)))}","chargebacks")
    redirect_back(fallback_location: root_path)
  end

  def submit_evidence
    @evidence=Evidence.new(evidence_docs)
    @evidence.save
    if params[:evidence]['image'].present?
      keys=params[:evidence]['image'].keys
      images_count=keys.split('evidence').try(:last) if keys.present?
      keys.each do |v|
        if params[:evidence]['image'][v].present?
          params[:evidence]['image'][v].each do |i, img_array|
            img_array.each do |img|
              image={add_image:img,evidence_id:@evidence.id}
              upload_docs(image)
            end
          end
        end
      end
    end
    @dispute_case=DisputeCase.find_by(id:@evidence.try(:dispute_id)) if @evidence.present?
    @transaction=@dispute_case.try(:charge_back_transaction) if @dispute_case.present?
    UserMailer.admin_dispute_email(@dispute_case,@transaction.try(:sender_name),nil).deliver_later if @dispute_case.present?
    ActivityLog.log(nil,"Evidence Submitted",current_user,params,response,"Chargeback rejected for Case# #{@evidence.try(:dispute_case).try(:case_number)} for #{number_with_precision(number_to_currency(@evidence.try(:dispute_case).try(:amount)))}","chargebacks")
    redirect_back(fallback_location: root_path)
    # flash[:success]=I18n.t 'merchant.controller.successful_evidence'
  end

  def upload_docs(image)
    @img= Image.new(image)

    if @img.validate
      @img.save
    else
      @error = @img.errors.full_messages.first
    end
  end


  def verify_admin_password
    if current_user.valid_password?(params[:password])
      render status: 200, json:{success: 'Verified'}
    else
      render status: 404, json:{error: 'You are not Authorized for this action.'}
    end
  end

  def parse_filter_date(first_date, second_date)
    first_date = DateTime.parse(first_date)
    second_date = DateTime.parse(second_date)
    date = parse_date("#{first_date.strftime("%m/%d/%Y")} - #{second_date.strftime("%m/%d/%Y")}")
    date = {
        first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
        second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,00, params[:offset]).utc
    }
    return date
  end

  private
  def evidence_docs
    params.require(:evidence).permit(:document,:description,:dispute_id,:file_type)
  end
  def doc_params
    params.require(:image).permit(:add_image,:file_type,:evidence_id)
  end
end

