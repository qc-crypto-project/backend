class V2::Merchant::ChecksController < V2::Merchant::BaseController
  require 'csv'
  include ApplicationHelper
  include UsersHelper
  include V2::Merchant::ChecksHelper
  include WithdrawHandler
  skip_before_action :require_merchant , only: [:policy, :bulk_check_detail, :destroy, :show, :load_updated_checks,:get_bank_by_routing_number]
  skip_before_action  :set_wallet_balance, only: [:update_check_form, :policy, :bulk_check_detail, :get_fee_sum, :destroy, :show, :load_updated_checks,:debit_card_deposit_form,:create_bulk_checks, :update_single_bulk_check,:get_bank_by_routing_number, :check_form, :create_check]
  skip_before_action :authenticate_user!, only: [:policy]
  before_action :validate_location, only: [:create]
  before_action :set_cache_headers, only: [:debit_card_deposit]

  class CheckError < StandardError; end

  def index

    @q = TangoOrder.ransack(params[:q])

    if params[:custom].present?
        params[:first_date] = DateTime.parse(params[:first_date])
        params[:second_date] = DateTime.parse(params[:second_date])
    end
      
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @heading = "Checks"
    @check = Check.new
    @checkIssueEnabled = false
    checkConfig = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @checkIssueEnabled = checkConfig.boolValue unless checkConfig.blank?
    @total_paid = @total_void = @total_pending = @total_failed = @total_unpaid = @total_in_process  = @total_paid_wire = number_with_precision(0, :precision => 2, :delimiter => ',')
    params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )
    if params[:q].present?
      params[:q]["name_or_recipient_or_wallet_name_cont"] = params[:q]["data_search"] 
      params[:q]["total_fee_eq"] = params[:q][:data_search] if !params[:q][:data_search].is_a? String
      params[:q]["id_eq"] = params[:q]["data_search"] if params[:q].present?
      params[:q]["amount_eq"] = params[:q]["data_search"] if params[:q].present?
      params[:q]["actual_amount_eq"] = params[:q]["data_search"] if params[:q].present?
    end

    if current_user.present? && !current_user.merchant_id.nil?
      parent_user = User.find(current_user.merchant_id)
      user_ids=[parent_user.id,current_user.id]
      if params[:wallet_id].present?
        wallet_ids = params[:wallet_id]
      else
        wallet_ids = current_user.wallets.pluck(:id)
      end
      if parent_user.oauth_apps.present?
        oauth_app = parent_user.oauth_apps.first
        @checkIssueEnabled = false if oauth_app.is_block && parent_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
      end
      @total_checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids, wallet_id: wallet_ids)
      @total_checks = @total_checks.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
      @total_checks = @total_checks.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
      @checks = @total_checks.order(id: :desc)
   else
      if current_user.sub_merchants.present?
        sub_id = current_user.sub_merchants.pluck(:id)
        user_ids=[current_user.id,sub_id]
        user_ids= user_ids.flatten if user_ids.present?
      else
        user_ids=current_user.id
      end
      if params[:wallet_id].present?
        @total_checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids, wallet_id: params[:wallet_id])
      else
        @total_checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids)
      end
      @total_checks = @total_checks.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
      @total_checks = @total_checks.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
      @checks = @total_checks.order(id: :desc)
      if @user.oauth_apps.present?
        oauth_app = @user.oauth_apps.first
        @checkIssueEnabled = false if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
      end
    end
    @checks = @checks.per_page_kaminari(params[:page]).per(page_size)
    @wallets = []

    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    elsif !@user.merchant_id.nil?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.primary.each do |w|
          @wallets << w
        end
      end
    end

    if @total_checks.present? 
        @total_paid = @total_checks.PAID.sum(:amount)
        @total_paid =  number_with_precision(@total_paid, precision: 2, delimiter: ',') 
        @total_pending = @total_checks.PENDING.sum(:amount)
        @total_pending =  number_with_precision(@total_pending, precision: 2, delimiter: ',') 
        @total_void = @total_checks.VOID.sum(:amount)
        @total_void =  number_with_precision(@total_void, precision: 2, delimiter: ',') 
        @total_failed = @total_checks.FAILED.sum(:amount)
        @total_failed =  number_with_precision(@total_failed, precision: 2, delimiter: ',') 
        @total_unpaid = @total_checks.UNPAID.sum(:amount)
        @total_unpaid =  number_with_precision(@total_unpaid, precision: 2, delimiter: ',')
        # @total_in_process = @total_checks.IN_PROCESS.sum(:amount)
        # @total_in_process =  number_with_precision(@total_in_process, precision: 2, delimiter: ',')
        @total_paid_wire = @total_checks.PAID_WIRE.sum(:amount)
        @total_paid_wire =  number_with_precision(@total_paid_wire, precision: 2, delimiter: ',')
    end
    render partial: 'v2/merchant/checks/checks_datatable' if request.xhr?
  end

  def new
    flash = []
    @check =Check.new
    @user = current_user
    # @user = User.find(current_user.merchant_id) if current_user.merchant_id.present?
    if @user.merchant?
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:location_id)
        @locations = Location.where(id: wallets)
      else
        @locations = current_user.get_locations
      end
    else
      @wallets = @user.wallets.primary
    end
    render partial: "new"
  end


  def update
      begin
        if params[:id].present?
          check = TangoOrder.find(params[:id].to_i)
          user = check.user
          check_old_status = check.status
          if check_old_status == "VOID"
            flash[:success] = check.instant_ach? ? "Successfully Voided ACH" : check.instant_pay? ? "Successfully Voided Push to Card" : check.check? ? "Successfully Voided Check" : flash[:success] = "Successfully Voided"
            return ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]            
          end
          if check.present?
            check_escrow = Wallet.check_escrow.first
            escrow_user = check_escrow.try(:users).try(:first)
            if params[:status] == "VOID"
              check_info = {
                  name: check.name,
                  check_email: check.recipient,
                  check_id: check.checkId,
                  check_number: check.number,
                  notes: params[:notes]
              }
              if check.status == "PENDING"
                wallet = Wallet.find(check.wallet_id)
                if current_user.present? && current_user.role!='merchant'
                  location=nil
                  merchant = merchant_to_parse(wallet.users.first)
                else
                  location = location_to_parse(wallet.location)
                  merchant = merchant_to_parse(wallet.location.merchant)
                end
                tags = {
                    "location" => location,
                    "merchant" => merchant,
                    "send_check_user_info" => check_info,
                }
                ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
                # ach_check_type=check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard :  TypesEnumLib::TransactionType::VoidCheck
                fee_perc_details = {}
                user_info = {}
                if check.amount_escrow.present?
                  main_tx = check.try(:transaction)
                  fee_perc_details["fee_perc"] = main_tx.try(:[],'tags').try(:[],'fee_perc')
                  receiver_dba=get_business_name(wallet)
                  user_info["splits"] = main_tx.try(:[],'tags').try(:[],'fee_perc')
                  params[:type2] = "VOID"
                  ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
              
                  transaction = Transaction.create(
                      to: nil,
                      from: wallet.id,
                      status: "pending",
                      amount: check.actual_amount,
                      sender: escrow_user,
                      sender_name: escrow_user.try(:name),
                      receiver_wallet_id: wallet.id,
                      sender_wallet_id: check_escrow.try(:id),
                      receiver_id: current_user.try(:id),
                      sender_balance: SequenceLib.balance(wallet.id),
                      receiver_balance: SequenceLib.balance(check_escrow.try(:id)),
                      receiver_name: receiver_dba,
                      action: 'transfer',
                      net_amount: check.actual_amount.to_f,
                      total_amount: check.amount.to_f,
                      ip: get_ip,
                      main_type: ach_check_type,
                      tags: tags,
                      gbox_fee: main_tx.try(:gbox_fee),
                      iso_fee: main_tx.try(:gbox_fee),
                      affiliate_fee: main_tx.try(:gbox_fee),
                      agent_fee: main_tx.try(:gbox_fee),
                      fee: main_tx.try(:fee),
                      net_fee: main_tx.try(:net_fee)
                  )
                   ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
             
                  issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, 0,user_info, check_info,check) if @user.MERCHANT? || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
                  check.amount_escrow = false
                else
                  receiver_dba=get_business_name(wallet)
                   ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
             
                  transaction = user.transactions.create(
                      to: nil,
                      from: wallet.id,
                      status: "pending",
                      amount: check.actual_amount,
                      receiver_wallet_id: wallet.id,
                      receiver_name: receiver_dba,
                      receiver_balance: SequenceLib.balance(wallet.id),
                      receiver_id: current_user.try(:id),
                      action: 'issue',
                      net_amount: check.actual_amount.to_f,
                      total_amount: check.actual_amount.to_f,
                      ip: get_ip,
                      main_type: ach_check_type,
                      tags: tags
                  )  
                  ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
               
                  issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type, check.instant_ach? ? TypesEnumLib::GatewayType::Quickcard : TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info)
                end
            

               
                if issue.present?
                  transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags.merge(fee_perc_details), timestamp: issue["timestamp"])
                  #= creating block transaction
                  parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                  save_block_trans(parsed_transactions,"sub","void") if parsed_transactions.present?
                end
                check.void_transaction_id = transaction.try(:id)
                if !check.under_review?
                  check.status = params[:status]
                  check.approved = true
                  check.settled = true
                else
                  flash[:error] = "Can't Process Under Review #{check.instant_ach? ? 'Ach':'Check'}"
                end
                if check.save
                  check.instant_ach? ? flash[:success] = "Successfully Voided ACH" : check.instant_pay? ? flash[:success] = "Successfully Voided Push to Card" : flash[:success] = "Successfully Voided"
                else
                  flash[:error] = "Error Occurred"
                end
              elsif check.checkId.present?
                url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
                http = Net::HTTP.new(url.host,url.port)
                http.use_ssl = true
                http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                requeste = Net::HTTP::Get.new(url)
                requeste["Accept"] = 'application/json'
                requeste["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
                response = http.request(requeste)
                checkData = JSON.parse(response.read_body)
                if checkData["status"] == "UNPAID"
                  check.status = params[:status]
                  check.save
                  url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
                  http = Net::HTTP.new(url.host,url.port)
                  http.use_ssl = true
                  http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                  requeste = Net::HTTP::Delete.new(url)
                  requeste["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
                  requeste["Content-Type"] ='application/json'
                  response = http.request(requeste)
                  case response
                  when Net::HTTPSuccess
                    wallet = Wallet.find(check.wallet_id)
                    if current_user.present? && current_user!='merchant'
                      location=nil
                      merchant = merchant_to_parse(wallet.users.first)
                    else
                      location = location_to_parse(wallet.location)
                      merchant = merchant_to_parse(wallet.location.merchant)
                    end
                    tags = {
                        "location" => location,
                        "merchant" => merchant,
                        "send_check_user_info" => check_info,
                    }
                    ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
                    transaction = @user.transactions.create(
                        to: nil,
                        from: wallet.id,
                        status: "pending",
                        amount: check.actual_amount,
                        sender: current_user,
                        sender_name: current_user.try(:name),
                        sender_wallet_id: wallet.id,
                        sender_balance: SequenceLib.balance(wallet.id),
                        action: 'issue',
                        net_amount: check.actual_amount.to_f,
                        total_amount: check.actual_amount.to_f,
                        ip: get_ip,
                        main_type: ach_check_type,
                        tags: tags
                    )
                    ach_check_type=check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck
                    issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
                    if issue.present?
                      transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
                      #= creating block transaction
                      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                      save_block_trans(parsed_transactions) if parsed_transactions.present?
                    end
                    check.status = params[:status]
                    check.settled = true
                    check.approved = true
                    check.void_transaction_id = transaction.try(:id)
                    if check.save
                      flash[:success] = I18n.t 'merchant.controller.successful_void_check'
                    else
                      flash[:error] = I18n.t 'merchant.controller.exception2'
                    end
                  when Net::HTTPUnauthorized
                    check.status = check_old_status
                    check.save
                    flash[:error] = I18n.t 'merchant.controller.unauthorize_access'
                  when Net::HTTPNotFound
                    check.status = check_old_status
                    check.save
                    flash[:error] = I18n.t 'merchant.controller.record_notfound'
                  when Net::HTTPServerError
                    check.status = check_old_status
                    check.save
                    flash[:error] = I18n.t 'merchant.controller.server_notrespnd'
                  else
                    check.status = check_old_status
                    check.save
                    error = JSON.parse(response.body)
                    flash[:error] = "#{error["error"]}"
                  end
                else
                  if checkData["status"] == "IN_PROCESS"
                    # flash[:error] = I18n.t 'merchant.controller.ex_time_limit' temporaily commented untill new US team create a new Error for it
                  else
                    msg = "The VOID cannot be processed at this time beacuse the check status is #{checkData["status"]}"
                    handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true) unless request.host == 'localhost'
                    flash[:error] = I18n.t 'merchant.controller.void_not_proced'
                  end
                end
              else
                flash[:error] = I18n.t 'merchant.controller.exception3'
              end
              if flash[:error].blank?
                order_type = ("ACH" if check.order_type == "instant_ach") || ("P2C" if check.order_type == "instant_pay") || "Check"
                activity_type = ("ach" if check.order_type == "instant_ach") || ("push_to_card" if check.order_type == "instant_pay") || ("checks" if check.order_type == "check")  || "bulk_checks"
                
                ActivityLog.log(nil,"#{order_type} Voided",current_user,params,response,"#{order_type} Voided [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]",activity_type)            
                ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]
              else
                ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error]   
              end
            end
          end
        end             
      rescue Exception => e
        flash[:error] = e.message
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error]   
      end
  end



  def show
    @check= TangoOrder.find params[:id]
    phone_number =  @check.try(:bulk_check).try(:phone)

    account = ''
    wallet = Wallet.find_by(id: @check.wallet_id) 
      account = wallet.location.try(:business_name) if wallet.present?
      amount = number_with_precision(@check.actual_amount, precision: 2, delimiter: ',') 
      fee = number_with_precision(@check.check_fee.to_f + @check.fee_perc.to_f, precision: 2, delimiter: ',') 
      total_amount = number_with_precision(@check.amount, precision: 2, delimiter: ',') 
    render json: {"account" => account,"recipient"=>@check.try(:recipient),"memo"=>@check.description,
      "number" => @check.try(:id),"status"=>@check.try(:status),"amount"=>amount,"fee"=>fee,"total_amount"=>total_amount,"phone_number"=> phone_number
    }
  end

  def location_data
    if @user.merchant?
      if params[:location_id].present?
        @location = Location.find(params[:location_id])
      else
        @location = Location.new
      end
      wallets = @location.wallets.primary.last
      if wallets.present?
        @balance = show_balance(wallets.id) - HoldInRear.calculate_pending(wallets.id)
      else
        @balance = 0
      end
        if @user.merchant?
          if @location.fees.present?
            fee =  @location.fees.try(:buy_rate).try(:first)
            if fee.present?
              @fee_dollar = fee.send_check
              @fee_perc = fee.redeem_fee
            end
          end
        end
        if @location.check_limit_type == "Transaction"
          @check_limit = @location.check_limit.to_f
          @check_limit_type = @location.check_limit_type
        elsif @location.check_limit_type == "Day"
          date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          instant_check = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",@location.id,date,"VOID",[0,5]).where(order_type: 'check',void_transaction_id: nil)
          @check_limit = @location.check_limit.to_f
          @total_count = instant_check.sum(:actual_amount)
          @check_limit_type = @location.check_limit_type
        end
      # =+=+=+=+=+=+=+=+=+ merchant end
    else
      if params[:location_id].present? && params[:location_id].to_i != 0
        @balance = show_balance(params[:location_id]) - HoldInRear.calculate_pending(params[:location_id])
          @fee_dollar = @user.system_fee.try(:[],'send_check')
          @fee_perc = @user.system_fee.try(:[],'redeem_fee')
          if @user.system_fee.try(:[],'check_limit_type') == "Transaction"
            @check_limit = @user.system_fee.try(:[],'check_limit').to_f
            @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
          elsif @user.system_fee.try(:[],'check_limit_type') == "Day"
            date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
            instant_check = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=? and tango_orders.order_type IN (?)",params[:location_id],date,"VOID",[0,5])
            @check_limit = @user.system_fee.try(:[],'check_limit').to_f
            @total_count = instant_check.sum(:actual_amount)
            @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
          end
        #adding limits for push to card
      else
        @balance = 0
      end
    end

      fee_dollar = @fee_dollar
      fee_perc = @fee_perc 

      location_block = false
      location_block_withdraw = false
      system_tran_type = false
      system_day_type = false

      if @location.present?
        if @location.try(:is_block) ||  @location.try(:block_withdrawal)
          if @location.try(:is_block)
            location_block = true
          else
            location_block_withdraw = true
          end
        end
      end

      @balance =  number_with_precision(@balance, precision: 2, delimiter: ',') 

      check_limit =  @check_limit
      check_limit_type = @check_limit_type 

      if @user.system_fee.try(:[],'check_limit_type').present? && @user.system_fee.try(:[],'check_limit_type') == "Transaction" 
        system_tran_type = true
      elsif @user.system_fee.try(:[],'check_limit_type').present? && @user.system_fee.try(:[],'check_limit_type') == "Day"
        system_day_type = true
      end
      
      render json: {"balance" => @balance,"fee_dollar1" => @fee_dollar , "fee_perc1" => @fee_perc,
        "location" => @location , "location_block" => location_block , "location_block_withdraw" => location_block_withdraw ,
        "check_limit" => check_limit , "check_limit_type" => check_limit_type , "total_count" => @total_count , "system_day_type" => system_day_type,
        "system_tran_type" => system_tran_type
      }
  end
  def create
    begin

    amount = params[:check][:amount].to_f
    recipient = current_user.merchant? && current_user.merchant_id.present? ? current_user.try(:parent_merchant).try(:email) : params[:check][:recipient]
    name = params[:check][:name]
    sender = params[:check][:sender]
    description = params[:check][:description]
    send_via = 'email'
    send_check_user_info = { name: name, check_email: recipient }
    begin
      raise I18n.t('errors.withdrawl.amount_must_greater_than_zero') if params[:check][:amount].to_f <= 0
      unless params[:location_id].blank?
        @user = User.find(params[:check][:sender_id]) if @user.blank?
        if @user.MERCHANT?
          location = Location.find_by(id: params[:location_id])
          @wallet = location.wallets.try(:primary).try(:first)
          params[:wallet_id] = @wallet.id
        else
          @wallet = Wallet.find(params[:location_id])
          location = @wallet.location
          params[:wallet_id] = @wallet.id
        end
        raise I18n.t('errors.withdrawl.blocked_check') if location.try(:block_withdrawal)
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
        main_type = updated_type(TypesEnumLib::TransactionType::SendCheck)
        if @user.present? && @wallet.present?
          amount_sum = 0
          fee_lib = FeeLib.new
          escrow_wallet = Wallet.check_escrow.first
          escrow_user = escrow_wallet.try(:users).try(:first)
          if @user.merchant?
            location_fee = location.fees if location.present?
            fee_object = location_fee.buy_rate.first if location_fee.present?
            fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
            fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
            amount_sum = amount + fee["fee"].to_f
            amount_sum = amount_sum.round(2)
            splits = fee["splits"]
            if splits.present?
              agent_fee = splits["agent"]["amount"]
              iso_fee = splits["iso"]["amount"]
              iso_balance = show_balance(location.iso.wallets.primary.first.id)
              if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
                if iso_balance < iso_fee
                  raise CheckError.new(I18n.t("errors.iso.iso_0001"))
                end
              else
                if iso_balance + iso_fee < agent_fee.to_f
                  raise CheckError.new(I18n.t("errors.iso.iso_0002"))
                end
              end
            end
            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if show_balance(@wallet.id, @user.ledger).to_f < amount_sum

            if location.check_limit_type.present? && location.check_limit != 0
              if params[:check][:amount].to_f > location.check_limit.to_f && location.check_limit_type == "Transaction"
                raise CheckError.new(I18n.t('errors.withdrawl.cant_proceed_daily_limit', location_limit: location.check_limit.to_f))
              end
              if location.check_limit_type == "Day"
                date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
                instant_checks = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",location.id,date,"VOID",[0,5])
                total_sum = instant_checks.sum(:actual_amount) + params[:check][:amount].to_f
                if total_sum.to_f > location.check_limit.to_f
                  raise CheckError.new(I18n.t('errors.withdrawl.cant_proceed_daily_limit', location_limit: location.check_limit.to_f))
                end
              end
            end
            pending_amount = HoldInRear.calculate_pending(params[:wallet_id])
            balance = show_balance(@wallet.id, @user.ledger).to_f

            if !@user.merchant_id.nil? && current_user.try(:permission).try(:check)
              user_id = @user.merchant_id
            else
              user_id = @user.id
            end
            main_user = User.find_by(id: user_id)
            if main_user.oauth_apps.present?
              oauth_app = main_user.oauth_apps.first
              raise CheckError.new('') if oauth_app.is_block && main_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
            end
            if main_user.oauth_apps.present?
              oauth_app = main_user.oauth_apps.first
              raise CheckError.new(I18n.t('merchant.controller.blocked_by_admin')) if oauth_app.is_block && main_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
            end
            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if balance - pending_amount < amount_sum
            ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
           
            check = TangoOrder.new(user_id: user_id,
                                   name: name,
                                   status: "PENDING",
                                   amount: amount_sum,
                                   actual_amount: amount,
                                   check_fee: fee_object.send_check.to_f,
                                   fee_perc: deduct_fee(fee_object.redeem_fee.to_f, amount),
                                   recipient: recipient,
                                   description: description,
                                   order_type: "check",
                                   approved: false,
                                   settled: false,
                                   wallet_id: @wallet.id,
                                   send_via: send_via,
                                   batch_date: Time.now.to_datetime + 1.day,
                                   amount_escrow: true
            )
            location = location_to_parse(@wallet.location) if @wallet.location.present?
            merchant = merchant_to_parse(@wallet.location.merchant) if @wallet.location.present?
            tags = {
                "fee_perc" => splits,
                "location" => location,
                "merchant" => merchant,
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
            total_fee = save_total_fee(splits, fee["fee"].to_f)
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: main_user,
                sender_name: sender_dba,
                sender_wallet_id: @wallet.id,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: total_fee.to_f,
                net_amount: amount.to_f,
                net_fee: fee["fee"].to_f,
                total_amount: amount_sum,
                gbox_fee: gbox_fee.to_f,
                iso_fee: save_iso_fee(splits),
                agent_fee: splits["agent"]["amount"].to_f,
                affiliate_fee: splits["affiliate"]["amount"].to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )
            if fee.present?
              user_info = fee["splits"]
              # withdraw = withdraw(amount,@wallet.id, fee["fee"].to_f || 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
              withdraw = withdraw(amount,@wallet.id, total_fee.to_f || 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
            end
            if withdraw.present? && withdraw["actions"].present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
              check.save
              flash[:success] = "Successfully sent a check to #{ recipient }"
            else
              flash[:error] = I18n.t('merchant.controller.unaval_feature')
            end
          elsif @user.iso? || @user.agent? || @user.affiliate?
            fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
            if fee_object.present?
              send_check_fee = fee_object["send_check"].present? ? fee_object["send_check"].to_f : 0
              fee_perc_fee = fee_object["redeem_fee"].present? ? deduct_fee(fee_object["redeem_fee"].to_f, amount) : 0
            else
              send_check_fee = 0
              fee_perc_fee = 0
            end
            fee = send_check_fee + fee_perc_fee
            amount_sum = amount + fee
            balance = show_balance(@wallet.id, @user.ledger).to_f
            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if balance < amount_sum
            if @user.iso?
              raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: @user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0) && @user.iso?
            elsif @user.affiliate? || @user.agent?
              raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: @user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0)
            end
            check = TangoOrder.new(user_id: @user.id,
                                   name: name,
                                   status: "PENDING",
                                   amount: amount_sum,
                                   actual_amount: amount,
                                   check_fee: send_check_fee,
                                   fee_perc: fee_perc_fee,
                                   recipient: recipient,
                                   description: description,
                                   order_type: "check",
                                   approved: false,
                                   settled: false,
                                   wallet_id: @wallet.id,
                                   send_via: send_via,
                                   batch_date: Time.now.to_datetime + 1.day,
                                   amount_escrow: true)
            tags = {
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: current_user,
                sender_wallet_id: @wallet.id,
                sender_name: sender_dba,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: fee.to_f,
                net_amount: amount_sum.to_f - fee.to_f,
                net_fee: fee.to_f,
                total_amount: amount_sum.to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )

            if fee > 0
              withdraw = custom_withdraw(amount,@wallet.id, fee, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,true)
            else
              withdraw = withdraw(amount,@wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
            end
            if withdraw.present? && withdraw["actions"].present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
              if check.save
                flash[:success] = "Successfully sent a check to #{ recipient }"
              else
                flash[:error] = "6005 Check Failed: Check Details validation failed!"
              end
            else
              flash[:error] = I18n.t('merchant.controller.unaval_feature')
            end
          else
            if @user.wallets.qc_support.present?
              check_fee = 0
            else
              check_fee = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
            end
            amount_sum = amount + check_fee

            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if show_balance(@wallet.id, @user.ledger).to_f < amount_sum

            check = TangoOrder.new(user_id: @user.id,name: name,status: "PENDING",amount: amount_sum,actual_amount: amount,check_fee: check_fee,fee_perc: 0,recipient: recipient,description: description,order_type: "check",approved: false,wallet_id: @wallet.id, account_token: encrypted_card_info, send_via: send_via,account_number: account_number,routing_number: routing_number,batch_date: Time.now.to_datetime + 1.day)
            tags = {
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: current_user,
                sender_name: sender_dba,
                sender_wallet_id: @wallet.id,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: check_fee.to_f,
                net_amount: amount_sum.to_f - check_fee.to_f,
                net_fee: check_fee.to_f,
                total_amount: amount_sum.to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )
            withdraw = withdraw(amount,@wallet.id, check_fee, TypesEnumLib::TransactionType::SendCheck, account_number, send_via,nil,send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)

            if withdraw.blank?
              flash[:error] = I18n.t('merchant.controller.unaval_feature')
            else
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              check.save
              flash[:success] = "Successfully sent a check to #{ recipient }"
            end
          end
        else
          flash[:error] = I18n.t 'merchant.controller.insufficient_balance'
          raise I18n.t 'merchant.controller.insufficient_balance'
        end
      end
    rescue => ex
      flash[:error] = I18n.t 'merchant.controller.chck_creation_faild'
      flash[:error] = ex.message if ex.class == CheckError
      msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
    ensure
      if flash[:error].blank? && flash[:success].blank?
        flash[:error] = I18n.t 'merchant.controller.except'
        msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\`"
        handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
      end
      if flash[:error].blank? and check.present?
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]   
        ActivityLog.log(nil,"Check Created",current_user,params,response,"Check Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","checks")            
      else
        if flash[:error].include?('Error Code')
          str = flash[:error]
          message = str[16, str.length-1]
          code = str[0,15]
        else
          message = flash[:error]
          code = " Error Code #"
        end
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error',error_code: code , message: message
      end
      # return redirect_back(fallback_location: root_path)
    end

    rescue Exception => e
      flash[:error] = e.message
      ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error]
    end
    flash.discard
  end
end