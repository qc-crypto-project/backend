class V2::Merchant::PermissionController < V2::Merchant::BaseController
  before_action :set_permission, only: [:show,:edit,:update,:destroy]
  respond_to :html, :json
  def index
    @q = Permission.ransack(params[:q])
    @heading = "Permissions List"
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    params[:q]["id_eq"] = params[:q][:name_cont] if params[:q].present?
    if @user.merchant_id.present?
      @permissions = Permission.where('merchant_id IN (?) OR permission_type IN (?)',[@user.merchant_id], [0,1]).ransack(params[:q].try(:merge, m: 'or')).result.per_page_kaminari(params[:page]).order(created_at: :desc).per(page_size)
    else
      @permissions = Permission.where('merchant_id = ? OR permission_type IN (?)',@user.id, [0,1]).ransack(params[:q].try(:merge, m: 'or')).result.per_page_kaminari(params[:page]).order(created_at: :desc).per(page_size)
    end
    @filters = [10,25,50,100]
    render partial: 'v2/merchant/permission/permission_datatable' if request.xhr?
  end

  def new
    @permission = Permission.new
    # @url='v2_merchant_permission_index_path'
    # render partial: 'v2/merchant/permission/new'
    # respond_to do |format|
    #   format.js
    # end
  end

  def create
    begin
      @permission = Permission.new(permission_params)
      if @user.merchant_id.present?
        @permission.merchant_id = @user.merchant_id
      else
        @permission.merchant_id = @user.id
      end
      if @permission.custom!
        flash[:success] = 'Permission successfully added!'
        ActivityLog.log(nil,"Permission Created",current_user,params,response,"Created New Permission Type [#{@permission.name}]","permissions")
      else
        flash[:error] = @permission.errors.first
      end
      redirect_to v2_merchant_permission_index_path
    rescue Exception => e
      flash[:error] = e.message
      redirect_to v2_merchant_permission_index_path
    end
  end

  def show
    # render partial: 'v2/merchant/permission/show'
  end

  def edit
    @permission = Permission.find(params[:id])
    # render partial: 'v2/merchant/permission/new'
    render new_v2_merchant_permission_path
    # respond_to do |format|
    #   format.js
    # end
  end

  def update
    old_permission_name = @permission.name if params["permission"]["name"] != @permission.name
    if @permission.update(permission_params)
      flash[:success] = 'Permission has been updated successfully!'
      ActivityLog.log(nil,"Permission Update",current_user,params,response,"Updated Permission name (#{old_permission_name}) to (#{@permission.name})","permissions") if old_permission_name.present?
      ActivityLog.log(nil,"Permission Update",current_user,params,response,"Updated Permission settings for ( #{@permission.name}]","permissions") if old_permission_name.blank?
    else
      flash[:error] = @permission.errors.first
    end
    redirect_to v2_merchant_permission_index_path
  end

  def destroy
    @user = User.merchant.unarchived_users.where(permission_id: @permission.id) if @permission.present?
    if @user.present?
      flash[:error] = 'Permission Assigned to Employees. Please Delete them First!'
    else
      if @permission.delete
        flash[:success] = 'Permission has been Deleted successfully!'
        ActivityLog.log(nil,"Permission Deleted",current_user,params,response,"Deleted [#{@permission.name}] from the list.","permissions")
      else
        flash[:error] = @permission.errors.first
      end
    end
    redirect_to v2_merchant_permission_index_path
  end

  def get_name
    permission_names = Permission.where(name: params[:val]).present?
    respond_to do |format|
      if permission_names.present?
        format.json {render :json => {success:true}}
      else
        format.json {render :json => {success:false}}
      end
    end
  end

  private

  def set_permission
    @permission = Permission.where(id: params[:id]).first if params[:id].present?
  end

  def permission_params
    params.require(:permission).permit(:name,:wallet,:refund,:transfer,:b2b,:virtual_terminal,:dispute_view_only,:dispute_submit_evidence,:accept_dispute,:funding_schedule,:check,:push_to_card,:ach,:gift_card,:sales_report,:checks_report,:gift_card_report,:user_view_only,:user_edit,:user_add,:developer_app,:merchant_id,:qr_scan,:qr_view_only,:qr_redeem, :view_accounts, :account_transfer, :tip_transfer, :withdrawal, :export_daily_batch, :view_chargeback_cases, :fight_chargeback, :accept_chargeback, :view_invoice, :create_invoice, :cancel_invoice, :customer_list, :approve_mr_transaction, :refund_mr_transaction, :export_list, :permission_view_only, :permission_edit, :permission_add, :api_key, :plugin, :fee_structure, :help, :check_view_only, :check_add, :check_void, :push_to_card_view_only, :push_to_card_add, :push_to_card_void, :ach_view_only, :ach_add, :ach_void, :gift_card_view_only, :gift_card_add, :gift_card_void, :export_account, :export_withdrawal, :export_chargeback, :export_invoice, :export_funding_schedule, :export_business_settings, :accounts, :withdrawals, :e_checks, :push_to_cards, :achs, :giftcards, :invoices, :exports, :business_settings, :employee_list, :permission_list, :risk_analysis, :chargebacks, :all )
  end

end
