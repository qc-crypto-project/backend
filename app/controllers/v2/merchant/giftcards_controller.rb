class V2::Merchant::GiftcardsController < V2::Merchant::BaseController
  require 'raas'
  include Merchant::GiftcardsHelper
  include UsersHelper
  include ApplicationHelper
  before_action :validate_location, only: [:buy_card]
  skip_before_action :set_wallet_balance, only: [:show, :buy_card]

  def index
    conditions = []
    parameters = []
    @heading = 'GiftCards'
    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    begin
      if params[:q].present?
        if params[:q][:data_search].to_s.include?(".") && (params[:q][:data_search].to_s.split(".").last == "0" || params[:q][:data_search].to_s.split(".").last == "00")
          amount = params[:q][:data_search].to_s.split(".").first
        elsif params[:q][:data_search].to_s.include?(".") && params[:q][:data_search].to_s.split(".").last.last == "0"
          amount = number_with_precision(params[:q][:data_search], precision: 1).to_s
        else
          amount = params[:q][:data_search].to_s
        end
        conditions << "name ILIKE ?"
        parameters << "%#{params[:q][:data_search]}%"
        conditions << "amount::VARCHAR LIKE ?"
        parameters << amount
        conditions = [conditions.join(" OR "), *parameters]
      end
      if @user.merchant? && @user.merchant_id.blank?
        user_ids = [@user.id, @user.try(:sub_merchants).try(:pluck, :id)].try(:flatten)
      else
        user_ids = [@user.id, @user.merchant_id]
      end
      tangoIntialOrders = TangoOrder.where(user_id: user_ids).where(conditions).order(created_at: :desc).select{|v| v.utid != nil}
      client = TangoClient.instance
      customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
      account_identifier = ENV["ACCOUNT_IDENTIFIER"]
      collect = Hash.new
      collect['account_identifier'] = account_identifier
      collect['customer_identifier'] = customer_identifier
      elements_per_block = 100
      collect['elements_per_block'] = elements_per_block
      page = 0
      collect['page'] = page
      orders = client.orders
      tangos = tangoIntialOrders.map do |order|
        if order.status.blank?
          object = orders.get_order(order.order_id)
          order.update(status: object.status)
        end
      end

      @tango_orders = Kaminari.paginate_array(tangoIntialOrders).page(params[:page]).per(params[:filter].present? ? params[:filter] : 10)

    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.try(:errors).present? && exc.try(:errors).try(:count).to_i > 0
        message = exc.errors.first.message
      end
      flash[:danger] = message
    end
    render partial: "v2/merchant/shared/giftcards_datatable" if request.xhr?
  end

  # for getting giftcards list
  def buy_giftcard
    @heading = 'Buy GiftCards'
    begin
      client = TangoClient.instance
      catalog = client.catalog
      @result = catalog.get_catalog()
      @giftcards = get_all_giftcards(@result)
      if params[:q].present?
        @giftcards = @giftcards.select do |g|
          if params[:q][:data_search].downcase().in? g[:brand_name].downcase() || params[:q][:data_search]
            g
          end
        end
      end
      @giftcards = Kaminari.paginate_array(@giftcards).page(params[:page]).per(params[:filter].present? ? params[:filter] : 10)
        # @giftcards = @giftcards
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      # if exc.class != Raas::RaasGenericException && exc.errors.present? && exc.errors.count > 0
      #   message = exc.errors.first.message
      # end
      flash[:error] = message
      return render_json_response({success: false, message: "#{message}"}, :ok)
    end
    render partial: 'v2/merchant/shared/buy_giftcard_datatable' if request.xhr?
  end


  def show
    @list=[]
    client = TangoClient.instance
    catalog = client.catalog
    @giftcards = catalog.get_catalog()

    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
      @user.wallets.primary.each do |w|
        # @wallets << w
        @location = w.location
        balance = show_balance(w.id)
        withdraw_balance = balance.to_f - HoldInRear.calculate_pending(w.id).to_f
        if @location.present?
          location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: withdraw_balance.to_f,
                  location_status: location_status,
                  block_giftcard: @location.block_giftcard,
                  giftcard_fee: @location.fees.buy_rate.first.giftcard_fee.to_f
              }

        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: withdraw_balance.to_f,
                  location_status: false,
                  block_giftcard: @user.block_giftcard,
                  giftcard_fee: @user.fees.first.giftcard_fee.to_f
              }

        end

      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    else
      @user.wallets.primary.each do |w|
        location = w.location
        # @wallets <<
        if location.present?
          location_status = location.is_block
          balance = show_balance(w.id)
          withdraw_balance = balance.to_f - HoldInRear.calculate_pending(w.id).to_f
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: withdraw_balance.to_f,
                  location_status: location_status,
                  block_giftcard: location.block_giftcard,
                  giftcard_fee: location.fees.first.giftcard_fee
              }
        end
      end
    end
    @giftcard={}
    @giftcards.brands.each do |b|
      br=b.items.select{|v|v.utid== params[:id]}
      if !br.empty?
        @giftcard = {gift_card: br.first,brand_name:b.brand_name,description:b.description,disclaimer:b.disclaimer, image_url: b.image_urls.map{|s| s.second}.third }
      end
    end

    # # for testing only
    # @list = [{:wallet_id=>22, :wallet_name=>"Cantt", :balance=>1.0, :location_status=>false, :block_giftcard=>false, :giftcard_fee=>"2.21"},
    #          {:wallet_id=>13, :wallet_name=>"DHA", :balance=>31800.39, :location_status=>false, :block_giftcard=>false, :giftcard_fee=>"0.00"}]
    # @giftcard = {:gift_card=> {:currency_code=>'CAD', :face_value=>nil, :max_value=>2000.0, :min_value=>0.01, :reward_name=>"Amazon.ca Gift Certificate",:reward_type=>"gift card", :status=>"active", :utid=>"U350230", :value_type=>"VARIABLE_VALUE"}, :brand_name=>"Amazon.ca", :description=> "<p>Amazon.ca Gift Certificates* never expire and can be redeemed towards millions of items at www.amazon.ca and certain of its affiliated websites. Amazon.ca&#39;s huge selection includes products in Books, Electronics, Music, Movies &amp; TV Shows, Video Games, Software, Home &amp; Garden, Sports &amp; Outdoors, Kitchen &amp; Dining, Computer &amp; PC Hardware, Watches, Home Appliances, Office Products, Camera &amp; Photo, Pet Supplies, and more. Amazon.ca is the place to find and discover almost anything you want to buy online at a great price.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Les Ch&egrave;ques-Cadeaux Amazon.ca* peuvent &ecirc;tre utilis&eacute;s pour des millions d&rsquo;articles sur www.amazon.ca et&nbsp; propose une immense s&eacute;lection de livres, produits &eacute;lectroniques, musique, t&eacute;l&eacute;chargements MP3, films et TV, v&ecirc;tements, jeux vid&eacute;o, logiciels, sports &amp; activit&eacute;s de plein air, jouets, articles de pu&eacute;riculture, ordinateurs &amp; bureautique, maison &amp; jardin, bijoux, beaut&eacute;, bricolage et d&eacute;coration d&rsquo;int&eacute;rieur, produits de bureau, vid&eacute;o &amp; photo, accessoires pour animaux et bien d&rsquo;autres choses encore. Amazon.ca est le site o&ugrave; trouver et acheter en ligne &agrave; un excellent prix quasiment tout ce que vous voulez.</p>\r\n", :disclaimer=> "<p>*Amazon.ca is not a sponsor of this promotion. Amazon.ca Gift Certificates (&quot;GCs&quot;) are redeemable only for eligible products on Amazon.ca. Return policies for products are available on Amazon.ca. Except as required by law, GCs cannot be reloaded, resold, transferred for value, redeemed for cash or applied to any other account. To view a GC balance or for more information about your GC, visit &quot;Your Account&quot; on Amazon.ca or contact us at <a href=\"http://www.amazon.ca/contact-us\">www.amazon.ca/contact-us</a>. GCs cannot be used to purchase other GCs. Amazon is not responsible if a GC is lost, stolen, destroyed or used without permission. For complete terms and conditions, see <a href=\"http://www.amazon.ca/gc-legal\">www.amazon.ca/gc-legal</a>. GCs are issued by Amazon.com.ca, Inc., a Delaware corporation. All Amazon &reg;, &trade; &amp; &copy; are IP of Amazon.com or its affiliates. No expiration date or service fees.</p>\r\n\r\n<p>*Amazon n&rsquo;est pas un sponsor de cette promotion. Les Ch&egrave;ques-Cadeaux Amazon.ca (CC) sont utilisables seulement sur les produits &eacute;ligibles vendus sur Amazon.ca. Les politiques de retour des produits sont disponibles sur Amazon.ca. Except&eacute; dans les cas pr&eacute;vus par la loi, les CC ne peuvent pas &ecirc;tre recharg&eacute;s, revendus, c&eacute;d&eacute;s en &eacute;change d&rsquo;une contrepartie, &eacute;chang&eacute;s contre de l&rsquo;argent liquide ou appliqu&eacute;s &agrave; un autre compte. Pour voir votre de solde de CC, ou pour toute autre information sur votre CC, consultez la page &laquo; Votre Compte &raquo; sur Amazon.ca ou contactez-nous sur <a href=\"http://www.amazon.ca/contact-us\">www.amazon.ca/contact-us</a>. Les CC ne peuvent pas &ecirc;tre utilis&eacute;s pour acheter d&rsquo;autres CC. Amazon n&rsquo;est pas responsable si un CC est perdu, vol&eacute;, d&eacute;truit ou utilis&eacute; sans votre permission. Pour voir les Modalit&eacute;s compl&egrave;tes, consultez la page <a href=\"http://www.amazon.ca/gc-legal\">www.amazon.ca/gc-legal</a>. Les CC sont &eacute;mis parAmazon.com.ca, Inc., soci&eacute;t&eacute; du Delaware. Tous les &reg;, TM et &copy; Amazon sont la propri&eacute;t&eacute; intellectuelle d&#39;Amazon.com ou ses filiales. Pas de date d&rsquo;expiration ou de frais.</p>\r\n", :image_url=>"https://dwwvg90koz96l.cloudfront.net/images/brands/b844636-200w-326ppi.png"}
    # @wallets = nil
    #render json: {"giftcard" => @giftcard,"wallets" => @wallets}
    render partial: 'show', locals: { giftcard: @giftcard,wallets: @wallets  }
  end


  def buy_card
    fee = 0
    unless params[:email].present? &&  params[:image_url].present? && params[:wallet_id].present? &&  params[:utid].present? && params[:amount].present?
      flash[:danger] = I18n.t('merchant.controller.missng_parms')
      # return redirect_back(fallback_location: root_path)
    end
    @user= current_user
    @user= User.find(current_user.merchant_id) if current_user.merchant_id.present? && current_user.try(:permission).admin?
    if @user.oauth_apps.present?
      oauth_app = @user.oauth_apps.first
      if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_giftcard}.all?
        flash[:error] = I18n.t 'merchant.controller.blocked_by_admin'
        # redirect_back(fallback_location: root_path)
      end
    end
    begin
      validate_parameters({wallet_id: params[:wallet_id], utid:  params[:utid], amount: params[:amount] })
      balance = show_balance(params[:wallet_id])
      client = TangoClient.instance
      if params[:amount].present? && params[:utid].present?
        fee_lib = FeeLib.new
        w = Wallet.find(params[:wallet_id])
        withdraw_balance = balance.to_f - HoldInRear.calculate_pending(w.id).to_f
        location = w.location
        if location.present?
          if location.is_block
            flash[:danger ] = I18n.t 'errors.withdrawl.blocked_location'
            # return redirect_back(fallback_location: root_path)
          end
          if location.block_giftcard
            flash[:danger ] = I18n.t 'errors.withdrawl.blocked_giftcard'
            # return redirect_back(fallback_location: root_path)
          end
        end
        amount = params[:amount].to_f
        actual_amount = params[:amount].to_f
        if @user.MERCHANT?
          location_fee = location.fees.buy_rate.first
          fee_class = Payment::FeeCommission.new(@user, nil, location_fee, w, location,TypesEnumLib::CommissionType::Giftcard)
          fee_value = fee_class.apply(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard)
          amount = amount + fee_value["fee"].to_f
          fee = fee_value["fee"].to_f
        elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
          data = deduct_giftcard_fee(@user,amount)
          amount = data[0]
          fee = data[1]
        else
          location_fee = fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
          amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
          fee = location_fee
        end
        if withdraw_balance.to_f < amount.to_f
          flash[:danger] = I18n.t("merchant.controller.insufficient_balance")
        else
          account = client.accounts
          account_info = account.get_account('quickcard')
          if account_info.present? && account_info.current_balance.present?
            tango_balance = account_info.current_balance
          end
          if tango_balance.present? && tango_balance < amount.to_f
            flash[:danger] = I18n.t 'merchant.controller.danger_excetption'
          else
            if @user.MERCHANT?
              if withdraw_balance.to_f < amount.to_f
                flash[:danger ] = I18n.t("merchant.controller.insufficient_balance")
                # return redirect_back(fallback_location: root_path)
              end
            end
            ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
            orders = client.orders
            customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
            account_identifier = ENV["ACCOUNT_IDENTIFIER"]

            body = Raas::CreateOrderRequestModel.new
            body.account_identifier = account_identifier
            body.amount = params[:amount].to_f || 0
            body.customer_identifier = customer_identifier
            body.send_email = true
            body.sender = {
                "email": 'admin@quickcard.me',
                "firstName": "QuickCard",
                "lastName": ""
            }
            body.recipient = {
                "email": params[:email],
                "firstName": @user.name || 'No Name',
                "lastName": ""
            }
            body.utid = params[:utid]
            result = orders.create_order(body)
            if result
              p "TANGO ORDER: ", result
              tango_order =  @user.tango_orders.create!(
                  :utid => params[:utid],
                  :account_identifier => account_identifier,
                  :amount => body.amount,
                  :name => result.reward_name,
                  :recipient => params[:email],
                  :order_id => result.reference_order_id,
                  :catalog_image => params[:image_url] || nil,
                  :wallet_id => params[:wallet_id],
                  :gift_card_fee => fee,
                  :status => result.status
              )
              if @user.MERCHANT?
                user_info = fee_value["splits"]
                user_info["utid"] = params[:utid].present? ? params[:utid] : ''
                user_info["recipient_email"] = params[:email].present? ? params[:email] : ''
                user_info["brand_name"] = result.reward_name.present? ? result.reward_name : ''
                # admin_wallet = Wallet.where(wallet_type: 3).first
                tags = {
                    "fee_perc" => user_info
                }
                gbox_fee = save_gbox_fee(fee_value["splits"], fee.to_f)
                total_fee = save_total_fee(fee_value["splits"], fee.to_f)
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: location.try(:business_name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: total_fee.to_f,
                    net_amount: actual_amount.to_f,
                    net_fee: total_fee.to_f,
                    total_amount: amount.to_f,
                    gbox_fee: gbox_fee.to_f,
                    iso_fee: save_iso_fee(fee_value["splits"]),
                    agent_fee: fee_value["splits"]["agent"]["amount"].to_f,
                    affiliate_fee: fee_value["splits"]["affiliate"]["amount"].to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
                    tags: tags
                )
                transact = withdraw(params[:amount].to_f,params[:wallet_id], location_fee.giftcard_fee.to_f, TypesEnumLib::TransactionType::GiftCardPurchase, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango)
              elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
                utid = params[:utid].present? ? params[:utid] : ''
                recipient_email = params[:email].present? ? params[:email] : ''
                brand_name = result.reward_name.present? ? result.reward_name : ''
                user_info = {utid: utid, recipient_email: recipient_email, brand_name:brand_name}
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: fee.to_f,
                    net_amount: actual_amount.to_f,
                    net_fee: fee.to_f,
                    total_amount: amount.to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
                    tags: user_info
                )
                transact = withdraw(amount,params[:wallet_id], fee, TypesEnumLib::TransactionType::GiftCard, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango,'other')
              else
                # user_info = fee_lib.giftcard_fee_divsion(location,location_fee, nil, nil)
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: location_fee.to_f,
                    net_amount: actual_amount,
                    net_fee: location_fee.to_f,
                    total_amount: amount.to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase)
                )
                transact = withdraw(params[:amount].to_f,params[:wallet_id], location_fee.to_f, TypesEnumLib::TransactionType::GiftCardPurchase, nil,nil, nil,nil,nil,nil,TypesEnumLib::GatewayType::Tango)
              end
              ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
              if transact.present?
                tango_order.update(transaction_id: transaction.id)
                transaction.update(seq_transaction_id: transact.id, tags: transact.actions.first.tags, status: "approved", timestamp: transact.timestamp)
                #= creating block transaction
                parsed_transactions = parse_block_transactions(transact.actions, transact.timestamp)
                save_block_trans(parsed_transactions) if parsed_transactions.present?
              end
              flash[:success] = I18n.t 'merchant.controller.successful_order'
            end

            # transact = transaction_between(admin_wallet.id, @user.wallet_id, amount, "Tango Transfer", 0, "")
          end
        end
      else
        flash[:error] = I18n.t 'merchant.controller.missng_parms'
      end
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.message.present?
        message = exc.message
      end
      flash[:error] = exc.message
    end
    if flash[:error].blank? and tango_order.present?
      ActivityLog.log(nil,"Giftcard Created",current_user,params,response,"Giftcard Created [##{tango_order.id}] for [$#{number_with_precision(amount.to_f, precision: 2)}]","giftcard")
      ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [tango_order.id]
    else
      ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', message: flash[:error].present? ? flash[:error] : "Something Went wrong"
    end
    flash.discard
  end


  def create_order
    params.require(:check).permit(:name , :sender ,:recepient ,:amount)
  end

end
