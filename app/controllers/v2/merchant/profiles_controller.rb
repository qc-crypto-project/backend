class V2::Merchant::ProfilesController < V2::Merchant::BaseController
  layout 'v2/merchant/layouts/application'
  include ProfileHandler

  def index
  end
  def faq

  end
  def privacy_policy
  end

  def update_two_step_verification
    current_user.update(two_step_verification: params[:two_step])
    cookies.delete :remember_tfa
  end
  def update_profile_email_number
    params[:user]["phone_number"] = params[:user]["phone_number_to_save"] if params[:user]["phone_number_to_save"].present?
    email = params[:mechant_email].downcase if params[:mechant_email].present?
    user = User.not_customers.where(email: email).first if email.present?
    if user.present?
      if user.id == current_user.id
        if user.email != email || user.phone_number != params[:user]["phone_numer"]
          if user.email != email
            text = I18n.t('notifications.email_change_notif', name: user.name)
            Notification.notify_user(user,user,user,"change email",nil,text)
          end
           user.update(email: email, phone_number: params[:user]["phone_number"] )
        end
        flash[:success] = 'Merchant Updated Successfully!'
        redirect_to v2_merchant_profiles_path
      else
        flash[:success] = 'A user with this email already exists!'
        redirect_to v2_merchant_profiles_path
      end
    else
      if current_user.email != email || current_user.phone_number != params[:user]["phone_numer"]
        if current_user.email != email
          text = I18n.t('notifications.email_change_notif', name: current_user.name)
          Notification.notify_user(current_user,current_user,current_user,"change email",nil,text)
        end
        current_user.update(email: email, phone_number: params[:user]["phone_number"] )
      end
      flash[:success] = 'Merchant Updated Successfully!'
      redirect_to v2_merchant_profiles_path
    end
  end

  def update_pin
    send_pin
  end

  def employee_list
    @heading = "Employee List"
    @user= User.new
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    if current_user.merchant_id.present?
      @sub_merchants= User.where(id: current_user.merchant_id).first.sub_merchants.where(archived: "false").where.not(id: current_user.id).per_page_kaminari(params[:page]).per(page_size)
      @wallets = Location.where(merchant_id: current_user.merchant_id)
    else
      if params[:q].present?
        params[:q]={name_or_email_or_phone_number_cont:params[:q]}
        @q= current_user.sub_merchants.where(archived: "false").ransack(params[:q])
        @sub_merchants=@q.result().per_page_kaminari(params[:page]).per(page_size)
      else
        @sub_merchants= current_user.sub_merchants.where(archived: "false").per_page_kaminari(params[:page]).per(page_size)
      end
      @wallets = Location.where(merchant_id: current_user.id)
    end
    @sub_merchant=User.new
    # @wallets = current_user.wallets
    @filters = [10,25,50,100]
    if params[:search].present? && !params[:page].present?
      render partial: 'v2/merchant/shared/profile_datatable'
    else
      render template: 'v2/merchant/profiles/employee_list'
      # render partial: 'v2/merchant/shared/profile_datatable'
    end

  end


end
