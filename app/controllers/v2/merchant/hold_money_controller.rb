class V2::Merchant::HoldMoneyController < V2::Merchant::BaseController

  def index
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    if params[:wallet_id].present?
      @user_wallets = Wallet.where(id: params[:wallet_id])
    else
      if current_user.merchant_id.present?
        @user_wallets = @user.wallets.primary.where.not(location_id: nil)
      else
        @user_wallets = @user.wallets.primary
      end
    end
    @locations = @user.get_locations
    @user_location_ids = @user_wallets.pluck(:location_id)
    if params[:query].present? && params[:query]['date'].present? && params[:location].present?
      date = parse_reports_date(params[:query][:date])
      location = params[:location]
      @hold_rear = HoldInRear.by_location_ids(location).where(created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).order(created_at: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    elsif params[:query].present? && params[:query]['date'].present?
      date = parse_reports_date(params[:query][:date])
      @hold_rear = HoldInRear.by_location_ids(@user_location_ids).where(created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).order(created_at: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    elsif params[:location].present?
      location = params[:location]
      @hold_rear = HoldInRear.by_location_ids(location).order(created_at: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    else
      @hold_rear = HoldInRear.is_created.by_location_ids(@user_location_ids).order(created_at: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    end
  end

  def transactions
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @hold_in_rear = HoldInRear.find_by(id: params[:hold_in_rear_id])
    created_at = @hold_in_rear.try(:created_at)
    wallet = @hold_in_rear.location.wallets.primary.last
    @wallet=wallet
     @transactions = Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["complete","approved"], main_type:["eCommerce","Virtual Terminal","Credit Card","Debit Card",TypesEnumLib::TransactionType::QCSecure,TypesEnumLib::TransactionType::Transfer3DS,TypesEnumLib::TransactionType::InvoiceDebitCard, TypesEnumLib::TransactionType::InvoiceCreditCard, TypesEnumLib::TransactionType::InvoiceRTP, TypesEnumLib::TransactionType::RTP],receiver_wallet_id: wallet.id).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["refunded"], receiver_wallet_id: wallet.id)).order(created_at: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
  end

  def reserve_money
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    if params[:wallet_id].present?
      @user_wallets = Wallet.where(id: params[:wallet_id])
    else
      if current_user.merchant_id.present?
        @user_wallets = @user.wallets.primary.where.not(location_id: nil)
      else
        @user_wallets = @user.wallets.primary
      end
    end
    @locations = @user.get_locations
    @user_location_ids = @user_wallets.pluck(:location_id)
    if params[:query].present? && params[:query]['date'].present? && params[:location].present?
      date = parse_reports_date(params[:query][:date])
      location = params[:location]
      @hold_rear = HoldInRear.by_location_ids(location).where(created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).where('reserve_amount > ?',0).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    elsif params[:query].present? && params[:query]['date'].present?
      date = parse_reports_date(params[:query][:date])
      @hold_rear = HoldInRear.by_location_ids(@user_location_ids).where(created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).where('reserve_amount > ?',0).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    elsif params[:location].present?
      location = params[:location]
      @hold_rear = HoldInRear.by_location_ids(location).where('reserve_amount > ?',0).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    else
      @hold_rear = HoldInRear.is_created.by_location_ids(@user_location_ids).where('reserve_amount > ?',0).order(reserve_release: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    end
  end

  def reserve_transactions
    @reserve=true
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @hold_in_rear = HoldInRear.find_by(id: params[:hold_in_rear_id])
    created_at = @hold_in_rear.try(:created_at)
    wallet = @hold_in_rear.location.wallets.primary.last
    @wallet=wallet
    # @transactions = Transaction.where(created_at: HoldInRear.pst_beginning_of_day(hold_in_rear.created_at)..HoldInRear.pst_end_of_day(hold_in_rear.created_at), status: ["complete","approved"], main_type:["eCommerce","Virtual Terminal","Credit Card"],to: wallet.id).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["refunded"], to: wallet.id)).distinct.per_page_kaminari(params[:page]).per(page_size)
     sale_transactions = BlockTransaction.where(created_at: HoldInRear.pst_beginning_of_day(@hold_in_rear.created_at)..HoldInRear.pst_end_of_day(@hold_in_rear.created_at), main_type:'Reserve Money Deposit',sender_wallet_id: wallet.id).distinct
     seq_parent_id=sale_transactions.pluck(:seq_parent_id)
     @transactions=Transaction.where(seq_transaction_id:seq_parent_id,status:['complete','approved','refunded']).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
  end


  private

  def parse_reports_date(date)
    offset = DateTime.now.in_time_zone(cookies[:timezone]).utc_offset
    first_date = date[0..9]
    first_date = Date.strptime(first_date, '%m-%d-%Y')
    firstdate = Time.new(first_date.year,first_date.month,first_date.day,00,00,00, offset).utc
    firsttime = firstdate.strftime("%H:%M:%S.00Z")
    firstdate = firstdate.strftime("%Y-%m-%dT#{firsttime}")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, '%m-%d-%Y')
    seconddate = Time.new(second_date.year,second_date.month,second_date.day,23,59,59, offset).utc
    secondtime = seconddate.strftime("%H:%M:%S.59Z")
    seconddate = seconddate.strftime("%Y-%m-%dT#{secondtime}")
    return {first_date: firstdate, second_date: seconddate}
  end


end
