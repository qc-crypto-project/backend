class V2::Merchant::AppsController < V2::Merchant::BaseController

  def index
    @heading = 'Apps'
    if current_user.merchant_id.present? 
      @apps=OauthApp.where(user_id: current_user.merchant_id).per_page_kaminari(params[:page]).per(10).order("id DESC")
    else
      @apps=OauthApp.where(user_id: current_user.id).per_page_kaminari(params[:page]).per(10).order("id DESC")
    end
     @location_apps = current_user.get_locations
  end

  def location_fees
    @locations = current_user.get_locations
  end

  def location_fee
    location = Location.find_by(id: params[:id])
    fee = location.fees.buy_rate.first
    misc_date = fee.misc_date.present? ? fee.misc_date.strftime("%m/%d/%Y") : ""
    service_date = fee.service_date.present? ? fee.service_date.strftime("%m/%d/%Y") : ""
    statement_date = fee.statement_date.present? ? fee.statement_date.strftime("%m/%d/%Y") : ""
    render json: {location: location, fee: fee, misc_date: misc_date, service_date: service_date, statement_date: statement_date}
  end

  def update_revealed
    auth_app = OauthApp.find_by_id(params[:id])
    # auth_app.update(revealed_by: "Revealed by: #{current_user.name.upcase}. #{ Time.now.strftime("%m/%d/%Y %I:%M %p")}")
    auth_app.update(revealed_by: current_user.name.upcase , revealed_date: Time.now)
    local_time_zone = false
    if cookies[:timezone].present?
      revealed_date = auth_app.revealed_date.in_time_zone(cookies[:timezone]).strftime("%m-%d-%Y %I:%M:%S %p")
    else
      revealed_date = auth_app.revealed_date.strftime("%m-%d-%Y %I:%M:%S %p")
      local_time_zone = true
    end
    # revealed_date = auth_app.try(:revealed_date).try(:strftime,("%m-%d-%Y %I:%M:%S %p"))
    render json: {"revealed_by"=>auth_app.revealed_by, "revealed_date"=>revealed_date , "local_time_zone"=>local_time_zone}
  end
end