class V2::Merchant::InstantAchController < V2::Merchant::BaseController
    skip_before_action  :set_wallet_balance, only: [:create_instant_ach, :new, :selected_locations, :location_data]
    skip_before_action :permitted_routes, only: [:location_data]

    class CheckError < StandardError; end

    def index
    	@q = TangoOrder.ransack(params[:q])

    	if params[:custom].present?
				if params[:custom] == "true"
      		params[:first_date] = DateTime.parse(params[:first_date])
      		params[:second_date] = DateTime.parse(params[:second_date])
				end
    	end
    	
	    page_size = params[:filter].present? ? params[:filter].to_i : 10
	    @achIssueEnabled = true
	    @filters = [10,25,50,100]
	    @total_paid = @total_void = @total_pending = @total_failed = @total_in_process = @total_paid_wire = number_with_precision(0, :precision => 2, :delimiter => ',')
		params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )
		if params[:q].present?
			params[:q]["name_or_account_number_or_wallet_location_business_name_cont"] = params[:q][:data_search]
	    	params[:q]["id_eq"] = params[:q][:data_search] 
	    	params[:q]["total_fee_eq"] = params[:q][:data_search] if !params[:q][:data_search].is_a? String
	    	params[:q]["amount_eq"] = params[:q][:data_search] 
	    	params[:q]["actual_amount_eq"] = params[:q][:data_search]
		end
		
	    if current_user.present? && !current_user.merchant_id.nil?
	      parent_user = User.find(current_user.merchant_id)
	      user_ids=[parent_user.id,current_user.id]

	      wallet_ids =  params[:wallet_id].present? ? params[:wallet_id] : current_user.wallets.pluck(:id)
	      if parent_user.oauth_apps.present?
	        oauth_app = parent_user.oauth_apps.first
	        @achIssueEnabled = false if oauth_app.is_block && parent_user.get_locations.uniq.map {|loc| loc.block_ach}.all?
	      end
	      	@total_achs = TangoOrder.where(order_type: [5,6] ,user_id: user_ids, wallet_id: wallet_ids)
	      	@total_achs = @total_achs.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
	        @total_achs = @total_achs.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
	    	@achs = @total_achs.order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
	    else
	      if current_user.sub_merchants.present?
	        sub_id = current_user.sub_merchants.pluck(:id)
	        user_ids=[current_user.id,sub_id]
	        user_ids= user_ids.flatten if user_ids.present?
	      else
	        user_ids=current_user.id
	      end
	      if @user.oauth_apps.present?
	        oauth_app = @user.oauth_apps.first
	        @achIssueEnabled = false if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_ach}.all?
		  end
			if params[:wallet_id].present?
				@total_achs = TangoOrder.where(order_type: [5,6] ,wallet_id: params[:wallet_id])
			else
				@total_achs = TangoOrder.where(order_type: [5,6] ,user_id: user_ids)
			end
			@total_achs = @total_achs.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
			@total_achs = @total_achs.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
	    	@achs = @total_achs.order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
	    end
	    if @total_achs.present? 
	    	# number_with_precision(ach.amount, precision: 2, delimiter: ',') 
	    	@total_paid = @total_achs.PAID.sum(:amount)
	    	@total_paid =  number_with_precision(@total_paid, precision: 2, delimiter: ',') 
	    	@total_pending = @total_achs.PENDING.sum(:amount)
	    	@total_pending =  number_with_precision(@total_pending, precision: 2, delimiter: ',') 
	    	@total_void = @total_achs.VOID.sum(:amount)
	    	@total_void =  number_with_precision(@total_void, precision: 2, delimiter: ',') 
	    	@total_failed = @total_achs.FAILED.sum(:amount)
	    	@total_failed =  number_with_precision(@total_failed, precision: 2, delimiter: ',') 
	    	@total_in_process = @total_achs.IN_PROCESS.sum(:amount) + @total_achs.IN_PROGRESS.sum(:amount)
	    	@total_in_process =  number_with_precision(@total_in_process, precision: 2, delimiter: ',') 
	    	@total_paid_wire = @total_achs.PAID_WIRE.sum(:amount)
	    	@total_paid_wire =  number_with_precision(@total_paid_wire, precision: 2, delimiter: ',')
	    end
		render partial: 'v2/merchant/instant_ach/ach_index_datatable' if request.xhr?
    end

    def show
    	@instant_ach= TangoOrder.find params[:id]
    	account = ''
    	wallet = Wallet.find_by(id: @instant_ach.wallet_id) 
        account = wallet.location.try(:business_name) if wallet.present?
        amount = number_with_precision(@instant_ach.actual_amount, precision: 2, delimiter: ',') 
       	fee = number_with_precision(@instant_ach.check_fee.to_f + @instant_ach.fee_perc.to_f, precision: 2, delimiter: ',') 
       	total_amount = number_with_precision(@instant_ach.amount, precision: 2, delimiter: ',') 
    	render json: {"ach_id"=>@instant_ach.id,"account" => account,"recipient"=>@instant_ach.try(:recipient),"memo"=>@instant_ach.description,
    		"number" => @instant_ach.try(:account_number),"status"=>@instant_ach.try(:status),"amount"=>amount,"fee"=>fee,"total_amount"=>total_amount
	    }
  	end


  	def location_data
	    if @user.merchant?
	      	if params[:location_id].present?
	        	@location = Location.find(params[:location_id])
	      	else
	        	@location = Location.new
	      	end
	      	wallets = @location.wallets.primary.last
	      	if wallets.present?
	        	@balance = show_balance(wallets.id) - HoldInRear.calculate_pending(wallets.id)
	      	else
	        	@balance = 0
	      	end
        	if @user.merchant?
          		if @location.fees.present?
            		fee =  @location.fees.try(:buy_rate).try(:first)
            		if fee.present?
              			@fee_dollar = fee.ach_fee_dollar
              			@fee_perc = fee.ach_fee
            		end
          		end
        	end
        	@banks = @location.bank_details
        	if @location.ach_limit_type == "Transaction"
          		@ach_limit = @location.ach_limit.to_f
          		@ach_limit_type = @location.ach_limit_type
        	elsif @location.ach_limit_type == "Day"
          		date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
							if current_user.merchant_id.present? #&& current_user.submerchant_type == 'admin_user'  ##  submerchant_type column are nil
          			instant_achs = current_user.parent_merchant.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status !=?",@location.id,date,"VOID").instant_ach
							else
								instant_achs = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status !=?",@location.id,date,"VOID").instant_ach
							end
          		@ach_limit = @location.ach_limit.to_f
          		@total_count = instant_achs.sum(:actual_amount)
          		@ach_limit_type = @location.ach_limit_type
        	end
        	begin
          		@bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT-ENCRYPTOR']}",@location.bank_account)
        	rescue
          		@bank_account = @location.bank_account
        	end
	      # =+=+=+=+=+=+=+=+=+ merchant end
			else
	      	if params[:location_id].present? && params[:location_id].to_i != 0
	        	@balance = show_balance(params[:location_id]) - HoldInRear.calculate_pending(params[:location_id])
          		@fee_dollar = @user.system_fee.try(:[],'ach_dollar')
          		@fee_perc = @user.system_fee.try(:[],'ach_percent')
          		if @user.system_fee.try(:[],'ach_limit_type') == "Transaction"
		            @ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
		            @ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
          		elsif @user.system_fee.try(:[],'ach_limit_type') == "Day"
              		date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
              		instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").instant_ach
              		@ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
              		@total_count = instant_achs.sum(:actual_amount)
              		@ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
          		end
	        	#adding limits for push to card
	      	else
	        	@balance = 0
	      	end
	    end

	    if @user.merchant? 
	    	merchant_user = true
	    elsif @user.iso? || @user.agent? || @user.affiliate? 
	    	merchant_user = false
	    end

	    if @banks.present?
  			@banks = @banks.map { |loc| loc if loc.account_no.present?}.compact
		end 

			if @banks.present? && @banks.length == 1
  		    d_account =  "****#{(AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @banks[0].account_no)).try(:last, 4)}"
	    end

	    wallet_id = nil
	    system_fee = nil
	    if @user.try(:[],'system_fee').try(:[],'bank_account').present?
	    	system_fee = true
	    	d_account = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user['system_fee']['bank_account'])
            d_account = "****#{d_account.last(4)}"
            wallet_id = @user.try(:wallets).try(:first).try(:primary).try(:id)
        end

        multiple_bank_details = []
        @balance =  number_with_precision(@balance, precision: 2, delimiter: ',') 
        @banks.map do |loc|
        	if loc.account_no.present?
          		d_account =  "•••• #{(AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", loc.account_no)).last(4)}"
          		multiple_bank_details << {id: loc.id , account: d_account}
          		# $("#bank_account_name").append('<option value="<%= loc.id %>" > <%= d_account %></option>');
       		end
    	end



        location_bank_details = nil

        if @location.bank_details.present?
        	location_bank_details = true
        end

	    render json: {"balance" => @balance,"fee_dollar" => @fee_dollar , "fee_perc" => @fee_perc,
	    	"merchant_user" => merchant_user ,"banks" => @banks , "d_account" => d_account , "wallet_id" => wallet_id , "system_fee" => system_fee,
	    	"location" => @location , "location_bank_details" => location_bank_details , "multiple_bank_details" => multiple_bank_details,"ach_limit" => @ach_limit , "ach_limit_type" => @ach_limit_type , "total_count" => @total_count
	    }
  	end

    def new
    	flash = []
	    if @user.merchant?
	      	if @user.merchant_id.present?
	        	wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:location_id)
	        	@locations = Location.where(id: wallets)
	      	else
	        	@locations = current_user.get_locations
	      	end
	    else
	      	@locations = @user.wallets.primary
	    end
	    render partial: "new"
		end

    def create
	    begin
	      	if params[:bank_routing].blank?
	        	result = setup_params
	        	raise "#{result.second}" if result.first
	      	end
	      	validate_parameters(
	          	{
	              location_id: params[:location_id],
	              bank_name: params[:bank_name],
	              amount: params[:amount],
	              bank_account_type: params[:bank_account_type],
	              bank_routing: params[:bank_routing],
	              bank_account: params[:bank_account],
	              confirm_bank_account: params[:confirm_bank_account],
	              email_address: params[:email_address],
	              description: params[:description]
	          	}
	      	)
	      	raise I18n.t('errors.withdrawl.unmatch_routing_number')  if params[:bank_account] != params[:confirm_bank_account]
	      	raise I18n.t('errors.withdrawl.amount_must_greater_than_zero') if params[:amount].to_f <= 0
					ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
	      	if @user.merchant?
		        location = Location.find(params[:location_id])
		        raise I18n.t('errors.withdrawl.blocked_ach') if location.block_ach
		        raise  I18n.t('errors.withdrawl.account_not_exist') if location.blank?
		        # raise "Error Code 7009 Sorry but it looks like this function is currently not available. Please contact your administrator for details." if location.try(:block_withdrawal)
		        merchant = current_user
						# if current_user.present? && !current_user.merchant_id.nil?
						# 	merchant = User.find(current_user.merchant_id)
							# merchant_id=merchant.id
						# else
						# 	merchant = current_user
							# merchant_id=merchant.id
						# end
		        if merchant.oauth_apps.present?
		          oauth_app = merchant.oauth_apps.first
		          raise  I18n.t('merchant.controller.blocked_by_admin') if oauth_app.is_block && merchant.get_locations.uniq.map {|loc| loc.block_ach}.all?
		        end
		        # raise "Merchant doesn't exist." if merchant.blank?
		        wallet = location.wallets.primary.first
		        raise I18n.t('errors.withdrawl.wallet_not_exist') if wallet.blank?

		        amount = params[:amount].to_f

		        bank_name = params[:bank_name]
		        recipient = merchant.merchant? && merchant.merchant_id.present? ? merchant.try(:parent_merchant).try(:email) : params[:email_address]
		        account_number = params[:bank_account]
		        routing_number = params[:bank_routing]
		        bank_account_type = params[:bank_account_type]
		        description = params[:description]
		        send_via = "ACH"
		        type = 8

		        # raise "Location first name or last name is missing." if location.first_name.blank? || location.last_name.blank?

		        #= calculate fee start
		        location_fee = location.fees if location.present?
		        fee_object = location_fee.buy_rate.first if location_fee.present?
		        fee_class = Payment::FeeCommission.new(merchant, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::ACH)
		        fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::ACH)
		        splits = fee["splits"]
	        	if splits.present?
	          		agent_fee = splits["agent"]["amount"]
	          		iso_fee = splits["iso"]["amount"]
	          		iso_balance = show_balance(location.iso.wallets.primary.first.id)
	          	if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
		            if iso_balance < iso_fee
		              raise CheckError.new(I18n.t("errors.iso.iso_0005"))
		            end
		        else
		            if iso_balance + iso_fee < agent_fee.to_f
		              raise CheckError.new(I18n.t("errors.iso.iso_0006"))
		            end
		        end
	        end
	        #= calculate fee end
	        amount_sum = amount + fee.try(:[],"fee").to_f
	        balance = show_balance(wallet.id).try(:to_f)
	        pending_amount = HoldInRear.calculate_pending(wallet.id)
	        raise 'Error Code 2039 Insuficient balance!' if balance - pending_amount < amount_sum
	        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'

	        card_info = {
	            :account_number => account_number,
	            :account_type => bank_account_type
	        }
	        card_info = card_info.to_json
	        account_number = account_number.last(4)
	        encrypted_account_info = AESCrypt.encrypt("#{merchant.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{wallet.id}", card_info)
	        rand = "#{rand(100..999)}#{Time.now.to_i.to_s}"
	        ach_international = location.ach_international
	        check = TangoOrder.new(user_id: merchant.id,
	                               name: bank_name,
	                               status: "PENDING",
	                               amount: amount_sum,
	                               actual_amount: amount,
	                               check_fee: fee_object.try(:ach_fee_dollar),
	                               fee_perc: deduct_fee(fee_object.try(:ach_fee).to_f, amount),
	                               recipient: recipient,
	                               checkId: rand,
	                               description: description,
	                               order_type: :instant_ach,
	                               batch_date:Time.zone.now + 1.day,
	                               approved: false,
	                               account_type: bank_account_type,
	                               wallet_id: wallet.id,
	                               settled: :false,
	                               account_token: encrypted_account_info,
	                               send_via: send_via,
	                               account_number: account_number,
	                               routing_number: routing_number,
	                               location_id: location.id,
	                               amount_escrow: true,
	                               ach_international: ach_international)
	        location1 = location_to_parse(location)
	        merchant1 = merchant_to_parse(merchant)
	        tags = {
	            "fee_perc" => splits,
	            "location" => location1,
	            "merchant" => merchant1,
	            "send_check_user_info" => nil,
	            "send_via" => send_via
	        }
	        escrow_wallet = Wallet.check_escrow.first
	        escrow_user = escrow_wallet.try(:users).try(:first)
	        gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
	        total_fee = save_total_fee(splits, fee["fee"].to_f)
	        sender_dba=get_business_name(wallet)
	        transaction = merchant.transactions.create(
	            to: recipient,
	            status: "pending",
	            amount: amount,
	            sender: merchant,
	            sender_name: sender_dba,
	            sender_wallet_id: wallet.id,
	            receiver_id: escrow_user.try(:id),
	            receiver_wallet_id: escrow_wallet.try(:id),
	            receiver_name: escrow_user.try(:name),
	            sender_balance: balance,
	            receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
	            action: 'transfer',
	            fee: total_fee.to_f,
	            net_amount: amount.to_f,
	            net_fee: total_fee.to_f,
	            total_amount: amount_sum.to_f,
	            gbox_fee: gbox_fee.to_f,
	            iso_fee: save_iso_fee(splits),
	            agent_fee: splits["agent"]["amount"].to_f,
	            affiliate_fee: splits["affiliate"]["amount"].to_f,
	            ip: get_ip,
	            main_type: "ACH",
	            tags: tags
	        )
	        user_info = fee.try(:[], "splits") || {}
	        withdraw = withdraw(amount,wallet.id, total_fee || 0, TypesEnumLib::GatewayType::ACH,account_number,send_via,user_info, nil, nil, nil,TypesEnumLib::GatewayType::Quickcard,nil,true)

	        if withdraw.present? && withdraw["actions"].present?
	          parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
	          save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
	          check.transaction_id = transaction.id
	          check.save
	          transaction.update(seq_transaction_id: withdraw["actions"].first["id"],
	                             status: "approved",
	                             privacy_fee: 0,
	                             tags: withdraw["actions"].first.tags,
	                             sub_type: withdraw["actions"].first.tags["sub_type"],
	                             timestamp: withdraw["timestamp"]
	          )
	           ActivityLog.log(nil,"ACH Created",current_user,params,response,"ACH Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","ach")
	          flash[:success] = "Successfully sent ACH request to Account ****#{ account_number.try(:last, 4) }"
	        else
	          flash[:error] = I18n.t 'merchant.controller.unaval_feature'
	        end
	      else
	        iso_agent_ach
	      end
	    rescue => exe
	      	flash[:error] = exe.message
	    ensure
	      	if flash[:error].blank? && flash[:success].blank?
	        	flash[:error] = I18n.t 'merchant.controller.except'
	      	end
	      	if flash[:error].blank?
	      		ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: ["**** #{check.account_number}"], ach_id: [check.id]
					else
						if flash[:error].include?('Error Code')
							str = flash[:error]
							message = str[16, str.length-1]
							code = str[0,15]
						else
							message = flash[:error]
							code = " Error Code #"
						end
	      		ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', error_code: code, message: message
	      	end
	      	# return redirect_back(fallback_location: root_path)
			end
			flash.discard
	end

   def setup_params
    if @user.merchant?
      location_bank = BankDetail.find(params[:bank_account_name])
      return [true,"Bank not added.\nPlease email support@greenboxpos.com with ACH Bank Information."] if location_bank.try(:bank_name).blank? || location_bank.try(:account_no).blank? || location_bank.try(:routing_no).blank? || location_bank.try(:bank_account_type).blank?
      return [true, I18n.t('api.errors.wallet_not_found')] if location_bank.blank?
      params[:bank_name] = location_bank.bank_name
      params[:bank_account_type] = location_bank.bank_account_type
      params[:bank_routing] = location_bank.routing_no
      params[:bank_account] = AESCrypt.decrypt("#{params[:location_id]}-#{ENV['ACCOUNT_ENCRYPTOR']}",location_bank.account_no)
      params[:confirm_bank_account] = AESCrypt.decrypt("#{params[:location_id]}-#{ENV['ACCOUNT_ENCRYPTOR']}",location_bank.account_no)
      params[:email_address] = @user.email
      return [false,nil]
    else
      params[:bank_name] = @user['system_fee']['bank_account_name']
      params[:bank_account_type] =  @user['system_fee']['bank_account_type']
      params[:bank_routing] = @user['system_fee']['bank_routing']
      params[:bank_account] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])
      params[:confirm_bank_account] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])
      params[:email_address] = @user.email
      return [false,nil]
    end
  end

end