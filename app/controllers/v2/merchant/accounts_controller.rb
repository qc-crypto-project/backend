class V2::Merchant::AccountsController < V2::Merchant::BaseController
  include V2::Merchant::AccountsHelper
  include Merchant::GiftcardsHelper
  include V2::Merchant::MaxmindHelper
  include WithdrawHandler
  require 'raas'

  def index
    conditions = []
    if params[:accounts_search].present?
      conditions = accounts_search_query(params[:accounts_search])
    end
    if params[:q].try(:[],:data_search).present?
      conditions = accounts_search_query(params[:q][:data_search])
    end
    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    if current_user.merchant? && current_user.merchant_id.nil?
      locations_ids = current_user.attached_locations
      if locations_ids.present?
        @locations = []
        @locations << Location.includes(:wallets).where(id: locations_ids).where(conditions)
        @locations << Location.includes(:wallets).where(merchant_id: current_user.id).where(conditions).order(created_at: :desc)
        @locations.flatten!
        @locations = Kaminari.paginate_array(@locations).page(params[:page]).per(page_size)
      else
        @locations = Location.where(merchant_id: current_user.id).eager_load(:wallets).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
      end
    elsif current_user.merchant? && current_user.merchant_id.present? && current_user.try(:permission).try(:admin?)
      @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact).eager_load(:wallets).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    else
      @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact).eager_load(:wallets).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    end
    @locationss = @locations.map do |l|
      wallets = l.wallets.primary.pluck(:id,:wallet_type,:slug)
      primary_wallet = wallets.flatten.first
      {
          name: locations_ids.present? && locations_ids.include?(l.id) ? "(M-#{l.merchant_id}) #{l.business_name}" : l.business_name,
          wallet_primary: primary_wallet,
          primary_slug: wallets.flatten.third,
          hold_in_rear_amount: HoldInRear.calculate_pending(primary_wallet),
      }
    end
    if @locationss.count == 1 && conditions.empty? && @locations.total_count == 1
      redirect_to v2_merchant_account_path(@locationss.first[:primary_slug])
    end
    cookies[:loader] = cookies[:loader].present? ? cookies[:loader].to_i + 1 : 1
    render partial: 'v2/merchant/accounts/index_accounts_div' if request.xhr?
  end

  def show
    @wallet = Wallet.includes(:location,:transaction_batches).includes(location: [:wallets, :fees, :bank_details]).friendly.find(params[:id])
    @location = @wallet.location
    @fee = @location.fees.try(:buy_rate).try(:first)
    @locationss = current_user.get_locations
    @hold = HoldInRear.calculate_pending_new(@location.id)
    # @next_release = @location.hold_in_rears.in_pending.where.not(release_date: Date.today + 1.day).first
    @next_release = @location.hold_in_rears.in_pending.where.not(release_date: Date.today).first
    @today_release = @location.hold_in_rears.where(release_date: Date.today).first

    location_wallets = @location.wallets
    @reserve = location_wallets.reserve.first
    @tip = location_wallets.tip.first
    @merchant_wallets=""
    if current_user.merchant_id.present?
      merchant = User.find(current_user.merchant_id)
    else
      merchant = current_user
    end
    first_date = Date.today.beginning_of_month
    second_date = Date.today
    @batches = @wallet.transaction_batches.sale.where("batch_date BETWEEN :first AND :second", first: first_date, second: second_date).order(batch_date: :desc)
    @total_net = @batches.first.try(:total_net).to_f - @batches.last.try(:starting_balance).to_f #if @batches.first.try(:total_net).to_f > 0
    # if @batches.blank?
    #   @batches = [manage_batches(@wallet.id, merchant.id, Date.today, "sale")]
    # elsif @batches.present?
    #   dates = @batches.pluck(:batch_date)
    #   unless dates.include?(Date.today)
    #     @batches << manage_batches(@wallet.id, merchant.id, Date.today, "sale", @batches.first.total_net)
    #     @batches.sort.reverse
    #   end
    # end
    @debit_cards = current_user.try(:cards).try(:instant_pay_cards)
    if current_user.merchant?
      @merchant_wallets= current_user.wallets.primary if current_user.merchant_id == nil
      if @merchant_wallets.blank? && current_user.merchant_id != nil
        @merchant_wallets= current_user.wallets.primary.reject{|v| v.location_id.nil?}
      end
      @transfer = Transfer.new(amount: "")
    end
  end

  def sales_batch
    if params[:custom].present?
      params[:first_date] = DateTime.parse(params[:first_date])
      params[:second_date] = DateTime.parse(params[:second_date])
    end
    @wallet = Wallet.where(id: params[:wallet_id]).last
    @batches = TransactionBatch.sale.where(merchant_wallet_id: params[:wallet_id].to_i).where("batch_date BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date).order(batch_date: :desc)
    @total_net = @batches.first.try(:total_net).to_f - @batches.last.try(:starting_balance).to_f
    render partial: 'v2/merchant/accounts/sales_batch'
  end

  def batch_transactions
    @types = [{value:"ACH",view:"ACH"},{value:"account_transfer",view:"Account Transfer"},{value: "cbk_dispute_accepted", view: "CBK Dispute Accepted"},{value: "cbk_fee", view: "CBK Fee"},{value: "cbk_hold", view: "CBK Hold"},{value: "cbk_lost", view: "CBK Lost"},{value: "cbk_won", view: "CBK Won"},{value:"credit_card",view:"Credit Card"},{value:TypesEnumLib::TransactionViewTypes::DebitCard,view:"Debit Card"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"Failed Check",view:"Failed Check"},{value:"Failed ACH",view:"Failed ACH"},{value:"Failed Push To Card",view:"Failed Push To Card"},{value:"giftcard_purchase",view:"Giftcard Purchase"},{value:"invoice-credit",view:"Invoice - Credit Card"},{value:"invoice-qc",view:"Invoice - QC"},{value:"Invoice - RTP",view:"Invoice - RTP"},{value: "Misc Fee", view: "Misc Fee"},{value: "Service Fee", view: "Monthly Fee"},{value:"debit_charge",view:"PIN Debit"},{value:"instant_pay",view:"Push to Card"},{value:"refund",view:"Refund"},{value:"Reserve_Money_Return",view:"Reserve Money Return"},{value:"Reserve money deposit",view:"Reserve Money Deposit"},{value: "retrievel_fee", view: "Retrieval Fee"},{value:"RTP",view:"RTP"},{value:"Send Check",view:"Send Check"},{value: "Subscription Fee", view: "Subscription Fee"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"Void ACH",view:"Void ACH"},{value:"Void Check",view:"Void Check"},{value:"Void Push To Card",view:"Void P2C"},{value:"QCP Secure",view:"QCP Secure"},{value:"QC Secure",view:"QC Secure"}]
    @decline_types = [{value:"Sale Issue",view:"Sale Issue"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"credit_card",view:"Credit Card"},{value:"3DS",view:"3DS"},{value:"Invoice - RTP",view:"Invoice - RTP"},{value:"RTP",view:"RTP"}]
    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    @batch = TransactionBatch.where(id: params[:batch_id]).first
    @wallet = Wallet.friendly.find(params[:wallet_id])
    @location = @wallet.location
    if params[:query].present?
      @transactions = batch_tx_search(params[:query],@batch.id, @wallet.id,params[:type],nil,nil,nil,nil,nil,nil,page_size, params[:page])
    else
      @transactions = @batch.batch_transactions.order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    end
  end

  def get_accounts
    if current_user.merchant? && current_user.merchant_id.nil?
      locations_ids = current_user.attached_locations
      if locations_ids.present?
        @locations = []
        @locations << Location.includes(:wallets).where(id: locations_ids)
        @locations << Location.includes(:wallets).where(merchant_id: current_user.id).order(created_at: :desc)
        @locations.flatten!
      else
        @locations = Location.where(merchant_id: current_user.id).eager_load(:wallets).order(created_at: :desc)
      end
    elsif current_user.merchant? && current_user.merchant_id.present? && current_user.try(:permission).admin?
      @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact).eager_load(:wallets).order(created_at: :desc)
    else
      @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact).eager_load(:wallets).order(created_at: :desc)
    end
    @locationss = @locations.map do |l|
      wallets = l.wallets.primary.pluck(:id,:wallet_type,:slug)
      primary_wallet = wallets.flatten.first
      {
          name: locations_ids.present? && locations_ids.include?(l.id) ? "(M-#{l.merchant_id}) #{l.business_name}" : l.business_name,
          wallet_primary: primary_wallet,
          primary_slug: wallets.flatten.third,
          updated_at: (Date.today - l.updated_at.to_date).to_i

      }
    end
    render partial: "v2/merchant/accounts/get_accounts"

  end

  def graph_data
    batches = TransactionBatch.where(merchant_wallet_id: params[:wallet_id]).where("batch_date BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date).order(batch_date: :asc).group_by{|e| e.batch_type}
    if batches.present?
      sale_batches = batches["sale"]
      sale_amounts = sale_batches.try(:pluck, :total_amount).try(:compact).try(:sum).to_f
      sale_amount = number_to_currency(number_with_precision(sale_amounts.to_f, precision: 2, delimiter: ','))
      sale_count = sale_batches.try(:pluck,:total_trxs).try(:compact).try(:sum).to_i
      sale_data = []
      if sale_batches.present?
        sale_batches.try(:map) do |e|
          sale_data.push({y: e.total_amount.to_f, x: e.batch_date.strftime("%b %d"), count: e.total_trxs})
          # sale_data.push({y: e.total_amount, x: e.batch_date.strftime("%m/%d/%Y"), count: e.total_trxs})
        end
      else
        # sale_data = [{y: 0, x: nil, count: 0}]
        sale_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
      end
      avg = number_to_currency(number_with_precision(sale_amounts.to_f/sale_count.to_i, precision: 2, delimiter: ','))
      avg_data = []
      if sale_batches.present?
        sale_batches.try(:map) do |e|
          # avg_data.push({y: e.total_amount.to_f/e.total_trxs.to_i, x: e.batch_date.strftime("%m/%d/%Y"), count: e.total_trxs})
          avg_data.push({y: e.total_trxs.to_i > 0 ? e.total_amount.to_f/e.total_trxs.to_i : 0, x: e.batch_date.strftime("%b %d"), count: e.total_trxs})
        end
      else
        # avg_data = [{y: 0, x: nil, count: 0}]
        avg_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
      end

      first_date = params[:first_date].to_date
      second_date = params[:second_date].to_date
      sale_txn_ids = TransactionBatchesTransactions.where(transaction_batch_id:sale_batches.pluck(:id)).pluck(:transaction_id)
      sale_txns = Transaction.includes(:card).where(id: sale_txn_ids).group_by{|e| e.try(:card).try(:brand).try(:downcase)} if sale_txn_ids.present?
      visa = sale_txns.try(:[],'visa')
      visa_group = visa.group_by{|e| e.try(:card).try(:card_type)} if visa.present?
      visa_sum = visa.pluck(:total_amount).sum if visa.present?
      visa_credit = visa_group["credit"].pluck(:total_amount).sum if visa_group.present? && visa_group["credit"].present?
      visa_debit = visa_group["debit"].pluck(:total_amount).sum if visa_group.present? && visa_group["debit"].present?

      master = sale_txns.try(:[],'mastercard')
      master_group = master.group_by{|e| e.try(:card).try(:card_type)} if master.present?
      master_sum = master.pluck(:total_amount).sum if master.present?
      master_credit = master_group["credit"].pluck(:total_amount).sum if master_group.present? && master_group["credit"].present?
      master_debit = master_group["debit"].pluck(:total_amount).sum if master_group.present? && master_group["debit"].present?

      amex = sale_txns.try(:[],'amex')
      amex_group = amex.group_by{|e| e.try(:card).try(:card_type)} if amex.present?
      amex_sum = amex.pluck(:total_amount).sum if amex.present?
      amex_credit = amex_group["credit"].pluck(:total_amount).sum if amex_group.present? && amex_group["credit"].present?
      amex_debit = amex_group["debit"].pluck(:total_amount).sum if amex_group.present? && amex_group["debit"].present?

      discover = sale_txns.try(:[],'discover')
      discover_group = discover.group_by{|e| e.try(:card).try(:card_type)} if discover.present?
      discover_sum = discover.pluck(:total_amount).sum if discover.present?
      discover_credit = discover_group["credit"].pluck(:total_amount).sum if discover_group.present? && discover_group["credit"].present?
      discover_debit = discover_group["debit"].pluck(:total_amount).sum if discover_group.present? && discover_group["debit"].present?

      cbks = DisputeCase.charge_back.where(merchant_wallet_id: params[:wallet_id]).where("recieved_date BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date).order(recieved_date: :asc)
      cbk_count = cbks.try(:count)
      cbk_perc = cbk_count.to_i/sale_count.to_i * 100 if sale_count.to_i > 0
      cbk_data = []
      if cbks.present?
        cbks.try(:map) do |e|
          # cbk_data.push({y: e.amount, x: e.recieved_date.strftime("%m/%d/%Y")})
          cbk_data.push({y: e.amount, x: e.recieved_date.strftime("%b %d")})
        end
      else
        # cbk_data = [{y: 0, x: nil}]
        cbk_data = [{y: 0, x: DateTime.now.strftime("%b %d")}]
      end

      refund_batches = batches["refund"]
      refund_amounts = refund_batches.try(:pluck, :total_amount)
      refund_amount = number_to_currency(number_with_precision(refund_amounts.try(:compact).try(:sum).to_f, precision: 2, delimiter: ','))
      refund_count = refund_batches.try(:pluck, :total_trxs).try(:sum).to_i
      refund_data = []
      if refund_batches.present?
        refund_batches.try(:map) do |e|
          refund_data.push({y: e.total_amount, x: e.batch_date.strftime("%b %d"), count: e.total_trxs})
          # refund_data.push({y: e.total_amount, x: e.batch_date.strftime("%m/%d/%Y"), count: e.total_trxs})
        end
      else
        # refund_data = [{y: 0, x: nil, count: 0}]
        refund_data = [{y: 0, x: DateTime.now.strftime("%b %d"), count: 0}]
      end

      invoice_batches = sale_batches
      invoice_amounts = invoice_batches.try(:pluck, :invoice_amount)
      invoice_amount = number_to_currency(number_with_precision(invoice_amounts.try(:compact).try(:sum).to_f, precision: 2, delimiter: ','))
      invoice_count = invoice_batches.try(:pluck,:invoice_count).try(:compact).try(:sum).to_i
      invoice_data = []
      if sale_batches.present?
        sale_batches.try(:map) do |e|
          invoice_data.push({y: e.invoice_amount, x: e.batch_date.strftime("%b %d"), count: e.invoice_count})
          # invoice_data.push({y: e.invoice_amount, x: e.batch_date.strftime("%m/%d/%Y"), count: e.invoice_count})
        end
      else
        invoice_data = [{y: 0, date: nil, count: 0}]
      end
      render json: {
          cbk_count: cbk_count.to_i,
          cbk_perc: cbk_perc,
          cbk_data: cbk_data,
          refund_amount: refund_amount,
          refund_count: refund_count.to_i,
          refund_data: refund_data,
          invoice_amount: invoice_amount,
          invoice_count: invoice_count,
          avg: avg,
          avg_data: avg_data,
          sale_amount: sale_amount,
          sale_count: sale_count,
          first_date: first_date.strftime("%B %d"),
          second_date: second_date.strftime("%B %d"),
          visa_amount: number_to_currency(number_with_precision(visa_sum.to_f, precision: 2, delimiter: ',')),
          visa_credit: visa_credit,
          visa_debit: visa_debit,
          master_amount: number_to_currency(number_with_precision(master_sum.to_f, precision: 2, delimiter: ',')),
          master_credit: master_credit,
          master_debit: master_debit,
          amex_amount: number_to_currency(number_with_precision(amex_sum.to_f, precision: 2, delimiter: ',')),
          amex_credit: amex_credit,
          amex_debit: amex_debit,
          discover_amount: number_to_currency(number_with_precision(discover_sum.to_f, precision: 2, delimiter: ',')),
          discover_credit: discover_credit,
          discover_debit: discover_debit,
          sale_data: sale_data,
          invoice_data: invoice_data
      }
    else
      render json: {cbk_count: 0,
                    cbk_perc: 0,
                    refund_amount: 0,
                    refund_count: 0,
                    invoice_amount: 0,
                    invoice_count: 0,
                    avg: 0,
                    sale_amount: 0,
                    sale_count: 0,
                    sale_data: 0,
                    invoice_data: 0,
                    refund_data: 0,
                    avg_data: 0,
                    cbk_data: 0,
                    visa_amount: number_to_currency(number_with_precision(0, precision: 2, delimiter: ',')),
                    visa_credit: nil,
                    visa_debit: nil,
                    master_amount: number_to_currency(number_with_precision(0, precision: 2, delimiter: ',')),
                    master_credit: nil,
                    master_debit: nil,
                    amex_amount: number_to_currency(number_with_precision(0, precision: 2, delimiter: ',')),
                    amex_credit: nil,
                    amex_debit: nil,
                    discover_amount: number_to_currency(number_with_precision(0, precision: 2, delimiter: ',')),
                    discover_credit: nil,
                    discover_debit: nil,
                    first_date: params[:first_date].to_date.strftime("%B %d"),
                    second_date: params[:second_date].to_date.strftime("%B %d")}
    end
  end

  def get_ach
    @achs = TangoOrder.instant_ach.includes(:location).where(wallet_id: params[:wallet_id]).limit(10).order(created_at: :desc)
    render partial: "v2/merchant/accounts/ach_body"
  end

  def get_p2c
    @p2cs = TangoOrder.instant_pay.includes(:location).where(wallet_id: params[:wallet_id]).limit(10).order(created_at: :desc)
    render partial: "v2/merchant/accounts/p2c_body"

  end

  def get_check
    @checks = TangoOrder.check.includes(:location).where(wallet_id: params[:wallet_id]).limit(10).order(created_at: :desc)
    render partial: "v2/merchant/accounts/check_body"

  end

  def get_giftcard
    begin
      client = TangoClient.instance
      # orders = client.orders
      customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
      account_identifier = ENV["ACCOUNT_IDENTIFIER"]
      collect = Hash.new
      collect['account_identifier'] = account_identifier
      collect['customer_identifier'] = customer_identifier
      elements_per_block = 100
      collect['elements_per_block'] = elements_per_block
      page = 0
      collect['page'] = page

      orders = client.orders
      if current_user.merchant? && current_user.merchant_id.blank?
        user_ids = [current_user.id, current_user.try(:sub_merchants).try(:pluck, :id)].try(:flatten)
      else
        user_ids = [current_user.id, current_user.merchant_id]
      end
      tangoIntialOrders = TangoOrder.where(user_id: user_ids).order(created_at: :desc).select{|v| v.utid != nil}
      tangos = tangoIntialOrders.map do |order|
        if order.status.blank?
          object = orders.get_order(order.order_id)
          order.update(status: object.status)
        end
      end
      @giftcards = Kaminari.paginate_array(tangoIntialOrders).page(params[:page]).per(10)
      render partial: "v2/merchant/accounts/giftcard_body"
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.try(:errors).present? && exc.try(:errors).try(:count).to_i > 0
        message = exc.errors.first.message
      end
      flash[:danger] = message
    end
  end

  def get_giftcard_data
    wallet = Wallet.find(params[:wallet_id])
    location = wallet.location
    if location.present?
      location_block = false
      if location.try(:is_block)
        location_block = true
      end
      balance = SequenceLib.balance(wallet.id) - HoldInRear.calculate_pending_new(location.id)
      fees = location.fees.buy_rate.first
      if params[:type] == 'giftcard'
        fee_dollar = fees.giftcard_fee.to_f
        giftcard_block = false;
        if location.try(:block_giftcard)
          giftcard_block = true;
        end
      end
    end
    render json: {
        balance: number_with_precision(balance.to_f, precision: 2, delimiter: ','),
        fee_dollar: fee_dollar,
        location_block: location_block,
        location_block_giftcard: giftcard_block
    }
  end

  def show_merchant_balance
    source = params[:source] if params[:source].present?
    if source.present?
      balance = show_balance(source)
      if balance.present?
        balance = balance - HoldInRear.calculate_pending(source)
        render status: 200 , json:{:balance => number_with_precision(balance,precision:2, delimiter: ','),:balance_val => balance}
      else
        render status: 200 , json:{:balance => ""}
      end
    else
      render status: 200 , json:{:balance => ""}
    end
  end

  def account_transfer
    if new_transfer_params[:amount].to_f <= 0
      ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
      flash[:error] = I18n.t 'errors.withdrawl.amount_must_greater_than_zero'
      return redirect_back(fallback_location: root_path)
    end
    if new_transfer_params[:from_wallet] == new_transfer_params[:to_wallet]
      ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
      flash[:error] = I18n.t 'merchant.controller.cant_same_walt'
      return redirect_back(fallback_location: root_path)
    end
    balance=show_balance(new_transfer_params[:from_wallet])
    balance= balance - HoldInRear.calculate_pending(new_transfer_params[:from_wallet])
    if balance < new_transfer_params[:amount].to_f
      ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
      flash[:notification_msg] = I18n.t('merchant.controller.insufficient_balance')
      return redirect_back(fallback_location: root_path)
    end
    timezone= TIMEZONE
    params[:transfer]["transfer_timezone"]=timezone
    transfer_date=new_transfer_params[:transfer_date] if new_transfer_params[:transfer_date].present?
    params[:transfer][:transfer_date]=Date.strptime(transfer_date,"%m/%d/%Y") if params[:transfer][:transfer_date].present?
    transfer=current_user.transfers.new(new_transfer_params)
    transfer.status="pending"
    transfer.save
    transfer_details = {
        amount: new_transfer_params[:amount],
        memo: new_transfer_params[:memo].present? ? new_transfer_params[:memo] : '',
        date: transfer_date,
        transfer_timezone: timezone
    }
    ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'fifty'
    sender_dba=get_business_name(Wallet.find_by(id:new_transfer_params[:from_wallet]))
    receiver_dba=get_business_name(Wallet.find_by(id:new_transfer_params[:to_wallet]))
    db_transaction = @user.transactions.create(
        to: new_transfer_params[:to_wallet],
        from: new_transfer_params[:from_wallet],
        status: "pending",
        action: 'transfer',
        sender: @user,
        receiver_id: @user.try(:id),
        sender_name: sender_dba,
        receiver_name: receiver_dba,
        sender_wallet_id: new_transfer_params[:from_wallet],
        receiver_wallet_id: new_transfer_params[:to_wallet],
        amount: new_transfer_params[:amount].to_f,
        net_amount: new_transfer_params[:amount].to_f,
        total_amount:  new_transfer_params[:amount].to_f,
        ip: get_ip,
        main_type: "account_transfer",
        tags: transfer_details,
        sender_balance: balance,
        receiver_balance: SequenceLib.balance(new_transfer_params[:to_wallet].to_i)
    )
    transaction=SequenceLib.transfer(number_with_precision(new_transfer_params[:amount],precision:2),new_transfer_params[:from_wallet],new_transfer_params[:to_wallet],"account_transfer",0,nil,TypesEnumLib::GatewayType::Quickcard,nil,current_user,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
    ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'finalize'
    if transaction.present? && transaction.actions.first.id.present?
      #= creating block transaction
      parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
      db_transaction.update(seq_transaction_id: transaction.actions.first.id, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
      transfer.sequence_id =transaction.actions.first.id
      transfer.status = "completed"
      transfer.save
      ActivityLog.log(nil,"Transfer Successful", current_user, params, response,"Account Transfer Between [#{sender_dba}] to [#{receiver_dba}] for [$#{new_transfer_params[:amount].to_f}]","accounts")
      ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'done',id: transaction.id[0..5]
      msg = I18n.t 'merchant.controller.notification_transfer'
      ActivityLog.log(16, msg,current_user,params)
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_transfer'
      return redirect_back(fallback_location: root_path)
    else
      ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_cannot_create_transfer'
      msg = "#{I18n.t 'merchant.controller.notification_cannot_create_transfer'} ---- Transfer Details: #{{id: transfer.id,status: transfer.status}.to_json} ----Transaction Details:  #{{id: db_transaction.slug,seq_id: db_transaction.seq_transaction_id}.to_json}"
      ActivityLog.log(17, msg,current_user,params)
      SlackService.notify("*Cannot Create account transfer -* \n Transfer Details: \`\`\` #{{id: transfer.id,status: transfer.status}.to_json} \`\`\` Transaction Details: \`\`\` #{{id: db_transaction.slug,seq_id: db_transaction.seq_transaction_id}.to_json} \`\`\` App: \`\`\` #{{URL: request.base_url, HOST: request.host, ACTION: "#{params[:controller]}/#{params[:action]}"}.to_json}  \`\`\` Params: \`\`\`#{request.params.to_json}\`\`\` User: \`\`\`#{@user.present? ? {id: @user.id, email: @user.email, role: @user.role }.to_json : 'No User Present!'}  \`\`\`")
      return redirect_back(fallback_location: root_path)
    end
  end

  def tip_transfer
    from_wallet= Wallet.find params[:from_wallet]
    sub_merchants = {"#{current_user.id}" => params[:amount] }
    balance = show_balance(from_wallet.id)
    transfer_details = {
        amount: params[:amount],
        memo: params[:tip_memo]
    }
    if balance > 0 && balance >= params[:amount].to_f
      unless from_wallet.nil?
        unless sub_merchants.blank?
          begin
            sub_merchants.each do |id,tip|
              if not tip.blank? or tip.nil?
                merchant= current_user
                if merchant
                  wallet = from_wallet.try(:location).try(:wallets).try(:primary).try(:first)
                  raise StandardError.new(I18n.t('api.errors.wallet_not_found')) if wallet.blank?
                  tr_type = get_transaction_type(TypesEnumLib::TransactionType::TipTransfer)
                  main_merchant = @user.merchant_id.present? ? User.find_by(id: merchant.merchant_id) : @user
                  local_transaction = Transaction.create(
                      amount: "#{tip.to_f}",
                      status: "pending",
                      sender_id: main_merchant.id,
                      receiver_id: merchant.id,
                      action: "transfer",
                      main_type: tr_type.first,
                      total_amount: tip.to_f,
                      net_amount: tip.to_f,
                      sender_wallet_id: from_wallet.id,
                      receiver_wallet_id: wallet.id,
                      sender_name: "Tip Account",
                      receiver_name: wallet.name,
                      sender_balance: balance,
                      receiver_balance: SequenceLib.balance(wallet.id)
                  )
                  transfer = SequenceLib.transfer(tip.to_f,from_wallet.id,wallet.id,tr_type.first,0,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil, nil, nil, nil, get_ip,nil ,nil ,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
                  ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'finalize'
                  if transfer.present?
                    @seq_id = transfer.actions.first.id
                    local_transaction.update(status: "approved",seq_transaction_id: transfer.actions.first.id, tags:transfer.actions.first.tags, timestamp: transfer.timestamp)
                    parsed_transactions = parse_block_transactions(transfer.actions, transfer.timestamp)
                    save_block_trans(parsed_transactions) if parsed_transactions.present?
                  end
                end
              end
            end
            ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'done',id: @seq_id[0..5]
            msg = I18n.t("merchant.controller.transfer_main_wallet_successful")
            ActivityLog.log('success', msg,current_user,params)
            flash[:success] = msg
          rescue StandardError=> ex
            ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
            msg = ex.message
            ActivityLog.log('error', msg,current_user,params)
            flash[:danger] = ex.message
          end
        else
          ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
          flash[:error] ="Please select at least one user!"
        end
      else
        ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
        flash[:danger] = "Wallet Missing!"
      end
    else
      ActionCable.server.broadcast "transfer_channel_#{session[:session_id]}", channel_name: "transfer_channel_#{session[:session_id]}", action: 'error'
      flash[:error] = I18n.t 'merchant.controller.insufficient_balance'
    end
    redirect_back(fallback_location: root_path)
  end

  def get_location_data
    location = Location.includes(:fees,:wallets).where(id: params[:location_id]).first
    if location.present?
      location_block = false
      location_block_withdraw = false
      if location.try(:is_block) ||  location.try(:block_withdrawal)
        if location.try(:is_block)
          location_block = true
        elsif location.try(:block_withdrawal)
          location_block_withdraw = true
        end
      end
      total_count = 0
      fees = location.fees.buy_rate.first
      wallet = location.wallets.primary.first.id
      balance = SequenceLib.balance(wallet) - HoldInRear.calculate_pending_new(params[:location_id])
      if params[:type] == "ach"
        fee_dollar = fees.ach_fee_dollar
        fee_perc = fees.ach_fee
        limit = location.ach_limit.to_f
        limit_type = location.ach_limit_type

        if location.ach_limit_type == "Day"
          date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          if current_user.merchant_id.present? #&& current_user.submerchant_type == 'admin_user'  ##  submerchant_type column are nil
            instant_achs = current_user.parent_merchant.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").instant_ach
          else
            instant_achs = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").instant_ach
          end
          total_count = instant_achs.sum(:actual_amount)
        end
        multiple_bank_details = []
        banks = location.bank_details
        if banks.present?
          banks = banks.map { |loc| loc if loc.account_no.present?}.compact
          banks.try(:map) do |loc|
            if loc.account_no.present?
              d_account =  "**** #{(AESCrypt.decrypt("#{location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", loc.account_no)).last(4)}"
              multiple_bank_details << {id: loc.id , account: d_account}
            end
          end
        end
      elsif params[:type] == "check"
        fee_dollar = fees.send_check.to_f
        fee_perc = fees.redeem_check.to_f
        limit = location.check_limit.to_f
        limit_type = location.check_limit_type
        if limit_type == "Day"
          date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          if current_user.merchant_id.present? #&& current_user.submerchant_type == 'admin_user'  ##  submerchant_type column are nil
            instant_check = current_user.parent_merchant.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",params[:location_id],date,"VOID",[0,5]).where(order_type: 'check',void_transaction_id: nil)
          else
            instant_check = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",params[:location_id],date,"VOID",[0,5]).where(order_type: 'check',void_transaction_id: nil)
          end
          total_count = instant_check.sum(:actual_amount)
        end
      elsif params[:type] == 'p2c'
        fee_dollar = fees.dc_deposit_fee_dollar.to_f
        fee_perc = fees.dc_deposit_fee.to_f
        limit = location.push_to_card_limit.to_f
        limit_type = location.push_to_card_limit_type
        if limit_type == "Day"
          startdate = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc
          enddate = DateTime.now.in_time_zone("Pacific Time (US & Canada)").end_of_day.utc
          date=startdate..enddate
          if current_user.merchant_id.present? #&& current_user.submerchant_type == 'admin_user'  ##  submerchant_type column are nil
            instant_achs = current_user.parent_merchant.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.status !=?",params[:location_id],"VOID").where(order_type: 'instant_pay',void_transaction_id: nil).where(created_at: date)
          else
            instant_achs = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.status !=?",params[:location_id],"VOID").where(order_type: 'instant_pay',void_transaction_id: nil).where(created_at: date)
          end
          total_count = instant_achs.sum(:actual_amount)
        end
      end
    end
    render json: {
        fee_dollar: fee_dollar,
        fee_perc: fee_perc,
        limit: limit,
        limit_type: limit_type,
        total_count: total_count,
        balance: balance.to_f,
        location_block: location_block,
        location_block_withdraw: location_block_withdraw,
        banks: multiple_bank_details,
    }
  end

  def reserve_transactions
    conditions = []
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = merchant_transactions_search_query(params[:query],params[:query][:offset])
    end
    @filters = [10,25,50,100]
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @wallet = Wallet.friendly.find params[:wallet_id]
    @transactions = BlockTransaction.joins(:sender).where('(sender_wallet_id = ? OR receiver_wallet_id = ?) AND block_transactions.main_type IN (?)', @wallet.id, @wallet.id, ["Reserve Money Return", "Reserve Money Deposit","Payed Tip","Account Transfer","Sale Tip", "Tip transfer"]).where(conditions).order(timestamp: :desc).per_page_kaminari(params[:page]).per(page_size)
  end

  def tip_transactions
    conditions = []
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = merchant_transactions_search_query(params[:query],params[:query][:offset])
    end
    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    @wallet = Wallet.friendly.find params[:wallet_id]
    @transactions = BlockTransaction.joins(:sender).where('(sender_wallet_id = ? OR receiver_wallet_id = ?) AND block_transactions.main_type IN (?)', @wallet.id, @wallet.id, ["Reserve Money Return", "Reserve Money Deposit","Payed Tip","Account Transfer","Sale Tip", "Tip transfer"]).where(conditions).order(timestamp: :desc).per_page_kaminari(params[:page]).per(page_size)
  end

  def account_transactions
    @types = [{value:"ACH",view:"ACH"},{value:"account_transfer",view:"Account Transfer"},{value: "cbk_dispute_accepted", view: "CBK Dispute Accepted"},{value: "cbk_fee", view: "CBK Fee"},{value: "cbk_hold", view: "CBK Hold"},{value: "cbk_lost", view: "CBK Lost"},{value: "cbk_won", view: "CBK Won"},{value:"credit_card",view:"Credit Card"},{value:TypesEnumLib::TransactionViewTypes::DebitCard,view:"Debit Card"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"Failed Check",view:"Failed Check"},{value:"Failed ACH",view:"Failed ACH"},{value:"Failed_Ach_Fee",view:"Failed ACH Fee"},{value:"Failed Push To Card",view:"Failed Push To Card"},{value:"giftcard_purchase",view:"Giftcard Purchase"},{value:"Invoice - Credit Card",view:"Invoice - Credit Card"},{value:"Invoice - Debit Card",view:"Invoice - Debit Card"},{value:"Invoice Qc",view:"Invoice - QC"},{value:"Invoice - RTP",view:"Invoice - RTP"},{value: "Misc Fee", view: "Misc Fee"},{value: "Service Fee", view: "Monthly Fee"},{value:"debit_charge",view:"PIN Debit"},{value:"instant_pay",view:"Push to Card"},{value:"refund",view:"Refund"},{value: TypesEnumLib::TransactionType::RefundFailed,view: "Refund Failed"},{value:"RTP",view:"RTP"},{value:"Reserve_Money_Return",view:"Reserve Money Return"},{value:"Reserve money deposit",view:"Reserve Money Deposit"},{value: "retrievel_fee", view: "Retrieval Fee"},{value:"Send Check",view:"Send Check"},{value: "Subscription Fee", view: "Subscription Fee"},{value:"Tip Transfer",view:"Tip Transfer"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"Void ACH",view:"Void ACH"},{value:"Void Check",view:"Void Check"},{value:"Void Push To Card",view:"Void P2C"},{value:"QCP Secure",view:"QCP Secure"},{value:"QC Secure",view:"QC Secure"}]
    @decline_types = [{value:"Sale Issue",view:"Sale Issue"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"credit_card",view:"Credit Card"},{value:"3DS",view:"3DS"},{value:"Invoice - RTP",view:"Invoice - RTP"},{value:"RTP",view:"RTP"}]

    @filters = [10,25,50,100]
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    @wallet = Wallet.friendly.find params[:wallet_id]
    @location = @wallet.try(:location)
    if params[:search].present?
      conditions = account_transactions_status_search(params[:search])
    end
    if params[:query].present?
      @transactions = searching_merchant_local_transaction(params[:query], @wallet.id,params[:type],nil,nil,nil,nil,nil,nil,page_size, params[:page])
    else
      if params[:type] == "decline"
        location_id = @location.try(:id)
        @transactions = Transaction.specific_declines(page_size, @wallet.id, location_id).per_page_kaminari(params[:page]).per(page_size)
      else
        if params[:search].present? && params[:search][:no_select]
          @transactions = []
          @transactions = Kaminari.paginate_array(@transactions).page(params[:page]).per(page_size)
        else
          @transactions = Transaction.specific_approved(page_size, @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
        end
      end
    end
    render partial: "v2/merchant/shared/account_transactions_datatable" if request.xhr?
  end

  def create_ach
    begin
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.amount_not_zero.title')  ,message: I18n.t('error_codes.account.amount_not_zero.description'), type: "ach" if params[:amount].to_f <= 0
      location = Location.joins(:wallets, :fees).where(id: params[:location_id]).last
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.account_not_exist.title'), message: I18n.t('error_codes.account.account_not_exist.description'), type: "ach" if location.blank?
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.blocked_ach.title'), message: I18n.t('error_codes.account.blocked_ach.description') , type: "ach" if location.block_ach?

      merchant = current_user
      if merchant.oauth_apps.present?
        oauth_app = merchant.oauth_apps.first
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.blocked_user.title'), message: I18n.t('error_codes.account.blocked_user.description') , type: "ach" if oauth_app.is_block && merchant.get_locations.uniq.map {|loc| loc.block_ach}.all?
      end
      wallet = location.wallets.primary.first
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.wallet_not_exist.title'), message: I18n.t('error_codes.account.wallet_not_exist.description'), type: "ach" if wallet.blank?

      amount = params[:amount].to_f

      bank = BankDetail.where(id: params[:bank_id]).last
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.no_bank_found.title'), message: I18n.t('error_codes.account.no_bank_found.description'), type: "ach" if bank.blank?

      bank_name = bank.try(:bank_name)
      recipient = merchant.merchant? && merchant.merchant_id.present? ? merchant.try(:parent_merchant).try(:email) : merchant.email
      account_number = AESCrypt.decrypt("#{location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", bank.account_no)
      routing_number = bank.routing_no
      bank_account_type = bank.bank_account_type
      # description = params[:description]
      description = params[:ach_memo]
      send_via = "ACH"

      #= calculate fee start
      location_fee = location.fees if location.present?
      fee_object = location_fee.buy_rate.first if location_fee.present?
      fee_class = Payment::FeeCommission.new(merchant, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::ACH)
      fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::ACH)
      splits = fee["splits"]
      if splits.present?
        agent_fee = splits["agent"]["amount"]
        iso_fee = splits["iso"]["amount"]
        iso_balance = show_balance(location.iso.wallets.primary.first.id)
        if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
          if iso_balance < iso_fee
            return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: 'Error Code ISO-0005', message: 'ACH Creation Failed!', type: "ach"
          end
        else
          if iso_balance + iso_fee < agent_fee.to_f
            return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: 'Error Code ISO-0006',message: 'ACH Creation Failed!', type: "ach"
          end
        end
      end
      #= calculate fee end
      amount_sum = amount + fee.try(:[],"fee").to_f
      balance = show_balance(wallet.id).try(:to_f)
      pending_amount = HoldInRear.calculate_pending(wallet.id)
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.insufficient_balance.title'), message: I18n.t('error_codes.account.insufficient_balance.description'), type: "ach" if balance - pending_amount < amount_sum

      card_info = {
          :account_number => account_number,
          :account_type => bank_account_type
      }
      card_info = card_info.to_json
      account_number = account_number.last(4)
      encrypted_account_info = AESCrypt.encrypt("#{merchant.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{wallet.id}", card_info)
      rand = "#{rand(100..999)}#{Time.now.to_i.to_s}"
      ach_international = location.ach_international
      check = TangoOrder.new(user_id: merchant.id,
                             name: bank_name,
                             status: "PENDING",
                             amount: amount_sum,
                             actual_amount: amount,
                             check_fee: fee_object.try(:ach_fee_dollar),
                             fee_perc: deduct_fee(fee_object.try(:ach_fee).to_f, amount),
                             recipient: recipient,
                             checkId: rand,
                             description: description,
                             order_type: :instant_ach,
                             batch_date:Time.zone.now + 1.day,
                             approved: false,
                             account_type: bank_account_type,
                             wallet_id: wallet.id,
                             settled: :false,
                             account_token: encrypted_account_info,
                             send_via: send_via,
                             account_number: account_number,
                             routing_number: routing_number,
                             location_id: location.id,
                             amount_escrow: true,
                             ach_international: ach_international)
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'fifty', type: "ach"
      location1 = location_to_parse(location)
      merchant1 = merchant_to_parse(merchant)
      tags = {
          "fee_perc" => splits,
          "location" => location1,
          "merchant" => merchant1,
          "send_check_user_info" => nil,
          "send_via" => send_via
      }
      escrow_wallet = Wallet.check_escrow.first
      escrow_user = escrow_wallet.try(:users).try(:first)
      gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
      total_fee = save_total_fee(splits, fee["fee"].to_f)
      sender_dba=get_business_name(wallet)
      transaction = merchant.transactions.create(
          to: recipient,
          status: "pending",
          amount: amount,
          sender: merchant,
          sender_name: sender_dba,
          sender_wallet_id: wallet.id,
          receiver_id: escrow_user.try(:id),
          receiver_wallet_id: escrow_wallet.try(:id),
          receiver_name: escrow_user.try(:name),
          sender_balance: balance,
          receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
          action: 'transfer',
          fee: total_fee.to_f,
          net_amount: amount.to_f,
          net_fee: total_fee.to_f,
          total_amount: amount_sum.to_f,
          gbox_fee: gbox_fee.to_f,
          iso_fee: save_iso_fee(splits),
          agent_fee: splits["agent"]["amount"].to_f,
          affiliate_fee: splits["affiliate"]["amount"].to_f,
          ip: get_ip,
          main_type: "ACH",
          tags: tags
      )
      user_info = fee.try(:[], "splits") || {}
      withdraw = withdraw(amount,wallet.id, total_fee || 0, TypesEnumLib::GatewayType::ACH,account_number,send_via,user_info, nil, nil, nil,TypesEnumLib::GatewayType::Quickcard,nil,true)
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'finalize', type: "ach"
      if withdraw.present? && withdraw["actions"].present?
        parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
        save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
        check.transaction_id = transaction.id
        check.save
        transaction.update(seq_transaction_id: withdraw["actions"].first["id"],
                           status: "approved",
                           privacy_fee: 0,
                           tags: withdraw["actions"].first.tags,
                           sub_type: withdraw["actions"].first.tags["sub_type"],
                           timestamp: withdraw["timestamp"]
        )
        ActivityLog.log(nil,"ACH Created",current_user,params,response,"ACH Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","ach")
        ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: [check.id], type: "ach"
      else
        ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.unavail_feature.title'), message: I18n.t('error_codes.account.unavail_feature.description'), type: "ach"
      end
    rescue => exc
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', message: exc.message, type: "ach"
    end
  end

  def create_check
    begin
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.amount_not_zero.title'), message: I18n.t('error_codes.account.amount_not_zero.description'), type: "check" if params[:amount].to_f <= 0
      location = Location.joins(:wallets, :fees).where(id: params[:location_id]).last
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.account_not_exist.title'), message: I18n.t('error_codes.account.account_not_exist.description'), type: "check" if location.blank?
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.unavailable_function.title'), message: I18n.t('error_codes.account.unavailable_function.description'), type: "check" if location.try(:block_withdrawal)

      recipient = current_user.merchant? && current_user.merchant_id.present? ? current_user.try(:parent_merchant).try(:email) : params[:email]
      name = params[:name]
      description = params[:check_memo]
      send_via = 'email'
      send_check_user_info = { name: name, check_email: recipient }
      wallet = location.wallets.try(:primary).try(:first)
      params[:wallet_id] = wallet.id

      main_type = updated_type(TypesEnumLib::TransactionType::SendCheck)
      amount = params[:amount].to_f
      balance = SequenceLib.balance(wallet.id, @user.ledger)

      escrow_wallet = Wallet.check_escrow.first
      escrow_user = escrow_wallet.try(:users).try(:first)
      location_fee = location.fees if location.present?
      fee_object = location_fee.buy_rate.first if location_fee.present?
      fee_class = Payment::FeeCommission.new(@user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::SendCheck)
      fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
      amount_sum = amount + fee["fee"].to_f
      amount_sum = amount_sum.round(2)
      splits = fee["splits"]
      if splits.present?
        agent_fee = splits["agent"]["amount"]
        iso_fee = splits["iso"]["amount"]
        iso_balance = show_balance(location.iso.wallets.primary.first.id)
        if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
          if iso_balance < iso_fee
            return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.check_creation_failed.title'), message: I18n.t('error_codes.account.check_creation_failed.description'), type: "check"
          end
        else
          if iso_balance + iso_fee < agent_fee.to_f
            return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.check_creation_failed_2.title'), message: I18n.t('error_codes.account.check_creation_failed_2.description'), type: "check"
          end
        end
      end
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.insufficient_balance.title') , message: I18n.t('error_codes.account.insufficient_balance.description'), type: "check" if balance.to_f < amount_sum
      if location.check_limit_type.present? && location.check_limit != 0
        if params[:amount].to_f > location.check_limit.to_f && location.check_limit_type == "Transaction"
          return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.daily_limit.title') , message: I18n.t('error_codes.account.daily_limit.description', location_limit: location.check_limit.to_f), type: "check"
        end
        if location.check_limit_type == "Day"
          date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          instant_checks = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",location.id,date,"VOID",[0,5])
          total_sum = instant_checks.sum(:actual_amount) + params[:amount].to_f
          if total_sum.to_f > location.check_limit.to_f
            return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.daily_limit.title') , message:  I18n.t('error_codes.account.daily_limit.description', location_limit: location.check_limit.to_f), type: "check"
          end
        end
      end
      pending_amount = HoldInRear.calculate_pending(params[:wallet_id])
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.insufficient_balance.title'), message: I18n.t('error_codes.account.insufficient_balance.description'), type: "check" if balance - pending_amount < amount_sum
      if !@user.merchant_id.nil? && current_user.try(:permission).try(:check)
        user_id = @user.merchant_id
      else
        user_id = @user.id
      end
      if user_id != @user.id
        main_user = User.find_by(id: user_id)
      else
        main_user = @user
      end

      if main_user.oauth_apps.present?
        oauth_app = main_user.oauth_apps.first
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.blocked_user.title') ,message:  I18n.t('error_codes.account.blocked_user.description'), type: "check" if oauth_app.is_block && main_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
      end

      check = TangoOrder.new(user_id: user_id,
                             name: name,
                             status: "PENDING",
                             amount: amount_sum,
                             actual_amount: amount,
                             check_fee: fee_object.send_check.to_f,
                             fee_perc: deduct_fee(fee_object.redeem_fee.to_f, amount),
                             recipient: recipient,
                             description: description,
                             order_type: "check",
                             approved: false,
                             settled: false,
                             wallet_id: wallet.id,
                             send_via: send_via,
                             batch_date: Time.now.to_datetime + 1.day,
                             amount_escrow: true
      )
      location_data = location_to_parse(location) if location.present?
      merchant = merchant_to_parse(location.merchant) if location.present?
      tags = {
          "fee_perc" => splits,
          "location" => location_data,
          "merchant" => merchant,
          "send_check_user_info" => send_check_user_info,
          "send_via" => send_via
      }
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'fifty', type: "check"
      gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
      total_fee = save_total_fee(splits, fee["fee"].to_f)
      sender_dba=get_business_name(wallet)
      transaction = @user.transactions.create(
          to: recipient,
          from: nil,
          status: "pending",
          amount: amount,
          sender: main_user,
          sender_name: sender_dba,
          sender_wallet_id: wallet.id,
          receiver_id: escrow_user.try(:id),
          receiver_wallet_id: escrow_wallet.try(:id),
          receiver_name: escrow_user.try(:name),
          sender_balance: balance,
          receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
          action: 'transfer',
          fee: total_fee.to_f,
          net_amount: amount.to_f,
          net_fee: fee["fee"].to_f,
          total_amount: amount_sum,
          gbox_fee: gbox_fee.to_f,
          iso_fee: save_iso_fee(splits),
          agent_fee: splits["agent"]["amount"].to_f,
          affiliate_fee: splits["affiliate"]["amount"].to_f,
          ip: get_ip,
          main_type: main_type,
          tags: tags
      )
      if fee.present?
        user_info = fee["splits"]
        withdraw = withdraw(amount,wallet.id, total_fee.to_f || 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
      end
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'finalize', type: "check"
      if withdraw.present? && withdraw["actions"].present?
        transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
        check.transaction_id = transaction.id
        #= creating block transaction
        parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
        save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
        check.save
        ActivityLog.log(nil,"Check Created",current_user,params,response,"Check Created [##{check.id}] for [$#{number_with_precision(check.amount, precision: 2)}]","checks")
        ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: check.id, type: "check"
      else
        ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: I18n.t('merchant.controller.unaval_feature'), type: "check"
      end
    rescue => ex
      message = I18n.t 'merchant.controller.chck_creation_faild'
      message = ex.message if ex.class == CheckError
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.check_creation_faild_try_again.title') , message: I18n.t('error_codes.account.check_creation_faild_try_again.description'), type: "check"
      msg = "*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
    end
  end

  def create_p2c
    begin
      amount = params[:amount].to_f
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.amount_not_zero.title'), message: I18n.t('error_codes.account.amount_not_zero.description'), type: "p2c" if amount.to_f <= 0
      name = params[:name]
      recipient = params[:email]
      description = params[:p2c_memo]
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.name_not_present.title'), message: I18n.t('error_codes.account.name_not_present.description'), type: "p2c" if name.blank?
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.minimum_name_length.title'), message: I18n.t('error_codes.account.minimum_name_length.description'), type: "p2c" if name.try(:strip).try(:length) < 2
      if params[:cardNoSafe].present?
        card_no = params[:cardNoSafe].gsub(' ', '')
      else
        card_no = params[:card_number]
      end

      card_bin_list = get_card_info(card_no)
      if card_no.present?
        reg = /\D/
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.invalid_card_number.title'), message: I18n.t('error_codes.account.invalid_card_number.description'), type: "p2c" if card_no.match(reg).present?
      end
      if card_bin_list.present? && card_bin_list.try(:[],'type').present? && card_bin_list.try(:[],'type')=="credit"
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.for_debit_card_only.title') , message: I18n.t('error_codes.account.for_debit_card_only.description'), type: "p2c"
      end

      ## Card Expiry :-:
      if params[:expiry_date].present?
        exp_month = params[:expiry_date].split('/').first
        exp_year = params[:expiry_date].split('/').last
        current_year = Time.current.year - 2000
        if exp_year.to_i < current_year || exp_month.to_i > 12 || exp_month.to_i < 0
          return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.invalid_card_number.title') , message: I18n.t('error_codes.account.invalid_card_number.description'), type: "p2c"
        end
      end
      if exp_year.present? && exp_year.to_s.size == 4
        exp_year = exp_year.to_i - 2000
      end
      exp_date = "20#{exp_year}-#{exp_month}"
      ##
      zip = params[:zip_code]
      send_via = 'instant_pay'

      location = Location.find_by(id: params[:location_id])
      return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.p2c_blocked.title'), message: I18n.t('error_codes.account.p2c_blocked.description'), type: "p2c" if location.push_to_card
      wallet = location.wallets.try(:primary).try(:first)
      params[:wallet_id] = wallet.id

      balance = show_balance(wallet.id, current_user.ledger).to_f
      last_4 = card_no.last(4) if card_no.present?
      first_6 = card_no.first(6) if card_no.present?
      merchant = current_user

      if (params[:card_id].present? && params[:other_card].blank?) || (params[:card_id].present? && params[:other_card].present? && params[:card_number].blank?) #= using exiting card
        card = Card.find_by(id: params[:card_id])
        if card.present? && card.card_type=="debit"
          card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, merchant.id)
          last_4 = card.last4
          first_6 = card.first6
          return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.invalid_card_number.title') , message: I18n.t('error_codes.account.invalid_card_number.description'), type: "p2c" if card_info.number.tr("0-9","").present?
          card_detail = {
              number: card_info.number,
              month: card_info.month,
              year: card_info.year,
          }
        else
          return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.for_debit_card_only.title') , message: I18n.t('error_codes.account.for_debit_card_only.description'), type: "p2c"
        end
      else
        if params[:save_card].present? &&  params[:save_card] == "on"
          return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.invalid_card_number.title') , message: I18n.t('error_codes.account.invalid_card_number.description'), type: "p2c" if card_no.tr("0-9","").present?
          card_detail = {
              number: card_no,
              month: exp_month,
              year: "20#{exp_year}",
          }
          card_bin_list = get_card_info(card_no)
          card1 = Payment::QcCard.new(card_detail, nil, merchant.id)
          cards = merchant.cards.instant_pay_cards.where(fingerprint: card1.fingerprint,merchant_id: merchant.id)
          if cards.blank?
            card = merchant.cards.create(qc_token:card1.qc_token,exp_date: "#{card_detail[:month]}/#{card_detail[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand, last4: last_4, name: merchant.name, card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: first_6,fingerprint: card1.fingerprint,merchant_id: merchant.id,instant_pay: true)
          else
            card = cards.first
            card.exp_date = params[:expiry_date] if params[:expiry_date].present?
          end
        end
      end

      if location.push_to_card_limit.to_f != 0 && location.push_to_card_limit_type == "Day"
        todays_orders = TangoOrder.orders_for_today(wallet.id, merchant.id, "instant_pay")
        todays_transactions_amount = todays_orders.pluck(:actual_amount).sum(&:to_f)
        todays_void_amount = todays_orders.VOID.pluck(:actual_amount).sum(&:to_f)
        todays_failed_amount = todays_orders.FAILED.pluck(:actual_amount).sum(&:to_f)
        valid_amount = todays_transactions_amount - todays_void_amount - todays_failed_amount + amount
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: "Error Code 4000 " , message: "Daily limit exceeded. Daily Transactions limit reached to $#{number_with_precision(location.push_to_card_limit, :precision => 2, delimiter: ',')} !", type: "p2c" if valid_amount > location.push_to_card_limit
      elsif location.push_to_card_limit.to_f != 0 && amount > location.push_to_card_limit
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: "Error Code 5007",message: "Sorry you are not allowed to process with more than $#{number_with_precision(location.push_to_card_limit, :precision => 2, delimiter: ',')}. Please contact your administrator for details.", type: "p2c"
      end

      if card_detail.present?
        account_token_info =  {
            :card_number => card_detail[:number],
            :expiry_date => "#{card_detail[:year]}-#{card_detail[:month]}"
        }
      else
        account_token_info =  {
            :card_number => card_no,
            :expiry_date => exp_date
        }
      end

      account_token_info = account_token_info.to_json
      encrypted_card_info = AESCrypt.encrypt("#{merchant.id}-#{ENV['CARD_ENCRYPTER']}-#{wallet.id}}", account_token_info)

      if user_signed_in? && wallet.present?
        location_fee = location.fees if location.present?
        fee_object = location_fee.buy_rate.first if location_fee.present?
        fee_class = Payment::FeeCommission.new(@user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::DebitCardDeposit)
        fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::DebitCardDeposit)
        total_fee = fee["fee"].to_f
        amount_sum = amount + total_fee
        db_check_fee = fee_object.dc_deposit_fee_dollar.to_f
        db_fee_perc = deduct_fee(fee_object.dc_deposit_fee.to_f, amount)

        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.insufficient_balance_cannot_create_check.title'),  message: I18n.t('error_codes.account.insufficient_balance_cannot_create_check.description'), type: "p2c" if balance < amount_sum
        pending_amount = HoldInRear.calculate_pending(params[:wallet_id])
        return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.insufficient_balance_cannot_create_check.title') , message: I18n.t('error_codes.account.insufficient_balance_cannot_create_check.description'), type: "p2c" if balance - pending_amount < amount_sum

        splits = fee.try(:[],"splits")
        if splits.present?
          agent_fee = splits["agent"]["amount"]
          iso_fee = splits["iso"]["amount"]
          iso_balance = show_balance(location.iso.wallets.primary.first.id)
          if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
            if iso_balance < iso_fee
              return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: 'Error Code ISO-0003'  ,message: 'P2C Creation Failed!', type: "p2c"
            end
          else
            if iso_balance + iso_fee < agent_fee.to_f
              return ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: 'Error Code ISO-0004'  ,message: 'P2C Creation Failed!', type: "p2c"
            end
          end
        end

        check_escrow = Wallet.check_escrow.first
        check = TangoOrder.new(user_id: merchant.id,
                               name: name,
                               status: "PENDING",
                               amount: amount_sum,
                               actual_amount: amount,
                               check_fee: db_check_fee,
                               fee_perc: db_fee_perc,
                               recipient: recipient,
                               description: description,
                               order_type: "instant_pay",
                               approved: false,
                               account_token: encrypted_card_info,
                               settled: false,
                               wallet_id: wallet.id,
                               send_via: send_via,
                               last4: last_4,
                               first6: first_6,
                               batch_date:Time.zone.now + 1.day,
                               zip_code: zip,
                               amount_escrow: true)

        location1 = location_to_parse(location)
        merchant1 = merchant_to_parse(merchant&.parent_merchant || merchant)
        send_check_user_info = { name: name, check_email: recipient,amount: amount,description: description,fee: total_fee }
        tags = {
            "fee_perc" => splits,
            "location" => location1,
            "merchant" => merchant1,
            "send_check_user_info" => { name: name, check_email: recipient,amount: amount,description: description,fee: total_fee },
            "send_via" => send_via,
            "card" => card
        }

        new_fee = save_gbox_fee(splits, fee.try(:[], "fee").to_f)
        total_fee = save_total_fee(splits, fee.try(:[], "fee").to_f)
        ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'fifty', type: "p2c"
        sender_dba=get_business_name(wallet)
        transaction = merchant.transactions.build(
            to: recipient,
            from: nil,
            status: "pending",
            charge_id: nil,
            amount: amount,
            sender: merchant,
            sender_name: sender_dba,
            sender_wallet_id: wallet.id,
            receiver_wallet_id: check_escrow.id,
            receiver_name: check_escrow.try(:users).try(:first).try(:name),
            receiver_id: check_escrow.try(:users).try(:first).try(:id),
            sender_balance: balance,
            receiver_balance: SequenceLib.balance(check_escrow.id),
            action: 'transfer',
            fee: total_fee,
            net_amount: amount,
            net_fee: total_fee,
            total_amount: amount_sum.to_f,
            gbox_fee: new_fee.to_f,
            iso_fee: save_iso_fee(splits),
            agent_fee: splits.try(:[],"agent").try(:[],"amount").to_f,
            affiliate_fee: splits.try(:[],"affiliate").try(:[],"amount").to_f,
            ip: get_ip,
            main_type: "instant_pay",
            tags: tags,
            last4: last_4,
            first6: first_6
        )
        user_info = fee.try(:[], "splits")

        withdraw = withdraw(amount,wallet.id, total_fee || 0, TypesEnumLib::TransactionType::DebitCardDeposit,last_4,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true, card)
        ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'finalize', type: "check"
        if withdraw.present? && withdraw["actions"].present?
          transaction.seq_transaction_id = withdraw["actions"].first["id"]
          transaction.status = "approved"
          transaction.privacy_fee = 0
          transaction.sub_type = withdraw["actions"].first.tags["sub_type"]
          transaction.timestamp = withdraw["timestamp"]
          transaction.save
          check.transaction_id = transaction.id
          check.save
          #= creating block transaction
          parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
          save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
          ActivityLog.log(nil,"Push to Card Created",current_user,params,response,"Push To Card Created [##{check.id}] for [$#{number_with_precision(amount.to_f, precision: 2)}]","push_to_card")
          ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'done', message: check.id, type: "p2c"
        else
          ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.unavail_feature.title')  ,message: I18n.t('error_codes.account.unavail_feature.description'), type: "p2c"
        end
      end
    rescue => ex
      message = I18n.t 'error_codes.account.debit_card_deposit_failed.description'
      message = ex.message
      ActionCable.server.broadcast "response_channel_#{session[:session_id]}", channel_name: "response_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.debit_card_deposit_failed.title'), message: message, type: "p2c"
      msg = "*Push To Card Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`"
      handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
    end
  end

  def transaction_detail
    if (params[:user_type] == "customer")
      @customer_present = true
      @customer_path = "/v2/merchant/invoices/client_details?id=#{params[:customer_id]}&type=transaction"
    end
    @wallet_path = params[:path]
    @wallet_name = params[:wallet]
    @transaction = Transaction.where(seq_transaction_id: params[:id]).first
    if @transaction.present?

      if @transaction.minfraud_result.present?
        @minfraud_result = @transaction.minfraud_result
        @minfraud_insight = @minfraud_result.insight_detail
      end
    else
      redirect_back(fallback_location: root_path)
    end

  end

  def maxmind_update
    @minfraud_result = MinfraudResult.find params[:id]
    db_transaction = @minfraud_result.main_transaction
    if @minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview &&
        @minfraud_result.qc_reason == TypesEnumLib::RiskReason::ManualReview
      ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '25'
      if params[:new_action] == "accept"
        @minfraud_result.qc_action = TypesEnumLib::RiskType::Accept
        @minfraud_result.save
        ActivityLog.log(nil,"MR Accepted", current_user, params, response,"Accepted MR for [#{db_transaction.seq_transaction_id}] for [$#{db_transaction.total_amount.to_f}]","accounts")
        flash[:success] = I18n.t('maxmind.accepted')
      elsif params[:new_action] == "refund"
        ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '50'

        #Refunding transactions start
        transaction_id = @minfraud_result.transaction_id
        transaction = Transaction.find(transaction_id)
        merch = transaction.receiver
        refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Manual Reject by Merchant", @user)
        refund_response = refund.process
        #Refunding transactions end
        ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '75'

        if refund_response[:success] == true
          @minfraud_result.qc_action = TypesEnumLib::RiskType::Reject
          @minfraud_result.save
          UserMailer.manual_review_refund(transaction_id).deliver_later
          ActivityLog.log(nil,"MR Rejected", current_user, params, response,"Rejected MR for [#{db_transaction.seq_transaction_id}] for [$#{db_transaction.total_amount.to_f}]","accounts")
          ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '100'
          flash[:success] = I18n.t('maxmind.rejected')
        else
          ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: 'error', message: refund_response[:message]
          flash[:error] = refund_response[:message]
        end
      else
        flash[:error] = I18n.t('maxmind.invalid_params')
      end
    else
      flash[:error] = I18n.t('maxmind.invalid_request')
    end
    redirect_back(fallback_location: root_path)
  end

  def present_card_verification
    if params[:card_number].blank?
      render json:{"success":"not_available"}, status: 200
    else
      count = params[:card_number].length
      params[:card_number] = params[:card_number].gsub(' ', '')
      if params[:present].present? && params[:present] == "present"
        card=Card.find_by_id(params[:card_number])
      else
        card=Card.where(first6:params[:card_number].first(6).to_i,last4: params[:card_number].last(4)).last
      end
      card_type=card.try(:card_type)
      if card_type.blank? || card_type=='credit'
        card_info = get_card_info(params[:card_number])
        card_type = card_info['type']
        card.update(card_type: card_type) if card.present?
      end

      if card_type=='debit'
        render json:{"success":"true", count: count}, status: 200
      elsif card_type=='credit'
        render json:{"success":"false", count: count} , status: 200
      else
        render json:{"success":"false", count: count}, status: 200
      end
    end
  end

  def transaction_event
    @txn_detail_url = params[:txn_detail_url]
    @transaction = Transaction.where(seq_transaction_id: params[:id]).first
    @events=@transaction.event_logs.where.not(event_type: nil)
    if @events.present?
      @transaction_created = @events.where(event_type: "transaction_created").first
      @transaction_refunded = @events.where(event_type: "transaction_refunded").first
      @partial_refunded = @events.where(event_type: "transaction_partial_refunded").first
      @chargeback_created = @events.where(event_type: "chargeback_created").first
      @chargeback_won = @events.where(event_type: "chargeback_won").first
      @chargeback_lost = @events.where(event_type: "chargeback_lost").first
      @dispute_accepted = @events.where(event_type: "dispute_accepted").first

      if @transaction_refunded.present? || @partial_refunded.present?
        @refund_transaction = Transaction.find(@transaction.refund_id)
      end
      if @chargeback_created.present?
        @dispute_case =@transaction.dispute_case
      end
    end
  end

  def tip_reserve_transaction_detail
    @wallet_path = params[:path]
    @wallet_name = params[:wallet]
    @transaction = Transaction.where(seq_transaction_id: params[:id]).first
    if @transaction.first6.present?
      @response = get_card_info(@transaction.first6)
    else
      @response = nil
    end
    @reserve = true
    if @transaction.minfraud_result.present?
      @minfraud_result = @transaction.minfraud_result
      @minfraud_insight = @minfraud_result.insight_detail
    end
    render template: 'v2/merchant/accounts/transaction_detail.html.erb'
  end

  def buy_giftcard
    @wallet = @wallet = Wallet.find(params[:wallet_id])
    client = TangoClient.instance
    catalog = client.catalog
    @result = catalog.get_catalog()
    @giftcards = get_all_giftcards(@result)
    # @giftcards = Kaminari.paginate_array(@giftcards).page(params[:page]).per(5)
    render partial: "v2/merchant/accounts/buy_giftcard"
  end

  def giftcard_show
    @wallet = @wallet = Wallet.find(params[:wallet_id])
    @list=[]
    client = TangoClient.instance
    catalog = client.catalog
    @giftcards = catalog.get_catalog()

    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
      @user.wallets.primary.each do |w|
        # @wallets << w
        @location = w.location
        if @location.present?
          location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  location_status: location_status,
                  block_giftcard: @location.block_giftcard
              }
        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  location_status: false,
                  block_giftcard: @user.block_giftcard
              }
        end

      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    else
      @user.wallets.primary.each do |w|
        location = w.location
        # @wallets <<
        if location.present?
          location_status = location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  location_status: location_status,
                  block_giftcard: location.block_giftcard
              }
        end
      end
    end
    @giftcard={}
    @giftcards.brands.each do |b|
      br=b.items.select{|v|v.utid== params[:id]}
      if !br.empty?
        @giftcard = {gift_card: br.first,brand_name:b.brand_name,description:b.description,disclaimer:b.disclaimer, image_url: b.image_urls.map{|s| s.second}.third }
      end
    end
    render partial: "v2/merchant/accounts/gift_card_buy"
  end

  def buy_card
    fee = 0
    unless params[:email].present? &&  params[:image_url].present? && params[:wallet_id].present? &&  params[:utid].present? && params[:amount].present?
      ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error',error_code: I18n.t('error_codes.account.gift_card_missing_parameters.title') , message: I18n.t('error_codes.account.gift_card_missing_parameters.description')
    end
    @user= current_user
    @user= User.find(current_user.merchant_id) if current_user.merchant_id.present? && current_user.try(:permission).admin?
    if @user.oauth_apps.present?
      oauth_app = @user.oauth_apps.first
      if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_giftcard}.all?
        ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.blocked_user.title') , message: I18n.t('error_codes.account.blocked_user.description')
      end
    end
    begin
      validate_parameters({wallet_id: params[:wallet_id], utid:  params[:utid], amount: params[:amount] })
      balance = show_balance(params[:wallet_id])
      client = TangoClient.instance
      if params[:amount].present? && params[:utid].present?
        fee_lib = FeeLib.new
        w = Wallet.find(params[:wallet_id])
        withdraw_balance = balance.to_f - HoldInRear.calculate_pending(w.id).to_f
        location = w.location
        if location.present?
          if location.is_block
            ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.blocked_location.title') , message: I18n.t('error_codes.account.blocked_location.description')
          end
          if location.block_giftcard
            ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.unavailable_function.title'), message: I18n.t('error_codes.account.unavailable_function.description')

          end
        end
        amount = params[:amount].to_f
        actual_amount = params[:amount].to_f
        if @user.MERCHANT?
          location_fee = location.fees.buy_rate.first
          fee_class = Payment::FeeCommission.new(@user, nil, location_fee, w, location,TypesEnumLib::CommissionType::Giftcard)
          fee_value = fee_class.apply(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard)
          amount = amount + fee_value["fee"].to_f
          fee = fee_value["fee"].to_f
        elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
          data = deduct_giftcard_fee(@user,amount)
          amount = data[0]
          fee = data[1]
        else
          location_fee = fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
          amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
          fee = location_fee
        end
        if  withdraw_balance.to_f < amount.to_f
          ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_insufficient_funds.title'), message: I18n.t('error_codes.account.gift_card_insufficient_funds.description')
        else
          account = client.accounts
          account_info = account.get_account('quickcard')
          if account_info.present? && account_info.current_balance.present?
            tango_balance = account_info.current_balance
          end
          if tango_balance.present? && tango_balance < amount.to_f
            ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.transaction_not_complete.title') ,message: I18n.t('error_codes.account.transaction_not_complete.description')
          else
            if @user.MERCHANT?
              if withdraw_balance.to_f < amount.to_f
                ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_insufficient_funds.title') , message: I18n.t('error_codes.account.gift_card_insufficient_funds.description')
              end
            end
            orders = client.orders
            customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
            account_identifier = ENV["ACCOUNT_IDENTIFIER"]

            body = Raas::CreateOrderRequestModel.new
            body.account_identifier = account_identifier
            body.amount = params[:amount].to_f || 0
            body.customer_identifier = customer_identifier
            body.send_email = true
            body.sender = {
                "email": 'admin@quickcard.me',
                "firstName": "QuickCard",
                "lastName": ""
            }
            body.recipient = {
                "email": params[:email],
                "firstName": @user.name || 'No Name',
                "lastName": ""
            }
            body.utid = params[:utid]
            result = orders.create_order(body)
            if result
              p "TANGO ORDER: ", result
              tango_order =  @user.tango_orders.create!(
                  :utid => params[:utid],
                  :account_identifier => account_identifier,
                  :amount => body.amount,
                  :name => result.reward_name,
                  :recipient => params[:email],
                  :order_id => result.reference_order_id,
                  :catalog_image => params[:image_url] || nil,
                  :wallet_id => params[:wallet_id],
                  :gift_card_fee => fee,
                  :status => result.status
              )
              ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'finalize'
              if @user.MERCHANT?
                user_info = fee_value["splits"]
                user_info["utid"] = params[:utid].present? ? params[:utid] : ''
                user_info["recipient_email"] = params[:email].present? ? params[:email] : ''
                user_info["brand_name"] = result.reward_name.present? ? result.reward_name : ''
                # admin_wallet = Wallet.where(wallet_type: 3).first
                tags = {
                    "fee_perc" => user_info
                }
                gbox_fee = save_gbox_fee(fee_value["splits"], fee.to_f)
                total_fee = save_total_fee(fee_value["splits"], fee.to_f)
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: total_fee.to_f,
                    net_amount: actual_amount.to_f,
                    net_fee: total_fee.to_f,
                    total_amount: amount.to_f,
                    gbox_fee: gbox_fee.to_f,
                    iso_fee: save_iso_fee(fee_value["splits"]),
                    agent_fee: fee_value["splits"]["agent"]["amount"].to_f,
                    affiliate_fee: fee_value["splits"]["affiliate"]["amount"].to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
                    tags: tags
                )
                transact = withdraw(params[:amount].to_f,params[:wallet_id], location_fee.giftcard_fee.to_f, TypesEnumLib::TransactionType::GiftCardPurchase, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango)
              elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
                utid = params[:utid].present? ? params[:utid] : ''
                recipient_email = params[:email].present? ? params[:email] : ''
                brand_name = result.reward_name.present? ? result.reward_name : ''
                user_info = {utid: utid, recipient_email: recipient_email, brand_name:brand_name}
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: fee.to_f,
                    net_amount: actual_amount.to_f,
                    net_fee: fee.to_f,
                    total_amount: amount.to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
                    tags: user_info
                )
                transact = withdraw(amount,params[:wallet_id], fee, TypesEnumLib::TransactionType::GiftCard, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango,'other')
              else
                # user_info = fee_lib.giftcard_fee_divsion(location,location_fee, nil, nil)
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: location_fee.to_f,
                    net_amount: actual_amount,
                    net_fee: location_fee.to_f,
                    total_amount: amount.to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase)
                )
                transact = withdraw(params[:amount].to_f,params[:wallet_id], location_fee.to_f, TypesEnumLib::TransactionType::GiftCardPurchase, nil,nil, nil,nil,nil,nil,TypesEnumLib::GatewayType::Tango)
              end
              if transact.present?
                tango_order.update(transaction_id: transaction.id)
                transaction.update(seq_transaction_id: transact.id, tags: transact.actions.first.tags, status: "approved", timestamp: transact.timestamp)
                #= creating block transaction
                parsed_transactions = parse_block_transactions(transact.actions, transact.timestamp)
                save_block_trans(parsed_transactions) if parsed_transactions.present?
              end
              ActivityLog.log(nil,"Giftcard Created",current_user,params,response,"Giftcard Created [##{tango_order.id}] for [$#{number_with_precision(amount.to_f, precision: 2)}]","giftcard")
              ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'done'

              # flash[:success] = I18n.t 'merchant.controller.successful_order'
            end

            # transact = transaction_between(admin_wallet.id, @user.wallet_id, amount, "Tango Transfer", 0, "")
          end
        end
      else
        ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error', error_code: I18n.t('error_codes.account.gift_card_missing_parameters.title'), message: I18n.t('error_codes.account.gift_card_missing_parameters.description')
      end
    rescue Exception => exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.message.present?
        message = exc.message
      end
      ActionCable.server.broadcast "giftcard_channel_#{session[:session_id]}", channel_name: "giftcard_channel_#{session[:session_id]}", action: 'error',error_code: "Error Code 5026 " ,message: message

      # flash[:warning] = exc.message
    end
    # redirect_back(fallback_location: root_path)
  end

  def get_help

  end

  def delete_p2c_card
    card = Card.find_by_id(params[:id])
    if card.present?
      card.delete
      result = true
    else
      result = false
    end
    render json: {result: result}
  end

  def get_refund_fee
    transaction = Transaction.where(id: params[:trans_id]).first if params[:trans_id].present?
    # transaction = Transaction.where(seq_transaction_id: params[:parent_id]).first if transaction.blank?
    if transaction.present?
      user_name = ''
      fee = 0
      wallet = transaction.receiver_wallet if transaction.receiver_wallet.present?
      fee = wallet.location.fees.buy_rate.first.refund_fee.to_f if wallet.present?
      if transaction.sender_wallet.present?
        wallet = transaction.sender_wallet
        user = wallet.users.first if wallet.present?
      else
        user = wallet.users.first if wallet.present?
      end
      user_name = user.name if user.present?
      # transaction[:amount] = transaction[:amount].to_f + transaction[:tip].to_f + transaction[:privacy_fee].to_f if transaction[:main_type].present? && transaction[:main_type] != TypesEnumLib::TransactionViewTypes::CreditCard
      if transaction.refund_log.nil?
        render status: 200 , json:{partial_amount: transaction[:total_amount].to_f,name: user_name,fee: fee}
      else
        if transaction.refund_log.present?
          if JSON.parse(transaction.refund_log).kind_of?(Array)
            temp = JSON.parse(transaction.refund_log)
            previousAmount = transaction[:total_amount].to_f - temp.sum{|h| h["amount"]}
          else
            temp = JSON.parse(transaction.refund_log)
            previousAmount = transaction[:total_amount].to_f - temp["amount"].to_f
          end
        else
          previousAmount = transaction[:total_amount]
        end
        render status: 200 , json:{partial_amount: previousAmount,name: user_name,fee: fee}
      end
    else
      render status: 200 , json:{ partial_amount: "" }
    end
  end

  def refund_merchant_transaction
    ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '25'
    reason_detail = params[:reason_detail].present? ? params[:reason_detail] : "NA"
    transaction = Transaction.where(id: params[:transaction]).first
    seq_transaction_id = transaction.seq_transaction_id
    t_amount = transaction.total_amount.to_f
    # merch_wall_id = transaction.receiver_wallet_id if transaction.present?
    # m_wallet = transaction.receiver_wallet
    merch = transaction.receiver
    if params[:partial_amount].present? && params[:partial_amount].to_f > 0 && params[:partial_amount].to_f < t_amount
      partial_amount = params[:partial_amount]
    end
    ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '50'
    refund = RefundHelper::RefundTransaction.new(seq_transaction_id, transaction,merch,partial_amount,reason_detail, @user)
    response = refund.process
    ## Activity Log saved - Temporary Solution.
    ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '75'
    unless @user.nil?
      refund_params = {}
      refund_params[:seq_transaction_id] = transaction.id
      refund_params[:user] = @user
      refund_params[:type] = "merchant"
      refund_params[:response] = response
      record_refund_transaction(refund_params)
    end
    if response[:success] == true
      ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: '100'
      flash[:success] = response[:message]
    else
      ActionCable.server.broadcast "refund_channel_#{session[:session_id]}", channel_name: "refund_channel_#{session[:session_id]}", action: 'error', message: response[:message]
      flash[:error] = response[:message]
    end
    # return redirect_back(fallback_location: root_path)
  end

  private

  def new_transfer_params
    params.require(:transfer).permit(:from_wallet ,:to_wallet, :amount, :transfer_date, :memo,:transfer_timezone)
  end

end
