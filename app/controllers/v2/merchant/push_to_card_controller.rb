class V2::Merchant::PushToCardController < V2::Merchant::BaseController

  class CheckError < StandardError; end

  def index
    #old name debit_card_deposit
    @push_to_card = true
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]

    params[:search] = eval(params[:search]) if params[:search].present? && ( params[:search].is_a? String )
    params[:q] = eval(params[:q]) if params[:q].present? && ( params[:q].is_a? String )
    if params[:q].present?
      params[:q]["name_or_recipient_or_last4_cont"] = params[:q][:data_search]
      params[:q]["id_eq"] = params[:q][:data_search]
      params[:q]["amount_eq"] = params[:q][:data_search]
      params[:q]["actual_amount_eq"] = params[:q][:data_search]
      params[:q]["wallet_location_business_name_cont"] = params[:q][:data_search]
      # params[:q]["created_at_cont"] = DateTime.strptime(params[:q][:data_search], "%m-%e-%Y    %H:%M:%S %p")
      #params[:q]["created_at"] = ActiveSupport::TimeZone['UTC'].parse(DateTime.strptime(params[:q][:data_search],  "%a, %e %b %Y ").asctime)
    end
    # @q = TangoOrder.ransack(params[:q])
    # @debit_deposits = AppConfig.where(key: AppConfig::Key::DebitCardDeposit).first
    # @push_to_card = @debit_deposits.boolValue unless @debit_deposits.blank?
    @heading = "Debit Card Deposit"
    @check = Check.new
    @direct_deposit = Check.new
    if current_user.present? && !current_user.merchant_id.nil?
      if params[:wallet_id].present?
        wallet_ids = params[:wallet_id]
      else
        wallet_ids = current_user.wallets.pluck(:id)
      end
      user_ids=[current_user.merchant_id,current_user.id]
      # if parent_user.oauth_apps.present?
      # oauth_app = parent_user.oauth_apps.first
      # @push_to_card = true if @user.get_locations.uniq.map {|loc| loc.push_to_card}.include?(true)
      # end

      @checks = TangoOrder.where(order_type: "instant_pay", user_id: user_ids,wallet_id: wallet_ids)
      # @checks = @checks.where("created_at BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
      @checks = @checks.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
      # @checks = @checks.ransack(params[:q]).result.sort.reverse
      @checks = @checks.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result.order(id: :desc)
      totals = @checks.to_a.group_by(&:status).map{ |status,amount| {status => amount.sum {|j| j.amount.to_f} }}
      totals = totals.reduce Hash.new, :merge
      @total_paid = totals["PAID"].present? ? totals["PAID"] : 0
      @total_pending = @checks.select{|check| check.PENDING?}.try(:pluck, :actual_amount).try(:sum).to_f
      @total_void = totals["VOID"].present? ? totals["VOID"] : 0
      @total_failed = totals["FAILED"].present? ? totals["FAILED"] : 0
      @total_paid_wire = totals["PAID_WIRE"].present? ? totals["PAID_WIRE"] : 0
    else
      if current_user.sub_merchants.present?
        sub_id = current_user.sub_merchants.pluck(:id)
        user_ids=[current_user.id,sub_id]
        user_ids= user_ids.flatten if user_ids.present?
      else
        user_ids=current_user.id
      end
      if params[:wallet_id].present?
        @checks = TangoOrder.where(order_type: "instant_pay", user_id: user_ids,wallet_id: params[:wallet_id])
        # @checks = @checks.where("created_at BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
        @checks = @checks.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
        # @checks = @checks.ransack(params[:q]).result.sort.reverse
        @checks = @checks.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result
      else
        @checks = TangoOrder.where(order_type: "instant_pay",user_id: user_ids)
        # @checks = @checks.where("created_at BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
        @checks = @checks.where("Date(tango_orders.created_at) BETWEEN :first AND :second", first: params[:first_date].to_date, second: params[:second_date].to_date) if params[:first_date].present? && params[:second_date].present?
        # @checks = @checks.ransack(params[:q]).result.sort.reverse
        @checks = @checks.where(id: TangoOrder.search_by_status(params["search"])).ransack(params[:q].try(:merge, m: 'or')).result.order(id: :desc)
      end
      # totals = TangoOrder.where(order_type: "instant_pay", user_id: user_ids).ransack(params[:q]).result.group(:status).sum(:amount)
      totals = @checks.to_a.group_by(&:status).map{ |status,amount| {status => amount.sum {|j| j.amount.to_f} }}
      totals = totals.reduce Hash.new, :merge
      @total_paid = totals["PAID"].present? ? totals["PAID"] : 0
      @total_pending = @checks.select{|check| check.PENDING?}.try(:pluck, :actual_amount).try(:sum).to_f
      @total_void = totals["VOID"].present? ? totals["VOID"] : 0
      @total_failed = totals["FAILED"].present? ? totals["FAILED"] : 0
      @total_paid_wire = totals["PAID_WIRE"].present? ? totals["PAID_WIRE"] : 0
    end
    @checks=Kaminari.paginate_array(@checks).page(params[:page]).per(page_size)
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.iso? || @user.agent? || @user.partner?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    elsif !@user.merchant_id.nil? && @user.try(:permission).admin? || @user.try(:permission).regular? || @user.try(:permission).custom?
      @user.wallets.primary.where.not(location_id: nil).each do |w|
        @wallets << w
      end
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.primary.each do |w|
          @wallets << w
        end
      end
    end
    render partial: 'v2/merchant/shared/push_2_card_datatable' if request.xhr?
  end

  def debit_card_deposit_form
    @check = Check.new
    @user = current_user
    if @user.merchant_id.present?
      merchant = User.find(@user.merchant_id)
    else
      merchant = @user
    end
    @debit_cards = current_user.try(:cards).try(:instant_pay_cards)
    if @user.merchant?
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:location_id)
        @locations = Location.where(id: wallets)
      else
        @locations = current_user.get_locations
      end
    else
      @wallets = @user.wallets.primary
    end
    render partial: "new"
  end

  def show
    @check= TangoOrder.find params[:id]
    render partial: "v2/merchant/push_to_card/p2c_detail"
  end
###############################
  def location_data
    if @user.merchant?
      if params[:location_id].present?
        @location = Location.find(params[:location_id])
      else
        @location = Location.new
      end
      wallets = @location.wallets.primary.last
      if wallets.present?
        @balance = show_balance(wallets.id) - HoldInRear.calculate_pending(wallets.id)
      else
        @balance = 0
      end
      if params[:type] == 'push_to_card'
        if @user.merchant?
          if @location.fees.present?
            fee =  @location.fees.try(:buy_rate).try(:first)
            if fee.present?
              @fee_dollar = fee.dc_deposit_fee_dollar
              @fee_perc = fee.dc_deposit_fee
            end
          end
        end
        if @location.push_to_card_limit_type == "Transaction"
          @push_to_card_limit = @location.push_to_card_limit.to_f
          @push_to_card_limit_type = @location.push_to_card_limit_type
        elsif @location.push_to_card_limit_type == "Day"
          # date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          startdate = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc
          enddate = DateTime.now.in_time_zone("Pacific Time (US & Canada)").end_of_day.utc
          date=startdate..enddate
          if current_user.merchant_id.present? #&& current_user.submerchant_type == 'admin_user'  ## some account have nil value in this column
            instant_achs = current_user.parent_merchant.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.status !=?",@location.id,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil).where(created_at: date)
          else
            instant_achs = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.status !=?",@location.id,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil).where(created_at: date)
          end
          @push_to_card_limit = @location.push_to_card_limit.to_f

          @total_count = instant_achs.sum(:actual_amount)
          @push_to_card_limit_type = @location.push_to_card_limit_type
        else
          @push_to_card_limit = @location.push_to_card_limit.to_f
          @push_to_card_limit_type = @location.push_to_card_limit_type
        end
      end
      # =+=+=+=+=+=+=+=+=+ merchant end
    else
      if params[:location_id].present? && params[:location_id].to_i != 0
        @balance = show_balance(params[:location_id]) - HoldInRear.calculate_pending(params[:location_id])
        if params[:type] == 'push_to_card'
          @fee_dollar = @user.system_fee.try(:[],'push_to_card_dollar')
          @fee_perc = @user.system_fee.try(:[],'push_to_card_percent')
          if @user.system_fee.try(:[],'push_to_card_limit_type') == "Transaction"
            @push_to_card_limit = @user.system_fee.try(:[],'push_to_card_limit').to_f
            @push_to_card_limit_type = @user.system_fee.try(:[],'push_to_card_limit_type')
          elsif @user.system_fee.try(:[],'push_to_card_limit_type') == "Day"
            date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
            instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil)
            @push_to_card_limit = @user.system_fee.try(:[],'push_to_card_limit').to_f
            @total_count = instant_achs.sum(:actual_amount)
            @push_to_card_limit_type = @user.system_fee.try(:[],'push_to_card_limit_type')
          end
        end
        #adding limits for push to card
      else
        @balance = 0
      end
    end
    @balance = number_with_precision(@balance, precision: 2, delimiter: ',')
    @fee_dollar = number_with_precision(@fee_dollar, precision: 2, delimiter: ',')
    @fee_perc = number_with_precision(@fee_perc, precision: 2, delimiter: ',')
    render json: {"balance" => @balance,"fee_dollar" => @fee_dollar , "fee_perc" => @fee_perc, "is_location_block" => @location.try(:push_to_card),'p2c_limit_type' => @location.push_to_card_limit_type ,
                  "user_p2c" => @user.push_to_card , "user_system_fee_p2c_limit_type" => @user.system_fee.try(:[], 'push_to_card_limit_type'),
                  "location" => @location ,"user" => @user, "p2c_card_limit" => @push_to_card_limit,"p2c_card_limit_type" => @push_to_card_limit_type, "p2c_total_count" => @total_count

    }
  end


############################################
# check"=>{"sender"=>"shanmerchant@gmail.com", "sender_id"=>"32", "name"=>"zair",
# "recipient"=>"zair@gmail.com", "amount"=>"10", "description"=>"zaza", "check_id"=>"29", "card_number"=>"", "expiry_date"=>"", "cvv"=>"", "zip_code"=>"2456"}, "location_id"=>"37", "result"=>"true", "controller"=>"merchant/checks", "action"=>"create_debit_card_deposit"}
# require action cable and
def create
  begin
    amount = params[:check][:amount].to_f
    raise CheckError.new(I18n.t("errors.withdrawl.amount_must_greater_than_zero")) if amount < 1

    account_no = params[:check][:account_number]
    confirm_no = params[:check][:confirm_account_number]
    name = params[:check][:name]
    raise CheckError.new("Name should be present!") if name.blank?
    raise CheckError.new("Name should have atleast 2 characters!") if name.try(:strip).try(:length) < 2
    recipient = params[:check][:recipient]
    description = params[:check][:description]
    card_no = params[:check][:card_number]
    card_bin_list = get_card_info(card_no)
    if card_no.present?
      reg = /\D/
      raise CheckError.new(I18n.t('api.registrations.invalid_card_data')) if card_no.match(reg).present?
    end
    if card_bin_list.present? && card_bin_list.try(:[],'type').present? && card_bin_list.try(:[],'type')=="credit"
      return flash[:error]=  I18n.t('merchant.controller.debit_card_only')
    end
## Card Expiry :-:
    if params[:check][:expiry_date].present?
      exp_month = params[:check][:expiry_date].split('/').first
      exp_year = params[:check][:expiry_date].split('/').last
      current_year = Time.current.year - 2000
      if exp_year.to_i < current_year || exp_month.to_i > 12 || exp_month.to_i < 0
        return flash[:error]= I18n.t('api.registrations.invalid_card_data')
      end
    end
    if exp_year.present? && exp_year.to_s.size == 4
      exp_year = exp_year.to_i - 2000
    end
    exp_date = "20#{exp_year}-#{exp_month}"
##
    account_type = params[:check][:account_type]
    cvv = params[:check][:cvv]
    addr_1 = params[:check][:address_line_1]
    addr_2 = params[:check][:address_line_2]
    routing_no = params[:check][:routing_number]
    zip = params[:check][:zip_code]
    send_via = 'instant_pay'
    send_check_user_info = { name: name, check_email: recipient }
    if @user.MERCHANT?
      @location = Location.find_by(id: params[:location_id])
      raise CheckError.new(I18n.t("errors.withdrawl.blocked_p2c")) if @location.push_to_card
      @wallet = @location.wallets.try(:primary).try(:first)
      params[:wallet_id] = @wallet.id
    else
      @wallet = Wallet.find(params[:location_id])
      params[:wallet_id] = @wallet.id
    end
    # @user = @wallet.users.first
    if current_user.merchant_id != nil
      @user = current_user
    elsif current_user.merchant_id == nil
      @user = @wallet.users.first
    end
    balance = show_balance(@wallet.id, current_user.ledger).to_f
    last_4 = params[:check][:card_number].last(4) if params[:check][:card_number].present?
    first_6 = params[:check][:card_number].first(6) if params[:check][:card_number].present?

    # if @user.present? && !@user.merchant_id.nil?
    #   merchant = User.find(@user.merchant_id)
    # else
    #   merchant = @user
    # end
    merchant = @user
    if (params[:check][:check_id].present? && params[:other_card].blank?) || (params[:check][:check_id].present? && params[:other_card].present? && params[:check][:card_number].blank?) #= using exiting card
      card = Card.find_by(id: params[:check][:check_id])
      if card.card_type.present? && card.card_type=="debit"
        card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, merchant.id)
        last_4 = card.last4
        first_6 = card.first6
        raise CheckError.new(I18n.t('api.registrations.invalid_card_data')) if card_info.number.tr("0-9","").present?
        card_detail = {
            number: card_info.number,
            month: card_info.month,
            year: card_info.year,
            # cvv: card_info.cvv
        }
      else
        return flash[:error]= I18n.t('merchant.controller.debit_card_only')
      end
    else
      if params[:save_card].present? &&  params[:save_card] == "on"
        raise CheckError.new(I18n.t('api.registrations.invalid_card_data')) if card_no.tr("0-9","").present?
        card_detail = {
            number: card_no,
            month: exp_month,
            year: "20#{exp_year}",
            # cvv: cvv
        }
        card_bin_list = get_card_info(card_no)
        card1 = Payment::QcCard.new(card_detail, nil, merchant.id)
        cards = merchant.cards.instant_pay_cards.where(fingerprint: card1.fingerprint,merchant_id: merchant.id)
        if cards.blank?
          card = merchant.cards.create(qc_token:card1.qc_token,exp_date: "#{card_detail[:month]}/#{card_detail[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand, last4: last_4, name: name, card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: first_6,fingerprint: card1.fingerprint,merchant_id: merchant.id,instant_pay: true)
        else
          card = cards.first
          card.exp_date = params[:check][:expiry_date] if params[:check][:expiry_date].present?
        end
      end
    end

    if @user.MERCHANT?
      if @location.push_to_card_limit_type == "Day"
        todays_orders = TangoOrder.orders_for_today(@wallet.id, merchant.id, "instant_pay")
        todays_transactions_amount = todays_orders.pluck(:actual_amount).sum(&:to_f)
        todays_void_amount = todays_orders.VOID.pluck(:actual_amount).sum(&:to_f)
        todays_failed_amount = todays_orders.FAILED.pluck(:actual_amount).sum(&:to_f)
        valid_amount = todays_transactions_amount - todays_void_amount - todays_failed_amount + amount
        raise CheckError.new(I18n.t('errors.withdrawl.cant_proceed_daily_limit', location_limit: number_with_precision(@location.push_to_card_limit, :precision => 2, delimiter: ','))) if valid_amount > @location.push_to_card_limit && @location.push_to_card_limit != 0
      else
        raise CheckError.new(I18n.t('errors.withdrawl.cant_procedd_txn_limit', location_limit: number_with_precision(@location.push_to_card_limit, :precision => 2, delimiter: ','))) if amount > @location.push_to_card_limit && @location.push_to_card_limit != 0
      end
    elsif @user.iso? || @user.agent? || @user.affiliate?
      if @user.system_fee.try(:[],"push_to_card_limit_type") == "Day"
        todays_orders = TangoOrder.orders_for_today(@wallet.id, merchant.id, "instant_pay")
        todays_transactions_amount = todays_orders.pluck(:actual_amount).sum(&:to_f)
        todays_void_amount = todays_orders.VOID.pluck(:actual_amount).sum(&:to_f)
        todays_failed_amount = todays_orders.FAILED.pluck(:actual_amount).sum(&:to_f)
        valid_amount = todays_transactions_amount - todays_void_amount - todays_failed_amount + amount
        raise CheckError.new(I18n.t('errors.withdrawl.cant_proceed_daily_limit', location_limit: number_with_precision(@user.system_fee.try(:[],"push_to_card_limit"), :precision => 2, delimiter: ','))) if valid_amount > (@user.system_fee.try(:[],'push_to_card_limit').present? ? @user.system_fee.try(:[],'push_to_card_limit').try(:to_f) : 0)
      else
        raise CheckError.new(I18n.t('errors.withdrawl.cant_procedd_txn_limit', location_limit: number_with_precision(@user.system_fee.try(:[],"push_to_card_limit"), :precision => 2, delimiter: ','))) if amount > (@user.system_fee.try(:[],'push_to_card_limit').present? ? @user.system_fee.try(:[],'push_to_card_limit').try(:to_f) : 0)
      end
    end

    if card_detail.present?
      account_token_info =  {
          :card_number => card_detail[:number],
          :expiry_date => "#{card_detail[:year]}-#{card_detail[:month]}"
      }
    else
      account_token_info =  {
          :card_number => card_no,
          :expiry_date => exp_date
      }
    end


    account_token_info = account_token_info.to_json
    encrypted_card_info = AESCrypt.encrypt("#{merchant.id}-#{ENV['CARD_ENCRYPTER']}-#{@wallet.id}}", account_token_info)
    ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'fifty'
    if user_signed_in? && @wallet.present?
      amount_sum = 0
      total_fee = 0
      fee_lib = FeeLib.new
      if (!account_no.nil? && !confirm_no.nil? && account_no == confirm_no) || (account_no.nil? || confirm_no.nil?)
        if current_user.MERCHANT?
          location = @wallet.location

          raise CheckError.new(I18n.t('errors.withdrawl.blocked_p2c')) if location.push_to_card

          location_fee = location.fees if location.present?
          fee_object = location_fee.buy_rate.first if location_fee.present?
          # fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
          fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::DebitCardDeposit)
          fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::DebitCardDeposit)
          total_fee = fee["fee"].to_f
          amount_sum = amount + total_fee
          db_check_fee = fee_object.dc_deposit_fee_dollar.to_f
          db_fee_perc = deduct_fee(fee_object.dc_deposit_fee.to_f, amount)
        else
          fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
          if fee_object.present?
            db_check_fee = fee_object["push_to_card_dollar"].present? ? fee_object["push_to_card_dollar"].to_f : 0
            db_fee_perc = fee_object["push_to_card_percent"].present? ? deduct_fee(fee_object["push_to_card_percent"].to_f, amount) : 0
          else
            db_check_fee = 0
            db_fee_perc = 0
          end
          total_fee = db_fee_perc + db_check_fee
          amount_sum = amount + total_fee
        end
        splits = fee.try(:[],"splits")
        if splits.present?
          agent_fee = splits["agent"]["amount"]
          iso_fee = splits["iso"]["amount"]
          iso_balance = show_balance(location.iso.wallets.primary.first.id)
          if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
            if iso_balance < iso_fee
              raise CheckError.new(I18n.t("errors.iso.iso_0003"))
            end
          else
            if iso_balance + iso_fee < agent_fee.to_f
              raise CheckError.new(I18n.t("errors.iso.iso_0004"))
            end
          end
        end
        if current_user.agent? || current_user.affiliate?
          raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: current_user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < current_user.system_fee.try(:[],'check_withdraw_limit').to_f
        elsif current_user.iso?
          raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: current_user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < current_user.system_fee.try(:[],'check_withdraw_limit').to_f && current_user.iso?
        end

        raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if balance < amount_sum
        pending_amount = HoldInRear.calculate_pending(params[:wallet_id])
        raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if balance - pending_amount < amount_sum

        # if  balance < amount_sum
        #   flash[:error] = "Insufficient balance You can not create check!"
        #   redirect_to debit_card_deposit_merchant_checks_path
        # end
        if current_user.present? && !current_user.merchant_id.nil? && current_user.try(:permission).admin?
          user_id=current_user.id
        else
          user_id=current_user.id
        end
        ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'finalize'
        check_escrow = Wallet.check_escrow.first
        check = TangoOrder.new(user_id: merchant.id,
                               name: name,
                               status: "PENDING",
                               amount: amount_sum,
                               actual_amount: amount,
                               check_fee: db_check_fee,
                               fee_perc: db_fee_perc,
                               recipient: recipient,
                               description: description,
                               order_type: "instant_pay",
                               approved: false,
                               account_token: encrypted_card_info,
                               settled: false,
                               wallet_id: @wallet.id,
                               send_via: send_via,
                               account_number: account_no,
                               routing_number: routing_no,
                               last4: last_4,
                               first6: first_6,
                               batch_date:Time.zone.now + 1.day,
                               zip_code: zip,
                               amount_escrow: true)

        location1 = location_to_parse(location)
        merchant1 = merchant_to_parse(merchant&.parent_merchant || merchant)
        tags = {
            "fee_perc" => splits,
            "location" => location1,
            "merchant" => merchant1,
            "send_check_user_info" => { name: name, check_email: recipient,amount: amount,description: description,fee: total_fee },
            "send_via" => send_via,
            "card" => card
        }

        if @user.MERCHANT?
          new_fee = save_gbox_fee(splits, fee.try(:[], "fee").to_f)
          total_fee = save_total_fee(splits, fee.try(:[], "fee").to_f)
        else
          total_fee = db_fee_perc + db_check_fee
          new_fee = total_fee
        end
        sender_dba=get_business_name(@wallet)
        transaction = merchant.transactions.build(
            to: recipient,
            from: nil,
            status: "pending",
            charge_id: nil,
            amount: amount,
            sender: merchant,
            sender_name: sender_dba,
            sender_wallet_id: @wallet.id,
            receiver_wallet_id: check_escrow.id,
            receiver_name: check_escrow.try(:users).try(:first).try(:name),
            receiver_id: check_escrow.try(:users).try(:first).try(:id),
            sender_balance: balance,
            receiver_balance: SequenceLib.balance(check_escrow.id),
            action: 'transfer',
            fee: total_fee,
            net_amount: amount,
            net_fee: total_fee,
            total_amount: amount_sum.to_f,
            gbox_fee: new_fee.to_f,
            iso_fee: save_iso_fee(splits),
            agent_fee: splits.try(:[],"agent").try(:[],"amount").to_f,
            affiliate_fee: splits.try(:[],"affiliate").try(:[],"amount").to_f,
            ip: get_ip,
            main_type: "instant_pay",
            tags: tags,
            last4: last_4,
            first6: first_6
        )


        # transaction = @user.transactions.build(to: recipient, from: nil, status: "approved", charge_id: nil , amount: amount_sum, sender: current_user, action: 'retire', last4: last_4, first6: first_6)

        user_info = fee.try(:[], "splits")
        withdraw = withdraw(amount,@wallet.id, total_fee || 0, TypesEnumLib::TransactionType::DebitCardDeposit,last_4,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true, card)
        if withdraw.present? && withdraw["actions"].present?
          transaction.seq_transaction_id = withdraw["actions"].first["id"]
          transaction.status = "approved"
          transaction.privacy_fee = 0
          transaction.sub_type = withdraw["actions"].first.tags["sub_type"]
          transaction.timestamp = withdraw["timestamp"]
          transaction.save
          check.transaction_id = transaction.id
          check.save
          #= creating block transaction
          parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
          save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
          flash[:success] = "Successfully sent a Push to Card to #{ last_4 }."
        else
          flash[:error] = I18n.t 'merchant.controller.unaval_feature'
        end
      end

    end
  rescue => ex
    flash[:error] = I18n.t 'merchant.controller.deposit_failed'
    flash[:error] = ex.message if ex.class == CheckError
    # SlackService.handle_exception "Debit Card Deposit failed", ex
    # SlackService.notify("*Push To Card Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`")
  ensure
    if flash[:error].blank? && flash[:success].blank?
      flash[:error] = I18n.t 'merchant.controller.deposit_failed'
    end
    if flash[:error].blank?
      ActivityLog.log(nil,"Push to Card Created",current_user,params,response,"Push To Card Created [##{check.id}] for [$#{number_with_precision(amount_sum, precision: 2)}]","push_to_card")
      ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'done', message: [check.id]
    else
      if flash[:error].include?('Error Code')
        str = flash[:error]
        message = str[16, str.length-1]
        code = str[0,15]
      else
        message = flash[:error]
        code = " Error Code #"
      end
      ActionCable.server.broadcast "withdraw_response_channel_#{session[:session_id]}", channel_name: "withdraw_response_channel_#{session[:session_id]}", action: 'error', error_code: code,message: message
    end
      # return redirect_back(fallback_location: root_path)
  end
  flash.discard
end

  def present_card_verification
    if params[:card_number].blank?
      render json:{"success":"not_available"}, status: 200
    else
      if params[:present].present? && params[:present] == "present"
        card=Card.find_by_id(params[:card_number])
      else
        card=Card.where(first6:params[:card_number].first(6).to_i,last4: params[:card_number].last(4)).last
      end
      card_type=card.try(:card_type)
      card_scheme = card.try(:brand)
      first6 = card.try(:first6)
      if card_type.blank? || card_type=='credit'
        card_info = get_card_info(params[:card_number])
        card_type = card_info['type']
        card_scheme = card_info['scheme']
        card.update(card_type: card_type) if card.present?
      end
      if card_type =='debit' || (card_scheme == "mastercard" && card_type != 'credit')
        render json:{"success": card_scheme}, status: 200
      elsif card_type=='credit'
        render json:{"success":"false"} , status: 200
      else
        render json:{"success":"false"}, status: 200
      end
    end
  end
######################################      DELELTE SAVED CARD in push 2 card
  def delete_debit_card
    card = Card.find_by_id(params[:card_number])
    card.destroy if card.present?
    if card.present? && card.destroyed?
      render json:{"success":"true"}, status: 200
    else
      render json:{"success":"false"} , status: 200
    end
  end

end
