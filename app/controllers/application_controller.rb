class ApplicationController < ActionController::Base
  include ApplicationHelper
  # protect_from_forgery with: :null_session
  before_action :set_current_user, :authenticate_user!, :set_notifications, :set_raven_context
  before_action :set_request_data

  protected

  def authenticate_user!
    return redirect_to new_user_session_path(request_url:params[:request_url]) unless user_signed_in?
    super
  end

  def admin
    flash[:alert] = I18n.t("devise.failure.page_not_exist") unless current_user&.have_admin_access?
    return redirect_to '/' unless current_user&.have_admin_access?
  end

  def check_admin
    if !current_user.admin? && current_user.support?
      redirect_to transactions_admins_path
    end
  end

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def set_notifications
    if user_signed_in? && current_user.present? && current_user.admin?
      # user = User.where(email: "admin@quickcard.me").first
      @admin_notifications = current_user.notifications.where(action: ['invoice', 'check']).unread
      @ach_notifications = current_user.notifications.where(action: ['instant_ach']).unread
    end
  end

  def set_current_user
    gon.cable_url = "ws://#{ENV['CABLE_URL']}"
    User.current = current_user
  end

  def set_raven_context
    Raven.user_context(
      id: current_user&.id || @user&.id || '#NoUser',
      email: current_user&.email || @user&.email,
      ip_address: request&.ip
    )
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

  # Sets the RequestInfo into a named Thread location so that it can be accessed by models and observers
  def set_request_data
    RequestInfo.current_request(request, current_user || nil)
  end

end
