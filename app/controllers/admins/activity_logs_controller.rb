class Admins::ActivityLogsController < AdminsController
  before_action :check_admin

  def index
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    @klass = "collapse mb-3"
    if params[:query].present?
      if params[:query]["user_id"].present? && params[:query]['merchant_id'].present?
        submerchant_ids =  User.where(merchant_id: params[:query]["merchant_id"]).pluck(:id)
        if submerchant_ids.present?
          submerchant_ids << params[:query]["user_id"].to_i unless submerchant_ids.include? params[:query]["user_id"].to_i
          submerchant_ids << params[:query]["merchant_id"].to_i
          ids = submerchant_ids
          @key = " user_id IN (:user_id)"
        else
          ids = [ params[:query]['merchant_id'].to_i, params[:query]["user_id"].to_i ]
          @key = " user_id IN (:user_id)"
        end
      elsif params[:query]["merchant_id"].present?
        submerchant_ids =  User.where(merchant_id: params[:query]["merchant_id"]).pluck(:id)
        if submerchant_ids.present?
          submerchant_ids << params[:query]["merchant_id"].to_i
          ids = submerchant_ids
          @key = " user_id IN (:user_id)"
        else
          ids = params[:query]["merchant_id"]
          @key = "user_id = :user_id"
        end
      elsif params[:query]["user_id"].present?
          ids = params[:query]["user_id"]
          @key = "user_id = :user_id"
      end
      if params[:query]["event"].present? && params[:query]["event"] != "tango_order"
        if @key.nil?
          @key = " action IN (:event)"
        else
          @key += " AND action IN (:event)"
        end
      elsif params[:query]["event"].present? && params[:query]["event"] == "tango_order"
        if @key.nil?
          @key = " activity_type = 11"
        else
          @key += " AND activity_type = 11"
        end
      end
      if params[:query]["date"].present?
        date = parse_date(params[:query][:date])
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset])
        second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset])
        if @key.nil?
          @key = " activity_logs.created_at BETWEEN :first_date AND :second_date"
        else
          @key += " AND activity_logs.created_at BETWEEN :first_date AND :second_date"
        end
      end

      event = nil
      if params[:query]["event"].present?
        event = params[:query]["event"].eql?(I18n.t("event_types.account_transfer")) ? [I18n.t("event_types.account_transfer"), I18n.t("event_types.transfer_post")] : params[:query]["event"].split(',')
      end
      if @key.present? && (params[:query]["user_id"].present? || params[:query]["merchant_id"].present? || params[:query]["date"].present? || params[:query]["event"].present?)
        @logs = ActivityLog.includes(:user).where(@key, user_id: ids, event: event,first_date: first_date,second_date: second_date)
      if params[:query]["event"].present? && params[:query]["event"] == I18n.t("event_types.refund_money")
        event.push(I18n.t("event_types.refund_transaction"), I18n.t("event_types.refund_merchant_transaction"))
      end

        if params[:query]["status"].present?
          if params[:query]["status"] == "True"
            @logs = @logs.where(activity_type: I18n.t("activity_type.api_success"))
          else
            @logs = @logs.where(activity_type: I18n.t("activity_type.api_error"))
          end
        end
        @logs = @logs.order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
      elsif params[:query]["status"].present?
        if params[:query]["status"] == "True"
          @logs = ActivityLog.includes(:user).where(activity_type: I18n.t("activity_type.api_success"))
        else
          @logs = ActivityLog.includes(:user).where(activity_type: I18n.t("activity_type.api_error"))
        end
        @logs = @logs.order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
       end
      @klass = "collapsed" if params[:query]["status"].present? || params[:query]["event"].present? || params[:query]["user_id"].present? || params[:query]["merchant_id"].present? || params[:query]["date"].present?
    end
    @logs = ActivityLog.includes(:user).order(id: :desc).per_page_kaminari(params[:page]).per(per_page) if @logs.nil?
    @events = Transaction::EVENTS
  end

  def affiliate_logs
    @klass = "collapse mb-3"
    if params[:query].present?
      if params[:query]["user_id"].present?
        @key = "user_id = :user_id"
      end
      if params[:query]["event"].present? && params[:query]["event"] != "tango_order"
        if @key.nil?
          @key = " action IN (:event)"
        else
          @key += " AND action IN (:event)"
        end
      elsif params[:query]["event"].present? && params[:query]["event"] == "tango_order"
        if @key.nil?
          @key = " activity_type = 11"
        else
          @key += " AND activity_type = 11"
        end
      end
      if params[:query]["date"].present?
        date = parse_date(params[:query][:date])
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset])
        second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset])
        if @key.nil?
          @key = " activity_logs.created_at BETWEEN :first_date AND :second_date"
        else
          @key += " AND activity_logs.created_at BETWEEN :first_date AND :second_date"
        end
      end
      event = params[:query]["event"].present? ? params[:query]["event"].split(',') : nil
      if @key.present? && (params[:query]["user_id"].present? || params[:query]["date"].present? || params[:query]["event"].present?)
        @logs = ActivityLog.includes(:user).where(action_type:'affiliate_program').where(@key, user_id: params[:query]["user_id"], event: event,first_date: first_date,second_date: second_date).or(ActivityLog.includes(:user).where(activity_type:'affiliate_activity').where(@key, user_id: params[:query]["user_id"], event: event,first_date: first_date,second_date: second_date))
        if params[:query]["status"].present?
          if params[:query]["status"] == "True"
            @logs = @logs.where(activity_type: "api_success")
          else
            @logs = @logs.where(activity_type: "api_error")
          end
        end
        @logs = @logs.order(id: :desc).per_page_kaminari(params[:page]).per(10)
      elsif params[:query]["status"].present?
        if params[:query]["status"] == "True"
          @logs = ActivityLog.includes(:user).where(activity_type: "api_success").where(action_type:'affiliate_program')
        else
          @logs = ActivityLog.includes(:user).where(activity_type: "api_error").where(action_type:'affiliate_program')
        end
        @logs = @logs.order(id: :desc).per_page_kaminari(params[:page]).per(10)
      end
      @klass = "collapsed"
    end
    @logs = ActivityLog.includes(:user).where(action_type:'affiliate_program').or(ActivityLog.includes(:user).where(activity_type:'affiliate_activity')).order(id: :desc).per_page_kaminari(params[:page]).per(10) if @logs.nil?
    @events = ["deposit","affiliate", "affiliate_user_create", "update", "admin_user_create"]
  end

  def email_track
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    if params[:q].present?
      if params[:q][:sent_at_eq].present?
        date = parse_date(params[:q][:sent_at_eq])
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
        second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
        params[:q][:sent_at_gteq] = first_date
        params[:q][:sent_at_lteq] = second_date
        params[:date] = params[:q][:sent_at_eq]
        params[:q][:sent_at_eq] = ''
      end
    end
    @mailer_option = [{value:"UserMailer#welcome_merchant",view:"Welcome Merchant"},{value:"UserMailer#welcome_user",view:"Welcome User"},{value:"Devise::Mailer#reset_password_instructions",view:"Reset Password"},{value:"UserMailer#new_location_email",view:"New Location Email"},{value:"UserMailer#transaction_email",view:"Transaction Email"}]

    @q = EmailMessage.ransack(params[:q])
    @email = @q.result(distinct: true).order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
  end

  def version
    @version_number = AppConfig.find_by(key: "version").try(:stringValue) || ENV['version_tag']
  end

  def post_version
    @version_number = AppConfig.find_by(key: "version")
    if @version_number.present?
      @version_number.update(stringValue: params[:version])
    else
      AppConfig.create(key: "version",stringValue: params[:version])
    end
    flash[:notice] = "Updated Successfully!"
    redirect_back(fallback_location: root_path)
  end

  def un_authorized
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    @logs = ActivityLog.includes(:user).unauthorized.per_page_kaminari(params[:page]).per(per_page)
  end

  def not_found
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    @logs = ActivityLog.includes(:user).not_found.per_page_kaminari(params[:page]).per(per_page)
  end

  def server_error
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    @logs = ActivityLog.includes(:user).server_error.per_page_kaminari(params[:page]).per(per_page)
  end

  def bad_request
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    @logs = ActivityLog.includes(:user).bad_request.per_page_kaminari(params[:page]).per(per_page)
  end

  def system
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    @logs = ActivityLog.includes(:user).message.per_page_kaminari(params[:page]).per(per_page)
  end

  def api_setting
    @users = User.merchant.where(merchant_id: nil)
    @apis_config = AppConfig.where(key: AppConfig::Key::APIsAuthorization).first
    @apis_config = AppConfig.new(key: AppConfig::Key::APIsAuthorization, stringValue: {virtual_existing: true, virtual_debit_charge: true, virtual_new_signup: true, virtual_terminal_transaction: true, virtual_transaction: true, refund: true}.to_json) unless @apis_config.present?
    @seq_down = AppConfig.sequence_down.first
    @api_names_tags = {virtual_existing: "Virtual Existing (Old)", virtual_debit_charge: "Virtual Debit Charge (Direct w/o Customer)", virtual_new_signup: "Virtual New Signup (Old)", virtual_terminal_transaction: "Virtual Terminal Transaction (Direct w/o Customer)", virtual_transaction: "Virtual Transaction (e-Commerce)", refund: "Refund (Credit Card)"}.to_json
    gon.apis_config = @apis_config.to_json.html_safe
  end

  def show
    @log = ActivityLog.find(params[:id])
    respond_to :js
  end
end