class Admins::InvoicesController < AdminsController
  layout 'admin'
  require 'raas'
  before_action :check_admin
  def index
    @invoices=TangoOrder.invoice.order(id: :desc)
    notifications = current_user.notifications.where(action: 'invoice').unread
    notifications.update_all(read_at: DateTime.now.utc)
  end

  def invoice_filter
    if !params[:all_invoices_check].nil?
      @invoices = TangoOrder.invoice.order(id: :desc)
    else
      @invoices = []
      if !params[:paid_invoices_check].nil?
        paid_invoices=TangoOrder.where(status: "PAID").invoice.order(id: :desc)
        @invoices = @invoices + paid_invoices
      end
      if !params[:unpaid_invoices_check].nil?
        unpaid_invoices=TangoOrder.where(status: "UNPAID").invoice
        @invoices = @invoices + unpaid_invoices
      end
      if !params[:cancelled_invoices_check].nil?
        void_invoices=TangoOrder.where(status: "CANCELED").invoice.order(id: :desc)
        @invoices = @invoices + void_invoices
      end
      if !params[:inprogress_invoices_check].nil?
        inprocess_invoices=TangoOrder.where(status: "IN_PROCESS").invoice.order(id: :desc)
        @invoices = @invoices + inprocess_invoices
      end
    end
    respond_to do |format|
      format.js {
        render 'admins/invoices/filtered_invoices.js.erb'
      }
    end
  end

  def show
    @check= TangoOrder.find params[:id]
    respond_to :js
  end

  def pay_invoice
    invoice = TangoOrder.find(params[:id])

    # app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    wallet = Wallet.where(id: invoice.wallet_id).first if invoice.wallet_id.present?
    # raise StandardError.new 'No Wallet attached with Check'
    wallet_user = wallet.users.first
    total_fee = invoice.check_fee.to_f + invoice.fee_perc.to_f
    if total_fee.to_f > 0
      if wallet_user.merchant?
        # fee_lib = FeeLib.new
        # qc_wallet = Wallet.where(wallet_type: 3).first
        location = wallet.location
        location_fee = location.fees if location.present?
        fee_object = location_fee.buy_rate.first if location_fee.present?
        fee_class = Payment::FeeCommission.new(wallet_user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::AddMoney)
        fee = fee_class.apply(invoice.amount.to_f, TypesEnumLib::CommissionType::AddMoney)
        user_info = fee["splits"]
        issue = issue_amount(invoice.amount,wallet,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Checkbook, fee["fee"], user_info,nil,invoice.number)
      else
        issue = issue_amount(invoice.amount,wallet,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Checkbook, total_fee, nil,nil,invoice.number, 'qc_wallet')
      end
    else
      issue = issue_amount(invoice.amount,wallet,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Checkbook, 0, nil,nil,invoice.number)
    end
    if issue.present?
      invoice.update(status: 'PAID')
      #= creating block transaction
      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
    end
    redirect_to admins_invoices_path
  end


  def edit
  end

  def verify_phone_number
    if params[:unlock].present?
      invoice = Wallet.find(params[:wallet_id]) if params[:wallet_id].present?
      invoice = Fee.find(params[:fee_id]) if params[:fee_id].present?
      invoice = Location.find(params[:location_id]) if params[:location_id].present?
    else
      invoice = TangoOrder.find(params[:invoice_id])
    end
    status=''
    if params[:phone_number].present?
      verification_phone_numbers = ENV["VERIFICATION_PHONE_NUMBERS"].present? ? ENV["VERIFICATION_PHONE_NUMBERS"].split(",") : [ENV['PHONE_NUMBER_FOR_PAY_INVOICE2'],ENV['PHONE_NUMBER_FOR_PAY_INVOICE1']]
      if (verification_phone_numbers.include?(params[:phone_number]))
        @random_code = rand(1_000..9_999)
        invoice.update(pincode: @random_code)
        # a = params[:phone_number]
        a = "1"+params[:phone_number]
        if params[:status_selected].present?
          status=params[:status_selected]=='PAID' ? 'Paid' : params[:status_selected]=='FAILED' ? 'Failed' : params[:status_selected]=='VOID' ? 'Void' : params[:status_selected]=='in_progress' ? 'In-Progress' : params[:status_selected]=='paid_wire' ? 'Paid-Wire' : params[:status_selected]
        end
        if params[:identity].present?
          if invoice.instant_ach?
            TextsmsWorker.perform_async(a, "Your pin Code for #{status} ACH is: #{@random_code}")
          else
            TextsmsWorker.perform_async(a, "Your pin Code for #{status} Check is: #{@random_code}")
          end
        else
          TextsmsWorker.perform_async(a, "Your pin Code for Invoice is: #{@random_code}")
        end
      else
        render status: 404, json:{error: 'Wrong Phone Number'}
      end
    end
    if params[:pincode].present?
      if params[:pincode] == invoice.pincode
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Wrong Pin Code'}
      end
    end
  end


end
