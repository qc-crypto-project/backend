class Admins::CommentsController < AdminsController

  def create
    @ticket = Ticket.friendly.find(params[:ticket_id])
    @comment = @ticket.comments.build
    @comment.body = params[:comment][:body]
    @comment.user_id  = current_user.id
    if @comment.save
      if params[:attachments].present?
        params[:attachments].first.split(',').each do |img|
          image=Image.find img
         @comment.images << image
        end
      end
      @ticket.update(status: "processed", merchant_attention: true)
      flash[:notice] = "Commented" 
      respond_to :js
    end
  end
end
