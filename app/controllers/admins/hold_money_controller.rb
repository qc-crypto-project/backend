class Admins::HoldMoneyController < AdminsController

  before_action :check_admin

  def index
    @heading = "Hold Release Schedule"
    if params[:query].present?
      key = nil
      location_keys=nil
      locations = []
      date = nil
      if params[:query]["name"].present?
        key = "users.name ILIKE :name"
      end

      if params[:query]["location_dba_name"].present?
        if location_keys.nil?
          location_keys = "business_name ILIKE :business_name"
        else
          location_keys += " Or business_name ILIKE :business_name"
        end
      end

      if params[:query]["name"].present?
        users = User.merchant.where(merchant_id: nil).includes(:wallets).where(key,name: "%#{params[:query]["name"]}%")
        users.each do |user|
          locations << user.wallets.primary.first.try(:location_id)
        end
        locations.compact!
      end
      if params[:query]["location_dba_name"].present?
        hold_in_rears_loc = Location.where(location_keys,business_name: "%#{params[:query]["location_dba_name"]}%").pluck(:id)
        if locations.present?
          locations.push(hold_in_rears_loc)
          locations = locations.flatten
          locations = locations.uniq
        else
          locations = hold_in_rears_loc
        end
      end
      if params[:query]["date"].present?
        date = parse_date(params[:query]["date"])
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).to_datetime.utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).to_datetime.utc
        }
      end

      if date.present? && locations.present?
        @hold_in_rears = HoldInRear.is_released.includes(:location).by_location_ids(locations).where(release_date: date[:first_date]..date[:second_date]).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(10)
      elsif date.present?
        @hold_in_rears = HoldInRear.is_released.where(release_date: date[:first_date]..date[:second_date]).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(10)
      elsif locations.present?
        @hold_in_rears = HoldInRear.is_released.by_location_ids(locations).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(10)
      else
        @hold_in_rears = []
        @hold_in_rears = Kaminari.paginate_array(@hold_in_rears).page(params[:page]).per(10)
      end
    else
      @hold_in_rears = HoldInRear.is_released.includes(:location).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(10)
    end
  end

  def transactions
    hold_in_rear = HoldInRear.find(params[:hold_in_rear_id])
    created_at = hold_in_rear.created_at
    wallet = hold_in_rear.location.wallets.primary.last
    @transactions = Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["complete","approved"], main_type:["eCommerce","Virtual Terminal","Credit Card"],to: wallet.id).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["refunded"], to: wallet.id)).distinct.per_page_kaminari(params[:page]).per(10)
  end
end
