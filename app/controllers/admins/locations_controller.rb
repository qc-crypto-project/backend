class Admins::LocationsController < AdminsController
  include AdminsHelper
  before_action :save_my_previous_url
  skip_before_action :check_admin
  skip_before_action :mtrac_admin, except: :new

  def index
    merchant_location = []
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    if params[:from_specific_gateway] == "true"
      payment_gateway_id = params[:payment_gateway_id]
      @locations = Location.asg_pay(payment_gateway_id)
    elsif params[:user_id].present?
      user = User.friendly.find params[:user_id]
      @locations = Location.where(merchant_id: user.id)
    else
      @filtered_locations = Location.joins(:merchant)
      if params[:query].present?
        params[:query]["find_by"] = params[:query]["find_by"].try(:strip)
        @locations=[]
        search_operation = 'like'
        if params[:query]["option"]=='merchant_name' || params[:query]["option"]=='merchant_email'
          search_by='lower(name)' if params[:query]["option"]=='merchant_name'
          search_by='lower(email)' if params[:query]["option"]=='merchant_email'
          merchant_name = User.where("#{search_by} #{search_operation} ?", "%#{params[:query]["find_by"].downcase}%").pluck(:id)
          if merchant_name.present?
            location_name  = @filtered_locations.where(merchant_id: merchant_name)
            @locations=location_name if location_name.present?
          end
        end
        if params[:query]["option"]=='merchant_id'
          merchant_id = User.where(id: params[:query]["find_by"]).pluck(:id)
          location_id  = @filtered_locations.where(merchant_id: merchant_id) if merchant_id.present?
          @locations=location_id if location_id.present?
        end
        if params[:query]["option"]=='main_wallet_id'
          location_id= Wallet.primary.where(id: params[:query]["find_by"]).pluck(:location_id)
          @locations  = @filtered_locations.where(id: location_id) if location_id.present?
        end
        if params[:query]["option"]=='business_name'
          @locations= @filtered_locations.where("lower(locations.business_name) #{search_operation} ?", "%#{params[:query]["find_by"].downcase}%")
        end
        if params[:query]["option"]=='web_site'
          @locations= @filtered_locations.where("lower(locations.web_site) #{search_operation} ?", "%#{params[:query]["find_by"].downcase}%")
        end
        if params[:query]["option"]=='category'
          @locations = @filtered_locations.joins(:category).where("categories.name ILIKE ?","%#{params[:query]["find_by"].downcase}%")
        end
        if params[:query]["option"]=='token'
          @locations = @filtered_locations.joins(:ecomm_platform).where("ecomm_platforms.name ILIKE ?","%#{params[:query]["find_by"].downcase}%")
          # @locations= Location.where("lower(ecom_platform) #{search_operation}?", "%#{params[:query]["find_by"].downcase}%")
        end
        if params[:query]["option"]=='city'
          @locations= @filtered_locations.where("lower(locations.city) #{search_operation}?", "%#{params[:query]["find_by"].downcase}%")
        end
        if params[:query]["option"]=='state'
          @locations= @filtered_locations.where("lower(locations.state) #{search_operation}?", "%#{params[:query]["find_by"].downcase}%")
        end
        if params[:query]["option"]=='location_secure_token'
          @locations= @filtered_locations.where("locations.location_secure_token = ?", "#{params[:query]["find_by"]}")
        end
      else
        @locations = @filtered_locations
      end
    end
    @locations = @locations.order(created_at: :desc ) unless @locations.empty?
    @locationss = Kaminari.paginate_array(@locations || []).page(params[:page] || 1).per(per_page)
  end

  def new
    @slot_gateway = PaymentGateway.active_gateways.slot.order(slot_name: :asc)
    if params[:user].present?
      @user = User.friendly.find(params[:user])
      @categories = Category.all.order(name: :asc)
      @ecom_platforms = EcommPlatform.all.order(name: :asc)
      @location = Location.new
      @location.fees.build
      @bank = BankDetail.new
    end
    render template: 'admins/locations/new'
  end

  def location_add_category
    @category = Category.new
    respond_to :js
  end

  def create_category
    begin
      if params[:name].present? and params[:name] != ""
        if Category.where(name: params[:name]).present?
          return render json:{message:"Category Already exist!"}
        else
          @category = Category.new(name: params[:name])
          @category.save
        end
        # redirect_back(fallback_location: root_path)
      end
      if @category.present?
        return render json:{id:@category.id,name:@category.name}
      else
        return render json:{message:"Fill Up Your Fields!"}
      end
    rescue ActiveRecord::RecordInvalid => invalid
      return render json:{message:"Category Already exist!"}
    end
  end

  def location_add_ecom_platform
    @ecom_platform = EcommPlatform.new
    respond_to :js
  end

  def create_ecom_platform
    if params[:name].present? and params[:name] != ""
      @ecom_platform = EcommPlatform.new(name: params[:name])
      @ecom_platform.save
    # redirect_back(fallback_location: root_path)
    end
    if
    @ecom_platform.present?
      render json:{id:@ecom_platform.id,name:@ecom_platform.name}
    else
      render json:{message:"Fill Up Your Fields!"}
    end
  end

  def export
    loc=JSON.parse(params[:query]) if params[:query].present?
    if params[:query].present?
      loc["find_by"] = loc["find_by"].try(:strip)
      @locations=[]
      search_operation = 'like'
      if loc["option"]=='merchant_name' || loc["option"]=='merchant_email'
        search_by='lower(name)' if loc["option"]=='merchant_name'
        search_by='lower(email)' if loc["option"]=='merchant_email'
        merchant_name = User.where("#{search_by} #{search_operation} ?", "%#{loc["find_by"].downcase}%").pluck(:id)
        if merchant_name.present?
          location_name  = Location.includes(:ecomm_platform).where(merchant_id: merchant_name)
          @locations=location_name if location_name.present?
        end
      end
      if loc["option"]=='merchant_id'
        merchant_id = User.where(id: loc["find_by"]).pluck(:id)
        location_id  = Location.includes(:ecomm_platform).where(merchant_id: merchant_id) if merchant_id.present?
        @locations=location_id if location_id.present?
      end
      if loc["option"]=='main_wallet_id'
        location_id= Wallet.primary.where(id: loc["find_by"]).pluck(:location_id)
        @locations  = Location.includes(:ecomm_platform).where(id: location_id) if location_id.present?
      end
      if loc["option"]=='business_name'
        @locations= Location.includes(:ecomm_platform).where("lower(business_name) #{search_operation} ?", "%#{loc["find_by"].downcase}%")
      end
      if loc["option"]=='web_site'
        @locations= Location.where("lower(web_site) #{search_operation} ?", "%#{loc["find_by"].downcase}%")
      end
      if loc["option"]=='category'
        @locations = Location.includes(:ecomm_platform).joins(:category).where("categories.name ILIKE ?","%#{loc["find_by"].downcase}%")
      end
      if loc["option"]=='token'
        @locations = Location.joins(:ecomm_platform).where("ecomm_platforms.name ILIKE ?","%#{loc["find_by"].downcase}%")
        # @locations= Location.where("lower(ecom_platform) #{search_operation}?", "%#{params[:query]["find_by"].downcase}%")
      end
      if loc["option"]=='city'
        @locations= Location.includes(:ecomm_platform).where("lower(city) #{search_operation}?", "%#{loc["find_by"].downcase}%")
      end
      if loc["option"]=='state'
        @locations= Location.includes(:ecomm_platform).where("lower(state) #{search_operation}?", "%#{loc["find_by"].downcase}%")
      end
    else
      @locations = Location.includes(:ecomm_platform).all
    end
    if params['merchant_location'].present?
      merchant = User.friendly.find(params["merchant_location"])
      send_data generate_csv(nil,true,merchant.id), filename: "M-#{merchant.id}_Locations.csv"
    else
      send_data generate_csv(@locations,false), filename: "Locations.csv"
    end

  end

  def generate_csv(locations,from_merchant,merchant_id=nil)
    if from_merchant
      locations = Location.includes(:merchant).where(merchant_id: merchant_id)
      CSV.generate do |csv|
        csv << %w{ Id Business_name Email Website Category }
        locations.each do |l|
          website = JSON(l.web_site) if l.web_site.present?
          csv << [l.id, l.business_name, l.email, website, l.category.present? ? l.category.try(:name) : ""]
        end
      end
    else
      gateway_ids = locations.pluck(:primary_gateway_id,:secondary_gateway_id, :ternary_gateway_id).flatten.compact.uniq
      gateways = PaymentGateway.where(id: gateway_ids).select("id AS id", "name AS name").group_by{|e| e.id}
      merchants = User.where(id: locations.pluck(:merchant_id).uniq).select("id AS id","name AS name", "email AS email").group_by{|e| e.id}
      wallets = Wallet.primary.where.not(location_id: nil).select("id AS id, location_id AS location_id").group_by{|e| e.location_id}
      categories = Category.select("id AS id, name AS name").group_by{|e| e.id}
      location_users = LocationsUsers.joins(:user).where("users.role IN (?)", [7,9,10]).where(location_id:locations.pluck(:id)).select("location_id", "users.role as user_role","users.name as users_name").group_by{|e| e.location_id}
      my_loc = {}
      location_users.each do |loc_id,loc_users|
        my_loc[loc_id] = {} if my_loc[loc_id].blank?
        loc_users.each do |loc_user|
          my_loc[loc_id]["iso"] = loc_user.users_name if loc_user.user_role == 7
          my_loc[loc_id]["agent"] = loc_user.users_name if loc_user.user_role == 9
          my_loc[loc_id]["affiliate"] = loc_user.users_name if loc_user.user_role == 10
        end
      end
      CSV.generate do |csv|
        csv << ['Merchant ID', 'Merchant Name', 'Merchant Email',  'DBA Name', 'Main Wallet ID', 'E-Commerce Platform', 'Category', 'Website', 'ISO Name', 'Agent Name', 'Affiliate Name', 'Risk', 'Bank 1', 'Bank 2', 'Bank 3']
        locations.find_each do |l|
          merchant = merchants[l.merchant_id].try(:first)
          if merchant.present?
            csv << [l.merchant_id, merchant.name, merchant.email, l['business_name'], wallets[l.id].try(:first).try(:id), l.ecomm_platform.try(:name), categories[l.category_id].try(:first).try(:name), l.web_site, my_loc.try(:[],l.id).try(:[],"iso"), my_loc.try(:[],l.id).try(:[],"agent"), my_loc.try(:[],l.id).try(:[],"affiliate"), l.risk, gateways[l.primary_gateway_id].try(:first).try(:name), gateways[l.secondary_gateway_id].try(:first).try(:name), gateways[l.ternary_gateway_id].try(:first).try(:name)]
          end
        end
      end
    end
  end

  def update_location_token
    location=Location.find_by(id:params['id'])
    location.update_token
    flash[:notice] = location.present? ?  "Successfully updated!" : "Something went wrong!"
    redirect_back(fallback_location: root_path)
  end

  def get_gateways
    @location = Location.find_by_id(params[:id])
    respond_to :js
  end

  def update_gateway
    location = Location.find_by_id(params[:location][:id])
    if location.present?
      if location.update(edit_location_params)
        flash[:success] = "Successfully updated the Gateway Information "
        redirect_back(fallback_location: locations_path)
      else
        flash[:error] = "Something went wrong! Location update Failed!"
        redirect_to locations_path
      end
    else
      flash[:error] = "No location found!"
      redirect_to locations_path
    end
  end

  def edit
    @slot_gateway = PaymentGateway.active_gateways.slot.order(slot_name: :asc)
    @location = Location.friendly.find params[:id]
    @images = Documentation.where(imageable_id: @location.id)
    slots = @location.slot
    @slot_over = slots.try(:slot_over)
    @slot_amount = slots.try(:amount)
    @slot_under = slots.try(:slot_under)
    redirect_back(fallback_location: root_path) if @location.nil?
    @categories = Category.all.order(name: :asc)
    @ecom_platforms = EcommPlatform.all.order(name: :asc)
    @user = @location.merchant
    @users = @location.users
    @agent = nil
    @iso = nil
    @affiliate = nil
    @user_1 = nil
    @user_2 = nil
    @user_3 = nil
    if @location.profit_split_detail.present?
      @profit_detail = JSON.parse(@location.profit_split_detail)
    end
    @users.each do |u|
      if u.iso?
        @iso = u
      end
      if u.agent?
        @agent = u
      end
      if u.affiliate?
        @affiliate = u
      end
    end
    if @location.fees.empty?
      f = Fee.where(location_id: @location.id)
      @location.fees << f
    end
    @fee_source = @location.fees.buy_rate.first
    @fee_source = @location.fees.buy_rate.new if @fee_source.nil?
    begin
    @location.bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@location.bank_account)
    rescue
      @location.bank_account
    end
    @duplicated = params["duplicated"]=="true" ? true : false #for duplicate button
    gon.location_edit_data = @location
    gon.location_data = @location
    gon.profit_split_data = iso_profit_split_locations(@location)
    gon.net_profit_locations = net_profit_split_locations(@location)
    gon.ez_merchant_data = ez_merchant_locations(@location)
    if @duplicated
      render partial: 'duplicate'
      # render :duplicate
    else
      render :edit
    end
  end

  def show
    @location = Location.friendly.find params[:id]
    @user = @location.merchant
    @wallets = @location.wallets
    render :show
  end

  def block_transactions
    wallet = Wallet.friendly.find(params[:id])
    sender_transactions = []
    receiver_transactions = []
    sender_transactions = wallet.sender_wallet_transactions
    receiver_transactions = wallet.receiver_wallet_transactions
    transactions = sender_transactions + receiver_transactions
    @transactions = Kaminari.paginate_array(transactions).page(params[:page]).per(10)
  end

  def transactions
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : params[:tx_filter1].present? ? params[:tx_filter1].to_i : 10
    @filters = [10,25,50,100]
    @transactions = []
    @tickets = []
    @page_ticket = 0
    @amount_deposit = 0
    @amount_spend = 0
    @weekly_deposit_limit = 0
    @weekly_spend_limit = 0
    @transactions_count = 0
    @wallet = Wallet.friendly.find(params[:wallet_id])
    @location = @wallet.location
    @user = @location.try(:merchant) || User.find_by_id(@location.merchant_id)
    @role = @user.role
    @wallet_balance = SequenceLib.balance(@wallet.id)
    @types = [{value:"ACH",view:"ACH"},{value:"ACH Deposit",view:"ACH Deposit"},{value:"account_transfer",view:"Account Transfer"},{value: "afp",view: "AFP"},{value: "Buy rate fee", view: TypesEnumLib::TransactionType::BuyRateFee},{value: "cbk_dispute_accepted", view: "CBK Dispute Accepted"},{value: "cbk_fee", view: "CBK Fee"},{value: "cbk_hold", view: "CBK Hold"},{value: "cbk_lost", view: "CBK Lost"},{value: "CBK Lost Retire", view: "CBK Lost Retire"},{value: "cbk_won", view: "CBK Won"},{value:"Credit Card",view:"Credit Card"},{value:TypesEnumLib::TransactionViewTypes::DebitCard,view:"Debit Card"},{value:"eCommerce",view:"eCommerce"},{value:I18n.t("withdraw.failed_check"),view:"Failed Check"},{value:"Failed ACH",view:"Failed ACH"},{value:"Failed Push To Card",view:"Failed Push To Card"},{value:"Giftcard Purchase",view:"Giftcard Purchase"},{value:"Invoice - Credit Card",view:"Invoice - Credit Card"},{value:"Invoice - Debit Card",view:"Invoice - Debit Card"},{value:"Invoice Qc",view:"Invoice - QC"},{value:"manual_retire",view:"Manual Retire"},{value: "Misc Fee", view: "Misc Fee"},{value: "Service Fee", view: "Monthly Fee"},{value:"PIN Debit",view:"PIN Debit"},{value:"instant_pay",view:"Push To Card"},{value:"payed_tip",view:"Paid Tip"},{value:"QR_Scan",view:"QR Code"},{value:"QCP Secure",view:"QCP Secure"},{value:"QC Secure",view:"QC Secure"},{value:"refund",view:"Refund"},{value:"Reserve money deposit",view:"Reserve Money Deposit"},{value:"Reserve_Money_Return",view:"Reserve Money Return"},{value: "retrievel_fee", view: "Retrieval Fee"},{value:"sale_giftcard",view:"Sale Giftcard"},{value:"Sale Issue",view:"Sale Issue"},{value:"Sales tip",view:"Sale Tip"},{value:"Send Check",view:"Send Check"},{value: "Subscription Fee", view: "Subscription Fee"},{value:"Virtual Terminal",view:"Virtual Terminal"},{value:"Void ACH",view:"Void ACH"},{value:"Void_Check",view:"Void Check"},{value:"void_push_to_card",view:"Void P2C"}]
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    payment_gateways = PaymentGateway.eager_load(:descriptor).active_gateways
    payment_gateways = payment_gateways.map {|pg|
      view = pg.descriptor.present? ? "(#{pg.descriptor.name}) - #{pg.name}" : pg.name
      {:value=> pg.id, :view=> view}
    }
    if payment_gateways.present?
      @gateways = [{value:TypesEnumLib::GatewayType::Checkbook,view:"Checkbook.io"},{value:TypesEnumLib::GatewayType::Quickcard,view:"Quickcard"},{value:TypesEnumLib::GatewayType::Tango,view:"Tango"}]
      @risk_evaluation_status= [{value: "reject_manual_review", view:"Rejected by manual review"}, {value: "pending_manual_review", view: "Pending Manual Review"}, {value: "expired_manual_review", view: "Manual Review Expired"}, {value: "accept_manual_review", view: "Accepted by Manual Review"}, {value: "accept_default", view: "Accepted by default"}]
      @gateways = @gateways.push(payment_gateways).flatten.uniq if payment_gateways.present?
    end
    if @user.present? && @user.merchant? && @wallet.primary?
      @processed_amount = Transaction.all_sale_b2b(@wallet.id).sum(:total_amount)
      @hold_in_rear_amount= HoldInRear.calculate_pending(@wallet.id)
      @withdraw_amount=@wallet_balance.to_f - @hold_in_rear_amount.to_f
      if @location.retail? || @location.ecommerce_service? || @location.processing_type.blank?
        @today_release=HoldInRear.calculate_released_on_date_custom(@wallet.location_id, Time.zone.now)
        @tomorrow_release=HoldInRear.calculate_released_on_date_custom(@wallet.location_id, Time.zone.now + 1.day)
      end
    end
    if params[:tab].present?
      if params[:tab] == 'menu_1'
        # @transactions_count = Transaction.where(to: "#{@wallet.id}").or(Transaction.where(from: "#{@wallet.id}")).count
        # @transactions_count = SequenceLib.all_count_transactions(@wallet.id).count
      elsif params[:tab] == 'menu_2'
        conditions = []
        if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
          conditions = transactions_search_query(params[:query],params[:trans],params[:offset])
        end
        if @user.present? && @user.merchant? && @wallet.primary?
          if params[:query].present?
            @transactions = Transaction.where(conditions).wallet_transactions(@wallet.id).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
          else
            @transactions = Transaction.all_approved(page_size, @wallet.id).per_page_kaminari(params[:page]).per(page_size)
          end
        else
          if params[:query].present?
            @transactions = Transaction.where(conditions).wallet_transactions(@wallet.id).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
          else
            @transactions = BlockTransaction.all_transactions(page_size, @wallet.id).order(timestamp: :desc).per_page_kaminari(params[:page]).per(page_size)
          end
        end
      elsif params[:tab] == 'menu_3'
        @page_ticket  = params[:page_ticket].present? ? params[:page_ticket].to_i : 1
        @tickets = @owner.tickets.order(updated_at: :desc).per_page_kaminari(params[:page]).per(10) if @owner.present?
      elsif params[:tab] == 'menu_4'
        if @wallet.primary?
          @total_disputes=DisputeCase.wallet_disputes(@wallet.id,page_size).order(recieved_date: :desc).per_page_kaminari(params[:page]).per(page_size)
          @total_disputes_amount=DisputeCase.total_wallet_disputes(@wallet.id).pluck(:amount).try(:sum)
          # @totalSentPending = @total_disputes.sent_pending.count
          # @totalWonReversed = @total_disputes.won_reversed.count
          # @totalLost = @total_disputes.lost.count
        end
      elsif params[:tab] == 'menu_5'
        filter=params[:tx_filter_checks].present? ? params[:tx_filter_checks] : 10
        @checks = TangoOrder.where(order_type: [0,5] ,wallet_id: @wallet.id).order(id: :desc) if @wallet.present?
        if @checks.present?
          @unpaid_check=@checks.select{|v| v.status == "UNPAID"}.count
          captured=@checks.select{|v| v.status == "captured"}.count
          process=@checks.select{|v| v.status == "IN_PROCESS"}.count
          @inprocess_check=captured+process
          @void_check = @checks.where(status: ["VOID"]).count
          @pending_check = @checks.where(status: ["PENDING"]).count
          @total_pending_amount = @checks.where(status: ["PENDING"]).pluck(:actual_amount).try(:sum, &:to_f)
          @total_process_achs = @checks.where(status: ["PAID","UNPAID","IN_PROGRESS","IN_PROCESS"]).pluck(:actual_amount).try(:sum, &:to_f)
          @paid__check = @checks.where(status: ["PAID"]).count
          @paid_by_wire_check = @checks.where(status: ["PAID_WIRE"]).count
          @failed_amount = @checks.where(status: ["FAILED"]).pluck(:actual_amount).try(:sum, &:to_f)
          @void_amount = @checks.where(status: ["VOID"]).pluck(:actual_amount).try(:sum, &:to_f)
          @failed_check = @checks.where(status: ["FAILED"]).count
          @total_withdraw = @checks.pluck(:actual_amount).try(:sum, &:to_f)
          @dollar_fee = @checks.pluck(:check_fee).try(:sum, &:to_f)
          @percent_fee = @checks.pluck(:fee_perc).try(:sum, &:to_f)
          @total_fee = @percent_fee.to_f + @dollar_fee.to_f
          @total_checks=@checks.count

        end
        @checks = @checks.per_page_kaminari(params[:page]).per(filter)
      elsif params[:tab] == 'menu_6'

        filter=params[:tx_filter_ach].present? ? params[:tx_filter_ach] : 10
        @ach = TangoOrder.where(order_type: [5,6] ,wallet_id: @wallet.id).order(id: :desc) if @wallet.present?
        if @ach.present?
          @void_check = @ach.where(status: ["VOID"]).count
          @pending_check = @ach.where(status: ["PENDING"]).count
          @total_pending_amount = @ach.where(status: ["PENDING"]).pluck(:actual_amount).try(:sum, &:to_f)
          @total_process_achs = @ach.where(status: ["PAID","UNPAID","IN_PROGRESS","IN_PROCESS"]).pluck(:actual_amount).try(:sum, &:to_f)
          @failed_amount = @ach.where(status: ["FAILED"]).pluck(:actual_amount).try(:sum, &:to_f)
          @void_amount = @ach.where(status: ["VOID"]).pluck(:actual_amount).try(:sum, &:to_f)
          @paid_check = @ach.where(status: ["PAID"]).count
          @paid_by_wire_check = @ach.where(status: ["PAID_WIRE"]).count
          @failed_check = @ach.where(status: ["FAILED"]).count
          @total_withdraw = @ach.pluck(:actual_amount).try(:sum, &:to_f)
          @dollar_fee = @ach.pluck(:check_fee).try(:sum, &:to_f)
          @percent_fee = @ach.pluck(:fee_perc).try(:sum, &:to_f)
          @total_fee = @percent_fee.to_f + @dollar_fee.to_f
          @total_checks=@ach.count
          @unpaid_check=@ach.select{|v| v.status == "UNPAID"}.count
          captured=@ach.select{|v| v.status == "captured"}.count
          process=@ach.select{|v| v.status == "IN_PROCESS"}.count
          @inprocess_ach=captured+process
        end
        @ach = @ach.per_page_kaminari(params[:page]).per(filter)
      elsif params[:tab] == 'menu_7'
        filter=params[:tx_filter_p2c].present? ? params[:tx_filter_p2c] : 10
        @p2c = TangoOrder.where(order_type: [7] ,wallet_id: @wallet.id).order(id: :desc) if @wallet.present?
        if @p2c.present?
          @void_check = @p2c.where(status: ["VOID"]).count
          @pending_check = @p2c.where(status: ["PENDING"]).count
          @total_pending_amount = @p2c.where(status: ["PENDING"]).pluck(:actual_amount).try(:sum, &:to_f)
          @total_process_achs = @p2c.where(status: ["PAID","UNPAID","IN_PROGRESS","IN_PROCESS"]).pluck(:actual_amount).try(:sum, &:to_f)
          @paid_check = @p2c.where(status: ["PAID"]).count
          @paid_by_wire_check = @p2c.where(status: ["PAID_WIRE"]).count
          @failed_amount = @p2c.where(status: ["FAILED"]).pluck(:actual_amount).try(:sum, &:to_f)
          @void_amount = @p2c.where(status: ["VOID"]).pluck(:actual_amount).try(:sum, &:to_f)
          @failed_check = @p2c.where(status: ["FAILED"]).count
          @total_withdraw = @p2c.pluck(:actual_amount).try(:sum, &:to_f)
          @dollar_fee = @p2c.pluck(:check_fee).try(:sum, &:to_f)
          @percent_fee = @p2c.pluck(:fee_perc).try(:sum, &:to_f)
          @total_fee = @percent_fee.to_f + @dollar_fee.to_f
          @total_checks=@p2c.count
        end
        @p2c = @p2c.per_page_kaminari(params[:page]).per(filter)
      elsif params[:tab] == 'menu_8'
        @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
        @risk_evaluation_status= [{value: "reject_manual_review", view:"Rejected by manual review"}, {value: "pending_manual_review", view: "Pending Manual Review"}, {value: "expired_manual_review", view: "Manual Review Expired"}, {value: "accept_manual_review", view: "Accepted by Manual Review"}, {value: "accept_default", view: "Accepted by default"}]

        page_size = params[:tx_filter_shippment].present? ? params[:tx_filter_shippment].to_i : 10
        conditions = []
        if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
          conditions = shipment_search_query(params[:query],params[:offset])
        end
        if params["query"].present? && params["query"]["risk_eval"].present?
          @txn = Transaction.joins(:tracker).joins(:minfraud_result).where(receiver_wallet_id: @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
        else
          @txn = Transaction.joins(:tracker).includes(:minfraud_result).where(receiver_wallet_id: @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
        end
        # @txn = @txn.distinct
        @totalTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).group("trackers.status").count
        @totalTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).group("trackers.status").sum(:total_amount)
        @totalNewTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where(trackers: {tracker_id: nil}).count
        @totalNewTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where(trackers: {tracker_id: nil}).sum(:total_amount)
        @totalOverDueTxnCount = @totalTxnCount["overdue"]
        @totalOverDueTxnAmount = @totalTxnAmount["overdue"]
        # @totalOverDueTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where("trackers.status < ?", Time.now).count
        # @totalOverDueTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where("trackers.est_delivery_date < ?", Time.now).sum(:total_amount)
      end
    else
      # @transactions_count = Transaction.where(to: "#{@wallet.id}").or(Transaction.where(from: "#{@wallet.id}")).count
      # @transactions_count = SequenceLib.all_count_transactions(@wallet.id).count
    end
    @iamlocation = "location"
    render template: 'admins/transactions'
  end

  def check_location_name
    @merchant = User.find(params[:user])
    @location = Location.where(merchant_id: @merchant.id,business_name: (params[:location].try(:[],"business_name")).strip)
    if params[:location_id].present? && @location.count == 1 && @location.first.id == params[:location_id].to_i && params[:duplicate] != "true"
      @location = []
    end
    if @location.present?
      render json: false
    else
      render json: true
    end
  end

  def create
    if params[:location][:merchant_id].present?
      @merchant = User.find params[:location][:merchant_id]
      @location = Location.new(new_location_params)
      @location.fees.first.buy_rate!
      @location.fees.first.high!
      @location.ach_international = (params["ach_international"].present? && params["ach_international"] == "true") ? true : false
      @location.ledger = @merchant.ledger if @merchant.ledger.present?
      @baked_users = []

      @location.web_site = params[:location][:web_site].to_json
      @location.location_secure_token = SecureRandom.urlsafe_base64(10)
      #these three line below (if user set the value = 0.00 then these will set them nil )
      params[:location][:sale_limit] = params[:location][:sale_limit].present? ? params[:location][:sale_limit].to_f > 0 ? params[:location][:sale_limit] : nil : nil
      params[:location][:monthly_processing_volume] = params[:location][:monthly_processing_volume].present? ? params[:location][:monthly_processing_volume].to_f > 0 ? params[:location][:monthly_processing_volume] : nil : nil
      params[:location][:average_ticket] = params[:location][:average_ticket].present? ? params[:location][:average_ticket].to_f > 0 ? params[:location][:average_ticket] : nil : nil


      if (params[:location][:on_hold_pending_delivery] == "1") && (params[:location][:hold_in_rear] == "1")
        if params[:location][:processing_type] == Location.processing_types[:retail].downcase
          params[:location][:on_hold_pending_delivery] = false
        else
          params[:location][:hold_in_rear] = false
        end
      end
      if @location.save
        # @location.bank_account= AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @location.bank_account) if @location.bank_account.present? && @location.bank_account.length < 25
        # @location.save
        TransactionBatch.create(merchant_wallet_id: @location.wallets.primary.first.id, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: "sale")
        if params[:location][:bank].present?
                  @bank = @location.bank_details.new(
                      bank_account_type: params[:location][:bank][:bank_account_type],
                      routing_no: params[:location][:bank][:routing_no],
                      bank_name: params[:location][:bank][:bank_name],
                      account_no: params[:location][:bank][:account_no])
                  @bank.account_no = AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @bank.account_no) if @bank.account_no.present?
                  @bank.save
                  if params[:location][:bank]['image'].present?
                    image = {add_image: params[:location][:bank][:image], bank_detail_id: @bank.id}
                    upload_docs(image, @bank.id)
                  end
         end
        ## CALL METHOD FOR BAKED_USER_CREATE :: HERE
        if params[:location][:profit_split] == "1" || params[:location][:baked_iso] == "1" || params[:location][:net_profit_split] == "1" || params[:location][:sub_merchant_split] == "1" || params[:location][:dispensary_credit_split] == "1" || params[:location][:dispensary_debit_split] == "1"
          create_baked_users(@location)
        end

        creating_slots(@location,params)
        users = params[:user][:ids].reject(&:empty?)
        unless users.empty?
          users.each do |user|
            user = User.find(user)
            wallet = user.wallets.first
            @location.users << user
            if @location.ledger.present? && @location.ledger != ENV["LEDGER_NAME"]
              SequenceLib.create(wallet.id, wallet.wallet_type, user.name, user.phone_number, user.email, @location.ledger)
            end
            if @location.iso.present? && user.iso? && !@merchant.is_block && @merchant.company_id!=2
              # UserMailer.new_location_email(@location.iso.id, @location.id, nil).deliver_later
            end
          end
          text = I18n.t('notifications.new_loc_notification', merchant_name: @merchant.name, dba_name: @location.business_name)
          Notification.notify_user(@merchant,@merchant,@location,"new location notify",nil,text)
          UserMailer.new_location_email(@merchant.id, @location.id, ENV["SUPPORT_EMAIL"]).deliver_later if @merchant.oauth_apps.first.is_block == false && @merchant.is_block == false && @merchant.company_id!=2
          UserMailer.integration(@merchant.id).deliver_later if @merchant.oauth_apps.first.is_block == false && @merchant.tos_checking == true && @merchant.is_block == false
        end
        flash[:success] = "Location created successfully!"
      else
        flash[:error] = @location.errors.full_messages.first
      end
      redirect_to "/admins/locations?user=#{params[:location][:merchant_id]}"
    else
      flash[:error] = "You can not create without merchant"
      redirect_back(fallback_location: root_path)
    end
  end

  def duplication
     begin
      @old_location = Location.friendly.find params[:location_id]
      @merchant = User.find params[:location][:merchant_id]
      if @old_location.present?
        @old_location.ach_international = (params["ach_international"].present? && params["ach_international"] == "true") ? true : false
        Location.transaction do
          @new_location = @old_location.dup
          @new_location.save
          @new_location.update_token
          Fee.transaction do
            old_fee = @old_location.fees.first
            @new_fee = old_fee.dup
            @new_fee.save
          end
          if @new_location.present? && @new_fee.present?
            @new_location.fees << @new_fee
            @new_location.users << @old_location.users
          end
          if params[:location][:documentation].present?
            documentation_data = params[:location][:documentation] if params[:location][:documentation].present?
            @documentation = save_documentation(documentation_data ,true)
          end
        end
        if params[:location].present? && params[:location][:fee].present? && params[:location][:fee][:id].present?
          params[:location][:fee][:id] = @new_fee.id
        end
        from_duplicate = true
        updation(@new_location, params, from_duplicate)
        @location = Location.find(@new_location.id)
        if params[:location][:bank].present? && params[:location][:bank]['image'].present?
          @bank = BankDetail.new
          @bank.bank_account_type = params[:location][:bank][:bank_account_type]
          @bank.bank_name = params[:location][:bank][:bank_name]
          @bank.routing_no = params[:location][:bank][:routing_no]
          @bank.location_id = @location.id
          @bank.account_no = AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", params[:location][:bank][:account_no])
          @bank.save
          if params[:location][:bank]['image'].present?
            image = {add_image: params[:location][:bank][:image], bank_detail_id: @bank.id}
            upload_docs(image, @bank.id)
          end
        end
        ###  CALL METHOD FOR BAKED_USER_CREATE :: HERE
        if params[:location][:profit_split] == "1" || params[:location][:baked_iso] == "1" || params[:location][:net_profit_split] == "1" || params[:location][:sub_merchant_split] == "1"
          create_baked_users(@new_location)
        end
        flash[:notice] = "Duplicated successfully!"
      else
        flash[:notice] = "Location not found!"
      end
    rescue => ex
      flash[:notice] = "Something went wrong. Please try again later!"
    ensure
      if @new_location.present?
        TransactionBatch.create(merchant_wallet_id: @new_location.wallets.primary.first.id, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: "sale")
        users = @new_location.users
        users.each do |user|
          if @new_location.iso.present? && user.iso? && !@merchant.is_block && @merchant.company_id!=2
             # UserMailer.new_location_email(@new_location.iso.id, @new_location.id, nil).deliver_later
          end
        end
        text = I18n.t('notifications.new_loc_notification', merchant_name: @merchant.name, dba_name: @new_location.business_name)
        Notification.notify_user(@merchant,@merchant,@new_location,"new location notify",nil,text)
        UserMailer.new_location_email(@merchant.id, @new_location.id, ENV["SUPPORT_EMAIL"]).deliver_later if @merchant.oauth_apps.first.is_block == false && @merchant.is_block == false && @merchant.company_id!=2
        UserMailer.integration(@merchant.id).deliver_later if @merchant.oauth_apps.first.is_block == false && @merchant.tos_checking == true && @merchant.is_block == false
        return redirect_to merchant_id_locations_path(user_id: @new_location.try(:merchant).try(:slug))
      else
        return redirect_back(fallback_location: root_path)
      end
    end
  end

  def next_baked_section
    @count = params[:count].to_i
    respond_to :js
  end

  def next_ez_merchant_section
    @ez_count = params[:ez_count].to_i
    respond_to :js
  end

  def next_profit_section
    @profit_count = params[:count].to_i
    respond_to :js
  end

  def update
    @location = Location.friendly.find params[:id]
    @location.ach_international = (params["ach_international"].present? && params["ach_international"] == "true") ? true : false
    #these three line below (if user set the value = 0.00 then these will set them nil )
    params[:location][:sale_limit] = params[:location][:sale_limit].present? ? params[:location][:sale_limit].to_f > 0 ? params[:location][:sale_limit] : nil : nil
    params[:location][:monthly_processing_volume] = params[:location][:monthly_processing_volume].present? ? params[:location][:monthly_processing_volume].to_f > 0 ? params[:location][:monthly_processing_volume] : nil : nil
    params[:location][:average_ticket] = params[:location][:average_ticket].present? ? params[:location][:average_ticket].to_f > 0 ? params[:location][:average_ticket] : nil : nil
    # save_image if params[:image]['add_image'].present?
      params[:location][:phone_number] = params[:location][:phone_code] + params[:location][:phone_number] if params[:location][:phone_number].present? && params[:location][:phone_code].present?
      params[:location][:cs_number] = params[:location][:cs_phone_code] + params[:location][:cs_number] if params[:location][:cs_phone_code].present? && params[:location][:cs_number].present?
    if params[:location][:documentation].present?
      documentation_data = params[:location][:documentation] if params[:location][:documentation].present?
      @documentation = save_documentation(documentation_data)
    end
    update_documentation()
    if @location
      result = updation(@location, params)
      save_activity(@location.try(:merchant_id),Time.now,current_user.id,'location',@location)
      if result
        flash[:success] = "Successfully updated the profile Information"
      else
        flash[:error] = "Something went wrong! Location update Failed!"
      end
      redirect_back(fallback_location: locations_path)
    else
      flash[:error] = "Something went wrong! Location update Failed!"
      redirect_to locations_path
    end
  end

  def update_documentation
    if @location.present?
      location_id = @location.id
      @documents = Documentation.where(imageable_id: location_id)
      @documents.each do |doc|
        if doc.is_deleted === true
          doc.document.destroy
          doc.destroy
        end
      end
    end
  end

  def updation(location, params, from_duplicate=nil)
    begin
      creating_slots(location,params)
      create_baked_users(location)
      location.web_site = params[:location][:web_site].to_json
    if params[:location][:ecomm_platform_id].present?
      location.ecomm_platform_id = params[:location][:ecomm_platform_id]
    end
    if (params[:location][:on_hold_pending_delivery] == "1") && (params[:location][:hold_in_rear] == "1")
      if params[:location][:processing_type] == Location.processing_types[:retail].downcase
        params[:location][:on_hold_pending_delivery] = false
      else
        params[:location][:hold_in_rear] = false
      end
    end
      if params[:location][:bank].present? && params[:location][:bank][:bank_account_type].present?
        @bank = @location.bank_details.new(
            bank_account_type: params[:location][:bank][:bank_account_type],
            routing_no: params[:location][:bank][:routing_no],
            bank_name: params[:location][:bank][:bank_name],
            account_no: params[:location][:bank][:account_no])
        @bank.account_no = AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @bank.account_no) if @bank.account_no.present?
        @bank.save
        merchant = @location.merchant
        merchant.update(merchant_ach_status:"complete", send_ach_bank_request: true) if merchant.try(:merchant_ach_status) == "initial"
        if params[:location][:bank]['image'].present?
          image = {add_image: params[:location][:bank][:image], bank_detail_id: @bank.id}
          upload_docs(image, @bank.id)
        end
      end
      if from_duplicate
        params[:location][:phone_number] = params[:location][:phone_code] + params[:location][:phone_number] if params[:location][:phone_number].present? && params[:location][:phone_code].present?
        params[:location][:cs_number] = params[:location][:cs_phone_code] + params[:location][:cs_number] if params[:location][:cs_phone_code].present? && params[:location][:cs_number].present?
      end
      if params[:location][:virtual_transaction_api] == "0" && params[:location][:virtual_terminal_transaction_api] == "0" && params[:location][:virtual_debit_api] == "0"
        params[:location][:block_api] = "0"
      end
      if params[:location][:sales] == "0" && params[:location][:virtual_terminal] == "0" && params[:location][:close_batch] == "0"
        params[:location][:disable_sales_transaction] = "0"
      end
      location.update(edit_location_params)
      wallet = location.wallets.primary.first
      wallet.update(name: params[:location][:business_name])
      reserve_wallet = location.wallets.reserve.first
      if reserve_wallet.present?
        reserve_wallet.update(name: params[:location][:business_name]+" reserve")
      end
      users = params[:user][:ids].reject{|v|v.empty?}
      user_ids = [location.try(:isos).try(:first).try(:id),location.try(:agents).try(:first).try(:id),location.try(:affiliates).try(:first).try(:id)] if location.try(:users).present?
      LocationsUsers.where(location_id: location.id, relation_type: "primary").delete_all
      # location.users.each do|t|
      #   location.users.delete(t.id)
      # end
      unless users.empty?
        baked_users_batch=location.baked_users.where(location_id:location.id,split_type:'locations_users')
        baked_users_ids=baked_users_batch.pluck(:user_id)
        old_users_ids=baked_users_ids -  params[:user][:ids].reject(&:blank?).map(&:to_i)
        baked_users_batch.where(user_id:old_users_ids.try(:flatten),split_type:'locations_users').update_all(active:false) if old_users_ids.present?
        params[:user][:ids].reject(&:blank?).each do |user|
          u = User.find(user.to_i)
          location.users <<  u
          baked_users=location.baked_users.where(location_id:location.id,user_id:u.id,split_type:'locations_users').try(:last)
          if baked_users.present?
            baked_users.update(active:true) unless baked_users.active?
          else
            Location.create_baked_users(location.id,u.try(:wallets).try(:primary).try(:first).try(:id), u.id,"locations_users",u.try(:name), '','',u.role,false, nil,  nil,"0")
          end
        end
      end
      if user_ids.present?
        # new_users=[users.try(:first).to_i,users.try(:second).to_i,users.try(:third).to_i]
        new_ids=[location.try(:isos).try(:first).try(:id),location.try(:agents).try(:first).try(:id),location.try(:affiliates).try(:first).try(:id)] if location.try(:users).present?
        unless user_ids.try(:first)==new_ids.try(:first) && user_ids.try(:second)==new_ids.try(:second) && user_ids.try(:third)==new_ids.try(:third)
          save_activity(location.try(:[],'merchant_id'),Time.now,current_user.id,'location_users',[new_ids,user_ids])
        end
      end
      unless params[:location][:fee].nil?
        k = params[:location][:fee]
        k['service_date'] = Date.strptime(k['service_date'], '%m/%d/%Y') if k['service_date'].present?
        k['statement_date'] = Date.strptime(k['statement_date'], '%m/%d/%Y') if k['statement_date'].present?
        k['misc_date'] = Date.strptime(k['misc_date'], '%m/%d/%Y') if k['misc_date'].present?
        if k["id"].present?
          fee = Fee.find(k["id"])
          if fee.present?
            fee.update(edit_fees_params(k))
          end
        else
          fee_params=params[:location][:fee].reject! {|k,v| k=='id'}
          fee_params[:fee_status]="buy_rate"
          fee_params[:risk]="high"
          location_fee_new = Fee.create(new_edit_fee)
          location.fees << location_fee_new
        end
      end
      return true
    rescue => e
      return false
    end
  end

  def creating_slots(location,params)
    if params[:location][:is_slot].present? && params[:location][:is_slot] == "1"
      slot_over_detail = {}
      slot_under_detail = {}
      slot_amount = 0
      if params[:query].present? && params[:query][:over].present? && params[:query][:over].values.present?
        params[:query][:over].values.each do |over|
          slot_over_detail.merge!(over)
        end
      end
      if params[:query].present? && params[:query][:under].present? && params[:query][:under].values.present?
        params[:query][:under].values.each do |under|
          slot_under_detail.merge!(under)
        end
      end
      slot_amount = params[:query].try(:[],"amount")
      if slot_over_detail.blank? || slot_under_detail.blank?
      else
        if location.slot.present?
          location.slot.update(amount: slot_amount, slot_under: slot_under_detail, slot_over: slot_over_detail)
        else
          Slot.create(location_id: location.id,amount: slot_amount, slot_under: slot_under_detail, slot_over: slot_over_detail)
        end
      end
    end
  end

  def in_valid_location
      location = Location.find params[:location_id]
      if params[:is_block] == "true"
        location.update(is_block:  true)
      elsif params[:is_block] == "false"
        location.update(is_block:  false)
      else

      end
      respond_to :js
  end
  def new_bank
    @bank = BankDetail.new
    @location = Location.find(params[:location_id])
  end

  def create_bank
    @location = Location.find(params[:location_id])
    @bank = @location.bank_details.new(add_new_bank_params)
    @bank.account_no = AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @bank.account_no)
    @bank.save
    if params[:bank_detail]['image'].present?
      image = {add_image: params[:bank_detail][:image], bank_detail_id: @bank.id}
      upload_docs(image, @bank.id)
    end
    flash[:success] = "Successfully Created Bank"
    redirect_to edit_location_path(:id => @location.id, :open_wallet_tab => "true")
  end

  def upload_docs(image, id)
    bank = BankDetail.find(id)
    if bank.image.present?
      @old_img = Image.where(bank_detail_id: id).last
      @old_img.delete
    end
    @img = Image.new(image)
    if @img.validate
      @img.save
    else
      @error = @img.errors.full_messages.first
    end
  end

  def edit_bank
    @location = Location.find(params[:location_id])
    @bank = @location.bank_details.find(params[:format])
    @bank.account_no = @bank.account_no.present? ? AESCrypt.decrypt("#{params[:location_id]}-#{ENV['ACCOUNT_ENCRYPTOR']}", @bank.account_no) : ""
  end

  def update_bank
    @open_wallet_tab = "true"
    @location = Location.find(params[:location_id])
    @bank = @location.bank_details.find(params[:format])
    params["bank_detail"]["account_no"] = AESCrypt.encrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", params["bank_detail"]["account_no"])
    if @bank.update(add_new_bank_params)
      if params[:bank_detail]['image'].present?
        image = {add_image: params[:bank_detail][:image], bank_detail_id: @bank.id}
        upload_docs(image, @bank.id)
      end
      merchant = @location.merchant
      merchant.update(merchant_ach_status:"complete", send_ach_bank_request: true) if merchant.try(:merchant_ach_status) == "pending" || merchant.try(:merchant_ach_status) == "initial"
      flash[:success] = "Successfully updated the Bank Information"
      redirect_to edit_location_path(:id => @location.id, :open_wallet_tab => "true") unless params[:ach_approve].present?
      redirect_back(fallback_location: root_path) if params[:ach_approve].present?
    else
      flash[:error] = "Can't updated the Bank Information"
      redirect_to edit_location_path(:id => @location.id, :open_wallet_tab => "true") unless params[:ach_approve].present?
      redirect_back(fallback_location: root_path) if params[:ach_approve].present?
    end
  end
  def delete_bank
    @location = Location.find(params[:location_id])
    @bank = @location.bank_details.find(params[:format])
    if @location.bank_details.count == 1
      merchant = @location.merchant
      merchant.update(merchant_ach_status:"initial", send_ach_bank_request: false)
    end
    @bank.destroy

    flash[:success] = "Successfully Deleted"
    redirect_to edit_location_path(:id => @location.id)
  end
  def document_delete
    @image = Documentation.find params[:image_id]
    @image.is_deleted = true
    @image.save
  end
  def d_account_no
    if current_user.valid_password?(params[:password])
      @bank = BankDetail.find(params[:bank_id])
      d_account = AESCrypt.decrypt("#{params[:location_id]}-#{ENV['ACCOUNT_ENCRYPTOR']}", @bank.account_no) if @bank.account_no.present?
      render status: 200, json: {success: 'Verified', acc_number: d_account}
    else
      render status: 404, json: {error: 'incorrect password'}
    end
  end

  private

  def create_baked_users(location)
    @baked_users = []
    profit_splits = {}

    profit_splits[:share_split] = params[:profit_splits][:share_split]
    profit_splits[:splits] = params[:profit_splits][:splits]
    if profit_splits.present?
      location.profit_split_detail = profit_splits.to_json
    end
    location.baked_users.where.not(split_type:'locations_users').update_all(active: false) if location.baked_users.present?
    if params[:location][:profit_split] == "1" || params[:location][:baked_iso] == "1" || params[:location][:net_profit_split] == "1" || params[:location][:sub_merchant_split] == "1" || params[:location][:dispensary_credit_split] == "1" || params[:location][:dispensary_debit_split] == "1"
      profit_shares = JSON.parse(profit_splits.to_json) if profit_splits.present?
      if profit_shares.present? && profit_shares["splits"]["dispensary_credit_split"].present? && params[:location][:dispensary_credit_split] == "1"
        dispensary_credit = profit_shares["splits"]["dispensary_credit_split"]
        dispensary_credit.each do |k,v|
          if v["name"].present? && v["wallet"].present?
            if k == "user_1"
              user1 = User.find(v["wallet"])
              Location.create_baked_users(location.id,user1.wallets.first.id, user1.id,"dispensary_credit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user1.role,false, nil,  nil,"0")
            elsif k == "user_2" && v["wallet"].present?
              user2 = User.find(v["wallet"])
              Location.create_baked_users(location.id,user2.wallets.first.id, user2.id,"dispensary_credit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user2.role,false, nil,  nil,"1")
            elsif k == "user_3" && v["wallet"].present?
              user3 = User.find(v["wallet"])
              Location.create_baked_users(location.id,user3.wallets.first.id, user3.id,"dispensary_credit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user3.role,false, nil,  nil,"2")
            end
          end
        end
      end

      if profit_shares.present? && profit_shares["splits"]["dispensary_debit_split"].present? && params[:location][:dispensary_debit_split] == "1"
        dispensary_credit = profit_shares["splits"]["dispensary_debit_split"]
        dispensary_credit.each do |k,v|
          if v["name"].present? && v["wallet"].present?
            if k == "user_1"
              user1 = User.find(v["wallet"])
              Location.create_baked_users(location.id,user1.wallets.first.id, user1.id,"dispensary_debit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user1.role,false, nil,  nil,"0")
            elsif k == "user_2" && v["wallet"].present?
              user2 = User.find(v["wallet"])
              Location.create_baked_users(location.id,user2.wallets.first.id, user2.id,"dispensary_debit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user2.role,false, nil,  nil,"1")
            elsif k == "user_3" && v["wallet"].present?
              user3 = User.find(v["wallet"])
              Location.create_baked_users(location.id,user3.wallets.first.id, user3.id,"dispensary_debit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user3.role,false, nil,  nil,"2")
            end
          end
        end
      end

      ## For SubMerchantSplit /EZ
      if profit_shares.present? && profit_shares["splits"]["ez_merchant"].present?
        splits_ez = profit_shares["splits"]["ez_merchant"]
        splits_ez.each do |key, value|
          if value.values.last["name"].present? && value.values.last["wallet"].present?
            locat = Location.find_by(id: value["ez_merchant_1"]["wallet"])   ## Why User? We've LocationID here
            user = locat.merchant
            if user.present?
              if value["ez_merchant_1"]["attach"].present? && value["ez_merchant_1"]["attach"] == "true"
                LocationsUsers.create(user_id: user.id, location_id: location.id, attach: true).secondary!
              else
                LocationsUsers.create(user_id: user.id, location_id: location.id).secondary!
              end
              Location.create_baked_users(location.id,locat.wallets.primary.first.id, user.id,"sub_merchant_split",value["ez_merchant_1"]["name"],value["ez_merchant_1"]["dollar"].to_f,value["ez_merchant_1"]["percent"].to_f,user.role,value["ez_merchant_1"]["attach"] == "true" ? true : false, false, false,nil)
            end
          end
        end
      end

      # @super_company
      #### put Profit_Split_GBox here
      if profit_shares.present? && profit_shares["splits"].try(:[],"profit_split").present?
        profit_split = profit_shares["splits"]["profit_split"]
        gbox = profit_shares["splits"]["profit_split"]["from"] == "gbox" ? true : false
        iso = profit_shares["splits"]["profit_split"]["from"] == "iso" ? true : false
        count = 0
        profit_split.except("from").each do |key, value|
          if value["name"].present? && value["wallet"].present?
            if value["role"].downcase == "merchants"
              loc = Location.find(value["wallet"])
              loc_wallet = loc.wallets.first.id
            else
              user = User.find(value["wallet"])
              usr_wallet = user.wallets.first.id
            end

            Location.create_baked_users(location.id,loc_wallet || usr_wallet, loc.present? ? loc.merchant_id : user.id,"gbox_profit_split",value["name"],value["dollar"].to_f,value["percent"].to_f,user.present? ? user.role : "merchant",false, gbox,  iso,"#{count}0")
            count = count + 1
          end
        end
      end

      if profit_shares.present?
        if profit_shares["splits"].present?
          profit_shares["splits"].except("ez_merchant").each do |key,value| # Getting all users in @super_users & split_profit's users in @super_company
            key_name = key.tr("0-9", "")
            unless key_name.blank?
              # @net_company
              #### put Net_Profit_Split here
              if key_name == "net" && location.net_profit_split
                if value.values.last["name"].present? && value.values.last["wallet"].present?
                  location_id = value.values.first["wallet"].to_i if value.keys.first.include?("merchant")
                  user_id = value.values.first["wallet"].to_i if !value.keys.first.include?("merchant")
                  if location_id.present?
                    loc = Location.find(location_id)
                    loc_wallet = loc.wallets.first.id
                  elsif user_id.present?
                    user = User.find(user_id)
                    usr_wallet = user.wallets.first.id
                  end

                  gbox = value.values.first["from"] == "gbox" ? true : false
                  iso = value.values.first["from"] == "iso" ? true : false

                  Location.create_baked_users(location.id,loc_wallet || usr_wallet, loc.present? ? loc.merchant_id : user.id,"net_profit_split",value.values.first["name"],0,value.values.first["percent"].to_f,user.present? ? user.role : "merchant",false, gbox,  iso,nil)
                end
              end
            else
              if value.values.first["name"].present? && value.values.first["wallet"].present?
                @baked_users.push(value)
              end
            end
          end

          baked_gbox = profit_shares["share_split"] == "gbox" ? true : false
          baked_iso = profit_shares["share_split"] == "iso" ? true : false

          ## For Baked_Users
          unless @baked_users.blank?
            @baked_users.each.with_index do |users, count|
              users.each do |k,v|
                if v["name"].present? && v["wallet"].present?
                  if k == "user_1"
                    user1 = User.find(v["wallet"])
                    Location.create_baked_users(location.id,user1.wallets.first.id, user1.id,"baked_profit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user1.role,false, baked_gbox,  baked_iso,"#{count}0")
                  elsif k == "user_2"
                    user2 = User.find(v["wallet"])
                    Location.create_baked_users(location.id,user2.wallets.first.id, user2.id,"baked_profit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user2.role,false, baked_gbox,  baked_iso,"#{count}1")
                  elsif k == "user_3"
                    user3 = User.find(v["wallet"])
                    Location.create_baked_users(location.id,user3.wallets.first.id, user3.id,"baked_profit_split",v["name"], v["dollar"].to_f,v["percent"].to_f,user3.role,false, baked_gbox,  baked_iso,"#{count}2")
                  end
                end
              end
            end
          end
        end
      end
    end
    location.save
  end

  def new_location_params
    params[:location][:fees_attributes].each do |p,k|
      k[:fee_status] = k[:fee_status].to_i
      k[:service_date] = Date.strptime(k[:service_date], '%m/%d/%Y') if k[:service_date].present?
      k[:statement_date] = Date.strptime(k[:statement_date], '%m/%d/%Y') if k[:statement_date].present?
      k[:misc_date] = Date.strptime(k[:misc_date], '%m/%d/%Y') if k[:misc_date].present?
    end
    params[:location][:risk] = params[:location][:risk].to_i
    params[:location][:phone_number] = params[:location][:phone_code] + params[:location][:phone_number]
    params[:location][:cs_number] = params[:location][:cs_phone_code] + params[:location][:cs_number]
    params.require(:location).permit(:merchant_id, :phone_number, :tax_id, :social_security_no,:zip, :category_id, :ecomm_platform_id, :country, :state, :city,
                                     :years_in_business, :business_name, :first_name, :last_name, :hold_in_rear, :hold_in_rear_days,:processing_type,:on_hold_pending_delivery,:on_hold_pending_delivery_days,:cs_number,:net_profit_split,
                                     :monthly_processing_volume, :extra_over,:ach_limit,:push_to_card_limit,:check_limit,:ach_limit_type,:push_to_card_limit_type,
                                     :check_limit_type,:average_ticket,:email ,:business_type, :address,:sale_limit, :sale_limit_percentage, :ledger, :block_withdrawal,
                                     :block_ip,:block_giftcard,:sales,:virtual_terminal,:close_batch,:disable_sales_transaction,:profit_split,:profit_split_detail,
                                     :baked_iso, :primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id, :risk,:block_api,:virtual_transaction_api,
                                     :virtual_terminal_transaction_api,:virtual_debit_api,:bank_routing, :bank_account,:bank_account_type, :email_receipt,
                                     :sms_receipt,:block_ach,:sub_merchant_split, :standalone,:standalone_type,:bank_account_name, :push_to_card,:eaze_reporting, :rtp_hook,
                                     :is_slot,:ach_flag, :ach_flag_text, :dispensary_credit_split, :dispensary_debit_split, :vip_card, :apply_load_balancer, :load_balancer_id,:transaction_limit,transaction_limit_offset: [],
      fees_attributes: [:risk,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar,:b2b_fee_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :gbox, :partner, :iso, :agent, :fee_id, :fee_status, :location_id, :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement, :service_date, :misc_date, :statement_date, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee_dollar, :ach_fee,:charge_back_percent, :charge_back_count, :refund_percent, :refund_count, :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee,:qr_refund,:qr_refund_percentage, :debit_unredeemed_fee, :debit_unredeemed_perc_fee, :credit_unredeemed_fee, :credit_unredeemed_perc_fee, :load_fee_credit, :load_fee_percent_credit, :load_fee_debit, :load_fee_percent_debit,:generate_label_fee, :tracking_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee])
  end

  def edit_location_params
    params[:location][:risk] = params[:location][:risk].to_i
    params.require(:location).permit(:merchant_id,:phone_number,:tax_id,:social_security_no,:zip,:category_id,:ecomm_platform_id, :country, :state,:city,
                                     :years_in_business,:business_name, :first_name, :last_name,:profit_split, :profit_split_detail, :baked_iso,:block_api,
                                     :virtual_transaction_api,:virtual_terminal_transaction_api,:virtual_debit_api,:bank_account, :bank_routing,:bank_account_name,
                                     :bank_account_type,:cs_number,:net_profit_split,:sub_merchant_split,:average_ticket,:email ,:business_type, :address,:sale_limit,
                                     :sale_limit_percentage, :ledger, :block_withdrawal,:block_ip,:block_giftcard,:sales,:virtual_terminal,:close_batch,
                                     :disable_sales_transaction, :hold_in_rear, :hold_in_rear_days,:processing_type,:on_hold_pending_delivery,:on_hold_pending_delivery_days,:primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id,
                                     :risk, :email_receipt, :sms_receipt,:block_ach,:standalone,:push_to_card,:standalone_type,:ach_limit,:push_to_card_limit,
                                     :check_limit,:ach_limit_type,:push_to_card_limit_type,:check_limit_type,:eaze_reporting,:is_slot, :monthly_processing_volume, :rtp_hook,
                                     :extra_over, :ach_flag, :ach_flag_text, :dispensary_credit_split, :dispensary_debit_split, :vip_card, :apply_load_balancer, :load_balancer_id,:processing_type,:on_hold_pending_delivery,:on_hold_pending_delivery_days,:transaction_limit,transaction_limit_offset: [])
  end

  def edit_fees_params(k)
    k.permit(:id,:risk,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :b2b_fee_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :gbox, :partner, :iso, :agent, :fee_id, :fee_status, :location_id,
             :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement, :service_date, :misc_date, :statement_date, :ach_fee_dollar, :ach_fee, :dc_deposit_fee_dollar, :dc_deposit_fee,:charge_back_percent, :charge_back_count, :refund_percent, :refund_count, :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee,:generate_label_fee, :tracking_fee,:qr_refund,:qr_refund_percentage, :debit_unredeemed_fee, :debit_unredeemed_perc_fee, :credit_unredeemed_fee, :credit_unredeemed_perc_fee, :load_fee_credit, :load_fee_percent_credit, :load_fee_debit, :load_fee_percent_debit, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee)
  end

  def new_addres_params
    params[:user].require(:addresses).permit(:street,:city,:zip, :country, :state)
  end

  def edit_addres_params
    params[:user].require(:addresses).permit(:street,:city,:zip, :country, :state)
  end

  def new_edit_fee
    params[:location].require(:fee).permit(:risk,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :b2b_fee_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :gbox, :partner, :iso, :agent, :fee_id, :fee_status, :location_id,
                                           :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee, :service, :misc, :statement, :service_date, :misc_date, :statement_date, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar,:qr_refund,:qr_refund_percentage, :debit_unredeemed_fee, :debit_unredeemed_perc_fee, :credit_unredeemed_fee, :credit_unredeemed_perc_fee,:generate_label_fee, :tracking_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee)
  end

  def add_new_bank_params
    params.require(:bank_detail).permit(:bank_account_type, :bank_name, :routing_no, :account_no, :location_id, :bank_detail_image)
  end


end
