class Admins::WalletsController <  AdminsController
  skip_before_action :check_admin
  include Admins::BlockTransactionsHelper

  def show
    if params[:id].blank? || params[:id].nil?
      flash[:error] = 'Invalid Record Id!'
      redirect_to '/'
    end
    @wallet = Wallet.find(params[:id])
    @balance = SequenceLib.balance(params[:id])
    @retire_transactions = all_transactions_except(@wallet.id, @wallet.id, "retire")
    @transactions = all_transactions_of_wallet(@wallet.id)
  end

  def retire_wallet
      @wallet = Wallet.friendly.find(params[:wallet_id])
      balance = show_balance(@wallet.id) - HoldInRear.calculate_pending(@wallet.id)
      
      if params["all"].present? && params["all"] == "on"
        if @wallet.nil?
          flash[:error] = "Wallet does not found !"
        else
          if balance.to_f > 0
            # app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
            @wallet.update(notes: params[:notes]) if params[:notes].present?
            user_info = {notes:params[:notes].present? ? params[:notes] : ''}
            transaction = Transaction.create(
                to: nil,
                from: @wallet.id,
                status: "pending",
                amount: balance.to_f,
                sender: @wallet.users.first,
                sender_wallet_id: @wallet.id,
                sender_balance: balance,
                action: 'retire',
                net_amount: balance.to_f,
                total_amount: balance.to_f,
                ip: get_ip,
                main_type: "manual_retire",
                tags: user_info
            )
            trs = SequenceLib.retire(balance, @wallet.id, 'manual_retire', '0', '0', TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,user_info)
            if trs.present?
              transaction.update(
                             status: "approved",
                             seq_transaction_id: trs.actions.first.id,
                             timestamp: trs.timestamp,
                             tags: trs.actions.first.tags
              )
              #= creating block transaction
              parsed_transactions = parse_block_transactions(trs.actions, trs.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
            end
            flash[:info] = "You have retired a wallet manually "
          else
            flash[:error] = "Oops! Wallet is empty!"
          end
        end
      else
        if @wallet.nil?
          flash[:error] = "Wallet does not found !"
        else
          if balance.to_f > 0
            # app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
            @wallet.update(notes: params[:notes]) if params[:notes].present?
            user_info = {notes:params[:notes].present? ? params[:notes] : ''}
            transaction = Transaction.create(
                to: nil,
                from: @wallet.id,
                status: "pending",
                amount: params["amount"].to_f,
                sender: @wallet.users.first,
                sender_wallet_id: @wallet.id,
                sender_balance: balance,
                action: 'retire',
                net_amount: params["amount"].to_f,
                total_amount: params["amount"].to_f,
                ip: get_ip,
                main_type: "manual_retire",
                tags: user_info
            )
            retire = SequenceLib.retire(params["amount"].to_f, @wallet.id, 'manual_retire', '0', '0', TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,user_info)
            if retire.present?
              transaction.update(
                             status: "approved",
                             seq_transaction_id: retire.actions.first.id,
                             timestamp: retire.timestamp,
                             tags: retire.actions.first.tags
              )
              #= creating block transaction
              parsed_transactions = parse_block_transactions(retire.actions, retire.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
            end
            flash[:info] = "You have retired a wallet manualy "
          else
            flash[:error] = "Oops! Wallet is empty!"
          end
        end
      end
    redirect_back(fallback_location: root_path)
  end

end