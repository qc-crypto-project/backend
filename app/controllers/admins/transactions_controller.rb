class Admins::TransactionsController < AdminsController
  include Merchant::TransactionsHelper
  include HTTParty
  include Zipline
  skip_before_action :check_admin
  def ach
    respond_to do |format|
      format.html
      format.json { render json: TransactionsDatatable.new(view_context) }
    end
  end

  # ----------------------- Generate export file start -----------------------#

  def generate_export_file
    if params[:search_query].present?
      time = parse_time(params[:search_query])
      date = params[:search_query][:date].present? ? parse_daterange(params[:search_query][:date]) : nil
      date = params[:date].present? ? parse_daterange(params[:date]) : nil if params[:date].present? && date.blank?
      date = parse_daterange(params[:search_query][:datie]) if params[:search_query][:datie].present? && date.blank?
      date = parse_daterange(params[:search_query][:export_date]) if params[:search_query][:export_date].present? && date.blank?
      if date.present?
        if params[:type] == "dispute"
          timezone = TIMEZONE
          timezone = cookies[:timezone] if cookies[:timezone].present?
          params[:offset] = params[:offset].present? ? params[:offset] : Time.now.in_time_zone(timezone).strftime("%:z")
          date = {
              first_date: Date.new(date.first[:year].to_i, date.first[:month].to_i, date.first[:day].to_i),
              second_date: Date.new(date.second[:year].to_i, date.second[:month].to_i, date.second[:day].to_i)
          }
        else
          date = {
              first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
              second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
          }
        end

      elsif params[:search_query][:time1].present? || params[:search_query][:time2].present?  #= if date not selected we'll use today's date
        date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
        }
      end
    else
      params[:date] = "#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}" if params[:date].blank?
      params[:offset] = params[:offset].present? ? params[:offset] : Time.now.in_time_zone(timezone).strftime("%:z")
      date = parse_date_time_new(params[:date], params[:offset])
      # date = {
      #     first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
      #     second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      # }
    end
    report = getting_old_report_admin(date,params)

    if report[:find] and report[:status] != "pending" and report[:status] != "crashed" and params["total_count"].present?
      if report[:report_file_id].present? and report[:report_file_id]["id"].present?
        foo = QcFile.find(report[:report_file_id]["id"].last)
        if foo.present? and foo.image.present?
          data = open("https:"+foo.image.url)
          file_row_size = data.readlines.size - 1 
          if file_row_size != params["total_count"].to_i
            report = {}
          end
        end
      end
    end

    message = ""
    file_id = ""
    params[:search_query]["dispute_case"] = params["dispute_case"] if params[:search_query].present?
    search_query = params[:search_query].to_json if params[:search_query].present?
    unless report[:find]
      if date.present?
        job_id = ExportFileWorker.perform_async(current_user.id, date[:first_date], date[:second_date], params[:type], params[:offset], params[:wallet], params[:send_via], search_query, params[:date], nil, params[:trans], params[:admin_user_id], params[:specific_wallet_id], session[:session_id])
      else
        job_id = ExportFileWorker.perform_async(current_user.id, nil, nil, params[:type], params[:offset], params[:wallet], params[:send_via], search_query, params[:date], nil, params[:trans], params[:admin_user_id], params[:specific_wallet_id], session[:session_id])
      end
    else
      job_id = "abc"
      if report[:status] == "pending"
        message = TypesEnumLib::Message::Pending
      elsif report[:status] == "crashed"
        message = TypesEnumLib::Message::Crashed
        heading = TypesEnumLib::Message::CrashedHeading
      else
        file_id = report[:report_file_id].try(:[],"id")
      end
    end
    if report[:id].present?
      old_report = Report.find_by(id: report[:id])
      @host_name = session[:session_id]
      report_user_id = current_user.id
      old_report.update(read: false, created_at: DateTime.now)
      reports_count = Report.where(read: false, user_id: report_user_id).count
      ActionCable.server.broadcast "report_channel_#{@host_name}", channel_name: "report_channel_#{@host_name}", action: 'sidebarcount', message: {count: reports_count}
    end
    respond_to do |format|
      format.json do
        render json: {
            jid: job_id,
            status: report[:status],
            message: message,
            heading: heading,
            type: params[:type],
            file_id: file_id.to_json
        }
      end
    end
  end

  def getting_old_report_admin(date,params)
    return_hash = {find: false, report_file_id: nil, status: ""}
    if date.present?
      reports = Report.where(first_date: date[:first_date], second_date: date[:second_date], user_id: current_user.id).select(:id,:report_type,:status,:file_id,:transaction_type,:params)
    end
    if reports.present?
      report_statuses = reports.pluck(:status).uniq
      if params[:type] == TypesEnumLib::WorkerType::Admin
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type], transaction_type: params[:trans], status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        if report.blank?
          if params[:search_query].present?
            report = reports.where.not(params: nil).where("report_type = ? AND transaction_type = ? AND params->>'type' = ? AND params->>'sender' = ? AND params->>'receiver' = ? AND params->>'amount' = ? AND params->>'date' = ? AND params->>'last4' = ? AND params->>'first6' = ? AND params->>'gateway' = ? AND params->>'block_id' = ? AND params->>'city' = ? AND params->>'state' = ? AND params->>'time1' = ? AND params->>'time2' = ?", params[:type], params[:trans], ["#{params[:search_query][:type]}"], params[:search_query][:sender], params[:search_query][:receiver], params[:search_query][:amount], params[:search_query][:date], params[:search_query][:last4], params[:search_query][:first6], "#{params[:search_query][:gateway]}", params[:search_query][:block_id], params[:search_query][:city], params[:search_query][:state], params[:search_query][:time1], params[:search_query][:time2]).last
          else
            report = reports.where(report_type: params[:type], transaction_type: params[:trans]).last
          end
        end
      elsif params[:type] == TypesEnumLib::WorkerType::AdminMerchantProfile
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type], wallet: params[:specific_wallet_id], transaction_type: params[:trans], status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        if report.blank?
          if params[:search_query].present?
            report = reports.where.not(params: nil).where("report_type = ? AND transaction_type = ? AND wallet = ? AND params->>'type' = ? AND params->>'sender' = ? AND params->>'receiver' = ? AND params->>'amount' = ? AND params->>'date' = ? AND params->>'last4' = ? AND params->>'first6' = ? AND params->>'gateway' = ? AND params->>'block_id' = ? AND params->>'city' = ? AND params->>'state' = ? AND params->>'time1' = ? AND params->>'time2' = ?", params[:type], params[:trans], params[:specific_wallet_id], ["#{params[:search_query][:type]}"], params[:search_query][:sender], params[:search_query][:receiver], params[:search_query][:amount], params[:search_query][:date], params[:search_query][:last4], params[:search_query][:first6], "#{params[:search_query][:gateway]}", params[:search_query][:block_id], params[:search_query][:city], params[:search_query][:state], params[:search_query][:time1], params[:search_query][:time2]).last
          else
            report = reports.where(report_type: params[:type],wallet: params[:specific_wallet_id], transaction_type: params[:trans]).last
          end
        end
      elsif params[:type] == TypesEnumLib::WorkerType::AdminUsers || params[:type] == TypesEnumLib::WorkerType::AdminMerchant
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type],admin_user_id: params[:admin_user_id],status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        report = reports.where(report_type: params[:type],admin_user_id: params[:admin_user_id]).last if report.blank?
      elsif params[:type] == TypesEnumLib::WorkerType::Dispute
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type], wallet: params[:specific_wallet_id], transaction_type: params[:trans], status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        if report.blank?
          if params[:search_query].reject {|k, v| v.nil? || v == "" || v == {}}.present?
            report = reports.where.not(params: nil).where("report_type = ? AND params->>'case_number_cont' = ? AND params->>'dispute_type_eq' = ? AND params->>'amount_eq' = ? AND params->>'card_number_cont' = ? AND params->>'merchant_wallet_location_business_name_cont' = ? AND params->>'id_cont' = ? AND params->>'recieved_date' = ?", params[:type], params[:search_query].try(:[], "q").try(:[], "case_number_cont"), params[:search_query].try(:[], "q").try(:[], "dispute_type_eq"),  params[:search_query].try(:[], "q").try(:[], "amount_eq"), params[:search_query].try(:[], "q").try(:[], "card_number_cont"), params[:search_query].try(:[], "q").try(:[], "merchant_wallet_location_business_name_cont"), params[:search_query].try(:[], "q").try(:[], "id_cont"), params[:search_query].try(:[], "q").try(:[], "recieved_date")).last
          else
            report = reports.where(report_type: params[:type],wallet: params[:specific_wallet_id], transaction_type: params[:trans]).last
          end
        end
      else
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type],status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        report = reports.where(report_type: params[:type]).last if report.blank?
      end

      if report.present?
        return_hash[:find] = true
        return_hash[:status] = report.try(:status)
        return_hash[:report_file_id] = report.try(:file_id)
        return_hash[:id] = report.try(:id)
      end
    end
    return return_hash
  end

  def send_email_export_file
    if params[:user_id].present? && params[:qc_file].present?
      user = User.find_by(id: params[:user_id])
      qc_file = QcFile.find_by(id: params[:qc_file])
      if user.present? && qc_file.present?
        UserMailer.send_export_file(user, qc_file.image.url).deliver_later
        render status: 200, json:{success: 'Done'}
      else
        render status: 404, json:{success: 'Error'}
      end
    else
      render status: 404, json:{success: 'Error'}
    end
  end


  def local_transaction_details
    @curr_user = User.find_by(id: params[:user_id]) if params[:user_id].present?
    @customer = false
    if params[:trans] == "merchant_reserve"
      @reserve_wallet = Wallet.find(params[:wallet])
    end
    if params[:trans] == "iso_agent_aff" || params[:trans] == "merchant_reserve"
      @seq_id = Transaction.find_by_id(params[:transaction]).seq_transaction_id if Transaction.find_by_id(params[:transaction]).present?
      local = BlockTransaction.where(id: params[:transaction]).first.present? ? BlockTransaction.where(id: params[:transaction]).first : BlockTransaction.where(sequence_id:@seq_id).first
      @trx = @transaction = local.try(:parent_transaction).present? ? local.parent_transaction : local
    elsif params[:trans] == "user"
      @transaction = Transaction.where(id: params[:transaction]).first
      @trx = @transaction
      @customer = true
    else
      @transaction = Transaction.where(id: params[:transaction]).first
      @trx = @transaction
    end
    if !@trx.blank?
      @merchant = @transaction.tags.try(:[], "merchant")
      @location = @transaction.tags.try(:[], "location")
      @type = @transaction.main_type
      if params[:trans] == "iso_agent_aff" || params[:trans] == "merchant_reserve"
        @amount = @transaction.amount_in_cents.to_f/100
      else
        @amount = @transaction.amount.to_f
      end
      @timestamp = @transaction.timestamp
      @activity = ActivityLog.where(transaction_id: @trx.id).first
      if @transaction.try(:main_type) == "CBK Hold" || @transaction.try(:main_type) == "cbk_lost" || @transaction.try(:main_type) == "cbk_won" || @transaction.try(:main_type) == "CBK Won" || @transaction.try(:main_type) == "cbk_hold" || @transaction.try(:main_type) == "CBK Fee" ||  @transaction.try(:main_type) == "cbk_fee" || @transaction.try(:main_type) == "CBK Lost" || @transaction.try(:main_type) == "cbk_dispute_accepted" || @transaction.try(:main_type) == "CBK Lost Retire" || @transaction.try(:main_type)=='Dispute Accepted'
        case_number = @transaction.tags.try(:[],"dispute_case").try(:[],"case_number")
        @dispute_case = DisputeCase.includes(:charge_transaction).find_by(case_number: case_number)
        @db_transaction = @dispute_case.charge_transaction
      end
    end

    if params[:trans] == "decline"
      @user = @trx.receiver
      if @trx.tags.present? && !@trx.tags["amount"].present?
        @trx.tags.store("amount",  @trx.total_amount==0 ? @trx.amount : @trx.total_amount)
      elsif @trx.tags.nil?
        @trx.tags=Hash.new
        @trx.tags.store("amount", @trx.total_amount==0 ? @trx.amount : @trx.total_amount)
      end
      @transaction_info = @trx.tags
    end

    if @transaction.nil?
      flash[:error] = "Transaction not found in our current ledger!"
      redirect_back(fallback_location: root_path)
      return
    end
    if params[:trans] == "decline"
      @d = @trx
      @transaction_info = @transaction_info
      @l = @location
      @m = @merchant
      @u = @user
      @amount = @amount
      @curr_user = @curr_user
      render partial: 'admins/shared/detail_decline_transaction'
    else
      @t = @transaction
      @db_t = @db_transaction
      @users = @users
      @dispute = @dispute_case
      @amount = @amount
      @curr_user = @curr_user
      render partial: 'local_transaction_details'
    end
  end

  def block_card
    card=Card.where(id:params[:card_id]).try(:first)
    if card.present? && params[:from_admin]
      if card.is_blocked?
        Card.where(fingerprint:card.fingerprint,exp_date:card.exp_date).update_all(is_blocked:false)
        render status: 200, json:{success: 'Unblocked'}
      else
        Card.where(fingerprint:card.fingerprint,exp_date:card.exp_date).update_all(is_blocked:true)
        render status: 200, json:{success: 'blocked'}
      end
    elsif card.present?
      Card.where(fingerprint:card.fingerprint,exp_date:card.exp_date).update_all(is_blocked:false,decline_attempts:0)
      flash[:success]=I18n.t('card.card_updated')
      render status: 200, json:{success: 'blocked'}
    end
  end

  def users_local_detail
    transaction = BlockTransaction.where(id: params[:transaction]).first
    @transaction = transaction.parent_transaction
    if @transaction.present?
      @trx = @transaction
      if !@trx.blank?
        @merchant = @transaction.tags.try(:[], "merchant")
        @location = @transaction.tags.try(:[], "location")
        @type = @transaction.main_type
        @amount = SequenceLib.dollars(@transaction.amount_in_cents).to_f
        @timestamp = @transaction.timestamp
        @activity = ActivityLog.where(transaction_id: @trx.id).first
      end
      if @transaction.main_type == "CBK Hold" || @transaction.main_type == "CBK Won" || @transaction.main_type == "CBK Fee"
        case_number = @transaction.tags.try(:[],"dispute_case").try(:[],"case_number")
        @dispute_case = DisputeCase.includes(:charge_transaction).find_by(case_number: case_number)
        @db_transaction = SequenceLib.db_parse_action(@dispute_case.charge_transaction,@dispute_case.created_at)
      end
    else
      flash[:error] = "Transaction not found in our current ledger!"
      redirect_back(fallback_location: root_path)
    end

    respond_to :js

  end

  def exported_files
    Report.where(file_id: nil).update_all(read: true)
    if current_user.support_mtrac?
      @files = Report.where(user_id: current_user.id, status: ["completed","pending"], read: [false,true]).order(created_at: :desc).per_page_kaminari(params[:page]).per(10)
    else
      mtrac_user = User.support_mtrac.pluck(:id)
      mtrac_user.push(current_user.id)
      @files = Report.where(user_id: mtrac_user, status: ["completed","pending"], read: [false,true]).order(created_at: :desc).per_page_kaminari(params[:page]).per(10)
    end
  end

  def export_download
    if params[:report_id].present?
      report = Report.find_by(id: params[:report_id])
      report.update(read: true) if report.present?
    end
    if params[:id].present?
      files = []
      ids = JSON(params[:id])
      if ids.present?
        if ids.class == Array
          if ids.count == 1
            foo = QcFile.find(ids.first)
            data = open("https:"+foo.image.url)
            send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
          else
            QcFile.where(id: ids).each do|foo|
              files.push([foo.image, foo.image_file_name, modification_time: 1.day.ago])
            end
            zipline(files, 'export.zip')
          end
        else
          foo = QcFile.find(params[:id])
          data = open("https:"+foo.image.url)
          send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
        end
      end
    end
  end
  # # ----------------------- Generate export file end -----------------------#

end
