class Admins::DisputesController < AdminsController
	layout 'admin'
	include TransactionCharge::ChargeATransactionHelper
	
	def cases
		page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
		@filters = [10,25,50,100]
		params[:q] = JSON.parse(params[:q]) if params[:q].present? && params[:q].class == String
		if params[:q].present?
			params[:q]["id_cont"] = params[:q]["id_cont"].try(:strip)
			params[:q]["case_number_cont"] = params[:q]["case_number_cont"].try(:strip)
			params[:q]["dispute_type_eq"] = params[:q]["dispute_type_eq"].try(:strip)
			params[:q]["amount_eq"] = params[:q]["amount_eq"].try(:strip)
			params[:q]["card_number_cont"] = params[:q]["card_number_cont"].try(:strip)
			params[:q]["charge_transaction_total_amount_eq"] = params[:q]["charge_transaction_total_amount_eq"].try(:strip)
			params[:q]["merchant_wallet_location_business_name_cont"] = params[:q]["merchant_wallet_location_business_name_cont"].try(:strip)
		end
    if params[:q].present?
      if params[:q][:recieved_date].present?
				date = parse_date(params[:q][:recieved_date])
				first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
				second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
				params[:q][:recieved_date_gteq] = first_date
				params[:q][:recieved_date_lteq] = second_date

			end
			if params[:q][:created_at].present?
				date = parse_date(params[:q][:created_at])
				first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
				second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
				params[:q][:created_at_gteq] = first_date
				params[:q][:created_at_lteq] = second_date
			end
			if params[:q][:due_date].present?
				date = parse_date(params[:q][:due_date])
				first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
				second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
				params[:q][:due_date_gteq] = first_date
				params[:q][:due_date_lteq] = second_date
			end
    end
		@case = params[:case] if params[:case].present?
		if params[:case].present? && params[:case] != "all"
			case_status = DisputeCase.statuses[params[:case]]
		else
			case_status = ""
		end

		if case_status.present? && params[:q].present?
			params[:q][:status_eq] = case_status
		elsif case_status.present? && params[:q].blank?
			params[:q] = {
						"status_eq" => case_status
			}
		end
		@q = DisputeCase.ransack(params[:q])
		# @dispute_cases = @q.result.includes(:charge_transaction, :merchant_wallet).order(due_date: :desc)
		# @disputeCases = @q.result.order(recieved_date: :desc).per_page_kaminari(params[:page]).per(10)
		# @disputeCases = @q.result.order(recieved_date: :desc).per_page_kaminari(params[:page]).per(page_size)
		@disputeCases = @q.result.order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
		
		# @disputeCases = @dispute_cases
		#
		@accept_disputes = AppConfig.where(key: AppConfig::Key::IssueDisputeConfig).first
		@accept_disputes = AppConfig.new(key: AppConfig::Key::IssueDisputeConfig) unless @accept_disputes.present?
		#
		@dispute_types = DisputeCase.group(:dispute_type).sum(:amount)
		@types = [{value:"0",view:"Chargeback"},{value:"1",view:"Retrievel"}]
		respond_to do |format|
			format.html
			format.csv { send_data @disputeCases.except(:limit, :offset).to_csv, filename: "dispute_cases-#{Date.today}.csv" }
		end
	end

	def bulk_cbk
		begin
			file = params[:file]
			@cbks = []
			CSV.foreach(file.path, headers: true) do |row|
				if row[0].present?
					if row[4].present?
						reason = Reason.find_or_create_by(title: row[4], reason_type: "dispute").id
					else
						reason = ''
					end
					if params[:type] == "charge_id"
						@cbks.push({charge_id: row[0].gsub('BP_TEST_',"").gsub("BP_LIVE_",''), type: row[1], received_date: row[2], due_date: row[3], reason: reason, amount: row[5], status: row[6], notes: row[7], fee: row[8] })
					elsif params[:type] == "card"
						@cbks.push({charge_id: row[0], type: row[1], received_date: row[2], due_date: row[3], reason: reason, first6: row[5], last4: row[6], amount: row[7], status: row[8], notes: row[9], fee: row[10] })
					end
				end
			end
			BulkCbkWorker.perform_async(@cbks,request.try(:remote_ip),current_user.id, session[:session_id], params[:type]) if @cbks.present?
		rescue => exc
			flash[:notice] = exc.message
			redirect_back(fallback_location: root_path)
		end
	end
	def bulk_cbk_for_lost
		begin
			file = params[:file]
			@cbks = []
			CSV.foreach(file.path, headers: true) do |row|
				@cbks << row.first[1]
			end
			BulkCbkWorker.perform_async(@cbks,request.try(:remote_ip),current_user.id, session[:session_id], "lost_bulk_cbks") if @cbks.present?
			flash[:success]= I18n.t('merchant.controller.file_imported_successfully')
			redirect_to imported_for_lost_admins_disputes_path
		rescue => exc
			flash[:notice] = exc.message
			redirect_back(fallback_location: root_path)
		end
	end

	def imported_cbk
		@cbks =[]
		@cbks = QcFile.bulk_cbk.order(id: :desc) unless current_user.support_mtrac?
	end
	def imported_for_lost
		@cbks =[]
		@cbks = QcFile.lost_bulk_cbk.order(id: :desc) unless current_user.support_mtrac?
	end

	def dispute_case
		if params[:id].present? || params[:transaction].present?
			@dispute_case = DisputeCase.where(id: params[:id]).first
      if @dispute_case.present?
				@dispute_case.recieved_date = @dispute_case.recieved_date.present? ? @dispute_case.recieved_date : Date.today.strftime("%d/%m/%Y")
				@dispute_case.due_date = @dispute_case.due_date.present? ? @dispute_case.due_date : Date.today.strftime("%d/%m/%Y")
				@transaction = @dispute_case.charge_transaction
				if @transaction.present?
					merchant = @transaction.receiver
					@merchant = merchant if merchant.present? && merchant.MERCHANT?
				else
					flash[:notice] = "Transaction Not Found!"
					redirect_to transactions_admins_path
				end
      else
				if params[:from_user_profile].present?
					blocktransaction = BlockTransaction.find_by(id: params[:transaction])
					@transaction = Transaction.find_by(seq_transaction_id: blocktransaction.sequence_id)
				elsif params[:transaction].present?
					@transaction = Transaction.where(id: params[:transaction]).first
				end
				if @transaction.dispute_case.nil?
					@dispute_case = @transaction.build_dispute_case(recieved_date:Date.today.strftime("%d/%m/%Y"),due_date:Date.today.strftime("%d/%m/%Y"))
				else
					@dispute_case = @transaction.dispute_case
        end
				if @dispute_case.present?
					if @dispute_case.id.nil?
						@dispute_case.status=nil
						@dispute_case.dispute_type=nil
						@dispute_case.amount=nil
					end
				end
			end
		else
			flash[:notice] = "Transaction Not Found on your local system!"
			redirect_to transactions_admins_path
		end
		if @dispute_case.present?
			@dispute_case.amount=number_with_precision(@dispute_case.amount,precision: 2) if @dispute_case.id.present?
    end
  	@dispute_case.attachments.new if @dispute_case.attachments.blank?

		render partial: "admins/disputes/dispute_case"
		gon.dispute_case = @dispute_case
		#respond_to :js
	end

	def submit_dispute_case
		params[:dispute_case][:charge_fee] = true  if params[:dispute_case][:charge_fee] == "1"
		params[:dispute_case][:charge_fee] = false if params[:dispute_case][:charge_fee] == "0"
		if params[:dispute_case][:due_date].present?
			params[:dispute_case][:due_date]=Date.strptime(params[:dispute_case][:due_date],'%m/%d/%Y')
		else
			params[:dispute_case][:due_date]=Time.now.strftime('%m/%d/%Y')
		end
		if params[:dispute_case][:recieved_date].present?
			params[:dispute_case][:recieved_date]=Date.strptime(params[:dispute_case][:recieved_date],'%m/%d/%Y')
		else
			params[:dispute_case][:recieved_date]=Time.now.strftime('%m/%d/%Y')
		end
		if dispute_case_params[:id].present?
			if DisputeCase.exists?(dispute_case_params[:id])
				@dispute_case = DisputeCase.find(dispute_case_params[:id])
				if @dispute_case.payment_gateway.blank?
					gateway = PaymentGateway.find_by(id: params[:dispute_case][:gateway])
					@dispute_case.payment_gateway_id = gateway.try(:id)
				end
				run_process = false
				if @dispute_case.charge_back? and (@dispute_case.sent_pending? || @dispute_case.under_review?) and (dispute_case_params[:status] == "won_reversed" or dispute_case_params[:status] == "lost" or dispute_case_params[:status] == "dispute_accepted")
					run_process = true
        end
				if @dispute_case.update(dispute_case_params)
          params[:dispute_case][:attachments_attributes].each do |key,value|
            if value["_destroy"] == "false" && value["add_image"] != nil
            image={add_image:value['add_image'],usage_id:params[:dispute_case]['usage_id']}
            upload_attachment(image)
            end
          end
					begin
						if run_process
							dispute_type_process(@dispute_case,nil,request.try(:remote_ip))
						end
					rescue Exception => exc
						flash[:alert] = "#{exc.message}"
					end
					trans=Transaction.find_by(id:@dispute_case.transaction_id)
					flash[:notice] = "Dispute Successfully Updated!"
					if dispute_case_params[:status] == "lost" && trans.receiver.oauth_apps.first.is_block == false
						UserMailer.cbk_lost_email(@dispute_case, trans).deliver_later
						UserMailer.refund_cbk_email(@dispute_case, trans).deliver_later
					end
				else
					flash[:alert] = "#{@dispute_case.errors.full_messages}"
				end
			end
		else
			dispute_case_transaction = DisputeCaseTransaction.new
			merchant_sequence_balance = SequenceLib.balance(dispute_case_params[:merchant_wallet_id])
			charge_back_fee = dispute_case_transaction.get_new_location_fee(dispute_case_params[:merchant_wallet_id],TypesEnumLib::CommissionType::ChargeBack,dispute_case_params[:amount].to_f, nil, params[:dispute_case][:charge_fee])
			retrievel_fee=dispute_case_transaction.get_new_location_fee(dispute_case_params[:merchant_wallet_id],TypesEnumLib::CommissionType::Retrieval,dispute_case_params[:amount].to_f, nil, params[:dispute_case][:charge_fee])
			# charge_back_fee=dispute_case_transaction.get_location_fee(dispute_case_params[:merchant_wallet_id],16, params[:dispute_case][:charge_fee])
			# retrievel_fee=dispute_case_transaction.get_location_fee(dispute_case_params[:merchant_wallet_id],17, params[:dispute_case][:charge_fee])
			if dispute_case_params[:amount].present? && charge_back_fee["fee"].to_f == 0
				charge_back_total_amount = dispute_case_params[:amount].to_f
			elsif charge_back_fee["fee"].present? && dispute_case_params[:amount].present?
				charge_back_total_amount = (dispute_case_params[:amount].to_f + charge_back_fee["fee"].to_f)
			else
				charge_back_total_amount = 0
			end
			retrievel_total_amount = retrievel_fee["fee"].to_f
			if (charge_back_total_amount < merchant_sequence_balance and dispute_case_params[:dispute_type] == "charge_back") or ( retrievel_total_amount < merchant_sequence_balance and dispute_case_params[:dispute_type] == "retrievel" )
				@dispute_case = DisputeCase.new(dispute_case_params)
				gateway = PaymentGateway.find_by(id: params[:dispute_case][:gateway])
				gateway.total_cbk_count = gateway.total_cbk_count.to_i + 1
				trans=Transaction.find_by(id:@dispute_case.transaction_id)
				@dispute_case.payment_gateway_id = gateway.try(:id)
				if @dispute_case.save
					gateway.save(validate: false)
					params[:dispute_case][:attachments_attributes].each do |key,value|
						if value["_destroy"] == "false" && value["add_image"] != nil
							image={add_image:value['add_image'],usage_id:params[:dispute_case]['usage_id']}
							upload_attachment(image)
						end
					end
					if dispute_case_params[:dispute_type] == "charge_back" and (dispute_case_params[:status] == "won_reversed" or dispute_case_params[:status] == "lost" or dispute_case_params[:status] == "dispute_accepted")
						dispute_type_charge_back_won_or_lost_on_create(@dispute_case,params[:dispute_case][:charge_fee],request.try(:remote_ip))
					else
						dispute_type_process(@dispute_case,params[:dispute_case][:charge_fee],request.try(:remote_ip))
					end
					user = User.find(Wallet.find(dispute_case_params[:merchant_wallet_id]).merchant.id) if dispute_case_params[:merchant_wallet_id].present?
					noti_amount = number_to_currency(number_with_precision(@dispute_case.amount, :precision => 2, delimiter: ','))
					text = I18n.t('notifications.cbk_received_notif',id: @dispute_case.case_number, amount: noti_amount)
					Notification.notify_user(user,user,@dispute_case,"CBK received",nil,text)
					flash[:notice] = "Dispute Successfully Saved!"
					loc_email = Location.where(id:trans.try(:[],"tags").try(:[],"location").try(:[],"id")).last.try(:email)
					UserMailer.merchant_dispute(@dispute_case, trans, loc_email).deliver_later if @dispute_case.present? && trans.receiver.oauth_apps.first.is_block == false
					if dispute_case_params[:status] == "lost" && trans.receiver.oauth_apps.first.is_block == false
						UserMailer.cbk_lost_email(@dispute_case, trans).deliver_later
						UserMailer.refund_cbk_email(@dispute_case, trans).deliver_later
					end
				else
					flash[:alert] = "#{@dispute_case.errors.full_messages}"
				end
			else
				flash[:notice] = "Merchant has no sufficient Balance in wallet!"
			end
		end
		if params[:requested_path] == "admin_transactions"
			return redirect_to transactions_admins_path(trans: "success")
		elsif params[:requested_path] == "location_cbks"
			redirect_back(fallback_location: root_path)
		else
			return redirect_to cases_admins_disputes_path(case: "all")
		end
  end

  def upload_attachment(image)
    @doc= Image.new(image)
    if @doc.validate
      @doc.save
    else
      @error = @doc.errors.full_messages.first
    end
  end

	def dispute_notification_notes
		if params[:note_id].present?
			@note = Note.find(params[:note_id])
			@transaction = @note.notable
		else
			@transaction = Transaction.find(params[:transaction_id])
			@note = @transaction.notes.new
		end
		respond_to :js
	end

	def delete_dispute_notification_notes
		if params[:note_id].present?
			# @note = Note.find(params[:note_id])
			params[:note_id].each do |n|
				@note = Note.find_by(id:n.to_i)
				@note.destroy

			end

		end
	end

	def dispute_evidence
		@images=[]
		@user=[]
		if params[:dispute_id].present?
			@dispute=DisputeCase.find_by(id:params[:dispute_id])
			@dispute.update(status:'under_review')
			flash[:success]="Dispute is under reviewed successfully"
			redirect_to cases_admins_disputes_path(case:'under_review')
		else
			if params[:trans_id].present?
				@transaction=Transaction.select(:sender_id,:ip).find(params[:trans_id])
				@user=User.find_by(id:@transaction.sender_id) if @transaction.present?
			end
			if params[:id].present?
				@evidence=Evidence.find_by(dispute_id:params[:id])
				if @evidence.present?
					@images=@evidence.images
					@url = dispute_evidence_admins_disputes_path(id:params[:id])
				end
			end
		end
	end

	def dispute_notification_note
		if params[:note_id].present?
			@note = Note.find(params[:note_id])
			@note.name = params[:name]
			@note.title = params[:title]
			@note.save
		elsif params[:all_data].present?
			params[:all_data].each do |index,val|
				@transaction = Transaction.find(val[:transaction_id])
				@note = @transaction.notes.new
				@note.name = val[:name]
				@note.title = val[:title]
				@note.save
			end

		end
		if @note.present?
			render json:{id:@note.id,title:@note.title,created_at: @note.created_at}
		else
			render json:{message:"Fill Up Your Fields!"}
		end
	end

	def dispute_notification_reason
		if params[:title].present? and params[:title] != ""
			@reason = Reason.new(reason_type:"dispute",title:params[:title])
			@reason.save
		end
		if @reason.present?
			render json:{id:@reason.id,title:@reason.title}
		else
			render json:{message:"Fill Up Your Fields!"}
		end
	end

	def dispute_notification_reasons
		@reason = Reason.new(reason_type:"dispute")
		respond_to :js
	end

	def get_case_number
		dispute_case=DisputeCase.find_by_case_number(params[:case_number]) if params[:case_number]
		render json:{case_number:dispute_case.present? ? dispute_case.case_number : ""}
	end

	private

	def dispute_type_process(dispute_case,check_for_fee=nil,request=nil)
		dispute_case_transaction = DisputeCaseTransaction.new
		charge_back_wallet = Wallet.charge_back.last
		if dispute_case.present? and charge_back_wallet.present?
			if dispute_case.charge_back? and dispute_case.sent_pending?
				dispute_case_transaction.transfer_charge_back_sent(dispute_case,nil,check_for_fee, charge_back_wallet)
			elsif dispute_case.retrievel?
				dispute_case_transaction.transfer_retreivel_fee(dispute_case,check_for_fee)
			elsif dispute_case.charge_back? and dispute_case.won_reversed?
				dispute_case_transaction.transfer_charge_back_won(dispute_case, charge_back_wallet)
			elsif dispute_case.charge_back? and (dispute_case.lost? || dispute_case.dispute_accepted?)
				dispute_case_transaction.transfer_charge_back_lost(dispute_case,request, charge_back_wallet)
			end
		end
	end

	def dispute_type_charge_back_won_or_lost_on_create(dispute_case,check_for_fee=nil,request=nil)
		dispute_case_transaction = DisputeCaseTransaction.new
		if dispute_case.present?
			if dispute_case.charge_back? and dispute_case.won_reversed?
				dispute_case_transaction.transfer_charge_back_sent(dispute_case,"charge_back_sent_plus_fee",check_for_fee)
				dispute_case_transaction.transfer_charge_back_won(dispute_case)
			elsif dispute_case.charge_back? and (dispute_case.lost? || dispute_case.dispute_accepted?)
				dispute_case_transaction.transfer_charge_back_sent(dispute_case,nil,check_for_fee)
				dispute_case_transaction.transfer_charge_back_lost(dispute_case,request)
			end
		end
	end
	def dispute_case_params
		params.require(:dispute_case).permit(:id,:merchant_wallet_id,:user_wallet_id,:case_number,:user_id,:transaction_id,:dispute_type,:recieved_date,:due_date,:amount,:card_type,:card_number,:status,:reason_id,:charge_fee, :payment_gateway_id,:chargeback_transaction_id)
  end

  def upload_docs
    params.require(:image).permit(:add_image,:usage_id,:location_id,:_destroy)
	end
end
