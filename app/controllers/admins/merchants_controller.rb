class Admins::MerchantsController < AdminsController
  #content_security_policy do |policy|
  #  append_content_security_policy_directives(script_src: %w("nonce-#{request.content_security_policy_nonce}"))
  #end
  include Wicked::Wizard
  before_action :set_steps
  before_action :setup_wizard, except: [ :new_sub_merchant , :create_sub_merchant , :edit_sub_merchant , :update_user_sub_merchant , :destroy_sub_merchant , :country_data]
  # steps :corporate, :owners, :location, :iso_agent, :fee, :setting, :documentation
  skip_before_action :check_admin
  skip_before_action :mtrac_admin, except: :show

  def index
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    if params[:query].present?
      conditions = search_sub_merchant(params)
      if conditions.present?
        @merchants = User.joins(:parent_merchant).where.not(merchant_id: nil).where(archived: "false").where(conditions).order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
        # @merchants = User.joins(:parent_merchant).where.not(merchant_id: nil).where(conditions).order(id: :desc).per_page_kaminari(params[:page]).per(10)
      else
        @merchants = User.where.not(merchant_id:nil).where(archived: "false").order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
      end
      @sub_merchant = true
      # params[:query]["role"] = 'merchant'
      # @merchants = []
      # search_operation = 'like' if params[:query]["equal"] == 'like'
      # search_operation = 'not like' if params[:query]["equal"] == 'not_equal'
      # search_by='name' if params[:query]["option"]=='name'
      # search_by='email' if params[:query]["option"]=='email'
      # search_by='phone_number' if params[:query]["option"]=='phone_number'
      # search_by='ref_no' if params[:query]["option"]=='ref_no'
      # search_by='location_secure_token' if params[:query]["option"]=='location_id'
      # search_by='business_name' if params[:query]["option"]=='location_dba_name'
      # if params[:query]["option"]=='name' || params[:query]["option"]=='email' || params[:query]["option"]=='phone_number' || params[:query]["option"]=='ref_no'
      #   if params[:query]["equal"]=='like' || params[:query]["equal"]=='not_equal'
      #     @merchants = User.merchant.where("#{search_by} #{search_operation} ?", "%#{params[:query]["find_by"]}%").where(:merchant_id=>nil)
      #   else
      #     @merchants = User.merchant.where(:"#{search_by}" => "#{params[:query]["find_by"]}").where(:merchant_id=>nil)
      #   end
      # end
      #   if params[:query]["option"]=='location_id' || params[:query]["option"]=='location_dba_name'
      #     if params[:query]["equal"]=='like' || params[:query]["equal"]=='not_equal'
      #       location_merchant_ids = Location.where("#{search_by} #{search_operation} ?", "%#{params[:query]["find_by"]}%").pluck(:merchant_id)
      #       if location_merchant_ids.present?
      #         @merchants =User.where('id IN (?)', location_merchant_ids).where(:merchant_id=>nil)
      #       end
      #     else
      #       location_merchant_ids = Location.where(:"#{search_by}" => "#{params[:query]["find_by"]}").pluck(:merchant_id)
      #       if location_merchant_ids.present?
      #         @merchants =User.where('id IN (?)', location_merchant_ids).where(:merchant_id=>nil)
      #       end
      #     end
      #   end
      #   if params[:query]["option"]=='wallet_id'
      #     if params[:query]["equal"]=='like' || params[:query]["equal"]=='equal'
      #         @merchants = Wallet.find(params[:query]["find_by"].to_i).users
      #     else
      #       @wallets = Wallet.where.not(id: params[:query]["find_by"].to_i)
      #       @abc = []
      #       @wallets.each do|a|
      #         if a.users.present?
      #           if a.users.first.merchant?
      #             @abc.push(a.users)
      #           end
      #         end
      #       end
      #       @merchants = @abc.flatten.uniq
      #     end
      #   end
      #   if params[:query]["gateway"].present?
      #     if params[:query]["equal"]=='like' || params[:query]["equal"]=='equal'
      #       @merchants = User.merchant.where(primary_gateway_id: params[:query]["gateway"].to_i,merchant_id: nil)
      #     else
      #       @merchants = User.merchant.where.not(primary_gateway_id: params[:query]["gateway"].to_i).where(:merchant_id=>nil)
      #     end
      #   end
      # @merchantss = Kaminari.paginate_array(@merchants).page(params[:page]).per(10)
      # @count = @merchants.count
    else
      if params[:from_specific_merchant] == "true"
        payment_gateway_id = params[:payment_gateway_id]
        locations = Location.where("primary_gateway_id = ? or secondary_gateway_id = ? or ternary_gateway_id = ?", payment_gateway_id ,payment_gateway_id ,payment_gateway_id)
        merchant_ids = locations.pluck(:merchant_id)
        @merchant = User.new
        @merchants = User.merchant.where(id: merchant_ids,low_risk: [nil,false]).includes(:oauth_apps).where(:merchant_id => nil).order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
      elsif params[:merchant_ids].present?
        @merchant = User.new
        @merchants = User.merchant.where(id: params[:merchant_ids],low_risk: [nil,false]).includes(:oauth_apps).where(:merchant_id => nil).order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
      else
        @merchant=[]
        @merchant = User.new
        if params[:archive].present?
          @merchants = User.archived_users.merchant.includes(:oauth_apps).where(:merchant_id => nil,low_risk: [nil,false]).complete_profile_users.order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
        elsif params[:sub_merchant] == "true"
          @merchant_id = params[:merchant_id].present? ? params[:merchant_id].to_i : nil
          if @merchant_id.present?
            @merchants = User.where(merchant_id: @merchant_id).where(archived: "false").order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
          else
            @merchants = User.where.not(merchant_id:nil).where(archived: "false").order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
          end
          @sub_merchant = true
        else
          @merchants = User.active_merchant.merchant.includes(:oauth_apps).where(:merchant_id => nil,low_risk: [nil,false]).complete_profile_users.order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
        end
      end
    end
  end

  def export
    # locations=JSON.parse(params['query']['export'])
    if params[:export_type]=='all'
      if params[:archive].present?
        locations=User.archived_users.merchant.includes(:oauth_apps).where(:merchant_id => nil,low_risk: [nil,false]).complete_profile_users.order(id: :desc)
      else
        locations=User.merchant.includes(:oauth_apps).includes(:owners).where(:merchant_id => nil,low_risk: [nil,false]).complete_profile_users.order(id: :desc)
      end
    else
      params[:query]=JSON.parse(params['query']['export'])
      search()
      locations=@merchants
    end

    send_data generate_csv(locations), filename: "Merchant.csv"
  end

  def generate_csv(merchants)
    locations = Location.where(merchant_id: merchants.pluck(:id)).select("id AS id", "merchant_id AS merchant_id").group_by{|e| e.merchant_id}
    CSV.generate do |csv|
      csv << ['Merchant ID', 'Created Day', 'Merchant Name', 'Total Locations', 'Owner\'s Name', 'Phone Number', 'Email', 'Status']
      merchants.each do |merchant|
        if merchant.oauth_apps.first.present?
          status = merchant.oauth_apps.first.is_block ? 'Inactive' : 'Active'
        else
          status = "Incomplete Profile"
        end
        # primary= PaymentGateway.find_by_id(merchant.primary_gateway_id) if merchant.primary_gateway_id.present?
        # primary=primary.blank? ? '' : primary.name
        # second= PaymentGateway.find_by_id(merchant.secondary_gateway_id) if merchant.secondary_gateway_id.present?
        # second=second.blank? ? '' : second.name
        # third= PaymentGateway.find_by_id(merchant.ternary_gateway_id) if merchant.ternary_gateway_id.present?
        # third=third.blank? ? '' : third.name
        csv << ["M-#{merchant.id}", merchant.created_at.to_date, merchant.name, locations[merchant.id].try(:count), merchant.owners.present? ? merchant.owners.last.try(:name) : merchant.owner_name, merchant.phone_number, merchant.email, status]
      end
    end
  end

  def new
    @merchant = User.new
  end

  def show
    begin
      if params[:merchant_id].present?
        @merch = User.find_by(id: params[:merchant_id])
      end
      if params[:status].present?
        if params[:status] == "l1" && params[:merchant_id].present?
          @locations = Location.where(merchant_id:params[:merchant_id]).select("id", "business_name")
          @merchant_id = params[:merchant_id]
        end
      end
      case step
      when :corporate
        merchant_data = {}
        if params[:merchant_id].present? && params[:status] == "c1"
          @merchant = User.find params[:merchant_id]
        end
        if params[:merchant_data_id].present?
          corporate_data = MerchantData.where(id: params[:merchant_data_id]).select(:corporate).first
          corporate = corporate_data.corporate
          if corporate.present? && corporate != "{}"
            merchant_data = corporate
            merchant_data[:profile] = corporate["profile_attributes"]
            @merchant = OpenStruct.new(merchant_data)
            @merchant.profile = OpenStruct.new(merchant_data[:profile])
          end
        end
      when :owners
        owners_arr  = []
        if params[:merchant_id].present?
          user = User.friendly.find params[:merchant_id]
          if user.owners.present?
            owners = user.owners
            @owners = owners.order("owners.id ASC")
          end
        end
        if params[:merchant_data_id].present?
          owners_data = MerchantData.where(id: params[:merchant_data_id]).select(:owners).first
          owners = owners_data.owners
          if owners.present? && owners != "{}"
            owners.each do |key,val|
              val["phone_number"] = val["phone_code"] + val["phone_number"]
              owners_arr[key.to_i] = val.with_indifferent_access
            end
            @owners = owners_arr.compact
          end
        end
      when :location
        if params[:merchant_data_id].present?
          location_data = MerchantData.where(id: params[:merchant_data_id]).select(:location).first
          location = location_data.location
          if location.present? && location != "{}"
            @location = OpenStruct.new(location)
          end
        end
        # if params[:location_id].present?
        #   @location = Location.find params[:location_id]
        # end
        @categories = Category.all.select('id', 'name').order(name: :asc)
        @ecom_platforms = EcommPlatform.all.select('id', 'name').order(name: :asc)
      when :iso_agent
        @agent = nil
        @iso = nil
        @affiliate = nil
        profit_splits = {}
        if params[:merchant_data_id].present?
          iso_agent_data = MerchantData.where(id: params[:merchant_data_id]).select(:iso_agent).first
          iso_agent = iso_agent_data.iso_agent
          if iso_agent.present? && iso_agent != "{}"
            location = {}
            iso_agent = OpenStruct.new(iso_agent)
            if iso_agent.baked_iso == "1"
              location[:baked_iso] = true
              profit_splits[:share_split] = iso_agent.profit_splits["share_split"]
            end
            if iso_agent.profit_split == "1"
              location[:profit_split] = true
            end
            if iso_agent.sub_merchant_split == "1"
              location[:sub_merchant_split] = true
            end
            if iso_agent.net_profit_split == "1"
              location[:net_profit_split] = true
            end
            profit_splits[:splits] = iso_agent.try(:profit_splits).try(:[], "splits")
            location[:profit_split_detail] = profit_splits.to_json
            @location = OpenStruct.new(location)
            @users = User.where(id: iso_agent.ids).select(:id, :company_name, :name, :role)
            if @users.present?
              @users.each do |u|
                if u.iso?
                  @iso = u
                end
                if u.agent?
                  @agent = u
                end
                if u.affiliate?
                  @affiliate = u
                end
              end
            end
          end
        end
        gon.location_data = @location
        gon.profit_split_data = iso_profit_split_locations(@location)
        gon.net_profit_locations = net_profit_split_locations(@location)
        gon.ez_merchant_data = ez_merchant_locations(@location)
      when :fee
        if params[:merchant_data_id].present?
          fee_data = MerchantData.where(id: params[:merchant_data_id]).select(:fees, :location).first
          fees = fee_data.fees
          @ach_international = fee_data.location.try(:[], "ach_international")
          if fees.present? && fees != "{}"
            fees = OpenStruct.new(fees)
            @fees = fees
          end
        end
      when :setting
        if params[:merchant_data_id].present?
          setting_data = MerchantData.where(id: params[:merchant_data_id]).select(:setting).first
          settings = setting_data.setting
          if settings.present? && settings != "{}"
            settings = OpenStruct.new(settings)
            fee = OpenStruct.new(settings[:fee])
            @location = settings
            @fees = fee
          end
        end
        gon.setting_location_data = @location
      when :bank_account
        bank_data = []
        if params[:merchant_data_id].present?
          bank_account = MerchantData.where(id: params[:merchant_data_id]).select(:bank_details).first
          bank_details = bank_account.bank_details
          img_ids = []
          if bank_details.present? && bank_details != "{}"
            bank_details.each do |key, bank|
              img_ids << bank["saved_image_id"]
              bank_data << OpenStruct.new(bank)
            end
            @bank_account = bank_data
          end
          @images = Image.where(id: img_ids).group_by(&:id)
        end
      when :documentation
        if params[:location_id].present?
          location_data = Location.find params[:location_id]
          if location_data.documentations.present?
            @images = location_data.documentations
          elsif params[:old_location_id].present?
            location_data = Location.find params[:old_location_id]
            if location_data.documentations.present?
              @images = location_data.documentations
            end
          end
        end
      end
      @count = 1
      if params[:user_id].present?
        @merchant= User.find params[:user_id]
        @wallets=@merchant.wallets
        respond_to do |format|
          format.html{ render :show}
          format.json { render json: UsersDatatable.new(@merchant)}
        end
      end
      render_wizard(nil, {}, {status:params[:status]})
    rescue => exc
      p "EXC", exc
      p "EXC", exc.backtrace
    end

  end

  def search
    @filters = [10,25,50,100]
    per_page = params[:filter] || "10"
    if params[:archive].present?
      user = {archived: true, merchant_id: nil}
    else
      user = {archived: false, merchant_id: nil}
    end
    if params[:query].present? && params[:query].select{ |k,v| v.present? }.present?
      @merchants = []
      params[:query]["role"] = 'merchant'
      @key = nil
      @location_keys=nil

      if params[:query]["name"].present?
        @key = "name ILIKE :name"
      end
      if params[:query]["email"].present?
        if @key.nil?
          @key = "email ILIKE :email"
        else
          @key += " Or email ILIKE :email"
        end
      end
      if params[:query]["phone_number"].present?
        if @key.nil?
          @key = "phone_number ILIKE :phone_number"
        else
          @key += " Or phone_number ILIKE :phone_number"
        end
      end
      if params[:query]["ref_no"].present?
        params[:query]["ref_no"] = params[:query]["ref_no"].try(:strip)
        if @key.nil?
          @key = "ref_no ILIKE :ref_no"
        else
          @key += " Or ref_no ILIKE :ref_no"
        end
      end
      if params[:query]["location_id"].present?
        @location_keys = "location_secure_token ILIKE :location_secure_token"
      end
      if params[:query]["location_dba_name"].present?
        if @location_keys.nil?
          @location_keys = "business_name ILIKE :business_name"
        else
          @location_keys += " Or business_name ILIKE :business_name"
        end
      end
      if params[:from_tickets].present?
        @key = nil
        if params[:query]["name"].present?
          @key = "name ILIKE :name"
        end
        if params[:query]["ref_no"].present?
          if @key.nil?
            @key = "ref_no ILIKE :ref_no"
          else
            @key += " AND ref_no ILIKE :ref_no"
          end
        end
      end
      if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present? || params[:query]["role"].present? || params[:query]["last4"].present? || params[:query]["wallet_id"].present?
        if params[:query]["name"].present? || params[:query]["email"].present? || params[:query]["phone_number"].present? || params[:query]["ref_no"].present?
          @merchants = User.where(user).merchant.where(low_risk: [nil,false]).complete_profile_users.where(@key,  name: "%#{params[:query]["name"].try(:strip)}%", email: "%#{params[:query]["email"].try(:strip)}%",phone_number: "%#{params[:query]["phone_number"].try(:strip)}%",ref_no: "%#{params[:query]["ref_no"].try(:strip)}%").order(created_at: :asc)
        end
        if params[:query]["wallet_id"].present?
          if Wallet.where(id: params[:query]["wallet_id"].try(:strip).to_i).present?
            w = Wallet.find(params[:query]["wallet_id"].try(:strip).to_i).users.where(user).where(low_risk: [nil,false])
            @merchants = @merchants | w
          end
        end
        if params[:query]["location_id"].present? || params[:query]["location_dba_name"].present?
          # loc = Location.where(location_secure_token: params[:query]["location_id"])
          #  loc = Location.where('location_secure_token LIKE ?', "%#{params[:query]["location_id"]}%")
          location_merchant_ids = Location.where(@location_keys, location_secure_token: "%#{params[:query]["location_id"].try(:strip)}%", business_name: "%#{params[:query]["location_dba_name"].try(:strip)}%").pluck(:merchant_id)
          if location_merchant_ids.present?
            @user_loc=User.where(user).complete_profile_users.where(low_risk: [nil,false]).where('id IN (?)', location_merchant_ids)
            if @user_loc.present?
              @merchants = @merchants | @user_loc
            end
          end
        end
      end
      if params[:query]["gateway"].present?
        w = User.where(user).merchant.complete_profile_users.where(low_risk: [nil,false],primary_gateway_id: params[:query]["gateway"].try(:strip).to_i,merchant_id: nil)
        @merchants = @merchants | w if w.present?
      end
      # @locations_count = 0
      # @merchants.each do |m|
      #   @locations_count = @locations_count + Location.where(merchant_id: m.id).count
      # end
      unless params[:export_type].present? && params[:export_type]=='export'
        @merchants = Kaminari.paginate_array(@merchants).page(params[:page]).per(per_page)
        render template: 'admins/merchants/index'
      end
    else
      redirect_to merchants_path(archive: params[:archive], filter: per_page )
    end
  end

  def create
    company = Company.find params[:user][:company_id]
    @merchant = User.new(new_merchant_params)
    @merchant.ledger = company.ledger if company.ledger.present?
    password = random_password
    @merchant.password = password
    @merchant.is_password_set = false
    @merchant.regenerate_token

    if !@merchant.valid?
      flash[:error] = @merchant.errors.full_messages.first
    else
      if @merchant.merchant!
        # @merchant.send_welcome_email('Merchant')
        flash[:success] = 'Merchant has been added Successfully!'
      else
        flash[:error] = @merchant.errors.full_messages.first
      end
    end
    respond_to do |format|
      format.html{ redirect_back(fallback_location: root_path) }
    end
  end

  def edit
    @merchant= User.friendly.find params[:merchant_id]
    @tos_accepted_by = User.find(@merchant.tos_user_id) if @merchant.tos_user_id.present?
    @locations = Location.where(merchant_id: @merchant.id)
    @images = Documentation.where(imageable_id: @locations.ids)
    @owners=@merchant.owners.order("owners.id ASC")
    @sub_merchant_edit_data = get_sub_merchant_data(@merchant)
    @assigned_location_ids = LocationsUsers.where(user_id:@merchant.id, attach:true, relation_type: "secondary").pluck(:location_id)
    render partial: 'edit'
  end

  def update
    merchant_data_id = nil
    merchant_id = nil
    location_id = nil
    old_location_id = nil
    loc = nil
    duplicate = nil
    @merchant = nil
    case step
    when :corporate
      params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
      if params[:user]["profile_attributes"]["country"] == "" &&  params["country"].present?
        params[:user]["profile_attributes"]["country"]= params["country"]
        params[:user]["profile_attributes"]["state"]= params["state"]
      end
      @merchant = save_merchant_data(params[:user])
      merchant_data_id = @merchant[:merchant]
      merchant_id = params[:merchant_id]
    when :owners
      merchant_data_id = params[:merchant_data_id]
      location_id = params[:location_id] if params[:location_id].present?
      @owner = save_owners_data
      @count = 1
    when :location
      if params[:user][:location]["country"]== "" && params["country"].present?
        params[:user][:location]["country"] = params["country"]
        params[:user][:location]["state"] = params["state"]
      end
      merchant_data_id = params[:merchant_data_id]
      @location = save_location_data
    when :iso_agent
      merchant_data_id = params[:merchant_data_id]
      @iso_agent = save_iso_agents_data
    when :fee
      merchant_data_id = params[:merchant_data_id]
      @fee = save_fee_data
    when :setting
      merchant_data_id = params[:merchant_data_id]
      @setting = save_setting_data
    when :bank_account
      merchant_data_id = params[:merchant_data_id]
      @bank_account = save_bank_account
    when :documentation
      merchant_data = save_all_data(params[:merchant_data_id])
      if params[:user].present?
        documentation_data = params[:user][:documentation] if params[:user][:documentation].present?
        if merchant_data[:success]
          @documentation = save_documentation(documentation_data)
        else
          flash[:error] = merchant_data[:message]
        end
      end
      merchant_id = params[:merchant_id]
      location_id = params[:location_id]
    end
    if params[:user_id].present?
      params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
      params[:merchant] = params[:user]
      params[:merchant] = params[:user].except(:form_id)
      if params[:user_owner_form_id].present?
        owner_ids = params[:user_owner_form_id].split(',')
        merchant_id = params[:user_id]
        remove_owners(owner_ids, merchant_id)
      end
      if  params[:user][:phone_number].present?
        phone_number = params[:user][:phone_code] + params[:user][:phone_number]
      end
      merchant = User.find params[:user_id]
      # saving sub merchant location
      if params[:user].try(:[], "assign_location") == "1"
        ez_data = params["profit_splits"].try(:[], "splits").try(:[], "ez_merchant")
        update_assigned_location_data(merchant, ez_data, true)
      else
        ez_data = JSON.parse(merchant.assigned_location_detail) if merchant.assigned_location_detail.present?
        update_assigned_location_data(merchant, ez_data, false)
      end
      if edit_merchant_params[:password].present?
        ex=edit_merchant_params
        ex[:phone_number] = phone_number
        merchant.update(ex)
        if params[:user].try(:[],'owner').present?
          params[:owner]=params[:user]['owner']
          params[:merchant_id]=params[:user][:user_id]
          save_owners(params[:owner])
        end
      else
        #  updating owners
        if params[:user].try(:[],'owner').present?
          params[:owner]=params[:user]['owner']
          params[:merchant_id]=params[:user][:user_id]
          save_owners(params[:owner])
        end
        ex=edit_merchant_params.except(:password_confirmation)
        ex = ex.except(:password)
        ex[:phone_number] = phone_number
        merchant.update(ex)

      end
      if merchant.errors.blank?
        merchant.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
        flash[:success] = "Merchant has been updated Successfully"
      else
        flash[:error] = merchant.errors.full_messages.first
      end
      params[:status] = "old-update"
      update_documentation()
      if params[:user][:documentation].present?
        documentation_data = params[:user][:documentation] if params[:user][:documentation].present?
        @documentation = save_documentation(documentation_data)
      end
      return redirect_back(fallback_location: root_path);
    end
    if @merchant.present? && @merchant[:success] == false
      redirect_to wizard_path(@step,{:status => params[:status], :merchant_id => params[:merchant_id], :location_id => location_id, :old_location_id => old_location_id, :loc => loc, :duplicate => duplicate})
    else
      redirect_to wizard_path(@next_step,{:status => params[:status], :merchant_id => merchant_id, :merchant_data_id => merchant_data_id, :location_id => location_id, :old_location_id => old_location_id, :loc => loc, :duplicate => duplicate, :archive => params[:archive]})
    end
  end

  def get_data
    merchant_id = params[:merchant_id].to_i
    locations = []
    keys = User.merchant.where(id: merchant_id).first.oauth_apps.first
    merchant = User.where(id: merchant_id).first
    if merchant.present?
      locations = []
      @locations=Location.where(merchant_id: merchant_id)
      if @locations.present?
        @locations.each do |location|
          locations << {
              location_name: location.business_name,
              location_key: location.location_secure_token
          }
        end
      end
      merchantData = [locations,keys]
      render status: 200, json:{success: 'Verified', :location => merchantData}
    else
      render status: 404, json:{:location => nil}
    end
  end

  def profile
    if params[:iso_merchant_id].present?
      params[:format] = User.find(params[:iso_merchant_id]).try(:slug)
    end
    @merchant  = User.includes(:profile).includes(:company).includes(:transactions).includes(:tickets).friendly.find params[:format]
    @merchant_wallets = @merchant.wallets.primary.pluck(:id)
    @dispute_case = DisputeCase.where(merchant_wallet_id: @merchant_wallets)
    @owners = @merchant.owners.order("owners.id ASC")
    @ownersCount = @owners.count
    @owners = @owners.limit(2)
    if params[:message].present?
      flash[:success] = params[:message]
      params.delete(:message)
      redirect_to request.path, :params => params
    end
  end

  def more_owners
    @merchant = User.find params[:merchant_id]
    @owners = @merchant.owners.order("owners.id ASC")
    respond_to do |format|
      format.js
    end
  end

  def remove_owner
    @merchant = User.find params[:merchant_id]
    @owner = Owner.find params[:owner_id]
    if @owner.destroy
      @owners = @merchant.owners.order("owners.id ASC")
      respond_to do |format|
        format.js {
          render 'admins/merchants/more_owners.js.erb'
        }
      end
    end
  end
  def remove_owners(owner_ids,merchant_id)
    @merchant = User.find(merchant_id)
    owner_ids.each do |id|
      @owner = Owner.find(id)
      @owner.destroy
    end
  end


  def delete_users
    @user = User.friendly.find(params[:user_id])
    @user.archived = true
    if @user.save
      flash[:notice=] = "Successfully deleted User."
      redirect_back(fallback_location: root_path)
    end
  end

  def update_status
    @merchant = User.find_by(id: params[:merchant_id])
    if @merchant.present?
      if @merchant.is_block?
        @merchant.is_block = false
      else
        @merchant.is_block = true
      end
      if @merchant.save(:validate => false)
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Error in updation'}
      end
    else
      render status: 404, json:{error: 'No Usr found'}
    end
  end

  def update_two_step
    @merchant = User.find_by(id: params[:merchant_id])
    if @merchant.present?
      if @merchant.two_step_verification?
        @merchant.two_step_verification = false
      else
        @merchant.two_step_verification = true
      end
      if @merchant.save(:validate => false)
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Error in updation'}
      end
    else
      render status: 404, json:{error: 'No Usr found'}
    end
  end

  def send_unblock_email
    @merchant = User.find_by(id: params[:merchant_id])
    if @merchant.present?
      @merchant.resend_unlock_instructions
      render status: 200, json:{success: 'Verified'}
    else
      render status: 404, json:{error: 'No Usr found'}
    end
  end

  def resend_welcome_email
    @merchant = User.find_by(id: params[:merchant_id])
    if @merchant.present?
      @role = @merchant.role
      @merchant.password_changed(true)
      if @merchant.merchant?
        @merchant.send_welcome_email(@role, "resend") if @merchant.oauth_apps.first.is_block == false
      else
        @merchant.send_welcome_email(@role, "resend") if @merchant.is_block == false
      end
      render status: 200, json:{success: 'Verified'}
    else
      render status: 404, json:{error: 'No Usr found'}
    end
  end

  def update_location_block
    location = Location.find params[:location_id]
    if params[:is_block] == "true"
      result = location.update(is_block:  true)
    elsif params[:is_block] == "false"
      result = location.update(is_block:  false)
    end
    if result
      render status: 200, json:{success: 'success'}
    else
      render status: 404, json:{error: 'Error in reset'}
    end
  end

  def unlock
    if params["type"].blank?
      @wallet = Wallet.friendly.find(params[:wallet_id])
      @balance = show_balance(@wallet.id) - HoldInRear.calculate_pending(@wallet.id)
    elsif params["type"] == "wallet"
      @wallet = Location.find_by(id: params[:location_id]).try(:wallets).try(:primary).try(:first)
    elsif params["type"] == "fee"
      @fee = Location.find_by(id: params[:location_id]).try(:fees).try(:buy_rate).try(:first)
    elsif params["type"] == "iso_agent"
      @location = Location.find_by(id: params[:location_id])
    end
    # @balance = show_balance(@wallet.id) - HoldInRear.calculate_pending(@wallet.id)
    respond_to :js
  end

  def unlock_post
    @object = Wallet.find_by(id: params[:wallet_id]) if params[:wallet_id].present?
    @object = Fee.find_by(id: params[:fee_id]) if params[:fee_id].present?
    @object = Location.find_by(id: params[:location_id]) if params[:location_id].present?
    if @object.update(is_locked: false)
      flash[:notice] = "Unlocked Successfully!"
    else
      flash[:notice] = "Something went wrong. Please try again later!"
    end
    if params[:wallet_id].present?
      redirect_to admins_merchants_wallet_detail_path(location_id: params[:location],format: params[:merchant])
    elsif params[:fee_id].present?
      redirect_to admins_merchants_location_fee_path(location_id: params[:location],format: params[:merchant])
    elsif params[:location_id].present?
      redirect_to admins_merchants_location_iso_agents_path(location_id: params[:location],format: params[:merchant])
    else
      redirect_back(fallback_location: root_path)
    end
  end

  def wallet_detail
    @merchant  = User.friendly.find params[:format]
    @location = Location.includes(:wallets).find_by(id: params[:location_id])
    begin
      @location.bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT-ENCRYPTOR']}",@location.bank_account)
    rescue
      @location.bank_account
    end
    @primary_wallet = @location.try(:wallets).try(:primary).try(:first)
    @reserve_wallet = @location.try(:wallets).try(:reserve).try(:first)
    @achs = TangoOrder.where(wallet_id: @primary_wallet.id).instant_ach.last(5)
    @giftcards = TangoOrder.where(wallet_id: @primary_wallet.id).where.not(utid: nil).last(5)
    @checks = TangoOrder.where(wallet_id: @primary_wallet.id).check.last(5)
    @requests = Request.where(wallet_id: @primary_wallet.id).order("requests.created_at DESC").last(5)
  end

  def transfer_money
    @transfer = Transfer.new
    @wallet = Wallet.find_by(id: params[:wallet_id])
    respond_to :js
  end

  def specfic_transactions
    @wallet = Wallet.find_by(id: params[:wallet_id])
    @last = true
    @transactions = []
    conditions = []
    per_page = params[:tx_filter] || 10
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = transactions_search_query(params[:query],params[:trans],params[:offset])
    end
    if params[:trans] == "undefined" || params[:trans] == "success" || params[:trans].blank?
      @transactions = Transaction.where.not(status: "pending").where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where.not(status: "pending").where(conditions).where(receiver_wallet_id: @wallet.try(:id))).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
    elsif params[:trans] == "refund"
      if params[:query].present? && params[:query][:type].present?
        @transactions = Transaction.where("tags ->> 'type' IN(?)",['refund_bank','refund']).where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where("tags ->> 'type' IN(?)",['refund_bank','refund']).where(conditions).where(receiver_wallet_id: @wallet.try(:id))).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
      else
        @transactions = Transaction.where("tags ->> 'type' IN(?)",['refund_bank','refund']).where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where("tags ->> 'type' IN(?)",['refund_bank','refund']).where(conditions).where(receiver_wallet_id: @wallet.try(:id))).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
      end
    elsif params[:trans] == "checks"
      @transactions = Transaction.where("tags ->> 'type' IN (?)",['send_check','Void_Check','bulk_check']).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where("tags ->> 'type' IN (?)",['send_check','Void_Check','bulk_check']).where(conditions).where(receiver_wallet_id: @wallet.try(:id))).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
    elsif params[:trans] == "b2b"
      @transactions = Transaction.where("tags ->> 'type' = 'b2b_transfer'").where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where("tags ->> 'type' = 'b2b_transfer'").where(receiver_wallet_id: @wallet.try(:id))).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
    elsif params[:trans] == "decline"
      if params[:query].present? && params[:query][:gateway].present?
        @transactions = Transaction.joins(:payment_gateway).where(status: "pending").where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.joins(:payment_gateway).where(receiver_wallet_id: @wallet.try(:id))).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
      else
        @transactions = Transaction.where(status: "pending").where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where(status: "pending").where(conditions).where(receiver_wallet_id: @wallet.try(:id))).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
      end
    else
      @transactions = Transaction.where(status: "pending").where(conditions).where(sender_wallet_id: @wallet.try(:id)).or(Transaction.where(status: "pending").where(conditions).where(receiver_wallet_id: @wallet.try(:id))).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
    end
    count=0
    if @transactions.present?
      count = @transactions.try(:count) || 0
    end
    @page_records = 0
    @page_no = 1
    @transactions_count = count
    @seq_dashboard_transaction = []
    @seq_dashboard_transaction << {transactions: @transactions, cursor: @cursor, last_page: @last_page, transactions_count: count, page_records: @page_records}
    @filters = [10,25,50,100]
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    if params[:trans] == "refund"
      @types = [{value:"refund",view:"Refund"},{value:"refund_bank",view:"Refund Bank"}]
    elsif params[:trans] == "checks"
      @types = [{value:"send_check",view:"Send Check"},{value:"Void_Check",view:"Void Check"},{value:"bulk_check",view:"Bulk Check"}]
    else
      @types = [{value:"send_check",view:"Send Check"},{value:"giftcard_purchase",view:"Giftcard Purchase"},{value:"ACH_deposit",view:"ACH Deposit"},{value: TypesEnumLib::TransactionType::BuyRateFee, view: TypesEnumLib::TransactionType::BuyRateFee},{value:"Void_Check",view:"Void Check"},{value:"payed_tip",view:"Paid Tip"},{value:"QR_Scan",view:"Sale"},{value:"sale_tip",view:"Sale Tip"},{value:"sale_giftcard",view:"Sale Giftcard"},{value:"sale_issue_api",view:"Sale Issue"},{value:"debit_charge",view:"PIN Debit"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"virtual_terminal_sale_api",view:"eCommerce"},{value:"credit_card",view:"Credit Card"}, {value: "Service Fee", view: "Monthly Fee"},{value: "Subscription Fee", view: "Subscription Fee"},{value: "Misc Fee", view: "Misc Fee"},{value: "cbk_fee", view: "CBK Fee"},{value: "cbk_hold", view: "CBK Hold"},{value: "cbk_won", view: "CBK Won"},{value: "cbk_lost", view: "CBK Lost"},{value: "cbk_lost_retire", view: "CBK Lost Retire"},{value: "retrievel_fee", view: "Retrieval Fee"},{value: "cbk_dispute_accepted", view: "CBK Dispute Accepted"},{value:"Reserve_Money_Return",view:"Reserve Money Return"}]
    end
    payment_gateways = PaymentGateway.active_gateways
    payment_gateways = payment_gateways.map {|pg| {:value=> pg.key, :view=> pg.name}} if payment_gateways.present?
    @gateways = [{value:TypesEnumLib::GatewayType::Checkbook,view:"Checkbook.io"},{value:TypesEnumLib::GatewayType::Quickcard,view:"Quickcard"},{value:TypesEnumLib::GatewayType::Tango,view:"Tango"},{value:TypesEnumLib::GatewayType::PinGateway,view:"Pin Gateway"},{value:TypesEnumLib::GatewayType::PinGateway,view:"Pin Gateway"}]
    @gateways = @gateways.push(payment_gateways).flatten.uniq if payment_gateways.present?
    render 'admins/shared/specific_wallet_transactions'
  end

  def specfic_decline_transactions
    amount = nil
    sender = nil
    receiver = nil
    @wallet = Wallet.find_by(id: params[:wallet_id])
    @declines = decline_search(params)
    @filters = [10,25,50,100]
    @types = [{value:"Virtual Terminal",view:"Virtual Terminal"},{value:"eCommerce",view:"eCommerce"}]
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    payment_gateways = PaymentGateway.active_gateways
    payment_gateways = payment_gateways.map {|pg| {:value=> pg.key, :view=> pg.name}} if payment_gateways.present?
    @gateways = []
    @gateways = @gateways.push(payment_gateways).flatten.uniq if payment_gateways.present?
    render 'admins/shared/specific_wallet_decline_transactions'
  end

  def profile_location
    @location = Location.find_by(id: params[:location_id])
    @location.ecom_platform = JSON(@location.ecom_platform) != [] ? JSON(@location.ecom_platform) : ''
    @categories = Category.all.order(name: :asc)
    @ecom_platforms = EcommPlatform.all.order(name: :asc)
    respond_to :js
  end

  def post_profile_location
    @location = Location.friendly.find params[:location_id]
    if @location.update(location_profile_params)
      flash[:success] = "Location Profile updated successfully"
      redirect_back(fallback_location: root_path)
    else
      flash[:error] = @location.errors.first
    end
  end

  def apis_keys
    @merchant  = User.friendly.find params[:merchant]
    @location = Location.find_by(id: params[:location_id])
    respond_to :js
  end

  def location_users
    @location = Location.find_by(id: params[:location_id])
    @merchant = User.friendly.find params[:format]
    @location_users = @location.try(:wallets).try(:primary).try(:first).try(:users).unarchived_users.where.not(merchant_id: nil)
  end

  def location_bank_account
    @location = Location.find_by(id: params[:location_id])
    @location.bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@location.bank_account) if @location.bank_account.present?
    @merchant = User.friendly.find params[:format]
    @image = Image.where(location_id: @location.try(:id)).first
  end

  def update_location_bank
    @location = Location.find params[:location_id]
    @merchant = User.friendly.find params[:format]
    params[:user] = params
    params[:user][:location] = params[:location]
    @location.update(edit_location_params)
    save_bank_image if params[:image]['add_image'].present?
    flash[:success] = "Location Bank saved successfully."
    return redirect_to admins_merchants_profile_path(@merchant)
  end

  def employee
    if params[:user_id].present?
      @user = User.find_by(id: params[:user_id])
    else
      @user = User.new
    end
    @merchant  = User.friendly.find params[:merchant_id]
    @location = Location.find params[:location_id]

    respond_to :js
  end

  def post_employee
    begin
      if params[:user_id].present?
        @sub_merchant=User.find params[:user_id]
        existing_locations = @sub_merchant.locations.pluck(:id).map{|c|c.to_s}
        existing_wallets = @sub_merchant.wallets.where.not(location_id: nil)
        coming_location = params[:user][:location_id]
        result = update_sub_merchant(@sub_merchant,params[:user])
        if result.first
          if @sub_merchant.errors.blank?
            if coming_location.present?
              existing_wallets.each do |wallet|
                @sub_merchant.wallets.delete(wallet)
              end
              location = Location.find(coming_location)
              @sub_merchant.wallets << location.wallets.primary.first if location.present?
            end
            flash[:success] = result.second
          else
            flash[:error] = @sub_merchant.errors.full_messages.first
          end
        else
          flash[:error] = result.second
        end
      else
        @sub_merchant = User.new(sub_merchant_params)
        user = User.where(email: @sub_merchant.email).first
        if user.present? && user.archived == false
          flash[:notice] = 'User already exists in the database with this email!'
        else
          @sub_merchant.regenerate_token
          @sub_merchant.is_block = false
          password = params[:user][:password]
          @sub_merchant.merchant!
          if @sub_merchant.errors.blank?
            if params[:user][:location_id].present?
              # params[:user][:wallets].each do |id|
              location=Location.find params[:user][:location_id]
              @sub_merchant.wallets << location.wallets.primary.first
              # end
            end
            flash[:success] = "User is successfully added!"
          else
            flash[:error] = @sub_merchant.errors.first
          end
        end
      end
    rescue => ex
      flash[:error] = ex.message
    end
    redirect_back(fallback_location: root_path)
  end

  def location_cbks
    @location = Location.find_by(id: params[:location_id])
    location_wallet = @location.wallets.primary.pluck(:id)
    @transactions  = Transaction.where(to: location_wallet).or(Transaction.where(to: location_wallet))
    @dispute_case = DisputeCase.includes(:reason).where(merchant_wallet_id: location_wallet)
    disputecase_filter(@dispute_case,params)
  end

  def export_location_cbks
    location = Location.find_by(id: params[:location_id])
    location_wallet = location.wallets.primary.pluck(:id)
    dispute_case = DisputeCase.includes(:reason).where(merchant_wallet_id: location_wallet)
    send_data to_csv(disputecase_filter(dispute_case,params)), filename: "disputecases.csv"
  end

  def to_csv(all)
    CSV.generate do |csv|
      csv << %w{ id Dba\ Name Case\ Number Dispute\ Type Received\ Date Due\ Date Amount Last\ 4 Status}
      all.each do |dispute_case|
        dbaName = dispute_case.merchant_wallet.location.business_name
        if dispute_case.try(:status) == 'won_reversed'
          status = "Won/Reversed"
        elsif dispute_case.try(:status) == 'sent_pending'
          status = "Sent/Pending"
        elsif dispute_case.try(:status) == 'lost'
          status = "Lost"
        elsif dispute_case.try(:status)=='dispute_accepted'
          status = "Dispute Accepted"
        end
        csv << [dispute_case[:id], dbaName, dispute_case.try(:case_number), dispute_case.try(:dispute_type).humanize.titleize, dispute_case[:recieved_date].nil? ? 'NA' : dispute_case[:recieved_date].strftime("%m-%d-%Y"), dispute_case[:due_date].nil? ? 'NA' : dispute_case[:due_date].strftime("%m-%d-%Y"), number_to_currency(number_with_precision(dispute_case.try(:amount).to_f,precision: 2, :delimiter => ',')), dispute_case.try(:card_number), status]

      end
    end
  end

  def disputecase_filter(dispute_case,params)
    if params[:type] == "all" || params[:type].blank?
      params[:type] = "all"
      @dispute_cases = dispute_case
    elsif params[:type] == "sent_pending"
      @dispute_cases = dispute_case.sent_pending
    elsif params[:type] == "won_reversed"
      @dispute_cases = dispute_case.won_reversed
    elsif params[:type] == "lost"
      @dispute_cases = dispute_case.lost
    elsif params[:type] == "dispute_accepted"
      @dispute_cases = dispute_case.dispute_accepted
    end
  end

  def next_baked_section
    @count = params[:count].to_i
    respond_to :js
  end

  def wallet_detail_transactions
    @filters = [10,25,50,100]
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
    @user = User.friendly.find params[:format]
    @primary_wallet = @user.try(:wallets).try(:primary).try(:first)
    @tango_orders = TangoOrder.where(wallet_id: @primary_wallet.id).order('created_at DESC')
    if params[:type].present?
      case params[:type]
      when "achs"
        @achs = @tango_orders.instant_ach
        @achs_count=@achs.count
        @achs=@achs.per_page_kaminari(params[:page]).per(page_size)
      when "gift Card/pre-paid Card"
        @giftcards = @tango_orders.where.not(utid: nil)
        @giftcards_count=@giftcards.count
        @giftcards=@giftcards.per_page_kaminari(params[:page]).per(page_size)
      when "checks"
        @checks = @tango_orders.check
        @checks_count = @checks.count
        @checks = @checks.per_page_kaminari(params[:page]).per(page_size)
      when "B2B (In the system)"
        @requests = Request.where(wallet_id: @primary_wallet.id)
        @requests_count = @requests.count
        @requests = @requests.per_page_kaminari(params[:page]).per(page_size)
      end
      render 'admins/merchants/wallet_detail_transactions'
    else
      redirect_back(fallback_location: root_path)
    end
  end

  def location_setting
    if params[:location_id].present?
      @location = Location.friendly.find params[:location_id]
      @fees = @location.fees.first
    end
  end

  def update_setting
    if params[:format].present?
      @merchant = User.friendly.find params[:format]
    end
    if params[:location_id].present?
      params[:location] = params[:user][:location]
      settings = save_setting
      if settings[:success] == true
        flash[:success] = "Location setting updated successfully"
      else
        flash[:error] = settings[:message]
      end
      return redirect_to admins_merchants_profile_path(@merchant)
    end
  end

  def location_fee
    if params[:location_id].present?
      @location = Location.find params[:location_id]
      @fees  = @location.fees.first
    end
  end

  def update_fee
    if params[:format].present?
      @merchant = User.friendly.find params[:format]
    end
    params[:merchant_id] = @merchant.try(:id)
    fees = save_fee(params[:location][:fee])
    if fees[:success] == true
      flash[:success] = "Location Fee updated successfully"
    else
      flash[:error] = fees[:message]
    end
    return redirect_to admins_merchants_profile_path(@merchant)
  end

  def location_documentation
    if params[:location_id].present?
      @images = Documentation.where(:imageable_id => params[:location_id])
    end
    if params[:format].present?
      @merchant = User.friendly.find params[:format]
      # @images = Documentation.where(:user_id => @merchant.id)
    end
  end

  def update_documentation
    if @merchant.present?
      location = Location.where(merchant_id: @merchant.id).first
    end
    # documentation = save_documentation(params[:user][:documentation]) if params[:user].present?
    if location.present?
      @documents = Documentation.where(imageable_id: location.id)
      @documents.each do |doc|
        if doc.is_deleted === true
          doc.document.destroy
          doc.destroy
        end
      end
    end
    # if documentation.try(:[], "success") == true
    #   flash[:success] = "Location documentation saved successfully."
    # elsif documentation.try(:[], "success") == false
    #   flash[:error] = documentation.try(:[],"message")
    # else
    #   flash[:success] = "Location documentation saved successfully."
    # end
    # return redirect_to admins_merchants_profile_path(@merchant)
  end

  def location_iso_agents
    if params[:location_id].present?
      @location = Location.find params[:location_id]
      if @location.profit_split_detail.present?
        @profit_detail = JSON.parse(@location.profit_split_detail)
      end
      @users = @location.users
      @agent = nil
      @iso = nil
      @affiliate = nil
      @users.each do |u|
        if u.iso?
          @iso = u
        end
        if u.agent?
          @agent = u
        end
        if u.affiliate?
          @affiliate = u
        end
      end
    end
  end

  def update_iso_agents
    iso_agents_data = {}
    iso_agents_data[:baked_iso] = params[:user][:location][:baked_iso]
    iso_agents_data[:profit_split] = params[:user][:location][:profit_split]
    iso_agents_data[:net_profit_split] = params[:user][:location][:net_profit_split]
    iso_agents_data[:sub_merchant_split] = params[:user][:location][:sub_merchant_split]
    iso_agents_data[:ids] = params[:user][:ids]
    profit_splits = params[:profit_splits].to_json
    iso_agents_data[:profit_splits] = JSON profit_splits
    if params[:format].present?
      @merchant = User.friendly.find params[:format]
    end
    iso_agent = save_iso_agent_data(iso_agents_data)
    if iso_agent[:success] == true
      flash[:success] = iso_agent[:message]
    else
      flash[:error] = iso_agent[:message]
    end
    return redirect_to admins_merchants_profile_path(@merchant)
  end

  def testing_buyrate_post
    amount=params[:amount].present? ? params[:amount] : 0
    type=params[:type].present? ? params[:type] : nil
    location=params[:location].present? ? params[:location] : nil
    location_object=location.present? ? location.find_by_id(location) : ''
    fee=location_object.fees.buy_rate.first
    wallet=location_object.wallets.primary.first
    fee_class = Payment::FeeCommission.new(nil, @user,fee,wallet,location_object,type)
    fee_result = fee_class.apply(amount.to_f,type)
    return render status: 200 , json:{:fees => fee_result.present? ? fee_result : ""}
  end

  def testing_buyrate
    wallets=@user.wallets.primary
  end

  def duplicate_owner
    @count = params[:count].to_i
    respond_to do |format|
      format.js
    end
  end

  def document_download
    data = open("https:"+params[:image])
    send_file(data.read, filename: "example", type: data.content_type, x_sendfile:true)
  end

  def document_delete
    @image = Documentation.find params[:image_id]
    @image.is_deleted = true
    @image.save
  end

  def save_merchant_data(merchant_data)
    result = nil
    @merchant = nil
    params[:merchant] = merchant_data
    merchant_data[:is_password_set] = false
    if params[:merchant].present?
      if params[:merchant_data_id].present?
        @merchant = MerchantData.find params[:merchant_data_id]
        merchant_data = @merchant.update(corporate: params[:merchant])
        if merchant_data
          result = {success:true, merchant:@merchant.try(:id)}
        else
          result = {success:false, message:merchant_data.errors.full_messages.first}
        end
      else
        merchant_data = MerchantData.new(corporate: params[:merchant])
        if merchant_data.save
          result = {success:true, merchant:merchant_data.id}
        else
          result = {success:false, message:merchant_data.errors.full_messages.first}
        end
      end
    end
    return result
  end


  def save_owners_data
    result = {}
    if params[:user][:owner].present?
      merchant_data = MerchantData.find params[:merchant_data_id]
      owners_data = merchant_data.update(owners:params[:user][:owner])
      if owners_data
        result = {success:true, message:"Owners saved Successfully"}
      else
        result = {success:false, message:owners_data.errors.full_messages.first}
      end
    end
    return result
  end

  def save_location_data
    result = {}
    if params[:user][:location].present?
      params[:user][:location][:phone_number] = params[:phone_code] + params[:user][:location][:phone_number]
      params[:user][:location][:cs_number] = params[:cs_number_code] + params[:user][:location][:cs_number]
      merchant_data = MerchantData.find params[:merchant_data_id]
      location_data = merchant_data.update( location: params[:user][:location])
      if location_data
        result = {success:true, message:"Location saved Successfully"}
      else
        result = {success:false, message:location_data.errors.full_messages.first}
      end
    end
    return result
  end

  def save_iso_agents_data
    iso_agent_data = {}
    result = {}
    iso_agent_data[:iso] = params[:iso]
    iso_agent_data[:agent] = params[:agent]
    iso_agent_data[:affiliate] = params[:affiliate]
    iso_agent_data[:ids] = params[:user][:ids]
    iso_agent_data[:baked_iso] = params[:user][:location][:baked_iso]
    iso_agent_data[:profit_split] = params[:user][:location][:profit_split]
    iso_agent_data[:net_profit_split] = params[:user][:location][:net_profit_split]
    iso_agent_data[:sub_merchant_split] = params[:user][:location][:sub_merchant_split]
    iso_agent_data[:profit_splits] = params[:profit_splits]
    if params[:user][:location].present?
      merchant_data = MerchantData.find params[:merchant_data_id]
      iso_data = merchant_data.update( iso_agent: iso_agent_data)
      if iso_data
        result = {success:true, message:"Location saved Successfully"}
      else
        result = {success:false, message:iso_data.errors.full_messages.first}
      end
    end
    return result
  end

  def save_fee_data
    result = {}
    if params[:user][:location].present?
      merchant_data = MerchantData.find params[:merchant_data_id]
      ach_international = (params["ach_international"].present? && params["ach_international"] == "true") ? true : false
      merchant_data.fees = params[:user][:location][:fee]
      merchant_data.location["ach_international"] = ach_international
      fee_data = merchant_data.save
      if fee_data
        result = {success:true, message:"Fee saved Successfully"}
      else
        result = {success:false, message:fee_data.errors.full_messages.first}
      end
    end
    return result
  end

  def save_bank_account
    result = {}
    if params[:user][:location].present?
      merchant_data = MerchantData.find params[:merchant_data_id]
      params[:user][:location][:bank].each do |key, bank|
        if bank['image'].present?
          image = {add_image: bank['image'], bank_detail_id: params[:merchant_data_id]}
          id = upload_docs(image, nil)
          params[:user][:location][:bank][key]["saved_image_id"] = id
        end
      end
      bank_data = merchant_data.update( bank_details: params[:user][:location][:bank]) if params[:user][:location][:bank].present?
      if bank_data
        result = {success:true, message:"Bank Account saved Successfully"}
      else
        result = {success:false, message:bank_data.errors.full_messages.first}
      end
    end
    return result
  end

  def save_setting_data
    result = {}
    if params[:user][:location].present?
      merchant_data = MerchantData.find params[:merchant_data_id]
      #these three line below (if user set the value = 0.00 then these will set them nil )
      params[:user][:location][:sale_limit] = params[:user][:location][:sale_limit].present? ? params[:user][:location][:sale_limit].to_f > 0 ? params[:user][:location][:sale_limit] : nil : nil
      params[:user][:location][:monthly_processing_volume] = params[:user][:location][:monthly_processing_volume].present? ? params[:user][:location][:monthly_processing_volume].to_f > 0 ? params[:user][:location][:monthly_processing_volume] : nil : nil
      params[:user][:location][:average_ticket] = params[:user][:location][:average_ticket].present? ? params[:user][:location][:average_ticket].to_f > 0 ? params[:user][:location][:average_ticket] : nil : nil
      setting_data = merchant_data.update( setting: params[:user][:location])
      if setting_data
        result = {success:true, message:"Fee saved Successfully"}
      else
        result = {success:false, message:setting_data.errors.full_messages.first}
      end
    end
    return result
  end

  def save_all_data(id)
    result = {}
    if id.present?
      form_data = MerchantData.find id
      @merchant = save_merchant(form_data.corporate)
      if @merchant[:success]
        result = {success: true, message:"Merchant saved successfully"}
        params[:merchant_id] = @merchant[:merchant_id]
        owners = save_owners(form_data.owners)
        if owners[:success]
          result = {success: true, message:"Owners saved successfully"}
          @location = save_location(form_data.location)
          if @location[:success]
            result = {success: true, message:"Location saved successfully"}
            params[:location_id] = @location[:location_id]
            iso_agent = save_iso_agent_data(form_data.iso_agent)
            if iso_agent[:success]
              result = {success: true, message:"Iso_Agent saved successfully"}
              fees = save_fee(form_data.fees)
              if fees[:success]
                result = {success: true, message:"Iso_Agent saved successfully"}
                settings = save_setting(form_data.setting)
                bank_account = save_bank_details(form_data.bank_details)
                if settings[:success]
                  result = {success: true, message:"Settings saved successfully"}
                  if bank_account[:success]
                    result = {success: true, message:"Bank Account saved successfully"}
                  else
                    result = {success: false, message:bank_account[:message]}
                  end
                else
                  result = {success: false, message:settings[:message]}
                end
              else
                result = {success: false, message:fees[:message]}
              end
            else
              result = {success: false, message:iso_agent[:message]}
            end
          else
            result = {success: false, message:@location[:message]}
          end
          merchant_location = Location.where(id: @location[:location_id]).try(:first) if @location.try(:[], :location_id).present?
          iso_wallet = merchant_location.try(:users).try(:iso).try(:first).try(:wallets).try(:first).try(:id)
          agent_wallet = merchant_location.try(:users).try(:agent).try(:first).try(:wallets).try(:first).try(:id)
          affiliate_wallet = merchant_location.try(:users).try(:affiliate).try(:first).try(:wallets).try(:first).try(:id)
          TransactionBatch.create(merchant_wallet_id: merchant_location.try(:wallets).try(:primary).try(:first).try(:id), iso_wallet_id: iso_wallet, agent_wallet_id: agent_wallet, affiliate_wallet_id: affiliate_wallet, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: "sale")
        else
          result = {success: false, message:owners[:message]}
        end
      else
        result = {success: false, message:@merchant[:message]}
      end
    end
    return result
  end

  def update_assigned_location_data(merchant, ez_data, is_assigned)
    if is_assigned
      if ez_data.present?
        LocationsUsers.where(user_id: merchant.id, relation_type: "secondary").delete_all
        ez_data.each do |key, value|
          if value.values.last["name"].present? && value.values.last["wallet"].present?
            locat = Location.find_by(id: value["ez_merchant_1"]["wallet"])   ## Why User? We've LocationID here
            if value["ez_merchant_1"].present?
              LocationsUsers.create(user_id: merchant.id, location_id: locat.id, attach: true).secondary!
            end
          end
        end
        merchant.update(assign_location: true, assigned_location_detail: ez_data.to_json)
      end
    else
      if ez_data.present?
        ez_data.each do |key, value|
          if value.values.last["name"].present? && value.values.last["wallet"].present?
            locat = Location.find_by(id: value["ez_merchant_1"]["wallet"])   ## Why User? We've LocationID here
            user = locat.merchant
            if user.present?
              LocationsUsers.where(user_id: merchant.id, location_id: locat.id, relation_type: "secondary").delete_all
            end
          end
        end
        merchant.update(assign_location: false, assigned_location_detail: nil)
      end
    end
  end

  def get_sub_merchant_data(merchant)
    ez_merchant = JSON.parse(merchant.try(:assigned_location_detail)) if merchant.try(:assigned_location_detail).present?
    edit_ez_data = []
    if ez_merchant.present?
      ez_merchant.each do |key, value|
        if value.values.last["name"].present? && value.values.last["wallet"].present?
          ez_data = {}
          ez_location = Location.where(id: value["ez_merchant_1"]["wallet"]).select(:id, :merchant_id, :business_name).first
          ez_data[:ez_merchant_id] = ez_location.merchant.try(:id)
          ez_data[:ez_merchant_name] = value["ez_merchant_1"]["name"]
          ez_data[:ez_merchant_locations] = ez_location.merchant.my_locations.pluck(:id, :business_name)
          ez_data[:ez_location_id] = ez_location.id
        end
        edit_ez_data.push(ez_data)
      end
    end
    edit_ez_data
  end


  def new_sub_merchant
    id = params[:id] if params[:id].present?
    @merchant = User.find(id) if id.present?
    if @merchant.merchant_id.present?
      @wallets = Location.where(id: @merchant.wallets.primary.pluck(:location_id).compact)
      @permission = Permission.where(merchant_id: [@merchant.merchant_id, nil])
    else
      @wallets = Location.where(id: @merchant.wallets.primary.pluck(:location_id).compact)
      @permission = Permission.where(merchant_id: [@merchant.id, nil])
    end
    @user= User.new
    render partial: "new_sub_merchant"
  end
  def create_sub_merchant
    @user = User.new
    begin
      params[:user][:name] = params[:user][:first_name]
      params[:user][:first_name].strip!
      params[:user][:last_name] = params[:user][:last_name]
      params[:user][:first_name] = params[:user][:first_name].partition(" ").first
      params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
      params[:user][:permission_id] = params[:user][:permission_id].first
      params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
      @sub_merchant = User.new(sub_merchant_params_new)
      @sub_merchant.merchant_id = params[:user][:merchant_id].to_i
      mail=params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
      user = User.where.not(role:'user').where(email: mail ).first
      if user.present? && user.archived == false
        flash[:notice] = I18n.t('merchant.controller.used_email')
      elsif user.present?
        user.archived = false
        if user.update(sub_merchant_params_new)
          flash[:success] = I18n.t 'merchant.controller.successful_user_add'
        else
          flash[:error] = user.errors.first
        end
      else
        @sub_merchant.regenerate_token
        @sub_merchant.is_block = false
        #@sub_merchant.submerchant_type = params[:submerchant_type]
        password = params[:user][:password]
        @sub_merchant.merchant!
        if @sub_merchant.errors.blank?
          if params[:user][:wallets]
            params[:user][:wallets].each do |id|
              location = Location.find id
              @sub_merchant.wallets << location.wallets.primary.first
            end
          end
          flash[:success] = I18n.t 'merchant.controller.successful_user_add'
        else
          flash[:error] = @sub_merchant.errors.first
        end
      end
        # ActivityLog.log(nil,"User Added",current_user,params,response,"Added Employee User #{params[:user][:name]} #{params[:user][:last_name]}","employees")
    rescue => ex
      flash[:error] = ex.message
    end
    redirect_back(fallback_location: root_path)
  end

  def edit_sub_merchant
    @sub_merchant = User.find(params[:id])
    @merchant = User.find @sub_merchant.merchant_id
    if @merchant.merchant_id.present?
      @wallets = Location.where(id: @merchant.wallets.primary.pluck(:location_id).compact)
    else
      @wallets = Location.where(id: @merchant.wallets.primary.pluck(:location_id).compact)
    end
    @permission = Permission.where(merchant_id: [@merchant.id, nil])
    render partial: "edit_sub_merchant"
  end

  def update_user_sub_merchant
    id = params[:user][:merchant_id].to_i if params[:user][:merchant_id].present?
    @sub_merchant= User.find(id)
    name=@sub_merchant.try(:name)
    ids=@sub_merchant.wallets.pluck(:id)
    params[:user][:name] = params[:user][:name]
    params[:user][:name].strip!
    params[:user][:first_name] = params[:user][:name].partition(" ").first
    # params[:user][:last_name] = params[:user][:name].partition(" ").last
    params[:user][:last_name] = params[:user][:last_name]
    params[:user][:permission_id] = params[:user][:permission_id].first
    existing_wallets = @sub_merchant.wallets.where.not(location_id: nil)
    coming_locations = params[:user][:wallets]
    params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
    result = update_sub_merchant_user(@sub_merchant,params[:user],params[:user][:merchant_id].to_i)
    # save_activity_logs(ids,name)
    if result.first
      if @sub_merchant.errors.blank?
        if coming_locations.present?
          existing_wallets.each do |wallet|
            @sub_merchant.wallets.delete(wallet)
          end
          # @sub_merchant.wallets.where.not(location_id: nil).destroy(existing_wallets) if @sub_merchant.wallets.present?
          coming_locations.each do |id|
            location = Location.find(id)
            @sub_merchant.wallets << location.wallets.primary.first if location.present?
          end
        end
        new_ids = @sub_merchant.wallets.pluck(:id)
        wallet_changes = ids - new_ids
        save_activity_logs(ids,name) if @sub_merchant.attributes_changed? || wallet_changes.present?
        flash[:success] = result.second
      else
        flash[:error] = @sub_merchant.errors.full_messages.first
      end
    else
      flash[:error] = result.second
    end
    redirect_back(fallback_location: root_path)
  end

  def update_sub_merchant_user(submerchant,params,sub_id)
    begin
      user = User.find_by(email: params["email"]) if params["email"].present? && params["email"] != submerchant.email
      if submerchant.email.present? && params["email"].present? && submerchant.email==params["email"] && params["password"].blank? && submerchant.name==params['name'] && submerchant.first_name ==params['first_name'] && submerchant.last_name==params['last_name'] && submerchant.permission_id == params['permission_id']
        return  [true,"Updated Successfully"]
      elsif submerchant.email.present? && params["email"].present? && submerchant.email!=params["email"] && submerchant.email.downcase==params["email"].downcase && params["password"].blank? && submerchant.name==params['name'] && submerchant.first_name ==params['first_name'] && submerchant.last_name==params['last_name'] && submerchant.permission_id == params['permission_id'].to_i && user.id==sub_id
        return  [false,"You can not edit the same email"]
      elsif user.present?
        return  [false,"A user with this email already exists"]
      else
        submerchant.email = params["email"]; submerchant.name = params["name"]; submerchant.phone_number = params["phone_number"]; submerchant.first_name = params["first_name"]; submerchant.last_name = params["last_name"]; submerchant.permission_id = params["permission_id"]
        if params["password"].blank?
          if submerchant.save
            return  [true,"Updated Successfully"]
          else
            return [false,submerchant.errors.full_messages.first]
          end
        else
          submerchant.password = params["password"]
          if submerchant.save
            return [true,"Updated Successfully"]
          else
            return [false,submerchant.errors.full_messages.first]
          end
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      return [false,invalid]
    end
  end
  def search_sub_merchant(params)

    conditions = []
    parameters = []

    if params[:query]["name"].present?
      conditions << "(parent_merchants_users.name ILIKE ?)"
      parameters << "#{params[:query]["name"].downcase}%".to_s
    end

    if params[:query]["phone_number"].present?
      conditions << "(users.phone_number ILIKE ?)"
      parameters << "#{params[:query]["phone_number"].downcase}%".to_s
    end

    if params[:query]["sub_merchant_id"].present?
      conditions << "(users.id = ?)"
      parameters << params[:query]["sub_merchant_id"].to_i
    end

    if params[:query]["email"].present?
      conditions << "(users.email ILIKE ?)"
      parameters << "#{params[:query]["email"].downcase}".to_s
    end

    if params[:query]["sub_merchant_name"].present?
      conditions << "(users.name ILIKE ?)"
      parameters << "%#{params[:query]["sub_merchant_name"].downcase}%".to_s
    end

    if params[:query]["mer_id"].present?
      conditions << "(users.merchant_id = ?)"
      parameters << params[:query]["mer_id"].to_i
    end

    conditions = [conditions.join(" AND "), *parameters] if conditions.present? && parameters.present?
  end

  def destroy_sub_merchant
    @sub_merchant = User.where(id: params[:id]).first
    if @sub_merchant.archived == false
      @sub_merchant.update(archived: true,permission_id: nil)
    else
      @sub_merchant.update(archived: false)
    end
    flash[:notice] = "Successfully Deleted."
    redirect_back(fallback_location: root_path)
  end

  def country_data
    if params[:country].present?
      if params[:owner_field].present?
        if params[:country] == "United States"
          country_code = "us"
        else
          country = ISO3166::Country.find_country_by_name(params[:country])
          country_code = country.alpha2 if country.present?
        end
      else
        country_code = params[:country]
      end
      @states = country_code.present? ? CS.states(country_code) : ""
      if request.xhr?
        respond_to do |format|
          format.json {
            render json: {states: @states}
          }
        end
      end
    end
  end


  #------------------------------------end Sub-merchant-user

  # ---------------------End Testing Merchant BuyRate---------------------------------------
  private

  def save_activity_logs(ids,name)
    changes=[]
    new_ids=@sub_merchant.wallets.pluck(:id)
    wallet_changes=ids-new_ids
    wallet_changes=wallet_changes.present? ? wallet_changes :  new_ids-ids
    user_audit=@sub_merchant.audits.where(created_at: (Time.now-50)..Time.now) if @sub_merchant.try(:audits).present?
    if user_audit.present?
      changes=user_audit.try(:first).try(:audited_changes)
      # changes=changes.map{|a| a.try(:humanize)}
      new_hash={}
      changes.to_hash.each_pair do |k,v|
        new_hash.merge!({k.humanize => v})
      end
      if new_hash['Permission'].present?
        permission=Permission.where(id:new_hash['Permission'].last).first
        new_hash['Permission']=permission.try(:name)
      end
      changes=new_hash
    end
    if changes.present? && wallet_changes.present?
      wallet=Wallet.where(id:wallet_changes).pluck(:name)
      changes=changes.merge({'Account Access'=>wallet.try("to_sentence")})
    elsif wallet_changes.present?
      wallet=Wallet.where(id:wallet_changes).pluck(:name)
      changes={'Account Access'=>wallet.try("to_sentence")}
    end
    if changes.present?
      changes.to_hash.each_pair do |k,v|
        if k=='Permission' || k=='Account Access'
          ActivityLog.log(nil,"User Update",current_user,params,response,"Modified #{name}'s #{k}  #{v}","employees")
        elsif k=='Encrypted password'
          ActivityLog.log(nil," Changed password",current_user,params,response," Changed password for #{name}","employees")
        else
          ActivityLog.log(nil,"User Update",current_user,params,response,"Changed #{name}'s #{k} from #{v.try(:first)} to #{v.try(:last)}","employees")
        end
      end
    end
  end

  def new_merchant_params
    if params[:merchant][:profile_attributes][:business_type].present?
      params[:merchant][:profile_attributes][:business_type] = params[:merchant][:profile_attributes][:business_type].to_i
    end
    if params[:merchant][:risk].present?
      params[:merchant][:risk] = params[:risk].to_i
    end
    if params[:merchant][:two_step_verification].present? && params[:merchant][:two_step_verification] == "true"
      params[:merchant][:two_step_verification] = true
    elsif params[:merchant][:two_step_verification].present? && params[:merchant][:two_step_verification] == "false"
      params[:merchant][:two_step_verification] = false
    end
    params.require(:merchant).permit(:issue_fee,:email,:is_password_set,:two_step_verification,:risk, :password, :password_confirmation, :name ,:address, :owner_name  , :phone_number , :status,  :company_id,:primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id, :first_name, :last_name, profile_attributes: [:business_type,:ein ,:business_location ,:street_address, :country, :city,:state, :zip_code, :tax_id, :years_in_business])
  end

  def edit_merchant_params
    if params[:merchant][:profile_attributes].present?
      if params[:merchant][:profile_attributes][:business_type].present?
        params[:merchant][:profile_attributes][:business_type] = params[:merchant][:profile_attributes][:business_type].to_i
      end
    end
    if params[:merchant][:risk].present?
      params[:merchant][:risk] = params[:merchant][:risk].to_i
    end
    if params[:merchant][:two_step_verification].present? && params[:merchant][:two_step_verification] == "true"
      params[:merchant][:two_step_verification] = true
    elsif params[:merchant][:two_step_verification].present? && params[:merchant][:two_step_verification] == "false"
      params[:merchant][:two_step_verification] = false
    end
    params.require(:merchant).permit(:issue_fee,:email, :risk,:two_step_verification,:password, :password_confirmation, :name ,:address, :owner_name  , :phone_number , :status,  :company_id, :primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id, :first_name, :last_name, profile_attributes: [:id, :business_type, :ein ,:business_location ,:street_address, :country, :city,:state, :zip_code, :tax_id, :years_in_business])
  end

  def new_location_params
    # params[:location][:fees_attributes].each do |p,k|
    #   k[:fee_status] = k[:fee_status].to_i
    # end
    params[:location][:risk] = params[:location][:risk].to_i
    params.require(:location).permit(:merchant_id, :phone_number, :tax_id, :social_security_no,:zip, :category_id, :ecomm_platform_id,:country, :state, :city, :years_in_business, :business_name, :first_name, :last_name, :hold_in_rear, :hold_in_rear_days,:processing_type,:on_hold_pending_delivery,:on_hold_pending_delivery_days,:cs_number,:net_profit_split,:bank_routing, :bank_account,:bank_account_type, :bank_account_name,
                                     :email ,:business_type, :address,:sale_limit, :sale_limit_percentage, :ledger, :block_withdrawal,:block_ip,:block_giftcard,:sales,:virtual_terminal,:close_batch,:disable_sales_transaction,:profit_split,:profit_split_detail, :baked_iso, :primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id, :risk,:block_api,:virtual_transaction_api,:virtual_terminal_transaction_api,:virtual_debit_api,:bank_routing, :bank_account,:bank_account_type, :email_receipt, :sms_receipt,
                                     :web_site, :low_ticket, :average_ticket, :high_ticket, :monthly_processing_volume, :over_high, :extra_over, :sub_merchant_split, :ach_limit, :ach_limit_type, :push_to_card_limit, :push_to_card_limit_type, :check_limit, :check_limit_type,:standalone, :eaze_reporting,:vip_card, :apply_load_balancer, :load_balancer_id, :transaction_limit , transaction_limit_offset: [],
                                     fees_attributes: [:risk,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :gbox, :partner, :iso, :agent, :fee_id, :fee_status, :location_id, :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement, :charge_back_percent, :charge_back_count, :refund_percent, :refund_count, :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee, :generate_label_fee, :tracking_fee, :qr_refund,:qr_refund_percentage, :debit_unredeemed_fee, :debit_unredeemed_perc_fee, :credit_unredeemed_fee, :credit_unredeemed_perc_fee, :load_fee_credit, :load_fee_percent_credit, :load_fee_debit, :load_fee_percent_debit, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee])
  end

  def edit_location_params
    params[:location][:risk] = params[:location][:risk].to_i if params[:location][:risk].present?
    params.require(:location).permit(:merchant_id,:phone_number,:tax_id,:social_security_no,:zip,:category_id,:ecomm_platform_id, :country, :state,:city, :years_in_business,:business_name, :first_name, :last_name,:profit_split, :profit_split_detail, :baked_iso,:block_api,:virtual_transaction_api,:virtual_terminal_transaction_api,:virtual_debit_api,:bank_account, :bank_routing,:bank_account_name,:bank_account_type,:cs_number,:net_profit_split,:sub_merchant_split,
                                     :email ,:business_type, :address,:sale_limit, :sale_limit_percentage, :ledger, :block_withdrawal,:block_ip,:block_giftcard,:sales,:virtual_terminal,:close_batch,:disable_sales_transaction, :hold_in_rear, :hold_in_rear_days,:processing_type,:on_hold_pending_delivery,:on_hold_pending_delivery_days,:primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id, :risk, :email_receipt, :sms_receipt,:block_ach,:standalone,:push_to_card,:standalone_type,:ach_limit,:push_to_card_limit,:check_limit,:ach_limit_type,:push_to_card_limit_type,:check_limit_type, :vip_card, :apply_load_balancer, :load_balancer_id , :transaction_limit,transaction_limit_offset: [])
  end

  def location_profile_params
    params.permit(:phone_number, :tax_id, :social_security_no,:zip, :category_id, :ecomm_platform_id, :country, :state, :city, :years_in_business, :business_name, :first_name, :last_name, :hold_in_rear, :hold_in_rear_days,:processing_type,:on_hold_pending_delivery,:on_hold_pending_delivery_days,:cs_number,:net_profit_split,
                  :email ,:business_type, :address,:sale_limit, :sale_limit_percentage, :ledger, :block_withdrawal,:block_ip,:block_giftcard,:sales,:virtual_terminal,:close_batch,:disable_sales_transaction,:profit_split,:profit_split_detail, :baked_iso, :primary_gateway_id, :secondary_gateway_id, :ternary_gateway_id, :risk,:block_api,:virtual_transaction_api,:virtual_terminal_transaction_api,:virtual_debit_api,:bank_routing, :bank_account,:bank_account_type, :email_receipt, :sms_receipt,
                  :web_site, :low_ticket, :average_ticket, :high_ticket, :monthly_processing_volume, :over_high, :extra_over, :vip_card, :apply_load_balancer, :load_balancer_id)
  end

  def new_fee_params
    params[:fee][:service_date]  = Date.strptime(params[:fee][:service_date], '%m/%d/%Y') if params[:fee][:service_date].present?
    params[:fee][:statement_date]  = Date.strptime(params[:fee][:statement_date], '%m/%d/%Y') if params[:fee][:statement_date].present?
    params[:fee][:misc_date]  = Date.strptime(params[:fee][:misc_date], '%m/%d/%Y') if params[:fee][:misc_date].present?
    params.require(:fee).permit(:risk,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,
                                :transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar,:b2b_fee_dollar, :b2b_fee, :redeem_fee, :send_check,
                                :add_money_check, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee_dollar, :ach_fee, :gbox, :partner, :iso, :agent, :fee_id,
                                :fee_status, :location_id, :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee,:refund_fee, :service,
                                :misc, :statement, :service_date, :misc_date, :statement_date, :charge_back_percent, :charge_back_count, :refund_percent,
                                :refund_count, :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee,:generate_label_fee, :tracking_fee,:qr_refund, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee,
                                :qr_refund_percentage, :debit_unredeemed_fee, :debit_unredeemed_perc_fee, :credit_unredeemed_fee, :credit_unredeemed_perc_fee, :load_fee_credit, :load_fee_percent_credit, :load_fee_debit, :load_fee_percent_debit)

  end

  def new_owner_params
    params.require(:owner).permit(:user_id, :name,:title, :ownership, :email, :address, :country, :city, :state, :phone_number, :zip_code, :ssn, :date_of_birth, :dl, :dl_state_issue)
  end

  def new_documentation_params
    params.require(:documentation).except(:owner_id_card).permit(:document, :type, :user_id)
  end

  def edit_profile_params
    params[:merchant].require(:profile_attributes).permit(:company_name,:business_type, :ein ,:business_location ,:street_address, :country, :city,:state, :zip_code, :tax_id, :years_in_business)
  end

  def edit_owner_params
    params.require(:owner).permit(:owner,:name,:title, :ownership ,:address, :country, :city,:state, :email, :ssn, :date_of_birth,:dl_state_issue)

  end

  def finish_wizard_path
    @merchant = User.where(id: params[:merchant_id]).first if params[:merchant_id].present?
    @slug = @merchant.slug if @merchant.present?
    # if params[:status].present? && (params[:status] == "new" || params[:status] == "new1")
    #   @merchant.update(is_profile_completed: true)
    # end
    if params[:status].present? && params[:status] == "old-update"
      merchants_path(archive:params[:archive])
    else
      if params[:status] == "c1"
        admins_merchants_profile_path(@slug, message:"Merchant updated successfully")
      elsif params[:status] == "l1" && params[:loc] != "duplicate"
        admins_merchants_profile_path(@slug, message: "Location saved successfully")
      elsif params[:status] == "l1" && params[:loc] == "duplicate"
        admins_merchants_profile_path(@slug, message: "Location duplicated successfully")
      else
        flash[:success] = "Merchant saved successfully"
        merchants_path
      end
    end
  end

  def sub_merchant_params
    params.require(:user).permit(:name, :first_name, :last_name, :email, :phone_number, :merchant_id, :password, :password_confirmation, :retail_sales, :archived, :submerchant_type)
  end

  def sub_merchant_params_new
    params.require(:user).permit(:name, :first_name, :last_name, :email, :phone_number,:merchant_id, :password, :password_confirmation, :retail_sales, :archived, :submerchant_type, :permission_id)
  end

  def update_sub_merchant(submerchant,params)
    begin
      submerchant.email = params["email"]; submerchant.name = params["name"]; submerchant.phone_number = params["phone_number"]; submerchant.last_name = params["last_name"]; submerchant.submerchant_type = params["submerchant_type"]
      if params["password"].blank?
        return [submerchant.save(:validate => false), "Updated Successfully"]
      else
        submerchant.password = params["password"]
        if submerchant.save
          return [true,"Updated Successfully"]
        else
          return [false,submerchant.errors.full_messages.first]
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      return [false,invalid]
    end
  end

  private
  def set_steps
    if params[:status] == "c1"
      self.steps = [:corporate, :owners]
    elsif params[:status] == "l1"
      self.steps = [:location, :iso_agent, :fee, :setting, :documentation]
    elsif params[:action] == "update" && params[:id] == "update"
      self.steps = [:update]
    else
      self.steps = [:corporate, :owners, :location, :iso_agent, :fee, :setting, :bank_account, :documentation]
    end

  end

end
