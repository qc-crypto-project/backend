class Admins::MessagesController < AdminsController
  def index
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = chat_search_query(params[:query],params[:offset])
    end
    #@chats = Message.all.group_by(&:transaction_id)
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @orders = Transaction.joins(:messages).where(conditions).distinct.order('id DESC').per_page_kaminari(params[:page]).per(page_size).per(page_size)
  end

  def show
    if params[:last_msg_id]#for json response code
      @chat = Message.where(transaction_id: params[:id]).order(id: :desc).where('id < ?', params[:last_msg_id]).limit(10)

      @chat= {params[:id] => @chat}
      @messages = @chat.values.flatten
      #for attachments
      next_item = -1
      @messages.each_with_index do |msg,index|
        if index <= next_item
          next
        end
        if msg.try(:attachments).present?
          msg.attachments.each_with_index do |attach,i|
            doc = attach
            doc.document_content_type = attach.document.url
            doc.document_file_size = msg.recipient_id
            @messages.insert(index,doc)
            next_item = index + i + 1
          end
        end
      end
    else
      @chat = Message.where(transaction_id: params[:id]).order(id: :desc).limit(10)

      @chat= {params[:id] => @chat}
      @messages = @chat.values.flatten
      @messages = @messages.sort_by &:created_at
    end

    respond_to do |format|
      format.html { render partial: "show" }
      format.json { render json: @messages }
    end
      # render partial: 'show'
  end
end
