class Admins::TicketsController < AdminsController

  skip_before_action :check_admin

  def index
    if params[:q].present?
      params[:q_value] = params[:q] if params[:q_value].blank?
      params[:q] = {"title_or_status_or_user_name_cont" => params[:q]}
      params[:q]["id_eq"] = params[:q_value]
      params[:q]["user_id_eq"] = params[:q_value]
      @tickets = Ticket.ransack(params[:q].try(:merge, m: 'or'))
      @tickets = @tickets.result
      if params[:ticket] == "flagged"
        @tickets = @tickets.where(device_type: nil).where.not(user_id: nil,flag: "no_flag",status:'closed').includes(:user).order(updated_at: :desc)
      elsif params[:ticket].present? && params[:ticket] == "untreated"
        @tickets = @tickets.where(device_type: nil, status: params[:ticket],flag: "no_flag").where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      elsif params[:ticket].present? && params[:ticket] == "closed"
        @tickets = Ticket.where(device_type: nil, status: params[:ticket]).where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      elsif params[:ticket].present? && params[:ticket] == "processed"
        @tickets = Ticket.where(device_type: nil, status: params[:ticket]).where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      else
        @tickets = @tickets.where(device_type: nil,flag: "no_flag").where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      end
    else
      if params[:ticket] == "flagged"
        @tickets = Ticket.where(device_type: nil).where.not(user_id: nil,flag: "no_flag",status:'closed').includes(:user).order(updated_at: :desc)
      elsif params[:ticket].present? && params[:ticket] == "untreated"
        @tickets = Ticket.where(device_type: nil, status: params[:ticket],flag: "no_flag").where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      elsif params[:ticket].present? && params[:ticket] == "closed"
        @tickets = Ticket.where(device_type: nil, status: params[:ticket]).where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      elsif params[:ticket].present? && params[:ticket] == "processed"
        @tickets = Ticket.where(device_type: nil, status: params[:ticket]).where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      else
        @tickets = Ticket.where(device_type: nil,flag: "no_flag").where.not(user_id: nil).includes(:user).order(updated_at: :desc)
      end
    end
    @tickets = Kaminari.paginate_array(@tickets).page(params[:page]).per(params[:filter] || 10)
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js
      format.html
    end
    # @all_tickets = @tickets.select{|all| all.flag == "no_flag"}
    # @tickets_processed = @tickets.select{|all| all.flag == "no_flag" && all.status == "processed" }
    # @tickets_flagged = @tickets.select{|all| all.flag != "no_flag" }
    # @tickets_untreated = @tickets.select{|all| all.flag == "no_flag" && all.status == "untreated" }
    # @tickets_closed = @tickets.select{|all| all.flag == "no_flag" && all.status == "closed" }
  end

  def edit
    @ticket = Ticket.friendly.find(params[:id])
    @comment = @ticket.comments.build
    @comments = @ticket.comments.includes(:user).order(created_at: :asc)
    Ticket.friendly.find(params[:id]).update(:get_attention => false)
  end

  def change_flag
    ticket = Ticket.find_by(id: params[:id])
    if ticket.present? && ticket.update(flag: params[:flag])
      if params[:from_edit].present?
        flash[:notice] = "Update Successfully!"
        redirect_to edit_ticket_path(id: ticket.id)
      else
         # render status: 200, json:{success: 'Updated'}
          redirect_to controller: 'tickets', tab: 'flagged'
      end
    else
      if params[:from_edit].present?
        flash[:notice] = "Something went wrong!"
        redirect_to edit_ticket_path(id: ticket.id)
      else
        render status: 404, json:{error: 'Something went wrong'}
      end
    end
  end

  def assign_to
    ticket = Ticket.find_by(id: params[:id])
    if ticket.present?
      status = params[:assigned_to].to_i
      ticket.update(assigned_to: status)
    end
  end

  def close_ticket
    @ticket = Ticket.friendly.find(params[:ticket_id])
    @ticket.update(status: "closed", get_attention: false)
    flash[:notice] = "Closed" 
    if params[:redirect_to].present?
      if params[:redirect_to][:make] == "admin_user"
            redirect_to user_transactions_path(params[:redirect_to][:user_id],tab:'menu_3')
      elsif params[:redirect_to][:make] == "location_wallet"
            redirect_to wallet_transactions_locations_path(params[:redirect_to][:user_id],tab:'menu_3')
      else
        redirect_to tickets_path
      end
    else
      redirect_to tickets_path
    end
  end

  def mail_support
    @tickets = Ticket.where.not(device_type: nil)
  end

end
