class Admins::AchGatewaysController < AdminsController
  before_action :check_admin
  before_action :strip_params, only: [:check_gateway_existance, :create, :update]
  before_action :set_admins_ach_gateway, only: [:show, :edit, :update, :destroy, :archive, :block_ach_gateways]

  # GET /admins/ach_gateways
  # GET /admins/ach_gateways.json
  def index
    @admins_ach_gateways = AchGateway.unarchived_gateways.ach
  end

  def archived_ach_gateways
    @admins_ach_gateways = AchGateway.archive_gateways.ach
  end

  def archived_push_to_card_gateways
    @admins_ach_gateways = AchGateway.archive_gateways.push_to_card
  end

  def push_to_card
    @admins_ach_gateways = AchGateway.unarchived_gateways.push_to_card
  end

  def archived_check_gateways
    @admins_ach_gateways = AchGateway.archive_gateways.check
  end

  def check
    @admins_ach_gateways = AchGateway.unarchived_gateways.check
  end

  # GET /admins/ach_gateways/1
  # GET /admins/ach_gateways/1.json
  def show
  end

  # GET /admins/ach_gateways/new
  def new
    @admins_ach_gateway = AchGateway.new(gateway_type: params[:gateway_type])
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /admins/ach_gateways/1/edit
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  def check_gateway_existance
    if params[:name].present?
      ach = AchGateway.where(gateway_type: params[:type], archive: false).where("bank_name ILIKE ?", params[:name] )
    elsif params[:descriptor].present?
      ach = AchGateway.where(gateway_type: params[:type], archive: false).where("descriptor ILIKE ?", params[:descriptor])
    end
    if ach.present?
      if params[:gateway_id].present? && params[:gateway_id] == ach.first.id.to_s
        render status: 404, json:{error: 'error'}
      else
        render status: 200, json:{success: 'success'}
      end
    else
      render status: 404, json:{error: 'error'}
    end
  end

  def block_ach_gateways
    if @admins_ach_gateway.inactive?
      @admins_ach_gateway.update_attributes(status: "active")
      flash[:notice] = "Gateway Unblocked Successfully!"
    else
      @admins_ach_gateway.update_attributes(status: "inactive")
      flash[:notice] = "Gateway Blocked Successfully!"
    end
    redirect_back(fallback_location: root_path)
  end

  # POST /admins/ach_gateways
  # POST /admins/ach_gateways.json
  def create
    begin
      @admins_ach_gateway = AchGateway.new(admins_ach_gateway_params)
      @admins_ach_gateway.gateway_type = params[:gateway_type] == "push_to_card" ? "push_to_card" : params[:gateway_type] == "check" ? "check" : "ach"
      values = []
      params[:gateway].each do |k,v|
        if v["value"].blank?
          if v["checked"].present? && v["checked"] == "on"
            v.delete("checked")
          end
        end
        values.push(v)
      end
      @admins_ach_gateway.export_col = values
      old_gateway = AchGateway.where(bank_name: @admins_ach_gateway.bank_name.strip).or(AchGateway.where(descriptor: @admins_ach_gateway.descriptor.strip)).where(gateway_type: @admins_ach_gateway.gateway_type).pluck(:archive)
      if old_gateway.present?
        if old_gateway.count == 2
          flash[:notice] = 'Gateway already exist!'
        elsif old_gateway.count == 1
          unless old_gateway.first
            flash[:notice] = 'Gateway already exist!'
          else
            @admins_ach_gateway.save(validate: false)
            flash[:notice] = params[:gateway_type] == "push_to_card" ? 'Push to Card gateway created successfully.' : params[:gateway_type] == "check" ? 'Check gateway created successfully.' : 'Ach gateway created successfully.'
          end
        end
      else
        if @admins_ach_gateway.save
          flash[:notice] = params[:gateway_type] == "push_to_card" ? 'Push to Card gateway created successfully.' : params[:gateway_type] == "check" ? 'Check gateway created successfully.' : 'Ach gateway created successfully.'
        else
          flash[:notice] = @admins_ach_gateway.try(:errors).try(:full_messages).try(:first)
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:notice] = invalid
    ensure
      if params[:gateway_type] == "push_to_card"
        return redirect_to push_to_card_admins_ach_gateways_path
      elsif params[:gateway_type] == "check"
        return redirect_to check_admins_ach_gateways_path
      else
        return redirect_to admins_ach_gateways_path
      end
    end
  end

  # PATCH/PUT /admins/ach_gateways/1
  # PATCH/PUT /admins/ach_gateways/1.json
  def update
    begin
      values = []
      params[:gateway].each do |k,v|
        if v["value"].blank?
          if v["checked"].present? && v["checked"] == "on"
            v.delete("checked")
          end
        end
        values.push(v)
      end
      @admins_ach_gateway.export_col = values
      if @admins_ach_gateway.update(admins_ach_gateway_params)
        flash[:notice] = params[:gateway_type] == "p2c" ? 'Push to Card gateway updated successfully.' : params[:gateway_type] == "check" ? 'Check gateway updated successfully.' : 'Ach gateway updated successfully.'
      else
        old_gateway = AchGateway.where(bank_name: @admins_ach_gateway.bank_name.strip).or(AchGateway.where(descriptor: @admins_ach_gateway.descriptor.strip)).where(gateway_type: @admins_ach_gateway.gateway_type).pluck(:archive)
        if old_gateway.present?
          if old_gateway.uniq.count == 2
            flash[:notice] = 'Gateway already exist. Please change Name and Descriptor!'
          elsif old_gateway.uniq.count == 1
            unless old_gateway.first
              flash[:notice] = 'Gateway already exist. Please change Name and Descriptor!'
            else
              @admins_ach_gateway.save(validate: false)
              flash[:notice] = params[:gateway_type] == "p2c" ? 'Push to Card gateway updated successfully.' : params[:gateway_type] == "check" ? 'Check gateway updated successfully.' : 'Ach gateway updated successfully.'
            end
          end
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:notice] = invalid
    ensure
      redirect_back(fallback_location: root_path)
    end
  end

  def archive
    if @admins_ach_gateway.archive?
      old_gateway = AchGateway.where("bank_name ILIKE ? ", @admins_ach_gateway.bank_name.strip).or(AchGateway.where("descriptor ILIKE ?", @admins_ach_gateway.descriptor.strip)).where(gateway_type: @admins_ach_gateway.gateway_type)
      if old_gateway.present?
        if old_gateway.pluck(:archive).uniq.count == 2
          flash[:notice] = 'Gateway already exist. Please change Name and Descriptor!'
        elsif old_gateway.pluck(:archive).uniq.count == 1
          if old_gateway.pluck(:archive).uniq.first
            @admins_ach_gateway.archive =  false
            @admins_ach_gateway.status = "active"
            @admins_ach_gateway.save(validate: false)
            flash[:notice] = "Gateway Unarchived Successfully!"
          else
            flash[:notice] = 'Gateway already exist. Please change Name and Descriptor!'
          end
        end
      else
        @admins_ach_gateway.update(archive: false,status: :active)
        flash[:notice] = "Gateway Unarchived Successfully!"
      end
    else
      @admins_ach_gateway.archive =  true
      @admins_ach_gateway.status = "inactive"
      @admins_ach_gateway.save(validate: false)
      flash[:notice] = "Gateway Archived Successfully!"
    end
    if @admins_ach_gateway.gateway_type == "p2c"
      return redirect_to p2c_admins_ach_gateways_path
    elsif @admins_ach_gateway.gateway_type == "check"
      return redirect_to check_admins_ach_gateways_path
    elsif @admins_ach_gateway.gateway_type == "ach"
      return redirect_to admins_ach_gateways_path
    else
      redirect_back(fallback_location: root_path)
    end
  end

  # DELETE /admins/ach_gateways/1
  # DELETE /admins/ach_gateways/1.json
  def destroy
    @admins_ach_gateway.destroy
    respond_to do |format|
      format.html { redirect_to admins_ach_gateways_url, notice: 'Ach gateway was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def strip_params
      params[:name] = params[:name].strip if params[:name].present?
      params[:descriptor] = params[:descriptor].strip if params[:descriptor].present?
      if params[:ach_gateway].present?
        params[:ach_gateway][:bank_name] = params[:ach_gateway][:bank_name].strip if params[:ach_gateway][:bank_name].present?
        params[:ach_gateway][:descriptor] = params[:ach_gateway][:descriptor].strip if params[:ach_gateway][:descriptor].present?
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_admins_ach_gateway
      @admins_ach_gateway = AchGateway.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admins_ach_gateway_params
        params.require(:ach_gateway).permit(:bank_name , :gateway_type ,:descriptor, :bank_fee_percent, :bank_fee_dollar,:transaction_limit, :ach_type)
    end
end