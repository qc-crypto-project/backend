class Admins::MerchantApisController < AdminsController
  before_action :check_admin

  def index
		@app_config = client_credentials
	end

  def submit_api
		app_config = client_credentials
		if params[:tab] == "fluid_transaction"
			if params[:first_name].present? and params[:last_name].present? and params[:cvc].present? and params[:card_number].present? and params[:amount].present? and params[:exp_date].present?
			url = "#{ENV['FLUID_PAY_API_URL']}/transaction"
		  	begin
		  		request_body = {
		  			"type" => "sale",
		  			"amount" => params[:amount],
		  			"payment_method" => {
		  				"card" => {
		  					"entry_type" => "keyed",
		  					"number" => params[:card_number],
		  					"expiration_date" => params[:exp_date],
		  					"cvc" => params[:cvc]
		  				}
		  			},
		  			"billing_address" => {
		  				"first_name" => params[:first_name],
		  				"last_name" => params[:last_name]
		  			}
		  		}
		  		response = RestClient.post(url,request_body,{content_type: :json, accept: :json, Authorization: '19VpZDXlUKuLPmkjXLhdIEIOFQi'})
		  	rescue => ex
		  		response = nil
		  		flash[:notice] = "Something Went Wrong!, Try Again Later"
		  	end
		else
			flash[:notice] = "Complete Your Information!."
  		  	response_params = {tab:params[:tab]}
	  	end
	  	if response.present?
	  		data = JSON.parse(response)
	  		response_params = {response_data: data,tab:'fluid_transaction',first_name:params[:first_name],last_name:params[:last_name],cvv:params[:cvv],card_number:params[:card_number],amount:params[:amount],exp_date:params[:exp_date]}
	    else
		  	response_params = {tab:'fluid_transaction',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],cvv:params[:cvv],zip_code:params[:zip_code],card_name:params[:card_name],amount:params[:amount],user_id:params[:user_id]}
	  	end
  	elsif params[:tab] == "fund_account"
			if params[:card_number].present? and params[:exp_date].present? and params[:cvv].present? and params[:zip_code].present? and params[:card_name].present? and params[:user_id].present?
			url = "#{params[:api]}?auth_token=#{app_config.stringValue}&card_number=#{params[:card_number]}&exp_date=#{params[:exp_date]}&cvv=#{params[:cvv]}&zip_code=#{params[:zip_code]}&card_name=#{params[:card_name]}&user_id=#{params[:user_id]}"
		  	begin
		  		response = RestClient.post url,{}, {content_type: :json, accept: :json}
		  	rescue => ex
		  		response = nil
		  		flash[:notice] = "Something Went Wrong!, Try Again Later"
		  	end
		else
			flash[:notice] = "Complete Your Information!."
  		  	response_params = {tab:params[:tab]}
	  	end
	  	if response.present?
	  		data = JSON.parse(response)
		  	response_params = {response_data: data,tab:'fund_account',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],cvv:params[:cvv],zip_code:params[:zip_code],card_name:params[:card_name],amount:params[:amount],user_id:params[:user_id]}
	    else
		  	response_params = {tab:'fund_account',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],cvv:params[:cvv],zip_code:params[:zip_code],card_name:params[:card_name],amount:params[:amount],user_id:params[:user_id]}
	  	end
  	elsif params[:tab] == "approving_request"
			if params[:request_id].present? and params[:pin_code].present?
			url = "#{params[:api]}?auth_token=#{app_config.stringValue}&request_id=#{params[:request_id]}&pin_code=#{params[:pin_code]}"
		  	begin
		  		response = RestClient.post url,{}, {content_type: :json, accept: :json}
		  	rescue => ex
		  		response = nil
		  		flash[:notice] = "Something Went Wrong!, Try Again Later"
		  	end
		else
			flash[:notice] = "Complete Your Information!."
  		  	response_params = {tab:params[:tab]}
	  	end
	  	if response.present?
	  		data = JSON.parse(response)
				response_params = {response_data: data,tab:'approving_request',auth_token:app_config.stringValue,request_id:params[:request_id],pin_code:params[:pin_code]}
			else
				response_params = {tab:'approving_request',auth_token:app_config.stringValue,request_id:params[:request_id],pin_code:params[:pin_code]}
	  	end
  	elsif params[:tab] == "requesting_code"
  		if params[:amount].present? and params[:from_id].present? and params[:location_secure_token].present?
	  		url = "#{params[:api]}?auth_token=#{app_config.stringValue}&amount=#{params[:amount]}&from_id=#{params[:from_id]}&location_secure_token=#{params[:location_secure_token]}&send_email=#{params[:send_email]}"
		  	begin
		  		response = RestClient.post url,{}, {content_type: :json, accept: :json}
		  	rescue => ex
		  		response = nil
		  		flash[:notice] = "Something Went Wrong!, Try Again Later"
		  	end
			else
				flash[:notice] = "Complete Your Information!."
				response_params = {tab:params[:tab]}
	  	end
	  	if response.present?
	  		data = JSON.parse(response)
		  	response_params = {response_data: data,tab:'requesting_code',auth_token:app_config.stringValue,amount:params[:amount],from_id:params[:from_id],location_secure_token:params[:location_secure_token]}
			else
				response_params = {tab:'requesting_code',auth_token:app_config.stringValue,amount:params[:amount],from_id:params[:from_id],location_secure_token:params[:location_secure_token]}
			end
  	elsif params[:tab] == "sign_up_registration"
			if params[:card_number].present? and params[:exp_date].present? and params[:cvv].present? and params[:email].present? and params[:zip_code].present? and params[:phone_number].present? and params[:first_name].present? and params[:last_name].present? and params[:postal_address].present? and params[:name].present?
  			url = "#{params[:api]}??auth_token=#{app_config.stringValue}&card_number=#{params[:card_number]}&exp_date=#{params[:exp_date]}&cvv=#{params[:cvv]}&email=#{params[:email]}&zip_code=#{params[:zip_code]}&phone_number=#{params[:phone_number]}&first_name=#{params[:first_name]}&last_name=#{params[:last_name]}&postal_address=#{params[:postal_address]}&name=#{params[:name]}"
				begin
		  		response = RestClient.post url,{}, {content_type: :json, accept: :json}
		  	rescue => ex
		  		response = nil
		  		flash[:notice] = "Something Went Wrong!, Try Again Later"
		  	end
			else
			flash[:notice] = "Complete Your Information!."
  		  	response_params = {tab:params[:tab]}
	  	end
	  	if response.present?
	  		data = JSON.parse(response)
	  		response_params = {response_data: data,tab:'sign_up_registration',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],cvv:params[:cvv],email:params[:email],zip_code:params[:zip_code],phone_number:params[:phone_number],first_name:params[:first_name],last_name:params[:last_name],postal_address:params[:postal_address],name:params[:name],amount:params[:amount]}
	  	else
	  		response_params = {tab:'sign_up_registration',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],cvv:params[:cvv],email:params[:email],zip_code:params[:zip_code],phone_number:params[:phone_number],first_name:params[:first_name],last_name:params[:last_name],postal_address:params[:postal_address],name:params[:name],amount:params[:amount]}
	  	end
  	elsif params[:tab] == "auth_token"
	  	if params[:client_id].present? and params[:client_secret].present? and params[:code_token].present?
	  		url = "#{params[:api]}?client_id=#{params[:client_id]}&client_secret=#{params[:client_secret]}&code=#{params[:code_token]}"
		  	begin
		  		response = RestClient.post url,{}, {content_type: :json, accept: :json}
		  	rescue => ex
		  		response = nil
		  		flash[:notice] = "Something Went Wrong!, Try Again Later"
		  	end
			else
				flash[:notice] = "Complete Your Information!."
				response_params = {tab:params[:tab]}
	  	end
	  	if response.present?
	  		data = JSON.parse(response)
				response_params = {response_data: data,tab:'auth_token',client_id:params[:client_id],client_secret:params[:client_secret],code_token:params[:code_token]}
	  	else
				response_params = {tab:'auth_token',client_id:params[:client_id],client_secret:params[:client_secret],code_token:params[:code_token]}
	  	end
  	end
		redirect_to admins_merchant_apis_path(response_params)
	end

	def vt_api
		@app_config = client_credentials
	end

  def submit_vt_api
			app_config = client_credentials
#= getting wallets id from wallet_details_url
			wallet_details_url = "#{request.base_url}/oauth/token?client_id=#{app_config.client_id}&code=#{app_config.stringValue}&client_secret=#{app_config.client_secret}"
			begin
				wallet_details_response = RestClient.post wallet_details_url,{}, {content_type: :json, accept: :json}
			rescue => ex
				wallet_details_response = nil
				flash[:notice] = "Something Went Wrong!, Try Again Later"
			end
#= When new sign_up_form submit
			if params[:tab] == "sign_up_registration"
				if params[:card_number].present? and params[:exp_date].present? and params[:card_cvv].present? and params[:email].present? and params[:phone_number].present? and params[:first_name].present? and params[:last_name].present? and params[:amount].present? and params[:name].present?
					if wallet_details_response.present?
						walletdata = JSON.parse(wallet_details_response)
						params_url = params[:api].gsub('base_url',request.base_url)
						url = "#{params_url}?auth_token=#{walletdata["access_token"]}&wallet_id=#{walletdata["wallet_id"]}&card_number=#{params[:card_number]}&exp_date=#{params[:exp_date]}&card_cvv=#{params[:card_cvv]}&email=#{params[:email]}&phone_number=#{params[:phone_number]}&first_name=#{params[:first_name]}&last_name=#{params[:last_name]}&name=#{params[:name]}&amount=#{params[:amount]}"
						begin
							response = RestClient.post url,{}, {content_type: :json, accept: :json}
						rescue => ex
							response = nil
							flash[:notice] = "Something Went Wrong!, Try Again Later"
						end
					end
				else
					flash[:notice] = "Complete Your Information!."
					response_params = {tab:params[:tab]}
				end
				if response.present?
					data = JSON.parse(response)
					response_params = {response_data: data,tab:'sign_up_registration',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],card_cvv:params[:card_cvv],email:params[:email],phone_number:params[:phone_number],first_name:params[:first_name],last_name:params[:last_name],amount:params[:amount],name:params[:name]}
				else
					response_params = {tab:'sign_up_registration',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],card_cvv:params[:card_cvv],email:params[:email],phone_number:params[:phone_number],first_name:params[:first_name],last_name:params[:last_name],name:params[:name],amount:params[:amount]}
				end
#= When Existing User form submit
			elsif params[:tab] == "existing_user"
				if params[:card_number].present? and params[:exp_date].present? and params[:card_cvv].present? and params[:phone_number].present? and params[:amount].present?
					if wallet_details_response.present?
						walletdata = JSON.parse(wallet_details_response)
						params_url = params[:api].gsub('base_url',request.base_url)
						url = "#{params_url}?auth_token=#{walletdata["access_token"]}&wallet_id=#{walletdata["wallet_id"]}&card_number=#{params[:card_number]}&exp_date=#{params[:exp_date]}&card_cvv=#{params[:card_cvv]}&phone_number=#{params[:phone_number]}&amount=#{params[:amount]}"
						begin
							response = RestClient.post url,{}, {content_type: :json, accept: :json}
						rescue => ex
							response = nil
							flash[:notice] = "Something Went Wrong!, Try Again Later4"
						end
					end
				else
					flash[:notice] = "Complete Your Information!."
					response_params = {tab:params[:tab]}
				end
				if response.present?
					data = JSON.parse(response)
					response_params = {response_data: data,tab:'existing_user',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],card_cvv:params[:card_cvv],phone_number:params[:phone_number],amount:params[:amount]}
				else
					response_params = {tab:'existing_user',auth_token:app_config.stringValue,card_number:params[:card_number],exp_date:params[:exp_date],card_cvv:params[:card_cvv],amount:params[:amount],phone_number:params[:phone_number]}
				end
#= When Virtual Terminal form submit
			elsif params[:tab] == "vt_transaction"
				if params[:card_number].present? and params[:card_exp_date].present? and params[:card_cvv].present? and params[:card_holder_name].present? and params[:amount].present?
					if wallet_details_response.present?
						walletdata = JSON.parse(wallet_details_response)
						params_url = params[:api].gsub('base_url',request.base_url)
						url = "#{params_url}?auth_token=#{walletdata["access_token"]}&wallet_id=#{walletdata["wallet_id"]}&card_number=#{params[:card_number]}&card_exp_date=#{params[:card_exp_date]}&card_cvv=#{params[:card_cvv]}&card_holder_name=#{params[:card_holder_name]}&amount=#{params[:amount]}"
						begin
							response = RestClient.post url,{}, {content_type: :json, accept: :json}
						rescue => ex
							response = nil
							flash[:notice] = "Something Went Wrong!, Try Again Later"
						end
					end
				else
					flash[:notice] = "Complete Your Information!."
					response_params = {tab:params[:tab]}
				end
				if response.present?
					data = JSON.parse(response)
					response_params = {response_data: data,tab:'vt_transaction',auth_token:app_config.stringValue,card_number:params[:card_number],card_exp_date:params[:card_exp_date],card_cvv:params[:card_cvv],card_holder_name:params[:card_holder_name],amount:params[:amount]}
				else
					response_params = {tab:'vt_transaction',auth_token:app_config.stringValue,card_number:params[:card_number],card_exp_date:params[:card_exp_date],card_cvv:params[:card_cvv],amount:params[:amount],card_holder_name:params[:card_holder_name]}
				end
#= When Debit Charge form submit
			elsif params[:tab] == "debit"
				if params[:amount].present? and params[:bank].present? and params[:tip].present? and params[:last4].present? and params[:terminal_id].present? and params[:transaction_number].present?
					if wallet_details_response.present?
						walletdata = JSON.parse(wallet_details_response)
						params_url = params[:api].gsub('base_url',request.base_url)
						url = "#{params_url}?auth_token=#{walletdata["access_token"]}&wallet_id=#{walletdata["wallet_id"]}&amount=#{params[:amount]}&bank=#{params[:bank]}&tip=#{params[:tip]}&last4=#{params[:last4]}&terminal_id=#{params[:terminal_id]}&transaction_number=#{params[:transaction_number]}"
						begin
							response = RestClient.post url,{}, {content_type: :json, accept: :json}
						rescue => ex
							response = nil
							flash[:notice] = "Something Went Wrong!, Try Again Later"
						end
					end
				else
					flash[:notice] = "Complete Your Information!."
					response_params = {tab:params[:tab]}
				end
				if response.present?
					data = JSON.parse(response)
					response_params = {response_data: data,tab:'debit',auth_token:app_config.stringValue,amount:params[:amount],bank:params[:bank],tip:params[:tip],last4:params[:last4],terminal_id:params[:terminal_id],transaction_number:params[:transaction_number]}
				else
					response_params = {tab:'debit',auth_token:app_config.stringValue,amount:params[:amount],bank:params[:bank],tip:params[:tip],last4:params[:last4],terminal_id:params[:terminal_id],transaction_number:params[:transaction_number]}
				end
#= When Refund form submit
			elsif params[:tab] == "refund"
				if params[:transact_id].present?
					if wallet_details_response.present?
						walletdata = JSON.parse(wallet_details_response)
						params_url = params[:api].gsub('base_url',request.base_url)
						url = "#{params_url}?auth_token=#{walletdata["access_token"]}&transact_id=#{params[:transact_id]}"
						begin
							response = RestClient.post url,{}, {content_type: :json, accept: :json}
						rescue => ex
							response = nil
							flash[:notice] = "Something Went Wrong!, Try Again Later"
						end
					end
				else
					flash[:notice] = "Complete Your Information!."
					response_params = {tab:params[:tab]}
				end
				if response.present?
					data = JSON.parse(response)
					response_params = {response_data: data,tab:'refund',auth_token:app_config.stringValue,transact_id:params[:transact_id]}
				else
					response_params = {tab:'refund',auth_token:app_config.stringValue,transact_id:params[:transact_id]}
				end
			end
			redirect_to vt_api_admins_merchant_apis_path(response_params)
		end

	private

  def save_credentials(app_config)
  	if app_config.present?
  		if app_config.client_id != params[:client_id] or app_config.client_secret != params[:client_secret]
  			if app_config.client_id != params[:client_id]
  				app_config.client_id = params[:client_id]
  			end
  			if app_config.client_secret != params[:client_secret]
  				app_config.client_secret = params[:client_secret]
  			end
	  		app_config.save
  		end
  	else
  		app_config = AppConfig.new
  		app_config.key = AppConfig::Key::MerchantApiKey
  		app_config.client_id = params[:client_id]
  		app_config.client_secret = params[:client_secret]
  		app_config.save
  	end
  	return app_config
  end

	def client_credentials
		app_config = AppConfig.find_by(key: AppConfig::Key::MerchantApiKey)
		if app_config.present?
			if params[:client_id].present? and params[:client_secret].present?
				app_config = save_credentials(app_config)
				if app_config.present?
					# if not app_config.stringValue.present?
						app_config = hit_access_token(app_config)
					# end
				end
			end
		else
			if params[:client_id].present? and params[:client_secret].present?
				app_config = save_credentials(app_config)
				if app_config.present?
					if not app_config.stringValue.present?
						app_config = hit_access_token(app_config)
					end
				end
			else
				app_config = AppConfig.new
			end
		end
		return app_config
	end

	def hit_access_token(app_config)
		client_credential = app_config
		url = "#{request.base_url}/oauth/token/retrieve?client_id=#{client_credential.client_id}&client_secret=#{client_credential.client_secret}"
		begin
			response = RestClient.post url,{}, {content_type: :json, accept: :json}
		rescue => ex
			response = nil
			flash[:notice] = "Something Went Wrong!, Try Again Later1"
		end
		unless response.nil?
			data = JSON.parse(response)
			if data["access_token"].present?
				client_credential.stringValue = data["access_token"]
				client_credential.save
			end
		else
			flash[:notice] = "Something Went Wrong!, Try Again Later2"
		end
		return client_credential
	end
end