class Admins::PluginController < ApplicationController
  layout 'admin'
  before_action :mtrac_admin
  def index

    @asset=Asset.new
    @plugins=Asset.all
  end

  def create
    @asset=Asset.new(plugin_params)
    if @asset.save
      User.merchant.find_each do |user|
        text = I18n.t('notifications.plugin_update_notification',platform: @asset.name, version: @asset.try(:version),  date: Date.today.strftime("%D"))
        Notification.notify_user(user,user,@asset,"plugin update",nil,text) 
      end
      flash[:success] = "Plugin Uploaded Successfully..."
      redirect_to admins_plugin_index_path
    else
      flash[:error] = @asset.errors.full_messages
      redirect_to admins_plugin_index_path
    end
  end

  def edit
    @asset=Asset.find(params[:id])
  end

  def update
    @asset=Asset.find(params[:id])
    if @asset.update(plugin_params)
      User.merchant.find_each do |user|
        text = I18n.t('notifications.plugin_update_notification',platform: @asset.name, version: @asset.try(:version),  date: Date.today.strftime("%D"))
        Notification.notify_user(user,user,@asset,"plugin update",nil,text)
      end
      flash[:success] = "Plugin Updated Successfully..."
    else
      flash[:error] = @asset.errors.full_messages
    end
    redirect_to admins_plugin_index_path
  end

  def destroy
    file=Asset.find(params[:id])
    if file.present?
      file.delete
      flash[:danger] = "Plugin Deleted..."
      redirect_to admins_plugin_index_path
    else
      flash[:error] = "File not Found!!!"
      redirect_to admins_plugin_index_path
      
    end
  end

  private
  def plugin_params
    params.require(:asset).permit(:file, :name,:version)
  end
end
