class Admins::IsosController < AdminsController
  skip_before_action :check_admin
  include Merchant::TransactionsHelper
  include Wicked::Wizard
  include AdminsHelper
  before_action :set_step
  before_action :setup_wizard
  before_action :user_data,only: [:show,:update]
  skip_before_action :mtrac_admin, only: [:profile, :merchants, :profile_wallet_details, :specfic_transactions, :show, :agent_affiliates, :system_fee]
  # def index
  #   @iso = User.new
  #   @isos = User.iso
  #   respond_to do |format|
  #     format.html{ render :index}
  #     format.json { render json: IsosDatatable.new(view_context)}
  #   end
  # end

  def new
    if params[:id].present?
      show()
    end
    # render template: 'admins/isos/new'
    # @iso = User.new
    # 2.times {@iso.fees.build}
    # @iso.fees.build(risk: "low")
    # @iso.fees.build(risk: "low")
    # @iso.fees.build(risk: "high")
    # @iso.fees.build
  end

  def profile
    @user = User.friendly.find params[:format]
    if @user.tos_user_id.present? && @user.tos_user_id != @user.id
      @tos_accepted_by = User.find(@user.tos_user_id)
    else
      @tos_accepted_by = @user
    end
    @user_wallet = @user.try(:wallets).try(:primary).try(:first)
    @buy_rate =  @user.try(:fees).try(:high).try(:first)
    @user_wallet.transaction do
      @user_wallet.update!(is_locked: true) if @user_wallet.present?
      @buy_rate.update!(is_locked: true) if @buy_rate.present?
    end
    @merchants = []
    @agents = []
    @affiliates = []
    @high_risk = []
    @low_risk = []
    locations = @user.locations.group_by{|e| e.merchant_id}
    merchants = User.where(id: locations.keys).group_by{|e| e.id}
    locations.each do|k,v|
      merchant = merchants[k].try(:first)
      v.each do |l|
        agents = l.agents
        affiliates = l.affiliates
        if merchant.present?
          if l.high?
            @high_risk.push(l)
          elsif l.dispensary?
            @low_risk.push(l)
          end
          @merchants.push(merchant)
        end
        if agents.present?
          @agents.push(agents)
        end
        if affiliates.present?
          @affiliates.push(affiliates)
        end
      end
    end
    @merchants = @merchants.flatten.uniq
    @agents = @agents.flatten.uniq
    @affiliates = @affiliates.flatten.uniq
    # @total_commission = BlockTransaction.where(main_type: "Fee Transfer").where(receiver_wallet_id: @user_wallet.id).pluck(:amount_in_cents).sum
    @total_commission = 0
    main_type = I18n.t('types.block_transaction.fee_type')
    first_date = DateTime.now.beginning_of_month
    second_date = DateTime.now
    @commission = BlockTransaction.by_wallet(@user_wallet.id).by_time(first_date, second_date).by_type(main_type).pluck(:amount_in_cents).sum
    @pending = @merchants.select{|m| m.is_block == true && m.archived == false }.count
    @approved = @merchants.select{|m| m.is_block == false }.size
    @active = @merchants.select{|m| m.archived == false && m.is_block == false }.count
    @high_risk = @high_risk.uniq{|u| u.merchant_id}.count
    @low_risk = @low_risk.uniq{|u| u.merchant_id}.count
    @profile = @user.try(:profile)
    @total_merchants = @merchants.count
    gon.merchants = @merchants
    gon.agents = @agents
    gon.affiliates = @affiliates
    render 'admins/shared/profiles/profile'
  end

  def create
    begin
    # if user_1 is empty baked iso profit will not saved
    commission_split_arrange()
    @iso = User.new(new_iso_params)
    password = random_password
    @iso.password = password
    @iso.is_password_set = false
    @iso.name = "#{@iso.first_name} #{@iso.last_name}"
    @iso.company_name = @iso.company_name
    @iso.first_name = @iso.first_name
    @iso.last_name = @iso.last_name
    @iso.regenerate_token
    @iso.is_block = false
    if @iso.iso!
      @iso.fees.each do |fee|
        fee.buy_rate!
      end
      if params[:user][:merchant_id].present?
        @merchant=User.friendly.find(params[:user][:merchant_id])
        @merchant.iso=@iso
      end
      message="Welcome to Quick Card"
      UserMailer.welcome_user(@iso ,message, 'ISO', password).deliver_later
      respond_to do |format|
        format.html{
          flash[:success] = 'Iso has been added Successfully!'
        }
      end
    else
      render status: 404, json:{error: 'No User found'}
    end
    redirect_back(fallback_location: root_path)
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:error] = invalid
      redirect_to  new_iso_path
    end
  end

  def update_status
    @user = User.find_by(id: params[:user_id])
    if @user.present?
      if @user.affiliate_program?
        @app = @user.oauth_apps.last
        if @app.present?
          app_block = nil
          if @app.is_block?
            @app.is_block = false
            app_block = false
          else
            @app.is_block = true
            date =  @app[:updated_at].strftime("%m-%d-%Y")
            app_block = true
          end
          @app.save(:validate => false)
          note=@app.is_block? ? "Inactivated" : 'Activated'
          save_activity(@user,Time.now,current_user.id,note,'')
          flash[:notice] = app_block == true ? "Inactivated Successfully!" : "Activated Successfully!"
          if app_block == true
            UserMailer.account_cancel(@app,date).deliver_later
          end
        end
        return redirect_back(fallback_location: root_path)
      else
        if @user.is_block?
          @user.is_block = false
        else
          @user.is_block = true
          date =  @user[:updated_at].strftime("%m-%d-%Y")
        end
        if @user.save(:validate => false)
          note=@user.is_block? ? "Inactivated" : "Activated"
          save_activity(@user,Time.now,current_user.id,note,'')
          if @user.is_block?
            UserMailer.account_cancel_partner(@user,date).deliver_later
          end
          render status: 200, json:{success: 'Verified'}
        else
          render status: 404, json:{error: 'Error in updation'}
        end
      end
    end
  end

  def update_two_step
    @user = User.find_by(id: params[:user_id])
    if @user.present?
      if @user.two_step_verification == false
        @user.two_step_verification = true
      elsif @user.two_step_verification == true
        @user.two_step_verification = false
      end
      if @user.save(:validate => false)
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Error in updation'}
      end
    else
      render status: 404, json:{error: 'No Usr found'}
    end
  end

  def unlock
    unless current_user.support_mtrac?
      if params["type"].blank?
        @wallet = Wallet.friendly.find(params[:wallet_id])
        @balance = show_balance(@wallet.id) - HoldInRear.calculate_pending(@wallet.id)
      elsif params["type"] == "wallet"
        @wallet = Wallet.find_by(id: params[:wallet_id])
      elsif params["type"] == "fee"
        @fee = Fee.find_by(id: params[:fee_id])
      elsif params["type"] == "iso_agent"
        @location = Location.find_by(id: params[:location_id])
      end
      respond_to do |format|
        format.js {
          render 'admins/shared/profiles/unlock.js.erb'
        }
      end
    end
  end

  def unlock_post
    @object = Wallet.find_by(id: params[:wallet_id]) if params[:wallet_id].present?
    @object = Fee.find_by(id: params[:fee_id]) if params[:fee_id].present?
    @object = Location.find_by(id: params[:location_id]) if params[:location_id].present?
    if @object.update(is_locked: false)
      flash[:notice] = "Unlocked Successfully!"
    else
      flash[:notice] = "Something went wrong. Please try again later!"
    end
    redirect_back(fallback_location: root_path)
  end

  def profile_wallet_details
    @filters = [10,25,50,100]
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
    @user = User.friendly.find params[:format]
    @primary_wallet = @user.try(:wallets).try(:primary).try(:first)
    @tango_orders = TangoOrder.where(wallet_id: @primary_wallet.id)
    if params[:type].present?
      case params[:type]
        when "achs"
          @achs = @tango_orders.instant_ach.order(created_at: :desc)
          @achs_count=@achs.count
          @achs=@achs.per_page_kaminari(params[:page]).per(page_size)
        when "gift Card/pre-paid Card"
          @giftcards = @tango_orders.where.not(utid: nil)
          @giftcards_count=@giftcards.count
          @giftcards=@giftcards.per_page_kaminari(params[:page]).per(page_size)
        when "checks"
          @checks = @tango_orders.check
          @checks_count = @checks.count
          @checks = @checks.per_page_kaminari(params[:page]).per(page_size)
        when "B2B (In the system)"
          @requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id)
          @requests_count = @requests.count
          @requests = @requests.per_page_kaminari(params[:page]).per(page_size)
      end
      render 'admins/iso_agent_affiliate_wallet_detail'
    else
      @achs = @tango_orders.instant_ach.order('created_at DESC').limit(5)
      @giftcards = @tango_orders.where.not(utid: nil).order('created_at DESC').limit(5)
      @checks = @tango_orders.check.order('created_at DESC').limit(5)
      @requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order('created_at DESC').limit(5)
      render 'admins/shared/profiles/wallet_details'
    end
  end

  def transfer_money
    @transfer = Transfer.new
    @wallet = Wallet.find_by(id: params[:wallet_id])
    @balance = @wallet.balance
    respond_to do |format|
      format.js {
        render 'admins/shared/profiles/transfer_money.js.erb'
      }
    end
  end
  def transactions_search_iso_agent_aff(params=nil,wallet=nil,offset=nil)

    conditions = []
    parameters = []

    if params[:id].present?
      params[:id] = params[:id].strip
      conditions << "block_transactions.sequence_id  ILIKE ?"
      parameters << "#{params[:id]}%"
    end

    date = parse_daterange_dash(params[:timestamp]) if params[:timestamp].present?
    time = parse_time(params)
    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, offset).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), offset).utc
      }
    end
    if date.present?
      if date[:first_date].present?
        conditions << "block_transactions.created_at >= ?"
        parameters << date[:first_date]
      end
      if date[:second_date].present?
        conditions << "block_transactions.created_at <= ?"
        parameters << date[:second_date]
      end
    end

    if params[:type].present?
      if params[:type] == ["Void Ach"]
        conditions << "(main_type IN (?)  )"
        parameters << ["Void_Ach", "Void ACH","Void ach"]
      elsif params[:type] == ["Void Check"]
        conditions << "(main_type IN (?) )"
        parameters << ["Void_Check", "Void Check"]
      else
        if params[:type].include?("Void ACH")
          params[:type].push("Void_Ach")
        end
        if params[:type].include?("Void Check")
          params[:type].push("Void_Check")
        end
        conditions << "(main_type IN (?) )"
        parameters << params[:type]
      end
    end

    if params[:business_name].present?
      params[:business_name] = params[:business_name].strip
      conditions << "(location_dba_name ILIKE (?) OR tags->'location'->>'business_name' ILIKE (?))"
      parameters << "%#{params[:business_name]}%"
      parameters << "%#{params[:business_name]}%"
    end

    if params[:merchant_txn_type].present?
      conditions << "(tags->'main_transaction_info'->>'transaction_type' IN(?) OR tags->'main_transaction_info'->>'transaction_sub_type' IN(?) OR main_type IN(?) OR tx_type IN(?) OR tx_sub_type IN(?))"
      parameters << params[:merchant_txn_type]
      parameters << params[:merchant_txn_type]
      parameters << params[:merchant_txn_type]
      parameters << params[:merchant_txn_type]
      parameters << params[:merchant_txn_type]
    end

    if params[:amount].present?
      new_var=number_with_precision(params[:amount], precision: 2)
      var=new_var.to_s.split('.').last
      storing_param=new_var.to_f
      if var=="00"
        storing_param=new_var
        storing_param=number_with_precision(storing_param, precision: 1).to_f
      end
      conditions << "(block_transactions.main_amount = ? OR tags->'main_transaction_info'->>'amount' = ?)"
      parameters << params[:amount].to_f
      parameters << "#{storing_param}"
    end

    if params[:merchant_name].present?
      params[:merchant_name] = params[:merchant_name].strip
      conditions << "tags->'merchant'->>'name' ILIKE ?"
      parameters << "%#{params[:merchant_name]}%"
    end

    if params[:receiver_name].present?
      params[:receiver_name]=params[:receiver_name].strip
      conditions <<  "(lower(receiver_name) LIKE ?)"
      parameters << "%#{params[:receiver_name].try(:downcase)}%"
    end

    unless conditions.empty?
      conditions = [conditions.join(" AND "), *parameters]
    end

    return conditions
  end

  def specfic_transactions
    conditions = []
    @filters = [10,25,50,100]
    @iso_agent_aff = true
    @transactions = []
    @page_no= params[:page].present? ? params[:page] : 1
    @transactions_count = 0
    @wallet = Wallet.find_by(id: params[:wallet_id])
    # @wallet = Wallet.find_by(id: params[:query]["wallet_id"]) if @wallet.blank?
    @user = @wallet.users.first
    @role = @user.role
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
    if params[:from_filter].present? && params[:from_filter] == "true" && (params[:query]["merchant_txn_type"].present? || params[:query]["type"].present?)
      if params[:query]["type"].class == String
        params[:query]["type"] = JSON.parse(params[:query]["type"]) if params[:query]["type"].present?
      end

      if params[:query]["merchant_txn_type"].class == String
        params[:query]["merchant_txn_type"] = JSON.parse(params[:query]["merchant_txn_type"]) if params[:query]["merchant_txn_type"].present?
      end

    end
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = transactions_search_iso_agent_aff(params[:query],@wallet,params[:offset])
    end
    @transactions = BlockTransaction.all_transactions(page_size, @wallet.id).where(conditions).order(timestamp: :desc).per_page_kaminari(params[:page]).per(page_size)
    @transactions_count=@transactions.count
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    search_type = params[:trans].present? ? params[:trans] : "reserve"
    @types = Transaction::ISO_SEARCH_TYPES[search_type.to_sym]
    @merchant_txn_types = Transaction::ISO_SEARCH_TYPES[:merchant_txn_type]
    render 'admins/shared/profiles/specific_wallet_transactions'
  end

  def merchants
    @user = User.includes(:locations).includes(:fees).includes(:tango_orders).friendly.find params[:format]
    @merchants = []
    @merchantss = []
    @user.locations.each do|l|
      if l.merchant.present?
        owner_name = l.merchant.try(:owner_name).present? ? l.merchant.owner_name : l.merchant.try(:owners).try(:first).try(:name)
        @merchants.push(l.merchant)
        if params[:type] == "all" or params[:type].blank?
          @merchantss.push({id: l.merchant.id,name: l.merchant.name,ownerName: owner_name,dba: l.business_name,merchant_number: l.merchant.phone_number,category: l.category,slug: l.merchant.slug,block: l.merchant.is_block,archived: l.merchant.archived})
        elsif params[:type] == "active"
          @merchantss.push({id: l.merchant.id,name: l.merchant.name,ownerName: owner_name,dba: l.business_name,merchant_number: l.merchant.phone_number,category: l.category,slug: l.merchant.slug,block: l.merchant.is_block,archived: l.merchant.archived}) if l.merchant.archived == false && l.merchant.is_block == false
        elsif params[:type] == "pending"
          @merchantss.push({id: l.merchant.id,name: l.merchant.name,ownerName: owner_name,dba: l.business_name,merchant_number: l.merchant.phone_number,category: l.category,slug: l.merchant.slug,block: l.merchant.is_block,archived: l.merchant.archived}) if l.merchant.is_block == true && l.merchant.archived == false
        elsif params[:type] == "inactive"
          @merchantss.push({id: l.merchant.id,name: l.merchant.name,ownerName: owner_name,dba: l.business_name,merchant_number: l.merchant.phone_number,category: l.category,slug: l.merchant.slug,block: l.merchant.is_block,archived: l.merchant.archived}) if l.merchant.archived == true
        end
      end
    end
    @merchants = @merchants.flatten.uniq
    @pending = @merchants.select{|m| m.is_block == true && m.archived == false }.count
    @inactive = @merchants.select{|m| m.archived == true }.count
    @active = @merchants.select{|m| m.archived == false && m.is_block == false }.count
    render 'admins/shared/profiles/merchants'
  end

  def agent_affiliates
    @user = User.includes(:locations).includes(:fees).includes(:tango_orders).friendly.find params[:format]
    @agents = []
    @agentss = []
    @affiliates = []
    @affiliatess = []
    @user.locations.each do|l|
      if !@user.agent?
        if l.agents.present?
          agent = l.agents.first
          @agentss.push(agent)
          if params[:type] == "all" or params[:type].blank?
            @agents.push({id: agent.id,name: agent.name.present? ? agent.name : "#{agent.first_name} #{agent.last_name}",dba: l.business_name,role: agent.role,phone_number: agent.phone_number,slug: agent.slug,block: agent.is_block,archived: agent.archived})
          elsif params[:type] == "agents"
            @agents.push({id: agent.id,name: agent.name.present? ? agent.name : "#{agent.first_name} #{agent.last_name}",dba: l.business_name,role: agent.role,phone_number: agent.phone_number,slug: agent.slug,block: agent.is_block,archived: agent.archived})
          end
        end
      end
      if l.affiliates.present?
        aff =l.affiliates.first
        @affiliatess.push(aff)
        if params[:type] == "all" or params[:type].blank?
          @affiliates.push({id: aff.id,name: aff.name.present? ? aff.name : "#{aff.first_name} #{aff.last_name}",dba: l.business_name,role: aff.role,phone_number: aff.phone_number,slug: aff.slug,block: aff.is_block,archived: aff.archived})
        elsif params[:type] == "affiliates"
          @affiliates.push({id: aff.id,name: aff.name.present? ? aff.name : "#{aff.first_name} #{aff.last_name}",dba: l.business_name,role: aff.role,phone_number: aff.phone_number,slug: aff.slug,block: aff.is_block,archived: aff.archived})
        end
      end
    end
    @users = @agents.flatten + @affiliates.flatten
    @users = @users.reverse.uniq{|u| u[:id]}
    @agents_count = @agentss.flatten.uniq.count
    @aff_count = @affiliatess.flatten.uniq.count
    @active_users = @agentss + @affiliatess
    @active_users = @active_users.uniq.select{|u| u.archived == false && u.is_block == false }.count
    render 'admins/shared/profiles/agent_affiliates'
  end

  def edit
    @iso= User.friendly.find(params[:id])
    @fees = @iso.fees.order(id: :asc)
    begin
      @iso['system_fee']['bank_account'] = AESCrypt.decrypt("#{@iso.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@iso['system_fee']['bank_account'])
    rescue
      @iso['system_fee']['bank_account']
    end
       # respond_to :js
  end

  def show
    case step
    when :iso_signup
      if params[:user_data_id].present? && @user_temp.try(:iso_agent)!="{}"
        @iso = User.new(@user_temp.try(:iso_agent)) if @user_temp.present?
      elsif params[:iso_id].present?
        @iso=User.find(params[:iso_id]) if params[:iso_id].present?
      else
        @iso=User.new
      end
    when :iso_buyrate
      if params[:user_data_id].present? && @user_temp.try(:buyrate)!="{}"
        @iso = User.new(@user_temp.try(:buyrate)) if @user_temp.present?
        @fees = @iso.fees
      elsif params[:iso_id].present?
        @iso=User.friendly.find(params[:iso_id]) if params[:iso_id].present?
        @fees = @iso.fees.order(id: :asc)
      else
        @iso = User.new
        2.times {@iso.fees.build}
      end
      gon.iso_baked_user = @iso.try(:fees).first
    when :system_fee
      if params[:user_data_id].present? && @user_temp.try(:fees)!="{}"
        @iso = User.new(@user_temp.fees) if @user_temp.present?
      elsif params[:iso_id].present?
        @iso=User.friendly.find(params[:iso_id]) if params[:iso_id].present?
      else
        @iso=User.new
      end
    when :iso_documentation
      if params[:iso_id].present?
        @iso=User.friendly.find(params[:iso_id]) if params[:iso_id].present?
        @images = Documentation.where(:imageable_id => params[:iso_id])
        @images.each do |doc|
          if doc.is_deleted === true
            doc.update(is_deleted: false)
          end
        end
      else
        @iso=User.new
      end
    end

    if params[:id]=="iso_documentation" || params[:id] != "wicked_finish"
      @step_index = current_step
      if params[:count].to_i == 1
        session.delete(:index)
        session[:index] = @step_index if session[:index].blank?
      end
      if params[:count].to_i != 2
        if !params[:content].present? && session[:index] < @step_index
          session.delete(:index)
          session[:index] = @step_index
        end
      end
    end
    gon.step_index = @step_index
    gon.index_session = session[:index]
    render_wizard(nil, {}, {user_data_id: params[:user_data_id] })
  end

  def document_delete
    @image = Documentation.find params[:image_id]
    if @image.is_deleted == false
      @image.is_deleted = true
      @image.save
      render json: true
    else
      render json: false
    end
  end

  def bank_account
    @user=User.friendly.find(params[:user_id]) if params[:user_id].present?
    begin
      @user['system_fee']['bank_account'] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user['system_fee']['bank_account'])
    rescue
      @user['system_fee']['bank_account']
    end
    render 'admins/shared/profiles/bank_account'
  end

  def save_bank_account
    @user=User.friendly.find(params[:format]) if params[:format].present?
    if @user.present?
      if @user.system_fee == "{}"
        @user.system_fee = bank_account_params
      else
        @user.system_fee.merge!(bank_account_params)
      end
      @user.system_fee["bank_account"] = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user.system_fee["bank_account"])
      if @user.try(:merchant_ach_status) == "initial"
        @user.merchant_ach_status = "complete"
        @user.send_ach_bank_request = true
      end
      @user.save
    end
    if params[:image].present? && params[:image]['add_image'].present?
      save_image_bank_account
    end
    flash[:success]="Bank Account Updated Successfully"
    if @user.admin_user?
      redirect_to profile_admins_admin_users_path(@user)
    else
      redirect_to profile_isos_path(@user)
    end
  end

  # def create
  #   begin
  #     params[:user][:fees_attributes]["0"]["commission"] = params[:user][:fees_attributes]["0"]["commission"].to_json
  #     params[:user][:fees_attributes]["1"]["commission"] = params[:user][:fees_attributes]["1"]["commission"].to_json
  #     high_splits = params[:user][:fees_attributes]["0"]["is_baked_iso"] == "0" ? nil : set_baked_iso(params[:user][:fees_attributes]["0"]["splits"])
  #     low_splits = params[:user][:fees_attributes]["1"]["is_baked_iso"] == "0" ? nil : set_baked_iso(params[:user][:fees_attributes]["1"]["splits"])
  #     # if user_1 is empty baked iso profit will not saved
  #     params[:user][:fees_attributes]["0"]["is_baked_iso"] = high_splits.present? ? params[:user][:fees_attributes]["0"]["is_baked_iso"] : "0"
  #     params[:user][:fees_attributes]["1"]["is_baked_iso"] = low_splits.present? ? params[:user][:fees_attributes]["1"]["is_baked_iso"] : "0"
  #     params[:user][:fees_attributes]["0"]["splits"] = high_splits
  #     params[:user][:fees_attributes]["1"]["splits"] = low_splits
  #     @iso = User.new(new_iso_params)
  #     password = random_password
  #     @iso.password = password
  #     @iso.name = "#{@iso.first_name} #{@iso.last_name}"
  #     @iso.first_name = @iso.first_name
  #     @iso.last_name = @iso.last_name
  #     @iso.regenerate_token
  #     if @iso.iso!
  #       @iso.fees.each do |fee|
  #         fee.buy_rate!
  #       end
  #       if params[:user][:merchant_id].present?
  #         @merchant=User.friendly.find(params[:user][:merchant_id])
  #         @merchant.iso=@iso
  #       end
  #       # message="Welcome to Quick Card"
  #       # UserMailer.welcome_user(@iso ,message, 'ISO', password).deliver_later
  #       respond_to do |format|
  #         format.html{
  #           flash[:success] = 'Iso has been added Successfully!'
  #         }
  #       end
  #     else
  #       flash[:error] = @iso.errors.full_messages.first
  #     end
  #     redirect_back(fallback_location: root_path)
  #   rescue ActiveRecord::RecordInvalid => invalid
  #     flash[:error] = invalid
  #     redirect_to  new_iso_path
  #   end
  # end

  # def edit
  #   if params[:iso_id].present?
  #     @iso= User.friendly.find(params[:iso_id])
  #     @fees = @iso.fees.order(id: :asc)
  #     # respond_to :js
  #   end
  # end

  def next_baked
    @count = params[:count].to_i
    @risk = params[:risk].to_i
    respond_to :js
  end

  def update
    case params[:id]
    when "iso_signup"
      iso_signup()
    when "iso_buyrate"
      iso_buyrate_save()
    when "system_fee"
      system_fee()
    when "iso_documentation"
      iso_domentation()
    end

    # if params[:iso_id].present?
    #   iso = User.friendly.find(params[:id])
    #   commission_split_arrange()
    #   if edit_iso_params[:password].present?
    #     iso.update(edit_iso_params)
    #     save_image if params[:image]['add_image'].present?
    #   else
    #     ex=edit_iso_params.except(:password_confirmation)
    #     ex = ex.except(:password)
    #     iso.update(ex)
    #     save_image if params[:image]['add_image'].present?
    #   end
    #   iso.name = "#{iso.first_name} #{iso.last_name}"
    #   iso.save
    #   if iso.errors.blank?
    #     iso.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
    #     flash[:success] = "ISO has been updated Successfully"
    #     redirect_back(fallback_location: root_path)
    #   else
    #     flash[:error] = iso.errors.full_messages.first
    #     redirect_back(fallback_location: root_path)
    #   end
    # end
  end

  def iso_signup
    params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
    params[:user][:profile_attributes][:years_in_business] = "#{params[:user].try(:[], "years_in_business").try(:[], "year")}-#{params[:user].try(:[], "years_in_business").try(:[], "month")}" if params[:user].try(:[], "profile_attributes").present?
    params[:user][:is_password_set] = false
    if params[:user][:iso_id].present?
      params[:user][:id] = params[:user][:iso_id]
      begin
        @iso = User.find(params[:user][:id])
        if params[:user][:profile_attributes].present?
          @iso.company_name = params[:user][:profile_attributes][:company_name]
        end
        if iso_update_params[:password].present?
          @iso.update(iso_update_params)
        else
          ex=iso_update_params.except(:password_confirmation)
          ex = ex.except(:password)
          @iso.update(ex)
        end
        if @iso.errors.blank?
          @iso.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
        end
        if @iso.profile.present?
          @iso.profile.update(iso_profile_params) if params[:user][:profile_attributes].present? && !iso_profile_params.blank?
          # flash[:success] = "Iso has been updated Successfully"
          if @iso.errors.blank?
            @iso.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
            flash[:success] = "Iso has been updated Successfully"
          else
            flash[:success] = "Password Previously used, try again."
            return redirect_back(fallback_location: root_path)
          end
        else
          flash[:error] = "Iso Profile Does Not Exist!"
        end
        @iso = User.friendly.find params[:user][:format] if params[:user][:format].present?
        return redirect_to profile_isos_path(@iso)
      end
    else
     if params[:user_data_id].present?
      @user_temp = MerchantData.find_by(id: params[:user_data_id])
      if @user_temp.present?
        @user_temp.update(iso_agent: iso_signup_params)
      end
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
     else
      @user_temp = MerchantData.create(iso_agent: iso_signup_params)
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
     end
  end

  def iso_buyrate_save
    # Commmission and Splits not Permitted in Funcation
    commission_split_arrange()

    if params[:user][:iso_id].present?
      @iso=User.friendly.find(params[:user][:iso_id])
      @iso.update(iso_buyrate_params)
      if @iso.present?
        @iso.fees.each do |fee|
          fee.buy_rate!
        end
      else
        flash[:error] = @iso.errors.full_messages.first
      end
      flash[:success] = "Iso has been updated Successfully"
      @iso = User.friendly.find params[:user][:format] if params[:user][:format].present?
      save_activity(@iso,Time.now,current_user.id,'iso_buyrate','')
      return redirect_to profile_isos_path(@iso)
      else
        if @user_temp.present?
      @user_temp.update(buyrate: iso_buyrate_params)
      end
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
  end

  def system_fee
    if params[:user][:iso_id].present?
      @iso=User.friendly.find(params[:user][:iso_id])
      @iso.system_fee.merge!(system_fee_params["system_fee"])
      @iso.system_fee[:ach_international] = (params["ach_international"].present? && params["ach_international"] == "true") ? true : false
      @iso.save
      flash[:success] = "Iso has been updated Successfully"
      @iso = User.friendly.find params[:user][:format] if params[:user][:format].present?
      save_activity(@iso,Time.now,current_user.id,'system_fee','')
      return redirect_to profile_isos_path(@iso)
    else
      if @user_temp.present?
        @user_temp.update(fees: system_fee_params)
      end
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
  end

  def iso_domentation
    if @user_temp.present?
      create_iso()
      redirect_to wizard_path(@next_step)
    elsif params[:user].present? ? params[:user][:iso_id].present? ? true : false : false
      @iso=User.friendly.find(params[:user][:iso_id])
      save_user_documentation(@iso, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
      @documents = Documentation.where(imageable_id: params[:user][:iso_id])
      @documents.each do |doc|
        if doc.is_deleted === true
          doc.document.destroy
          doc.destroy
        end
      end
      respond_to do |format|
        format.html{
          flash[:success] = 'Iso has been added Successfully!'
        }
      end
      @iso = User.friendly.find params[:user][:format] if params[:user][:format].present?
      return redirect_to profile_isos_path(@iso)
    end
  end

  def create_iso
    begin
      if !params[:user].present?
        params[:user]={}
      end
      params[:user].merge!(@user_temp.try(:iso_agent)).merge!(@user_temp.try(:buyrate)).merge!(@user_temp.try(:fees))
      @iso = User.new(new_iso_params)
      password = random_password
      @iso.password = password
      @iso.regenerate_token
      @iso.is_block = false
      if @iso.iso!
        @iso.fees.each do |fee|
          fee.buy_rate!
        end
        TransactionBatch.create(iso_wallet_id: @iso.wallets.first.id, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: "sale")
        if @iso.try(:[],'merchant_id').present?
          @merchant=User.friendly.find(@iso.try(:[],'merchant_id'))
          @merchant.iso=@iso
        end
        @iso.update company_name: @iso.try(:profile).try(:company_name) if @iso.profile.present?
        save_user_documentation(@iso, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
        message="Welcome to Quick Card"
        UserMailer.welcome_user(@iso ,message, 'ISO', password).deliver_later
      else
        flash[:error] = @iso.errors.full_messages.first
      end
      respond_to do |format|
        format.html{
          flash[:success] = 'Iso has been added Successfully!'
        }
      end
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:error] = invalid
      redirect_to users_path(user: "isos")
    end
  end

  def details
    @batches=[]
    @low_batches=[]
    @iso=User.find_by_id(params[:id])
    # -----------High Risk Batch-------------
    @iso_batch=Batch.where(user_id: @iso.id,is_closed: false,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::High).first
    db_batches=Batch.where(iso_id: @iso.id,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::High)
    @iso_commission=Batch.where(iso_id: @iso.id,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::High).sum(:iso_profit)
    @iso_transaction=Batch.where(iso_id: @iso.id,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::High).sum(:total_transactions)
    @batches=db_batches if db_batches.present?
    # ----------Low Risk Batch----------------
    @iso_batch_low=Batch.where(user_id: @iso.id,is_closed: false,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::Low).first
    db_batches_low=Batch.where(iso_id: @iso.id,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::Low)
    @iso_commission_low=Batch.where(iso_id: @iso.id,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::Low).sum(:iso_profit)
    @iso_transaction_low=Batch.where(iso_id: @iso.id,batch_type: TypesEnumLib::Batch::BuyRateCommission,batch_risk: TypesEnumLib::Batch::Low).sum(:total_transactions)
    @low_batches=db_batches_low if db_batches_low.present?
  end

  def destroy
  end


  def setting
    @user= User.find(id= params[:user_id])
    respond_to do |format|
      format.js {
        render 'admins/shared/profiles/setting.js.erb'
      }
    end
  end

  private

  def user_data
    if params[:user_data_id].present?
      @user_temp = MerchantData.find_by(id: params[:user_data_id])
    end
  end

  def current_step
    steps.index(step) + 1
  end

  def finish_wizard_path
    session.delete(:index) if session[:index].present?
    return users_path(user: 'isos')
  end

  def doc_params
    params.require(:image).permit(:add_image,:usage_id,:location_id)
  end

  def bank_account_params
    params[:user].require(:system_fee).permit(:bank_account, :bank_routing, :bank_account_type, :bank_account_name)
  end

  def commission_split_arrange
    if params[:user][:fees_attributes].present? && params[:user][:fees_attributes]['0'].present?
      params[:user][:fees_attributes]["0"]["commission"]=params[:user][:fees_attributes]["0"]["commission"].to_json
      high_splits = params[:user][:fees_attributes]["0"]["is_baked_iso"] == "0" ? nil : set_baked_iso(params[:user][:fees_attributes]["0"]["splits"])
      params[:user][:fees_attributes]["0"]["is_baked_iso"] = high_splits.present? ? params[:user][:fees_attributes]["0"]["is_baked_iso"] : "0"
      params[:user][:fees_attributes]["0"]["splits"] = high_splits
    end
    if params[:user][:fees_attributes].present? && params[:user][:fees_attributes]['1'].present?
      params[:user][:fees_attributes]["1"]["commission"]=params[:user][:fees_attributes]["1"]["commission"].to_json
      low_splits = params[:user][:fees_attributes]["1"]["is_baked_iso"] == "0" ? nil : set_baked_iso(params[:user][:fees_attributes]["1"]["splits"])
      params[:user][:fees_attributes]["1"]["is_baked_iso"] = low_splits.present? ? params[:user][:fees_attributes]["1"]["is_baked_iso"] : "0"
      params[:user][:fees_attributes]["1"]["splits"] = low_splits
    end
  end

  def new_iso_params
    # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,:is_password_set,
                                 profile_attributes: [:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code],
                                 system_fee: [
                                     :redeem_fee,
                                     :send_check,
                                     :giftcard_fee,
                                     :add_money_dollar,
                                     :add_money_percent,
                                     :b2b_dollar,
                                     :b2b_percent,
                                     # :invoice_card_fee_perc,
                                     # :invoice_card_fee,
                                     :ach_percent,
                                     :ach_dollar,
                                     :push_to_card_percent,
                                     :push_to_card_dollar,
                                     :bank_account,
                                     :bank_routing,
                                     :bank_account_type,
                                     :bank_account_name,
                                     :ach_limit,
                                     :ach_limit_type,
                                     :push_to_card_limit,
                                     :push_to_card_limit_type,
                                     :check_limit,
                                     :check_limit_type,
                                     :check_withdraw_limit
                                 ],
                                 fees_attributes: [:risk,:customer_fee,:b2b_fee_dollar, :b2b_fee,  :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,:is_baked_iso,
                                                   :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee, :refund_fee, :service, :misc, :statement, :splits, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar, :qr_refund, :qr_refund_percentage, :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee])
  end

  def iso_signup_params
    # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,:is_password_set,
                                 profile_attributes: [
                                     :company_name,
                                     :years_in_business,
                                     :tax_id,
                                     :street_address,
                                     :ein,
                                     :country,
                                     :city,
                                     :state,
                                     :zip_code
                                 ])
  end

  def edit_iso_params
    # params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,)
    params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,:company_name,
                                 profile_attributes: [:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code],
                                 system_fee: [
                                     :redeem_fee,
                                     :send_check,
                                     :giftcard_fee,
                                     :add_money_dollar,
                                     :add_money_percent,
                                     :b2b_dollar,
                                     :b2b_percent,
                                     # :invoice_card_fee_perc,
                                     # :invoice_card_fee,
                                     :push_to_card_percent,
                                     :push_to_card_dollar,
                                     :ach_percent,
                                     :ach_dollar,
                                     :bank_account,
                                     :bank_routing,
                                     :bank_account_type,
                                     :bank_account_name,
                                     :ach_limit,
                                     :ach_limit_type,
                                     :push_to_card_limit,
                                     :push_to_card_limit_type,
                                     :check_limit,
                                     :check_limit_type,
                                     :check_withdraw_limit,
                                     :failed_push_to_card_fee,
                                     :failed_ach_fee,
                                     :failed_check_fee

                                 ],
                                 fees_attributes: [:id, :customer_fee, :b2b_fee_dollar, :b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission,:is_baked_iso,
                                                   :transaction_fee_app, :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement, :splits, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar, :qr_refund, :qr_refund_percentage, :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee])
  end

  def iso_update_params
    params.require(:user).except(:iso_id,:format, :profile_attributes).permit(:id, :email, :password, :password_confirmation, :name, :phone_number, :company_name)
  end

  def iso_profile_params
    params[:user].require(:profile_attributes).permit(:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code)
  end

  def iso_buyrate_params
    params.require(:user).permit(
        fees_attributes: [:id,:risk,:customer_fee,:b2b_fee_dollar, :b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,:is_baked_iso,:ach_fee,:ach_fee_dollar,:dc_deposit_fee,:dc_deposit_fee_dollar,
                          :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee, :refund_fee, :service, :misc, :statement, :splits, :qr_refund, :qr_refund_percentage,
                          :failed_push_to_card_fee, :failed_ach_fee, :failed_check_fee])

  end

  def system_fee_params
    params.require(:user).permit(
        system_fee: [
            :redeem_fee,
            :send_check,
            :giftcard_fee,
            :add_money_dollar,
            :add_money_percent,
            :b2b_dollar,
            :b2b_percent,
            # :invoice_card_fee_perc,
            # :invoice_card_fee,
            :ach_percent,
            :ach_dollar,
            :push_to_card_percent,
            :push_to_card_dollar,
            :ach_limit,
            :ach_limit_type,
            :push_to_card_limit,
            :push_to_card_limit_type,
            :check_limit,
            :check_limit_type,
            :check_withdraw_limit,
            :failed_push_to_card_fee,
            :failed_ach_fee,
            :failed_check_fee
        ])
  end

  # def iso_documentation_params
  #    params[:user].require(:documentation).permit(user_image:[], w9_form_image:[], schedule_a_image:[],misc_document_img:[])
  # end

  # def edit_iso_params
  #   # params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,)
  #   params.require(:user).permit(:id,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,
  #                            system_fee: [
  #                                :redeem_fee,
  #                                :send_check,
  #                                :giftcard_fee,
  #                                :add_money_dollar,
  #                                :add_money_percent,
  #                                :b2b_dollar,
  #                                :b2b_percent
  #                            ],
  #                            fees_attributes: [:id,:customer_fee,:b2b_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission,:is_baked_iso,
  #                                              :transaction_fee_app, :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement, :splits])
  # end

  def set_baked_iso(splits)
    if splits.present?
      if splits["share_split"].present? && splits["splits"].present?
        if splits["splits"].try(:[], "0").present?
          if splits["splits"]["0"].try(:[], "user_1").present?
            if splits["splits"]["0"]["user_1"].try(:[], "user").present?
              splits.to_json
            end
          end
        end
      end
    end
  end

  def set_step
    if params[:action] == "update" && params[:id] == "iso_signup" && params[:from] == "profile"
      self.steps = [:iso_signup]
    else
      self.steps = [:iso_signup, :iso_buyrate, :system_fee, :iso_documentation]
    end
  end
end