class Admins::CustomerDisputesController < AdminsController
  layout 'admin'
    def customer_disputes

      if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
        conditions = dispute_search_query(params[:query],params[:offset])
      end
      page_size = params[:filter].present? ? params[:filter].to_i : 10
      @filters = [10,25,50,100]
      if params[:dispute_type] == "cancel"
        @status= [{value: "pending", view:"Pending"}, {value: "full_refund", view: "Full Refund"}, {value: "partial_cancel_order", view: "Partial Refund and Cancel Order"}, {value: "partial_send_order", view: "Partial Refund and Cancel Order"}]
        @customer_disputes = CustomerDispute.joins(:dispute_transaction).where(dispute_type: "cancel").where(conditions).order('id DESC').per_page_kaminari(params[:page]).per(page_size).per(page_size)
      else
        @status= [{value: "pending", view:"Pending"}, {value: "approved", view: "Approved"}, {value: "declined", view: "Declined"}, {value: "refunded", view: "Refunded"}]
        @customer_disputes = CustomerDispute.joins(:dispute_transaction).where(dispute_type: "refund").where(conditions).order('id DESC').per_page_kaminari(params[:page]).per(page_size).per(page_size)
      end
    end

    def show
      @customer_dispute = CustomerDispute.find(params[:id])
      @transaction = @customer_dispute.dispute_transaction
      @tracker = @transaction.tracker
      @customer = @customer_dispute.customer
      @dispute_request_first = @customer_dispute.dispute_requests.first
      @dispute_requests_last = @customer_dispute.dispute_requests.last
      @merchant_offer = @customer_dispute.dispute_requests.where(request_type: "merchant_offer").first
      @final_merchant_offer = @customer_dispute.dispute_requests.where(request_type: "merchant_final_offer").last
      @pending_counter_offer = @customer_dispute.dispute_requests.where(user_id: @customer_dispute.customer_id,status: "pending",title: "Counteroffer").last
      @customer_offer =  @customer_dispute.dispute_requests.where(request_type: "counter_offer").last
      @return_to_sender = @customer_dispute.dispute_requests.where(request_type: "return_to_sender").last
      @customer_dispute = CustomerDispute.find(params[:id])
      # TODO there should be only 1 pending dispute_requests
      render partial: 'show'
    end
end
