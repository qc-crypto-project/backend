class Admins::CompaniesController < AdminsController

  skip_before_action :authenticate_user!, only: [:form_validate]

  def index
    @company = Company.new
    @companies = Company.all
    respond_to do |format|
      format.html{ render :index}
      format.json { render json: CompaniesDatatable.new(view_context)}
    end
  end

  def new
    render template: 'admins/companies/new'
  end

  def show
    @company= Company.friendly.find params[:id]
    @merchant= User.new
    @merchants = User.merchant.where(company_id: @company.id)
    respond_to do |format|
      format.html{ render :show}
      format.json { render json: MerchantsDatatable.new(view_context)}
    end
  end

  def create
    @company= Company.new(new_company_params)
    @company.company_email = @company.company_email.downcase
    if new_company_params[:company_email].present? && User.where(:email => new_company_params[:company_email].downcase).count <= 0
      if @company.save
        check = check_ledger(@company) if @company.ledger.present? && @company.ledger != ENV['LEDGER_NAME']
        if check.present? && check.try(:seq_code).present?
          @company.destroy
          flash[:error] = "Ledger not found!"
        else
          user = create_company_admin(@company)
          UserMailer.welcome_merchant(user.id, user.password, "Company Signup").deliver_later
          respond_to do |format|
            format.html{
              flash[:success] = 'Company has been added Successfully!'
            }
          end
        end
      else
        flash[:error] = @company.errors.full_messages.first
      end
    else
      flash[:error] = 'A user with provided email already exists in the system.'
    end
    redirect_back(fallback_location: root_path)
  end

  def edit
    @company= Company.friendly.find params[:id]
    respond_to :js
  end

  def update
    company = Company.friendly.find params[:id]
    if params[:company][:password].present?
      user = company.users.where(email: company.company_email).first
      user.update(password: params[:company][:password])
    end
    if params[:company][:company_email].present?
      user = company.users.select{|m| m.partner?}.first
      user.update(email: params[:company][:company_email]) if user.present?
    end
    if params[:company][:phone_number].present?
      user = company.users.select{|m| m.partner?}.first
      user.update(phone_number: params[:company][:phone_number]) if user.present?
    end
    company.update(edit_company_params)
    if company.errors.blank?
      flash[:success] = "Company has been updated Successfully"
      redirect_back(fallback_location: root_path)
    else
      flash[:error] = "Error occured in Edit"
      redirect_back(fallback_location: root_path)
    end
  end

  def form_validate
  	phone_number = params[:phone_number] || params.try(:[], :user).try(:[], :phone_number) || params[:company].try(:[],:phone_number)
    email =  params.try(:[], :user).try(:[], :email) || params[:email] || params.try(:[], :user).try(:[], :email) || params[:company].try(:[],:company_email)
    table = User
    table = Location if params[:location].present?
    if email.present? && params[:user_id].present?
      email = email.downcase
      user = table.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(email: email).first

      ids=user.id if user.present?
      ids=user.company_id if params[:company].present? && user.present?
      if user.present? && ids != params[:user_id].to_i
        render json: false
      else
        render json: true
      end
    elsif phone_number.present? && params[:user_id].present?
      phone_number = phone_number.gsub('+','')
      user = table.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(phone_number: phone_number).first
      ids = table.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      ids = table.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(phone_number: phone_number).pluck(:company_id).uniq if params[:company].present?
      area_code =  phone_number.present? && phone_number.slice(1,3)
      if phone_number.present? && area_code == '839' && phone_number.length==11
        res = true
        else
        res = Phonelib.valid?(phone_number)
       end
      if ids.present? && ids.count > 0 && !ids.include?(params[:user_id].to_i) || !res
        render json: false
      else
        render json: true
      end
    end
    if email.present? && params[:user_id].blank?
      user = table.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(email: email).first
      if user.present?
        render json: false
      else
        render json: true
      end
    elsif phone_number.present? && params[:user_id].blank?
      phone_number = phone_number.gsub('+','')
      ids = table.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      area_code =  phone_number.present? && phone_number.slice(1,3)
      if phone_number.present? && area_code == '839' && phone_number.length==11
        res = true
      else
        res = Phonelib.valid?(phone_number)
      end
      if ids.present? && ids.count > 0 || !res
        render json: false
      else
        render json: true
      end
    end

  end

  def check_phone_format
    phone_number = params[:phone_number] || params.try(:[], :user).try(:[], :phone_number) || params[:company].try(:[],:phone_number)
    if phone_number.present?
      res = Phonelib.valid?(phone_number)
      render json: res
    end
  end

  def check_phone_number_format
    phone_number = params[:phone_number]
    if phone_number.present?
      res = Phonelib.valid?(phone_number)
      render json: res
    end
  end

  def destroy
  end

  private

  def new_company_params
    params.require(:company).permit(:name,:company_email,:category,:phone_number, :ledger)
  end

  def edit_company_params
    params.require(:company).permit(:name,:company_email,:category,:phone_number, :ledger)
  end


  def check_ledger(company)
    begin
      ledger = Sequence::Client.new(ledger_name: company.ledger, credential:  ENV['LEDGER_CREDENTIAL'] )
      key = ledger.keys.create(id: "#{company.id}")
    rescue => ex
      return ex
    end
  end

  def create_company_admin(company)
    password = SecureRandom.hex(9)
    user = User.new(name: company.name, email: company.company_email.downcase, company_id: company.id, phone_number: company.phone_number, ledger: company.ledger)
    password = random_password
    user.password = password
    if user.partner!
      user.update(ref_no: "CP-#{user.id}")
    end
    return user
  end
end
