class Admins::LoadBalancersController < AdminsController
  before_action :check_admin

  def index
    # @filters = [4,6,50,100]
    @load_balancers = LoadBalancer.eager_load(:payment_gateways).order(id: :desc).per_page_kaminari(params[:page]).per(4)
  end

  def new
    @descriptors = PaymentGateway.load_balancer_gateway.group_by{|v|v.try(:descriptor).try(:name)}
  end

  def edit
    @load_balancer = LoadBalancer.eager_load(:payment_gateways).eager_load(:load_balancer_rules).find_by(id: params[:id])
    # @descriptors = PaymentGateway.main_gateways.active_gateways.eager_load(:descriptor).where(load_balancer_id: nil).where.not(descriptor_id: nil).or(PaymentGateway.eager_load(:descriptor).where(id: @load_balancer.try(:payment_gateways).try(:pluck,:id))).group_by{|v|v.descriptor.name}
    @descriptors = PaymentGateway.main_gateways.active_gateways.eager_load(:descriptor).where(load_balancer_id: nil).where.not(descriptor_id: nil).group_by{|v|v.descriptor.name}
    render template: "admins/load_balancers/new"
  end

  def show
    @load_balancer = LoadBalancer.find(params[:id])
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    if @load_balancer.present?
      if params[:req] == "active"
        @all_locations = Location.active_with_load_balancer(@load_balancer.id)
        @active_locations = @all_locations.where(:is_block => :false).distinct.count
        @query = @all_locations.ransack(params[:q])
        @locations = @query.result(distinct: true).per_page_kaminari(params[:page]).per(page_size)
        @all_locations_map = @all_locations.map{|u| u.id}
      elsif params[:req] == "inactive"
        @all_locations = Location.inactive_with_load_balancer(@load_balancer.id)
        @active_locations = @all_locations.where(:is_block => :true).distinct.count
        @query = @all_locations.ransack(params[:q])
        @locations = @query.result(distinct: true).per_page_kaminari(params[:page]).per(page_size)
        @all_locations_map = @all_locations.map{|u| u.id}
      else
        @all_locations = Location.where(load_balancer_id: @load_balancer.id)
        @active_locations = @all_locations.distinct.count
        @query = @all_locations.ransack(params[:q])
        @locations = @query.result(distinct: true).per_page_kaminari(params[:page]).per(page_size)
        @all_locations_map = @all_locations.map{|u| u.id}
      end
    end
  end

  def update_load_balancer
    Location.where(id: params["locations"].first.split(',')).update_all(load_balancer_id: params['load_balancer_id'])
    flash[:success] = 'Load Balancer updated successfully!'
    respond_to do |format|
      format.js {render js: "window.location.pathname='#{load_balancer_configs_app_configs_path}'"}
      format.html {redirect_to load_balancer_configs_app_configs_path}
    end
  end

  def create
    ActiveRecord::Base.transaction do
      load_balancer = LoadBalancer.where(id: params[:load_balancer_id]).first_or_initialize
      if load_balancer.try(:id).present?
      load_balancer.current_id = current_user.id
      load_balancer.request = request
      load_balancer.action_name = params[:commit]
      end
      load_balancer.update(name: params[:load_balancer_name])
      if load_balancer.payment_gateways.present?
        old_gateways = PaymentGateway.where(id: load_balancer.payment_gateways.pluck(:id))
        old_gateways.update_all(load_balancer_id: nil) if old_gateways.present?
      end
      new_gateways = PaymentGateway.where(id: params[:selected_gateway].split(','))
      new_gateways.update_all(load_balancer_id: load_balancer.id) if new_gateways.present?
      rule_identifiers = LoadBalancerRule.identifiers.keys
      if params[:deleted_rules_ids].present? # deleting rules on updating
        load_balancer.load_balancer_rules.where(id: params[:deleted_rules_ids].split(',')).delete_all
      end

      rule_identifiers&.each do |identifier|
        if params.present? && params[identifier.to_sym].present?
          params[identifier.to_sym]&.each do |key,value|
            load_balancer_rule = load_balancer.load_balancer_rules.where(id: value[:id]).first_or_initialize
            # if load_balancer_rule.try(:id).present?
              case identifier
              when "rotate_processor"
                if load_balancer_rule.try(:id).present? && load_balancer_rule.count.to_i > 0
                  a = value["count"].to_i
                  # a b c
                  # 3 1 0
                  #
                  if load_balancer.try(:payment_gateways).try(:active_load_gateways).present?
                    load_balancer.try(:payment_gateways).try(:active_load_gateways)&.each do |pg|
                      if (pg.rotation_count.to_i % load_balancer_rule.count.to_i) != 0
                        pg.rotation_count = pg.rotation_count.to_i % load_balancer_rule.count.to_i
                        a = 0
                      else
                        pg.rotation_count = a
                      end
                      pg.save
                    end
                  end
                else
                  update_rotation = load_balancer.payment_gateways.active_load_gateways
                  update_rotation.update_all(rotation_count: 0) if update_rotation.present?
                end
              when "rotate_processor_with_amount"
                pgs = PaymentGateway.where(id: value["descriptor"]).active_load_gateways
                if load_balancer_rule.try(:id).present? && load_balancer_rule.count.to_i > 0
                  a = value["count"].to_i
                  if pgs.present?
                    pgs&.each do |pg|
                      if (pg.amount_count.to_i % load_balancer_rule.count.to_i) != 0
                        pg.amount_count = pg.amount_count.to_i % load_balancer_rule.count.to_i
                        a = 0
                      else
                        pg.amount_count = a
                      end
                      pg.save
                    end
                  end
                else
                  pgs.update_all(amount_count: 0) if pgs.present?
                end
              when "rotate_processor_on_cc_brand"
                pgs = PaymentGateway.where(id: value["descriptor"]).active_load_gateways
                if load_balancer_rule.try(:id).present? && load_balancer_rule.count.to_i > 0
                  a = value["count"].to_i
                  if pgs.present?
                    pgs&.each do |pg|
                      if (pg.cc_brand_count.to_i % load_balancer_rule.count.to_i) != 0
                        pg.cc_brand_count = pg.cc_brand_count.to_i % load_balancer_rule.count.to_i
                        a = 0
                      else
                        pg.cc_brand_count = a
                      end
                      pg.save
                    end
                  end
                else
                  pgs.update_all(cc_brand_count: 0) if pgs.present?
                end
              when "rotate_processor_with_cc"
                if load_balancer_rule.try(:id).present? && load_balancer_rule.count.to_i > 0
                  a = value["count"].to_i
                  if load_balancer.try(:payment_gateways).try(:active_load_gateways).present?
                    load_balancer.try(:payment_gateways).try(:active_load_gateways).&each do |pg|
                      if (pg.cc_number_count.to_i % load_balancer_rule.count.to_i) != 0
                        pg.cc_number_count = pg.cc_number_count.to_i % load_balancer_rule.count.to_i
                        a = 0
                      else
                        pg.cc_number_count = a
                      end
                      pg.save
                    end
                  end
                else
                  update_rotate_processor_with_cc = load_balancer.payment_gateways.active_load_gateways
                  update_rotate_processor_with_cc.update_all(cc_number_count: 0) if update_rotate_processor_with_cc.present?
                end
              end
            # else
            #   load_balancer.payment_gateways.active_load_gateways.update_all(rotation_count: 0, cc_number_count: 0, cc_brand_count: 0, amount_count: 0)
            # end


            # load_balancer_rule = load_balancer.load_balancer_rules.new
            # load_balancer_rule.identifier = identifier
            # load_balancer_rule.update(value.except(:descriptor))

            load_balancer_rule.update(permitted_rule_params(value))

            # if value[:id].present? && value[:descriptor].present? # updating
            #   PaymentGateway.where(load_balancer_rule_id: load_balancer_rule.id).update_all(load_balancer_rule_id: nil)
            # end
            # PaymentGateway.where(id: value[:descriptor].split(',')).update_all(load_balancer_rule_id: load_balancer_rule.id) if value[:descriptor].present?
          end
        end
      end
    end
    redirect_to admins_load_balancers_path

  end

  def update
    if params[:status].present? && params[:id].present?
      balancer = LoadBalancer.find_by(id: params[:id])
      balancer.current_id = current_user.id
      balancer.request = request
      balancer.action_name = action_name
      balancer.update(active: params[:status]) if balancer.present?
    else
      render json: false
    end
  end

  def permitted_rule_params(params)
    params.permit(:count, :identifier, :operator, :amount, :no_of_hours, :percentage, :rule_type, :alert_receivers_emails, :decline_reason, :status, :country, :card_brand, :descriptor)
  end

end
