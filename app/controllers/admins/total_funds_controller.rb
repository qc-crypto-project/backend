class Admins::TotalFundsController < AdminsController
  before_action :check_admin
  before_action :mtrac_admin

  def index
    if params[:q].present?
      batches = search_total_withdraws
      ach_batches = batches.group_by_day(day_start: 22) {|u| u.batch_date }.sort.reverse.to_h
    else
      ach_batches = TangoOrder.withdraw_batches.sort.reverse.to_h
    end

    @issue_checks = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @issue_checks = AppConfig.new(key: AppConfig::Key::IssueCheckConfig) unless @issue_checks.present?

    @ach_batches = Kaminari.paginate_array(ach_batches,total_count: ach_batches.try(:length)).page(params[:page]).per(5)
    respond_to do |format|
      format.html
      format.js {render 'admins/total_funds/total_funds_search.js.erb'}
    end
  end

  def search_total_withdraws
    batch_query_search_params
  end
end
