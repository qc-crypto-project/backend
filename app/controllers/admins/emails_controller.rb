class Admins::EmailsController < AdminsController
  include Admins::EmailsHelper
  def index
    if params[:q].present?
      search_val = params[:q].try(:strip)
      params[:q] = {}
      params[:q]["subject_or_sender_signature_or_search_option_or_status_cont"] = search_val
      # begin
      #   date = search_val.to_datetime
      #   params[:q]["created_at_in"] =  [date.beginning_of_day,date.end_of_day]
      # rescue => ex
      # ensure
        @q = EmailNotification.ransack(params[:q].try(:merge, m: 'or'))
        @emails = @q.result.order(id: :desc).per_page_kaminari(params[:page]).per(params[:filter] || 10)
      params[:q] = search_val
      # end
    else
      @emails = EmailNotification.all.order(id: :desc).per_page_kaminari(params[:page]).per(params[:filter] || 10)
    end
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js
      format.html
    end
  end

  def edit
    @email_notification = EmailNotification.includes(:email_messages,:images).find_by(id: params[:id])
    @from_sig = EmailNotification.sender_info
    @email_messages = @email_notification.email_messages
    @email_messages_count = @email_messages.group(:status).count
    sql = "Select id,name from categories"
    @category = ActiveRecord::Base.connection.execute(sql)
    @images = @email_notification.images
    respond_to :js
  end

  def delete_schedule
    notification = EmailNotification.find_by(id: params[:id])
    if notification.present?
      Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}"
      notification.update(status: "deleted")
      flash[:notice] = "Email Notification Deleted Successfully"
    else
      flash[:notice] = "Missing Notification"
    end
    redirect_to admins_emails_path
  end

  def resend
    if params[:reschedule_form].present?
      creation_result=reschedule(params,params[:notification_id])
    else
      creation_result = create_email_notification(params)
    end
    flash[:notice] = creation_result[:message]
    return redirect_to admins_emails_path
  end

  def export
    @batch=EmailNotification.where(id: params[:id])
    send_data generate_csv(@batch), filename: "EmailNotification.csv"
  end

  def new
    @from_sig = EmailNotification.sender_info
    sql = "Select id,name from categories"
    @category = ActiveRecord::Base.connection.execute(sql)
    respond_to :js
  end

  def generate_csv(batch)
    CSV.generate do |csv|
      csv << %w{ UserId Name Subject Email Status}
      batch.each do |b|
        b.email_messages.each do|email|
          slug = email.user_name.split('-')
          csv << ["#{slug.first}-#{email.user_id}", "#{slug.second}", b.subject, email.to, email.status]
        end
      end
    end

  end

  def create
    creation_result = create_email_notification(params)
    flash[:notice] = creation_result[:message]
    return redirect_to admins_emails_path
  end


  def update
  end

  def resend_email
    @email_batch = EmailNotification.find_by(id: params[:id])
    # params[:schedule_date] = @email_batch.schedule_date.present? ? @email_batch.schedule_date.to_datetime.strftime("%m/%d/%Y %H:%M %p") : ""
    params[:users] = @email_batch.search_option
    params[:category] = @email_batch.category_id
    params[:body] = @email_batch.body
    params[:subject] = @email_batch.subject
    params[:sender] = @email_batch.sender
    params[:old_images] = @email_batch.images.pluck(:id).join(',')
    creation_result = create_email_notification(params)
    flash[:notice] = creation_result[:message]
    return redirect_to admins_emails_path
  end

  private

  def create_email_notification(params)
    params[:schedule_date] = params[:schedule_date].to_datetime.strftime("%m/%d/%Y %H:%M %p") if params[:from_suggestion].present?
    @offset = Time.now.in_time_zone(cookies[:timezone]).utc_offset
    utc_time = Time.strptime(params[:schedule_date], "%m/%d/%Y %I:%M %P") if params[:schedule_date].present?
    @schedule_date = params[:schedule_date].present? ? Time.new("#{utc_time.year}", "#{utc_time.month}", "#{utc_time.day}", "#{utc_time.hour}","#{utc_time.min}",00, @offset).utc : nil
    emails = []
    message = ""    
    if User.active_users.where(role: params[:users]).count > 998 && params[:category].blank?
      return call_worker(params, @schedule_date)
    else
      if params[:users] != "merchant"
        emails = User.active_users.where(role: params[:users]).pluck(:id,:email,:name,:role)
      elsif params[:users] == "merchant" && params[:category].blank?
        # emails = User.active_users.where(role: params[:users]).pluck(:id,:email,:name,:role)
        emails = User.active_users.where(role: params[:users]).joins(:oauth_apps).where("oauth_apps.is_block = ?",false).pluck(:id,:email,:name,:role)
      elsif params[:users] == "merchant" && params[:category].present?
        sql ="SELECT users.id AS id, users.email AS email, users.name AS name, users.role AS role FROM locations INNER JOIN users ON locations.merchant_id=users.id WHERE locations.category_id = #{params[:category]} AND users.is_block = false AND users.archived = false"
        emails = ActiveRecord::Base.connection.execute(sql).values
      end
    end
    return call_worker(params, @schedule_date) if emails.count > 998
    return {success: false, message: "'To' Users not found!"} if emails.blank?
    notification = EmailNotification.create(subject: params[:subject], body: params[:body], schedule_date: @schedule_date, search_option: params[:users], sender: params[:sender],category_id: params[:category],status: "processed",sender_signature: get_sender_email(params[:sender]))
    attachments_url = []
    if params[:attachments].present? && notification.present?
      params[:attachments].each do|image|
        result = save_image(image,notification.id)
        if result[:success]
          attachments_url.push({id:result[:id],url: result[:url], type: result[:type], name: result[:name]})
        else
          return {success: false, message: "Invalid attachment! #{image.original_filename}"}
        end
      end
    end

    if params[:old_images].present?
      old_images = params[:old_images].split(',')
      old_images.each do|id|
        image = Image.find(id)
        new_image = image.dup
        new_image.email_notification_id = notification.id
        new_image.save
        attachments_url.push({id:new_image.id,url: image.add_image.url, type: new_image.add_image_content_type, name: new_image.add_image_file_name})
      end
    end

    if @schedule_date.present?
      if Time.now.utc >= @schedule_date
        response_result = notification_email(notification.id,emails,attachments_url)
        message = response_result[:message]
      else
        notification.update(status: "pending")

        # EmailNotificationWorker.perform_at(@schedule_date,notification.id)
        Sidekiq::Cron::Job.create(name: "EmailNotification#{notification.id}", cron: "#{@schedule_date.min} #{@schedule_date.hour} #{@schedule_date.day} #{@schedule_date.month} *", class: EmailNotificationWorker)
        message = "Email notification sent successful"
      end
    else
      response_result = notification_email(notification.id,emails,attachments_url)
      message = response_result[:message]
    end
    return {success: true, message: message}
  end

  def reschedule(params,id)
    return_result = {success: false, message: "Something went wrong!"}
    params[:schedule_date] = params[:schedule_date].to_datetime.strftime("%m/%d/%Y %H:%M %p") if params[:from_suggestion].present?
    @offset = Time.now.in_time_zone(cookies[:timezone]).utc_offset
    utc_time = Time.strptime(params[:schedule_date], "%m/%d/%Y %I:%M %P") if params[:schedule_date].present?
    @schedule_date = params[:schedule_date].present? ? Time.new("#{utc_time.year}", "#{utc_time.month}", "#{utc_time.day}", "#{utc_time.hour}","#{utc_time.min}",00, @offset).utc : nil
    notification = EmailNotification.find_by(id: id)
    if notification.present?
      emails = []
      if User.active_users.where(role: params[:users]).count > 10 && params[:category].blank?
        return reschedule_call_worker(params, id, @schedule_date)
      else
        if notification.search_option != "merchant"
          emails = User.active_users.where(role: notification.search_option).pluck(:id,:email,:name,:role)
        elsif notification.search_option == "merchant" && notification.category_id.blank?
          emails = User.active_users.where(role: notification.search_option).pluck(:id,:email,:name,:role)
        elsif notification.search_option == "merchant" && notification.category_id.present?
          sql =  "SELECT users.id AS id, users.email AS email, users.name AS name, users.role AS role FROM locations INNER JOIN users ON locations.merchant_id=users.id WHERE locations.category_id = #{notification.category_id} AND users.is_block = false AND users.archived = false"
          emails = ActiveRecord::Base.connection.execute(sql).values
        end
      end
      return reschedule_call_worker(params, id, @schedule_date) if emails.count > 998
      if emails.present?
        attachments_url = []
        if notification.images.present?
          notification.images.each do|image|
            attachments_url.push({id: image.id,url: image.add_image.url, type: image.add_image_content_type, name: image.add_image_file_name})
          end
        end
        if @schedule_date.present?
          if Time.now.utc >= @schedule_date
            response_result = notification_email(notification.id,emails,attachments_url)
            notification.update(subject: params[:subject], body: params[:body], schedule_date: @schedule_date, sender: params[:sender],status: "processed") if response_result[:success]
            message = response_result[:message]
            Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}" if response_result[:success]
          else
            notification.update(subject: params[:subject], body: params[:body], schedule_date: @schedule_date, sender: params[:sender],status: "pending")
            # EmailNotificationWorker.perform_at(@schedule_date,notification.id)
            Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}" if notification.present?
            Sidekiq::Cron::Job.create(name: "EmailNotification#{notification.id}", cron: "#{@schedule_date.min} #{@schedule_date.hour} #{@schedule_date.day} #{@schedule_date.month} *", class: EmailNotificationWorker)
            message = "Email notification sent successful"
          end
        else
          response_result = notification_email(notification.id,emails,attachments_url)
          notification.update(subject: params[:subject], body: params[:body], schedule_date: @schedule_date, sender: params[:sender],status: "processed") if response_result[:success]
          message = response_result[:message]
          Sidekiq::Cron::Job.destroy "EmailNotification#{notification.id}" if response_result[:success]
        end
        return_result = {success: true, message: message}
      end
    end
    return return_result
  end

end
