class Admins::AdminUsersController < AdminsController 
	include Wicked::Wizard
	steps :admin_user_signup, :system_fee
	before_action :user_data,only: [:show,:update]
	def index
		@admin_users = User.admin_user.all.order(id: :desc)
	end

	def new
		# @admin_user = User.admin_user.new
		# @admin_user.build_profile
		if params[:id].present?
      		show()
    	end
	end

	def show
		case step
	    when :admin_user_signup
	      if params[:user_data_id].present? && @user_temp.try(:iso_agent)!="{}"
	        @admin_user = User.new(@user_temp.try(:iso_agent)) if @user_temp.present?
	      elsif params[:admin_user_id].present?
	        @admin_user=User.friendly.find params[:admin_user_id] if params[:admin_user_id].present?
	      else
	        @admin_user=User.new
	      end
	    when :system_fee
	      if params[:user_data_id].present? && @user_temp.try(:fees)!="{}"
	        @admin_user = User.new(@user_temp.fees) if @user_temp.present?
	      elsif params[:admin_user_id].present?
	        @admin_user=User.friendly.find(params[:admin_user_id]) if params[:admin_user_id].present?
	      else
	        @admin_user=User.new
	      end
	    end
	    gon.step_index = @step_index
	    gon.index_session = session[:index]
	    render_wizard(nil, {}, {user_data_id: params[:user_data_id] })
	end

	def create
		params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
		@admin_user = User.admin_user.new(admin_user_params)
		@admin_user.is_block = false
		password = random_password
    	@admin_user.password = password
    	@admin_user.is_password_set = false
		if @admin_user.save
			flash[:notice] = "Admin User Created Successfully"
			redirect_to users_path(user: "admin_users")
		end
		ActivityLog.create(params:params,action:'admin_user_create',user_id:current_user.id,activity_type:'user_activity',action_type:'affiliate_program')
	end

	def edit
		@admin_user = User.admin_user.find(params[:id])
		@admin_user.build_profile if @admin_user.profile.nil?
	end

	def update
		# params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
		# @admin_user = User.admin_user.find(params[:id])
		# if @admin_user.update(admin_user_params)
		# 	flash[:notice] = "Admin User Updated Successfully"
		# 	redirect_to users_path(user: "admin_users")
		# end
		case params[:id]
	    when "admin_user_signup"
	      admin_user_signup()
	    when "system_fee"
	      system_fee()
	    end
	end


  def admin_user_signup
	    params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
	    params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
			if params[:user][:admin_user_id].present?
	      params[:user][:id] = params[:user][:admin_user_id]
	      begin
	        @admin_user = User.friendly.find(params[:user][:admin_user_id])
	        @admin_user.build_profile if @admin_user.profile.nil?
	        if params[:user][:profile_attributes].present?
	          @admin_user.company_name = params[:user][:profile_attributes][:company_name] if params[:user][:profile_attributes][:company_name].present?
	        end
	        if @admin_user.profile.present?
	        	params[:user][:profile_attributes][:id] = @admin_user.profile.id
				params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
				if @admin_user.update(admin_user_params)
					flash[:success] = "Admin User has been updated Successfully"
					save_activity_logs
					return redirect_to profile_admins_admin_users_path(@admin_user)
					# return redirect_back(fallback_location: root_path)
	          	else
	            	flash[:success] = "Something went wrong."
	            	return redirect_back(fallback_location: root_path)
	          	end
	        else
	          flash[:error] = "Admin User Profile Does Not Exist!"
					end
	        @admin_user = User.friendly.find params[:user][:format] if params[:user][:format].present?
	        return redirect_to profile_admins_admin_users_path(@admin_user)
	      end
	    else
	      if params[:user_data_id].present?
	        new1 = MerchantData.find_by_id params[:user_data_id]
	        if new1.present?
	          @user_temp = new1.update(iso_agent: admin_user_signup_params)
	        end

	        redirect_to wizard_path(@next_step,{:user_data_id => params[:user_data_id] })
	      else
	        @user_temp = MerchantData.create(iso_agent: admin_user_signup_params)
	        redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
	      end

	    end
	  end



  	def system_fee


  		if params[:user][:admin_user_id].present?
	      	@admin_user=User.friendly.find(params[:user][:admin_user_id])
	      	if @admin_user.system_fee != "{}"
				@admin_user.system_fee.merge!(system_fee_params["system_fee"])
			else
				@admin_user.system_fee = system_fee_params["system_fee"]
			end
	      	@admin_user.save
	      	flash[:success] = "Admin User fees has been updated Successfully"
	      	@admin_user = User.friendly.find params[:user][:format] if params[:user][:format].present?
	      	save_activity(@admin_user,Time.now,current_user.id,'system_fee','')

	      	# @admin_user.affiliate_programs.each do |affiliate_program|
	      	# 	affiliate_program.system_fee = system_fee_params["system_fee"]
	      	# 	affiliate_program.save
	      	# end
	      	# return redirect_to profile_isos_path(@admin_user)
	      	return redirect_to users_path(user: "admin_users")
	    else
	  		if @user_temp.present?
	      		result = create_admin_user()
	      		if result[:success]
	      			 flash[:success] = 'Admin User has been added Successfully!'
	        		redirect_to users_path(user: "admin_users")
	      		else
	        		flash[:error] = result[:message]
	        		redirect_to users_path(user: "admin_users")
	      		end
	      	end
	     end
  	end


  	def create_admin_user
  		begin
	      if !params[:user].present?
	       params[:user]={}
	      end
	      # params[:user].merge!(@user_temp.iso_agent).merge!(@user_temp.fees)
	      params[:user].merge!(@user_temp.iso_agent)
	      @admin_user = User.new(admin_user_params)
	      password = random_password
	      @admin_user.password = password
	      @admin_user.is_password_set = false
	      @admin_user.is_block = false
	      @admin_user.regenerate_token
	      if @admin_user.admin_user!
	        @admin_user.update company_name: @admin_user.try(:profile).try(:company_name) if @admin_user.profile.present?
					ActivityLog.create(params:params,action:'admin_user_create',user_id:current_user.id,activity_type:'user_activity',action_type:'affiliate_program')
	        return {success: true, message: "Admin User has been added Successfully"}
	        respond_to do |format|
	          format.html{
	            flash[:success] = 'Admin User has been added Successfully!'
	          }
	        end
	        redirect_to users_path(user: "admin_users")
	      else
	        flash[:error] = @admin_user.errors.full_messages.first
	      end
	    rescue ActiveRecord::RecordInvalid => invalid
	      flash[:error] = invalid
	      return {success: false, message: invalid}
	      # redirect_to users_path(user: "affiliates")
	    end
  	end


  	def profile
	    @user = User.includes(:locations).includes(:fees).includes(:tango_orders).friendly.find params[:format]
	    @tos_accepted_by = User.find(@user.tos_user_id) if @user.tos_user_id.present?
	    @user_wallet = @user.try(:wallets).try(:primary).try(:first)
	    @wallet=@user.try(:wallets).try(:primary).try(:first)
	    @buy_rate =  @user.try(:fees).try(:high).try(:first)
	    @system_fee =  @user.try(:fees).try(:high).try(:first)
	    render 'admins/admin_users/profile'
	  end


	def user_data
    	if params[:user_data_id].present?
      		@user_temp = MerchantData.find_by(id: params[:user_data_id])
    	end
  	end

	def current_step
	    steps.index(step) + 1
	end

	def get_data
		admin_user_id = params[:admin_user_id].to_i
    	keys = User.admin_user.where(id: admin_user_id).first.oauth_apps.first
	    if keys.present?
	      	adminUserData = keys
	      	render status: 200, json:{success: 'Verified', :location => adminUserData}
	    else
	      render status: 404, json:{:location => nil}
	    end
	end



  	def system_fee_params
	    params.require(:user).permit(
	        system_fee: [
	            :redeem_fee,
	            :send_check,
	            :giftcard_fee,
	            :add_money_dollar,
	            :add_money_percent,
	            :b2b_dollar,
	            :b2b_percent,
	            :ach_percent,
	            :ach_dollar,
	            :push_to_card_percent,
	            :push_to_card_dollar,
	            :ach_limit,
	            :ach_limit_type,
	            :push_to_card_limit,
	            :push_to_card_limit_type,
	            :check_limit,
	            :check_limit_type,
	            :check_withdraw_limit,
	            :failed_push_to_card_fee,
	            :failed_ach_fee,
	            :failed_check_fee
	        ])
  	end



  	def admin_user_signup_params
	    # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
	    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,
	                                 profile_attributes: [
	                                     :company_name,
	                                     :years_in_business,
	                                     :tax_id,
	                                     :street_address,
	                                     :ein,
	                                     :country,
	                                     :city,
	                                     :state,
	                                     :zip_code
	                                 ])
  	end


  	def admin_user_params
    	params.require(:user).permit(:id, :email, :password, :password_confirmation, :name, :phone_number, :company_name , 
    		profile_attributes: [:id , :company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code ,:_destroy],
    		system_fee: [
                :redeem_fee,
                :send_check,
                :giftcard_fee,
                :add_money_dollar,
                :add_money_percent,
                :b2b_dollar,
                :b2b_percent,
             	:ach_percent,
	            :ach_dollar,
	            :push_to_card_percent,
	            :push_to_card_dollar,
	            :bank_account,
	            :bank_routing,
	            :bank_account_type,
	            :bank_account_name,
	            :ach_limit,
	            :ach_limit_type,
	            :push_to_card_limit,
	            :push_to_card_limit_type,
	            :check_limit,
	            :check_limit_type,
	            :check_withdraw_limit,
	            :failed_push_to_card_fee,
	            :failed_ach_fee,
	            :failed_check_fee
             ]
    		)
		end

  def save_activity_logs
		user_audit=@admin_user.audits.where(created_at: (Time.now-20)..Time.now) if @admin_user.try(:audits).present?
		profile_audit=@admin_user.profile.audits.where(created_at: (Time.now-20)..Time.now) if @admin_user.try(:audits).present?
		new_hash={}
		if user_audit.present?
			changes=user_audit.try(:first).try(:audited_changes)
			changes.to_hash.each_pair do |k,v|
				new_hash.merge!({"#{k}" => v})
			end
		end
		if profile_audit.present?
			profile_audit=profile_audit.try(:first).try(:audited_changes)
			profile_audit.to_hash.each_pair do |k,v|
				new_hash.merge!({"#{k}" => v})
			end
		end
		ip_address = request.remote_ip.present? ? request.remote_ip : request.env['REMOTE_ADDR']
		new_hash=new_hash.to_json
		ActivityLog.create(note:new_hash,activity_type: :affiliate_activity,user_id:@admin_user.try(:id),host:request.env["SERVER_NAME"],url: request.url,browser: request.env['HTTP_USER_AGENT'],ip_address: ip_address, action:action_name,action_type:'affiliate_program' )
	end
end