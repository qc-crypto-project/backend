class Admins::SalesController < AdminsController


  def index
    @header = "Sale Requests"
    conditions = []
    if params[:q].present?
      if params[:q][:batch_date].present?
        date = parse_date(params[:q][:batch_date])
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00,0)
        second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59,0)
        batch_start_date = first_date.to_datetime.in_time_zone("Pacific Time (US & Canada)").utc
        batch_end_date = second_date.to_datetime.in_time_zone("Pacific Time (US & Canada)").utc
        conditions = ['created_at BETWEEN ? AND ?', batch_start_date, batch_end_date]
      end
    end
    batches_array = Batch.qr_scan.where(conditions).order(created_at: :desc).group_by{|b| b.created_at}
    @batches =  Kaminari.paginate_array(batches_array,total_count: batches_array.try(:length)).page(params[:page]).per(5)
    render 'batches'
  end

  def batch_detail
    @batch = Batch.find_by(id: params[:id])
    @type = @batch.batch_detail["#{params[:type]}"]
    @batch_date = @batch.created_at.strftime("%Y-%m-%d")
    conditions = []
    if params[:q].present?
      params[:q] = params[:q].try(:strip)
      parameters = []
      conditions << "users.name = ?"
      parameters << params[:q]
      conditions << "requests.amount LIKE ?"
      parameters << params[:q]
      conditions = [conditions.join(" OR "), *parameters]
    end
    @all_request = Request.qr_code.where(id: @type["id"]).group_by{|r| r.status}
    @requests = Request.joins(:sender,:reciever).qr_code.where(id: @type["id"]).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(params[:filter] || 10)
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js
      format.html
      format.csv { send_data @requests.except(:limit, :offset).to_csv(cookies[:timezone]), filename: "qr_requests-#{@batch_date}.csv" }
    end
  end

  def qr_refund
    @header = "QR Pending Refunds"
    @requests = Request.qr_code.where(status: "in_progress", created_at: 60.minutes.ago..Time.now).order(created_at: :desc).per_page_kaminari(params[:page]).per(10)
    render 'index'
  end

  def edit
    @request = Request.find_by(id: params[:id])
    respond_to :js
  end

  def update
    ids = []
    if params[:refund_all].present?
      ids = Request.joins(:qr_card).where('requests.status = ? AND requests.request_type LIKE ?','in_progress', "qr_code").pluck("qr_cards.id")
    else
      request = Request.find_by(id: params[:id])
      ids = request.try(:qr_card).try(:id)
    end
    fee = apply_fee(params)
    job_id = RefundTransactionWorker.perform_async(ids,fee)
    flash[:notice] =  job_id.present? ? "Please wait, Refunds are in progress!" : "Something Went Wrong!"
    redirect_back(fallback_location: root_path)
  end

  def refund_all
    if params[:id].present?
      @batch = Batch.find_by(id: params[:id])
      @type = @batch.batch_detail["#{params[:type]}"]
      @requests = Request.qr_code.where.not(created_at: 60.minutes.ago..Time.now).where(id: @type["id"],status: "in_progress")
    else
      @requests = Request.qr_code.where(status: "in_progress")
    end
    respond_to :js
  end

  private

  def apply_fee(params)
    return params[:apply_fee].present? && params[:apply_fee] == "on" ? true : false
  end

end