class Admins::ChecksController < AdminsController
  include ApplicationHelper
  include CheckbookHelper
  include UsersHelper
  include WithdrawHandler
  layout 'admin'
  before_action :check_admin
  # before_action :set_config

  class AdminCheckError < StandardError; end

  def index
    order_types = [:check, :bulk_check, :debit_card_deposit]
    @check_batches = TangoOrder.checks_list(order_types)
    @check_batches = @check_batches.to_a.reverse.to_h if @check_batches.present?
    @issue_checks = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @issue_checks = AppConfig.new(key: AppConfig::Key::IssueCheckConfig) unless @issue_checks.present?
  end

  def batch_checks
    @batch_date = Date.today
    order_types = [:check, :bulk_check, :debit_card_deposit]
    params[:batch_search_params] = JSON(params[:batch_search_params]) if params[:batch_search_params].present?
    if params[:q].present?
      params[:q_value] = params[:q].try(:strip) if params[:q_value].blank?
      params[:q] = {"name_or_send_via_or_recipient_or_user_name_cont" => params[:q_value]}
      params[:q][:id_eq] = params[:q_value]
      @checks = getting_filtered_check(params)
      @grouped_checks = @checks.group_by{|check| check.status}
    elsif params[:from_filter] == "true"
      @checks = getting_filtered_check(params)
    elsif params[:batch_search_params].present?
      @checks = getting_batch_searched_checks(params)
    elsif params[:batch_date]
      @batch_date = params[:batch_date]
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      params[:all_checks] = "on"
      @checks = TangoOrder.order_type(order_types).batch_date(batch_start_date..batch_end_date).order_by
      @check_ids = @checks.pluck(:id)
      notifications = current_user.notifications.where(action: 'check').unread
      notifications.update_all(read_at: DateTime.now.utc)
    else
      params[:all_checks] = "on"
      @checks = TangoOrder.includes(:user).order_type(order_types).order_by
    end
    @grouped_checks = @checks.group_by{|check| check.status}
    @pending_achs = @grouped_checks[I18n.t("withdraw.pending")].try(:count)
    @total_pending_amount = @grouped_checks[I18n.t("withdraw.pending")].try(:pluck, :actual_amount).try(:sum, &:to_f)
    @total_process_achs = @grouped_checks.select{|status, check| status != I18n.t("withdraw.pending") && status != I18n.t("withdraw.failed") && status != I18n.t("withdraw.void")}.try(:values).try(:flatten).try(:pluck, :actual_amount).try(:sum, &:to_f)
    @paid_achs = @grouped_checks[I18n.t("withdraw.paid")].try(:count)
    @paid_by_wire_achs = @grouped_checks[I18n.t("withdraw.paid_by_wire")].try(:count)
    @failed_achs = @grouped_checks[I18n.t("withdraw.failed")].try(:count)
    @total_withdraw = @checks.pluck(:actual_amount).try(:sum, &:to_f)
    @void_requests = @grouped_checks[I18n.t("withdraw.void")].try(:count)
    @issue_checks = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @issue_checks = AppConfig.new(key: AppConfig::Key::IssueCheckConfig) unless @issue_checks.present?
    @all_checks = @checks
    @checks = Kaminari.paginate_array(@checks).page(params[:page]).per(params[:filter] || 10)
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js {render 'admins/checks/filtered_checks.js.erb'}
      format.html
      format.csv { send_data generate_csv(@all_checks), filename: "checks.csv" }
    end
  end

  ############################################################


  def approve_by_instant_pay
    check = TangoOrder.find(params[:id])
    result = instant_pay_approve_func(check)
    if result[:success]
      flash[:success] = result[:message]
    else
      flash[:error] = result[:message]
    end
    return redirect_back(fallback_location: root_path )
  end

  def under_review
    id=JSON.parse(params[:id]) if params[:id].present?
    if params[:status].present? && params[:status]=='enabled'
      TangoOrder.where(id:id).update(under_review:true)
    else
      TangoOrder.where(id:id).update(under_review:false)
    end
    
  end


  ############################################################

  def check_batches
    params[:q] = {}
    if params[:query].present?
      params[:query]["find_by"] = params[:query]["find_by"].try(:strip)
    end
    if params[:query][:option].present?
      if params[:query][:option] == "id"
        params[:q][:id_eq] = params[:query]["find_by"]
      elsif params[:query][:option] == "check_number"
        params[:q][:number_eq] = params[:query]["find_by"]
      elsif params[:query][:option] == "account_number"
        params[:q][:account_number_eq] = params[:query]["find_by"]
      elsif params[:query][:option] == "name"
        params[:q][:name_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "business_name"
        params[:q][:wallet_location_business_name_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "recipient"
        params[:q][:recipient_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "actual_amount"
        params[:q][:actual_amount_eq] = params[:query]["find_by"]
      end
    end
    order_types = [:check, :bulk_check, :debit_card_deposit]
    @check_batches = TangoOrder.checks_list_with_ransack(order_types)
    @issue_checks = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @issue_checks = AppConfig.new(key: AppConfig::Key::IssueCheckConfig) unless @issue_checks.present?
    render 'admins/checks/index.html.erb'
  end

  def check_batch_date
    if params[:check_id].present?
      @check=TangoOrder.find(params[:check_id])
      datetime = Date.strptime(params[:batch_date], "%m/%d/%Y").to_datetime.end_of_day
      if @check.update(:batch_date => datetime)
        flash[:success] = "Batch Date Updated Successfully"
      end
    end
    if params[:check_ids].present?
      check_ids = JSON(params[:check_ids])
      check_ids.delete(params[:check_id])
      redirect_to admins_batch_checks_path(check_ids: check_ids,batch_date: params[:batch_date])
    else
      redirect_to admins_total_funds_path
    end
  end

  def admin_checks
    order_types = [:check, :bulk_check, :debit_card_deposit]
    @checks = TangoOrder.order_type(order_types).order(id: :desc)
    @issue_checks = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @issue_checks = AppConfig.new(key: AppConfig::Key::IssueCheckConfig) unless @issue_checks.present?
    @debit_deposit = AppConfig.where(key: AppConfig::Key::DebitCardDeposit).first
    @debit_deposit = AppConfig.new(key: AppConfig::Key::DebitCardDeposit) unless @debit_deposit.present?

    notifications = current_user.notifications.where(action: 'check').unread
    notifications.update_all(read_at: DateTime.now.utc)
  end

  def send_check
    @list=[]
    respond_to :js

  end

  def get_fee_sum
    if params[:amount].to_f > 0 && params[:wallet].present?
      fee_total = 0
      amount = params[:amount].to_f
      @wallet = Wallet.find(params[:wallet])
      location = @wallet.location
      @user=Wallet.find(params[:wallet]).users.first
      fee_object = location.fees.buy_rate.first if location.present?
      if @user.MERCHANT?
        if params[:type].to_i == 0
          check_fee = fee_object.send_check.to_f if fee_object.present?
          fee_perc = deduct_fee(fee_object.redeem_fee.to_f, amount) if fee_object.present?
        elsif params[:type].to_i == 1
          check_fee = fee_object.add_money_check.to_f if fee_object.present?
          fee_perc = deduct_fee(fee_object.redeem_check.to_f, amount) if fee_object.present?
        end
        fee_sum = amount
        fee_total = check_fee + fee_perc.to_f
      elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE? || @user.affiliate_program?
        if params[:type].to_i == 0
          fee_sum,fee = deduct_sendcheck_fee(@user,amount)
        elsif params[:type].to_i == 1
          fee_sum,fee = deduct_add_money_fee(@user, amount)
        end
      elsif @user.wallets.qc_support.present?
        fee_sum = amount
      else
        fee_lib = FeeLib.new
        fee_sum = amount
        fee_total = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
      end
      render status: 200, json: {fee_sum: fee_sum, fee_total: fee_total}
    else
      render status: 200, json: {fee_sum: 0}
    end
  end

  def getUsers
    if params[:merchant_id].present?
      @check =Check.new
      id=params[:merchant_id]
      @user = User.find(id) if params[:merchant_id].present?
      email=@user.email

      @wallets = Array.new
      @list=[]
      if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
        @user.wallets.primary.each do |w|
          @wallets << w
        end
        if @user.wallets.qc_support.present?
          @wallets << @user.wallets.qc_support.first
        end
      else
        @main_user = User.find(@user.merchant_id)
        @user.wallets.primary.each do |w|
          @wallets << w
        end
        # @main_user.locations.reject {|v| v.nil?}.each do |l|
        #   l.wallets.primary.each do |w|
        #     @wallets << w
        #   end
        # end
      end
      @wallets.each do |w|
        @location = w.location
        if @location.present?
          location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: location_status
              }
        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: false
              }
        end
      end
      return render status: 200 , json:{:list => @list, :sender_email=>email}
    else
      usersList = []
      isos = User.iso.where("lower(first_name) LIKE :query", query: "#{params[:query].downcase}%") if params[:query].present?
      agents = User.agent.where("lower(first_name) LIKE :query", query: "#{params[:query].downcase}%") if params[:query].present?
      merchants = User.merchant.where("lower(name) LIKE :query", query: "#{params[:query].downcase}%") if params[:query].present?
      affiliates = User.affiliate.where("lower(name) LIKE :query", query: "#{params[:query].downcase}%") if params[:query].present?
      if isos.present?
        isos.each do |iso|
          usersList << {
              label: iso.name,
              category: "Isos",
              id: iso.id
          }
        end
      end
      if agents.present?
        agents.each do |agent|
          usersList << {
              label: agent.name,
              category: "Agents",
              id: agent.id
          }
        end
      end
      if affiliates.present?
        affiliates.each do |affiliate|
          usersList << {
              label: affiliate.name,
              category: "Affiliates",
              id: affiliate.id
          }
        end
      end
      if merchants.present?
        merchants.each do |merchant|
          usersList << {
              label: merchant.name,
              category: "Merchants",
              id: merchant.id
          }
        end
      end
      if isos.present? || agents.present? || merchants.present? || affiliates.present?
        render status: 200 , json:{:users => usersList}
      else
        render status: 200 , json:{:users => ""}
      end
    end
  end



  def process_check(response,user,amount,wallet,account_number,fee_object,dd,fee,amount_sum,check_fee,fee_perc, send_via, encrypted_card_info)
    flash_msg = ''
    @user = user
    @wallet = wallet
    digital_check = JSON.parse(response.body)
    if response.message == "CREATED"
      if @user.MERCHANT?
        fee_perc = deduct_fee(fee_object.redeem_fee.to_f, amount)
        check_fee = fee_object.send_check.to_f
        amount_sum = amount + fee["fee"].to_f
      end
      check = TangoOrder.new(user_id: @user.id, name: digital_check["name"], status: digital_check["status"], amount: amount_sum, actual_amount: amount, check_fee: check_fee, fee_perc: fee_perc, recipient: digital_check["recipient"], description: digital_check["description"], order_type: "check", approved: true, settled: false, wallet_id: @wallet.id, account_token: encrypted_card_info, from_admin: true, number: digital_check["number"], send_via: send_via)
      transaction = @user.transactions.build(to: digital_check["recipient"], from: nil, status: "approved", charge_id: nil , amount: digital_check["amount"], sender: current_user,sender_name: current_user.try(:name), action: 'retire')
      if transaction.present?
        transaction.save
      end
      if dd.present?
        check.account_number = account_number
      end
      send_check_user_info = {
          name: check.name,
          check_email: check.recipient,
          check_id: check.checkId,
          check_number: check.number
      }
      if @user.MERCHANT?
          if fee.present?
            user_info = fee["splits"]
            withdraw = withdraw(amount,@wallet.id, fee["fee"].to_f || 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
          end
      else
        if fee > 0
          withdraw = custom_withdraw(amount,@wallet.id, fee, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
        else
          withdraw = withdraw(amount,@wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
        end
      end
      if withdraw.present? && withdraw["actions"].present?
        transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", fee: withdraw["actions"].first.tags["fee"], privacy_fee: withdraw["actions"].first.tags.try(:[], "privacy_fee"), tip: withdraw["actions"].first.tags.try(:[], "tip"), reserve_money: withdraw["actions"].first.tags.try(:[], "fee_perc").try(:[], "reserve").try(:[], "amount"), tags: withdraw["actions"].first.tags, main_type: withdraw["actions"].first.tags["type"], sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
        check.transaction_id = transaction.id
        check.save
      else
        flash_msg = I18n.t('merchant.controller.unaval_feature')
      end
      flash_msg = "Successfully sent a check to #{ check.recipient }"
    else
      flash_msg = I18n.t('merchant.controller.exception3')
    end
    return flash_msg
  end

  def get_bank_by_routing_number
    # Preparing Data
    parameters = [["routing_number", "#{params[:routing_number]}"]]
    uri = URI("#{ENV['CHECKBOOK_ROUTING_URL_LIVE']}get_bank_by_routing_number")
    # Sending Request To Checkboox IO
    response = Net::HTTP.post_form( uri, parameters)
    if response.is_a?(Net::HTTPSuccess)
      if response.read_body.present?
        render status: 200 , json:{:response => response.read_body}
      else
        render status: 200 , json:{:response => ""}
      end
    else
      render status: 200 , json:{:response => ""}
    end
  end

  def show
    @check= TangoOrder.includes(:user).where(id:params[:id]).last
    @user = @check.user
    if @user.merchant? && @user.merchant_id.present?
      @parent_merchant = @user.parent_merchant
    end
    @audits = @check.audits
    respond_to :js
  end

  def approve
    check = TangoOrder.find params[:id]
    result = approve_checks(check)
    if result.present?
      if result[:success] == false
        flash[:error] = result[:message]
      else
        flash[:success] = result[:message]
      end
    end
    return redirect_back(fallback_location: root_path )
  end


  def approve_checks(check,ispaid=nil)
    return_hash = {}
    app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    type = 0
    @check = check
    amount = @check.actual_amount.to_f
    recipient = @check.recipient
    name = @check.name
    description = @check.description
    send_via = @check.send_via
    account_number = @check.account_number
    if @check.user_id.present?
      begin
        @user = User.find(@check.user_id)
        @wallet = Wallet.find(@check.wallet_id)
        location = @wallet.location
        # if location.present? && location.block_withdrawal
        #   flash[:error] = "Checks are blocked for this user"
        #   return redirect_back(fallback_location: root_path)
        # end
        if @user.present? && @wallet.present?
          amount_sum = 0
          fee_lib = FeeLib.new
          if @user.MERCHANT?
            location_fee = location.fees if location.present?
            fee_object = location_fee.buy_rate.first if location_fee.present?
            fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
            fee = fee_class.apply(amount.to_f, TypesEnumLib::TransactionType::CheckDeposit)
            amount_sum = amount + fee["fee"].to_f
          elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE? || @user.affiliate_program?
            amount_sum, fee = deduct_sendcheck_fee(@user,amount)
          else
            amount_sum = amount + fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
            check_fee = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
          end
          check = @check
          check_info = {
              name: check.name,
              check_email: check.recipient,
              check_id: check.checkId,
              check_number: check.number
          }

          url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/digital")

          @recipient=recipient
          http = Net::HTTP.new(url.host,url.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_PEER
          request = Net::HTTP::Post.new(url)
          request.body = {name: name, recipient: recipient, amount: amount,description: description}.to_json
          request["Authorization"] = "#{ENV['CHECKBOOK_KEY']}:#{ENV['CHECKBOOK_SECRET']}"
          request["Content-Type"] ='application/json'
          response = http.request(request)
          p "CHECKBOOK REQUEST RESPOSNE: ", response
          error = nil
          case response
          when Net::HTTPSuccess
            digital_check = JSON.parse(response.body)
            if response.message == "CREATED"
              if check.check?
                escrow_wallet = Wallet.check_escrow.first
                escrow_user = escrow_wallet.try(:users).try(:first)
                main_tx = check.try(:transaction)
                check_info[:check_id] = digital_check["id"]
                check_info[:check_number] = digital_check["number"]
                transaction = Transaction.create(
                    to: nil,
                    from: @wallet.id,
                    status: "pending",
                    amount: check.actual_amount,
                    sender: escrow_user,
                    sender_name: escrow_user.try(:name),
                    receiver_wallet_id: @wallet.id,
                    sender_wallet_id: escrow_wallet.try(:id),
                    sender_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                    receiver_balance: SequenceLib.balance(@wallet.try(:id)),
                    action: 'retire',
                    net_amount: check.actual_amount.to_f,
                    total_amount: check.amount.to_f,
                    gbox_fee: main_tx.try(:gbox_fee),
                    iso_fee: main_tx.try(:iso_fee),
                    affiliate_fee: main_tx.try(:affiliate_fee),
                    agent_fee: main_tx.try(:agent_fee),
                    fee: main_tx.try(:fee),
                    net_fee: main_tx.try(:net_fee),
                    ip: get_ip,
                    main_type: TypesEnumLib::TransactionType::CheckDeposit,
                )
                if @user.MERCHANT?
                  issue = issue_checkbook_amount(check.actual_amount, check.wallet_id ,TypesEnumLib::TransactionType::CheckDeposit,TypesEnumLib::GatewayType::Checkbook, fee["fee"],fee["splits"], check_info,check,true,nil,nil,ispaid)
                elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
                  issue = issue_checkbook_amount(check.actual_amount, check.wallet_id ,TypesEnumLib::TransactionType::CheckDeposit,TypesEnumLib::GatewayType::Checkbook, fee,nil, check_info,check,true,true)
                end
                if issue.present?
                  transaction.update(
                                 seq_transaction_id: issue.actions.first.id,
                                 tags: issue.actions.first.tags,
                                 timestamp: issue.timestamp,
                                 status: "approved"
                  )
                  parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                  save_block_trans(parsed_transactions,"sub","UNPAID") if parsed_transactions.present?
                  check.void_transaction_id = transaction.id
                end
              end
              check.checkId = digital_check["id"]
              check.number = digital_check["number"]
              check.catalog_image = digital_check["image_uri"]
              check.status = digital_check["status"]
              check.approved = true
              check.amount_escrow = false
              check.bulk_check.update(:status => "Success", :response_message => response.body) if check.bulk_check.present?
              check.save
              # flash[:success] = "Successfully approve a check to #{ check.recipient }"
              return_hash = {success: true, message: "Succesfully approve a check to #{ check.recipient }" }
            else
              raise I18n.t('merchant.controller.exception3')
            end
            # return redirect_back(fallback_location: root_path)
          when Net::HTTPUnauthorized
            error = JSON.parse(response.body)
            raise I18n.t 'merchant.controller.unauthorize_access'
          when Net::HTTPNotFound
            error = JSON.parse(response.body)
            raise I18n.t('merchant.controller.record_notfound')
          when Net::HTTPServerError
            error = JSON.parse(response.body)
            raise I18n.t('merchant.controller.server_notrespnd')
          else
            error = JSON.parse(response.body)
            unless [I18n.t('checkbook.error.insufficient_funds'),I18n.t('checkbook.error.limit_exceed')].include? error["error"]
              if @wallet.location.present?
                location = location_to_parse(@wallet.location)
                merchant = merchant_to_parse(@wallet.location.merchant)
              elsif @wallet.try('users').try('first').present? &&  @wallet.try('users').try('first').role!='merchant'
                location=nil
                merchant = merchant_to_parse(@wallet.users.first)
              end
              tags = {
                  "location" => location,
                  "merchant" => merchant,
                  "send_check_user_info" => check_info,
              }
              check = create_failed_withdrawal_transaction(check,@user,@wallet,tags,check_info)
              check.status = "FAILED"
              check.approved = true
              check.settled = true
              check.save
            end
            raise "#{error["error"]}"
          end
        else
          raise I18n.t('merchant.controller.exception3')
        end
      rescue => ex
        p "CHECKBOOK.IO ERROR THROWN: ", ex
        msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
        handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
        return_hash = {success: false, message: ex.message}
      end
    else
      return_hash = {success: false, message: "Please select a wallet and try again " }
    end
    return return_hash
  end

################################################################

  def approve_debit_card_deposit
    if params[:id].present?
      instants = params[:id].split(',')
      instants.each do|c|

      @check = TangoOrder.find_by(id: c.to_i)
      # @check = TangoOrder.find(params[:id])
      app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
      type = 0
      amount = @check.actual_amount.to_f
      recipient = @check.recipient
      name = @check.name
      description = @check.description
      send_via = @check.send_via
      account_number = ''
      zip = ''
      account_type = ''
      cvv = ''
      address_line_1 = ''
      address_line_2 = ''
      routing_number = ''
      account_number = ''

      if @check.user_id.present?
        begin
          @user = User.find(@check.user_id)
          @wallet = Wallet.find(@check.wallet_id)
          # balance = show_balance(@wallet.id, @user.ledger).to_f
          location = @wallet.location
          if location.present? && location.block_withdrawal
            flash[:error] = "Checks are blocked for this user"
            return redirect_back(fallback_location: root_path)
          end

          if @user.present? && @wallet.present?
            url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/instant")
            @recipient=recipient
            http = Net::HTTP.new(url.host,url.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_PEER
            request = Net::HTTP::Post.new(url)
            decrypted = AESCrypt.decrypt("#{@check.user_id}-#{ENV['CARD_ENCRYPTER']}-#{@check.wallet_id}}",  @check.account_token)
            data = JSON(decrypted)
            card_number= data["card_number"]
            request.body = {name: name, recipient: recipient, amount: amount, description: description, card_number: card_number, expiration_date: data["expiry_date"], zip: zip, account_type: account_type, cvv: cvv, address_line_1: address_line_1, address_line_2: address_line_2, routing_number: routing_number, account_number: account_number }.to_json
            request["Authorization"] = "#{ENV['CHECKBOOK_KEY']}:#{ENV['CHECKBOOK_SECRET']}"
            # request["Authorization"] = @@check_book_key+":"+@@check_book_secret
            request["Content-Type"] ='application/json'
            response = http.request(request)

            p "CHECKBOOK REQUEST RESPOSNE: ", response
            error = nil
            case response
              when Net::HTTPSuccess
                digital_check = JSON.parse(response.body)
                if response.message == "CREATED"
                  check = @check
                  check.checkId = digital_check["id"]
                  check.number = digital_check["number"]
                  check.catalog_image = digital_check["image_uri"]
                  check.status = digital_check["status"]
                  check.approved = true
                  check.bulk_check.update(:status => "Success", :response_message => response.body) if check.bulk_check.present?
                  check.save

                  flash[:success] = "Successfully approved an Instant Pay transaction to #{ check.recipient }"
                else
                  raise "Something went wrong, please contact your administrator."
                end
                # return redirect_back(fallback_location: root_path)
            when Net::HTTPUnauthorized
              error = JSON.parse(response.body)
              raise I18n.t 'merchant.controller.unauthorize_access'
            when Net::HTTPNotFound
              error = JSON.parse(response.body)
              raise I18n.t('merchant.controller.record_notfound')
            when Net::HTTPServerError
              error = JSON.parse(response.body)
              raise "Wrong credentials! Please create the request again with the correct user credentials or contact your administrator."
            else
              error = JSON.parse(response.body)
              raise "#{error["error"]}"
            end
          else
            raise "Something went wrong, please contact your administrator."
          end
        rescue => ex
          p "CHECKBOOK.IO ERROR THROWN: ", ex
          flash[:error] = ex.message
          msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
          handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
        end
      else
        flash[:error] = "Please select a wallet and try again "
      end
        end
    end
    return redirect_back(fallback_location: root_path )

  end


  # def approve_by_ach
  #   check = TangoOrder.find(params[:id])
  #   result = ach_approve_func(check)
  #   if result[:success]
  #     flash[:success] = result[:message]
  #   else
  #     flash[:error] = result[:message]
  #   end
  #   return redirect_back(fallback_location: root_path )
  # end

  def approve_by_instant_ach
    check = TangoOrder.find(params[:id])
    result = instant_ach_approve_func(check)
    if result[:success]
      flash[:success] = result[:message]
    else
      flash[:error] = result[:message]
    end
    return redirect_back(fallback_location: root_path )
  end

  def refresh_status  # trigger on refresh button click
    @checks = TangoOrder.where(order_type: "check" ,settled: false).or(TangoOrder.where(order_type: "bulk_check" ,settled: false))
    @checks = @checks.select{|c| c.checkId.present? }
    $remaining = @checks.count
    respond_to :js
  end

  def refreshing_check  # trigger on start processing button on modal
    begin
      @worker_id = []
      @checks = TangoOrder.where(order_type: "check",settled: false).or(TangoOrder.where(order_type: "bulk_check" ,settled: false))
      @checks = @checks.select{|c| c.checkId.present? }
      checkbook_cheqs = get_cheq()
      checkbook_statuses = checkbook_cheqs.symbolize_keys[:checks].map{|c| {checkId: c.symbolize_keys[:id], status: c.symbolize_keys[:status]} }
      order_updates = {}
      checkbook_statuses.each{|c| order_updates[TangoOrder.where(checkId: c[:checkId]).first.try(:id)] = c}
      order_updates = order_updates.delete_if { |k, v| k.nil? || v.nil? }
      void_failed = order_updates.select{|key,value| value[:status] == "VOID" || value[:status] == "FAILED" }
      other_than_void_failed = order_updates.select{|key,value| value[:status] != "VOID" || value[:status] != "FAILED" }
      if void_failed.count > 0
        void_failed.each do|key,value|
          update_check_order(TangoOrder.find_by(id: key), value[:status])
        end
      end
      if other_than_void_failed.count > 0
        TangoOrder.update(other_than_void_failed.keys, other_than_void_failed.values)
      end
      render status: 200, json:{success: 'Good'}
    rescue => ex
      p "Exception==========>> #{ex.message}"
      render status: 404, json:{success: 'Something went wrong!'}
    end
  end

  def remaining_check
    job_id = params[:job_id]
    job_status = Sidekiq::Status.get_all(job_id).symbolize_keys
    render status: 200, json:{ status: job_status[:check_status]}
  end

  def export_checks
    checks = JSON.parse(params[:checks], object_class: OpenStruct)
    send_data generate_csv(checks), filename: "checks.csv"
  end

  def update
    begin
      if params[:id].present?
        result = check_status_possibility(params[:id])
        if result[:success]
          response = withdrawal_bulk_status_update
          if response[:success]
            flash[:success] = response[:message]
          else
            flash[:error] = response[:message]
          end
        else
          flash[:error] = result[:message]
        end
      end
    rescue AdminCheckError => exc
      flash[:error] = "Sorry for Inconvinence"
    ensure
      return redirect_back(fallback_location: root_path )
    end
  end


  def edit
  end

  def set_config
    @configs = AppConfig.all
  end

  # def pay_invoice
  #   invoice = TangoOrder.find(params[:id])
  #
  #   app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
  #       wallet = Wallet.where(id: invoice.wallet_id).first if invoice.wallet_id.present?
  #       # raise StandardError.new 'No Wallet attached with Check'
  #
  #   total_fee = invoice.check_fee + invoice.fee_perc
  #       if total_fee.to_f > 0
  #         fee_lib = FeeLib.new
  #         # qc_wallet = Wallet.where(wallet_type: 3).first
  #         wallet_user = wallet.users.first
  #         company = wallet_user.company
  #         company_user =  company.users.where(email: company.company_email).first
  #         company_wallet = company_user.wallets.first.id
  #         location = wallet.location
  #         location_check_fee = location.fees.add_check.first if location.present?
  #         redeem_check = location.fees.redeem_check.first if location.present?
  #         user_info = fee_lib.divide_add_money_fee(location,location_check_fee,company,redeem_check, invoice.fee_perc , company_wallet )
  #       end
  #       if total_fee.to_f > 0
  #         issue_amount(invoice.amount,wallet,'invoice',app_config.stringValue, total_fee, user_info)
  #         # if issue.present?
  #         #   fee_lib.divide_merchant_check_fee(location_check_fee, location, company_wallet, company_user,qc_wallet, 'invoice_fee', app_config.stringValue)
  #         #   fee_lib.divide_check_fee_perc(redeem_check,location, company_wallet, company_user,qc_wallet, invoice.fee_perc, 'invoice_fee', app_config.stringValue)
  #         # end
  #       else
  #         issue_amount(invoice.amount,wallet,'invoice',app_config.stringValue, 0)
  #       end
  #
  #       invoice.update(status: 'PAID')
  #       redirect_to admins_invoices_path
  # end

  #= checkbook worker method
  def get_cheq()
    begin
      url =URI("#{ENV['CHECKBOOK_URL_LIVE']}/check")
      http = Net::HTTP.new(url.host,url.port)
      http.use_ssl =true
      http.verify_mode =OpenSSL::SSL::VERIFY_PEER
      request= Net::HTTP::Get.new(url)
      request["Authorization"]=ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
      request["Content-Type"]='application/json'
      response =http.request(request)
      case response
      when Net::HTTPSuccess
        cheque = JSON.parse(response.body, class: OpenStruct)
        return cheque
      when Net::HTTPUnauthorized
        return false
      when Net::HTTPNotFound
        return false
      when Net::HTTPServerError
        return false
      else
        return false
      end
    rescue => ex
      return false
    end
  end

  def validation(check, result)
    check_status = result["status"]
    app_config = AppConfig.where(key: AppConfig::Key::PaymentProcessor).first
    wallet = Wallet.where(id: check.wallet_id).first if check.wallet_id.present?
    if wallet.present?
      check_info = {
          name: check.name,
          check_email: check.recipient,
          check_id: check.checkId,
          check_number: check.number
      }
      if check.status == "UNPAID"
        if check_status == "PAID"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "FAILED"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "VOID"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "IN_PROCESS"
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == 'UNPAID'
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == "PRINTED"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        end
      elsif check.status == "IN_PROCESS"
        if check_status == "PAID"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "FAILED"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "VOID"
          issue_checkbook_amount(check.amount, wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        elsif check_status == "IN_PROCESS"
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == 'UNPAID'
          check.update(status: result["status"], settled: false) if result["status"].present? && check.present?
        elsif check_status == "PRINTED"
          check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
        end
      elsif check_status == "PRINTED"
        check.update(status: result["status"], settled: true) if result["status"].present? && check.present?
      end
      # if check_status == "RETURNED"
      #   issue_checkbook_amount(check.amount, wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
      # elsif check_status == "FAILED"
      #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0,nil, check_info)
      # elsif check_status == "VOID"
      #   # if check.status != 'VOID'
      #   issue_checkbook_amount(check.amount, wallet,'send_check',app_config.stringValue, 0, nil, check_info)
      #   # end
      # elsif check_status == "EXPIRED"
      #   issue_checkbook_amount(check.amount,wallet ,'send_check',app_config.stringValue, 0, nil, check_info)
      # else
      #   puts "no if"
      # end
    end
  end

  def bulk_status_change
    ids = params[:ids]
    ids = ids.split(",").map { |s| s.to_i }
    params[:type] = params[:check_status]
    false_achs = []
    result = nil
    ids.each do |id|
      check = TangoOrder.find_by(id: id)
      params[:id] = id
      if params[:type] != "FAILED" && (check.status == "PAID" || check.status == "PAID_WIRE")
        false_achs.push(check.id)
      elsif check.status.try(:downcase) === 'failed'
        false_achs.push(check.id)
      elsif check.status == "VOID"
        false_achs.push(check.id)
      elsif params[:type] == "VOID" && (check.status == "IN_PROCESS" || check.status == "IN_PROGRESS")
        false_achs.push(check.id)
      elsif check.status == "PENDING" && !(params[:type] == "IN_PROCESS" || params[:type] == "VOID" )
        false_achs.push(check.id)
      elsif params[:type] == "IN_PROCESS" && (check.status == "VOID" || check.status == "IN_PROCESS" || check.status == "IN_PROGRESS")
        false_achs.push(check.id)
      elsif (params[:type] == "IN_PROCESS" && check.check? ) && (check.status == "VOID" || check.status == "UNPAID" || check.status == "UNPAID")
        false_achs.push(check.id)
      else
        if params[:type] == "PAID" && (check.status == "IN_PROCESS" || check.status == "IN_PROGRESS")
          ispaid = true
          result = approve_checks(check,ispaid)
        else
          result = withdrawal_bulk_status_update
        end
      end
      if result.present?
        if result[:success] == false
          flash[:error] = result[:message]
        else
          flash[:success] = result[:message]
        end
      end
    end
    false_achs = false_achs.join(',')
    if false_achs.length > 0
      flash[:error] = "The Selected Status can not be applied to ID #{false_achs}. Please remove it and try again later!"
    end
    return redirect_back(fallback_location: root_path )
  end

  def check_status_possibility(id)
    check = TangoOrder.find_by(id: id)
    response = {}
    if params[:type] == "FAILED" && (check.status == "PAID" || check.status == "PAID_WIRE") && !check.ach_gateway.try(:manual_integration?)
      response = {success: false, message: "The Selected Status can not be applied to ID #{id}. Please remove it and try again later!"}
    else
      response = {success: true, message: ""}
    end
    response
  end
  private
  def getting_filtered_check(params)
    search_query_params
    if params[:all_checks].present?
      statuses = ["PAID", "UNPAID","VOID","IN_PROCESS","PENDING","PRINTED","PAID_WIRE","IN_PROGRESS", "FAILED"]
      if params[:q].present?
        if params[:q_value].present? && params[:q_value].include?("-")
          user_id = params[:q_value].split("-").last
          params[:q][:user_id_eq] = user_id
        else
          params[:q][:user_id_eq] = params[:q_value] if params[:q_value].to_i != 0
        end
        params[:q][:number_eq] = params[:q_value] if params[:q_value].to_i != 0
        params[:q][:actual_amount_eq] = params[:q_value] if params[:q_value].to_i != 0
        params[:q][:amount_eq] = params[:q_value] if params[:q_value].to_i != 0
        params[:q][:id_eq] = params[:q_value] if params[:q_value].to_i != 0
      else
        params[:q] = {}
      end
    else
      if params[:q].present?
        if params[:q_value].present? && params[:q_value].include?("-")
          user_id = params[:q_value].split("-").last
          params[:q][:user_id_eq] = user_id
        else
          params[:q][:user_id_eq] = params[:q_value] if params[:q_value].to_i != 0
        end
        params[:q][:number_eq] = params[:q_value] if params[:q_value].to_i != 0
        params[:q][:actual_amount_eq] = params[:q_value] if params[:q_value].to_i != 0
        params[:q][:id_eq] = params[:q_value] if params[:q_value].to_i != 0
      else
        params[:q] = {}
      end
      statuses = []
      if params[:paid_checks].present?
        statuses.push("PAID")
      end
      if params[:unpaid_checks].present?
        statuses.push("UNPAID")
      end
      if params[:void_checks].present?
        statuses.push("VOID")
      end
      if params[:inprogress_checks].present?
        statuses.push("IN_PROGRESS")
      end
      if params[:printed_checks].present?
        statuses.push("PRINTED")
      end
      if params[:pending_checks].present?
        statuses.push("PENDING")
      end
      if params[:paid_wire].present?
        statuses.push("PAID_WIRE")
      end
      if params[:failed].present?
        statuses.push("FAILED")
      end
    end
    @checks = TangoOrder.includes(:user).order_type([:check, :bulk_check, :debit_card_deposit]).ransack(params[:q].try(:merge, m: 'or')).result
    if params[:batch_date].present?
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      @checks = @checks.batch_date(batch_start_date..batch_end_date).status(statuses)
      @checks = @checks.select{|check| statuses.include?(check.status)} if statuses.present?
    else
      @checks = @checks.select{|check| statuses.include?(check.status)} if statuses.present?
    end
    return @checks.reverse
  end

  def generate_csv(checks)
    CSV.generate do |csv|
      csv << %w{ id check_number send_via user_id name recipient amount creation_date status dba_flag_text}
      checks.each do |c|
          csv << [c.id, c.number.blank? ? "-" : c.number, c.send_via.present? ? c.send_via : " - ", c.user_id,c.name,c.recipient, number_to_currency(number_with_precision(c.actual_amount.to_f, :precision => 2)), c.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y %r"), c.status, c.wallet.try(:location).try(:ach_flag_text)]
      end
    end
  end

  def getting_batch_searched_checks(params)
    search_query_params
    order_types = [:check, :bulk_check, :debit_card_deposit]
    checks = TangoOrder.includes(:user).order_type(order_types).ransack(params[:q]).result
    if params[:batch_date].present?
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      @checks = checks.batch_date(batch_start_date..batch_end_date).order(id: :desc)
    else
      @checks = checks.order(id: :desc)
    end
    return @checks
  end

  def search_query_params
    params[:q] = {} if params[:q].blank?
    if params[:batch_search_params].present?
      find_by = params[:batch_search_params][:find_by].try(:strip)
      option = params[:batch_search_params][:option]
    end
    if params[:batch_search_params].try(:[], "status").present?
      if params[:batch_search_params][:status] == "IN_PROCESS"
        params[:q][:status_in] = ["IN_PROGRESS", "UNPAID"]
      else
        params[:q][:status_cont] = params[:batch_search_params][:status]
      end
    end
    if option.present?
      if option == "id"
        params[:q][:id_eq] = find_by
      elsif option == "check_number"
        params[:q][:number_eq] = find_by
      elsif option == "account_number"
        params[:q][:account_number_eq] = find_by
      elsif option == "name"
        params[:q][:name_cont] = find_by
      elsif option == "business_name"
        params[:q][:wallet_location_business_name_cont] = find_by
      elsif option == "recipient"
        params[:q][:recipient_cont] = find_by
      elsif option == "amount"
        params[:q][:actual_amount_eq] = find_by
      elsif params[:q][:option] == "merchant_id"
        params[:q][:user_id_eq] = find_by
      end
    end
  end
end
