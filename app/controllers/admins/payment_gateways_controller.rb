class Admins::PaymentGatewaysController < ApplicationController
  include ApplicationHelper
  layout 'admin'
  before_action :set_admins_payment_gateway, only: [:show, :edit, :update]
  before_action :check_admin

  def index
    # @app_config = AppConfig.where(key: AppConfig::Key::MidDisable).first

      name = "%#{params[:search].downcase}%" if params[:search].present?
      search = params[:search].present? ? ["lower(descriptors.name) ILIKE ? OR lower(payment_gateways.name) ILIKE ?", name, name] : []
      @descriptors = Descriptor.includes(:payment_gateways).references(:payment_gateways).where(search).order(id: :desc).per_page_kaminari(params[:page]).per(3)
      status =  params[:type] == "archived" ? ["payment_gateways.is_deleted = ? AND payment_gateways.status = ?", true, "archived"] : []
      @descriptors = Descriptor.includes(:payment_gateways).references(:payment_gateways).where(search).where(status).order(id: :desc).per_page_kaminari(params[:page]).per(3)
      # @descriptors = Descriptor.includes(:payment_gateways).references(:payment_gateways).all.order(id: :desc).per_page_kaminari(params[:page]).per(3)

    # if params[:search].present?
    #   name = "%#{params[:search].downcase}%"
    #   @descriptors = Descriptor.includes(:payment_gateways).references(:payment_gateways).where("lower(descriptors.name) ILIKE ? OR lower(payment_gateways.name) ILIKE ?", name, name).order(id: :desc).per_page_kaminari(params[:page]).per(3)
    #   if params[:type] == "archived"
    #     admins_payment_gateways = @descriptors.map{|x| x.payment_gateways.archived_gateways.main_gateways.where("lower(name) ILIKE ?", name) }.flatten.uniq
    #   else
    #     admins_payment_gateways = @descriptors.map{|x| x.payment_gateways.active_gateways.main_gateways.where("lower(name) ILIKE ?", name) }.flatten.uniq
    #   end
    #   @search_gateways = admins_payment_gateways.group_by{|x| x.descriptor_id}
    # else
    #   @descriptors = Descriptor.includes(:payment_gateways).references(:payment_gateways).all.order(id: :desc).per_page_kaminari(params[:page]).per(3)
    #   if params[:type] == "archived"
    #     admins_payment_gateways = @descriptors.map{|x| x.payment_gateways.archived_gateways.main_gateways}.flatten.uniq
    #   else
    #     admins_payment_gateways = @descriptors.map{|x| x.payment_gateways.active_gateways.main_gateways}.flatten.uniq
    #   end
    # end
    # start_of_month = DateTime.now.in_time_zone("Pacific Time (US & Canada)").at_beginning_of_month
    # start_of_today = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day

    # Query only above limited payment gateway transactions
    # transactions = Transaction.where('created_at >= :date AND main_type IN (:main_types) AND status IN (:statuses)', date: start_of_month, main_types: [TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::Issue3DS], statuses: ["approved", "refunded","complete"]).where('payment_gateway_id IN (:ids)', ids: admins_payment_gateways.pluck(:id)).select(:id, :created_at, :total_amount, :payment_gateway_id).group_by(&:payment_gateway_id)
    # disputes = DisputeCase.select(:id, :payment_gateway_id).where('created_at >= :date', date: start_of_month).group_by(&:payment_gateway_id)

    # Prepare Monthly/Daily Processed amounts
    # @total_transactions = {}
    # @monthly_processed = {}
    # @today_processed = {}
    # @cbks = {}
    # admins_payment_gateways.each do |gateway|
    #   @total_transactions[gateway.id] = transactions[gateway.id].try(:count) || 0
    #   @monthly_processed[gateway.id] = transactions[gateway.id].try(:pluck, :total_amount).try(:sum).to_f
    #   today_trs = transactions[gateway.id]&.select{|trx| trx.created_at >= start_of_today} || []
    #   @today_processed[gateway.id] = today_trs.try(:p luck, :total_amount).try(:sum).to_f
    #   @cbks[gateway.id] = disputes[gateway.id].try(:count) || 0
    # end
  end

  def specific_merchant
    payment_gateway_id = params[:payment_gateway_id]
    redirect_to merchants_path(payment_gateway_id: payment_gateway_id, from_specific_merchant: true)
  end

  def show
  end

  def new
    @payment_gateway = PaymentGateway.new
    @misc = @payment_gateway.misc_fees.build
    @transaction_types = @payment_gateway.transaction_types.build
    @vertical_types = @payment_gateway.vertical_types.build
    @fee = @payment_gateway.build_fee
  end

  def edit
    @payment_gateway = PaymentGateway.find_by(id: params[:id])
    @misc_fees = @payment_gateway.misc_fees
    if @misc_fees.blank?
      @misc_fee = @payment_gateway.misc_fees.build
    end
    @transaction_types = @payment_gateway.transaction_types
    if @transaction_types.blank?
      @transaction_types = @payment_gateway.transaction_types.build
    end
    @vertical_types = @payment_gateway.vertical_types
    if @vertical_types.blank?
      @transaction_types = @payment_gateway.vertical_types.build
    end
    strip_empty_params(@payment_gateway) if @payment_gateway.present?
    @payment_gateway.client_secret = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'], @payment_gateway.client_secret) if @payment_gateway.client_secret.present?
    @payment_gateway.client_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'], @payment_gateway.client_id) if @payment_gateway.client_id.present?
    @payment_gateway.authentication_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'], @payment_gateway.authentication_id) if @payment_gateway.authentication_id.present?
    @payment_gateway.signature_maker = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'], @payment_gateway.signature_maker) if @payment_gateway.signature_maker.present?
    @payment_gateway.processor_id = AESCrypt.decrypt(ENV['SECRET_KEY_GATEWAY'], @payment_gateway.processor_id) if @payment_gateway.processor_id.present?
    @old = params[:old]
    respond_to :js
  end

  def strip_empty_params(params)
    if params[:misc_fees_attributes].present? && !params[:misc_fees_attributes].blank?
      params[:misc_fees_attributes].reject! { |key,value| value["name"].blank? && value["percentage_fee"].blank? && value["dollar_fee"].blank? }
    end
    if params[:transaction_types_attributes].present? && !params[:transaction_types_attributes].blank?
      params[:transaction_types_attributes].reject! { |key,value| value["name"].blank? }
    end
    if params[:vertical_types_attributes].present? && !params[:vertical_types_attributes].blank?
      params[:vertical_types_attributes].reject! { |key,value| value["title"].blank? }
    end
  end

  def create
    setting_fee_date
    payment_gateway = params[:payment_gateway].slice(:name,:client_secret,:client_id,:authentication_id,:signature_maker,:key)
    payment_gateway.each { |key, value| value.strip! unless value.blank? && value.is_a?(Array)}
    params[:payment_gateway][:client_secret] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_secret]) if params[:payment_gateway][:client_secret].present?
    params[:payment_gateway][:client_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_id]) if params[:payment_gateway][:client_id].present?
    params[:payment_gateway][:authentication_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:authentication_id]) if params[:payment_gateway][:authentication_id].present?
    params[:payment_gateway][:signature_maker] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:signature_maker]) if params[:payment_gateway][:signature_maker].present?
    params[:payment_gateway][:processor_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:processor_id]) if params[:payment_gateway][:processor_id].present?
    strip_empty_params(params[:payment_gateway]) if params[:payment_gateway].present?
    @admins_payment_gateway = PaymentGateway.new(admins_payment_gateway_params)
    @admins_payment_gateway.card_selection = setting_for_card(params["card_selection"],PaymentGateway.card_types)
    respond_to do |format|
      if @admins_payment_gateway.save
        descriptor = Descriptor.find(params[:payment_gateway][:descriptor_id])
        update_descriptor(descriptor)
        if @admins_payment_gateway.ficer?
          @admins_payment_gateway.fee.buy_rate!
          @admins_payment_gateway.fee.low!
        end
        flash[:notice] = "New Payment Gateway Successfully Created!"
      else
        flash[:notice] = "Something Went Wrong While Creating New Payment Gateway!"
      end
      format.html { redirect_to admins_payment_gateways_path}
    end
  end

  def update
    setting_fee_date
    payment_gateway = params[:payment_gateway].slice(:name,:client_secret,:client_id,:authentication_id,:signature_maker,:key)
    payment_gateway.each { |key, value| value.strip! unless value.blank? && value.is_a?(Array)}
    params[:payment_gateway][:client_secret] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_secret]) if params[:payment_gateway][:client_secret].present?
    params[:payment_gateway][:client_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_id]) if params[:payment_gateway][:client_id].present?
    params[:payment_gateway][:authentication_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:authentication_id]) if params[:payment_gateway][:authentication_id].present?
    params[:payment_gateway][:signature_maker] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:signature_maker]) if params[:payment_gateway][:signature_maker].present?
    params[:payment_gateway][:processor_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:processor_id]) if params[:payment_gateway][:processor_id].present?
    strip_empty_params(params[:payment_gateway]) if params[:payment_gateway].present?
    @admins_payment_gateway.current_user = current_user
    @admins_payment_gateway.request_ip = request.try(:ip)
    respond_to do |format|
      if @admins_payment_gateway.update(admins_payment_gateway_params)
        if @admins_payment_gateway.daily_limit.to_f > @admins_payment_gateway.daily_volume_achived.to_f
          @admins_payment_gateway.blocked_by_load_balancer = false
          @admins_payment_gateway.save
        end
        descriptor = Descriptor.find_by(id: params[:payment_gateway][:descriptor_id])
        update_descriptor(descriptor)
        @admins_payment_gateway.update(card_selection: setting_for_card(params["card_selection"],PaymentGateway.card_types))
        save_payment_logs
        flash[:notice] = "Payment Gateway Successfully Updated!"
      else
        if @admins_payment_gateway.errors.present?
          flash[:notice] = @admins_payment_gateway.errors.full_messages.to_sentence
        else
          flash[:notice] = "Something Went Wrong While Updating New Payment Gateway!"
        end
      end
      format.html { redirect_back(fallback_location: root_path)}
    end
  end

  #functions for adding new descriptor start
  def new_descriptor
    @descriptor = Descriptor.where(id: params[:id]).first_or_initialize
  end

  def create_descriptor
    descriptor = Descriptor.where(id: params[:descriptor][:id]).first_or_initialize
    descriptor.name = params[:descriptor][:name]
    respond_to do |format|
      if descriptor.save
        flash[:notice] = "Gateway Saved Successfully!"
      else
        if descriptor.errors.present?
          flash[:notice] = descriptor.errors.full_messages.to_sentence
        else
          flash[:notice] = "Something Went Wrong While Creating New Gateway!"
        end
      end
      format.html { redirect_to admins_payment_gateways_path}
    end
  end

  # functions for adding new descriptor end

  def old_gateways
    @payment_gateways = PaymentGateway.main_gateways.active_gateways.where(descriptor_id: nil).per_page_kaminari(params[:page]).per(10)
  end

  def setting_fee_date
    if params[:payment_gateway].present?
      if params[:payment_gateway]["fee_attributes"].present?
        if params[:payment_gateway]["fee_attributes"]["misc_date"].present?
          params[:payment_gateway]["fee_attributes"]["misc_date"] = Date.strptime(params[:payment_gateway]["fee_attributes"]["misc_date"], '%m/%d/%Y')
        end
        if params[:payment_gateway]["fee_attributes"]["service_date"].present?
          params[:payment_gateway]["fee_attributes"]["service_date"] = Date.strptime(params[:payment_gateway]["fee_attributes"]["service_date"], '%m/%d/%Y')
        end
        if params[:payment_gateway]["fee_attributes"]["statement_date"].present?
          params[:payment_gateway]["fee_attributes"]["statement_date"] = Date.strptime(params[:payment_gateway]["fee_attributes"]["statement_date"], '%m/%d/%Y')
        end
      end
    end
  end

  def setting_for_card(params,actual)
    temp_hash = {}
    actual.each do|ak,av|
      if params.present? && params[ak].present?
        temp_hash.merge!({ak=>"on"})
      else
        temp_hash.merge!({ak=>"off"})
      end
    end
    return temp_hash
  end

  def archive
    @payment_gateway = PaymentGateway.find_by(id: params[:id])
    if @payment_gateway&.is_deleted?
      @payment_gateway.update(status: params[:status] || "active",is_deleted: false, is_block: false)
      ActivityLog.create(params:{'gateway_id'=>@payment_gateway.try(:id),'name'=>@payment_gateway.try(:name)},note:"Gateway is #{params[:status]}",activity_type: "user_activity",user_id:current_user.id,action:'update',ip_address:request.ip)
      flash[:notice] = "Gateway Unarchived Successfully!"
    elsif !@payment_gateway&.is_deleted?
      check_loc = Location.have_gateway(@payment_gateway.id)
      if check_loc == []
        is_block = params[:status] == "active" ? false : true
        deleted = params[:status] == "archived" ? true : false
        @payment_gateway.update(status: params[:status], is_block: is_block, is_deleted: deleted)
        message = params[:status] == "active" ? "Active" : params[:status] == "in_active" ? "In Active" : params[:status] == "terminated" ? "Terminated" : "Archived"
        ActivityLog.create(params:{'gateway_id'=>@payment_gateway.try(:id),'name'=>@payment_gateway.try(:name)},note:"Gateway is #{message}",activity_type: "user_activity",user_id:current_user.id,action:'update',ip_address:request.ip)
        flash[:notice] = "Gateway #{message} Successfully!"
      else
        flash[:notice] = "Error: Merchant is assigned to this Gateway"
      end
    end
    redirect_to admins_payment_gateways_path
  end

  def postverticaltype
    @vertical_type = VerticalType.create(title: params[:title])
    if params["pay_id"].present? && params["pay_id"] != "0"
      gate =PaymentGateway.find_by(id: params["pay_id"].to_i).update(vertical_type_id: @vertical_type.id)
    end
    if @vertical_type.present?
      render json:{id:@vertical_type.id,title:@vertical_type.title}
    else
      render json:{message:"Fill Up Your Fields!"}
    end
  end

  def block_gateways
    @payment_gateway = PaymentGateway.find_by(id: params[:id])
    if @payment_gateway.is_block?
      @payment_gateway.update(is_block: false)
      flash[:notice] = "Gateway Unblocked Successfully!"
    else
      @payment_gateway.update(is_block: true)
      flash[:notice] = "Gateway Blocked Successfully!"
    end
    redirect_to admins_payment_gateways_path
  end

  def archived_gateways
    @admins_payment_gateways = PaymentGateway.where(is_deleted: true)
  end

  def unique_descriptor
    gateways = 1
    descriptor = Descriptor.find_by(id: params[:id])
    gateways = descriptor.payment_gateways.where(name: params[:descriptor]).count if descriptor.present?
    if gateways == 0
      render json: true
    else
      render json: false
    end
  end

  def archive_descriptor
    if params[:status].present? && params[:id].present?
      descriptor = Descriptor.find_by(id: params[:id])
      descriptor.update(active: params[:status]) if descriptor.present?
    else
      render json: false
    end
  end

  def save_payment_logs
    # audits=@admins_payment_gateway.audits.where(created_at: (Time.now-20)..Time.now).try(:last).try(:audited_changes)
    # ActivityLog.create(params:{'gateway_id'=>@admins_payment_gateway.try(:id),'name'=>@admins_payment_gateway.try(:key)},note:audits.to_json,activity_type: "user_activity",user_id:current_user.id,action:'update',ip_address:request.ip) if audits.present?
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admins_payment_gateway
      @admins_payment_gateway = PaymentGateway.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admins_payment_gateway_params
      params.require(:payment_gateway).permit(:type, :name, :client_secret, :client_id,:authentication_id,:signature_maker,:key,:virtual_tx,
                                              :transaction_fee,:charge_back_fee,:retrieval_fee,
                                              :per_transaction_fee,:reserve_money,:reserve_money_days,:descriptor_id,
                                              :monthly_service_fee,:account_processing_limit, :misc_fee, :misc_fee_dollars, :vertical_type_id, :processor_id, :monthly_limit, :daily_limit,:cbk_max_percent,:cbk_max_count,:average_ticket,:high_ticket,:is_block,:enable_refund,
                                              misc_fees_attributes: [:id, :name, :misc_type, :dollar_fee, :percentage_fee, :_destroy],
                                              transaction_types_attributes: [:id, :name, :_destroy],
                                              vertical_types_attributes: [:id, :title, :_destroy],
                                              fee_attributes: [:id,:risk,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :fee_status,:payment_gateway_id, :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement, :service_date, :misc_date, :statement_date, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee_dollar, :ach_fee,:vmcardrate, :distransfee, :amexrate, :amextransfee, :monthlygatewayfee, :annualccsalesvol])
    end

  def descriptor_params
    params.require(:descriptor).permit(:name)
  end

  def update_descriptor(descriptor)
    gateways = descriptor.payment_gateways
    daily_limit = gateways.pluck(:daily_limit).sum(&:to_f)
    monthly_limit = gateways.pluck(:monthly_limit).sum(&:to_f)
    cbk_limit = gateways.pluck(:cbk_max_percent).sum(&:to_f)
    descriptor.update(daily_limit: daily_limit, monthly_limit: monthly_limit, cbk_limit: cbk_limit/gateways.count)
  end
end
