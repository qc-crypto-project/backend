class Admins::ReportsController < AdminsController
  include Merchant::TransactionsHelper
  include Admins::ReportsHelper
  before_action :get_offset, only: [:generate_report, :run_report]
  skip_before_action :mtrac_admin

  def index
    offset = Time.now.in_time_zone(cookies[:timezone]).strftime("%z")
    return redirect_to transactions_admins_path(trans: "success", offset: offset) if current_user.present? && current_user.support_mtrac? && params[:kind] != "wallet"
    if params[:kind] == "chargeback"
      @locations = Location.eager_load(:wallets,:merchant).where('wallets.wallet_type = ?',0)
      @gateways = PaymentGateway.select(:id, :name).order(name: :asc)
      @parameters = [
          {value: "total_tx",view: "Total Tx"},
          {value: "total_processed",view: "Total Processed"},
          {value: "received_count",view: "Received Count"},
          {value: "received_amount",view: "Received Amount"},
          {value: "cbk_perc",view: "CBK %"},
          # {value: "cbk_volume",view: "CBK Volume"},
          # {value: "in_progress",view: "In Progress"},
          # {value: "in_progress_amount",view: "In Progress Amount"},
          # {value: "cbk_won",view: "CBK Won"},
          # {value: "cbk_won_amount",view: "CBK Won Amount"},
          # {value: "cbk_lost",view: "CBK Lost"},
          # {value: "cbk_lost_amount",view: "CBK Lost Amount"},
      ]
      @users = User.merchant.pluck(:id, :name)
      @m_ids = @users.map {|row| row[0]}.sort
      @m_names = @users.map {|row| row[1]}.sort_by(&:downcase)
      @users.sort_by!{|i,n| n.downcase}
      render 'admins/reports/chargeback'
    elsif params[:kind] == "sale"
      @categories = Category.all.order(name: :asc)
      @locations = Location.includes(:wallets).all
      @filters = [10,25,50,100]
      @types = [
          {value: TypesEnumLib::TransactionViewTypes::DebitCard, view: TypesEnumLib::TransactionViewTypes::DebitCard},
          {value: TypesEnumLib::TransactionViewTypes::SaleIssue, view: TypesEnumLib::TransactionViewTypes::SaleIssue},
          {value: TypesEnumLib::TransactionViewTypes::PinDebit, view: TypesEnumLib::TransactionViewTypes::PinDebit},
          {value: TypesEnumLib::TransactionViewTypes::VirtualTerminal, view: TypesEnumLib::TransactionViewTypes::VirtualTerminal},
          {value: TypesEnumLib::TransactionViewTypes::Ecommerce, view: TypesEnumLib::TransactionViewTypes::Ecommerce},
          {value: TypesEnumLib::TransactionViewTypes::CreditCard, view: TypesEnumLib::TransactionViewTypes::CreditCard}
      ]
      render 'admins/reports/sales'
    elsif params[:kind] == "gateway"
      render 'admins/reports/gateway'
    elsif params[:kind] == "merchant"
      @locations = Location.eager_load(:wallets, :category).where("wallets.wallet_type = ?", 0)
      @categories = Category.all.order(name: :asc)
      @status = ["Active", "Inactive"]
      @filters = [10,25,50,100]
      render 'admins/reports/merchant_reports'
      if params[:from_filter] == "true"
        generate_report(params)
      end
    elsif params[:kind] == "wallet"
      @filters = [10,25,50,100]
      @categories = Category.all.order(name: :asc)
      @locations = Location.includes(:wallets).all
      @users = User.includes(:wallets).where(role: [:iso, :agent, :affiliate])
      render 'admins/reports/wallet_reports'
    elsif params[:kind] == "withdrawal"
      @filters = [10,25,50,100]
      @categories = [
          {value: t("withdraw.ACH"),view: "ACH"},
          {value: t("withdraw.send_check"), view: "Check"},
          {value: t("withdraw.p2c"), view: "Push to Card"}
      ]
      @types = [
          {value: t('withdraw.pending'),view: "Pending"},
          {value: t('withdraw.paid'), view: "Paid"},
          {value: t('withdraw.void'), view: "Void"},
          {value: t('withdraw.inprogress'), view: "In Process"},
          {value: t('withdraw.failed'), view: "Failed"}
      ]
      @ach_category = [
          {value: t('withdraw.CBD'),view: "CBD"},
          {value: t('withdraw.dispensary'), view: "Dispensary"},
          {value: t('withdraw.ACH_international'), view: "ACH International"},
          {value: t('withdraw.other'), view: "Other"}
      ]
      render 'admins/reports/withdrawal'
    elsif params[:kind] == "terminal_id"
      @categories = Category.all.order(name: :asc)
      render 'admins/reports/terminal_id'
    elsif params[:kind] == "affiliate_program"
      @locations = User.admin_user
      render 'admins/reports/affiliate_program'
    elsif params[:kind] == "cbk_detail"
      @filters = [10,25,50,100]
      render 'admins/reports/cbk_detail'
    end
  end

  def generate_report
    @type = params[:query].try(:[], "type")
    if params[:query].try(:[], "date").present?
      date = parse_date(params[:query]["date"])
      first_date = "#{date.first[:day]}-#{date.first[:month]}-#{date.first[:year]}".to_date
      second_date = "#{date.second[:day]}-#{date.second[:month].strip}-#{date.second[:year]}".to_date + 1.day
      first_date = "#{first_date.year}-#{first_date.month}-#{first_date.day} 07:00:00"
      second_date = "#{second_date.year}-#{second_date.month}-#{second_date.day} 06:59:59"
      if @type == "cbk_detail"
        received_first_date = "#{date.first[:day]}-#{date.first[:month]}-#{date.first[:year]}".to_date
        received_second_date = "#{date.second[:day]}-#{date.second[:month].strip}-#{date.second[:year]}".to_date
      end
    end
    if @type == "chargeback"
      if params[:query].present?
        reports = DisputeCase.run_chargeback(params[:query], first_date, second_date)
      end
      @chargebacks = reports.try(:[], :reports)
      @params = reports.try(:[], :params)
    elsif @type == "sale"
      tx_filter = params[:tx_filter] || 10
      @qc_wallet = Wallet.qc_support.first
      wallets = getting_sale_report(params[:query])
      if wallets.present? && params[:query]["types"].present?
        @sales = Transaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets.keys)
                     .where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date)
                     .where(main_type: params[:query]["types"], status: ["approved", "refunded","complete"]).order(timestamp: :desc).per_page_kaminari(params[:page]).per(tx_filter)
      elsif wallets.present? && (params[:query]["types"].blank? || params[:query]["categories"].blank?)
        @sales = Transaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: wallets.keys)
                     .where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date)
                     .where(main_type: [
                     TypesEnumLib::TransactionViewTypes::DebitCard,
                     TypesEnumLib::TransactionViewTypes::SaleIssue,
                     TypesEnumLib::TransactionViewTypes::PinDebit,
                     TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::CreditCard], status: ["approved", "refunded","complete"]).order(timestamp: :desc).per_page_kaminari(params[:page]).per(tx_filter)
      elsif params[:query]["types"].present? && wallets.blank?
        @sales = Transaction.where("timestamp BETWEEN :first AND :second", first: first_date, second: second_date)
                     .where(main_type: params[:query]["types"], status: ["approved", "refunded","complete"]).order(timestamp: :desc).per_page_kaminari(params[:page]).per(tx_filter)
      end
    elsif @type == "gateway"
      @details = PaymentGateway.get_report(params[:query]["gateway"], first_date, second_date, params[:query]["date"], params[:query])
    elsif @type == "merchant"
      result = getting_merchant_report(params[:query], first_date, second_date,params[:page], nil,params[:tx_filter], params[:from_filter])
      @transactions  = result.try(:[],:details)
      @txn = result.try(:[],:location)
    elsif @type == "wallet"
      @details = getting_wallet_report(params[:query])
    elsif @type == "withdrawal"
      if params[:summary_report] == "summary_report"
        @run_report_type = params[:summary_report]
      else
        @run_report_type = params[:detail_report]
      end
      first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, 0)
      second_date = Time.new(date.second[:year], date.second[:month].strip, date.second[:day], 23,59,59, 0)
      first_date = first_date + 22.hours
      second_date = second_date + 22.hours
      result = getting_withdrawal_report(params[:query], first_date, second_date,params[:page], params[:tx_filter], @run_report_type)
      @details  = result.try(:[],:transactions)
      @txn = result.try(:[],:original)
    elsif @type == "terminal_id"
      if params[:query][:dba_name].present? || params[:query][:select_all].present? || params[:query][:date].present?
        AdminReportWorker.perform_async(first_date, second_date, params[:query]["type"], params[:query].to_json, @offset, session.id)
      end
    elsif @type == "affiliate_program"
      first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, 0)
      second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, 0)
      result = getting_affiliate_program_report(params[:query], first_date, second_date)
      @transactions = result
    elsif @type == "cbk_detail"
      tx_filter = params[:tx_filter] || 10
      @disputes = DisputeCase.charge_back.where("recieved_date BETWEEN :first AND :second",first: received_first_date, second: received_second_date).order(recieved_date: :desc).per_page_kaminari(params[:page]).per(tx_filter)
      wallets = Wallet.where(id: @disputes.pluck(:merchant_wallet_id).uniq).select("wallets.id AS id","wallets.location_id AS location_id").group_by{|e| e.id}
      locations = Location.joins(:wallets,:merchant).where("wallets.wallet_type = ?",0).where(id: wallets.values.flatten.pluck(:location_id).uniq.compact).select("locations.id AS id", "locations.business_name AS business_name","wallets.id AS wallet_id", "locations.category_id as category_id","users.name as name", "users.id as user_id").group_by{|e| e.wallet_id}
      categories = Category.where(id: locations.values.flatten.pluck(:category_id).uniq.compact).select("id as id, name as name").group_by{|e| e.id}
      reasons = Reason.where(id: @disputes.pluck(:reason_id).uniq).select("id AS id", "title AS title").group_by{|e| e.id}
      transactions = Transaction.where(id: @disputes.pluck(:transaction_id).flatten.compact.uniq).select("id AS id", "seq_transaction_id AS seq_transaction_id","total_amount AS total_amount","fee AS fee","created_at as created_at", "timestamp as timestamp", "card_id as card_id", "tags as tags").group_by{|e| e.id}
      gateways = PaymentGateway.where(id: @disputes.pluck(:payment_gateway_id).uniq.compact).select("id as id", "name as name").group_by{|e| e.id}
      @disputes_data = []
      @disputes.each do |d|
        user = Wallet.where(id: d.user_wallet_id).try(:first).try(:users).try(:first)
        transaction = transactions[d.transaction_id].try(:first)
        location = locations[d.merchant_wallet_id].try(:first)
        if location.present?
          dba_name = location.try(:business_name).gsub(",", " ")
        end
        if transaction.card.present?
          brand = transaction.card.brand
        else
          brand = transaction.tags["card"].present? ? transaction.tags["card"].try(:[], "brand") : transaction.tags["previous_issue"].try(:[], "tags").try(:[], "card").try(:[], "brand")
        end
        @disputes_data.push({
                                case_number:  "#{d.try(:case_number)}",
                                customer_name: user.try(:name).present? ? user.try(:name) : user.try(:first_name),
                                merchant_id: location.try(:user_id),
                                merchant_name: location.try(:name),
                                dba_name: dba_name,
                                dba_category: categories[location.try(:category_id)].try(:first).try(:name),
                                received_date: d[:recieved_date].nil? ? 'NA' : d[:recieved_date].strftime("%m-%d-%Y"),
                                created_at: d[:created_at].nil? ? 'NA' : d[:created_at].to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p"),
                                cbk_amount: number_to_currency(number_with_precision(d.try(:amount).to_f,precision: 2, :delimiter => ',')),
                                due_date: d[:due_date].nil? ? 'NA' : d[:due_date].strftime("%m-%d-%Y"),
                                reason: reasons[d.try(:reason_id)].try(:first).try(:title)  ,
                                transaction_time: transaction.try(:timestamp).present? ? transaction.try(:timestamp).to_datetime.in_time_zone("Pacific Time (US & Canada)").strftime("%m/%d/%Y %I:%M:%S %p") : "N/A",
                                tx_amount: number_to_currency(number_with_precision(transaction.try(:total_amount).to_f,precision: 2, :delimiter => ',')),
                                tx_id: transaction.try(:seq_transaction_id),
                                descriptor: gateways[d.payment_gateway_id].try(:first).try(:name),
                                status: d.status,
                                brand: brand
                             })
      end
    end
    respond_to :js
  end

  def old_reports
    @header = params[:status].try(:capitalize)
    @reports = QcFile.where(status: params[:status]).order(id: :desc).per_page_kaminari(params[:page]).per(10)
  end

  def run_report
    if params[:query].try(:[], "date").present?
      date = parse_date(params[:query]["date"])
      if params[:query][:type] == "affiliate_program"
        first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, 0)
        second_date = Time.new(date.second[:year], date.second[:month].strip, date.second[:day], 23,59,59, 0)
      else
        first_date = "#{date.first[:day]}-#{date.first[:month]}-#{date.first[:year]}".to_date
        second_date = "#{date.second[:day]}-#{date.second[:month].strip}-#{date.second[:year]}".to_date + 1.day
        first_date = "#{first_date.year}-#{first_date.month}-#{first_date.day} 07:00:00"
        second_date = "#{second_date.year}-#{second_date.month}-#{second_date.day} 06:59:59"
        if params[:query]["type"] == "cbk_detail"
          received_first_date = "#{date.first[:day]}-#{date.first[:month]}-#{date.first[:year]}".to_date
          received_second_date = "#{date.second[:day]}-#{date.second[:month].strip}-#{date.second[:year]}".to_date
        end
      end
    end
    if params[:query]["type"] == "withdrawal"
      first_date = first_date.to_datetime + 14.hours
      second_date = second_date.to_datetime + 14.hours
    end
    if params[:query]["type"] == "cbk_detail"
      DisputeWorker.perform_async(received_first_date, received_second_date, current_user.id)
    else
      AdminReportWorker.perform_async(first_date, second_date, params[:query]["type"], params[:query].to_json, @offset, session[:session_id], current_user.id)
    end
    respond_to :js
  end

  def autocomplete_location
    locations = Location.includes(:wallets).where('lower(business_name) LIKE ?', "%#{params[:q].downcase}%").limit(15) if params[:q].present?
    if locations.present?
      locations = locations.map{|l| {name: l.business_name, id: {id: l.wallets.primary.first.id, name: l.business_name, reserve: l.wallets.reserve.first.id, category: l.category.try(:name)}.to_json}}
    end
    render json: locations
  end

  def autocomplete_location_WR

    #params[:q] = params[:q].strip
    #value=  params[:q].split('-').present?

    if params[:q].try(:downcase).try(:include?, 'm-')
      value = params[:q].split('-').second
      locations = Location.joins(:wallets, :users).where('locations.merchant_id = ? ', "#{value}").distinct if value.present?
      # #.select("locations.id,locations.business_name,locations.category_id AS category_id,wallets.reserve.first AS reserve_id locations.merchant_id as merchant_id,wallets.id as wallets_id,users.id as user_id,users.name AS user_name").uniq
      if locations.present?
        locations = locations.map{|l| {name: "M-#{l.merchant_id}  #{l.merchant.try(:name)} #{l.business_name}", id: {id: l.wallets.primary.first.id, name: l.business_name, reserve: l.wallets.reserve.first.id, category: l.category.try(:name),m_id: l.merchant_id}.to_json}}
      end
    else
      locations = Location.includes(:wallets).where('lower(business_name) LIKE ?', "%#{params[:q].downcase}%").limit(15) if params[:q].present?
        if locations.present?
          locations = locations.map{|l| {name: "#{l.business_name}    M-#{l.merchant_id}", id: {id: l.wallets.primary.first.id, name: l.business_name, reserve: l.wallets.reserve.first.id, category: l.category.try(:name)}.to_json}}
        end
    end
    render json: locations
  end


  def autocomplete_user
    if params[:type] == "iso"
      users = User.iso.where('lower(company_name) LIKE :name OR lower(name) LIKE :name', name: "%#{params[:q].downcase}%").limit(15)
    elsif params[:type] == "agent"
      users = User.agent.where('lower(name) LIKE ? ', "%#{params[:q].downcase}%").limit(15)
    elsif params[:type] == "affiliate"
      users = User.affiliate.where('lower(name) LIKE ? ', "%#{params[:q].downcase}%").limit(15)
    end
    if users.present?
      users = users.map{|l| {name: l.company_name || l.name, id: {id:l.id, wallet_id: l.wallets.first.id, name: l.company_name || l.name}.to_json}}
    end
    render json: users
  end

  def search_user
    json = []
    value= params[:q]
    if params[:type].present? && params[:type]=='category'
      users=Category.where("lower(name) LIKE ?", "%#{params[:q].downcase}%").limit(15)
      if users.present?
        users =  users.map{|l| {name: l.name, id: l.id}}
      end
    else
      users = User.select('users.id', 'users.name', 'users.company_name', 'users.role','users.merchant_id')
                  .where('users.first_name ILIKE ? OR users.name ILIKE ? OR users.company_name ILIKE ? ', "%#{params[:q].downcase}%", "%#{params[:q].downcase}%" , value)
                  .where.not(role: :atm).distinct
                  .limit(15) #here we limit the search results to 100

      # users1 = users.map{|l| {name: l.company_name || l.name, id: {id:l.id, name: l.company_name || l.name}.to_json}}
      if users.present?
        users = users.collect do |u| #here we use the search_results that holds the data of users
          role = nil
          if u.merchant? && u.merchant_id.present?
            wallet = u.id
            role = "sub merchant"
          elsif u.merchant? && u.merchant_id.blank?
            wallet = u.id
          elsif u.iso? || u.agent? || u.affiliate? || u.user?
            wallet = u.id
          elsif u.qc?
            wallet = u.id
          end
          if u.role=="iso" #here we display the suggestions in a specific format
            ref_no= "ISO-"
          elsif u.role=="agent"
            ref_no= "A-"
          elsif u.role=="affiliate"
            ref_no= "AF-"
          elsif u.role=="merchant"
            ref_no= "M-"
          elsif u.role="user"
            ref_no="C-"
          end
          {
              "id" => wallet,
              "name" =>  u.name.try(:capitalize).present? ?   ref_no + u.name.try(:capitalize)  : ref_no  + u.name.try(:capitalize),
          }
        end
      end
    end

    render json: users
  end

  def autopopulate_partner
    users = []
    if params[:id].present?
      iso = User.iso.where(id: params[:id]).first
      locations = iso.locations
      locations.find_each do |l|
        users.push(l.agents)
      end
    end
    if users.present?
      users = users.flatten.uniq
      @partners = users.map{|l| {name: l.company_name || l.name, id: {id:l.id, wallet_id: l.wallets.first.id, name: l.company_name || l.name}.to_json}}
    end
    respond_to :js
    # render json: users.to_json
  end

  def autopopulate_affiliates
    affiliates = User.affiliate_program.where(admin_user_id: params[:ids],archived: false).select(:id,:name)
    affiliates = affiliates.map do |u|
      { value: u.id, name: u.name  }
    end
    render json: affiliates
  end

  private

  def get_offset
    @offset = Time.now.in_time_zone(cookies[:timezone]).utc_offset
  end

end
