# require "benchmark"
class Admins::OrderBanksController < AdminsController
  layout 'admin'
  before_action :query_orders, only: [:index]
  before_action :check_admin
  KNOWN_BANKS =  I18n.t("known_banks").values
  def index
    @list = []
    # data = implementation_debit(@decline) if @decline.present? && params[:gateway] == 'debit_pin'
    data = implementation(@decline)
    @list = data.first
    @total_amount = data.second
    @total_decline = data.third
    @total = data.fourth
    @total_approved = data.fifth
    @total_order_declined = data[5]
  end

  def export

    if params[:query][:date].present?
      date = parse_date(params[:query][:date])
      first_date = Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset])
      second_date = Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset])
    else
      first_date = Time.now.beginning_of_day.utc
      second_date = Time.now.end_of_day.utc
    end
    job_id = ExportCCReportWorker.perform_async(params["gateway"],params["offset"],first_date,second_date,current_user.id,session[:session_id],params[:bankstatus])

    respond_to do |format|
      format.html
      format.js
      # format.csv { send_data generate_csv(decline), filename: "qc-cc-report-#{params[:gateway]}(#{params[:query][:date]}).csv" }
    end
  end

  def export_summary
    orders = JSON.parse(params[:orders])
    respond_to do |format|
      format.html
      format.csv { send_data generate_summary_csv(orders), filename: "qc-processing-report-summary-#{params[:gateway]}(#{params[:query][:date]}).csv" }
    end
  end

  private

  def query_orders
    # params["gateway"] = PaymentGateway.active_gateways.first.key if !params[:gateway].present?
    params[:gateway] = ["all"] if !params[:gateway].present?
    if params.try(:[], "query").try(:[], "date").present?
      date = parse_date(params[:query][:date])
      @date = date
      first_date = "#{date.first[:day]}-#{date.first[:month]}-#{date.first[:year]}".to_date
      second_date = "#{date.second[:day]}-#{date.second[:month]}-#{date.second[:year]}".to_date + 1.day
      first_date = DateTime.new(first_date.year, first_date.month, first_date.day, 7,00,00)
      second_date = DateTime.new(second_date.year, second_date.month, second_date.day, 06,59,59)
    else
      date = Date.today
      first_date = DateTime.new(date.year, date.month, date.day, 7,00,00)
      second_date = DateTime.new(date.year, date.month, date.day, 06,59,59)
    end
    if params[:gateway].present?  || params[:bank_status].present?
      if params[:gateway].present? && params[:bank_status].present?
        if params[:gateway].include?("all")
          gateways = PaymentGateway.where(is_deleted: params[:bank_status]).pluck(:id)
        else
          gateways = PaymentGateway.where(id: params[:gateway],is_deleted: params[:bank_status]).pluck(:id)
        end
        @decline = Transaction.by_date(first_date, second_date).by_transactions_types(gateways)
        # @decline = OrderBank.includes(:payment_gateway).references(:payment_gateway).where("bank_type = :gateway AND payment_gateways.is_deleted = :bankstatus AND order_banks.created_at BETWEEN :first AND :second",bankstatus: params[:bank_status], first: first_date, second: second_date,gateway: params[:gateway]).where.not("order_banks.status IN (?)", ["depricated","duplicate"]).group("order_banks.card_type","order_banks.status","order_banks.card_sub_type")
      elsif params[:bank_status].present?
        gateways = PaymentGateway.where(is_deleted: params[:bank_status]).pluck(:id)
        @decline = Transaction.includes(:card).references(:card).where('(main_type IN (:types) AND payment_gateway_id IN (:gateways)) OR (lower(main_type) IN (:rtp))', types: [TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::DebitCard,TypesEnumLib::TransactionViewTypes::PinDebit],gateways: gateways, rtp: [TypesEnumLib::TransactionType::RTP.downcase, TypesEnumLib::TransactionType::InvoiceRTP.downcase]).where('transactions.created_at BETWEEN :first AND :second', first: first_date, second: second_date).select("total_amount as total_amount").group_by{ |e| e.main_type.downcase == TypesEnumLib::TransactionType::RTP.downcase ? "#{e.main_type.downcase}_#{e.status}" : e.status == "refunded" || e.status == "complete" ? "#{e.try(:card).try(:brand).try(:downcase)}_approved_#{e.try(:card).try(:card_type).try(:downcase)}" : "#{e.try(:card).try(:brand).try(:downcase)}_#{e.status}_#{e.try(:card).try(:card_type).try(:downcase)}"}
      else
        if params[:gateway].include?("all")
          gateways = PaymentGateway.all.pluck(:id)
        else
          gateways = params[:gateway]
        end
        @decline = Transaction.includes(:card).references(:card).where('(main_type IN (:types) AND payment_gateway_id IN (:gateways)) OR (lower(main_type) IN (:rtp))', types: [TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::DebitCard,TypesEnumLib::TransactionViewTypes::PinDebit],gateways: gateways, rtp: [TypesEnumLib::TransactionType::RTP.downcase, TypesEnumLib::TransactionType::InvoiceRTP.downcase]).where('transactions.created_at BETWEEN :first AND :second', first: first_date, second: second_date).select("total_amount as total_amount").group_by{ |e| e.main_type.downcase == TypesEnumLib::TransactionType::RTP.downcase ? "#{e.main_type.downcase}_#{e.status}" : e.status == "refunded" || e.status == "complete" ? "#{e.try(:card).try(:brand).try(:downcase)}_approved_#{e.try(:card).try(:card_type).try(:downcase)}" : "#{e.try(:card).try(:brand).try(:downcase)}_#{e.status}_#{e.try(:card).try(:card_type).try(:downcase)}"}
        # @decline = OrderBank.where(bank_type: params[:gateway], created_at: first_date..second_date).where.not(status: ["depricated","duplicate"]).group(:card_type,:status,:card_sub_type)
      end
    else
      gateways = []
      @decline = Transaction.includes(:card).references(:card).where('(main_type IN (:types) AND payment_gateway_id IN (:gateways)) OR (lower(main_type) IN (:rtp))', types: [TypesEnumLib::TransactionViewTypes::SaleIssue,TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::DebitCard,TypesEnumLib::TransactionViewTypes::PinDebit],gateways: gateways, rtp: [TypesEnumLib::TransactionType::RTP.downcase, TypesEnumLib::TransactionType::InvoiceRTP.downcase]).where('transactions.created_at BETWEEN :first AND :second', first: first_date, second: second_date).select("total_amount as total_amount").group_by{ |e| [TypesEnumLib::TransactionType::RTP.downcase,TypesEnumLib::TransactionType::InvoiceRTP.downcase].include?(e.main_type.downcase) ? "#{e.main_type.downcase}_#{e.status}" : e.status == "refunded" || e.status == "complete" ? "#{e.try(:card).try(:brand).try(:downcase)}_approved_#{e.try(:card).try(:card_type).try(:downcase)}" : "#{e.try(:card).try(:brand).try(:downcase)}_#{e.status}_#{e.try(:card).try(:card_type).try(:downcase)}"}
      # @decline = OrderBank.where(created_at: first_date..second_date).where.not(status: ["depricated","duplicate"]).group(:card_type,:status,:card_sub_type)
    end
    # payment_gateway = PaymentGateway.find_by(key: params[:gateway])
    if params[:gateway].blank?
      @header = "All Gateways"
    elsif gateways.present?
      if gateways.count > 1
        @header = "Multiple Gateways"
      elsif gateways.count  == 1
        @header = "Gateways"
      end
    end
  end

  def implementation(decline)
    grouped_decline = decline
    # sum = grouped_decline.sum(:total_amount)
    # count = grouped_decline.try(:values).try(:flatten).try(:count).to_i
    sum_hash = {}
    grouped_decline.each do |key,value|
      # new_key = key.join("_").split.join("_").downcase
      # if sum_hash[key].present?
        sum_hash[key] = value.pluck(:total_amount).sum
      # else
      #   sum_hash[key] = value.pluck(:total_amount).sum
      # end
    end
    others_sum_hash = sum_hash.except(*KNOWN_BANKS)
    others_decline_sum_hash = others_sum_hash.select{|k,v| k.include?("pending")}
    others_approve_sum_hash = others_sum_hash.select{|k,v| k.include?("approved")}
    others_approve = others_approve_sum_hash.values.sum
    others_decline = others_decline_sum_hash.values.sum
    others_amount = sum_hash["_approved_"].to_f + others_approve.to_f

    #rtp hash
    rtp_approve_amount = sum_hash["rtp_approved"].to_f
    rtp_decline_amount = sum_hash["rtp_pending"].to_f
    rtp_total = rtp_approve_amount.to_f + rtp_decline_amount.to_f

    #Invoicertp hash
    invoice_rtp_approve_amount = sum_hash["invoice - rtp_approved"].to_f
    invoice_rtp_decline_amount = sum_hash["invoice - rtp_pending"].to_f
    invoice_rtp_total = invoice_rtp_approve_amount.to_f + invoice_rtp_decline_amount.to_f

    # "debit_pin_approve_"
    debit_pin_approve_amount = sum_hash["debit_pin_approved_"].to_f
    _decline_amount = sum_hash["_pending_"].to_f + sum_hash["unknown_pending_"].to_f + others_decline.to_f

    # "manual-refund" && "manual-refund-dan"
    manual_refund_hash = sum_hash.select{|k,v| k.include?("manual-refund") || k.include?("manual_refund")}
    manual_refund_amount = manual_refund_hash.values.sum

    # visa
    visa_approve_credit = sum_hash["visa_approved_credit"].to_f + sum_hash["visa_approved_"].to_f + sum_hash["visa_approved_visa"].to_f
    visa_approve_debit = sum_hash["visa_approved_debit"].to_f
    visa_decline_credit = sum_hash["visa_pending_credit"].to_f + sum_hash["visa_pending_"].to_f + sum_hash["visa_pending_visa"].to_f
    visa_decline_debit = sum_hash["visa_pending_debit"].to_f

    visa_approve = visa_approve_credit + visa_approve_debit
    visa_decline = visa_decline_credit + visa_decline_debit
    visa_credit_amount = visa_approve_credit + visa_decline_credit
    visa_debit_amount = visa_approve_debit + visa_decline_debit

    # mastercard
    mastercard_approve_debit = sum_hash["mastercard_approved_debit"].to_f
    mastercard_approve_credit = sum_hash["mastercard_approved_credit"].to_f + sum_hash["mastercard_approved_"].to_f + sum_hash["mastercard_approved_mastercard"].to_f
    mastercard_decline_debit = sum_hash["mastercard_pending_debit"].to_f
    mastercard_decline_credit = sum_hash["mastercard_pending_credit"].to_f + sum_hash["mastercard_pending_"].to_f + sum_hash["mastercard_pending_mastercard"].to_f

    master_approve = mastercard_approve_credit + mastercard_approve_debit
    master_decline = mastercard_decline_credit + mastercard_decline_debit
    master_credit_amount = mastercard_approve_credit + mastercard_decline_credit
    master_debit_amount = mastercard_approve_debit + mastercard_decline_debit

    # discover
    discover_approve_credit = sum_hash["discover_approved_credit"].to_f + sum_hash["discover_approved_"].to_f + sum_hash["discover_approved_discover"].to_f
    discover_approve_debit = sum_hash["discover_approved_debit"].to_f
    discover_decline_credit = sum_hash["discover_pending_credit"].to_f + sum_hash["discover_pending_"].to_f + sum_hash["discover_pending_discover"].to_f
    discover_decline_debit = sum_hash["discover_pending_debit"].to_f

    discover_decline = discover_decline_debit + discover_decline_credit
    discover_approve =  discover_approve_credit + discover_approve_debit
    discover_credit_amount = discover_approve_credit + discover_decline_credit
    discover_debit_amount = discover_decline_debit + discover_approve_debit

    # amex
    # american express
    # amex_approve_debit = sum_hash["amex_approve_debit"].to_f + sum_hash["american_express_approve_debit"].to_f
    amex_approve_credit = sum_hash["amex_approved_credit"].to_f + sum_hash["amex_approved_"].to_f + sum_hash["american_express_approved_credit"].to_f + sum_hash["american_express_approved_"].to_f + sum_hash["american_express_approved_american_express"].to_f
    # amex_decline_debit = sum_hash["amex_decline_debit"].to_f + sum_hash["american_express_decline_debit"].to_f
    amex_decline_credit = sum_hash["amex_pending_credit"].to_f + sum_hash["amex_pending_"].to_f + sum_hash["american_express_pending_credit"].to_f + sum_hash["american_express_pending_"].to_f + sum_hash["american_express_pending_american_express"].to_f

    amex_approve = amex_approve_credit
    amex_decline = amex_decline_credit
    amex_credit_amount = amex_decline_credit + amex_approve_credit
    # amex_debit_amount = amex_decline_debit + amex_approve_debit

    count_hash = {}
    grouped_decline.each do |key,value|
      # new_key = key.join("_").split.join("_").downcase
      # if count_hash[key].present?
        count_hash[key] = value.count
      # else
      #   count_hash[new_key] = value
      # end
    end

    others_count_hash = count_hash.except(*KNOWN_BANKS)
    others_decline_count_hash = others_count_hash.select{|k,v| k.include?("pending")}
    others_approve_count_hash = others_count_hash.select{|k,v| k.include?("approved") }

    others_approve_count = others_approve_count_hash.values.sum
    others_decline_count = others_decline_count_hash.values.sum

    others_count = count_hash["_approved_"].to_i + others_approve_count.to_i

    #rtp hash
    rtp_approve_count = count_hash["rtp_approved"].to_i
    rtp_decline_count = count_hash["rtp_pending"].to_i

    #Invoicertp hash
    invoice_rtp_approve_count = count_hash["invoice - rtp_approved"].to_i
    invoice_rtp_decline_count = count_hash["invoice - rtp_pending"].to_i

    # "debit_pin_approve_"
    debit_pin_approve_count = count_hash["debit_pin_approved_"].to_f
    _decline_count = count_hash["_pending_"].to_i + count_hash["unknown_pending_"].to_i + others_decline_count.to_i
    # "manual-refund" && "manual-refund-dan"
    manual_refund_count_hash = count_hash.select{|k,v| k.include?("manual-refund") || k.include?("manual_refund")}
    manual_refund_count = manual_refund_count_hash.values.sum

    # visa
    count_visa_approve_credit = count_hash["visa_approved_credit"].to_i + count_hash["visa_approved_"].to_i + count_hash["visa_approved_visa"].to_i
    count_visa_approve_debit = count_hash["visa_approved_debit"].to_i
    count_visa_decline_credit = count_hash["visa_pending_credit"].to_i + count_hash["visa_pending_"].to_i + count_hash["visa_pending_visa"].to_i
    count_visa_decline_debit = count_hash["visa_pending_debit"].to_i

    visa_decline_count = count_visa_decline_debit + count_visa_decline_credit
    visa_approve_count = count_visa_approve_debit + count_visa_approve_credit

    # mastercard
    count_mastercard_approve_debit = count_hash["mastercard_approved_debit"].to_i
    count_mastercard_approve_credit = count_hash["mastercard_approved_credit"].to_i + count_hash["mastercard_approved_"].to_i + count_hash["mastercard_approved_mastercard"].to_i
    count_mastercard_decline_debit = count_hash["mastercard_pending_debit"].to_i
    count_mastercard_decline_credit = count_hash["mastercard_pending_credit"].to_i + count_hash["mastercard_pending_"].to_i + count_hash["mastercard_pending_mastercard"].to_i

    mastercard_approve_count = count_mastercard_approve_credit + count_mastercard_approve_debit
    mastercard_decline_count = count_mastercard_decline_credit + count_mastercard_decline_debit

    # discover
    count_discover_approve_credit = count_hash["discover_approved_credit"].to_i + count_hash["discover_approved_"].to_i + count_hash["discover_approved_discover"].to_i
    count_discover_approve_debit = count_hash["discover_approved_debit"].to_i
    count_discover_decline_credit = count_hash["discover_pending_credit"].to_i + count_hash["discover_pending_"].to_i + count_hash["discover_pending_discover"].to_i
    count_discover_decline_debit = count_hash["discover_pending_debit"].to_i

    discover_approve_count = count_discover_approve_credit + count_discover_approve_debit
    discover_decline_count = count_discover_decline_credit + count_discover_decline_debit

    # amex
    # american express

    # count_amex_approve_debit = count_hash["amex_approve_debit"].to_i + count_hash["american_express_approve_debit"].to_i
    count_amex_approve_credit = count_hash["amex_approved_credit"].to_i + count_hash["amex_approved_"].to_i + count_hash["american_express_approved_credit"].to_i + count_hash["american_express_approved_"].to_i + count_hash["american_express_approved_american_express"].to_i
    # count_amex_decline_debit = count_hash["amex_decline_debit"].to_i + count_hash["american_express_decline_debit"].to_i
    count_amex_decline_credit = count_hash["amex_pending_credit"].to_i + count_hash["amex_pending_"].to_i + count_hash["american_express_pending_credit"].to_i + count_hash["american_express_pending_"].to_i + count_hash["american_express_pending_american_express"].to_i

    amex_approve_count = count_amex_approve_credit
    amex_decline_count = count_amex_decline_credit

    total = 0
    total_decline = 0
    total = visa_approve + visa_decline + master_approve + master_decline + discover_approve + discover_decline + amex_approve + amex_decline + debit_pin_approve_amount + manual_refund_amount + _decline_amount + others_amount.to_f + rtp_approve_amount.to_f + rtp_decline_amount.to_f + invoice_rtp_approve_amount.to_f + invoice_rtp_decline_amount.to_f
    # total = sum_hash.reject {|k,v| k == "_approve_" || k == "_decline_" || k == "debit_pin_approve_" || k == "debit_pin_decline" }.values.sum

    total_decline = visa_decline + master_decline + discover_decline + amex_decline + _decline_amount + others_decline + rtp_decline_amount.to_f + invoice_rtp_decline_amount.to_f
    total_approved = visa_approve +  master_approve + discover_approve + amex_approve  + debit_pin_approve_amount + manual_refund_amount + others_amount.to_f + rtp_approve_amount.to_f + invoice_rtp_approve_amount.to_f
    total_order_approved = visa_approve_count + mastercard_approve_count + discover_approve_count + amex_approve_count + debit_pin_approve_count + manual_refund_count + others_count + rtp_approve_count.to_i + invoice_rtp_approve_count.to_i
    total_order_declined = visa_decline_count + mastercard_decline_count + discover_decline_count + amex_decline_count  + _decline_count + others_decline_count + rtp_decline_count.to_i + invoice_rtp_decline_count.to_i
    if visa_credit_amount > 0
      @list.push({type: "Visa Credit",total_order: count_visa_approve_credit, approved_amount: visa_approve_credit ,decline_amount: visa_decline_credit ,total_amount: visa_credit_amount, decline: visa_decline_credit*100/visa_credit_amount, order_declined: count_visa_decline_credit})
    else
      @list.push({type: "Visa Credit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end

    if visa_debit_amount > 0
      @list.push({type: "Visa Debit", total_order: count_visa_approve_debit, approved_amount: visa_approve_debit , decline_amount: visa_decline_debit , total_amount: visa_debit_amount, decline: visa_decline_debit*100/visa_debit_amount,   order_declined: count_visa_decline_debit })
    else
      @list.push({type: "Visa Debit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end

    if master_credit_amount > 0
      @list.push({type: "Mastercard Credit",total_order: count_mastercard_approve_credit,approved_amount: mastercard_approve_credit ,decline_amount:mastercard_decline_credit,total_amount: master_credit_amount, decline: mastercard_decline_credit*100/master_credit_amount, order_declined: count_mastercard_decline_credit})
    else
      @list.push({type: "Mastercard Credit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end

    if master_debit_amount > 0
      @list.push({type: "Mastercard Debit" ,total_order: count_mastercard_approve_debit, approved_amount: mastercard_approve_debit  ,decline_amount:mastercard_decline_debit, total_amount: master_debit_amount , decline: mastercard_decline_debit*100/master_debit_amount ,  order_declined: count_mastercard_decline_debit })
    else
      @list.push({type: "Mastercard Debit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end

    if amex_credit_amount > 0
      @list.push({type: "American Express Credit",total_order: count_amex_approve_credit,approved_amount: amex_approve_credit, decline_amount:amex_decline_credit ,total_amount: amex_credit_amount ,decline: amex_decline_credit*100/amex_credit_amount, order_declined: count_amex_decline_credit})
    else
      @list.push({type: "American Express Credit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end

    # if amex_debit_amount > 0
    #   @list.push({type: "American Express Debit",total_order: count_amex_approve_debit,approved_amount: amex_approve_debit, decline_amount:amex_decline_debit ,total_amount: amex_debit_amount ,decline: amex_decline_debit*100/amex_debit_amount, order_declined: count_amex_decline_debit})
    # else
    #   @list.push({type: "American Express Debit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    # end

    if discover_credit_amount > 0
      @list.push({type: "Discover Credit",total_order: count_discover_approve_credit,approved_amount: discover_approve_credit, decline_amount:discover_decline_credit,total_amount: discover_credit_amount ,decline: discover_decline_credit*100/discover_credit_amount, order_declined: count_discover_decline_credit})
    else
      @list.push({type: "Discover Credit",total_order: 0,approved_amount:0 ,decline_amount:0 ,total_amount:0 ,decline: 0, order_declined: 0})
    end

    if discover_debit_amount > 0
      @list.push({type: "Discover Debit",total_order: count_discover_approve_debit,approved_amount: discover_approve_debit, decline_amount:discover_decline_debit,total_amount: discover_debit_amount ,decline: discover_decline_debit*100/discover_debit_amount, order_declined: count_discover_decline_debit})
    else
      @list.push({type: "Discover Debit",total_order: 0,approved_amount:0 ,decline_amount:0 ,total_amount:0 ,decline: 0, order_declined: 0})
    end

    if debit_pin_approve_amount > 0
      @list.push({type: "Debit PIN",total_order: debit_pin_approve_count, approved_amount: debit_pin_approve_amount ,decline_amount: 0 ,total_amount: debit_pin_approve_amount, decline: 0, order_declined: 0})
    else
      @list.push({type: "Debit PIN",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end

    if manual_refund_amount > 0
      @list.push({type: "Manual Refund",total_order: manual_refund_count, approved_amount: manual_refund_amount ,decline_amount: 0 ,total_amount: manual_refund_amount, decline: 0, order_declined: 0})
    else
      @list.push({type: "Manual Refund",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    end
    # if _decline_amount > 0
    #   @list.push({type: "Others",total_order: 0, approved_amount: 0 ,decline_amount: _decline_amount ,total_amount: _decline_amount, decline: 100, order_declined: _decline_count})
    # else
    #   @list.push({type: "Others",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
    # end
    #
    if others_amount > 0
      @list.push({type: "Others",total_order: others_count ,approved_amount: others_amount, decline_amount:_decline_amount,total_amount: others_amount.to_f + _decline_amount.to_f ,decline: _decline_amount.to_f*100/others_amount, order_declined: _decline_count})
    else
      @list.push({type: "Others",total_order: 0,approved_amount:0 ,decline_amount:0 ,total_amount:0 ,decline: 0, order_declined: 0})
    end

    if rtp_total > 0
      @list.push({type: "RTP",total_order: rtp_approve_count ,approved_amount: rtp_approve_amount, decline_amount:rtp_decline_amount,total_amount: rtp_total.to_f ,decline: rtp_decline_amount.to_f*100/rtp_total.to_f, order_declined: rtp_decline_count})
    else
      @list.push({type: "RTP",total_order: 0,approved_amount:0 ,decline_amount:0 ,total_amount:0 ,decline: 0, order_declined: 0})
    end

    if invoice_rtp_total > 0
      @list.push({type: "Invoice - RTP",total_order: invoice_rtp_approve_count ,approved_amount: invoice_rtp_approve_amount, decline_amount:invoice_rtp_decline_amount,total_amount: invoice_rtp_total.to_f ,decline: invoice_rtp_decline_amount.to_f*100/invoice_rtp_total.to_f, order_declined: invoice_rtp_decline_count})
    else
      @list.push({type: "Invoice - RTP",total_order: 0,approved_amount:0 ,decline_amount:0 ,total_amount:0 ,decline: 0, order_declined: 0})
    end

    return [@list ,total ,total_decline ,total_order_approved ,total_approved , total_order_declined]
  end

  # def implementation_debit(decline)
  #   debit_amount=0
  #   debit_decline=0
  #   debit_pin = decline.select{|v| v[:card_type].try(:downcase) == "debit_pin" }
  #   debit_pin_approve = debit_pin.select{|v| v[:status] == "approve" }
  #
  #   debit_pin_declined = debit_pin.select{|v| v[:status] == "decline"}
  #   debit_pin_declined_count = debit_pin_declined.count
  #
  #   debit_pin.each do|e|
  #     if e[:status] == "decline"
  #       debit_decline =+ e[:amount] + debit_decline
  #     end
  #     debit_amount =+ e[:amount] + debit_amount
  #   end
  #
  #   total = 0
  #   total_decline = 0
  #   total =  debit_amount
  #   total_decline = debit_decline
  #   total_approve =  debit_pin_approve.count
  #   total_approved = total.to_f - total_decline.to_f
  #   total_order_declined = debit_pin_declined_count
  #   list=[]
  #   if debit_pin.empty?
  #     @list.push({type: "Pin Debit",total_order: 0,approved_amount: 0,decline_amount:0,total_amount:0,decline: 0, order_declined: 0})
  #   else
  #     @list.push({type: "Pin Debit",total_order: debit_pin_approve.count,approved_amount: debit_amount.to_f - debit_decline.to_f,decline_amount:debit_decline,total_amount: debit_amount,decline: debit_decline*100/debit_amount, order_declined: debit_pin_declined_count}) if debit_amount > 0
  #   end
  #   return [@list,total,total_decline,total_approve,total_approved, total_order_declined]
  # end

  # def generate_csv(orders)
  #   CSV.generate do |csv|
  #     csv << %w{ id bank_type status card_type card_sub_type amount created_at user merchant }
  #     orders.in_batches(of: 500) do |batch|
  #       sleep 0.5
  #       batch.each do |order|
  #         csv << [order[:id], order[:bank_type], order[:status], order[:card_type],order[:card_sub_type].try(:titleize), order[:amount], order[:created_at].in_time_zone("Pacific Time (US & Canada)").strftime("%Y-%m-%d %I:%M:%S %p"), order[:user_id], order[:merchant_id]]
  #       end
  #     end
  #   end
  # end
  def generate_summary_csv(lists)
    CSV.generate do |csv|
      csv << %w{ card_type total_order approved_amount order_declined decline_amount total_amount decline}
      lists.each do |list|
        csv << [list["type"], list["total_order"], number_with_precision(list["approved_amount"],precision:2),list["order_declined"], number_with_precision(list["decline_amount"],precision:2), number_with_precision(list["total_amount"], precision:2),number_with_precision(list["decline"],precision:2) ]
      end
        csv << ["Total", params["total"], "$"+params["total_approved"], params["total_order_declined"], "$"+params["total_decline"], "$"+params["total_amount"]]
    end
  end
end
