class Admins::MaxmindController < AdminsController
  before_action :check_admin

  def show
    @minfraud_result = MinfraudResult.find(params[:id])
    @minfraud_insight = @minfraud_result.insight_detail
    render partial: 'show'
  end

  def edit
    @moderate_risk = AppConfig.where(key: AppConfig::Key::ModerateRiskMaxMind).first_or_create(stringValue: "0.0")
    @high_risk = AppConfig.where(key: AppConfig::Key::HighRiskMaxMind).first_or_create(stringValue: "0.0")
    @approve_manual_review = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first_or_create(stringValue: "0")
  end

  def update
    if params[:high_risk]
      @high_risk = AppConfig.where(key: AppConfig::Key::HighRiskMaxMind).first
      @high_risk.update(stringValue: params[:high_risk])
      flash[:notice] = "Updated Successfully!"
    elsif params[:moderate_risk]
      @moderate_risk = AppConfig.where(key: AppConfig::Key::ModerateRiskMaxMind).first
      @moderate_risk.update(stringValue: params[:moderate_risk])
      flash[:notice] = "Updated Successfully!"
    elsif params[:approve_manual_review]
      @approve_manual_review = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first
      @approve_manual_review.update(stringValue: params[:approve_manual_review])
      flash[:notice] = "Updated Successfully!"
    else
      flash[:error] = "Something Went Wrong."
    end
    redirect_back(fallback_location: root_path)
  end

end
