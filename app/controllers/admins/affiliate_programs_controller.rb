class Admins::AffiliateProgramsController < AdminsController
	include Wicked::Wizard
	steps :affiliate_program_signup,:bank_account
	before_action :user_data,only: [:show,:update]
	def index
		@affiliate_programs = User.affiliate_program.all.order(id: :desc)
	end

	def new
		# @affiliate_program = User.affiliate_program.new
		# @affiliate_program.build_profile
		if params[:id].present?
      		show()
    	end
	end


  	def show
	    case step
	    when :affiliate_program_signup
	      if params[:user_data_id].present? && @user_temp.try(:iso_agent)!="{}"
	        @affiliate = User.new(@user_temp.try(:iso_agent)) if @user_temp.present?
	      elsif params[:affiliate_program_id].present?
	        @affiliate=User.friendly.find params[:affiliate_program_id] if params[:affiliate_program_id].present?
	      else
	        @affiliate=User.new
	      end
	    when :bank_account
	      if params[:user_data_id].present? && @user_temp.try(:fees)!="{}"
	        @affiliate = User.new(@user_temp.fees) if @user_temp.present?
	      elsif params[:affiliate_program_id].present?
	        @affiliate=User.friendly.find(params[:affiliate_program_id]) if params[:affiliate_program_id].present?
	      else
	        @affiliate=User.new
	      end
	    # when :affiliate_program_documentation
	    #   if params[:affiliate_program_id].present?
	    #     @affiliate=User.friendly.find(params[:affiliate_program_id]) if params[:affiliate_program_id].present?
	    #     @images = Documentation.where(:imageable_id => params[:affiliate_program_id])
	    #     @images.each do |doc|
	    #       if doc.is_deleted === true
	    #         doc.update(is_deleted: false)
	    #       end
	    #     end
	    #   else
	    #     @affiliate=User.new
	    #   end
	    end

	    # if params[:id]=="affiliate_documentation" || params[:id] != "wicked_finish"

	    if params[:id] != "wicked_finish"
	      @step_index = current_step
	      if params[:count].to_i == 1
	        session.delete(:index)
	        session[:index] = @step_index if session[:index].blank?
	      end
	      if params[:count].to_i != 2
	        if session[:index].present? && session[:index] < @step_index
	          session.delete(:index)
	          session[:index] = @step_index
	        end
	      end
	    end
	    gon.step_index = @step_index
	    gon.index_session = session[:index]
	    render_wizard(nil, {}, {user_data_id: params[:user_data_id] })
  	end


    def update
	    case params[:id]
	    when "affiliate_program_signup"
	      affiliate_signup()
	    when "bank_account"
	      bank_account()
	    # when "affiliate_program_documentation"
	    #   affiliate_domentation()
	    end
	end


    def affiliate_signup
	    params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
	    params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
	    if params[:user][:affiliate_program_id].present?
	      params[:user][:id] = params[:user][:affiliate_program_id]
	      begin
	        @affiliate = User.friendly.find(params[:user][:affiliate_program_id])
	        if params[:user][:profile_attributes].present?
	          @affiliate.company_name = params[:user][:profile_attributes][:company_name]
	        end
	        if affiliate_update_params[:password].present?
	          @affiliate.update(affiliate_update_params)
	        else
	          ex=affiliate_update_params.except(:password_confirmation)
	          ex = ex.except(:password)
	          @affiliate.update(ex)
	        end
	        if @affiliate.errors.blank?
	          @affiliate.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
	        end
	        if @affiliate.profile.present?
	          @affiliate.profile.update(affiliate_profile_params) if !affiliate_profile_params.blank?
	          # flash[:success] = "Affiliate has been updated Successfully"
	          if @affiliate.errors.blank?
	            @affiliate.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
	            ActivityLog.create(params:params[:user],action:'affiliate_user_update',user_id:@affiliate.try(:id),activity_type:'affiliate_activity',action_type:'affiliate_program')
	            flash[:success] = "Affiliate has been updated Successfully"
	          else
	            flash[:success] = "Password Previously used, try again."
	            return redirect_back(fallback_location: root_path)
	          end
	        else
	          flash[:error] = "Affiliate Profile Does Not Exist!"
	        end
	        @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
	        return redirect_to profile_isos_path(@affiliate)
	      end
	    else
	      if params[:user_data_id].present?
	        new1 = MerchantData.find_by_id params[:user_data_id]
	        if new1.present?
	          @user_temp = new1.update(iso_agent: affiliate_signup_params)
	        end

	        redirect_to wizard_path(@next_step,{:user_data_id => params[:user_data_id] })
	      else
	        @user_temp = MerchantData.create(iso_agent: affiliate_signup_params)
	        redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
	      end

	    end
  	end

  	def bank_account
    	if params[:user][:affiliate_program_id].present?
	      @affiliate=User.friendly.find(params[:user][:affiliate_program_id])
	      @affiliate.system_fee.merge!(system_fee_params["system_fee"])
	      @affiliate.save
	      flash[:success] = "Affiliate has been updated Successfully"
	      @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
	      save_activity(@affiliate,Time.now,current_user.id,'system_fee','')
	      return redirect_to profile_isos_path(@affiliate)
	    else
	        if params[:image].present? && params[:image]['add_image'].present?
      			save_image_bank_account
    		end
	      	@user_temp.update(fees: system_fee_params)
	      	# redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })

      		result = create_affiliate()
      		if result[:success]
        		# redirect_to wizard_path(@next_step)
        		redirect_to users_path(user: "affiliate_programs")
      		else
        		flash[:error] = result[:message]
        		redirect_to users_path(user: "affiliate_programs")
      		end


	    end
  	end

 #  	def affiliate_domentation
	#     if @user_temp.present?
	#       result = create_affiliate()
	#       if result[:success]
	#         # redirect_to wizard_path(@next_step)
	#         redirect_to users_path(user: "affiliate_programs")
	#       else
	#         flash[:error] = result[:message]
	#         redirect_to users_path(user: "affiliate_programs")
	#       end
	#     elsif params[:user].present? ? params[:user][:affiliate_program_id].present? ? true : false : false
	#       @affiliate=User.friendly.find(params[:user][:affiliate_program_id])
	#       save_user_documentation(@affiliate, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
	#       @documents = Documentation.where(imageable_id: params[:user][:affiliate_program_id])
	#       @documents.each do |doc|
	#         if doc.is_deleted === true
	#           doc.document.destroy
	#           doc.destroy
	#         end
	#       end
	#       respond_to do |format|
	#         format.html{
	#           flash[:success] = 'Affiliate has been added Successfully!'
	#         }
	#       end
	#       @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
	#       return redirect_to profile_isos_path(@affiliate)
	#     end
	# end



  	def create_affiliate
	    begin
	      if !params[:user].present?
	       params[:user]={}
	      end
	      params[:user].merge!(@user_temp.iso_agent).merge!(@user_temp.fees)
	      @affiliate = User.new(new_affiliate_params)
	      password = random_password
	      @affiliate.password = password
	      @affiliate.is_password_set = false
	      @affiliate.is_block = false
	      @affiliate.regenerate_token
	      if @affiliate.affiliate_program!
	        @affiliate.update company_name: @affiliate.try(:profile).try(:company_name) if @affiliate.profile.present?
					user_data={}
					if params[:user].present? && params[:user].try(:[],'profile_attributes').present?
						new_hash=params.to_unsafe_h
						user_data=new_hash[:user][:profile_attributes]
						user_data=user_data.merge({:name=>new_hash[:user][:name]}) if new_hash[:user][:name].present?
						user_data=user_data.merge({:email=>new_hash[:user][:email]}) if new_hash[:user][:email].present?
						user_data=user_data.merge({:phone_number=>new_hash[:user][:phone_number]}) if new_hash[:user][:phone_number].present?
						user_data=user_data.merge({:bank_account=>new_hash[:user][:system_fee]}) if new_hash[:user][:system_fee].present?
					end
					ActivityLog.create(params:user_data,action:'affiliate_user_create',user_id:@affiliate.try(:id),activity_type:'affiliate_activity',action_type:'affiliate_program') if user_data.present?
	        @affiliate.system_fee["bank_account"] = AESCrypt.encrypt("#{@affiliate.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@affiliate.system_fee["bank_account"]) if @affiliate.try(:system_fee).try(:[],"bank_account")
			@affiliate.save
			bank_account_image = Image.where(usage_id: params[:user_data_id]).last
			bank_account_image.update(location_id: @affiliate.wallets.last.id,usage_id: @affiliate.wallets.last.id) if bank_account_image.present?
	        # save_user_documentation(@affiliate, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
	        message="Welcome to Quick Card"
	    	UserMailer.welcome_affiliate(@affiliate,"Welcome to Quick Card","affiliate_program",password).deliver_later
			# UserMailer.welcome_user(@affiliate ,message, 'Affiliate', password).deliver_later
	       	return {success: true, message: "Affiliate has been added Successfully"}
	        respond_to do |format|
	          format.html{
	            flash[:success] = 'Affiliate has been added Successfully!'
	          }
	        end
	        return redirect_to users_path(user: "affiliate_programs")
	      else
	        flash[:error] = @affiliate.errors.full_messages.first
	      end
	    rescue ActiveRecord::RecordInvalid => invalid
	      flash[:error] = invalid
	      return {success: false, message: invalid}
	      # redirect_to users_path(user: "affiliates")
	    end
  	end



	def create
		params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
		params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
		@affiliate_program = User.affiliate_program.new(affiliate_program_params)
		@affiliate_program = User.admin_user.find(params[:user][:admin_user_id])
		# @affiliate_program.system_fee = @affiliate_program.system_fee
		@affiliate_program.is_block = false
		password = random_password
    	@affiliate_program.password = password
    	@affiliate_program.is_password_set = false
		if @affiliate_program.save
			ActivityLog.create(params:params,action:'affiliate_user_create',user_id:@affiliate_program.try(:id),activity_type:'affiliate_activity',action_type:'affiliate_program')
			@affiliate_program.send_welcome_email("affiliate_program")
			flash[:notice] = "Affiliate Program Created Successfully"
			redirect_to users_path(user: "affiliate_programs",admin_user: @affiliate_program.admin_user_id)
		end
	end

	def edit
		@affiliate_program = User.affiliate_program.find(params[:id])
		@affiliate_program.build_profile if @affiliate_program.profile.nil?
	end

	# def update
	# 	params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
	# 	params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
	# 	@affiliate_program = User.affiliate_program.find(params[:id])
	# 	if affiliate_program_params[:password].present?
 #          @affiliate_program.update(affiliate_program_params)
 #        else
 #          ex=affiliate_program_params.except(:password_confirmation)
 #          ex = ex.except(:password)
 #          @affiliate_program.update(ex)
 #        end
 #        if @affiliate_program.errors.blank?
 #          	@affiliate_program.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
 #        end
	# 	if @affiliate_program.errors.blank?
 #        	@affiliate_program.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
 #        	# flash[:success] = "Affiliate has been updated Successfully"
 #        	save_activity_logs
	# 		flash[:notice] = "Affiliate Program Updated Successfully"
	# 		redirect_to users_path(user: "affiliate_programs",admin_user: @affiliate_program.admin_user_id)
 #      	else
 #        	flash[:success] = "Password Previously used, try again."
 #        	return redirect_back(fallback_location: root_path)
 #      	end
	# end
private


  	def user_data
    	if params[:user_data_id].present?
      		@user_temp = MerchantData.find_by(id: params[:user_data_id])
    	end
  	end

  	def current_step
    	steps.index(step) + 1
  	end


  	def affiliate_program_params
    	params.require(:user).permit(:id,:admin_user_id,  :email, :password, :password_confirmation, :name, :phone_number, :company_name , profile_attributes: [:id , :company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code ,:_destroy])
	end


	def affiliate_update_params
    	params.require(:user).except(:affiliate_program_id,:format, :profile_attributes).permit(:email,:admin_user_id, :password, :password_confirmation, :name, :phone_number, :company_name)
  	end

    def affiliate_profile_params
    	params[:user].require(:profile_attributes).permit(:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code)
  	end

  	# def doc_params
   #  	params.require(:image).permit(:add_image,:usage_id,:location_id)
  	# end

  	def affiliate_signup_params
	    # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
	    params.require(:user).permit(:email,:admin_user_id, :password, :password_confirmation, :name, :phone_number,:merchant_id,
	                                 profile_attributes: [
	                                     :company_name,
	                                     :years_in_business,
	                                     :tax_id,
	                                     :street_address,
	                                     :ein,
	                                     :country,
	                                     :city,
	                                     :state,
	                                     :zip_code
	                                 ])
  	end


    def system_fee_params
    	params.require(:user).permit(
        	system_fee: [
       			:bank_account, :bank_routing, :bank_account_type, :bank_account_name
        	])
  	end



  	def new_affiliate_params
    	params.require(:user).permit(:email,:admin_user_id, :password, :password_confirmation, :name, :phone_number,:merchant_id,:is_password_set,
                                 profile_attributes: [:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code],
                                 system_fee: [
                                     :bank_account, :bank_routing, :bank_account_type, :bank_account_name
                                 ])
    end

	def save_activity_logs
		user_audit=@affiliate_program.audits.where(created_at: (Time.now-20)..Time.now) if @affiliate_program.try(:audits).present?
		profile_audit=@affiliate_program.profile.audits.where(created_at: (Time.now-20)..Time.now) if @affiliate_program.try(:audits).present?
		new_hash={}
		if user_audit.present?
			changes=user_audit.try(:first).try(:audited_changes)
			changes.to_hash.each_pair do |k,v|
				if k.present? && k=='admin_user_id'
					first_user=User.where(id:v.try(:first)).try(:first)
					second_user=User.where(id:v.try(:last)).try(:first)
					new_hash.merge!({"#{k}" => [first_user.try(:name),second_user.try(:name)]})
				else
					new_hash.merge!({"#{k}" => v})
				end
			end
		end
		if profile_audit.present?
			profile_audit=profile_audit.try(:first).try(:audited_changes)
			profile_audit.to_hash.each_pair do |k,v|
				new_hash.merge!({"#{k}" => v})
			end
		end
		ip_address = request.remote_ip.present? ? request.remote_ip : request.env['REMOTE_ADDR']
		new_hash=new_hash.to_json
		ActivityLog.create(note:new_hash,activity_type: :affiliate_activity,user_id:@affiliate_program.try(:id),host:request.env["SERVER_NAME"],url: request.url,browser: request.env['HTTP_USER_AGENT'],ip_address: ip_address, action:action_name,action_type:'affiliate_program' )
	end


end