class Admins::AffiliatesController < AdminsController
  skip_before_action :check_admin
  include Wicked::Wizard
  steps :affiliate_signup, :affiliate_buyrate, :system_fee, :affiliate_documentation
  before_action :user_data,only: [:show,:update]
  skip_before_action :mtrac_admin, only: [:show]

  # def index
  #   @affiliate = User.new
  #   @affiliates = User.affiliate
  #   respond_to do |format|
  #     format.html{ render :index}
  #     format.json { render json: AffiliatesDatatable.new(view_context)}
  #   end
  # end

  def new
    if params[:id].present?
      show()
    end
    # @affiliate = User.new
    # @affiliate.fees.build
    # render template: 'admins/affiliates/new'
  end

  # def show
  #   @affiliate = User.friendly.find(params[:id])
  #   @wallets = @affiliate.wallets
  # end
  def show
    case step
    when :affiliate_signup
      if params[:user_data_id].present? && @user_temp.try(:iso_agent)!="{}"
        @affiliate = User.new(@user_temp.try(:iso_agent)) if @user_temp.present?
      elsif params[:affiliate_id].present?
        @affiliate=User.friendly.find params[:affiliate_id] if params[:affiliate_id].present?
      else
        @affiliate=User.new
      end
    when :affiliate_buyrate
      if params[:user_data_id].present? && @user_temp.try(:buyrate)!="{}"
        @affiliate = User.new(@user_temp.try(:buyrate)) if @user_temp.present?
        @fees = @affiliate.fees
      elsif params[:affiliate_id].present?
        @affiliate=User.friendly.find params[:affiliate_id] if params[:affiliate_id].present?
        @fees = @affiliate.fees.order(id: :asc)
      else
        @affiliate = User.new
        @fee = @affiliate.fees.build
      end
    when :system_fee
      if params[:user_data_id].present? && @user_temp.try(:fees)!="{}"
        @affiliate = User.new(@user_temp.fees) if @user_temp.present?
      elsif params[:affiliate_id].present?
        @affiliate=User.friendly.find(params[:affiliate_id]) if params[:affiliate_id].present?
      else
        @affiliate=User.new
      end
    when :affiliate_documentation
      if params[:affiliate_id].present?
        @affiliate=User.friendly.find(params[:affiliate_id]) if params[:affiliate_id].present?
        @images = Documentation.where(:imageable_id => params[:affiliate_id])
        @images.each do |doc|
          if doc.is_deleted === true
            doc.update(is_deleted: false)
          end
        end
      else
        @affiliate=User.new
      end
    end

    if params[:id]=="affiliate_documentation" || params[:id] != "wicked_finish"
      @step_index = current_step
      if params[:count].to_i == 1
        session.delete(:index)
        session[:index] = @step_index if session[:index].blank?
      end
      if params[:count].to_i != 2
        if session[:index].present? && session[:index] < @step_index
          session.delete(:index)
          session[:index] = @step_index
        end
      end
    end
    gon.step_index = @step_index
    gon.index_session = session[:index]
    render_wizard(nil, {}, {user_data_id: params[:user_data_id] })
  end

  # def create
  #   begin
  #     @affiliate = User.new(new_affiliate_params)
  #     @affiliate.name = "#{@affiliate.first_name} #{@affiliate.last_name}"
  #     password = random_password
  #     @affiliate.password = password
  #     @affiliate.regenerate_token
  #     @affiliate.is_block = false
  #     if @affiliate.affiliate!
  #       @affiliate.fees.each do |fee|
  #         fee.buy_rate!
  #         fee.high!
  #       end
  #       if params[:user][:merchant_id].present?
  #         @merchant = User.find params[:user][:merchant_id]
  #         @merchant.affiliate = @affiliate
  #       end
  #       message = "Welcome to Quick Card"
  #       UserMailer.welcome_user(@affiliate, message, 'Affiliate', password).deliver_later
  #       respond_to do |format|
  #         format.html{
  #           flash[:success] = 'Affiliate has been added Successfully!'
  #         }
  #       end
  #     else
  #       flash[:error] = @affiliate.errors.full_messages.first
  #     end
  #     redirect_to  users_path(user: "affiliates")
  #   rescue ActiveRecord::RecordInvalid => invalid
  #     flash[:error] = invalid
  #     redirect_to  new_affiliate_path
  #   end
  # end

  # def edit
  #   @affiliate = User.friendly.find params[:id]
  # end
  def edit
    @affiliate = User.friendly.find params[:id]
    @affiliate['system_fee']['bank_account'] = AESCrypt.decrypt("#{@affiliate.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@agent['system_fee']['bank_account']) if @affiliate['system_fee']['bank_account'].present?
  end

  def update
    case params[:id]
    when "affiliate_signup"
      affiliate_signup()
    when "affiliate_buyrate"
      affiliate_buyrate_save()
    when "system_fee"
      system_fee()
    when "affiliate_documentation"
      affiliate_domentation()
    end
  end
  # def update
  #   affiliate = User.friendly.find params[:id]
  #   if edit_affiliate_params[:password].present?
  #     affiliate.update(edit_affiliate_params)
  #   else
  #     ex=edit_affiliate_params.except(:password_confirmation)
  #     ex = ex.except(:password)
  #     affiliate.update(ex)
  #   end
  #   affiliate.name = "#{affiliate.first_name} #{affiliate.last_name}"
  #   affiliate.fees.each do |fee|
  #     fee.buy_rate!
  #     fee.high!
  #   end
  #   affiliate.save
  #   if affiliate.errors.blank?
  #     affiliate.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
  #     flash[:success] = "Affiliate has been updated Successfully"
  #     redirect_to controller: 'users', action: 'index', user: 'affiliates'
  #   else
  #     flash[:error] =  affiliate.errors.full_messages.first
  #     return redirect_back(fallback_location: root_path)
  #   end
  # end

  def affiliate_signup
    params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
    params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
    if params[:user][:affiliate_id].present?
      params[:user][:id] = params[:user][:affiliate_id]
      begin
        @affiliate = User.friendly.find(params[:user][:affiliate_id])
        if params[:user][:profile_attributes].present?
          @affiliate.company_name = params[:user][:profile_attributes][:company_name]
        end
        if affiliate_update_params[:password].present?
          @affiliate.update(affiliate_update_params)
        else
          ex=affiliate_update_params.except(:password_confirmation)
          ex = ex.except(:password)
          @affiliate.update(ex)
        end
        if @affiliate.errors.blank?
          @affiliate.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
        end
        if @affiliate.profile.present?
          @affiliate.profile.update(affiliate_profile_params) if !affiliate_profile_params.blank?
          # flash[:success] = "Affiliate has been updated Successfully"
          if @affiliate.errors.blank?
            @affiliate.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
            flash[:success] = "Affiliate has been updated Successfully"
          else
            flash[:success] = "Password Previously used, try again."
            return redirect_back(fallback_location: root_path)
          end
        else
          flash[:error] = "Affiliate Profile Does Not Exist!"
        end
        @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
        return redirect_to profile_isos_path(@affiliate)
      end
    else
      if params[:user_data_id].present?
        new1 = MerchantData.find_by_id params[:user_data_id]
        if new1.present?
          @user_temp = new1.update(iso_agent: affiliate_signup_params)
        end

        redirect_to wizard_path(@next_step,{:user_data_id => params[:user_data_id] })
      else
        @user_temp = MerchantData.create(iso_agent: affiliate_signup_params)
        redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
      end

    end
  end

  def affiliate_buyrate_save
    if params[:user][:affiliate_id].present?
      @affiliate=User.friendly.find(params[:user][:affiliate_id])
      @affiliate.update(affiliate_buyrate_params)
      if @affiliate.present?
        @affiliate.fees.each do |fee|
          fee.buy_rate!
          fee.high!
        end
      else
        flash[:error] = @affiliate.errors.full_messages.first
      end
      flash[:success] = "Affiliate has been updated Successfully"
      @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
      save_activity(@affiliate,Time.now,current_user.id,'buy_rate','')
      return redirect_to profile_isos_path(@affiliate)
    else
      @user_temp.update(buyrate: affiliate_buyrate_params)
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
  end

  def system_fee
    if params[:user][:affiliate_id].present?
      @affiliate=User.friendly.find(params[:user][:affiliate_id])
      @affiliate.system_fee.merge!(system_fee_params["system_fee"])
      @affiliate.save
      flash[:success] = "Affiliate has been updated Successfully"
      @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
      save_activity(@affiliate,Time.now,current_user.id,'system_fee','')
      return redirect_to profile_isos_path(@affiliate)
    else
      @user_temp.update(fees: system_fee_params)
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
  end

  def affiliate_domentation
    if @user_temp.present?
      result = create_affiliate()
      if result[:success]
        redirect_to wizard_path(@next_step)
      else
        flash[:error] = result[:message]
        redirect_to users_path(user: "affiliates")
      end
    elsif params[:user].present? ? params[:user][:affiliate_id].present? ? true : false : false
      @affiliate=User.friendly.find(params[:user][:affiliate_id])
      save_user_documentation(@affiliate, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
      @documents = Documentation.where(imageable_id: params[:user][:affiliate_id])
      @documents.each do |doc|
        if doc.is_deleted === true
          doc.document.destroy
          doc.destroy
        end
      end
      respond_to do |format|
        format.html{
          flash[:success] = 'Affiliate has been added Successfully!'
        }
      end
      @affiliate = User.friendly.find params[:user][:format] if params[:user][:format].present?
      return redirect_to profile_isos_path(@affiliate)
    end
  end

  def create_affiliate
    begin
      if !params[:user].present?
       params[:user]={}
      end
      params[:user].merge!(@user_temp.iso_agent).merge!(@user_temp.buyrate).merge!(@user_temp.fees)
      @affiliate = User.new(new_affiliate_params)
      password = random_password
      @affiliate.password = password
      @affiliate.is_password_set = false
      @affiliate.is_block = false
      @affiliate.regenerate_token
      if @affiliate.affiliate!
        @affiliate.fees.each do |fee|
          fee.buy_rate!
          fee.high!
        end
        TransactionBatch.create(affiliate_wallet_id: @affiliate.wallets.first.id, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: "sale")
        if @affiliate.try(:[],'merchant_id').present?
          @merchant=User.friendly.find(@affiliate.try(:[],'merchant_id'))
          @merchant.affiliate=@affiliate
        end
        @affiliate.update company_name: @affiliate.try(:profile).try(:company_name) if @affiliate.profile.present?
        save_user_documentation(@affiliate, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
        message="Welcome to Quick Card"
        UserMailer.welcome_user(@affiliate ,message, 'Affiliate', password).deliver_later
        return {success: true, message: "Affiliate has been added Successfully"}
        respond_to do |format|
          format.html{
            flash[:success] = 'Affiliate has been added Successfully!'
          }
        end
      else
        flash[:error] = @affiliate.errors.full_messages.first
      end
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:error] = invalid
      return {success: false, message: invalid}
      # redirect_to users_path(user: "affiliates")
    end
  end

  def edit
    @affiliate = User.friendly.find params[:id]
    begin
      @affiliate['system_fee']['bank_account'] = AESCrypt.decrypt("#{@affiliate.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@affiliate['system_fee']['bank_account'])
    rescue
      @affiliate['system_fee']['bank_account']
    end
  end

  private

  def setting
    @user = User.find(id = params[:user_id])
    respond_to do |format|
      format.js {
        render 'admins/shared/profiles/setting.js.erb'
      }
    end
  end

  def user_data
    if params[:user_data_id].present?
      @user_temp = MerchantData.find_by(id: params[:user_data_id])
    end
  end

  def current_step
    steps.index(step) + 1
  end

  def finish_wizard_path
    session.delete(:index) if session[:index].present?
    return users_path(user: 'affiliates')
  end

  def new_affiliate_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,:is_password_set,
                                 profile_attributes: [:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code],
                                 system_fee: [
                                     :redeem_fee,
                                     :send_check,
                                     :giftcard_fee,
                                     :add_money_dollar,
                                     :add_money_percent,
                                     :b2b_dollar,
                                     :b2b_percent,
                                     # :invoice_card_fee_perc,
                                     # :invoice_card_fee,
                                     :ach_percent,
                                     :ach_dollar,
                                     :push_to_card_percent,
                                     :push_to_card_dollar,
                                     :bank_account,
                                     :bank_routing,
                                     :bank_account_type,
                                     :bank_account_name,
                                     :ach_limit,
                                     :ach_limit_type,
                                     :push_to_card_limit,
                                     :push_to_card_limit_type,
                                     :check_limit,
                                     :check_limit_type,
                                     :check_withdraw_limit,
                                     :failed_push_to_card_fee,
                                     :failed_ach_fee,
                                     :failed_check_fee
                                 ],
                                 fees_attributes: [:risk,:customer_fee,:b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,:ach_fee,:ach_fee_dollar,:dc_deposit_fee,:dc_deposit_fee_dollar,
                                                   :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,
                                                   :refund_fee, :service, :misc, :statement, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar, :qr_refund_percentage, :failed_push_to_card_percent_fee, :failed_ach_percent_fee, :failed_check_percent_fee])
  end

  def affiliate_update_params
    params.require(:user).except(:affiliate_id,:format, :profile_attributes).permit(:email, :password, :password_confirmation, :name, :phone_number, :company_name)
  end

  def affiliate_profile_params
    params[:user].require(:profile_attributes).permit(:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code)
  end

  def affiliate_signup_params
    # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,
                                 profile_attributes: [
                                     :company_name,
                                     :years_in_business,
                                     :tax_id,
                                     :street_address,
                                     :ein,
                                     :country,
                                     :city,
                                     :state,
                                     :zip_code
                                 ])
  end

  def affiliate_buyrate_params
    params.require(:user).permit(
        fees_attributes: [:id,:risk,:customer_fee,:b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
                          :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement,:dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee_dollar, :ach_fee, :qr_refund_percentage, :failed_push_to_card_percent_fee, :failed_ach_percent_fee, :failed_check_percent_fee])
  end

  def system_fee_params
    params.require(:user).permit(
        system_fee: [
            :redeem_fee,
            :send_check,
            :giftcard_fee,
            :add_money_dollar,
            :add_money_percent,
            :b2b_dollar,
            :b2b_percent,
            # :invoice_card_fee_perc,
            # :invoice_card_fee,
            :ach_percent,
            :ach_dollar,
            :push_to_card_percent,
            :push_to_card_dollar,
            :ach_limit,
            :ach_limit_type,
            :push_to_card_limit,
            :push_to_card_limit_type,
            :check_limit,
            :check_limit_type,
            :check_withdraw_limit,
            :failed_push_to_card_fee,
            :failed_ach_fee,
            :failed_check_fee
        ])
  end

  def edit_affiliate_params
    params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,
                                 system_fee: [
                                     :redeem_fee,
                                     :send_check,
                                     :giftcard_fee,
                                     :add_money_dollar,
                                     :add_money_percent,
                                     :b2b_dollar,
                                     :b2b_percent,
                                     # :invoice_card_fee_perc,
                                     # :invoice_card_fee,
                                     :ach_percent,
                                     :ach_dollar,
                                     :push_to_card_percent,
                                     :push_to_card_dollar,
                                     :bank_account,
                                     :bank_routing,
                                     :bank_account_type,
                                     :bank_account_name,
                                     :ach_limit,
                                     :ach_limit_type,
                                     :push_to_card_limit,
                                     :push_to_card_limit_type,
                                     :check_limit,
                                     :check_limit_type,
                                     :check_withdraw_limit,
                                     :failed_push_to_card_fee,
                                     :failed_ach_fee,
                                     :failed_check_fee
                                 ],
                                 fees_attributes: [:id,:risk,:customer_fee,:b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
                                                   :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,
                                                   :refund_fee, :service, :misc, :statement, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar, :qr_refund_percentage, :failed_push_to_card_percent_fee, :failed_ach_percent_fee, :failed_check_percent_fee])
  end

  # def affiliate_documentation_params
  #   params[:user].require(:documentation).permit(user_image:[], w9_form_image:[], schedule_a_image:[])
  # end

  # def edit_affiliate_params
  #   params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,
  #                                system_fee: [
  #                                    :redeem_fee,
  #                                    :send_check,
  #                                    :giftcard_fee,
  #                                    :add_money_dollar,
  #                                    :add_money_percent,
  #                                    :b2b_dollar,
  #                                    :b2b_percent,
  #                                    :ach_percent,
  #                                    :ach_dollar,
  #                                    :push_to_card_percent,
  #                                    :push_to_card_dollar
  #                                ],
  #                                fees_attributes: [:id,:risk,:customer_fee,:b2b_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
  #                                                  :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,
  #                                                  :refund_fee, :service, :misc, :statement, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar])
  # end
end
