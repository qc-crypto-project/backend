class Admins::ExportAccountsController < AdminsController
  require 'csv'
  layout "admin"

  def index
    @users = SequenceLib.export_accounts_list('usd')
  end

  def export
    @users = SequenceLib.export_accounts_list('usd')
    respond_to do |format|
      format.html
      format.csv { send_data generate_csv(@users), filename: "qc-accounts-#{Date.today}.csv" }
    end
  end
  def get_user_count
    # p params[:user_id]
    @wallet=Array.new
    us = User.find(params[:user_id])
    if us.present?
        us.wallets.primary.each do |w|
          @wallet << w.id
        end
    end
    start_date = params[:start_date]
    end_date = params[:end_date]
    p start_date,end_date
    count = SequenceLib.get_user_sales_count(@wallet,start_date,end_date)
    render :json => {hola: count.to_json}
  end
  private

  def read_file
    CSV.generate do |csv|
      csv << %w{ seq_parent_id timestamp Type gateway sender_wallet_id receiver_wallet_id total_amount_in_cents Tx_Amount_in_cents Privacy_fee Net_Amount fee_in_cents iso_fee gbox_fee agent_fee affiliate_fee tip_cents hold_money_cents}
      CSV.foreach('/Users/fahadkhan/Desktop/quick card/Reports/report 2019/data_march_final.csv', headers: true) do |row|
        time = row[1].to_datetime
        time = time.in_time_zone("Pacific Time (US & Canada)")
        csv << [row[0],time,row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13],row[14],row[15],row[16]]
      end
    end
  end

  def generate_csv(users)
    CSV.generate do |csv|
      csv << %w{ name email phone_number amount }
      users.each do |user|
        csv << [user[:name], user[:email], user[:phone_number],user[:amount]]
      end
    end
  end
end
