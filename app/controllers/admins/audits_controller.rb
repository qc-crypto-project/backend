class Admins::AuditsController < AdminsController
  layout 'admin'

  def users
    respond_to do |format|
      format.html
      format.json { render json: UsersReportDatatable.new(view_context) }
    end
  end

  def user_report
    @user = User.friendly.find(params[:id])
  end

  def transactions
    sequence_page = SequenceLib.transactions_page(nil)
    @next = sequence_page[:cursor]
    @transactions = parse_wallet(nil, sequence_page[:items])
  end

  def transaction_report
    @transaction = Transaction.where(seq_transaction_id: params[:id])
    if @transaction.present?
      @audits = @transaction.audits
    else
      @audits = []
    end
  end

  def parse_wallet(wallet, transactions)
    list = []
    admin_wallet = Wallet.find(wallet) if wallet.present?
    transactions.each do |obj|
      if obj.present?
        list << filter_actions(obj, wallet).map do |t|
          {
              id: t.id,
              timestamp: obj.timestamp,
              type: parse_type(wallet, t),
              amount: number_to_currency(SequenceLib.dollars(t.amount)),
              adminAmount: number_to_currency(parse_admin_amount(t)),
              destination: t.destination_account_id,
              source: t.source_account_id,
              reference: t.tags,
              wallet: admin_wallet
          }
        end
      end
    end
    list.reduce([], :concat).reject(&:nil?)
  end
  def filter_actions(obj, wallet)
    return obj.actions.select{|o| o.source_account_id == wallet.to_s || o.destination_account_id == wallet.to_s} if wallet.present?
    obj.actions
  end
  def parse_type(wallet, action)
    return action.type if wallet.nil?
    if action.type == 'issue' && action.destination_account_id == wallet.to_s
      'Deposit'
    else
      "Send To #{action.destination_account_id}"
    end
  end
  def parse_admin_amount(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    fee = transaction.tags['fee'].to_f
    if fee.present? && fee > 0 && amount != fee
      return number_with_precision(fee, precision: 2)
    else
      return number_with_precision(amount, precision: 2)
    end
  end
end
