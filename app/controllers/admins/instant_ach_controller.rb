require 'net/ftp'
require 'tempfile'
class Admins::InstantAchController < AdminsController
  before_action :check_admin
  include PosMerchantHelper

  def index
    @ach_ids = []
    @batch_date = Date.today
    params[:q] = JSON(params[:q]) if params[:q].present? && params[:q].class == String
    params[:batch_search_params] = JSON(params[:batch_search_params]) if params[:batch_search_params].present? && params[:batch_search_params].class == String
    if params[:batch_search_params].present?
      params[:batch_search_params] = JSON(params[:batch_search_params]) if params[:batch_search_params].class == String
      params[:q] = {}
      @achs = getting_batch_searched_achs
    elsif params[:batch_date].present?
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      if params[:batch_ach_id].present?
        category = @category = params[:batch_ach_id].split("-").first
        if category == "DI"
          wallets = Wallet.dispensary_wallets
        elsif category == "CB"
          wallets = Wallet.cbd_wallets
        # elsif category == "AI"
        #   wallets = Wallet.joins(:tango_orders).where("tango_orders.ach_international = ?", "true").uniq.pluck(:id)
        elsif category == "OT"
          wallets = Wallet.other_wallets
        end
        order_type = "instant_ach"
        if category == "AI"
          @achs = TangoOrder.batch_list(order_type, batch_start_date..batch_end_date, true,[false, nil])
        elsif category == "AFP"
          @achs = TangoOrder.batch_list(order_type, batch_start_date..batch_end_date, [false, nil] , true)
        elsif category == "OT"
          @achs = TangoOrder.batch_list(order_type, batch_start_date..batch_end_date, [false, nil], [false, nil])
          @achs = @achs.select{|ach| !wallets.include?(ach.wallet_id)}
        elsif category == "CB" || category == "DI"
          @achs = TangoOrder.batch_list(order_type, batch_start_date..batch_end_date, [false, nil], [false, nil])
          @achs = @achs.select{|ach| wallets.include?(ach.wallet_id)}
        else
          @achs = TangoOrder.order_type(order_type).batch_date(batch_start_date..batch_end_date)
        end
      end
      notifications = current_user.notifications.where(action: 'instant_ach').unread
      notifications.update_all(read_at: DateTime.now.utc)
    else
      @batch_ach_id = 0
      @achs = TangoOrder.includes(:wallet,:ach_gateway,:user).instant_ach
    end
    @grouped_achs = @achs.group_by{|ach| ach.status}
    @batch_date = params[:batch_date].to_date.strftime("%m/%d/%Y") if params[:batch_date].present?
    @batch_ach_id = params[:batch_ach_id] if params[:batch_ach_id].present?
    @ach_ids = @achs.pluck(:id)
    @total_withdraw = @achs.pluck(:actual_amount).sum
    @total_pending_amount = @grouped_achs[I18n.t("withdraw.pending")].try(:pluck, :actual_amount).try(:sum, &:to_f)
    @total_process_achs = @grouped_achs.select{|status, ach| status != I18n.t("withdraw.pending") && status != I18n.t("withdraw.failed") && status != I18n.t("withdraw.void")}.try(:values).try(:flatten).try(:pluck, :actual_amount).try(:sum, &:to_f)
    @void_requests = @grouped_achs[I18n.t("withdraw.void")].try(:count)
    @pending_achs = @grouped_achs[I18n.t("withdraw.pending")].try(:count)
    @paid_achs = @grouped_achs[I18n.t("withdraw.paid")].try(:count)
    @paid_by_wire_achs = @grouped_achs[I18n.t("withdraw.paid_by_wire")].try(:count)
    @failed_achs = @grouped_achs[I18n.t("withdraw.failed")].try(:count)
    @in_process_achs = @grouped_achs.select{|status, achs| status == I18n.t("withdraw.inprocess") || status == I18n.t("withdraw.inprogress")}.try(:values).try(:flatten).try(:count)
    @achs = Kaminari.paginate_array(@achs.reverse).page(params[:page]).per(params[:filter] || 10)
    @filters = [10,25,50,100]
  end

  def achs_search
    if params[:q].present?
      if params[:q] != "__"
        params[:q_value] = params[:q].try(:strip) if params[:q_value].blank?
        params[:q] = {"status_or_ach_gateway_descriptor_or_wallet_location_business_name_or_wallet_location_category_name_cont" => params[:q_value]}
        params[:q][:id_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
        params[:q][:actual_amount_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
        params[:q][:amount_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
      end
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      if params[:batch_ach_id].present?
        category = params[:batch_ach_id].split("-").first
        if category == "DI"
          wallets = Wallet.dispensary_wallets
        elsif category == "CB"
          wallets = Wallet.cbd_wallets
        elsif category == "OT"
          wallets = Wallet.other_wallets
        end
        order_type = "instant_ach"
        if category == "AI"
          achs1 = TangoOrder.batch_list_with_ransack(order_type, true, params[:q].try(:merge, m: 'or'), [false,nil])
        elsif category == "AFP"
          achs1 = TangoOrder.batch_list_with_ransack(order_type, [false,nil], params[:q].try(:merge, m: 'or') , true)
        elsif category == "OT"
          achs1 = TangoOrder.batch_list_with_ransack(order_type, [false,nil], params[:q].try(:merge, m: 'or') ,[false, nil])
          achs1 = achs1.select{|ach| !wallets.include?(ach.wallet_id)}
        elsif category == "CB" || category == "DI"
          achs1 = TangoOrder.batch_list_with_ransack(order_type, [false, nil], params[:q].try(:merge, m: 'or') ,[false, nil])
          achs1 = achs1.select{|ach| wallets.include?(ach.wallet_id)}
        else
          achs1 = TangoOrder.order_type(order_type).batch_date(batch_start_date..batch_end_date).ransack(params[:q].try(:merge, m: 'or')).try(:result)
        end
      end
      achs1 = achs1.select{|ach| (batch_start_date..batch_end_date).include?(ach.batch_date)}
    else
      params[:batch_search_params] = JSON params[:batch_search_params] if params[:batch_search_params].present?
      achs1 = getting_batch_searched_achs
    end
    @achs = Kaminari.paginate_array(achs1).page(params[:page]).per(params[:filter] || 10)
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js {render 'admins/instant_ach/filtered_achs.js.erb'}
    end
  end

  def batches_search
    #   @cat = "OT"
    #   wallet = Wallet.joins(location: :category).where("categories.name ILIKE  '%CBD%' or categories.name ILIKE  '%Dispensary%' or categories.name ILIKE  '%Hemp%'").pluck(:id)
    #   achs1 = TangoOrder.where.not(wallet_id: wallet)
    params[:q] = {}
    if params[:query].present?
      params[:query]["find_by"] = params[:query]["find_by"].try(:strip)
    end
    if params[:query][:option].present?
      if params[:query][:option] == "id"
         params[:q][:id_eq] = params[:query]["find_by"]
      elsif params[:query][:option] == "business_name"
        params[:q][:wallet_location_business_name_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "ach_gateway_descriptor"
        params[:q][:ach_gateway_descriptor_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "name"
        params[:q][:name_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "category"
        params[:q][:wallet_location_category_name_cont] = params[:query]["find_by"]
      elsif params[:query][:option] == "actual_amount"
        params[:q][:actual_amount_eq] = params[:query]["find_by"]
      elsif params[:query][:option] == "amount"
        params[:q][:amount_eq] = params[:query]["find_by"]
      end
    end
    achs = TangoOrder.ransack(params[:q].try(:merge, m: 'or'))
    @ach_batches = achs.result.includes(:ach_gateway).instant_ach.group_by_day(time_zone: "Pacific Time (US & Canada)", day_start: 14) {|u| u.batch_date }
    render 'admins/instant_ach/batches.html.erb'
  end

  def export
    @batch_date = Date.today
    ach_ids = JSON.parse("#{params[:ach_ids]}")
    if ach_ids.present?
      @batch_date = params[:batch_date]
      @achs = TangoOrder.where(id: ach_ids).order(id: :desc)
    else
      @achs = TangoOrder.instant_ach.order(id: :desc)
    end
    gateway = AchGateway.find_by(id: params[:ach_gateway_id])
    respond_to do |format|
      format.html
      format.csv { send_data generate_csv(@achs.except(:limit, :offset),gateway, params[:offset]) , filename:"Ach_#{@batch_date}.csv" }
    end
  end

  def new
    @merchants=User.merchant || []
    @locations= []
    @wallets= Wallet.all || []
    respond_to :js
  end

  def update_recipient
    if params[:check_id].present? && params[:tango_order][:recipient].present?
      check = TangoOrder.find(params[:check_id])
      check.recipient = params[:tango_order][:recipient]
      check.save!
    end
  end

  def selected_locations
    if params[:merchant_id].present?
      user = User.find(params[:merchant_id])
      @locations = user.get_locations
    else
      @locations = []
    end
    respond_to :js
  end

  def instant_ach_batch_date
    if params[:ach_id].present? && params[:batch_date].present?
      @ach_pending=TangoOrder.find(params[:ach_id])
      datetime = Date.strptime(params[:batch_date], "%m/%d/%Y").to_datetime.end_of_day
      if @ach_pending.update(:batch_date => datetime)
        flash[:success] = "Batch Date Updated Successfully"
        redirect_to admins_total_funds_path
      end
    end
  end

  def instant_ach_approved
    if params[:ids].present? && params[:ach_gateway_id].present?
      achs = params[:ids].split(',')
      achs.each do|c|
        check = TangoOrder.find_by(id: c.to_i)
        if check.present? && check.status != "PENDING" || check.status != "IN_PROCESS"
          flash[:error] = "#{check.id} is already approved. Please remove it and try again later!"
          break
        end
        if check.present?
          result = instant_ach_approve_func(check,params[:ach_gateway_id])
          if result[:success] == false
            if result[:message].present?
              flash[:error] = result[:message]
            else
              if check.checkId.present?
                flash[:error] = "We have Error on #{check.checkId.last(6)}. Please try again later!"
              else
                flash[:error] = "We have an Error on #{check.id}. Please try again later!"
              end
            end
            break
          else
            flash[:success] = "Successfully approved!"
          end
        else
          flash[:error] = "Something went wrong. Please try again later!"
        end
      end
    else
      flash[:error] = "Please select transactions!"
    end
    return redirect_back(fallback_location: root_path )
  end

  def batches
    @ach_batches = TangoOrder.instant_ach.group_by_day(time_zone: "Pacific Time (US & Canada)", day_start: 14) {|u| u.batch_date }
  end

  def location_data
    if params[:location_id].present?
      @location = Location.find(params[:location_id])
    else
      @location = Location.new
    end
    begin
      @bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT-ENCRYPTOR']}",@location.bank_account)
    rescue
      @bank_account = @location.bank_account
    end
    walet_id = @location.wallets.primary.last.id
    @balance = show_balance(walet_id) - HoldInRear.calculate_pending(walet_id)
    respond_to :js
  end

  def approve_ach_request
    @user=User.find(params[:id]) if params[:id].present?
    if @user.merchant?
      @locations_id=@user.get_locations.pluck(:id)
      @banks=BankDetail.where(location_id:@locations_id)
      @locations=@banks.each do |bank|
        bank.account_no=AESCrypt.decrypt("#{bank.location_id}-#{ENV['ACCOUNT_ENCRYPTOR']}",bank.account_no)
      end
      # @locations=@user.get_locations.each do |loc|
      #
      #    loc.bank_account=AESCrypt.decrypt("#{loc.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",loc.bank_account)
      # end
    else
      @locations=@user.wallets.primary.first
      @wallet=@user.wallets.primary.first.id
      @img= Image.where(location_id: @wallet).first
      @user['system_fee']['bank_account']=AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user['system_fee']['bank_account'])
    end
  end
  def disapprove_ach_request
    user_id = params[:id]
    user = User.find user_id
    if user.merchant_id.present?
      user.update(send_ach_bank_request: false)
      main_user = User.find_by(id: user.merchant_id)
      users = User.where(id: user.id).or(User.where(id: main_user.id))
      users.update_all(merchant_ach_status: "initial")
    else
      user.update(merchant_ach_status: "initial",send_ach_bank_request: false)
    end
    if user.merchant?
      if user.merchant_id.present?
        @location = main_user.my_locations.pluck(:id)
      else
        @location = user.my_locations.pluck(:id)
      end
      @location.each do |location|
         l= Location.find_by(id: location)
          l.bank_details
          l.bank_details.destroy_all
      end
    else
        @wallet=user.wallets.primary.first.id
        @img= Image.where(location_id: @wallet).first
        @img.delete
        user.merchant_ach_status= "initial"
        user.send_ach_bank_request= false
        user['system_fee']['bank_account']= {}
        user['system_fee']['bank_routing']=  {}
        user['system_fee']['bank_account_type']={}
        user['system_fee']['bank_account_name']={}
        user.save
    end
    return redirect_to user_request_verification_admins_path(req: "banks")
  end

  def edit_bank_details
    @user=User.find(params[:user_id]) if params[:user_id].present?
    @img= Image.where(location_id: params[:wallet_id]).first
    @user['system_fee']['bank_account']=AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@user['system_fee']['bank_account'])
  end

  def update_bank_details
    @user=User.find(params[:format]) if params[:format].present?
    @user.system_fee["bank_account_type"] =  params[:user][:system_fee][:bank_account_type]
    @user.system_fee['bank_account_name'] = params[:bank_name]
    @user.system_fee["bank_routing"] = params[:bank_routing]
    @user.system_fee["bank_account"] = params[:bank_account]

    @user.system_fee["bank_account"] = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])

    if @user.save
       if params[:user][:image].present?
         @old_img = Image.where(location_id:  params[:location_id]).last
         if @old_img.present?
           @old_img.delete
         end
         image={add_image:params[:user]['image'],location_id:params[:location_id]}
         @doc= Image.new(image)
         if @doc.validate
           @doc.save
         else
           @error = @img.errors.full_messages.first
         end
       end
     flash[:success] = "Successfully updated the Bank Information"
        redirect_back(fallback_location: root_path)
     else
       flash[:error] = "Can't updated the Bank Information"
      redirect_back(fallback_location: root_path)
    end
  end

  def approve_request
    user = User.find_by(id: params[:id])
    date_time = DateTime.now
    if user.merchant?
      if user.merchant_id.present?
        main_user = User.find_by(id: user.merchant_id)
      else
        main_user = user
      end
      users = User.where(id: main_user.id).or(User.where(merchant_id: main_user.id))
      users.update_all(merchant_ach_status: "complete",  approve_date: date_time)
    else
      user.update(merchant_ach_status: 'complete', approve_date: date_time)
    end

    flash[:notice] = "ACH Request approved!"
    return redirect_to user_request_verification_admins_path(req: "banks")
  end

  def image_delete
    if params[:table] == "bank"
      img=Image.where(bank_detail_id: params[:id]).first
    else
      img=Image.where(location_id: params[:id]).first
    end
    if img.delete
      render json: true
    end
  end

  def get_bank_by_routing_number
    parameters = [["routing_number", "#{params[:routing_number]}"]]
    uri = URI("#{ENV['CHECKBOOK_ROUTING_URL_LIVE']}get_bank_by_routing_number")
    # Sending Request To Checkboox IO
    response = Net::HTTP.post_form( uri, parameters)
    if response.is_a?(Net::HTTPSuccess)
      if response.read_body.present?
        render status: 200 , json:{:response => response.read_body}
      else
        render status: 200 , json:{:response => ""}
      end
    else
      render status: 200 , json:{:response => ""}
    end
  end

  def verification
  end

  def verify_phone_number
    invoice_ids = JSON.parse(params[:invoice_id])
    achs = TangoOrder.where(id: invoice_ids)
    # invoice = TangoOrder.find(params[:invoice_id])
    if params[:phone_number].present?
      verification_phone_numbers = ENV["VERIFICATION_PHONE_NUMBERS"].present? ? ENV["VERIFICATION_PHONE_NUMBERS"].split(",") : [ENV['PHONE_NUMBER_FOR_PAY_INVOICE2'],ENV['PHONE_NUMBER_FOR_PAY_INVOICE1']]
      if (verification_phone_numbers.include?(params[:phone_number]))
        @random_code = rand(1_000..9_999)
        achs.update_all(pincode: @random_code)
        # a = params[:phone_number]
        a = "1"+params[:phone_number]
        TextsmsWorker.perform_async(a, "Your pin Code for Export is: #{@random_code}")
      else
        render status: 404, json:{error: 'Wrong Phone Number'}
      end
    end
    if params[:pincode].present?
      if achs.pluck(:pincode).uniq.count == 1 && achs.pluck(:pincode).uniq.first == params[:pincode]
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Wrong Pin Code'}
      end
    end
  end


  def bulk_status_change
    begin
      ids = params[:ids]
      ids = ids.split(",").map { |s| s.to_i }
      params[:type] = params[:ach_status]
      params[:status] = I18n.t("withdraw.void")
      # params[:charge] = "charge"
      params[:issue] = "issue"
      false_achs = []
      result = nil
      if ids.present?
        if params["ach_gateway_id"].present?
          ach_gateway_id = params["ach_gateway_id"].to_i
          ach_gateway = AchGateway.find(ach_gateway_id)
          # ach_gateway_type = ach_gateway.ach_type
          if ach_gateway.manual_integration?
            ids.each do |id|
              check = TangoOrder.find_by(id: id)
              params[:id] = id
              if params[:type] != I18n.t("withdraw.failed") && (check.status == I18n.t("withdraw.paid") || check.status == I18n.t("withdraw.paid_by_wire"))
                false_achs.push(check.id)
              elsif check.status.try(:downcase) === 'failed'
                false_achs.push(check.id)
              elsif check.status == I18n.t("withdraw.void")
                false_achs.push(check.id)
              elsif params[:type] == I18n.t("withdraw.void") && (check.status == I18n.t("withdraw.inprocess") || check.status == I18n.t("withdraw.inprogress"))
                false_achs.push(check.id)
              elsif check.status == I18n.t("withdraw.pending") && !(params[:type] == I18n.t("withdraw.inprocess") || params[:type] == I18n.t("withdraw.void") )
                false_achs.push(check.id)
              elsif params[:type] == I18n.t("withdraw.inprocess") && (check.status == "VOID" || check.status == I18n.t("withdraw.inprocess") || check.status == I18n.t("withdraw.inprogress"))
                false_achs.push(check.id)
              else
                result = withdrawal_bulk_status_update
              end
              if result.present?
                if result[:success]
                  flash[:success] = result[:message]
                else
                  raise StandardError.new result[:message]
                end
              end
            end
            false_achs = false_achs.join(',')
            if false_achs.length > 0
              flash[:error] = "The Selected Status can not be applied to ID #{false_achs}. Please remove it and try again later!"
            end
          elsif ach_gateway.try(:ftp?)
              flash[:success] = "ACH Gateway of type FTP is currently unavailable. Please select from other types under your ACH Gateway."
          elsif ach_gateway.direct_integration?
            if params[:type] == I18n.t("withdraw.failed") && (check.status == I18n.t("withdraw.paid") || check.status == I18n.t("withdraw.paid_by_wire"))
              false_achs.push(check.id)
            else
              result = instant_ach_approved_direct
            end
          end
        else
          ids.each do |id|
            check = TangoOrder.find_by(id: id)
            params[:id] = id
            if params[:type] != I18n.t("withdraw.failed") && (check.status == I18n.t("withdraw.paid") || check.status == I18n.t("withdraw.paid_by_wire")) && !check.ach_gateway.try(:manual_integration?)
              false_achs.push(check.id)
            elsif check.status == I18n.t("withdraw.failed") || check.status == "Failed"
              false_achs.push(check.id)
            elsif check.status == I18n.t("withdraw.void")
              false_achs.push(check.id)
            elsif params[:type] == I18n.t("withdraw.void") && (check.status == I18n.t("withdraw.inprocess") || check.status == I18n.t("withdraw.inprogress"))
              false_achs.push(check.id)
            elsif check.status == I18n.t("withdraw.pending") && !(params[:type] == I18n.t("withdraw.inprocess") || params[:type] == I18n.t("withdraw.void") )
              false_achs.push(check.id)
            elsif params[:type] == I18n.t("withdraw.inprocess") && (check.status == I18n.t("withdraw.void") || check.status == I18n.t("withdraw.inprocess") || check.status == I18n.t("withdraw.inprogress"))
              false_achs.push(check.id)
            elsif params[:type] == I18n.t("withdraw.failed") && (check.status == I18n.t("withdraw.paid") || check.status == I18n.t("withdraw.paid_by_wire")) && !check.ach_gateway.try(:manual_integration?)
              false_achs.push(check.id)
            else
              result = withdrawal_bulk_status_update
            end
            if result.present?
              if result[:success]
                flash[:success] = result[:message]
              else
                raise StandardError.new result[:message]
              end
            end
          end
          false_achs = false_achs.join(',')
          if false_achs.length > 0
            flash[:error] = "The Selected Status can not be applied to ID #{false_achs}. Please remove it and try again later!"
          end
        end
      end
    rescue => ex
      flash[:error] = ex.message
    ensure
      return redirect_back(fallback_location: root_path )
    end
  end

  ################################################


  def instant_ach_approved_direct
    if params[:ids].present? && params[:ach_gateway_id].present?
      achs = params[:ids].split(',')
      send_file = AchGateway.find_by_id params[:ach_gateway_id]
      if send_file.present?
        flash[:error] = "Please select less than or equal to 100 ach" if achs.count > 100
        checks_from_ids = TangoOrder.where(id: achs)
        checks_from_ids = checks_from_ids.reject{|key, value| key.status != "PENDING"}
        tchecks = TangoOrder.where(id: achs).group_by{|e| e.status}
        if tchecks.present?
          checks = tchecks.reject{|key, value| key != "PENDING"}
          datetime = DateTime.now
          date = datetime.in_time_zone("Pacific Time (US & Canada)").time
          file = File.new(Rails.root + "tmp/ach-#{DateTime.now.to_i}.csv", "w")
          file << CSV.generate do |csv|
            checks.values.flatten.each do |check|
              user_id = check.user_id
              decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{check.wallet_id}",  check.account_token)
              acc_number = JSON.parse(decrypted) if decrypted.present?
              check_name = check.name.gsub(/[^0-9A-Za-z]/, '')
              csv << [2092, check_name,2092, check_name, check.account_type.first(1),"cr", date.strftime("%Y-%m-%d"), "ACH-#{check.id}", check_name, check.routing_number,acc_number.try(:[], 'account_number'), "version 1.0", check.actual_amount]
            end
          end
          file.close
          if checks.present?
            upload = Payment::InstantAch.new(file)
            data = upload.start_process
            if data.present?
              data = JSON.parse(data)
              if data.kind_of?(Array)
                if data[0].include? "loaded"
                  file_id = data[0].split("loaded=")
                  file_id = file_id[1].to_i
                  flash[:success] = "File Uploaded Successfully"
                  checks = TangoOrder.where(id: achs, status: "PENDING")
                  checks.update_all(status: 'PAID',shape_file_id: file_id, ach_gateway_id: params[:ach_gateway_id], approved: true)
                  open_file = File.open(file, "r")
                  strfile = StringIO.new(open_file.read)
                  qc_file = AchFile.new(file_id: file_id, file: strfile)
                  qc_file.file.instance_write(:content_type, 'text/csv')
                  qc_file.file.instance_write(:file_name, "ach_file_export.csv")
                  qc_file.save
                elsif data[0].include? "duplicate"
                  flash[:error] = "File is duplicate"
                end
              end
              if checks_from_ids.present?
                checks_from_ids.each do|check|
                  instant_ach_approve_func(check,params[:ach_gateway_id])
                end
              end
            else
              flash[:error] = "Batch failed, please try sending the checks individually"
            end

          else
            flash[:error] = "All ACHs are already approved"
          end
        end
      else
        achs.each do|c|
          check = TangoOrder.find_by(id: c.to_i)
          if check.present? && check.status != "PENDING"
            flash[:error] = "#{check.id} is already approved. Please remove it and try again later!"
            break
          end
          if check.present?
            result = instant_ach_approve_func(check,params[:ach_gateway_id])
            if result[:success] == false
              if result[:message].present?
                flash[:error] = result[:message]
              else
                if check.checkId.present?
                  flash[:error] = "We have Error on #{check.checkId.last(6)}. Please try again later!"
                else
                  flash[:error] = "We have an Error on #{check.id}. Please try again later!"
                end
              end
              break
            else
              flash[:success] = "Successfully approved!"
            end
          else
            flash[:error] = "Something went wrong. Please try again later!"
          end
        end
      end
    else
      flash[:error] = "Please select transactions!"
    end
  end

  private

  ##########################################################################



  def generate_csv_ftp(checks,gateway, offset = nil)
    CSV.generate do |csv|
      export_columns = gateway.try(:export_col)
      export_hash = {"routing_number"=>"ROUTING", "account_number"=>"ACCOUNT", "amount"=>"AMOUNT", "name"=>"NAME", "account_type"=>"TYPE", "checkId"=>"UNIQUE ID"}
      export_columns.each do |arr|
        if arr["key"] != "routing_number" && arr["key"] != "account_number" && arr["key"] != "amount" && arr["key"] != "name" && arr["key"] != "account_type" && arr["key"] != "checkId"
          export_hash[arr["key"]] = arr["value"]
        end
      end
      csv << export_hash.values
      # csv << ["Bank Routing #","Account #", "Amount (USD)", "Name On Bank Account", "Account Type", "Unique ID"]
      checks.each do |c|
        data = {}
        if export_hash.keys.any? {|a| ["account_number", "account_type"].include?(a)}
          # begin
          user_id = c.user_id
          decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
          data = JSON(decrypted)
        end
        arr = []
        export_hash.keys.each do |key|
          if key == "routing_number"
            routing_number = "'#{c.routing_number}"
            arr.push(routing_number)
          elsif key == "account_number"
            account_number = "'#{data["account_number"]}"
            arr.push(account_number)
          elsif key == "amount"
            amount = c.public_send(:actual_amount)
            amount_updated = number_with_precision(amount.to_f, :precision => 2)
            arr.push(amount_updated.to_f)
          elsif key == "account_type"
            account_type = "CHK"
            # acc_type = data["account_type"]
            # if acc_type.include?('checking')
            #   account_type = "CHK"
            # else
            #   account_type = data["account_type"]
            # end
            arr.push(account_type)
          elsif key == "checkId"
            arr.push(c.id)
          elsif key == "category"
            wallet = c.wallet
            if wallet.location.present?
              category = wallet.location.try(:category).try(:name)
            else
              category = c.user.name
            end
            arr.push(category)
          elsif key == "batch_date" && offset.present?
            arr.push(c.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"))
          else
            if key == "Check_ID"
              key = "checkId"
            end
            arr.push(c.public_send(key))
          end
        end
        csv << arr
      end
    end
  end



  ##########################################################################

  def generate_csv(checks,gateway, offset = nil)
  CSV.generate do |csv|
      export_columns = gateway.try(:export_col)
      export_hash = {}
      export_columns.each do |arr|
        if arr["value"].present? && arr["checked"] == "on"
          export_hash[arr["key"]] = arr["value"]
        end
      end
      csv << export_hash.values
      # csv << ["Bank Routing #","Account #", "Amount (USD)", "Name On Bank Account", "Account Type", "Unique ID"]
      checks.each do |c|
        data = {}
        if export_hash.keys.any? {|a| ["account_number", "account_type"].include?(a)}
          # begin
            user_id = c.user_id
            decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
            data = JSON(decrypted)
          # rescue Exception => exc
          #   if c.user.merchant_id.blank?
          #     user_ids = c.user.sub_merchants.pluck(:id)
          #     user_ids.each do |user_id|
          #       begin
          #         decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
          #         data = JSON(decrypted)
          #         break
          #       rescue Exception => e
          #       end
          #     end
          #   else
          #     user_id = c.user.merchant_id
          #     begin
          #       decrypted = AESCrypt.decrypt("#{user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
          #       data = JSON(decrypted)
          #     rescue Exception => e
          #     end
          #   end
          # end
        end
        arr = []
        export_hash.keys.each do |key|
          if key == "account_number"
              account_number = "'#{data["account_number"]}"
              arr.push(account_number)
          elsif key == "account_type"
              account_type = data["account_type"]
              arr.push(account_type)
          elsif key == "routing_number"
              routing_number = "'#{c.routing_number}"
              arr.push(routing_number)
          elsif key == "amount"
            amount = c.public_send(key)
            amount_updated = number_with_precision(amount.to_f, precision: 2)
            arr.push(amount_updated)
          elsif key == "actual_amount"
            amount = c.public_send(key)
            amount_updated = number_with_precision(amount.to_f, precision: 2)
            arr.push(amount_updated)
          elsif key == "category"
            wallet = c.wallet
            if wallet.location.present?
              category = wallet.location.try(:category).try(:name)
            else
              category = c.user.name
            end
            arr.push(category)
          elsif key == "checkId"
            arr.push(c.id)
          elsif key == "batch_date" && offset.present?
            arr.push(c.created_at.to_datetime.new_offset(offset).strftime("%m/%d/%Y %I:%M:%S %p"))
          elsif key == "dba_flag_text"
            arr.push(c.wallet.try(:location).try(:ach_flag_text))
          else
            if key == "Check_ID"
              key = "id"
            end
            arr.push(c.public_send(key))
          end
        end
        csv << arr
        # decrypted = AESCrypt.decrypt("#{c.user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
        # data = JSON(decrypted)
        # account_number = data["account_number"]
        # account_type = data["account_type"]
        # csv << [c.id, c.user_id,c.name,c.recipient, number_to_currency(number_with_precision(c.actual_amount.to_f, :precision => 2)), c.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y %r"), c.status,account_number,account_type, c.routing_number, batch_date]
        # csv << [c.routing_number, account_number,number_to_currency(number_with_precision(c.actual_amount.to_f, :precision => 2)),c.name,account_type,c.checkId ]
      end
    end
  end

  def getting_batch_searched_achs
    params[:q] = params[:batch_search_params]
    @achs = batch_query_search_params
    return @achs
  end

end
