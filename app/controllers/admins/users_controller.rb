class Admins::UsersController < AdminsController
  include AdminsHelper
  include Merchant::TransactionsHelper
  include Admins::UsersHelper
  before_action :show_user , only: [:show, :update, :destroy, :edit]
  skip_before_action :authenticate_user!, only: [:create]
  skip_before_action :check_admin
  skip_before_action :mtrac_admin
  before_action :set_params, only: :index

  def index
    if params[:search_input].present?
      search_users(params)
    elsif params[:query].present?
      if params[:query].reject{|k,v| v.blank? }.blank?
        flash[:notice] = "All search fields are empty!"
        get_users
      else
        get_users_with_advance_search
      end
    else
      get_users
    end
    @company = Company.new
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js
      format.html
    end
  end

  def new
    @greenbox_user = User.find_by(id: params[:id])
    render partial: "new"
  end

  def create
    begin
      user = User.where(id: params[:user].try(:[],:id)).first_or_initialize
      params[:user][:name] = "#{params[:user].try(:[],:first_name)} #{params[:user].try(:[],:last_name)}"
      params[:user][:phone_number] = "#{params[:user].try(:[],:phone_code)}#{params[:user].try(:[],:phone_number)}"
      user.update(new_user_params.reject{|k,v| v.blank?})
      flash[:notice] = params[:user].try(:[],:id).present? ? "Update Successfully!" : "Created Successfully!"
    rescue => ex
    ensure
      redirect_back(fallback_location: root_path)
    end

  end

  def transactions
    @filters = [10,25,50,100]
    @transactions = []
    @tickets = []
    @page_ticket = 0
    @amount_deposit = 0
    @amount_spend = 0
    @weekly_deposit_limit = 0
    @weekly_spend_limit = 0
    @transactions_count = 0
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : params[:tx_filter1].present? ? params[:tx_filter1].to_i : 10
    @wallet = Wallet.friendly.find(params[:user_id])
    @user = @wallet.users.first
    if @user.user? || @user.qc? || @wallet.check_escrow? || @user.support? || @user.role.blank? || @user.merchant?
        payment_gateways = PaymentGateway.active_gateways
        payment_gateways = payment_gateways.map {|pg| {:value=> pg.key, :view=> pg.name}} if payment_gateways.present?
        @gateways = [{value:TypesEnumLib::GatewayType::Checkbook,view:"Checkbook.io"},{value:TypesEnumLib::GatewayType::Quickcard,view:"Quickcard"},{value:TypesEnumLib::GatewayType::Tango,view:"Tango"},{value:TypesEnumLib::GatewayType::PinGateway,view:"Pin Gateway"},{value:TypesEnumLib::GatewayType::PinGateway,view:"Pin Gateway"}]
        @gateways = @gateways.push(payment_gateways).flatten.uniq if payment_gateways.present?
        @merchants_txn_type = Transaction::ISO_SEARCH_TYPES[:merchant_txn_type_others] unless @user.qc?
        @types = Transaction::ISO_SEARCH_TYPES[:ledger_user_types]
        @merchants_txn_type = Transaction::ISO_SEARCH_TYPES[:merchant_txn_types_qc] if @user.qc?
    end
    offset = params[:offset].present? ? params[:offset] : cookies[:timezone].present? ? Time.now.in_time_zone(cookies[:timezone]).strftime("%:z") : Time.now.in_time_zone(TIMEZONE).strftime("%:z")
    @wallet_balance = SequenceLib.balance(@wallet.id)
    if @user.present? && @user.merchant? && !@wallet.tip?
      @processed_amount = Transaction.where('sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string', wallets: @wallet.id, string: "#{@wallet.id}").where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee"]).sum(:total_amount)
      @hold_in_rear_amount= HoldInRear.calculate_pending(@wallet.id)
      @withdraw_amount=@wallet_balance.to_f - @hold_in_rear_amount.to_f
      @today_release=HoldInRear.calculate_released_on_date_custom(@wallet.location_id, Time.zone.now)
      @tomorrow_release=HoldInRear.calculate_released_on_date_custom(@wallet.location_id, Time.zone.now + 1.day)
    end
    flash[:notice] = "Page not available!"
    return redirect_to transactions_admins_path(trans: "success", offset: offset) if current_user.present? && (current_user.support_mtrac? || current_user.support?)
    flash.clear
    if @user.MERCHANT? && @wallet.location.present?
      @location = @wallet.location
      @i_am_location = "iamlocation"
    else
      @location = @user
    end
    @iso_agent_aff = true if ["qc", "iso", "partner", "agent", "affiliate", "support"].include? @user.role
    if params[:tab].present?
      if params[:tab] == 'menu_1'
      elsif params[:tab] == 'menu_2'
        if @user.present? && @user.merchant? && @wallet.primary?
          @transactions = Transaction.where('sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string', wallets: @wallet.id, string: "#{@wallet.id}").where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee"]).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
        elsif @user.present? && @user.user?
          conditions = user_wallet_search_conditions(params)
          @role = @user.role
          if params[:q].present?
            @transactions = Transaction.left_outer_joins(:sender,:receiver, :payment_gateway).where('sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string', wallets: @wallet.id, string: "#{@wallet.id}").where(status: ["approved", "refunded", "pending","complete","CBK Won", "Chargeback", "cbk_fee"]).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
          else
            @transactions = Transaction.where('sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string', wallets: @wallet.id, string: "#{@wallet.id}").where(status: ["approved", "refunded", "pending","complete","CBK Won", "Chargeback", "cbk_fee"]).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
          end
        else
          @role = @user.role
          page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
          if params[:q].present?
            @transactions = ledger_wallets_search(params[:q], @wallet.id, page_size, params[:page], params[:offset])
          else
            @transactions = BlockTransaction.where('sender_wallet_id = :wallet OR receiver_wallet_id = :wallet', wallet: @wallet.id).order(timestamp: :desc).per_page_kaminari(params[:page]).per(page_size)
          end
        end
      elsif params[:tab] == 'menu_3'
        @page_ticket  = params[:page_ticket].present? ? params[:page_ticket].to_i : 1
        @tickets = @user.tickets.page(@page_ticket)
      elsif params[:tab] == 'menu_4'
        @total_disputes = DisputeCase.wallet_disputes(@wallet.id,page_size).per_page_kaminari(params[:page]).per(page_size)
      elsif params[:tab] == 'menu_5'
        filter=params[:tx_filter].present? ? params[:tx_filter] : 10
        @checks = TangoOrder.where(order_type: [0] ,wallet_id: @wallet.id).order(id: :desc) if @wallet.present?
        if @checks.present?

          @unpaid_check=@checks.select{|v| v.status == "UNPAID"}.count
          captured=@checks.select{|v| v.status == "captured"}.count
          process=@checks.select{|v| v.status == "IN_PROCESS"}.count
          @inprocess_check=captured+process
          @void_check = @checks.where(status: ["VOID"]).count
          @pending_check = @checks.where(status: ["PENDING"]).count
          @total_pending_amount = @checks.where(status: ["PENDING"]).pluck(:actual_amount).try(:sum, &:to_f)
          @total_process_achs = @checks.where(status: ["PAID","UNPAID","IN_PROGRESS","IN_PROCESS"]).pluck(:actual_amount).try(:sum, &:to_f)
          @paid_check = @checks.where(status: ["PAID"]).count
          @paid_by_wire_check = @checks.where(status: ["PAID_WIRE"]).count
          @failed_check = @checks.where(status: ["FAILED"]).count
          @total_withdraw = @checks.pluck(:actual_amount).try(:sum, &:to_f)
          @dollar_fee = @checks.pluck(:check_fee).try(:sum, &:to_f)
          @percent_fee = @checks.pluck(:fee_perc).try(:sum, &:to_f)
          @total_fee = @percent_fee.to_f + @dollar_fee.to_f
          @total_checks=@checks.count
        end
        @checks = @checks.per_page_kaminari(params[:page]).per(filter)
      elsif params[:tab] == 'menu_6'
        filter=params[:tx_filter].present? ? params[:tx_filter] : 10
        @ach = TangoOrder.where(order_type: [5,6] ,wallet_id: @wallet.id).order(id: :desc) if @wallet.present?
        if @ach.present?
          @void_check = @ach.where(status: ["VOID"]).count
          @pending_check = @ach.where(status: ["PENDING"]).count
          @total_pending_amount = @ach.where(status: ["PENDING"]).pluck(:actual_amount).try(:sum, &:to_f)
          @total_process_achs = @ach.where(status: ["PAID","UNPAID","IN_PROGRESS","IN_PROCESS"]).pluck(:actual_amount).try(:sum, &:to_f)
          @paid_check = @ach.where(status: ["PAID"]).count
          @paid_by_wire_check = @ach.where(status: ["PAID_WIRE"]).count
          @failed_check = @ach.where(status: ["FAILED"]).count
          @total_withdraw = @ach.pluck(:actual_amount).try(:sum, &:to_f)
          @dollar_fee = @ach.pluck(:check_fee).try(:sum, &:to_f)
          @percent_fee = @ach.pluck(:fee_perc).try(:sum, &:to_f)
          @total_fee = @percent_fee.to_f + @dollar_fee.to_f
          @total_checks=@ach.count
          @unpaid_check=@ach.select{|v| v.status == "UNPAID"}.count
          captured=@ach.select{|v| v.status == "captured"}.count
          process=@ach.select{|v| v.status == "IN_PROCESS"}.count
          @inprocess_ach=captured+process
        end
        @ach = @ach.per_page_kaminari(params[:page]).per(filter)
      elsif params[:tab] == 'menu_7'
        filter=params[:tx_filter].present? ? params[:tx_filter] : 10
        @p2c = TangoOrder.where(order_type: [7] ,wallet_id: @wallet.id).order(id: :desc) if @wallet.present?
        if @p2c.present?
          @void_check = @p2c.where(status: ["VOID"]).count
          @pending_check = @p2c.where(status: ["PENDING"]).count
          @total_pending_amount = @p2c.where(status: ["PENDING"]).pluck(:actual_amount).try(:sum, &:to_f)
          @total_process_achs = @p2c.where(status: ["PAID","UNPAID","IN_PROGRESS","IN_PROCESS"]).pluck(:actual_amount).try(:sum, &:to_f)
          @paid_check = @p2c.where(status: ["PAID"]).count
          @paid_by_wire_check = @p2c.where(status: ["PAID_WIRE"]).count
          @failed_check = @p2c.where(status: ["FAILED"]).count
          @total_withdraw = @p2c.pluck(:actual_amount).try(:sum, &:to_f)
          @dollar_fee = @p2c.pluck(:check_fee).try(:sum, &:to_f)
          @percent_fee = @p2c.pluck(:fee_perc).try(:sum, &:to_f)
          @total_fee = @percent_fee.to_f + @dollar_fee.to_f
          @total_checks=@p2c.count
        end
        @p2c = @p2c.per_page_kaminari(params[:page]).per(filter)
      elsif params[:tab] == 'menu_8'
        @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
        @risk_evaluation_status= [{value: "reject_manual_review", view:"Rejected by manual review"}, {value: "pending_manual_review", view: "Pending Manual Review"}, {value: "expired_manual_review", view: "Manual Review Expired"}, {value: "accept_manual_review", view: "Accepted by Manual Review"}, {value: "accept_default", view: "Accepted by default"}]
        conditions = []
        if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
          conditions = shipment_search_query(params[:query],params[:offset])
        end
        if @user.merchant?
          if  params["query"].present? && params["query"]["risk_eval"].present?
            @txn = Transaction.joins(:tracker).joins(:minfraud_result).where(receiver_wallet_id: @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(filter)
          else
            @txn = Transaction.joins(:tracker).includes(:minfraud_result).where(receiver_wallet_id: @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(filter)
          end
          @totalTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).group("trackers.status").count
          @totalTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).group("trackers.status").sum(:total_amount)
          @totalNewTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where(trackers: {tracker_id: nil}).count
          @totalNewTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where(trackers: {tracker_id: nil}).sum(:total_amount)
          # @totalOverDueTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where("trackers.est_delivery_date < ?", Time.now).count
          # @totalOverDueTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet.id).where("trackers.est_delivery_date < ?", Time.now).sum(:total_amount)
          @totalOverDueTxnCount = @totalTxnCount["overdue"]
          @totalOverDueTxnAmount = @totalTxnAmount["overdue"]
        else
          if  params["query"].present? && params["query"]["risk_eval"].present?
            @txn = Transaction.joins(:tracker).joins(:minfraud_result).where(sender_wallet_id: @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(filter)
          else
            @txn = Transaction.joins(:tracker).includes(:minfraud_result).where(sender_wallet_id: @wallet.id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(filter)
          end

        end

        # @txn = @txn.distinct

      end

    else
      # if @user.merchant?
      #   # @transactions_count = Transaction.where("sender_wallet_id = :wallet OR receiver_wallet_id = :wallet", wallet: @wallet.id).count
      # else
      #   # @transactions_count = BlockTransaction.where("sender_wallet_id = :wallet OR receiver_wallet_id = :wallet", wallet: @wallet.id).count
      # end
      # @transactions_count = SequenceLib.all_count_transactions(@wallet.id).count
    end
    @iamuser="user"
    respond_to do |format|
      if @user.iso? || @user.agent? || @user.affiliate? || @user.partner? || @wallet.qc_support? || @user.user? ||  @wallet.check_escrow? || @wallet.escrow? || @wallet.charge_back? || @user.support? || @user.role.blank? || @user.merchant?
        format.html{render 'admins/users_transactions'}
      else
        format.html{render 'admins/transactions'}
      end
    end
  end

  def user_wallet_search_conditions(params)
    if params[:q].present?
      if params[:tx_filter].present?  && params[:q]["gateway"].class== String
        params[:q]["gateway"] = JSON.parse(params[:q]["gateway"]) if params[:q]["gateway"].present?
      end
      if params[:tx_filter].present? && params[:q]["type"].class== String
        params[:q]["type"] = JSON.parse(params[:q]["type"]) if params[:q]["type"].present?
      end
      conditions = []
      parameters = []
      @wallet_user = Wallet.find_by(id:params[:q][:wallet_id]) if params[:q][:wallet_id].present? && @wallet.blank?
      date = parse_daterange(params[:q][:timestamp]) if params[:q][:timestamp].present?
      time = parse_time(params)
      if date.present?
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),time.second.last(2),time.second.last(2), params[:offset]).utc
        }
      end
      if date.present?
        if date[:first_date].present?
          # if !@wallet_user.try(:primary?)
          #   conditions << "block_transactions.created_at >= ?"
          # else
          conditions << "transactions.created_at >= ?"
          # end
          parameters << date[:first_date]
        end
        if  date[:second_date].present?
          # if !@wallet_user.try(:primary?)
          #   conditions << "block_transactions.created_at <= ?"
          # else
          conditions << "transactions.created_at <= ?"
          # end
          parameters << date[:second_date]
        end
      end
      if params[:q][:id].present?
        params[:q][:id] = params[:q][:id].try(:strip)
        if !@wallet_user.try(:primary?)
          conditions << "transactions.seq_transaction_id  ILIKE ?"
        else
          conditions << "transactions.seq_transaction_id  ILIKE ?"
        end
        parameters << "#{params[:q][:id]}%"
      end
      if params[:q][:receiver_name].present?
        params[:q][:receiver_name] = params[:q][:receiver_name].strip
        conditions << "transactions.receiver_name ILIKE ? "
        parameters << "%#{params[:q][:receiver_name]}%"
      end
      if params[:q][:sender_name].present?
        params[:q][:sender_name] = params[:q][:sender_name].strip
        conditions << "transactions.sender_name ILIKE ? "
        parameters << "%#{params[:q][:sender_name]}%"
      end
      if params[:q]["amount"].present?
        params[:q]["amount"] = params[:q]["amount"].try(:strip)
        new_var = params[:q]["amount"]
        var = new_var.to_s.split('.').last
        storing_param=new_var.to_f
        if var=="00"
          storing_param=new_var
          storing_param=number_with_precision(storing_param, precision: 1).to_f
        end
        conditions << "(amount = ? OR total_amount = ? OR tags->'main_transaction_info'->>'amount' = ?)"
        parameters << params[:q]["amount"]
        parameters << params[:q]["amount"]
        parameters << params[:q]["amount"]
      end
      if params[:q][:gateway].present?
        conditions << "payment_gateways.key IN (?) "
        parameters << params[:q][:gateway]
      end
      if params[:q]["last4"].present?
        params[:q]["last4"] = params[:q]["last4"].try(:strip)
        if !@wallet.try(:primary?)
          conditions << "block_transactions.last4 LIKE ?"
        else
          conditions << "transactions.last4 LIKE ?"
        end
        parameters << params[:q]["last4"]
      end
      # first6 = params[:first6].present? ? params[:first6].split.join : nil
      if params[:q]["first6"].present?
        params[:q]["first6"] = params[:q]["first6"].try(:strip)
        conditions << "transactions.first6 LIKE ?"
        parameters << params[:q]["first6"]
      end
      if params[:q][:type].present?
        conditions << "main_type IN (?) "
        parameters << params[:q][:type]
      end
      if params[:q][:card_number].present?
        params[:q][:card_number] = params[:q][:card_number].strip
        conditions << "users.name ILIKE (?) "
        parameters << params[:q][:card_number]
      end

      unless conditions.empty?
        conditions = [conditions.join(" AND "), *parameters]
      end
    end

    return conditions
  end

  def ticket_details
    @make_to = params[:path_to]
    @ticket = Ticket.find(params[:ticket_id])
    @wallet = Wallet.friendly.find (params[:user_id])
    @user = @wallet.users.first
    respond_to :js
  end

  def wallet_details
    @wallet = Wallet.friendly.find(params[:wallet_id])
    @balance = show_balance(@wallet.id) - HoldInRear.calculate_pending(@wallet.id)
    respond_to :js
  end

  def submit_user_main_information
    message = ""
    user_params = main_information_user_params
    if user_params.present?
      if user_params[:location_id].present?
        if Location.exists?(user_params[:location_id])
          @location = Location.find(user_params[:location_id])
          if user_params[:first_name].present?
            @location.first_name = user_params[:first_name]
          end
          if user_params[:last_name].present?
            @location.last_name = user_params[:last_name]
          end
          if params[:phone].present?
            @location.phone_number = user_params[:phone]
          end
          if user_params[:email].present?
            @location.email = user_params[:email]
          end
          if user_params[:is_block].present?
            @location.is_block = user_params[:is_block] == 'true' ? true : false
          end
          if @location.save
            message = "Successfully updated Location."
          else
            message = "Failed. #{@location.errors.full_messages}"
          end
        else
          @user1 = User.find(user_params[:location_id])
          if user_params[:first_name].present?
            if @user1.first_name.nil?
              @user1.name = user_params[:first_name]
            else
              @user1.first_name = user_params[:first_name]
            end
          end
          if user_params[:last_name].present?
            @user1.last_name = user_params[:last_name]
          end
          if user_params[:first_name].present? && user_params[:last_name].present?
            @user1.name = "#{user_params[:first_name]} #{user_params[:last_name]}"
          end
          if params[:phone].present?
            @user1.phone_number = user_params[:phone]
          end
          if user_params[:email].present?
            @user1.email = user_params[:email]
          end
          if user_params[:is_block].present?
            @user1.is_block = user_params[:is_block] == 'true' ? true : false
          end
          if @user1.save
            @user1.wallets.first.update(name: @user1.name)
            message = "Successfully updated User."
          else
            message = "Failed. #{@user1.errors.full_messages}"
          end
        end
      else
        message = "Failed. Try Again!"
      end
    else
      message = "Failed. Try Again!"
    end
    if params[:path_to].present?
      if params[:path_to] == "admin_user"
        flash[:notice] = message
        redirect_to user_transactions_path
      elsif params[:path_to] == "location_wallet"
        flash[:notice] = message
        redirect_to wallet_transactions_locations_path(wallet_id:params[:user_id])
      end
    end
  end

  def all_users
    @users = User.user.where.not(email: current_user.email, archived: true).order('id ASC')
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context) }
    end
  end

  def edit
    @user = User.friendly.find(params[:id])
    @role = @user.role
    respond_to :js
  end

  def update_role
    @user = User.friendly.find(params[:user_id])
    @user.update(role: params[:role])
    redirect_to users_path
  end

  def show
    @wallets = @user.wallets
  end

  def update
    if @user.update_attributes(sign_up_params.merge(block_withdrawal:params[:user][:block_withdrawal], block_ach: params[:user][:block_ach],
                                                    block_giftcard: params[:user][:block_giftcard], push_to_card: params[:user][:push_to_card]))
      flash[:notice] = "Successfully updated User."
      redirect_back(fallback_location: root_path)
    else
      render :action => 'edit'
    end
  end

  def destroy
    getting_users = User.where(email: @user.blocked_email).or(User.where(phone_number: @user.phone_number))
    getting_users_with_email_or_phone = getting_users.count
    if @user.archived == false
      if @user.merchant?
        ids=[]
        ids=@user.wallets.primary.pluck(:id) if @user.wallets.present?
        transactions=Transaction.where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",ids,ids).where.not(status:'pending').select(:id, :sender_wallet_id, :receiver_wallet_id, :status)
        if transactions.present? && transactions.size > 0
          flash[:notice] = "This merchant has one or more transactions and can not be archived."
        else
          @user.archived = true
          @user.save(:validate => false)
          flash[:success] = params[:greenbox_users].present? ? "Inactive Successfully." : "Archived Successfully."
        end
      elsif @user.admin_user?
          # @user.affiliate_programs.each do |affiliate_program|
          #   affiliate_program.archived = true
          #   affiliate_program.save(:validate => false)
          # end
          @user.archived = true
          @user.save(:validate => false)
          flash[:success] = "Archived Successfully."
      else
        if @user.locations.blank? # if user is iso, agent or aff
          @user.archived = true
          @user.is_block = true
          @user.save(:validate => false)
          flash[:success] = params[:greenbox_users].present? ? "Inactive Successfully." : "Archived Successfully."
        else
          user_name = @user.role == "iso" ? @user.role.upcase : @user.role.titleize
          count = @user.locations.count > 1 ? "s" : ""
          flash[:archive] = "#{user_name} assigned to #{@user.locations.count} Location#{count}. Please remove it before archiving!"
        end
      end
    else
      if getting_users_with_email_or_phone > 1
        unless getting_users.pluck(:archived).include?(false)
          @user.archived = false
          unless @user.merchant?
            @user.is_block = false
          end
          @user.save(:validate => false)
          flash[:success] = params[:greenbox_users].present? ? "Active Successfully." : "Unarchived Successfully."
        else
          flash[:notice] = "Phone # or Email already exist with new user, please change the phone# and Email before activating the user"
        end
      else
        @user.update(archived: false) if @user.merchant?
        @user.update(archived: false, is_block: false) unless @user.merchant?
        flash[:success] = params[:greenbox_users].present? ? "Active Successfully." : "Unarchived Successfully."
      end
    end
    note=@user.archived == true ? "Archived": 'Un-Archived'
    save_activity(@user,Time.now,current_user.id,note,'') if flash[:success].present?
    respond_to do |format|
      format.html {
        redirect_back(fallback_location: root_path)
      }
      format.json { head :no_content }
    end
  end

  def edit_archive
    @olduser = User.find_by(id: params[:id])
    @user = User.new
    respond_to :js
  end

  def post_edit_archive
    if params[:user][:email].present? && params[:user][:phone_number].present? && params[:user][:form_id].present?
      u = User.find_by(id: params[:user][:form_id])
      checking_email = User.where(email: params[:user][:email])
      checking_phone = User.where(email: params[:user][:phone_number])
      if u.email == params[:user][:email] || u.phone_number == params[:user][:phone_number]
        flash[:notice] = "Can't update with existing email or phone number!"
      elsif checking_email.count == 0 && checking_phone.count == 0
        u.update!(email: params[:user][:email], phone_number: params[:user][:phone_number])
        flash[:notice] = "Update successfully!"
      else
        flash[:notice] = "Try with new email or phone number!"
      end
    else
      flash[:notice] = "Missing parameters!"
    end
    redirect_back(fallback_location: root_path)
  end

  def archived_users
    if params[:search_input].present?
      search_users(params)
    elsif params[:query].present?

      if params[:query].reject{|k,v| v.blank? }.blank?
        @users = User.search_users_with_role(params[:user]).archived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(10)
      else
        @users = search(params).search_users_with_role(params[:user]).archived_users.order('users.created_at DESC').per_page_kaminari(params[:page]).per(10)
      end
    else
      if params[:user] == "affiliate_programs" and params[:admin_user]
        @users = User.search_users_with_role(params[:user]).where(admin_user_id: params[:admin_user]).archived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(10)
      else
        @users = User.search_users_with_role(params[:user]).archived_users.order('created_at DESC').per_page_kaminari(params[:page]).per(10)
      end
    end
    @filters = [10,25,50,100]
  end


  private

  def sign_up_params
    params.require(:user).permit(:name ,:postal_address , :phone_number,:is_block,:password,:password_confirmation, :role_ids => 3)
  end

  def show_user
    # @id = EncryptedId.decrypt('aqwertyuiop[]lkjhgfdsazxcvbnmkjh', params[:id])
    @user = User.friendly.find(params[:id])

  end

  def new_user_params
    params.require(:user).permit(:email ,:password, :name, :first_name ,:last_name, :role, :is_block, :phone_number)
  end

  def main_information_user_params
    params.permit(:location_id, :email, :is_block, :first_name, :last_name, :phone)
    # params.require(:user).permit(:id, :email, :is_block, :name, :last_name, :phone_number)
  end
end
