class Admins::BlockTransactionsController < AdminsController
  layout "admin"
  include Admins::BlockTransactionsHelper

  def index
    # @transactions = BlockTransaction.where.not(main_type: ["fee_transfer", "Fee_Transfer", "fee", "Reserve_Money_Deposit"])
    # transactions = @transactions.select{|v| v.seq_parent_id == nil || v.parent_id == nil}.count
    # atm_transactions = @transactions.select{|v| v.seq_parent_id != nil && v.parent_id != nil && v.main_type == "ATM Cash Deposit"}.count
    @transactions = BlockTransaction.order(timestamp: :desc).per_page_kaminari(params[:page]).per(10)
    @t_count = @transactions.total_count
    # @transactions = Kaminari.paginate_array(@transactions).page(params[:page]).per(10)
  end

  def sync_transactions
    begin
      if params[:date].present?
        date = parse_date_and_time(params[:date])
        puts "getting sequence"
        job_id = SyncTransactionsWorker.perform_async(date)
        puts "job id", job_id
      else
        last_transaction = BlockTransaction.last
        if last_transaction.present?
          date = {first_date: last_transaction.timestamp.strftime("%Y-%m-%dT%H:%M:%S.00Z")}
        end
        job_id = SyncTransactionsWorker.perform_async(date)
      end
      render json: {
          jid: job_id
      }
    rescue => e
      puts "sync transactions Error from controller ", e
    end
  end

  def sync_status
    respond_to do |format|
      format.json do
        job_id = params[:job_id]
        puts "sync job id",job_id
        job_status = Sidekiq::Status.get_all(job_id).symbolize_keys
        render json: {
            status: job_status[:new_status],
            percentage: Sidekiq::Status::pct_complete(job_id),
            total_transactions: job_status[:total_transactions] || "0",
        }
      end
    end
  end

  def update_balances
    job_id = BalanceUpdateWorker.perform_async
    respond_to do |format|
      format.json do
        render json: {
            jid: job_id
        }
      end
    end
  end

  def balance_status
    respond_to do |format|
      format.json do
        job_id = params[:job_id]

        job_status = Sidekiq::Status.get_all(job_id).symbolize_keys
        render json: {
            status: job_status[:new_status]
        }
      end
    end
  end

  private

  def parse_date_and_time(date)
    first_date = date[0..9]
    first_date = Date.strptime(first_date, '%m/%d/%Y')
    first_date = first_date.strftime("%Y-%m-%dT%H:%M:%S.00Z")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, '%m/%d/%Y')
    second_date = second_date.strftime("%Y-%m-%dT23:59:59.59Z")
    return {first_date: first_date, second_date: second_date}
  end
end
