class Admins::AppConfigsController < AdminsController
  layout 'admin'
  include Admins::AppConfigsHelper
  include Admins::PaymentGatewaysHelper
  before_action :check_admin
  before_action :set_config
  skip_before_action :authenticate_user!, only: [:update]

  require 'socket'

  def index
    @stripe_config = @configs.find{|c| c.key === AppConfig::Key::PaymentProcessor }
    @cards_config = @configs.find{|c| c.key === 'gift_cards_config' }
    if not @cards_config.present?
      @cards_config = AppConfig.new
      @cards_config.key = 'gift_cards_config'
      @cards_config.save
    end
    if !@stripe_config
      flash[:error] = 'No Configs found!'
      redirect_to '/admins'
    end
  end

  def create
    config = AppConfig.find(params[:id]) if params[:id].present?
    config = AppConfig.where(:key => params[:key]).first if params[:key].present? && config.blank?
    if config.present?
      if params[:app_config].present? && params[:key].present?
        stringvalue = JSON.parse(config.stringValue)
        value =  stringvalue[params[:app_config][:stringValue]]
        stringvalue[params[:app_config][:stringValue]] = !value
        config.update(stringValue: stringvalue.to_json)
        buttonValue = JSON.parse(config.stringValue)
        buttonValue = buttonValue[params[:app_config][:stringValue]]
      elsif params[:app_config].present? && !params[:key].present?
        config.update(api_config_params)
      else
        config.update(:boolValue => !config.boolValue)
      end
    end
    config = AppConfig.create!(app_config_params.merge({:key => params[:key]})) if config.blank?
    # merchant_checks_enabled_emails() if config.boolValue == true && config.key == AppConfig::Key::IssueCheckConfig
    respond_to do |format|
      # format.html
      # format.js
      format.json { render json: config }
    end
    # -----------------------------------------Added slack notification for API settings-------------------------------------------------
    if buttonValue == true
      button_value = 'ON'
    else
      button_value = 'OFF'
    end
    if config.boolValue == true
      button_valuee = 'ON'
    else
      button_valuee = 'OFF'
    end
    if config.key == "sequence_down"
      msg = config.key + " switch has been turned " + button_valuee + " by the " + current_user[:name] + " logged in as Admin from IP " + request.remote_ip
    else
      msg = (params[:app_config].present? && params[:app_config][:stringValue].present?) ? params[:app_config][:stringValue] : config.key + " switch has been turned " + params[:app_config].present? ? button_value : button_valuee  + " by the " + current_user[:name] + " logged in as Admin from IP " + request.remote_ip
    end
    if config.boolValue == false
      merchant_checks_enabled_emails()
    elsif  config.boolValue == true
      system_maintenance_on_email()
    end

    SlackService.notify(msg, "#quickcard-internal")
  end

  def merchant_modal_search
    users = User.merchant.where(primary_gateway_id: params[:gateway_id].to_i)
    @users =users.where("name ILIKE :name",  name: "%#{params["key"]}%")
    respond_to :js
  end

  def atm_config
    @stripe_config = @configs.find{|c| c.key === AppConfig::Key::ATMPaymentProcessor }
    @cards_config = @configs.find{|c| c.key === 'gift_cards_config' }
    if not @cards_config.present?
      @cards_config = AppConfig.new
      @cards_config.key = 'gift_cards_config'
      @cards_config.save
    end
    if !@stripe_config
      flash[:error] = 'No Configs found!'
      redirect_to '/admins'
    end
  end

  def update_atm_congif
    if params[:id].present?
      object = @configs.find{|c| c.id === params[:id].to_i }
      if object.present?
        # if params[:secondary].present? && params[:value] != 'bridge_pay'
        #   object.update(:secondary_option => params[:value])
        # else
        object.update(:stringValue => params[:value])
        # end
        flash[:success] = "Bank Option Updated!"
      else
        flash[:error] = "Config Not Found!"
      end
    else
      flash[:error] = "Invalid Params!"
    end
    render :partial => 'layouts/flash'

  end

  def merchant_configs_logs
    @merchant_logs = ConfigLog.where.not(total_merchants: nil).order(created_at: "DESC").eager_load(:user)
  end

  def merchant_app_configs
    @filters = [10, 25, 50, 100]
    details = []
    @users = User.merchant.where(merchant_id: nil)
    @apis_config = AppConfig.where(key: AppConfig::Key::APIsAuthorization).first
    @apis_config = AppConfig.new(key: AppConfig::Key::APIsAuthorization, stringValue: {virtual_existing: true, virtual_debit_charge: true, virtual_new_signup: true, virtual_terminal_transaction: true, virtual_transaction: true, refund: true}.to_json) unless @apis_config.present?
    @seq_down = AppConfig.sequence_down.first
    if params[:search_input] && params[:q].present?
      if params[:status] == "active" || params[:status].blank?
        @payment_gateway = PaymentGateway.all_active("%#{params[:q].strip}%")
      elsif params[:status] == "inactive"
        @payment_gateway = PaymentGateway.all_inactive("%#{params[:q].strip}%")
      end
  else
    if params[:status] == "active" || params[:status].blank?
      # @payment_gateway = PaymentGateway.main_gateways.where(is_block: false).order('created_at DESC').per_page_kaminari(params[:page]).per(params[:tx_filter] || 10)
      @payment_gateway = PaymentGateway.all_active(nil)
    elsif params[:status] == "inactive"
      @payment_gateway = PaymentGateway.all_inactive(nil)
    end
    end
    @api_names_tags = {virtual_existing: "Virtual Existing (Old)", virtual_debit_charge: "Virtual Debit Charge (Direct w/o Customer)", virtual_new_signup: "Virtual New Signup (Old)", virtual_terminal_transaction: "Virtual Terminal Transaction (Direct w/o Customer)", virtual_transaction: "Virtual Transaction (e-Commerce)", refund: "Refund (Credit Card)"}.to_json
    @payment_gateway.each do |pg|
      g_details={
          created_at: pg.created_at.strftime("%D"),
          name: pg.name,
          key: pg.key,
          total_mid: Location.have_gateway(pg.id).distinct.count,
          status: pg,
          id: pg.id
      }
      details.push(g_details)
    end
    @payment_gateway = details.sort_by{|e| -e[:total_mid]}
    @payment_gateway = Kaminari.paginate_array(@payment_gateway).page(params[:page]).per(params[:tx_filter] || 10)
    gon.apis_config = @apis_config.to_json.html_safe
    respond_to do |format|
      format.js
      format.html
    end
  end

  def load_balancer_configs
    @filters = [10, 25, 50, 100]
    details = []
    if params[:search_input] && params[:q].present?
      if params[:status] == "active" || params[:status].blank?
        @load_balancers = LoadBalancer.active_with_name_like(params[:q])
      elsif params[:status] == "inactive"
        @load_balancers = LoadBalancer.inactive_with_name_like(params[:q])
      end
    else
      if params[:status] == "active" || params[:status].blank?
        @load_balancers = LoadBalancer.where(active: true).order('created_at DESC')
      elsif params[:status] == "inactive"
        @load_balancers = LoadBalancer.where(active: false).order('created_at DESC')
      end
    end
    @load_balancers.each do |load_balancer|
      load_balancer_details={
          created_at: load_balancer.created_at.strftime("%D"),
          name: load_balancer.name,
          total_mid: Location.where(load_balancer_id: load_balancer.id).distinct.count,
          status: load_balancer,
          id: load_balancer.id
      }
      details.push(load_balancer_details)
    end
    @load_balancers = details.sort_by{|e| -e[:total_mid]}
    @load_balancers = Kaminari.paginate_array(@load_balancers).page(params[:page]).per(params[:tx_filter] || 10)
    respond_to do |format|
      format.js
      format.html
    end
  end

  def merchant_modal
    @payment_gateway = PaymentGateway.find_by(:key => params[:gateway])
    if @payment_gateway.present?
      @query = Location.have_gateway(@payment_gateway.id).ransack(params[:q])
      @users_merchant = @query.result(distinct: true)
    else
      users = User.merchant.where(merchant_id: nil)
      if params[:gateway] == 'stripe'
        @users_merchant = users.stripe
      elsif params[:gateway] == 'stripe_pop'
        @users_merchant = users.stripe_pop
      elsif params[:gateway] == 'stripe_pop_2'
        @users_merchant = users.stripe_pop_2
      elsif params[:gateway] == 'converge'
        @users_merchant = users.converge
      elsif params[:gateway] == 'fluid_pay'
        @users_merchant = users.fluid_pay
      end
    end
    respond_to :js
  end

  def show
    @payment_gateway = PaymentGateway.find(params[:id])
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    if @payment_gateway.present?
      if params[:req] == "active"
        params[:q] = (valid_jsons?(params.try(:[], "q"))) ? JSON.parse(params[:q]) : params[:q]
        @all_locations = Location.where(is_block: false).have_gateway(@payment_gateway.id)
       @active_locations = @all_locations.where(:is_block => :false).distinct.count
        @query = @all_locations.ransack(params[:q])
        @locations = @query.result(distinct: true).per_page_kaminari(params[:page]).per(page_size)
        @all_locations_map = @all_locations.map{|u| u.id}
      elsif params[:req] == "inactive"
        params[:q] = (valid_jsons?(params.try(:[], "q"))) ? JSON.parse(params[:q]) : params[:q]
        @all_locations = Location.where(:is_block => true).have_gateway(@payment_gateway.id)
        @active_locations = @all_locations.where(:is_block => :true).distinct.count
        @query = @all_locations.ransack(params[:q])
        @locations = @query.result(distinct: true).per_page_kaminari(params[:page]).per(page_size)
        @all_locations_map = @all_locations.map{|u| u.id}
      else
        params[:q] = (valid_jsons?(params.try(:[], "q"))) ? JSON.parse(params[:q]) : params[:q]
        @all_locations = Location.have_gateway(@payment_gateway.id)
        @active_locations = @all_locations.distinct.count
        @query = @all_locations.ransack(params[:q])
        @locations = @query.result(distinct: true).per_page_kaminari(params[:page]).per(page_size)
        @all_locations_map = @all_locations.map{|u| u.id}
      end
    end
  end
  def show_config_log_details
    @config_log_id = params[:id]
    @log_details = ConfigLog.find(params[:id]).config_log_details
    respond_to :js
  end

  def update_payment_gateway
    if params[:id].present?
      config_log = ConfigLog.find(params[:id])
      payment_gateway = PaymentGateway.find_by(key: config_log.from)
      new_config = ConfigLog.create! to: config_log.from, from: config_log.to, total_merchants: config_log.total_merchants
      config_details = config_log.config_log_details
      config_details.each do |d|
        if d.location_id.present?
          Location.find(d.location_id).update(primary_gateway_id: payment_gateway.try(:id))
          ConfigLogDetail.create! location_id: d.location_id, config_log_id: new_config.id
        elsif d.user_id.present?
          User.find(d.user_id).update(payment_gateway: new_config.to)
          ConfigLogDetail.create! user_id: d.user_id, config_log_id: new_config.id
        end
      end
      flash[:success] = "Payment method has been reverted successfully"
      redirect_to merchant_app_configs_app_configs_path
    else
      locations_ids = params[:locations].first.split(',') if params[:locations].present?
      if locations_ids.present?
        users_hash = []
        locations = Location.where(id: locations_ids)
        if params[:primary_gateway_id].present? || params[:secondary_gateway_id].present? || params[:ternary_gateway_id].present?  
          if params[:primary_gateway_id].present?
            gateway = PaymentGateway.find_by(id: params[:primary_gateway_id].to_i)
            log = ConfigLog.create! to: gateway.key, from: params[:gateway], total_merchants: locations_ids.count
            locations.update_all(primary_gateway_id: params[:primary_gateway_id].to_i)
            locations_ids.each do |user|
              users_hash << {
                  :location_id => user.to_i,
                  :config_log_id => log.id
              }
            end
            ConfigLogDetail.import(users_hash)
          end
          if params[:secondary_gateway_id].present?
            gateway = PaymentGateway.find_by(id: params[:secondary_gateway_id].to_i)
            log = ConfigLog.create! to: gateway.key, from: params[:gateway], total_merchants: locations_ids.count
            locations.update_all(secondary_gateway_id: params[:secondary_gateway_id].to_i)
            locations_ids.each do |user|
              users_hash << {
                  :location_id => user.to_i,
                  :config_log_id => log.id
              }
            end
            ConfigLogDetail.import(users_hash)
          elsif params[:bulk_checked].present? && params[:secondary_gateway_id].blank?
            locations.update_all(secondary_gateway_id: params[:secondary_gateway_id].to_i)
            locations_ids.each do |user|
              users_hash << {
                  :location_id => user.to_i,
                  :config_log_id => ""
              }
            end
            ConfigLogDetail.import(users_hash)
          end
          if params[:ternary_gateway_id].present?
            gateway = PaymentGateway.find_by(id: params[:ternary_gateway_id].to_i)
            log = ConfigLog.create! to: gateway.key, from: params[:gateway], total_merchants: locations_ids.count
            locations.update_all(ternary_gateway_id: params[:ternary_gateway_id].to_i)
            locations_ids.each do |user|
              users_hash << {
                  :location_id => user.to_i,
                  :config_log_id => log.id
              }
            end
            ConfigLogDetail.import(users_hash)
          elsif params[:bulk_checked].present? && params[:ternary_gateway_id].blank?
            locations.update_all(ternary_gateway_id: params[:ternary_gateway_id].to_i)
            locations_ids.each do |user|
              users_hash << {
                  :location_id => user.to_i,
                  :config_log_id => ""
              }
            end
            ConfigLogDetail.import(users_hash)
          end
        else
          if params.has_key?(:primary_gateway_id)
            locations.update_all(primary_gateway_id: params[:primary_gateway_id].to_i)
          end
          if params.has_key?(:secondary_gateway_id)
            locations.update_all(secondary_gateway_id: params[:secondary_gateway_id].to_i)
          end
          if params.has_key?(:ternary_gateway_id)
            locations.update_all(ternary_gateway_id: params[:ternary_gateway_id].to_i)
          end
        end
        flash[:success] = 'Payment Gateway updated successfully!'
        respond_to do |format|
          format.js {render js: "window.location.pathname='#{merchant_app_configs_app_configs_path}'"}
          format.html {redirect_to merchant_app_configs_app_configs_path}
        end
      end
    end

  end

  def fee_update
    if params[:kind].present?
      if params[:kind] == 'user'
        user_fee_params.keys.each do |key|
          if AppConfig.where(key: key).present?
            AppConfig.where(:key => key).first.update(:user_fee => user_fee_params[key].to_f)
            flash[:success] = "Fee Updated!"
          else
            a = AppConfig.new(key: key, user_fee: user_fee_params[key])
            a.save!
            flash[:success] = "Fee Updated!"
          end
        end
      end
    end
    redirect_to '/admins/app_configs/fee/user'
  end

  def update
    if params[:id].present?
      object = @configs.find{|c| c.id === params[:id].to_i }
      if object.present?
        # if params[:secondary].present? && params[:value] != 'bridge_pay'
        #   object.update(:secondary_option => params[:value])
        # else
        ConfigLog.create(to: object.stringValue, from: params["value"])
          object.update(:stringValue => params[:value])
        # end
        if !params[:mobile_config].present?
        flash[:success] = "Bank Option Updated!"
        end
        else
          if !params[:mobile_config].present?
        flash[:error] = "Config Not Found!"
        end
      end
    else
      flash[:error] = "Invalid Params!"
    end
    render :partial => 'layouts/flash'
  end

  def update_graph_setting
    if params[:id].present?
        app=AppConfig.where(id:params[:id]).try(:first)
        app.update(:boolValue => params[:value]=='true' ? false : true)
          flash[:success] = "#{app.key.try(:humanize)} is turned #{!app.boolValue.present? ? 'Off' : 'On'}"
    else
      flash[:error] = "Invalid Params!"
    end
    render :partial => 'layouts/flash'
  end

  def update_accounting_setting
    if params[:id].present?
      app=AppConfig.where(id:params[:id]).try(:first)
      app.update(:boolValue => params[:value]=='true' ? false : true)
      flash[:success] = "#{app.key.try(:humanize)} is turned #{!app.boolValue.present? ? 'Off' : 'On'}"
    else
      flash[:error] = "Invalid Params!"
    end
    render :partial => 'layouts/flash'
  end

  def mid_disable
    app_config = AppConfig.where(key: AppConfig::Key::MidDisable).first
    if app_config.present?
      app_config = app_config.update_columns(boolValue: params[:mid_disable])
    else
      app_config = AppConfig.create!(key: "mid_disable",boolValue: params[:mid_disable])
    end
    respond_to do |format|
      format.json { render json: app_config }
    end
  end

  private

  def set_config
    @configs = AppConfig.all
  end

  def user_fee_params
    params.permit(AppConfig::Fee::UserCheckFee.to_sym, AppConfig::Fee::UserGiftCardFee.to_sym, AppConfig::Fee::UserPaypalCardFee.to_sym, AppConfig::Fee::UserVisaCardFee.to_sym, AppConfig::Fee::UserDepositLimit.to_sym, AppConfig::Fee::DepositCard.to_sym, AppConfig::Fee::UserSpendingLimit.to_sym, AppConfig::Fee::InstantDailyLimit.to_sym, AppConfig::Fee::InstantPerTxLimit.to_sym)
  end

  def api_config_params
    params.require(:app_config).permit(:boolValue, :stringValue)
  end

  def app_config_params
    params.permit(:boolValue, :stringValue)
  end
end
