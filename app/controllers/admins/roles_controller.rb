class Admins::RolesController < AdminsController
  before_action :set_role

  def edit
    if @role.title == "merchant"
      @user = User.find_by_name('QC Support')
      @wallet = @user.wallets.first
      @balance = show_balance(@wallet.id)
    end
    if @role.title == "atm"
      @user = User.find_by_name('QC Support')
      @wallet = @user.wallets.first
      @balance = show_balance(@wallet.id)
    end
  end

  def update
    if @role.update(charge_percentage: params[:role][:charge_percentage])
      redirect_to root_path
    else
      redirect_to edit_role_path(@role)
    end
  end


  private

  def set_role
    @role = Role.find_by_title(params[:id])
  end
end
