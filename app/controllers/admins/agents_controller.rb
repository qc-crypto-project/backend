class Admins::AgentsController < AdminsController

  include Wicked::Wizard
  steps :agent_signup, :agent_buyrate, :system_fee, :agent_documentation
  skip_before_action :check_admin
  before_action :user_data,only: [:show,:update]
  skip_before_action :mtrac_admin, only: [:show]

  def index

    # @agent = User.new
    # @agents = User.agent
    # respond_to do |format|
    #   format.html{ render :index}
    #   format.json { render json: AgentsDatatable.new(view_context)}
    # end
  end

  def new
    if params[:count].present?
      show()
    end
    # @agent = User.new
    # @agent.fees.build
    # render template: 'admins/agents/new'
  end

  #  The Funcationality of Iso Controller

  def show
    case step
    when :agent_signup
      if params[:user_data_id].present? && @user_temp.try(:iso_agent)!="{}"
        @agent = User.new(@user_temp.try(:iso_agent)) if @user_temp.present?
      elsif params[:agent_id].present?
        @agent=User.friendly.find params[:agent_id] if params[:agent_id].present?
      else
        @agent = User.new
      end
    when :agent_buyrate
      if params[:user_data_id].present? && @user_temp.try(:buyrate)!="{}"
        @agent = User.new(@user_temp.try(:buyrate)) if @user_temp.present?
        @fees = @agent.fees
      elsif params[:agent_id].present?
        @agent=User.friendly.find params[:agent_id] if params[:agent_id].present?
        @fees = @agent.fees.order(id: :asc)
      else
        @agent = User.new
        @fee = @agent.fees.build
      end
    when :system_fee
      if params[:user_data_id].present? && @user_temp.try(:fees)!="{}"
        @agent = User.new(@user_temp.fees) if @user_temp.present?
      elsif params[:agent_id].present?
        @agent=User.friendly.find(params[:agent_id]) if params[:agent_id].present?
      else
        @agent=User.new
      end
    when :agent_documentation
      if params[:agent_id].present?
        @agent=User.friendly.find(params[:agent_id]) if params[:agent_id].present?
        @images = Documentation.where(:imageable_id => params[:agent_id])
        @images.each do |doc|
          if doc.is_deleted === true
            doc.update(is_deleted: false)
          end
        end
      else
        @agent=User.new
      end
    end

    if params[:id]=="agent_documentation" || params[:id] != "wicked_finish"
      @step_index = current_step
      if params[:count].to_i == 1
        session.delete(:index)
        session[:index] = @step_index if session[:index].blank?
      end
      if params[:count].to_i != 2
        if session[:index].present? && session[:index] < @step_index
          session.delete(:index)
          session[:index] = @step_index
        end
      end
    end
    gon.step_index = @step_index
    gon.index_session = session[:index]
    render_wizard(nil, {}, {user_data_id: params[:user_data_id] })
  end

  def update
    case params[:id]
    when "agent_signup"
      agent_signup()
    when "agent_buyrate"
      agent_buyrate_save()
    when "system_fee"
      system_fee()
    when "agent_documentation"
      agent_domentation()
    end
  end

  def agent_signup
    params[:user][:phone_number] = params[:phone_code] + params[:user][:phone_number] if params[:phone_code].present?
    params[:user][:profile_attributes][:years_in_business] = params[:user][:years_in_business][:year] +"-"+ params[:user][:years_in_business][:month]
    if params[:user][:agent_id].present?
      params[:user][:id] = params[:user][:agent_id]
      begin
        @agent = User.friendly.find(params[:user][:id])
        if params[:user][:profile_attributes].present?
          @agent.company_name = params[:user][:profile_attributes][:company_name]
        end
        if agent_update_params[:password].present?
          @agent.update(agent_update_params)
        else
          ex=agent_update_params.except(:password_confirmation)
          ex = ex.except(:password)
          @agent.update(ex)
        end
        if @agent.errors.blank?
          @agent.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
        end
        if @agent.profile.present?
          @agent.profile.update(agent_profile_params) if !agent_profile_params.blank?
          # flash[:success] = "Agent has been updated Successfully"
          if @agent.errors.blank?
            @agent.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
            flash[:success] = "Agent has been updated Successfully"
          else
            flash[:success] = "Password Previously used, try again."
            return redirect_back(fallback_location: root_path)
          end
        else
          flash[:error] = "Agent Profile Does Not Exist!"
        end
        @agent = User.friendly.find params[:user][:format] if params[:user][:format].present?
        return redirect_to profile_isos_path(@agent)
      end
    else
      if params[:user_data_id].present?
        new1 = MerchantData.find_by_id params[:user_data_id]
        if new1.present?
          @user_temp = new1.update(iso_agent: agent_signup_params)
        end

        redirect_to wizard_path(@next_step,{:user_data_id => params[:user_data_id] })
      else
        @user_temp = MerchantData.create(iso_agent: agent_signup_params)
        redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
      end
    end
  end

  def agent_buyrate_save
    if params[:user][:agent_id].present?
      @agent=User.friendly.find(params[:user][:agent_id])
      @agent.update(agent_buyrate_params)
      if @agent.present?
        @agent.fees.each do |fee|
          fee.buy_rate!
          fee.high!
        end
      else
        flash[:error] = @agent.errors.full_messages.first
      end
      flash[:success] = "Agent has been updated Successfully"
      @agent = User.friendly.find params[:user][:format] if params[:user][:format].present?
      save_activity(@agent,Time.now,current_user.id,'buy_rate','')
      return redirect_to profile_isos_path(@agent)
    else
      @user_temp.update(buyrate: agent_buyrate_params)
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
  end

  def system_fee
    if params[:user][:agent_id].present?
      @agent=User.friendly.find(params[:user][:agent_id])
      @agent.system_fee.merge!(system_fee_params["system_fee"])
      @agent.save
      flash[:success] = "Agent has been updated Successfully"
      @agent = User.friendly.find params[:user][:format] if params[:user][:format].present?
      save_activity(@agent,Time.now,current_user.id,'system_fee','')
      return redirect_to profile_isos_path(@agent)
    else
      @user_temp.update(fees: system_fee_params)
      redirect_to wizard_path(@next_step,{:user_data_id => @user_temp.try(:id) })
    end
  end

  def edit
    @agent = User.friendly.find params[:id]
    begin
      @agent['system_fee']['bank_account'] = AESCrypt.decrypt("#{@agent.id}-#{ENV['ACCOUNT_ENCRYPTOR']}",@agent['system_fee']['bank_account'])
    rescue
      @agent['system_fee']['bank_account']
    end
  end

  def agent_domentation
    if @user_temp.present?
      create_agent()
      redirect_to wizard_path(@next_step)
    elsif params[:user].present? ? params[:user][:agent_id].present? ? true : false : false
      @agent=User.friendly.find(params[:user][:agent_id])
      save_user_documentation(@agent, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
      @documents = Documentation.where(imageable_id: params[:user][:agent_id])
      @documents.each do |doc|
        if doc.is_deleted === true
          doc.document.destroy
          doc.destroy
        end
      end
      respond_to do |format|
        format.html{
          flash[:success] = 'Agent has been added Successfully!'
        }
      end
      @agent = User.friendly.find params[:user][:format] if params[:user][:format].present?
      return redirect_to profile_isos_path(@agent)
    end
  end

  def create_agent
    begin
      if !params[:user].present?
        params[:user] = {}
      end
      params[:user].merge!(@user_temp.iso_agent).merge!(@user_temp.buyrate).merge!(@user_temp.fees)
      @agent = User.new(new_agent_params)
      password = random_password
      @agent.password = password
      @agent.is_block = false
      @agent.is_password_set = false
      @agent.regenerate_token
      if @agent.agent!
        @agent.fees.each do |fee|
          fee.buy_rate!
          fee.high!
        end
        TransactionBatch.create(agent_wallet_id: @agent.wallets.first.id, batch_date: Time.now.to_datetime.in_time_zone("Pacific Time (US & Canada)").to_date, batch_type: "sale")
        if @agent.try(:[],'merchant_id').present?
          @merchant=User.friendly.find(@agent.try(:[],'merchant_id'))
          @merchant.agent=@agent
        end
        @agent.update company_name: @agent.try(:profile).try(:company_name) if @agent.profile.present?
        save_user_documentation(@agent, params[:user][:documentation]) if params[:user].present? ? params[:user][:documentation].present? ? true : false :false
        message="Welcome to Quick Card"
        UserMailer.welcome_user(@agent ,message, 'Agent', password).deliver_later
        respond_to do |format|
          format.html{
            flash[:success] = 'Agent has been added Successfully!'
          }
        end
      else
        flash[:error] = @iso.errors.full_messages.first
      end
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:error] = invalid
      redirect_to users_path(user: "agents")
    end
  end

  def setting
    @user = User.find(id = params[:user_id])
    respond_to do |format|
      format.js {
        render 'admins/shared/profiles/setting.js.erb'
      }
    end
  end

  private

  def user_data
    if params[:user_data_id].present?
      @user_temp = MerchantData.find_by(id: params[:user_data_id])
    end
  end

  def current_step
    steps.index(step) + 1
  end

  def finish_wizard_path
    session.delete(:index) if session[:index].present?
    return users_path(user: 'agents')
  end

  def agent_signup_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,
                                 profile_attributes: [
                                     :company_name,
                                     :years_in_business,
                                     :tax_id,
                                     :street_address,
                                     :ein,
                                     :country,
                                     :city,
                                     :state,
                                     :zip_code
                                 ])
  end

  def new_agent_params
    # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone_number,:merchant_id,
                                 profile_attributes: [:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code],
                                 system_fee: [
                                     :redeem_fee,
                                     :send_check,
                                     :giftcard_fee,
                                     :add_money_dollar,
                                     :add_money_percent,
                                     :b2b_dollar,
                                     # :invoice_card_fee_perc,
                                     # :invoice_card_fee,
                                     :b2b_percent,
                                     :ach_percent,
                                     :ach_dollar,
                                     :push_to_card_percent,
                                     :push_to_card_dollar,
                                     :bank_account,
                                     :bank_routing,
                                     :bank_account_type,
                                     :bank_account_name,
                                     :ach_limit,
                                     :ach_limit_type,
                                     :push_to_card_limit,
                                     :push_to_card_limit_type,
                                     :check_limit,
                                     :check_limit_type,
                                     :check_withdraw_limit
                                 ],
                                 fees_attributes: [:risk,:customer_fee,:b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
                                                   :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,
                                                   :charge_back_fee,:refund_fee, :service, :misc, :statement, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar, :qr_refund_percentage,:failed_push_to_card_percent_fee, :failed_ach_percent_fee, :failed_check_percent_fee])
  end

  def edit_agent_params
    params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,
                                 system_fee: [
                                     :redeem_fee,
                                     :send_check,
                                     :giftcard_fee,
                                     :add_money_dollar,
                                     :add_money_percent,
                                     :b2b_dollar,
                                     :b2b_percent,
                                     # :invoice_card_fee_perc,
                                     # :invoice_card_fee,
                                     :ach_percent,
                                     :ach_dollar,
                                     :push_to_card_percent,
                                     :push_to_card_dollar,
                                     :bank_account,
                                     :bank_routing,
                                     :bank_account_type,
                                     :bank_account_name,
                                     :ach_limit,
                                     :ach_limit_type,
                                     :push_to_card_limit,
                                     :push_to_card_limit_type,
                                     :check_limit,
                                     :check_limit_type,
                                     :check_withdraw_limit,
                                     :failed_push_to_card_fee,
                                     :failed_ach_fee,
                                     :failed_check_fee
                                 ],
                                 fees_attributes: [:id,:risk,:customer_fee,:b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
                                                   :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,
                                                   :charge_back_fee,:refund_fee, :service, :misc, :statement, :dc_deposit_fee_dollar, :dc_deposit_fee, :ach_fee, :ach_fee_dollar, :qr_refund_percentage,:failed_push_to_card_percent_fee, :failed_ach_percent_fee, :failed_check_percent_fee])
  end

  def agent_update_params
    params.require(:user).except(:agent_id, :format, :profile_attributes).permit(:id,:email, :password, :password_confirmation, :name, :phone_number, :company_name)
  end

  def agent_profile_params
    params[:user].require(:profile_attributes).permit(:company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code)
  end

  def agent_buyrate_params
    params.require(:user).permit(
        fees_attributes: [:id,:risk,:customer_fee,:b2b_fee, :invoice_card_fee_perc, :invoice_card_fee, :invoice_debit_card_fee_perc, :invoice_debit_card_fee, :invoice_rtp_fee_perc, :invoice_rtp_fee, :rtp_fee_perc, :rtp_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,
                          :add_money_check, :transaction_fee_app,:is_baked_iso,:ach_fee,:ach_fee_dollar,:dc_deposit_fee,:dc_deposit_fee_dollar,
                          :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar,
                          :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,
                          :refund_fee, :service, :misc, :statement, :qr_refund_percentage,
                          :failed_push_to_card_percent_fee, :failed_ach_percent_fee, :failed_check_percent_fee
        ])
  end

  def system_fee_params
    params.require(:user).permit(
        system_fee: [
            :redeem_fee,
            :send_check,
            :giftcard_fee,
            :add_money_dollar,
            :add_money_percent,
            :b2b_dollar,
            # :invoice_card_fee_perc,
            # :invoice_card_fee,
            :b2b_percent,
            :ach_percent,
            :ach_dollar,
            :push_to_card_percent,
            :push_to_card_dollar,
            :ach_limit,
            :ach_limit_type,
            :push_to_card_limit,
            :push_to_card_limit_type,
            :check_limit,
            :check_limit_type,
            :check_withdraw_limit,
            :failed_push_to_card_fee,
            :failed_ach_fee,
            :failed_check_fee
        ])
  end

  # def agent_documentation_params
  #   params[:user].require(:documentation).permit(user_image:[], w9_form_image:[], schedule_a_image:[])
  # end



  #    End of Iso Controller


  #
  # def show
  #   @agent = User.friendly.find(params[:id])
  #   @wallets = @agent.wallets
  # end

  # def create
  #   begin
  #   @agent = User.new(new_agent_params)
  #   @agent.name = "#{@agent.first_name} #{@agent.last_name}"
  #   @agent.first_name = @agent.first_name
  #   @agent.last_name = @agent.last_name
  #   password = random_password
  #   @agent.password = password
  #   @agent.is_block = false
  #   @agent.regenerate_token
  #   if @agent.agent!
  #     @agent.fees.each do |fee|
  #       fee.buy_rate!
  #       fee.high!
  #     end
  #     if params[:user][:merchant_id].present?
  #        @merchant = User.find params[:user][:merchant_id]
  #        @merchant.agent = @agent
  #     end
  #     message = "Welcome to Quick Card"
  #     UserMailer.welcome_user(@agent, message, 'Agent', password).deliver_later
  #     respond_to do |format|
  #       format.html{
  #         flash[:success] = 'Agent has been added Successfully!'
  #       }
  #     end
  #   else
  #     flash[:error] = @agent.errors.full_messages.first
  #   end
  #   redirect_to  users_path(user: "agents")
  # rescue ActiveRecord::RecordInvalid => invalid
  #   flash[:error] = invalid
  #   redirect_to  new_agent_path
  # end
  # end

  # def edit
  #   @agent = User.friendly.find params[:id]
  # end

  # def update
  #   agent = User.friendly.find params[:id]
  #     if edit_agent_params[:password].present?
  #       agent.update(edit_agent_params)
  #       save_image if params[:image]['add_image'].present?
  #     else
  #       ex=edit_agent_params.except(:password_confirmation)
  #       ex = ex.except(:password)
  #       agent.update(ex)
  #       save_image if params[:image]['add_image'].present?
  #     end
  #   agent.name = "#{agent.first_name} #{agent.last_name}"
  #   agent.fees.each do |fee|
  #     fee.buy_rate!
  #     fee.high!
  #   end
  #   agent.save
  #   if agent.errors.blank?
  #     agent.update(last_password_update: Time.zone.now) unless params[:user][:password].blank?
  #     flash[:success] = "agent has been updated Successfully"
  #     redirect_to controller: 'users', action: 'index', user: 'agents'
  #   else
  #     flash[:error] =  agent.errors.full_messages.first
  #     return redirect_back(fallback_location: root_path)
  #   end
  # end
  #
  # def destroy
  # end
  #
  # private
  #
  # def new_agent_params
  #   # params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
  #   params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,
  #                                system_fee: [
  #                                    :redeem_fee,
  #                                    :send_check,
  #                                    :giftcard_fee,
  #                                    :add_money_dollar,
  #                                    :add_money_percent,
  #                                    :b2b_dollar,
  #                                    :b2b_percent
  #                                ],
  #                                fees_attributes: [:risk,:customer_fee,:b2b_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
  #                                                  :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement])
  # end
  #
  # def edit_agent_params
  #   params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,
  #                                system_fee: [
  #                                    :redeem_fee,
  #                                    :send_check,
  #                                    :giftcard_fee,
  #                                    :add_money_dollar,
  #                                    :add_money_percent,
  #                                    :b2b_dollar,
  #                                    :b2b_percent
  #                                ],
  #                                fees_attributes: [:id,:risk,:customer_fee,:b2b_fee, :redeem_fee, :redeem_check,:giftcard_fee,:customer_fee_dollar,:send_check,:add_money_check,:commission, :transaction_fee_app,
  #                                                  :transaction_fee_app_dollar, :transaction_fee_dcard, :transaction_fee_dcard_dollar, :transaction_fee_ccard, :transaction_fee_ccard_dollar,:retrievel_fee,:charge_back_fee,:refund_fee, :service, :misc, :statement])
  # end
end
