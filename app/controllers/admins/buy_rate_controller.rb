class Admins::BuyRateController < AdminsController
  def index
    @programs = Fee.buy_program
  end

  def new
    @fee = Fee.new
    @path = admins_buy_rate_index_path
    respond_to :js
  end


  def edit
    @fee = Fee.find params[:id]
    @path = admins_buy_rate_path
    respond_to :js
  end

  def create
    fee = Fee.new(new_fee_params)
    if fee.save
      fee.high!
      fee.buy_program!
      flash[:success] = "Program Created Successfully"
    else
      flash[:error] = "Cannot Create Program"
    end
    redirect_to admins_buy_rate_index_path
  end

  def update
    fee = Fee.find params[:id]
    if fee.update(edit_fee_params)
      flash[:success] = "Program Edited Successfully"
    else
      flash[:error] = "Cannot Edit Program"
    end
    redirect_to admins_buy_rate_index_path
  end

  def getAgents
    params[:query] = params[:query].strip
    agents = User.agent.where("first_name LIKE :query", query: "#{params[:query]}%") if params[:query].present?
    if agents.present?
      agentsList=[]
      agents.each do |agent|
        agentsList << {
            label: agent.name,
            category: "Agents",
            id: agent.id
        }
      end
      render status: 200 , json:{:agents => agentsList}
    else
      render status: 200 , json:{:agents => ""}
    end
  end

  def getAffiliates
    params[:query] = params[:query].strip
    affiliates = User.affiliate.where("first_name LIKE :query", query: "#{params[:query]}%") if params[:query].present?
    if affiliates.present?
      affiliatesList=[]
      affiliates.each do |affiliate|
        affiliatesList << {
            label: affiliate.name,
            category: "Affiliates",
            id: affiliate.id
        }
      end
      render status: 200 , json:{:affiliates => affiliatesList}
    else
      render status: 200 , json:{:affiliates => ""}
    end
  end

  def getUsers
    usersList = []
    params[:query] = params[:query].strip
    value = params[:query].split('-').last if params[:query].split('-').present?
    if params[:users].present?
      if params[:users] == "for_users"
        isos = User.iso.unarchived_users.where("lower(company_name) ILIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
        agents = User.agent.unarchived_users.where("lower(name) ILIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
        affiliates = User.affiliate.unarchived_users.where("lower(name) ILIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
      elsif params[:users] == "isos"
        isos = User.iso.unarchived_users.where("lower(company_name) LIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
      elsif params[:users] == "agents"
        agents = User.agent.unarchived_users.where("lower(company_name) ILIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
      elsif params[:users] == "affiliates"
        affiliates = User.affiliate.unarchived_users.where("lower(company_name) ILIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
      elsif params[:users] == "ez_merchant"
        ez_merchants = User.merchant.active_users.where(merchant_id: nil).complete_profile_users.where("lower(name) ILIKE :query OR CAST(id AS VARCHAR) = :value", query: "%#{params[:query].downcase}%", value: value) if params[:query].present?
      end
    else
      companies = User.partner.unarchived_users.where("lower(name) LIKE :query OR CAST(id AS VARCHAR) = :value ", query: "#{params[:query].downcase}%", value: value) if params[:query].present?
      isos = User.iso.unarchived_users.where("lower(company_name) LIKE :query OR CAST(id AS VARCHAR) = :value", query: "#{params[:query].downcase}%", value: value) if params[:query].present?
      agents = User.agent.unarchived_users.where("lower(company_name) LIKE :query OR CAST(id AS VARCHAR) = :value", query: "#{params[:query].downcase}%", value: value) if params[:query].present?
      merchants = User.merchant.unarchived_users.complete_profile_users.where("lower(name) LIKE :query OR CAST(id AS VARCHAR) = :value", query: "#{params[:query].downcase}%", value: value) if params[:query].present?
      affiliates = User.affiliate.unarchived_users.where("lower(company_name) LIKE :query OR CAST(id AS VARCHAR) = :value", query: "#{params[:query].downcase}%", value: value) if params[:query].present?
    end
    if companies.present?
      companies.each do |company|
        ref_no="C-#{company.id}"
        usersList << {
            label: "(" +ref_no +")" + " - " + company.try(:name).try(:capitalize),
            category: "Companies",
            id: company.id
        }
      end
    end
    if isos.present?
      isos.each do |iso|
        ref_no= "ISO-#{iso.id}"
        usersList << {
            label: iso.try(:company_name).present? ? "(" +ref_no +")" + " - " + iso.company_name.capitalize : "(" +ref_no +")" + " - " + iso.try(:name).try(:capitalize),
            category: "Isos",
            id: iso.id
        }
      end
    end
    if agents.present?
      agents.each do |agent|
        ref_no= "A-#{agent.id}"
        if agent.company_name == "n/a"
          agent.company_name = ""
        end
        usersList << {
            label: agent.try(:company_name).present? ? "(" +ref_no +")" + " - " + agent.company_name.capitalize : "(" +ref_no +")" + " - " + agent.try(:name).try(:capitalize),
            category: "Agents",
            id: agent.id
        }
      end
    end
    if affiliates.present?
      affiliates.each do |affiliate|
        ref_no= "AF-#{affiliate.id}"
        if affiliate.company_name == "n/a"
          affiliate.company_name = ""
        end
        usersList << {
            label: affiliate.try(:company_name).present? ? "(" +ref_no +")" + " - " + affiliate.company_name.capitalize : "(" +ref_no +")" + " - " + affiliate.try(:name).try(:capitalize),
            category: "Affiliates",
            id: affiliate.id
        }
      end
    end
    if merchants.present?
      merchants.each do |merchant|
        ref_no= "M-#{merchant.id}"
        usersList << {
            label: "(" +ref_no +")" + " - " + merchant.try(:name).try(:capitalize),
            category: "Merchants",
            id: merchant.id
        }
      end
    end
    if ez_merchants.present?
      if params[:location].present?
        location = Location.find_by(id: params[:location])
        if location.present?
          ez_merchants = ez_merchants.reject{|v| v.id == location.merchant.id}
        end
      end
        ez_merchants.try(:each) do |merchant|
          ref_no= "M-#{merchant.id}"
          if (merchant.id != params[:current_merchant].to_i)
            usersList << {
              label: "(" +ref_no +")" + " - " + merchant.try(:name).try(:capitalize),
              category: "Merchants",
              id: merchant.id
          }
          else
            if usersList.blank?
              render status: 404, json:{error: ''}
              end
          end
        end
    end
    if companies.present? || isos.present? || agents.present? || merchants.present? || affiliates.present? || ez_merchants.present?
      render status: 200 , json:{:users => usersList}
    else
      render status: 200 , json:{:users => ""}
    end
  end

  def getMerchantLocations
    merchant_id= params[:merchant_id].present? ? params[:merchant_id] : 0
    merchant= User.find_by_id(merchant_id)
    merchant_locations=[]
    if merchant.present?
      merchant_wallets=merchant.wallets
      merchant_wallets = merchant_wallets.primary if merchant_wallets.present?
      if merchant_wallets.present?
        merchant_wallets.each do |merchant_wallet|
          location_wallet=merchant_wallet.location
          location_name=""
          location_name="#{location_wallet.try(:first_name)} #{location_wallet.try(:last_name)}" if location_wallet.try(:business_name).nil?
          location_name=location_wallet.business_name if location_wallet.try(:business_name).present?
          if location_wallet.present?
            merchant_locations << {
                id: location_wallet.id,
                name: location_name
            }
          end
        end
        return render status: 200 , json:{:locations => merchant_locations}
      end
    end
    return render status: 200 , json:{:locations => ""}
  end

  def verify_admin
    wallet = Wallet.find(params[:wallet_id])
    if params[:phone_number].present?
      verification_phone_numbers = ENV["VERIFICATION_PHONE_NUMBERS"].present? ? ENV["VERIFICATION_PHONE_NUMBERS"].split(",") : [ENV['PHONE_NUMBER_FOR_PAY_INVOICE2'],ENV['PHONE_NUMBER_FOR_PAY_INVOICE1']]
      if (verification_phone_numbers.include?(params[:phone_number]))
        @random_code = rand(1_000..9_999)
        wallet.update(pincode: @random_code)
        # a = params[:phone_number]
        a = "1"+params[:phone_number]
        if params[:identity].present?
          TextsmsWorker.perform_async(a, "Your pin Code for Retire money is: #{@random_code}")
        else
          TextsmsWorker.perform_async(a, "Your pin Code for Retire money is: #{@random_code}")
        end

      else
        render status: 404, json:{error: 'Wrong Phone Number'}
      end
    end
    if params[:pincode].present?
      if params[:pincode] == wallet.pincode
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Wrong Pin Code'}
      end
    end
  end

  def verify_admin_password
    if current_user.valid_password?(params[:password])
      render status: 200, json:{success: 'Verified'}
    else
      render status: 404, json:{error: 'You are not Authorized for this action.'}
    end
  end

  private

  def new_fee_params
    params.require(:fee).permit(:name,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :gbox, :partner, :iso, :agent, :fee_id, :fee_status, :location_id, :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee, :commission)
  end

  def edit_fee_params
    params.require(:fee).permit(:id,:name,:customer_fee,:customer_fee_dollar,:transaction_fee_app,:transaction_fee_app_dollar,:transaction_fee_dcard,:transaction_fee_dcard_dollar,:transaction_fee_ccard,:transaction_fee_ccard_dollar, :b2b_fee, :redeem_fee, :send_check,:add_money_check, :gbox, :partner, :iso, :agent, :fee_id, :fee_status, :location_id, :redeem_check, :giftcard_fee, :reserve_fee, :days,:retrievel_fee,:charge_back_fee, :commission)
  end
end
