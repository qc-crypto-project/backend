class Admins::EmailTemplatesController < AdminsController

  def index
    @email_templates = Emailtemplate.all  
  end

  def edit
    @et = Emailtemplate.find(params[:id])
  end

  def update
    @et = Emailtemplate.find(params[:id])
    @et.update(email_params)
    redirect_to root_path
  end

  private

  def email_params
    params.require(:emailtemplate).permit(:body ,:title , :category)
  end
end
