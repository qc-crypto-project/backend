class Admins::TransfersController < AdminsController
  layout 'admin'
  include ApplicationHelper
  # autocomplete :user, :name, :full => true
  def index
    @heading = "Account Transfer"
    @transfers = Transfer.includes(:sender, :receiver).order("created_at DESC").per_page_kaminari(params[:page]).per(10)
    @transfer = Transfer.new(amount: "")
  end

  def get_location_wallets
    wallets = []
    location = Location.find_by_id(params[:location_id])
    if location.present?
      wallets = location.wallets
    end
    if wallets.present?
      render status: 200 , json:{:wallets => wallets}
    else
      render status: 200 , json:{:wallets => ""}
    end
  end

  # -------------get Wallet Name-----------
  def get_wallet_name
    from_wallet = Wallet.find(params[:from_wallet]) if params[:from_wallet].present?
    to_wallet = Wallet.find(params[:to_wallet]) if params[:to_wallet].present?
    from_wallet_name = ""
    to_wallet_name = ""
    if from_wallet.present?
      from_wallet_name = from_wallet.name
    end
    if to_wallet.present?
      to_wallet_name = to_wallet.name
    end
    render status: 200 , json:{:from_wallet => from_wallet_name,:to_wallet => to_wallet_name}
  end

  def get_user_details
    wallet=Wallet.find(params[:wallet_id])
    user=wallet.users.first
    user_name=user.name
    wallet_id=wallet.id
    if user.MERCHANT?
      wallets=[]
      locations=[]
      user_type="merchant"
      location=wallet.location #here with the help of wallet_id, we find the location to whom this wallets belongs.
      location_id=location.id
      location_wallets=location.wallets
      if location_wallets.present?
        location_wallets.each do |location_wallet| #here we show all the wallets of a specific location
          wallets << {
              id: location_wallet.id,
              name: location_wallet.name
          }
        end
      end
      primary_wallets=user.wallets.primary
      if primary_wallets.present? #here if primary wallet is present then we get its name and display it in list.
        primary_wallets.each do |merchant_wallet|
          wallet_location=merchant_wallet.location
          location_name=""
          location_name=wallet_location.first_name+" "+wallet_location.last_name if wallet_location.business_name.nil?
          location_name=wallet_location.business_name if wallet_location.business_name.present?
          if wallet_location.present?
            locations << {
                id: wallet_location.id,
                name: location_name
            }
          end
        end
      end
    else
    user_type="not_merchant"
    end
    if user.present?
      if user_type=="merchant" #here we display suggestions for Merchant/ISO/Affiliate/Agent and their details as per the input
        render status: 200 , json:{:user_type => user_type,:user_name => user_name.present? ? user_name : "",:wallet_id => wallet_id.present? ? wallet_id : "", :location_id => location_id.present? ? location_id : "", :locations => locations, :wallets => wallets}
      else
        render status: 200 , json:{:user_type => user_type,:user_name => user_name.present? ? user_name : "",:wallet_id => wallet_id.present? ? wallet_id : ""}
      end
    else
      render status: 200 , json:{:user_type => ''}
    end
  end

  def autocomplete_user_name #this function helps in suggesting users like merchant/ISO/Affiliate/Agent
    limit = 100
    json = []
    params[:term] = params[:term].strip #here :term contains the input and we remove the spaces from it using strip
    value= params[:term].split('-').last if params[:term].split('-').present? #here we split the input at '-'
    search_results = User
      .select('users.id', 'users.name', 'users.company_name', 'users.role', 'users.merchant_id', 'wallets.wallet_type')
      .joins(:wallets)
      .where("wallets.wallet_type = ? OR wallets.wallet_type = ?", 0, 3) #here 0,3 are wallet types like primary or tip wallet etc
      .where('users.first_name ILIKE ? OR users.name ILIKE ? OR users.company_name ILIKE ?  OR CAST(users.id AS VARCHAR) = ?', "%#{params[:term].downcase}%", "%#{params[:term].downcase}%", "%#{params[:term].downcase}%" , value) #here we search on the basis of user name, company name and user id
      .where.not(role: :atm).where.not(archived: :true).distinct
      .limit(limit) #here we limit the search results to 100
    # searched_users = User.includes(:wallets).where('lower(first_name) LIKE ? OR lower(name) LIKE ? OR lower(company_name) LIKE ?', "%#{params[:term].downcase}%", "%#{params[:term].downcase}%", "%#{params[:term].downcase}%").where.not(role: :atm)
    # if params[:term].present?
    #   isos = searched_users.iso.limit(20)
    #   agents = searched_users.agent.limit(20)
    #   affiliates = searched_users.affiliate.limit(20)
    #   users = searched_users.user.limit(20)
    #   merchants = searched_users.merchant.limit(20)
    #   users = merchants + users + isos + agents + affiliates + searched_users.qc
    # end
    if search_results.present?
      json = search_results.collect do |u| #here we use the search_results that holds the data of users
        role = nil
        if u.merchant? && u.merchant_id.present?
          wallet = u.wallets.primary.where(location_id: nil).try(:first).try(:id)
          role = "sub merchant"
        elsif u.merchant? && u.merchant_id.blank?
          wallet = u.id
        elsif u.iso? || u.agent? || u.affiliate? || u.user? || u.partner?
          wallet = u.wallets.primary.try(:first).try(:id)
        elsif u.qc?
          wallet = Wallet.qc_support.first.try(:id)
        end
        if u.iso? #here we display the suggestions in a specific format
          ref_no= "ISO-#{u.id}"
        elsif u.agent?
          ref_no= "A-#{u.id}"
        elsif u.affiliate?
          ref_no= "AF-#{u.id}"
        elsif u.merchant?
          ref_no= "M-#{u.id}"
        elsif u.user?
          ref_no="C-#{u.id}"
        elsif u.partner?
          ref_no="P-#{u.id}"
        end
        puts "role: #{u.role}, wallet: #{wallet}"
        { "id" => wallet,
          "label" =>  u.company_name.present? ?  "(#{ref_no}) - #{u.company_name.try(:capitalize)}" : u.try(:name).present? ? "(#{ref_no}) - #{u.name.try(:capitalize)}" : "",
          "value" => u.company_name.present? ? "(#{ref_no}) - #{u.company_name.try(:capitalize)}" :u.try(:name).present? ? "(#{ref_no}) - #{u.name.try(:capitalize)}" : "",
          "category" => role.present? ? role.try(:capitalize) : u.role.try(:capitalize)
        }
      end
    end

    # json = search_results.map do |user|
    #   {
    #     id: user.qc? ? Wallet.qc_support.try(:first).try(:id) : user.wallet_id,
    #     label: user.company_name.try(:capitalize) || user.name.try(:capitalize),
    #     value: user.company_name || user.name,
    #     category: user.merchant? && user.merchant_id.present? ? 'Sub Merchant' : user.role.try(:humanize)
    #   }
    # end.to_json
    render json: json
  end

  def get_merchant_location
    user = User.find_by(id: params[:id])
    json = {}
    if user.present?
      locationsList = []
      wallets = user.wallets.primary.where.not(:location_id => nil)
      if wallets.present?
        wallets.each do |w|
          merchant_location = w.location
          if merchant_location.present?
            locationsList << { #here we populate the location list of a specific merchant
                id: merchant_location.id,
                name: merchant_location.business_name,
            }
          else
            # locationsList = []
          end
        end
      end
      json = { "locations" => locationsList}
      render json: json
    else
      render json: json
    end
  end
  #   --------------------------Merchant Transfer Add----------------------------------
  def transfer_post
    begin
      if !new_transfer_params[:to_wallet].present?
        flash[:error] = "Please Select a Valid User to Transfer Amount To!"
        return redirect_back(fallback_location: root_path)
      elsif !new_transfer_params[:from_wallet].present?
        flash[:error] = "Please Select a Valid User to Transfer Amount From!"
        return redirect_back(fallback_location: root_path)
      end
      if new_transfer_params[:amount].to_f <= 0 #here we convert the amount to float and check if its zero
        flash[:error] = "Error Amount Cannot Be 0"
        return redirect_back(fallback_location: root_path)
      end
      if new_transfer_params[:from_wallet] == new_transfer_params[:to_wallet] #here we check if we are sending the amount to the same wallet
        flash[:error] = "Error Wallets Cannot Be Same"
        return redirect_back(fallback_location: root_path)
      end
      balance = show_balance(new_transfer_params[:from_wallet]) #here if balance is less than the amount entered then an error is displayed.
      if balance < new_transfer_params[:amount].to_f
        flash[:error] = "Not Enough Balance in Wallet"
        return redirect_back(fallback_location: root_path)
      end

      timezone = TIMEZONE
      timezone = cookies[:timezone] if cookies[:timezone].present?
      today_date = Time.now.in_time_zone(timezone).to_date.strftime("%m/%d/%Y")
      params[:transfer]["transfer_timezone"] = timezone
      transfer_date = new_transfer_params[:transfer_date] if new_transfer_params[:transfer_date].present?
      params[:transfer][:transfer_date] = Date.strptime(transfer_date,"%m/%d/%Y") if params[:transfer][:transfer_date].present?
      transfer = Transfer.new(new_transfer_params)
      wallets = Wallet.where(id: [new_transfer_params[:from_wallet],new_transfer_params[:to_wallet]]).group_by{|e| e.id}
      sender = wallets[new_transfer_params[:from_wallet].to_i].first #here we display the sender info via wallet
      sender_user = sender.users.first if sender.present?
      transfer.user_id = sender_user.id
      receiver = wallets[new_transfer_params[:to_wallet].to_i].first #here we display the receiver info via wallet
      receiver_user = receiver.users.first if receiver.present?
      status = nil
      if today_date == transfer_date #here we do the transaction if transfer date is the same as today else if set it as pending
        transfer_details = {
            amount: new_transfer_params[:amount],
            memo: new_transfer_params[:memo].present? ? new_transfer_params[:memo] : '',
            date: transfer_date,
            transfer_timezone: timezone
        }
        tags = {
            "transfer_details" => transfer_details
        }
        db_transaction = Transaction.create( #here we create the transaction using the details we found in above code.
            to: new_transfer_params[:to_wallet],
            from: new_transfer_params[:from_wallet],
            status: "pending",
            amount: new_transfer_params[:amount],
            sender_wallet_id: new_transfer_params[:from_wallet],
            receiver_wallet_id: new_transfer_params[:to_wallet],
            sender_name: sender_user.try(:name),
            receiver_name: receiver_user.try(:name),
            sender_balance: SequenceLib.balance(new_transfer_params[:from_wallet]),
            receiver_balance: SequenceLib.balance(new_transfer_params[:to_wallet]),
            sender: sender_user,
            receiver: receiver_user,
            tags: tags,
            action: "transfer",
            net_amount: new_transfer_params[:amount],
            total_amount: new_transfer_params[:amount],
            ip: get_ip,
            main_type: TypesEnumLib::TransactionType::AccountTransfer
        )
        #below we set the transaction using sequence
        transaction = SequenceLib.transfer(number_with_precision(new_transfer_params[:amount],precision:2),new_transfer_params[:from_wallet],new_transfer_params[:to_wallet],TypesEnumLib::TransactionType::AccountTransfer,0,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
        if transaction.present? && transaction.id.present?
          db_transaction.update( #here we update the transaction that we performed above
              tags: transaction.actions.first.tags,
              timestamp: transaction.timestamp,
              seq_transaction_id: transaction.actions.first.id,
              status: I18n.t("statuses.approved")
          )
          #= creating block transaction
          parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          transfer.sequence_id = transaction.actions.first.id
          transfer.status = I18n.t("statuses.completed")
          status = transfer.save
          @wallet = Wallet.friendly.find(params[:transfer][:from_wallet])
          @wallet.balance = SequenceLib.balance(@wallet.id)
          @wallet = Wallet.friendly.find(params[:transfer][:to_wallet])
          @wallet.balance = SequenceLib.balance(@wallet.id)
        end
      else
        transfer.status="pending" #here if today isn't transfer day then we set status as pending
        status=transfer.save
      end
      if status.present?
        msg = I18n.t("merchant.controller.notification_transfer")
        activity_type = I18n.t("statuses.success")
      else
        msg = I18n.t('errors.withdrawl.cant_transfer')
        activity_type = I18n.t("statuses.error")
      end
    rescue => exc
      msg = exc.message
      activity_type = I18n.t("statuses.error")
    ensure
      ActivityLog.log(activity_type, msg,current_user,params)
      flash[activity_type.to_sym] = msg
      return redirect_back(fallback_location: root_path)
    end
  end

  def export
    if params[:export].present?
      @key,@key1 = search_query(params[:export])
      date = params[:export]["date"].present? ? parse_daterange(params[:export]["date"]) : nil
      if date.present?
        date = {
            first_date: Date.new(date.first[:year].to_i, date.first[:month].to_i, date.first[:day].to_i),
            second_date: Date.new(date.second[:year].to_i, date.second[:month].to_i, date.second[:day].to_i)
        }
      end
      wallets = Wallet.joins(:location).where(@key1, dba_name: "%#{params[:export]["dba_name_search"].try(:strip)}%").pluck(:id) if @key1.present?
      conditions = ["from_wallet IN (?) OR to_wallet IN (?)", wallets,wallets] if wallets.present?
      @transfers = Transfer.joins(:user).where(@key, name: "%#{params[:export]["merchant_name_search"].try(:strip)}%", merchant_id: params[:export]["merchant_id_search"].try(:strip), amount: params[:export]["amount_search"].try(:strip), transfer_date_start: date.try(:[],:first_date), transfer_date_end: date.try(:[],:second_date)).where(conditions).distinct.order(transfer_date: :desc)
      # @transfers = Transfer.joins(user: :my_locations).where(@key, name: "%#{params[:export]["merchant_name_search"].try(:strip)}%", merchant_id: params[:export]["merchant_id_search"].try(:strip), dba_name: "%#{params[:export]["dba_name_search"].try(:strip)}%", amount: params[:export]["amount_search"].try(:strip), transfer_date_start: date.try(:[],:first_date), transfer_date_end: date.try(:[],:second_date)).where(conditions).distinct.order(transfer_date: :desc)
    else
      @transfers = Transfer.includes(:sender, :receiver).order("transfer_date DESC")
    end
    send_data generate_csv(@transfers), filename: "Transfer.csv"
  end

  def generate_csv(transfers)
    CSV.generate do |csv|
      csv << ['Txn ID', 'Transfer Date', 'Merchant Name', 'Transfer From', 'Transfer To', 'Amount', 'Memo']
      transfers.each do |transfer|
        csv << [transfer.sequence_id, transfer.transfer_date.strftime("%m/%d/%Y"), transfer.user.try(:name), transfer.sender.try(:name), transfer.receiver.try(:name), number_to_currency(number_with_precision(transfer.amount, :precision => 2, delimiter: ',')), transfer.memo]
      end
    end
  end

  def search
    if params[:query].present? && params[:query].select{ |k,v| v.present? }.present?
      @key,@key1 = search_query(params[:query])
      date = params[:query]["date"].present? ? parse_daterange(params[:query]["date"]) : nil
      if date.present?
        date = {
            first_date: Date.new(date.first[:year].to_i, date.first[:month].to_i, date.first[:day].to_i),
            second_date: Date.new(date.second[:year].to_i, date.second[:month].to_i, date.second[:day].to_i)
        }
      end
      wallets = Wallet.joins(:location).where(@key1, dba_name: "%#{params[:query]["dba_name_search"].try(:strip)}%").pluck(:id) if @key1.present?
      conditions = ["from_wallet IN (?) OR to_wallet IN (?)", wallets,wallets] if wallets.present?
      @transfers = Transfer.joins(:user).where(@key, name: "%#{params[:query]["merchant_name_search"].try(:strip)}%", merchant_id: params[:query]["merchant_id_search"].try(:strip), amount: params[:query]["amount_search"].try(:strip), transfer_date_start: date.try(:[],:first_date), transfer_date_end: date.try(:[],:second_date)).where(conditions).distinct.order(transfer_date: :desc).per_page_kaminari(params[:page]).per(10)
      @heading = "Account Transfer"
      @transfer = Transfer.new(amount: "")
      # @transfers = Kaminari.paginate_array(@transfers).page(params[:page]).per(10)
    else
      @heading = "Account Transfer"
      @transfer = Transfer.new(amount: "")
      @transfers = Transfer.includes(:sender, :receiver).order("transfer_date DESC").per_page_kaminari(params[:page]).per(10)
    end
    render template: 'admins/transfers/index'
  end

  private

  def search_query(query)
    key = nil
    key1 = nil
    if query["merchant_name_search"].present?
      key = "users.name ILIKE :name"
    end
    if query["merchant_id_search"].present?
      if key.nil?
        key = "users.id = :merchant_id"
      else
        key += " AND users.id = :merchant_id"
      end
    end
    if query["amount_search"].present?
      if key.nil?
        key = "amount = :amount"
      else
        key += " AND amount = :amount"
      end
    end
    if query["dba_name_search"].present?
      if key1.nil?
        key1 = "locations.business_name ILIKE :dba_name"
      else
        key1 += " AND locations.business_name ILIKE :dba_name"
      end
    end

    date = query["date"].present? ? parse_daterange(query["date"]) : nil

    if date.present?
      date = {
          first_date: Date.new(date.first[:year].to_i, date.first[:month].to_i, date.first[:day].to_i),
          second_date: Date.new(date.second[:year].to_i, date.second[:month].to_i, date.second[:day].to_i)
      }
    end

    if date.present? && date.try(:[],:first_date).present?
      if key.nil?
        key = "transfer_date >= :transfer_date_start"
      else
        key += " AND transfer_date >= :transfer_date_start"
      end
    end
    if date.present? && date.try(:[],:second_date).present?
      if key.nil?
        key = "transfer_date <= :transfer_date_end"
      else
        key += " AND transfer_date <= :transfer_date_end"
      end
    end
    return [key,key1]
  end

  def new_transfer_params
    # params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,)
    params.require(:transfer).permit(:from_wallet ,:to_wallet, :amount, :transfer_date, :memo,:transfer_timezone)
  end
end