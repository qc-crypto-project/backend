class Admins::PartnersController < AdminsController
  skip_before_action :check_admin
  def index
    @partner = User.new
    @partners = User.partner
    respond_to do |format|
      format.html{ render :index}
      format.json { render json: PartnersDatatable.new(view_context, @partners)}
    end
  end

  def new
    render template: 'admins/partners/new'
  end

  def create
    @partner = User.new(new_partner_params)
    @merchant=User.find params[:user][:merchant_id]
    @partner.regenerate_token
    if @partner.partner!
      @merchant.partner= @partner
      respond_to do |format|
        format.html{
          flash[:success] = 'Partner has been added Successfully!'
        }
      end
    else
      flash[:error] = @partner.errors.full_messages.first
    end
    redirect_back(fallback_location: root_path)
  end

  def edit
    @partner= User.find params[:id]
    respond_to :js
  end

  def update
    partner = User.find params[:id]
    partner.update(edit_partner_params )
    if partner.errors.blank?
      flash[:success] = "Partner has been updated Successfully"
      redirect_back(fallback_location: root_path)
    else
      flash[:error] = "Error occured in Edit"
      redirect_back(fallback_location: root_path)
    end
  end

  def destroy
  end

  private

  def new_partner_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
  end

  def edit_partner_params
    params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status,  :merchant_id, :first_name, :last_name,)
  end
end
