class Admins::InstantPayController < AdminsController

  include WithdrawHandler
  before_action :check_admin

  def index
    @ach_ids = []
    @original_batch_amount = 0
    @actual_batch_amount = 0
    @batch_date = params[:batch_date]
    if params[:q].present? && params[:q] != "__"
      @all_achs = getting_filtered_achs(params)
    elsif params[:batch_search_params].present?
      params[:batch_search_params] = JSON(params[:batch_search_params])
      @all_achs = getting_batch_searched_checks(params)
    elsif @batch_date.present?
      batch_start_date_int = (@batch_date.to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      @all_achs = TangoOrder.instant_pay.batch_date(batch_start_date..batch_end_date)
    else
      @all_achs = TangoOrder.instant_pay
    end
    @category = params[:batch_p2c_id].split("-").first
    if @category == "AFP"
      @all_achs = @all_achs.where(ach_afp: true)
    else
      @all_achs = @all_achs.where(ach_afp: nil)
    end
    @grouped_p2cs = @all_achs.group_by{|ach| ach.status}
    @original_batch_amount = @all_achs.pluck(:actual_amount).try(:sum, &:to_f)
    @pending_achs = @grouped_p2cs["PENDING"].try(:count)
    @total_pending_amount = @grouped_p2cs["PENDING"].try(:pluck, :actual_amount).try(:sum, &:to_f)
    @total_process_achs = @grouped_p2cs.select{|status, p2c| status != "PENDING" && status != "FAILED" && status != "VOID"}.try(:values).try(:flatten).try(:pluck, :actual_amount).try(:sum, &:to_f)
    @paid_achs = @grouped_p2cs["PAID"].try(:count)
    @paid_by_wire_achs = @grouped_p2cs["PAID_WIRE?"].try(:count)
    @failed_achs = @grouped_p2cs["FAILED"].try(:count)
    @ach_ids_status = @all_achs.pluck(:id, :status)
    if @ach_ids_status.present?
      @ach_ids_status.each do |row|
        @ach_ids.push(row.first)
      end
    end
    @all_achs = @all_achs.order(id: :desc).per_page_kaminari(params[:page]).per(params[:filter] || 10)
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js
      format.html
      format.csv { send_data generate_csv(@all_achs.except(:limit, :offset),@batch_date), filename: "instantPay_#{@batch_date}.csv" }
    end
  end

  def update_name
    if params[:check_id].present? && params[:tango_order][:name].present?
      check = TangoOrder.find(params[:check_id])
      check.name = params[:tango_order][:name]
      check.save!
    end
  end

  def new
    @merchants=User.merchant || []
    @locations= []
    @wallets= Wallet.all || []
    respond_to :js
  end

  def selected_locations
    if params[:merchant_id].present?
      user = User.find(params[:merchant_id])
      @locations = user.get_locations
    else
      @locations = []
    end
    respond_to :js
  end

  def instant_pay_batch_date
    if params[:ach_id].present?
      @ach_pending=TangoOrder.find(params[:ach_id])
      datetime = Date.strptime(params[:batch_date], "%m/%d/%Y").to_datetime.end_of_day
      if @ach_pending.update(:batch_date => datetime)
        flash[:success] = "Batch Date Updated Successfully"
        redirect_to admins_instant_pay_batches_path
      end
    end
  end

  def instant_pay_approved(ispaid=nil)
    result = {}
    if params[:id].present?
      # instants_pays = params[:ids].split(',')
      # instants_pays.each do|c|
      check = TangoOrder.find_by(id: params[:id])
      if check.present?
        if check.status != "PENDING"
          flash[:error] = "#{check.id} is already approved. Please remove it and try again later!"
          result = {success: false, message: "#{check.id} is already approved. Please remove it and try again later!"}
        else
          amount = check.actual_amount.to_f
          recipient = check.recipient
          name = check.name
          description = check.description
          send_via = check.send_via
          account_number = ''
          zip = ''
          account_type = ''
          cvv = ''
          address_line_1 = ''
          address_line_2 = ''
          routing_number = ''
          account_number = ''
          if check.user_id.present?
            begin
              user = User.find(check.user_id)
              wallet = Wallet.find(check.wallet_id)
              # balance = show_balance(@wallet.id, @user.ledger).to_f
              location = wallet.location
              if location.present? && location.block_withdrawal
                # flash[:error] = "Push to Card is blocked for this user"
                result = {success: false, message: "Error Code 4001 Push to Card is blocked for this user"}
              end
              if user.present? && wallet.present? && check.instant_pay?
                amount_sum = 0
                fee_lib = FeeLib.new
                if user.MERCHANT?
                  location_fee = location.fees if location.present?
                  fee_object = location_fee.buy_rate.first if location_fee.present?
                  fee_class = Payment::FeeCommission.new(user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::DebitCardDeposit)
                  fee = fee_class.apply(amount.to_f, TypesEnumLib::TransactionType::PushtoCardDeposit)
                  amount_sum = amount + fee.symbolize_keys[:fee].to_f
                elsif user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
                  amount_sum, fee = deduct_p2_fee(user,amount)
                  fee = {fee: fee}
                else
                  amount_sum = amount + fee_lib.get_card_fee(amount, 'echeck').to_f
                  check_fee = fee_lib.get_card_fee(amount.to_f, 'echeck').to_f
                end
                check_info = {
                    name: check.name,
                    check_email: check.recipient,
                    check_id: check.checkId,
                    check_number: check.number
                }

                url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/instant")
                @recipient=recipient
                http = Net::HTTP.new(url.host,url.port)
                http.use_ssl = true
                http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                request = Net::HTTP::Post.new(url)
                decrypted = AESCrypt.decrypt("#{check.user_id}-#{ENV['CARD_ENCRYPTER']}-#{check.wallet_id}}",  check.account_token)
                data = JSON(decrypted)
                card_number= data["card_number"]
                expiry_date = data["expiry_date"]
                if expiry_date.to_s.size > 7
                  exp_year = expiry_date.split('-').first
                  exp_month = expiry_date.split('-').last
                  if exp_year.size > 4
                    expiry_date = "#{exp_year.last(4)}-#{exp_month}"
                  end
                end
                request.body = {name: name, recipient: recipient, amount: amount, description: description, card_number: card_number, expiration_date: expiry_date, zip: zip, account_type: account_type, cvv: cvv, address_line_1: address_line_1, address_line_2: address_line_2, routing_number: routing_number, account_number: account_number }.to_json
                request["Authorization"] = "#{ENV['CHECKBOOK_KEY']}:#{ENV['CHECKBOOK_SECRET']}"
                # request["Authorization"] = @@check_book_key+":"+@@check_book_secret
                request["Content-Type"] ='application/json'
                response = http.request(request)
                p "CHECKBOOK REQUEST RESPOSNE: ", response
                error = nil
                case response
                when Net::HTTPSuccess
                  digital_check = JSON.parse(response.body)
                  if response.message == "CREATED"
                    escrow_wallet = Wallet.check_escrow.first
                    escrow_user = escrow_wallet.try(:users).try(:first)
                    gbox_fee = save_gbox_fee(fee["splits"], fee.symbolize_keys[:fee].to_f)
                    total_fee = save_total_fee(fee["splits"], fee.symbolize_keys[:fee].to_f)
                    check_info[:check_id] = digital_check["id"]
                    check_info[:check_number] = digital_check["number"]
                    transaction = Transaction.create(
                        from: escrow_wallet.try(:id),
                        status: "pending",
                        amount: amount,
                        sender: escrow_user,
                        sender_name: escrow_user.try(:name),
                        sender_wallet_id: escrow_wallet.try(:id),
                        sender_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                        action: 'retire',
                        fee: total_fee.to_f,
                        net_amount: check.actual_amount.to_f,
                        net_fee: total_fee.to_f,
                        total_amount: check.amount.to_f,
                        gbox_fee: gbox_fee.to_f,
                        iso_fee: save_iso_fee(fee["splits"]),
                        agent_fee: fee["splits"].try(:[],"agent").try(:[],"amount").to_f,
                        affiliate_fee: fee["splits"].try(:[],"affiliate").try(:[],"amount").to_f,
                        ip: get_ip,
                        main_type: TypesEnumLib::TransactionType::PushtoCardDeposit,
                        )
                    if user.MERCHANT?
                      issue = issue_checkbook_amount(check.actual_amount, check.wallet_id ,TypesEnumLib::TransactionType::PushtoCardDeposit,TypesEnumLib::GatewayType::Checkbook, fee.symbolize_keys[:fee],fee["splits"], check_info,check,true,nil,nil,ispaid)
                    elsif user.ISO? || user.AGENT? || user.AFFILIATE? || user.affiliate_program?
                      issue = issue_checkbook_amount(check.actual_amount, check.wallet_id ,TypesEnumLib::TransactionType::PushtoCardDeposit,TypesEnumLib::GatewayType::Checkbook, fee.symbolize_keys[:fee].to_f,nil, check_info,check,true,true)
                    end
                    if issue.present?
                      transaction.update(
                          status: "approved",
                          seq_transaction_id: issue.actions.first.id,
                          timestamp: issue.timestamp,
                          tags: issue.actions.first.tags
                      )
                      parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                      save_block_trans(parsed_transactions, "sub","PAID") if parsed_transactions.present?
                      check.void_transaction_id = transaction.id
                    end
                    check.checkId = digital_check["id"]
                    check.number = digital_check["number"]
                    check.catalog_image = digital_check["image_uri"]
                    check.status = digital_check["status"]
                    check.approved = true
                    check.amount_escrow = false
                    check.bulk_check.update(:status => "Success", :response_message => response.body) if check.bulk_check.present?
                    check.save
                    result = {success: true, message: "Succesfully approved a Push to Card transaction to #{ check.recipient }"}
                  else
                    raise "Something went wrong, please contact your administrator."
                  end
                  # return redirect_back(fallback_location: root_path)
                when Net::HTTPUnauthorized
                  error = JSON.parse(response.body)
                  raise I18n.t 'merchant.controller.unauthorize_access'
                when Net::HTTPNotFound
                  error = JSON.parse(response.body)
                  raise I18n.t('merchant.controller.record_notfound')
                when Net::HTTPServerError
                  error = JSON.parse(response.body)
                  raise "Wrong credentials! Please create the request again with the correct user credentials or contact your administrator."
                else
                  error = JSON.parse(response.body)
                  unless [I18n.t('checkbook.error.insufficient_funds'),I18n.t('checkbook.error.limit_exceed')].include? error["error"]
                    if wallet.location.present?
                      location = location_to_parse(wallet.location)
                      merchant = merchant_to_parse(wallet.location.merchant)
                    elsif wallet.try('users').try('first').present? &&  wallet.try('users').try('first').role!='merchant'
                      location=nil
                      merchant = merchant_to_parse(wallet.users.first)
                    end
                    tags = {
                        "location" => location,
                        "merchant" => merchant,
                        "send_check_user_info" => check_info,
                    }
                    check = create_p2c_failed_withdrawal_transaction(check,user,wallet,tags,check_info)
                    check.status = "FAILED"
                    check.approved = true
                    check.settled = true
                    check.save
                  end
                  errorMessage = "#{error["error"]}, Reason: #{error["more_info"]}"
                  raise error["more_info"].present? ? "#{errorMessage}" : error["error"]
                end
              else
                raise "Something went wrong, please contact your administrator."
              end
            rescue => ex
              p "CHECKBOOK.IO ERROR THROWN: ", ex
              msg = "#{error.try(:[], "error")}, Reason: #{error.try(:[], "more_info")}"
              handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'))
              result = {success: false, message: ex.message}
            end
          else
            # flash[:error] = "Please select a wallet and try again "
            result = {success: false, message: "Please select a wallet and try again."}
          end
        end
      end
      return result
      # if params[:ids].present?
      #   achs = params[:ids].split(',')
      #   achs.each do|c|
      #     check = TangoOrder.find_by(id: c.to_i)
      #     if check.status != "PENDING"
      #       flash[:error] = "#{check.id} is already approved. Please remove it and try again later!"
      #       break
      #     end
      #     if check.present?
      #       result = instant_pay_approve_func(check,params)
      #       if result[:success] == false
      #         if check.checkId.present?
      #           flash[:error] = "We have Error on #{check.checkId.last(6)}. Please try again later!"
      #         else
      #           flash[:error] = "We have an Error on #{check.id}. Please try again later!"
      #         end
      #         break
      #       else
      #         flash[:success] = "Successfully approved!"
      #       end
      #     else
      #       flash[:error] = "Something went wrong. Please try again later!"
      #     end
      #   end
      # else
      #   flash[:error] = "Please select transactions!"
      # end
      # return redirect_back(fallback_location: root_path )
    end

  end


  # def instant_pay_approved
  #   if params[:ids].present?
  #     achs = params[:ids].split(',')
  #     achs.each do|c|
  #       check = TangoOrder.find_by(id: c.to_i)
  #       if check.status != "PENDING"
  #         flash[:error] = "#{check.id} is already approved. Please remove it and try again later!"
  #         break
  #       end
  #       if check.present?
  #         result = instant_pay_approve_func(check)
  #         if result[:success] == false
  #           if check.checkId.present?
  #             flash[:error] = "We have Error on #{check.checkId.last(6)}. Please try again later!"
  #           else
  #             flash[:error] = "We have an Error on #{check.id}. Please try again later!"
  #           end
  #           break
  #         else
  #           flash[:success] = "Successfully approved!"
  #         end
  #       else
  #         flash[:error] = "Something went wrong. Please try again later!"
  #       end
  #     end
  #   else
  #     flash[:error] = "Please select transactions!"
  #   end
  #   return redirect_back(fallback_location: root_path )
  # end
  #
  # #
  def ach_batch_date
    if params[:ach_id].present?
      @ach_pending=TangoOrder.find(params[:ach_id])
      datetime = Date.strptime(params[:batch_date], "%m/%d/%Y").to_datetime.end_of_day
      if @ach_pending.update(:batch_date => datetime)
        flash[:success] = "Batch Date Updated Successfully"
        redirect_to admins_total_funds_path
      end
    end
  end

  def bulk_status_change
    ids = params[:ids]
    ids = ids.split(",")
    params[:type] = params[:p2c_status]
    params[:issue] = "issue"
    false_p2cs = []
    result = nil
    ids.each do |id|
      if id.to_i.to_s == id
        check = TangoOrder.find_by(id: id)
        params[:id] = id
        if params[:type] != "FAILED" && (check.status == "PAID" || check.status == "PAID_WIRE")
          false_p2cs.push(check.id)
        elsif check.status.try(:downcase) === 'failed'
          false_p2cs.push(check.id)
        elsif check.status == "VOID"
          false_p2cs.push(check.id)
        elsif params[:type] == "VOID" && (check.status == "IN_PROCESS" || check.status == "IN_PROGRESS")
          false_p2cs.push(check.id)
        else
          if params[:type] == "PAID" && check.status == "PENDING"
            ispaid = true
            result = instant_pay_approved(ispaid)
          else
            result = withdrawal_bulk_status_update
          end
        end
        if result.present?
          if result[:success] == false
            flash[:error] = result[:message]
          else
            flash[:success] = result[:message]
          end
        end
      end
    end
    false_p2cs = false_p2cs.join(',')
    if false_p2cs.length > 0
      flash[:error] = "The Selected Status can not be applied to ID #{false_p2cs}. Please remove it and try again later!"
    end
    return redirect_back(fallback_location: root_path )
  end

  def batches

    @debit_deposit = AppConfig.where(key: AppConfig::Key::DebitCardDeposit).first
    @debit_deposit = AppConfig.new(key: AppConfig::Key::DebitCardDeposit) unless @debit_deposit.present?


    @ach_batches = TangoOrder.instant_pay.group_by_day(time_zone: "Pacific Time (US & Canada)", day_start: 14) {|u| u.batch_date }
    # @pagination = Kaminari.paginate_array([],total_count: @ach_batches.try(:length)).page(params[:page]).per(1)
  end

  def location_data
    if params[:location_id].present?
      @location = Location.find(params[:location_id])
    else
      @location = Location.new
    end
    begin
      @bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT-ENCRYPTOR']}",@location.bank_account)
    rescue
      @bank_account = @location.bank_account
    end
    walet_id = @location.wallets.primary.last.id
    @balance = show_balance(walet_id) - HoldInRear.calculate_pending(walet_id)
    respond_to :js
  end

  def get_bank_by_routing_number
    parameters = [["routing_number", "#{params[:routing_number]}"]]
    uri = URI("#{ENV['CHECKBOOK_ROUTING_URL_LIVE']}get_bank_by_routing_number")
    # Sending Request To Checkboox IO
    response = Net::HTTP.post_form( uri, parameters)
    if response.is_a?(Net::HTTPSuccess)
      if response.read_body.present?
        render status: 200 , json:{:response => response.read_body}
      else
        render status: 200 , json:{:response => ""}
      end
    else
      render status: 200 , json:{:response => ""}
    end
  end

  def verification
  end

  def verify_phone_number
    invoice_ids = JSON.parse(params[:invoice_id])
    achs = TangoOrder.where(id: invoice_ids)
    # invoice = TangoOrder.find(params[:invoice_id])
    if params[:phone_number].present?
      verification_phone_numbers = ENV["VERIFICATION_PHONE_NUMBERS"].present? ? ENV["VERIFICATION_PHONE_NUMBERS"].split(",") : [ENV['PHONE_NUMBER_FOR_PAY_INVOICE2'],ENV['PHONE_NUMBER_FOR_PAY_INVOICE1']]
      if (verification_phone_numbers.include?(params[:phone_number]))
        @random_code = rand(1_000..9_999)
        achs.update_all(pincode: @random_code)
        # a = params[:phone_number]
        a = "1"+params[:phone_number]
        TextsmsWorker.perform_async(a, "Your pin Code for Upload is: #{@random_code}")
      else
        render status: 404, json:{error: 'Wrong Phone Number'}
      end
    end
    if params[:pincode].present?
      if achs.pluck(:pincode).uniq.count == 1 && achs.pluck(:pincode).uniq.first == params[:pincode]
        render status: 200, json:{success: 'Verified'}
      else
        render status: 404, json:{error: 'Wrong Pin Code'}
      end
    end
  end

  def update
    begin
      p2c = TangoOrder.where(id: params[:id]).select(:id, :status).last if params[:id].present?
      if params[:type] == "PAID" && p2c.status == "PENDING"
        ispaid = true
        result = instant_pay_approved(ispaid)
      else
        result = withdrawal_bulk_status_update
      end
      if result[:success] == false
        flash[:error] = result[:message]
      else
        flash[:success] = result[:message]
      end
  rescue AdminCheckError => exc
    flash[:error] = "Sorry for Inconvinence"
  ensure
    return redirect_back(fallback_location: root_path )
  end
  end

  private

  def getting_filtered_achs(params)
    params[:q_value] = params[:q].try(:strip) if params[:q_value].blank?
    params[:q] = {"status_or_name_cont" => params[:q_value]}
    params[:q][:user_name_cont] =  params[:q_value]
    #params[:q][:user_id_eq] = params[:q_value]
    params[:q][:user_id_eq] = params[:q_value].try(:split, "-").try(:last).to_i == ""  ? "" : params[:q_value].try(:split, "-").try(:last) if params[:q_value].try(:split,"-").try(:last).to_i != 0
    params[:q][:id_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
    params[:q][:number_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
    params[:q][:actual_amount_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
    params[:q][:amount_eq] = params[:q_value].to_i == ""  ? "" : params[:q_value] if params[:q_value].to_i != 0
    ach_ids = params[:ach_ids]
    ach_ids = JSON.parse(ach_ids) if ach_ids.class == String && params[:ach_ids].present?
    achs = TangoOrder.ransack(params[:q].try(:merge, m: 'or'))
    if params[:batch_date].present?
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      @achs = achs.result.instant_pay.batch_date(batch_start_date..batch_end_date)
    else
      @achs = achs.result.instant_pay
    end
    return @achs
  end

  def getting_batch_searched_checks(params)
    params[:q] = params[:batch_search_params] if params[:batch_search_params].present?
    result = batch_query_search_params
    p2cs = TangoOrder.instant_pay.includes(:user).ransack(params[:q]).result
    if params[:batch_date].present?
      batch_start_date_int = (params[:batch_date].to_datetime.in_time_zone("Pacific Time (US & Canada)") - 2.hours).utc
      batch_start_date = batch_start_date_int + 24.hours
      batch_end_date = batch_start_date + 24.hours
      @p2cs = p2cs.batch_date(batch_start_date..batch_end_date)
    else
      @p2cs = p2cs.order(id: :desc).per_page_kaminari(params[:page]).per(params[:filter] || 10)
    end
    return @p2cs
  end


  def generate_csv(checks,batch_date)
    CSV.generate do |csv|
      csv << ['Id', 'Creation Date', 'Name on Card', 'Funding Amount', 'Fees', 'Total', 'Status', 'DBA Flag Text']
      # csv << %w{ id check_number send_via user_id name recipient amount creation_date status }
      checks.each do |c|
        # decrypted = AESCrypt.decrypt("#{c.user_id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{c.wallet_id}",  c.account_token)
        # data = JSON(decrypted)
        # account_number = data["account_number"]
        # account_type = data["account_type"]
        fee = c.amount.to_f - c.actual_amount.to_f
        csv << [c.id, c.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y %r"), c.name, number_to_currency(number_with_precision(c.actual_amount.to_f, :precision => 2)), number_to_currency(number_with_precision(fee, :precision => 2)), number_to_currency(number_with_precision(c.amount.to_f, :precision => 2)), c.status, c.wallet.try(:location).try(:ach_flag_text)]
      end
    end
  end

end
