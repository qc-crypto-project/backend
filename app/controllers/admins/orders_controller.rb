class Admins::OrdersController < AdminsController
  def new
    @tracker = Tracker.where(transaction_id: params[:transaction] ).last
    render partial: "admins/transactions/add_tracking"
  end

  def update
    order = Tracker.find(params[:tracker_id])
    begin
      p = Payment::EasyPostGateway.new
      ep_gateway = p.create_tracker(params[:track_cod])

      order.tracker_code = params[:track_cod]
      order.tracker_id = ep_gateway[:id]
      order.status = ep_gateway[:status]
      order.signed_by = ep_gateway[:signed_by]
      order.carrier = ep_gateway[:carrier]
      dates = ep_gateway[:tracking_details].present? ? tracker_dates(JSON.parse(ep_gateway[:tracking_details].to_json)) : nil
      order.shipment_date = dates.first.to_datetime if dates.present? && dates.first.present?
      order.delivered_date = dates.second.to_datetime if dates.present? && dates.second.present?
      order.est_delivery_date = ep_gateway[:est_delivery_date]
      order.tracking_details = ep_gateway[:tracking_details]

      order.save
    rescue Exception => exc
      flash[:error] = exc.message
    end

    return redirect_back(fallback_location: root_path)
  end

  def show
    @tracker = Tracker.where(transaction_id: params[:transaction]).last
    @order_status = @tracker.status
    render partial: "shared/tracking_detail"
  end

  def cancel
    update_tracker_minfraud = false
    #seq_id -- tracnsaction ,merchant reciver_id, nil -- cancel by this ID , logged in user
    transaction = Transaction.find(params[:t_id])
    txn_seq = transaction.seq_transaction_id
    merchant = transaction.receiver
    user = current_user
    begin
      refund = RefundHelper::RefundTransaction.new(txn_seq,transaction,merchant,nil,params[:reason], user)
      #check transaction not blank , amount is already refunded or not and get buy rate fee of merchant location and refund transaction to user
      response = refund.process
    rescue Stripe::InvalidRequestError => exe
      if exe.code == "charge_already_refunded"
        update_tracker_minfraud = true
      end
    end

    if (response.present? && response[:success]) || update_tracker_minfraud
      tracker = Tracker.find(params[:tracker_id])
      tracker.cancel!
      if transaction.minfraud_result.present? && transaction.minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview
        transaction.minfraud_result.update(qc_action: TypesEnumLib::RiskType::Reject)
      end
    end
    redirect_back(fallback_location: root_path)
  end
end