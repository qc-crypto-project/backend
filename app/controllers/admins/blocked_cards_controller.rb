class Admins::BlockedCardsController < ApplicationController
  include Zipline
layout 'admin'
before_action :check_admin
  def cards
    if params[:status] == "true"
      flash[:success] = "All cards successfully uploaded."
    elsif params[:status] == "false"
      flash[:error] = "Some cards were not saved."
    end
    @filters = [10,25,50,100]
    params[:q] = JSON.parse(params[:q]) if params[:q].present? && params[:q].class == String
    if params[:q].present?
      params[:q]["id_eq"] = params[:q]["id_eq"].try(:strip)
      params[:q]["last4_cont"] = params[:q]["last4_cont"].try(:strip)
      params[:q]["first6_eq"] = params[:q]["first6_eq"].try(:strip)
      params[:q]["exp_date_cont"] = params[:q]["exp_date_cont"].try(:strip)
      params[:q]["name_cont"] = params[:q]["name_cont"].try(:strip)
    end
    if params[:card] == "vip"
      @header = "VIP Cards"
      params[:q].present? ? params[:q][:is_vip_eq] = "true" : params[:q] = {"is_vip_eq" => "true"}
    else
      @header = "Blocked Cards"
      params[:q].present? ? params[:q][:is_blocked_eq] = "true" : params[:q] = {"is_blocked_eq" => "true"}
    end
    @q = Card.ransack(params[:q])
    # @all_block = @q.result.pluck(:id)
    # @all_block = @all_block.map { |n| n.to_s }
    @blockedCards = @q.result.order(created_at: :desc).per_page_kaminari(params[:page]).per(params[:tx_filter] || 10) unless params[:card]=='blocked'
    @blockedCards = @q.result.select('DISTINCT on (fingerprint,exp_date) cards.*').per_page_kaminari(params[:page]).per(params[:tx_filter] || 10) if params[:card]=='blocked'
    @all_block = @blockedCards.pluck(:id).map{ |n| n.to_s }
    respond_to do |format|
      format.html
      format.js
    end
  end



  def bulk_cards
    begin
      file = params[:file]
      count = CSV.read(file.path).count
      if count > 50001
        flash[:notice] = "File records must be equal to 50000"
        return redirect_back(fallback_location: root_path)
      end
      s3 = Aws::S3::Resource.new(
             credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],ENV['AWS_SECRET_ACCESS_KEY']),
             region: ENV['AWS_REGION']
          )
      @cards = []
      if params[:card] == "vip"
        type = "vip"
        is_blocked = false
        is_vip = true
        obj = s3.bucket('quickard').object("vip_cards/#{file.original_filename}")
        obj.upload_file("#{file.path}")
      else
        type = "blocked"
        is_blocked = true
        is_vip = false
        obj = s3.bucket('quickard').object("blocked_cards/#{file.original_filename}")
        obj.upload_file("#{file.path}")
      end
      CSV.foreach(file.path, headers: true) do |row|
        if type == "vip"
          headers=row.try(:headers).map{|a|  a.try(:humanize).try(:downcase) }
          first6=''
          last4=''
          if headers.present? && (headers.include?('first6') || headers.include?('last4'))
            first6=row[headers.find_index('first6')] if headers.include?('first6')
            last4=row[headers.find_index('last4')] if headers.include?('last4')
          end
          if !first6.present? || !last4.present?
            ActionCable.server.broadcast "cards_channel_#{session[:session_id]}", channel_name: "cards_channel_#{session[:session_id]}", action: 'circulation' , message: "First6 length is less than 6" if  !first6.present?
            ActionCable.server.broadcast "cards_channel_#{session[:session_id]}", channel_name: "cards_channel_#{session[:session_id]}", action: 'circulation' , message: "last4 length is less than 4" if !last4.present?
            return false
          elsif (!first6.match?(/\A[0-9']*\z/) || !last4.match?(/\A[0-9']*\z/)) || (first6.try(:length) < 6 || last4.try(:length) < 4)
            ActionCable.server.broadcast "cards_channel_#{session[:session_id]}", channel_name: "cards_channel_#{session[:session_id]}", action: 'circulation' , message: "First6 format is not correct" if  !first6.match?(/\A[0-9']*\z/) || first6.try(:length) < 6
            ActionCable.server.broadcast "cards_channel_#{session[:session_id]}", channel_name: "cards_channel_#{session[:session_id]}", action: 'circulation' , message: "Last4 format is not correct" if last4.try(:length) < 4 || !last4.match?(/\A[0-9']*\z/)
            return false
          end
          unless @cards.pluck(:first6).include?(first6) && @cards.pluck(:last4).include?(last4)
            @cards.push({CardholderName: row[headers.find_index('cardholder name')], first6: first6, last4: last4,exp_date: row[headers.find_index('expiry date')], is_blocked: is_blocked, is_vip: is_vip })
          end
        else
          unless @cards.pluck(:card_number).include?(row[0]) && @cards.pluck(:exp_date).include?(row[1])
            @cards.push({card_number:row[0] , exp_date: row[1], is_blocked: is_blocked, is_vip: is_vip })
          end
        end
      end
      BlockedCardsWorker.perform_async(@cards,1,session[:session_id], type)

      # flash[:success] = "All cards successfully uploaded."
    rescue => exc
      flash[:notice] = exc.message
    end
    # redirect_back(fallback_location: root_path)
  end



#########################################


  def import_cards
    if params[:status]&.== "true"
      flash[:success] = "All cards successfully uploaded."
    elsif params[:status]&.== "false"
      flash[:error] = "Some cards were not saved."
    end
    if params[:card]&.=="vip"
      @header = "VIP Cards"
      @cbks = QcFile.vip_cards.order(id: :desc)
    else
      @header = "Blocked Cards"
      @cbks = QcFile.bulk_cards.order(id: :desc)
    end
    params[:status]=nil
    redirect_to  import_cards_to_admins_blocked_cards_path(card:params[:card])
  end

  def import_cards_to
    if params[:card]&.=="vip"
      @header = "VIP Cards"
      @cbks = QcFile.vip_cards.order(id: :desc)
    else
      @header = "Blocked Cards"
      @cbks = QcFile.bulk_cards.order(id: :desc)
    end
    render :'/admins/blocked_cards/import_cards.html.erb'
  end


  #############################################




  def export_download
    if params[:id].present?
      files = []
      ids = JSON(params[:id])
      if ids.present?
        if ids.class == Array
          if ids.count == 1
            foo = QcFile.find(ids.first)
            data = open("https:"+foo.image.url)
            send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
          else
            QcFile.where(id: ids).each do|foo|
              files.push([foo.image, foo.image_file_name, modification_time: 1.day.ago])
            end
            zipline(files, 'export.zip')
          end
        else
          foo = QcFile.find(params[:id])
          data = open("https:"+foo.image.url)
          send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
        end
      end
    end
  end
  def destroy
    block_card = Card.find(params[:id]) if params[:id].present?
    block_card.destroy if block_card.present?
    flash[:notice] = "Card deleted successfully!"
    redirect_back(fallback_location: root_path)
  end

  def destroy_multiple_block_cards
    ids = params[:block_card_arr].split(",")
    if params[:card].present? && params[:card]=='blocked' && ids.present?
      ids.each do |id|
        card=Card.where(id:id).try(:first)
        Card.where(fingerprint:card.fingerprint,exp_date:card.exp_date).update_all(is_blocked:false) if card.present?
      end
    else
      Card.where(:id => ids).destroy_all if ids.present?
    end
    redirect_back(fallback_location: root_path)
  end
  def export_cards
    export_type = "block_vip_export"
    AdminReportWorker.perform_async(nil, nil, export_type, params[:card_type], nil, session[:session_id], current_user.id)
  end
  # # ----------------------- Generate export file end -----------------------#
end
