class Admins::ErrorMessageController < AdminsController
  # Main Error Messages Table
  before_action :update_cache, only: [:index]
  def index
    if params[:query].blank? || ( params[:query][:error_code].blank? && params[:query][:error_message].blank? )
      @error_messages = ErrorMessageCache.list.sort
    else
      @error_messages = ErrorMessage.where(error_code: params[:query][:error_code], error_message: params[:query][:error_message])
    end
  end

  def new
  end

  #Create new Error Message
  def create
    if params[:error].present? && params[:error]["error_code"].present?
      error_messages = ErrorMessage.find_by(error_code: params[:error]["error_code"])
      if error_messages.present?
        flash[:error] = I18n.t('api.errors.used_previously')
      else
        ErrorMessage.create(new_error_params)
        flash[:success] = I18n.t('merchant.controller.message_created_successfully')
      end
    end
    redirect_back(fallback_location: root_path)
  end

  def update_cache
    ErrorMessageCache.refresh_cache
  end

  private

  def new_error_params
    params.require(:error).permit(:error_code ,:error_message, :error_reason, :error_description)
  end

end
