class Admins::MechantBankApisController < AdminsController

  def index
    @merchants = User.where(low_risk: true).order(id: :desc).per_page_kaminari(params[:page]).per(10)
  end

  def new
    @merchant= User.new
    @location = Location.new
    render 'form'
  end

  def boltpay

  end

  def show
    @merchant = User.find_by(id: params[:id])
    if @merchant.low_risk_details.present? && @merchant.low_risk_details["merchant_application_id"] != 0 && @merchant.low_risk_details.try(:[],"application_status").blank? && @merchant.low_risk_details.try(:[],"application_status_label").blank?
      url = "https://merchantapp.io/greenboxtest/api/v1/MerchantApplication/MerchantApplicationDetails?merchantApplicationId=#{@merchant.low_risk_details["merchant_application_id"]}"
      response = RestClient.get(url,{content_type: :json, accept: :json, "X-AuthenticationKeyId": "4a128d57-27bf-4531-873b-24a193318b56", "X-AuthenticationKeyValue": "g0-0jjfR_oEM~msJbv1CBFPxqHmt49M25_c~Gp~G*4^qHWkhm7M1f7A3o.oMhA5s"})
      if response.present?
        response = JSON(response.body)
        low_risk_details = @merchant.low_risk_details
        low_risk_details[:application_status] = response["ApplicationStatus"]
        low_risk_details[:application_status_label] = response["ApplicationStatusLabel"]
        low_risk_details[:under_writing_results] = response["UnderwritingResults"]
        low_risk_details[:boarding_results] = response["BoardingResults"]
        @merchant.update(low_risk_details: low_risk_details)
      end
    end
    respond_to :js
  end

  def edit
    @merchant= User.find_by(id: params[:id])
    @location = Location.find_by(merchant_id: @merchant.id)
    render 'form'
  end

  def update

  end

  def create
    begin
      validate_request_params(
          :legal_name => params[:legal_name],
          :email => params[:email],
          :dba_name => params[:dba_name],
          :tax_id => params[:tax_id],
          :zip => params[:zip],
          :ownershiptype => params[:ownershiptype],
          :businesstype => params[:businesstype],
          :business_phone_number => params[:business_phone_number],
          :business_website => params[:business_website],
          :process_annually => params[:process_annually],
          :contact_title => params[:contact_title],
          :contact_name => params[:contact_name],
          :contact_number => params[:contact_number],
          :contact_email => params[:contact_email],
          :bank_account_dda_type => params[:bank_account_dda_type],
          :bank_account_ach_type => params[:bank_account_ach_type],
          :account_number => params[:account_number],
          :routing_number => params[:routing_number],
          :DateofIncorporation => params[:DateofIncorporation]
      )
      merchant = User.find_by(email: params[:email], low_risk: true)
      raise StandardError.new "Merchant already exist!" if merchant.present? && merchant.low_risk_status == "complete"

      begin
        date = params[:DateofIncorporation].to_date
      rescue
        raise StandardError.new "Invalid Date. Please use dd/mm/yyyy format"
      end
      tax_id = params[:tax_id].gsub(/[^0-9]/, '')
      raise StandardError.new "Tax ID must be 9 digit long." if tax_id.size != 9

      zip = params[:zip].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect zip" if zip.size < 5

      valid_email = EmailValidator.valid?(params[:email])
      raise StandardError.new("Invalid Email Format") unless valid_email

      business_website = URI.parse(params[:business_website])
      raise StandardError.new("Invalid Email Format") unless business_website.kind_of?(URI::HTTP || URI::HTTPS)

      business_phone_number = params[:business_phone_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect business phone number" if business_phone_number.size != 10

      contact_number = params[:contact_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect contact number" if contact_number.size != 10

      contact_email = URI.parse(params[:contact_email])
      raise StandardError.new("Invalid Email Format") unless contact_email

      routing_number = params[:routing_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect routing number" if routing_number.size != 9

      account_number = params[:account_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect account number" if account_number.size > 17

      gateway = PaymentGateway.ficer.first
      if merchant.blank?
        merchant = User.create(name: params[:legal_name], email: params[:email], phone_number: params[:business_phone_number], zip_code: params[:zip], owner_name: params[:ownershiptype], password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019',role: "merchant", low_risk: true, country: params[:country], street: params[:street1], city: params[:city], state: params[:state],low_risk_status: "pending")
        if merchant.present?
          location = Location.create( business_name: params[:dba_name], first_name: params[:contact_title], last_name: params[:contact_name],
                                      email: params[:contact_email], web_site: [params[:business_website]], trip_business_type: params[:businesstype],
                                      years_in_business: params[:DateofIncorporation], city: params[:contact_city], state: params[:contact_state],
                                      zip: params[:contact_zip], tax_id: params[:tax_id], phone_number: params[:contact_number],
                                      cs_number: params[:contact_number], merchant_id: merchant.id, monthly_processing_volume: params[:process_annually],
                                      dda_type: params[:bank_account_dda_type], bank_account: params[:account_number],
                                      bank_routing: params[:routing_number], ach_type: params[:bank_account_ach_type],
                                      primary_gateway_id: gateway.id
          )
          location.fees << gateway.fee
        end
      else
        merchant.update(name: params[:legal_name], phone_number: params[:business_phone_number], zip_code: params[:zip], owner_name: params[:ownershiptype], country: params[:country], street: params[:street1], city: params[:city], state: params[:state],low_risk_status: "pending")
        location = Location.find_by(merchant_id: merchant.id)
        if location.present?
          location.update( business_name: params[:dba_name], first_name: params[:contact_title], last_name: params[:contact_name],
                           email: params[:contact_email], web_site: [params[:business_website]], trip_business_type: params[:businesstype],
                           years_in_business: params[:DateofIncorporation], city: params[:contact_city], state: params[:contact_state],
                           zip: params[:contact_zip], tax_id: params[:tax_id], phone_number: params[:contact_number],
                           cs_number: params[:contact_number], monthly_processing_volume: params[:process_annually],
                           dda_type: params[:bank_account_dda_type], bank_account: params[:account_number], bank_routing: params[:routing_number],
                           ach_type: params[:bank_account_ach_type]
          )
        end
      end

      customFields = []

      customFields.push({"Id": 7788, "UserDefinedId": "legal.name", "Value": {"name": "#{params[:legal_name]}"}},
                        {"Id": 7789, "UserDefinedId": "legal.dba", "Value": {"dba": "#{params[:dba_name]}"}},
                        {"Id": 7790, "UserDefinedId": "legal.address", "Value": {"Country": "#{params[:country]}", "Street1": "#{params[:street1]}", "Street2": "#{params[:street2]}", "City": "#{params[:city]}", "State": "#{params[:state]}", "Zip": "#{params[:zip]}"}},
                        {"Id": 7791, "UserDefinedId": "legal.ownershiptype", "Value": {"ownershiptype": "#{params[:ownershiptype]}"}},
                        {"Id": 7792, "UserDefinedId": "legal.taxid", "Value": {"taxid": params[:tax_id]}},
                        {"Id": 7793, "UserDefinedId": "Legal.DateofIncorporation", "Value": {"Month": "#{date.month}", "Day": "#{date.day}", "Year": "#{date.year}"}},
                        {"Id": 7794, "UserDefinedId": "legal.phone", "Value": {"business_phone_number": "#{params[:business_phone_number]}"}},
                        {"Id": 7795, "UserDefinedId": "legal.email", "Value": {"business_email": "#{params[:email]}"}},
                        {"Id": 7796, "UserDefinedId": "legal.website", "Value": {"business_website": "#{params[:business_website]}"}},
                        {"Id": 7797, "UserDefinedId": "bank.routingnumber", "Value": {"routing_number": "#{params[:routing_number]}"}},
                        {"Id": 7798, "UserDefinedId": "bank.routingnumber.confirm", "Value": {"confirm_routing_number": "#{params[:routing_number]}"}},
                        {"Id": 7799, "UserDefinedId": "bank.acctnumber", "Value": {"account_number": "#{params[:account_number]}"}},
                        {"Id": 7800, "UserDefinedId": "bank.acctnumber.confirm", "Value": {"confirm_account_number": "#{params[:account_number]}"}},
                        {"Id": 7827, "UserDefinedId": "pc.name", "Value": {"FirstName": "#{params[:contact_name]}", "MiddleName": "#{params[:contact_middle_name]}", "LastName": "#{params[:contact_last_name]}"}},
                        {"Id": 7828, "UserDefinedId": "pc.title", "Value": {"title": "#{params[:contact_title]}"}},
                        {"Id": 7829, "UserDefinedId": "pc.address", "Value": {"Country": "#{params[:contact_country]}", "Street1": "#{params[:contact_street1]}", "Street2": "#{params[:contact_street2]}", "City": "#{params[:contact_city]}", "State": "#{params[:contact_state]}", "Zip": "#{params[:contact_zip]}"}},
                        {"Id": 7830, "UserDefinedId": "pc.email", "Value": {"contact_email": "#{params[:contact_email]}"}},
                        {"Id": 7831, "UserDefinedId": "pc.phone", "Value": {"contact_number": "#{params[:contact_number]}"}},
                        {"Id": 7832, "UserDefinedId": "bank.ach", "Value": {"ach": "#{params[:bank_account_ach_type]}"}},
                        {"Id": 7833, "UserDefinedId": "legal.annualprocessing", "Value": {"process_annually": "#{params[:process_annually]}"}},
                        {"Id": 7864, "UserDefinedId": "legal.businesstype", "Value": {"businesstype": "#{params[:businesstype]}"}},
                        {"Id": 7865, "UserDefinedId": "bank.dda", "Value": {"dda": "#{params[:bank_account_dda_type]}"}}
                        )

      low_risk_details = merchant.low_risk_details.present? ? merchant.low_risk_details : {}

      data = {"AuthenticationKeyId": "4a128d57-27bf-4531-873b-24a193318b56",
              "AuthenticationKeyValue": "g0-0jjfR_oEM~msJbv1CBFPxqHmt49M25_c~Gp~G*4^qHWkhm7M1f7A3o.oMhA5s",
              "Merchant_EmailAddress": params[:email],
              "CustomFieldAnswers": customFields}

      url = "https://merchantapp.io/greenboxtest/api/v1/MerchantApplication/Submit"
      response = RestClient.post(url,data.to_json,{content_type: :json, accept: :json})
      if response.present?
        response = JSON(response.body)

        low_risk_details[:status] = response["Status"]
        low_risk_details[:status_message] = response["StatusMessage"]
        low_risk_details[:merchant_application_id] = response.try(:[],"MerchantApplicationId")
        low_risk_details[:external_merchant_application_id] = response.try(:[],"ExternalMerchantApplicationId")
        low_risk_details[:infinicept_application_id] = response.try(:[],"InfiniceptApplicationId")
        if response["Errors"].present?
          message = response["Errors"].pluck("Message").join(' ')
          low_risk_details[:error_message] = message
          raise StandardError.new "#{message}"
        else
          merchant.low_risk_status = "complete"
        end
      end
      flash[:merchant_bank] = "Submit Successfully. MerchantApplicationId: #{response["MerchantApplicationId"]}, InfiniceptApplicationId: #{response.try(:[],"InfiniceptApplicationId")}"
    rescue StandardError => exc
      flash[:merchant_bank] = exc.message
    ensure
      if merchant.present?
        merchant.low_risk_details = low_risk_details
        merchant.save
      end
      redirect_to admins_mechant_bank_apis_index_path
    end
  end


  private
  def validate_request_params(args)
    args.each do |name, value|
      if value.blank?
        raise StandardError.new "Missing required parameter: #{name}"
      end
    end
  end
end
