class Admins::SlotGatewaysController < AdminsController
  include ApplicationHelper
  layout 'admin'
  before_action :set_admins_payment_gateway, only: [:show, :edit, :update, :destroy]
  before_action :check_admin

  def index
    @admins_payment_gateways = PaymentGateway.active_gateways.slot.order(:id)
    @payment_gateway = PaymentGateway.new
  end

  def show
  end

  def edit
    @payment_gateway = PaymentGateway.find_by(id: params[:id])
    respond_to :js
  end

  def validate_slot
    slot = PaymentGateway.find_by(slot_name: params[:slot_name])
    if slot.present?
      render status: 404, json:{error: 'Already exist'}
    else
      render status: 200, json:{success: 'Ready to go'}
    end
  end

  def reset_slot_setting
    begin
      if params[:key].present? && params[:key] == "all" && params[:attempt].present? && params[:attempt] == "all"
        flash[:notice] = "Reset Successfully!" if reset_all
      elsif params[:key].present? && params[:key] != "all" && params[:attempt].present? && params[:attempt] != "all"
        flash[:notice] = "Reset Successfully!" if reset_single(params[:key],params[:attempt],params[:location_id])
      else
        flash[:notice] = "Something went wrong. Please try again!"
      end
    rescue => e
      flash[:notice] = "#{e.message}"
    end
    redirect_back(fallback_location: root_path)
  end

  def reset_single(key,attempt,location_id=nil)
    if location_id.blank?
      slot = AppConfig.where(key: key).try(:first)
      if slot.present?
        slot_detail = slot.try(:slot_detail)
        slot_detail = slot_detail.each do|k,v|
          v["count#{attempt}"] = "0"
          v["total_count#{attempt}"] = "0"
        end
        slot.update(slot_detail: slot_detail)
        true
      else
        false
      end
    else
      location = Location.find_by(id: params[:location_id])
      if location.present?
        slot = location.slot
        if slot.present?
          slot_detail = slot.try(key)
          slot_detail = slot_detail.each do|k,v|
            v["count#{attempt}"] = "0"
            v["total_count#{attempt}"] = "0"
          end
          slot.update("#{key}": slot_detail)
          true
        else
          false
        end
      end
    end
  end

  def reset_all
    status = false
    slot_over = AppConfig.where(key: AppConfig.keys["slot_over"]).try(:first)
    if slot_over.present?
      slot_detail_over = slot_over.try(:slot_detail)
      slot_detail_over = slot_detail_over.each do|k,v|
        v["count1"] = "0"
        v["total_count1"] = "0"
        v["count2"] = "0"
        v["total_count2"] = "0"
      end
      slot_over.update(slot_detail: slot_detail_over)
      status = true
    end
    if status
      slot_under = AppConfig.where(key: AppConfig.keys["slot_under"]).try(:first)
      if slot_under.present?
        slot_detail_under = slot_under.try(:slot_detail)
        slot_detail_under = slot_detail_under.each do|k,v|
          v["count1"] = "0"
          v["total_count1"] = "0"
          v["count2"] = "0"
          v["total_count2"] = "0"
        end
        slot_under.update(slot_detail: slot_detail_under)
        status = true
      end
    end
    status
  end

  def create
    begin
      if params["payment_gateway"]["slot_name"].present? && params["payment_gateway"]["slot_name"].try(:downcase).first(4) != "slot"
        flash[:error] = "Invalid Slot name!"
        return redirect_back(fallback_location: root_path)
      end
      params[:payment_gateway].each { |key, value| value.strip! unless value.blank? && value.is_a?(Hash) }
      @admins_payment_gateway = PaymentGateway.new(admins_payment_gateway_params)
      @admins_payment_gateway.card_selection = setting_for_card(params["card_selection"],PaymentGateway.card_types)
      @admins_payment_gateway.type = "slot"
      respond_to do |format|
        if @admins_payment_gateway.save
          flash[:notice] = "New Payment Gateway Successfully Created!"
        else
          flash[:notice] = "Something Went Wrong While Creating New Payment Gateway!"
        end
        format.html { redirect_to admins_slot_gateways_path}
      end
    rescue ActiveRecord::RecordInvalid => invalid
      flash[:error] = invalid
      redirect_back(fallback_location: root_path)
    end
  end

  def update
    params[:payment_gateway].each { |key, value| value.strip! unless value.blank? && value.is_a?(Array) }
    params[:payment_gateway][:client_secret] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_secret]) if params[:payment_gateway][:client_secret].present?
    params[:payment_gateway][:client_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_id]) if params[:payment_gateway][:client_id].present?
    params[:payment_gateway][:authentication_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:authentication_id]) if params[:payment_gateway][:authentication_id].present?
    params[:payment_gateway][:signature_maker] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:signature_maker]) if params[:payment_gateway][:signature_maker].present?
    params[:payment_gateway][:processor_id] = AESCrypt.encrypt(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:processor_id]) if params[:payment_gateway][:processor_id].present?

    # params[:payment_gateway][:client_secret] = AESCrypt.encrypt_ee(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_secret]) if params[:payment_gateway][:client_secret].present?
    # params[:payment_gateway][:client_id] = AESCrypt.encrypt_ee(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:client_id]) if params[:payment_gateway][:client_id].present?
    # params[:payment_gateway][:authentication_id] = AESCrypt.encrypt_ee(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:authentication_id]) if params[:payment_gateway][:authentication_id].present?
    # params[:payment_gateway][:signature_maker] = AESCrypt.encrypt_ee(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:signature_maker]) if params[:payment_gateway][:signature_maker].present?
    # params[:payment_gateway][:processor_id] = AESCrypt.encrypt_ee(ENV['SECRET_KEY_GATEWAY'], params[:payment_gateway][:processor_id]) if params[:payment_gateway][:processor_id].present?
    if params[:payment_gateway][:allow_decimal].blank?
      params[:payment_gateway][:allow_decimal] = false
    end

    respond_to do |format|
      if @admins_payment_gateway.update(admins_payment_gateway_params)
        @admins_payment_gateway.update(card_selection: setting_for_card(params["card_selection"],PaymentGateway.card_types))
        flash[:notice] = "Payment Gateway Successfully Updated!"
      else
        flash[:notice] = "Something Went Wrong While Updating New Payment Gateway!"
      end
      format.html { redirect_back(fallback_location: root_path)}
    end
  end

  def setting_for_card(params,actual)
    temp_hash = {}
    actual.each do|ak,av|
      if params.present? && params[ak].present?
        temp_hash.merge!({ak=>"on"})
      else
        temp_hash.merge!({ak=>"off"})
      end
    end
    return temp_hash
  end

  def archive
    @payment_gateway = PaymentGateway.find_by(id: params[:id])
    if @payment_gateway.present? && @payment_gateway.is_deleted?
      @payment_gateway.update(is_deleted: false, is_block: false)
      flash[:notice] = "Gateway Unarchived Successfully!"
    elsif @payment_gateway.is_deleted == false
      @payment_gateway.update(is_deleted: true, is_block: true)
      flash[:notice] = "Gateway Archived Successfully!"
    else
      flash[:notice] = "Error.!"
    end
    redirect_to admins_slot_gateways_path
  end

  def postverticaltype
    @vertical_type = VerticalType.create(title: params[:title])
    if params["pay_id"].present? && params["pay_id"] != "0"
      gate =PaymentGateway.find_by(id: params["pay_id"].to_i).update(vertical_type_id: @vertical_type.id)
    end
    if @vertical_type.present?
      render json:{id:@vertical_type.id,title:@vertical_type.title}
    else
      render json:{message:"Fill Up Your Fields!"}
    end
  end

  def block_gateways
    @payment_gateway = PaymentGateway.find_by(id: params[:id])
    if @payment_gateway.is_block?
      @payment_gateway.update(is_block: false)
      flash[:notice] = "Gateway Unblocked Successfully!"
    else
      @payment_gateway.update(is_block: true)
      flash[:notice] = "Gateway Blocked Successfully!"
    end
    redirect_to admins_slot_gateways_path
  end

  def archived_slot_gateways
    @payment_gateway = PaymentGateway.where(is_deleted: true).slot
  end
  
  def slot_management
    @payment_gateway = PaymentGateway.active_gateways.slot
    slots = AppConfig.where(key: [AppConfig::Key::SlotOver,AppConfig::Key::SlotUnder])
    @slot_over = slots.try(:slot_over).try(:first).try(:slot_detail)
    @slot_amount = slots.try(:first).try(:stringValue)
    @slot_under = slots.try(:slot_under).try(:first).try(:slot_detail)
  end

  def post_slot_management
    slot_over_detail = {}
    slot_under_detail = {}
    slot_amount = 0
    if params[:query].present? && params[:query][:over].present? && params[:query][:over].values.present?
      params[:query][:over].values.each do |over|
        slot_over_detail.merge!(over)
      end
    end
    if params[:query].present? && params[:query][:under].present? && params[:query][:under].values.present?
      params[:query][:under].values.each do |under|
        slot_under_detail.merge!(under)
      end
    end
    slot_amount = params[:query].try(:[],"amount")
    if slot_over_detail.blank? || slot_under_detail.blank?
      flash[:error] = "Invalid Slots Data!"
    else
      slot_over = AppConfig.where(key: AppConfig::Key::SlotOver).try(:first)
      if slot_over.present?
        slot_over.update(stringValue: slot_amount,slot_detail: slot_over_detail)
      else
        AppConfig.create(key: AppConfig::Key::SlotOver,stringValue: slot_amount, boolValue: true,slot_detail: slot_over_detail)
      end
      slot_under = AppConfig.where(key: AppConfig::Key::SlotUnder).try(:first)
      if slot_under.present?
        slot_under.update(stringValue: slot_amount,slot_detail: slot_under_detail)
      else
        AppConfig.create(key: AppConfig::Key::SlotUnder,stringValue: slot_amount, boolValue: true,slot_detail: slot_under_detail)
      end
      flash[:notice] = "Updated Successfully!"
    end
    redirect_back(fallback_location: root_path)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admins_payment_gateway
    @admins_payment_gateway = PaymentGateway.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admins_payment_gateway_params
    params.require(:payment_gateway).permit(:type, :name, :client_secret, :client_id,:authentication_id,:signature_maker,:key,:slot_name,
                                            :transaction_fee,:charge_back_fee,:retrieval_fee,
                                            :per_transaction_fee,:reserve_money,:reserve_money_days,
                                            :monthly_service_fee,:account_processing_limit, :misc_fee, :misc_fee_dollars, :vertical_type_id, :processor_id,:allow_decimal)
  end
end