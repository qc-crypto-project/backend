class ImagesController < ApplicationController
  layout 'admin'

  def delete_image
    if params[:id].present?
        ids = params[:id].split(',').map(&:to_i)
        # @images = Image.where(id: ids)
        ids.each do |n|
          @image = Image.find_by(id:n.to_i)
          @image.destroy
        end
        @image.destroy
    end
  end
  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
    if params[:id].present?
      ids=params[:id].tr('[]', '').split(',').map(&:to_i)
      ids.each do |n|
        @image = Image.find_by(id:n.to_i)
        @image.destroy
      end
    end
  end


  def upload_docs
    @old_doc= Image.where(location_id: params[:image].try(:[], :location_id)).last if params[:image].try(:[], :location_id).present?
    @old_doc.delete if @old_doc.present?
    @doc= Image.new(doc_params)
    if @doc.validate
      @doc.save
    else
      @error = @doc.errors.full_messages.first
    end
    respond_to do |f|
      f.js{}
      f.json{render status: 200 , json: {success: true , doc: @doc,image_url:@doc.add_image.url}}
      f.html{}
    end
  end

  def cancel_upload_doc
    image = Image.find params[:document_id]
    image.delete
    render status: 200 , json:{success: true , message: 'Deleted successfully'}
  end

  private

  def image_params
    params.require(:image).permit(:add_image,:add_id)
  end

  def doc_params
    params.require(:image).permit(:add_image,:usage_id,:location_id)
  end
end
