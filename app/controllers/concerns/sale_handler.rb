module SaleHandler
  extend ActiveSupport::Concern
  include TransactionCharge::ChargeATransactionHelper
  include ActivityLogHandler

  class DuplicationError < StandardError; end

  included do
    # Define custom handlers

    rescue_from StandardError do |exc|
      response_message = "#{exc.message}"
      record_it(7 , response_message)
      ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'failed', message: response_message
    end

    rescue_from JWT::DecodeError do |e|
      if params[:action] == "payment"
        render_json_response({success: true, message: "Link has expired" },:ok)
      else
        flash[:error] = "Your Session has expired"
        redirect_to root_path
      end
    end

    rescue_from DuplicationError do |exc|
      puts "==============="
      puts exc
      puts "==============="
    end

    rescue_from Exception do |exc|
      if params[:duplicate_tx].present?
      else
        issue_raw_transaction = IssueRawTransaction.new(@card_customer.try(:email),params)
        if params[:converge_txn_id].present?
          elavon = Payment::ElavonGateway.new
          elavon.void(params[:converge_txn_id],@payment_gateway)
        end
        make_i_can_preauth_void(params[:i_can_pay_txn_id], request, params[:i_can_pay_txn_amount],@payment_gateway) if params[:i_can_pay_txn_id].present?
        make_bolt_pay_void(params[:bolt_pay_txn_id], nil,@payment_gateway) if params[:bolt_pay_txn_id].present?
        saving_decline_for_seq(issue_raw_transaction,@payment_gateway)if issue_raw_transaction.present?
        response_message = "#{exc.message}"
        record_it(7 , response_message)
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'failed', message: response_message
      end
    end

    rescue_from Stripe::CardError, Stripe::InvalidRequestError do |e|
      response_message = "#{e.message}"
      record_it(7 , response_message)
      ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'failed', message: response_message
    end

    rescue_from Timeout::Error  do |e|
      response_message = I18n.t 'merchant.controller.req_timedout'
      record_it(7 , response_message)
      ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'failed', message: response_message
    end


  end

  private

  def saving_decline_for_seq(issue_raw_transaction,payment_gateway = nil)
    if params[:both]
      location_fee = issue_raw_transaction.get_location_fees(params[:merchant_wallet_id],@user,params[:amount],nil,TypesEnumLib::TransactionType::CreditTransaction)
      #= building hash for Decline
      transaction_info = {amount: params[:amount].to_f}
      reserve_detail = location_fee.first if location_fee.present?
      user_info = location_fee.second if location_fee.present?
      fee = location_fee.third if location_fee.present?
      transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
      #= end building process
      OrderBank.create(merchant_id: params[:merchant_wallet_id],user_id: params[:wallet_id],bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id))
    elsif params[:order_bank].present?
      order_bank = params[:order_bank]
      OrderBank.create(merchant_id: order_bank.merchant_id,user_id: order_bank.user_id,bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,card_type: nil,card_sub_type:nil,status: "decline",amount: order_bank.amount,transaction_info: order_bank.transaction_info,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id))
    end
  end

  def make_elavon_payment_void(transaction_id, payment_gateway = nil)
    if transaction_id.present? #Void from Converge in case of seq failure
      elavon = Payment::ElavonGateway.new
      elavon.void(transaction_id,payment_gateway)
    end
  end

  def make_i_can_preauth_void(transaction_id, request, amount = nil, payment_gateway = nil)
    if transaction_id.present? #Void from ICanPay in case of seq failure
      i_can_pay = Payment::ICanPay.new(payment_gateway)
      i_can_pay.refund(transaction_id,amount, request)
    end
  end

  def make_bolt_pay_void(transaction_id, amount = nil, payment_gateway = nil)
    if transaction_id.present?
      bolt_pay = Payment::BoltPayGateway.new
      bolt_pay.refund(transaction_id, amount, payment_gateway)
    end
  end

end