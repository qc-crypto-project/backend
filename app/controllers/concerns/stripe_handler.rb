module StripeHandler
  extend ActiveSupport::Concern

  included do
    # Define custom handlers
    rescue_from Stripe::CardError, Stripe::InvalidRequestError do |e|
      # OrderBank.create(user_id: @user.id, bank_type: "stripe",card_type: nil,card_sub_type:nil,status: "decline",amount: amount )
      log(e)
      p "Stripe::CardError, Stripe::InvalidRequestError", e
      flash[:error] = "Stripe card payment declined with a decline code!"
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def log(exc)
    back_trace = exc.backtrace.select { |x| x.match(Rails.application.class.parent_name.downcase)}.first
    error = {Exception: exc.to_s ,URL: request.url.to_s, PARAMS: request.params.to_s ,BACKTRACE: back_trace.to_s}.to_s
    return if request.host == 'localhost'
    # SlackService.notify(msg)
  end
end