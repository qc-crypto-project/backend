module CheckoutHandler
  extend ActiveSupport::Concern

  class TransactionError < StandardError; end
  class DuplicationError < StandardError; end
  class DuplicateTransactionError < StandardError; end
  class ActivityLogSuccess < StandardError; end
  class ActivityLogFailure < StandardError; end

  included do
    # Define custom handlers

    rescue_from StandardError do |exc|
      if params[:action]  == "payment"
        issue_raw_transaction =TransactionCharge::ChargeATransactionHelper::IssueRawTransaction.new(@user.try(:email),params)
        saving_decline_for_seq(issue_raw_transaction,@payment_gateway, @transaction)
        ActivityLog.log(7, "#{exc.message}", @merchant)
        if @request_id.present?
          ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{exc.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
        else
          ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{exc.message}",url: @fail_url
        end
      elsif params[:action]  == "rtp_payment"
        ActivityLog.log("rtp", "#{exc.message}",@merchant, params,response,nil,nil,"failed", @activity)
        @error = exc.message
        @url = params["fail_url"]
        render template: 'checkout/checkout/rtp_payment'
      elsif params[:action]  == "rtp_payment_hook"
        ActivityLog.log(@hook ? "rtp_hook" :"rtp", "#{exc.message}",@merchant, params,response,nil,nil,"failed", @activity) if exc.message != "No pending Transaction found!"
      else
        flash[:error] = exc.message
        redirect_to new_user_session_path
      end
    end

    rescue_from DuplicationError do |exc|
      puts "==============="
      puts exc
      puts "==============="
    end

    rescue_from DuplicateTransactionError do |exc|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{exc.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{exc.message}",url: @fail_url
      end
    end

    rescue_from ActivityLogSuccess do |e|
      ActivityLog.log(4,"#{e.message}",@merchant)
    end

    rescue_from ActivityLogFailure do |e|
      ActivityLog.log(7,"#{e.message}",@merchant)
    end

    rescue_from TransactionError do |e|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{e.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{e.message}",url: @fail_url
      end
    end

    rescue_from JWT::DecodeError do |e|
      if params[:action] == "payment"
        render_json_response({success: true, message: "Link has expired" },:ok)
      else
        flash[:error] = "Your Session has expired"
        redirect_to new_user_session_path
      end

    end

    rescue_from Stripe::CardError, Stripe::InvalidRequestError do |e|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{e.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{e.message}",url: @fail_url
      end
    end

    rescue_from Timeout::Error do |e|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{I18n.t 'merchant.controller.req_timedout'}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{I18n.t 'merchant.controller.req_timedout'}",url: @fail_url
      end
    end

  end

end