module ApiHandler
  extend ActiveSupport::Concern


  included do

    rescue_from StandardError do |exc|
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end

    rescue_from Stripe::CardError, Stripe::InvalidRequestError do |e|
      return render_json_response({:success => false, :message => e.message}, :ok)
    end

    rescue_from Timeout::Error do |e|
      return render_json_response({:success => false, :message => e.message}, :ok)
    end


  end

end