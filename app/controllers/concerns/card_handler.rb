module CardHandler
  extend ActiveSupport::Concern

  def check_bin_list(current_user,merchant_id,user_id)
    card_name = params[:card_holder_name]
    @card_info = {
        number: params[:card_number],
        month: params[:card_exp_date].first(2),
        first_name: params[:card_holder_name] || card_name,
        year: "20#{params[:card_exp_date].last(2)}",
        exp_date: "#{params[:card_exp_date].first(2)}/#{params[:card_exp_date].last(2)}"
    }
    card1 = Payment::QcCard.new(@card_info,nil,user_id)
    #get card by fingerprint
    card = current_user.cards.cards_with_fingerprint_merchant_id(card1.fingerprint, merchant_id,@card_info[:exp_date]).first
    #if card not exist than cvv parameter is required otherwise optional
    if card.blank?
      card = current_user.cards.new(exp_date: "#{@card_info[:month]}/#{@card_info[:year].last(2)}", last4: params[:card_number].last(4),first6: params[:card_number].first(6).to_i,merchant_id: merchant_id)
    end
    card.check_bin
    card.qc_token = card1.qc_token
    card.fingerprint = card1.fingerprint
    card.exp_date = "#{@card_info[:month]}/#{@card_info[:year].last(2)}"
    card.name = params[:card_holder_name]
    card.brand = card1.card_brand if card.brand.blank?
    card.save
    card_bin_list={
        'number'=>{'length'=>params[:card_number].try(:length)},
        'scheme'=>card.try(:brand),
        'type'=>card.try(:card_type),
        'bank'=>{'name'=>card.try(:bank)},
        'country'=>card.try(:country),
        'first6'=>params[:card_number].to_s.first(6).to_s,
        'last4'=>params[:card_number].to_s.last(4).to_s
    }
    return {card: card,card_bin_list:card_bin_list}
  end


  private


end