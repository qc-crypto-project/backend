module ProfileHandler
  extend ActiveSupport::Concern

  private

  def send_pin(type = nil, reason = nil)
    if params[:send_with].present?
      if params[:send_with]=='send_mobile' || params[:send_with]=='first_send'
        @random_code = rand(1_00000..9_99999)
        current_user.pin_code = @random_code
        current_user.save
        TextsmsWorker.perform_async(current_user.phone_number, "Your Two-Step Authentication Code for QuickCard: #{@random_code}")
        # else
        UserMailer.send_pincode(current_user).deliver_now
      end
      render status: 200, json:{data: 'correct pin code'}
    end
    if params[:pincode].present?
      if params[:pincode] == current_user.pin_code
        render json:{"success":"true"}, status: 200
      else
        render json:{"success":"false"}, status: 404
      end
    end
    if params[:password].present?
      if current_user.update(password: params[:password])
        sign_in(current_user, :bypass => true)
        flash[:success] = 'Password updated successfully!'
      else
        flash[:error] = current_user.errors.full_messages.first
      end
      if current_user.merchant?
        redirect_to v2_merchant_profiles_path
      else
        redirect_to v2_partner_profiles_path
      end
    end
  end
end