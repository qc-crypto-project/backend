module WebExceptionHandler
  extend ActiveSupport::Concern

  class AuthorizationError < StandardError; end

  included do
    # Define custom handlers
    # rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
    # rescue_from ArgumentError, with: :four_zero_zero
    rescue_from AuthorizationError, with: :unauthorized_request

    # rescue_from ActiveRecord::RecordNotFound do |e|
    #   flash[:error] = "Invalid Parameters! Bad Request!"
    #   redirect_to '/'
    # end

    # rescue_from ActiveRecord::RecordInvalid do |e|
    #   flash[:error] = "Invalid Parameters! Bad Request!"
    #   redirect_to '/'
    # end

    # rescue_from StandardError do |e|
    #   flash[:error] = "Invalid Parameters! Bad Request!"
    #   redirect_to '/'
    # end
  end

  private

  # Response with message; Status code 400 - Bad Request
  def four_zero_zero(e)
    error = e.message
    respond_with_msg error, 400
  end

  # Response with message; Status code 422 - unprocessable entity
  def four_twenty_two(e)
    error = "Invalid Parameters! Bad Request!"
    respond_with_msg error, 422
  end

  # Response with message; Status code 498 - Unauthorized
  def four_ninety_eight(e)
    error = "Invalid Parameters! Bad Request!"
    respond_with_msg error, 498
  end

  # Response with message; Status code 401 - Unauthorized
  def four_zero_one(e)
    error = "Invalid Parameters! Bad Request!"
    respond_with_msg error, 401
  end

  # Response with message; Status code 400 - Unauthorized
  def unauthorized_request(e)
    error = "You are not Authorized for this request! Contact App Administrator for more information!"
    respond_with_msg error, 400
  end

  def respond_with_msg msg, code
    if request.format.html?
      flash[:error] = msg
      redirect_to '/'
    else
      render :json => { error: msg, status: code}
    end
  end
end
