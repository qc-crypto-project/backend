module InvoiceHandler
  extend ActiveSupport::Concern
  include ActivityLogHandler
  included do
    # Define custom handlers

    rescue_from StandardError do |exc|
      flash[:error] = exc.message
      if params[:redirect_url].present?
        redirect_to params[:redirect_url]
      else
        message = I18n.t('api.registrations.account_limit_true').split('%')
        message = message[0] if message.present?
        error_message = exc.message.split('[') if exc.message.present?
        error_message = error_message[0] if error_message.present?
        if error_message.present? && message.present? && error_message == message
          response_message = "#{exc.message}"
          record_it(7 , response_message)
        end
        redirect_back(fallback_location: root_path)
      end
    end

    rescue_from JWT::DecodeError do |e|
      flash[:error] = "Your Session has expired"
      redirect_to root_path
    end
  end

end