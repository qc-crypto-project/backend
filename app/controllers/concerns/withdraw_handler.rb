module WithdrawHandler
  extend ActiveSupport::Concern

  private

  def handle_withdraw_exception msg, channel, formatted_msg = false
    channel = ENV["STACK"] == I18n.t("environments.live") && channel == I18n.t('slack.channels.withdraw_alert_channel') ? I18n.t('slack.channels.withdraw_alert_channel') : I18n.t('slack.channels.quickcard_exception')
    params.try(:except, :card_number, :cvv, :expiry_date)
    msg = formatted_msg ? msg : SlackService.create_message(params, msg, current_user, "CheckBook Error")
    SlackService.notify(msg, channel)
  end
end
