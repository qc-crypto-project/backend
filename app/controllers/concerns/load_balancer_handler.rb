module LoadBalancerHandler
  extend ActiveSupport::Concern
  include LoadBalancerHelper
  include TransactionCharge::ChargeATransactionHelper

  class TransactionError < StandardError; end
  class DuplicationError < StandardError; end
  class DuplicateTransactionError < StandardError; end
  class ActivityLogSuccess < StandardError; end
  class ActivityLogFailure < StandardError; end

  included do

    rescue_from StandardError do |exc|
      if params[:action] == "payment"
        issue_raw_transaction =TransactionCharge::ChargeATransactionHelper::IssueRawTransaction.new(@user.try(:email),params)
        saving_decline_for_seq(issue_raw_transaction,@payment_gateway, @transaction)
        ActivityLog.log("vt_error", "#{exc.message}", @merchant)
        if @request_id.present?
          ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{exc.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
        else
          ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{exc.message}",url: @fail_url
        end

      elsif ["virtual_terminal_transaction", "virtual_terminal_transaction_debit","virtual_transaction","virtual_transaction_card"].include?(params[:action])
        return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: @error_code.try(:humanize), :message => exc.message},:ok)

      elsif params[:action] == "submit_virtual_terminal"
        if params[:duplicate_tx].blank?
          issue_raw_transaction = IssueRawTransaction.new(@card_customer.try(:email),params)
          saving_decline_for_seq(issue_raw_transaction,@payment_gateway)if issue_raw_transaction.present?
          ActivityLog.log("vt_error", "#{exc.message}", current_user, params)
          ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'failed', message: "#{exc.message}"
        end
      elsif params[:action]  == "rtp_payment"
        SlackService.notify("RTP: #{exc.message}")
        ActivityLog.log("rtp", "#{exc.message}",@merchant, params,response,nil,nil,"failed", @activity)
        @error = exc.message
        @url = params["fail_url"]
        if params[:item_code].blank?
          params["fail_url"] = "#{params["fail_url"]}?success=false&status=#{TypesEnumLib::Statuses::Failed}&message=#{exc.message}&quickcard_id=#{@transaction.quickcard_id}&amount=#{params[:amount]}&date=#{@transaction.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)")}"
          @url = params["fail_url"]
          @transaction.rtp_webhook({status: false, success: TypesEnumLib::Statuses::Failed, message: "#{exc.message}", quickcard_id: @transaction.quickcard_id, amount: params[:amount], date: @transaction.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)")}) if @transaction.present?
        end
        render template: 'checkout/checkout/rtp_payment'
      elsif params[:action]  == "rtp_payment_hook"
        if params[:item_code].blank?
          @transaction.rtp_webhook({status: false, success: TypesEnumLib::Statuses::Failed, message: "#{exc.message}", quickcard_id: @transaction.try(:quickcard_id), amount: params[:amount], date: @transaction.try(:created_at).try(:to_datetime).try(:in_time_zone, "Pacific Time (US & Canada)")}) if @transaction.present?
        end
        ActivityLog.log(@hook ? "rtp_hook" :"rtp", "#{exc.message}",@merchant, params,response,nil,nil,"failed", @activity) if exc.message != "No pending Transaction found!"
      end
    end

    rescue_from DuplicateTransactionError do |exc|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{exc.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{exc.message}",url: @fail_url
      end
    end

    rescue_from ActivityLogSuccess do |e|
      ActivityLog.log("api_success","#{e.message}",@merchant)
    end

    rescue_from ActivityLogFailure do |e|
      ActivityLog.log("vt_error","#{e.message}",@merchant)
    end

    rescue_from TransactionError do |e|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{e.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{e.message}",url: @fail_url
      end
    end

    rescue_from JWT::DecodeError do |e|
      if params[:action] == "payment"
        render_json_response({success: true, message: "Link has expired" },:ok)
      else
        flash[:error] = "Your Session has expired"
        redirect_to new_user_session_path
      end

    end

    rescue_from Stripe::CardError, Stripe::InvalidRequestError do |e|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{e.message}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{e.message}",url: @fail_url
      end
    end

    rescue_from Timeout::Error do |e|
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_error', message: "#{I18n.t 'merchant.controller.req_timedout'}",url: @fail_url, invoice_id: @request_id.present? ? "true" : "", old_url: @old_url
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'error', message: "#{I18n.t 'merchant.controller.req_timedout'}",url: @fail_url
      end
    end

  end

  def instantiate_load_balancer(load_balancer = nil,card_brand_type, card_bin_list)
    @card_bin_list = card_bin_list
    @card_brand_type = card_brand_type
    @load_balancer = TransactionLoadBalancer.new(@location, load_balancer,card_brand_type)
    check_location_status
    card_status
    country_status
  end

  def check_location_status
    if !@location.load_balancer.active?
      @error_code = "error_2009"
      raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
    end
  end

  def card_status
    card_block = @load_balancer.check_card_blocked(params[:card_number])
    if card_block.present?
      @error_code = card_block
      raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
    end
  end

  def country_status
    if @card_bin_list.present? && @card_bin_list["country"].present? && @card_bin_list["country"]["alpha2"].present?
      decline = @load_balancer.decline_on_cc_country(@card_bin_list["country"]["alpha2"])
      if decline
        @error_code = 'error_2005'
        raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
      end
    end
  end

  # getting bank gateway for charge after load balancer processing
  def get_gateway(load_amount, card)
    @load_balancer.after_getting_issue_amount(load_amount)
    @payment_gateway = nil
    raise StandardError.new I18n.t('api.registrations.no_gateway') if @load_balancer.payment_gateways.blank?

    #= LOAD BALANCER
    @payment_gateway = @load_balancer.rotate_processor
    if @payment_gateway.blank? && @load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor? }.present?
      @error_code = 'error_2001'
      raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
    end

    @payment_gateway = @load_balancer.rotate_processor_with_cc(card,@payment_gateway)
    if @payment_gateway.blank? && @load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_with_cc? }.present?
      @error_code = 'error_2003'
      raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
    end

    if @load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_with_amount? }.present? && @load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_on_cc_brand?}.present?
      @payment_gateway = @load_balancer.merger_of_rotation_on_amount_and_cc(load_amount.to_f,card.brand,@payment_gateway)
    elsif @load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_with_amount?}.present?
      @payment_gateway = @load_balancer.rotate_processor_with_amount(load_amount.to_f,@payment_gateway)
    elsif @load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_on_cc_brand?}.present?
      @payment_gateway = @load_balancer.rotate_processor_on_cc_brand(card.brand,@payment_gateway) if card.present? && card.brand.present?
    end

    if @payment_gateway.blank?
      @error_code = "error_2001"
      @error_code = @load_balancer.error_code if @load_balancer.error_code.present?
      raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
    end

    load_daily_amount = @payment_gateway.daily_volume_achived
    load_monthly_amount = @payment_gateway.monthly_volume_achived

    @load_balancer.block_processor_daily_100(@payment_gateway,load_amount + load_daily_amount.to_f)
    @load_balancer.block_processor_monthly_100(@payment_gateway,load_amount + load_monthly_amount.to_f)
    @payment_gateway.save
    if @payment_gateway.is_block?
      @error_code = "error_2012"
      raise StandardError.new I18n.t("api.load_balancers.#{@error_code}")
    end

    @load_balancer.update_card_group_transaction_attempts(card)
    @load_balancer.block_card_for_processed_count(card)
    card.save

  end

  def update_load_balancer_in_failed_case(seq_transaction_id, card)
    decline_message = nil
    if seq_transaction_id.try(:[],:message).try(:[],:message).present?
      decline_message = seq_transaction_id[:message][:message]
    end
    @load_balancer.block_card_for_reason(decline_message, card) if decline_message.present?
    @load_balancer.update_gateway_decline_attempts(@payment_gateway,decline_message)
    @load_balancer.processor_decline_times(@payment_gateway)
    @load_balancer.processor_decline_percent(@payment_gateway)
    @load_balancer.update_card_decline_transaction_attempts(card)
    @load_balancer.block_card_for_decline_count(card)
    save_card_n_gateway(card)
  end

  def update_load_balancer_in_success_case(seq_transaction_id, card)
    @payment_gateway.error_message = []
    @payment_gateway.decline_count = 0
    @payment_gateway.last_status = I18n.t("statuses.approve")

    @load_balancer.do_calculation_for_daily_limit(@payment_gateway,seq_transaction_id[:issue_amount]) if seq_transaction_id[:issue_amount].present?
    @load_balancer.do_calculation_for_monthly_limit(@payment_gateway,seq_transaction_id[:issue_amount]) if seq_transaction_id[:issue_amount].present?
    current_amount_daily = @payment_gateway.daily_volume_achived
    current_amount_monthly = @payment_gateway.monthly_volume_achived
    @load_balancer.block_processor_daily_100(@payment_gateway,current_amount_daily)
    @load_balancer.block_processor_monthly_100(@payment_gateway,current_amount_monthly)

    if @load_balancer.load_balancer_rules.select{|lbr| lbr.processor_daily_volume?}.present? || @load_balancer.load_balancer_rules.select{|lbr| lbr.block_processor_daily_volume?}.present?
      @load_balancer.processor_daily_volume(@payment_gateway,current_amount_daily) if current_amount_daily.present?
      @load_balancer.block_processor_daily_volume(@payment_gateway,current_amount_daily) if current_amount_daily.present?
    end
    if @load_balancer.load_balancer_rules.select{|lbr| lbr.processor_monthly_volume?}.present?
      @load_balancer.processor_monthly_volume(@payment_gateway,current_amount_monthly) if current_amount_monthly.present?
    end
    save_card_n_gateway(card)
    @load_balancer.group_volume
  end

  def save_card_n_gateway(card)
    @load_balancer.update_card_transaction_attempts(card,@payment_gateway)
    @load_balancer.update_gateway_transaction_count(@payment_gateway)
    ActiveRecord::Base.transaction do
      @payment_gateway.save
      card.save
    end
  end

  private

  def saving_decline_for_seq(issue_raw_transaction,payment_gateway = nil, transaction= nil)
    if params[:both] || params[:action] == "payment"
      receiver_wallet_id = transaction.try(:receiver_wallet_id) || params[:merchant_wallet_id]
      sender = transaction.try(:sender) || @user
      total_amount = transaction.try(:total_amount) || params[:amount]
      location_fee = issue_raw_transaction.get_location_fees(receiver_wallet_id, sender, total_amount,nil,TypesEnumLib::TransactionType::CreditTransaction)
      #= building hash for Decline
      transaction_info = {amount: total_amount.to_f}
      reserve_detail = location_fee.first if location_fee.present?
      user_info = location_fee.second if location_fee.present?
      fee = location_fee.third if location_fee.present?
      transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
      #= end building process
      OrderBank.create(merchant_id: receiver_wallet_id,user_id: sender.try(:id),bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,           card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id),load_balancer_id: payment_gateway.try(:load_balancer_id))
    elsif params[:order_bank].present?
      order_bank = params[:order_bank]
      OrderBank.create(merchant_id: order_bank.merchant_id,user_id: order_bank.user_id,bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,card_type: nil,card_sub_type:nil,status: "decline",amount: order_bank.amount,transaction_info: order_bank.transaction_info,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id))
    end

  end

end