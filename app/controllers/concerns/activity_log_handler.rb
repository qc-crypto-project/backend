module ActivityLogHandler
	extend ActiveSupport::Concern

	private

	def record_it(type = nil, reason = nil)
	    unless @user.nil?
	      clean_parameters
	      activity = ActivityLog.new
	      activity.url = request.url
	      activity.host = request.host
	      activity.browser = request.env["HTTP_USER_AGENT"]
	      if request.remote_ip.present?
	        activity.ip_address = request.remote_ip
	      end
	      activity.controller = controller_name
	      activity.action = action_name
				message = I18n.t('api.registrations.account_limit_true').split('%')
				message = message[0] if message.present?
				error_message = reason.split('[') if reason.present?
				error_message = error_message[0] if error_message.present?
	      activity.action_type = "transaction_amount_limit" if error_message.present? && message.present? && error_message == message
	      activity.respond_with = reason.present? ? {response: reason} : response.body
	      unless type.nil?
	        activity.activity_type = type
	      end
	      if params[:user_image].present?
	        if params[:user_image].original_filename.present?
	          params[:user_image] = params[:user_image].original_filename
	        else
	          params[:user_image] = 'unknown image format'
	        end
	      end
	      activity.transaction_id= params[:transaction_id]
	      activity.params = params.except(:card_cvv)
	      activity.user = @user
	      activity.save
	    end
	end
end