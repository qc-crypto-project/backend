class Api::AtmsController < Api::ApiController
  skip_before_action :authenticate_user, except: [:atm_charge, :atm_service]
  include HelpsHelper

  def balance
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      wallet = @user.wallets.first
      wallet_id = wallet.id
      current_balance = show_balance(wallet_id)
      @retire_transactions = all_transactions_except(wallet_id, wallet_id, "retire")
      if @retire_transactions.empty?
        @retire_balance = 0
      else
        @retire_balance = @retire_transactions.pluck(:amount).reduce(:+)
      end
      return render_json_response({:success => true, current_balance: "$#{current_balance}", total: "$#{cents_to_dollars(@retire_balance) + current_balance.to_i}"}, :ok)
    end
    rescue => e
      puts "=======EXCEPTION HANDLED======", e.message
      return render_json_response({:message => "Something went wrong #{e}",:success => false}, :ok)
  end

  def retire
    return render_json_response({:message => I18n.t("errors.checkout.depreciated"),:success => false}, :ok)
      wallet = @user.wallets.first
      balance = dollars_to_cents(show_balance(wallet.id))
      if params[:amount].present?
        if balance.to_f >= dollars_to_cents(params[:amount]).to_f
          amount = dollars_to_cents(params[:amount])
          @user.withdraw(amount, wallet.id)
          notes_minus(@user.id,@user.denominations)
          return render_json_response({:message => "Successfully withdraw", current_balance:balance - cents_to_dollars(params[:amount].to_i),:success => true}, :ok)
        else
          return render_json_response({:message => "Low Balance",:success => false}, :ok)
        end
      else
        if balance == "0"
          return render_json_response({:message => "ATM account cleared successfully", :success => true}, :ok)
        else
          wallet = @user.wallets.first
          notes_minus(@user.id,@user.denominations)
          @user.withdraw(balance, wallet.id)
          return render_json_response({:message => "ATM account Already cleared successfully",:success => true}, :ok)
        end
      end
  end

  def atm_charge
    if params[:card_token].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        charge = Stripe::Charge.create({
          :amount => (params[:amount].to_f * 100).round,
          :currency => "usd",
          :description => "ATM charge",
          :source => params[:card_token]
        })
        return render_json_response({success: true, message: "Successfully charged"}, :ok)
        #@issue = issue(@user.wallets.first.id, dollars_to_cents(params[:amount]))
      rescue Stripe::CardError, Stripe::InvalidRequestError => e
        return render_json_response({success: false, message: e.message},:ok)
      rescue
        return render_json_response({success: false, message: "Something Went wrong!"}, :ok)
      end
    else
      return render_json_response({:success => false, :message => 'CardTokenId is not Provided!'}, :ok)
    end
  end

  def atm_service
    begin
    raise StandardError.new(I18n.t("errors.checkout.depreciated"))
    if greater_amount
      code = rand(16 ** 16)
      wallet = Wallet.new(name: "#{code}")
      wallet.generate_token
      wallet.token = code
      if wallet.save
        qr_card = QrCard.new(wallet_id: wallet.id,price: params[:amount],category: "atm")
        qr_card.generate_token
        if qr_card.save(validate: false)
          w = SequenceLib.create(wallet.id, qr_card.token)
          tx = SequenceLib.issue(wallet.id,params[:amount], wallet.id,'ATM Cards', '', 'stripe', '' )
          balance = show_balance(wallet.id)
          if balance.present? and balance.first.nil?
             balance = 0
          else
             balance = balance.first.amount
          end
          return render_json_response({success: true,balance:balance, wallet_id: wallet.id, wallet_name: wallet.name,code: qr_card.token }, :ok)
        else
         return render_json_response({success: false,message: "Card not saved" }, :ok)
        end
      else
        return render_json_response({success: false,message: "Wallet not saved" }, :ok)
      end
    else
      return render_json_response({success: false, message: "Amount can not be 0!"},:ok)
    end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def scan_atm
    if params[:qr_token].present? and params[:to_id].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        qr_card = QrCard.where(token: params[:qr_token]).first
        if qr_card.present?
          if qr_card and qr_card.is_valid == true
            price = qr_card.price.to_f
            if qr_card.message.present?
              send_text_message(qr_card.message,@user.phone_number)
              push_notification(qr_card.message, @user)
            end
            begin
              balances = show_balance(qr_card.wallet_id)
              if balances.to_f < dollars_to_cents(qr_card.price).to_f
                return render_json_response({:success => false, :message => 'Transaction Cannot Perfom Due to Low balance!'}, :ok)
              else
                data = ''
                current_transaction = transaction_between(params[:to_id], qr_card.wallet_id,price,"By Qr Card:#{qr_card.id}", 0,data)
                #twilio_text = send_text_message("#{number_to_currency(price)} has been successfully transferred to your wallet from #{user.name}.\nNote From Sender: #{params[:message]}", user.phone_number)
                balance = show_balance(params[:to_id])
                transactions = all_transactions(params[:to_id],params[:to_id],params[:page_number])
                qr_card.update(is_valid: false)
                return render_json_response({:success => true, :message => 'Successfull :)',transactions: transactions,transaction: current_transaction, balance: (balance.to_f/100).round(2)}, :ok)
              end
            rescue => ex
              puts "=======EXCEPTION HANDLED======", ex.message
              return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
            end
          else
            return render_json_response({:success => false, :message => 'Card Already Used'}, :ok)
          end
        else
         return render_json_response({:success => false, :message => 'Card Not Found!'}, :ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({:success => false, :message => 'Incomplete Params'}, :ok)
    end
  end

end
