class Api::CardsController < Api::ApiController
  include ApplicationHelper
  before_action :setting_stripe_for_charge, only: [:charge,:create]
  include FluidPayHelper
  require 'raas'

  def create # simply create a new card
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters({
        # :cvv => params[:cvv],
        :name => params[:name],
        :exp_date => params[:exp_date],
        :card_number => params[:card_number]
      })
      # card = {:user_id => @user.id, :name => params[:name], :exp_date => params[:exp_date], :brand => tokenization.card_brand(params[:card_number]), :last4 => params[:card_number][12..16], cvv: params[:cvv], card_type: tokenization.card_type(params[:card_number])}
      # tokens = tokenization.create_all(params[:name], params[:card_number], params[:exp_date], params[:cvv], @user.stripe_customer_id, @user)
      # stripe_token = tokenization.stripe(params[:name], params[:card_number], params[:exp_date], params[:cvv], nil, @user, ENV["stripe_pop_secret_key"])
      # stripe_token = tokenization.stripe(params[:name], params[:card_number], params[:exp_date], params[:cvv], @user.stripe_customer_id, @user, ENV["stripe_secret_key"])
      # card = Card.new(card.merge(payeezy: tokens[:payeezy], elavon: tokens[:elavon], stripe: tokens[:stripe][:card_id], bridge_pay: tokens[:bridge_pay]))
      # card.stripe2 = stripe_token[:card_id]

      #creating card for
      card_info = {
          number: params[:card_number],
          month: params[:exp_date].first(2),
          year: "20#{params[:exp_date].last(2)}",
          # cvv: params[:card_cvv]
      }
      card_bin_list = get_card_info(params[:card_number])
      card1 = Payment::QcCard.new(card_info,nil,@user.id)
      card = @user.cards.where(fingerprint: card1.fingerprint).first_or_create(qc_token:card1.qc_token,exp_date: "#{card_info[:month]}/#{card_info[:year].last(2)}",brand: card1.card_brand, last4: params[:card_number].last(4), name: params[:name], card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: params[:card_number].first(6).to_i,fingerprint: card1.fingerprint)

      # fluid_pay_payment = FluidPayPayment.new
      # ############ Fluid Pay Payment ###################
      # fluid_pay_payment_return = fluid_pay_payment.get_token_id('19VpZDXlUKuLPmkjXLhdIEIOFQi',params[:card_number],card.exp_date,card.cvv,@user.fluid_pay_customer_id)
      # if not @user.fluid_pay_customer_id.present?
      #   @user.update(fluid_pay_customer_id:fluid_pay_payment_return.first)
      # end
      # card.fluid_pay = fluid_pay_payment_return.second
      ####################################################
      # card.save!
      cards = @user.cards.where(archived: false).order(created_at: :desc)
      return render_json_response({success: true, message: "Card Added Successfully!", cards: cards}, :ok)
    rescue => exc
      p "-------EXCPTION OCURRED--------", exc.inspect
      return render_json_response({success: false, message: "#{exc.message}"}, :ok)
    end
  end

  def charge # after user create a new card ,we set all information of customer card like wallet, fluid_pay_payment and deposit limit etc
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      fee_lib = FeeLib.new
      validate_parameters({:id => params[:id], :amount => params[:amount]})
      card = Card.where(id: params[:id], :user_id => @user.id).first
      raise StandardError.new('Card not found!') unless card.present?
      app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
      raise StandardError.new('PaymentProcessor not configured!') unless app_config.present?
      wallet = @user.wallets.first
      wallet_id = wallet.id
      qc_wallet = Wallet.qc_support.first
      charge_card = CardToken.new
      fluid_pay_payment = FluidPayPayment.new
      deposit_limit = fee_lib.get_limit(params[:amount],'deposit')
      raise StandardError, "Your weekly limit is: #{fee_lib.show_limit('deposit')}" if !deposit_limit
      amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount], 'card').to_f
      if app_config.stringValue == 'stripe' || app_config.stringValue == 'stripe_pop' || app_config.stringValue == 'stripe_pop_2'
        customer = charge_card.create_stripe_customer( @user)
        raise StandardError.new('Card not configured properly! Please add card again and try again') if customer.nil?
        stripeParams = { :amount => (dollars_to_cents(amount).to_i), :currency => "usd" }
        unless card.qc_token.present?
          stripeParams[:customer] = customer
          stripeParams[:source] = charge_card.get_card_source(card)
        else
          qcCard = Payment::QcCard.new({},card.qc_token,@user.id)
          stripeParams[:card] = {
              :number => qcCard.number,
              :exp_month => qcCard.month,
              :exp_year => qcCard.year,
              # :cvc => qcCard.cvv,
              :cvc => params[:card_cvv],
              :customer => customer,
          }
        end
        charge = Stripe::Charge.create(stripeParams)
        charge = charge.id
        if charge.present?
          OrderBank.create(user_id: @user.id, bank_type: "#{app_config.stringValue}",card_type: nil,card_sub_type:nil,status: "approve",amount: amount )
        else
          OrderBank.create(user_id: @user.id, bank_type: "#{app_config.stringValue}",card_type: nil,card_sub_type:nil,status: "decline",amount: amount )
        end
      elsif app_config.stringValue == 'payeezy'
        transaction_id = charge_card.charge_payeezy(card, amount)
        charge = transaction_id
      elsif app_config.stringValue == 'converge'
        charge_id = charge_card.charge_elavon(card.elavon, amount, card.exp_date, @user.email)
        charge = charge_id
      elsif app_config.stringValue == 'bridge_pay'
        charge_id = charge_card.charge_bridge_pay(card.bridge_pay, amount, card.name, card.exp_date)
        charge = charge_id
      elsif app_config.stringValue == 'fluid_pay'
        fluid_pay_payment = fluid_pay_payment.chargeAmount('19VpZDXlUKuLPmkjXLhdIEIOFQi',dollars_to_cents(amount),@user.fluid_pay_customer_id,card.fluid_pay,'making a transactions')
        charge = fluid_pay_payment.first
      end
      transaction = @user.transactions.build(to: wallet_id, from: app_config.stringValue, status: "pending",charge_id: charge, amount: amount, sender: @user, action: 'issue')
      if transaction.save!
        data = {
          :issue_amount => dollars_to_cents(amount),
          :transaction_id => charge,
          :merchant_name => @user.name
        }
        tx=SequenceLib.issue(wallet_id, amount, qc_wallet, "charge", fee_lib.get_card_fee(params[:amount], 'card'),nil, app_config.stringValue, nil, card.last4, nil, nil, nil, get_ip, data)
        transaction.update(seq_transaction_id: tx.actions.first.id)
        balance = SequenceLib.balance(wallet_id)
        return render_json_response({success: true, balance: balance}, :ok)
      end
      raise StandardError.new('Invalid PaymentProcessor!')
    rescue => exc
      return render_json_response({success: false, message: "#{exc.message}"}, :ok)
    end
  end

  def delete # in this a card moved into archived list, not delelted the card entirly
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters({:id => params[:id]})
      card = Card.where(:id => params[:id], :user_id => @user.id).first
      raise StandardError.new('Card not found!') if card.nil?
      card.update(archived: true)
      # customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
      # customer.sources.retrieve(card.stripe).delete()
      # card.destroy!
      return render_json_response({success: true, message: "Card Deleted Successfully!"}, :ok)
    rescue => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end

  def list #simply return a list of active cards and accounts
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      cards = @user.cards.where(archived: false).order(created_at: :desc)
      # cards = @user.cards.all
      accounts = Account.where(:user_id => @user.id)
      return render_json_response({success: true, cards: cards, accounts: accounts}, :ok)
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"}, :ok)
    end
  end

  def catalogues #simply catalogs render
    begin
      raise Exception.new(I18n.t("errors.checkout.depreciated"))
      client = TangoClient.instance
      catalog = client.catalog
      result = catalog.get_catalog()
      return render_json_response({success: true, message: "Tango Card Catalogues!", catalogues: result}, :ok)
    rescue Exception => exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.errors.present? && exc.errors.count > 0
        message = exc.errors.first.message
      end
      return render_json_response({success: false, message: "#{message}"}, :ok)
    end
  end

  def setting_stripe_for_charge
    app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    if app_config.present?
      if app_config.stringValue == "stripe"
        publishable_key = ENV['stripe_publishable_key']
        secret_key = ENV['stripe_secret_key']
      elsif app_config.stringValue == "stripe_pop"
        publishable_key = ENV['stripe_pop_publishable_key']
        secret_key = ENV['stripe_pop_secret_key']
      end
    end
    Stripe.api_key = secret_key
  end

  def order #for buying Gift card
    if ENV['TANGO_CARDS'] == 'false'
      return render_json_response({success: false, message: "You can't purchase Giftcards at the moment! Please try again later!"}, :ok)
    end
    begin
      raise Exception.new(I18n.t("errors.checkout.depreciated"))
      balance = dollars_to_cents(show_balance(@user.wallet_id))
      client = TangoClient.instance
      if params[:amount].present? && params[:utid].present? && @user.first_name.present? && @user.last_name.present?
        fee_lib = FeeLib.new
        amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount].to_f, 'gift_card').to_f
        if balance.to_i < dollars_to_cents(amount).to_i
          return render_json_response({success: false, message: I18n.t("merchant.controller.insufficient_balance")}, :ok)
        else
          # admin_wallet = Wallet.where(wallet_type: 3).first
          transact = withdraw(params[:amount].to_f,@user.wallet_id, fee_lib.get_card_fee(params[:amount].to_f, 'gift_card').to_f, TypesEnumLib::TransactionType::GiftCardPurchase)
          # transact = transaction_between(admin_wallet.id, @user.wallet_id, amount, "Tango Transfer", 0, "")
          if transact.present?
            orders = client.orders
            customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
            account_identifier = ENV["ACCOUNT_IDENTIFIER"]

            body = Raas::CreateOrderRequestModel.new
            body.account_identifier = account_identifier
            body.amount = params[:amount] || 0
            body.customer_identifier = customer_identifier
            body.send_email = true
            body.sender = {
                "email": 'admin@quickcard.me',
                "firstName": "QuickCard",
                "lastName": ""
            }
            body.recipient = {
                "email": @user.email,
                "firstName": @user.first_name || 'No Name',
                "lastName": @user.last_name || 'No Name'
            }
            body.utid = params[:utid]

            result = orders.create_order(body)
            if result
              p "TANGO ORDER: ", result
              @user.tango_orders.create!(
                  :utid => params[:utid],
                  :account_identifier => account_identifier,
                  :amount => body.amount,
                  :order_id => result.reference_order_id,
                  :catalog_image => params[:catalog_image] || nil
              )
              return render_json_response({success: true, message: "Order created successfully!", order: result}, :ok)
            end
          else
            return render_json_response({success: false, message: "Sequence Transaction Failure!"}, :ok)
          end
        end
      end
      return render_json_response({success: false, message: "Invalid Parameters! Amount & UTID must be given!"}, :ok)
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.errors.present? && exc.errors.count > 0
        message = exc.errors.first.message
      end
      return render_json_response({success: false, message: "#{message}"}, :ok)
    end
  end

  def orders
    begin
      raise Exception.new(I18n.t("errors.checkout.depreciated"))
      client = TangoClient.instance

      orders = client.orders

      customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
      account_identifier = ENV["ACCOUNT_IDENTIFIER"]

      collect = Hash.new

      collect['account_identifier'] = account_identifier
      collect['customer_identifier'] = customer_identifier

      # start_date = DateTime.now
      # collect['start_date'] = start_date
      # end_date = DateTime.now
      # collect['end_date'] = end_date

      elements_per_block = 100
      collect['elements_per_block'] = elements_per_block
      page = 0
      collect['page'] = page

      # result = orders.get_orders(collect)
      # p "sdjfhjkshdfkjdshf", result
      # p "sdkjhjksdhf", @user.tango_orders.pluck(:order_id)
      orders = client.orders
      tangoIntialOrders = @user.tango_orders.select{|v| v.utid != nil}
      tango_orders = tangoIntialOrders.map do |order|
        object = orders.get_order(order.order_id)
        object.to_hash.merge(:catalog_image => order.catalog_image)
      end

      return render_json_response({success: true, message: "Tango Orders List!", orders: tango_orders}, :ok)
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.errors.present? && exc.errors.count > 0
        message = exc.errors.first.message
      end
      return render_json_response({success: false, message: "#{message}"}, :ok)
    end
  end

  def get_order
    begin
      raise Exception.new(I18n.t("errors.checkout.depreciated"))
      client = TangoClient.instance
      if params[:id].present?
        tangoOrder = TangoOrder.where(:order_id => params[:id]).first
        orders = client.orders
        result = orders.get_order(params[:id])
        return render_json_response({success: true, message: "Requested Order Details!", order: result, :catalog_image => tangoOrder.catalog_image}, :ok)
      end
      return render_json_response({success: false, message: "Invalid Parameters! Order ID missing!"}, :ok)
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.errors.present? && exc.errors.count > 0
        message = exc.errors.first.message
      end
      return render_json_response({success: false, message: "#{message}"}, :ok)
    end
  end

  def elavon_ach_transact
    success = true
    message = 'ACH Transaction Successfully done!'
    parsed_hash = {}
    begin
      raise Exception.new(I18n.t("errors.checkout.depreciated"))
      if params[:account_number].present? && params[:first_name].present? && params[:last_name].present? && params[:amount].present? && params[:aba_number].present?
        url = "https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do?ssl_merchant_id=002544&ssl_user_id=webpage&ssl_pin=6FCEZY&ssl_transaction_type=ecspurchase&ssl_amount=#{params[:amount]}&ssl_aba_number=#{params[:aba_number]}&ssl_bank_account_number=#{params[:account_number]}&ssl_agree=1&ssl_first_name=#{params[:first_name]}&ssl_last_name=#{params[:last_name]}&ssl_show_form=false&ssl_bank_account_type=0&ssl_ecs_product_code=WEB&ssl_result_format=ascii"
        response = HTTParty.get(url)
        if response.success?
          response.parsed_response.split("\n").map do |item|
            arr = item.split("=")
            parsed_hash[arr[0]] = arr[1]
          end
          if parsed_hash['errorName'].present?
            message = parsed_hash['errorName']
            success = false
          end
        end
        return render_json_response({success: success, message: message, data: parsed_hash}, :ok)
      else
        return render_json_response({success: false, message: "Incomplete Parameters!"}, :ok)
      end
    rescue => exc
      message = exc.message ? exc.message : 'Elavon Exception Occured!'
      return render_json_response({success: false, message: "#{message}"}, :ok)
    end
  end

  private

  def credit_card_transaction(app_config, card, amount)
    if app_config.secondary_option == 'stripe'
      charge = Stripe::Charge.create({
                                         :amount => (dollars_to_cents(amount)),
                                         :currency => "usd",
                                         :customer => @user.stripe_customer_id,
                                         :source => card.stripe
                                     })
      return charge.id
    elsif app_config.secondary_option == 'converge'
      charge_id = charge_card.charge_elavon(card.elavon, amount, card.exp_date, @user.email)
      return charge_id
    elsif app_config.secondary_option == 'payeezy'
      charge_id = charge_card.charge_elavon(card.elavon, amount, card.exp_date, @user.email)
      return charge_id
    end
  end

end
