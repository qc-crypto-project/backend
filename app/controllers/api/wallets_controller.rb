class Api::WalletsController < Api::ApiController
  require 'net/http'
  require 'net/https'
  require 'uri'
  require 'rqrcode'
  require 'barby'
  require 'barby/barcode'
  require 'barby/barcode/qr_code'
  require 'barby/outputter/png_outputter'
  include FluidPayHelper
  include ApplicationHelper
  include RefundHelper
  include TransactionCharge::ChargeATransactionHelper
  include ExceptionHandler
  include Api::RegistrationsHelper
  include Admins::BlockTransactionsHelper
  include PosMerchantHelper

  FAILED = 'failed'
  SUCCESS = 'success'

  skip_before_action :authenticate_user, only: [:search, :show_balance_of_wallet]
  before_action :check_bank_status, only: [:refund_money]
  before_action :allow_if_active , only: [:scanner]
  before_action :location_ip_policy, only: [:virtual_debit_charge]

  def create
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:name].present?
      wallet = Wallet.new(name: params[:name], user_ids: @user.id)
      if wallet.save
        ledger = @@ledger
        balance = show_balance(wallet.id)
        return render_json_response({success: true, wallet: WalletSerializer.new(wallet), wallet_name: wallet.name, balance:balance, wallet_qr: wallet.qr_image},:ok)
      else
        return render_json_response({:success => false, :message => 'Something went wrong Wallet not Saved!'}, :ok)
      end
    else
     return render_json_response({:success => false, :message => 'Wallet name should be provided!'}, :ok)
    end
  end

  def check_transaction_status
    return render_json_response({:success => false, message: "Please provide authentication token"}, :ok) if params[:auth_token].blank?
    return render_json_response({:success => false, message: "Please provide transaction id"}, :ok) if params[:id].blank?
    transaction = Transaction.where(seq_transaction_id: params[:id]).first
    return render_json_response({:success => false, message: "No transaction found"}, :ok) if transaction.blank?
    return render_json_response({success: false, message: "Only virtual terminal transactions can be checked"}, :ok) if transaction.from != "Credit"
    order_bank = OrderBank.where(transaction_id: transaction.id).first
    return render_json_response({:success => false, message: "No transaction found"}, :ok) if order_bank.blank?
    gateway = transaction.payment_gateway.try(:name)
    if order_bank.status == "decline"
      parse = JSON.parse(order_bank.transaction_info)
      message = parse["message"]
      return render_json_response({success: true, status: order_bank.status, message: message, transaction_id: transaction.seq_transaction_id}, false)
    elsif order_bank.status == "approve"
      return render_json_response({success: true, status: order_bank.status, transaction_id: transaction.seq_transaction_id, auth_code: transaction.charge_id, descriptor_id: gateway}, false)
    end
  end

  def transaction_last4
    return render_json_response({:success => false, message: "Please provide authentication token"}, :ok) if params[:auth_token].blank?
    return render_json_response({:success => false, message: "Please provide amount"}, :ok) if params[:amount].blank?
    return render_json_response({:success => false, message: "Please provide last4"}, :ok) if params[:last4].blank?
    merchant = User.authenticate_by_token(params[:auth_token]).first
    return render_json_response({:success => false, message: "No user found with auth token"}, :ok) if merchant.blank? || (merchant.present? && !merchant.merchant?)
    transaction = merchant.transactions.where(amount: params[:amount].to_f, last4: params[:last4]).last
    return render_json_response({:success => false, message: "No Transaction found"}, :ok) if transaction.blank?
    location_secure_token = Wallet.find_by(id: transaction.try(:to).to_i).try(:location).try(:location_secure_token)
    return render_json_response({:success => false, message: "No Transaction found"}, :ok) if location_secure_token.blank?
    order_bank = OrderBank.where(transaction_id: transaction.id).last
    return render_json_response({:success => false, message: "No transaction found"}, :ok) if order_bank.blank?
    gateway = transaction.payment_gateway.try(:name)
    card = transaction.tags.try(:[],"card")
    if card.present?
      payType = card["brand"].capitalize
      cardType = card["card_type"].capitalize
    end
    if order_bank.status == "decline"
      parse = JSON.parse(order_bank.transaction_info)
      message = parse["message"]
      return render_json_response({success: true, status: order_bank.status, message: message, transaction_id: transaction.seq_transaction_id, descriptor_id: gateway,location_id: location_secure_token, card_type: "#{cardType.try(:downcase)} - #{payType.try(:downcase)}"}, false)
    elsif order_bank.status == "approve"
      return render_json_response({success: true, status: "approved", transaction_id: transaction.seq_transaction_id, auth_code: transaction.auth_code || transaction.charge_id, descriptor_id: gateway,location_id: location_secure_token, card_type: "#{cardType.try(:downcase)} - #{payType.try(:downcase)}"}, false)
    end
  end

  def check_transaction_reference
    params[:ref_id] = params[:RefId] if params[:RefId].present?
    #ref_id is required otherwise raise error and return false
    if params[:ref_id].present? 
      #get transaction by giving ref_if
      transaction = Transaction.where(ref_id: params[:ref_id]).last
      if transaction.present?
        #get activity log by transaction
        activity = transaction.activity_logs.last
        if activity.present?
          if activity.present? && activity.respond_with.present?
            response = activity.respond_with.class == String ? JSON.parse(activity.respond_with) : activity.respond_with
            response.transform_keys!(&:to_sym) if response.present?
          else
            response = {:success => false, message: "Transaction Activity needs to be updated."}
          end
        else
          response = {:success => false, message: "Transaction not found."}
        end
      else
        response = {:success => false, message: "Transaction not found."}
      end
    else
      response = {:success => false, message: "Please provide ref_id"}
    end
    return render_json_response(response, :ok)
  end

  def generate_token
    return render_json_response({:success => false, :token => I18n.t("errors.checkout.depreciated")}, :ok)
    @token = Stripe::Token.create(:card => {  :number => params[:number], :exp_month => params[:exp_month], :exp_year => params[:exp_year], :cvc => params[:cvc] },)
    return render_json_response({:success => true, :token => @token.id}, :ok)
  end

  def wallets
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated")},:ok)
    if params[:auth_token].present?
        wallets = @user.wallets.map { |wallet| {wallet_id: wallet.id, wallet_name: wallet.name, wallet_qr: "https:#{wallet.qr_image}"} }
        return render_json_response({success: true, message: "Successfully",wallets: wallets},:ok)
      else
        return render_json_response({success: false, message: "User Not Found"},:ok)
      end
  end

  def show_wallet
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:wallet_id].present?
        wallet = Wallet.where(id: params[:wallet_id]).first
        if wallet.present?
          wallet_name = wallet.name
          wallet_qr = wallet.qr_image
          balance = show_balance(params[:wallet_id])
          # if params[:next].present?
          # for getting single page of 10 transactions
          transactions = single_page_transactions(params[:wallet_id], params[:wallet_id], params[:next])
          myTransaction = []
          nextVar = ""
          last_page = true
          if transactions[:transactions].present?
            myTransaction = transactions[:transactions]
            nextVar = transactions[:next]
            last_page = transactions[:last_page]
          end
          # else
          #   transactions = all_transactions(params[:wallet_id],params[:wallet_id],params[:page_number])
          # end
          return render_json_response({
              success: true,
              wallet_qr: "https:#{wallet_qr}",
              wallet_token: wallet.token,
              wallet_name: wallet_name,
              balance:balance,
              transactions: myTransaction,
              next: nextVar,
              last_page: last_page
           },:ok)
        else
          return render_json_response({success: false, message: "Wallet Not Found"},:ok)
        end
      else
        return render_json_response({:success => false, :message => 'Walet id not found'}, :ok)
      end
  end

  def show_walet
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    wallet = @user.wallets.first
      if wallet.present?
        wallet_id = wallet.id
        wallet_name = wallet.name
        wallet_name = wallet.name
        wallet_qr = wallet.qr_image
        balance = show_balance(wallet_id)
        transactions = all_transactions(wallet_id,wallet_id,params[:page_number])
        return render_json_response({success: true,wallet: WalletSerializer.new(wallet),  wallet_id: wallet_id,  wallet_qr: "https:#{wallet_qr}", wallet_token: wallet.token,wallet_name: wallet_name,balance:balance, transactions: transactions, last_page: transactions.nil? ? true : false  },:ok)
      else
        return render_json_response({success: false, message: "Wallet Not Found"},:ok)
      end

  end

  def generator
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if  params[:from_id].present?
        if greater_amount
          # req = {"authentication_token"=>"examplecode98798","walet"=>"examplecode98798","amount"=>"examplecode98798"}
          if @user.nil?
            return render_json_response({:message => "User not Found!", :success => false}, :not_found)
          else
            wallet = Wallet.where(id: params[:from_id]).first
            if wallet.nil?
              return render_json_response({:success => false, :message => 'Wallet not belong to you'}, :ok)
            else
              balance = dollars_to_cents(show_balance(wallet.id))
              if balance.to_f >= dollars_to_cents(params[:amount]).to_f
                qr_card = QrCard.new(user_id: @user.id, wallet_id: wallet.id, message: params[:message])
                qr_card.generate_token
                qr_card.price = params[:amount]
                req = {"qr_token" => qr_card.token}
                barcode = Barby::QrCode.new(req.to_json, level: :q, size: 10)
                base64_output = Base64.encode64(barcode.to_png({ xdim: 5 }))
                data = "data:image/png;base64,#{base64_output}"
                image = Paperclip.io_adapters.for(data)
                image.original_filename = "qr_image.png"
                qr_card.image = image
                qr_card.category = "sent"
                if params[:tip].present?
                  qr_card.tip = params[:tip].to_f
                end
                qr_card.save!
                return render_json_response({success: true, barcode_image: "https:#{qr_card.image.url}", qr_code: qr_card.token},:ok)
              else
                return render_json_response({:success => false, :message => 'You Dont Have Enough balance!'}, :ok)
              end
            end
          end
        else
          return render_json_response({success: false, message: "Amount can not be 0!"},:ok)
        end
    else
      return render_json_response({:success => false, :message => 'You just passed empty service yayyyyy'}, :ok)
    end
  end

  def scanner
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    email_option = [true , false]
    if (params[:qr_token].present? && params[:to_id].present?) || (params[:qr_token].present? && params[:location_secure_token].present?)
        if params[:location_secure_token].present?
          location=authorized_merchant_app(@user.id,params[:location_secure_token])
          if location
            params[:to_id] = location.wallets.first.id
          else
            return render_json_response({:success => false, :message => 'Invalid app/location credentials!'}, :ok)
          end
        end

        qr_card = QrCard.includes(:user).where(token: params[:qr_token]).first
        if params[:amount].present? && qr_card.present? && dollars_to_cents(params[:amount]) != dollars_to_cents(qr_card.price)
          return render_json_response({:success => false, :message => 'QR & Invoice Amount didn\'t match!'}, :ok)
        end

        if qr_card.present? && qr_card.is_valid == true
          user = qr_card.user
          if @user.id == user.id
            return render_json_response({:success => false, :message => 'Cannot transfer to your own wallet.'}, :ok)
          else
            if qr_card && qr_card.is_valid == true
              if user && Wallet.where(id: qr_card.wallet_id).first.present?
                @wallet =  Wallet.where(id: params[:to_id]).first
                if @wallet.present?

                  if qr_card.tip.to_i > 0 && @user.merchant?
                    price = qr_card.price.to_f + qr_card.tip.to_f
                  else
                    price = qr_card.price.to_f
                  end

                  balances = show_balance(qr_card.wallet_id)
                  if balances.to_i >= qr_card.price.to_f.to_i

                    if @user.merchant?
                      data = ''
                      if qr_card.tip.to_f > 0
                        wallet= @user.wallets.where(wallet_type: 'tip',location_id: location.id).first
                        current_transaction = transaction_between(wallet.id, qr_card.wallet_id, qr_card.tip.to_f, "qr_card", 0, data)
                      end
                      wallet = Wallet.where(id: params[:to_id]).first
                      if wallet
                        data = ''
                        current_transaction = transaction_between(wallet.id, qr_card.wallet_id, qr_card.price.to_f, "qr_card", 0, data)
                        balance = show_balance(wallet.id)
                        transactions = all_transactions(wallet.id,wallet.id, params[:page_number] || 0)
                        qr_card.update(is_valid: false)
                        return render_json_response({:success => true, :message => "Successfully transferred $#{format_dollar_amount(qr_card.price)}", amount: format_dollar_amount(qr_card.price), transactions: transactions, balance:balance}, :ok)
                      else
                        return render_json_response({:success => false, :message => 'Your tip wallet does not exist!'}, :ok)
                      end
                    end

                    if qr_card.tip.to_i > 0 && @user.user?
                      return render_json_response({:success => false, :message => 'Please remove Tip from the QR Code & Try Again !'}, :ok)
                    else
                      data = ''
                      transaction_between(params[:to_id], qr_card.wallet_id, qr_card.price.to_f, "qr_card", "0", data)
                      balance = show_balance(params[:to_id])
                      transactions = all_transactions(params[:to_id], params[:to_id], params[:page_number] || 0)
                      qr_card.update(is_valid: false)
                      if qr_card.message.present?
                        send_text_message(qr_card.message, @user.phone_number)
                        push_notification(qr_card.message, @user)
                      end
                      if params[:send_email].present? &&  email_option.include?(params[:send_email]) && params[:send_email] == true
                        UserMailer.inform_merchant(@user, qr_card )
                      end
                      return render_json_response({:success => true, :message => "Successfully transferred $#{format_dollar_amount(qr_card.price.to_f)}", amount: format_dollar_amount(qr_card.price.to_f), transactions: transactions, balance:balance}, :ok)
                    end
                  else
                    return render_json_response({:success => false, :message => 'Transaction can\'t be completed due to Low Balance!'}, :ok)
                  end
                else
                  return render_json_response({:success => false, :message => I18n.t('api.errors.wallet_not_found')}, :ok)
                end
              else
                return render_json_response({:success => false, :message => 'wallet and user ids doesnot match'}, :ok)
              end
            else
              return render_json_response({:success => false, :message => 'QR is already used!'}, :ok)
            end
          end
        else
          val = params[:qr_token]
          if params[:qr_token].first == "%"
            regexp = Regexp.new('=([d+])?\w+')
            val = regexp.match(params[:qr_token]).to_s.tr('=','')
          end
          quickcard = Giftcard.where(token: val).first
          if quickcard.present?
            if quickcard.is_valid == true
              @wallet =  Wallet.where(id: params[:to_id]).first
              if @wallet.present?
                balance = show_balance(quickcard.token)
                if balance.to_f >= params[:amount].to_f
                  data = ''
                  transaction_between(@wallet.id, quickcard.token, params[:amount].to_f, "qr_card", "0", data)
                  #quickcard.update(is_valid: false)
                  balance = balance.to_f - params[:amount].to_f
                  return render_json_response({success: true, balance:balance,  message: "Successfully transafered! #{balance}$ to you wallet #{@wallet.name}"},:ok)
                else
                return render_json_response({success: false, message: I18n.t('merchant.controller.insufficient_balance')},:ok)
                end
              else
                return render_json_response({success: false, message: "Wallet not belongs to you"},:ok)
              end
            else
              return render_json_response({success: false, message: "Validation expires."},:ok)
            end
          else
            return render_json_response({:success => false, :message => 'Card has expired!'}, :ok)
          end
        end
    else
      return render_json_response({:success => false, :message => 'Incomplete Params'}, :ok)
    end
  end

  def scanner_reverse
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:from_id].present? && params[:wallet_qr].present?
        if greater_amount
          wallets = Wallet.includes(:users).where('id = ? or token = ?', params[:from_id], params[:wallet_qr])
          wallet = wallets.select{|w| w.id == params[:from_id].to_i}.first
          to = wallets.select{|w| w.token == params[:wallet_qr]}.first
          if wallet.present?
            if to.nil?
              return render_json_response({:success => false, :message => 'Reciever Not Found '}, :ok)
            else
             transfer_amount = params[:amount].to_f
             to_user = to.users.first
             if @user.atm?
               if issue_amount(transfer_amount * 2, wallet, 'ATM Deposit', 'ATM', 0).nil?
                 return render_json_response({:success => false, :message => 'ATM Asset Issue Failure!'}, :ok)
               end
             end
             balance = show_balance(wallet.id)
             if balance.to_f >= transfer_amount
               message = "By Scanning Wallet #{to.id}(#{to.name})"
               if params[:denominations].present?
                 notes_plus(@user.id,params[:denominations])
                 data = params[:denominations]
               else
                 data=''
               end
               to_wallet = to_user.present? || to.oauth_app.present? ? to.id : to.token
               transaction = transaction_between(to_wallet, wallet.id, transfer_amount, message,"0", data)
               message = "#{number_to_currency(params[:amount])} has been successfully transferred to your wallet from #{@user.name}"
               twilio_text = send_text_message(message, to_user.phone_number) if to_user
               card = Giftcard.where(:token => params[:wallet_qr]).first
               if card.present?
                 card.update(:is_valid => true, :balance => (card.balance.to_f + params[:amount].to_f).to_s)
               end
               push_notification(message, to_user) if to_user
               balance = balance.to_f - transfer_amount
               transactions = @user.atm? ? [] : all_transactions(wallet.id,wallet.id,params[:page_number])
               return render_json_response({:success => true, :message => 'Successful :)',transactions: transactions, balance: balance}, :ok)
             else
              return render_json_response({:success => false, :message => 'You Dont have Enough Balance!'}, :ok)
             end
            end
          else
            return render_json_response({:success => false, :message => 'Wallet does not belong to Sender'}, :ok)
          end
        else
          return render_json_response({success: false, message: "Amount cannot be 0!"},:ok)
        end
    else
      return render_json_response({:success => false, :message => 'Incomplete Params'}, :ok)
    end
  end

  def search
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:search].present?
      users = User.includes(:wallets).where("lower(name) LIKE ? or phone_number LIKE ?", "%#{params[:search].downcase}%", "%#{params[:search]}%")
      wallets = Wallet.where(name: "#{params[:search]}")
      a = users | wallets
      if !a.nil?
        if a.first.class.name  == "User"
          @@search_wallets = a.map{|u| u.wallets.map{|w|{ user_name: u.name,user_id: u.id,wallet_name: w.name,wallet_id: w.id} }}.flatten
        else
          @@search_wallets = a.map{|w| {user_name: w.users.first.name,user_id: w.users.first.id,wallet_name: w.name, wallet_id: w.id }}
        end
        return render_json_response({success: true, wallets: @@search_wallets }, :ok)
      else
        return render_json_response({:success => false, :message => ' Wallet does not match'}, :ok)
      end
    end
  end

  # ------------------------------Payzee Refund-------------------------------
  def refund_with_payzee
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if !params[:transact_id] || !params[:transact_tag] || !params[:amount]
      return render_json_response({:success => false, :message => "Please provide all parameters transaction_id, transaction_tag and amount"},:ok)
    end
    refund=CardToken.new
    responseData=refund.refund_payzee(params[:transact_id],params[:transact_tag],params[:amount])
    return render_json_response(responseData,:ok)
  end

  # ------------------------------Payzee Refund End---------------------------
  # -------------------------------Stripe Refund------------------------------
  def refund_with_stripe
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    return render_json_response({:success => false, :message => "Please provide charge id"},:ok) if params[:charge].nil?
    refund = Stripe::Refund.create({
                                       charge: params[:charge]
                                   })
    if refund['status'] == 'succeeded'
      return render_json_response({:success => true, :message => 'Successfully Refunded', :amount => refund['amount']},:ok)
    end
    return render_json_response({:success => false, :message => 'Amount Not Refunded'},:ok)
  end

  # -------------------------------Stripe Refund End--------------------------
  def issue_with_stripe
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:amount].to_f > 0
      if params[:card_token].present? or params[:bank_account_token].present?
          wallet = @user.wallets.first
          wallet_id = wallet.id
          wallet_name = wallet.name
          amount = params[:amount].to_f
          # result = amount * 100
          # result = result.to_s
          if params[:type] == "card" || !params[:type].present?
            charge = Stripe::Charge.create({
                                               :amount => (dollars_to_cents(params[:amount]).to_i),
                                               :currency => "usd",
                                               :customer => @user.stripe_customer_id,
                                               :card => params[:card_token]
                                            })
          elsif params[:type] == "account"
            charge = Stripe::Charge.create({
                                               :amount => (dollars_to_cents(params[:amount]).to_i),
                                               :currency => "usd",
                                               :source => params[:bank_account_token],
                                               :customer => @user.stripe_customer_id
                                            })
            transaction = @user.transactions.build(to: wallet_id, from: "stripe", status: "pending", charge_id: charge.id, amount: params[:amount], sender: @user, action: 'issue')
            if transaction.save!
              balance = show_balance(wallet_id)
              return render_json_response({success: true, balance:balance}, :ok)
            else
              return render_json_response({:success => false, :message => "Cannot save transaction"},:ok)
            end
          else
            return render_json_response({:success => false, :message => "Type should be account or card"},:ok)
          end
          if params[:denominations].present?
            notes_plus(@user.id,params[:denominations])
          end
          issue = @user.issue_with_stripe(params[:amount],wallet_id, "By Stripe")
          balance = show_balance(wallet_id)
          transactions = all_transactions(wallet_id,wallet_id,params[:page_number])
          return render_json_response({success: true, wallet: WalletSerializer.new(wallet), balance:balance, charge: charge,wallet_id: wallet_id, wallet_name: wallet_name, issue: issue, transactions: transactions }, :ok)
      else
        return render_json_response({:success => false, :message => 'CardToken missing!'}, :ok)
      end
    else
      return render_json_response({:success => false, :message => 'Invalid Amount!'}, :ok)
    end
  end

  def issue_from_settings
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
   if params[:amount].to_f > 0
     if params[:card_token].present? or params[:bank_account_token].present?
         wallet = @user.wallets.first
         wallet_id = wallet.id
         wallet_name = wallet.name
         amount = params[:amount].to_f
         # result = amount
         result = amount.to_s
         if params[:boolValue] == "true"
           if params[:type] == "card" || !params[:type].present?
             charge = Stripe::Charge.create({
                                                :amount => (dollars_to_cents(params[:amount]).to_i),
                                                :currency => "usd",
                                                :customer => @user.stripe_customer_id,
                                                :source => params[:card_token]
                                            })
             transaction = @user.transactions.build(to: wallet_id, from: "stripe", status: "pending", charge_id: charge.id, amount: params[:amount], sender: @user, action: 'issue')
             if transaction.save!
               issue = @user.issue_with_stripe(result,wallet_id, "By Stripe")
               balance = show_balance(wallet_id)
               return render_json_response({success: true, balance:balance}, :ok)
             else
               return render_json_response({:success => false, :message => "Cannot save transaction"},:ok)
             end
           elsif params[:type] == "account"
             charge = Stripe::Charge.create({
                                                :amount => (dollars_to_cents(params[:amount]).to_i),
                                                :currency => "usd",
                                                :source => params[:bank_account_token],
                                                :customer => @user.stripe_customer_id
                                            })
             transaction = @user.transactions.build(to: wallet_id, from: "stripe", status: "pending", charge_id: charge.id, sender: @user, action: 'issue')
             if transaction.save!
               issue = @user.issue_with_stripe(result,wallet_id, "By Stripe")
               balance = show_balance(wallet_id)
               return render_json_response({success: true, balance:balance}, :ok)
             else
               return render_json_response({:success => false, :message => "Cannot save transaction"},:ok)
             end
           end
         else
           parsed_hash = {}
           expiry_date = params[:exp_date]
           exp_month, exp_year = expiry_date.slice!(0...2), expiry_date
           url = "https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do?ssl_merchant_id=#{ENV["converge_merchant_id"]}&ssl_user_id=#{ENV["converge_user_id"]}&ssl_pin=#{ENV["converge_pin"]}&ssl_show_form=false&ssl_result_format=ascii&ssl_card_number=#{params[:card_number]}&ssl_exp_date=20#{exp_year}&ssl_transaction_type=ccgettoken&ssl_cvv2cvc2_indicator=1&cvv=#{params[:cvv]}&ssl_verify=N"
           response = HTTParty.get(url)
           if response.success?
             response.parsed_response.split("\n").map do |item|
               arr = item.split("=")
               parsed_hash[arr[0]] = arr[1]
             end
             if parsed_hash["errorCode"].present?
               return render_json_response({success: false, message: parsed_hash["errorName"]}, :ok)
             else
               transaction = @user.transactions.build(to: wallet_id, from: "evalon", status: "pending",charge_id: parsed_hash["ssl_token"], amount: params[:amount], sender: @user, action: 'issue')
               if transaction.save!
                 issue = @user.issue_with_elvano(result,wallet_id, "By Elvano")
                 balance = show_balance(wallet_id)
                 return render_json_response({success: true, balance:balance}, :ok)
               else
                 return render_json_response({:success => false, :message => "Cannot save transaction"},:ok)
               end
             end
           end
         end
     else
       return render_json_response({success: false, message: 'Incomplete Parameters'},:ok)
     end
   else
     return render_json_response({:success => false, :message => "Amount should not be negative"},:ok)
   end
  end

  def issue_money
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:amount].to_f > 0
      if params[:card_token].present? or params[:bank_account_token].present?
          wallet = @user.wallets.first
          wallet_id = wallet.id
          wallet_name = wallet.name
          amount = params[:amount].to_f
          # result = amount * 100
          result = amount.to_s
          if params[:type] == "card" or !params[:type].present?
            charge = Stripe::Charge.create({
                                               :amount => (dollars_to_cents(params[:amount]).to_i),
                                               :currency => "usd",
                                               :customer => @user.stripe_customer_id,
                                               :card => params[:card_token]
                                            }
            )
          elsif params[:type] == "account"
            charge = Stripe::Charge.create({:amount => (dollars_to_cents(params[:amount]).to_i), :currency => "usd", :customer => params[:bank_account_token]})
            transaction = @user.transactions.build(to: wallet_id, from: "stripe", status: "pending", charge_id: charge.id, action: 'issue', amount: params[:amount])
            if transaction.save
              balance = show_balance(wallet_id)
              return render_json_response({success: true, balance: balance}, :ok)
            else
              return render_json_response({:success => false, :message => "Cannot save transaction"},:ok)
            end
          else
            return render_json_response({:success => false, :message => " params[:type] Type should be account or card"},:ok)
          end

            if params[:denominations].present?
              notes_plus(@user.id,params[:denominations])
            end
            issue = @user.issue_with_stripe(result,wallet_id,"By Stripe")
            balance = show_balance(wallet_id)
            return render_json_response({success: true, balance: balance}, :ok)

      else
        return render_json_response({:success => false, :message => 'cardTokenId is not Provided'}, :ok)
      end
    else
      return render_json_response({:success => false, :message => 'Amount should not be negtive'}, :ok)
    end
  end

  def issue_with_payeezy
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    parsed_hash = {}
    # url = "https://api-cert.payeezy.com/v1/transactions/tokens"
    # response = HTTParty.post(url)
    payload = {
        type: params[:type_token],
        credit_card: {
          type: params[:type],
          cardholder_name: params[:cardholder_name],
          card_number: params[:credit_card],
          exp_date: params[:expiry],
          cvv: params[:cvv]
        },
        auth: false
    }
    url = URI.parse("https://api-cert.payeezy.com/v1/transactions/tokens")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    req = Net::HTTP::Post.new(url)
    req['apikey'] = "seaREnX5W09NYdmiPLsajw8egG0qSApW"
    req['token'] = "fdoa-e90ddc04d5f7577025ade3ab16f7f1d6e90ddc04d5f75770"
    req['Content-type'] = 'application/json'
    req['apisecret'] = "b29599f35d8e8fd6676f72ac4eb1f59b16178b0541776a1c958f1885fc998f94"
    # timestamp
    req['timestamp'] = (Time.now.to_f*1000).ceil
    # secure random code
    @random_code = 16.times.map{rand(10)}.join
    req['nonce'] = @random_code
    # request body
    req.body = payload.to_json
    # data
    credit = ''
    payload_data = payload[:type]
    payload[:credit_card].each do |key, value|
      credit = credit + value
    end
    auth = payload[:auth].to_s
    combine = payload_data + credit + auth
    data = req['apikey'] + @random_code + req['timestamp'] + req['token'] + combine
    # hmac generate
    digest = OpenSSL::Digest.new('sha256')
    hmac = OpenSSL::HMAC.hexdigest(digest, req['apisecret'], data)
    authorization = Base64.strict_encode64(hmac).strip()
    req['Authorization'] = authorization
    puts "Request", JSON.parse(req.to_json)
    res = http.request(req)
    p "RESPONSE:   ", res.body
    # res = Net::HTTP.start(url.host, url.port) {|http|
    #   http.request(req)
    # }
    render :json => res
  end

  def issue
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:amount].to_f > 0
        if params[:denominations].present?
          notes_plus(@user.id,params[:denominations])
        end
        wallet = @user.wallets.first
        wallet_id = wallet.id
        wallet_name = wallet.name
        issue = @user.issue(dollars_to_cents(params[:amount]),wallet_id)
        balance = show_balance(wallet_id)
        return render_json_response({success: true, balance: balance}, :ok)
    else
      return render_json_response({:success => false, :message => 'Amount should not be negtive'}, :ok)
    end
  end

  def withdraw
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:amount].to_f > 0
      wallet = @user.wallets.first
      if wallet.present?
          wallet_name = wallet.name
          wallet_id = wallet.id
          balance = dollars_to_cents(show_balance(wallet.id))
          if balance.to_f >= dollars_to_cents(params[:amount]).to_f
            amount = dollars_to_cents(params[:amount])
            if params[:denominations].present?
              notes_minus(@user.id,params[:denominations])
              if notes_amount >= params[:amount]
                withdraw = @user.withdraw(amount, wallet_id)
              else
                return render_json_response({success: false,message: "Dont have enough balance" }, :ok)
              end
            else
              withdraw = @user.withdraw(amount,wallet_id)
            end
            transactions = all_transactions(wallet_id,wallet_id,params[:page_number])
            balance = balance.to_f - dollars_to_cents(params[:amount]).to_f
            return render_json_response({success: true,balance:balance,wallet: WalletSerializer.new(wallet), wallet_id: wallet_id, wallet_name: wallet_name, withdraw: withdraw,transactions: transactions }, :ok)
          else
            return render_json_response({:success => false, :message => 'You Dont have Enough Balance!'}, :ok)
          end

      else
        return render_json_response({:success => false, :message => 'wallet  does not match'}, :ok)
      end
    else
      return render_json_response({:success => false, :message => "Amount should not be negtive"},:ok)
    end
  end

  def transfer_amount
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:to_id].present? and params[:amount].to_f > 0
        if params[:from_id].present?
          if @user.nil?
            return render_json_response({:success => false, :message => 'Invalid Authentication Token Provided!'}, :ok)
          else
            wallet = Wallet.where(id: params[:from_id]).first
            to_wallet = Wallet.where(id: params[:to_id]).first
            if to_wallet.present?
              if !wallet.nil?
                wallet_id = wallet.id
                wallet_name = wallet.name
                  if params[:platform] === 'ATM'
                    atm_transfer(to_user, from_user)
                  end
                  balance = show_balance(params[:from_id])
                  amount_to_transfer = params[:amount].to_f
                  if balance.to_f >= amount_to_transfer
                    amount = params[:amount]
                    data = ''
                    tx = transaction_between(params[:to_id], params[:from_id], amount,'p2p_payment',"0",data)
                    reciept = to_wallet.users.first
                    message = "#{number_to_currency(params[:amount])} has been successfully transferred to your wallet from #{@user.name}."
                    if params[:message].present? and params[:message] != ""
                      message = message + "\nNote: #{params[:message]}"
                    end
                    if !reciept.nil?
                      twilio_text = send_text_message(message, reciept.phone_number)
                      push_notification(message, reciept)
                    end
                    return render_json_response({
                      success: true,
                      amount_transferred: amount_to_transfer,
                      balance: balance.to_f - amount_to_transfer.to_f
                    }, :ok)
                  else
                    return render_json_response({:success => false, :message => 'User don\'t have enough Balance to complete transaction!'}, :ok)
                  end
              else
                return render_json_response({:success => false, :message => 'Invalid Authentication Token for wallet!'}, :ok)
              end
            else
              return render_json_response({:success => false, :message => 'Reciever not Found!'}, :ok)
            end
          end
        else
          return render_json_response({:success => false, :message => 'Sender is required!'}, :ok)
        end
    else
      return render_json_response({:success => false, :message => 'Receiver is required!'}, :ok)
    end
  end

  def transaction
    if params[:to_id].present? and params[:amount].to_f > 0
        if params[:from_id].present?
          if @user.nil?
            return render_json_response({:success => false, :message => 'Invalid Authentication Token Provided!'}, :ok)
          else
            wallet = Wallet.where(id: params[:from_id]).first
            to_wallet = Wallet.where(id: params[:to_id]).first
            if to_wallet.present?
              if !wallet.nil?
                wallet_id = wallet.id
                wallet_name = wallet.name
                  if params[:platform] === 'ATM'
                    atm_transfer(to_user, from_user)
                  end
                  balance = show_balance(params[:from_id])
                  amount_to_transfer = params[:amount].to_f
                if balance.to_f >= amount_to_transfer
                    amount = params[:amount]
                    data = ''
                    tx = transaction_between(params[:to_id], params[:from_id], amount,'p2p_payment',0, data)
                    transactions = all_transactions(params[:from_id], params[:from_id], 0)
                    reciept = to_wallet.users.first
                    message = "#{number_to_currency(params[:amount])} has been successfully transferred to your wallet from #{@user.name}."
                    if params[:message].present? and params[:message] != ""
                      message = message + "\nNote: #{params[:message]}"
                    end
                    if !reciept.nil?
                      twilio_text = send_text_message(message, reciept.phone_number)
                      push_notification(message, reciept)
                    end
                    return render_json_response({
                      success: true,
                      amount_transferred: number_with_precision(number_to_currency(amount_to_transfer), precision: 2),
                      # balance: cents_to_dollars(balance.to_f - amount_to_transfer.to_f),
                      balance: balance.to_f - amount_to_transfer.to_f,
                      wallet_id: wallet_id,
                      wallet_name: wallet_name,
                      wallet: WalletSerializer.new(wallet),
                      sender_message: twilio_text,
                      transactions: transactions
                    }, :ok)
                  else
                    return render_json_response({:success => false, :message => 'User don\'t have enough Balance to complete transaction!'}, :ok)
                  end
              else
                return render_json_response({:success => false, :message => 'Invalid Authentication Token for wallet!'}, :ok)
              end
            else
              return render_json_response({:success => false, :message => 'Reciever not Found!'}, :ok)
            end
          end
        else
          return render_json_response({:success => false, :message => 'Sender is required!'}, :ok)
        end
    else
      return render_json_response({:success => false, :message => 'Receiver is required!'}, :ok)
    end
  end

  def show_balance_of_wallet
    if params[:wallet_id].present?
      wallet = Wallet.where(id: params[:wallet_id]).first
      if wallet.present?
        balance = show_balance(wallet.id)
        return render_json_response({success: true, balance:balance, wallet_name: wallet.name},:ok)
      else
        return render_json_response({success: false, message: "Wallet Not Found!"},:ok)
      end
    else
      return render_json_response({success: false, message: "Empty Service"},:ok)
    end
  end

  def refund_money
    # return maintenance_response() if  @api_config.fetch("refund") { true }.to_s == 'false'
    if params[:transact_id].present?
      transaction = Transaction.where(seq_transaction_id: params[:transact_id]).first
      transaction = Transaction.where(quickcard_id: params[:transact_id]).first if transaction.blank?
      return render_json_response({success: false, message: "No Transaction found"},:ok) if transaction.blank?
      merchant = transaction.receiver
      refund = RefundHelper::RefundTransaction.new(params[:transact_id],transaction,merchant,params[:partial_amount],params[:reason], @user)
      #check transaction not blank , amount is already refunded or not and get buy rate fee of merchant location and refund transaction to user
      response = refund.process
      return render_json_response(response,:ok)
    end
  end

  def refund_payment_gateway
    if params[:transact_id].present?
      tx = SequenceLib.get_transaction_id(params[:transact_id])
      tx = tx.map do |t|
        {
            amount:t.actions.first.amount.to_f,
            destination_id: t.actions.first.destination_account_id.to_i,
            source_id: t.actions.first.source_account_id.to_i,
            payment_source: t.actions.first.tags["source"],
            type: t.actions.first.type
        }
      end
      if tx.present?
        return render_json_response({success: false, message: "Transaction type not issue."}, :ok) if tx.first[:type] != "issue"
        return render_json_response({success: false, message: "Payment source is not Bridge Pay"}, :ok) if tx.first[:payment_source] != "Bridge Pay"
        transaction = Transaction.where(seq_transaction_id: params[:transact_id]).first
        if transaction.status != "refunded"
          user = Wallet.where(id: tx.first[:destination_id]).first.id
          if tx.first[:destination_id] == user
            refund = CardToken.new
            refund.refund_bridge_pay(transaction.charge, tx.first[:amount])
            seq_transaction = SequenceLib.transfer(cents_to_dollars(tx.first[:amount]),tx.first[:destination_id], tx.first[:source_id], 'refund', 0, nil, tx.first[:payment_source],nil)
            if seq_transaction.present?
              balance = show_balance(user)
              status = "refunded"
              if transaction.update(status: status)
                return render_json_response({success: true, message: "Successfully refunded.", balance: balance},:ok)
              end
            end
          else
            return render_json_response({success: false, message: "Wallet does not belongs to user."},:ok)
          end
        else
          return render_json_response({success: false, message: "Already Refunded."},:ok)
        end
      else
        return render_json_response({success: false, message: "No Transaction found."},:ok)
      end
    else
      return render_json_response({success: false, message: "Please provide Transaction Id."},:ok)
    end
  end

  def balance
    begin
      validate_parameters(
        :auth_token=>params[:auth_token],
        :wallet_id=>params[:wallet_id]
      )
      if @user.merchant? or @user.admin?
        render json: {success: true,balance: SequenceLib.balance(params[:wallet_id]),status: true}
      else
        return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => 'User is not merchant' }, :ok)
      end
    rescue Exception => e
      return handle_standard_error(e)
    end  
  end

  #Todo Need to add descriptor and payment_gateway in response and handle dynamicaly payment gateways
  def virtual_debit_charge
    #return maintenance response
    return sequence_down() if @seq_down.present? && @seq_down == true
    return maintenance_response() if  @api_config.fetch("virtual_debit_charge") { true }.to_s == 'false'
    #  api for direct sequence transactions into merchant wallet
    begin
       @eaze_reporting = true
      @cultivate_params = true
      #raise error if auth_token is not exist
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.wallet.auth_token')},:ok) if params[:auth_token].nil?
      #raise error if location_id and wallet_id blank
      if params[:location_id].nil?
        if params[:wallet_id].nil?
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.wallet.loc_miss')},:ok)
        end
      end
      #check amount should be greater than 0 otherwise raise error, last four digits
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.wallet.amount_miss')},:ok) if params[:amount].nil?
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.wallet.card_miss')},:ok) if params[:last4].nil?

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.amount_error')},:ok) if params[:amount].to_f <= 0
      #get user from auth token
      user = User.authenticate_by_token(params[:auth_token]).first
      #check user exist
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.wallet.user_not_merchant')},:ok) if !user.merchant?
      issue_raw_transaction = IssueRawTransaction.new(nil,nil)
      #get wallet and location from location id or wallet id
      if params[:location_id].present?
        location = Location.where(location_secure_token: params[:location_id]).first
        # return render_json_response({success: false, message: "Sorry but Location is currently not available. Please contact you administrator for details."}, :ok) if location.nil?
        wallet = location.wallets.primary.first
      else
        wallet = Wallet.find_by(id: params[:wallet_id])
        location = wallet.location
      end
      @location = location
      params[:merchant_wallet_id] = wallet.id
      #check user exist in wallet users list
      unless wallet.users.pluck(:id).include?(@user.id)
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.wrong_user')}, :ok)
      end
      #get merchant total balance from sequence library by giving wallet id
      merchant_balance = show_balance(wallet.id, user.ledger)
      #get buy_rate fee of location to calculate amount which transfer to merchant wallet
      buyRate = location.fees.buy_rate.first
      tx_amount = params[:amount].to_f
      #add privacy fee into amount if exist
      if params[:privacy_fee].present?
        privacy_fee = params[:privacy_fee].to_f
        params[:amount] = params[:amount].to_f + privacy_fee
      end

      #get amount fee check for merchant iso agnet and affiliate
      if params[:payment_type].present?
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.wallet.payment_type')}, :ok) if params[:payment_type].downcase != 'credit' && params[:payment_type].downcase != 'debit'
        amount_fee_check = check_fee_amount(location, user, nil, wallet, buyRate, params[:amount], merchant_balance, TypesEnumLib::CommissionType::C2B, params[:payment_type].downcase)
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Transaction processing failed: The amount $#{params[:amount]} is less than the minimum amount required to process a transaction. Kindly contact administrator."}, :ok)if amount_fee_check.present? && amount_fee_check[:status] == false
        location_fee = issue_raw_transaction.get_location_fees(wallet.id, user, params[:amount], nil, params[:payment_type].downcase)
      else
        amount_fee_check = check_fee_amount(location, user, nil, wallet, buyRate, params[:amount], merchant_balance, TypesEnumLib::CommissionType::C2B, 'debit')
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Transaction processing failed: The amount $#{params[:amount]} is less than the minimum amount required to process a transaction. Kindly contact administrator."}, :ok)if amount_fee_check.present? && amount_fee_check[:status] == false
        location_fee = issue_raw_transaction.get_location_fees(wallet.id, user, params[:amount], params[:amount], 'debit')
      end
      if amount_fee_check.present? && amount_fee_check[:status] == true
        if amount_fee_check[:splits].present?
          agent_fee = amount_fee_check[:splits]["agent"]["amount"]
          iso_fee = amount_fee_check[:splits]["iso"]["amount"]
          iso_balance = show_balance(location.iso.wallets.primary.first.id)
          if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
            if iso_balance < iso_fee
              return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('errors.iso.iso_0007')}, :ok)
            end
          else
            if iso_balance + iso_fee < agent_fee.to_f
              return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('errors.iso.iso_0008')}, :ok)
            end
          end
        end
      end
      params[:location_fee] = location_fee
      tip_details = {}
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t("errors.withdrawl.blocked_location")}, :ok) if location.is_block
      if location.virtual_debit_api.present? && location.virtual_debit_api
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.unavailable')}, :ok)
      end
       if params[:tip].present?
         params[:tip] = params[:tip].to_f
       end
      if params[:tip].present? && params[:tip] > 0
        tip_wallet = location.wallets.tip.first
        if tip_wallet.present?
          tip_details = {:wallet_id => tip_wallet.id, :sale_tip => params[:tip].to_f}
          params[:amount] = params[:amount].to_f + params[:tip].to_f
        end
      end
      #create sales_info object and merge terminal_id transaction_number payment_type card_type and tpn
      sales_info = {}
      if params[:terminal_id].present?
        sales_info = {
            terminal_id: params[:terminal_id]
        }
      end
      if params[:transaction_number].present?
        sales_info = sales_info.merge({transaction_number: params[:transaction_number]})
      end
      if params[:payment_type].present?
        sales_info = sales_info.merge({payment_type: params[:payment_type]})
      end
      if params[:card_type].present?
        sales_info = sales_info.merge({card_type: params[:card_type]})
      end
      if params[:tpn].present?
        sales_info = sales_info.merge({tpn: params[:tpn]})
      end
      # if !user.issue_fee
      #   transaction = SequenceLib.issue(params[:wallet_id],params[:amount],nil, "sales", 0, nil, params[:bank], nil, params[:last4], user.name, nil, nil, get_ip, nil, user.ledger, nil, nil, tip_details, sales_info)
      # else
      # qc_wallet = Wallet.escrow.first
      #get first wallet of type escrow
      qc_wallet = Wallet.escrow.first
      params[:ref_id] = params[:RefId] if params[:RefId].present?
      first6 = params[:first6] if params[:first6].present?
      last4 = params[:last4] if params[:last4].present?
      #check ref_id exist or correct
      if params[:ref_id].present?
        sales_info = sales_info.merge({ref_id: params[:ref_id]})
        #get transaction with pending status and given re_id
        balance = SequenceLib.balance(wallet.id).to_f
        trans = Transaction.where(ref_id: params[:ref_id],status: "pending")
        if trans.present?
          if params[:slot_1].present? && params[:slot_1] == "approved"
            gateway = PaymentGateway.where(slot_name: 'slot_1').first
          elsif params[:slot_2].present? && params[:slot_2] == "approved"
            gateway = PaymentGateway.where(slot_name: 'slot_2').first
          end
          transaction_db = trans.last
          if transaction_db.clerk_id.present?
            sales_info = sales_info.merge({clerk_id: transaction_db.clerk_id})
            clerk_id = transaction_db.clerk_id
          end
          old_amount = transaction_db.total_amount.to_f
          # if old_amount.to_f != params[:amount].to_f
          #   discount = old_amount.to_f - params[:amount].to_f
          #   if discount.to_f > 0
          #     if transaction_db.privacy_fee.present? && transaction_db.privacy_fee.to_f >= discount.to_f
          #       transaction_db.privacy_fee = transaction_db.privacy_fee.to_f - discount.to_f
          #     end
          #   end
          # end
          if params[:privacy_fee].blank?
            privacy_fee = transaction_db.privacy_fee.to_f
          end
          if params[:tip].present? && params[:tip] == 0
            params[:tip] = transaction_db.tip.to_f
            tip_wallet = location.wallets.tip.first
            if tip_wallet.present?
              tip_details = {:wallet_id => tip_wallet.id, :sale_tip => params[:tip].to_f}
            end
          end
          gbox_fee = save_gbox_fee(location_fee.try(:second), location_fee.try(:third).to_f) || 0
          total_fee = save_total_fee(location_fee.try(:second), location_fee.try(:third).to_f) || 0
          transaction_db.total_amount = params[:amount]
          transaction_db.net_amount = params[:amount].to_f - total_fee.to_f - location_fee.try(:first).try(:[],"amount").to_f - transaction_db.tip.to_f
          transaction_db.fee = total_fee
          transaction_db.net_fee = total_fee.to_f - transaction_db.privacy_fee.to_f
          transaction_db.reserve_money = location_fee.try(:first).try(:[],"amount").to_f
          transaction_db.payment_gateway_id = gateway.try(:id)
          # transaction_db.discount = discount.to_f
          transaction_db.gbox_fee = gbox_fee.to_f
          transaction_db.iso_fee = save_iso_fee(location_fee.try(:second))
          transaction_db.agent_fee = location_fee.try(:second).try(:[],"agent").try(:[],"amount").try(:to_f)
          transaction_db.affiliate_fee = location_fee.try(:second).try(:[],"affiliate").try(:[],"amount").try(:to_f)
          transaction_db.terminal_id = params[:terminal_id]
          transaction_db.receiver_balance = balance
        else
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Wrong RefId, Please try with correct RefId"},:ok)
        end
      else
        if params[:clerk_id].present?
          sales_info = sales_info.merge({clerk_id: params[:clerk_id]})
          clerk_id = params[:clerk_id]
        end
        gbox_fee = save_gbox_fee(location_fee.try(:second), location_fee.try(:third).to_f) || 0
        total_fee = save_total_fee(location_fee.try(:second), location_fee.try(:third).to_f) || 0
        transaction_db = Transaction.create(to: wallet.id,
                                                  from: nil,
                                                  status: "pending",
                                                  amount: tx_amount,
                                                  receiver: user,
                                                  receiver_name: user.try(:name),
                                                  action: 'issue',
                                                  receiver_wallet_id: wallet.id,
                                                  first6: first6,
                                                  last4: last4,
                                                  total_amount: params[:amount],
                                                  net_amount: params[:amount].to_f - total_fee.to_f - location_fee.try(:first).try(:[],"amount").to_f - params[:tip].to_f,
                                                  main_type: updated_type(TypesEnumLib::TransactionType::SaleType, TypesEnumLib::GatewayType::PinGateway),
                                                  fee: total_fee.to_f,
                                                  net_fee: total_fee.to_f - params[:privacy_fee].to_f,
                                                  tags: location_fee.try(:second),
                                                  reserve_money: location_fee.try(:first).try(:[],"amount"),
                                                  tip: params[:tip].to_f,
                                                  privacy_fee: params[:privacy_fee].nil? ? 0.0 : params[:privacy_fee].try(:to_f),
                                                  clerk_id: clerk_id,
                                                  gbox_fee: gbox_fee.to_f,
                                                  iso_fee: save_iso_fee(location_fee.try(:second)),
                                                  agent_fee: location_fee.try(:second).try(:[],"agent").try(:[],"amount").try(:to_f),
                                                  affiliate_fee: location_fee.try(:second).try(:[],"affiliate").try(:[],"amount").try(:to_f),
                                                  terminal_id: params[:terminal_id],
                                                  receiver_balance: balance
        )
      end
      params[:transaction_id] = transaction_db.id
      if params[:status].present?
        status = params[:status].downcase == "declined" ? true : false
        if status
          build_decline("error")
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Declined!"},:ok)
        end
      end
      amount_transfer = params[:amount].to_f - params[:tip].to_f
      transaction = SequenceLib.issue(wallet.id,amount_transfer,qc_wallet, TypesEnumLib::TransactionType::SaleType, location_fee.third, nil,gateway.try(:name) || TypesEnumLib::GatewayType::PinGateway, nil, params[:last4], user.name, nil, location_fee.second, get_ip, nil, user.ledger, nil, location_fee.first, tip_details, sales_info, nil, "debit",TypesEnumLib::TransactionSubType::DebitCharge, nil, nil, nil, nil, privacy_fee)
      if transaction.blank?
        return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok)
      end
      # sequence_balance = show_balance(wallet.id,nil)
      # new_balance = number_with_precision(sequence_balance.to_f, precision: 2)
      #update transaction status and creating block transactions
      if transaction.present?
        transaction_db.status = "approved"
        transaction_db.seq_transaction_id = transaction.present? ? transaction.actions.first.id : nil
        transaction_db.tags = transaction.actions.first.tags
        transaction_db.timestamp = transaction.timestamp
        transaction_db.save
        amount= params[:amount].to_f - transaction_db.tip.to_f
        HoldInRear.hold_money_in_my_wallet(wallet.id, amount,location_fee.try(:first).try(:[],"amount"))
        #= creating block transaction
        parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
        # Transaction for tip transfer
        if tip_details.present?
          merchant = user
          tip_block_txn = parsed_transactions.last
          tip_block_txn[:tags]["sequence_id"] = tip_block_txn[:seq_parent_id]
          create_transaction_helper(merchant, tip_details[:wallet_id], wallet.id, user, tip_block_txn[:receiver], tip_block_txn[:action], nil, nil, nil, nil, nil, tip_block_txn[:net_amount], tip_block_txn[:net_amount], nil, tip_block_txn[:type], tip_block_txn[:sub_type], tip_block_txn[:tags], nil, nil, tip_block_txn[:gateway], nil, nil,"approved", tip_block_txn[:id], tip_block_txn[:timestamp])
        end
        reserve_detail = location_fee.try(:first)
        if reserve_detail.present?
          merchant = user
          reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
          if reserve_tx.present?
            create_transaction_helper(merchant, reserve_detail["wallet"], wallet.id, merchant, merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail["amount"].to_f, reserve_detail["amount"].to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, nil, nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
          end
        end
        save_block_trans(parsed_transactions) if parsed_transactions.present?
        order_bank = OrderBank.create(merchant_id: user.id, bank_type: TypesEnumLib::DeclineTransacitonTpe::Debit,card_type: "debit_pin",status: "approve",amount: params[:amount].to_f, transaction_id: transaction_db.id, payment_gateway_id: gateway)
        return render_json_response({:success => true, :status => TypesEnumLib::Statuses::Success, :message => "Approved!",seq_transaction_id:transaction.actions.first.id, clerk_id: clerk_id},:ok)
      end
    rescue Sequence::APIError => e
      build_decline(e)
      return handle_sequence_error(e)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def create_merchant_bank
    return maintenance_response() if  @api_config.fetch("create_merchant_bank") { true }.to_s == 'false'
    begin
      validate_parameters(
          :legal_name => params[:legal_name],
          :email => params[:email],
          :dba_name => params[:dba_name],
          :tax_id => params[:tax_id],
          :zip => params[:zip],
          :ownershiptype => params[:ownershiptype],
          :businesstype => params[:businesstype],
          :business_phone_number => params[:business_phone_number],
          :business_website => params[:business_website],
          :process_annually => params[:process_annually],
          :contact_title => params[:contact_title],
          :contact_name => params[:contact_name],
          :contact_number => params[:contact_number],
          :contact_email => params[:contact_email],
          :bank_account_dda_type => params[:bank_account_dda_type],
          :bank_account_ach_type => params[:bank_account_ach_type],
          :account_number => params[:account_number],
          :routing_number => params[:routing_number],
          :DateofIncorporation => params[:DateofIncorporation]
      )
      owner = ['Government','LLC','NonProfit','Partnership', 'PrivateCorporation','PublicCorporation','SoleProprietorship']
      raise StandardError.new "Please select correct ownershiptype, ex: 'Government','LLC','NonProfit','Partnership', 'PrivateCorporation','PublicCorporation','SoleProprietorship'" unless owner.include? params[:ownershiptype]

      business = ['Auto Rental','ECommerce','Lodging','MOTO','Restaurant','Retail']
      raise StandardError.new "Please select correct businesstype, ex: 'Auto Rental','ECommerce','Lodging','MOTO','Restaurant','Retail'" unless business.include? params[:businesstype]

      ddatype = ['Checking', 'Savings']
      raise StandardError.new "Please select correct bank_account_dda_type, ex: 'Checking', 'Savings'" unless owner.include? params[:bank_account_dda_type]

      achtype = ['CommercialChecking', 'PrivateChecking']
      raise StandardError.new "Please select correct bank_account_ach_type, ex: 'CommercialChecking', 'PrivateChecking'" unless owner.include? params[:bank_account_ach_type]

      merchant = User.find_by(email: params[:email], low_risk: true)
      raise StandardError.new "Merchant already exist!" if merchant.present? && merchant.low_risk_status == "complete"

      begin
        date = params[:DateofIncorporation].to_date
      rescue
        raise StandardError.new "Invalid Date. Please use dd/mm/yyyy format"
      end
      tax_id = params[:tax_id].gsub(/[^0-9]/, '')
      raise StandardError.new "Tax ID must be 9 digit long." if tax_id.size != 9

      zip = params[:zip].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect zip" if zip.size < 5

      valid_email = EmailValidator.valid?(params[:email])
      raise StandardError.new("Invalid Email Format") unless valid_email

      business_website = URI.parse(params[:business_website])
      raise StandardError.new("Invalid Email Format") unless business_website.kind_of?(URI::HTTP || URI::HTTPS)

      business_phone_number = params[:business_phone_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect business phone number" if business_phone_number.size != 10

      contact_number = params[:contact_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect contact number" if contact_number.size != 10

      contact_email = URI.parse(params[:contact_email])
      raise StandardError.new("Invalid Email Format") unless contact_email

      routing_number = params[:routing_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect routing number" if routing_number.size != 9

      account_number = params[:account_number].gsub(/[^0-9]/, '')
      raise StandardError.new "Incorrect account number" if account_number.size > 17

      gateway = PaymentGateway.ficer.first
      if merchant.blank?
        merchant = User.create(name: params[:legal_name], email: params[:email], phone_number: params[:business_phone_number], zip_code: params[:zip], owner_name: params[:ownershiptype], password: 'Quickcard-2019', password_confirmation: 'Quickcard-2019',role: "merchant", low_risk: true, country: params[:country], street: params[:street1], city: params[:city], state: params[:state],low_risk_status: "pending")
        if merchant.present?
          location = Location.create( business_name: params[:dba_name], first_name: params[:contact_title], last_name: params[:contact_name],
                                      email: params[:contact_email], web_site: [params[:business_website]], trip_business_type: params[:businesstype],
                                      years_in_business: params[:DateofIncorporation], city: params[:contact_city], state: params[:contact_state],
                                      zip: params[:contact_zip], tax_id: params[:tax_id], phone_number: params[:contact_number],
                                      cs_number: params[:contact_number], merchant_id: merchant.id, monthly_processing_volume: params[:process_annually],
                                      dda_type: params[:bank_account_dda_type], bank_account: params[:account_number],
                                      bank_routing: params[:routing_number], ach_type: params[:bank_account_ach_type],
                                      primary_gateway_id: gateway.id, risk: :low
          )
          location.fees << gateway.fee
        end
      else
        merchant.update(name: params[:legal_name], phone_number: params[:business_phone_number], zip_code: params[:zip], owner_name: params[:ownershiptype], country: params[:country], street: params[:street1], city: params[:city], state: params[:state],low_risk_status: "pending")
        location = Location.find_by(merchant_id: merchant.id)
        if location.present?
          location.update( business_name: params[:dba_name], first_name: params[:contact_title], last_name: params[:contact_name],
                           email: params[:contact_email], web_site: [params[:business_website]], trip_business_type: params[:businesstype],
                           years_in_business: params[:DateofIncorporation], city: params[:contact_city], state: params[:contact_state],
                           zip: params[:contact_zip], tax_id: params[:tax_id], phone_number: params[:contact_number],
                           cs_number: params[:contact_number], monthly_processing_volume: params[:process_annually],
                           dda_type: params[:bank_account_dda_type], bank_account: params[:account_number], bank_routing: params[:routing_number],
                           ach_type: params[:bank_account_ach_type], risk: :low
          )
        end
      end

      customFields = []

      # optional
      # params[:country], params[:street1], params[:street2], params[:city], params[:state]"
      # params[:contact_middle_name], params[:contact_last_name]
      # params[:contact_country], params[:contact_street1], params[:contact_street2], params[:contact_city], params[:contact_state], params[:contact_zip]

      customFields.push({"Id": 7788, "UserDefinedId": "legal.name", "Value": {"name": "#{params[:legal_name]}"}},
                        {"Id": 7789, "UserDefinedId": "legal.dba", "Value": {"dba": "#{params[:dba_name]}"}},
                        {"Id": 7790, "UserDefinedId": "legal.address", "Value": {"Country": "#{params[:country]}", "Street1": "#{params[:street1]}", "Street2": "#{params[:street2]}", "City": "#{params[:city]}", "State": "#{params[:state]}", "Zip": "#{params[:zip]}"}},
                        {"Id": 7791, "UserDefinedId": "legal.ownershiptype", "Value": {"ownershiptype": "#{params[:ownershiptype]}"}},
                        {"Id": 7792, "UserDefinedId": "legal.taxid", "Value": {"taxid": params[:tax_id]}},
                        {"Id": 7793, "UserDefinedId": "Legal.DateofIncorporation", "Value": {"Month": "#{date.month}", "Day": "#{date.day}", "Year": "#{date.year}"}},
                        {"Id": 7794, "UserDefinedId": "legal.phone", "Value": {"business_phone_number": "#{params[:business_phone_number]}"}},
                        {"Id": 7795, "UserDefinedId": "legal.email", "Value": {"business_email": "#{params[:email]}"}},
                        {"Id": 7796, "UserDefinedId": "legal.website", "Value": {"business_website": "#{params[:business_website]}"}},
                        {"Id": 7797, "UserDefinedId": "bank.routingnumber", "Value": {"routing_number": "#{params[:routing_number]}"}},
                        {"Id": 7798, "UserDefinedId": "bank.routingnumber.confirm", "Value": {"confirm_routing_number": "#{params[:routing_number]}"}},
                        {"Id": 7799, "UserDefinedId": "bank.acctnumber", "Value": {"account_number": "#{params[:account_number]}"}},
                        {"Id": 7800, "UserDefinedId": "bank.acctnumber.confirm", "Value": {"confirm_account_number": "#{params[:account_number]}"}},
                        {"Id": 7827, "UserDefinedId": "pc.name", "Value": {"FirstName": "#{params[:contact_name]}", "MiddleName": "#{params[:contact_middle_name]}", "LastName": "#{params[:contact_last_name]}"}},
                        {"Id": 7828, "UserDefinedId": "pc.title", "Value": {"title": "#{params[:contact_title]}"}},
                        {"Id": 7829, "UserDefinedId": "pc.address", "Value": {"Country": "#{params[:contact_country]}", "Street1": "#{params[:contact_street1]}", "Street2": "#{params[:contact_street2]}", "City": "#{params[:contact_city]}", "State": "#{params[:contact_state]}", "Zip": "#{params[:contact_zip]}"}},
                        {"Id": 7830, "UserDefinedId": "pc.email", "Value": {"contact_email": "#{params[:contact_email]}"}},
                        {"Id": 7831, "UserDefinedId": "pc.phone", "Value": {"contact_number": "#{params[:contact_number]}"}},
                        {"Id": 7832, "UserDefinedId": "bank.ach", "Value": {"ach": "#{params[:bank_account_ach_type]}"}},
                        {"Id": 7833, "UserDefinedId": "legal.annualprocessing", "Value": {"process_annually": "#{params[:process_annually]}"}},
                        {"Id": 7864, "UserDefinedId": "legal.businesstype", "Value": {"businesstype": "#{params[:businesstype]}"}},
                        {"Id": 7865, "UserDefinedId": "bank.dda", "Value": {"dda": "#{params[:bank_account_dda_type]}"}}
      )

      low_risk_details = {}

      data = {"AuthenticationKeyId": "4a128d57-27bf-4531-873b-24a193318b56",
              "AuthenticationKeyValue": "g0-0jjfR_oEM~msJbv1CBFPxqHmt49M25_c~Gp~G*4^qHWkhm7M1f7A3o.oMhA5s",
              "Merchant_EmailAddress": params[:email],
              "CustomFieldAnswers": customFields}

      url = "https://merchantapp.io/greenboxtest/api/v1/MerchantApplication/Submit"
      response = RestClient.post(url,data.to_json,{content_type: :json, accept: :json})
      if response.present?
        response = JSON(response.body)

        low_risk_details[:status] = response["Status"]
        low_risk_details[:status_message] = response["StatusMessage"]
        low_risk_details[:merchant_application_id] = response.try(:[],"MerchantApplicationId")
        low_risk_details[:external_merchant_application_id] = response.try(:[],"ExternalMerchantApplicationId")
        low_risk_details[:infinicept_application_id] = response.try(:[],"InfiniceptApplicationId")
        if response["Errors"].present?
          message = response["Errors"].pluck("Message").join(' ')
          low_risk_details[:error_message] = message
          raise StandardError.new "#{message}"
        else
          merchant.low_risk_status = "complete"
        end
      end
      return render_json_response({:success => true, :status => TypesEnumLib::Statuses::Success, :MerchantApplicationId => response["MerchantApplicationId"], :InfiniceptApplicationId => response["InfiniceptApplicationId"], :message => response["StatusMessage"]},:ok)
    rescue StandardError => exc
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message},:ok)
    ensure
      if merchant.present?
        merchant.low_risk_details = low_risk_details
        merchant.save
      end
    end
  end

  private

  def build_decline(e)
    #= building hash for Decline
    transaction_info = {amount: params[:amount].to_f}
    reserve_detail = params[:location_fee].first if params[:location_fee].present?
    user_info = params[:location_fee].second if params[:location_fee].present?
    fee = params[:location_fee].third if params[:location_fee].present?
    approved_slot = params[:approved_slot] if params[:approved_slot].present?
    declined_slot = params[:declined_slot] if params[:declined_slot].present?
    transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info,declined_slot: declined_slot, approved_slot: approved_slot})
    #= end building process
    OrderBank.create(merchant_id: @user.id, bank_type: TypesEnumLib::DeclineTransacitonTpe::Debit,card_type: "debit_pin",status: "decline",amount: params[:amount].to_f,transaction_info: transaction_info.to_json, transaction_id: params[:transaction_id] )
  end

  def qr_params
    params.permit(:image, :user_id)
  end

  def allow_if_active
    if params[:action] == 'scanner'
      if params[:to_id].present?
        wallet = @user.wallets.where(id: params[:to_id]).first
        location = wallet.location
      elsif params[:location_secure_token].present? && params[:qr_token].present?
        location=Location.where(location_secure_token: params[:location_secure_token]).first
      end
      unless location.nil?
        if location.is_block
          return render_json_response({success: false , :message => 'Location can not be accessed'}, :ok)
        end
      end
    end
  end
  # ------------------------ Refund Debit Transaction------------------------------
  def refund_debit_transaction(db_transaction,transaction)
    amount=cents_to_dollars(transaction["amount"]).to_f
    name="NA"
    name=@user.name if @user.present?
    name=@current_user.name if @current_user.present?
    lastFour=transaction["tags"]["last4"] ? transaction["tags"]["last4"] : "NA"
    source=transaction["tags"]["source"] ? transaction["tags"]["source"].downcase : "NA"
    #----------------------------Perform Sequence Retire Refund--------------------------------
    seq_transaction = SequenceLib.retire(amount,transaction["destination_account_id"], 'refund',0,0,source,nil,name,nil,lastFour)
    if seq_transaction.present?
      if db_transaction.update(status: 'refunded')
        return [true,"Successfully Refunded"]
      end
    else
      return [false,"Sequence Refund Unsuccessful"]
    end
  end

  private

  def update_log(transaction,log)
    if transaction.refund_log.nil?
    else
      if JSON.parse(transaction.refund_log).kind_of?(Array)
        temp = JSON.parse(transaction.refund_log)
        temp.last["sequence_id"] = log
      else
        temp = JSON.parse(transaction.refund_log)
        temp["sequence_id"] = log
      end
      transaction.refund_log= temp.to_json
      transaction.save
    end
  end
end
