class Api::AffiliateProgramController < Api::ApiController
	def affiliate
		begin

			validate_parameters(
				contact_name: params[:contact_name],
          		contact_email_address: params[:contact_email_address],
          		contact_phone_number: params[:contact_phone_number],
          		street_address: params[:street_address],
          		city: params[:city],
          		zip_code: params[:zip_code],
          		months_in_business: params[:months_in_business]
			)

    		admin_user = @user
    		return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'Un-authorized Credentials: Please contact to administrator for more information!'}, :ok) unless @user.admin_user?
	      	
	      	email = params[:contact_email_address].downcase
      		email_user = User.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(email: email).first
      		return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'The email you are trying is already in use, please try a new one'}, :ok) if email_user.present?
	      	
	      	phone_number = params[:contact_phone_number]
	      	phone_number = "1" + phone_number if phone_number.length == 10
	      	phone_user = User.where.not(role: I18n.t("role.name.user")).complete_profile_users.where(phone_number: phone_number).first
	      	return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'The Phone you are trying is already in use, please try a new one'}, :ok) if phone_user.present?
	      	return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'Invalid Phone'}, :ok) if phone_number.length < 9 
	      	
	      	params[:user] = {"email"=>params[:contact_email_address],"name"=>params[:contact_name],"phone_number"=>phone_number,"profile_attributes"=>{"company_name"=>params[:company_name],"street_address"=>params[:street_address],"country"=>params[:country],"state"=>params[:state],"city"=>params[:city],"zip_code"=>params[:zip_code],"tax_id"=>params[:tax_id],"ein"=>params[:social_security_number]}}
	      	params[:user][:profile_attributes][:years_in_business] = params[:years_in_business] +"-"+ params[:months_in_business]
			@affiliate_program = User.affiliate_program.new(affiliate_program_params)
			@affiliate_program.company_name = params[:user][:profile_attributes][:company_name]
			@affiliate_program.is_block = false
			password = random_password
	    	@affiliate_program.password = password
	    	@affiliate_program.is_password_set = false
	    	@affiliate_program.admin_user_id = admin_user.id
	    	# @affiliate_program.system_fee = admin_user.system_fee
	    	if @affiliate_program.save
	    		# @affiliate_program.send_welcome_email("affiliate_program")
	    		UserMailer.welcome_affiliate(@affiliate_program,"Welcome to Quick Card","affiliate_program",password).deliver_later
				return render_json_response({:success => true , message: "affiliate created!" , AP_ID: @affiliate_program.id }, :ok)
			else
				return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message =>  @affiliate_program.errors.messages}, :ok) 
			end
		rescue Exception => e
			return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => e.message}, :ok) 
		end
	end

	def deposit
		begin
			validate_parameters(
          		affiliate_id: params[:affiliate_id],
          		amount: params[:amount]
			)
    		admin_user = @user
    		return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'Un-authorized Credentials: Please contact to administrator for more information!'}, :ok) unless @user.admin_user?
	      	affiliate_program = admin_user.affiliate_programs.find_by(id: params[:affiliate_id])  
			return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'Unable to retrieve affiliate program under this admin user! please check your keys'}, :ok) if affiliate_program.blank?
			return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.archived_affiliate_program')}, :ok) if affiliate_program.is_block || affiliate_program.archived?		
		    app = affiliate_program.oauth_apps.last
			return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'Affiliate Blocked: Please contact to administrator for more information!'}, :ok) if app.present? && app.is_block
			wallet =  affiliate_program.wallets.first  
			# qc_wallet = Wallet.escrow.first
			transaction_db = Transaction.create(to: wallet.id,
                                              from: nil,
                                              status: "pending",
                                              amount: params[:amount],
                                              receiver: affiliate_program,
                                              receiver_name: affiliate_program.try(:name),
                                              action: 'issue',
                                              receiver_wallet_id: wallet.id,
                                              total_amount: params[:amount],
                                              net_amount: params[:amount].to_f,
                                              main_type: TypesEnumLib::TransactionType::AFP,
                                              receiver_balance: SequenceLib.balance(wallet.id)
    		)
			transaction = SequenceLib.issue(wallet.id,params[:amount],nil,TypesEnumLib::TransactionType::AFP, nil, nil, admin_user.name)
			if transaction.present? && transaction.id.present?
		    	transaction_db.update(seq_transaction_id: transaction.present? ? transaction.actions.first.id : nil, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
		        parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
		        save_block_trans(parsed_transactions) if parsed_transactions.present?
		        return render_json_response({:success => true ,transaction_id: transaction_db.seq_transaction_id, message: "Amount successfully issued!"}, :ok)
		    else
		        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => 'Amount can not be issuing'}, :ok) 
		    end
		rescue Exception => e
			return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => e.message}, :ok) 
		end
	end

	def affiliate_program_params
    	params.require(:user).permit(:id, :email, :password, :password_confirmation, :name, :phone_number, :company_name , profile_attributes: [:id , :company_name, :years_in_business, :tax_id, :street_address, :ein, :country, :city, :state, :zip_code ,:_destroy])
  	end
end