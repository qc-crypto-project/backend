class Api::ApiController < ActionController::Base
  include ActionView::Helpers::NumberHelper
  include ApplicationHelper
  require 'resolv'
  FATAL_EXCEPTIONS = [UncaughtThrowError, NoMethodError, NameError, RangeError, TypeError, ThreadError, ScriptError, SecurityError]

  before_action :authenticate_user
  # after_action :record_it
  #
  before_action :set_request_data

  def set_request_data
    RequestInfo.current_request(request, current_user || nil )
  end

  def location_ip_policy
    begin
      params[:ip_details] = {}
      if params[:location_id].present?
        location = Location.where(location_secure_token: params[:location_id]).first
      elsif params[:wallet_id].present?
        wallet = Wallet.find_by(id: params[:wallet_id])
        location = wallet.location if wallet.present?
      end
      if location.blank?
        return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.declined_msg')}, :ok) if params[:location_id].present? && !params[:wallet_id].present?
        return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.wrong_user')}, :ok) if params[:wallet_id].present? && !params[:location_id].present?
      end
      if @user.oauth_apps.present?
        oauth_app = @user.oauth_apps.first
        return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.blocked_user') }, :ok) if oauth_app.is_block
      end
      if location.block_api #if api blocked for location
        if params["action"] == "virtual_terminal_transaction" || params["action"] == "virtual_transaction_card"
          return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.disabled_apis') }, :ok) if location.virtual_terminal_transaction_api
        elsif params["action"] == "virtual_transaction" || params["action"] == "virtual_existing_signup" || params["action"] == "virtual_new_signup"
          return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.disabled_apis')  }, :ok) if location.virtual_transaction_api
        elsif params["action"] == "virtual_debit_charge" || params["action"] == "virtual_terminal_transaction_debit"
          return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.disabled_apis') }, :ok) if location.virtual_debit_api
        end
      end
      return render_json_response({ :success => false,:status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.bad_parameters') }, :ok) unless location.present?
      if (location.present? && location.is_block) || (location.web_site.blank?)
        return render_json_response({ :success => false, :message => I18n.t("errors.withdrawl.blocked_location") }, :ok)
      end
      if !!location.block_ip
        validUrls = []
        locationUrls = valid_json?(location.web_site) ? JSON.parse(location.web_site) : [location.web_site]
        locationUrls.each do |httpUrl|
          return unless httpUrl.present?
          url = URI.parse(httpUrl).host.try(:downcase)
          return unless url.present?
          if !!(url =~ Resolv::IPv4::Regex)
            validUrls << url
            names = Resolv.getnames(url).map { |domain| urlDomain(domain) }
            validUrls.concat(names)
            names.each { |name| validUrls.concat(Resolv.getaddresses(name)) }
          else
            ips = Resolv.getaddresses(url)
            validUrls.concat(ips)
            ips.each { |ip| validUrls.concat(Resolv.getnames(ip)) }
            validUrls << urlDomain(url) if url.present?
          end
        end
        requestIps = []
        clientIps = [request.ip, request.remote_ip].uniq
        clientIps.each { |ip| requestIps.concat(Resolv.getnames(ip)) }
        requestIps = requestIps.map { |domain| urlDomain(domain) }
        requestIps.concat(clientIps)
        params[:ip_details] = {valid_ips: requestIps, client_ips: clientIps}
        return render_json_response({ :success => false, :message => I18n.t('api.errors.blocked_ips') }, :ok) if (validUrls & requestIps).length <= 0
      end
    rescue StandardError => exc
      return handle_standard_error(exc, I18n.t('api.errors.blocked_ips'))
    end
  end

  private

  def handle_sequence_error(e)
    mask_card_param
    response = JSON.parse(e.response.body)
    action_error = response['data']['actions'][0]
    error = {Exception: response.to_s, URL: request.url.to_s, PARAMS: request.params.to_s , BACKTRACE: action_error.to_s}
    unless request.host != 'localhost'
      Raven.capture_exception(e)
      SlackService.notify(error.to_s)
    end

    return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.bad_parameters')}, :ok)
  end

  def handle_standard_error(exc, message = nil)
    mask_card_param
    notifyTitle = "Internal Server Error"
    if FATAL_EXCEPTIONS.include?(exc.class) && request.host != 'localhost'
      if request.params[:card_number].present?
        request.params[:card_number] = "XXXX-X#{params[:card_number].last(8)}"
      end
      Raven.capture_exception(exc)
      back_trace = exc.backtrace.select { |x| x.match(Rails.application.class.parent_name.downcase)}.first(5)
      SlackService.notify("*API FATAL ERROR - #{exc.class.name}* \n App: \`\`\` #{{URL: request.base_url, HOST: request.host, ACTION: "#{params[:controller]}/#{params[:action]}"}.to_json} \`\`\` Error: \`\`\` #{exc.to_json} \`\`\` Params: \`\`\`#{request.params.to_json}\`\`\` User: \`\`\`#{@user.present? ? {id: @user.id, email: @user.email, role: @user.role }.to_json : 'No User Present!'} \`\`\` BACKTRACE: \`\`\` #{back_trace.to_json} \`\`\`")
    else
      notifyTitle = "Uncaught Exception Error"
      message = exc.message unless message.present?
    end
    return render_json_response(
      {
        :success => false,
        :status => TypesEnumLib::Statuses::Failed,
        :message => message || 'QC Error: Check your parameters, you may have passed some invalid parameters. See error object for more details!',
        :error => {
          :type => "#{exc.class}",
          :details => exc.message,
          :handle_standard_error => true
        }
      }, :ok
    )
  end

  def return_no_wallets(user, present)
    request.params[:card_number] = "#{(request.params[:card_number].to_s.first(4))}********#{(request.params[:card_number].to_s.last(4))}"
    if present == true
      message = "No wallet exists for user. Creating one now "
    else
      message = "No wallet exists for user"
    end
    SlackService.notify("*API FATAL ERROR * \n App: \`\`\` #{{URL: request.base_url, HOST: request.host, ACTION: "#{params[:controller]}/#{params[:action]}"}.to_json} \`\`\` Error: \`\`\` #{message} \`\`\` Params: \`\`\`#{request.params.to_json}\`\`\` User: \`\`\`#{@user.present? ? {id: @user.id, email: @user.email, role: @user.role }.to_json : 'No User Present!'} \`\`\`") unless request.host == 'localhost'
  end

  def record_it(type = nil, reason = nil)
    clean_parameters
    unless @user.nil?
      activity = ActivityLog.new
      activity.url = parse_url(request.url)
      activity.host = request.host
      activity.browser = request.env["HTTP_USER_AGENT"]
      if request.remote_ip.present?
        activity.ip_address = request.remote_ip
      end
      activity.controller = controller_name
      activity.action_type = @user.admin_user? ? 'affiliate_program' : nil
      activity.action = action_name
      activity.ref_id = params[:RefId] || params[:ref_id]
      response_hash = JSON.parse(reason)
      message = I18n.t('api.registrations.account_limit_true').split('%')
      message = message[0] if message.present?
      error_message = response_hash["message"].split('[') if response_hash["message"].present?
      error_message = error_message[0] if error_message.present?
      activity.action_type = "transaction_amount_limit" if error_message.present? && message.present? && error_message == message
      activity.respond_with = reason.present? ? reason == I18n.t('api.errors.declined') ? I18n.t('api.errors.declined_msg') : reason : response.body
      activity.response_status = response.status
      unless type.nil?
        activity.activity_type = type
      end
      if params[:user_image].present?
        if params[:user_image].original_filename.present?
          params[:user_image] = params[:user_image].original_filename
        else
          params[:user_image] = 'unknown image format'
        end
      end
      activity.transaction_id = params[:transaction_id]
      params[:ip_details][:request_ip] = request.ip if params[:ip_details].try(:[],:request_ip).present?
      params[:ip_details][:request_remote_ip] = request.remote_ip if params[:ip_details].try(:[],:request_remote_ip).present?
      params[:issue_amount] = params[:issue_amount].to_f if params.try(:[],:issue_amount).present?
      clean_parameters(params[:registration]) if params[:registration].present?
      activity.params = params.except(:card_cvv)
      activity.user = @user
      if @cultivate_params.present?
        activity_reason = JSON.parse reason
        puts "========================================>>>>>>>>>>>>><<<<<<<<<<<<<<<<<========================="
        if activity_reason['success']
          puts "Standlone Success"
          if @location.present?
            if @location.standalone
              culti_url = @location.standalone_url
              activity.cultivate_response = cultivate_api('success', activity_reason['status'],params[:transaction_id],culti_url) if culti_url.present?
            end
          end
        else
          puts "Standlone Decline"
          if @location.present?
            if @location.standalone
              culti_url = @location.standalone_url
              activity.cultivate_response = cultivate_api('decline', activity_reason['message'],params[:transaction_id],culti_url) if culti_url.present?
            end
          end
        end
        puts "========================================>>>>>>>>>>>>><<<<<<<<<<<<<<<<<========================="
      end
      if @eaze_reporting.present?
        puts "========================================>>>>>>>>>>>>><<<<<<<<<<<<<<<<<========================="
        activity_reason = JSON.parse reason
        if activity_reason['success']
          puts "EAZE Reporting Success"
          if @location.present?
            if @location.eaze_reporting
              eaze_reporting_url = ENV.fetch("EAZE_WEBHOOK_URL") { '' }
              activity.cultivate_response = cultivate_api('success', activity_reason['status'],params[:transaction_id],eaze_reporting_url,true) if eaze_reporting_url.present?
            end
          end
        else
          puts "EAZE Reporting DEcline"
          if @location.present?
            if @location.eaze_reporting
              eaze_reporting_url = ENV.fetch("EAZE_WEBHOOK_URL") { '' }
              activity.cultivate_response = cultivate_api('success', activity_reason['status'],params[:transaction_id],eaze_reporting_url,true) if eaze_reporting_url.present?
            end
          end
        end
        puts "========================================>>>>>>>>>>>>><<<<<<<<<<<<<<<<<========================="
      end
      activity.save
    end
  end

  def greater_amount
    if params[:amount].present? && params[:amount].to_i > 0
      return true
    else
      return false
    end
  end

  def render_json_response(resource, status)
    mask_card_param
    record_it(resource[:success] == true ? 4 : 5, resource.to_json)
    resource[:status] = resource[:success] == true ? TypesEnumLib::Statuses::Success : TypesEnumLib::Statuses::Failed if resource[:status].blank?
    Thread.current[:response]=resource
    if resource[:bank_response].present?
      #removing bank response from refund api response to user
      resource.reject!{|k,v| k == :bank_response}
    end
    render json: resource.to_json, status: status, adapter: :json_api, meta: default_meta
  end

  def saving_response(resource)
    mask_card_param
    record_it(resource[:success] == true ? 4 : 5, resource.to_json)
  end

  def send_text_message(body,to)
    begin
      TextsmsWorker.perform_async(to,body)
      return true
    rescue Exception => exc
      return false
    end
  end

  def user_name(id)
    user = User.find_by_id(id) if id.present?
    if user.present?
      return user.name
    else
      return ''
    end
  end

  def authenticate_user
    # sequence_down()
    amount_validation = validate_amount(
        {
            amount: params[:amount],
            tip: params[:tip],
            privacy_fee: params[:privacy_fee],
            total_amount: params[:total_amount],
            partial_amount: params[:partial_amount]
        }
    )

    return amount_validation if amount_validation.present?
    token = params[:auth_token] || ""
    @user = User.authenticate_by_token(token).first if token.present? && token != ""
    authenticate_transaction if @user.present? && (params["action"] != "virtual_transaction_3ds_success" && params["action"] != "transaction_last4")
    @current_stripe_type = nil
    config = AppConfig.where(key: AppConfig::Key::APIsAuthorization).first if @user.present?
    @api_config = JSON.parse(config.stringValue) if config.present? && config.stringValue.present?
    seq_down = AppConfig.sequence_down.first
    @seq_down = seq_down.try(:boolValue)
    return render_json_response({:success => false, :message => I18n.t('api.registrations.bad_request')}, :ok)  if @user.blank? || @user.nil?
    if @user.admin_user?
      return render_json_response({:success => false, :message => I18n.t('api.errors.archived_admin_user')}, :ok)  if @user.is_block || @user.archived
    else
      return render_json_response({:success => false, :message => I18n.t('api.registrations.unauth')}, :ok)  if @user.is_block || @user.archived
    end
  end

  def authenticate_transaction
    catch_duplicate_time = ENV["DUPLICATE_TRANSACTION_TIME"] || 300
    params_card_number = "#{(params[:card_number].to_s.first(4))}********#{(params[:card_number].to_s.last(4))}"
    activities = @user.activity_logs.where("params->'amount' ? :amount AND created_at >= :time", time: Time.now - catch_duplicate_time.to_i.seconds, amount: params["amount"].to_s)
    duplicate = activities.detect {|activity|
      if (params[:card_token].present? || params[:card].present?) && (params[:action] == "virtual_transaction" || params[:action] == "virtual_transaction_card")
        valid_number_amount?(activity) && (activity.api_success? || activity.vt_success?)
      else
        valid_number_amount?(activity) && activity.params["card_number"].last(4) == params_card_number.last(4) && (activity.api_success? || activity.vt_success?)
      end
    }
    if duplicate.present?
      return render_json_response({:success => false, :message => I18n.t('api.errors.duplicate_transaction')}, :ok)
    end
  end

  def valid_number_amount?(activity)
    if activity.params["card_number"].present? && activity.params["amount"].present?
      true
    else
      false
    end
  end

  def current_user
    @user
  end

  def random_token
    return Digest::SHA1.hexdigest([Time.now, rand].join)
  end

  def default_meta
    {
      :licence => 'Techcreatix-2019',
      :authors => ['Techcreatix'],
      :logged_in => (@current_user ? true : false)
    }
  end

  def authorized_merchant_app(merchant_id, location_token)
    location = Location.where(merchant_id: merchant_id, location_secure_token: location_token).first
    oauthapp = OauthApp.where(user_id: merchant_id).first
    if location.present? && oauthapp.present?
      return location
    else
      return false

    end
  end

  def urlDomain(url)
    return '' unless url.present?
    url = url.gsub('http://', '')
    return url if url.include? 'compute.amazonaws.com'
    split = url.split(/\./)
    if split.length > 2
      return "#{split[1] || ''}.#{split[2] || ''}"
    else
      return url
    end
  end

  def mask_card_param
    params[:card_number] = "#{(params[:card_number].to_s.first(4))}********#{(params[:card_number].to_s.last(4))}" if params[:card_number].present?
    Rails.application.config.filter_parameters.each do |key|
      params[key] = "[FILTERED]" if key != :card_number
    end
  end

  def check_bank_status
    return maintenance_response() if ENV.fetch("BANK_SERVICE_BLOCKED") { false }.to_s == 'true'
  end

  def maintenance_response
    return render_json_response({ success: false, status: TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.system_maintaince')}, :ok)

  end

  def sequence_down
    return render_json_response({ success: false, status: TypesEnumLib::Statuses::Failed, :message => I18n.t('api.errors.block_chain_issue')}, :ok)
  end

  def parse_url(url)
    uri = URI(url)
    uri.query = ""
    return uri.to_s
  end
  def validate_amount(args)
    args.each do |name,value|
      if value.present?
        valid_amount_regex = /\A[+-]?\d+(\.[0-9]{1,2})?\z/
        result = !!(value =~ valid_amount_regex)
        if !result
          return render_json_response({:success => false, :message => I18n.t('api.registrations.invalid_amount',amount_type: name)}, :ok)
        end
      end
    end
    return nil
  end


end
