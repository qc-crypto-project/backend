class Api::RequestsController < Api::ApiController
  before_action :allow_if_active , only: [:request_code, :approve_request_code]
  # before_action :location_policy , only:[:request_code, :approve_request_code]
  def request_money
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters({:amount => params[:amount], :to_id => params[:to_id], :wallet_id => params[:wallet_id]})
      raise StandardError.new('Invalid Amount Requested!') if params[:to_id].nil?
      raise StandardError.new('Receiver Id is required to request money!') if params[:to_id].nil?
      raise StandardError.new('You cannot request yourself!') if @user.id == params[:to_id]
      reciever = @user
      sender = User.includes(:wallets).where(id: params[:to_id]).first
      validate_parameters({:sender => sender, :reciever => reciever})
      wallet = @user.wallets.where(id: params[:wallet_id]).first
      raise StandardError.new('Invalid Wallet for Request!') if wallet.nil? || wallet.blank?
      request = Request.new(amount: params[:amount], sender_id: reciever.id, sender_name: reciever.name, reciever_id: sender.id, reciever_name: sender.name, status: 'in_progress', wallet_id: wallet.id)
      if request.save!
        requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
        # requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
        message = "#{reciever.name} has sent you a request for #{number_to_currency(params[:amount])} "
        if params[:message].present? and params[:message] != ""
          message = message + "\nNote: #{params[:message]}"
        end
        twilio_text = send_text_message(message, sender.phone_number)
        push_notification(message, sender)
        # twilio_text = send_text_message("You Have requested for #{number_to_currency(params[:amount])} to #{sender.name}", reciever.phone_number)
        return render_json_response({success: true, message: "Successfully sent", auth_token: @user.authentication_token}, :ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.inspect
      return render_json_response({:success => false, :message => "#{ex.message}"},:ok)
    end
  end

  def request_code
    email_option = [true ,false]
    if (params[:amount].present? && params[:to_id].present? && params[:from_id].present?) || (params[:amount].present? && params[:location_secure_token].present? && params[:from_id].present?)
      if greater_amount
        begin
          raise StandardError.new(I18n.t("errors.checkout.depreciated"))
          reciever = @user
          sender = User.includes(:wallets).where(id: params[:from_id]).first
          if reciever.id == sender.id
            return render_json_response({success: false, message: "You cannot request yourself!"}, :ok)
          else
            if sender.present? && reciever.present?
              if params[:location_secure_token].present?
                location = authorized_merchant_app(reciever.id, params[:location_secure_token])
                if location
                  wallet = location.wallets.first
                else
                  return render_json_response({:success => false, :message => 'Invalid app/location credentials!'}, :ok)
                end
              else
                wallet = @user.wallets.where(id: params[:to_id]).first
              end

              if wallet.present?
                request = Request.new(amount: params[:amount], sender_id: reciever.id, sender_name: reciever.name, reciever_id: sender.id, reciever_name: sender.name, status: 'in_progress', wallet_id: wallet.id)
                # request.update!(sender_access_token: params[:sender_access_token])
                @loca = wallet.location
                if @loca.sale_limit.present?
                  if @loca.sale_limit_percentage.present?
                    sale_limit = @loca.sale_limit
                    sale_limit_percentage = @loca.sale_limit_percentage
                    total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
                    if params[:amount].to_f > total_limit
                      return render_json_response({success: false, message: I18n.t("api.registrations.trans_limit")},:ok)
                    end
                  else
                    if params[:amount].to_f > @loca.sale_limit
                      return render_json_response({success: false, message: I18n.t("api.registrations.trans_limit")},:ok)
                    end
                  end
                end
                @random_code = rand(1_000..9_999)
                # user.update!(card_json: card_data)
                request.update!(:pin_code => @random_code)
                if request.save
                  requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
                  requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
                  message = "Your pin code is: #{@random_code}\n #{reciever.name} has requested $ #{params[:amount].to_f}"
                  # if params[:message].present? and params[:message] != ""
                  #   message = message + "\nNote: #{params[:message]}"
                  # endlocation
                  twilio_text = send_text_message(message, sender.phone_number)
                  push_notification(message, sender)
                  params[:send_email] = true
                  if params[:send_email].present? &&  email_option.include?(params[:send_email]) && params[:send_email] == true
                    UserMailer.inform_merchant(sender, message).deliver_later
                  end

                  # twilio_text = send_text_message("You Have requested for #{number_to_currency(params[:amount])} to #{sender.name}", reciever.phone_number)
                  return render_json_response({success: true,  request: request.id, message: "Request successfully sent"},:ok)
                else
                  return render_json_response({success: false, message: "Request Not sent"},:ok)
                end
              else
                return render_json_response({success: false, message: "Wallet doesn\'t belong to you"},:ok)
              end
            else
              return render_json_response({success: false, message: "Sender or Reciever not found!"},:ok)
            end
          end
        rescue => ex
          puts "=======EXCEPTION HANDLED======", ex.message
          return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
        end
      else
        return render_json_response({success: false, message: "Amount can not be 0"},:ok)
      end
    else
      return render_json_response({success: false, message: "You passed empty service!"},:ok)
    end
  end

  def requests
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
      requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
      return render_json_response({success: true, message: "Successfully", requests: requests,  auth_token: @user.authentication_token},:ok)
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def pending_requests
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      requests = Request.where("(sender_id = ? or reciever_id = ?) and status = ?", @user.id, @user.id,"in_progress").order(updated_at: :desc)
      requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
      return render_json_response({success: true, message: "Successfully", requests: requests,  auth_token: @user.authentication_token},:ok)
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def reject_request
    if params[:request_id].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        if @user.present?
          request = Request.where(id: params[:request_id], reciever_id: @user.id).first
          if request.present?
            if request.status == "cancel"
              return render_json_response({success: false, message: "Cancelled"},:ok)
            else
              if request.status == "approved"
                return render_json_response({success: false, message: "Already approved"},:ok)
              else
                request.update(status: 'rejected')
                requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
                requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
                return render_json_response({success: true, message: "Reject Successfully", requests: requests},:ok)
              end
            end
          else
            return render_json_response({success: false, message: "Request not found!"},:ok)
          end
        else
         return render_json_response({success: false, message: "You r Not found!"},:ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({success: false, message: "You passed empty service"},:ok)
    end
  end

  def approved_requests
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      requests = Request.where(reciever_id: @user.id, status: 'approved').order(updated_at: :desc)
      return render_json_response({success: true, message: "Successfully", requests: requests,  auth_token: @user.authentication_token},:ok)
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def rejected_requests
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      requests = Request.where(reciever_id: @user.id, status: 'rejected').order(updated_at: :desc)
      return render_json_response({success: true, message: "Successfully", requests: requests,  auth_token: @user.authentication_token},:ok)
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def cancel_request
    if params[:request_id].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        request = Request.where(sender_id: @user.id, id: params[:request_id]).first
        if request.present?
          if request.status == "approved" or request.status == "rejected"
            return render_json_response({success: false, message: "Allready approved or rejected"},:ok)
          else
            request.update(status: "cancel")
            requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
            requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
            return render_json_response({success: true, message: "Cancelled Successfully", requests: requests},:ok)
          end
        else
          return render_json_response({success: false, message: "Request not_found"},:ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({success: false, message: "Incomplete params"},:ok)
    end
  end


  def approve_request
    if params[:request_id].present? && params[:from_id].present?
     begin
       raise StandardError.new(I18n.t("errors.checkout.depreciated"))
       request = Request.where(reciever_id: @user.id, id: params[:request_id]).first
       if request.present?
        if request.status == "cancel"
          return render_json_response({success: false, message: "Allready Cancelled"},:ok)
        else
          if request.status == "approved"
            return render_json_response({success: false, message: "Allready approved"},:ok)
          else
            wallet = @user.wallets.where(id: params[:from_id]).first
            if wallet.present?
              reciever = User.where(id: request.reciever_id).first
              balance = show_balance(params[:from_id])
              if balance.to_f >= request.amount.to_f
                sender = User.where(id: request.sender_id).first
                data = ''
                current_transaction = transaction_between(request.wallet_id, params[:from_id], request.amount, "Request no:#{request.id}","0", data)
                message = "#{reciever.name} has accepted your request for #{number_to_currency(request.amount.to_f)} ."
                twilio_text = send_text_message(message, sender.phone_number)
                push_notification(message, sender)
                new_balance =  balance.to_f - request.amount.to_f.to_f
                transactions = all_transactions(params[:from_id],params[:from_id],params[:page_number])
                request.update(status: 'approved')
                requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(created_at: :desc)
                requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
                return render_json_response({success: true, auth_token: @user.authentication_token,requests: requests,balance: new_balance.to_f, transactions: transactions },:ok)
              else
                return render_json_response({success: false, message: "You Dont have Enough Balance"},:ok)
              end
            else
              return render_json_response({success: false, message: I18n.t('api.errors.wallet_not_found')},:ok)
            end
          end
        end
       else
        return render_json_response({success: false, message: "request not found"},:ok)
       end
     rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
     end
    else
      return render_json_response({success: false, message: "Incomplete params"},:ok)
    end
  end

  #   def approve_ach_request_code
  #   if params[:request_id].present? && params[:pin_code].present?
  #     begin
  #       request = Request.where(id: params[:request_id], pin_code: params[:pin_code]).first
  #       if request.present?
  #         if request.status == "cancel"
  #           return render_json_response({success: false, message: "Already Cancelled"},:ok)
  #         else
  #           if request.status == "approved"
  #             return render_json_response({success: false, message: "Already approved"},:ok)
  #           else
  #             if request.pin_code == params[:pin_code]
  #               wallet = Wallet.where(id: request.wallet_id).first
  #               if wallet.present?
  #                 reciever = User.where(id: request.reciever_id).first
  #                 reciever_wallet = reciever.wallets.first
  #                 balance = show_balance(reciever_wallet.id)
  #                 fee_lib = FeeLib.new
  #                 fee_lib.update_limit(reciever,'deposit')
  #                 deposit_limit = fee_lib.get_limit(reciever,request.amount.to_f,'deposit')
  #                 if !deposit_limit
  #                   message = fee_lib.show_limit(reciever,'deposit')
  #                   return render_json_response({success: false, message: "Your weekly limit is: #{message}"},:ok)
  #                 end
  #                 if balance.to_f < request.amount.to_f
  #                   reciever_card = reciever.cards.last
  #                   new_amount = request.amount.to_f - balance.to_f
  #                   issue = approve_request_issue(reciever, new_amount, reciever_card, request)
  #                   balance = show_balance(reciever_wallet.id)
  #                 end
  #                 if balance.to_f >= request.amount.to_f
  #                   sender = User.where(id: request.sender_id).first
  #                   data = ''
  #                   current_transaction = transaction_between(request.wallet_id,reciever_wallet.id,request.amount, "request",0, data)
  #                   message = "You have recieved $ #{request.amount.to_f} from #{reciever.name}."
  #                   twilio_text = send_text_message(message, sender.phone_number)
  #                   push_notification(message, sender)
  #                   sender_balance = show_balance(request.wallet_id)
  #                   new_balance =  number_with_precision(sender_balance.to_f + request.amount.to_f, precision: 2)
  #                   # transactions = all_transactions(params[:from_id],params[:from_id],params[:page_number])
  #                   request.update(status: 'approved')
  #                   return render_json_response({success: true, balance: new_balance.to_f,Transaction_id: current_transaction[:transaction_id] , message: "Successfully transferred!" },:ok)
  #                 else
  #                   return render_json_response({success: false, message: "You Dont have Enough Balance"},:ok)
  #                 end
  #               else
  #                 return render_json_response({success: false, message: I18n.t('api.errors.wallet_not_found')},:ok)
  #               end
  #             else
  #               return render_json_response({success: false, message: "pin code incorrect"}, :ok)
  #             end
  #           end
  #         end
  #       else
  #         return render_json_response({success: false, message: "request not found"},:ok)
  #       end
  #     rescue => ex
  #       puts "=======EXCEPTION HANDLED======", ex.message
  #       return render_json_response({:success => false, :message => "#{ex.message}"},:ok)
  #     end
  #   else
  #     return render_json_response({success: false, message: "Incomplete params"},:ok)
  #   end
  # end

  def approve_request_code
    if params[:request_id].present? && params[:pin_code].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        fee_lib = FeeLib.new
        request = Request.where("id = ? AND pin_code = ? ",params[:request_id],params[:pin_code]).first
        if request.present?
          if request.status == "cancel"
            return render_json_response({success: false, message: "Already Cancelled"},:ok)
          else
            if request.status == "approved"
              return render_json_response({success: false, message: "Already approved"},:ok)
            else
              if request.pin_code == params[:pin_code]
                wallet = Wallet.where(id: request.wallet_id).first
                if wallet.present?
                  @loca = wallet.location
                  if @loca.sale_limit.present?
                    if @loca.sale_limit_percentage.present?
                      sale_limit = @loca.sale_limit
                      sale_limit_percentage = @loca.sale_limit_percentage
                      total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
                      if request.amount.to_f > total_limit
                        return render_json_response({success: false, message: I18n.t("api.registrations.trans_limit")},:ok)
                      end
                    else
                      if request.amount.to_f > @loca.sale_limit
                        return render_json_response({success: false, message: I18n.t("api.registrations.trans_limit")},:ok)
                      end
                    end
                  end

                  reciever = User.where(id: request.reciever_id).first
                  reciever_wallet = reciever.wallets.first
                  balance = show_balance(reciever_wallet.id, reciever.ledger)
                  if balance.to_f < request.amount.to_f
                    reciever_card = reciever.cards.last
                    new_amount = request.amount.to_f - balance.to_f
                    issue =  approve_request_issue(reciever, new_amount, reciever_card, request, reciever.ledger)
                    data = {
                        :issue_transaction_id => issue.first.present? ? issue.first[:transaction_id] : nil,
                        :issue_amount => new_amount,
                        :transaction_id => issue.second.present? ? issue.second : nil,
                        :merchant_name => reciever.name
                      }
                    balance = show_balance(reciever_wallet.id, reciever.ledger)
                  end
                  if balance.to_f >= request.amount.to_f
                    sender = User.where(id: request.sender_id).first
                    current_transaction = transaction_between(request.wallet_id,reciever_wallet.id,request.amount, TypesEnumLib::TransactionType::SaleType,0, data, nil,reciever.ledger,nil,TypesEnumLib::TransactionSubType::DebitCharge)
                    message = "You have recieved $ #{request.amount.to_f} from #{reciever.name}."
                    twilio_text = send_text_message(message, sender.phone_number)
                    push_notification(message, sender)
                    sender_balance = show_balance(request.wallet_id, sender.ledger)
                    new_balance =  number_with_precision(sender_balance.to_f, precision: 2)
                    # transactions = all_transactions(params[:from_id],params[:from_id],params[:page_number])
                    request.update(status: 'approved')
                    return render_json_response({success: true, balance: new_balance.to_f,Transaction_id: current_transaction[:transaction_id] , message: "Successfully transferred!" },:ok)
                  else
                    return render_json_response({success: false, message: "You Dont have Enough Balance"},:ok)
                  end
                else
                  return render_json_response({success: false, message: I18n.t('api.errors.wallet_not_found')},:ok)
                end
              else
                return render_json_response({success: false, message: "pin code incorrect"}, :ok)
              end
            end
          end
        else
          return render_json_response({success: false, message: "request not found"},:ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "#{ex.message}"},:ok)
      end
    else
      return render_json_response({success: false, message: "Incomplete params"},:ok)
    end
  end

  def approve_request_without_issue
    if params[:request_id].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        fee_lib = FeeLib.new
        request = Request.where(id: params[:request_id]).first
        if request.present?
          if request.status == "cancel"
            return render_json_response({success: false, message: "Already Cancelled"},:ok)
          else
            if request.status == "approved"
              return render_json_response({success: false, message: "Already approved"},:ok)
            else
              wallet = Wallet.where(id: request.wallet_id).first
              if wallet.present?
                reciever = User.where(id: request.reciever_id).first
                reciever_wallet = reciever.wallets.first
                balance = show_balance(reciever_wallet.id, reciever.ledger)
                if balance.to_f < request.amount.to_f
                  reciever_card = reciever.cards.last
                  new_amount = request.amount.to_f - balance.to_f
                  issue =  approve_request_wihtout_payment(reciever, new_amount, reciever_card, request, reciever.ledger)
                  data = {
                      :issue_transaction_id => issue.first.present? ? issue.first[:transaction_id] : nil,
                      :issue_amount => new_amount,
                      :transaction_id => issue.second.present? ? issue.second : nil,
                      :merchant_name => reciever.name
                  }
                  balance = show_balance(reciever_wallet.id, reciever.ledger)
                end
                if balance.to_f >= request.amount.to_f
                  sender = User.where(id: request.sender_id).first
                  current_transaction = transaction_between(request.wallet_id,reciever_wallet.id,request.amount, TypesEnumLib::TransactionType::SaleType,0, nil,data, reciever.ledger,nil,TypesEnumLib::TransactionSubType::DebitCharge)
                  message = "You have recieved $ #{request.amount.to_f} from #{reciever.name}."
                  twilio_text = send_text_message(message, sender.phone_number)
                  push_notification(message, sender)
                  sender_balance = show_balance(request.wallet_id, sender.ledger)
                  new_balance =  number_with_precision(sender_balance.to_f, precision: 2)
                  request.update(status: 'approved')
                  return render_json_response({success: true, balance: new_balance.to_f,Transaction_id: current_transaction[:transaction_id] , message: "Successfully transferred!" },:ok)
                else
                  return render_json_response({success: false, message: "You Dont have Enough Balance"},:ok)
                end
              else
                return render_json_response({success: false, message: I18n.t('api.errors.wallet_not_found')},:ok)
              end
            end
          end
        else
          return render_json_response({success: false, message: "request not found"},:ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "#{ex.message}"},:ok)
      end
    else
      return render_json_response({success: false, message: "Incomplete params"},:ok)
    end
  end

  def scanner_request
    if  params[:req_token].present? and params[:from_id]
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        if @user.nil?
          return render_json_response({:success => false, :message => 'User not found with auth_token'}, :ok)
        else
          qr_card = QrCard.includes(:user).where(token: params[:req_token]).first
          if qr_card.present?
            user = qr_card.user
            if @user.id == user.id
              return render_json_response({:success => false, :message => 'Cannot transfer to Your wallet.'}, :ok)
            else
              if qr_card && qr_card.is_valid == true
                if user && user.wallets.where(id: qr_card.wallet_id).first.present?
                  price = qr_card.price.to_f
                  if qr_card.message.present?
                    send_text_message(qr_card.message, @user.phone_number)
                    push_notification(qr_card.message, @user)
                  end
                  balance = show_balance(params[:from_id])
                  if balance.to_f >= qr_card.price.to_f
                    data  = ''
                    current_transaction = transaction_between(qr_card.wallet_id, params[:from_id],price, "by qr code:#{qr_card.id}", "0", data)
                    message = "#{number_to_currency(price)} has been successfully transferred to your wallet from #{user.name}.\nNote: #{params[:message]}"
                    twilio_text = send_text_message(message, user.phone_number)
                    push_notification(message, user)
                    transactions = all_transactions(params[:from_id],params[:from_id],params[:page_number])
                    qr_card.update(is_valid: false)
                    balance = balance.to_f - qr_card.price.to_f
                    qr_cards = QrCard.where(user_id: @user.id)
                    return render_json_response({:success => true, qr_cards: qr_cards, :message => 'Successful :)',transactions: transactions, balance: balance}, :ok)
                  else
                    return render_json_response({:success => false, :message => 'You Dont Have Enough balance!'}, :ok)
                  end
                else
                  return render_json_response({:success => false, :message => 'wallet and user ids doesnot match'}, :ok)
                end
              else
                return render_json_response({:success => false, :message => 'Card Already Used'}, :ok)
              end
            end
          else
           return render_json_response({:success => false, :message => 'Card Not Found!'}, :ok)
          end
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({:success => false, :message => 'Incomplete Params'}, :ok)
    end
  end

  def generator_request
    if  params[:to_id].present?
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if greater_amount
        if @user.nil?
          return render_json_response({:message => "User not Found!", :success => false}, :not_found)
          return
        else
          wallet = @user.wallets.where(id: params[:to_id]).first
          if wallet.nil?
            return render_json_response({:success => false, :message => 'Wallet not  belong to you'}, :ok)
          else
            qr_card = QrCard.create(user_id: @user.id, wallet_id: wallet.id, message: params[:message])
            qr_card.generate_token
            qr_card.price = params[:amount]
            req = {"req_token"=>qr_card.token, "amount"=>params[:amount]}
            req_into_json = req.to_json
            barcode = Barby::QrCode.new(req_into_json, level: :q, size: 10)
            base64_output = Base64.encode64(barcode.to_png({ xdim: 5 }))
            data = "data:image/png;base64,#{base64_output}"
            image = Paperclip.io_adapters.for(data)
            image.original_filename = "qr_image.png"
            qr_card.image = image
            qr_card.category = "request"
            qr_card.save
            qr_cards = QrCard.where(user_id: @user.id)
            return render_json_response({success: true,qr_cards: qr_cards, barcode_image: "https:#{qr_card.image.url}", qr_code: qr_card.token},:ok)
          end
        end
      else
        return render_json_response({success: false, message: "Amount can not be 0!"},:ok)
      end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({:success => false, :message => 'You just passed empty service yayyyyy'}, :ok)
    end
  end

  private
  def allow_if_active
    begin
      if params[:action] == 'request_code'
        if params[:to_id].present? && params[:from_id].present?
          wallet = @user.wallets.where(id: params[:to_id]).first unless @user.nil?
          location = wallet.location if wallet.location.present?
        elsif params[:location_secure_token].present?
          location=Location.where(location_secure_token: params[:location_secure_token]).first
        end
        unless location.nil?
           if location.is_block
             return render_json_response({success: false , :message => 'Location can not be accessed'}, :ok)
           end
        end

      elsif params[:action] == 'approve_request_code'
        location= nil
        if params[:request_id].present? && params[:pin_code].present?
          request = Request.where("id = ? AND pin_code = ? ",params[:request_id],params[:pin_code]).first
          wallet = Wallet.where(id: request.wallet_id).first if request.present?
          location = wallet.location if wallet.present?
        end
        if location.present? && location.is_block
          return render_json_response({success: false , :message => 'Location can not be accessed'}, :ok)
        end
      end

  rescue => ex
    puts "=======EXCEPTION HANDLED======", ex.message
    return render_json_response({:success => false, :message => "#{ex.message}"},:ok)
  end

  end
end
