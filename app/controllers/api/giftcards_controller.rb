class Api::GiftcardsController < Api::ApiController
  include ApplicationHelper
  skip_before_action :authenticate_user, :except => [:test_android_notification, :test_email]

  def create
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if greater_amount
        amount = dollars_to_cents(params[:amount])
        wallet = @user.wallets.first
        @issue = issue_amount(amount, wallet, 'GiftCard', 'Stripe',0)
        balance = dollars_to_cents(show_balance(wallet.id))
        if balance.to_f >= amount.to_f
          # gift_card = Giftcard.new(wallet_id: wallet.id, balance: dollars_to_cents(params[:amount]))
          # gift_card.generate_token
          # gift_card.company = Company.first
          # p "gift_card.errors", gift_card.errors
          # if gift_card.save!
            # transaction_between(@gift_card.wallet_id, @gift_card.wallet_id, amount, "by ATM", "0", '')
            return render_json_response({success: true, message: "Created", gift_card: Giftcard.last, amount: 1},:ok)
          # else
          #   return render_json_response({success: false, message: "Giftcard not Created! Try Again!"},:ok)
          # end
        else
          return render_json_response({success: false, message: I18n.t('merchant.controller.insufficient_balance')},:ok)
        end
      else
        return render_json_response({success: false, message: I18n.t('merchant.controller.insufficient_balance')},:ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def scan
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:token].present? && params[:to_wallet].present?
       @giftcard = Giftcard.where(token: params[:token]).first
       if @giftcard.present? && @giftcard.is_valid == true
         if @giftcard.balance.to_f > 0
           wallet =  @user.wallets.where(id: params[:to_wallet]).first
           if wallet.present?
            balance = show_balance(@giftcard.token)
            data = ''
            transfer_amount = params[:amount].present? ? params[:amount] : balance
            transaction_between(wallet.id, @giftcard.token, transfer_amount, TypesEnumLib::TransactionType::SaleType, "0", data,nil,nil,nil,TypesEnumLib::TransactionSubType::Giftcard)
            return render_json_response({success: true, message: "Successfully transferred! $ #{transfer_amount} to you wallet #{wallet.name}"}, :ok)
           else
            return render_json_response({success: false, message: I18n.t('errors.withdrawl.wallet_not_exist')}, :ok)
           end
         else
          return render_json_response({success: false, message: I18n.t('errors.withdrawl.insufficient_amount_for_gc')}, :ok)
         end
       else
         return render_json_response({success: false, message:  I18n.t('errors.withdrawl.invalid_quickcard')}, :ok)
       end
      else
        return render_json_response({success: false, message: I18n.t('errors.withdrawl.incomplete_service')}, :ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"}, :ok)
    end
  end

  def balance
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated")},:ok)
    if params[:token].present?
        regexp = Regexp.new('=([d+])?\w+')
        val = regexp.match(params[:token]).to_s.tr('=','')
        quick_card = Giftcard.where(token: val).first if val.present?
        if quick_card.present?
         balance = show_balance(quickcard.token)
         return render_json_response({success: true, message: "ok", balance:balance},:ok)
        else
         return render_json_response({success: false, message: "Card not valid."},:ok)
        end
    else
      return render_json_response({success: false, message: I18n.t('errors.withdrawl.incomplete_service')},:ok)
    end
  end

  # def test_android_notification
  #  if (!Rpush::Gcm::App.find_by_name("QuickCard"))
  #   app = Rpush::Gcm::App.new
  #   # let's name this one pushme_droid
  #   app.name = "QuickCard"
  #   # FCM auth key from firebase project
  #   app.auth_key = "AAAAXUngNas:APA91bFO_FpYqLfb1sMbOxEbSzY2aYVQLIFmwC5VbvdlXSBc5sxfciHYRBCdwo2xOKUZs8u9y-xXIAHMh3GVd27qrgVj0Lt7GxhwFU36EqunkGkZbpgob12Z73dy_FrhKGzkpYxcaac3"
  #   app.connections = 1
  #   # save our app in db
  #   app.save!
  #  end
  #  n = Rpush::Gcm::Notification.new

  #           # use the pushme_droid app we previously registered in our initializer file to send the notification
  #     n.app = Rpush::Gcm::App.find_by_name("QuickCard")
  #     n.registration_ids = ["dVWtUqaXNnk:APA91bGUL_lvwIJbjrrW7RoXisB1gjkHRA4ZhcLsvbgE9gXXl2W7aVoCffxV-Uo3Jdz__SFwArEMszlnAhd9jjmkBZ8101hfFc2aHf7GOx73bMzw799kimINv1Qgsi-WZHI5d0Laa4jR"]

  #           # parameter for the notification
  #     n.notification = {
  #       body: 'Just wanted to tell you that you are beautiful!',
  #           title: 'oyyyyyy ',
  #           sound: 'default'
  #       }
  #   if n.save
  #     Rpush.push
  #   end
  # end

  # def test_email
  #   t =NotificationMailer.send_email(params[:to]).deliver_now
  #   render :json => {t:  t}
  # end

end
