class Api::MerchantAccountsController < Api::ApiController
	skip_before_action :authenticate_user, only: :accounts
	
	def accounts
		user = User.authenticate_by_token(params[:auth_token]).first
    merchant_account = {}
		if user.try(:merchant?) && user.try(:my_locations) && user.try(:merchant_id).nil?
			merchant_account[:location] = user.my_locations.first.as_json(only: [:id, :business_name])
			merchant_account[:location].merge!({ "balance": user.wallets.primary.first.balance })
		end
		render :json => merchant_account
	end
end
