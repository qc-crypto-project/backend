class Api::CheckoutController < Api::ApiController

  include ApplicationHelper
  skip_before_action :authenticate_user, only: [:invoice_checkout, :country_data,:get_invoice_rtp_info]

  ALGORITHM = 'HS256'

  def get_product_info
    begin
      validate_parameters(
          :auth_token => params[:auth_token],
          :location_id => params[:location_id],
          # :website_url => params[:website_url],
          :success_url => params[:success_url],
          :fail_url => params[:fail_url],
          :total_amount => params[:total_amount]
      )

      if params[:shipping].present?
        validate_parameters(
            :phone_number => params[:shipping][:phone_no]
        )
      end

      merchant = User.authenticate_by_token(params[:auth_token]).first

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('errors.checkout.authentication_failed')}, :ok) if merchant.blank?

      location = Location.where(location_secure_token: params[:location_id]).last

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('errors.withdrawl.account_not_exist')}, :ok) if location.blank?
      check_amount_limit(location)

      #check user exist in wallet users list
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.wrong_user')}, :ok) unless location.merchant.id == merchant.id || location.merchant.id == merchant.try(:parent_merchant).try(:id)

      # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => "Provided website url not present under location settings!"}, :ok) if allow == false
      # checking if merchant website is present under its location setting or not ------- end ------
      phone_number=params[:shipping].try(:[],:phone_no)
      if phone_number.blank?
        phone_number=rand.to_s[2..11]
      end
      puts "---------------------------------------"
      puts params
      puts "---------------------------------------"
      if params[:shipping].present?
        name = params[:shipping][:name]
        name = name.split(" ")
        first_name = name.first
        name.shift
        last_name = name.join
        shipping_email = params[:shipping][:email].present? ? params[:shipping][:email].strip.gsub(" ", "+") : ""
        user = User.where(phone_number: params[:shipping].try(:[],:phone_no),role: :user)
        user = user.first

        if user.present?
          user.source = merchant.name
          user.is_block = false
          user.archived = false
          user.first_name = first_name
          user.last_name = last_name
          user.name = "#{first_name} #{last_name}"
        else
          user = User.new
          user.name = params[:shipping][:name]
          user.first_name = first_name
          user.last_name = last_name
          user.email = shipping_email
          user.phone_number = phone_number
          user.source = merchant.name
          password = random_password
          user.password = password
          user.role = :user
          user.is_block = false
          user.archived = false
        end

        user.save
      end
      transaction = nil
      if params[:request_id].present?
        @request = Request.find_by(id: params[:request_id])
        if @request.present?
          transaction = @request.invoice_transaction
        end
        if transaction.blank?
          transaction = Transaction.create(
              sender_name: user.name,
              sender_id: user.id,
              sender_wallet_id: user.wallets.first.try(:id),
              receiver_id: merchant.id,
              receiver_name: location.try(:business_name) || merchant.name,
              receiver_wallet_id: location.wallets.primary.first.try(:id),
              action: "transfer"
          )
        end
      else
        transaction = Transaction.create(
            sender_name: user.name,
            sender_id: user.id,
            sender_wallet_id: user.wallets.first.try(:id),
            receiver_id: merchant.id,
            receiver_name: location.try(:business_name) || merchant.name,
            receiver_wallet_id: location.wallets.primary.first.try(:id),
            action: "transfer"
        )
      end

      if params[:shipping].present?
        shipping_email = params[:shipping][:email].present? ? params[:shipping][:email].strip.gsub(" ", "+") : ""
        shipping_address = Address.where(id: params[:shipping][:id]).first_or_initialize
        shipping_address.name = params[:shipping][:name]
        shipping_address.email = shipping_email
        shipping_address.phone_number = params[:shipping].try(:[],:phone_no)
        shipping_address.street = params[:shipping][:street]
        shipping_address.city = params[:shipping][:city]
        shipping_address.state = params[:shipping][:state]
        shipping_address.country = params[:shipping][:country]
        shipping_address.zip = params[:shipping][:zip]
        shipping_address.address_type = "Shipping"
        shipping_address.save
      end

      if params[:billing].present?
        billing_email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
        # billing_address = ActiveRecord::Base::Address.where(id: params[:billing][:id]).first_or_initialize
        billing_address = Address.where(id: params[:billing][:id]).first_or_initialize
        billing_address.name = params[:billing][:name]
        billing_address.email = billing_email
        billing_address.phone_number = params[:billing].try(:[],:phone_no)
        billing_address.street = params[:billing][:street]
        billing_address.city = params[:billing][:city]
        billing_address.state = params[:billing][:state]
        billing_address.country = params[:billing][:country]
        billing_address.zip = params[:billing][:zip]
        billing_address.address_type = "Billing"
        billing_address.save
      end

      user.addresses << shipping_address
      user.addresses << billing_address

      total_products_amount = 0
      if params[:products].present?
        params[:products].each do |k,v|
          product = Product.where(id: v[:product_id]).first_or_initialize
          product.name = v["name"]
          product.description = v["description"]
          product.amount = v["amount"]
          product.quantity = v["quantity"]
          product.website_url = params[:website_url]
          product.transaction_id = transaction.id
          product.order_id = params[:order_id]
          product.save
          total_products_amount += (product.amount * product.quantity)
        end
      end

      payload = {auth_token: params[:auth_token], checkout_id: transaction.id, merchant_name: merchant.name,success_url: params[:success_url],fail_url: params[:fail_url],redirect_url: params[:redirect_url],order_id: params[:order_id],request_id: params[:request_id], exp: (Time.zone.now + 60.minute).to_i}
      token = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM

      transaction.shipping_id = shipping_address.try(:id)
      transaction.billing_id = billing_address.try(:id)
      transaction.delivery_status = "pending"
      transaction.status = "pending"
      transaction.main_type = "QCP Secure"
      transaction.sub_type = "invoice" if params[:request_id].present?
      transaction.tax = params[:total_tax].to_f
      transaction.discount = params[:discount].to_f
      transaction.shipping_amount = params[:shipping_amount].to_f
      transaction.amount = total_products_amount
      transaction.total_amount = params[:total_amount].to_f
      transaction.redirect_url = checkout_index_path(token: token)
      transaction.order_id = params[:order_id].present? ? params[:order_id] : params[:request_id]
      if params[:request_id].present?
        request = Request.find(params[:request_id])
        transaction.shipping_amount = request.shipping_handling_fee
      end
      transaction.save
      if params["invoice_checkout"] == "true"
        return render_json_response({:redirect_url => new_checkout_path(token: token), :success => true}, :ok)
      else
        return render_json_response({:redirect_url => ENV["APP_ROOT"]+new_checkout_path(token: token), :success => true}, :ok)
      end

    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def get_transaction_info
    begin
      validate_parameters(
          :auth_token => params[:auth_token],
          :location_id => params[:location_id],
          # :website_url => params[:website_url],
          :success_url => params[:success_url],
          :fail_url => params[:fail_url],
          :total_amount => params[:total_amount],
          # :phone_number => params[:phone_no],
          :billing => params[:billing]
      )
      if params[:billing].present?
        validate_parameters(
            :phone_number => params[:billing][:phone_no]
        )
      end
      merchant = User.authenticate_by_token(params[:auth_token]).first

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('errors.checkout.authentication_failed')}, :ok) if merchant.blank?
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('errors.checkout.invalid_country_code')}, :ok) if Address.get_country_name(params[:billing].try(:[], :country)).blank?

      location = Location.where(location_secure_token: params[:location_id]).last

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('errors.withdrawl.account_not_exist')}, :ok) if location.blank?
      check_amount_limit(location)
      #check user exist in wallet users list
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.wrong_user')}, :ok) unless location.merchant.id == merchant.id

      # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => "Provided website url not present under location settings!"}, :ok) if allow == false
      # checking if merchant website is present under its location setting or not ------- end ------
      phone_number=params[:billing].try(:[],:phone_no)
      if phone_number.blank?
        phone_number=rand.to_s[2..11]
      end
      if params[:billing].present?
        name = params[:billing][:name]
        name = name.split(" ")
        first_name = name.first
        name.shift
        last_name = name.join
        shipping_email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
        user = User.where(email: shipping_email,role: :user).or(User.where(phone_number: params[:billing][:phone_no],role: :user))
        user = user.first

        if user.present?
          user.source = merchant.name
          user.is_block = false
          user.archived = false
          user.first_name = first_name
          user.last_name = last_name
          user.name = "#{first_name} #{last_name}"
        else
          user = User.new
          user.name = params[:billing][:name]
          user.first_name = first_name
          user.last_name = last_name
          user.email = shipping_email
          user.phone_number = phone_number
          user.source = merchant.name
          password = random_password
          user.password = password
          user.role = :user
          user.is_block = false
          user.archived = false
        end
        user.save

        billing_email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
        # billing_address = ActiveRecord::Base::Address.where(id: params[:billing][:id]).first_or_initialize
        billing_address = Address.where(id: params[:billing][:id]).first_or_initialize
        billing_address.name = params[:billing][:name]
        billing_address.first_name = first_name
        billing_address.last_name = last_name
        billing_address.email = billing_email
        billing_address.phone_number = params[:billing].try(:[],:phone_no)
        billing_address.street = params[:billing][:street]
        billing_address.city = params[:billing][:city]
        billing_address.state = params[:billing][:state]
        billing_address.country = params[:billing][:country]
        billing_address.zip = params[:billing][:zip]
        billing_address.address_type = "Billing"
        billing_address.user_id = user.id
        billing_address.save

      end
      transaction = Transaction.create(
          sender_name: user.name,
          sender_id: user.id,
          sender_wallet_id: user.wallets.first.try(:id),
          receiver_id: merchant.id,
          receiver_name: location.try(:business_name) || merchant.name,
          receiver_wallet_id: location.wallets.primary.first.try(:id),
          action: "transfer"
      )


      payload = {api_checkout: true, auth_token: params[:auth_token], checkout_id: transaction.id, merchant_name: merchant.name,success_url: params[:success_url],fail_url: params[:fail_url],redirect_url: params[:redirect_url],order_id: params[:order_id],request_id: params[:request_id], exp: (Time.zone.now + 60.minute).to_i}
      token = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM

      transaction.billing_id = billing_address.try(:id)
      transaction.delivery_status = "pending"
      transaction.status = "pending"
      transaction.main_type = TypesEnumLib::TransactionType::QCSecure
      transaction.sub_type = "invoice" if params[:request_id].present?
      transaction.tax = params[:total_tax].to_f
      transaction.shipping_amount = params[:shipping_amount].to_f
      transaction.amount = params[:total_amount]
      transaction.total_amount = params[:total_amount].to_f
      transaction.redirect_url = checkout_index_path(token: token)
      transaction.order_id = params[:order_id].present? ? params[:order_id] : params[:request_id]
      transaction.save

      return render_json_response({:redirect_url => ENV["APP_ROOT"]+new_checkout_path(token: token), :success => true}, :ok)

    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def get_rtp_info
    begin
      result = creating_rtp_transaction
      return render_json_response({:redirect_url => invoice_rtp_checkout_checkout_index_url(token: result[:token]), :quickcard_id => result[:quickcard_id], :success => true}, :ok)
    rescue StandardError => exc
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    end
  end

  def get_invoice_rtp_info
    begin
      set_params_for_invoice
      result = creating_rtp_transaction
      redirect_to invoice_rtp_checkout_checkout_index_path(token: result[:token])
    rescue StandardError => exc
      flash[:notice] = exc.message
      redirect_to root_path
    end
  end

  def country_data
    if params[:country].present?
      @states = CS.states(params[:country])
      if request.xhr?
        respond_to do |format|
          format.json {
            render json: {states: @states}
          }
        end
      end
    end
  end

  def invoice_checkout
    begin
      if params[:request_url].present?
        decode_token = JWT.decode params[:request_url], Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
        decode_token = decode_token.first
        url = get_product_info_api_checkout_index_url
        request_body = { shipping: decode_token.try(:[],'shipping'),
                         billing: decode_token.try(:[],'billing'),
                         auth_token: decode_token.try(:[],'auth_token'),
                         products: decode_token.try(:[],'products'),
                         success_url: decode_token.try(:[],'success_url'),
                         fail_url: decode_token.try(:[],'fail_url'),
                         request_id: decode_token.try(:[],'request_id'),
                         total_tax: decode_token.try(:[],'total_tax'),
                         total_amount: decode_token.try(:[],'total_amount'),
                         location_id: decode_token.try(:[],'location_id'),
                         invoice_checkout: decode_token.try(:[],'invoice_checkout'),
                         percent_tax: decode_token.try(:[],'percent_tax'),
                         amount: decode_token.try(:[],'amount')

        }
        @request = Request.in_progress.where(id: decode_token.try(:[],'request_id')).first
        raise StandardError.new "No pending Invoice found!" if @request.blank?
        if @request.apply_tax?
          if decode_token.try(:[],'total_tax').to_f > 0
            tax_on_SH = (decode_token.try(:[],'percent_tax').to_f/100)*@request.shipping_handling_fee.to_f
            request_body[:total_tax] = @request.tax_method == "percent" ? ((decode_token.try(:[],'percent_tax').to_f / 100) * decode_token.try(:[],'amount').to_f) + tax_on_SH.to_f  : decode_token.try(:[],'percent_tax').to_f
          end
        end
        if @request.discount.to_f > 0
          discount = @request.discount
          if @request.discount_method == "percent"
            discount = (discount.to_f/100)*@request.amount.to_f
          end
          request_body[:discount] = discount
        end
        response = RestClient.post(url,request_body,{content_type: :json, accept: :json, Authorization: '19VpZDXlUKuLPmkjXLhdIEIOFQi'})
        response = JSON(response)
        if response["success"]
          redirect_to response["redirect_url"]
        else
          raise StandardError.new "Failed"
        end
      end
    rescue StandardError => exc
      flash[:success] = exc.message == "Signature has expired" ? "Due Date has passed" : exc.message
      redirect_to new_user_session_path(role: "merchant")
    end
  end

  private
  def check_mobile_number(number)
    phone = Phonelib.parse(number)
    if phone.valid? && phone.type == :mobile
      return true
    else
      return false
    end
  end

  def set_params_for_invoice
    if params[:request_url].present?
      decode_token = JWT.decode params[:request_url], Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
      decode_token = decode_token.first
      params[:billing] = decode_token.try(:[],'billing')
      params[:shipping] = decode_token.try(:[],'shipping')
      params[:auth_token] = decode_token.try(:[],'auth_token')
      params[:request_id] = decode_token.try(:[],'request_id')
      params[:total_tax] = decode_token.try(:[],'total_tax')
      params[:total_amount] = decode_token.try(:[],'total_amount')
      params[:shipping_amount] = decode_token.try(:[],'shipping_amount')
      params[:location_id] = decode_token.try(:[],'location_id')
      params[:website_url] = decode_token.try(:[],'website_url')
      params[:success_url] = decode_token.try(:[],'success_url')
      params[:fail_url] = decode_token.try(:[],'fail_url')
      params[:invoice_checkout] = decode_token.try(:[],'invoice_checkout')
      @request = Request.in_progress.where(id: decode_token.try(:[],'request_id')).first
      raise StandardError.new "No pending Invoice found!" if @request.blank?
      if params[:total_tax].to_f > 0
        params[:total_tax] = (params[:total_tax].to_f/100)*@request.amount.to_f
      end
      if @request.apply_tax?
        if decode_token.try(:[],'total_tax').to_f > 0
          tax_on_SH = (decode_token.try(:[],'total_tax').to_f/100)*@request.shipping_handling_fee.to_f
          params[:total_tax] = (params[:total_tax].to_f + tax_on_SH).to_s
        end
      end
    end
  end

  def creating_rtp_transaction
    validate_parameters(
        :auth_token => params[:auth_token],
        :location_id => params[:location_id],
        :success_url => params[:success_url],
        :fail_url => params[:fail_url],
        :total_amount => params[:total_amount],
        :billing => params[:billing]
    )

    validate_parameters(
        "billing[phone_number]" => params[:billing][:phone_no],
        "billing[email]" => params[:billing][:email],
        "billing[first_name]" => params[:billing][:first_name],
        "billing[last_name]" => params[:billing][:last_name],
        "billing[city]" => params[:billing][:city],
        "billing[country]" => params[:billing][:country],
        "billing[state]" => params[:billing][:state],
        "billing[zip]" => params[:billing][:zip],
        "billing[street]" => params[:billing][:street],
        )
    merchant = User.find_by(authentication_token: params[:auth_token])

    raise StandardError.new I18n.t('errors.checkout.authentication_failed') if merchant.blank?

    raise StandardError.new I18n.t('errors.checkout.invalid_country_code') if Address.get_country_name(params[:billing].try(:[], :country)).blank?

    location = Location.where(location_secure_token: params[:location_id]).last

    raise StandardError.new  I18n.t('errors.checkout.no_location_found') if location.blank?

    #check user exist in wallet users list
    raise StandardError.new I18n.t('api.registrations.wrong_user') unless (location.merchant.id == merchant.id || location.merchant.id == merchant.merchant_id)

    # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => "Provided website url not present under location settings!"}, :ok) if allow == false
    # checking if merchant website is present under its location setting or not ------- end ------
    phone_number=params[:billing].try(:[],:phone_no)
    if phone_number.blank?
      phone_number=rand.to_s[2..11]
    end
    if params[:billing].present?
      first_name = params[:billing][:first_name]
      last_name = params[:billing][:last_name]
      name = params[:billing][:name].present? ? params[:billing][:name] : first_name == last_name ? first_name : "#{first_name} #{last_name}"
      email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
      user = User.where(phone_number: params[:billing][:phone_no],role: :user)
      user = user.first

      if user.present?
        user.source = merchant.name
        user.email = email
        user.is_block = false
        user.archived = false
        user.name = name
        user.first_name = first_name
        user.last_name = last_name
      else
        user = User.new
        user.name = name
        user.first_name = first_name
        user.last_name = last_name
        user.email = email
        user.phone_number = phone_number
        user.source = merchant.name
        password = random_password
        user.password = password
        user.role = :user
        user.is_block = false
        user.archived = false
      end
      user.save

      billing_email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
      billing_address = Address.where(id: params[:billing][:id]).first_or_initialize
      billing_address.name = params[:billing][:name]
      billing_address.first_name = params[:billing][:first_name]
      billing_address.last_name = params[:billing][:last_name]
      billing_address.suit = params[:billing][:suit]
      billing_address.email = billing_email
      billing_address.phone_number = params[:billing].try(:[],:phone_no)
      billing_address.street = params[:billing][:street]
      billing_address.city = params[:billing][:city]
      billing_address.state = params[:billing][:state]
      billing_address.country = params[:billing][:country]
      billing_address.zip = params[:billing][:zip]
      billing_address.address_type = "Billing"
      billing_address.user_id = user.id
      billing_address.save
    end

    if params[:shipping].present?
      shipping_email = params[:shipping][:email].present? ? params[:shipping][:email].strip.gsub(" ", "+") : ""
      shipping_address = Address.where(id: params[:shipping][:id]).first_or_initialize
      shipping_address.name = params[:shipping][:name]
      shipping_address.email = shipping_email
      shipping_address.phone_number = params[:shipping].try(:[],:phone_no)
      shipping_address.street = params[:shipping][:street]
      shipping_address.city = params[:shipping][:city]
      shipping_address.state = params[:shipping][:state]
      shipping_address.country = params[:shipping][:country]
      shipping_address.zip = params[:shipping][:zip]
      shipping_address.address_type = "Billing"
      shipping_address.user_id = user.id
      shipping_address.save
    end

    transaction = Transaction.new
    transaction = @request.invoice_transaction if @request.present? && @request.invoice_transaction.present?
    transaction.sender_name = user.name
    transaction.sender_id = user.id
    transaction.sender_wallet_id = user.wallets.first.try(:id)
    transaction.receiver_id = merchant.id
    transaction.receiver_name = merchant.name
    transaction.receiver_wallet_id = location.wallets.primary.first.try(:id)
    transaction.action = "transfer"
    transaction.billing_id = billing_address.try(:id)
    transaction.shipping_id = shipping_address.try(:id)
    transaction.save

    @request.products.update_all(transaction_id: transaction.id) if @request.present?



    payload = {api_checkout: true, billing_id: billing_address.id,  auth_token: params[:auth_token], transaction_id: transaction.id, merchant_id: merchant.id, user_id: user.id, success_url: params[:success_url], fail_url: params[:fail_url], redirect_url: params[:redirect_url], order_id: params[:order_id], request_id: params[:request_id], exp: (Time.zone.now + 60.minute).to_i}
    token = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM

    transaction.delivery_status = "pending"
    transaction.status = "pending"
    transaction.main_type = params[:request_id].present? ? TypesEnumLib::TransactionType::InvoiceRTP : TypesEnumLib::TransactionType::RTP
    transaction.sub_type = "invoice" if params[:request_id].present?
    transaction.tax = params[:total_tax].to_f
    transaction.shipping_amount = params[:shipping_amount].to_f
    transaction.amount = params[:total_amount]
    transaction.total_amount = params[:total_amount].to_f
    transaction.redirect_url = checkout_index_path(token: token)
    transaction.order_id = @request.present? ? @request.id : params[:order_id]
    transaction.save

    return {success: true, token: token, quickcard_id: transaction.quickcard_id}
  end

  def check_amount_limit(location)
    amounts = []
    if location.transaction_limit?
      unless location.transaction_limit_offset.include? params[:total_amount].to_f
        amounts = location.transaction_limit_offset.map{|a| "#{number_with_precision(number_to_currency(a))}"} if location.transaction_limit_offset.present?
        params[:location_id] = location.id
        raise StandardError.new  I18n.t('api.registrations.account_limit_true', amount_limit: amounts)
      end
    end
  end
end