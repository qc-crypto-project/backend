class Api::SessionsController < Api::ApiController
  include ExceptionHandler

  require 'twilio-ruby'
  require 'rqrcode'
  require 'barby'
  require 'barby/barcode'
  require 'barby/barcode/qr_code'
  require 'barby/outputter/png_outputter'
  require 'json'

  skip_before_action :authenticate_user, :only => [:create]

  def create
    if validate_parameters({email: params[:email], password:  params[:password] })
      user = User.find_by(:email => params[:email])
      raise UserNotFoundError if user.nil?
      raise UserDisabledError if user.archived || user.is_block
      if user.valid_password?(params[:password])
        user.regenerate_token
        user.save!
        return render :status => :ok, :json => user
      else
        return render_json_response({:success => false, :message => I18n.t('api.registrations.invalid_login')}, :ok)
      end
    end
  end

  def sign_out
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:authentication_token].present?
      # user = User.where(:authentication_token => token).first
      head 404 && return unless @user
      @user.update_attributes(:authentication_token => nil, device_token: nil, registeration_id: nil)
      return render_json_response({:success => true, :message => "Signed Out Successfully!"}, :ok)
    end
    return render_json_response({:success => false, :message => "Invalid Params!"}, :ok)
  end

  def destroy
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    if params[:authentication_token].present?
      user = User.where(:authentication_token => token).first
      return render_json_response({:success => false, :message => "User not Found!"}, :not_found)
      # if user.update_attributes(:device_token => nil, :authentication_token => nil)
      #   return render_json_response({:success => true, :message => "Signed Out Successfully!"}, :ok)
      # end
    else
      return render_json_response({:success => false, :message => "Invalid Params!"}, :ok)
    end
  end

  private

  def sign_in(user, customer)
    if user.setting.touch_id_login == false
      @random_code = rand(1_000..9_999)
      @encrypted = AESCrypt.encrypt("#{ENV['ENCRYPTION-CONFIRMATION-CODE']}",  "#{@random_code}")
      user.update!(:confirmation_code => @random_code, :confirmation_code_enc => @encrypted)
      TextsmsWorker.perform_async(params[:phone_number], @random_code)
      # send_text_message(@random_code, params[:phone_number])
      signed_in(user, customer, params[:device_token])
    else
      if user.device_token == nil || user.device_token != params[:device_token]
        @random_code = rand(1_000..9_999)
        @encrypted = AESCrypt.encrypt("#{ENV['ENCRYPTION-CONFIRMATION-CODE']}", "#{@random_code}")
        user.update(:confirmation_code => @random_code, :confirmation_code_enc => @encrypted)
        TextsmsWorker.perform_async(params[:phone_number], @random_code)
        # send_text_message(@random_code, params[:phone_number])
        user.setting.update(touch_id_login: false)
        signed_in(user, customer, params[:device_token])
      else
        signed_in(user, customer, params[:device_token])
      end
    end
  end

  def signed_in(user, customer, device_token)
    user.regenerate_token
    user.device_type = params[:device_type]
    user.save!
    wallet = user.wallets.first
    wallet_id = wallet.id
    balance = show_balance(wallet_id)
    wallet_name = wallet.name
    transactions = single_page_transactions(wallet.id, wallet.id, params[:next])
    myTransaction = []
    nextVar = ""
    last_page = true
    if transactions[:transactions].present?
      myTransaction = transactions[:transactions]
      nextVar = transactions[:next]
      last_page = transactions[:last_page]
    end
    fee_lib= FeeLib.new
    return render_json_response({
                                    :success => true,
                                    :user => UserSerializer.new(user),
                                    :auth_token => user.authentication_token,
                                    :message => I18n.t('api.registrations.success_login'),
                                    :wallet_id => wallet_id,
                                    :wallet_qr => "https:#{user.wallets.first.qr_image}",
                                    :wallet_name => wallet_name,
                                    :code => @random_code,
                                    :balance => balance,
                                    :card_fee =>  fee_lib.card_fee('card'),
                                    :e_check_fee =>  fee_lib.card_fee('echeck'),
                                    :gift_card_fee =>  fee_lib.card_fee('gift_card'),
                                    :transactions => myTransaction,
                                    :next => nextVar,
                                    :last_page => last_page,
                                    :show_gift_card => AppConfig.exists?(key:'gift_cards_config') ? AppConfig.find_by(key:'gift_cards_config').boolValue : false
                                }, :ok)
  end

  def oauth_redirect(resource)
    redirect_uri = session[:oauth_app]['redirect_uri']
    redirect_to "#{redirect_uri}?code=#{resource.oauth_code}"
  end
end
