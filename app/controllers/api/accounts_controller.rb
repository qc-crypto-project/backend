class Api::AccountsController < Api::ApiController
  include ApplicationHelper

  def create
    message = I18n.t('api.errors.incomplete')
    success = false
    account = nil
    if @user
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        if params[:stripe_public_token].present? && account_params[:customer_id].present? && account_params[:name].present?
          bank_token = ach_stripe(params[:stripe_public_token], account_params[:customer_id])
          customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
          bank_account = customer.sources.create(source: bank_token)
          # customer = Stripe::Customer.create(:source => bank_token, :email => @user.email)
          account = @user.accounts.build(stripe_bank_account_token: bank_account.id, name: account_params[:name], customer_id: customer.id)
          if account.save
            success = true
            message = I18n.t 'api.errors.save'
          end
        end
        if account_params[:plaid_token].present? && account_params[:name].present? && account_params[:customer_id].present?
          if account.nil?
            account = @user.accounts.create(account_params)
            if account.present?
              success = true
              message = I18n.t 'api.errors.save'
            end
          else
            if account.update(account_params)
              success = true
              message = I18n.t 'api.errors.save'
            end
          end
        end
        return render_json_response({:success => success, :message => message, :data => account}, :ok)
      rescue  => e
        p "Exception Handled : ", e
        return render_json_response({success: false, message: "Exception Occured: #{e}" },:ok)
      end
    end
  end

  def delete
    message = I18n.t 'api.errors.incomplete'
    success = false
    if @user
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        if params[:id].present?
          account = Account.find(params[:id])
          if account.user_id != @user.id
            return render_json_response({success: false, message: "Invalid Account Id sent!"},:ok)
          end
          if account.stripe_bank_account_token && account.customer_id
            customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
            customer.sources.retrieve(account.stripe_bank_account_token).delete()
            # customer = Stripe::Customer.retrieve(account.customer_id)
            # customer.delete
          end
          account.destroy
          success = true
          message = I18n.t 'api.errors.delete'
        end
        return render_json_response({success: success, message: message},:ok)
      rescue Stripe::CardError, Stripe::InvalidRequestError => e
        p "Exception Handled : ", e
        account.destroy
        return render_json_response({success: true, message: "Account Deleted but stripe customer token was invalid!"},:ok)
      rescue => e
        p "Exception Handled : ", e
        return render_json_response({success: false, message: "Something went wrong #{e.message}"},:ok)
      end
    end
  end

  def phone_api
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters(
        auth_token: params[:auth_token],
        phone_number: params[:phone_number]
      )
      user = User.user.where(phone_number: params[:phone_number]).last
      if user.present?
        return render_json_response({:success => true, message: user.name, wallet_id: user.wallets.first.try(:id)}, :ok)
      else
        return render_json_response({:success => true, message: "New Customer"}, :ok)
      end
    rescue ArgumentError => exc
      return render_json_response({:success => false, message: exc.message}, :ok)
    rescue StandardError => exc
      return render_json_response({:success => false, message: "Communication Failed"}, :ok)
    end
  end

  private

  def account_params
    params.require(:account).permit(:name ,:plaid_token, :stripe_token, :customer_id)
  end
end
