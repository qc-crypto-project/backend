class Api::Users::SalesController < Api::ApiController

  def qr_scan
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters(:auth_token => params[:auth_token])
      qr_card = nil
      if params[:qr_token].present?
        qr_card = QrCard.where(token: params[:qr_token]).last
      elsif params[:request_id].present?
        qr_card = Request.find_by(id: params[:request_id]).try(:qr_card)
      end
      raise StandardError.new("Couldn't find the QR Code in our system!") if qr_card.blank?
      transaction_method(qr_card)
    rescue StandardError => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end

  def search
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters(
          :auth_token => params[:auth_token],
          :phone_number => params[:phone_number]
      )
      customer = User.find_by(phone_number: params[:phone_number])
      raise StandardError.new("No Customer found!") if customer.blank?

      requests = []
      Request.eager_load(qr_card: :card_transaction).where(reciever_id: @user.id, sender_id: customer.id, status: "in_progress", created_at: Time.now.utc - 1.hour..Time.now.utc).each do|req|
        qr_card = req.qr_card
        issue_transaction = req.qr_card.card_transaction
        requests.push({customer_name:customer.try(:name) || "#{customer.try(:first_name)} #{customer.try(:last_name)}",request_id: req.id,token: qr_card.token, transaction_amount: number_to_currency(number_with_precision(issue_transaction.amount.to_f), precision: 2), tip: number_to_currency(number_with_precision(issue_transaction.tip.to_f), precision: 2), load_wallet_fee: number_to_currency(number_with_precision(issue_transaction.fee.to_f), precision: 2), cash_back_fee: number_to_currency(number_with_precision(issue_transaction.privacy_fee.to_f), precision: 2), total_amount: number_to_currency(number_with_precision(issue_transaction.total_amount.to_f), precision: 2)})
      end
      raise StandardError.new("No pending requests found!") if requests.blank?
      return render_json_response({:success => true, :message => "Successfully Retrieved", requests: requests}, :ok)
    rescue StandardError => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end


  def signature_sale
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters(:auth_token => params[:auth_token],:signature_image => params[:signature_image])
      raise StandardError.new("Invalid signature") unless ["image/jpg", "image/jpeg", "image/gif", "image/png"].include?(params[:signature_image].try(:content_type))
      qr_card = nil
      if params[:qr_token].present?
        qr_card = QrCard.where(token: params[:qr_token]).last
      elsif params[:request_id].present?
        qr_card = Request.find_by(id: params[:request_id]).try(:qr_card)
      end
      raise StandardError.new("Couldn't find the QR Code in our system!") if qr_card.blank?
      transaction_method(qr_card)
    rescue StandardError => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end

  private
  def transaction_method(qr_card)
    merchant_wallet = Wallet.eager_load(:location).where(id: qr_card.wallet_id).last

    raise StandardError.new("Invalid Token!") unless @user.id == qr_card.request.reciever_id

    location = merchant_wallet.location
    raise StandardError.new(I18n.t("errors.withdrawl.blocked_location")) if location.is_block
    raise StandardError.new("QR Code already used") if qr_card.blank? || !qr_card.is_valid

    user = qr_card.user
    user_wallet = Wallet.where(id: qr_card.from_id).last
    raise StandardError.new("You can't charge your own card!") if @user.id == user.id
    raise StandardError.new("Couldn't find the QR Code in our system!") if user.blank? || user_wallet.blank?
    raise StandardError.new("Invalid Destination Account! Contact App Administrator.") if merchant_wallet.blank?

    amount = qr_card.price.to_f
    balance = show_balance(qr_card.from_id)

    raise StandardError.new("Insufficiant QR Code Balance!") if balance.to_f < amount.to_f

    issue_transaction = qr_card.card_transaction
    gateway = issue_transaction.payment_gateway
    card = issue_transaction.card
    if issue_transaction.tip.to_f > 0
      tip_details = {
          sale_tip: issue_transaction.tip,
          wallet_id: location.wallets.tip.first.id
      }
    end
    # amount = (qr_card.price.to_f + issue_transaction.fee.to_f + issue_transaction.privacy_fee.to_f) - issue_transaction.tip.to_f
    amount = qr_card.price.to_f  - issue_transaction.tip.to_f
    data = {
        "id" => issue_transaction.seq_transaction_id,
        "transaction_id"=>issue_transaction.charge_id,
        "timestamp"=>issue_transaction.timestamp,
        "type"=>issue_transaction.main_type,
        "sub_type"=>issue_transaction.sub_type,
        "destination"=>issue_transaction.receiver_wallet_id,
        "issue_amount"=>issue_transaction.total_amount,
        "tags"=>issue_transaction.tags,
        "fee"=>issue_transaction.fee,
        "privacy_fee"=>issue_transaction.privacy_fee,
        "tip"=>issue_transaction.tip,
        "reserve_money"=>issue_transaction.reserve_money,
        "card_id"=>issue_transaction.card_id,
        "first6"=>issue_transaction.first6,
        "last4"=>issue_transaction.last4,
        "discount" => issue_transaction.discount
    }
    if issue_transaction.sub_type == "qr_debit_card"
      batch_type = "debit"
      main_type = TypesEnumLib::TransactionSubType::QRDebitCard
    else
      batch_type = "credit"
      main_type = TypesEnumLib::TransactionSubType::QRCreditCard
    end
    current_transaction = transaction_between(qr_card.wallet_id,qr_card.from_id,amount.to_f, main_type,0, nil, data,@user.ledger, gateway.try(:key),nil,card.try(:card_type).try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction,card,tip_details,nil,gateway, card.try(:card_type), issue_transaction.try(:privacy_fee).to_f,nil,issue_transaction.amount, true )
    raise StandardError.new("Something went wrong please try again") if balance.to_f < amount.to_f
    Sidekiq::Status.cancel "#{qr_card.job_id}"
    qr_card.is_valid = false
    qr_card.save
    request = qr_card.request
    old_type = request.status.dup
    request.update(status: "approved", qr_scanned: true) if request.present?
    request.create_qr_batch(batch_type,old_type)
    save_signature(request) if params[:signature_image].present?

    return render_json_response({:success => true, :message => "Successfully Approved", transaction_amount: number_to_currency(number_with_precision(issue_transaction.amount.to_f), precision: 2), tip: number_to_currency(number_with_precision(issue_transaction.tip.to_f), precision: 2), load_wallet_fee: number_to_currency(number_with_precision(issue_transaction.fee.to_f), precision: 2), cash_back: number_to_currency(number_with_precision(issue_transaction.privacy_fee.to_f), precision: 2), total_amount: number_to_currency(number_with_precision(issue_transaction.total_amount.to_f), precision: 2),customer_name:user.try(:name) || "#{user.try(:first_name)} #{user.try(:last_name)}"}, :ok)
  end

  def save_signature(request)
    params[:request_id] = request.id
    params[:add_image] = params[:signature_image]
    params[:usage_id] = request.sender_id
    signature = Image.new(doc_params)
    if signature.validate
      signature.save
    else
      raise StandardError.new signature.errors.full_messages.first
    end
  end

  def doc_params
    params.permit(:add_image,:request_id,:usage_id)
  end
end
