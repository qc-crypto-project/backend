class Api::Users::SettingsController <  Api::ApiController

  def settings
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    @setting = @user.setting
    return render_json_response({success: true, settings: SettingSerializer.new(@setting) },:ok)
  end

  def update_settings
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    @setting = @user.setting.update(update_settings_params)
     if @setting
      @setting = @user.setting
       return render_json_response({success: true, settings: SettingSerializer.new(@setting) },:ok)
     else
       return render_json_response({success: false, message: "Something went wrong" },:ok)
     end
  end

  def notifier
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    if params[:type].present?
      if params[:type] == "email"
        if NotificationMailer.send_email(params[:to]).deliver_later
          return render_json_response({success: true, message: "Successfully Sent emil" },:ok)
        end
      elsif params[:type] == "number"
        str = params[:to].to_s.scan(/\d+/).join
        if str.first == "0"
          str = "92#{str.sub!(/^0/, "")}"
        elsif str.first == "+"
          str.slice!(0)
        else
          str = "1#{str}"
        end
        send_text_message("Please click the link below or download the app Quick Card \n https://quickcard.herokuapp.com", str)
        return render_json_response({success: true, message: "Successfully Sent msg on your number!" },:ok)
      else
        return render_json_response({success: true, message: "Type should be email or number" },:ok)
      end
    else
      return render_json_response({success: false, message: I18n.t('errors.withdrawl.incomplete_service') },:ok)
    end
  end

  private
  def update_settings_params
    params.permit(:push_notification, :touch_id_login, :touch_id_transaction)
  end
end