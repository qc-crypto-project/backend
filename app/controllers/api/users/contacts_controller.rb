class Api::Users::ContactsController < Api::ApiController
  include ApplicationHelper

  def contacts
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated")},:ok)
    if params[:numbers].present?
      list = []
      begin
        params[:numbers].map do |str|
          str = str.to_s.scan(/\d+/).join
          if str.first == "0"
            list.push("92#{str.sub!(/^0/, "")}" )
          elsif str.first == "+"
            str.slice!(0)
            list.push(str)
          else
            list.push("1#{str}")
          end
        end
        @users = User.where('phone_number IN (?)',list)
        if @users.present?
          @friends = @user.friends.where.not(id: @user.id)
          @friends = @friends.map{|u| {id: u.id, name: u.name, phone_number: u.phone_number, wallet_id: u.wallet_id}}
          return render_json_response({success: true, message: "You got a list of users", users:  @friends},:ok)
        else
          @users = @user.friends
          @users = @users.map{|u| {id: u.id, name: u.name, phone_number: u.phone_number, wallet_id: u.wallet_id}}
          return render_json_response({success: true, users: @users },:ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({success: false, message: I18n.t('errors.withdrawl.incomplete_service')},:ok)
    end
  end

  def contact_list
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
     @friends =  @user.friends.where.not(id: @user.id)
     if @friends.present?
       @friends = @friends.compact.map{|u| {id: u.id, name: u.name, phone_number: u.phone_number, wallet_id: u.wallet_id} }
       return render_json_response({success: true, message: "You got a list of friends", friends:  @friends},:ok)
     else
       return render_json_response({success:  true, users: []},:ok)
     end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def wallet_details
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:wallet_id].present?
        wallet = Wallet.includes(:users).where(id: params[:wallet_id]).first
        if wallet.present?
          user = wallet.users.first
          if user.present?
            @friends_count = user.friends.count
            # user_transactions = all_transactions(params[:wallet_id], params[:wallet_id], params[:page_number])
            between_us_transactions = between_us_transactions(@user.wallet_id, params[:wallet_id], params[:page_number])
            return render_json_response({success: true, message: "Wallet Found", friends_count:  @friends_count, between_us_transactions: between_us_transactions, user: OnlyuserSerializer.new(user)},:ok)
          else
            return render_json_response({success: false, message: "Wallet not Found"},:ok)
          end
        else
          return render_json_response({success: false, message: "Wallet not Found"},:ok)
        end

      else
        return render_json_response({success: false, message: "Empty Service"},:ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def wallet_transactions_pages
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:wallet_id].present? and params[:page_number].present?
        wallet = Wallet.includes(:users).where(id: params[:wallet_id]).first
        if wallet.present?
          user = wallet.users.first
          if user.present?
            user_transactions = all_transactions(params[:wallet_id], params[:wallet_id], params[:page_number])
            return render_json_response({success: true, message: "Wallet Found",  user_transactions: user_transactions, last_page: (user_transactions.nil? or user_transactions.empty?) ? true : false },:ok)
          else
            return render_json_response({success: false, message: "Wallet not Found"},:ok)
          end
        else
          return render_json_response({success: false, message: "Wallet not Found"},:ok)
        end

      else
        return render_json_response({success: false, message: "Empty Service"},:ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end

  def between_us_transactions_pages
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:wallet_id].present? and params[:page_number].present?
        wallet = Wallet.includes(:users).where(id: params[:wallet_id]).first
        if wallet.present?
          user = wallet.users.first
          if user.present?
            between_us_transactions = between_us_transactions(@user.wallet_id, params[:wallet_id], params[:page_number])
            return render_json_response({success: true, message: "Wallet Found",  between_us_transactions: between_us_transactions, last_page: (between_us_transactions.nil? or between_us_transactions.empty?) ? true : false },:ok)
          else
            return render_json_response({success: false, message: "Wallet not Found"},:ok)
          end
        else
          return render_json_response({success: false, message: "Wallet not Found"},:ok)
        end

      else
        return render_json_response({success: false, message: "Empty Service"},:ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  end



end
