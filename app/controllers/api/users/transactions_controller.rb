class Api::Users::TransactionsController < Api::ApiController

  def webhook
    raise StandardError.new(I18n.t("errors.checkout.depreciated"))
    payload = request.body.read
    sig_header = request.env['HTTP_STRIPE_SIGNATURE']
    event = nil
    endpoint_secret = "whsec_PM5zvtHIUiG9iqvGWlCjh7DZvPxJ3cJy"

    begin
      event = Stripe::Webhook.construct_event(
        payload, sig_header, endpoint_secret
      )
      if params[:type] == 'charge.succeeded'
        transaction = Transaction.where(charge_id: params[:data][:object][:id]).first
        if transaction.present?
          if transaction.status == "pending"
            issue = issue_with_stripe(params[:data][:object][:amount].to_i, transaction.to, "By Stripe")
            transaction.update(status: "approved")
            balance = show_balance(transaction.to)
            return render_json_response({success: true, balance:balance}, :ok)
          else
            return render_json_response({success: false,message: 'Already succeeded or cencelled.'}, :ok)
          end
        else
          return render_json_response({success: false, message: "not found"}, :ok)
        end
      elsif params[:type] == "charge.failed"
        transaction = Transaction.where(charge_id: params[:data][:object][:id]).first
        if transaction.present?
          if transaction.status == "pending"
             transaction.update(status: "cancelled")
             return render_json_response({success: false, message: "cencelled"}, :ok)
          else
            return render_json_response({success: false, message: "Already succeeded or cencelled."}, :ok)
          end
        else
           return render_json_response({success: false, message: "Not found."}, :ok)
        end
      else
        return render_json_response({success: false, message: "nothing"}, :ok)
      end
    rescue => e
      return render_json_response({success: false, message: "Something went wrong #{e.message}"}, :ok)
    end
  end

end
