class Api::Users::TicketsController < Api::ApiController

  def create_ticket
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:ticket_message].present? and params[:title].present?
        user = @user
        ticket = Ticket.new(message: params[:ticket_message],user_id: user.id, title: params[:title] )
        ticket.save
        return render_json_response({success: true, ticket: ticket },:ok)
      else
        return render_json_response({success: false, message: I18n.t('errors.withdrawl.incomplete_service') },:ok)
      end
    rescue => e
      return render_json_response({success: false, message: "Somthing Went wrong" },:ok)
    end
  end

  def tickets
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      tickets = @user.tickets.order(updated_at: :desc)
      return render_json_response({success: true, tickets: tickets },:ok)
    rescue  => e
      return render_json_response({success: false, message: "Somthing Went wrong" },:ok)
    end
  end


  def ticket
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      ticket = @user.tickets.where(id: params[:ticket_id]).first
      if ticket.nil?
        return render_json_response({success: false, message: "Ticket not found" },:ok)
      else
        comments = ticket.comments.order(created_at: :desc)
        return render_json_response({success: true, ticket: ticket , comments: ActiveModel::Serializer::ArraySerializer.new(comments, each_serializer: CommentSerializer) },:ok)
      end
    rescue  => e
      return render_json_response({success: false, message: "Somthing Went wrong" },:ok)
    end
  end

  def create_comment
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:ticket_id].present? and params[:body].present?
        ticket = Ticket.where(id: params[:ticket_id]).first
        if ticket.nil?
          return render_json_response({success: false, message: "Ticket not found" },:ok)
        else
         comment = Comment.new(ticket_id: params[:ticket_id], user_id: @user.id, body: params[:body])
         comment.save
         Ticket.where(id: params[:ticket_id]).first.update(:get_attention => true)
         return render_json_response({success: true, comment:  CommentSerializer.new(comment)},:ok)
        end
      else
        return render_json_response({success: false, message: I18n.t('errors.withdrawl.incomplete_service') },:ok)
      end
    rescue => e
      return render_json_response({success: false, message: "Something Went wrong #{e.message}" },:ok)
    end
  end
end
