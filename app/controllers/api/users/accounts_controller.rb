class Api::Users::AccountsController < Api::ApiController

  def create
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:stripe_public_token].present? and params[:account_id].present? and params[:name].present?
        bank_token = ach_stripe(params[:stripe_public_token],params[:account_id])
        customer = Stripe::Customer.create(
          :source => bank_token,
          :email => @user.email
        )
        account = @user.accounts.build(stripe_bank_account_token: bank_token, name: params[:name], customer_id: customer.id)
        account.save
        return render_json_response({success: true, account: account },:ok)
      else
        return render_json_response({success: false, message: "incomplete params" },:ok)
      end
    rescue  => e
      return render_json_response({success: false, message: "Something went wrong #{e}" },:ok)
    end
  end

  def delete
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:stripe_bank_account_token].present?
        customer = Stripe::Customer.retrieve(params[:stripe_bank_account_token])
        customer.delete
        @user.accounts.where(stripe_bank_account_token: params[:stripe_bank_account_token]).delete_all
        accounts = @user.accounts.all
        return render_json_response({success: true, message: "Successfully",accounts: accounts},:ok)
      else
        return render_json_response({success: false, message: "incomplete service"},:ok)
      end
    rescue => e
      return render_json_response({success: false, message: "Something went wrong #{e.message}"},:ok)
    end
  end
end