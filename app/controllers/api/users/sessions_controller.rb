class Api::Users::SessionsController < Api::ApiController
  # include ApiHandler
  skip_before_action :authenticate_user, only: [:create,:forget_password]

  def create
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters(
          :email => params[:email],
          :password => params[:password]
      )
      user = User.merchant.find_by(email: params[:email])
      raise StandardError.new("Invalid Credentials") if user.blank? || !user.valid_password?(params[:password])
      user.regenerate_token
      user.save!
      return render_json_response({:success => true, :message => "success", :auth_token => user.authentication_token}, :ok)
    rescue StandardError => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end

  def forget_password
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      validate_parameters(
          :email => params[:email]
      )
      user = User.merchant.find_by(email: params[:email])
      user.send_reset_password_instructions if user.present?
      return render_json_response({:success => true, :message => "If your email is in our system, a password reset link has been sent."}, :ok)
    rescue StandardError => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end

  def sign_out
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      @user.regenerate_token
      @user.save
      return render_json_response({:success => true, :message => "Sign Out Successfully!"}, :ok)
    rescue StandardError => exc
      return render_json_response({:success => false, :message => exc.message}, :ok)
    end
  end

end
