class Api::SupportsController < Api::ApiController
  include ExceptionHandler
  include Admins::BlockTransactionsHelper
  skip_before_action :authenticate_user, only: [:update_block,:update_block_import]

  def create
    validate_parameters(params)
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      user = User.find_by_email(params[:email])
      if user.present?
      if params[:description].present? && params[:device_type].present?
        ticket= Ticket.new(message: params[:description],user_id: user.id, title: 'App Support',device_type: params[:device_type])
         ticket.save
         UserMailer.support_email(user,'App Support',ticket).deliver_later
         return render_json_response({:success => true, :message => "Successfully sent"}, :ok)
      else
        return render_json_response({:success => false, :message => "Decription or device type is missing "}, :ok)
      end
      else
        return render_json_response({:success => false, :message => "User not found with this email !"}, :ok)
      end
    rescue =>  ex
      p "====EXCEPTION HANDLED===="
      return render_json_response({:success => false, :message => ex.message}, :ok)
    end
  end

  def update_block
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:seq_data].present?
        tx_object = OpenStruct.new(eval(params[:seq_data]))
        block_tx = BlockTransaction.where(sequence_id: tx_object.id.gsub('\\x','')).first
        if block_tx.blank?
          cron_job = CronJob.create(name: "sequence_missing")
          data = parse_single_block(tx_object)
          wallets = Wallet.includes(:users).where(id: [data[:sender], data[:receiver]]).group_by{|e| e.id}
          sender_user = wallets[data[:sender].to_i].try(:first).try(:users).try(:first) if wallets[data[:sender].to_i].present?
          receiver_user = wallets[data[:receiver].to_i].try(:first).try(:users).try(:first) if wallets[data[:receiver].to_i].present?
          sender_dba=get_dba_name(nil,wallets[data[:sender].to_i].try(:first))
          receiver_dba=get_dba_name(nil,wallets[data[:receiver].to_i].try(:first))
          block_tx = create_block_transaction(data,sender_user,receiver_user,sender_dba, receiver_dba,tx_object.transaction_id.gsub('\\x',''))
          msg = "*Block Transaction Added* \n sequence_id: \`\`\`#{block_tx.sequence_id}\`\`\` amount: \`\`\`#{block_tx.amount_in_cents}\`\`\` sender: \`\`\` #{block_tx.sender_name} \`\`\`\`\`\` receiver: \`\`\` #{block_tx.receiver_name} \`\`\`"
          SlackService.notify(msg,"#qc-files")
          cron_job.result = {id: block_tx.sequence_id}
          cron_job.sequence_ids!
          return render_json_response({:success => true, :id => block_tx.id, sequence_id: data[:id]}, :ok)
        else
          return render_json_response({success: true, id: block_tx.sequence_id}, :ok)
        end
      end
    rescue => exc
      return render_json_response({:success => true, message: exc.message}, :ok)
    end
  end

  def update_block_import
    begin
      raise StandardError.new(I18n.t("errors.checkout.depreciated"))
      if params[:seq_data].present?
        object = eval(params[:seq_data])
        block_transactions = []
        object.each do |obj|
          tx_object = OpenStruct.new(obj)
          data = parse_single_block(tx_object)
          block_transactions << BlockTransaction.new(
                sender_wallet_id: data[:sender].to_i,
                receiver_wallet_id: data[:receiver].to_i,
                amount_in_cents: data[:amount],
                tx_amount: data[:transaction_amount],
                fee_in_cents: data[:fee],
                tip_cents: data[:tip],
                hold_money_cents: data[:hold_money],
                net_fee: data[:net_fee].to_f,
                total_net: data[:net_amount].to_f,
                privacy_fee: data[:privacy_fee],
                iso_fee: data[:iso_fee],
                agent_fee: data[:agent_fee],
                partner_fee: data[:partner_fee],
                gbox_fee: data[:gbox_fee],
                affiliate_fee: data[:affiliate_fee],
                action: data[:action].parameterize.underscore.to_sym,
                sequence_id: data[:sequence_id],
                seq_block_id: data[:seq_block_id],
                timestamp: data[:timestamp].to_datetime,
                main_type: data[:type],
                sub_type: data[:sub_type],
                tx_type: data[:tx_type],
                tx_sub_type: data[:tx_sub_type],
                tags: data[:tags],
                last4: data[:last4],
                first6: data[:first6],
                card_id: data[:card_id],
                charge_id: data[:charge_id],
                gateway: data[:gateway],
                api_type: data[:api_type],
                ip: data[:ip],
                position: data[:position],
                status: :approved,
                location_dba_name: data[:location_dba_name],
                merchant_name: data[:merchant_name],
                source_name: data[:source_name],
                destination_name: data[:destination_name]
            )
        end
        if block_transactions.present? && block_transactions.count > 0
          BlockTransaction.import block_transactions, on_duplicate_key_update: {
              conflict_target: [:sequence_id],
              columns:
                  [
                      :sender_wallet_id,
                      :receiver_wallet_id,
                      :amount_in_cents,
                      :tx_amount,
                      :fee_in_cents,
                      :tip_cents,
                      :hold_money_cents,
                      :net_fee,
                      :total_net,
                      :privacy_fee,
                      :iso_fee,
                      :agent_fee,
                      :partner_fee,
                      :gbox_fee,
                      :affiliate_fee,
                      :action,
                      :sequence_id,
                      :seq_block_id,
                      :timestamp,
                      :main_type,
                      :sub_type,
                      :tx_type,
                      :tx_sub_type,
                      :tags,
                      :last4,
                      :first6,
                      :card_id,
                      :charge_id,
                      :gateway,
                      :api_type,
                      :ip,
                      :position,
                      :status,
                      :location_dba_name,
                      :merchant_name,
                      :source_name,
                      :destination_name,
                      :updated_at,
                      :created_at
                  ]
          }
        end
        return render_json_response({:success => true, sequence_ids: object.pluck(:id)}, :ok)
      end
    rescue => exc
      return render_json_response({:success => true, message: exc.message}, :ok)
    end
  end

  private

  def create_block_transaction(data,sender_user, receiver_user, sender_dba, receiver_dba, parent_id)
    parent_tx = BlockTransaction.where(seq_parent_id: parent_id, position: '0').select('block_transactions.id AS id','block_transactions.sequence_id AS sequence_id').first
    block = BlockTransaction.create(
        sender_id: sender_user.try(:id),
        receiver_id: receiver_user.try(:id),
        sender_wallet_id: data[:sender].to_i,
        receiver_wallet_id: data[:receiver].to_i,
        sender_name: sender_dba,
        receiver_name: receiver_dba,
        amount_in_cents: data[:amount],
        tx_amount: data[:transaction_amount],
        fee_in_cents: data[:fee],
        tip_cents: data[:tip],
        hold_money_cents: data[:hold_money],
        net_fee: data[:net_fee].to_f,
        total_net: data[:net_amount].to_f,
        privacy_fee: data[:privacy_fee],
        iso_fee: data[:iso_fee],
        agent_fee: data[:agent_fee],
        partner_fee: data[:partner_fee],
        gbox_fee: data[:gbox_fee],
        affiliate_fee: data[:affiliate_fee],
        action: data[:action].parameterize.underscore.to_sym,
        sequence_id: data[:sequence_id],
        seq_parent_id: parent_tx.try(:sequence_id),
        seq_block_id: data[:seq_block_id],
        parent_id: parent_tx.try(:id),
        timestamp: data[:timestamp].to_datetime,
        main_type: data[:type],
        sub_type: data[:sub_type],
        tx_type: data[:tx_type],
        tx_sub_type: data[:tx_sub_type],
        tags: data[:tags],
        last4: data[:last4],
        first6: data[:first6],
        card_id: data[:card_id],
        charge_id: data[:charge_id],
        gateway: data[:gateway],
        api_type: data[:api_type],
        ip: data[:ip],
        position: data[:position],
        status: :approved,
        location_dba_name: data[:location_dba_name],
        merchant_name: data[:merchant_name],
        source_name: data[:source_name],
        destination_name: data[:destination_name],
        main_amount: parent_tx.try(:amount_in_cents),
        clerk_id: data[:clerk_id],
        terminal_id: data[:terminal_id],
        ref_id: data[:ref_id],
        location_id: data[:location_id],
        iso_id: data[:iso_id],
        agent_id: data[:agent_id],
        affiliate_id: data[:affiliate_id],
        card_type: data[:card].try(:[], "card_type"),
        card_brand: data[:card].try(:[], "brand"),
        payment_gateway_id: data[:payment_gateway_id]
    )
    block
  end
  
end
