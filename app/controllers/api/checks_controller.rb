class Api::ChecksController < Api::ApiController
  include ApplicationHelper
  include Merchant::ChecksHelper

  @@check_book_key = ENV['CHECKBOOK_KEY']
  @@check_book_secret = ENV['CHECKBOOK_SECRET']
  def create
    if params[:auth_token].present? && User.where(authentication_token: params[:auth_token]).present?
      if  params[:amount].present?
        begin
          raise StandardError.new(I18n.t("errors.checkout.depreciated"))
          @wallet=Wallet.find(params[:wallet_id])
          @user= User.where(authentication_token: params[:auth_token]).first
          if @user.present?
            if  @wallet.present?
              fee_lib = FeeLib.new
              amount_total = 0
              if @user.MERCHANT?
                location = @wallet.location
                location_fee = location.fees.send_check.first if location.present?
                redeem_fee = location.fees.redeem_fee.first if location.present?
                check_fee = location_fee.send_check.to_f if location_fee.present?
                fee_perc = deduct_fee(redeem_fee.redeem_fee.to_f, amount)
                amount_total = amount + check_fee + fee_perc.to_f
              else
                amount_fee = fee_lib.get_card_fee(params[:amount], 'echeck').to_f
                amount = params[:amount].to_f
                amount_total = amount + amount_fee
              end

              if show_balance(@wallet.id).to_f >= amount_total
                @recipient = params[:recipient]
                if (@recipient =~ Devise.email_regexp) == 0 && @recipient.present?
                  url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/digital")
                  http = Net::HTTP.new(url.host,url.port)
                  http.use_ssl = true
                  http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                  request = Net::HTTP::Post.new(url)
                  request.body = {name: "#{@user.first_name} #{@user.last_name}", recipient: params[:recipient], amount: params[:amount].to_f}.to_json
                  request["Authorization"] = @@check_book_key+":"+@@check_book_secret
                  request["Content-Type"] ='application/json'
                  response = http.request(request)
                  case response
                    when Net::HTTPSuccess
                      digital_check = JSON.parse(response.body)
                      check = TangoOrder.new
                      check.name = "#{@user.first_name} #{@user.last_name}"
                      check.sender = @user.email
                      check.amount = digital_check["amount"]
                      check.user_id = @user.id
                      check.name = digital_check["name"]
                      check.recipient = digital_check["recipient"]
                      check.checkId = digital_check["id"]
                      check.number = digital_check["number"]
                      check.description = digital_check["description"]
                      check.catalog_image = digital_check["image_uri"]
                      check.status = digital_check["status"]
                      check.wallet_id = @wallet.id
                      check.status = true
                      check_info = {
                          name: check.name,
                          check_email: check.recipient,
                          check_id: check.checkId,
                          check_number: check.number
                      }
                      if @user.MERCHANT?
                        # qc_wallet = Wallet.where(wallet_type: 3).first
                        company = @user.company
                        company_user =  company.users.where(email: company.company_email).first
                        company_wallet = company_user.wallets.first.id
                        location = @wallet.location
                        location_fee = location.fees.send_check.first if location.present?
                        redeem_fee = location.fees.redeem_fee.first if location.present?
                        check_fee = location_fee.send_check.to_f if location_fee.present?
                        fee_perc = deduct_fee(redeem_fee.redeem_fee.to_f, amount)
                        fee_sum = amount + check_fee + fee_perc.to_f
                        total_fee_deduct = check_fee.to_f + fee_perc.to_f
                        if show_balance(@wallet.id).to_f > fee_sum
                          if check_fee > 0
                            user_info = lib.divide_send_check_fee(location,location_fee,company,redeem_fee, fee_perc ,check_fee, company_wallet )
                            withdraw(amount,@wallet.id, total_fee_deduct, TypesEnumLib::TransactionType::SendCheck, nil,'email',user_info,check_info)
                          else
                            withdraw(amount, @wallet.id, 0, TypesEnumLib::TransactionType::SendCheck, nil, 'email',nil, check_info)
                          end
                          # if fee_perc.to_f > 0
                          #   transfer = SequenceLib.transfer(fee_perc, @wallet.id, qc_wallet.id, 'fee', 0, 'withdraw_check_fee','stripe', '')
                          # end
                          # if withdraw.present?
                          #   fee = fee_lib.divide_merchant_check_fee(location_fee, location, company_wallet, company_user, qc_wallet, 'withdraw_check_fee')
                          #   fee_perc = fee_lib.divide_merchant_check_perc(redeem_fee, location, company_wallet,company_user, qc_wallet, fee_perc, 'withdraw_check_fee')
                          # end
                        else
                          return render_json_response({success: false, message: I18n.t('merchant.controller.insufficient_balance')}, :ok)
                        end
                      else
                        withdraw(amount,@wallet.id, fee_lib.get_card_fee(params[:amount], 'echeck'), 'withdraw_check', nil, 'email', nil, check_info)
                      end
                      check.save
                      return render_json_response({success: true, message: "Successfully send a check of $#{number_with_precision(check.amount, precision: 2)} to #{ check.recipient }"}, :ok)
                    when Net::HTTPUnauthorized
                      return render_json_response({success: false, message: I18n.t('merchant.controller.unauthorize_access')}, :ok)
                    when Net::HTTPNotFound
                      return render_json_response({success: false, message: I18n.t('merchant.controller.record_notfound')}, :ok)
                    when Net::HTTPServerError
                      return render_json_response({success: false, message: I18n.t('merchant.controller.server_notrespnd')}, :ok)
                    else
                      return render_json_response({success: false, message: I18n.t('merchant.controller.exception3')}, :ok)
                  end
                else
                  return render_json_response({success: false, message: I18n.t('api.registrations.invalid_email')}, :ok)
                end
              else
                return render_json_response({success: false, message: I18n.t('merchant.controller.insufficient_balance')}, :ok)
              end
            else
              return render_json_response({success: false, message: I18n.t('api.registrations.wrong_user')}, :ok)
            end
          else
            return render_json_response({success: false, message: I18n.t('errors.withdrawl.unathurized_user')}, :ok)
          end
        rescue => ex
          p"-----------EXCEPTION HANDLED #{ex.message}"
        end
      else
        render_json_response({success: false, message: I18n.t('errors.withdrawl.amount_missing')}, :ok)
      end
    else
      render_json_response({success: false, message: I18n.t('errors.withdrawl.unathurized_user')}, :ok)
    end

  end

  private
  def create_order
    params.require(:check).permit(:name , :sender ,:recepient ,:amount)
  end
end
