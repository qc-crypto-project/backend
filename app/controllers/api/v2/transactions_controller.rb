class Api::V2::TransactionsController < Api::ApiController
  include ApplicationHelper
  include FluidPayHelper
  include TransactionCharge::ChargeATransactionHelper
  include Merchant::SalesHelper
  include RegistrationsHelper
  include Api::RegistrationsHelper
  include Api::PossHelper
  include LoadBalancerHelper

  FAILED = 'failed'
  SUCCESS = 'success'

  before_action :location_ip_policy, only: [:virtual]
  before_action :check_bank_status, only: [:virtual]

  def virtual
    #check maintenance response
    return maintenance_response() if @api_config.present? && @api_config.fetch("virtual_transaction") { true }.to_s == 'false'
    return sequence_down() if @seq_down.present? && @seq_down == true
    return virtual_transaction_method
  end

  def virtual_transaction_method
    begin
      fee_lib = FeeLib.new
      stripe = Payment::StripeGateway.new
      # validate all required params
      # getting location and wallet
      if params[:location_id].present?
        @location = Location.where(location_secure_token: params[:location_id]).includes(load_balancer: :load_balancer_rules).first

        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_loc')}, :ok) unless @location.present?
        wallet = @location.wallets.primary.eager_load(:users).first
      else
        wallet = Wallet.find_by(id: params[:wallet_id])
        @location = wallet.location
      end



      raise SaleValidationError.new I18n.t('api.registrations.loc_sale_true') if @location.sales == true
      gateway = @location.primary_gateway.present? ? @location.primary_gateway.virtual_tx : @user.primary_gateway.present? ? @user.primary_gateway.virtual_tx : ''

      if gateway.present? && gateway == true # if virtual_tx checkbox in gateway is true then these params are mandatory country,city,zip_code,state,street
        if params[:card_token].present?
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :card_token => params[:card_token],
              :card_cvv => params[:card_cvv],
              :country => params[:country],
              :city => params[:city],
              :zip_code => params[:zip_code],
              :state => params[:state],
              :street => params[:street]
          )

          raise StandardError.new I18n.t('api.registrations.invalid_card_token') unless setting_params
        else
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :name => params[:name],
              :email => params[:email],
              :phone_number => params[:phone_number],
              :last_name => params[:last_name],
              :first_name => params[:first_name],
              :card_number => params[:card_number],
              :card_cvv => params[:card_cvv],
              :exp_date => params[:exp_date],
              :country => params[:country],
              :city => params[:city],
              :zip_code => params[:zip_code],
              :state => params[:state],
              :street => params[:street]
          )
        end
      else
        if params[:card_token].present?
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :card_token => params[:card_token],
              :card_cvv => params[:card_cvv],
              )

          raise StandardError.new I18n.t('api.registrations.invalid_card_token') unless setting_params
        else
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :name => params[:name],
              :email => params[:email],
              :phone_number => params[:phone_number],
              :last_name => params[:last_name],
              :first_name => params[:first_name],
              :card_number => params[:card_number],
              :exp_date => params[:exp_date],
              :card_cvv => params[:card_cvv],
              )
        end
      end


      raise SaleValidationError.new I18n.t('api.registrations.phone_no') if !check_string(params[:phone_number])
      params[:country] = params[:country] if params[:country].present?
      params[:street] = params[:street] if params[:street].present?
      raise StandardError.new I18n.t('api.registrations.amount_error') if params[:amount].to_f <= 0
      valid_email = EmailValidator.valid?(params[:email])
      raise StandardError.new I18n.t('api.registrations.invalid_email') if !valid_email
      #raise SaleValidationError.new "You can't make sale with your own account" if @user.phone_number == params[:phone_number]
      # getting user
      phone_number_checking = User.user.unarchived_users.where(phone_number: params[:phone_number])
      if phone_number_checking.present?  #===========>>>>> Existing User
        getting_user = phone_number_checking.select{|a| a.role == "user"}.first
        if getting_user.blank?
          user = phone_number_checking.first
        else
          user = getting_user
        end
        user.first_name = params[:first_name]
        user.last_name = params[:last_name]
        user.name = params[:name]
        user.save
        # creating new user with merchant or iso or agent or aff or company parameters
        if user.merchant? || user.iso? || user.agent? || user.affiliate? || user.partner?
          raise SaleValidationError.new I18n.t('api.registrations.unauth') if user.is_block? || user.archived
          user = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address,:city,:state, :role))
          user.source = @user.name if @user.present?
          user.role = :user
          user.ledger = @user.ledger if @user.present? && @user.ledger.present?
          password = random_password
          user.password = password
          user.regenerate_token
          user.save(:validate => false)
        end
        # raise SaleValidationError.new "Merchant can't make a sale. Please contact you administrator for details." if !user.user?
      else                                    #===================>>>>> New User
        # user = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address,:city,:state))
        user = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address,:address,:city,:state,:shipping_first_name,:shipping_last_name,:shipping_company,:shipping_address,:shipping_city,:shipping_state,:shipping_country,:shipping_zip_code))
        user.source = @user.name if @user.present?
        user.ledger = @user.ledger if @user.present? && @user.ledger.present?
        password = random_password
        user.password = password
        user.regenerate_token
        user.role = "user"
        user.save(:validate => false)
      end

      # ------------------------------Updating Shipping & Billing Info-------------------------------
      user.zip_code=params[:zip_code] if params[:zip_code].present?
      user.address=params[:address] if params[:address].present?
      user.city=params[:city] if params[:city].present?
      user.state=params[:state] if params[:state].present?
      user.shipping_first_name=params[:shipping_first_name] if params[:shipping_first_name].present?
      user.shipping_last_name=params[:shipping_last_name] if params[:shipping_last_name].present?
      user.shipping_company=params[:shipping_company] if params[:shipping_company].present?
      user.shipping_address=params[:shipping_address] if params[:shipping_address].present?
      user.shipping_city=params[:shipping_city] if params[:shipping_city].present?
      user.shipping_state=params[:shipping_state] if params[:shipping_state].present?
      user.shipping_country=params[:shipping_country] if params[:shipping_country].present?
      user.shipping_zip_code=params[:shipping_zip_code] if params[:shipping_zip_code].present?
      user.save(:validate => false)

      if user.wallets.primary.first.blank?
        user.create_wallet(user.name, user.id, 0, user)
        if user.wallets.primary.first.present?
          return_no_wallets(user, true)
        else
          return_no_wallets(user, false)
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.unexpected')}, :ok)
        end
      end
      raise SaleValidationError.new I18n.t('api.registrations.sale') if !user.user?
      raise SaleValidationError.new I18n.t('api.registrations.invalid_user') if user.blank?
      # raise SaleValidationError.new I18n.t('api.registrations.wrong_details') if !user.user?

      #creating card for records
      card_info = {
          number: params[:card_number],
          month: params[:exp_date].first(2),
          year: "20#{params[:exp_date].last(2)}",
          # cvv: params[:card_cvv]
      }
      #check is card valid
      # stripe.valid_card(params[:card_number]) #checking card is valid or not
      return render_json_response({success: false, status: TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card_data')}, :ok) if params[:exp_date].length != 4

      card_bin_list = get_card_info(params[:card_number])
      card_brand_type = "#{card_bin_list["scheme"]}_#{card_bin_list["type"] || 'credit'}".try(:downcase) if card_bin_list.present? && card_bin_list["scheme"].present?
      if @location.apply_load_balancer?
        load_balancer = TransactionLoadBalancer.new(@location, nil,card_brand_type)
        if !@location.load_balancer.active?
          error_code = "error_2009"
          return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: error_code.humanize, :message => I18n.t("api.load_balancers.#{error_code}")},:ok)
        end
        card_block = load_balancer.check_card_blocked(params[:card_number])
        if card_block.present?
          error_code = card_block
          return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: error_code.humanize, :message => I18n.t("api.load_balancers.#{error_code}")},:ok)
        end
      end
      card_block = check_card_blocked(params[:card_number],"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}", card_info)
      if card_block
        msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: Declined transaction because the Card is in the Blocked Cards List"
        SlackService.notify(msg, "#qc-risk-alert")
        raise ArgumentError.new I18n.t('api.errors.blocked_card')
      end
      if @location.vip_card && !Card.check_vip_card?(params[:card_number],"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}")
        msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: #{I18n.t("api.errors.non_vip_card")}"
        SlackService.notify(msg, "#qc-risk-alert")
        raise StandardError.new(I18n.t('api.errors.non_vip_card'))
      end

      # load balancer decline on card rule
      if @location.apply_load_balancer?
        if card_bin_list["country"].present? && card_bin_list["country"]["alpha2"].present?
          decline = load_balancer.decline_on_cc_country(card_bin_list["country"]["alpha2"])
          if decline
            return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, error_code:"Error 2005", message: I18n.t('api.load_balancers.error_2005')}, :ok)
          end
        end
      end

      if card_bin_list["bank"].present? && card_bin_list["bank"]["name"].present?
        card_bank = card_bin_list["bank"]["name"].present? ? card_bin_list["bank"]["name"] : ''
      end

      card1 = Payment::QcCard.new(card_info, nil, user.id)
      card = user.cards.where(fingerprint: card1.try(:fingerprint),merchant_id: @user.id).first
      if card.blank?
        if params[:card_cvv].blank?
          raise ArgumentError.new I18n.t('api.registrations.miss_cvv')
        end
        card = user.cards.new(qc_token:card1.qc_token,exp_date: "#{card_info[:month]}/#{card_info[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand, last4: params[:card_number].last(4), name: user.name, card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: params[:card_number].first(6).to_i,fingerprint: card1.fingerprint,merchant_id: @user.id,flag: "straight", bank: card_bank)
      else
        # if params[:card_cvv].blank? || params[:card_cvv] == "0"
        #   old_card_detail = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
        #   if old_card_detail.cvv.present?
        #     params[:card_cvv] = old_card_detail.cvv
        #     card.exp_date = "#{card_info[:month]}/#{card_info[:year].last(2)}"
        #     card.name = params[:card_holder_name]
        #     card.brand = card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand
        #     card.card_type = card_bin_list && card_bin_list["type"]
        #     card.merchant_id = @user.id
        #     card.bank = card_bank
        #   end
        # else
        card.qc_token = card1.qc_token
        card.exp_date = "#{card_info[:month]}/#{card_info[:year].last(2)}"
        card.name = params[:card_holder_name]
        card.brand = card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand
        card.card_type = card_bin_list && card_bin_list["type"]
        card.merchant_id = @user.id
        card.bank = card_bank
        # end
      end
      validate_parameters(
          :card_cvv => params[:card_cvv]
      )
      result = recurring_flaged_trans(card,params)
      raise SaleValidationError.new "#{result[:message]}" unless result[:success]

      if !@location.apply_load_balancer? && check_card_decline(card)
        card.update(decline_attempts: card.decline_attempts.to_i + 1)
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card')}, :ok)
      end

      raise SaleValidationError.new "No User exists" if wallet.try(:users).blank?
      raise SaleValidationError.new I18n.t('api.registrations.wrong_user') unless wallet.try(:users).pluck(:id).include?(@user.id)
      params[:wallet_id] = user.try(:wallets).try(:first).try(:id)
      params[:merchant_wallet_id] = wallet.id

      if wallet.present?
        raise SaleValidationError.new I18n.t("errors.withdrawl.blocked_location") if @location.is_block
        raise SaleValidationError.new I18n.t('api.registrations.unavailable') if @user.oauth_apps.first.is_block

        # checking sale limits for specific locations
        if @location.sale_limit.present?
          if @location.sale_limit_percentage.present?
            sale_limit = @location.sale_limit
            sale_limit_percentage = @location.sale_limit_percentage
            total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
            raise SaleValidationError.new I18n.t('api.registrations.trans_limit') if params[:amount].to_f > total_limit
          else
            raise SaleValidationError.new I18n.t('api.registrations.trans_limit') if params[:amount].to_f > @location.sale_limit
          end
        end

        # checking tip
        tip_amount = 0
        amount = 0
        if params[:tip].present? && params[:tip].to_f > 0
          tip_detail = nil
          tip_amount = params[:tip].to_f
          tip_wallet = @location.wallets.where(wallet_type: 'tip').first
          if tip_amount > 0
            tip_detail = {wallet_id: tip_wallet.try(:id),sale_tip: tip_amount}
          end
        end
        if tip_amount > 0
          amount = params[:amount].to_f + tip_amount.to_f
        else
          amount = params[:amount]
        end
      end

      # going for transaction
      data = {}
      params[:both] = false
      if user.present?
        reciever = user
        #getting users wallet
        reciever_wallet = reciever.wallets.first

        # getting wallet balance
        balance = show_balance(reciever_wallet.id, reciever.ledger)
        merchant_balance = show_balance(wallet.id, @user.ledger)
        buyRate = @location.fees.buy_rate.first
        amount_fee_check = check_fee_amount(@location, @user, user, wallet, buyRate, params[:amount], merchant_balance, nil, 'credit')

        return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.amount')}, :ok)if amount_fee_check.present? && amount_fee_check[:status] == false
        if amount_fee_check.present? && amount_fee_check[:status] == true
          if amount_fee_check[:splits].present?
            agent_fee = amount_fee_check[:splits]["agent"]["amount"]
            iso_fee = amount_fee_check[:splits]["iso"]["amount"]
            iso_balance = show_balance(@location.iso.wallets.primary.first.id)
            if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
              if iso_balance < iso_fee
                return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: 'ISO-0007 Cannot process transaction at the moment. Please contact administrator!'}, :ok)
              end
            else
              if iso_balance + iso_fee < agent_fee.to_f
                return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: 'ISO-0008 Cannot process transaction at the moment. Please contact administrator!'}, :ok)
              end
            end
          end
        end
        gateway = @location.primary_gateway.present? ? @location.primary_gateway.key : @user.primary_gateway.present? ? @user.primary_gateway.key : ''
        if gateway == TypesEnumLib::GatewayType::ICanPay
          gateway_check = check_icanpay(gateway, params)
          if gateway_check.present? && gateway_check == false
            return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.i_can_pay')}, :ok)
          end
        end
        # if wallet balance < amount to pay
        # if balance.to_f < params[:amount].to_f + tip_amount.to_f
        my_amount = params[:amount].to_f + tip_amount.to_f
        if balance.to_f != my_amount
          params[:both] = true
          new_amount = params[:amount].to_f
          fee_for_issue = 0
          # checking card fee
          if @user.present? && @user.MERCHANT? && !@user.issue_fee
            issue_amount = new_amount
          else
            issue_amount = new_amount + fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f + tip_amount
            fee_for_issue = fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f
          end

          params[:issue_amount] = issue_amount.to_f
          qc_wallet = Wallet.qc_support.first
          issue_raw_transaction = IssueRawTransaction.new(user.email,params,params[:card_cvv])

          seq_transaction_id = {:message => {:message => I18n.t('api.registrations.no_gateway')}}



          if @location.apply_load_balancer?
            load_amount = issue_amount + params[:privacy_fee].to_f + params[:tip].to_f
            load_balancer.after_getting_issue_amount(load_amount)
            @payment_gateway = nil
            return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.no_gateway')},:ok)  if load_balancer.payment_gateways.blank?

            #= LOAD BALANCER
            @payment_gateway = load_balancer.rotate_processor
            return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: "Error 2001", :message => I18n.t('api.load_balancers.error_2001')},:ok) if @payment_gateway.blank? && load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor? }.present?

            @payment_gateway = load_balancer.rotate_processor_with_cc(card,@payment_gateway)
            return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: "Error 2003", :message => I18n.t('api.load_balancers.error_2003')},:ok) if @payment_gateway.blank? && load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_with_cc? }.present?

            # @payment_gateway = new_payment_gateway if new_payment_gateway.present?

            if load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_with_amount? }.present? && load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_on_cc_brand?}.present?
              @payment_gateway = load_balancer.merger_of_rotation_on_amount_and_cc(load_amount.to_f,card.brand,@payment_gateway)
            elsif load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_with_amount?}.present?
              @payment_gateway = load_balancer.rotate_processor_with_amount(load_amount.to_f,@payment_gateway)
            elsif load_balancer.load_balancer_rules.select{|lbr| lbr.rotate_processor_on_cc_brand?}.present?
              @payment_gateway = load_balancer.rotate_processor_on_cc_brand(card.brand,@payment_gateway) if card.present? && card.brand.present?
            end
            if @payment_gateway.present?
              load_daily_amount = @payment_gateway.daily_volume_achived
              load_monthly_amount = @payment_gateway.monthly_volume_achived

              load_balancer.block_processor_daily_100(@payment_gateway,load_amount + load_daily_amount.to_f)
              load_balancer.block_processor_monthly_100(@payment_gateway,load_amount + load_monthly_amount.to_f)
              @payment_gateway.save
              if @payment_gateway.is_block?
                error_code = "error_2012"
                return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: error_code.humanize, :message => I18n.t("api.load_balancers.#{error_code}")},:ok)
              end

              load_balancer.update_card_group_transaction_attempts(card)
              load_balancer.block_card_for_processed_count(card)
              card.save

              seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@payment_gateway,card_bin_list,nil,nil,nil,params[:flag],@location.apply_load_balancer)
              gateway = @payment_gateway.key if @payment_gateway.present?

              unless seq_transaction_id.try(:[],:id).present?
                decline_message = nil
                if seq_transaction_id.try(:[],:message).try(:[],:message).present?
                  decline_message = seq_transaction_id[:message][:message]
                end
                load_balancer.update_card_transaction_attempts(card,@payment_gateway)
                load_balancer.update_gateway_transaction_count(@payment_gateway)
                load_balancer.block_card_for_reason(decline_message, card) if decline_message.present?
                load_balancer.update_gateway_decline_attempts(@payment_gateway,decline_message)
                load_balancer.processor_decline_times(@payment_gateway)
                load_balancer.processor_decline_percent(@payment_gateway)

                load_balancer.update_card_decline_transaction_attempts(card)
                load_balancer.block_card_for_decline_count(card)

                card.save
                @payment_gateway.save
              end
            else
              error_code = "error_2001"
              error_code = load_balancer.error_code if load_balancer.error_code.present?
              return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, error_code: error_code.humanize, :message => I18n.t("api.load_balancers.#{error_code}")},:ok)
            end
          else
            if @location.primary_gateway.present?
              @payment_gateway = @location.primary_gateway if !@location.primary_gateway.is_block
              #= LOAD BALANCER first_payment_gateway
              seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.secondary_gateway,card_bin_list,nil,nil,nil,params[:flag],@location.apply_load_balancer)
              gateway = @payment_gateway.key if @payment_gateway.present?
              if @location.primary_gateway.is_block && seq_transaction_id.nil?
                seq_transaction_id = {:message => {:message => "Processing: Inactive – Please contact customer support."}}
              end
              #= LOAD BALANCER secondary_payment_gateway
              if seq_transaction_id.try(:[],:response).blank? && seq_transaction_id.try(:[],:id).blank? && @location.secondary_gateway.present? && !@location.secondary_gateway.is_block
                message = ""
                if seq_transaction_id[:message][:message].present?
                  message = seq_transaction_id[:message][:message]
                end
                saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
                @payment_gateway = @location.secondary_gateway
                seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.ternary_gateway,card_bin_list,nil,nil,nil,params[:flag],@location.apply_load_balancer)
                gateway = @payment_gateway.key
              end
              #= LOAD BALANCER third_payment_gateway
              if seq_transaction_id.try(:[],:response).blank? && seq_transaction_id.try(:[],:id).blank? && @location.ternary_gateway.present? && !@location.ternary_gateway.is_block
                if seq_transaction_id[:message][:message].present?
                  message = seq_transaction_id[:message][:message]
                end
                saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
                @payment_gateway = @location.ternary_gateway
                seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,nil,card_bin_list,nil,nil,nil,params[:flag],@location.apply_load_balancer)
                gateway = @payment_gateway.key
              end

            end
          end

          if seq_transaction_id.try(:[], :blocked).present?
            return maintenance_response
          elsif seq_transaction_id.try(:[], :decline_message).present?
            return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:decline_message]}, :ok)
          elsif seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank?
            if params[:order_bank].present? && params[:charge_id].present?
              params[:order_bank].update(status: "seq_crash")
            end
            if seq_transaction_id.blank?
              return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok)
            end
            return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message]}, :ok)
          end
        end

        data = seq_transaction_id if seq_transaction_id.present?
        data = data.merge(get_card_info(params[:card_number])) unless data.empty?
        data.delete(:gateway_fee_details) if data[:gateway_fee_details].present?
        balance = show_balance(reciever_wallet.id)
        raise SaleValidationError.new I18n.t('api.registrations.balance') if balance.to_f < params[:amount].to_f
        sender = @user
        # @payment_gateway = @location.primary_gateway if @payment_gateway.blank?
        gateway = @payment_gateway.key if gateway.blank?
        current_transaction = transaction_between(wallet.id,reciever_wallet.id,params[:amount].to_f, TypesEnumLib::TransactionType::SaleType,0, nil, data,reciever.ledger, gateway,TypesEnumLib::TransactionSubType::SaleVirtualApi,card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction, nil,tip_detail,nil,@payment_gateway,nil, params[:privacy_fee],params[:flag])
        if current_transaction.blank?
          return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok)
        end

        if seq_transaction_id.try(:[],:id).present? && @location.apply_load_balancer?
          @payment_gateway.error_message = []
          @payment_gateway.decline_count = 0
          @payment_gateway.last_status = I18n.t("statuses.approve")
          load_balancer.update_card_transaction_attempts(card,@payment_gateway)
          load_balancer.update_gateway_transaction_count(@payment_gateway)
          load_balancer.do_calculation_for_daily_limit(@payment_gateway,seq_transaction_id[:issue_amount]) if seq_transaction_id[:issue_amount].present?
          load_balancer.do_calculation_for_monthly_limit(@payment_gateway,seq_transaction_id[:issue_amount]) if seq_transaction_id[:issue_amount].present?
          current_amount_daily = @payment_gateway.daily_volume_achived
          current_amount_monthly = @payment_gateway.monthly_volume_achived
          load_balancer.block_processor_daily_100(@payment_gateway,current_amount_daily)
          load_balancer.block_processor_monthly_100(@payment_gateway,current_amount_monthly)

          if load_balancer.load_balancer_rules.select{|lbr| lbr.processor_daily_volume?}.present? || load_balancer.load_balancer_rules.select{|lbr| lbr.block_processor_daily_volume?}.present?
            load_balancer.processor_daily_volume(@payment_gateway,current_amount_daily) if current_amount_daily.present?
            load_balancer.block_processor_daily_volume(@payment_gateway,current_amount_daily) if current_amount_daily.present?
          end
          if load_balancer.load_balancer_rules.select{|lbr| lbr.processor_monthly_volume?}.present?
            load_balancer.processor_monthly_volume(@payment_gateway,current_amount_monthly) if current_amount_monthly.present?
          end
          @payment_gateway.save
          card.save
          load_balancer.group_volume
        elsif seq_transaction_id.try(:[],:id).present?
          @payment_gateway.update_daily_monthly_limits(seq_transaction_id[:issue_amount])
        end

        maintain_batch(@user.id, wallet.id, amount,TypesEnumLib::Batch::Primary)
        if @location.high?
          if @location.iso.present?
            maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High)
          end
          maintain_batch(@user.id, wallet.id, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true)
        elsif @location.dispensary?
          if @location.iso.present?
            maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low)
          end
          maintain_batch(@user.id, wallet.id, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true)
        end

        if gateway.present?
          payment_gateway = @payment_gateway
          # payment_gateway = PaymentGateway.where(key: gateway).first
          if payment_gateway.present?
            message_amount = params[:amount].to_f + params[:tip].to_f + params[:privacy_fee].to_f
            message = sale_text_message(number_with_precision(message_amount,precision:2), @location.business_name, payment_gateway.try(:name),params["card_number"].last(4),reciever.name, @location.cs_number || @location.phone_number)
            # Email/Text Notifications
            amount = number_to_currency(number_with_precision(params[:amount].to_f + params[:tip].to_f + params[:privacy_fee].to_f, precision: 2, delimiter: ','))
            UserMailer.transaction_email(@location.id, amount, payment_gateway.try(:name), reciever.id, Date.today.to_json,card.brand,params[:card_number].last(4), current_transaction[:id] || 'TR_ID', params[:action]).deliver_later if @location.try(:email_receipt)
            # Sending Text Message
            twilio_text = send_text_message(message, user.phone_number) if @location.try(:sms_receipt) && message.present? && user.try(:phone_number).present?
          end
        end
        push_notification(message, user)
        # = show_balance(wallet.id, sender.ledger)
        # new_balance =  number_with_precision(sender_balance.to_f, precision: 2)
        return render_json_response({:success => true, :status => TypesEnumLib::Statuses::Success, transaction_id: current_transaction[:transaction_id] , message: message}, :ok)
      end
    rescue ArgumentError, SaleValidationError  => exc
      handle_standard_error(exc)
        # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue Stripe::CardError, Stripe::InvalidRequestError => e
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => e.message},:ok)
    rescue Sequence::APIError => e
      issue_raw_transaction = IssueRawTransaction.new(user.email,params)
      #Void from Payment Failure in case of seq failure
      make_elavon_payment_void(params[:converge_txn_id],@payment_gateway)
      make_i_can_preauth_void(params[:i_can_pay_txn_id], request, params[:i_can_pay_txn_amount],@payment_gateway)
      make_bolt_pay_void(params[:bolt_pay_txn_id], nil,@payment_gateway)

      saving_decline_for_seq(issue_raw_transaction,@payment_gateway.id)
      return handle_sequence_error(e)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end
end