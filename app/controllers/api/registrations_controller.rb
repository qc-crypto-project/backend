class Api::RegistrationsController < Api::ApiController
  include ApplicationHelper
  include FluidPayHelper
  include TransactionCharge::ChargeATransactionHelper
  include Merchant::SalesHelper
  include RegistrationsHelper
  include Api::RegistrationsHelper
  include Api::PossHelper
  include LoadBalancerHelper
  include LoadBalancerHandler
  include CardHandler

  FAILED = 'failed'
  SUCCESS = 'success'

  before_action :location_ip_policy, only: [:virtual_terminal_transaction, :virtual_terminal_transaction_debit,:virtual_transaction,:virtual_transaction_card]
  before_action :check_bank_status, only: [:virtual_terminal_transaction, :virtual_terminal_transaction_debit, :virtual_transaction,:virtual_transaction_card,:card_tokenization]

  def virtual_terminal_transaction
    #render maintenance response
    return maintenance_response() if  @api_config.fetch("virtual_terminal_transaction") { true }.to_s == 'false'
    return sequence_down() if @seq_down.present? && @seq_down == true

    #initialize variables
    @eaze_reporting = true
    response_message = ""
    response_status = SUCCESS
    seq_transaction_id = nil
    new_balance = nil
    tip_details = nil
    begin

      #check all required parameters auth_token , location_id or wallet_id , amount , card_number ,card_exp_date , card_holder_name  are exist
      validate_parameters(
          :auth_token => params[:auth_token],
          :location_or_wallet_id => params[:location_id].present? ? params[:location_id] :params[:wallet_id],
          :amount => params[:amount],
          :card_number => params[:card_number],
          :card_exp_date => params[:card_exp_date],
          :card_cvv => params[:card_cvv],
          :card_holder_name => params[:card_holder_name]
      )

      params[:ref_id] = params[:RefId] if params[:RefId].present?
      params[:country] = params[:country] if params[:country].present?
      params[:street] = params[:street] if params[:street].present?
      # @virtual_terminal_transaction = true

      #amount greater than 0 will be acceptable ,less than 0 or equal to zero it will raise error
      if params[:amount].to_f <= 0
        raise StandardError.new I18n.t('api.registrations.amount_error')
      end

      #get wallet from location_id if location_id is present otherwise get from wallet_id
      if params[:location_id].present?
        @location = Location.where(location_secure_token: params[:location_id]).first
        wallet = @location.wallets.primary.eager_load(:users).first
      else
        wallet = Wallet.eager_load(:users).find(params[:wallet_id])
        @location = wallet.location
      end
      raise SaleValidationError.new I18n.t('api.registrations.loc_sale_true') if @location.sales == true
      #check user exist in wallet users list
      raise SaleValidationError.new I18n.t('api.registrations.wrong_user') unless wallet.users.pluck(:id).include?(@user.id)
      
      params[:wallet_id] = wallet.id
      params[:card_number]= params[:card_number].try(:split).try(:join)
      #create card if not exist and cvv is required for new card or if card exist then check card is block or not and card decline attempts should be less than CARD_DECLINE_ATTEMPTS or 6
      result_card = getting_or_creating_card
      unless result_card[:status]
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card')}, :ok)
      else
        card = result_card[:card]
        card_bin_list = result_card[:card_bin_list]
      end
      #create all transactions and sequence block transactions for all users
      return virtual_terminal_transaction_method(params,wallet,@location,card_bin_list,card)
    rescue ArgumentError, SaleValidationError  => exc
      @cultivate_params = true
      @card_number = params[:card_number]
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def virtual_transaction
    #check maintenance response
    return maintenance_response() if @api_config.present? && @api_config.fetch("virtual_transaction") { true }.to_s == 'false'
    return sequence_down() if @seq_down.present? && @seq_down == true
    return virtual_transaction_method
  end

  def virtual_transaction_card
    return maintenance_response() if  @api_config.fetch("card_virtual_transaction") { true }.to_s == 'false'
    return sequence_down() if @seq_down.present? && @seq_down == true
    begin
      if params[:card].present? && params[:location_id].present?
        card_info = AESCrypt.decrypt_dd( params[:location_id],params[:card])
        card_info = JSON(card_info) unless card_info.blank?
        raise StandardError.new I18n.t('api.registrations.invalid_card_data') if card_info.blank? || card_info["card_number"].blank? || card_info["exp_date"].blank? || card_info["cvv"].blank?
        params[:card_number] = card_info["card_number"]
        params[:exp_date] = card_info["exp_date"]
        params[:card_cvv] = card_info["cvv"]
      elsif params[:card_token].present?
      else
        raise StandardError.new I18n.t('api.registrations.invalid_card')
      end
      return virtual_transaction_method
    rescue StandardError => exc
      # Raven.capture_exception(exc)
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message},:ok)
    end
  end

  def virtual_transaction_method
    begin
      fee_lib = FeeLib.new
      stripe = Payment::StripeGateway.new
      # validate all required params
      # getting location and wallet
      if params[:location_id].present?
        @location = Location.where(location_secure_token: params[:location_id]).includes(load_balancer: :load_balancer_rules).first

        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_loc')}, :ok) unless @location.present?
        wallet = @location.wallets.primary.eager_load(:users).first
      else
        wallet = Wallet.find_by(id: params[:wallet_id])
        @location = wallet.location
      end
      raise SaleValidationError.new I18n.t('api.registrations.loc_sale_true') if @location.sales == true

      gateway = @location.primary_gateway.present? ? @location.primary_gateway.virtual_tx : @user.primary_gateway.present? ? @user.primary_gateway.virtual_tx : ''

      if gateway.present? && gateway == true # if virtual_tx checkbox in gateway is true then these params are mandatory country,city,zip_code,state,street
        if params[:card_token].present?
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :card_token => params[:card_token],
              :card_cvv => params[:card_cvv],
              :country => params[:country],
              :city => params[:city],
              :zip_code => params[:zip_code],
              :state => params[:state],
              :street => params[:street]
              )

          raise StandardError.new I18n.t('api.registrations.invalid_card_token') unless setting_params
        else
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :name => params[:name],
              :email => params[:email],
              :phone_number => params[:phone_number],
              :last_name => params[:last_name],
              :first_name => params[:first_name],
              :card_number => params[:card_number],
              :card_cvv => params[:card_cvv],
              :exp_date => params[:exp_date],
              :country => params[:country],
              :city => params[:city],
              :zip_code => params[:zip_code],
              :state => params[:state],
              :street => params[:street]
              )
        end
      else
        if params[:card_token].present?
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :card_token => params[:card_token],
              :card_cvv => params[:card_cvv],
          )

          raise StandardError.new I18n.t('api.registrations.invalid_card_token') unless setting_params
        else
          validate_parameters(
              :auth_token => params[:auth_token],
              :location_or_wallet_id => params[:location_id].present? ? params[:location_id] : params[:wallet_id],
              :amount => params[:amount],
              :name => params[:name],
              :email => params[:email],
              :phone_number => params[:phone_number],
              :last_name => params[:last_name],
              :first_name => params[:first_name],
              :card_number => params[:card_number],
              :exp_date => params[:exp_date],
              :card_cvv => params[:card_cvv],
              )
          
          raise StandardError.new I18n.t('api.registrations.invalid_email') unless valid_email?
        end
      end
      
      total_amount =  params[:amount].to_f + params[:privacy_fee].to_f + params[:tip].to_f
      if @location.transaction_limit?
        amounts = []
        unless @location.transaction_limit_offset.include? total_amount
          params[:grand_total] = total_amount
          amounts = @location.transaction_limit_offset.map{|a| "#{number_with_precision(number_to_currency(a))}"} if @location.transaction_limit_offset.present?
          params[:location_id] = @location.id
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.account_limit_true', amount_limit: amounts.join(","))}, :ok)
        end
      end
      
      raise SaleValidationError.new I18n.t('api.registrations.phone_number') if !check_string(params[:phone_number])
      params[:country] = params[:country] if params[:country].present?
      params[:street] = params[:street] if params[:street].present?
      raise StandardError.new I18n.t('api.registrations.amount_error') if params[:amount].to_f <= 0
      valid_email = EmailValidator.valid?(params[:email])
      raise StandardError.new I18n.t('api.registrations.invalid_email') if !valid_email
      #raise SaleValidationError.new "You can't make sale with your own account" if @user.phone_number == params[:phone_number]
      # getting user
      phone_number_checking = User.user.unarchived_users.where(phone_number: params[:phone_number])
      if phone_number_checking.present?  #===========>>>>> Existing User
        getting_user = phone_number_checking.select{|a| a.role == "user"}.first
        if getting_user.blank?
          user = phone_number_checking.first
        else
          user = getting_user
        end
        # user.first_name = params[:first_name]
        # user.last_name = params[:last_name]
        # user.name = params[:name]
        # user.save
        # creating new user with merchant or iso or agent or aff or company parameters
        if user.merchant? || user.iso? || user.agent? || user.affiliate? || user.partner?
          raise SaleValidationError.new I18n.t('api.registrations.unauth') if user.is_block? || user.archived
          user = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address,:city,:state, :role))
          user.source = @user.name if @user.present?
          user.role = :user
          user.ledger = @user.ledger if @user.present? && @user.ledger.present?
          password = random_password
          user.password = password
          user.regenerate_token
          user.save(:validate => false)
        end
        # raise SaleValidationError.new "Merchant can't make a sale. Please contact you administrator for details." if !user.user?
      else                                    #===================>>>>> New User
        # user = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address,:city,:state))
        user = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address,:address,:city,:state,:shipping_first_name,:shipping_last_name,:shipping_company,:shipping_address,:shipping_city,:shipping_state,:shipping_country,:shipping_zip_code))
        user.source = @user.name if @user.present?
        user.ledger = @user.ledger if @user.present? && @user.ledger.present?
        password = random_password
        user.password = password
        user.regenerate_token
        user.role = "user"
        user.save(:validate => false)
      end

      # ------------------------------Updating Shipping & Billing Info-------------------------------
      user.email = params[:email]
      billing_address = save_billing_info(user, "billing")
      user.addresses << billing_address unless user.addresses.try(:inclide?, billing_address)
      user.shipping_first_name=params[:shipping_first_name] if params[:shipping_first_name].present?
      user.shipping_last_name=params[:shipping_last_name] if params[:shipping_last_name].present?
      user.shipping_company=params[:shipping_company] if params[:shipping_company].present?
      user.shipping_address=params[:shipping_address] if params[:shipping_address].present?
      user.shipping_city=params[:shipping_city] if params[:shipping_city].present?
      user.shipping_state=params[:shipping_state] if params[:shipping_state].present?
      user.shipping_country=params[:shipping_country] if params[:shipping_country].present?
      user.shipping_zip_code=params[:shipping_zip_code] if params[:shipping_zip_code].present?
      user.save(:validate => false)
      if user.wallets.primary.first.blank?
        user.create_wallet(user.name, user.id, 0, user)
        if user.wallets.primary.first.present?
          return_no_wallets(user, true)
        else
          return_no_wallets(user, false)
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.unexpected')}, :ok)
        end
      end
      raise SaleValidationError.new I18n.t('api.registrations.sale') if !user.user?
      raise SaleValidationError.new I18n.t('api.registrations.invalid_user') if user.blank?
      # raise SaleValidationError.new I18n.t('api.registrations.wrong_details') if !user.user?
      #creating card for records
      params[:card_holder_name] = params[:card_holder_name] || params[:name] || "#{params[:first_name]} #{params[:last_name]}"
      params[:card_number]= params[:card_number].try(:split).try(:join)
      params[:card_exp_date]= params[:exp_date]
      return render_json_response({success: false, status: TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_expiry')}, :ok) if params[:exp_date].length != 4
      bin_result=check_bin_list(user,@user.id,user.id)
      card=bin_result[:card]
      card_bin_list=bin_result[:card_bin_list]
      card_brand_type = "#{card_bin_list["scheme"]}_#{card_bin_list["type"] || 'credit'}".try(:downcase) if card_bin_list.present? && card_bin_list["scheme"].present?

      if @location.apply_load_balancer?
        instantiate_load_balancer(nil,card_brand_type,card_bin_list)
      else
        if @location.primary_gateway.present? && @location.primary_gateway.descriptor.present? && !@location.primary_gateway.descriptor.active?
          raise StandardError.new I18n.t("api.registrations.inactive")
        end
      end

      card_block = check_card_blocked(params[:card_number],"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}", @card_info)
      if card_block
        msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: #{I18n.t("api.errors.blocked_card")}"
        SlackService.notify(msg, "#qc-risk-alert")
        raise ArgumentError.new I18n.t('api.errors.blocked_card')
      end
      if @location.vip_card && !Card.check_vip_card?(params[:card_number],"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}")
        msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_holder_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: #{I18n.t("api.errors.non_vip_card")}"
        SlackService.notify(msg, "#qc-risk-alert")
        raise StandardError.new(I18n.t('api.errors.non_vip_card'))
      end
      result = recurring_flaged_trans(card,params)
      raise SaleValidationError.new "#{result[:message]}" unless result[:success]

      if !@location.apply_load_balancer? && check_card_decline(card)
        card.update(decline_attempts: card.decline_attempts.to_i + 1)
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card')}, :ok)
      end

      raise SaleValidationError.new I18n.t('api.registrations.no_user_exist') if wallet.try(:users).blank?
      raise SaleValidationError.new I18n.t('api.registrations.wrong_user') unless wallet.try(:users).pluck(:id).include?(@user.id)
      params[:wallet_id] = user.try(:wallets).try(:first).try(:id)
      params[:merchant_wallet_id] = wallet.id

      if wallet.present?
        raise SaleValidationError.new I18n.t("errors.withdrawl.blocked_location") if @location.is_block
        raise SaleValidationError.new I18n.t('api.registrations.unavailable') if @user.oauth_apps.first.is_block

        # checking sale limits for specific locations
        if @location.sale_limit.present?
          if @location.sale_limit_percentage.present?
            sale_limit = @location.sale_limit
            sale_limit_percentage = @location.sale_limit_percentage
            total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
            raise SaleValidationError.new I18n.t('api.registrations.trans_limit') if params[:amount].to_f > total_limit
          else
            raise SaleValidationError.new I18n.t('api.registrations.trans_limit') if params[:amount].to_f > @location.sale_limit
          end
        end

        # checking tip
        tip_amount = 0
        amount = 0
        if params[:tip].present? && params[:tip].to_f > 0
          tip_detail = nil
          tip_amount = params[:tip].to_f
          tip_wallet = @location.wallets.where(wallet_type: 'tip').first
          if tip_amount > 0
            tip_detail = {wallet_id: tip_wallet.try(:id),sale_tip: tip_amount}
          end
        end
        if tip_amount > 0
          amount = params[:amount].to_f + tip_amount.to_f
        else
          amount = params[:amount]
        end
      end

      # going for transaction
      data = {}
      params[:both] = false
      if user.present?
        reciever = user
        #getting users wallet
        reciever_wallet = reciever.wallets.first

        # getting wallet balance
        balance = show_balance(reciever_wallet.id, reciever.ledger)
        merchant_balance = show_balance(wallet.id, @user.ledger)
        buyRate = @location.fees.buy_rate.first
        amount_fee_check = check_fee_amount(@location, @user, user, wallet, buyRate, params[:amount], merchant_balance, nil, 'credit')

        return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.amount')}, :ok)if amount_fee_check.present? && amount_fee_check[:status] == false
        if amount_fee_check.present? && amount_fee_check[:status] == true
          if amount_fee_check[:splits].present?
            agent_fee = amount_fee_check[:splits]["agent"]["amount"]
            iso_fee = amount_fee_check[:splits]["iso"]["amount"]
            iso_balance = show_balance(@location.iso.wallets.primary.first.id)
            if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
              if iso_balance < iso_fee
                return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: 'ISO-007 Cannot process transaction at the moment. Please contact administrator'}, :ok)
              end
            else
              if iso_balance + iso_fee < agent_fee.to_f
                return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: 'ISO-008 Cannot process transaction at the moment. Please contact administrator'}, :ok)
              end
            end
          end
        end
        gateway = @location.primary_gateway.present? ? @location.primary_gateway.key : @user.primary_gateway.present? ? @user.primary_gateway.key : ''
        if gateway == TypesEnumLib::GatewayType::ICanPay
          gateway_check = check_icanpay(gateway, params)
          if gateway_check.present? && gateway_check == false
            return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.i_can_pay')}, :ok)
          end
        end
        # if wallet balance < amount to pay
        # if balance.to_f < params[:amount].to_f + tip_amount.to_f
        my_amount = params[:amount].to_f + tip_amount.to_f
        if balance.to_f != my_amount
          params[:both] = true
          new_amount = params[:amount].to_f
          fee_for_issue = 0
          # checking card fee
          if @user.present? && @user.MERCHANT? && !@user.issue_fee
            issue_amount = new_amount
          else
            issue_amount = new_amount + fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f + tip_amount
            fee_for_issue = fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f
          end

          params[:issue_amount] = issue_amount.to_f
          qc_wallet = Wallet.qc_support.first
          issue_raw_transaction = IssueRawTransaction.new(user.email,params,params[:card_cvv])

          seq_transaction_id = {:message => {:message => I18n.t('api.registrations.no_gateway')}}

          if @location.apply_load_balancer?
            load_amount = issue_amount + params[:privacy_fee].to_f + params[:tip].to_f
            get_gateway(load_amount, card)
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@payment_gateway,card_bin_list,nil,nil,nil,params[:flag],@location.apply_load_balancer)
            gateway = @payment_gateway.key if @payment_gateway.present?
            if seq_transaction_id.try(:[],:id).blank?
              update_load_balancer_in_failed_case(seq_transaction_id, card)
            end
          else
            if @location.primary_gateway.present?
              @payment_gateway = @location.primary_gateway if !@location.primary_gateway.is_block
              #= LOAD BALANCER first_payment_gateway
              seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.secondary_gateway,card_bin_list,nil,nil,nil,params[:flag])
              gateway = @payment_gateway.key if @payment_gateway.present?
              if @location.primary_gateway.is_block && seq_transaction_id.nil?
                seq_transaction_id = {:message => {:message => "Processing: Inactive – Please contact customer support."}}
              end
              #= LOAD BALANCER secondary_payment_gateway
              if seq_transaction_id.try(:[],:response).blank? && seq_transaction_id.try(:[],:id).blank? && @location.secondary_gateway.present? && !@location.secondary_gateway.is_block
                message = ""
                if seq_transaction_id[:message][:message].present?
                  message = seq_transaction_id[:message][:message]
                end
                saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
                @payment_gateway = @location.secondary_gateway
                seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.ternary_gateway,card_bin_list,nil,nil,nil,params[:flag])
                gateway = @payment_gateway.key
              end
              #= LOAD BALANCER third_payment_gateway
              if seq_transaction_id.try(:[],:response).blank? && seq_transaction_id.try(:[],:id).blank? && @location.ternary_gateway.present? && !@location.ternary_gateway.is_block
                if seq_transaction_id[:message][:message].present?
                  message = seq_transaction_id[:message][:message]
                end
                saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
                @payment_gateway = @location.ternary_gateway
                seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, tip_detail, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,nil,card_bin_list,nil,nil,nil,params[:flag])
                gateway = @payment_gateway.key
              end
            end
          end

          if seq_transaction_id.try(:[], :blocked).present?
            return maintenance_response
          elsif seq_transaction_id.try(:[], :decline_message).present?
            return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:decline_message]}, :ok)
          elsif seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank?
            if params[:order_bank].present? && params[:charge_id].present?
              params[:order_bank].update(status: "seq_crash")
            end
            if seq_transaction_id.blank?
              return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok)
            end
            return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message]}, :ok)
          end
        else
          @payment_gateway = user.receiver_transactions.where(amount: my_amount).last.try(:payment_gateway)
        end

        data = seq_transaction_id if seq_transaction_id.present?
        data = data.merge(get_card_info(params[:card_number])) unless data.empty?
        data.delete(:gateway_fee_details) if data[:gateway_fee_details].present?
        balance = show_balance(reciever_wallet.id)
        raise SaleValidationError.new I18n.t('api.registrations.balance') if balance.to_f < params[:amount].to_f
        sender = @user
        gateway = @payment_gateway.key if gateway.blank?
        if @payment_gateway.present? &&  (@payment_gateway.knox_payments? || @payment_gateway.payment_technologies? || @payment_gateway.total_pay?)
          gateway=params[:bank_descriptor].present? ? params[:bank_descriptor] : gateway
        end
        current_transaction = transaction_between(wallet.id,reciever_wallet.id,params[:amount].to_f, TypesEnumLib::TransactionType::SaleType,0, nil, data,reciever.ledger, gateway,TypesEnumLib::TransactionSubType::SaleVirtualApi,card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction, nil,tip_detail,nil,@payment_gateway,nil, params[:privacy_fee],params[:flag])
        if current_transaction.blank?
          return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok)
        end

        if seq_transaction_id.try(:[],:id).present? && @location.apply_load_balancer?
          update_load_balancer_in_success_case(seq_transaction_id, card)
        elsif seq_transaction_id.try(:[],:id).present?
          @payment_gateway.update_daily_monthly_limits(seq_transaction_id[:issue_amount])
        end

        maintain_batch(@user.id, wallet.id, amount,TypesEnumLib::Batch::Primary)
        if @location.high?
          if @location.iso.present?
            maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High)
          end
          maintain_batch(@user.id, wallet.id, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true)
        elsif @location.dispensary?
          if @location.iso.present?
            maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low)
          end
          maintain_batch(@user.id, wallet.id, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true)
        end
        if gateway.present?
          payment_gateway = @payment_gateway
          gateway_name = @payment_gateway.try(:name)
          if @payment_gateway.present? &&  (@payment_gateway.knox_payments? || @payment_gateway.payment_technologies? || @payment_gateway.total_pay?)
            gateway_name = params[:bank_descriptor].present? ? params[:bank_descriptor] : gateway_name
          end
          if payment_gateway.present?
            message_amount = params[:amount].to_f + params[:tip].to_f + params[:privacy_fee].to_f
            message = sale_text_message(number_with_precision(message_amount,precision:2), @location.business_name, gateway_name,params["card_number"].last(4),reciever.name, @location.cs_number || @location.phone_number)
            # Email/Text Notifications
            amount = number_to_currency(number_with_precision(params[:amount].to_f + params[:tip].to_f + params[:privacy_fee].to_f, precision: 2, delimiter: ','))
            UserMailer.transaction_email(@location.id, amount, gateway_name, reciever.id, Date.today.to_json,card.brand,params[:card_number].last(4), current_transaction[:transaction_id] || 'TR_ID', params[:action]).deliver_later if @location.try(:email_receipt)
            # Sending Text Message

            twilio_text = send_text_message(message, user.phone_number) if @location.try(:sms_receipt) && message.present? && user.try(:phone_number).present?
          end
        end

        push_notification(message, user)

        # = show_balance(wallet.id, sender.ledger)
        # new_balance =  number_with_precision(sender_balance.to_f, precision: 2)
        # return render_json_response({:success => true, :status => TypesEnumLib::Statuses::Success, transaction_id: current_transaction[:transaction_id] , message: message}, :ok)
        return render_json_response({:success => true, :status => TypesEnumLib::Statuses::Success, transaction_id: current_transaction[:transaction_id], quickcard_id: params[:db_quickcard_id] , message: message}, :ok)
      end
    rescue ArgumentError, SaleValidationError  => exc
      handle_standard_error(exc)
      # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue Stripe::CardError, Stripe::InvalidRequestError => e
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => e.message},:ok)
    rescue Sequence::APIError => e
      issue_raw_transaction = IssueRawTransaction.new(user.email,params)
      #Void from Payment Failure in case of seq failure
      make_elavon_payment_void(params[:converge_txn_id],@payment_gateway)
      make_i_can_preauth_void(params[:i_can_pay_txn_id], request, params[:i_can_pay_txn_amount],@payment_gateway)
      make_bolt_pay_void(params[:bolt_pay_txn_id], nil,@payment_gateway)

      saving_decline_for_seq(issue_raw_transaction,@payment_gateway.id)
      return handle_sequence_error(e)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def virtual_terminal_transaction_debit
    #render maintenance response
    return maintenance_response() if  @api_config.fetch("virtual_terminal_transaction_debit") { true }.to_s == 'false'
    return sequence_down() if @seq_down.present? && @seq_down == true
    begin
      #check all required parameters auth_token , location_id or wallet_id , amount , card_number ,card_exp_date , card_holder_name and ref_id are exist
      validate_parameters(
          :auth_token => params[:auth_token],
          :location_or_wallet_id => params[:location_id].present? ? params[:location_id] :params[:wallet_id],
          :amount => params[:amount],
          :card_number => params[:card_number],
          :card_exp_date => params[:card_exp_date],
          :card_cvv => params[:card_cvv],
          :card_holder_name => params[:card_holder_name],
          :RefId => params[:RefId]
      )
      params[:country] = params[:country] if params[:country].present?
      params[:street] = params[:street] if params[:street].present?
      params[:ref_id] = params[:RefId]
      params[:card_number]= params[:card_number].try(:split).try(:join)
      # @virtual_terminal_transaction = true
      #check amount value greater than 0
      if params[:amount].to_f <= 0
        raise StandardError.new I18n.t('api.registrations.amount_error')
      end
      tx = Transaction.where(ref_id: params[:ref_id]).last
      if tx.present?
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.already_exist_ref_id')}, :ok)
      end
      #get wallet from location_id if location_id is present otherwise get from wallet_id
      if params[:location_id].present?
        location = Location.where(location_secure_token: params[:location_id]).first
        wallet = location.wallets.primary.eager_load(:users).first
      else
        # need to discuss with yawar wallet = Wallet.find_by(id: params[:wallet_id])
        wallet = Wallet.eager_load(:users).find(params[:wallet_id])
        location = wallet.location
      end
      @location = location
      raise SaleValidationError.new I18n.t('api.registrations.loc_sale_true') if @location.sales == true
      #check user exist in wallet users list
      raise SaleValidationError.new I18n.t('api.registrations.wrong_user') unless wallet.users.pluck(:id).include?(@user.id)
      
      params[:wallet_id] = wallet.id
      #create card if not exist and cvv is required for new card or if card exist then check card is block or not and card decline attempts should be less than CARD_DECLINE_ATTEMPTS or 6
      result_card = getting_or_creating_card
      unless @location.apply_load_balancer?
        unless result_card[:status]
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card')}, :ok)
        end
      end
      card = result_card[:card]
      card_bin_list = result_card[:card_bin_list]
      #= Slots logic
      # if card_bin_list["type"].present?
      #   if card_bin_list["type"].try(:downcase) == "debit"
      #     if params[:privacy_fee].to_f == 0
      #       return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined, message: I18n.t('api.registrations.privacy_fee')}, :ok)
      #     end
      #     if location.is_slot && location.slot.present?
      #       slot = location.slot
      #       if params[:amount].to_f > slot.amount.try(:to_f)   # amount is greater => over logic
      #         result = getting_slot(slot,params[:amount].to_f, params[:privacy_fee].to_f, params[:tip].to_f,slot.slot_over,"slot_over")
      #       else
      #         result = getting_slot(slot,params[:amount].to_f, params[:privacy_fee].to_f, params[:tip].to_f,slot.slot_under,"slot_under")
      #       end
      #     else
      #       slot = AppConfig.find_by(key: AppConfig::Key::SlotOver)
      #       slot_amount = slot.try(:stringValue)
      #       if params[:amount].to_f > slot_amount.try(:to_f)   # amount is greater => over logic
      #         result = getting_slot(slot,params[:amount].to_f, params[:privacy_fee].to_f, params[:tip].to_f)
      #       else
      #         slot = AppConfig.find_by(key: AppConfig::Key::SlotUnder)
      #         result = getting_slot(slot,params[:amount].to_f, params[:privacy_fee].to_f, params[:tip].to_f)
      #       end
      #     end
      #     #create debit transaction , return false if ref_id is same or blank
      #     amount_merchant1 = result.try(:[],:a_merchant1).to_f
      #     amount_merchant2 = result.try(:[],:a_merchant2).to_f
      #     if amount_merchant1.to_f > (params[:amount].to_f + params[:privacy_fee].to_f + params[:tip].to_f)
      #       privacy_fee = params[:privacy_fee].to_f + (amount_merchant1 - (params[:amount].to_f + params[:privacy_fee].to_f + params[:tip].to_f))
      #     elsif amount_merchant2.to_f > (params[:amount].to_f + params[:privacy_fee].to_f + params[:tip].to_f)
      #       privacy_fee = params[:privacy_fee].to_f + (amount_merchant2 - (params[:amount].to_f + params[:privacy_fee].to_f + params[:tip].to_f))
      #     else
      #       privacy_fee = params[:privacy_fee]
      #     end
      #     # ref_id = create_transaction_debit(@user, wallet.id, @user.id, card, params[:amount], request, params[:ref_id],result,privacy_fee, params[:tip], params[:clerk_id], params[:terminal_id],TypesEnumLib::TransactionType::SaleType, TypesEnumLib::GatewayType::PinGateway)
      #     ref_id = create_transaction_debit(@user, wallet.id, @user.id, card, params[:amount], request, params[:ref_id],nil,privacy_fee, params[:tip], params[:clerk_id], params[:terminal_id])
      #     return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Already exist or Invalid RefId!"}, :ok) if ref_id.blank?
      #     return render_json_response({:success => true,:status => TypesEnumLib::Statuses::Pending, merchant1: result.try(:[],:merchant1), amount_merchant1: "$#{number_with_precision(amount_merchant1, precision: 2)}", merchant2: result.try(:[],:merchant2), amount_merchant2: "$#{number_with_precision(amount_merchant2, precision: 2)}", RefId: params[:RefId], clerk_id: params[:clerk_id]}, :ok)
      #   end
      # end
      #create all transaction and block transactions for all users merchant iso agent and affiliate
      return virtual_terminal_transaction_method(params,wallet,location,card_bin_list,card)
    rescue ArgumentError, SaleValidationError  => exc
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def virtual_terminal_transaction_method(params,wallet,location,card_bin_list,card)
    #initialize variables
    response_message = ""
    response_status = SUCCESS
    seq_transaction_id = nil
    new_balance = nil
    tip_details = nil
    begin

      total_amount =  params[:amount].to_f + params[:privacy_fee].to_f + params[:tip].to_f
      if @location.transaction_limit?
        amounts = []
        unless @location.transaction_limit_offset.include? total_amount
          params[:grand_total] = total_amount
          params[:location_id] = @location.id
          amounts = @location.transaction_limit_offset.map{|a| "#{number_with_precision(number_to_currency(a))}"} if @location.transaction_limit_offset.present?
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.account_limit_true', amount_limit: amounts.join(","))}, :ok)
        end
      end

      card_brand_type = "#{card_bin_list["scheme"]}_#{card_bin_list["type"] || 'credit'}".try(:downcase) if card_bin_list.present? && card_bin_list["scheme"].present?
      if @location.apply_load_balancer?
        instantiate_load_balancer(nil,card_brand_type,card_bin_list)
      else
        if @location.primary_gateway.present? && @location.primary_gateway.descriptor.present? && !@location.primary_gateway.descriptor.active?
          raise StandardError.new I18n.t("api.registrations.inactive")
        end
      end

      #set first_name or last_name parameters if card_holder_name present otherwise set from user name
      if params[:card_holder_name].present?
        tmp_name = params[:card_holder_name].split(' ')
        params[:first_name] = tmp_name.first
        if tmp_name.count > 1
          params[:last_name] = [1..tmp_name.length].map{ |m| tmp_name[m]}.flatten.join(' ')
        else
          params[:last_name] = tmp_name.first
        end
      else
        params[:first_name] = "#{@user.name}"
        params[:last_name] = "#{@user.name}"
      end
      #set email if card_holder_name present otherwise set from user name
      if params[:card_holder_name].present?
        email = params[:card_holder_name].strip.split(' ').join.downcase+"@crm.me"
      else
        email = @user.name.strip.split(' ').join.downcase+"@crm.me"
      end
      issue_raw_transaction = IssueRawTransaction.new(email,params,params[:card_cvv])

      #create FeeLib object to get card fee on the base of payment instuments
      fee_lib = FeeLib.new

      #add issue fee in amount if user issue fee exist
      if @user.present? && @user.MERCHANT? && !@user.issue_fee
        issue_amount = params[:amount].to_f
      else
        issue_amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f
      end
      #set issue amount parameters
      params[:issue_amount] = issue_amount
      #get first wallet which type is escrow
      qc_wallet = Wallet.escrow.first
      if wallet.present?
        #raise error if location or user is block
        raise SaleValidationError.new I18n.t("errors.withdrawl.blocked_location") if location.is_block
        raise SaleValidationError.new I18n.t('api.registrations.unavailable') if @user.oauth_apps.first.is_block
        # if user give tip
        if params[:tip].present? && params[:tip].to_f > 0
          tip_wallet = location.wallets.tip.first
          # here we are sending tip wallet id and params tip i.e. 1
          tip_details = {wallet_id:tip_wallet.id, sale_tip: params[:tip].to_f}
        end
        #check location sale limitations
        if location.sale_limit.present?
          if location.sale_limit_percentage.present?
            sale_limit = location.sale_limit
            sale_limit_percentage = location.sale_limit_percentage
            total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
            if params[:amount].to_f > total_limit
              raise StandardError.new I18n.t('api.registrations.trans_limit')
            end
          else
            if params[:amount].to_f > location.sale_limit
              raise StandardError.new I18n.t('api.registrations.trans_limit')
            end
          end
        end
        balance = show_balance(wallet.id, @user.ledger)
        buyRate = location.fees.buy_rate.first

        #get total fee on location buy rate and each users splits
        amount_fee_check = check_fee_amount(location, @user, nil, wallet, buyRate, params[:amount], balance, TypesEnumLib::CommissionType::C2B, card_bin_list["type"].try(:downcase))
        #tolat fee is greater than location total balance than status will be false
        if amount_fee_check.present? && amount_fee_check[:status] == false
          @cultivate_params  = true
          @card_number = params[:card_number]
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.amount')}, :ok)
        end
        #set iso and agent balance from amount_fee_check split object
        if amount_fee_check.present? && amount_fee_check[:status] == true
          if amount_fee_check[:splits].present?
            agent_fee = amount_fee_check[:splits]["agent"]["amount"]
            iso_fee = amount_fee_check[:splits]["iso"]["amount"]
            iso_balance = show_balance(location.iso.wallets.primary.first.id)
            if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
              #iso balance should be greater than iso fee
              if iso_balance < iso_fee
                @cultivate_params = true
                @card_number = params[:card_number]
                return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: 'ISO-0007 Cannot process transaction at the moment. Please contact administrator!' }, :ok)
              end
            else
              if iso_balance + iso_fee < agent_fee.to_f
                @cultivate_params = true
                @card_number = params[:card_number]
                return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: 'ISO-0008 Cannot process transaction at the moment. Please contact administrator!'}, :ok)
              end
            end
          end
        end
      end
      sales_info = {}
      if params[:terminal_id].present?
        sales_info = {
            terminal_id: params[:terminal_id]
        }
      end
      if params[:card_type].present?
        sales_info = sales_info.merge({card_type: params[:card_type]})
      end
      if params[:tpn].present?
        sales_info = sales_info.merge({tpn: params[:tpn]})
      end
      if params[:transaction_number].present?
        sales_info = sales_info.merge({transaction_number: params[:transaction_number]})
      end
      if params[:ref_id].present?
        sales_info = sales_info.merge({ref_id: params[:ref_id]})
      end
      seq_transaction_id = {:message => {:message => I18n.t('api.registrations.no_gateway')}}
      if card_bin_list["type"].try(:downcase) == "debit"
        main_type = TypesEnumLib::TransactionSubType::DebitCard
        decline_type = TypesEnumLib::DeclineTransacitonTpe::DebitCard
      elsif card_bin_list["type"].try(:downcase) == "credit"
        main_type = TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        decline_type = TypesEnumLib::DeclineTransacitonTpe::Credit
      elsif card_bin_list.try(:[], "type").nil?
        main_type = TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
        decline_type = TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
      end
      #= for new payment gateways integration
      if @location.apply_load_balancer?
        load_amount = issue_amount + params[:privacy_fee].to_f + params[:tip].to_f
        get_gateway(load_amount, card)
        seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, card_bin_list["type"].try(:downcase) || "credit", tip_details, wallet.merchant, params[:card_number],email,decline_type,nil,main_type,@payment_gateway,card_bin_list,sales_info,nil,nil,params[:flag],@location.apply_load_balancer)
        gateway = @payment_gateway.key if @payment_gateway.present?
        if seq_transaction_id.try(:[],:id).blank?
          update_load_balancer_in_failed_case(seq_transaction_id, card)
        else
          update_load_balancer_in_success_case(seq_transaction_id, card)
        end
      else
        if location.primary_gateway.present?
          #if location primary gateway type is i_can_pay than country city street state zip_code should be present
          if location.primary_gateway.try(:key) == TypesEnumLib::GatewayType::ICanPay
            gateway_check = check_icanpay(location.primary_gateway.try(:key), params)
            if gateway_check.present? && gateway_check == false
              @cultivate_params = true
              @card_number = params[:card_number]
              return render_json_response({:success => false , :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.i_can_pay')}, :ok)
            end
          end

          @payment_gateway = location.primary_gateway
          seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, card_bin_list["type"].try(:downcase) || "credit", tip_details, wallet.merchant, params[:card_number],email,decline_type,nil,main_type,location.secondary_gateway,card_bin_list,sales_info,nil,nil,params[:flag])
          gateway = location.primary_gateway.key
          #= LOAD BALANCER secondary_payment_gateway
          if seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[],:id).blank? && location.secondary_gateway.present?
            message = ""
            if seq_transaction_id.try(:[], "message").try(:[], "message").present?
              message = seq_transaction_id[:message][:message]
            end
            saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
            @payment_gateway = location.secondary_gateway
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, card_bin_list["type"].try(:downcase) || "credit", tip_details, wallet.merchant, params[:card_number],email,decline_type,nil,main_type,location.ternary_gateway,card_bin_list,sales_info,nil,nil,params[:flag])
            gateway = location.secondary_gateway.key
          end
          # LOAD BALANCER third_payment_gateway
          if seq_transaction_id.try(:[],:response).blank? && seq_transaction_id.try(:[],:id).blank?  && location.ternary_gateway.present?
            message = ""
            if seq_transaction_id[:message][:message].present?
              message = seq_transaction_id[:message][:message]
            end
            saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message]})
            @payment_gateway = location.ternary_gateway
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, card_bin_list["type"].try(:downcase) || "credit", tip_details, wallet.merchant, params[:card_number],email,decline_type,nil,main_type,nil,card_bin_list,sales_info,nil,nil,params[:flag])
            gateway = location.ternary_gateway.key
          end
          if seq_transaction_id.try(:[],:id).present?
            if @payment_gateway.present?
              @payment_gateway.update_daily_monthly_limits(seq_transaction_id[:issue_amount])
            end
          end
        end
      end

      if issue_raw_transaction.present? && issue_raw_transaction.params.present? && issue_raw_transaction.params.try(:[], "slug").present?
        order_bank_id = issue_raw_transaction.params.try(:[], "slug")
      end
      if seq_transaction_id.try(:[], :blocked).present?
        return maintenance_response
      elsif seq_transaction_id.try(:[], :decline_message).present?
        @cultivate_params = true
        @card_number = params[:card_number]
        return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:decline_message], transaction_id: order_bank_id}, :ok)
      elsif seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank?
        if params[:order_bank].present? && params[:charge_id].present?
          params[:order_bank].update(status: "seq_crash")
        end
        if seq_transaction_id.blank?
          @cultivate_params = true
          @card_number = params[:card_number]
          return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence'), transaction_id: order_bank_id},:ok)
        end
        @cultivate_params = true
        @card_number = params[:card_number]
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message], transaction_id: order_bank_id}, :ok)
        # raise SaleValidationError.new seq_transaction_id[:message][:message]
      end
      current_merchant_balance = show_balance(wallet.id, nil)
      new_balance = number_with_precision(current_merchant_balance.to_f, precision: 2)
    rescue Stripe::CardError, Stripe::InvalidRequestError => e
      @cultivate_params = true
      @card_number = params[:card_number]
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => e.message},:ok)
    rescue Sequence::APIError => e
      issue_raw_transaction = IssueRawTransaction.new(email,params)

      #Void from Payment Failure in case of seq failure
      make_elavon_payment_void(params[:converge_txn_id],@payment_gateway)
      make_i_can_preauth_void(params[:i_can_pay_txn_id], request, params[:i_can_pay_txn_amount],@payment_gateway)
      make_bolt_pay_void(params[:bolt_pay_txn_id], nil,@payment_gateway)

      location_fee = issue_raw_transaction.get_location_fees(params[:wallet_id],@user,params[:amount],nil,card.card_type.try(:downcase) || TypesEnumLib::TransactionType::CreditTransaction)
      #= building hash for Seq Decline
      transaction_info = {amount: params[:amount].to_f}
      reserve_detail = location_fee.first if location_fee.present?
      user_info = location_fee.second if location_fee.present?
      fee = location_fee.third if location_fee.present?
      transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
      #= end building process
      OrderBank.create(merchant_id: params[:wallet_id],user_id: nil,bank_type: card.card_type.try(:downcase) || TypesEnumLib::DeclineTransacitonTpe::Credit,card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id])
      return handle_sequence_error(e)
    rescue ArgumentError, SaleValidationError  => exc
      @cultivate_params = true
      @card_number = params[:card_number]
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue StandardError => exc
      return handle_standard_error(exc)
    else
      response_message = "Successfully Done!"
      response_status =  TypesEnumLib::Statuses::Success

      if seq_transaction_id[:id].present? && new_balance.present?
        amount = params[:amount].to_f
        if params[:hold_money_location_fee].present?
          location_fee = params[:hold_money_location_fee]
          reserve_detail = location_fee.first if location_fee.present?
          fee = location_fee.third if location_fee.present?
          reserved_amount = reserve_detail[:amount] if reserve_detail.present?
          amount = amount - fee if fee.present?
          amount = amount - reserved_amount if reserved_amount.present?
        end
        if params[:privacy_fee].present?
          amount = amount + params[:privacy_fee].to_f
        end
        HoldInRear.hold_money_in_my_wallet(params[:wallet_id],amount,reserved_amount.to_f)
        bank_id = seq_transaction_id[:transaction_id].present? ? seq_transaction_id[:transaction_id] : ""
        bank_id = seq_transaction_id[:approval_code].present? ? seq_transaction_id[:approval_code] : bank_id
        @cultivate_params = true
        @card_number = params[:card_number]
        p_gateway_name = @payment_gateway.present? ? @payment_gateway.name : ""
        if @payment_gateway.present? &&  (@payment_gateway.knox_payments? || @payment_gateway.payment_technologies? || @payment_gateway.total_pay?)
          p_gateway_name=params[:bank_descriptor].present? ? params[:bank_descriptor] : p_gateway_name
        end
        return render_json_response({success: true,status: response_status, quickcard_id: params[:issue_quickcard_id], transaction_id: seq_transaction_id[:id], seq_transaction_id: seq_transaction_id[:id],auth_code: bank_id, card_type: "#{card.card_type} - #{card.brand}", descriptor_id: p_gateway_name}, :ok)
        # render_json_response({success: true, status: response_status, balance: new_balance, transaction_id: seq_transaction_id[:id], seq_transaction_id: seq_transaction_id[:id],auth_code: bank_id, message: response_message, new_balance: new_balance, descriptor_id: @payment_gateway.present? ? @payment_gateway.name : "" }, :ok)
      else
        response_message = seq_transaction_id[:message] if seq_transaction_id[:message].present?
        response_status = TypesEnumLib::Statuses::Failed
        @cultivate_params = true
        @card_number = params[:card_number]
        return render_json_response({success: false, status: response_status, message: response_message}, :ok)
      end
    end

  end

  def card_tokenization
    return maintenance_response() if  @api_config.fetch("tokenization") { true }.to_s == 'false'
    begin
      validate_parameters(
          :auth_token => params[:auth_token],
          :zip_code => params[:zip_code],
          :address => params[:address],
          :card_number => params[:card_number],
          :exp_date => params[:exp_date],
          # :card_cvv => params[:card_cvv],
          :first_name => params[:first_name],
          :last_name => params[:last_name],
          :phone_number => params[:phone_number],
          :email => params[:email]
      )
      user = User.unarchived_users.where(phone_number: params[:phone_number])
      if user.present?
        user = user.last
      else
        address_info = ZipCodes.identify(params[:zip_code])
        raise StandardError.new "Invalid Zip code" if address_info.blank?
        user = User.new(email: params[:email], name: "#{params[:first_name]} #{params[:last_name]}", last_name: params[:last_name], first_name: params[:first_name], phone_number: params[:phone_number], zip_code: params[:zip_code], postal_address: params[:address],city: address_info[:city],state:address_info[:state_name])
        user.source = @user.name if @user.present?
        user.role = :user
        user.ledger = @user.ledger if @user.present? && @user.ledger.present?
        password = random_password
        user.password = password
        user.regenerate_token
        user.save
      end
      if user.id.present?
        card_info = {
            number: params[:card_number],
            month: params[:exp_date].first(2),
            first_name: params[:card_holder_name],
            year: "20#{params[:exp_date].last(2)}",
            # cvv: params[:card_cvv],
            exp_date: "#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}"
        }
        card_block = check_card_blocked(params[:card_number],"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}", card_info)
        if card_block
          raise StandardError.new(I18n.t('api.errors.blocked_card'))
        end

        card_bin_list = get_card_info(params[:card_number])
        card1 = Payment::QcCard.new(card_info,nil,user.id)
        card = user.cards.where(fingerprint: card1.fingerprint,merchant_id: @user.id).last
        if card.blank?
          raise StandardError.new "CVV Incorrect" if params[:card_cvv] == "0"
          card = Card.create(merchant_id: @user.id,user_id: user.id,qc_token:card1.qc_token,exp_date: "#{card_info[:month]}/#{card_info[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand, last4: params[:card_number].last(4), name: params[:card_holder_name], card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: params[:card_number].first(6).to_i,fingerprint: card1.fingerprint)
        # else
        #   raise StandardError.new "Card Already Exit"
        end
      end
      return render_json_response({:success => true, :card_token => card.fingerprint},:ok)
    rescue StandardError => exc
      Raven.capture_exception(exc)
      return render_json_response({:success => false, :message => exc.message},:ok)
    end
  end

  def setting_params
    params[:card_token] = params[:card_token].gsub(" ","+") if params[:card_token].include? (" ")
    card = Card.includes(:user).where(fingerprint: params[:card_token]).last
    if card.present?
      user = card.user
      params[:first_name] = user.first_name
      params[:last_name] = user.last_name
      params[:name] = user.name
      params[:email] = user.email
      params[:phone_number] = user.phone_number
      card_detail = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
      if card_detail.number.present?
        # params[:card_cvv] = card_detail.cvv
        params[:exp_date] = "#{card_detail.month}#{card_detail.year.last(2)}"
        params[:card_number] = card_detail.number
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def valid_email?
    params[:email].present? && (params[:email] =~ VALID_EMAIL_REGEX)
  end


  private


  def saving_decline_for_seq(issue_raw_transaction,payment_gateway_id = nil)
    if params[:both]
      location_fee = issue_raw_transaction.get_location_fees(params[:merchant_wallet_id],@user,params[:amount],nil,TypesEnumLib::TransactionType::CreditTransaction)
      #= building hash for Decline
      transaction_info = {amount: params[:amount].to_f}
      reserve_detail = location_fee.first if location_fee.present?
      user_info = location_fee.second if location_fee.present?
      fee = location_fee.third if location_fee.present?
      transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
      #= end building process
      OrderBank.create(merchant_id: params[:merchant_wallet_id],user_id: params[:wallet_id],bank_type: TypesEnumLib::DeclineTransacitonTpe::Ecommerce,card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id], payment_gateway_id:payment_gateway_id)
    elsif params[:order_bank].present?
      order_bank = params[:order_bank]
      OrderBank.create(merchant_id: order_bank.merchant_id,user_id: order_bank.user_id,bank_type: TypesEnumLib::DeclineTransacitonTpe::Ecommerce,card_type: nil,card_sub_type:nil,status: "decline",amount: order_bank.amount,transaction_info: order_bank.transaction_info,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway_id)
    elsif params[:issue_amount].blank?
      location_fee = issue_raw_transaction.get_location_fees(params[:merchant_wallet_id],@user,params[:amount],nil,TypesEnumLib::TransactionType::CreditTransaction)
      #= building hash for Decline
      transaction_info = {amount: params[:amount].to_f}
      reserve_detail = location_fee.first if location_fee.present?
      user_info = location_fee.second if location_fee.present?
      fee = location_fee.third if location_fee.present?
      transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
      #= end building process
      OrderBank.create(merchant_id: params[:merchant_wallet_id],user_id: params[:wallet_id],bank_type: TypesEnumLib::DeclineTransacitonTpe::Ecommerce,card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway_id)
    end
  end

  def save_billing_info(user, type)
    address_id = user.addresses.try(:last).try(:id)
    new_address = Address.where(id: address_id).first_or_initialize
    new_address.name = user.name
    new_address.email = user.email
    new_address.phone_number = user.phone_number
    new_address.street = params[:street] if params[:street].present?
    new_address.suit = params[:suit] if params[:suit].present?
    new_address.city = params[:city] if params[:city].present?
    new_address.state = params[:state] if params[:state].present?
    new_address.country = params[:country] if params[:country].present?
    new_address.zip = params[:zip_code] if params[:zip_code].present?
    new_address.address_type = type
    new_address.save
    return new_address
  end
end
