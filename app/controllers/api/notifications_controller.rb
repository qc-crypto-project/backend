class Api::NotificationsController < Api::ApiController

  def list
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    notifications = Notification.includes(:actor).where(recipient: @user).recent
    json = []
    notifications.each do |notification|
      obj = {}
      obj[:id] = notification.id
      obj[:unread] = !notification.read_at?
      obj[:actor] = NotifyUserSerializer.new(notification.actor)
      obj[:action] = notification.action
      obj[:type] = "#{notification.notifiable.class.to_s.underscore.humanize.downcase}"
      # obj[:message] = "You #{notification.action} $#{n.notifiable.amount} from #{n.actor.name}"
      obj[:notifier] = notification.notifiable
      json.push(obj)
    end
    return render_json_response({success: true, notifications: json },:ok)
  end

  def count
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    count = Notification.where(recipient: @user).unread.count
    return render_json_response({success: true, count: count },:ok)
  end

  def mark_as_read
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    notifications = Notification.where(recipient: @user).unread
    notifications.update_all(read_at: Time.zone.now) if notifications.count > 0
    return render_json_response({success: true, message: 'Updated!' },:ok)
  end

  def archive
    return render_json_response({success: false, message: I18n.t("errors.checkout.depreciated") },:ok)
    notification = params[:notification_id]
    notification = Notification.where(recipient: @user, id: notification)
    notification.update(archived: true)
    return render_json_response({success: true,message: 'Deleted'},:ok)
  end
end
