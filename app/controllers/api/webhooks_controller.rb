class Api::WebhooksController < Api::ApiController
  include CheckbookHelper
  include OrdersHelper
  include WithdrawHandler
  skip_before_action :authenticate_user

  def checkbook
    msg = "details from checkbook: type: #{params[:type]}: check_status: #{params[:status]}: check_id: #{params[:id]}: url: #{request.host} \n Response from Webhook: \`\`\`#{params.to_json}\`\`\`"
    handle_withdraw_exception(msg, I18n.t('slack.channels.withdraw_alert_channel'), true)
    check = TangoOrder.find_by(checkId: params[:id])
    update_check_order(check, params[:status])
    render :json => {:status => 200}
  end

  def sengrid_email
    update_email={}
    if params[:_json].present?
      params[:_json].each do |e|
       msg_id= e[:sg_message_id].split('.').first if e[:sg_message_id].present?
         email=EmailMessage.find_by(to:e[:email],message_id:msg_id)
         if email.present?
           if update_email[email.id].present?
             update_email[email.id].status=e[:event]
           else
             email.status=e[:event] if email.present?
             update_email=update_email.merge({email.id=>email})
           end
         end
      end
    end
    EmailMessage.import update_email.values, on_duplicate_key_update: {conflict_target: [:id], columns: [:status]} if update_email.present?

  end

  def quickcard_status
    #api to check if server is up or not
    render_json_response({:success => true}, :ok)
  end

  def easypost_hook
    begin
      track = params[:result]
      if track.present?
        tracker = Tracker.where("tracker_code = ? OR return_tracking_code = ? ", track[:result][:tracking_code] , track[:result][:tracking_code]).last
        if tracker.present?
          if tracker.is_return == true
            tracker.return_tracking_details = JSON.parse(track[:tracking_details].to_json) if track[:tracking_details].present?
            tracker.return_status = track[:tracking_details].last[:status] if track[:tracking_details].present?
            dates = track[:tracking_details].present? ? tracker_dates(JSON.parse(track[:tracking_details].to_json)) : nil
            tracker.return_shipment_date = dates.first.to_datetime if dates.present? && dates.first.present?
            tracker.return_delivered_date = dates.second.to_datetime if dates.present? && dates.second.present?
            tracker.return_est_delivery_date = track[:est_delivery_date]
            tracker.save!
          else
            tracker.tracking_details = JSON.parse(track[:tracking_details].to_json) if track[:tracking_details].present?
            tracker.status = track[:tracking_details].last[:status] if track[:tracking_details].present?
            dates = track[:tracking_details].present? ? tracker_dates(JSON.parse(track[:tracking_details].to_json)) : nil
            tracker.shipment_date = dates.first.to_datetime if dates.present? && dates.first.present?
            tracker.delivered_date = dates.second.to_datetime if dates.present? && dates.second.present?
            tracker.est_delivery_date = track[:est_delivery_date]
            tracker.save!
          end
        end
      end
    rescue
      #SlackService.notify(msg, "#qc-files")
      # TODO notify on slack
    end
    render :json => {status: 200}
  end
  def cbk_defense
    SlackService.notify(params.except("controller","action"),"#qc-withdrawals")
    render_json_response({success: true}, :ok)
  end
end
