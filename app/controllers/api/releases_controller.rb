class Api::ReleasesController <  Api::ApiController

  include ExceptionHandler
  include ApplicationHelper

  skip_before_action :authenticate_user, only: [:create]

  def create
    if params[:auth_token].present? && params[:auth_token] == '0922ea70159b00cd5d58258d612de97c78c624938a1115412a9e59a0cadb0e96d2d9c0ab2116d68f3e3079cb1253ab51a7b044eaf475ca95850b4123019c2729'
      begin
        raise StandardError.new(I18n.t("errors.checkout.depreciated"))
        if params[:version].present?
          release = Release.new(version: params[:version])
          if params[:note].present?
            release.note = params[:note]
          end
          if release.save
            msg = "Successfully deployed release to live version: #{release.version}"
            push_to_slack(msg)
            return render_json_response({success: true, message: "Successfully created release"},:ok)
          end
        else
          old_release = Release.last
          if old_release.present?
            if (old_release.version).to_s.include?('.9')
              version = number_with_precision(old_release.version.to_f.round(), precision: 1)
            else
              version = number_with_precision(old_release.version.to_f + 0.1, precision: 1)
            end
          else
            version = 1.0
          end
          release = Release.new(version: version)
          if params[:note].present?
            release.note = params[:note]
          end
          if release.save
            msg = "Successfully deployed release to live version: #{release.version}"
            push_to_slack(msg)
            return render_json_response({success: true, message: "Successfully created release"},:ok)
          end
        end
      rescue => ex
        p"-----------EXCEPTION HANDLED #{ex.message}"
      end
    else
      return render_json_response({success: false, message: "Please provide authentication token!"},:ok)
    end
  end

  private

  def push_to_slack(msg)
    SlackService.notify(msg, '#quickcard')
  end
end
