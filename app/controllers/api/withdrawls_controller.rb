class Api::WithdrawlsController < Api::ApiController

  def submit
    return render_json_response({:success => false, :message => I18n.t("errors.checkout.depreciated")}, :ok)
    error = 'Withdrawl Request Failed! Try Again!'
    wallet = Wallet.find(withdrawl_params[:wallet_id]) if withdrawl_params[:wallet_id].present?
    wallet = @user.wallets.first if withdrawl_params[:wallet_id].nil?
    if withdrawl_params[:account_number].present? && wallet.present?
      # escrow_wallet = User.where(name: 'Escrow Support').first.wallets.first
      if wallet.present?
        balance = show_balance(wallet.id)
        amount = dollars_to_cents(withdrawl_params[:amount])
        if balance.to_f >= amount.to_f
          @user.withdraw(amount, wallet.id)
          # escrow = Escrow.create(to_id: escrow_wallet.id, from_id: wallet.id, amount: withdrawl_params[:amount], status: 'pending')
          # if escrow.errors.any?
          #   error = escrow.errors.full_messages.first
          # else
          #   transaction_between(escrow_wallet.id, wallet.id, amount, "Escrow Transfer", "0", '')
            request = @user.withdrawls.create(withdrawl_params.merge({:wallet_id => wallet.id}))
            if request.errors.any?
              error = request.errors.full_messages.first
            else
              return render_json_response({:success => true, :message => 'Withdrawl Request Submitted!'}, :ok)
            end
          # end
        else
          error = 'Wallet Balance is Low!'
        end
      end
      error = 'Target Wallet not Found!' if wallet.nil?
    else
      error = 'Invalid Parameters!'
    end
    return render_json_response({:success => false, :message => error}, :ok)
  end

  private

  def withdrawl_params
    params.require(:withdrawl).permit(:bank_name ,:account_number, :routing_number, :amount)
  end
end
