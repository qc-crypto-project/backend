class Api::PossController < Api::ApiController
  include Api::PossHelper
  include Api::RegistrationsHelper
  include TransactionCharge::ChargeATransactionHelper
  include RegistrationsHelper

  def pos_scanner
    if params[:qr_token].present? and params[:to_id].present?
      begin
        if greater_amount
          qr_card = QrCard.includes(:user).where(token: params[:qr_token]).first
          if qr_card.present?
            if qr_card.price.to_f == params[:amount].to_f
              user = qr_card.user
              if @user.id == user.id
                return render_json_response({:success => false, :message => 'Cannot transfer to Your wallet.'}, :ok)
              else
                if qr_card and qr_card.is_valid == true
                  if user and user.wallets.where(id: qr_card.wallet_id).first.present?
                    price = qr_card.price.to_f
                    if qr_card.message.present?
                      send_text_message(qr_card.message,@user.phone_number)
                      push_notification(qr_card.message, @user)
                    end
                    balances = show_balance(qr_card.wallet_id)
                    if balances.to_f >= qr_card.price.to_f
                      data = ''
                      current_transaction = transaction_between(params[:to_id], qr_card.wallet_id,price,"By Qr Card:#{qr_card.id}","0", data)
                      #twilio_text = send_text_message("#{number_to_currency(price)} has been successfully transferred to your wallet from #{user.name}.\nNote From Sender: #{params[:message]}", user.phone_number)
                      balance = show_balance(params[:to_id])
                      transactions = all_transactions(params[:to_id],params[:to_id],params[:page_number])
                      qr_card.update(is_valid: false)
                      return render_json_response({:success => true, :message => "Successfully transferred $#{format_dollar_amount(qr_card.price)}",amount: format_dollar_amount(qr_card.price),transactions: transactions, balance: balance.to_f.round(2)}, :ok)
                    else
                      return render_json_response({:success => false, :message => 'Transaction Cannot Perfom Due to Low balance!'}, :ok)
                    end
                  else
                    return render_json_response({:success => false, :message => 'wallet and user ids doesnot match'}, :ok)
                  end
                else
                  return render_json_response({:success => false, :message => 'Card Already Used'}, :ok)
                end
              end
            else
              return render_json_response({:success => false, :message => 'Card Not equals to your amount'}, :ok)
            end
          else
            return render_json_response({:success => false, :message => 'Card Not Found!'}, :ok)
          end
        else
          return render_json_response({success: false, message: "Amount can not be 0!"},:ok)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        return render_json_response({:success => false, :message => "Something Went Wrong #{ex.message}"},:ok)
      end
    else
      return render_json_response({:success => false, :message => 'Incomplete Params'}, :ok)
    end
  end

  def generate_qr
    return maintenance_response()
    return sequence_down() if @seq_down.present? && @seq_down == true
    begin
      validate_parameters(
          :auth_token => params[:auth_token],
          :amount => params[:amount],
          :phone_number => params[:phone_number],
          :card_number => params[:card_number],
          :card_exp_date => params[:card_exp_date],
          :card_holder_name => params[:card_holder_name],
          :zip_code => params[:zip_code],
          :location_or_wallet_id => params[:location_id].present? ? params[:location_id] :params[:wallet_id]
      )
      return render_json_response({success: false, status: TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card_data')}, :ok) if params[:card_exp_date].length != 4
      raise StandardError.new I18n.t('api.registrations.amount_error') if params[:amount].to_f <= 0
      params[:exp_date] = params[:card_exp_date]
      params[:ref_id] = params[:RefId]

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Already exist or Invalid RefId!"}, :ok) if Transaction.exists?(ref_id: params[:ref_id])

      user = User.where(role: "user",phone_number: params[:phone_number]).last
      if params[:location_id].present?
        location = Location.where(location_secure_token: params[:location_id]).first
        wallet = location.wallets.primary.first
      else
        wallet = Wallet.where(id: params[:wallet_id]).last
        location = wallet.location if wallet.present?
      end
      @location = location
      #check user exist in wallet users list
      raise SaleValidationError.new I18n.t('api.registrations.wrong_user') unless wallet.users.pluck(:id).include?(@user.id)
      params[:wallet_id] = wallet.id
      merchant = @user

      if params[:card_holder_name].present?
        email = params[:card_holder_name].strip.split(' ').join.downcase+"@gbox.com"
      else
        email = "quickcard#{rand(1..1000)}@gbox.com"
      end
      if user.blank?
        user = User.new(email: email, source: merchant.try(:name), password: random_password, role: :user,
                        name: params[:card_holder_name],
                        phone_number: params[:phone_number],
                        last_name: params[:card_holder_name],
                        first_name: params[:card_holder_name],
                        zip_code: params[:zip_code])
        user.regenerate_token
        user.save(:validate => false)
      else
        user.update(name: params[:card_holder_name], last_name: params[:card_holder_name], first_name: params[:card_holder_name], zip_code: params[:zip_code])
      end
      user_primary_wallet = user.wallets.primary.first
      params[:wallet_id] = user_primary_wallet.try(:id)
      user_wallet = params[:wallet_id]
      params[:merchant_wallet_id] = wallet.id
      @card_customer = user
      result_card = getting_or_creating_card
      unless result_card[:status]
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.invalid_card')}, :ok)
      else
        card = result_card[:card]
        card_bin_list = result_card[:card_bin_list]
      end
      card_type = card_bin_list["type"].try(:downcase) == "debit" ? "debit" : "credit"
      merchant_balance = SequenceLib.balance(wallet.id)
      issue_raw_transaction = IssueRawTransaction.new(nil,nil)
      buyRate = location.fees.buy_rate.first
      total_amount = params[:amount].to_f #+ params[:tip].to_f
      amount_fee_check = check_fee_amount(location, merchant, nil, wallet, buyRate, total_amount, merchant_balance, TypesEnumLib::CommissionType::C2B, 'debit',true)
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Transaction processing failed: The amount $#{total_amount} is less than the minimum amount required to process a transaction. Kindly contact administrator."}, :ok)if amount_fee_check.present? && amount_fee_check[:status] == false
      location_fee = issue_raw_transaction.get_location_fees(wallet.id, merchant, total_amount, total_amount, card_type,nil,true)
      #= Slots logic
      if card_bin_list["type"].present? && card_bin_list["type"].try(:downcase) == "debit"
        if location.is_slot && location.slot.present?
          slot = location.slot
          if params[:amount].to_f > slot.amount.try(:to_f)   # amount is greater => over logic
            result = getting_slot(slot,params[:amount].to_f, location_fee.try(:third).to_f, params[:tip].to_f,slot.slot_over,"slot_over")
          else
            result = getting_slot(slot,params[:amount].to_f, location_fee.try(:third).to_f,  params[:tip].to_f,slot.slot_under,"slot_under")
          end
        else
          slot = AppConfig.find_by(key: AppConfig::Key::SlotOver)
          slot_amount = slot.try(:stringValue)
          if params[:amount].to_f > slot_amount.try(:to_f)   # amount is greater => over logic
            result = getting_slot(slot,params[:amount].to_f, location_fee.try(:third).to_f, params[:tip].to_f,nil, nil)
          else
            slot = AppConfig.find_by(key: AppConfig::Key::SlotUnder)
            result = getting_slot(slot,params[:amount].to_f,  location_fee.try(:third).to_f, params[:tip].to_f,nil, nil)
          end
        end
        #create debit transaction , return false if ref_id is same or blank
        amount_merchant1 = result.try(:[],:a_merchant1).to_f
        amount_merchant2 = result.try(:[],:a_merchant2).to_f
        amount_actual = params[:amount].to_f + params[:tip].to_f + location_fee.try(:third).to_f

        if amount_merchant1.to_f > amount_actual.to_f
          privacy_fee = number_with_precision(amount_merchant1 - amount_actual.to_f, precison: 2)
        elsif amount_merchant2.to_f > amount_actual.to_f
          privacy_fee = number_with_precision(amount_merchant2 - amount_actual.to_f, precision: 2)
        else
          privacy_fee = params[:privacy_fee].to_f
        end
        # TypesEnumLib::TransactionViewTypes::SaleIssue, TypesEnumLib::TransactionSubType::QRDebitCard
        create_transaction_debit(user, user_primary_wallet.id, user.id, card, total_amount, request, params[:ref_id],TypesEnumLib::TransactionViewTypes::SaleIssue,privacy_fee, params[:tip], params[:clerk_id], params[:terminal_id], location_fee.try(:third).to_f,nil,TypesEnumLib::TransactionSubType::QRDebitCard,"issue",user,user_primary_wallet,wallet.id,location_fee.try(:third).to_f)
        original_amount = params[:amount].to_f + params[:tip].to_f
        load_fee = location_fee.third.to_f
        return render_json_response({:success => true,:status => TypesEnumLib::Statuses::Pending, merchant1: result.try(:[],:merchant1), amount_merchant1: "$#{number_with_precision(amount_merchant1, precision: 2)}", merchant2: result.try(:[],:merchant2), amount_merchant2: "$#{number_with_precision(amount_merchant2, precision: 2)}", :original_amount => "$#{number_with_precision(original_amount, precision: 2)}", :load_fee => "$#{number_with_precision(load_fee, precision: 2)}", :cashback_fee => "$#{number_with_precision(privacy_fee, precision: 2)}", tip: number_with_precision(number_to_currency(params[:tip].to_f),precision: 2), RefId: params[:RefId], clerk_id: params[:clerk_id]}, :ok)
      else
        create_transaction_debit(user, user_primary_wallet.id, user.id, card, total_amount, request, params[:ref_id],TypesEnumLib::TransactionViewTypes::SaleIssue,privacy_fee, params[:tip], params[:clerk_id], params[:terminal_id], location_fee.try(:third).to_f,nil,TypesEnumLib::TransactionSubType::QRCreditCard,"issue",user,user_primary_wallet,wallet.id,location_fee.try(:third).to_f)
        original_amount = params[:amount].to_f #+ params[:tip].to_f
        load_fee = location_fee.third.to_f
        total_amount = original_amount + load_fee + params[:tip].to_f
        return render_json_response({:success => true, :status => TypesEnumLib::Statuses::Pending, total_amount: "$#{number_with_precision(total_amount, precision: 2)}", :original_amount => "$#{number_with_precision(original_amount, precision: 2)}", :load_fee => "$#{number_with_precision(load_fee, precision: 2)}", tip: number_with_precision(number_to_currency(params[:tip].to_f),precision: 2), RefId: params[:RefId], clerk_id: params[:clerk_id]}, :ok)
      end
    rescue ArgumentError, SaleValidationError  => exc
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  def generate_qr_debit
    return maintenance_response()
    return sequence_down() if @seq_down.present? && @seq_down == true
    begin
      @eaze_reporting = true
      @cultivate_params = true
      validate_parameters(
          auth_token: params[:auth_token],
          ref_id: params[:RefId],
          location_or_wallet_id: params[:location_id].present? ? params[:location_id] :params[:wallet_id],
          status: params[:status]
      )
      if params[:status].present? && params[:status].downcase != "declined"
        validate_parameters(
            card_cvv: params[:card_cvv],
            amount: params[:amount]
        )
      end
      raise StandardError.new I18n.t('api.registrations.amount_error') if params[:amount].to_f <= 0
      params[:exp_date] = params[:card_exp_date]
      params[:ref_id] = params[:RefId]

      trans = Transaction.where(ref_id: params[:RefId],status: "pending").first
      if trans.present?
        transaction_db = trans
        if trans.main_type == "Sale Issue" && trans.sub_type == "qr_credit_card"
          card_type = "credit"
        elsif trans.main_type == "Sale Issue" && trans.sub_type == "qr_debit_card"
          card_type = "debit"
          validate_parameters(approved_slot: params[:approved_slot]) if params[:status] == "approved"
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Wrong approved slot, Please try with correct Slot"},:ok) unless PaymentGateway.slot.exists?(slot_name: params[:approved_slot])
        else
          return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Wrong RefId, Please try with correct RefId"},:ok)
        end
      else
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Wrong RefId, Please try with correct RefId"},:ok)
      end

      params[:issue_amount] = params[:amount].to_f
      params[:virtual_transaction_debit] = true

      if params[:location_id].present?
        location = Location.where(location_secure_token: params[:location_id]).first
        return render_json_response({success: false, message: "Sorry but Location is currently not available. Please contact you administrator for details."}, :ok) if location.nil?
        wallet = location.wallets.primary.first
      else
        wallet = Wallet.find_by(id: params[:wallet_id])
        location = wallet.location
      end
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Location or Wallet ID mismatch"}, :ok) if "#{wallet.id}" != transaction_db.from
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t("errors.withdrawl.blocked_location")}, :ok) if location.is_block
      @location = location
      merchant = location.merchant
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.wallet.user_not_merchant')},:ok) if !merchant.merchant?
      params[:merchant_wallet_id] = wallet.id
      params[:wallet_id] = transaction_db.receiver_wallet_id
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: I18n.t('api.registrations.wrong_user')}, :ok) unless wallet.users.pluck(:id).include?(@user.id)

      #get merchant total balance from sequence library by giving wallet id
      merchant_balance = show_balance(wallet.id, merchant.ledger)
      #get buy_rate fee of location to calculate amount which transfer to merchant wallet
      buyRate = location.fees.buy_rate.first

      #get amount fee check for merchant iso agnet and affiliate
      issue_raw_transaction = IssueRawTransaction.new(nil,params,params[:card_cvv])
      amount_fee_check = check_fee_amount(location, merchant, nil, wallet, buyRate, transaction_db.amount, merchant_balance, TypesEnumLib::CommissionType::C2B, card_type,true)
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: "Transaction processing failed: The amount $#{transaction_db.amount} is less than the minimum amount required to process a transaction. Kindly contact administrator."}, :ok) if amount_fee_check.present? && amount_fee_check[:status] == false
      location_fee = issue_raw_transaction.get_location_fees(wallet.id, merchant, transaction_db.amount, transaction_db.amount, card_type,nil,true)
      params[:location_fee] = location_fee
      tip_details = {}
      sales_info = {}
      sales_info = sales_info.merge({terminal_id: params[:terminal_id]}) if params[:terminal_id].present?
      sales_info = sales_info.merge({transaction_number: params[:transaction_number]}) if params[:transaction_number].present?
      sales_info = sales_info.merge({payment_type: params[:payment_type]}) if params[:payment_type].present?
      sales_info = sales_info.merge({card_type: params[:card_type]}) if params[:card_type].present?
      sales_info = sales_info.merge({tpn: params[:tpn]}) if params[:tpn].present?
      sales_info = sales_info.merge({ref_id: params[:RefId]})
      qc_wallet = Wallet.escrow.first

      user = transaction_db.receiver
      if transaction_db.clerk_id.present?
        sales_info = sales_info.merge({clerk_id: transaction_db.clerk_id})
        clerk_id = transaction_db.clerk_id
      end
      if transaction_db.tip.to_f > 0
        tip_wallet = location.wallets.tip.first
        tip_details = {:wallet_id => tip_wallet.id, :sale_tip => transaction_db.tip.to_f} if tip_wallet.present?
      end
      location_fee.second.except!("reserve") if location_fee.second.try(:[],"reserve").present?
      location_fee[0] = nil
      total_fee = save_total_fee(location_fee.try(:second), location_fee.try(:third).to_f) || 0
      transaction_db.total_amount = params[:amount]
      transaction_db.net_amount = params[:amount].to_f - total_fee.to_f - location_fee.try(:first).try(:[],"amount").to_f - transaction_db.tip.to_f - transaction_db.privacy_fee.to_f
      transaction_db.fee = total_fee
      transaction_db.net_fee = total_fee.to_f
      transaction_db.reserve_money = location_fee.try(:first).try(:[],"amount").to_f
      transaction_db.terminal_id = params[:terminal_id]
      card = transaction_db.card
      transaction_db.save
      params[:transaction_id] = transaction_db.id

      if params[:status].present? && params[:status].downcase == "declined"
        build_decline_debit("error")
        return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Declined!"},:ok)
      end

      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Declined,:message => "Status mismatch can only be approved or declined"},:ok) if params[:status] != "approved" && params[:status] != "declined"

      card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, user.id)
      params[:card_number] = card_info.number
      card_bin_list = get_card_info(params[:card_number])

      seq_transaction_id = {}
      if card_type == "credit"
        @payment_gateway = @location.try(:primary_gateway) if !@location.try(:primary_gateway).try(:is_block)
        gateway = @payment_gateway.key if @payment_gateway.present?
        if @location.primary_gateway.present?
          #= LOAD BALANCER first_payment_gateway
          seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,nil,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.secondary_gateway,card_bin_list,nil,nil,nil,params[:flag])
          #= LOAD BALANCER secondary_payment_gateway
          if @location.primary_gateway.is_block && seq_transaction_id.nil?
            seq_transaction_id = {:message => {:message => "Processing: Inactive – Please contact customer support."}}
          end
          if seq_transaction_id.try(:[],:response).blank? && @location.secondary_gateway.present? && !@location.secondary_gateway.is_block
            message = ""
            message = seq_transaction_id[:message] if seq_transaction_id[:message].present?
            saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
            @payment_gateway = @location.secondary_gateway
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,nil,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.ternary_gateway,card_bin_list,nil,nil,nil,params[:flag])
            gateway = @payment_gateway.key
          end
          #= LOAD BALANCER third_payment_gateway
          if seq_transaction_id.try(:[],:response).blank? && @location.ternary_gateway.present? && !@location.ternary_gateway.is_block
            message = seq_transaction_id[:message] if seq_transaction_id[:message].present?
            saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: message})
            @payment_gateway = @location.ternary_gateway
            seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,nil,TypesEnumLib::TransactionSubType::SaleIssueApi,nil,card_bin_list,nil,nil,nil,params[:flag])
            gateway = @payment_gateway.key
          end

          if seq_transaction_id.try(:[],:id).present?
            if @payment_gateway.present?
              @payment_gateway.update_daily_monthly_limits(seq_transaction_id[:issue_amount])
            end
          end

          if seq_transaction_id.try(:[], :blocked).present?
            return maintenance_response
          elsif seq_transaction_id.try(:[], :decline_message).present?
            return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:decline_message]}, :ok)
          elsif seq_transaction_id.try(:[], :response).blank?
            params[:order_bank].update(status: "seq_crash") if params[:order_bank].present? && params[:charge_id].present?
            if seq_transaction_id.blank?
              return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok)
            end
            return render_json_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message]}, :ok)
          end
        end
      else
        @payment_gateway = PaymentGateway.slot.where(slot_name: params[:approved_slot]).first if params[:approved_slot].present?
        gateway = @payment_gateway.try(:name) || TypesEnumLib::GatewayType::PinGateway
      end
      issue_sub_type = card_type == "debit" ? TypesEnumLib::TransactionSubType::QRDebitCard : TypesEnumLib::TransactionSubType::QRCreditCard
      transaction = SequenceLib.issue(transaction_db.receiver_wallet_id,params[:issue_amount],wallet.id, TypesEnumLib::TransactionType::SaleType, transaction_db.fee, nil,gateway, nil, params[:card_number].last(4), nil, nil, location_fee.second, get_ip, nil, nil, nil, nil, nil, sales_info, nil, card_type == "credit" ? "qr_code" : card_type,issue_sub_type,nil)

      return render_json_response({ :success => false, :status => TypesEnumLib::Statuses::Failed, :message => I18n.t('api.registrations.sequence')},:ok) if transaction.blank?
      transaction_db.status = "approved"
      transaction_db.charge_id = seq_transaction_id.try(:[],:response) if seq_transaction_id.present?
      transaction_db.seq_transaction_id = transaction.present? ? transaction.actions.first.id : nil
      transaction_db.tags = transaction.actions.first.tags
      transaction_db.payment_gateway_id = @payment_gateway.id
      transaction_db.timestamp = transaction.timestamp
      transaction_db.save
      #= creating block transaction
      parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
      OrderBank.create(merchant_id: transaction_db.receiver_id, bank_type: TypesEnumLib::DeclineTransacitonTpe::Debit,card_type: "debit_pin",status: "approve",amount: params[:amount].to_f, transaction_id: transaction_db.id, payment_gateway_id: gateway) if card_type == "debit"
      qr_create(transaction_db.receiver, merchant, wallet, transaction_db.receiver_wallet_id, transaction.actions.first.id,card_type,transaction_db.id)
    rescue ArgumentError => exc
      return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, :message => exc.message}, :ok)
    rescue StandardError => exc
      return handle_standard_error(exc)
    end
  end

  private

  def qr_create(user, merchant, wallet, user_wallet, seq_id,type=nil,local_tx_id=nil)
    #creating request starts
    request = Request.create(amount: params[:amount], status: "in_progress", sender_id: user.id, reciever_id: merchant.id, wallet_id: wallet.id, sender_name: user.try(:name), request_type: "qr_code")
    request.create_qr_batch(type)
    #creating request end
    qr_card = QrCard.new(user_id: user.id, wallet_id: wallet.id, from_id: user_wallet)
    qr_card.generate_token
    qr_card.price = params[:amount]
    req = {"qr_token" => qr_card.token}
    barcode = Barby::QrCode.new(req.to_json, level: :q, size: 10)
    base64_output = Base64.encode64(barcode.to_png({ xdim: 5 }))
    data = "data:image/png;base64,#{base64_output}"
    image = Paperclip.io_adapters.for(data)
    image.original_filename = "qr_image.png"
    qr_card.image = image
    qr_card.category = "sent"
    qr_card.request_id = request.id
    qr_card.transaction_id = params[:transaction_id].present? ? params[:transaction_id] : local_tx_id
    qr_card.save!
    job_id = RefundTransactionWorker.perform_in(60.minutes, qr_card.id)
    qr_card.job_id = job_id
    qr_card.save
    render_json_response({success: true, barcode_image: "https:#{qr_card.image.url}", qr_code: qr_card.token, seq_transaction_id: seq_id},:ok)
  end

  def credit_transaction(email, card, merchant, location, user, card_bin_list, wallet, user_wallet,params)
    tip_details = {
        sale_tip: params[:tip].to_f
    }
    issue_raw_transaction = IssueRawTransaction.new(email,params,params[:card_cvv])
    qc_wallet = Wallet.escrow.first
    @payment_gateway = location.primary_gateway
    seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, merchant, qc_wallet, request, "qr_code", tip_details, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,nil,TypesEnumLib::TransactionSubType::QRCreditCard,location.ternary_gateway,card_bin_list)
    if seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[],:id).blank? && location.secondary_gateway.present?
      saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message]})
      @payment_gateway = location.secondary_gateway
      seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, merchant, qc_wallet, request, "qr_code", tip_details, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Credit,nil,TypesEnumLib::TransactionSubType::QRCreditCard,location.ternary_gateway,card_bin_list)
    end
    # LOAD BALANCER third_payment_gateway
    if seq_transaction_id.try(:[],:response).blank? && seq_transaction_id.try(:[],:id).blank?  && location.ternary_gateway.present?
      saving_response({:success => false,:status => TypesEnumLib::Statuses::Declined, message: seq_transaction_id[:message][:message]})
      @payment_gateway = location.ternary_gateway
      seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, merchant, qc_wallet, request, "qr_code", tip_details, user, params[:card_number],nil,TypesEnumLib::DeclineTransacitonTpe::Credit,nil,TypesEnumLib::TransactionSubType::QRCreditCard,location.ternary_gateway,card_bin_list)
    end

    if seq_transaction_id.try(:[], :id).present?
      @payment_gateway.update_daily_monthly_limits(seq_transaction_id[:issue_amount]) if @payment_gateway.present?
      params[:amount] = params[:privacy_fee].to_f + params[:tip].to_f + params[:amount].to_f
      qr_create(user, merchant, wallet, user_wallet, seq_transaction_id[:id],"credit")
    else
      render_json_response({success: false, :status => TypesEnumLib::Statuses::Declined, message: 'Can not process at the moment please try later'}, :ok)
    end
  end

  def build_decline_debit(e)
    #= building hash for Decline
    transaction_info = {amount: params[:amount].to_f}
    reserve_detail = params[:location_fee].first if params[:location_fee].present?
    user_info = params[:location_fee].second if params[:location_fee].present?
    fee = params[:location_fee].third if params[:location_fee].present?
    transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
    #= end building process
    OrderBank.create(merchant_id: @user.id, bank_type: TypesEnumLib::DeclineTransacitonTpe::Debit,card_type: "debit_pin",status: "decline",amount: params[:amount].to_f,transaction_info: transaction_info.to_json, transaction_id: params[:transaction_id] )
  end

end
