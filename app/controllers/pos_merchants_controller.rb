class PosMerchantsController < ApplicationController
  before_action :admin
  layout 'admin'

  def new
    @user = User.new
  end

  def create
    @pos = User.new(sign_up_params)
    @pos.role_ids = 5
    @pos.regenerate_token
    if @pos.save
      flash[:notice] = "Successfully created User."
      redirect_back(fallback_location: root_path)
    else
      flash[:notice] = "Something went wrong"
    end
  end

  def edit
    @pos = User.friendly.find(params[:id])
  end

  def transactions
    @user = User.friendly.find(params[:pos_merchant_id])
    @wallet = @user.wallets.first
    @balance = show_balance(@wallet.id)
    @retire_transactions = all_transactions_except(@wallet.id, @wallet.id, "retire")
    @transactions = all_transactions_of_wallet(@wallet.id)
    @transactions = parse_wallet(nil, @transactions)
  end

  def index
    @poss = User.merchant.where(archived: false).reject{|i| i.wallets.count <= 0}
    @pos_count = @poss.count
    @pos = User.new
  end

  def archived_pos
    @pos = User.merchant.where(archived: true)
  end

  def show
    @atm = User.friendly.find(params[:id])
  end

  def update
    @pos = User.friendly.find(params[:id])
    @pos.update(sign_up_params)
    respond_to do |format|
      format.html {
        flash[:notice] = "Successfully."
        redirect_back(fallback_location: root_path)
      }
      format.json { head :no_content }
    end
  end

  def destroy
    @user = User.friendly.find(params[:id])
    if @user.destroy
      flash[:notice] = "Successfully deleted User."
      redirect_to root_path
    end
  end



  private

  def parse_wallet(wallet, transactions)
    list = []
    admin_wallet = Wallet.find(wallet) if wallet.present?
    transactions.each do |obj|
      if obj.present?
        list << filter_actions(obj, wallet).map do |t|
          {
              id: t.id,
              timestamp: obj.timestamp,
              type: parse_type(wallet, t),
              amount: number_to_currency(SequenceLib.dollars(t.amount)),
              adminAmount: number_to_currency(parse_admin_amount(t)),
              destination: t.destination_account_id,
              source: t.source_account_id,
              reference: t.tags,
              wallet: admin_wallet
          }
        end
      end
    end
    list.reduce([], :concat).reject(&:nil?)
  end
  def filter_actions(obj, wallet)
    return obj.actions.select{|o| o.source_account_id == wallet.to_s || o.destination_account_id == wallet.to_s} if wallet.present?
    obj.actions
  end
  def parse_type(wallet, action)
    return action.type if wallet.nil?
    if action.type == 'issue' && action.destination_account_id == wallet.to_s
      'Deposit'
    else
      "Send To #{action.destination_account_id}"
    end
  end

  def parse_admin_amount(transaction)
    amount = SequenceLib.dollars(transaction.amount)
    fee = transaction.tags['fee'].to_f
    if fee.present? && fee > 0 && amount != fee
      return number_with_precision(fee, precision: 2)
    else
      return number_with_precision(amount, precision: 2)
    end
  end

  def sign_up_params
    params.require(:user).permit(:name ,:postal_address ,:email, :phone_number ,:is_block, :role_ids => 3)
  end


end
