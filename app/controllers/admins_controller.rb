class AdminsController < ApplicationController
  layout 'admin'

  include ActionView::Helpers::NumberHelper
  include Admins::TransactionsHelper
  include ApplicationHelper
  include AdminsHelper
  include FluidPayHelper
  include CardsHelper
  include UsersHelper
  # before_action :check_route
  before_action :admin
  before_action :save_my_previous_url
  include Api::RegistrationsHelper
  before_action :mtrac_admin, :support_customer
  skip_before_action :mtrac_admin, :except => [:index, :verifications]
  before_action :check_admin, only: [:index, :verifications]

  # sidebar -> Verification -> Merchant Verification
  def verifications
    if params[:search_input] && params[:q].present?
      if params[:req] == "all"
        @reqs = User.unarchived_users.where("ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value ",  value: "%#{params[:q].strip}%").where(merchant_id: nil).merchant.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:req] == "pending"
        @reqs = User.unarchived_users.where("ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value ",  value: "%#{params[:q].strip}%").where(merchant_id: nil).pending_users.merchant.order('created_at DESC').pending_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:req] == "approved"
        @reqs = User.unarchived_users.where("ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value ",  value: "%#{params[:q].strip}%").where(merchant_id: nil).approved_users.merchant.order('created_at DESC').approved_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:req] == "banks"
        if params[:others] == "approved"
          @reqs = User.where("ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value",  value: "%#{params[:q].strip}%").where(merchant_ach_status: 'complete').where(send_ach_bank_request: true).order('created_at DESC').approved_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
        elsif params[:others] == "pending"
          @reqs = User.where("ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value",  value: "%#{params[:q].strip}%").where(merchant_ach_status: 'pending').where(send_ach_bank_request: true).order('created_at DESC').approved_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
        else
          @reqs = User.where("ref_no ILIKE :value OR name ILIKE :value OR email ILIKE :value",  value: "%#{params[:q].strip}%").where(merchant_ach_status:['pending','complete']).where(send_ach_bank_request: true).order('created_at DESC').approved_users.per_page_kaminari(params[:page]).per(params[:filter] || 10)
        end
      else
        params[:req] = "all"
        @reqs = User.where("name ILIKE :value OR email ILIKE :value OR phone_number ILIKE :value",  value: "%#{params[:q].strip}%").where(merchant_id: nil).merchant.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      end
    else
      if params[:req] == "all"
        @reqs = User.where.not(email: 'admin@admin.com', archived: true).where(merchant_id: nil).merchant.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:req] == "pending"
        @reqs = User.where.not(email: 'admin@admin.com', archived: true).where(merchant_id: nil).pending_users.merchant.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:req] == "approved"
        @reqs = User.where.not(email: 'admin@admin.com', archived: true).approved_users.merchant.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      elsif params[:req] == "banks"
        if params[:others] == "approved"
          @reqs = User.where.not(email: 'admin@admin.com').where(merchant_ach_status: 'complete').where(send_ach_bank_request: true).order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
        elsif params[:others] == "pending"
          @reqs = User.where.not(email: 'admin@admin.com').where(merchant_ach_status: 'pending').where(send_ach_bank_request: true).order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
        else
          @reqs = User.where.not(email: 'admin@admin.com').where(merchant_ach_status: ['pending','complete']).where(send_ach_bank_request: true).order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
        end
      else
        params[:req] = "all"
        @reqs = User.where.not(email: 'admin@admin.com').where(merchant_id: nil).merchant.order('created_at DESC').per_page_kaminari(params[:page]).per(params[:filter] || 10)
      end
    end
    @filters = [10,25,50,100]
    respond_to do |format|
      format.js
      format.html
    end
  end

  # Approve Verfication Merchant
  def approve_status
    @user = User.find(params[:id])
    if @user.archived == false
      if @user.is_block == true
        @user.update(is_block: false)
        @user.update(approve_date: Time.now)
        @user.update(approved_by: current_user.id)
        if @user.merchant?
          if @user.tos_checking == false
            check = true
          else
            check = false
          end
          @user.password_changed(check)
          @user.send_welcome_email('Merchant') if @user.company_id!=2 && @user.oauth_apps.first.is_block == false
        elsif @user.iso?
          password = random_password
          @user.update(password: password)
          message="Welcome to Quick Card"
          UserMailer.welcome_user(@user ,message, 'ISO', password).deliver_later if @user.oauth_apps.first.is_block == false
        end
      end
    elsif @user.archived == true
      # flash[:notice] = "Kindly Unacrhive first!"
      render json:{"success":"false"} , status: 200
    end
    render json:{"success":"true"} , status: 200
    # redirect_back(fallback_location: root_path)
  end

  # Reject Verfication Merchant
  def reject_status
    @user = User.find(params[:id])
    if @user.is_block == false
     @user.update(is_block: true)
    end
    redirect_back(fallback_location: root_path)
  end

  # Transaction List
  def transactions
    transaction_main_types
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
    conditions = []
    maxmind_conditions =[]
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = transactions_search_query(params[:query],params[:trans],params[:offset])
    end
    if params["query"].present? && params["query"]["risk_eval"].present?
      maxmind_conditions = maxmind_transactions_search_query(params["query"]["risk_eval"])
    end
    if params[:trans] == "undefined" || params[:trans] == "success" || params[:trans].blank?
      params[:query]['type'] = (valid_json?(params.try(:[], "query").try(:[], "type").to_s) ? JSON.parse(params.try(:[], "query").try(:[], "type").to_s) : params.try(:[], "query").try(:[], "type")) if params[:query].present?
      if params.try(:[], "query").try(:[], "type").try(:length) == 1 && params.try(:[], "query").try(:[], "type").include?("refund")
        @transactions = Transaction.all_approved(page_size, nil).where(conditions).per_page_kaminari(params[:page]).per(page_size)
      else
        if params["query"].present? && params["query"]["risk_eval"].present?
          @transactions = Transaction.all_approved(page_size, nil).where(maxmind_conditions).where(conditions).per_page_kaminari(params[:page]).per(page_size)
        else
          @transactions = Transaction.all_approved(page_size, nil).where(conditions).per_page_kaminari(params[:page]).per(page_size)
        end
      end
    elsif params[:trans] == "refund"
      @transactions = Transaction.all_refund(page_size).where(conditions).per_page_kaminari(params[:page]).per(page_size)
    elsif params[:trans] == "checks"
      @transactions = Transaction.where(conditions).where("main_type IN (?)",["Send Check", I18n.t("withdraw.failed_check"),"Void_Check", "Void Check",'bulk_check']).where(status: "approved").order('transactions.created_at DESC').per_page_kaminari(params[:page]).per(page_size)
    elsif params[:trans] == "ach"
      @transactions = Transaction.where(main_type: ['ACH','Void_Ach', 'Failed ACH','ACH_deposit','Void ACH'],status: "approved").where(conditions).order('transactions.created_at DESC').per_page_kaminari(params[:page]).per(page_size)
    elsif params[:trans] == "push_to_card"
      @transactions = Transaction.where(main_type: ["instant_pay", 'Failed Push To Card',"void_push_to_card"],status: "approved").where(conditions).order('transactions.created_at DESC').per_page_kaminari(params[:page]).per(page_size)
    elsif params[:trans] == "b2b"
      @transactions = Transaction.all_b2b(page_size).where(conditions).per_page_kaminari(params[:page]).per(page_size)
    elsif params[:trans] == "decline"
      @transactions = Transaction.all_declines(page_size).where(conditions).per_page_kaminari(params[:page]).per(page_size)
    else
      @transactions = Transaction.all_declines(page_size).where(conditions).per_page_kaminari(params[:page]).per(page_size)
    end
    @filters = [10,25,50,100]
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]

    search_type = params[:trans].present? ? params[:trans] : "success"
    @types = Transaction::SEARCH_TYPES[search_type.to_sym]  

    payment_gateways = PaymentGateway.eager_load(:descriptor).active_gateways

    payment_gateways = payment_gateways.map {|pg|
      view = pg.descriptor.present? ? "(#{pg.descriptor.name}) - #{pg.name}" : pg.name
      {:value=> pg.id, :view=> view}
    } if payment_gateways.present?

    @gateways = [{value:TypesEnumLib::GatewayType::Checkbook,view:"Checkbook.io"},{value:TypesEnumLib::GatewayType::Quickcard,view:"Quickcard"},{value:TypesEnumLib::GatewayType::Tango,view:"Tango"}]
    @risk_evaluation_status= [{value: "reject_manual_review", view:"Rejected by manual review"}, {value: "pending_manual_review", view: "Pending Manual Review"}, {value: "expired_manual_review", view: "Manual Review Expired"}, {value: "accept_manual_review", view: "Accepted by Manual Review"}, {value: "accept_default", view: "Accepted by default"}]
    @gateways = @gateways.push(payment_gateways).flatten.uniq if payment_gateways.present?
    render 'admins/shared/admin_transactions'
  end

# ---------------Refund Admin Transaction-----------------------------------------------------
  def get_partialAmount
    transaction = Transaction.where(id: params[:trans_id]).first if params[:trans_id].present?
    user_name = ''
    fee = 0
    wallet = Wallet.find_by_id(params[:destination]) if params[:destination].present?
    if wallet.present?
      if transaction.try(:main_type).present? && transaction.try(:main_type)=='Qr Debit Card'
         fee_object=wallet.try(:location).try(:fees).try(:buy_rate).try(:first)
        if (fee_object.try(:qr_refund).present? && fee_object.try(:qr_refund) > 0) || (fee_object.try(:qr_refund_percentage).present? && fee_object.try(:qr_refund_percentage) > 0)
          fee_dollar = fee_object.try(:qr_refund)
          amount=params[:amount].present? ? params[:amount] : transaction[:total_amount]
          fee_percentage = fee_object.try(:qr_refund_percentage).present? ? (fee_object.try(:qr_refund_percentage)/100 * amount.to_f) : 0
          fee = fee_dollar + fee_percentage
        end
        # fee = wallet.try(:location).try(:fees).try(:buy_rate).try(:first).try(:qr_refund)
      else
        fee = wallet.try(:location).try(:fees).try(:buy_rate).try(:first).try(:refund_fee)
      end
    end
    if params[:source].present?
      wallet = Wallet.find_by_id(params[:source])  if params[:source].present?
      user = wallet.users.first if wallet.present?
    else
      user = wallet.users.first if wallet.present?
    end
    user_name=user.name if user.present?
    if transaction.present?
      # transaction[:amount] = transaction[:amount].to_f + transaction[:tip].to_f + transaction[:privacy_fee].to_f if transaction[:sub_type].present? && transaction[:sub_type] != TypesEnumLib::TransactionSubType::VirtualTerminalTransaction
      if transaction.refund_log.nil?
        render status: 200 , json:{partial_amount: transaction[:total_amount].to_f,name: user_name,fee: fee}
      else
        if transaction.refund_log.present?
          if JSON.parse(transaction.refund_log).kind_of?(Array)
            temp = JSON.parse(transaction.refund_log)
            previousAmount = transaction[:total_amount].to_f - temp.sum{|h| h["amount"]}
          else
            temp = JSON.parse(transaction.refund_log)
            previousAmount = transaction[:total_amount].to_f - temp["amount"].to_f
          end
        else
          previousAmount = transaction[:total_amount]
        end
        render status: 200 , json:{partial_amount: previousAmount,name: user_name,fee: fee}
      end
    else
      render status: 200 , json:{ partial_amount: "" }
    end
  end

  def refund_transaction
    reason_detail = params[:reason_detail].present? ? params[:reason_detail] : "NA"
    # refund_fee = params[:refund_fee].present? ? params[:refund_fee].to_f : 0
    transaction = Transaction.where(id: params[:transaction]).first
    seq_transaction_id = transaction.seq_transaction_id
    t_amount = transaction.total_amount.to_f

    # merch_wall_id = transaction.receiver_wallet_id if transaction.present?
    # m_wallet = Wallet.find_by_id(merch_wall_id)
    merch = transaction.receiver
    if params[:partial_amount].present? && params[:partial_amount].to_f > 0 && params[:partial_amount].to_f < t_amount
      partial_amount = params[:partial_amount]
    end
    refund = RefundHelper::RefundTransaction.new(seq_transaction_id,transaction,merch,partial_amount,reason_detail, current_user)
    if transaction.main_type == TypesEnumLib::TransactionViewTypes::QrDebitCard
      refund_response = refund.refund_debit_issue
    else
      refund_response = refund.process
    end
    ## Activity Log saved - Temporary Solution.
    refund_params = {}
    refund_params[:seq_transaction_id] = transaction.id
    refund_params[:user] = @user
    refund_params[:type] = "admin"
    refund_params[:response] = refund_response
    record_refund_transaction(refund_params)
    if partial_amount.present?
      ActivityLog.log(nil,"Partial Refund", current_user, params, response,"Issued partial refund for [#{transaction.seq_transaction_id}] for [$#{partial_amount}]","accounts")
    else
      ActivityLog.log(nil,"Complete Refund", current_user, params, response,"Issued full refund for [#{transaction.seq_transaction_id}] for [$#{transaction.total_amount.to_f}]","accounts")

    end

    if refund_response[:success] == true
      flash[:success] = refund_response[:message]
    else
      flash[:error] = refund_response[:message]
    end

    return redirect_back(fallback_location: root_path)
  end


  def refund_failed_transaction
    begin
      refund_transaction = Transaction.includes(:sender_wallet,:sender).find_by_id(params[:transaction])
      parent_transaction = Transaction.parent_refund_transaction(refund_transaction)
      if  [I18n.t('types.transaction.VirtualTerminal'),I18n.t('types.transaction.Ecommerce'),I18n.t('types.transaction.QcpSecure'),I18n.t('types.transaction.QcSecure')].include? parent_transaction.try(:main_type)
        #issue amount to customer
        parent_wallet = parent_transaction.sender_wallet
        customer = parent_transaction.sender
        customer_transaction_db =  Transaction.create(to: parent_wallet.id,
                          from: nil,
                          status: "pending",
                          amount: params[:partial_amount],
                          receiver: customer,
                          receiver_name: customer.try(:name),
                          action: 'issue',
                          receiver_wallet_id: parent_wallet.id,
                          total_amount: params[:partial_amount],
                          net_amount: params[:partial_amount].to_f,
                          main_type: TypesEnumLib::TransactionType::RefundFailed,
                          receiver_balance: SequenceLib.balance(parent_wallet.id)
            )
        customer_transaction = SequenceLib.issue(parent_wallet.id,params[:partial_amount],nil,TypesEnumLib::TransactionType::RefundFailed, nil, nil,TypesEnumLib::GatewayType::Quickcard)
        if customer_transaction.present? && customer_transaction.id.present?
          customer_transaction_db.update(seq_transaction_id: customer_transaction.present? ? customer_transaction.actions.first.id : nil, tags: customer_transaction.actions.first.tags, status: "approved", timestamp: customer_transaction.timestamp)
          customer_parsed_transactions = parse_block_transactions(customer_transaction.actions, customer_transaction.timestamp)
          save_block_trans(customer_parsed_transactions) if customer_parsed_transactions.present?
          #tranfer amount to merchant from customer
          wallet = refund_transaction.sender_wallet
          merchant = refund_transaction.sender
          transaction_db =  Transaction.create(to: wallet.id,
                            from: parent_wallet.id,
                            status: "pending",
                            amount: params[:partial_amount],
                            receiver: merchant,
                            receiver_name: merchant.try(:name),
                            sender: customer,
                            sender_name: customer.try(:name),
                            action: 'issue',
                            receiver_wallet_id: wallet.id,
                            sender_wallet_id: parent_wallet.id,
                            total_amount: params[:partial_amount],
                            net_amount: params[:partial_amount].to_f,
                            main_type: TypesEnumLib::TransactionType::RefundFailed,
                            receiver_balance: SequenceLib.balance(wallet.id)
              )
          # transaction = SequenceLib.issue(wallet.id,params[:partial_amount],nil,TypesEnumLib::TransactionType::RefundFailed, nil, nil, nil)
          transaction = SequenceLib.transfer(number_with_precision(params[:partial_amount],precision:2),parent_wallet.id,wallet.id,TypesEnumLib::TransactionType::RefundFailed,0,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil)
          
          if transaction.present? && transaction.id.present?
            transaction.actions.first.tags["refund_failed_details"] = {"reason"=>params[:reason_detail],"parent_transaction_id"=>refund_transaction.seq_transaction_id,"parent_refund_transaction_id"=>refund_transaction.try(:tags).try(:[],"refund_details").try(:[],"parent_transaction_id")}
            transaction_db.update(seq_transaction_id: transaction.present? ? transaction.actions.first.id : nil, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
            parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
            save_block_trans(parsed_transactions) if parsed_transactions.present?
            flash[:success] = "Failed Refunded Successfully"
            return redirect_back(fallback_location: root_path)
          end  
        end 
      else
        wallet = refund_transaction.sender_wallet
        merchant = refund_transaction.sender
        transaction_db =  Transaction.create(to: wallet.id,
                          from: nil,
                          status: "pending",
                          amount: params[:partial_amount],
                          receiver: merchant,
                          receiver_name: merchant.try(:name),
                          action: 'issue',
                          receiver_wallet_id: wallet.id,
                          total_amount: params[:partial_amount],
                          net_amount: params[:partial_amount].to_f,
                          main_type: TypesEnumLib::TransactionType::RefundFailed,
                          receiver_balance: SequenceLib.balance(wallet.id)
            )
        transaction = SequenceLib.issue(wallet.id,params[:partial_amount],nil,TypesEnumLib::TransactionType::RefundFailed, nil, nil,TypesEnumLib::GatewayType::Quickcard)
        if transaction.present? && transaction.id.present?
          transaction.actions.first.tags["refund_failed_details"] = {"reason"=>params[:reason_detail],"parent_transaction_id"=>refund_transaction.seq_transaction_id,"parent_refund_transaction_id"=>refund_transaction.try(:tags).try(:[],"refund_details").try(:[],"parent_transaction_id")}
          transaction_db.update(seq_transaction_id: transaction.present? ? transaction.actions.first.id : nil, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
          parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          flash[:success] = "Failed Refunded Successfully"
          return redirect_back(fallback_location: root_path)
        end  
      end      
    rescue Exception => e
      flash[:error] = e.message
      return redirect_back(fallback_location: root_path)
    end
  end

  def refund_check
    trans_id = params[:trans_id]
    @transaction = Transaction.where(seq_transaction_id: trans_id).first
    respond_to do |format|
      format.js{
        render "admins/refund_status.js.erb"
      }
    end
  end
# ------------------------------Admin Refunds End---------------------------------
  def update_valid
    @app = OauthApp.find(params[:id])
    if params[:is_block] == "true"
      date =  current_user[:updated_at].strftime("%m-%d-%Y")
      merchant = @app.user
      locations = @app.user.get_locations if merchant.MERCHANT?
      if locations.present?
        locations.update_all(
            block_ip: true,
            virtual_transaction_api: true,
            virtual_terminal_transaction_api: true,
            virtual_debit_api: true,
            block_giftcard: true,
            block_withdrawal: true,
            disable_sales_transaction: true,
            sales: true,
            virtual_terminal: true,
            close_batch: true,
            block_api: true,
            block_ach: true,
            push_to_card: true,
            standalone: false,
            email_receipt: false,
            sms_receipt: false
        )
      end
    else
      merchant = @app.user
      locations = @app.user.get_locations if merchant.MERCHANT?
      if locations.present?
        locations.update_all(
            block_ip: false,
            virtual_transaction_api: false,
            virtual_terminal_transaction_api: false,
            virtual_debit_api: false,
            block_giftcard: false,
            block_withdrawal: false,
            disable_sales_transaction: false,
            sales: false,
            virtual_terminal: false,
            close_batch: false,
            block_api: false,
            block_ach: false,
            push_to_card: false,
            standalone: false,
            email_receipt: true,
            sms_receipt: true
        )
      end
    end
    @app.is_block = true? params[:is_block]
    @app.save(:validate => false)
    note=@app.is_block? ? "Inactivated" : 'Activated'
    save_activity(merchant,Time.now,current_user.id,note,'')
    flash[:notice] = params[:is_block] == "true" ? "Inactivated Successfully!" : "Activated Successfully!"
    if params[:is_block] == 'true'
      UserMailer.account_cancel(@app,date).deliver_later
    end
    return redirect_back(fallback_location: root_path)
  end

  def true?(obj)
    obj.to_s == "true"
  end
  
private

  def get_users(transaction)
    if transaction.present?
      users = {}
      if transaction[:fee_perc].present?
        # for old transactions users
        if transaction[:fee_perc]["partner_email"].present?
          partner = User.find_by(email: transaction[:fee_perc]["partner_email"])
          if partner.present?
            if partner.wallets.present?
              partner = partner.wallets.primary.first
              users = users.merge({partner: partner})
            end
          end
        end
        if transaction[:fee_perc]["iso_email"].present?
          iso = User.find_by(email: transaction[:fee_perc]["iso_email"])
          if iso.present?
            if iso.wallets.present?
              iso = iso.wallets.primary.first
              users = users.merge({iso: iso})
            end
          end
        end
        if transaction[:fee_perc]["agent_email"].present?
          agent = User.find_by(email: transaction[:fee_perc]["agent_email"])
          if agent.present?
            if agent.wallets.present?
              agent = agent.wallets.primary.first
              users = users.merge({agent: agent})
            end
          end
        end
      #   for old transacatoin user ends
        # # for new transacations
        if transaction[:fee_perc]["iso"].present?
          if transaction[:fee_perc]["iso"]["email"].present?
            if transaction[:fee_perc]["iso"]["wallet"].present?
              iso_wallet = Wallet.find_by(id: transaction[:fee_perc]["iso"]["wallet"])
              if iso_wallet.present?
                users = users.merge({iso: iso_wallet})
              end
            end
          end
        end
        if transaction[:fee_perc]["agent"].present?
          if transaction[:fee_perc]["agent"]["email"].present?
            if transaction[:fee_perc]["agent"]["wallet"].present?
              agent_wallet = Wallet.find_by(id: transaction[:fee_perc]["agent"]["wallet"])
              if agent_wallet.present?
                users = users.merge({agent: agent_wallet})
              end
            end
          end
        end
        if transaction[:fee_perc]["affiliate"].present?
          if transaction[:fee_perc]["affiliate"]["email"].present?
            if transaction[:fee_perc]["affiliate"]["wallet"].present?
              affiliate_wallet = Wallet.find_by(id: transaction[:fee_perc]["affiliate"]["wallet"])
              if affiliate_wallet.present?
                users = users.merge({affiliate: affiliate_wallet})
              end
            end
          end
        end
      end
      if transaction[:source_id].present? && (transaction[:source_id] != "Credit" && transaction[:source_id] != "Debit" && transaction[:source_id] != "Sales_issue")
        source_wallet = Wallet.find_by_id(transaction[:source_id])
        source = source_wallet.users.first if source_wallet.present?
        company = source.try(:company).try(:name)
      end
      if transaction[:destination].present?
        destination_wallet = Wallet.find_by_id(transaction[:destination])
        destination = destination_wallet.users.first if destination_wallet.present?
        company = destination.try(:company).try(:name)
      end
      users = users.merge({
          source: source,
          destination: destination,
          company: company,
          source_wallet: source_wallet.try(:id),
          destination_wallet: destination_wallet.try(:id)
                         })
      return users
    end
  end

  def save_my_previous_url
    # session[:previous_url] is a Rails built-in variable to save last url.
    if request.referer.present?
      if params['user_id'].present? && URI(request.referer).path == '/admins/merchants/search'
      cookies[:original_referrer]=request.referer
    elsif params['user_id'].present? && URI(request.referer).path == '/admins/merchants'
      cookies[:original_referrer]=request.referer
    else
      @back_url = request.referer
    end

    else
      @back_url = admins_path
    end
  end

  def check_route
    unless current_user&.try(:have_admin_access?)
      set_route
    end
  end

end
