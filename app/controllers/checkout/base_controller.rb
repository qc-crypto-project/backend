class Checkout::BaseController < ActionController::Base
  include Custom::ResponseHelper
  include Custom::TransactionHelper
  protect_from_forgery with: :null_session, except: [:rtp_payment, :rtp_payment_hook, :rtp_payment1]

  before_action :set_request_data

  def set_request_data
     RequestInfo.current_request(request, current_user || nil )
  end

end