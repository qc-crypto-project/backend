class Checkout::CheckoutController < Checkout::BaseController
  include RegistrationsHelper
  include Api::RegistrationsHelper
  include PosMerchantHelper
  include ApplicationHelper
  include TransactionCharge::ChargeATransactionHelper
  include Merchant::SalesHelper
  include CheckoutHandler
  include Api::CheckoutHelper
  layout 'checkout' , :except => [:documentation,:select_payment_options]
  ALGORITHM = 'HS256'
  require 'net/https'
  require 'uri'
  require 'json'

  def documentation
    render layout: "checkout_documentation"
  end


  def new
    begin
    decode_token = JWT.decode params[:token], Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM if params[:token].present?
    decode_token = decode_token.first
    @merchant_name = decode_token["merchant_name"]
    @merchant_website_url = decode_token["website_url"]
    @success_url = decode_token["success_url"]
    @fail_url = decode_token["fail_url"]
    @redirect_url = decode_token["redirect_url"]
    transaction_id = decode_token["checkout_id"]
    @transaction = Transaction.where(id: transaction_id).last
    if @transaction.status == "pending"
      @products = @transaction.products
      @card =  Card.new
      if @transaction.shipping_id.present?
        @shipping_address=Address.find(@transaction.shipping_id)
      end
      if @transaction.billing_id.present?
        @billing_address=Address.find(@transaction.billing_id)
      end
      @billing_states = CS.states(@billing_address.country) if @billing_address.present?
      @shipping_states = CS.states(@shipping_address.country) if @shipping_address.present?
      if decode_token["request_id"].present?
        @request = Request.where(id: decode_token["request_id"]).last
        render template: 'checkout/checkout/invoice_checkout'
      end
      if decode_token["api_checkout"].present?
        render template: 'checkout/checkout/api_checkout'
      end
    elsif @transaction.status == "canceled"
      redirect_to error_checkout_index_path(error: "Transaction is cancelled!")
    else
      redirect_to error_checkout_index_path(error: "Transaction already processed!")
    end


    rescue JWT::DecodeError => exc
     flash[:error] = "Your Session has expired"
     redirect_to root_path
    end
  end


  def get_card_information
    response = get_card_info(params[:card_number])
    render status: 200 , json:{:scheme => response.try(:[], "scheme"),:bank => response.try(:[], "bank").try(:[], "name")}
  end

  def select_payment_options
    @request = Request.find(params[:request_id])
    render template: 'checkout/select_payment_options'
  end

  def select_payment_options_post
    if params[:payment_option] == "ach"
      params[:intended_url] = ach_payment_merchant_invoices_path
      redirect_to ach_payment_merchant_invoices_path(intended_url:params[:intended_url])
    elsif params[:payment_option] == "quickcard"
      params[:intended_url] = quickcard_payment_merchant_invoices_path
      payload = {intended_url: params[:intended_url], invoice_id: params[:request_id], exp: (Time.zone.now + 60.minute).to_i}
      request_url = JWT.encode payload, Rails.application.secrets.secret_key_base, ALGORITHM
      redirect_to quickcard_payment_merchant_invoices_path(request_url: request_url)
    end
  end

  def checkout
    @request = Request.find(params[:request_id])
    @merchant = @request.sender
    @user =@request.reciever
    wallet_id = @request.wallet_id
    @location = Wallet.find(wallet_id).location
    website_url=JSON.parse(@location.web_site).first
    @products = @request.products
    if @request.tax_method == "percent"
      tax=(@request.tax / 100)* @request.amount.to_i
    else
      tax = @request.tax
    end

    param = {
        'auth_token' => @merchant.authentication_token,
        'shipping_name' => @user.name,
        'shipping_email' => @user.email,
        'shipping_phone_number' => @user.phone_number,
        'shipping_street' => @user.address,
        'shipping_city' => @user.city,
        'shipping_state' => @user.state,
        'shipping_country' => @user.country,
        'shipping_zip' => @user.zip_code,
        'billing_name' => params[:name],
        'billing_email' => @user.email,
        'billing_phone_number' => @user.phone_number,
        'billing_street' => params[:street],
        'billing_city' => params[:city],
        'billing_state' => params[:state],
        'billing_country' => params[:country],
        'billing_zip' => params[:zip],
        'website_url' => website_url,
        'success_url' => 'https://success-url.com/',
        'fail_url' => 'https://fail-url.com/',
        'order_id' => params[:request_id],
        'total_amount' => @request.total_amount,
        'total_tax' => tax,
        'location_id' => @location.location_secure_token,
        'request_id' => @request.id,
        'products' => @products
    }

    res=get_checkout_link(param)
    if res[:success]
      redirect_to new_checkout_path(token: res[:token])
    else
      render_json_response({:success => false, :status => res[:status], :message => res[:message]}, :ok)
    end


  end

  def invoice_rtp_checkout
    begin
      if params[:token].present?
        decode_token = JWT.decode params[:token], Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
        decode_token = decode_token.first
        params[:billing_id] = decode_token.try(:[],'billing_id')
        params[:request_id] = decode_token.try(:[],'request_id')
        params[:transaction_id] = decode_token.try(:[],'transaction_id')
        params[:merchant_id] = decode_token.try(:[],'merchant_id')
        params[:user_id] = decode_token.try(:[],'user_id')
        @success_url = decode_token.try(:[],'success_url')
        @fail_url = params[:request_id].present? ? request.url : decode_token.try(:[],'fail_url')

        @transaction = Transaction.where(status: "pending",id: params[:transaction_id]).first
        raise StandardError.new I18n.t('errors.checkout.no_pending_transaction') if @transaction.blank?
        @request = Request.in_progress.where(id: params[:request_id]).first if params[:request_id].present?
        @products = @request.try(:products)

        if @transaction.shipping_id.present?
          @shipping_address=Address.find(@transaction.shipping_id)
        end
        if @transaction.billing_id.present?
          @billing_address=Address.find(@transaction.billing_id)
        end
        @billing_states = CS.states(@billing_address.country) if @billing_address.present?
        @shipping_states = CS.states(@shipping_address.country) if @shipping_address.present?
        if params[:request_id].present?
          render template: 'checkout/checkout/invoice_rtp_checkout'
        else
          render template: 'checkout/checkout/rtp_checkout'
        end
      end
    rescue StandardError => exc
      flash[:success] = exc.message == I18n.t('errors.checkout.expired_signature') ? I18n.t('errors.checkout.due_date_passed') : exc.message
      redirect_to new_user_session_path(role: "merchant")
    end
  end

  def invoice_rtp_request
    begin
      if params[:request_url].present?
        decode_token = JWT.decode params[:request_url], Rails.application.secrets.secret_key_base, true, algorithm: ALGORITHM
        decode_token = decode_token.first
        params[:request_id] = decode_token.try(:[],'request_id')
        params[:billing] = JSON(params[:billing_address]) if params[:request_id].present?
        params[:shipping] = JSON(params[:shipping_address]) if params[:request_id].present?
        params[:transaction_id] = decode_token.try(:[],'transaction_id')

        @transaction = Transaction.find_by(id: params[:transaction_id])
        raise StandardError.new I18n.t('errors.checkout.no_pending_transaction') if @transaction.blank?
        @billing = update_billing(params[:billing]) if params[:billing].present?
        @shipping = update_billing(params[:shipping]) if params[:shipping].present?
        user = @transaction.sender
        merchant = @transaction.receiver

        if params[:request_id].present?
          @request = Request.in_progress.where(id: decode_token.try(:[],'request_id')).first
          raise StandardError.new I18n.t('errors.checkout.no_pending_invoice') if @request.blank?
          @products = @request.products
          item_name = ""#”2 x socks, 1 x shoes”
          quantity = 0
          @products.each do|p|
            item_name = item_name + "#{p.quantity} x #{p.name}, "
            quantity = quantity + p.quantity
          end
          first_name = params[:billing][:first_name]
          last_name = params[:billing][:last_name]
          name = params[:billing][:name].present? ? params[:billing][:name] : first_name == last_name ? first_name : "#{first_name} #{last_name}"
          email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
          user.update(first_name: first_name, last_name: last_name, name: name, email: email)
          @transaction.update(sender_name: user.name)
        else
          item_name = "1 x rtpapi  "
          quantity = 1
          user = update_user(user)
          @transaction.update(sender_name: user.name, sender_wallet_id: user.wallets.primary.first.id, sender_id: user.id)
        end
        custom_param = {authentication_token: merchant.authentication_token, success_url: decode_token["success_url"], fail_url: decode_token["fail_url"]}

        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: '50percent'
        url = "https://www.debitway.com/process/index.php"

        request_body = {
            "identifier" => "XJY0657627ny082",
            "website_unique_id" => "e1ye0MjBhIB9hTSd",
            "item_name" => item_name[0..-3],
            "quantity" => quantity.to_s,
            "amount" => number_with_precision(@transaction.total_amount, precision: 2),
            "currency" => "USD",
            "return_url" => rtp_payment_transactions_url,
            "language" => "en",
            "first_name" => @billing.try(:name) || @billing.try(:first_name),
            "last_name" => @billing.try(:name) || @billing.try(:last_name),
            "email" => @billing.try(:email),
            "phone" => @billing.try(:phone_number),
            "address" => @billing.street,
            "city"=> @billing.city.tr("0-9", ""),
            "state_or_province"=>@billing.state,
            "zip_or_postal_code"=>@billing.zip,
            "country"=>@billing.country,
            "merchant_transaction_id"=>"#{@transaction.id}_#{rand(100..999)}#{Time.now.to_i.to_s}",
            "item_code"=>"#{@request.try(:id)}",
            "custom"=>custom_param.to_json
        }
        if @shipping.present?
          request_body["shipping_address"] = @shipping.street
          request_body["shipping_city"] = @shipping.city.tr("0-9", "")
          request_body["shipping_state_or_province"] = @shipping.state
          request_body["shipping_zip_or_postal_code"] = @shipping.zip
          request_body["shipping_country"] = @shipping.country
        end

        SlackService.notify("Request:   #{request_body.to_json}", "#qc-files") unless request.host == 'localhost'

        @response = RestClient.post(url,request_body,{content_type: :json, accept: :json})
        if @response.body.include?("customer_errors_meaning")
          raise StandardError.new @response.body.partition('customer_errors_meaning').last.partition("<input").first.gsub("value=", "").gsub(">","")
        end
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'rtp_success', response_body: @response.body
      end
    rescue StandardError => exc
      ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'rtp_error',url: request.env['HTTP_REFERER'], message: exc.message
    end
  end

  def test_webhook
    response = {status: params["status"], success: params["success"], message: params["message"], quickcard_id: params["quickcard_id"], amount: params["amount"], date: params["date"]}
    SlackService.notify("WebHook: #{response}", "qc-files")
  end


  def update_billing(address)
    new_address = Address.where(id: address.try(:[],:id)).first_or_initialize
    new_address.name = address[:name]
    new_address.first_name = address[:first_name] if address[:first_name].present?
    new_address.last_name = address[:last_name] if address[:last_name].present?
    new_address.email = address[:email] if address[:email].present?
    new_address.phone_number = address[:phone_number] if address[:phone_number].present?
    new_address.street = address[:street]
    new_address.suit = address[:suit]
    new_address.city = address[:city]
    new_address.state = address[:state]
    new_address.country = address[:country]
    new_address.zip = address[:zip]
    new_address.save
    return new_address
  end

  def update_user(user)
    if params[:billing].present?
      first_name = params[:billing][:first_name]
      last_name = params[:billing][:last_name]
      name = params[:billing][:name].present? ? params[:billing][:name] : first_name == last_name ? first_name : "#{first_name} #{last_name}"
      email = params[:billing][:email].present? ? params[:billing][:email].strip.gsub(" ", "+") : ""
      user = User.user.find_by(phone_number: params[:billing][:phone_number])
      if user.present?
        user.update(email: email, first_name: first_name, last_name: last_name)
      else
        user = User.new
        user.name = name
        user.first_name = first_name
        user.last_name = last_name
        user.email = email
        user.phone_number = params[:billing][:phone_number]
        user.source = @transaction.receiver.try(:name)
        password = random_password
        user.password = password
        user.role = :user
        user.is_block = false
        user.archived = false
        user.regenerate_token
        user.save(:validate => false)
      end
    end
    return user
  end


end