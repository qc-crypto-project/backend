class Checkout::TransactionsController < Checkout::BaseController
  include RegistrationsHelper
  include Api::RegistrationsHelper
  include PosMerchantHelper
  include ApplicationHelper
  include TransactionCharge::ChargeATransactionHelper
  include Merchant::SalesHelper
  # include CheckoutHandler
  include LoadBalancerHandler
  include CardHandler
  layout "checkout", only: [:rtp_payment1, :rtp_payment]

  ALGORITHM = 'HS256'

  def status
    decode_token = params[:token].jwt_decode if params[:token].present?
    decode_token = decode_token.first
    transaction_id = decode_token["checkout_id"]
    transaction = Transaction.where(id: transaction_id).last
    transaction.update(status: "canceled") if transaction.present?

  end


  def payment
    # Decoding token
    @old_url = request.env["HTTP_REFERER"]
    decode_token(params[:token])
    if @request_id.present?
      @request = Request.in_progress.where(id: @request_id).first
      raise StandardError.new I18n.t('errors.checkout.no_pending_invoice') if @request.blank?
      @invoice_request = @request
    end
    @transaction = Transaction.where(id: @transaction_id).last
    @location=@transaction.receiver_wallet.try(:location)
    @user_id=@transaction.try(:sender_id)
    @billing_id = @transaction.try(:billing_id)


    if @transaction.status != TypesEnumLib::Statuses::Pending
      raise StandardError.new I18n.t('errors.checkout.duplicate')
    end
    if params[:billing_address].present? && params[:shipping_address].present?
      update_addresses(params[:billing_address],params[:shipping_address])
    end

    check_duplicate_transaction
    #Setting card data
    card_info
    if @location.apply_load_balancer?
      card_brand_type = "#{@card_bin_list["scheme"]}_#{@card_bin_list["type"] || 'credit'}".try(:downcase) if @card_bin_list.present? && @card_bin_list["scheme"].present?
      instantiate_load_balancer(nil,card_brand_type,@card_bin_list)
    else
      if @location.primary_gateway.present? && @location.primary_gateway.descriptor.present? && !@location.primary_gateway.descriptor.active?
        raise StandardError.new I18n.t("api.registrations.inactive")
      end
    end
    
    # Maxmind Risk Calculator START
    maxmind_minfraud = Payment::MaxmindMinfraud.new(request, params, @card, @transaction.sender,@transaction.shipping_id, @transaction.billing_id)
    saved_result = maxmind_minfraud.save_result_in_db(@transaction.id,@website_url) if maxmind_minfraud.minfraud_insights_response.present?
    if saved_result.present?
      if saved_result.qc_action == TypesEnumLib::RiskType::Reject
        ActivityLog.log(5,I18n.t('api.risk_score.high'),@merchant)
        raise TransactionError.new I18n.t('api.risk_score.high')
      end
    end
    # Maxmind Risk Calculator END

    #Transaction Process
    transaction=transaction_process
    if transaction.try(:[],:error).present?
      raise TransactionError.new transaction[:error]
    end
    @data = @seq_transaction_id if @seq_transaction_id.present?
    @data = @data.merge(get_card_info(@card_number)) if @data.present?
    @data.delete(:gateway_fee_details) if @data[:gateway_fee_details].present?
    user_balance = show_balance(@user_wallet_id)
    raise SaleValidationError.new I18n.t('api.registrations.balance') if user_balance.to_f < @transaction.total_amount.to_f
    # sender = @user
    # main_type = params[:main_type].present? ? TypesEnumLib::TransactionType::QCSecure : TypesEnumLib::TransactionType::Transfer3DS
    # transaction_between(to, from, amount, reference, fee, fee_type = nil, data = nil , ledger = nil, source = nil,sub_reference=nil,credit_check=nil,card=nil,tip_in_qr_sale=nil,trans_id=nil,payment_gateway=nil,card_type=nil, privacy_fee = nil,flag=nil,qr_scan_tx_amount=nil,load_fee=nil)
    main_type = @invoice_request.present? ? "Invoice - #{@card.card_type.present? ? @card.card_type.try(:capitalize) : @card_bin_list["type"].try(:capitalize) || "Credit"} Card" : @transaction.main_type
    gateway_name = @payment_gateway.name
    if @payment_gateway.present? &&  (@payment_gateway.knox_payments? || @payment_gateway.payment_technologies? || @payment_gateway.total_pay?)
      gateway_name = params[:bank_descriptor].present? ? params[:bank_descriptor] : gateway_name
    end
    current_transaction = transaction_between(@transaction.receiver_wallet_id,@transaction.sender_wallet_id,@transaction.total_amount, main_type,0, nil, @data,nil, gateway_name ,@transaction.sub_type ,@card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction, nil,nil,nil,@payment_gateway,nil, nil,nil)

    # transfer_action=transfer
    # if transfer_action.try(:[],:error).present?
    #   raise TransactionError.new transfer_action[:error]
    # end
    # #Transfer from user to merchant
    # main_type = params[:main_type].present? ? TypesEnumLib::TransactionType::QCSecure : TypesEnumLib::TransactionType::Transfer3DS
    # current_transaction = SequenceLib.transfer(@transaction.total_amount, @transaction.sender_wallet_id, @transaction.receiver_wallet_id, main_type, number_with_precision(@fee['fee'].to_f, precision: 2), nil, @payment_gateway.name, nil, @location.merchant, @location, @user_info, nil,get_ip, @reserve_detail, nil, @data,nil,nil,nil,nil,nil,@card,nil,nil,@payment_gateway,0,@card.card_type,@gateway_fees)
    # if current_transaction.blank?
    #   raise TransactionError.new I18n.t('errors.checkout.sequence')
    # end
    # if current_transaction.present?
    #   @transaction.update(
    #       status: "approved",
    #       website: @website_url,
    #       timestamp: current_transaction.timestamp,
    #       seq_transaction_id: current_transaction.actions.first.id,
    #       tags: current_transaction.actions.first.tags,
    #       created_at: current_transaction.timestamp
    #   )
    #   # Tracker.create(
    #   #     status: :new_order,
    #   #     user_id: @transaction.try(:sender_id),
    #   #     transaction_id: @transaction.id,
    #   #     location_id: @location.id
    #   # )
    #   parsed_transactions = parse_block_transactions(current_transaction.actions, current_transaction.timestamp)
    #   reserve_detail = @transaction.tags["fee_perc"].try(:[], "reserve")
    #   if reserve_detail.present? && reserve_detail.try(:[],"amount").to_f > 0
    #     reserve_tx = parsed_transactions.select{|e| e[:type] == "Reserve Money Deposit"}.try(:last)
    #     if reserve_tx.present?
    #       create_transaction_helper(@merchant, reserve_detail["wallet"], @transaction.receiver_wallet_id, @merchant, @merchant, "transfer", nil, nil,nil,nil, nil, reserve_detail.try(:[],"amount").to_f, reserve_detail.try(:[],"amount").to_f, nil, reserve_tx[:type], reserve_tx[:sub_type], reserve_tx[:tags], nil, nil,nil, get_ip, nil, "approved", reserve_tx[:id],reserve_tx[:timestamp])
    #     end
    #   end
    #   save_block_trans(parsed_transactions) if parsed_transactions.present?
    # current_transaction = transaction_between(wallet.id,reciever_wallet.id,params[:amount].to_f, TypesEnumLib::TransactionType::SaleType,0, nil, data,reciever.ledger, gateway,TypesEnumLib::TransactionSubType::SaleVirtualApi,card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction, nil,tip_detail,nil,@payment_gateway,nil, params[:privacy_fee],params[:flag])
    if current_transaction.blank?
      raise TransactionError.new I18n.t('errors.checkout.sequence')
    end
    if current_transaction.present?
        fee_perc = current_transaction.try(:[],:tags).try(:[],"fee_perc")
        maintain_batch(@merchant.id, @wallet.id.to_i, @transaction.total_amount,TypesEnumLib::Batch::Primary)


      if @merchant.high?
        if @location.iso.present?
          maintain_batch(@location.iso.id, @location.iso.wallets.first.id, @transaction.total_amount, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High,nil,nil,nil,nil,nil,nil,fee_perc.try(:[],"total_bonus"))
        end
        maintain_batch(@merchant.id, @wallet.id, @transaction.total_amount,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true,fee_perc.try(:[],"iso").try(:[], "amount"),fee_perc.try(:[],"agent").try(:[], "amount"),fee_perc.try(:[],"affiliate").try(:[], "amount"))
      elsif @merchant.low?
        if @location.iso.present?
          maintain_batch(@location.iso.id, @location.iso.wallets.first.id, @transaction.total_amount, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low,nil,nil,nil,nil,nil,nil,fee_perc.try(:[],"total_bonus"))
        end
        maintain_batch(@merchant.id, @wallet.id, @transaction.total_amount,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true,fee_perc.try(:[],"iso").try(:[], "amount"),fee_perc.try(:[],"agent").try(:[], "amount"),fee_perc.try(:[],"affiliate").try(:[], "amount"))
      end
      if @transaction.minfraud_result.present?
        if @transaction.minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview
          UserMailer.manual_review_3ds(@transaction.id).deliver_later
          allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
          txn_id = @transaction.try(:seq_transaction_id)[0..5]
          amount = number_with_precision(@transaction.try(:amount), precision: 2, delimiter: ',')
          text = I18n.t('notifications.risk_review', txn_id: txn_id, amount: amount, days: allowed_days)
          Notification.notify_user(@merchant,@merchant,@transaction,"manual review transaction",nil,text)
        end
      end

      message = sale_text_message(number_with_precision(@transaction.total_amount.to_f,precision:2), @location.business_name, gateway_name ,@card_number.last(4),@user.name,@location.phone_number)
      send_text_message(message, @user.phone_number) if @location.try(:sms_receipt) && message && @user.try(:phone_number).present? && params[:main_type].blank?
      trans_amount = number_to_currency(number_with_precision(@transaction.total_amount, precision: 2, delimiter: ','))
      # UserMailer.transaction_email(@location.id,trans_amount,(@payment_gateway.try(:name) || '---'),@user.id,Date.today.to_json,@card.brand,@card_number.last(4),current_transaction.actions.first.id, params[:action]).deliver_later if @location.try(:email_receipt)
      UserMailer.checkout_order_email(@user.id,@order_id,@location.id,@card.brand,@card_number.last(4),gateway_name,@transaction,@request).deliver_later if  params[:main_type].blank?
      UserMailer.checkout_order_email(@user.id,@order_id,@location.id,@card.brand,@card_number.last(4),gateway_name,@transaction).deliver_later if  params[:main_type] == TypesEnumLib::TransactionType::QCSecure
      # notification_text = I18n.t('notifications.checkout', dba_name: @location.business_name, amount: trans_amount )
      # Notification.notify_user(@user,@user,@transaction,"checkout",nil,notification_text)
      # User.user.send_reset_password_instructions(email: @user.email) if @user.old_passwords.blank?
      if @request_id.present?
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'invoice_done',url: @success_url,descriptor: gateway_name,last4: "****#{@card_number.last(4)}", card_brand:@card.brand, invoice_id: @request_id.present? ? "true" : ""
      else
        ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'done',url: @success_url,descriptor: gateway_name,last4: "****#{@card_number.last(4)}", card_brand:@card.brand
      end
      # record_it(4,"Successfully Done","3DS",merchant)
      params[:sequence_transaction_id] = @transaction.seq_transaction_id
      clean_parameters(params)
      @request.update(status: 'approved') if @request.present?
      ActivityLog.log(4,"Successfully Done",@merchant, params,response)
      # raise ActivityLogSuccess.new "Successfully Done"
    else
      raise TransactionError.new I18n.t('api.registrations.sequence')
      #record_it(7, I18n.t('api.registrations.sequence'), "3DS", merchant)
      raise ActivityLogFailure.new I18n.t('api.registrations.sequence')
    end

  end

  def rtp_payment
    sleep(2)
    SlackService.notify("From Redirect URL : #{params}", "#qc-files") unless request.host == 'localhost'
    params["custom"] = JSON(params["custom"])
    params["success_url"] = params["custom"]["success_url"]
    params["fail_url"] = params["custom"]["fail_url"]
    params["db_transaction_id"] = params[:merchant_transaction_id].split('_').first
    if params[:item_code].present? 
      @request = Request.where(id: params[:item_code]).first
        if @request.blank? || @request.pending? || @request.past_due? || @request.cancel?
          raise StandardError.new I18n.t('errors.checkout.no_pending_invoice')
        end
    end
    @transaction = Transaction.where(id: params["db_transaction_id"]).last
    if params[:item_code].blank?
      params["success_url"] = "#{params["success_url"]}?success=true&status=#{TypesEnumLib::Statuses::Approved}&message=#{I18n.t('errors.checkout.successfully_done')}&quickcard_id=#{@transaction.quickcard_id}&amount=#{params[:amount]}&date=#{@transaction.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)")}"
      params["fail_url"] = "#{params["fail_url"]}?success=false&status=#{TypesEnumLib::Statuses::Failed}&quickcard_id=#{@transaction.quickcard_id}&amount=#{params[:amount]}&date=#{@transaction.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)")}"
    end
    raise StandardError.new I18n.t('errors.checkout.failed_from_bank') if params["transaction_status"] == "declined"
    raise StandardError.new I18n.t('errors.checkout.no_transaction_found') if @transaction.blank?
    @location = @transaction.receiver_wallet.try(:location)
    raise StandardError.new I18n.t('errors.checkout.failed_from_bank') if params["transaction_status"] == "declined"

    # @authentication = User.find_by(authentication_token: params["custom"]["authentication_token"])
    # raise StandardError.new "Authentication Failed!" if @authentication.blank?
    if verify_transaction(params) == true
      return redirect_to rtp_payment1_transactions_path(activity_id: @activity.id, request_id: params[:item_code], success_url: params["success_url"], fail_url: params["fail_url"], location_name: @location.try(:business_name))
    end
    @transaction = Transaction.find_by(id: params["db_transaction_id"])
    if @transaction.try(:status) == "approved"
      return redirect_to rtp_payment1_transactions_path(activity_id: @activity.id, request_id: params[:item_code], success_url: params["success_url"], fail_url: params["fail_url"], location_name: @location.try(:business_name))
    end
    raise StandardError.new I18n.t('errors.checkout.no_pending_transaction') if @transaction.status != "pending"

    # approve_params = approve_debitWay_transaction(params[:transaction_id]) if params["transaction_status"] == "pending"

    @user_id = @transaction.try(:sender_id)
    @billing_id = @transaction.try(:billing_id)

    raise StandardError.new I18n.t('errors.checkout.duplicate') if @transaction.status != TypesEnumLib::Statuses::Pending

    # check_duplicate_transaction

    #Issue Transaction Process
    transaction=issue_transaction_process
    raise StandardError.new transaction[:error] if transaction.try(:[],:error).present?
    @data = @seq_transaction_id if @seq_transaction_id.present?
    user_balance = show_balance(@user_wallet_id)
    raise StandardError.new I18n.t('api.registrations.balance') if user_balance.to_f < @transaction.total_amount.to_f

    main_type = @request.present? ? TypesEnumLib::TransactionType::InvoiceRTP : TypesEnumLib::TransactionType::RTP
    check_credit = @request.present? ? "invoice_rtp" : "rtp"
    current_transaction = transaction_between(@transaction.receiver_wallet_id, @transaction.sender_wallet_id, @transaction.total_amount, main_type,0, nil, @data,nil, "QuickCard" ,@transaction.sub_type , check_credit, nil,nil,nil,nil,nil, nil,nil)
    if current_transaction.blank?
      raise StandardError.new I18n.t('errors.checkout.sequence')
    end
    fee_perc = current_transaction.try(:[],:tags).try(:[],"fee_perc")
    maintain_batch(@merchant.id, @wallet.id.to_i, @transaction.total_amount,TypesEnumLib::Batch::Primary)

    if @merchant.high?
      if @location.iso.present?
        maintain_batch(@location.iso.id, @location.iso.wallets.first.id, @transaction.total_amount, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High,nil,nil,nil,nil,nil,nil,fee_perc.try(:[],"total_bonus"))
      end
      maintain_batch(@merchant.id, @wallet.id, @transaction.total_amount,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true,fee_perc.try(:[],"iso").try(:[], "amount"),fee_perc.try(:[],"agent").try(:[], "amount"),fee_perc.try(:[],"affiliate").try(:[], "amount"))
    elsif @merchant.low?
      if @location.iso.present?
        maintain_batch(@location.iso.id, @location.iso.wallets.first.id, @transaction.total_amount, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low,nil,nil,nil,nil,nil,nil,fee_perc.try(:[],"total_bonus"))
      end
      maintain_batch(@merchant.id, @wallet.id, @transaction.total_amount,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true,fee_perc.try(:[],"iso").try(:[], "amount"),fee_perc.try(:[],"agent").try(:[], "amount"),fee_perc.try(:[],"affiliate").try(:[], "amount"))
    end
    if @transaction.minfraud_result.present?
      if @transaction.minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview
        UserMailer.manual_review_3ds(@transaction.id).deliver_later
        allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
        txn_id = @transaction.try(:seq_transaction_id)[0..5]
        amount = number_with_precision(@transaction.try(:amount), precision: 2, delimiter: ',')
        text = I18n.t('notifications.risk_review', txn_id: txn_id, amount: amount, days: allowed_days)
        Notification.notify_user(@merchant,@merchant,@transaction,"manual review transaction",nil,text)
      end
    end

    message = sale_text_message(number_with_precision(@transaction.total_amount.to_f,precision:2), @location.business_name, @payment_gateway.try(:name), @card_number.present? ? @card_number.try(:last,4) : "****",@user.name,@location.phone_number)
    send_text_message(message, @user.phone_number) if @location.try(:sms_receipt) && message && @user.try(:phone_number).present? && params[:main_type].blank?
    trans_amount = number_to_currency(number_with_precision(@transaction.total_amount, precision: 2, delimiter: ','))
    # UserMailer.checkout_order_email(@user.id,@order_id,@location.id,@card.brand,@card_number.last(4),@payment_gateway.try(:name),@transaction).deliver_later if  params[:main_type].blank?
    # UserMailer.checkout_order_email(@user.id,@order_id,@location.id,@card.brand,@card_number.last(4),@payment_gateway.try(:name),@transaction).deliver_later if  params[:main_type] == TypesEnumLib::TransactionType::QCSecure
    params[:sequence_transaction_id] = @transaction.seq_transaction_id
    clean_parameters(params)
    @request.update(status: 'approved') if @request.present?
    ActivityLog.log(4,I18n.t('errors.checkout.successfully_done'),@merchant, params,response,nil,nil,"approved", @activity)
    if params[:item_code].blank?
      @transaction.rtp_webhook({:success => true, :status => TypesEnumLib::Statuses::Approved, :message => I18n.t('errors.checkout.successfully_done'), quickcard_id: @transaction.quickcard_id, amount: params[:amount], date: @transaction.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)")})
    end
    @url = params["success_url"]
    SlackService.notify("RTP Success URL: #{@url}")
    # ActionCable.server.broadcast "checkout_channel_#{session[:session_id]}", channel_name: "checkout_channel_#{session[:session_id]}", action: 'rtp_payment_success', url: @url, location_name: @location.try(:business_name), not_request: @request.blank?
    render template: 'checkout/checkout/rtp_payment'
  end

  def rtp_payment1
    @activity = ActivityLog.find_by(id: params[:activity_id])
    if @activity.present?
      @request = params[:request_id]
      @error = @activity.respond_with.try(:[], "response") if @activity.response_status == "failed"
      @success = true if @activity.response_status == "approved"
      @status = @activity.response_status
      @location_name = params[:location_name]
      @url = @error.present? ? params[:fail_url] : @success.present? ? params[:success_url] : ""
      render template: 'checkout/checkout/rtp_payment'
    end
  end


  def rtp_payment_hook
    @hook = true
    SlackService.notify("From Hook : #{params}", "#qc-files") unless request.host == 'localhost'

    params["db_transaction_id"] = params[:merchant_transaction_id].split('_').first
    @transaction = Transaction.where(id: params["db_transaction_id"]).last
    raise StandardError.new I18n.t('errors.checkout.failed_from_bank') if params["transaction_status"] == "declined"

    raise StandardError.new I18n.t('errors.checkout.no_pending_transaction') if @transaction.blank? || @transaction.try(:status) == "approved"
    @location = @transaction.receiver_wallet.try(:location)

    return if verify_transaction(params) == true

    # approve_params = approve_debitWay_transaction(params[:transaction_id]) if params["transaction_status"] == "pending"

    if params[:item_code].present?
      @request = Request.in_progress.where(id: params[:item_code]).first
      raise StandardError.new I18n.t('errors.checkout.no_pending_invoice') if @request.blank?
    end
    @user_id = @transaction.try(:sender_id)
    @billing_id = @transaction.try(:billing_id)

    #Issue Transaction Process
    transaction=issue_transaction_process
    raise StandardError.new transaction[:error] if transaction.try(:[],:error).present?
    @data = @seq_transaction_id if @seq_transaction_id.present?
    user_balance = show_balance(@user_wallet_id)
    raise StandardError.new I18n.t('api.registrations.balance') if user_balance.to_f < @transaction.total_amount.to_f

    main_type = @request.present? ? TypesEnumLib::TransactionType::InvoiceRTP : TypesEnumLib::TransactionType::RTP
    check_credit = @request.present? ? "invoice_rtp" : "rtp"
    current_transaction = transaction_between(@transaction.receiver_wallet_id, @transaction.sender_wallet_id, @transaction.total_amount, main_type,0, nil, @data,nil, "QuickCard" ,@transaction.sub_type , check_credit, nil,nil,nil,nil,nil, nil,nil)

    if current_transaction.blank?
      raise StandardError.new I18n.t('errors.checkout.sequence')
    end
    fee_perc = current_transaction.try(:[],:tags).try(:[],"fee_perc")
    maintain_batch(@merchant.id, @wallet.id.to_i, @transaction.total_amount,TypesEnumLib::Batch::Primary)


    if @merchant.high?
      if @location.iso.present?
        maintain_batch(@location.iso.id, @location.iso.wallets.first.id, @transaction.total_amount, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High,nil,nil,nil,nil,nil,nil,fee_perc.try(:[],"total_bonus"))
      end
      maintain_batch(@merchant.id, @wallet.id, @transaction.total_amount,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true,fee_perc.try(:[],"iso").try(:[], "amount"),fee_perc.try(:[],"agent").try(:[], "amount"),fee_perc.try(:[],"affiliate").try(:[], "amount"))
    elsif @merchant.low?
      if @location.iso.present?
        maintain_batch(@location.iso.id, @location.iso.wallets.first.id, @transaction.total_amount, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low,nil,nil,nil,nil,nil,nil,fee_perc.try(:[],"total_bonus"))
      end
      maintain_batch(@merchant.id, @wallet.id, @transaction.total_amount,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true,fee_perc.try(:[],"iso").try(:[], "amount"),fee_perc.try(:[],"agent").try(:[], "amount"),fee_perc.try(:[],"affiliate").try(:[], "amount"))
    end
    if @transaction.minfraud_result.present?
      if @transaction.minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview
        UserMailer.manual_review_3ds(@transaction.id).deliver_later
        allowed_days = AppConfig.where(key: AppConfig::Key::ApproveManualReviewDays).first.try(:stringValue).try(:to_i) || 5
        txn_id = @transaction.try(:seq_transaction_id)[0..5]
        amount = number_with_precision(@transaction.try(:amount), precision: 2, delimiter: ',')
        text = I18n.t('notifications.risk_review', txn_id: txn_id, amount: amount, days: allowed_days)
        Notification.notify_user(@merchant,@merchant,@transaction,"manual review transaction",nil,text)
      end
    end

    message = sale_text_message(number_with_precision(@transaction.total_amount.to_f,precision:2), @location.business_name, @payment_gateway.try(:name), @card_number.present? ? @card_number.try(:last,4) : "****",@user.name,@location.phone_number)
    send_text_message(message, @user.phone_number) if @location.try(:sms_receipt) && message && @user.try(:phone_number).present? && params[:main_type].blank?
    trans_amount = number_to_currency(number_with_precision(@transaction.total_amount, precision: 2, delimiter: ','))
    # UserMailer.checkout_order_email(@user.id,@order_id,@location.id,@card.brand,@card_number.last(4),@payment_gateway.try(:name),@transaction).deliver_later if  params[:main_type].blank?
    # UserMailer.checkout_order_email(@user.id,@order_id,@location.id,@card.brand,@card_number.last(4),@payment_gateway.try(:name),@transaction).deliver_later if  params[:main_type] == TypesEnumLib::TransactionType::QCSecure
    params[:sequence_transaction_id] = @transaction.seq_transaction_id
    clean_parameters(params)
    @request.update(status: 'approved') if @request.present?
    a = ActivityLog.log(4,I18n.t('errors.checkout.successfully_done'),@merchant, params,response,nil,nil,"approved", @activity)
    if params[:item_code].blank?
      @transaction.rtp_webhook({:success => true, :status => TypesEnumLib::Statuses::Approved, :message => I18n.t('errors.checkout.successfully_done'), quickcard_id: @transaction.quickcard_id, amount: params[:amount], date: @transaction.created_at.to_datetime.in_time_zone("Pacific Time (US & Canada)")})
    end
  end

  private

  def check_duplicate_transaction
    catch_duplicate_time = ENV["DUPLICATE_TRANSACTION_TIME"] || 1000
    params_card_number = "#{(params[:cardNoSafe].to_s.first(4))}********#{(params[:cardNoSafe].to_s.last(4))}"
    activities = @transaction.receiver.activity_logs.where("params->'order_bank'->>'amount' = :amount AND created_at >= :time", time: Time.now - catch_duplicate_time.to_i.seconds, amount: @transaction.total_amount.to_s)
    duplicate = activities.detect {|activity| valid_log?(activity) && activity.params["cardNoSafe"].last(4) == params_card_number.last(4) && (activity.action == "payment" && activity.api_success?) }
    if duplicate.present?
      params[:duplicate_tx] = I18n.t('api.errors.duplicate_transaction')
      raise DuplicateTransactionError.new I18n.t('api.errors.duplicate_transaction')
    end
  end

  def valid_log?(activity)
    if activity.params["cardNoSafe"].present? && activity.params["order_bank"]["amount"].present?
      true
    else
      false
    end
  end

  def decode_token(token)
    decode_token = token.jwt_decode if token.present?
    decode_token = decode_token.first
    @transaction_id = decode_token["checkout_id"]
    @request_id = decode_token["request_id"]
    @website_url = decode_token["website_url"]
    @order_id = decode_token["order_id"]
    if decode_token["success_url"].include?('?')
      @success_url = "#{decode_token["success_url"]}&order_id=#{@order_id}"
    else
      @success_url = "#{decode_token["success_url"]}?order_id=#{@order_id}"
    end
    if decode_token["fail_url"].include?('?')
      @fail_url = "#{decode_token["fail_url"]}&order_id=#{@order_id}"
    else
      @fail_url = "#{decode_token["fail_url"]}?order_id=#{@order_id}"
    end
    if params[:api_checkout].present?
      address(params[:billing], "Billing")
    end

  end

  def address(address, type)
    new_address = Address.where(id: address[:id]).first_or_initialize
    new_address.name = "#{address[:first_name]} #{address[:last_name]}"
    new_address.email = address[:email]
    new_address.phone_number = address[:phone_number]
    new_address.street = address[:street]
    new_address.suit = address[:suit]
    new_address.city = address[:city]
    new_address.state = address[:state]
    new_address.country = address[:country]
    new_address.zip = address[:zip]
    new_address.address_type = type
    new_address.save
    return new_address
  end

  def card_info
    @card_number=params[:cardNoSafe].gsub(/\s+/, "")
    params[:card_number]=@card_number
    params[:card_holder_name]=params[:card][:name]
    params[:card_exp_date]=params[:card][:exp_date]
    bin_result=check_bin_list(@transaction&.receiver,@transaction&.receiver&.id,@user_id)
    @card=bin_result[:card]
    @card_bin_list=bin_result[:card_bin_list]
      @transaction.card_id = @card.id
      @transaction.save
  end

  def update_addresses(billing,shipping)
    if billing.present?
      address = JSON.parse(billing)
      billing_address = @transaction.try(:billing_address)
      billing_address.update(
          name: address["name"],
          email: address["email"],
          phone_number: address["phone_number"],
          street: address["street"],
          suit: address["suit"],
          city: address["city"],
          state: address["state"],
          country: address["country"],
          zip: address["zip"]
      )
      user = @transaction.try(:sender)
      user.update(email: address["email"])
    end

    if shipping.present?
      address = JSON.parse(shipping)
      shipping_address = @transaction.try(:shipping_address)
      shipping_address.update(
          street: address["street"],
          suit: address["suit"],
          city: address["city"],
          state: address["state"],
          country: address["country"],
          zip: address["zip"]
      )
    end
  end

  def verify_transaction(params)
    @activity = ActivityLog.where("params->>'merchant_transaction_id' = ? AND response_status IN (?)", params[:merchant_transaction_id], ["pending","approved"]).first
    return true if @activity.present?
    @activity = ActivityLog.log(@hook ? "rtp_hook" :"rtp", nil,nil,params,nil,nil,nil,"pending")
    return @activity
  end

  def approve_debitWay_transaction(transaction_id)

    url = "https://www.debitway.com/integration/index.php"

    request_body = {
        "identifier" => "XJY0657627ny082",
        "action" => "approve",
        "transaction_id" => transaction_id,
        "vericode"=> "Veriyg0neIHGez"
    }
    # if params["shipment_method"].present?
    #   request_body["shipment_method"] = params["shipping_method"]
    #   request_body["shipping_tracking_number"] = params["shipping_tracking_number"]
    #   request_body["shipping_date"] = params["shipping_date"]
    #   request_body["shipping_comments"] = params["shipping_comments"]
    # end

    @response = RestClient.post(url,request_body,{content_type: :json, accept: :json})
    if @response.body.include?("errors_meaning")
      raise StandardError.new @response.body.partition('errors_meaning').last[1..-1]
    end
  end
end