# frozen_string_literal: true

class Users::UnlocksController < DeviseController
  prepend_before_action :require_no_authentication
  skip_before_action :authenticate_user!


end