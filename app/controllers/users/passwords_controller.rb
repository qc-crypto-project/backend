# frozen_string_literal: true

class Users::PasswordsController < Devise::PasswordsController
  prepend_before_action :require_no_authentication
  skip_before_action :authenticate_user!
  # append_before_action :assert_reset_token_passed, only: :edit

  #reseting merchant password from admin side
  def from_admin_reset
    self.resource = resource_class.where.not(role: I18n.t("role.name.user")).send_reset_password_instructions(resource_params)
    if successfully_sent?(resource)
      render status: 200, json:{success: 'reset'}
    else
      render status: 404, json:{error: 'Error in reset'}
    end
  end

  def unlock_account
    if User.find_by(id: params[:id]).update(failed_attempts: 0,unlock_token: nil,locked_at:nil)
      flash[:notice] = "Unlock Successfully!"
    else
      flash[:notice] = "Link expired, Please contact to administrator"
    end
    redirect_to root_path
  end
  # GET /resource/password/new
  # def new
  # end

  # # POST /resource/password
  # def create
  # end

  # # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  # end

  # # PUT /resource/password
  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    if self.resource.blank? || self.resource.id.blank?
      reset_params = params[:user]
      self.resource = resource_class.where(reset_password_token: reset_params[:reset_password_token]).first
      if self.resource.present?
        if self.resource.persisted?
          if self.resource.reset_password_period_valid?
            self.resource.reset_password(reset_params[:password], reset_params[:password_confirmation])
          else
            self.resource.errors.add(:reset_password_token, :expired)
          end
        end
      end
    end
    yield resource if block_given?

    if resource.present? && resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      if Devise.sign_in_after_reset_password
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message!(:notice, flash_message)
        resource.after_database_authentication
        sign_in(resource_name, resource)
      else
        set_flash_message!(:notice, :updated_not_active)
      end
      respond_with resource, location: after_resetting_password_path_for(resource) 
    else
      set_minimum_password_length
      respond_with resource, location: edit_user_password_path(resource)
    end
  end
  
  def approve_request
    user = User.find_by(id: params[:id])
    user.update(merchant_ach_status: 2)
    flash[:notice] = "ACH Request approved!"
    return redirect_to root_path
  end

  def approve_request
    user = User.find_by(id: params[:id])
    user.update(merchant_ach_status: 'complete')
    flash[:notice] = "ACH Request approved!"
    redirect_back(fallback_location: root_path)
  end

  protected

  def after_resetting_password_path_for(resource)
    resource.forget_me!
    cookies.signed[TwoFactorAuthentication::REMEMBER_TFA_COOKIE_NAME] = {
              value: "#{resource.class}-#{resource.public_send(Devise.second_factor_resource_id)}",
              expires: 1.seconds.from_now
          }
    Devise.sign_in_after_reset_password ? after_sign_in_path_for(resource) : new_session_path(resource_name)
  end

end