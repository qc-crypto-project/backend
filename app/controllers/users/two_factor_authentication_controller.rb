require 'devise/version'

class Users::TwoFactorAuthenticationController < DeviseController
  prepend_before_action :authenticate_scope!
  before_action :prepare_and_validate, :handle_two_factor_authentication

  def show
    url = request.try(:env).try(:[],'HTTP_REFERER')
    if url.present?
      uri = URI::parse(url)
      @params = CGI::parse(uri.query) if uri.present? && uri.query.present?
    end
    if @params.try(:[],"role").present? && @params.try(:[],"role").try(:first) == "merchant"
      render template: 'users/two_factor_authentication/merchants/show'
    else
      # render template: 'users/two_factor_authentication/merchants/show'
      render partial: 'users/two_factor_authentication/merchants/partner_two_steps_verify'
    end
  end

  def update
    if params.try(:[],"role").present? && params.try(:[],"role") == "merchant"
      render template: 'users/two_factor_authentication/merchants/show' and return if params[:token].nil?
    else
      render template: 'users/two_factor_authentication/show' and return if params[:token].nil?
    end
    if resource.authenticate_otp(params[:token])
      if Time.now.utc > resource.direct_otp_sent_at + resource_class.direct_otp_valid_for
        flash[:notice]="The passcode you entered is expired, Click on Try Again to get a new code!"
        return redirect_to user_two_factor_authentication_path(role: params[:role])
        # redirect_to new_user_session_path , :notice => 'Your code is expired'
      end
      after_two_factor_success_for(resource)
    else
      after_two_factor_fail_for(resource)
    end
  end

  def handle_two_factor_authentication
    unless resource.two_step_verification
      after_two_factor_success_for(resource)
    end
  end

  private

  def after_two_factor_success_for(resource)
    set_remember_two_factor_cookie(resource)

    warden.session(resource_name)[TwoFactorAuthentication::NEED_AUTHENTICATION] = false
    # For compatability with devise versions below v4.2.0
    # https://github.com/plataformatec/devise/commit/2044fffa25d781fcbaf090e7728b48b65c854ccb
    if respond_to?(:bypass_sign_in)
      bypass_sign_in(resource, scope: resource_name)
    else
      sign_in(resource_name, resource, bypass: true)
    end
    set_flash_message :notice, :success
    resource.update_attribute(:second_factor_attempts_count, 0)

    redirect_to after_two_factor_success_path_for(resource)
  end

  def set_remember_two_factor_cookie(resource)
    resource.remember_me!
    expires_seconds = resource.class.remember_otp_session_for_seconds
    if expires_seconds && expires_seconds > 0
      cookies.signed[TwoFactorAuthentication::REMEMBER_TFA_COOKIE_NAME] = {
          value: "#{resource.class}-#{resource.public_send(Devise.second_factor_resource_id)}",
          expires: expires_seconds.seconds.from_now
      }
    end
  end

  def after_two_factor_success_path_for(resource)
    stored_location_for(resource_name) || :root
  end

  def after_two_factor_fail_for(resource)
    resource.second_factor_attempts_count += 1
    resource.save
    count = Devise.max_login_attempts - resource.second_factor_attempts_count
    flash[:notice] = "Passcode you entered do not match our records. You have #{count} more tries remaining."


    if resource.max_login_attempts?
      resource.second_factor_attempts_count = 0
      resource.save
      flash[:notice] = "Sorry You have Reached maximum limits of passcode"
      sign_out(resource)
      redirect_to user_session_path(role: params[:role])
    else
      render template: 'users/two_factor_authentication/merchants/show'
      # render :show
    end
  end

  def authenticate_scope!
    self.resource = send("current_#{resource_name}")
  end

  def prepare_and_validate
    redirect_to :root and return if resource.nil?
    @limit = resource.max_login_attempts
    if resource.max_login_attempts?
      resource.second_factor_attempts_count = 0
      resource.save
        flash[:notice] = "Sorry You have Reached maximum limits of passcode"
        sign_out(resource)
        redirect_to user_session_path(role: params[:role])
    end
  end
end
