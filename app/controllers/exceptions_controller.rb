class ExceptionsController < ActionController::Base
  layout false
  include ApplicationHelper

  def show
    @exception       = request.env['action_dispatch.exception']
    @status_code     = ActionDispatch::ExceptionWrapper.new(request.env, @exception).status_code
    @rescue_response = ActionDispatch::ExceptionWrapper.rescue_responses[@exception.class.name]
    details
    notify_app

    respond_to do |format|
      format.html { render :show, layout: nil }
      format.xml  { render xml: @details, root: "error", status: @status_code }
      format.json { render json: {error: @details}, status: @status_code }
    end
  end

  protected

  def details
    @details ||= {}.tap do |h|
      I18n.with_options scope: [:exception, :show, @rescue_response], exception_name: @exception.class.name, exception_message: @exception.message do |i18n|
        h[:name]    = i18n.t "#{@exception.class.name.underscore}.title", default: i18n.t(:title, default: @exception.class.name)
        h[:message] = i18n.t "#{@exception.class.name.underscore}.description", default: i18n.t(:description, default: @exception.message)
      end
    end
  end

  helper_method :details

  private

  def notify_app()
    clean_parameters(request.params)
    back_trace = @exception.backtrace.select { |x| x.match(Rails.application.class.parent_name.downcase)}.first(5)
    @user ||= current_user
    if @status_code == 422 || @status_code >= 500
      SlackService.notify("*#{@rescue_response.to_s.classify} - #{@status_code} - #{@exception.class.name}* \n App: \`\`\` #{{URL: request.base_url, HOST: request.host, ACTION: "#{params[:controller]}/#{params[:action]}"}.to_json} \`\`\` Error: \`\`\` #{@exception.to_json} \`\`\` Params: \`\`\`#{request.params.to_json}\`\`\` User: \`\`\`#{@user.present? ? {id: @user.id, email: @user.email, role: @user.role }.to_json : 'No User Present!'} \`\`\` BACKTRACE: \`\`\` #{back_trace.to_json} \`\`\`")
    end
    activity = ActivityLog.new
    activity.url = request.url
    activity.host = request.host
    activity.browser = request.env['HTTP_USER_AGENT']
    if request.remote_ip.present?
      activity.ip_address  = request.remote_ip
    else
      activity.ip_address = request.env['REMOTE_ADDR']
    end
    activity.params = request.params.except(:card_cvv).to_json
    activity.user = @user
    activity.respond_with = {success: false, code: @status_code, message: @details[:message], error: {
      type: @exception.class.name,
      message: @exception.message,
    },
    backtrace: back_trace
    }
    if @status_code > 500
      activity.server_error!
    elsif @status_code == 404
      activity.not_found!
    elsif @status_code > 400
      activity.bad_request!
    end
  end
end
