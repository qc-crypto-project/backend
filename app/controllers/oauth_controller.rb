class OauthController < ActionController::Base
  include ExceptionHandler

  before_action :authenticate_credentials, :only => [:token]
  after_action :record_it

  def revoke
    render_response('Bad Request', 400) && return if params[:token].blank?
    user = User.where(:authentication_token => params[:token]).first
    if user.present?
      user.regenerate_token
      user.save
      render_response({ :access_token => user.authentication_token }) && return
    end
    render_response('Unauthorized', 401) && return
  end

  def token
    if @location.present?
      render_response({ :access_token => @location.location_secure_token })
    else
      if params[:client_secret].present? && @app.present? && @app.secret === params[:client_secret]
        @app.user.regenerate_token
        @app.user.save
        if params[:location_secure_token].present? || params[:location_id].present?
          location_id=params[:location_secure_token] || params[:location_id]
          i_can_pay_3ds = false
          merchant_location=Location.find_by_location_secure_token(location_id)
          if merchant_location.present?
            i_can_pay_3ds = true if merchant_location.primary_gateway.present? && merchant_location.primary_gateway.type == TypesEnumLib::GatewayType::ICanPay
            render_response({
              access_token: @app.user.authentication_token,
              payment_gateway: "Quickcard",
              second_payment_gateway: "Quickcard",
              is_3ds: i_can_pay_3ds
            })
          else
            render_response({ access_token: @app.user.authentication_token })
          end
        else
          # primary_gateway = @app.user.primary_gateway || @app.user.get_locations().first.try(:primary_gateway)
          # secondary_gateway = @app.user.secondary_gateway || @app.user.get_locations().first.try(:secondary_gateway)
          response = {
            access_token: @app.user.try(:authentication_token),
            payment_gateway: "Quickcard",
            second_payment_gateway: "Quickcard"
          }
          render_response(response)
        end
        return
      else
        render_response('Unable to retrieve auth token! please check your keys', 401) && return
      end
    end
  end

  private

  def authenticate_credentials
    render_response('Bad Request', 400) && return unless params[:client_id].present?
    @app = OauthApp.includes(:user).where(:key => params[:client_id]).first
    render_response('Unable to retrieve auth token! please check your keys', 401) && return unless @app.present?
    if @app.is_block
      @app.user.regenerate_token
      @app.user.save!
      render_response('Un-authorized Credentials: Please contact to administrator for more information!', 401)
    else
      if params[:LOCATION_ID].present?
        @location= Location.where(location_secure_token:  params[:LOCATION_ID]).first
        render_response('Invalid Parameters: Wrong LOCATION_ID provided!', 401) && return unless @location.present?
        render_response('Un-authorized Location: Please contact to administrator for more information!', 401) && return if @location.is_block
      end
    end
  end

  def record_it
    if @app.blank? && params[:client_id].present? && params[:client_secret].present?
      @app = OauthApp.where(:key => params[:client_id]).first
    end
    ActivityLog.create!(
      :url => request.url,
      :host => request.host,
      :browser => request.env['HTTP_USER_AGENT'],
      :controller => controller_name,
      :action => action_name,
      :params => params.except(:card_cvv),
      :ip_address => (request.remote_ip.present? ? request.remote_ip : request.env['REMOTE_ADDR']),
      :user_id => (@app.present? ? @app.user_id : nil),
      :respond_with => @response
    )
  end

  def render_response(response, status = 200)
    if status != 200
      @response = {message: response, status: status}
      render plain: response, status: status
    else
      @response = response
      render json: response
    end
  end
end
