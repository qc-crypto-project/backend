class HomeController < ApplicationController

  	def index
    	set_route
  	end

    def set_route
	    sign_out :user unless user_signed_in?
	    if current_user.merchant? && current_user.try(:permission).try(:regular?)
	      redirect_to v2_merchant_sales_path
			elsif  current_user.merchant? && current_user.try(:permission).try(:custom?)
				# if current_user.try(:permission).wallet || current_user.try(:permission).refund
				#   redirect_to v2_merchant_accounts_path
				# elsif current_user.try(:permission).transfer
				#   redirect_to transfers_merchant_locations_path
				# elsif current_user.try(:permission).b2b
				#   redirect_to merchant_invoices_path
				# elsif current_user.try(:permission).virtual_terminal
				#   redirect_to virtual_terminal_merchant_sales_path
				# elsif current_user.try(:permission).dispute_view_only || current_user.try(:permission).dispute_submit_evidence || current_user.try(:permission).accept_dispute
				#   redirect_to merchant_disputes_path
				# elsif current_user.try(:permission).funding_schedule
				#   redirect_to merchant_deposites_path
				# elsif current_user.try(:permission).check
				#   redirect_to merchant_checks_path
				# elsif current_user.try(:permission).push_to_card
				#   redirect_to debit_card_deposit_merchant_checks_path
				# elsif current_user.try(:permission).ach
				#   redirect_to merchant_instant_ach_index_path
				# elsif current_user.try(:permission).gift_card
				#   redirect_to merchant_orders_merchant_giftcards_path
				# elsif current_user.try(:permission).sales_report
				#   redirect_to merchant_reports_path
				# elsif current_user.try(:permission).checks_report
				#   redirect_to checks_report_merchant_reports_path
				# elsif current_user.try(:permission).gift_card_report
				#   redirect_to gift_cards_report_merchant_reports_path
				# elsif current_user.try(:permission).user_view_only || current_user.try(:permission).user_edit || current_user.try(:permission).user_add
				#   redirect_to merchant_settings_path
				# elsif current_user.permission.try(:qr_redeem) || current_user.permission.try(:qr_scan) || current_user.permission.try(:qr_view_only)
				# 	redirect_to sale_requests_merchant_sales_path
				# elsif current_user.try(:permission).developer_app
				#   redirect_to merchant_apps_path
				# end

				if current_user.try(:permission).view_accounts || current_user.try(:permission).account_transfer || current_user.try(:permission).refund || current_user.try(:permission).tip_transfer || current_user.try(:permission).export_daily_batch
					redirect_to v2_merchant_accounts_path
				elsif current_user.try(:permission).achs || current_user.try(:permission).ach_view_only || current_user.try(:permission).ach_add || current_user.try(:permission).ach_void
					redirect_to v2_merchant_instant_ach_index_path
				elsif current_user.try(:permission).push_to_cards || current_user.try(:permission).push_to_card_view_only || current_user.try(:permission).push_to_card_add || current_user.try(:permission).push_to_card_void
					redirect_to v2_merchant_push_to_card_index_path
				elsif current_user.try(:permission).e_checks || current_user.try(:permission).check_view_only || current_user.try(:permission).check_void || current_user.try(:permission).check_add
					redirect_to v2_merchant_checks_path
				elsif current_user.try(:permission).giftcards || current_user.try(:permission).gift_card_view_only || current_user.try(:permission).gift_card_add || current_user.try(:permission).gift_card_void
					redirect_to v2_merchant_giftcards_path
				elsif current_user.try(:permission).chargebacks || current_user.try(:permission).view_chargeback_cases || current_user.try(:permission).fight_chargeback || current_user.try(:permission).accept_chargeback
					redirect_to v2_merchant_chargebacks_path
				elsif current_user.try(:permission).invoices || current_user.try(:permission).view_invoice || current_user.try(:permission).create_invoice || current_user.try(:permission).cancel_invoice
					redirect_to payee_v2_merchant_invoices_path
				elsif current_user.try(:permission).customer_list
					redirect_to clients_v2_merchant_invoices_path
				elsif current_user.try(:permission).virtual_terminal
					redirect_to v2_merchant_sales_path
				elsif current_user.try(:permission).employee_list || current_user.try(:permission).user_view_only  || current_user.try(:permission).user_edit || current_user.try(:permission).user_add
					redirect_to v2_merchant_settings_path
				elsif current_user.try(:permission).permission_list || current_user.try(:permission).permission_view_only  || current_user.try(:permission).permission_edit || current_user.try(:permission).permission_add
					redirect_to v2_merchant_permission_index_path
				elsif current_user.try(:permission).api_key
					redirect_to v2_merchant_apps_path
				elsif current_user.try(:permission).plugin
					redirect_to v2_merchant_plugins_path
				elsif current_user.try(:permission).fee_structure
					redirect_to v2_merchant_location_fees_path
				elsif current_user.try(:permission).help
					redirect_to get_help_v2_merchant_accounts_path
				elsif current_user.try(:permission).funding_schedule
					redirect_to v2_merchant_hold_money_index_path
				end
		elsif current_user.merchant? && current_user.try(:merchant_id).blank? 
	      	redirect_to v2_merchant_accounts_path
	    elsif current_user.partner? || current_user.agent? || current_user.iso? || current_user.affiliate? || current_user.qc? || current_user.affiliate_program?
	      redirect_to v2_partner_accounts_path
	    elsif current_user.admin?
	      redirect_to transactions_admins_path
	    elsif current_user.support? || current_user.support_mtrac?
	      redirect_to transactions_admins_path
	    elsif current_user.user?
	      redirect_to customers_orders_path
	    elsif current_user.role.blank?
	      sign_out :user
	    end
	 end
end
