class Merchant::ReportsController < Merchant::BaseController
  require 'write_xlsx'
  include ApplicationHelper
  include Merchant::ChecksHelper
  include ActionView::Helpers::NumberHelper
  include Merchant::ReportsHelper
  skip_before_action :set_wallet_balance, only: [:get_wallets]

  SEQ_FAILURE_MSG = "Server is busy right now. Please try again later!"
  REPORT_ERROR = "Report search failed! Try updating your report search criteria."
  #report for sales
  def index
    begin
      @heading = "Sales Report"
      @wallet = []
      if current_user.merchant_id.present? && current_user.try(:permission).admin?
        # @locations = User.find(@user.merchant_id).get_locations
        @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id))
      else
        locations_ids = @user.attached_locations
        locations = Location.where(id: locations_ids)
        if locations.present?
          @locations = @user.get_locations + locations
        else
          @locations = @user.get_locations
        end
      end
      @count = 0
      if current_user.merchant_id.present? && current_user.submerchant_type == "admin_user"
        @wallet = User.find(@user.merchant_id).wallets.compact.uniq
      else
        @wallet = @user.wallets.compact.uniq
      end
      if params[:query].present?
        if !params[:q].present? && params[:query][:date].present?
          date = parse_reports_date(params[:query][:date])
        else
          date = parse_reports_date(params[:query])
        end
        user_wallet = Wallet.find(params[:wallet_id])
        @primary_wallet = user_wallet.primary? ? true : false
        @reserve_wallet = user_wallet.reserve? ? true : false
        @tip_wallet = user_wallet.tip? ? true : false
        if user_wallet.tip?
          @user_wallet = user_wallet.name + " - " + user_wallet.location.business_name
        else
          @user_wallet = user_wallet.name
        end
        if @reserve_wallet || @tip_wallet
          if @reserve_wallet
            @reports = BlockTransaction.where(receiver_wallet_id: params[:wallet_id], main_type:[TypesEnumLib::TransactionViewTypes::ReserveMoneyDeposit], timestamp: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).per_page_kaminari(params[:page]).per(10)
          elsif @tip_wallet
            @reports = BlockTransaction.where(receiver_wallet_id: params[:wallet_id], main_type:[TypesEnumLib::TransactionViewTypes::SaleTip], timestamp: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).per_page_kaminari(params[:page]).per(10)
          end
        else
          if params[:status] == "Approved"
            @reports = Transaction.where(receiver_wallet_id: params[:wallet_id], main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce, TypesEnumLib::TransactionViewTypes::VirtualTerminal, TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::PinDebit], created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc, status: ["approved", "refunded","complete"]).per_page_kaminari(params[:page]).per(10)
          elsif params[:status] == "Decline"
            @reports = Transaction.where(receiver_wallet_id: params[:wallet_id], main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce, TypesEnumLib::TransactionViewTypes::VirtualTerminal, TypesEnumLib::TransactionViewTypes::CreditCard,TypesEnumLib::TransactionViewTypes::PinDebit], created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc, status: ["pending"]).per_page_kaminari(params[:page]).per(10)
          end
        end
        if @primary_wallet
          @totals = @reports.pluck("
          SUM(transactions.total_amount) AS amount,
          SUM(CAST(COALESCE(transactions.amount, '0') AS DECIMAL)) AS tx_amount,
          SUM(transactions.privacy_fee) AS privacy_fee,
          SUM(transactions.tip) AS marks,
          SUM(transactions.fee) AS fee,
          SUM(transactions.net_fee) AS net_fee,
          SUM(transactions.net_amount) AS total_net,
          SUM(transactions.reserve_money) AS reserve
        ")
        else
          @totals = @reports.pluck("
            SUM(block_transactions.amount_in_cents) AS amount,
            SUM(block_transactions.tx_amount) AS tx_amount,
            SUM(block_transactions.privacy_fee) AS privacy_fee,
            SUM(block_transactions.tip_cents) AS marks,
            SUM(block_transactions.fee_in_cents) AS fee,
            SUM(block_transactions.net_fee) AS net_fee,
            SUM(block_transactions.total_net) AS total_net,
            SUM(block_transactions.hold_money_cents) AS reserve
          ")
        end
        p "@totals@totals", @totals
        if @reports.present? && @reports.count > 0
          if @primary_wallet
            @reports = @reports.order(created_at: :desc)
          else
            @reports = @reports.order(timestamp: :desc)
          end
          @count = @reports.total_count
        end
      end

      if params[:q].present?
        params_value = params[:q]
        if params[:q].strip.to_i.to_s == params[:q].strip
          params[:q] = { sender_wallet_id_or_receiver_wallet_id_or_last4_or_total_amount_or_privacy_fee_or_tip_or_fee_or_net_fee_or_reserve_money_or_terminal_transaction_eq: params[:q].strip }
        else
          params[:q] = { amount_or_main_type_or_card_card_type_or_card_brand_cont: params[:q].strip }
        end
        @q = @reports.ransack(params[:q].try(:merge, m: 'or'))
        @reports = @q.result().per_page_kaminari(params[:page]).per(10)
        params[:q] = params_value
      end
      
      respond_to do |format|
        format.js
        format.html
      end
    rescue => exc
      flash[:notice] = "Report search failed! Try updating your report search criteria."
      redirect_back(fallback_location: root_path)
    end
  end

  def get_wallets
    location = Location.find_by(id: params[:id])
    if location.present?
      wallets = location.wallets.order(wallet_type: :asc)
    end
    respond_to do |format|
      format.html
      format.json { render json: wallets }
    end
  end

  def my_reports
    begin
      if @user.partner?
        @wallets = @user.wallets.primary
        users = User.where(company_id: @user.company.id).uniq
        merchants = []
        users.each do |u|
          if u.merchant?
            merchants << u.id
          end
        end
        @merchants = User.find(merchants)
      elsif @user.qc?
        @merchants = User.merchant.where(merchant_id: nil)
      else
        location = @user.locations.pluck(:id).uniq
        @wallets = Wallet.where(location_id: location).primary
        merchants =  @wallets.map{|v| v.location.merchant}.pluck(:id).uniq
        @merchants = User.find(merchants)
      end
      @role = @user.role
      if params[:query].present?
        if  params[:wallet_ids].present?
          date = parse_reports_date(params[:query][:date])
          transax= []
          @reports=[]
          @new={}
          wallets_ids = []
          if @user.qc?
            current_wallet = @user.wallets.first.id
          else
            current_wallet = @user.wallets.primary.first.id
          end
          User.where(id: params[:wallet_ids]).each do |merchant|
            merchant.wallets.primary.each do |wallet|
              wallets_ids << wallet.id
            end
            merchant=merchant.name
            if @new[merchant]
              @new[merchant] = [{:total_transactions => 0, :total_recieved => 0, :total_processed_amount=> 0}]
            else
              @new[merchant] = [{:total_transactions => 0, :total_recieved => 0, :total_processed_amount=> 0}]
            end
          end

          if current_user.iso?
            @reports = BlockTransaction.where("tags->'fee_perc'->'iso'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc)
          elsif current_user.agent?
            @reports = BlockTransaction.where("tags->'fee_perc'->'agent'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc)
          elsif current_user.qc?
            @reports = BlockTransaction.where("tags->'fee_perc'->'gbox'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc)
          elsif current_user.affiliate?
            @reports = BlockTransaction.where("tags->'fee_perc'->'affiliate'->'wallet' = ?", "#{current_wallet}").where(receiver_wallet_id: wallets_ids,main_type:[TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::CreditCard, TypesEnumLib::TransactionViewTypes::PinDebit], timestamp: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc)
          end

          if @reports.blank?
            flash[:notice] = SEQ_FAILURE_MSG
            return redirect_back(fallback_location: root_path())
          end

          unless @reports.nil?
            @total_amount= 0
            @total_trx= 0
            @total_processed_amount= 0
            @txs=@reports
            @count = @txs.count
            tx={}
            @txs.each do |t|
              ref = t.tags
              fee_perc = ref["fee_perc"]
              if current_user.iso?
                # Avoid those which does not belongs to the user
                if current_user.email == fee_perc["iso_email"] || ( fee_perc["iso"].present? && (current_user.email == fee_perc["iso"]["email"] || current_wallet == fee_perc["iso"].try(:[], "wallet") ))
                  iso_fee = 0
                  if fee_perc["iso_fee"].present?
                    if fee_perc["iso_total_fee"].present?
                      iso_fee = fee_perc["iso_total_fee"].to_f
                    else
                      iso_fee = fee_perc["iso_fee"].to_f
                    end
                  elsif fee_perc["iso_total_fee"].present?
                    iso_fee = fee_perc["iso_total_fee"].to_f
                  elsif fee_perc["iso"].try(:[],"amount").present?
                    iso_fee = fee_perc["iso"]["amount"].to_f
                  end
                  merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
                  # merchant=Wallet.find(t[:destination]).location.merchant.name
                  if @new.key?(merchant)
                    iso_transaction = @new[merchant][0][:total_transactions] + 1
                    iso_recieved = @new[merchant][0][:total_recieved].to_f + iso_fee.to_f
                    iso_profit = @new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
                    @new[merchant]=[{:total_transactions => iso_transaction , :total_recieved => iso_recieved, :total_processed_amount => iso_profit}]
                  end
                end
              elsif current_user.partner?
                # Avoid those which does not belongs to the user
                if current_user.email == fee_perc["partner_email"] || ( fee_perc["partner"].present? && (current_user.email == fee_perc["partner"]["email"] || current_wallet == fee_perc["partner"].try(:[], "wallet") ))
                  partner_fee = 0
                  if fee_perc["partner_fee"].present?
                    if fee_perc["partner_total_fee"].present?
                      partner_fee = fee_perc["partner_total_fee"].to_f
                    else
                      partner_fee = fee_perc["partner_fee"].to_f
                    end
                  elsif fee_perc["partner_total_fee"].present?
                    partner_fee = fee_perc["partner_total_fee"].to_f
                  elsif fee_perc["partner"]["amount"].present?
                    partner_fee = fee_perc["partner"]["amount"].to_f
                  end
                  merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
                  if @new.key?(merchant)
                    total_transaction = @new[merchant][0][:total_transactions] + 1
                    total_recieved = @new[merchant][0][:total_recieved].to_f + partner_fee.to_f
                    total_profit = @new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
                    @new[merchant]=[{:total_transactions => total_transaction , :total_recieved => total_recieved, :total_processed_amount => total_profit}]
                  end
                end

              elsif current_user.qc?
                # Avoid those which does not belongs to the user
                # if current_user.email == fee_perc["partner_email"] || ( fee_perc["partner"].present? && current_user.email == fee_perc["partner"]["email"])
                gbox_fee = 0
                if fee_perc["gbox_fee"].present?
                  if fee_perc["gbox_total_fee"].present?
                    gbox_fee = fee_perc["gbox_total_fee"].to_f
                  else
                    gbox_fee = fee_perc["gbox_fee"].to_f
                  end
                elsif fee_perc["gbox_total_fee"].present?
                  gbox_fee = fee_perc["gbox_total_fee"].to_f
                elsif fee_perc["gbox"]["amount"].present?
                  gbox_fee = fee_perc["gbox"]["amount"].to_f
                end
                merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
                if @new.key?(merchant)
                  total_transaction = @new[merchant][0][:total_transactions] + 1
                  total_recieved = @new[merchant][0][:total_recieved].to_f + gbox_fee.to_f
                  total_profit = @new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
                  @new[merchant]=[{:total_transactions => total_transaction , :total_recieved => total_recieved, :total_processed_amount => total_profit}]
                end
                # end

              elsif current_user.agent?
                # Avoid those which does not belongs to the user
                if current_user.email == fee_perc["agent_email"] || ( fee_perc["agent"].present? && (current_user.email == fee_perc["agent"]["email"] || current_wallet == fee_perc["agent"].try(:[], "wallet") ))
                  agent_fee = 0
                  if fee_perc["agent_fee"].present?
                    if fee_perc["agent_total_fee"].present?
                      agent_fee = fee_perc["agent_total_fee"].to_f
                    else
                      agent_fee = fee_perc["agent_fee"].to_f
                    end
                  elsif fee_perc["agent_total_fee"].present?
                    agent_fee = fee_perc["agent_total_fee"].to_f
                  elsif fee_perc["agent"]["amount"].present?
                    agent_fee = fee_perc["agent"]["amount"].to_f
                  end
                  merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
                  if @new.key?(merchant)
                    agent_transaction = @new[merchant][0][:total_transactions] + 1
                    agent_recieved = @new[merchant][0][:total_recieved].to_f + agent_fee.to_f
                    agent_profit = @new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
                    @new[merchant]=[{:total_transactions => agent_transaction , :total_recieved => agent_recieved, :total_processed_amount => agent_profit}]
                  end
                end
              elsif current_user.affiliate?
                # Avoid those which does not belongs to the user
                if current_user.email == fee_perc["affiliate_email"] || ( fee_perc["affiliate"].present? && (current_user.email == fee_perc["affiliate"]["email"] || current_wallet == fee_perc["affiliate"].try(:[], "wallet") ))
                  affiliate_fee = 0
                  if fee_perc["affiliate_fee"].present?
                    if fee_perc["affiliate_total_fee"].present?
                      affiliate_fee = fee_perc["affiliate_total_fee"].to_f
                    else
                      affiliate_fee = fee_perc["affiliate_fee"].to_f
                    end
                  elsif fee_perc["affiliate_total_fee"].present?
                    affiliate_fee = fee_perc["affiliate_total_fee"].to_f
                  elsif fee_perc["affiliate"]["amount"].present?
                    affiliate_fee = fee_perc["affiliate"]["amount"].to_f
                  end
                  merchant = ref.try(:[], "merchant").try(:[], "name") || Wallet.find(t.receiver_wallet_id).location.merchant.name
                  if @new.key?(merchant)
                    affiliate_transaction = @new[merchant][0][:total_transactions] + 1
                    affiliate_recieved = @new[merchant][0][:total_recieved].to_f + affiliate_fee.to_f
                    affiliate_profit = @new[merchant][0][:total_processed_amount].to_f + SequenceLib.dollars(t.amount_in_cents).to_f
                    @new[merchant]=[{:total_transactions => affiliate_transaction , :total_recieved => affiliate_recieved, :total_processed_amount => affiliate_profit}]
                  end
                end
              end
            end
            @new.map{|v|v.last}.flatten.each do |ss|
              unless ss.nil?
                @total_amount += ss[:total_recieved]
                @total_trx += ss[:total_transactions]
                @total_processed_amount = @total_processed_amount.to_f + ss[:total_processed_amount]
              end
            end
          end
        else
          flash[:error] = I18n.t 'merchant.controller.sel_last_merchant'
          redirect_back(fallback_location:  root_path)
        end
      end
    rescue Sequence::APIError => exc
      flash[:notice] = SEQ_FAILURE_MSG
      redirect_back(fallback_location: root_path)
    rescue StandardError => exc
      redirect_back(fallback_location: root_path)
    end
  end

  def checks_report
    @heading = "Checks Report"
    @wallet=Array.new
    if current_user.merchant_id.present?
      current_user.wallets.where.not(location_id: nil).primary.each do |w|
        @wallet << w
      end
      if User.find(@user.merchant_id).wallets.qc_support.present?
        @wallet << @user.wallets.qc_support.first
      end
    else
      @user.wallets.primary.each do |w|
        @wallet << w
      end
      if @user.wallets.qc_support.present?
        @wallet << @user.wallets.qc_support.first
      end
    end

    if params[:query].present?
      date = parse_reports_date(params[:query][:date])
      @user_wallet = Wallet.find(params[:wallet_id])
      reports = Transaction.where(sender_wallet_id: params[:wallet_id], main_type: [TypesEnumLib::TransactionViewTypes::SendCheck,TypesEnumLib::TransactionViewTypes::BulkCheck], created_at: date[:first_date]..date[:second_date])
      if reports.present?
        if params[:query]["send_via"] == "all"
          @reports = reports.order(created_at: :desc)
        elsif params[:query]["send_via"] == "direct_deposit"
          @reports = reports.select{|v| v.tags["send_via"] == "Direct Deposit" }.sort_by{|e| e[:created_at]}
        elsif params[:query]["send_via"] == "email"
          @reports = reports.select{|v| v.tags["send_via"] == "email" }.sort_by{|e| e[:created_at]}
        end
        @count = @reports
        @total_fee = @reports.pluck(:fee)
        @total_fee = @total_fee.inject(0){|sum,x| sum + x.to_f}
        total_amount = @reports.pluck(:total_amount)
        @total_amount = total_amount.inject(0){|sum,x| sum + x.to_f}
        total_net = @reports.pluck(:net_amount)
        @total_after_fee = total_net.inject(0){|sum,x| sum.to_f + x.to_f}
      else
        flash[:notice] = I18n.t 'merchant.controller.notice_check_notfound'
      end
    end
  end

  def send_money_report
    @heading = "Send Money Report"
    @wallet=Array.new
    @user.wallets.primary.each do |w|
      @wallet << w
    end
    if params[:query].present?
      date = parse_reports_date(params[:query][:date])
      @user_wallet = Wallet.find(params[:wallet_id])
      reports = all_transactions_with_time(params[:wallet_id], date[:first_date], date[:second_date], 'p2p_payment').to_a
      if reports.blank?
        flash[:notice] = SEQ_FAILURE_MSG
        return redirect_back(fallback_location: root_path())
      end
      if params[:query][:date][0..9] == params[:query][:date][13..22]
        @reports = reports.select{|v| v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") == params[:query][:date][0..9] && v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") == params[:query][:date][13..22]}
      else
        @reports = reports.select{|v| v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") >= params[:query][:date][0..9] && v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") <= params[:query][:date][13..22]}
      end
      @count = @reports
      total_amount = @reports.select{|v| v[:new_type] != "Fee"}.pluck(:amount)
      @total_amount = total_amount.inject(0){|sum,x| sum+x}
    end
  end

  def close_batch_report
    @heading = "Close Batch Report"
    @wallet=Array.new
    if current_user.merchant_id.present? && current_user.submerchant_type == "admin_user"
      User.find(@user.merchant_id).wallets.primary.each do |w|
        @wallet << w
      end
    else
      @user.wallets.primary.each do |w|
        @wallet << w
      end
    end
    if params[:query].present?
      date = parse_date_without_time(params[:query][:date])
      @user_wallet = Wallet.find(params[:wallet_id])
      close_batch = Batch.where(wallet_id: params[:wallet_id], user_id: @user.id,is_closed: true,  batch_type: 'primary')
      if close_batch.present?
        @close_batch = close_batch.select{|v| v.close_date.to_s >= date[:first_date].to_s && v.close_date.to_s  <= date[:second_date].to_s}
        total_transactions = @close_batch.pluck(:total_transactions)
        @total_transactions = total_transactions.inject(0){|sum,x| sum+x}
        total_sales = @close_batch.pluck(:total_sales)
        @total_sales = total_sales.inject(0){|sum,x| sum+x}
      end
    end
  end

  def gift_cards_report
    @heading = "Gift Card Reports"
    @wallet=Array.new
    if current_user.merchant_id.present?
      current_user.wallets.where.not(location_id: nil).primary.each do |w|
        @wallet << w
      end
    else
      @user.wallets.primary.each do |w|
        @wallet << w
      end
    end
    if @user.wallets.qc_support.present?
      @wallet << @user.wallets.qc_support.first
    end
    if params[:query].present?
      date = parse_date_without_time(params[:query][:date])
      @user_wallet = Wallet.find(params[:wallet_id])
      if current_user.merchant_id.present? && current_user.submerchant_type == "admin_user"
        giftcards = TangoOrder.where(user_id: current_user.merchant_id,wallet_id: params[:wallet_id], account_identifier: "quickcard")
      else
        giftcards = TangoOrder.where(user_id: @user.id,wallet_id: params[:wallet_id], account_identifier: "quickcard")
      end
      if params[:query][:date][0..9] == params[:query][:date][13..22]
        @giftcards = giftcards.select{|v| v.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") == params[:query][:date][0..9] && v.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") == params[:query][:date][13..22]}
      else
        @giftcards = giftcards.select{|v| v.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") >= params[:query][:date][0..9] && v.created_at.in_time_zone(cookies[:timezone]).strftime("%m/%d/%Y") <= params[:query][:date][13..22]}
      end
      total_amount = @giftcards.pluck(:amount)
      @total_amount = total_amount.compact.inject(0){|sum,x| sum+x}
      total_fee = @giftcards.pluck(:gift_card_fee)
      @total_fee = total_fee.compact.inject(0){|sum,x| sum+x}
      @total_with_fee = @total_fee + @total_amount
    end
  end

  private

  def parse_reports_date(date)
    offset = DateTime.now.in_time_zone(cookies[:timezone]).utc_offset
    first_date = date[0..9]
    first_date = Date.strptime(first_date, '%m/%d/%Y')
    firstdate = Time.new(first_date.year,first_date.month,first_date.day,00,00,00, offset).utc
    firsttime = firstdate.strftime("%H:%M:%S.00Z")
    firstdate = firstdate.strftime("%Y-%m-%dT#{firsttime}")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, '%m/%d/%Y')
    seconddate = Time.new(second_date.year,second_date.month,second_date.day,23,59,59, offset).utc
    secondtime = seconddate.strftime("%H:%M:%S.59Z")
    seconddate = seconddate.strftime("%Y-%m-%dT#{secondtime}")
    return {first_date: firstdate, second_date: seconddate}
  end

  def parse_date_without_time(date)
    first_date = date[0..9]
    first_date = Date.strptime(first_date, "%m/%d/%Y")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, "%m/%d/%Y")
    return {first_date: first_date, second_date: second_date}
  end
end
