class Merchant::TransactionsController < Merchant::BaseController
  include ApplicationHelper
  include Merchant::ChecksHelper
  include Merchant::TransactionsHelper
  include FluidPayHelper
  include Zipline
  skip_before_action :require_merchant, only: [:get_merchant_amount, :refund_check]
  skip_before_action :set_wallet_balance, only: [:previouss_transactions,:next_transactions,:details_modal, :get_merchant_amount, :refund_check, :refund_merchant_transaction, :generate_export_file, :export_download]
  before_action :validate_location , only: [:transfer_amount]
  before_action :is_merchant?, only: [:index]
  skip_before_action :verify_tos_acceptance, only: [:index]
  include Api::RegistrationsHelper

  def index

    if params[:query].present?
      params[:query][:wallet_ids]= params[:query][:wallet_ids].kind_of?(Array)? params[:query][:wallet_ids] : params[:query][:wallet_ids].present? ? JSON.parse(params[:query][:wallet_ids]) : ""
      params[:query]["DBA_name"] = params[:query]["DBA_name"].kind_of?(Array)? params[:query]["DBA_name"] : params[:query]["DBA_name"].present? ? JSON.parse(params[:query]["DBA_name"]) : ""
      params[:query]["type"] =  params[:query]["type"].kind_of?(Array)?  params[:query]["type"] :  params[:query]["type"].present? ? JSON.parse(params[:query]["type"]) : ""
      params[:query]["type1"] =  params[:query]["type1"].kind_of?(Array)?  params[:query]["type1"] : params[:query]["type1"].present? ? JSON.parse(params[:query]["type1"]) : ""
    end

    @filters = [10,25,50,100]
    @heading = "Transactions"
    per_page = params[:tx_filter] || 10
    if @user.agent? || @user.iso? || @user.affiliate?
      location_ids = @user.baked_users.pluck(:location_id)
      locations = @user.locations
      baked_locations = Location.where(id: location_ids)
      locations = locations + baked_locations
      @merchants = locations.map{|e| e.merchant}.compact.uniq if locations.present?
      @locations=locations.map{|g|g.business_name}.compact.uniq if locations.present?
    end

    if @user.merchant?
      locations_ids = @user.attached_locations
      locations = Location.where(id: locations_ids)
      ids = []
      ids << @user.wallets.primary.pluck(:id)
      if locations.present?
        locations.each do |location|
          ids << location.try(:wallets).try(:primary).try(:first).try(:id)
        end
      end
      @wallets = ids.flatten.uniq
      if params[:query].present?
        @transactions = searching_local_transaction(params[:query], @wallets).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
      else
        @transactions = Transaction.where(sender_wallet_id: @wallets,status: ["approved", "refunded", "complete"]).or(Transaction.where(to: @wallet.try(:id),status: ["CBK Won", "Chargeback", "cbk_fee"])).or(Transaction.where(from: @wallet.try(:id),status: ["CBK Won", "Chargeback", "cbk_fee"])).or(Transaction.where(receiver_wallet_id: @wallets,status: ["approved", "refunded", "complete"])).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
      end
    else
      @wallets = @user.try(:wallets).try(:primary).pluck(:id)
      if params[:query].present?
        @transactions = searching_iso_local_transaction(params[:query], @wallets,nil,nil,nil,nil,nil,nil).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
        # @transactions = searching_local_transaction(params[:query], @wallets).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
      else
        @transactions = BlockTransaction.where("receiver_wallet_id IN (:wallets) OR sender_wallet_id IN (:wallets)", wallets: @wallets).where.not(sub_type:["instant_pay","ACH_deposit","send_check","cbk_hold_fee"]).order(id: :desc).per_page_kaminari(params[:page]).per(per_page)
                            # .where.not(main_type: ["Fee Transfer"],sub_type:["instant_pay","ACH_deposit","send_check"])
      end
    end
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    @types=[{value:"Ach",view:"ACH"},{value:"ACH Deposit",view:"ACH Deposit"},{value:"Account Transfer",view:"Account Transfer"},{value:"B2B Transfer",view:"B2B"}, {value: "Buy rate fee", view: "Buy Rate Fee"},{value:"Fee Transfer",view:"Fee Transfer"},{value:"Gift Card Purchase",view:"Gift Card"},{value:"Instant Pay",view:"Push to Card"},{value:"Send Check",view:"Send Check"},{value:"Void Ach",view:"Void ACH"},{value:"Void Push To Card",view:"Void P2C"},{value:"Void Check",view:"Void Check"},{value:"Failed check",view:"Failed Check"},{value:"Failed ACH",view:"Failed ACH"},{value:"Failed Push To Card",view:"Failed Push To Card"},{value:"Misc fee",view:"Misc Fee"},{value:"Subscription fee",view:"Subscription Fee"}]
    @types1=[{value:"ACH_deposit",view:"ACH Deposit"},{value:"cbk_fee",view:"CBK Fee"},{value: "credit_card", view: "Credit Card"},{value: "virtual_terminal_sale_api", view: "eCommerce"},{value:"Gift Card Purchase",view:"Gift Card"},{value: "Monthly Fee", view: "Monthly Fee"},{value: "debit_charge", view: "PIN Debit"},{value:"instant_pay",view:"Push to Card"},{value:"send_check",view:"Send Check"},{value:"virtual_terminal_sale",view:"Virtual Terminal"},{value:"Failed Check",view:"Failed Check"},{value:"Failed ACH",view:"Failed ACH"},{value:"Failed Push To Card",view:"Failed Push To Card"},{value:"Misc Fee",view:"Misc Fee"},{value:"Subscription Fee",view:"Subscription Fee"}]
    respond_to do |format|
      if @user.iso? || @user.agent? || @user.affiliate? || @user.partner? || @user.qc?
        format.html{render :local_iso_agent_view}
      else
        format.html{render :local_list}
      end
    end
  end

  def next_transactions
    @seq_dashboard_transaction = JSON.parse(params[:seq_dashboard_transaction])
    list = []
    transactions = SequenceLib.next_page(params[:cursor])
    if transactions.present? && @user.merchant?
      list.push(parse_merchants_transactions(transactions, nil)) # parsing transactions for showing
    elsif transactions.present?
      list.push(parse_company_transactions(@user.wallets.first.id,transactions.try(:[], :items))) # parsing transactions for showing
    end
    @transactions = list.flatten.compact.sort_by{|i| i[:timestamp]}.reverse
    @cursor = transactions.try(:[],:cursor)
    @last_page = transactions.try(:[],:last_page)
    @page_records = params[:page_size].to_i
    @transactions_count = @page_records + @transactions.count
    @seq_dashboard_transaction << {transactions: @transactions, cursor: @cursor, last_page: @last_page, transactions_count: @transactions_count, page_records: @page_records}
    respond_to :js
  end

  def previouss_transactions
    @seq_dashboard_transaction = JSON.parse(params[:seq_dashboard_transaction])
    @transactions = []
    @cursor = nil
    @last_page = nil
    @page_records = 0
    @transactions_count = 0
    if @seq_dashboard_transaction.present?
      @seq_dashboard_transaction.pop()
      transactions = @seq_dashboard_transaction.last["transactions"]
      transactions.each do |transaction|
        @transactions << transaction.symbolize_keys
      end
      @cursor = @seq_dashboard_transaction.last["cursor"]
      @last_page = @seq_dashboard_transaction.last["last_page"]
      @page_records= @seq_dashboard_transaction.last["page_records"].to_i
      @transactions_count = @seq_dashboard_transaction.last["transactions_count"].to_i
    end
    respond_to :js
  end

  def getting_transaction_from_seq(params)
    if @user.MERCHANT? && @user.merchant_id.nil?
      #= Merchant
      locations_ids = @user.attached_locations
      locations = Location.where(id: locations_ids)
      ids = []
      ids << @user.wallets.primary.pluck(:id)
      if locations.present?
        locations.each do |location|
          ids << location.try(:wallets).try(:primary).try(:first).try(:id)
        end
      end
      ids = ids.flatten.uniq
      if params[:query].present?
        if params[:from].present? && params[:from] == "hold_money"
          result = searching_transaction(params[:query], [params[:wallet_id]])
        else
          result = searching_transaction(params[:query], ids)
        end
        transactions = []
        unless result.nil?
          transactions.push(parse_merchants_transactions(result, nil)) # parsing transactions for showing
        end
        transactions = transactions.flatten.compact
        @transactions = transactions.select{|v| v[:type] != "Sale Issue"}.sort_by{|i| i[:timestamp]}.reverse
        @cursor = result[:cursor] if result.present?
        last_page = result[:last_page] if result.present?
      else
        transactions = all_merchant_transactions(ids, '', @user.ledger).to_a
        if transactions.present?
          @cursor = transactions.second
          @transactions = transactions.first.select{|v| v[:type] != "Sale Issue"}.sort_by{|i| i[:timestamp]}.reverse
          last_page = transactions.third
        end
      end
    elsif @user.MERCHANT? && !@user.merchant_id.nil? && @user.try(:permission).admin?
      #= Sub merchant with admin_user
      parent_merchant = User.find(@user.merchant_id)
      if params[:query].present?
        result = searching_transaction(params[:query], parent_merchant.wallets.primary.collect{|v|v.id})
        transactions = []
        unless result.nil?
          transactions.push(parse_merchants_transactions(result, nil)) # parsing transactions for showing
        end
        transactions = transactions.flatten.compact
        @transactions = transactions.sort_by{|i| i[:timestamp]}.reverse
        @cursor = result[:cursor] if result.present?
        last_page = result[:last_page] if result.present?
      else
        transactions = all_merchant_transactions(parent_merchant.wallets.primary.collect{|v|v.id}, '', @user.ledger).to_a
        if transactions.present?
          @transactions = transactions.first.select{|v| v[:type] != "Sale Issue"}.sort_by{|i| i[:timestamp]}.reverse
          @cursor = transactions.second
          last_page = transactions.third
        end
      end
    elsif @user.MERCHANT? && @user.retail_sales == false && @user.merchant_id.present?
      #= Sub merchant with regular_user
      #= getting transactions for sub merchant regular user from sequence
      @wallets_transactions = Array.new
      locations = @user.locations
      locations.each do |wallet|
        @wallets_transactions << wallet
      end
      if params[:query].present?
        result = searching_transaction(params[:query], @wallets_transactions.collect{|v|v.id})
        transactions = []
        unless result.nil?
          transactions.push(parse_merchants_transactions(result, nil)) # parsing transactions for showing
        end
        transactions = transactions.flatten.compact
        @transactions = transactions.sort_by{|i| i[:timestamp]}.reverse
        @cursor = result[:cursor] if result.present?
        last_page = result[:last_page] if result.present?
      else
        transactions = all_merchant_transactions(@wallets_transactions.collect{|v|v.id}, '').to_a
        if transactions.present?
          @transactions = transactions.first.sort_by{|i| i[:timestamp]}.reverse
          @cursor = transactions.second
          last_page = transactions.third
        end
      end
    else
      #= ISO, AGENT, Company, Aff
      user_wallet = @user.wallets.first.id
      qc_wallet = Wallet.qc_support.first.id

      if params[:query].present?
        result = searching_transaction(params[:query], user_wallet)
        all_transactions = []
        if result.present?
          all_transactions.push(parse_company_transactions(user_wallet,result[:items]))
        end
        @cursor = result[:cursor] if result.present?
        last_page = result[:last_page] if result.present?
      else
        all_transactions = all_company_transactions(user_wallet, user_wallet, 0 ).to_a
        @cursor = all_transactions.second
        last_page = all_transactions.third
      end
      if all_transactions.first.present?
        if user_wallet != qc_wallet
          transactions = all_transactions.first.select{|v| v[:destination].to_i != qc_wallet}.to_a
          @transactions = transactions.sort_by{|i|i[:timestamp]}.reverse
        else
          @transactions = all_transactions.first.sort_by{|i|i[:timestamp]}.reverse
        end
      end
    end
    return {:transactions => @transactions, :cursor => @cursor ,:last_page => last_page}
  end

  def searching_local_transaction(params, wallets = nil)
    name_wallets = []
    amount = nil
    search_wallets = []
    conditions = []
    parameters = []
    if @user.merchant?
      if params[:name].present?
        name_users = User.where("name ILIKE :name",  name: "%#{params[:name]}%")
        if name_users.present?
          name_users.each do |u|
            if u.wallets.present?
              if u.wallets.primary.first.present?
                # name_wallets << u.wallets.primary.pluck(:id)
                name_wallets << u.wallets.primary.first.id
              end
            end
          end
        end
      end
      name_wallets = name_wallets.uniq
      search_wallets << name_wallets.flatten
      if params[:email].present?
        email_user = User.where(email: params[:email]).first
        email_wallets = email_user.wallets.primary.pluck(:id) if email_user.present?
        search_wallets << email_wallets.flatten.uniq if email_wallets.present?
      end

      if params[:phone_number].present?
        phone_user = User.where(phone_number: params[:phone_number]).first
        phone_wallets = phone_user.wallets.primary.pluck(:id) if phone_user.present?
        search_wallets << phone_wallets.flatten.uniq if phone_wallets.present?
      end
    else

      if params[:name].present?
        conditions << "lower(tags->'merchant'->>'name') LIKE ?"
        parameters << "%#{params[:name].try(:downcase)}%"
      end
      if params[:Receiver_name].present?
        params[:Receiver_name]=params[:Receiver_name].strip
        conditions <<  "lower(users.name) LIKE ?"
        parameters << "%#{params[:Receiver_name].try(:downcase)}%"
      end
      # if params[:DBA_name].present?
      #   params[:DBA_name]=params[:DBA_name]
      #   conditions << "lower(tags->'location'->>'business_name') LIKE ?"
      #   parameters << "%#{params[:DBA_name].try(:downcase)}%"
      # end
      dba_name = ''
      if params[:DBA_name].present?
        if params[:DBA_name].kind_of?(Array)
          dba_name = params[:DBA_name]
        else
          dba_name = JSON.parse(params[:DBA_name])
        end
      end
      if dba_name.present?
            conditions<< "tags->'location'->>'business_name' IN (?) "
            parameters<< dba_name
      end
    end
    if params[:amount].present?
      if @user.merchant?
        conditions << "amount::VARCHAR LIKE ?"
        parameters << number_with_precision(params[:amount], precision: 1)
      else
        new_var=params[:amount]
        new_var=number_with_precision(params[:amount], precision: 2)
        var=new_var.to_s.split('.').last
        storing_param=new_var
        if var=="00"
          storing_param=new_var
          storing_param=number_with_precision(storing_param, precision: 1)
          end
        conditions << "tags->'main_transaction_info'->>'amount'::VARCHAR LIKE ?"
        parameters << "#{storing_param}%".to_s
      end
    end
    search_wallets = search_wallets.flatten.uniq if search_wallets.present?
    if search_wallets.present?
      conditions << "sender_wallet_id IN (?)"
      parameters << search_wallets
      conditions << "OR receiver_wallet_id IN (?)"
      parameters << search_wallets
    end
    # time = parse_time(params)
    # date = params[:date].present? ? parse_daterange(params[:date]) : nil
    # date = parse_daterange(params[:datie]) if params[:datie].present? && date.blank?
    # if date.present?
    #   date = {
    #       first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).to_datetime.rfc3339,
    #       second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).to_datetime.rfc3339
    #   }
    #    date[:first_date] = "#{date[:first_date].first(11)}#{time.first}#{date[:first_date].last(9)}"
    #    date[:second_date] = "#{date[:second_date].first(11)}#{time.second}#{date[:second_date].last(9)}"
    # elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
    #   date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
    #   date = {
    #       first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).to_datetime.rfc3339,
    #       second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).to_datetime.rfc3339
    #   }
    #   # date[:first_date] = "#{date[:first_date].first(11)}#{time.first}#{date[:first_date].last(9)}"
    #   # date[:second_date] = "#{date[:second_date].first(11)}#{time.second}#{date[:second_date].last(9)}"
    # end
    #
    if params[:offset].present?
      unless params[:offset].include?(':')
        params[:offset].insert(3,':')
      end
    end
    time = parse_time(params)
    date = params[:date].present? ? parse_daterange(params[:date]) : nil
    date = parse_daterange(params[:datie]) if params[:datie].present? && date.blank?
    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    elsif params[:time1].present? || params[:time2].present?  #= if date not selected we'll use today's date
      date = parse_date("#{Date.today.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),00,00, params[:offset]).utc
      }
    end
    if date.present? && date.try(:[],:first_date).present?
      conditions << "block_transactions.timestamp >= ?"
      parameters << date[:first_date]
    end
    if date.present? && date.try(:[],:second_date).present?
      conditions << "block_transactions.timestamp <= ?"
      parameters << date[:second_date]
    end
    type = ''
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if type.present?
      conditions << "(main_type IN(?) OR tags ->> 'sub_type' IN(?) OR sub_type IN (?))"
      parameters << type
      parameters << type
      parameters << type
    end
    name = ''
    if params[:wallet_ids].present?
      if params[:wallet_ids].kind_of?(Array)
        name = params[:wallet_ids]
        # name = [name.join(" OR "), *parameters]
      else
        name = JSON.parse(params[:wallet_ids])
      end
    end
    if name.present?
      # if name.length==1
        conditions<< "tags->'merchant'->>'id' IN (?) "
        parameters<< name
      # else
      #   name.each do |name|
      #     conditions<< "tags->'merchant'->>'id' = ? "
      #     conditions=[conditions.join(" OR ")]
      #     parameters<< name
        end
      # end
    # end

    last4 = params[:last4].present? ? params[:last4] : nil
    if last4.present?
      conditions << "last4 LIKE ?"
      parameters << "%#{last4}%"
    end
    conditions = [conditions.join(" AND "), *parameters]
    # #= removing AND from search wallets query
    if search_wallets.present?
      conditions.first.slice!(24)
      conditions.first.slice!(24)
      conditions.first.slice!(24)
    end
    # # #= insert user wallets into conditions
    # query_a = " AND receiver_wallet_id IN (?) OR sender_wallet_id IN (?)"
    # query_a = " AND receiver_wallet_id IN (?) OR sender_wallet_id IN (?)"
    # conditions.first << query_a
    # conditions << wallets
    # conditions << wallets
    # else
    #   nil
    # end
    if @user.merchant?
      t = Transaction.where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets)
      t.where.not(main_type: "Sale Issue").where(status: ["approved", "refunded", "complete"]).where(conditions)
    elsif params[:Receiver_name].present?
      t = BlockTransaction.joins(:receiver).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets)
      t.where(conditions) if t.present?
    else
      t = BlockTransaction.where(conditions).where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets)
      # t.where(conditions) if t.present?
    end
  end

  def searching_transaction(params, wallets)
    name_wallets = []
    amount = nil
    if params[:name].present?
      name_users = User.where("name ILIKE :name",  name: "%#{params[:name]}%")
      if name_users.present?
        name_users.each do |u|
          if u.wallets.present?
            if u.wallets.primary.first.present?
              # name_wallets << u.wallets.primary.pluck(:id)
              name_wallets << u.wallets.primary.first.id
            end
          end
        end
      end
    end
    name_wallets = name_wallets.uniq
    if params[:email].present?
      email_user = User.where(email: params[:email]).first
      email_wallets = email_user.wallets.primary.pluck(:id) if email_user.present?
      email_wallet = email_wallets.first if email_wallets.present?
    end

    if params[:phone_number].present?
      phone_user = User.where(phone_number: params[:phone_number]).first
      phone_wallets = phone_user.wallets.primary.pluck(:id) if phone_user.present?
      phone_wallet = phone_wallets.first if phone_wallets.present?
    end
    if params[:amount].present?
      amount = SequenceLib.cents(params[:amount])
    end
    date = params[:date].present? ? parse_daterange(params[:date]) : nil
    if date.present?
      time_params={time1:params[:time1],time2:params[:time2]}
      time = parse_time(time_params)
      # params[:offset]="+05:00"
      unless params[:offset].include?(':')
        params[:offset].insert(3,':')
      end
      puts "Offset is ======" , params[:offset]
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).to_datetime.rfc3339,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),59,59, params[:offset]).to_datetime.rfc3339
      }
      puts "date of time" , date
    end
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if params[:block_id].present?
      block_id=params[:block_id]
      block_id  = Transaction.where("seq_transaction_id ILIKE :block_id", block_id: "#{params[:block_id]}%").try(:first).try(:seq_transaction_id) if params[:block_id].present?
    end
    last4 = params[:last4].present? ? params[:last4] : nil

    transactions = SequenceLib.transactions_merchant_search([wallets].flatten,name_wallets,email_wallet,phone_wallet,amount,date,last4,nil,type,block_id)
    return transactions
  end

  # ----------------------- Generate export file start -----------------------#
  def generate_export_file
    if params[:search_query].present?
      if params[:search_query].try(:[],"date").present?
        date = parse_date_with_time(params[:search_query].try(:[],"date"), params[:offset])
      elsif params[:search_query].try(:[],"export_date").present?
        date = parse_date_with_time(params[:search_query].try(:[],"export_date"), params[:offset])
      end
      if params[:search_query]["time1"].present? || params[:search_query]["time2"].present?
        time=parse_export_time(params[:search_query]["time1"],params[:search_query]["time2"])
      end
      if time.present? && (params[:search_query]["date"].present? || params[:search_query]["datie"].present?)
        date[:first_date] = "#{date[:first_date].first(11)}#{time.first}"
        date[:second_date] = "#{date[:second_date].first(11)}#{time.second}"
      end
      if time.present?
        date[:first_date] = "#{date[:first_date].first(11)}#{time.first}"
        date[:second_date] = "#{date[:second_date].first(11)}#{time.second}"
      end
    elsif params[:start_date].present? || params[:end_date].present?
      date = parse_refund_date(params[:start_date], params[:end_date])
    else
      date = parse_date_with_time(params[:date], params[:offset]) if params[:date].present?
    end
    report = getting_old_report(date,params)
    send_via = params[:send_via] if params[:send_via]
    search_query = params[:search_query].to_json if params[:search_query].present?
    message = ""
    file_id = ""
    unless report[:find]
      if date.present?
        job_id = ExportFileWorker.perform_async(@user.id, date[:first_date], date[:second_date], params[:type], params[:offset], params[:wallet_id], send_via, search_query, nil, params[:status],params[:trans],nil,nil,session[:session_id],params[:email])
      else
        job_id = ExportFileWorker.perform_async(@user.id, nil, nil, params[:type], params[:offset], params[:wallet_id], send_via, search_query, nil, params[:status],params[:trans],nil,nil,session[:session_id],params[:email])
      end
    else
      job_id = "abc"
      if report[:status] == "pending"
        message = TypesEnumLib::Message::Pending
      elsif report[:status] == "crashed"
        heading = TypesEnumLib::Message::CrashedHeading
        message = TypesEnumLib::Message::Crashed
      else
        file_id = report[:report_file_id].try(:[],"id")
      end
    end
    respond_to do |format|
      format.json do
        render json: {
            jid: job_id,
            status: report[:status],
            heading: heading,
            message: message,
            type: params[:type],
            file_id: file_id.to_json
        }
      end
    end
  end

  def exported_files
    Report.where(file_id: nil).update_all(read: true)
    user_id = current_user.merchant_id.present? ? current_user.merchant_id : current_user.id
    @files = Report.where(user_id: user_id, status: ["completed","pending"], read: [false,true]).order(id: :desc).per_page_kaminari(params[:page]).per(10)
  end

  def getting_old_report(date,params)
    return_hash = {find: false, report_file_id: nil, status: ""}
    if date.present?
      reports = Report.where(first_date: date[:first_date], second_date: date[:second_date], user_id: @user.id).select(:report_type,:status,:file_id,:transaction_type,:params)
    end
    if reports.present?
      report_statuses = reports.pluck(:status).uniq
      if params[:type] == TypesEnumLib::WorkerType::Transactions
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type], transaction_type: params[:trans], status: "crashed").last
        end
        if report.blank?
          if params[:search_query].present?
            if @user.merchant?
              report = reports.where.not(params: nil).where("report_type = ? AND transaction_type = ? AND params->>'date' = ? AND params->>'name' = ? AND params->>'email' = ? AND params->>'last4' = ? AND params->>'phone' = ? AND params->>'amount' = ? AND params->>'type' = ?" , params[:type], params[:trans], params[:search_query][:date], params[:search_query][:name], params[:search_query][:email], params[:search_query][:last4], params[:search_query][:phone],params[:search_query][:amount],["#{params[:search_query][:type]}"]).last
            elsif @user.iso?
              report = reports.where.not(params: nil).where("report_type = ? AND params->>'date' = ? AND params->>'amount' = ? AND params->>'wallets_id' = ? AND params->>'DBA_name' = ? AND params->>'Receiver_name' = ? AND params->>'type' = ? AND params->>'time1' = ? AND params->>'time2' = ?", params[:type], params[:search_query][:date],params[:search_query][:amount],params[:search_query][:wallets_id], params[:search_query][:DBA_name], params[:search_query][:Receiver_name], ["#{params[:search_query][:type]}"], params[:search_query][:time1], params[:search_query][:time2]).last
            else
              report = reports.where.not(params: nil).where("report_type = ? AND params->>'date' = ? AND params->>'amount' = ? AND params->>'DBA_name' = ? AND params->>'Receiver_name' = ? AND params->>'type' = ? AND params->>'time1' = ? AND params->>'time2' = ?", params[:type], params[:search_query][:date],params[:search_query][:amount], params[:search_query][:DBA_name], params[:search_query][:Receiver_name], ["#{params[:search_query][:type]}"], params[:search_query][:time1], params[:search_query][:time2]).last
            end
          else
            report = reports.where(report_type: params[:type], transaction_type: params[:trans]).last
          end
        end
      else
        if report_statuses.include? ("crashed")
          report = reports.where(report_type: params[:type],status: "crashed").last
        else
          report = reports.where(status: "pending").last
        end
        report = reports.where(report_type: params[:type]).last if report.blank?
      end

      if report.present?
        return_hash[:find] = true
        return_hash[:status] = report.try(:status)
        return_hash[:report_file_id] = report.try(:file_id)
      end
    end
    return return_hash
  end

  def send_email_export_file
    if params[:user_id].present? && params[:qc_file].present?
      user = User.find_by(id: params[:user_id])
      qc_file = QcFile.find_by(id: params[:qc_file])
      if user.present? && qc_file.present?
        UserMailer.send_export_file(user, qc_file.image.url).deliver_later
        render status: 200, json:{success: 'Done'}
      else
        render status: 404, json:{success: 'Error'}
      end
    else
      render status: 404, json:{success: 'Error'}
    end
  end

  def update_email_status
       cron_job = CronJob.find(params[:hidden_field_merchant_email].to_i)
       cron_job.update(email: true)
  end

  def kill_job
    # queue = Sidekiq::Queue.new
    # queue.each do |job|
    #   job.delete if job.jid == params[:job_id]
    # end
    if params[:job_id].present?
      del_status = 0
      job = Sidekiq::Status::get_all params[:job_id]
      unless job.blank?
        del_status = Sidekiq::Status.delete(params[:job_id])
      end
      if del_status == 1
        render status: 200, json:{success: 'Done'}
      else
        render status: 404, json:{success: 'Error'}
      end
    else
      render status: 404, json:{success: 'Error'}
    end
  end

  def export_download
    if params[:report_id].present?
      report = Report.find_by(id: params[:report_id])
      report.update(read: true) if report.present?
    end
    if params[:id].present?
      files = []
      ids = JSON(params[:id])
      if ids.present?
        if ids.class == Array
          if ids.count == 1
            foo = QcFile.find(ids.first)
            data = open("https:"+foo.image.url)
            send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
          else
            QcFile.where(id: ids).each do|foo|
              files.push([foo.image, foo.image_file_name, modification_time: 1.day.ago])
            end
            zipline(files, 'export.zip')
          end
        else
          foo = QcFile.find(params[:id])
          data = open("https:"+foo.image.url)
          send_data data.read, filename: "#{foo.image_file_name}", type: "text/csv", disposition: 'attachment'
        end
      end
    end
  end
  # # ----------------------- Generate export file end -----------------------#

  # ------------------------------Merchant Refunds View------------------------------------
  def refund
    @heading = "Refund Transactions"
    if params["end_date"].present?
      end_date_param = params["end_date"] + " 23:59:59"
      end_date = end_date_param.to_datetime + 1.day
    end
    start_date = params["start_date"].to_datetime - 1.day if params["start_date"].present?
    # ---------Merchant Transactions---------------
    if @user.MERCHANT? && @user.merchant_id.nil? && @user.wallets.present?
      ids = []
      ids << @user.wallets.primary.pluck(:id)
      locations_ids = @user.attached_locations
      if locations_ids.present?
        locations = Location.where(id: locations_ids)
        if locations.present?
          locations.each do |location|
            ids << location.wallets.try(:primary).try(:first).try(:id)
          end
        end
      end
      ids = ids.flatten.uniq
      @transactions = all_merchant_wallets_transactions(ids, 'refund', @user.ledger,params[:start_date].present? ? start_date.utc.rfc3339 : nil,params[:end_date].present? ? end_date.utc.rfc3339 : nil).to_a
      @transactions = @transactions.sort_by{|i| i[:timestamp]}.reverse
      # ---------Submerchant Admin Transactions------
    elsif @user.MERCHANT? && !@user.merchant_id.nil? && @user.try(:permission).admin? && @user.wallets.present?
      abc = User.find(@user.merchant_id)
      @transactions = all_merchant_wallets_transactions(abc.wallets.primary.or(abc.wallets.primary).collect{|v|v.id}, 'refund', @user.ledger,params[:start_date].present? ? start_date.utc.rfc3339 : nil,params[:end_date].present? ? end_date.utc.rfc3339 : nil).to_a
      @transactions = @transactions.sort_by{|i| i[:timestamp]}.reverse
    else
      @transactions = []
    end

    # ------------Change UTC Date According To Local Timezone If Present-----------------------
    @transactions = @transactions.select{|v|v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%d-%m-%Y") >= params[:start_date] and v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%d-%m-%Y") <= params[:end_date]}.sort_by{|i| i[:timestamp]}.reverse if params[:start_date].present? && params[:end_date].present?
    @transactions = @transactions.select{|v|v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%d-%m-%Y") >= params[:start_date]}.sort_by{|i| i[:timestamp]}.reverse if params[:start_date].present? && !params[:end_date].present?
    @transactions = @transactions.select{|v|v[:timestamp].in_time_zone(cookies[:timezone]).strftime("%d-%m-%Y") <= params[:end_date]}.sort_by{|i| i[:timestamp]}.reverse if params[:end_date].present? && !params[:start_date].present?
    # ------------Change UTC Date According To Local Timezone If Present End-----------------------
    @transactions = Kaminari.paginate_array(@transactions).page(params[:page]).per(10)
  end

  # ---------------Refund Merchant Transaction-----------------------------------------------------
  def get_merchant_amount
    transaction = Transaction.where(id: params[:trans_id]).first if params[:trans_id].present?
    # transaction = Transaction.where(seq_transaction_id: params[:parent_id]).first if transaction.blank?
    if transaction.present?
      user_name = ''
      fee = 0
      wallet = Wallet.find_by_id(params[:destination]) if params[:destination].present?
      fee = wallet.location.fees.buy_rate.first.refund_fee if wallet.present?
      if params[:source].present?
        wallet = Wallet.find_by_id(params[:source])  if params[:source].present?
        user = wallet.users.first if wallet.present?
      else
        user = wallet.users.first if wallet.present?
      end
      user_name = user.name if user.present?
      # transaction[:amount] = transaction[:amount].to_f + transaction[:tip].to_f + transaction[:privacy_fee].to_f if transaction[:main_type].present? && transaction[:main_type] != TypesEnumLib::TransactionViewTypes::CreditCard
      if transaction.refund_log.nil?
        render status: 200 , json:{partial_amount: transaction[:total_amount].to_f,name: user_name,fee: fee}
      else
        if transaction.refund_log.present?
          if JSON.parse(transaction.refund_log).kind_of?(Array)
            temp = JSON.parse(transaction.refund_log)
            previousAmount = transaction[:total_amount].to_f - temp.sum{|h| h["amount"]}
          else
            temp = JSON.parse(transaction.refund_log)
            previousAmount = transaction[:total_amount].to_f - temp["amount"].to_f
          end
        else
          previousAmount = transaction[:total_amount]
        end
        render status: 200 , json:{partial_amount: previousAmount,name: user_name,fee: fee}
      end
    else
      render status: 200 , json:{ partial_amount: "" }
    end
  end

  def refund_merchant_transaction
    params[:transaction] = ActiveSupport::JSON.decode(params[:transaction]) if params[:js].present?
    reason_detail = params[:reason_detail].present? ? params[:reason_detail] : "NA"
    transaction = Transaction.where(id: params[:transaction]).first
    seq_transaction_id = transaction.seq_transaction_id
    t_amount = transaction.total_amount.to_f

    # merch_wall_id = transaction.receiver_wallet_id if transaction.present?
    # m_wallet = transaction.receiver_wallet
    merch = transaction.receiver
    if params[:partial_amount].present? && params[:partial_amount].to_f > 0 && params[:partial_amount].to_f < t_amount
      partial_amount = params[:partial_amount]
    end
    refund = RefundHelper::RefundTransaction.new(seq_transaction_id, transaction,merch,partial_amount,reason_detail, @user)
    response = refund.process
    ## Activity Log saved - Temporary Solution.
    unless @user.nil?
    refund_params = {}
    refund_params[:seq_transaction_id] = transaction.id
    refund_params[:user] = @user
    refund_params[:type] = "merchant"
    refund_params[:response] = response
    record_refund_transaction(refund_params)
    end
    if response[:success] == true
      flash[:success] = response[:message]
    else
      flash[:error] = response[:message]
    end
    return redirect_back(fallback_location: root_path)
  end
  # ------------------------------Merchant Refunds End---------------------------------
  def details
    @transaction = Transaction.find params[:transaction_id]
    respond_to do |format|
      format.html{ render :template => "merchant/transactions/details"}
    end
  end

  def refund_check
    trans_id = params[:trans_id]
    @transaction = Transaction.where(seq_transaction_id: trans_id).first
    respond_to do |format|
      format.js{
        render "merchant/transactions/refund_status.js.erb"
      }
    end
  end

  def verify_user
    if current_user.valid_password?(params[:password])
      render status: 200, json:{success: 'Verified'}
    else
      render status: 404, json:{error: 'You are not Authorized for this action.'}
    end
  end

  def wallet_details
    wallet= Wallet.find params[:wallet_id]
    if wallet.present?
      render status: 200 ,json: {wallet: wallet}
    else
      render status: 404, json: {error: 'Un Authorized '}
    end
  end

  def transfer
    @heading = "Send Money"
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.iso? || @user.agent?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.where(wallet_type: 'primary').each do |w|
          @wallets << w
        end
      end
    end
  end

  def transfer_amount #=---------send money
    if params[:to_id].present? and params[:amount].to_f > 0
      begin
        if params[:from_id].present?
          if @user.nil?
            flash[:error] = I18n.t 'merchant.controller.inval_auth_token'
            redirect_back(fallback_location: root_path)
          else
            wallet = Wallet.where(id: params[:from_id]).first
            to_wallet = Wallet.where(id: params[:to_id]).first
            reciept = to_wallet.users.first
            if reciept.merchant? && wallet.users.first.merchant?
              flash[:error] = I18n.t 'merchant.controller.cant_transfer_mer'
              redirect_back(fallback_location: root_path)
            else
              if to_wallet.present?
                if !wallet.nil?

                  wallet_id = wallet.id
                  wallet_name = wallet.name
                  # if params[:platform] === 'ATM'
                  #   atm_transfer(to_user, from_user)
                  # end
                  balance = show_balance(params[:from_id])
                  amount_to_transfer = params[:amount].to_f
                  if balance.to_f >= amount_to_transfer
                    amount = amount_to_transfer
                    data = ''
                    user = to_wallet.users.first
                    tx = transaction_between(params[:to_id], params[:from_id], amount,'p2p_payment',0, data,nil,nil,nil,nil)
                    transactions = all_transactions(params[:from_id], params[:from_id], 0)
                    message = "$ #{params[:amount]} has been successfully transferred to your wallet from #{@user.name}."
                    if params[:message].present? and params[:message] != ""
                      message = message + "\nNote: #{params[:message]}"
                    end
                    if !reciept.nil?
                      twilio_text = send_text_message(message, reciept.phone_number)
                      push_notification(message, reciept)
                    end

                    flash[:success] = "You have successfully Transfered $ #{ number_with_precision( amount_to_transfer, precision: 2)} to #{reciept.email} "
                    redirect_back(fallback_location: root_path)
                    # return render_json_response({
                    #                                 success: true,
                    #                                 amount_transferred: cents_to_dollars(amount_to_transfer),
                    #                                 balance: cents_to_dollars(balance.to_f - amount_to_transfer.to_f),
                    #                                 wallet_id: wallet_id,
                    #                                 wallet_name: wallet_name,
                    #                                 wallet: WalletSerializer.new(wallet),
                    #                                 sender_message: twilio_text,
                    #                                 transactions: transactions
                    #                             }, :ok)
                  else
                    flash[:error] = I18n.t 'merchant.controller.not_enough_to_trnsfr'
                    redirect_back(fallback_location: root_path)
                  end
                else
                  flash[:error] = I18n.t 'merchant.controller.inval_auth_token_forwallet'
                  redirect_back(fallback_location: root_path)
                end

              else
                flash[:error] = I18n.t 'merchant.controller.rec_not_found'
                redirect_back(fallback_location: root_path)
              end
            end
          end
        else
          flash[:error] = I18n.t 'merchant.controller.send_req'
          redirect_back(fallback_location: root_path)
        end
      rescue => ex
        puts "=======EXCEPTION HANDLED======", ex.message
        flash[:error] = I18n.t 'merchant.controller.exception4'
        redirect_back(fallback_location: root_path)
      end
    else
      flash[:error] = I18n.t 'merchant.controller.recv_req'
      redirect_back(fallback_location: root_path)
    end
  end

  def details_modal
    local = BlockTransaction.where(id: params[:transaction_id]).first
    @transaction = local.try(:parent_transaction)
    if @transaction.blank?
      @transaction = local
    end
    if @transaction.present?
      if @transaction.main_type == "CBK Hold" || @transaction.main_type == "CBK Won" || @transaction.main_type == "CBK Fee"
        case_number = @transaction.tags.try(:[],"dispute_case").try(:[],"case_number")
        @dispute_case = DisputeCase.includes(:charge_transaction).find_by(case_number: case_number)
        @db_transaction = @dispute_case.charge_transaction
      end
    end

    if @transaction.nil?
      flash[:error] =  I18n.t('merchant.controller.ledger_not_found')
      redirect_back(fallback_location: root_path)
    end
    respond_to :js
  end

  def local_details_modal
    if params[:trans] == 'reserve'
      local = BlockTransaction.where(id: params[:transaction_id]).first
      @trx = @transaction = local.try(:parent_transaction).present? ? local.parent_transaction : local
    else
      @transaction = Transaction.where(id: params[:transaction_id]).first
      if @transaction.try(:main_type) == "refund_fee"
        id = @transaction.tags["refund_details"].try(:[],"parent_db_transaction_id")
        @transaction = Transaction.find_by(id: id) if id.present?
      end
      @trx = @transaction
    end
    # if @transaction.present?
    #   @merchant = @transaction.tags.try(:[], "merchant")
    #   @type = @transaction.main_type
    #   @amount = @transaction.amount.to_f
    #   @timestamp = @transaction.timestamp
    #   @activity = ActivityLog.where(transaction_id: @trx.id).first
    # end
    if !@trx.blank?
      @merchant = @transaction.tags.try(:[], "merchant")
      @location = @transaction.tags.try(:[], "location")
      @type = @transaction.main_type
      if params[:trans] == "reserve"
        @amount = @transaction.amount_in_cents.to_f/100
      elsif params[:trans] == "3ds_tracker"
        @tracker_3ds = true;
        @wallet_id = params[:wallet_id]
      else
        @amount = @transaction.amount.to_f
      end
      @timestamp = @transaction.timestamp
      @activity = ActivityLog.where(transaction_id: @trx.id).first
      if @transaction.main_type == "cbk_hold" || @transaction.main_type == "cbk_won" || @transaction.main_type == "cbk_fee" || @transaction.main_type == "cbk_lost"
        case_number = @transaction.tags.try(:[],"dispute_case").try(:[],"case_number")
        @dispute_case = DisputeCase.includes(:charge_transaction).find_by(case_number: case_number)
        @db_transaction = @dispute_case.try(:charge_transaction)
        # @db_transaction = SequenceLib.db_parse_action(@dispute_case.charge_transaction,@dispute_case.created_at)
      end
    end
    if @transaction.nil?
      flash[:error] = I18n.t('merchant.controller.ledger_not_found')
      redirect_back(fallback_location: root_path)
    end
    @t = @trx
    @db_t = @db_transaction
    @dispute = @dispute_case
    #respond_to do |format|
      render partial: "merchant/transactions/local_details"
      #format.html
    #end
  end

  def pay_tip
    from_wallet= Wallet.find params[:from_wallet]
    sub_merchants = {"#{current_user.id}" => params[:amount] }
    balance = show_balance(from_wallet.id)
    if balance > 0 && balance >= params[:amount].to_f
      unless from_wallet.nil?
        unless sub_merchants.blank?
          begin
            sub_merchants.each do |id,tip|
              if not tip.blank? or tip.nil?
                merchant= current_user
                if merchant
                  # all_wallets = merchant.wallets.where(wallet_type: 'primary')
                  # wallet=nil
                  # all_wallets.each do|wal|
                  #   wallet = wal if wal.users.count == 1
                  # end
                  wallet = from_wallet.try(:location).try(:wallets).try(:primary).try(:first)
                  raise StandardError.new(I18n.t('api.errors.wallet_not_found')) if wallet.blank?
                  tr_type = get_transaction_type(TypesEnumLib::TransactionType::AccountTransfer)
                  main_merchant = @user.merchant_id.present? ? User.find_by(id: merchant.merchant_id) : @user
                  local_transaction = Transaction.create(
                      amount: "#{tip.to_f}",
                      status: "pending",
                      sender_id: main_merchant.id,
                      receiver_id: merchant.id,
                      action: "transfer",
                      main_type: tr_type.first,
                      total_amount: tip.to_f,
                      net_amount: tip.to_f,
                      sender_wallet_id: from_wallet.id,
                      receiver_wallet_id: wallet.id,
                      sender_name: @user.name,
                      receiver_name: merchant.name,
                      sender_balance: balance,
                      receiver_balance: SequenceLib.balance(wallet.id)
                  )
                  transfer = SequenceLib.transfer(tip.to_f,from_wallet.id,wallet.id,tr_type.first,0,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil, nil, nil, nil, get_ip)
                  if transfer.present?
                    local_transaction.update(status: "approved",seq_transaction_id: transfer.actions.first.id, tags:transfer.actions.first.tags, timestamp: transfer.timestamp)
                    parsed_transactions = parse_block_transactions(transfer.actions, transfer.timestamp)
                    save_block_trans(parsed_transactions) if parsed_transactions.present?
                  end
                end
              end
            end
            flash[:success] = "Transfered to main wallet successfully!"
          rescue StandardError=> ex
            flash[:danger] = ex.message
          end
        else
          flash[:error] ="Please select atleast one user!"
        end
      else
        flash[:danger] = "Wallet Missing!"
      end
    else
      flash[:error] = I18n.t 'merchant.controller.insufficient_balance'
    end

    redirect_back(fallback_location: root_path)
  end
end
