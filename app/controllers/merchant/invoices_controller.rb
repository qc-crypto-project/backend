class Merchant::InvoicesController < Merchant::BaseController
  include ApplicationHelper
  include UsersHelper
  skip_before_action :set_wallet_balance, only: [:new, :detail_modal, :create, :update_invoice_status]
  before_action :validate_location

  def index
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @heading = "Business to Business"
    # @invoices = Request.all.paginate(:page => params[:page], :per_page => 10)
    @request = Request.new
    if current_user.merchant_id.present?
      wallet_ids = current_user.wallets.primary.where.not(location_id: nil).pluck(:id).map(&:to_s)
      @invoices = Request.where("sender_id = ? or reciever_id = ?", current_user.merchant_id, current_user.merchant_id).where("wallet_id IN (:wallet_id) OR from_wallet_id IN (:wallet_id) OR from_wallet_id IS NULL", wallet_id: wallet_ids,nil: nil).where(request_type:[nil,'b2b']).order(created_at: :desc)
    else
      @invoices = Request.where("sender_id = ? or reciever_id = ?", current_user.id, current_user.id).where(request_type:[nil,'b2b']).order(created_at: :desc)
    end
    if params[:req].present?
      if params[:req] == "approved"
        @invoices = @invoices.where(status:'approved').paginate(:page => params[:page], :per_page => page_size)
      elsif params[:req] == "pending"
        @invoices = @invoices.where(status:'in_progress').paginate(:page => params[:page], :per_page => page_size)
      elsif params[:req] == "rejected"
        @invoices = @invoices.where(status:['rejected','cancel']).paginate(:page => params[:page], :per_page => page_size)
      else
        @invoices = @invoices.paginate(:page => params[:page], :per_page => page_size)
      end
    else
      @invoices = @invoices.paginate(:page => params[:page], :per_page => page_size)
    end
    @invoice=Request.new
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.where(wallet_type: 'primary').each do |w|
          @wallets << w
        end
      end
    end
    if @user.MERCHANT? && @user.merchant_id.blank?
      Notification.where(recipient_id: @user.id,read_at: nil).where.not(status:['approved','rejected']).or(Notification.where(actor_id: @user.id,status: ['rejected','approved'])).update(read_at: Time.now,status:'read')
    elsif @user.SUBMERCHANT? && @user.merchant_id.present?
      Notification.where(recipient_id: @user.merchant_id,read_at: nil).update(read_at: Time.now)
    elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
      Notification.where(recipient_id: @user.id,read_at: nil).where.not(status:['approved','rejected']).or(Notification.where(actor_id: @user.id,status: ['rejected','approved'])).update(read_at: Time.now,status:'read')
    end
    render template: 'merchant/invoices/index'
  end

  def create
    if params[:amount].present? && params[:to_id].present? && params[:wallet_id].present?
      if params[:amount].present? && params[:amount].to_f > 0
        begin
          # Receiver means guy who will receive the money
          reciever = @user
          reciever = User.find(@user.merchant_id) if current_user.MERCHANT? && current_user.merchant_id.present?
          sender = User.includes(:wallets).where(id: params[:to_id]).first
          if reciever.id == sender.id
            redirect_to merchant_invoices_path(tab:'wallet')
            flash[:error] = I18n.t 'merchant.controller.cant_request'
          elsif
            sender.role === "user"
            redirect_to merchant_invoices_path(tab:'wallet')
            flash[:error] = "You cannot request from a Customer!"
          else
            if sender.present? && reciever.present?
              if current_user.MERCHANT? && current_user.merchant_id.present? && current_user.try(:permission).admin?
                wallet = User.find(@user.merchant_id).wallets.where(id: params[:wallet_id]).first
              else
                wallet = @user.wallets.where(id: params[:wallet_id]).first
              end
              if wallet.present?
                description= params[:description] if params[:description].present?
                request = Request.new(description: description,amount: params[:amount], sender_id: reciever.id, sender_name: reciever.name, reciever_id: sender.id, reciever_name: sender.name, status: 'in_progress', wallet_id: wallet.id, request_type: "b2b")
                if request.save
                  if params[:images].present?
                    image = {add_image: params[:images], request_id: request.id}
                    @img = Image.new(image)
                    if @img.validate
                      @img.save
                    else
                      @error = @img.errors.full_messages.first
                    end
                  end
                  # if params[:attachments].present?
                  #    params[:attachments].first.split(',').each do |img|
                  #        image=Image.find img
                  #        request.images << image
                  #   end
                  # end
                  requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
                  requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
                  message = "#{reciever.name} has sent you a request for #{number_to_currency(params[:amount])} "
                  if params[:message].present? and params[:message] != ""
                    message = message + "\nNote: #{params[:message]}"
                  end
                  twilio_text = send_text_message(message, sender.phone_number)
                  push_notification(message, sender)
                  # notification_for_request(request,sender,reciever)
                  # twilio_text = send_text_message("You Have requested for #{number_to_currency(params[:amount])} to #{sender.name}", reciever.phone_number)
                  redirect_to merchant_invoices_path(tab:'wallet')
                  flash[:success] = "Successfully sent"
                  # return render_json_response({success: true,  requests: requests, message: "Successfully sent",  auth_token: @user.authentication_token},:ok)
                else
                  redirect_to merchant_invoices_path(tab:'wallet')
                  flash[:error] = I18n.t 'merchant.controller.req_not_send'
                  # return render_json_response({success: false, message: "Request Not sent"},:ok)
                end
              else
                redirect_to merchant_invoices_path(tab:'wallet')
                flash[:error] = I18n.t 'merchant.controller.not_belong_wallet'
              end
            else
              redirect_to merchant_invoices_path(tab:'wallet')
              flash[:error] = "Sender or Reciever not found!"
            end
          end
        rescue => ex
          puts "=======EXCEPTION HANDLED======", ex.message
          redirect_to merchant_invoices_path(tab:'wallet')
          flash[:error] = "#{ex.message}"
        end
      else
        redirect_to merchant_invoices_path(tab:'wallet')
        flash[:error] = I18n.t 'errors.withdrawl.amount_must_greater_than_zero'
      end
    else
      redirect_to merchant_invoices_path(tab:'wallet')
      flash[:error] = I18n.t 'merchant.controller.empty_service'
    end
  end

  def update_invoice_status
    begin
      status = params[:status]
      invoice = params[:invoice_id]
      from_wallet = params[:from_wallet].nil? ? nil : params[:from_wallet]
      params[:page_number] = 0
      @errors = Array.new
      @success = Array.new
      @wallet = ''
      if status.present? && invoice.present?
        @request = Request.where(id: invoice).first
        if @request.present?
          case status.to_i
          when 1
            @request.update(status: "cancel")
            @success << "Request Cancelled"
          when 2
            if @user.present?
              if @request.present?
                @request.update(status: 'rejected')
                @success << "Request Rejected"
              else
                @errors << "Request not found!"
              end
            else
              @errors << "User not found!"
            end
          when 3
            @wallet = @user.wallets.where(id: from_wallet).first
            if @wallet.present?
              reciever = User.where(id: @request.reciever_id).first #reciever is the one who received request
              balance = show_balance(@wallet.id)
              balance = balance - HoldInRear.calculate_pending(@wallet.id)
              if balance.to_f >= @request.amount.to_f
                sender = User.where(id: @request.sender_id).first # sender is the one who sent request and we will deduct fee from sender
                data = ''
                if sender.merchant? && reciever.merchant?
                  current_transaction = transaction_between(@request.wallet_id,@wallet.id ,@request.amount, TypesEnumLib::TransactionType::B2b,0, data,nil,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true)
                  seq_tx = current_transaction.try(:[], :seq_tx) if current_transaction.try(:[], :seq_tx).present?
                elsif sender.merchant? && (reciever.iso? || reciever.agent? || reciever.affiliate?)
                  current_transaction = transaction_between(@request.wallet_id,@wallet.id ,@request.amount, TypesEnumLib::TransactionType::B2b,0, data,nil,nil,TypesEnumLib::GatewayType::Quickcard,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true)
                  seq_tx = current_transaction.try(:[], :seq_tx) if current_transaction.try(:[], :seq_tx).present?
                elsif sender.iso? || reciever.iso? || sender.agent? || reciever.agent? || sender.affiliate? || reciever.affiliate?
                  amount_sum, fee = deduct_b2b_fee(sender, @request.amount.to_f)
                  if (reciever.iso? || reciever.agent? || reciever.affiliate?) && reciever.system_fee.present? && reciever.system_fee != "{}"
                    limit_withdraw = reciever.system_fee["check_withdraw_limit"]
                    raise StandardError.new "You can't make transaction this time, Withdraw limit exceed!" if (balance.to_f - @request.amount.to_f) < limit_withdraw.to_f
                  end
                  transaction = create_transaction(@user,@request.wallet_id, @wallet.id, reciever, sender, @request.amount.to_f, fee)
                  current_transaction = SequenceLib.direct_transfer(@request.amount.to_f, @wallet.id, @request.wallet_id, TypesEnumLib::TransactionType::B2b, fee,  TypesEnumLib::TransactionType::B2bFee, TypesEnumLib::GatewayType::Quickcard, get_ip,nil)
                  if current_transaction.present?
                    seq_tx = current_transaction
                    transaction.update(
                        status: "approved",
                        seq_transaction_id: current_transaction.actions.first.id,
                        tags: current_transaction.actions.first.tags,
                        timestamp: current_transaction.timestamp
                    )
                    parsed_transactions = parse_block_transactions(current_transaction.actions, current_transaction.timestamp)
                    if parsed_transactions.present? && parsed_transactions.select{|v| v[:type] == "B2B Fee"}.first.present?
                      fee_block_txn = parsed_transactions.select{|v| v[:type] == "B2B Fee"}.first
                      fee = SequenceLib.dollars(fee_block_txn[:amount].to_f)
                      receiver = Wallet.find(fee_block_txn[:receiver]).try(:users).try(:first)
                      fee_tx = create_transaction(@user, fee_block_txn[:receiver] , fee_block_txn[:sender], sender, receiver, fee, fee, TypesEnumLib::TransactionType::B2bFee)
                      fee_tx.update(
                          status: "approved",
                          seq_transaction_id: fee_block_txn[:seq_parent_id],
                          tags: fee_block_txn[:tags],
                          timestamp: fee_block_txn[:timestamp]
                      )
                    end
                  end
                elsif reciever.qc? || sender.qc?
                  if !sender.merchant?
                    transaction = create_transaction(@user,@request.wallet_id, @wallet.id, reciever, sender, @request.amount.to_f, fee)
                    seq_tx = current_transaction[:seq_tx] if current_transaction[:seq_tx].present?
                  end
                  if sender.merchant?
                    current_transaction = transaction_between(@request.wallet_id,@wallet.id ,@request.amount, TypesEnumLib::TransactionType::B2b,0, data,nil,nil,TypesEnumLib::GatewayType::Quickcard)
                    seq_tx = current_transaction.try(:[], :seq_tx) if current_transaction.try(:[], :seq_tx).present?
                  elsif sender.iso? || sender.agent? || sender.affiliate?
                    amount_sum, fee = deduct_b2b_fee(sender, @request.amount.to_f)
                    current_transaction = SequenceLib.direct_transfer(@request.amount.to_f, @wallet.id, @request.wallet_id, TypesEnumLib::TransactionType::B2b, fee,  nil, TypesEnumLib::GatewayType::Quickcard, get_ip,nil)
                    seq_tx = current_transaction
                  else
                    current_transaction = SequenceLib.direct_transfer(@request.amount.to_f, @wallet.id, @request.wallet_id, TypesEnumLib::TransactionType::B2b, fee,  nil, TypesEnumLib::GatewayType::Quickcard, get_ip,nil)
                    seq_tx = current_transaction
                  end
                  if !sender.merchant? && transaction.present? && current_transaction.present?
                    transaction.update(seq_transaction_id: current_transaction.actions.first.id,
                                       status: "approved",
                                       tags: current_transaction.actions.first.tags,
                                       timestamp: current_transaction.timestamp)
                  end
                end
                if current_transaction.nil? || [current_transaction].flatten.size <= 0
                  @errors << "Sequence not responding please try again later or Contact App Administrator!"
                else
                  #= creating block transaction
                  parsed_transactions = parse_block_transactions(seq_tx.actions, seq_tx.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                  message = "#{reciever.name} has accepted your request for #{number_to_currency(@request.amount.to_f)} ."
                  twilio_text = send_text_message(message, sender.phone_number)
                  push_notification(message, sender)
                  # request = Request.new(description: description,amount: params[:amount], sender_id: reciever.id, sender_name: reciever.name, reciever_id: sender.id, reciever_name: sender.name, status: 'in_progress', wallet_id: wallet.id)
                  @request.update(status: 'approved', from_wallet_id: @wallet.id)
                  @success << "Transferred Successfully"
                end
              else
                @errors << "Insufficient Funds for this Request"
              end
            else
              @errors << "Wallet does not exist"
            end
          else
            @errors << "Something wrong happened !Please try again"
          end
          flash[:success] = @success.first if @success.present?
          flash[:error] = @errors.first if @errors.present?
          redirect_to merchant_invoices_path
        else
          flash[:error] = @errors.first
          redirect_to merchant_invoices_path
        end
      else
        flash[:error] = @errors.first
        redirect_to merchant_invoices_path
      end
    rescue StandardError => exc
      flash[:error] = exc.message
      redirect_to merchant_invoices_path
    end

  end

  def new
    @request = Request.new
    @wallets=Array.new
    location_status = false
    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
      if @user.qc?
        @wallets = @user.wallets
      else
        @wallets = @user.wallets.primary
      end
    elsif @user.merchant? && @user.merchant_id.present?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    else
      @main_user = current_user.parent_merchant
      @wallets = @main_user.wallets.primary
    end
    @list=[]
    wallet_count = @wallets.count
    @wallets.each do |w|
      @location = w.location
      if @location.present?
        location_status = @location.is_block
        if wallet_count > 1
          @list <<
            {
              wallet_id: w.id,
              wallet_name: w.name,
              # balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
              location_status: location_status
            }
        else
          @list <<
            {
              wallet_id: w.id,
              wallet_name: w.name,
              balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
              location_status: location_status
            }
        end
        @new = 0
      else
        @list <<
          {
            wallet_id: w.id,
            wallet_name: w.name,
            balance: show_balance(w.id),
            location_status: false
          }
      end
    end

    render partial: "merchant/invoices/new_request.html.erb"
  end

  def get_balance
    if params[:wallet_id].present?
      balance= show_balance(params[:wallet_id])
      if balance.present?
        balance= number_with_precision(balance, precision: 2, :delimiter => ',')
        render status: 200, json:{success: 'true', :balance => balance}
      else
        render status: 404, json:{error: 'sorry'}
      end
    end
  end

  def inbox
    render template: 'merchant/invoices/inbox'
  end

  def sent
    render template: 'merchant/invoices/sent'
  end

  def status_modal
    @invoice= Request.find params[:invoice_id]
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
    elsif @user.merchant_id.present?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    else
      @main_user = User.find(@user.merchant_id)
      @main_user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.where(wallet_type: 'primary').each do |w|
          @wallets << w
        end
      end
    end
    respond_to :js
  end

  def detail_modal
    @user= current_user
    @wallets=[]
    @request = Request.find(params[:invoice_id])
    @invoice= @request
    @notifications = Notification.where(read_at: nil,notifiable_id: @request.id ,recipient_id: current_user.id, notifiable_type: "Request" )
    unless @notifications.blank?
        @notifications.last.update(read_at: Time.now)
        # ActionCable.server.broadcast "notification_channel_#{current_user.id}", channel_name: "notification_channel_#{current_user.id}" , message: 1
    end
    if (@user.merchant? && @user.merchant_id.nil?) || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
      @user.wallets.primary.each do |w|
        @wallets << w
      end
    elsif @user.qc?
      @wallets = @user.wallets.qc_support
    elsif @user.merchant_id.present?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    end
    @list=[]
    if @user.qc?
      @wallets.try(:each) do |wallet|
        @list << {
            wallet_id: wallet.id,
            wallet_name: wallet.name,
            balance: show_balance(wallet.id) - HoldInRear.calculate_pending(wallet.id),
            location_status: false
        }
      end
    else
      @wallets.each do|w|
        @location = w.location
        if @location.present?
          location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
                  location_status: location_status
              }
        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: false
              }
        end
      end
    end
    # @notifications = Notification.where(read_at: nil,notifiable_id: @request.id ,recipient_id: current_user.id, notifiable_type: "Request" )
    @notifications_count = Notification.where(read_at: nil,recipient_id: current_user.id, notifiable_type: "Request" ).count
    respond_to :js
  end

  private

  def notification_for_request(notifiable,recipient,actor)
    notification = Notification.new(recipient_id: recipient.id, actor_id: actor.id)
    notification.notifiable= notifiable
    notification.save
  end

  def reject_request(invoice_id)
    if params[:request_id].present?
    begin
      if @user.present?
        request = Request.where(id: params[:request_id], reciever_id: @user.id).first
        if request.present?
          if request.status == "cancel"
            return render_json_response({success: false, message: "Cancelled"},:ok)
          else
            if request.status == "approved"
              return render_json_response({success: false, message: "Allready approved"},:ok)
            else
              request.update(status: 'rejected')
              requests = Request.where("sender_id = ? or reciever_id = ?", @user.id, @user.id).order(updated_at: :desc)
              requests = requests.map{|x| {id: x.id, sender_name: user_name(x.sender_id) , reciever_name: user_name(x.reciever_id) ,sender_id: x.sender_id, reciever_id: x.reciever_id, wallet_id: x.wallet_id,amount: x.amount, status: x.status, created_at: x.created_at, updated_at: x.updated_at }}
              return render_json_response({success: true, message: "Reject Successfully", requests: requests},:ok)
            end
          end
        else
          return render_json_response({success: false, message: "Request not found!"},:ok)
        end
      else
        return render_json_response({success: false, message: "You r Not found!"},:ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  else
    return render_json_response({success: false, message: "You passed empty service"},:ok)
  end
  end

  def approve_request(invoice , wallet_id)
    if invoice.present?  and wallet_id.present?
    begin
      @request = Request.where(reciever_id: @user.id,id: invoice).first
      if @request.present?
        if @request.status == "cancel"
          # return render_json_response({success: false, message: "Allready Cancelled"},:ok)
          respond_to  do |format|
            format.js
          end
        else

        end
      else
        return render_json_response({success: false, message: "request not found"},:ok)
      end
    rescue => ex
      puts "=======EXCEPTION HANDLED======", ex.message
      return render_json_response({:success => false, :message => "Somthing Went Wrong #{ex.message}"},:ok)
    end
  else
    return render_json_response({success: false, message: "Incomplete params"},:ok)
  end
  end

  def user_name(id)
    user = User.find_by_id(id) if id.present?
    if user.present?
      return user.name
    else
      return ''
    end
  end

  def create_transaction(user, to, from, reciever, sender, amount, fee, type=nil)
    sender_dba=get_business_name(Wallet.find_by(id:from))
    receiver_dba=get_business_name(Wallet.find_by(id:to))
    type = type == TypesEnumLib::TransactionType::B2bFee ? type : updated_type(TypesEnumLib::TransactionType::B2b)
    user.transactions.create(to: to,
                              from: from,
                              status: "pending",
                              action: 'transfer',
                              sender: reciever,
                              sender_name: sender_dba,
                              receiver: sender,
                              receiver_name: receiver_dba,
                              sender_wallet_id: from,
                              receiver_wallet_id: to,
                              sender_balance: SequenceLib.balance(from),
                              receiver_balance: SequenceLib.balance(to),
                              amount: amount.to_f,
                              fee: fee,
                              main_type: type,
                              ip: get_ip,
                              net_fee: fee,
                              net_amount: amount.to_f - fee.to_f,
                              total_amount: amount.to_f
    )
  end

end
