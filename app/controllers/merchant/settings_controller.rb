class Merchant::SettingsController < Merchant::BaseController
  skip_before_action :set_wallet_balance, only: [:new, :edit, :edit_user, :update, :destroy]
  skip_before_action :require_merchant, only: [:edit_user]

  def index
    @heading = "Employee List"
    @user= User.new
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
    if current_user.merchant_id.present?
      @sub_merchants= User.where(id: current_user.merchant_id).first.sub_merchants.where(archived: "false").where.not(id: current_user.id).order(created_at: "DESC").per_page_kaminari(params[:page]).per(page_size)
      @wallets = Location.where(merchant_id: current_user.merchant_id)
    else
      @sub_merchants= current_user.sub_merchants.where(archived: "false").order(created_at: "DESC").per_page_kaminari(params[:page]).per(page_size)
      @wallets = Location.where(merchant_id: current_user.id)
    end
    @sub_merchant=User.new
    # @wallets = current_user.wallets
    @filters = [10,25,50,100]
    render template: 'merchant/settings/index'
  end
  def show
    # p "KIND INSPECT", params[:kind]
  end

  def edit_user
    @merchant = User.find(params[:id])
    render partial: 'merchant/settings/merchant_modal', locals: {user: @merchant}
    # respond_to  do |format|
    #   format.js
    # end
  end

  def update_user
    user = User.unarchived_users.where.not(:role => :user).find_by_email(params[:user][:email])
    params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
    if user.present? && current_user.email != params[:user][:email]
      flash[:error] = I18n.t 'merchant.controller.used_email'
      redirect_back(fallback_location: root_path)
      return
    end
    user_data = merchant_params
    if params[:user].present? && ( ! params[:user][:password].present? || ! params[:user][:password_confirmation].present?)
      user_data = user_data.except(:password)
      user_data = user_data.except(:password_confirmation)
    else #if password is change
      user_data[:is_password_set] = true
    end
    if current_user.update(user_data)
      sign_in(current_user, :bypass => true)
      flash[:success] = I18n.t 'merchant.controller.successful_user_update'
      redirect_back(fallback_location: root_path)
    else
      flash[:error] = current_user.errors.full_messages.first
      redirect_back(fallback_location: root_path)
    end
  end

  def new
    if current_user.merchant_id.present?
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
      @permission = Permission.where(merchant_id: [current_user.merchant_id, nil])
    else
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
      @permission = Permission.where(merchant_id: [current_user.id, nil])
    end
    @user= User.new
    respond_to :js
  end

  def new_sub_merchant
    begin
      params[:user][:name].strip!
      params[:user][:last_name] = params[:user][:name].partition(" ").last
      params[:user][:first_name] = params[:user][:name].partition(" ").first
      params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
      params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
      @sub_merchant = User.new(sub_merchant_params)
     @sub_merchant.merchant_id = current_user.merchant_id || current_user.id
     mail=params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
     user = User.where.not(role:'user').where(email: mail ).first
     if user.present? && user.archived == false
      flash[:notice] = I18n.t('merchant.controller.used_email')
     elsif user.present?
        user.archived = false
        if user.update(sub_merchant_params)
          flash[:success] = I18n.t 'merchant.controller.successful_user_add'
        else
          flash[:error] = user.errors.first
        end
     else
      @sub_merchant.regenerate_token
      @sub_merchant.is_block = false
      #@sub_merchant.submerchant_type = params[:submerchant_type]
      password = params[:user][:password]
      @sub_merchant.merchant!
      if @sub_merchant.errors.blank?
        if params[:user][:wallets]
          params[:user][:wallets].each do |id|
            location = Location.find id
            @sub_merchant.wallets << location.wallets.primary.first
          end
        end
        flash[:success] = I18n.t 'merchant.controller.successful_user_add'
      else
        flash[:error] = @sub_merchant.errors.first
      end
     end
    rescue => ex
      flash[:error] = ex.message
    end
    respond_to  do |format|
      format.html{ redirect_to merchant_settings_path }
    end
  end

  def edit
    @sub_merchant = User.find(params[:id])
    @merchant = User.find @sub_merchant.merchant_id
    if current_user.merchant_id.present?
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
    else
      @wallets = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact)
    end
    @permission = Permission.where(merchant_id: [@merchant.id, nil])
    # @wallets = (@sub_merchant.locations + Location.where(merchant_id: @merchant.id)).uniq
    respond_to  do |format|
      format.js
    end
  end

  def update
    @sub_merchant=User.find params[:id]
    # existing_locations = @sub_merchant.locations.pluck(:id).map{|c|c.to_s}
    existing_wallets = @sub_merchant.wallets.where.not(location_id: nil)
    coming_locations = params[:user][:wallets]
    # params[:user][:email]=params[:user][:email].downcase if params[:user].present? && params[:user][:email].present?
    params[:user][:name].strip!
    params[:user][:first_name] = params[:user][:name].partition(" ").first
    params[:user][:last_name] = params[:user][:name].partition(" ").last
    params[:user][:phone_number] = params[:phone_code].present? ? "#{params[:phone_code]}#{params[:user][:phone_number]}": params[:user][:phone_number] unless params[:password_only].present?
    result = update_sub_merchant(@sub_merchant,params[:user],params[:id].to_i)
    if result.first
      if @sub_merchant.errors.blank?
        if coming_locations.present?
          existing_wallets.each do |wallet|
            @sub_merchant.wallets.delete(wallet)
          end
          # @sub_merchant.wallets.where.not(location_id: nil).destroy(existing_wallets) if @sub_merchant.wallets.present?
          coming_locations.each do |id|
            location = Location.find(id)
            @sub_merchant.wallets << location.wallets.primary.first if location.present?
          end
        end
        # if coming_locations &&  coming_locations.size > existing_locations.size
        #    locations_to_add= coming_locations.sort - existing_locations
        #    locations_to_add.each do |id|
        #     location=Location.find id
        #     @sub_merchant.wallets << location.wallets.primary.first
        #    end
        # elsif coming_locations && coming_locations.size < existing_locations.size
        #   existing_locations.each do |id|
        #     if  !coming_locations.include? id
        #       l_to_del=@sub_merchant.locations.find id
        #       @sub_merchant.wallets.delete(l_to_del.wallets.primary.first)
        #       existing_locations = existing_locations - [id]
        #     end
        #   end
        #   coming_locations.each do |id|
        #     if !existing_locations.include? id
        #       l=@sub_merchant.locations.find id
        #       @sub_merchant.wallets << l.wallets.primary.first
        #     end
        #   end
        # elsif  coming_locations &&  coming_locations.size == existing_locations.size
        #   if existing_locations.size==1 && coming_locations.size == 1
        #     if existing_locations.first != coming_locations.first
        #       l=Location.find coming_locations.first
        #       @sub_merchant.wallets << l.wallets.primary.first
        #       ld=Location.find existing_locations.first
        #       @sub_merchant.wallets.delete(ld.wallets.primary.first)
        #     end
        #   else
        #     coming_locations.each do |c|
        #       if !existing_locations.include? c
        #         location= Location.find c.to_i
        #         @sub_merchant.wallets << location.wallets.primary.first
        #         # location_to_delete=@sub_merchant.locations.find c
        #         # @sub_merchant.locations.delete(location_to_delete)
        #       else
        #         # location_to_delete=@sub_merchant.locations.find c
        #         # @sub_merchant.locations.delete(location_to_delete)
        #       end
        #     end
        #   end
        # end
        # @sub_merchant.send_welcome_email('Merchant')
        flash[:success] = result.second
      else
        flash[:error] = @sub_merchant.errors.full_messages.first
      end
    else
      flash[:error] = result.second
    end
    redirect_to merchant_settings_path
  end

  def update_sub_merchant(submerchant,params,sub_id)
    begin
      user = User.find_by(email: params["email"]) if params["email"].present? && params["email"] != submerchant.email
      if submerchant.email.present? && params["email"].present? && submerchant.email==params["email"] && params["password"].blank? && submerchant.name==params['name'] && submerchant.first_name==params['first_name'] && submerchant.last_name==params['last_name'] && submerchant.permission_id == params['permission_id']
        return  [true,"Updated Successfully"]
      elsif submerchant.email.present? && params["email"].present? && submerchant.email!=params["email"] && submerchant.email.downcase==params["email"].downcase && params["password"].blank? && submerchant.name==params['name'] && submerchant.first_name==params['first_name'] && submerchant.last_name==params['last_name'] && submerchant.permission_id == params['permission_id'] && user.id==sub_id
        return  [false,"You can not edit the same email"]
      elsif user.present?
        return  [false,"A user with this email already exists"]
      else
        submerchant.email = params["email"]; submerchant.name = params["name"]; submerchant.phone_number = params["phone_number"]; submerchant.first_name = params['first_name']; submerchant.last_name = params["last_name"]; submerchant.permission_id = params["permission_id"]
        if params["password"].blank?
          if submerchant.save
            return  [true,"Updated Successfully"]
          else
            return [false,submerchant.errors.full_messages.first]
          end
        else
          submerchant.password = params["password"]
          if submerchant.save
            return [true,"Updated Successfully"]
          else
            return [false,submerchant.errors.full_messages.first]
          end
         end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      return [false,invalid]
    end
  end

  def destroy
    @sub_merchant = User.where(id: params[:id]).first
    if @sub_merchant.archived == false
      @sub_merchant.update(archived: true,permission_id: nil)
    else
      @sub_merchant.update(archived: false)
    end
    respond_to do |format|
      format.html {
        flash[:notice] = "Successfully Deleted."
        redirect_back(fallback_location: merchant_transactions_path)
      }
      format.json { head :no_content }
    end
  end

  private

  def merchant_params
    if params[:user][:two_step_verification].present?
      if params[:user][:two_step_verification].to_i == 0
        params[:user][:two_step_verification] = false
      else
        params[:user][:two_step_verification] = true
      end
    end
    params.require(:user).permit(:name,:company_name,:email, :phone_number,:last_name, :merchant_id, :password, :password_confirmation, :retail_sales, :archived,:two_step_verification)
  end

  def sub_merchant_params
    params.require(:user).permit(:name, :email, :phone_number, :first_name, :last_name, :merchant_id, :password, :password_confirmation, :retail_sales, :archived, :permission_id)
  end

  # def edit_sub_merchant_params
  #   params.require(:user).permit(:id,:email,:name, :phone_number,:last_name, :merchant_id, :password, :submerchant_type)
  # end
end
