
class Merchant::BaseController < ApplicationController
  require 'uri/http'
  require 'uri/https'
  require 'net/http'
  require 'will_paginate/array'
  include ApplicationHelper
  include StripeHandler

  # Using Merchant Layout for Merchant namespace views
  layout "merchant"

  # protect_from_forgery with: :null_session

  # rescue_from ActionController::InvalidAuthenticityToken do |exception|
  #   sign_out
  #   redirect_to '/'
  # end

  # all child controllers will automatically enforce access to merchants only
  before_action :authenticate_user!, :set_wallet_balance, :require_merchant, :verify_tos_acceptance, :set_merchant_permission, :permitted_routes
  skip_before_action :require_merchant, :set_wallet_balance, only: [:show_wallet_balance, :verify_user]

  def verify_user
    params[:email]=params[:email].downcase if params[:email].present?
    params[:phone_number]=params[:phone_number].downcase if params[:phone_number].present?
    wallet=''
    if params[:user_phone].present? && !params[:user_phone].blank?
      @user = User.unarchived_users.where.not(:role => :user).where(phone_number: params[:user_phone]).first
    elsif params[:ref_no].present? && !params[:ref_no].blank?
      @user = User.unarchived_users.where(ref_no: params[:ref_no].upcase).where.not(:role => :user).first
    elsif  params[:email].present? && params[:password].present?
    # @user =  User.unarchived_users.where.not(:role => :user).find_by_email(params[:email])
    @user = current_user
      if @user.valid_password?(params[:password])
        @user = @user
      else
        @user= nil
      end
    elsif  params[:email].present?
      @user =  User.unarchived_users.where.not(role: 'user').find_by_email(params[:email])
      if @user.present?
        @user= @user
      else
        @user= nil
      end
    end
    if @user.present?
      @user.wallets.present? ? (wallet= @user.wallets.first) : (wallet = "No wallet")
      render status: 200, json:{success: 'Verified', :user => @user, :wallet => wallet, qr_codes: params[:qr_codes]} #used qr codes to be used in redeem qr functionality
    else
      render status: 200, json:{success: "not found",error: 'You are not Authorized for this action.'}
    end
  end

  def verifying_user
    phone_number = params[:phone_number]
    email = params[:email]
    table = User
    if email.present?
      email = email.downcase
      user = table.where.not(role:'user').complete_profile_users.where(email: email).first if params[:not_user] != 'notuser'
      user = table.where.not(role:'user').complete_profile_users.where(email:email).first if params[:not_user]=='notuser'
      ids=user.id if user.present?
      if user.present? && ids != params[:user_id].to_i && !user.archived?
        render status: 200, json:{success: 'notverified'}
      else
        render status: 200, json:{success: 'Verified', :user => @user}
      end
    elsif phone_number.present?
      user = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).first
      ids = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      res = Phonelib.valid?(phone_number)
      message = !res ? "Invalid Phone Number" : "A user with this phone number already exists"
      if ids.present? && ids.count > 0 && !ids.include?(params[:user_id].to_i)  && !user.archived? || !res
        render status: 200, json:{success: 'notverified',message: message}
      else
        render status: 200,json:{success: 'Verified', :user => @user}
      end
    end
  end
  def verifying_user_phone_email
    phone_number = params[:phone_number]
    email = params[:email]
    table = User
    if email.present? && params[:user_id].present?
      email = email.downcase
      user = table.where.not(role:'user').complete_profile_users.where(email: email).first if params[:not_user] != 'notuser'
      user = table.where.not(role:'user').complete_profile_users.where(email:email).first if params[:not_user]=='notuser'
      ids=user.id if user.present?
      if user.present? && ids != params[:user_id].to_i && !user.archived?
         render status: 200, json:{success: 'notverified'}
      else
        render status: 200, json:{success: 'Verified', :user => @user}
      end
    elsif phone_number.present? && params[:user_id].present?
      user = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).first
      ids = table.where.not(role: 'user').complete_profile_users.where(phone_number: phone_number).pluck(:id).uniq
      res = Phonelib.valid?(phone_number)
      message = !res ? "Invalid Phone Number" : "A user with this phone number already exists"
      if ids.present? && ids.count > 0 && !ids.include?(params[:user_id].to_i)  && !user.archived? || !res
        render status: 200, json:{success: 'notverified',message: message}
      else
        render status: 200,json:{success: 'Verified', :user => @user}
      end
    end
  end

  def valid_location
    if params[:wallet_id].present?
      @wallet = Wallet.find params[:wallet_id]
      if @wallet.present?
        render status: 200, json: {success: true , location: @wallet.location}
      else
        render status: 404, json: {success: false}
      end
    end
  end

  def show_wallet_balance
    wallet_id = params[:wallet_id]
    balance = show_balance(wallet_id)
    location = Wallet.find(wallet_id).location
    render status: 200 , json:{:balance => number_with_precision(balance.to_f, precision: 2), location: location.block_withdrawal}
  end

  def verify_tos_acceptance
    return unless current_user.merchant? || current_user.iso? || current_user.agent? || current_user.affiliate?

    if current_user.merchant_id.blank? && current_user.tos_checking == false
      return redirect_to '/'
    elsif current_user.merchant_id.present?
      user = User.find(current_user.merchant_id)
      if user.tos_checking == false
        if current_user.try(:permission).admin?
          return redirect_to '/'
        else
          redirect_to virtual_terminal_merchant_sales_path
        end
      end
    end
    if current_user.iso? || current_user.agent? || current_user.affiliate?
      if current_user.tos_checking == false
        return redirect_to merchant_transactions_path
      end
    end
  end

  def permitted_routes
    if @user.present? && @user.merchant? && session[:previous_url].present? && @user_permission.present? && !@user_permission.admin?
      allowed = true
      case params["controller"]
      when "merchant/instant_ach"
        allowed = false unless @user_permission.try(:ach)
      when "merchant/checks"
        if params[:action] == "index"
          allowed = false unless @user_permission.try(:check)
        elsif params[:action] == "debit_card_deposit"
          allowed = false unless @user_permission.try(:push_to_card)
        elsif params[:action] == "all_bulk_checks"
          allowed = false unless @user_permission.try(:check)
        end
      when "merchant/giftcards"
        allowed = false unless @user_permission.try(:gift_card)
      when "merchant/sales"
        if params[:action] == "sale_requests"
          allowed = false unless @user_permission.try(:qr_redeem) || @user_permission.try(:qr_scan) || @user_permission.try(:qr_view_only)
        elsif params[:action] == "virtual_terminal"
          allowed = false unless @user_permission.try(:virtual_terminal)
        end
      when "merchant/invoices"
        allowed = false unless @user_permission.try(:b2b)
      when "merchant/hold_money"
        allowed = false unless @user_permission.try(:funding_schedule)
      when "merchant/disputes"
        if params[:action] == "upload_evidence"
          allowed = false unless @user_permission.try(:dispute_submit_evidence)
        elsif params[:action] == "index"
          if @user_permission.try(:dispute_view_only) || @user_permission.try(:accept_dispute) || @user_permission.try(:dispute_submit_evidence)
          else
            allowed = false
          end
        end
      when "merchant/locations"
        if params[:action] == "transfers"
          allowed = false unless @user_permission.try(:transfer)
        elsif params[:action] == "index"
          allowed = false unless @user_permission.try(:wallet)
        elsif params[:action] == "transactions"
          allowed = false unless @user_permission.try(:wallet)
        end
      when "merchant/reports"
        if  @user_permission.admin? || @user_permission.custom?
          allowed = false
        else 
          if params[:action] == "index"
            allowed = false unless @user_permission.try(:sales_report) 
          elsif params[:action] == "checks_report"
            allowed = false unless @user_permission.try(:checks_report)
          elsif params[:action] == "gift_cards_report"
            allowed = false unless @user_permission.try(:gift_card_report)
          end
        end
      when "merchant/supports"
        # allowed = false unless @user_permission.
      when "merchant/settings"
        allowed = false unless @user_permission.user_view_only || @user_permission.user_edit || @user_permission.user_add 
      when "merchant/permission"
        allowed = false unless @user_permission.admin? || @user.merchant_id.nil?
      when "merchant/plugins"
        # allowed = false unless @user_permission.funding_schedule
      when "merchant/apps"
        allowed = false unless @user_permission.try(:developer_app)
      end
      if allowed
        session[:previous_url] = request.env["REQUEST_PATH"]
      else
        flash[:error] = "You don't have required permission!"
        redirect_back fallback_location: session[:previous_url]
      end
    else
      session[:previous_url] = request.env["REQUEST_PATH"]
    end
  end

  private

  def validate_location
    if params[:wallet_id].present?
      @wallet = Wallet.find params[:wallet_id]
    elsif params[:from_id]
      @wallet = Wallet.where(id: params[:from_id]).first
    end
    if @wallet.present?
      @location = @wallet.location
      if @location.present? && @location.is_block
        flash[:error] = I18n.t("errors.withdrawl.blocked_location")
        redirect_back(fallback_location: root_path)
      end
    end
  end

  def require_merchant
    if current_user && current_user.merchant?
      @user=current_user
    elsif current_user && current_user.iso? || current_user.qc?
      @user=current_user
    elsif current_user && current_user.agent?
      @user=current_user
    elsif current_user && current_user.partner?
      @user = current_user
    elsif current_user && current_user.affiliate?
      @user = current_user
    else
      flash[:error] = I18n.t 'devise.failure.page_not_exist'
      redirect_to '/'
    end
  end
  def is_merchant?
    redirect_to root_path if current_user.role == "merchant"
  end
  def set_wallet_balance
    @balance = 0
    # @hold_money_amount = 0
    wallets = []
    # @reserve_balance = 0
    if current_user.present? && current_user.iso? || current_user.agent? || current_user.affiliate?
      @balance = show_balance(current_user.wallets.primary.first.id, current_user.ledger)
    elsif current_user.present? && current_user.qc?
      @balance = show_balance(current_user.wallets.first.id, current_user.ledger)
    end
    # if current_user.present? && !current_user.MERCHANT?
    #   # @balance = current_user.wallets.primary.pluck(:balance).map(&:to_f).sum
    #   @balance = show_balance(current_user.wallets.first.id, current_user.ledger)
    #   # if current_user.ISO? || current_user.AGENT? || current_user.PARTNER? || current_user.AFFILIATE?
    #   #   @balance = show_balance(current_user.wallets.first.id, current_user.ledger)
    #   # elsif current_user.admin? && current_user.company_id != nil
    #   #   @balance = show_balance(current_user.wallets.first.id, current_user.ledger)
    #   # end
    # elsif current_user.present? && current_user.merchant_id.present? && current_user.MERCHANT? && current_user.submerchant_type == "regular_user"
    #   # @balance = current_user.wallets.primary.pluck(:balance).map(&:to_f).sum
    #   # @reserve_balance = current_user.wallets.reserve.pluck(:balance).map(&:to_f).sum
    #   current_user.wallets.primary.each do |w|
    #     @balance = @balance + show_balance(w.id, current_user.ledger)
    #     wallets << w.id
    #   end
    #   current_user.wallets.reserve.each do |w|
    #     @reserve_balance = @reserve_balance + show_balance(w.id, current_user.ledger)
    #   end
    #   # locations = current_user.locations
    #   # unless locations.nil?
    #   #   locations.each do |l|
    #   #     l.wallets.where.not(wallet_type: 'tip').where.not(wallet_type: 'reserve').each do |w|
    #   #       @balance= @balance+show_balance(w.id, current_user.ledger)
    #   #     end
    #   #     l.wallets.where(wallet_type: 'reserve').each do |wallet|
    #   #       @reserve_balance = @reserve_balance + show_balance(wallet.id, current_user.ledger)
    #   #     end
    #   #   end
    #   # end
    # elsif current_user.present? && current_user.merchant_id.present? && current_user.MERCHANT? && current_user.try(:permission).admin?
    #   user = User.find(current_user.merchant_id)
    #   # @balance = user.wallets.primary.pluck(:balance).map(&:to_f).sum
    #   # @reserve_balance = user.wallets.reserve.pluck(:balance).map(&:to_f).sum
    #   user.wallets.primary.each do |w|
    #     @balance = @balance + show_balance(w.id, current_user.ledger)
    #     wallets << w.id
    #     # @hold_money_amount = @hold_money_amount + HoldInRear.calculate_pending(w.id)
    #   end
    #   user.wallets.reserve.each do |wallet|
    #     @reserve_balance = @reserve_balance + show_balance(wallet.id, current_user.ledger)
    #   end
    #   # locations = Location.where(merchant_id: User.find(current_user.merchant_id).id)
    #   # unless locations.nil?
    #   #   locations.each do |l|
    #   #     l.wallets.where.not(wallet_type: 'tip').where.not(wallet_type: 'reserve').each do |w|
    #   #       @balance= @balance+show_balance(w.id, current_user.ledger)
    #   #     end
    #   #     l.wallets.where(wallet_type: 'reserve').each do |wallet|
    #   #       @reserve_balance = @reserve_balance + show_balance(wallet.id, current_user.ledger)
    #   #     end
    #   #   end
    #   # end
    # else
    #   current_user.wallets.primary.each do |wallet|
    #     @balance = @balance + show_balance(wallet.id, current_user.ledger)
    #     wallets << wallet.id
    #   end
    #   current_user.wallets.reserve.each do |wallet|
    #     @reserve_balance = @reserve_balance + show_balance(wallet.id, current_user.ledger)
    #   end
    #   # @balance = current_user.wallets.primary.pluck(:balance).map(&:to_f).sum
    #   # @reserve_balance = current_user.wallets.reserve.pluck(:balance).map(&:to_f).sum
    #   # locations = Location.where(merchant_id: current_user.id)
    #   # unless locations.nil?
    #   #   locations.each do |l|
    #   #     l.wallets.where.not(wallet_type: 'tip').where.not(wallet_type: 'reserve').each do |w|
    #   #       @balance= @balance+show_balance(w.id, current_user.ledger)
    #   #     end
    #   #     l.wallets.where(wallet_type: 'reserve').each do |wallet|
    #   #       @reserve_balance = @reserve_balance + show_balance(wallet.id, current_user.ledger)
    #   #     end
    #   #   end
    #   # end
    # end
    # @hold_money_amount = HoldInRear.calculate_pending_total(wallets)
    # @balance = @balance - @hold_money_amount
    if current_user.present? && current_user.merchant_id.present? && current_user.MERCHANT? && current_user.submerchant_type == "admin_user"
      parent_user = User.find(current_user.merchant_id)
      @notifications = Notification.where(read_at: nil,recipient_id: parent_user.id, notifiable_type: "Request" )
    else
        @notifications = Notification.where(read_at: nil,recipient_id: current_user.id, notifiable_type: "Request" ).where.not(status:['rejected','approved']).or(Notification.where(status: ['approved','rejected'],actor_id: current_user.id, notifiable_type: "Request" ))
                           # .or(Notification.where(actor_id: current_user.id, notifiable_type: "Request" ))
        @chat_notification=Message.where(recipient_id:current_user.id).where(read_at:nil).count
    end
  end

  def set_merchant_permission
    @user_permission = (current_user.try(:merchant_id) && current_user.try(:permission_id)).present? ? current_user.try(:permission) : Permission.admin.first
  end

end
