class Merchant::HoldMoneyController < Merchant::BaseController

  def index
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @heading = "Schedule Details"
    if params[:wallet_id].present?
      @user_wallets = Wallet.where(id: params[:wallet_id])
    else
      if current_user.merchant_id.present?
        @user_wallets = @user.wallets.primary.where.not(location_id: nil)
      else
        @user_wallets = @user.wallets.primary
      end
    end
    @locations = @user.get_locations
    @user_location_ids = @user_wallets.pluck(:location_id)
    if params[:query].present? && params[:location].present?
      date = parse_reports_date(params[:query][:date])
      location = params[:location]
      @hold_rear = HoldInRear.by_location_ids(location).where(created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    elsif params[:query].present?
      date = parse_reports_date(params[:query][:date])
      @hold_rear = HoldInRear.by_location_ids(@user_location_ids).where(created_at: date[:first_date].to_datetime.utc..date[:second_date].to_datetime.utc).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    else
      @hold_rear = HoldInRear.is_created.by_location_ids(@user_location_ids).order(release_date: :desc).distinct.per_page_kaminari(params[:page]).per(page_size)
    end
  end

  def transactions
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    hold_in_rear = HoldInRear.find_by(id: params[:hold_in_rear_id])
    created_at = hold_in_rear.try(:created_at)
    wallet = hold_in_rear.location.wallets.primary.last
    # @transactions = Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["complete"], main_type:["sale"],action: ["transfer"],to: wallet.id).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["complete"], main_type:["sale"],sub_type:["credit_card","debit_charge"],to: wallet.id)).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["approved"], main_type:["sale"],sub_type:["debit_charge"],to: wallet.id)).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["refunded"], to: wallet.id)).distinct.per_page_kaminari(params[:page]).per(10)
    @transactions = Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["complete","approved"], main_type:["eCommerce","Virtual Terminal","Credit Card"],to: wallet.id).or(Transaction.where(created_at: HoldInRear.pst_beginning_of_day(created_at)..HoldInRear.pst_end_of_day(created_at), status: ["refunded"], to: wallet.id)).distinct.per_page_kaminari(params[:page]).per(page_size)
  end

  private

  def parse_reports_date(date)
    offset = DateTime.now.in_time_zone(cookies[:timezone]).utc_offset
    first_date = date[0..9]
    first_date = Date.strptime(first_date, '%m/%d/%Y')
    firstdate = Time.new(first_date.year,first_date.month,first_date.day,00,00,00, offset).utc
    firsttime = firstdate.strftime("%H:%M:%S.00Z")
    firstdate = firstdate.strftime("%Y-%m-%dT#{firsttime}")
    second_date = date[13..22]
    second_date = Date.strptime(second_date, '%m/%d/%Y')
    seconddate = Time.new(second_date.year,second_date.month,second_date.day,23,59,59, offset).utc
    secondtime = seconddate.strftime("%H:%M:%S.59Z")
    seconddate = seconddate.strftime("%Y-%m-%dT#{secondtime}")
    return {first_date: firstdate, second_date: seconddate}
  end

end
