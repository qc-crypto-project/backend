class Merchant::PermissionController < Merchant::BaseController
  before_action :set_permission, only: [:show,:edit,:update,:destroy]
  def index
    @heading = "Permissions List"
    page_size = params[:tx_filter].present? ? params[:tx_filter].to_i : 10
    if @user.merchant_id.present?
      @permissions = Permission.where('merchant_id IN (?) OR permission_type IN (?)',[@user.merchant_id], [0,1]).per_page_kaminari(params[:page]).per(page_size)
    else
      @permissions = Permission.where('merchant_id = ? OR permission_type IN (?)',@user.id, [0,1]).per_page_kaminari(params[:page]).per(page_size)
    end
    @filters = [10,25,50,100]
  end

  def new
    @permission = Permission.new
    respond_to do |format|
      format.js
    end
  end

  def create
    @permission = Permission.new(permission_params)
    if @user.merchant_id.present?
      @permission.merchant_id = @user.merchant_id
    else
      @permission.merchant_id = @user.id
    end
    if @permission.custom!
      flash[:success] = 'Permission successfully added!'
    else
      flash[:error] = @permission.errors.first
    end
    redirect_to merchant_permission_index_path
  end

  def show
    respond_to do |format|
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def update
    if @permission.update(permission_params)
      flash[:success] = 'Permission has been updated successfully!'
    else
      flash[:error] = @permission.errors.first
    end
    redirect_to merchant_permission_index_path
  end

  def destroy
    @user = User.merchant.unarchived_users.where(permission_id: @permission.id) if @permission.present?
    if @user.present?
      flash[:error] = 'Permission Assigned to Employees. Please Delete them First!'
    else
      if @permission.delete
        flash[:success] = 'Permission has been Deleted successfully!'
      else
        flash[:error] = @permission.errors.first
      end
    end
    redirect_to merchant_permission_index_path
  end

  private

  def set_permission
    @permission = Permission.where(id: params[:id]).first if params[:id].present?
  end

  def permission_params
    params.require(:permission).permit(:name,:wallet,:refund,:transfer,:b2b,:virtual_terminal,:dispute_view_only,:dispute_submit_evidence,:accept_dispute,:funding_schedule,:check,:push_to_card,:ach,:gift_card,:sales_report,:checks_report,:gift_card_report,:user_view_only,:user_edit,:user_add,:developer_app,:merchant_id,:qr_scan,:qr_view_only,:qr_redeem)
  end

end
