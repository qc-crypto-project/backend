class Merchant::SupportsController < Merchant::BaseController

  skip_before_action :set_wallet_balance, only: [:create_ticket, :comment, :close_ticket]

  def index
    @heading = 'Support'
    @tickets= @user.tickets.order(updated_at: :desc).per_page_kaminari(params[:page]).per(10)
    @tickets= User.find(@user.merchant_id).tickets.order(updated_at: :desc).per_page_kaminari(params[:page]).per(10) if current_user.merchant_id.present? && current_user.try(:permission).admin?
    @ticket = Ticket.new
    render template: 'merchant/supports/index'
  end

  def create_ticket
    if params[:ticket][:message].present? && params[:ticket][:title].present?
      params[:ticket][:user_id] = current_user.merchant_id if current_user.merchant_id.present? && current_user.submerchant_type == "admin_user"
      ticket=Ticket.create(ticket_params)

      if params[:attachments].present?
        params[:attachments].first.split(',').each do |img|
          image=Image.find img
          ticket.images << image
        end
      end
      if ticket.errors.blank?
        flash[:success]  = I18n.t 'merchant.controller.successful_ticket'
        redirect_to merchant_supports_path
      else
        flash[:error]  = ticket.errors.full_messages.first
        redirect_to merchant_supports_path
      end
    end
  end

  def show
    @heading = "Ticket Details"
    @ticket = Ticket.find params[:id]
    @comments = @ticket.comments.order(created_at: :asc)
    @ticket.update(merchant_attention: false)
    # @comments = Comment.where(user_id: current_user.id).sort
    # p "sdkjksjdhkjashkjdas", @comments
    render template: 'merchant/supports/show'
  end

  def comment
    begin
      if params[:body].present? and params[:ticket_id].present?
        body =params[:body]
        ticket=Ticket.find params[:ticket_id]
        if current_user.merchant_id.present? && current_user.try(:permission).admin?
          comment= Comment.new(body: body , user_id: current_user.merchant_id ,ticket_id: ticket.id )
        else
          comment= Comment.new(body: body , user_id: current_user.id ,ticket_id: ticket.id )
        end

        if comment.save

          if params[:attachments].present?
            params[:attachments].first.split(',').each do |img|
              image=Image.find img
              comment.images << image
            end
          end

          Ticket.find(params[:ticket_id]).update(:get_attention => true, status: 'untreated')


          flash[:message]  = I18n.t('merchant.controller.message_add_comment')
          redirect_to merchant_support_path(ticket.id)
        else
          flash[:error] = comment.errors.full_messages.first
        end
    end
    rescue => e
      flash[:message]  = I18n.t('merchant.controller.message_comment_fail')
      redirect_to merchant_supports_path
    end
  end

  def close_ticket
    if ! params[:id].nil?
      @ticket = Ticket.find(params[:id])
      @ticket.update(status: "closed", get_attention: false)
      flash[:notice] = I18n.t('merchant.controller.closed')
      redirect_to merchant_supports_path
    else
      flash[:error] = I18n.t('merchant.controller.not_closed')
      redirect_to merchant_supports_path
    end
  end

  def open_ticket
    if ! params[:id].nil?
      @ticket = Ticket.find(params[:id])
      @ticket.update(status: "processed")
      flash[:notice] = I18n.t('merchant.controller.processed')
      redirect_to merchant_support_path(@ticket.id)
    else
      flash[:error] = I18n.t('merchant.controller.not_processed')
      redirect_to merchant_supports_path
    end
  end

  private

  def ticket_params
    params.require(:ticket).permit(:title, :message, :user_id)
  end
end
