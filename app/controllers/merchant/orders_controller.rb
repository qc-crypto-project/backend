class Merchant::OrdersController < Merchant::BaseController
  include OrdersHelper

  def index
    @wallet_id = params[:wallet_id].present? ? params[:wallet_id] : nil
    @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
    @risk_evaluation_status= [{value: "reject_manual_review", view:"Rejected by manual review"}, {value: "pending_manual_review", view: "Pending Manual Review"}, {value: "expired_manual_review", view: "Manual Review Expired"}, {value: "accept_manual_review", view: "Accepted by Manual Review"}, {value: "accept_default", view: "Accepted by default"}]

    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    conditions = []
    if params[:query].present? && params[:query].reject{|k,v| v.nil? || v == "" || v == {}}.present?
      conditions = shipment_search_query(params[:query],params[:offset])
    end
    if @wallet_id.blank?
      if current_user.merchant_id.present?
        @user_wallets = @user.wallets.primary.where.not(location_id: nil)
        @wallet_id = @user_wallets.pluck(:id)
      else
        @user_wallets = @user.wallets.primary
        @wallet_id = @user_wallets.pluck(:id)
      end
    end
    if params["query"].present? && params["query"]["risk_eval"].present?
      @txn = Transaction.joins(:tracker).joins(:minfraud_result).where(receiver_wallet_id: @wallet_id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    else
      @txn = Transaction.joins(:tracker).includes(:minfraud_result).where(receiver_wallet_id: @wallet_id).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    end
    @txn = @txn.distinct
    @totalTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet_id).group("trackers.status").count
    @totalTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet_id).group("trackers.status").sum(:total_amount)
    @totalNewTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet_id).where(trackers: {tracker_id: nil}).count
    @totalNewTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet_id).where(trackers: {tracker_id: nil}).sum(:total_amount)
    # @totalOverDueTxnCount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet_id).where("trackers.est_delivery_date < ?", Time.now).count
    # @totalOverDueTxnAmount = Transaction.joins(:tracker).where(receiver_wallet_id: @wallet_id).where("trackers.est_delivery_date < ?", Time.now).sum(:total_amount)
    @totalOverDueTxnCount = @totalTxnCount["overdue"]
    @totalOverDueTxnAmount = @totalTxnAmount["overdue"]

  end

  def new
    @tracker = Tracker.where(transaction_id: params[:t_id]).last
    render partial: "merchant/orders/add_tracking"
  end

  def update
    order = Tracker.find(params[:tracker_id] )
    begin
      p = Payment::EasyPostGateway.new
      ep_gateway = p.create_tracker(params[:track_cod])
      order.tracker_code = params[:track_cod]
      order.tracker_id = ep_gateway[:id]
      order.status = ep_gateway[:status]
      order.signed_by = ep_gateway[:signed_by]
      order.carrier = ep_gateway[:carrier]
      dates = ep_gateway[:tracking_details].present? ? tracker_dates(JSON.parse(ep_gateway[:tracking_details].to_json)) : nil
      order.shipment_date = dates.first.to_datetime if dates.present? && dates.first.present?
      order.delivered_date = dates.second.to_datetime if dates.present? && dates.second.present?
      order.est_delivery_date = ep_gateway[:est_delivery_date]
      order.tracking_details = ep_gateway[:tracking_details]

      order.save
    rescue Exception => exc
      flash[:error] = exc.message
    end

    redirect_to merchant_orders_path(wallet_id: params[:wallet_id])
  end

  def show
    @tracker = Tracker.find(params[:tracker_id])
    @order_status = @tracker.status
    render partial: "shared/tracking_detail"
  end

  def cancel
    #seq_id -- tracnsaction ,merchant reciver_id, nil -- cancel by this ID , logged in user
    transaction = Transaction.find(params[:t_id])
    txn_seq = transaction.seq_transaction_id
    merchant = transaction.receiver
    #@user is already available
    refund = RefundHelper::RefundTransaction.new(txn_seq,transaction,merchant,nil,params[:reason], @user)
    #check transaction not blank , amount is already refunded or not and get buy rate fee of merchant location and refund transaction to user
    response = refund.process
    if response[:success]
      tracker = Tracker.find(params[:tracker_id])
      tracker.cancel!
      if transaction.minfraud_result.present? && transaction.minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview
        transaction.minfraud_result.update(qc_action: TypesEnumLib::RiskType::Reject)
      end
    end
    redirect_to merchant_orders_path(wallet_id: params[:wallet_id])
  end

  def add_label
    transaction = Transaction.find(params[:t_id])
    if params[:step].blank?
      @warehouses = Address.where(address_type: "warehouse").map{|w|[w.name,w.id]}
      @to = transaction.shipping_address
      params[:step] = I18n.t('easypost.step.from_and_to')
    elsif params[:step] == I18n.t('easypost.step.rate')
    elsif params[:step] == I18n.t('easypost.step.complete')
      @tracker = Tracker.find(params[:tracker_id])
    end
  end

  def post_add_label
    begin
      easypost = Payment::EasyPostGateway.new(request, @user.id, @user.easypost_id.try(:[], "production_key"))
      if params[:step] == I18n.t('easypost.step.from_and_to')
        super_user = User.first
        if @user.easypost_id.blank?
          name = @user.name.present? ? @user.name : "#{@user.first_name} #{@user.lasst_name}"
          child_user = easypost.create_child_user(name, @user.phone_number, super_user.easypost_id.try(:[],:user_id))
          raise StandardError.new "#{child_user[:message].present? ? child_user[:message] : "Something went Wrong!"}" unless child_user[:success]
          super_user.update(easypost_id: {"user_id" => child_user[:response]["parent_id"]}) if super_user.easypost_id.try(:[],:user_id).blank?
          easypost_ids = {"user_id" => child_user[:response]["parent_id"],
                          "test_key" => child_user[:response]["apis_key"].select{|k,v| k[:mode] == "test"}.first[:key],
                          "production_key" => child_user[:response]["apis_key"].select{|k,v| k[:mode] == "production"}.first[:key]}
          @user.update(easypost_id: easypost_ids)
        end
        from = Address.new(from_and_to_params(params[:from]))
        to = Address.where(id: params[:to_id]).first_or_initialize
        to_params = from_and_to_params(params[:to])
        easypost_from = easypost.address(from.company, from.street, from.street, from.city, from.state, from.zip, from.phone_number, from.email)
        easypost_to = easypost.address(to_params[:company], to_params[:street], to_params[:street], to_params[:city], to_params[:state], to_params[:zip], to_params[:phone_number], to_params[:email])
        raise StandardError.new "Missing parameters!" if !easypost_from[:success] || !easypost_from[:success]
        from.address_code = easypost_from[:response]["id"]
        to_params["address_code"] = easypost_to[:response]["id"]
        from.save
        to.update(to_params)
        return redirect_to add_label_merchant_orders_path(t_id: params[:t_id],tracker_id: params[:tracker_id], step: I18n.t('easypost.step.rate'), from_address_id: from.address_code, to_address_id: to.address_code)
      elsif params[:step] == I18n.t('easypost.step.rate')
        @tracker = Tracker.find_by(id: params[:tracker_id])
        easypost_id = {
            to_address_id: params[:to_address_id],
            from_address_id: params[:from_address_id],
            parcel_id: params[:parcel_id],
            shipment_id: params[:shipment_id],
            rate_id: params[:shipping][:rate_id]
        }
        generate_label = easypost.generate_label(easypost_id[:shipment_id], easypost_id[:rate_id])
        raise StandardError.new "Missing parameters!" unless generate_label[:success]
        raise StandardError.new "Something went wrong, please try again later!" if validate_generate_label_response(generate_label[:response])
        easypost_id[:label_id] = generate_label[:response]["postage_label"]["id"]
        @tracker.label_url = generate_label[:response]["postage_label"]["label_url"]
        @tracker.tracker_code = generate_label[:response]["tracking_code"]
        @tracker.tracker_id = generate_label[:response]["tracker"]["id"]
        @tracker.carrier = params[:shipping][:carrier]

        ep_gateway = generate_label[:response]["tracker"]
        dates = ep_gateway[:tracking_details].present? ? tracker_dates(JSON.parse(ep_gateway[:tracking_details].to_json)) : nil
        @tracker.shipment_date = dates.first.to_datetime if dates.present? && dates.first.present?
        @tracker.delivered_date = dates.second.to_datetime if dates.present? && dates.second.present?

        @tracker.easypost_id = easypost_id
        @tracker.est_delivery_date = Time.now.utc + generate_label[:response]["selected_rate"]["est_delivery_date"].to_i.days
        @tracker.save
        create_fee_transaction(true)
        return redirect_to add_label_merchant_orders_path(t_id: params[:t_id],tracker_id: params[:tracker_id], step: I18n.t('easypost.step.complete'))
      end
    rescue StandardError => e
      flash[:notice] = e.message
      return redirect_to merchant_orders_path
    end
  end

  def get_rates
    message = "Something went wrong, please try again later!"
    easypost = Payment::EasyPostGateway.new(request, @user.id, @user.easypost_id.try(:[], "production_key"))
    parcel = easypost.parcel(params[:length], params[:width], params[:height], params[:weight])
    if parcel[:success]
      shipment = easypost.create_shipment_get_rate(params[:to_address_id], params[:from_address_id], parcel[:response]["id"])
      if shipment[:success]
        rates = shipment[:response]["rates"]
        rates = rates.group_by{|r| r["carrier"]}
        if rates.present?
          carriers = rates.keys
          render status: 200, json:{success: 'Verified', :rates => rates, :carriers => carriers, parcel_id: parcel[:response]["id"], shipment_id: shipment[:response]["id"]}
        else
          if shipment[:response]["messages"].present?
            shipment[:response]["messages"].each do|m|
              message = m["message"] if m["carrier_account_id"].blank?
            end
          end
          render status: 404, json:{success: 'failed', message: message}
        end
      else
        render status: 404, json:{success: 'failed', message: message}
      end
    else
      render status: 404, json:{success: 'failed', message: message}
    end
  end

  def warehouse
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @warehouses = Address.where(address_type: "warehouse").order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    @filters = [10,25,50,100]
  end

  def add_warehouse
    @warehouse = Address.where(id: params[:id]).first_or_initialize
    render partial: "merchant/orders/add_warehouse"
  end

  def post_warehouse
    begin
      address = Address.where(id: params[:address].try(:[],:id)).first_or_initialize
      params[:address][:phone_number] = "#{params[:phone_code]}#{params[:phone_number].gsub('+','')}"
      params[:address][:name] = "#{params[:address][:first_name]} #{params[:address][:last_name]}"
      address.update(from_and_to_params(params[:address]).reject{|k,v| v.blank?})
      flash[:notice] = params[:user].try(:[],:id).present? ? "Update Successfully!" : "Created Successfully!"
    rescue => ex
    ensure
      redirect_back(fallback_location: root_path)
    end
  end

  def get_warehouse
    warehouse = Address.where(id: params[:id]).first
    if warehouse.present?
      render status: 200, json:{success: 'Verified', warehouse: warehouse}
    else
      render status: 404, json:{success: 'Verified'}
    end
  end

  def add_carrier_key

  end

  def carrier_key
    render partial: "merchant/orders/carrier_key"
  end

  #EZ1000000001	pre_transit
  #EZ2000000002	in_transit
  #EZ3000000003	out_for_delivery
  #EZ4000000004	delivered
  #EZ5000000005	return_to_sender
  #EZ6000000006	failure
  #EZ7000000007	unknown
  #
  #
  def from_and_to_params(params)
    params.permit(:name, :first_name, :last_name, :email, :phone_number, :street, :city, :state, :zip, :company, :country, :whom, :tracker_id, :address_type)
  end

  def validate_generate_label_response(response)
    if response["postage_label"].try(:[],"id").blank? || response["postage_label"].try(:[],"label_url").blank? || response["tracking_code"].blank? || response["tracker"].try(:[],"id").blank?
      return true
    else
      return false
    end
  end

end