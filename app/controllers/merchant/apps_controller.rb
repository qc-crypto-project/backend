class Merchant::AppsController < Merchant::BaseController

  def index
    @heading = 'Apps'
    if current_user.merchant_id.present? 
      @apps=OauthApp.where(user_id: current_user.merchant_id).per_page_kaminari(params[:page]).per(10).order("id DESC")
    else
      @apps=OauthApp.where(user_id: current_user.id).per_page_kaminari(params[:page]).per(10).order("id DESC")
    end
  end

  def create
    @app = OauthApp.new(app_params)
    if current_user.merchant_id.present? && current_user.submerchant_type == "admin_user"
      @app.user_id = current_user.merchant_id
    else
      @app.user_id = current_user.id
    end
    @app.key = 'ID' + random_token
    @app.secret = 'SE' + random_token
    @app.token = random_token
    if @app.save
      flash[:notice] = I18n.t 'merchant.controller.app_created'
      if current_user.MERCHANT?
        redirect_back(fallback_location: root_path)
      else
        redirect_back(fallback_location: root_path)
      end
    else
      flash[:notice] = I18n.t 'merchant.controller.app_saving'
      redirect_back(fallback_location: root_path)
    end
  end

  def new
  end

  def edit
  end

  def update
    @heading = "Application Settings"
    @app = OauthApp.find(params[:id])
    @app.update(key: 'ID' + random_token,secret:  'SE' + random_token ,token: random_token)
    respond_to :js
  end

  def show
    @heading = "Application Settings"
    @app = OauthApp.find params[:id]
  end

  def destroy
    @app = OauthApp.find(params[:id])
    if @app.destroy
      flash[:notice] = I18n.t 'merchant.controller.app_del'
      redirect_to root_path
    end
  end

  def show_app
    @app = OauthApp.find(params[:user_id])
    render partial: 'shared/app_modal', locals: { user: @app }
  end

  def update_valid
    @app = OauthApp.find(params[:id])
    @app.is_block = params[:is_block]
    @app.save(:validate => false)
  end

  private

  def app_params
    params.require(:oauth_app).permit(:name, :description, :redirect_uri, :website, :privacy_url, :tos, :organization_name, :organization_web)
  end
end
