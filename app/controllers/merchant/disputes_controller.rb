class Merchant::DisputesController < Merchant::BaseController
  include TransactionCharge::ChargeATransactionHelper
  before_action :check_dispute_issue,only: [:index]
  def index
    if @user.merchant? && @user.merchant_id.nil?
      @wallets = current_user.wallets.primary.order('id ASC')
    elsif @user.merchant_id.present?
      @wallets = @user.wallets.primary.where.not(location_id: nil).order('id ASC')
    end
    @wallet_ids = @wallets.pluck(:id)
    @list=[]
    @list << {
        wallet_id: @wallets.pluck(:id),
        dba_name: 'All Locations'
    }
    @wallets.each do|w|
      @location = w.location
      business_name = @location.try(:business_name)
        @list <<
            {
                wallet_id: w.id,
                dba_name: business_name
            }
    end
    page_size = params[:tx_filter2].present? ? params[:tx_filter2].to_i : 10
    if params[:dispute_id].present?
      #it presents when merchant redirect to disputes from email received
      @dispute_case=DisputeCase.where(merchant_wallet_id: @wallet_ids,id:params[:dispute_id])
    else
      if params[:q].present? || params[:date_query].present?
        wallet_ids= params[:location_ids].present? ? JSON.parse(params['location_ids']) : @wallet_ids
        date = params[:date_query].present? ? parse_daterange(params[:date_query]) : parse_date("#{Date.current.beginning_of_month.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
        date = {
            first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
            second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).utc
        }
        @dispute_case=DisputeCase.where(merchant_wallet_id:wallet_ids).where(created_at: date[:first_date]..date[:second_date])
      else
        if params[:query].present?
          if params[:query]['location'].present?
            wallet_ids = JSON.parse(params[:query]['location'])
          end
          # if params[:query]['date'].present?
            date = params[:query]['date'].present? ? parse_daterange(params[:query]['date']) : parse_date("#{Date.current.beginning_of_month.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
            date = {
                first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
                second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).utc
            }
          # end
          @dispute_case=DisputeCase.where(merchant_wallet_id:wallet_ids).where(created_at: date[:first_date]..date[:second_date])
        else
          @dispute_case=DisputeCase.where(merchant_wallet_id:@wallet_ids).where('dispute_cases.created_at > ?',Date.current.beginning_of_month.beginning_of_day)
           # @dispute_case=DisputeCase.where(merchant_wallet_id:wallet_ids).where('dispute_cases.created_at > ?','01/08/2019'.to_datetime)
        end
        if params[:query].present? && date.present?
          total_transactions = Transaction.where(receiver_wallet_id: wallet_ids,main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::CreditCard], created_at: date[:first_date]..date[:second_date], status: ["approved", "refunded", "complete"]).count
        else
          total_transactions = Transaction.where(receiver_wallet_id: @wallet_ids,main_type: [TypesEnumLib::TransactionViewTypes::Ecommerce,TypesEnumLib::TransactionViewTypes::VirtualTerminal,TypesEnumLib::TransactionViewTypes::PinDebit,TypesEnumLib::TransactionViewTypes::CreditCard], created_at: Date.current.beginning_of_month..Date.today, status: ["approved", "refunded", "complete"]).count
        end
        # total_transactions = Transaction.where(sender_wallet_id: wallet_ids,status: ["approved", "refunded", "complete"]).where('created_at>= ?',Date.current.beginning_of_month.beginning_of_day).count+(Transaction.where(from: wallet_ids,status: ["CBK Won", "Chargeback", "cbk_fee"])).count+(Transaction.where(receiver_wallet_id: wallet_ids,status: ["approved", "refunded", "complete"])).where('created_at>= ?',Date.current.beginning_of_month.beginning_of_day).count+(Transaction.where(to: wallet_ids,status: ["CBK Won", "Chargeback", "cbk_fee"])).where('created_at>= ?',Date.current.beginning_of_month.beginning_of_day).count
        d=Date.today
        t1 = Time.new(d.year, d.month, d.day, 0,0,0)
        t2 = Time.new(d.year, d.month, d.day, 23,59)
        t3=Time.new(d.year,d.month,1,0,0,0)
        @amount_sum=0
        @dispute_case.each do |item|
          @amount_sum+=item.amount
        end
        @today_cbk=@dispute_case.where('created_at BETWEEN :first AND :second',first: t1, second: t2).count
        @month_cbk=@dispute_case.where('created_at BETWEEN :first AND :second',first: t3, second: t2).count
        cbk_perc=@dispute_case.count
        if cbk_perc > 0 && total_transactions > 0
        @cbk_perc=((cbk_perc.to_f/total_transactions.to_f)*100).to_i
        end
      end
    end
    dispute_case_status=nil
    dispute_case_type=nil
    if params[:q].present?
      dispute_type_keys=DisputeCase.dispute_types.keys
      dispute_type_keys.select!{|a| a.include?(params[:q].downcase.tr(" ","_"))}
      if dispute_type_keys.present?
        dispute_case_type=DisputeCase.dispute_types[dispute_type_keys.first]
      end
      status_keys = DisputeCase.statuses.keys
      status_keys.select!{|a| a.include?(params[:q].downcase.tr(" ","_"))}
      if status_keys.present?
        dispute_case_status = DisputeCase.statuses[status_keys.first]
      end
    end
    if params[:q].present?
      params[:q]={case_number_or_card_number_or_amount_eq:params[:q]}
      if dispute_case_status.present?
        params[:q]={status_eq:dispute_case_status}
      elsif dispute_case_type.present?
        params[:q]={dispute_type_eq:dispute_case_type}
      else
      params[:q]['merchant_wallet_location_business_name_cont']=  params[:q]['case_number_or_card_number_or_amount_eq']
      end
    end
    @q = @dispute_case.ransack(params[:q].try(:merge, m: 'or'))
    @dispute_case=@q.result().order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    if params[:tx_filter].present? && params[:tx_filter]=="All Cases" || params[:tx_filter].nil?
      @dispute_case
    elsif params[:tx_filter].present? && params[:tx_filter]=="Dispute Accepted"
      @dispute_case=@dispute_case.dispute_accepted.order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    elsif params[:tx_filter].present? && params[:tx_filter]=="Lost"
      @dispute_case=@dispute_case.lost.order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    elsif params[:tx_filter].present? && params[:tx_filter]=="Sent/Pending"
      @dispute_case=@dispute_case.sent_pending.order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    elsif params[:tx_filter].present? && params[:tx_filter]=="Under Review"
      @dispute_case=@dispute_case.under_review.order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    elsif params[:tx_filter].present? && params[:tx_filter]=="Won/Reversed"
      @dispute_case=@dispute_case.won_reversed.order(id: "desc").per_page_kaminari(params[:page]).per(page_size)
    end
    @filters = ["All Cases","Dispute Accepted","Lost","Sent/Pending","Under Review","Won/Reversed"]
    @filters2=[10,25,50,100]
    # @dispute_case= @dispute_case.where('dispute_cases.created_at > ?','01/08/2019'.to_datetime)
    @sent_pending=@dispute_case.sent_pending.count
    @lost= @dispute_case.lost.count
    @under_review= @dispute_case.under_review.count
    @won_reversed= @dispute_case.won_reversed.count
    @dispute_accepted= @dispute_case.dispute_accepted.count
    @dispute_case.where(read_at: nil).update(read_at:Date.today) if @dispute_case.present?
    # DisputeCase.where(merchant_wallet_id:wallet_ids, read_at: nil).where('dispute_cases.created_at > ?','01/08/2019'.to_datetime).update(read_at:Date.today) if @dispute_case.present?
    @graph={sent_pending:@dispute_case.sent_pending.count,under_review:@dispute_case.under_review.count,lost:@dispute_case.lost.count,won_reversed:@dispute_case.won_reversed.count,dispute_accepted:@dispute_case.dispute_accepted.count}
    @graph=@graph.reject{|k,v| v  == 0}
    respond_to do |format|
      format.js
      format.html
    end
  end


  def export
    if @user.merchant? && @user.merchant_id.nil?
      wallet_ids=current_user.wallets.primary.pluck(:id)
    elsif @user.merchant_id.present? && @user.try(:permission).admin?
      @parent = User.find_by(id: @user.merchant_id)
      wallet_ids = @parent.wallets.primary.pluck(:id)
    end
    wallet_ids= params[:location_ids].present? ? JSON.parse(params['location_ids']) : wallet_ids
    date = params[:date_query].present? ? parse_daterange(params[:date_query]) : parse_date("#{Date.current.beginning_of_month.strftime("%m/%d/%Y")} - #{Date.today.strftime("%m/%d/%Y")}")
    date = {
        first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
        second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).utc
    }
    if params[:status].present?
      status=params[:status]=='Lost' ? 'lost' : params[:status]=='Dispute Accepted' ? 'dispute_accepted' : params[:status]=='Sent/Pending' ? 'sent_pending' : params[:status]=='Under Review'? 'under_review' : params[:status]=='Won/Reversed'?  'won_reversed' : ''
    end
    if status.present?
      @dispute_case=DisputeCase.where(merchant_wallet_id:wallet_ids,status:status).where(created_at: date[:first_date]..date[:second_date])
    else
      @dispute_case=DisputeCase.where(merchant_wallet_id:wallet_ids).where(created_at: date[:first_date]..date[:second_date])
    end
    if params[:q].present?
      # @dispute_case=DisputeCase.where(merchant_wallet_id:wallet_ids)
      dispute_type_keys=DisputeCase.dispute_types.keys
      dispute_type_keys.select!{|a| a.include?(params[:q].downcase.tr(" ","_"))}
      if dispute_type_keys.present?
        dispute_case_type=DisputeCase.dispute_types[dispute_type_keys.first]
      end
      status_keys = DisputeCase.statuses.keys
      status_keys.select!{|a| a.include?(params[:q].downcase.tr(" ","_"))}
      if status_keys.present?
        dispute_case_status = DisputeCase.statuses[status_keys.first]
      end
      params[:q]={case_number_or_card_number_or_amount_eq:params[:q]}
      if dispute_case_status.present?
        params[:q]={status_eq:dispute_case_status}
      elsif dispute_case_type.present?
        params[:q]={dispute_type_eq:dispute_case_type}
      else
        params[:q]['merchant_wallet_location_business_name_cont']=  params[:q]['case_number_or_card_number_or_amount_eq']
      end
      @q = @dispute_case.ransack(params[:q].try(:merge, m: 'or'))
      @dispute_case=@q.result().order(id: "desc")
    end
    # dispute_cases=dispute_cases.where('dispute_cases.created_at > ?','01/08/2019'.to_datetime)
     send_data generate_csv(@dispute_case), filename: "DisputeCases.csv"
  end

  def generate_csv(dispute_cases)
    CSV.generate do |csv|
      csv << %w{ DBA_Name Case_Number Dispute_Type Received_Date Due_Date Amount Last4 Status}
      dispute_cases.each do |item|
        csv << [item.merchant_wallet.location.business_name, item.try(:case_number), item.try(:dispute_type).try(:humanize).try(:titleize), item.recieved_date.nil? ? "NA": item[:recieved_date].strftime("%m-%d-%Y"),item.due_date.nil? ? "NA": item[:due_date].strftime("%m-%d-%Y"), number_with_precision(item.try(:amount),precision: 2), item.try(:card_number), item.try(:status)]
      end
    end
  end

  def accept_dispute
    dispute=DisputeCase.find_by(id:params[:dispute_id])
    dispute_case_transaction = DisputeCaseTransaction.new
    if dispute.present?
      if dispute.charge_back? and (dispute.sent_pending?)
        dispute.status='dispute_accepted'
        dispute_case_transaction.transfer_charge_back_lost(dispute,request.try(:remote_ip),'merchant_request')
        dispute.save
        flash[:success]=I18n.t 'merchant.controller.successful_dispute_accept'
        # UserMailer.merchant_dispute(dispute).deliver_later
        trans=Transaction.find_by(id:dispute.transaction_id)
        UserMailer.admin_dispute_email(dispute,trans.tags.try(:[],'location').try(:[],'business_name'),'accept_dispute').deliver_later
        flash[:success]=I18n.t 'merchant.controller.successful_update'
      else
        flash[:success]=I18n.t 'merchant.controller.successful_error'
      end
    end
    redirect_back(fallback_location: root_path)
  end

  def upload_evidence
    @images=[]
    @user=[]
    if params[:trans_id].present?
      @transaction=Transaction.select(:sender_id,:ip).find(params[:trans_id])
      @user=User.find_by(id:@transaction.sender_id) if @transaction.present?
    end
    if params[:id].present?
      @evidence=Evidence.find_by(dispute_id:params[:id])
      if @evidence.present?
        @images=@evidence.images
        @url = merchant_dispute_path(id:params[:id])
      else
        @evidence=Evidence.new
        @url = submit_evidence_merchant_disputes_path
      end
    end
  end

  def submit_evidence
    @evidence=Evidence.new(evidence_docs)
    @evidence.shipping_date=Date.strptime(params[:evidence]['shipping_date'], '%m/%d/%Y') if params[:evidence]['shipping_date'].present?
    @evidence.save
    if params[:evidence]['image'].present?
      params[:evidence]['image'].each do |key,img|
        image={add_image:params[:evidence][:image][key],evidence_id:@evidence.id,file_type:key}
        upload_docs(image)
      end
    end
    dispute=DisputeCase.find_by(id:@evidence.dispute_id)
    trans=Transaction.find_by(id:dispute.transaction_id)
    UserMailer.admin_dispute_email(dispute,trans.tags.try(:[],'location').try(:[],'business_name'),nil).deliver_later
    flash[:success]=I18n.t 'merchant.controller.successful_evidence'
    redirect_back(fallback_location: root_path)
  end

  def upload_docs(image)
     @img= Image.new(image)
    if @img.validate
      @img.save
    else
      @error = @img.errors.full_messages.first
    end
  end

  def delete_doc
    img=Image.where(id:params[:id]).first
    if img.present?
      img.add_image.destroy()
      img.destroy
    end
  end

  def update
    @evidence=Evidence.find_by(id:params[:evidence]['id'])
    @evidence.update(evidence_docs)
    if params[:evidence]['image'].present?
      params[:evidence]['image'].each do |key,img|
        image={add_image:params[:evidence][:image][key],evidence_id:@evidence.id,file_type:key} if @evidence.try(:id).present?
        upload_docs(image)
      end
    end
    flash[:success]=I18n.t 'merchant.controller.successful_evidence_update'
    redirect_back(fallback_location: root_path)
  end

  private
  def evidence_docs
    params.require(:evidence).permit(:document,:description,:email,:customer_ip,:name,:dispute_id,:shipping_date,:shipping_address,:tracking_number,:package_carrier,:billing_address,:file_type)
  end
  def doc_params
    params.require(:image).permit(:add_image,:file_type,:evidence_id)
  end

  def check_dispute_issue
    @accept_disputes = AppConfig.where(key: AppConfig::Key::IssueDisputeConfig).first
    @accept_disputes = AppConfig.new(key: AppConfig::Key::IssueDisputeConfig) unless @accept_disputes.present?
    if !@accept_disputes.boolValue
      redirect_to merchant_locations_path
    end
  end

end
