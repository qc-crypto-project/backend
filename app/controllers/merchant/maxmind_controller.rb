class Merchant::MaxmindController < Merchant::BaseController
  include Merchant::MaxmindHelper

  def index
    page_size = params[:tx_maxmind_filter].present? ? params[:tx_maxmind_filter].to_i : 10
    @filters = [10,25,50,100]
    @risk_evaluation_status= [{value: "reject_manual_review", view:"Rejected by manual review"}, {value: "pending_manual_review", view: "Pending Manual Review"}, {value: "expired_manual_review", view: "Manual Review Expired"}, {value: "accept_manual_review", view: "Accepted by Manual Review"}, {value: "accept_default", view: "Accepted by default"}]
    if current_user.merchant_id.present?
      @user_wallets = @user.wallets.primary.where.not(location_id: nil)
    else
      @user_wallets = @user.wallets.primary
    end

    if params.present? && params["query"].present?
      if params["query"]["date"].present? || params["risk_status"].present?
        conditions = transactions_search_query_maxmind_merchant(params)

        if conditions.present?
          @manual_review_transactions = Transaction.joins(:minfraud_result).where(receiver_wallet_id: @user_wallets.pluck(:id)).where(conditions).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
        else
          @manual_review_transactions = Transaction.joins(:minfraud_result).where(receiver_wallet_id: @user_wallets.pluck(:id)).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
        end
      else
        @manual_review_transactions = Transaction.joins(:minfraud_result).where(receiver_wallet_id: @user_wallets.pluck(:id)).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
      end
    else
      @manual_review_transactions = Transaction.joins(:minfraud_result).where(receiver_wallet_id: @user_wallets.pluck(:id)).order(created_at: :desc).per_page_kaminari(params[:page]).per(page_size)
    end
  end

  def show
    @minfraud_result = MinfraudResult.find params[:id]
    @minfraud_insight = @minfraud_result.insight_detail

    render :partial => "show"
  end

  def update
    @minfraud_result = MinfraudResult.find params[:id]
    if @minfraud_result.qc_action == TypesEnumLib::RiskType::PendingReview &&
        @minfraud_result.qc_reason == TypesEnumLib::RiskReason::ManualReview
      if params[:new_action] == "a"
        @minfraud_result.qc_action = TypesEnumLib::RiskType::Accept
        @minfraud_result.save
        flash[:success] = I18n.t('maxmind.accepted')
      elsif params[:new_action] == "r"

        #Refunding transactions start
        transaction = Transaction.find(@minfraud_result.transaction_id)
        merch = transaction.receiver
        refund = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,nil,"Manual Reject by Merchant", @user)
        response = refund.process
        #Refunding transactions end

        if response[:success] == true
          @minfraud_result.qc_action = TypesEnumLib::RiskType::Reject
          @minfraud_result.save
          flash[:success] = I18n.t('maxmind.rejected')
        else
          flash[:error] = response[:message]
        end
      else
        flash[:error] = I18n.t('maxmind.invalid_params')
      end
    else
      flash[:error] = I18n.t('maxmind.invalid_request')
    end
    #redirect_back(fallback_location: root_path)
    respond_to do |format|
      format.html { redirect_back(fallback_location: root_path) }
      format.js
    end
  end

end