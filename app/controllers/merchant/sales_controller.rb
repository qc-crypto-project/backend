class Merchant::SalesController < Merchant::BaseController
  include ApplicationHelper
  include Merchant::SalesHelper
  include FluidPayHelper
  include Merchant::BaseHelper
  include RegistrationsHelper
  include Api::RegistrationsHelper
  include TransactionCharge::ChargeATransactionHelper
  before_action :allow_if_active , only: [:scanner]
  before_action :authenticate_transaction , only: [:submit_virtual_terminal]
  skip_before_action :set_wallet_balance, only: [:verify_email_and_phone_number, :submit_virtual_terminal]
  skip_before_action :verify_tos_acceptance, only: [:virtual_terminal]
  before_action :set_cache_headers, only: [:virtual_terminal]

  def index
  end
  def country
    if params[:country].present?
      @states = CS.states(params[:country])
      if request.xhr?
        respond_to do |format|
          format.json {
            render json: {states: @states}
          }
        end
      end
    end
  end

  def search_requests_query(params)
    conditions = []
    parameters = []

    if params[:last4].present?
      conditions << "last4 LIKE ?"
      parameters << "#{params[:last4].split.join}%"
    end
    if params[:first6].present?
      conditions << "first6 LIKE ?"
      parameters << "#{params[:first6].split.join}%"
    end

    if params[:amount].present?
      conditions << "total_amount = ?"
      parameters << params[:amount].to_f
    end

    if params[:phone].present?
      sender = User.find_by(phone_number: params[:phone])
      if sender.present?
        conditions << "requests.sender_id = ? "
        parameters << "#{sender.try(:id)}"
      end
    end
    [conditions.join(" AND "), *parameters]
  end

  def virtual_terminal
    access = merchant_allowed
    if access[:virtual_terminal]
      return redirect_to merchant_transactions_path
    end
    @heading = 'Virtual Terminal'
    @virtual_terminal = {:boolValue=>false}
    @wallets=Array.new
    location_status = false

    if @user.merchant_id.nil? || @user.iso? || @user.agent?
      @wallets = @user.wallets.primary.order('id ASC')
    elsif !@user.merchant_id.nil? && @user.MERCHANT?
      @wallets = @user.wallets.primary.reject{|v| v.location_id.nil?}
    else
      @main_user = current_user.parent_merchant
      @wallets = @main_user.wallets.primary.order('id ASC')
    end
    @list=[]
    @wallets.each do|w|
      @location = w.location
      location_status = @location.is_block
      if @location.virtual_terminal == false || @location.virtual_terminal == nil
        @list <<
            {
                wallet_id: w.id,
                wallet_name: w.name,
                location_status: location_status
            }
      end
      @new = 0
    end
    if @list.size == 1 && location_status
      flash[:danger] = I18n.t("errors.withdrawl.blocked_location")
    end
  end

  def verify_email_and_phone_number
    response_json = false
    response_name = ""
    response_first_name = ""
    response_last_name = ""
    response_email = ""
    response_number = ""
    response_address = ""
    response_zip_code = ""
    response_state = ""
    response_country = ""
    user_role = ""
    response_id = 0
    if params[:phone_number].present? && params[:phone_code].present?
      params[:phone_number].rstrip!
      phonenumber = params[:phone_code] + params[:phone_number]
      if User.user.exists?(phone_number: phonenumber) || User.user.exists?(phone_number: params[:phone_number])
        user = User.user.unarchived_users.where(phone_number: [phonenumber,params[:phone_number]])
        user = user.first
        if user.present?
          user_role = user.role
          if params[:for_new].present? || params[:for_existing].present?
            unless user_role == "user"
              response_json = false
            else
              response_json = true
            end
          else
            response_json = true
          end
          response_name = user.name
          response_first_name = user.first_name
          response_last_name = user.last_name
          response_email = user.email
          response_address = user.postal_address
          response_zip_code = user.zip_code
          response_city = user.city
          response_state = user.state
          response_country = user.country
          response_id = user.id
        else
          response_json = false
        end

      end
      if !Phonelib.valid?(phonenumber)
        response_phone_invalid = true
      end
    elsif params[:email].present?
      params[:email].rstrip!
      if User.user.exists?(email:params[:email])
        user = User.user.unarchived_users.where(email: params[:email])
        user = user.first
        # getting_user = user.user
        # if getting_user.blank?
        #   user = user.first
        # else
        #   user = getting_user.first
        # end
        if user.present?
          user_role = user.role
          if params[:for_new].present? || params[:for_existing].present?
            unless user_role == "user"
              response_json = false
            else
              response_json = true
            end
          else
            response_json = true
          end
          response_name = user.name
          response_number = user.phone_number
          response_id = user.id
        else
          response_json = false
        end
      end
    end
    render json:{response_json:response_json,name:response_name,first_name:response_first_name,last_name:response_last_name,email:response_email,phone_number:response_number,address: response_address,zip_code: response_zip_code,city: response_city, state: response_state, country: response_country,user_role: user_role,response_id:response_id, invalid_phone:response_phone_invalid},status:200
  end

  def card_number_verify
    if params[:card_number].present?
      card_number = params[:card_number].gsub(/\s+/, "")
      card_info = get_card_info(card_number.first(6))
      render json:{response_json:card_info, success:true},status: 200
    end
  end

  def submit_virtual_terminal
    begin
      card_lib = CardToken.new
      fee_lib = FeeLib.new
      merchant = current_user.parent_merchant
      merchant = merchant.nil? ? current_user : merchant
      if merchant.oauth_apps.present?
        oauth_app = merchant.oauth_apps.first
        if oauth_app.is_block
          flash[:notification_msg] = I18n.t('merchant.controller.blocked_by_admin')
          record_it(7, I18n.t('merchant.controller.blocked_by_admin'))
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
        end
      end
      card_number = params[:card_number].gsub(/\s+/, "")
      if params[:existing_user] == "true" #- params validation for existing user
        if not (params[:wallet_id].present? and params[:verified_user_id].present? and params[:card_name].present? and card_number.present? and params[:card_cvv].present? and params[:exp_date].present? and params[:amount].present?)
          flash[:notification_msg] = "Please fill out missing fields!"
          record_it(7, "Please fill out missing fields!")
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
        end
      else                                 #- params validation for new user
        if not (params[:wallet_id].present? and params[:card_name].present? and card_number.present? and params[:card_cvv].present? and params[:exp_date].present? and params[:amount].present? and params[:first_name].present? and params[:last_name].present? and params[:postal_address].present? and params[:country].present? and params[:city].present? and params[:state].present? and params[:zip_code].present? and params[:phone_number].present? and params[:email].present?)
          flash[:notification_msg] = "Please fill out missing fields!"
          record_it(7, "Please fill out missing fields!")
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
        end
      end

      if params[:phone_number].present? && params[:phone_code].present?
        params[:phone_number1] = "#{params[:phone_code]}#{params[:phone_number]}"
      end
      if params[:amount].to_f <= 0
        flash[:notification_msg] = "Amount must be greater then 0"
        record_it(7,"Amount must be greater then 0")
        return redirect_to virtual_terminal_merchant_sales_path
      end
      params["exp_date"] = params["exp_date"].split("/").join
      # getting wallet
      if params[:wallet_id].present?
        wallet = Wallet.where(id: params[:wallet_id]).first
        if wallet.present?
          @location = wallet.location
          # checking for location availabilty
          if @location.present?
            if @location.is_block
              flash[:notification_msg] =I18n.t("errors.withdrawl.blocked_location")
              record_it(7,I18n.t("errors.withdrawl.blocked_location"))
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            end
            if !@location.virtual_terminal.nil? && @location.virtual_terminal
              flash[:notification_msg] = I18n.t("api.registrations.unavailable")
              record_it(7,I18n.t("api.registrations.unavailable"))
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            end
            # checking for location sale limit, you are not allowed if sale is greater than limit
            if @location.sale_limit.present?
              if @location.sale_limit_percentage.present?
                sale_limit = @location.sale_limit
                sale_limit_percentage = @location.sale_limit_percentage
                total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
                if params[:amount].to_f > total_limit
                  flash[:notification_msg] = I18n.t("api.registrations.trans_limit")
                  record_it(7,I18n.t("api.registrations.trans_limit"))
                  return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
                end
              else
                if params[:amount].to_f > @location.sale_limit
                  flash[:notification_msg] = I18n.t("api.registrations.trans_limit")
                  record_it(7,I18n.t("api.registrations.trans_limit"))
                  return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
                end
              end
            end
          end
        end
        if params[:verified_user_id].present?     #= for existing user
          sender = User.find(params[:verified_user_id])
          if sender.present?
            params[:name] = "#{params[:first_name]} #{params[:last_name]}"
            sender.update(name: params[:name],first_name: params[:first_name], last_name: params[:last_name], email: params[:email], postal_address: params[:postal_address], city: params[:city],country: params[:country], state: params[:state], zip_code: params[:zip_code])
            sender_wallet = sender.wallets.first
            sender_wallet.update(name: params[:name])
          end
          if current_user.id == sender.id
            flash[:notification_msg] = "You can't make a sale with your own account"
            record_it(7,"You can't make a sale with your own account")
            return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
          end
          unless sender.role == "user"
            flash[:notification_msg] = "User Not Found. Please add new user!"
            record_it(7,"User Not Found. Please add new user!")
            return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
          end
          firstname = sender.first_name
          firstname = sender.name if firstname.nil?
          lastname = sender.last_name
          lastname = sender.name if lastname.nil?
          params[:first_name] = firstname
          params[:last_name] = lastname
          params[:email] = sender.email
          params[:phone_number] = sender.phone_number
          user = sender
        else                                    #= for new user
          user = User.unarchived_users.where(phone_number: [params[:phone_number],params[:phone_number1]])
          # getting_user = user.select{|a| a.role == "user"}.first if user.present?
          if user.blank?                #= for brand new users
            params[:name] = "#{params[:first_name]} #{params[:last_name]}"
            params[:phone_number] = "#{params[:phone_code]}#{params[:phone_number]}"
            sender = User.new(params.permit(:name, :email, :last_name, :first_name, :phone_number, :zip_code, :postal_address, :country, :city,:state))
            sender.source = merchant.name if merchant.present?
            sender.ledger = merchant.ledger if merchant.present? && merchant.ledger.present?
            @random_code = rand(1_000..9_999)
            password = "#{rand(1..10)}#{Devise.friendly_token.first(8)}"
            sender.password = password
            sender.pin_code = @random_code
            password = random_password
            sender.password = password
            sender.role = "user"
            sender.regenerate_token
            # if !sender.valid?
            unless sender.save(:validate => false)
              flash[:notification_msg] = sender.errors.full_messages.first
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            end
            # sender.user!
            # if sender.save!
            balance = 0
            # end
          else #= creating new user with merchant or iso or agent or aff or company parameters
            user = user.first
            if user.merchant? || user.iso? || user.agent? || user.affiliate? || user.partner?
              # if user.is_block? || user.archived
              #   flash[:notification_msg] = "Un-authorized user. Please contact you administrator for details."
              #   return redirect_to virtual_terminal_merchant_sales_path
              # end
              user = User.new(name: "#{params[:first_name]} #{params[:last_name]}", email: params[:email], last_name: params[:last_name], first_name: params[:first_name], phone_number: params[:phone_number], zip_code: user.zip_code, postal_address:user.postal_address, country:user.try(:country), city: user.city,state: user.state)
              user.source = @user.name if @user.present?
              user.role = :user
              @random_code = rand(1_000..9_999)
              user.pin_code = @random_code
              user.ledger = @user.ledger if @user.present? && @user.ledger.present?
              user.regenerate_token
              user.save(:validate => false)
              balance = 0
              sender = user
            else
              flash[:notification_msg] = "Sorry user not found!"
              record_it(7,"Sorry user not found!")
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            end
          end
        end
        #creating card for records
        card_info = {
            number: card_number,
            month: params[:exp_date].first(2),
            year: "20#{params[:exp_date].last(2)}",
            # cvv: params[:card_cvv]
        }

        card_block = check_card_blocked(card_number,"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}",card_info)
        if card_block
          msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{@user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_name]}\nC.C #: #{params[:card_number].first(6)}******#{params[:card_number].last(4)}\nReason: Error Code 3007 The Card is declined, please try another card."
          SlackService.notify(msg, "#qc-risk-alert")
          flash[:notification_msg] = I18n.t("api.registrations.invalid_card")
          record_it(7,I18n.t("api.registrations.invalid_card"))
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
          # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: }, :ok)
        end
        if @location.vip_card && !Card.check_vip_card?(card_number,"#{params[:exp_date].first(2)}/#{params[:exp_date].last(2)}")
          flash[:notification_msg] = I18n.t('api.errors.non_vip_card')
          record_it(7,I18n.t('api.errors.non_vip_card'))

          msg = "Date/time: #{Time.now.strftime("%m/%d/%Y")} \nMerchant ID: M-#{user.id} \nDBA Name: #{@location.try(:business_name)}\nAmount: #{number_to_currency(number_with_precision(params[:amount], :precision => 2, delimiter: ','))}\nCardholder Name: #{params[:card_name]}\nC.C #: #{card_number.first(6)}******#{card_number.last(4)}\nReason: #{I18n.t("api.errors.non_vip_card")}"
          SlackService.notify(msg, "#qc-risk-alert")
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
          # return render_json_response({:success => false, :status => TypesEnumLib::Statuses::Failed, message: }, :ok)
        end

        card_bin_list = get_card_info(card_number)
        if card_bin_list["bank"].present? && card_bin_list["bank"]["name"].present?
          card_bank = card_bin_list["bank"]["name"].present? ? card_bin_list["bank"]["name"] : ''
        end


        qcCard = Payment::QcCard.new(card_info, nil, sender.id)
        card = sender.cards.where(fingerprint: qcCard.fingerprint,merchant_id: @user.id).first
        if card.blank?
          card = sender.cards.new(qc_token:qcCard.qc_token,exp_date: "#{card_info[:month]}/#{card_info[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : qcCard.card_brand, last4: params[:card_number].strip.last(4).to_s, name: user.name, card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: params[:card_number].first(6).to_s,fingerprint: qcCard.fingerprint,merchant_id: @user.id,flag: "straight", bank: card_bank)
        else
          card.qc_token = qcCard.qc_token
          card.last4=params[:card_number].strip.last(4).to_s
          card.exp_date = "#{card_info[:month]}/#{card_info[:year].last(2)}"
          card.name = params[:card_name]
          card.brand = card_bin_list["scheme"].present? ? card_bin_list["scheme"] : qcCard.card_brand
          card.card_type = card_bin_list && card_bin_list["type"]
          card.merchant_id = @user.id
          card.bank = card_bank
        end
        card.save
        
        if check_card_decline(card)
          flash[:notification_msg] =  "Decline please try new card."
          record_it(7 , "Decline please try new card.")
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
        end
        
        params[:wallet_id] = sender.try(:wallets).try(:first).try(:id)
        params[:merchant_wallet_id] = wallet.try(:id)

        data = {}
        params[:both] = false
        gateway = @location.primary_gateway.present? ? @location.primary_gateway.key : merchant.payment_gateway
        merchant_balance = show_balance(wallet.id, merchant.ledger)
        buyRate = @location.fees.buy_rate.first
        amount_fee_check = check_fee_amount(@location, merchant, sender, wallet, buyRate, params[:amount], merchant_balance, nil, 'credit')
        if amount_fee_check.present? && amount_fee_check[:status] == false
          flash[:notification_msg] =  I18n.t("api.registrations.amount")
          record_it(7,I18n.t("api.registrations.amount"))
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
        end
        if amount_fee_check.present? && amount_fee_check[:status] == true
          if amount_fee_check[:splits].present?
            agent_fee = amount_fee_check[:splits]["agent"]["amount"]
            iso_fee = amount_fee_check[:splits]["iso"]["amount"]
            iso_balance = show_balance(@location.iso.wallets.primary.first.id)
            if amount_fee_check[:splits]["iso"]["iso_buyrate"].present? && amount_fee_check[:splits]["iso"]["iso_buyrate"] == true
              if iso_balance < iso_fee
                flash[:notification_msg] = I18n.t("errors.iso.iso_0007")
                record_it(7,I18n.t("errors.iso.iso_0007"))
                return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
              end
            else
              if iso_balance + iso_fee < agent_fee.to_f
                flash[:notification_msg] = I18n.t("errors.iso.iso_0008")
                record_it(7,I18n.t("errors.iso.iso_0008"))
                return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
              end
            end
          end
        end
        if sender.present?  # checking for user
          sender_wallet = sender.wallets.first
          balance = show_balance(sender_wallet.id, sender.ledger) # user wallet's balance
          fee_for_issue = 0
          if balance.to_f != params[:amount].to_f
            # if balance.to_f < params[:amount].to_f  # user's wallet doesnt have enough balance to sale
            params[:both] = true
            new_amount = params[:amount].to_f
            if merchant.present? && merchant.MERCHANT? && !merchant.issue_fee
              issue_amount = new_amount
            else
              issue_amount = new_amount + fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f
              fee_for_issue = fee_lib.get_card_fee(params[:amount].to_f, 'card').to_f
            end
            params[:issue_amount] = issue_amount.to_f
            qc_wallet = Wallet.qc_support.first
            issue_raw_transaction = IssueRawTransaction.new(sender.email,params,params[:card_cvv])
            seq_transaction_id = {:message => {:message => I18n.t('api.errors.no_gateway', location_contact_no: @location.try(:phone_number) , location_email: @location.try(:email)) }}
            # load balancer starts for issue amount to user
            # LOAD BALANCER FIRST payment_gateway
            if @location.primary_gateway.present?
              # LOAD BALANCER FIRST payment_gateway
              @payment_gateway = @location.primary_gateway if !@location.primary_gateway.is_block
              seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, sender, card_number,nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.secondary_gateway,card_bin_list)
              gateway = @payment_gateway.key if @payment_gateway.present?
              if @location.primary_gateway.is_block && seq_transaction_id.nil?
                seq_transaction_id = {:message => {:message => "Processing: Inactive – Please contact customer support."}}
              end
              # LOAD BALANCER secondary_payment_gateway
              if seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank? && @location.secondary_gateway.present? && !@location.secondary_gateway.is_block
                record_it(7, seq_transaction_id[:message][:message].present? ? seq_transaction_id[:message][:message] : "Decline From Bank")
                @payment_gateway = @location.secondary_gateway
                seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, sender, card_number,nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,@location.ternary_gateway,card_bin_list)
                gateway = @payment_gateway.key
              end
              # LOAD BALANCER third_payment_gateway
              if seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank? && @location.ternary_gateway.present? && !@location.ternary_gateway.is_block
                record_it(7, seq_transaction_id[:message][:message].present? ? seq_transaction_id[:message][:message] : "Decline From Bank")
                @payment_gateway = @location.ternary_gateway
                seq_transaction_id = load_balancer(issue_raw_transaction, nil, card, @user, qc_wallet, request, nil, nil, sender, card_number,nil,TypesEnumLib::DeclineTransacitonTpe::Ecommerce,fee_for_issue,TypesEnumLib::TransactionSubType::SaleIssueApi,nil,card_bin_list)
                gateway = @payment_gateway.key
              end
              if seq_transaction_id.try(:[],:id).present?
                if @payment_gateway.present?
                  @payment_gateway.update_daily_monthly_limits(seq_transaction_id[:issue_amount])
                end
              end
            end
            if seq_transaction_id.try(:[],:blocked).present?
              record_it(7, I18n.t('merchant.controller.unaval_feature'))
              flash[:notification_msg] = I18n.t('merchant.controller.unaval_feature')
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            elsif seq_transaction_id.try(:[], :decline_message).present?
              flash[:notification_msg] = I18n.t('errors.withdrawl.decline_use_new_card')
              record_it(7 , I18n.t('errors.withdrawl.decline_use_new_card'))
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            elsif seq_transaction_id.try(:[], :response).blank? && seq_transaction_id.try(:[], :id).blank?
              if params[:order_bank].present? && params[:charge_id].present?
                params[:order_bank].update(status: "seq_crash")
              end
              if seq_transaction_id.blank?
                record_it(7 , I18n.t('merchant.controller.unaval_feature'))
                flash[:notification_msg] = I18n.t('merchant.controller.unaval_feature')
              else
                flash[:notification_msg] = "#{seq_transaction_id[:message][:message]}"
              end
              record_it(7,seq_transaction_id[:message][:message].present? ? seq_transaction_id[:message][:message] : "Decline From Bank")
              return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
            end
          end
        end
        data = seq_transaction_id if seq_transaction_id.present?
        # creating object for previous refund
        data = data.merge(get_card_info(card_number)) unless data.empty?
        data.delete(:gateway_fee_details) if data[:gateway_fee_details].present?
        balance = show_balance(sender.wallets.first.id, sender.ledger)
        if balance.to_f >= params[:amount].to_f
          # sender = current_user
          # transfering amount to Merchant(seq transaction)
          # transaction = sender.transactions.build(to: params[:merchant_wallet_id].to_i, from: sender.wallets.first.id, status: "approved", charge_id: nil , amount: params[:amount].to_f, sender: current_user, action: 'issue', tags: seq_transaction_id.try(:[], :tags))
          # if transaction.present?
          #   transaction.save
          #   trans_id = transaction.id
          # end
          @payment_gateway = @location.primary_gateway if @payment_gateway.blank?
          gateway = @payment_gateway.key if gateway.blank?
          current_transaction = transaction_between(params[:merchant_wallet_id].to_i,sender.wallets.first.id,params[:amount].to_f, TypesEnumLib::TransactionType::SaleType,0, nil, data,sender.ledger, gateway,TypesEnumLib::TransactionSubType::SaleVirtual,card.card_type.try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction,nil,nil,nil,@payment_gateway)
          if current_transaction.nil? || current_transaction.size <=0
            flash[:error] = I18n.t('api.registrations.sequence')
            return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
          end
          maintain_batch(merchant.id, params[:merchant_wallet_id].to_i, params[:amount].to_f,TypesEnumLib::Batch::Primary)
          if merchant.high?
            if @location.iso.present?
              maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High,nil,nil,nil,nil,nil,nil,current_transaction[:bonus] || nil)
            end
            maintain_batch(merchant.id, params[:merchant_wallet_id].to_i, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::High, nil, @location.iso.id, true,current_transaction[:iso_fee] || nil,current_transaction[:agent_fee] || nil,current_transaction[:affiliate_fee] || nil)
          elsif merchant.low?
            if @location.iso.present?
              maintain_batch(@location.iso.id, @location.iso.wallets.first.id, params[:amount].to_f, TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low,nil,nil,nil,nil,nil,nil,current_transaction[:bonus] || nil)
            end
            maintain_batch(merchant.id, params[:merchant_wallet_id].to_i, params[:amount].to_f,TypesEnumLib::Batch::BuyRateCommission, nil, TypesEnumLib::Batch::Low, nil, @location.iso.id, true,current_transaction[:iso_fee] || nil,current_transaction[:agent_fee] || nil,current_transaction[:affiliate_fee] || nil)
          end
          if gateway.present?
            payment_gateway = PaymentGateway.find_by(key: gateway)
            if payment_gateway.present?
              message = sale_text_message(number_with_precision(params[:amount].to_f,precision:2), @location.business_name, payment_gateway.try(:name),card_number.last(4),sender.name, @location.cs_number || @location.phone_number)
              # Email/Text Notifications
              amount = number_to_currency(number_with_precision(params[:amount], precision: 2, delimiter: ','))
              UserMailer.transaction_email(@location.id,amount,(payment_gateway.try(:name) || '---'),sender.id,Date.today.to_json,card.brand,card_number.last(4),current_transaction[:id], params[:action]).deliver_later if @location.try(:email_receipt)
              # Sending Text Message
              twilio_text = send_text_message(message, sender.phone_number) if @location.try(:sms_receipt) && message && user.try(:phone_number).present?
            end
          end
          push_notification(message, sender)
        else
          flash[:notification_msg] =  I18n.t("merchant.controller.insufficient_balance")
          return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
        end
      else
        return redirect_to virtual_terminal_merchant_sales_path
      end
    rescue Stripe::CardError, Stripe::InvalidRequestError => e
      flash[:notification_msg] = "#{e.message}"
      return redirect_to virtual_terminal_merchant_sales_path
    rescue Timeout::Error => e
      flash[:error] = I18n.t 'merchant.controller.req_timedout'
      return redirect_to virtual_terminal_merchant_sales_path(request.parameters)
    rescue Exception => exc
      issue_raw_transaction = IssueRawTransaction.new(sender.email,params)
      if params[:converge_txn_id].present?
        elavon = Payment::ElavonGateway.new
        elavon.void(params[:converge_txn_id],@payment_gateway)
      end
      make_i_can_preauth_void(params[:i_can_pay_txn_id], request, params[:i_can_pay_txn_amount],@payment_gateway)
      make_bolt_pay_void(params[:bolt_pay_txn_id], nil,@payment_gateway)

      saving_decline_for_seq(issue_raw_transaction,@payment_gateway)
      record_it(7,"#{exc.message}")
      flash[:notification_msg] = "#{exc.message}"
      return redirect_to virtual_terminal_merchant_sales_path
    rescue StandardError => exc
      flash[:notification_msg] = exc
      return redirect_to virtual_terminal_merchant_sales_path
    else
      record_it(6, "Successfully Done!")
      flash[:notification_msg] = "Successfully Done!"
      return redirect_to virtual_terminal_merchant_sales_path
    end
  end

  def transactions
    access = merchant_allowed
    if access[:sales]
      return redirect_to merchant_transactions_path
    end
    @heading = 'Sales'
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.iso? || @user.agent?
      @wallets = @user.wallets.primary.order('id ASC')
    elsif !@user.merchant_id.nil? && @user.MERCHANT? && @user.try(:permission).regular?
      @wallets = @user.wallets.primary.reject{|v| v.location_id.nil?}
    else
      @main_user = current_user.parent_merchant
      @wallets = @main_user.wallets.primary.order('id ASC')
    end
    @list=[]
    @wallets.each do|w|
      @location = w.location
      if @location.sale_limit.present?
        if @location.sale_limit_percentage.present?
          sale_limit = @location.sale_limit
          sale_limit_percentage = @location.sale_limit_percentage
          total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
          @new = total_limit
        else
          @new = @location.sale_limit
        end
      else
        @new = 0
      end
      location_status = @location.is_block
      if @location.sales == false || @location.sales == nil
        @list <<
            {
                wallet_id: w.id,
                wallet_name: w.name,
                balance: @new.present? ? @new : 0,
                location_status: location_status
            }
      end
      @new = 0
    end
    render template: 'merchant/sales/new'
  end

  def sale_requests
    if @user.merchant_id.present?
      user_id = @user.merchant_id
    else
      user_id = @user.id
    end
    if params[:query].present?
      conditions = search_requests_query(params[:query])
      conditions = conditions.reject{|e| e == ""}
      if conditions.present?
        if @user.merchant_id.present?
          wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:id)
          @requests = Request.joins(qr_card: :card_transaction).where(reciever_id: user_id,wallet_id: wallets).order('requests.created_at DESC').per_page_kaminari(params[:page]).per(10)
        else
          @requests = Request.joins(qr_card: :card_transaction).where(reciever_id: user_id).where(conditions).order('requests.created_at DESC').per_page_kaminari(params[:page]).per(10)
        end
      end
    end
  end

  def scanner_sale
    if params[:qr_token].present?
      qr_card = QrCard.where(token: params[:qr_token]).last
      if qr_card.present?
        merchant_wallet = Wallet.where(id: qr_card.wallet_id).last
        if @user.merchant_id.present?
          allowed_wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:id)
          if !allowed_wallets.include? (merchant_wallet.try(:id))
            if params[:scanner].present?
              return render json: {success: false, message: "Location access denied!"}
            else
              flash[:error] = "Location access denied!"
              return redirect_to qr_codes_merchant_sales_path
            end
          end
        end
        requested_token = true
        if @user.merchant_id.present?
          requested_token = false if @user.merchant_id == qr_card.request.reciever_id
        else
          requested_token = false if @user.id == qr_card.request.reciever_id
        end
        if requested_token
          if params[:scanner].present?
            return render json: {success: false, message: "Invalid Token!"}
          else
            flash[:error] = "Invalid Token!"
            return redirect_to qr_codes_merchant_sales_path
          end
        end
        user_wallet = Wallet.where(id: qr_card.from_id).last
        location = merchant_wallet.location
        if location.is_block
          if params[:scanner].present?
            return render json: {success: false, message: I18n.t("errors.withdrawl.blocked_location")}
          else
            flash[:error] = I18n.t("errors.withdrawl.blocked_location")
            return redirect_to qr_codes_merchant_sales_path
          end
        end
        if qr_card.present? && qr_card.is_valid == true
          user = qr_card.user
          if @user.id == user.id
            if params[:scanner].present?
              return render json: {success: false, message: "You can't charge your own card!"}
            else
              flash[:error] = "You can't charge your own card!"
              return redirect_to qr_codes_merchant_sales_path
            end
          else
            if user && user_wallet.present?
              if merchant_wallet.present?
                amount = qr_card.price.to_f
                balance = show_balance(qr_card.from_id)
                if balance.to_f >= amount.to_f
                  issue_transaction = qr_card.card_transaction
                  gateway = issue_transaction.payment_gateway
                  card = issue_transaction.card
                  if issue_transaction.tip.to_f > 0
                    tip_details = {
                        sale_tip: issue_transaction.tip,
                        wallet_id: location.wallets.tip.first.id
                    }
                  end
                  # amount = (qr_card.price.to_f + issue_transaction.fee.to_f + issue_transaction.privacy_fee.to_f) - issue_transaction.tip.to_f
                  amount = qr_card.price.to_f  - issue_transaction.tip.to_f
                  data = {
                      "id" => issue_transaction.seq_transaction_id,
                      "transaction_id"=>issue_transaction.charge_id,
                      "timestamp"=>issue_transaction.timestamp,
                      "type"=>issue_transaction.main_type,
                      "sub_type"=>issue_transaction.sub_type,
                      "destination"=>issue_transaction.receiver_wallet_id,
                      "issue_amount"=>issue_transaction.total_amount,
                      "tags"=>issue_transaction.tags,
                      "fee"=>issue_transaction.fee,
                      "privacy_fee"=>issue_transaction.privacy_fee,
                      "tip"=>issue_transaction.tip,
                      "reserve_money"=>issue_transaction.reserve_money,
                      "card_id"=>issue_transaction.card_id,
                      "first6"=>issue_transaction.first6,
                      "last4"=>issue_transaction.last4,
                      "discount" => issue_transaction.discount
                  }
                  if issue_transaction.sub_type == "qr_debit_card"
                    batch_type = "debit"
                    main_type = TypesEnumLib::TransactionSubType::QRDebitCard
                  else
                    batch_type = "credit"
                    main_type = TypesEnumLib::TransactionSubType::QRCreditCard
                  end
                  current_transaction = transaction_between(qr_card.wallet_id,qr_card.from_id,amount.to_f, main_type,0, nil, data,@user.ledger, gateway.try(:key),nil,card.try(:card_type).try(:capitalize) || TypesEnumLib::TransactionType::CreditTransaction,card,tip_details,nil,gateway, card.try(:card_type), issue_transaction.try(:privacy_fee).to_f,nil,issue_transaction.amount, true )
                  if current_transaction.present?
                    Sidekiq::Status.cancel "#{qr_card.job_id}"
                    qr_card.is_valid = false
                    qr_card.save
                    request = qr_card.request
                    old_type = request.status.dup
                    request.update(status: "approved", qr_scanned: true) if request.present?
                    request.create_qr_batch(batch_type,old_type)
                    record_it(6,"Successfully Done!")
                    if params[:scanner].present?
                      return render json: {success: true,message: "Successfully Approved"}
                    else
                      flash[:success] = "Successfully Approved"
                      return redirect_to qr_codes_merchant_sales_path
                    end
                  else
                    if params[:scanner].present?
                      return render json: {success: true,message: "Something went wrong please try again"}
                    else
                      flash[:error] = "Something went wrong please try again"
                      return redirect_to qr_codes_merchant_sales_path
                    end
                  end
                else
                  if params[:scanner].present?
                    return render json: {success: false, message: "Insufficiant QR Code Balance!"}
                  else
                    flash[:error] = "Insufficiant QR Code Balance!"
                    return redirect_to qr_codes_merchant_sales_path
                  end
                end
              else
                if params[:scanner].present?
                  return render json: {success: false, message: "Invalid Destination Account! Contact App Administrator."}
                else
                  flash[:error] = "Invalid Destination Account! Contact App Administrator."
                  return redirect_to qr_codes_merchant_sales_path
                end
              end
            else
              if params[:scanner].present?
                return render json: {success: false, message: "Couldn't find the QR Code in our system!"}
              else
                flash[:error] = "Couldn't find the QR Code in our system!"
                return redirect_to qr_codes_merchant_sales_path
              end
            end
          end
        else
          if params[:scanner].present?
            return render json: {success: false, message: "QR Code already used"}
          else
            flash[:error] = "QR Code already used"
            return redirect_to qr_codes_merchant_sales_path
          end
        end
      else
        if params[:scanner].present?
          return render json: {success: false, message: "Couldn't find the QR Code in our system!"}
        else
          flash[:error] = "Couldn't find the QR Code in our system!"
          return redirect_to qr_codes_merchant_sales_path
        end
      end
    else
      if params[:scanner].present?
        return render json: {success: false, message: "Please Scan again!"}
      else
        flash[:error] = "Please Scan again!"
        return redirect_to qr_codes_merchant_sales_path
      end
    end
  end

  def wallet_details
    if !params[:wallet_id].blank?
      @batch = Batch.where(user_id: @user.id, wallet_id: params[:wallet_id], is_closed: false).first
      @wallet = Wallet.find params[:wallet_id]
    else
      @error = 'Please select a valid wallet'
    end
    respond_to :js
  end

  def redeem_modal
    if params[:request_id].present?
      request = Request.where(id: params[:request_id]).last
      if request.present?
        qr_card = request.qr_card
        @qr_id = qr_card.token
        @qr_status = qr_card.is_valid
        @request_status = request.status
      end
    elsif params[:qr_codes].present?
      #using this same function to go to qr codes page
      @qr_code = params[:qr_codes]
    end
    respond_to :js
  end

  def qr_codes
    @filters = [10,25,50,100]
    if @user.merchant_id.present?
      user_id = @user.merchant_id
    else
      user_id = @user.id
    end
    if params[:query].present?
      conditions = search_requests_query(params[:query])
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:id)
        @requests = Request.joins(qr_card: :card_transaction).where(reciever_id: user_id,wallet_id: wallets).where(conditions).order('requests.created_at DESC').per_page_kaminari(params[:page]).per(params[:tx_filter] || 10)
      else
        @requests = Request.joins(qr_card: :card_transaction).where(reciever_id: user_id).where(conditions).order('requests.created_at DESC').per_page_kaminari(params[:page]).per(params[:tx_filter] || 10)
      end
    else
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:id)
        @requests = Request.qr_code.where(reciever_id: user_id, wallet_id: wallets).order(created_at: :desc).per_page_kaminari(params[:page]).per(params[:tx_filter] || 10)
      else
        @requests = Request.qr_code.where(reciever_id: user_id).order(created_at: :desc).per_page_kaminari(params[:page]).per(params[:tx_filter] || 10)
      end
    end
  end

  def verify_sale_limit
    wallet=Wallet.find(params[:wallet_id])
    @location = wallet.location
    if @location.sale_limit.present?
      if @location.sale_limit_percentage.present?
        sale_limit = @location.sale_limit
        sale_limit_percentage = @location.sale_limit_percentage
        total_limit = (sale_limit_percentage/100 * sale_limit) + sale_limit
        render status: 200, json:{success: 'Found with percentage', :limit => total_limit }
      else
        render status: 200, json:{success: 'Found', :limit => @location.sale_limit}
      end
    else
      render status: 404, json:{error: 'You are not Authorized for this action.'}
    end
  end

  def close_batch
    if !params[:wallet_id].blank?
      @update_batch =  Batch.where(user_id: @user.id, wallet_id: params[:wallet_id], is_closed: false, batch_type: 'primary').first
      @update_wallet = Wallet.find params[:wallet_id]
      if @update_batch.present?
        @update_batch.update(is_closed: true, close_date: Date.today, close_time: Time.now.strftime("%H:%M"))
      else
        @error = "Please do sales first"
      end
    else
      @error = "Something wrong happened please try again!"
    end
    respond_to :js
  end

  def batch
    access = merchant_allowed
    if access[:close_batch]
      return redirect_to merchant_transactions_path
    end
    @heading = "Close Batch"
    @wallets = Array.new
    if @user.merchant_id.nil? || @user.iso? || @user.agent?
      @user.wallets.where(wallet_type: 'primary').order('id ASC').each do |w|
        @location = w.location
        if @location.close_batch == false || @location.close_batch == nil
          @wallets << w
        end
      end
    elsif !@user.merchant_id.nil? && @user.MERCHANT? && @user.try(:permission).regular?
      @wallets = @user.wallets.primary.reject{|v| v.location_id.nil?}
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.where(wallet_type: 'primary').order('id ASC').each do |w|
          @location = w.location
          if @location.close_batch == false || @location.close_batch == nil
            @wallets << w
          end
        end
      end
    end

    render template: "merchant/sales/batch"
  end

  private

  def allow_if_active
    if params[:action] == 'scanner'
      if params[:to_id].present? && params[:qr_token].present?
        wallet =  Wallet.where(id: params[:to_id]).first
        location = wallet.location
      elsif params[:location_secure_token].present? && params[:qr_token].present?
        location=Location.where(location_secure_token: params[:location_secure_token]).first
      end
      unless location.nil?
        if location.is_block
          respond_to do |f|
            f.html{
              flash[:error] = I18n.t("errors.withdrawl.blocked_location")
              redirect_to transactions_merchant_sales
            }
            f.js{render js: "windows.location.reload();"}
          end

          # return render_json_response({success: false , :message => 'Location can not be accessed'}, :ok)
        end
      end
    end
  end

  def authenticate_transaction
    catch_duplicate_time = ENV["DUPLICATE_TRANSACTION_TIME"] || 300
    params_card_number = "#{(params[:card_number].to_s.first(4))}********#{(params[:card_number].to_s.last(4))}"
    activities = @user.activity_logs.where("params->'amount' ? :amount AND created_at >= :time", time: Time.now - catch_duplicate_time.to_i.seconds, amount: params["amount"].to_s)
    duplicate = activities.detect {|activity| valid_number_amount?(activity) && activity.params["card_number"].last(4) == params_card_number.last(4) && (activity.api_success? || activity.vt_success?)}
    if duplicate.present?
      flash[:notification_msg] = I18n.t('api.errors.duplicate_transaction')
      record_it(7, I18n.t('api.errors.duplicate_transaction'))
      return redirect_to virtual_terminal_merchant_sales_path
    end
  end

  def valid_number_amount?(activity)
    if activity.params["card_number"].present? && activity.params["amount"].present?
      true
    else
      false
    end
  end

  def record_it(type = nil, reason = nil)
    unless @user.nil?
      clean_parameters
      activity = ActivityLog.new
      activity.url = request.url
      activity.host = request.host
      activity.browser = request.env["HTTP_USER_AGENT"]
      if request.remote_ip.present?
        activity.ip_address = request.remote_ip
      end
      activity.controller = controller_name
      activity.action = action_name
      activity.respond_with = reason.present? ? {response: reason} : response.body
      unless type.nil?
        activity.activity_type = type
      end
      if params[:user_image].present?
        if params[:user_image].original_filename.present?
          params[:user_image] = params[:user_image].original_filename
        else
          params[:user_image] = 'unknown image format'
        end
      end
      activity.transaction_id= params[:transaction_id]
      activity.params = params.except(:card_cvv)
      activity.user = @user
      activity.save
    end
  end

  def saving_decline_for_seq(issue_raw_transaction,payment_gateway = nil)
    if params[:both]
      location_fee = issue_raw_transaction.get_location_fees(params[:merchant_wallet_id],@user,params[:amount],nil,TypesEnumLib::TransactionType::CreditTransaction)
      #= building hash for Decline
      transaction_info = {amount: params[:amount].to_f}
      reserve_detail = location_fee.first if location_fee.present?
      user_info = location_fee.second if location_fee.present?
      fee = location_fee.third if location_fee.present?
      transaction_info = transaction_info.merge({:reserve_detail => reserve_detail, :fee => fee,:user_info => user_info})
      #= end building process
      OrderBank.create(merchant_id: params[:merchant_wallet_id],user_id: params[:wallet_id],bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,card_type: nil,card_sub_type:nil,status: "decline",amount: params[:amount],transaction_info: transaction_info.to_json,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id))
    elsif params[:order_bank].present?
      order_bank = params[:order_bank]
      OrderBank.create(merchant_id: order_bank.merchant_id,user_id: order_bank.user_id,bank_type: payment_gateway.try(:key) || TypesEnumLib::DeclineTransacitonTpe::VirtualTerminal,card_type: nil,card_sub_type:nil,status: "decline",amount: order_bank.amount,transaction_info: order_bank.transaction_info,transaction_id: params[:transaction_id], payment_gateway_id: payment_gateway.try(:id))
    end
  end

end
