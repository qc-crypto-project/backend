class Merchant::BankController < Merchant::BaseController

  def index
    @banks =  BankDetail.all.order(id: :desc)
    @iv       = 'a2xhcgAAAAAAAAAA'
    @password = Digest::SHA256.digest("#{ENV['BANK_ECRYPTION_CODE']}")
    #bank = BankDetail.first
  end

  def create
    bank_name = params[:bank_name]
    account_name = params[:account_name]
    account_no = params[:account_no]
    routing_no = params[:routing_no]
    account_digits = account_no.split(//).last(4).join
    routing_digits = routing_no.split(//).last(4).join
    enc_account_no = AESCrypt.encrypt("#{ENV['BANK-ENCRYPTION-CODE']}",  account_no)
    enc_routing_no = AESCrypt.encrypt("#{ENV['BANK-ENCRYPTION-CODE']}",  routing_no)
    BankDetail.create! bank_name: bank_name, account_name: account_name, account_no: enc_account_no, routing_no: enc_routing_no, account_digit: account_digits, routing_digit: routing_digits
    flash[:success] = I18n.t 'merchant.controller.bank_detail'
    redirect_to merchant_bank_index_path
  end
end
