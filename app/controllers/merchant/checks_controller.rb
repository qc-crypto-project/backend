class Merchant::ChecksController < Merchant::BaseController
  require 'csv'
  include ApplicationHelper
  include UsersHelper
  include Merchant::ChecksHelper

  skip_before_action :require_merchant , only: [:policy, :bulk_check_detail, :destroy, :show, :load_updated_checks,:get_bank_by_routing_number]
  skip_before_action  :set_wallet_balance, only: [:update_check_form, :policy, :bulk_check_detail, :get_fee_sum, :destroy, :show, :load_updated_checks,:debit_card_deposit_form,:create_bulk_checks, :update_single_bulk_check,:get_bank_by_routing_number, :check_form, :create_check]
  skip_before_action :authenticate_user!, only: [:policy]
  before_action :validate_location, only: [:create]
  before_action :set_cache_headers, only: [:debit_card_deposit]

  class CheckError < StandardError; end

  def index
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @heading = "Checks"
    @check = Check.new
    @direct_deposit = Check.new
    @checkIssueEnabled = false
    checkConfig = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @checkIssueEnabled = checkConfig.boolValue unless checkConfig.blank?
    if current_user.present? && !current_user.merchant_id.nil?
      parent_user = User.find(current_user.merchant_id)
      user_ids=[parent_user.id,current_user.id]
      wallet_ids = current_user.wallets.pluck(:id)
      if parent_user.oauth_apps.present?
        oauth_app = parent_user.oauth_apps.first
        @checkIssueEnabled = false if oauth_app.is_block && parent_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
      end
      @checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids, wallet_id: wallet_ids).order(id: :desc)
      # @checks = @checks - @checks.map do |a| a.send_via == "debit_card" end
    else
      if current_user.sub_merchants.present?
        sub_id = current_user.sub_merchants.pluck(:id)
        user_ids=[current_user.id,sub_id]
        user_ids= user_ids.flatten if user_ids.present?
      else
        user_ids=current_user.id
      end
      @checks = TangoOrder.where(order_type: [0,5] ,user_id: user_ids).order(id: :desc)
      if @user.oauth_apps.present?
        oauth_app = @user.oauth_apps.first
        @checkIssueEnabled = false if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
      end
    end
    @checks = @checks.per_page_kaminari(params[:page]).per(page_size)
    @wallets = []

    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    elsif !@user.merchant_id.nil?
      @wallets = @user.wallets.primary.where.not(location_id: nil)
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.primary.each do |w|
          @wallets << w
        end
      end
    end
    render template: 'merchant/checks/index'
  end

  def new
  end

  def show
    @check= TangoOrder.find params[:id]
    respond_to :js
  end

  def destroy
    @bulk_check = BulkCheck.find params[:id]
    @check = @bulk_check
    @wallet_id = params[:wallet_id]
    # bulk_id = @bulk_check.bulk_check_instance.id
    @bulk_check.destroy
    @checks = @bulk_check.bulk_check_instance.bulk_checks

    @bulk_count = @checks.count
    a = @checks.map {|x| x.amount.to_f}
    @total_amount = a.sum

    @wallets = current_user.wallets.primary if current_user.MERCHANT?
    # @inst = @check.bulk_check_instance
    @check.bulk_check_instance.update(total_amount: @total_amount.to_f)
    @list=[]
    @wallets.each do|w|
      @list << {
          wallet_id: w.id,
          wallet_name: w.name,
          balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
          fee: get_location_fee_for_bulk_checks(@total_amount,w, @bulk_count)
      }
    end

    respond_to do |format|
      format.js {
        render 'merchant/checks/bulk_checks_ajax/del_checks.js.erb'
      }
    end
    # flash[:success] = "Successfully deleted!"
    # redirect_to show_bulk_merchant_checks_path(:id => bulk_id)
  end

  def update_single_bulk_check
    @check = BulkCheck.find(params[:check_id_single].to_i)
    @wallet_id = params[:wallet_id]
    @check.update(name: params[:name],
                  last_name: params[:last_name],
                  phone: params[:phone],
                  email: params[:email],
                  amount: params[:amount].to_f,
                  routing_number: params[:routing_number],
                  account_number: params[:account_number],
                  confirm_account: params[:confirm_account])
    @checks = @check.bulk_check_instance.bulk_checks
    @bulk_count = @checks.count
    a = @checks.map {|x| x.amount.to_f}
    @total_amount = a.sum
    #
    @wallets = current_user.wallets.primary if current_user.MERCHANT?
    # @inst = @check.bulk_check_instance
    @check.bulk_check_instance.update(total_amount: @total_amount.to_f)
    @list=[]
    @wallets.each do|w|
      @list << {
          wallet_id: w.id,
          wallet_name: w.name,
          balance: show_balance(w.id) - HoldInRear.calculate_pending(w.id),
          fee: get_location_fee_for_bulk_checks(@total_amount,w, @bulk_count)
      }
    end
    respond_to do |format|
      format.js{
        render 'merchant/checks/bulk_checks_ajax/edit_bulk_check.js.erb'
      }
    end
  end

  def bulk_check_detail
    @check = BulkCheck.find params[:id]
    respond_to :js
  end

  def bulk_checks
    file = params[:file]
    parent = BulkCheckInstance.create! bulk_type: "bulk_checks", user_id: current_user.id
    parent_id = parent.id
    @inst_id = parent_id
    @inst = parent
    @valid_csv = true
    CSV.foreach(file.path, headers: true) do |row|
      flag = validate_checks_bulk_data?(row)
      unless flag
        flash[:error] = 'CSV File Contain Invalid Check(\'s).Please enter Valid Information!'
        @valid_csv = false
        redirect_back(fallback_location: root_path)
        break
      end
    end

    if @valid_csv
      CSV.foreach(file.path, headers: true) do |row|
        rec = BulkCheck.new(name: row[0], last_name: row[1], phone: row[2], email: row[3], routing_number: row[4], account_number: row[5], confirm_account: row[6], amount: row[7].to_f, bulk_check_instance_id: parent_id )
        rec.save!
      end
    end

    @wallets = current_user.wallets.primary if current_user.MERCHANT?
    @checks = BulkCheckInstance.find(parent_id).bulk_checks
    a = @checks.map {|x| x.amount.to_f}
    @total_amount = number_with_precision(a.try(:sum, &:to_f), :precision => 2, :delimiter => ',').to_f
    parent.update(total_amount: @total_amount.to_f)
    @list=[]
    @wallets.each do|w|
      @list << {
          wallet_id: w.id,
          wallet_name: w.name,
          balance:  number_with_precision((show_balance(w.id) - HoldInRear.calculate_pending(w.id)).to_f, :precision => 2, :delimiter => ','),
          fee:  get_location_fee_for_bulk_checks(@total_amount,w, @checks.count),
          fee_and_total: number_with_precision(get_location_fee_for_bulk_checks(@total_amount,w, @checks.count).to_f + @total_amount.to_f, :precision => 2, :delimiter => ',')
      }
    end
    # fee = @list.pluck(:fee).sum
    # if fee > 0
    #   parent.update(fee: fee)
    # end
  end

  def check_under_review
    id=JSON.parse(params[:id]) if params[:id].present?
    @check_review = TangoOrder.where(id: id).first
    render json: @check_review.under_review?
  end

  def load_updated_checks
    if params[:bulk_instance_id].nil?
      @checks = current_user.bulk_check_instances.last.bulk_checks
      @inst_id = current_user.bulk_check_instances.last
    else
      @checks = BulkCheckInstance.find(params[:bulk_instance_id]).bulk_checks
      @inst_id = BulkCheckInstance.find(params[:bulk_instance_id])
    end
    @in_pending = @checks.where(is_processed: true, status: "PENDING").count
    @processed = @checks.where(is_processed: true, status: "Success").count
    @all_processed = @checks.where(is_processed: true).count
    respond_to do |format|
      format.js {
        render 'merchant/checks/show_bulk_checks.js.erb'
      }
    end
  end

  def resend_check
    # resend failed check from bulk
    @check = BulkCheck.find(params[:check_id])
    wallet_id = @check.bulk_check_instance.wallet
    if (!params[:routing_number].nil? && !params[:account_number].nil? && !params[:confirm_account].nil? && params[:account_number] == params[:confirm_account]) || (params[:routing_number].nil? && params[:account_number].nil? && params[:confirm_account].nil?)
      @check.update(routing_number: params[:routing_number], account_number: params[:account_number], confirm_account: params[:confirm_account] )
      create_single_check_from_bulk(@check, wallet_id)
      @status = @check.status
      bulk_id = @check.bulk_check_instance.id
      if @status == "Success"
        wallet = Wallet.find(wallet_id)
        if wallet.present?
          location = wallet.location
          fee_object = location.fees.buy_rate.first if location.present?
          check_fee = fee_object.send_check.to_f if fee_object.present?
          fee_perc = deduct_fee(fee_object.redeem_fee.to_f, @check.amount)
          fee_sum = check_fee + fee_perc.to_f
          fee_class = Payment::FeeCommission.new(@user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::SendCheck)
          fee = fee_class.apply(@check.amount.to_f, TypesEnumLib::CommissionType::SendCheck)
          user_info = fee["splits"]
          withdraw = withdraw(@check.amount,wallet.id, fee_sum, TypesEnumLib::TransactionType::BulkCheck,nil,nil,user_info, nil, @user.ledger,nil,TypesEnumLib::GatewayType::Checkbook)
        end
        if withdraw.present?
          @check.update(sent: true)
        end
        @status = "Success"
      else
        @status = @status
      end
    else
      @status = "Resend"
    end

    respond_to do |format|
      format.js{
        render 'merchant/checks/bulk_checks_ajax/resend.js.erb'
      }
    end
    # redirect_to show_bulk_merchant_checks_path(:id => bulk_id)

  end

  def all_bulk_checks
    @bulks = current_user.bulk_check_instances.where.not(wallet: nil).order(id: "desc")
    @bulks = Kaminari.paginate_array(@bulks).page(params[:page]).per(10)
    #@bulks = @bulks.paginate(:page => params[:page], :per_page => 10)
    @checkIssueEnabled = false
    checkConfig = AppConfig.where(key: AppConfig::Key::IssueCheckConfig).first
    @checkIssueEnabled = checkConfig.boolValue unless checkConfig.blank?
    if current_user.present? && !current_user.merchant_id.nil? && current_user.submerchant_type == "admin_user"
      parent_user = User.find(current_user.merchant_id)
      if parent_user.oauth_apps.present?
        oauth_app = parent_user.oauth_apps.first
        @checkIssueEnabled = false if oauth_app.is_block && parent_user.get_locations.try(:first).try(:block_withdrawal)
      end
    else
      if @user.oauth_apps.present?
        oauth_app = @user.oauth_apps.first
        @checkIssueEnabled = false if oauth_app.is_block && @user.get_locations.try(:first).try(:block_withdrawal)
      end
    end
    render template: 'merchant/checks/all_bulk_checks'
  end

  def show_bulk
    @inst_id = BulkCheckInstance.find(params[:id])
    @inst = @inst_id
    @checks = @inst_id.bulk_checks.includes(:tango_order)
    @in_pending = @checks.where(is_processed: true, status: "PENDING").count
    @processed = @checks.where(is_processed: true, status: "Success").count
    wallet_id = @inst_id.wallet
    @wallet = Wallet.find_by_id(wallet_id) if current_user.MERCHANT?
    a = @checks.map {|x| x.amount.to_f}
    @total_amount = a.sum
    @list=[]
    @list <<
        {
            wallet_id: @wallet.id,
            wallet_name: @wallet.name,
            balance: show_balance(@wallet.id),
            fee: get_location_fee_for_bulk_checks(@total_amount,@wallet, @checks.count)
        }
    @stat = "Processed"
  end

  def create_bulk_checks
    # BulkcheckWorker.perform_async(params)
    Thread.new do
      bulk_check = BulkCheckInstance.find(params[:bulk_instance_id].to_i)
      bulk_check.update(start_process: true)
      checks = bulk_check.bulk_checks
      bulk_check.update(wallet: params[:wallet_id].to_i)
      checks.each do |check|
        create_single_check_from_bulk(check, params[:wallet_id].to_i)
      end
      withdraw_checks = checks.where(is_processed: true, status: "PENDING", sent: false)
      if withdraw_checks.present?
        withdraw_amount = withdraw_checks.pluck(:amount).sum
        wallet = Wallet.find(params[:wallet_id].to_i)
        location = wallet.location
        fee_object = location.fees.buy_rate.first if location.present?
        check_fee = withdraw_checks.count * fee_object.send_check.to_f if fee_object.present?
        fee_perc = deduct_fee(fee_object.redeem_fee.to_f, withdraw_amount)
        fee_sum = check_fee + fee_perc.to_f
        fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
        fee = fee_class.apply(withdraw_amount.to_f, TypesEnumLib::CommissionType::SendCheck, withdraw_checks.count)
        user_info = fee["splits"]
        send_check_info = {
            bulk_checks: {
                checks_ids: withdraw_checks.pluck(:id),
                bulk_check_instance: bulk_check.id,
                total_checks: checks.count
            }
        }
        location_info = location_to_parse(wallet.location) if wallet.location.present?
        merchant_info = merchant_to_parse(wallet.location.merchant) if wallet.location.present?
        tags = {
            "fee_perc" => user_info,
            "location" => location_info,
            "merchant" => merchant_info,
            "send_check_user_info" => send_check_info
        }
        transaction = @user.transactions.create(
            status: "pending",
            amount: withdraw_amount,
            sender: @user,
            sender_wallet_id: wallet.id,
            sender_name: @user.try(:name),
            action: 'retire',
            fee: fee_sum.to_f,
            net_amount: withdraw_amount.to_f - fee_sum.to_f,
            net_fee: fee_sum.to_f,
            total_amount: withdraw_amount.to_f + fee_sum.to_f,
            gbox_fee: user_info["gbox"]["amount"].to_f,
            iso_fee: user_info["iso"]["amount"].to_f,
            agent_fee: user_info["agent"]["amount"].to_f,
            affiliate_fee: user_info["affiliate"]["amount"].to_f,
            ip: get_ip,
            main_type: updated_type(TypesEnumLib::TransactionType::BulkCheck),
            tags: tags
        )
        withdraw = withdraw(withdraw_amount,wallet.id, fee_sum, TypesEnumLib::TransactionType::BulkCheck,nil,nil,user_info, send_check_info, @user.ledger,nil,TypesEnumLib::GatewayType::Checkbook)
        if withdraw.present?
          #= creating block transaction
          parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
          save_block_trans(parsed_transactions) if parsed_transactions.present?
          transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
          withdraw_checks.each do |check|
            check.update(sent: true)
          end
        end
      end
    end
  end

  def create_check
    amount = params[:check][:amount].to_f
    recipient = params[:check][:recipient]
    name = params[:check][:name]
    sender = params[:check][:sender]
    description = params[:check][:description]
    send_via = 'email'
    send_check_user_info = { name: name, check_email: recipient }
    begin
      raise "Amount Must be greater then 0." if params[:check][:amount].to_f <= 0
      unless params[:location_id].blank?
        @user = User.find(params[:check][:sender_id]) if @user.blank?
        if @user.MERCHANT?
          location = Location.find_by(id: params[:location_id])
          @wallet = location.wallets.try(:primary).try(:first)
          params[:wallet_id] = @wallet.id
        else
          @wallet = Wallet.find(params[:location_id])
          location = @wallet.location
          params[:wallet_id] = @wallet.id
        end

        raise I18n.t("api.registrations.unavailable") if location.try(:block_withdrawal)

        main_type = updated_type(TypesEnumLib::TransactionType::SendCheck)
        if @user.present? && @wallet.present?
          amount_sum = 0
          fee_lib = FeeLib.new
          escrow_wallet = Wallet.check_escrow.first
          escrow_user = escrow_wallet.try(:users).try(:first)
          if @user.merchant?
            location_fee = location.fees if location.present?
            fee_object = location_fee.buy_rate.first if location_fee.present?
            fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
            fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
            amount_sum = amount + fee["fee"].to_f
            amount_sum = amount_sum.round(2)
            splits = fee["splits"]
            if splits.present?
              agent_fee = splits["agent"]["amount"]
              iso_fee = splits["iso"]["amount"]
              iso_balance = show_balance(location.iso.wallets.primary.first.id)
              if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
                if iso_balance < iso_fee
                  raise CheckError.new(I18n.t("errors.iso.iso_0001"))
                end
              else
                if iso_balance + iso_fee < agent_fee.to_f
                  raise CheckError.new(I18n.t("errors.iso.iso_0002"))
                end
              end
            end
            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if show_balance(@wallet.id, @user.ledger).to_f < amount_sum

            if location.check_limit_type.present? && location.check_limit != 0
              if params[:check][:amount].to_f > location.check_limit.to_f && location.check_limit_type == "Transaction"
                raise CheckError.new("Exceeds Transaction Limit of $ #{location.check_limit.to_f}")
              end
              if location.check_limit_type == "Day"
                date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
                instant_checks = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",location.id,date,"VOID",[0,5])
                total_sum = instant_checks.sum(:actual_amount) + params[:check][:amount].to_f
                if total_sum.to_f > location.check_limit.to_f
                  raise CheckError.new("Exceeds Daily Limit of $ #{location.check_limit.to_f}")
                end
              end
            end
            pending_amount = HoldInRear.calculate_pending(params[:wallet_id])
            balance = show_balance(@wallet.id, @user.ledger).to_f

            if !@user.merchant_id.nil? && current_user.try(:permission).try(:check)
              user_id = @user.merchant_id
            else
              user_id = @user.id
            end
            main_user = User.find_by(id: user_id)
            if main_user.oauth_apps.present?
              oauth_app = main_user.oauth_apps.first
              raise CheckError.new('') if oauth_app.is_block && main_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
            end
            if main_user.oauth_apps.present?
              oauth_app = main_user.oauth_apps.first
              raise CheckError.new(I18n.t('merchant.controller.blocked_by_admin')) if oauth_app.is_block && main_user.get_locations.uniq.map {|loc| loc.block_withdrawal}.all?
            end
            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if balance - pending_amount < amount_sum
            check = TangoOrder.new(user_id: user_id,
                                   name: name,
                                   status: "PENDING",
                                   amount: amount_sum,
                                   actual_amount: amount,
                                   check_fee: fee_object.send_check.to_f,
                                   fee_perc: deduct_fee(fee_object.redeem_fee.to_f, amount),
                                   recipient: recipient,
                                   description: description,
                                   order_type: "check",
                                   approved: false,
                                   settled: false,
                                   wallet_id: @wallet.id,
                                   send_via: send_via,
                                   batch_date: Time.now.to_datetime + 1.day,
                                   amount_escrow: true
            )
            location = location_to_parse(@wallet.location) if @wallet.location.present?
            merchant = merchant_to_parse(@wallet.location.merchant) if @wallet.location.present?
            tags = {
                "fee_perc" => splits,
                "location" => location,
                "merchant" => merchant,
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
            total_fee = save_total_fee(splits, fee["fee"].to_f)
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: main_user,
                sender_name: sender_dba,
                sender_wallet_id: @wallet.id,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: total_fee.to_f,
                net_amount: amount.to_f,
                net_fee: fee["fee"].to_f,
                total_amount: amount_sum,
                gbox_fee: gbox_fee.to_f,
                iso_fee: save_iso_fee(splits),
                agent_fee: splits["agent"]["amount"].to_f,
                affiliate_fee: splits["affiliate"]["amount"].to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )
            if fee.present?
              user_info = fee["splits"]
              # withdraw = withdraw(amount,@wallet.id, fee["fee"].to_f || 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
              withdraw = withdraw(amount,@wallet.id, total_fee.to_f || 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
            end
            if withdraw.present? && withdraw["actions"].present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
              check.save
              flash[:success] = "Successfully sent a check to #{ recipient }"
            else
              flash[:error] = I18n.t 'merchant.controller.unaval_feature'
            end
          elsif @user.iso? || @user.agent? || @user.affiliate?
            fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
            if fee_object.present?
              send_check_fee = fee_object["send_check"].present? ? fee_object["send_check"].to_f : 0
              fee_perc_fee = fee_object["redeem_fee"].present? ? deduct_fee(fee_object["redeem_fee"].to_f, amount) : 0
            else
              send_check_fee = 0
              fee_perc_fee = 0
            end
            fee = send_check_fee + fee_perc_fee
            amount_sum = amount + fee
            balance = show_balance(@wallet.id, @user.ledger).to_f
            raise CheckError.new(I18n.t("merchant.controller.insufficient_balance")) if balance < amount_sum
            if @user.iso?
              raise CheckError.new("Error Code 6003 Send Check failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0) && @user.iso?
            elsif @user.affiliate? || @user.agent?
              raise CheckError.new("Error Code 6003 Send Check failed, you must keep min $#{@user.system_fee.try(:[],'check_withdraw_limit').try(:to_i)} available in your account. Please try again.") if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0)
            end
            check = TangoOrder.new(user_id: @user.id,
                                   name: name,
                                   status: "PENDING",
                                   amount: amount_sum,
                                   actual_amount: amount,
                                   check_fee: send_check_fee,
                                   fee_perc: fee_perc_fee,
                                   recipient: recipient,
                                   description: description,
                                   order_type: "check",
                                   approved: false,
                                   settled: false,
                                   wallet_id: @wallet.id,
                                   send_via: send_via,
                                   batch_date: Time.now.to_datetime + 1.day,
                                   amount_escrow: true)
            tags = {
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: current_user,
                sender_wallet_id: @wallet.id,
                sender_name: sender_dba,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: fee.to_f,
                net_amount: amount_sum.to_f - fee.to_f,
                net_fee: fee.to_f,
                total_amount: amount_sum.to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )

            if fee > 0
              withdraw = custom_withdraw(amount,@wallet.id, fee, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,true)
            else
              withdraw = withdraw(amount,@wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,nil,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true)
            end
            if withdraw.present? && withdraw["actions"].present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
              if check.save
                flash[:success] = "Successfully sent a check to #{ recipient }"
              else
                flash[:error] = I18n.t 'merchant.controller.chck_failed'
              end
            else
              flash[:error] = I18n.t 'merchant.controller.unaval_feature'
            end
          else
            if @user.wallets.qc_support.present?
              check_fee = 0
            else
              check_fee = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
            end
            amount_sum = amount + check_fee

            raise CheckError.new(I18n.t('merchant.controller.insufficient_balance')) if show_balance(@wallet.id, @user.ledger).to_f < amount_sum

            check = TangoOrder.new(user_id: @user.id,name: name,status: "PENDING",amount: amount_sum,actual_amount: amount,check_fee: check_fee,fee_perc: 0,recipient: recipient,description: description,order_type: "check",approved: false,wallet_id: @wallet.id, account_token: encrypted_card_info, send_via: send_via,account_number: account_number,routing_number: routing_number,batch_date: Time.now.to_datetime + 1.day)
            tags = {
                "send_check_user_info" => send_check_user_info,
                "send_via" => send_via
            }
            sender_dba=get_business_name(@wallet)
            transaction = @user.transactions.create(
                to: recipient,
                from: nil,
                status: "pending",
                amount: amount,
                sender: current_user,
                sender_name: sender_dba,
                sender_wallet_id: @wallet.id,
                receiver_id: escrow_user.try(:id),
                receiver_wallet_id: escrow_wallet.try(:id),
                receiver_name: escrow_user.try(:name),
                sender_balance: balance,
                receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
                action: 'transfer',
                fee: check_fee.to_f,
                net_amount: amount_sum.to_f - check_fee.to_f,
                net_fee: check_fee.to_f,
                total_amount: amount_sum.to_f,
                ip: get_ip,
                main_type: main_type,
                tags: tags
            )
            withdraw = withdraw(amount,@wallet.id, check_fee, TypesEnumLib::TransactionType::SendCheck, account_number, send_via,nil,send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)

            if withdraw.blank?
              flash[:error] = I18n.t 'merchant.controller.unaval_feature'
            else
              #= creating block transaction
              parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
              save_block_trans(parsed_transactions) if parsed_transactions.present?
              transaction.update(seq_transaction_id: withdraw["actions"].first["id"],status: "approved", tags: withdraw["actions"].first.tags, sub_type: withdraw["actions"].first.tags["sub_type"], timestamp: withdraw["timestamp"])
              check.transaction_id = transaction.id
              check.save
              flash[:success] = "Successfully sent a check to #{ recipient }"
            end
          end
        else
          flash[:error] = I18n.t 'merchant.controller.insufficient_balance'
          raise I18n.t 'merchant.controller.insufficient_balance'
        end
      end
    rescue => ex
      flash[:error] = I18n.t 'merchant.controller.chck_creation_faild'
      flash[:error] = ex.message if ex.class == CheckError
      SlackService.notify("*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`")
    ensure
      if flash[:error].blank? && flash[:success].blank?
        flash[:error] = I18n.t 'merchant.controller.except'
        SlackService.notify("*Check Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\`")
      end
      return redirect_back(fallback_location: root_path)
    end
  end

  def create
    type = nil
    amount = nil
    recipient = nil
    name = nil
    sender = nil
    url = nil
    account_number = nil
    send_via = nil
    app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    dd = params[:direct_deposit]
    if params[:invoice].present?
      #invoice
      type = params[:invoice].to_i
      amount = params[:tango_order][:amount].to_f
      recipient = params[:tango_order][:recipient]
      name = params[:tango_order][:name]
      sender = params[:tango_order][:sender]
      description = params[:tango_order][:description]
    else
      #digitalCheck
      type = 0
      amount = params[:check][:amount].to_f
      recipient = params[:check][:recipient]
      name = params[:check][:name]
      sender = params[:check][:sender]
      description = params[:check][:description]
    end
    if !params[:wallet_id].blank?
      begin
        @user = User.find_by_email sender if @user.blank?
        @wallet = Wallet.find(params[:wallet_id])
        balance = show_balance(@wallet.id, @user.ledger).to_f
        if @user.present? && @wallet.present?
          amount_sum = 0
          fee_lib = FeeLib.new
          if @user.MERCHANT?
            location = @wallet.location
            if location.block_withdrawal
              flash[:error] = I18n.t 'api.registrations.unavailable'
              return redirect_back(fallback_location: root_path)
            end
            location_fee = location.fees if location.present?
            fee_object = location_fee.buy_rate.first if location_fee.present?
            if type == 1
              fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::AddMoney)
              fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::AddMoney)
              amount_sum = amount + fee["fee"].to_f
              amount_sum = amount_sum.round(2)
            elsif type == 0
              fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
              fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
              amount_sum = amount + fee["fee"].to_f
              amount_sum = amount_sum.round(2)
            end
          elsif @user.iso? || @user.affiliate? || @user.agent?
            if type == 1
              amount_sum,fee = deduct_add_money_fee(@user, amount)
            elsif type == 0
              amount_sum,fee = deduct_sendcheck_fee(@user, amount)
            end
          else
            amount_sum = amount + fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
            check_fee = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
          end
          if type == 0
            if dd.present? && dd.to_i == 1
              url = URI("https://checkbook.io/v3/check/direct")
              raise "Account Type is missing " unless params[:check][:account_type].present?
              raise I18n.t("errors.withdrawl.unmatch_routing_number") unless params[:check][:account_number] == params[:check][:confirm_account_number]
            else
              url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/digital")
            end

            if  balance < amount_sum
              flash[:error] = I18n.t 'merchant.controller.insufficient_balance'
              return redirect_back(fallback_location: root_path)
            end
          elsif type == 1
            url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/invoice")
          end

          @recipient=recipient
          http = Net::HTTP.new(url.host,url.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_PEER
          request = Net::HTTP::Post.new(url)
          if dd.present? && dd.to_i == 1
            account_number = params[:check][:account_number]
            account_type = params[:check][:account_type]
            routing_number = params[:check][:routing_number]
            request.body = {name: name, recipient: recipient, amount: amount,description: description ,account_number: account_number , routing_number: routing_number ,account_type: account_type }.to_json
          else
            if type == 1
              request.body = {name: name, recipient: recipient, amount: amount_sum,description: description}.to_json
            else
              request.body = {name: name, recipient: recipient, amount: amount,description: description}.to_json
            end
          end
          request["Authorization"] = @@check_book_key+":"+@@check_book_secret
          request["Content-Type"] ='application/json'
          response = http.request(request)
          case response
          when Net::HTTPSuccess
            digital_check = JSON.parse(response.body)
            if response.message == "CREATED"
              check= TangoOrder.new
              check.user_id = @user.id
              check.name = digital_check["name"]
              check.amount = digital_check["amount"]
              if type == 0
                check.actual_amount = amount
                if @user.merchant?
                  check.fee_perc = deduct_fee(fee_object.redeem_fee.to_f, amount)
                  check.check_fee = fee_object.send_check.to_f
                elsif @user.iso? || @user.agent? || @user.affiliate?
                  if @user.system_fee.present? && @user.system_fee != "{}"
                    check.fee_perc = deduct_fee(@user.system_fee["redeem_fee"].try(:to_f), amount)
                    check.check_fee = @user.system_fee["send_check"].to_f
                  end
                end
              elsif type == 1
                check.actual_amount = amount
                if @user.merchant?
                  check.check_fee = fee_object.add_money_check.to_f
                  check.fee_perc = deduct_fee(fee_object.redeem_check.to_f, amount)
                elsif @user.iso? || @user.agent? || @user.affiliate?
                  if @user.system_fee.present? && @user.system_fee != "{}"
                    check.fee_perc = deduct_fee(@user.system_fee["add_money_percent"].try(:to_f), amount)
                    check.check_fee = @user.system_fee["add_money_dollar"].to_f
                  end
                end
              end
              check.recipient = digital_check["recipient"]
              check.checkId = digital_check["id"]
              check.number = digital_check["number"]
              check.description = digital_check["description"]
              check.catalog_image = digital_check["image_uri"]
              check.status = digital_check["status"]
              check.order_type = type
              check.batch_date = Time.now.to_datetime + 1.day
              check.settled = false
              if dd.present?
                check.account_number = params[:check][:account_number].last(4)
                # check.routing_number= params[:check][:routing_number]
              end
              check.wallet_id = @wallet.id
              if account_number.nil?
                account_number = nil
                send_via = 'email'
              else
                account_number = account_number.last(4)
                send_via = 'direct_deposit'
              end
              send_check_user_info = {
                  name: check.name,
                  check_email: check.recipient,
                  check_id: check.checkId,
                  check_number: check.number
              }
              if @user.MERCHANT?
                if type == 0
                  fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
                  fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
                  fee_sum = amount + fee["fee"].to_f
                  if show_balance(@wallet.id, @user.ledger).to_f >= fee_sum
                    if fee["fee"].to_f > 0
                      lib = FeeLib.new
                      user_info = fee["splits"]
                      # user_info = lib.divide_send_check_fee(location,location_fee,company,redeem_fee, fee_perc ,check_fee, company_wallet )
                      withdraw = withdraw(amount,@wallet.id, fee["fee"].to_f, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
                    else
                      withdraw = withdraw(amount, @wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,account_number,send_via,user_info, send_check_user_info,@user.ledger,nil,TypesEnumLib::GatewayType::Checkbook)
                    end
                  else
                    raise StandardError I18n.t('merchant.controller.insufficient_balance')
                  end
                end
              else
                if type == 0
                  withdraw(digital_check["amount"],@wallet.id, fee_lib.get_card_fee(params[:amount].to_f, 'echeck'), TypesEnumLib::TransactionType::SendCheck, account_number, send_via,nil,send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook)
                end
              end
              check.save
              if type== 0
                flash[:success] = "Successfully sent a check to #{ check.recipient }"
              elsif type == 1
                flash[:success] = "Successfully created invoice for  #{ check.recipient }"
              end
            else
              flash[:error] = I18n.t 'merchant.controller.exception1'
            end
            return redirect_back(fallback_location: root_path)
          when Net::HTTPUnauthorized
            flash[:error] = I18n.t 'merchant.controller.unauthorize_access'
            return redirect_back(fallback_location: root_path)
          when Net::HTTPNotFound
            flash[:error] = I18n.t 'merchant.controller.record_notfound'
            return redirect_back(fallback_location: root_path)
          when Net::HTTPServerError
            flash[:error] = I18n.t 'merchant.controller.server_notrespnd'
            redirect_back(fallback_location: root_path)
          else
            error = JSON.parse(response.body)
            flash[:error] = "#{error["error"]}"
            return redirect_back(fallback_location: root_path)
          end
        else
          flash[:error] = I18n.t 'merchant.controller.exception1'
          return redirect_back(fallback_location: root_path )
        end
      rescue => ex
        p "-----------EXCEPTION HANDLED #{ex.message}"
        flash[:error] = I18n.t 'merchant.controller.exception1'
        return redirect_back(fallback_location: root_path)
      end
    else
      flash[:error] = I18n.t 'merchant.controller.sel_wallet'
      return redirect_back(fallback_location: root_path )
    end
  end

  def get_fee_sum
    if params[:amount].to_f > 0 && params[:wallet].present?
      fee_total = 0
      amount = params[:amount].to_f
      amount_plus_fee = amount
      if @user.MERCHANT?
        location = Location.find_by(id: params[:wallet])
        @wallet = location.wallets.try(:primary).try(:first)
      else
        @wallet = Wallet.find(params[:wallet])
        location = @wallet.location
      end
      fee_object = location.fees.buy_rate.first if location.present?
      if @user.MERCHANT?
        if params[:type].to_i == 0
          check_fee = fee_object.send_check.to_f if fee_object.present?
          fee_perc = deduct_fee(fee_object.redeem_fee.to_f, amount) if fee_object.present?
        elsif params[:type].to_i == 1
          check_fee = fee_object.add_money_check.to_f if fee_object.present?
          fee_perc = deduct_fee(fee_object.redeem_check.to_f, amount) if fee_object.present?
        elsif params[:type].to_i == 2
          check_fee = fee_object.dc_deposit_fee_dollar.to_f if fee_object.present?
          fee_perc = deduct_fee(fee_object.dc_deposit_fee.to_f, amount) if fee_object.present?
        end
        fee_sum = amount
        fee_total = check_fee.to_f + fee_perc.to_f
        amount_plus_fee = amount_plus_fee + fee_total
      elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
        if params[:type].to_i == 0
          fee_sum,fee = deduct_sendcheck_fee(@user,amount)
        elsif params[:type].to_i == 1
          fee_sum,fee = deduct_add_money_fee(@user, amount)
        elsif params[:type].to_i == 2
          fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
          if fee_object.present?
            db_check_fee = fee_object["push_to_card_dollar"].present? ? fee_object["push_to_card_dollar"].to_f : 0.0
            db_fee_perc = fee_object["push_to_card_percent"].present? ? deduct_fee(fee_object["push_to_card_percent"].to_f, amount) : 0
          else
            db_check_fee = 0
            db_fee_perc = 0
          end
          fee_total = db_fee_perc.to_f + db_check_fee.to_f
          amount_plus_fee = amount_plus_fee + fee_total
          fee_sum = amount
        end
      elsif @user.wallets.qc_support.present?
        fee_sum = amount
      else
        fee_lib = FeeLib.new
        fee_sum = amount
        fee_total = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
      end

      render status: 200, json: {fee_sum: fee_sum, fee_total: fee_total,amount_plus_fee: amount_plus_fee,fee: fee}
    else
      render status: 200, json: {fee_sum: 0, fee_total: 0,amount_plus_fee: 0}
    end
  end

  def invoices
    @list = []
    @heading = "Invoice"
    @user = current_user
    @user = User.find(current_user.merchant_id) if current_user.merchant_id.present? && current_user.submerchant_type == "admin_user"
    @invoice =  TangoOrder.new
    @invoices = TangoOrder.where(order_type: 1 , user_id: @user.id).order("created_at DESC").per_page_kaminari(params[:page]).per(10)
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
      @user.wallets.includes(:location).where(wallet_type: 'primary').each do |w|
        # @wallets << w
        @location = w.location
        if @location.present?
          location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: location_status
              }
        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: false
              }
        end

      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    else
      @main_user = User.find(@user.merchant_id)
      @main_user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.where(wallet_type: 'primary').each do |w|
          # @wallets << w
          location_status = l.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: location_status
              }
        end
      end
    end
    render template: 'merchant/checks/invoices'
  end

  def new_invoice
    @invoice = TangoOrder.new
    @invoices= TangoOrder.where(type: 1)
    @user= current_user
    @wallets= Location.where(merchant_id: current_user.id).map{|v|v.wallets}.flatten.reject{|v|v.wallet_type == 'tip' }
    render template: 'new_invoice'
  end

  def edit
  end

  def update
    if params[:id].present?
      check = TangoOrder.find(params[:id].to_i)
      user = check.user
      check_old_status = check.status
      if check_old_status == "VOID"
        flash[:success] = check.instant_ach? ? "Successfully Voided ACH" : check.instant_pay? ? "Successfully Voided Push to Card" : check.check? ? "Successfully Voided Check" : flash[:success] = "Successfully Voided"
        return redirect_back(fallback_location: root_path)
      end
      if check.present?
        check_escrow = Wallet.check_escrow.first
        escrow_user = check_escrow.try(:users).try(:first)
        if params[:status] == "VOID"
          check_info = {
              name: check.name,
              check_email: check.recipient,
              check_id: check.checkId,
              check_number: check.number,
              notes: params[:notes]
          }
          if check.status == "PENDING"
            wallet = Wallet.find(check.wallet_id)
            if current_user.present? && current_user.role!='merchant'
              location=nil
              merchant = merchant_to_parse(wallet.users.first)
            else
              location = location_to_parse(wallet.location)
              merchant = merchant_to_parse(wallet.location.merchant)
            end
            tags = {
                "location" => location,
                "merchant" => merchant,
                "send_check_user_info" => check_info,
            }
            ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
            # ach_check_type=check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ? TypesEnumLib::TransactionType::VoidPushtoCard :  TypesEnumLib::TransactionType::VoidCheck
            fee_perc_details = {}
            if check.amount_escrow.present?
              main_tx = check.try(:transaction)
              fee_perc_details["fee_perc"] = main_tx.try(:[],'tags').try(:[],'fee_perc')
              receiver_dba=get_business_name(wallet)
              transaction = Transaction.create(
                  to: nil,
                  from: wallet.id,
                  status: "pending",
                  amount: check.actual_amount,
                  sender: escrow_user,
                  sender_name: escrow_user.try(:name),
                  receiver_wallet_id: wallet.id,
                  sender_wallet_id: check_escrow.try(:id),
                  receiver_id: current_user.try(:id),
                  receiver_balance: SequenceLib.balance(wallet.id),
                  sender_balance: SequenceLib.balance(check_escrow.try(:id)),
                  receiver_name: receiver_dba,
                  action: 'transfer',
                  net_amount: check.actual_amount.to_f,
                  total_amount: check.amount.to_f,
                  ip: get_ip,
                  main_type: ach_check_type,
                  tags: tags,
                  gbox_fee: main_tx.try(:gbox_fee),
                  iso_fee: main_tx.try(:gbox_fee),
                  affiliate_fee: main_tx.try(:gbox_fee),
                  agent_fee: main_tx.try(:gbox_fee),
                  fee: main_tx.try(:fee),
                  net_fee: main_tx.try(:net_fee)
              )
              issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info,check) if @user.MERCHANT? || @user.ISO? || @user.AGENT? || @user.AFFILIATE?
              check.amount_escrow = false
            else
              receiver_dba=get_business_name(wallet)
              transaction = user.transactions.create(
                  to: nil,
                  from: wallet.id,
                  status: "pending",
                  amount: check.actual_amount,
                  receiver_wallet_id: wallet.id,
                  receiver_name: receiver_dba,
                  receiver_balance: SequenceLib.balance(wallet.id),
                  receiver_id: current_user.try(:id),
                  action: 'issue',
                  net_amount: check.actual_amount.to_f,
                  total_amount: check.actual_amount.to_f,
                  ip: get_ip,
                  main_type: ach_check_type,
                  tags: tags
              )
              issue=issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Checkbook, 0,nil, check_info)
            end

            if issue.present?
              transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags.merge(fee_perc_details), timestamp: issue["timestamp"])
              #= creating block transaction
              parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
              save_block_trans(parsed_transactions,"sub","void") if parsed_transactions.present?
            end
            check.void_transaction_id = transaction.try(:id)
            if !check.under_review?
              check.status = params[:status]
              check.approved = true
              check.settled = true
            else
              flash[:error] = "Can't Process Under Review #{check.instant_ach? ? 'Ach':'Check'}"
            end
            if check.save
              check.instant_ach? ? flash[:success] = "Successfully Voided ACH" : check.instant_pay? ? flash[:success] = "Successfully Voided Push to Card" : flash[:success] = "Successfully Voided"
            else
              flash[:error] = "Error Occurred"
            end
          elsif check.checkId.present?
            url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
            http = Net::HTTP.new(url.host,url.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_PEER
            requeste = Net::HTTP::Get.new(url)
            requeste["Accept"] = 'application/json'
            requeste["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
            response = http.request(requeste)
            checkData = JSON.parse(response.read_body)
            if checkData["status"] == "UNPAID"
              check.status = params[:status]
              check.save
              url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/#{check.checkId}")
              http = Net::HTTP.new(url.host,url.port)
              http.use_ssl = true
              http.verify_mode = OpenSSL::SSL::VERIFY_PEER
              requeste = Net::HTTP::Delete.new(url)
              requeste["Authorization"] = ENV['CHECKBOOK_KEY']+":"+ENV['CHECKBOOK_SECRET']
              requeste["Content-Type"] ='application/json'
              response = http.request(requeste)
              case response
              when Net::HTTPSuccess
                wallet = Wallet.find(check.wallet_id)
                if current_user.present? && current_user!='merchant'
                  location=nil
                  merchant = merchant_to_parse(wallet.users.first)
                else
                  location = location_to_parse(wallet.location)
                  merchant = merchant_to_parse(wallet.location.merchant)
                end
                tags = {
                    "location" => location,
                    "merchant" => merchant,
                    "send_check_user_info" => check_info,
                }
                ach_check_type=updated_type(check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck)
                transaction = @user.transactions.create(
                    to: nil,
                    from: wallet.id,
                    status: "pending",
                    amount: check.actual_amount,
                    sender: current_user,
                    sender_name: current_user.try(:name),
                    sender_wallet_id: wallet.id,
                    sender_balance: SequenceLib.balance(wallet.id),
                    action: 'issue',
                    net_amount: check.actual_amount.to_f,
                    total_amount: check.actual_amount.to_f,
                    ip: get_ip,
                    main_type: ach_check_type,
                    tags: tags
                )
                ach_check_type=check.instant_ach? ? TypesEnumLib::TransactionType::VoidAch : check.instant_pay? ?  TypesEnumLib::TransactionType::VoidPushtoCard : TypesEnumLib::TransactionType::VoidCheck
                issue = issue_checkbook_amount(check.actual_amount, wallet ,ach_check_type,TypesEnumLib::GatewayType::Quickcard, 0,nil, check_info)
                if issue.present?
                  transaction.update(seq_transaction_id: issue["actions"].first["id"],status: "approved", tags: issue["actions"].first.tags, timestamp: issue["timestamp"])
                  #= creating block transaction
                  parsed_transactions = parse_block_transactions(issue.actions, issue.timestamp)
                  save_block_trans(parsed_transactions) if parsed_transactions.present?
                end
                check.status = params[:status]
                check.settled = true
                check.approved = true
                check.void_transaction_id = transaction.try(:id)
                if check.save
                  flash[:success] = I18n.t 'merchant.controller.successful_void_check'
                else
                  flash[:error] = I18n.t 'merchant.controller.exception2'
                end
              when Net::HTTPUnauthorized
                check.status = check_old_status
                check.save
                flash[:error] = I18n.t 'merchant.controller.unauthorize_access'
              when Net::HTTPNotFound
                check.status = check_old_status
                check.save
                flash[:error] = I18n.t 'merchant.controller.record_notfound'
              when Net::HTTPServerError
                check.status = check_old_status
                check.save
                flash[:error] = I18n.t 'merchant.controller.server_notrespnd'
              else
                check.status = check_old_status
                check.save
                error = JSON.parse(response.body)
                flash[:error] = "#{error["error"]}"
              end
            else
              if checkData["status"] == "IN_PROCESS"
                # flash[:error] = I18n.t 'merchant.controller.ex_time_limit' temporaily commented untill new US team create a new Error for it
              else
                msg = "The VOID cannot be processed at this time beacuse the check status is #{checkData["status"]}"
                SlackService.notify(msg) unless request.host == 'localhost'
                flash[:error] = I18n.t 'merchant.controller.void_not_proced'
              end
            end
          else
            flash[:error] = I18n.t 'merchant.controller.exception3'
          end
          # redirect_to merchant_checks_path
          return redirect_back(fallback_location: root_path)
        end
      end
    end

  end

  def update_check_form
    @check = BulkCheck.find(params[:checid].to_i)
    respond_to :js
  end

  def check_form
    @check =Check.new
    @user = current_user
    # @user = User.find(current_user.merchant_id) if current_user.merchant_id.present?
    if @user.merchant?
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:location_id)
        @locations = Location.where(id: wallets)
      else
        @locations = current_user.get_locations
      end
    else
      @wallets = @user.wallets.primary
    end
    respond_to :js
  end

  def direct_deposit
  end

  def policy
    check_id = params[:id]
    check_status = params[:status]
    type = params[:type]
    # app_config = AppConfig.where(:key => AppConfig::Key::PaymentProcessor).first
    begin
      if check_id.present? && check_status.present?
        valid_check = TangoOrder.where(checkId: check_id).first
        wallet = Wallet.where(id: valid_check.wallet_id).first if valid_check.wallet_id.present?
        check_info = {
            name: valid_check.name,
            check_email: valid_check.recipient,
            check_id: valid_check.checkId,
            check_number: valid_check.number,
            checkbook_status: check_status,
            check_local_status: check.status
        }
        if type == 'CHECK'
          if valid_check.status == 'UNPAID'
            case check_status
            when 'RETURNED'
              issue_amount(valid_check.amount,wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0, nil,check_info)
              valid_check.update(status: 'RETURNED', settled: true)
              msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{wallet.id}.\`\`\`"
              SlackService.notify(msg) unless request.host == 'localhost'
              render status: 200 , json:{success: 'Success'}
            when 'FAILED'
              issue_amount(valid_check.amount,wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0, nil, check_info)
              valid_check.update(status: 'FAILED', settled: true)
              msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{wallet.id}.\`\`\`"
              SlackService.notify(msg) unless request.host == 'localhost'
              render status: 200 , json:{success: 'Success'}
              return;
            when "UNPAID"
              valid_check.update(status: 'UNPAID', settled: false)
              msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` "
              SlackService.notify(msg) unless request.host == 'localhost'
              render status: 200 , json:{success: 'Success'}
            when "CANCELED"
              # issue_amount(valid_check.amount,wallet,'e-checks','', 0)
              valid_check.update(status: 'VOID', settled: true)
              msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` "
              SlackService.notify(msg) unless request.host == 'localhost'
              render status: 200 , json:{success: 'Success'}
              return;
            when "PRINTED"
              valid_check.update(status: 'PRINTED', settled: true)
              msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` "
              SlackService.notify(msg) unless request.host == 'localhost'
              render status: 200 , json:{success: 'Success'}
              return;
            when "VOID"
              if valid_check.status != 'VOID'
                issue_amount(valid_check.amount, wallet,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0, nil, check_info)
                valid_check.update(status: 'VOID', settled: true)
                msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{wallet.id}.\`\`\`"
                SlackService.notify(msg) unless request.host == 'localhost'
                render status: 200 , json:{success: 'Success'}
              end
              return;
            when "EXPIRED"
              issue_amount(valid_check.amount,wallet ,TypesEnumLib::TransactionType::VoidCheck,TypesEnumLib::GatewayType::Quickcard, 0, nil, check_info)
              valid_check.update(status: 'EXPIRED', settled: true)
              msg = "*Check Status Update.*\ntype: \`\`\`#{type}\`\`\` check_status: \`\`\`#{check_status}\`\`\` check_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money transfered back to wallet: #{wallet.id}.\`\`\`"
              SlackService.notify(msg) unless request.host == 'localhost'
              render status: 200 , json:{success: 'Success'}
              return;
            else
              render status: 400, json:{success: 'error'}
              return;
            end
          end
        elsif type == "INVOICE"
          case  check_status
          when 'PAID'
            total_fee = valid_check.check_fee.to_f + valid_check.fee_perc.to_f
            if total_fee.to_f > 0
              if wallet.users.first.MERCHANT?
                # fee_lib = FeeLib.new
                wallet_user = wallet.users.first
                location = wallet.location
                location_fee = location.fees if location.present?
                fee_object = location_fee.buy_rate.first if location_fee.present?
                fee_class = Payment::FeeCommission.new(wallet_user, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::AddMoney)
                fee = fee_class.apply(valid_check.amount.to_f, TypesEnumLib::CommissionType::AddMoney)
                user_info = fee["splits"]
                issue_amount(valid_check.actual_amount,wallet,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Quickcard, total_fee, user_info, check_info)
              else
                issue_amount(valid_check.actual_amount,wallet,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Quickcard, total_fee, nil, check_info, nil,'qc_wallet')
              end
            else
              issue_amount(valid_check.amount, wallet,TypesEnumLib::TransactionType::ACHdeposit,TypesEnumLib::GatewayType::Quickcard, 0, nil, check_info)
            end

            # fee = fee_lib.divide_merchant_check_fee(location_check_fee, location, company_wallet, qc_wallet)
            valid_check.update(status: 'PAID')
            msg = "*Invoice Status Update.*\ntype: \`\`\`#{type}\`\`\` invoice_status: \`\`\`#{check_status}\`\`\` invoice_info: \`\`\`#{check_info}\`\`\` message: \`\`\`Money issued to wallet: #{wallet.id}.\`\`\`"
            SlackService.notify(msg) unless request.host == 'localhost'
            render status: 200
          when 'CANCELED'
            # issue_amount(valid_check.amount,wallet,'invoice','', 0)
            valid_check.update(status: 'CANCELED')
            msg = "*Invoice Status Update.*\ntype: \`\`\`#{type}\`\`\` invoice_status: \`\`\`#{check_status}\`\`\` invoice_info: \`\`\`#{check_info}\`\`\` "
            SlackService.notify(msg) unless request.host == 'localhost'
            render status: 200
          when 'UNPAID'
            valid_check.update(status: 'UNPAID')
            msg = "*Invoice Status Update.*\ntype: \`\`\`#{type}\`\`\` invoice_status: \`\`\`#{check_status}\`\`\` invoice_info: \`\`\`#{check_info}\`\`\`"
            SlackService.notify(msg) unless request.host == 'localhost'
            render status: 200
          when 'OVERDUE'
            valid_check.update(status: 'OVERDUE')
            msg = "*Invoice Status Update.*\ntype: \`\`\`#{type}\`\`\` invoice_status: \`\`\`#{check_status}\`\`\` invoice_info: \`\`\`#{check_info}\`\`\` "
            SlackService.notify(msg) unless request.host == 'localhost'
            render status: 200
          when 'VOID'
            valid_check.update(status: 'VOID')
            msg = "*Invoice Status Update.*\ntype: \`\`\`#{type}\`\`\` invoice_status: \`\`\`#{check_status}\`\`\` invoice_info: \`\`\`#{check_info}\`\`\` "
            SlackService.notify(msg) unless request.host == 'localhost'
            render status: 200
          else
            p "INVOICE ELSE"
          end
        end
      end
    rescue =>  ex
      p "-----EXCEPTION HANDLED #{ex.message}"
    end
    # id’: <check id>, ‘status’: <check status>}
  end

  def delete
  end

  def debit_card_deposit
    @push_to_card = true
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    
    # @debit_deposits = AppConfig.where(key: AppConfig::Key::DebitCardDeposit).first
    # @push_to_card = @debit_deposits.boolValue unless @debit_deposits.blank?
    @heading = "Debit Card Deposit"
    @check = Check.new
    @direct_deposit = Check.new
    if current_user.present? && !current_user.merchant_id.nil?
      parent_user = User.find(current_user.merchant_id)
      user_ids=[parent_user.id,current_user.id]
      wallet_ids = current_user.wallets.pluck(:id)
      # if parent_user.oauth_apps.present?
        # oauth_app = parent_user.oauth_apps.first
        # @push_to_card = true if @user.get_locations.uniq.map {|loc| loc.push_to_card}.include?(true)
      # end
      @checks = TangoOrder.where(order_type: "instant_pay", user_id: user_ids, wallet_id: wallet_ids).sort.reverse
    else
      if current_user.sub_merchants.present?
        sub_id = current_user.sub_merchants.pluck(:id)
        user_ids=[current_user.id,sub_id]
        user_ids= user_ids.flatten if user_ids.present?
      else
        user_ids=current_user.id
      end
      @checks = TangoOrder.where(order_type: "instant_pay",user_id: user_ids).sort.reverse
      if @user.oauth_apps.present?
        # oauth_app = @user.oauth_apps.first
        # @push_to_card = true if @user.get_locations.uniq.map {|loc| loc.push_to_card}.include?(true)
      end
    end
    @checks=Kaminari.paginate_array(@checks).page(params[:page]).per(page_size)
    @wallets=Array.new
    if @user.merchant_id.nil? || @user.iso? || @user.agent? || @user.partner?
      @user.wallets.where(wallet_type: 'primary').each do |w|
        @wallets << w
      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    elsif !@user.merchant_id.nil? && @user.try(:permission).admin? || @user.try(:permission).regular? || @user.try(:permission).custom?
      @user.wallets.primary.where.not(location_id: nil).each do |w|
        @wallets << w
      end
    else
      @user.locations.reject {|v| v.nil?}.each do |l|
        l.wallets.primary.each do |w|
          @wallets << w
        end
      end
    end
    render template: 'merchant/checks/debit_card_deposit'

  end

  def present_card_verification
    if params[:card_number].blank?
      render json:{"success":"not_available"}, status: 200
    else
      if params[:present].present? && params[:present] == "present"
        card=Card.find_by_id(params[:card_number])
      else
        card=Card.where(first6:params[:card_number].first(6).to_i,last4: params[:card_number].last(4)).last
      end
      card_type=card.try(:card_type)
      first6 = card.try(:first6)
      if card_type.blank? || card_type=='credit'
        card_info = get_card_info(params[:card_number])
        card_type = card_info['type']
        card.update(card_type: card_type) if card.present?
      end

      if card_type=='debit'
        render json:{"success":"true"}, status: 200
      elsif card_type=='credit'
        render json:{"success":"false"} , status: 200
      else
        render json:{"success":"false"}, status: 200
      end
    end
  end

  def debit_card_deposit_form
    @check = Check.new
    @user = current_user
    if @user.merchant_id.present?
      merchant = User.find(@user.merchant_id)
    else
      merchant = @user
    end
    @debit_cards = merchant.cards.instant_pay_cards if merchant.present?
    if @user.merchant?
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:location_id)
        @locations = Location.where(id: wallets)
      else
        @locations = current_user.get_locations
      end
    else
      @wallets = @user.wallets.primary
    end
    render partial: "debit_card_deposit_form"
  end
  ######################################      DELELTE SAVED CARD in push 2 card
  def delete_debit_card
    card = Card.find_by_id(params[:card_number])
    card.destroy if card.present?
  end
  #############################################

  def create_debit_card_deposit
    begin
      amount = params[:check][:amount].to_f
      raise CheckError.new("Sorry, Amount must be greater than Zero!") if amount <= 0

      account_no = params[:check][:account_number]
      confirm_no = params[:check][:confirm_account_number]
      name = params[:check][:name]
      recipient = params[:check][:recipient]
      description = params[:check][:description]
      card_no = params[:check][:card_number]
      card_bin_list = get_card_info(card_no)
      if card_no.present?
        reg = /\D/
        raise CheckError.new(I18n.t('api.registrations.invalid_card_data')) if card_no.match(reg).present?
      end
      if card_bin_list.present? && card_bin_list.try(:[],'type').present? && card_bin_list.try(:[],'type')=="credit"
        return flash[:error]="For Debit Card Use Only."
      end
## Card Expiry :-:
      if params[:check][:expiry_date].present?
        exp_month = params[:check][:expiry_date].split('/').first
        exp_year = params[:check][:expiry_date].split('/').last
        current_year = Time.current.year - 2000
        if exp_year.to_i < current_year || exp_month.to_i > 12 || exp_month.to_i < 0
          return flash[:error]= I18n.t('api.registrations.invalid_card_data')
        end
      end
      if exp_year.present? && exp_year.to_s.size == 4
        exp_year = exp_year.to_i - 2000
      end
      exp_date = "20#{exp_year}-#{exp_month}"
##
      account_type = params[:check][:account_type]
      cvv = params[:check][:cvv]
      addr_1 = params[:check][:address_line_1]
      addr_2 = params[:check][:address_line_2]
      routing_no = params[:check][:routing_number]
      zip = params[:check][:zip_code]
      send_via = 'instant_pay'
      send_check_user_info = { name: name, check_email: recipient }
      if @user.MERCHANT?
        @location = Location.find_by(id: params[:location_id])
        raise CheckError.new(I18n.t("errors.withdrawl.blocked_p2c")) if @location.push_to_card
        @wallet = @location.wallets.try(:primary).try(:first)
        params[:wallet_id] = @wallet.id
      else
        @wallet = Wallet.find(params[:location_id])
        params[:wallet_id] = @wallet.id
      end
      @user = @wallet.users.first
      balance = show_balance(@wallet.id, current_user.ledger).to_f
      last_4 = params[:check][:card_number].last(4) if params[:check][:card_number].present?
      first_6 = params[:check][:card_number].first(6) if params[:check][:card_number].present?

      if @user.present? && !@user.merchant_id.nil?
        merchant = User.find(@user.merchant_id)
      else
        merchant = @user
      end

      if (params[:check][:check_id].present? && params[:other_card].blank?) || (params[:check][:check_id].present? && params[:other_card].present? && params[:check][:card_number].blank?) #= using exiting card
        card = Card.find_by(id: params[:check][:check_id])
        if card.card_type.present? && card.card_type=="debit"
          card_info = Payment::QcCard.new({fingerprint: card.fingerprint}, card.qc_token, merchant.id)
          last_4 = card.last4
          first_6 = card.first6
          raise CheckError.new("Error Code 9001 Invalid card number. Please use valid card!") if card_info.number.tr("0-9","").present?
          card_detail = {
              number: card_info.number,
              month: card_info.month,
              year: card_info.year,
              # cvv: card_info.cvv
          }
        else
          return flash[:error]= I18n.t('merchant.controller.debit_card_only')
        end
      else
        if params[:save_card].present? &&  params[:save_card] == "on"
          raise CheckError.new("Error Code 9001 Invalid card number. Please use valid card!") if card_no.tr("0-9","").present?
          card_detail = {
              number: card_no,
              month: exp_month,
              year: "20#{exp_year}",
              # cvv: cvv
          }
          card_bin_list = get_card_info(card_no)
          card1 = Payment::QcCard.new(card_detail, nil, merchant.id)
          cards = merchant.cards.instant_pay_cards.where(fingerprint: card1.fingerprint,merchant_id: merchant.id)
          if cards.blank?
            card = merchant.cards.create(qc_token:card1.qc_token,exp_date: "#{card_detail[:month]}/#{card_detail[:year].last(2)}",brand: card_bin_list["scheme"].present? ? card_bin_list["scheme"] : card1.card_brand, last4: last_4, name: merchant.name, card_type: card_bin_list["type"].present? ? card_bin_list["type"] : nil,first6: first_6,fingerprint: card1.fingerprint,merchant_id: merchant.id,instant_pay: true)
          else
            card = cards.first
          end
        end
      end


      if @user.MERCHANT?
        if @location.push_to_card_limit_type == "Day"
          todays_orders = TangoOrder.where(wallet_id: @wallet.id,user_id: merchant.id,order_type: "instant_pay",created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
          todays_transactions_amount = todays_orders.pluck(:actual_amount).sum(&:to_f)
          todays_void_amount = todays_orders.where(status: 'VOID').pluck(:actual_amount).sum(&:to_f)
          # todays_void_amount = TangoOrder.where(wallet_id: @wallet.id,user_id: merchant.id,order_type: "instant_pay",status: "VOID", created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).pluck(:actual_amount).sum(&:to_f)
          valid_amount = todays_transactions_amount - todays_void_amount + amount
          raise CheckError.new("Error Code 4000 - Daily limit exceeded. Daily Transactions limit reached to $#{number_with_precision(@location.push_to_card_limit, :precision => 2, delimiter: ',')} !") if valid_amount > @location.push_to_card_limit
        else
          raise CheckError.new("Error Code 5007 Sorry you are not allowed to process with more than $#{number_with_precision(@location.push_to_card_limit, :precision => 2, delimiter: ',')}. Please contact your administrator for details.") if amount > @location.push_to_card_limit
        end
      elsif @user.iso? || @user.agent? || @user.affiliate?
        if @user.system_fee.try(:[],"push_to_card_limit_type") == "Day"
          todays_orders = TangoOrder.where(wallet_id: @wallet.id,user_id: merchant.id,order_type: "instant_pay",created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
          todays_transactions_amount = todays_orders.pluck(:actual_amount).sum(&:to_f)
          todays_void_amount = todays_orders.where(status: 'VOID').pluck(:actual_amount).sum(&:to_f)
          valid_amount = todays_transactions_amount - todays_void_amount + amount
          raise CheckError.new("Error Code 4000 - Daily limit exceeded. Daily Transactions limit reached to $#{number_with_precision(@user.system_fee.try(:[],"push_to_card_limit"), :precision => 2, delimiter: ',')} !") if valid_amount > (@user.system_fee.try(:[],'push_to_card_limit').present? ? @user.system_fee.try(:[],'push_to_card_limit').try(:to_f) : 0)
        else
          raise CheckError.new("Error Code 5007 Sorry you are not allowed to process with more than $#{number_with_precision(@user.system_fee.try(:[],"push_to_card_limit"), :precision => 2, delimiter: ',')}. Please contact your administrator for details.") if amount > (@user.system_fee.try(:[],'push_to_card_limit').present? ? @user.system_fee.try(:[],'push_to_card_limit').try(:to_f) : 0)
        end
      end

      if card_detail.present?
        account_token_info =  {
            :card_number => card_detail[:number],
            :expiry_date => "#{card_detail[:year]}-#{card_detail[:month]}"
        }
      else
        account_token_info =  {
            :card_number => card_no,
            :expiry_date => exp_date
        }
      end



      account_token_info = account_token_info.to_json
      encrypted_card_info = AESCrypt.encrypt("#{merchant.id}-#{ENV['CARD_ENCRYPTER']}-#{@wallet.id}}", account_token_info)

      if user_signed_in? && @wallet.present?
        amount_sum = 0
        total_fee = 0
        fee_lib = FeeLib.new
        if (!account_no.nil? && !confirm_no.nil? && account_no == confirm_no) || (account_no.nil? || confirm_no.nil?)
          if current_user.MERCHANT?
            location = @wallet.location

            raise CheckError.new(I18n.t("api.registrations.unavailable")) if location.push_to_card

            location_fee = location.fees if location.present?
            fee_object = location_fee.buy_rate.first if location_fee.present?
            # fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
            fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::DebitCardDeposit)
            fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::DebitCardDeposit)
            total_fee = fee["fee"].to_f
            amount_sum = amount + total_fee
            db_check_fee = fee_object.dc_deposit_fee_dollar.to_f
            db_fee_perc = deduct_fee(fee_object.dc_deposit_fee.to_f, amount)
          else
            fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
            if fee_object.present?
              db_check_fee = fee_object["push_to_card_dollar"].present? ? fee_object["push_to_card_dollar"].to_f : 0
              db_fee_perc = fee_object["push_to_card_percent"].present? ? deduct_fee(fee_object["push_to_card_percent"].to_f, amount) : 0
            else
              db_check_fee = 0
              db_fee_perc = 0
            end
            total_fee = db_fee_perc + db_check_fee
            amount_sum = amount + total_fee
          end
          splits = fee.try(:[],"splits")
          if splits.present?
            agent_fee = splits["agent"]["amount"]
            iso_fee = splits["iso"]["amount"]
            iso_balance = show_balance(location.iso.wallets.primary.first.id)
            if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
              if iso_balance < iso_fee
                raise CheckError.new(I18n.t("errors.iso.iso_0001"))
              end
            else
              if iso_balance + iso_fee < agent_fee.to_f
                raise CheckError.new(I18n.t("errors.iso.iso_0002"))
              end
            end
          end
          if current_user.agent? || current_user.affiliate?
            raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: current_user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < current_user.system_fee.try(:[],'check_withdraw_limit').to_f
          elsif current_user.iso?
            raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: current_user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < current_user.system_fee.try(:[],'check_withdraw_limit').to_f && current_user.iso?
          end

          raise CheckError.new(I18n.t('merchant.controller.insufficient_balance')) if balance < amount_sum
          pending_amount = HoldInRear.calculate_pending(params[:wallet_id])
          raise CheckError.new(I18n.t('merchant.controller.insufficient_balance')) if balance - pending_amount < amount_sum

          # if  balance < amount_sum
          #   flash[:error] = "Insufficient balance You can not create check!"
          #   redirect_to debit_card_deposit_merchant_checks_path
          # end
          if current_user.present? && !current_user.merchant_id.nil? && current_user.try(:permission).admin?
            user_id=current_user.id
          else
            user_id=current_user.id
          end
          check_escrow = Wallet.check_escrow.first
          check = TangoOrder.new(user_id: merchant.id,
                                 name: name,
                                 status: "PENDING",
                                 amount: amount_sum,
                                 actual_amount: amount,
                                 check_fee: db_check_fee,
                                 fee_perc: db_fee_perc,
                                 recipient: recipient,
                                 description: description,
                                 order_type: "instant_pay",
                                 approved: false,
                                 account_token: encrypted_card_info,
                                 settled: false,
                                 wallet_id: @wallet.id,
                                 send_via: send_via,
                                 account_number: account_no,
                                 routing_number: routing_no,
                                 last4: last_4,
                                 first6: first_6,
                                 batch_date:Time.zone.now + 1.day,
                                 zip_code: zip,
                                 amount_escrow: true)

          location1 = location_to_parse(location)
          merchant1 = merchant_to_parse(merchant)
          tags = {
              "fee_perc" => splits,
              "location" => location1,
              "merchant" => merchant1,
              "send_check_user_info" => { name: name, check_email: recipient,amount: amount,description: description,fee: total_fee },
              "send_via" => send_via,
              "card" => card
          }

          if @user.MERCHANT?
            new_fee = save_gbox_fee(splits, fee.try(:[], "fee").to_f)
            total_fee = save_total_fee(splits, fee.try(:[], "fee").to_f)
          else
            total_fee = db_fee_perc + db_check_fee
            new_fee = total_fee
          end
          sender_dba=get_business_name(@wallet)
          transaction = merchant.transactions.build(
              to: recipient,
              from: nil,
              status: "pending",
              charge_id: nil,
              amount: amount,
              sender: merchant,
              sender_name: sender_dba,
              sender_wallet_id: @wallet.id,
              receiver_wallet_id: check_escrow.id,
              receiver_name: check_escrow.try(:users).try(:first).try(:name),
              receiver_id: check_escrow.try(:users).try(:first).try(:id),
              sender_balance: balance,
              receiver_balance: SequenceLib.balance(check_escrow.id),
              action: 'transfer',
              fee: total_fee,
              net_amount: amount,
              net_fee: total_fee,
              total_amount: amount_sum.to_f,
              gbox_fee: new_fee.to_f,
              iso_fee: save_iso_fee(splits),
              agent_fee: splits.try(:[],"agent").try(:[],"amount").to_f,
              affiliate_fee: splits.try(:[],"affiliate").try(:[],"amount").to_f,
              ip: get_ip,
              main_type: "instant_pay",
              tags: tags,
              last4: last_4,
              first6: first_6
          )


          # transaction = @user.transactions.build(to: recipient, from: nil, status: "approved", charge_id: nil , amount: amount_sum, sender: current_user, action: 'retire', last4: last_4, first6: first_6)

          user_info = fee.try(:[], "splits")
          withdraw = withdraw(amount,@wallet.id, total_fee || 0, TypesEnumLib::TransactionType::DebitCardDeposit,last_4,send_via,user_info, send_check_user_info, @user.ledger, nil,TypesEnumLib::GatewayType::Checkbook,nil,true, card)
          if withdraw.present? && withdraw["actions"].present?
            transaction.seq_transaction_id = withdraw["actions"].first["id"]
            transaction.status = "approved"
            transaction.privacy_fee = 0
            transaction.sub_type = withdraw["actions"].first.tags["sub_type"]
            transaction.timestamp = withdraw["timestamp"]
            transaction.save
            check.transaction_id = transaction.id
            check.save
            #= creating block transaction
            parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
            save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
            flash[:success] = "Successfully sent a Push to Card to #{ last_4 }."
          else
            flash[:error] = I18n.t 'merchant.controller.unaval_feature'
          end
        end

      end
    rescue => ex
     flash[:error] = I18n.t('merchant.controller.chck_creation_faild')
     flash[:error] = ex.message if ex.class == CheckError
     SlackService.handle_exception "Debit Card Deposit failed", ex
     SlackService.notify("*Push To Card Creation Failure.* params: \`\`\`#{params.to_json}\`\`\` user: \`\`\`#{{id: @user.id, email: @user.email, role: @user.role}.to_json}\`\`\` execption: \`\`\` #{ex.to_json} \`\`\`")
    ensure
      if flash[:error].blank? && flash[:success].blank?
        flash[:error] = "Unexpected error occured"
      end
      return redirect_back(fallback_location: root_path)
    end
  end




  ###############################################
  # def create_debit_card_deposit
  #   account_no = params[:check][:account_number]
  #   confirm_no = params[:check][:confirm_account_number]
  #   name = params[:check][:name]
  #   recipient = params[:check][:recipient]
  #   amount = params[:check][:amount].to_f
  #   description = params[:check][:description]
  #   card_no = params[:check][:card_number]
  #   expdate = params[:check][:expiry_date].split('/')
  #   exp_date = "20#{expdate[1]}-#{expdate[0]}"
  #   account_type = params[:check][:account_type]
  #   cvv = params[:check][:cvv]
  #   addr_1 = params[:check][:address_line_1]
  #   addr_2 = params[:check][:address_line_2]
  #   routing_no = params[:check][:routing_number]
  #   # exp_date = Date.parse(ex_date)
  #   zip = params[:check][:zip_code]
  #   @wallet = Wallet.find(params[:wallet_id])
  #   balance = show_balance(@wallet.id, current_user.ledger).to_f
  #   if user_signed_in? && @wallet.present?
  #     amount_sum = 0
  #     fee_lib = FeeLib.new
  #     if (!account_no.nil? && !confirm_no.nil? && account_no == confirm_no) || (account_no.nil? || confirm_no.nil?)
  #       if current_user.MERCHANT?
  #         location = @wallet.location
  #         if location.block_withdrawal
  #           flash[:error] = "Error Code 7009 Sorry but it looks like this function is currently not available. Please contact your administrator for details."
  #           redirect_to debit_card_deposit_merchant_checks_path
  #         end
  #         location_fee = location.fees if location.present?
  #         fee_object = location_fee.buy_rate.first if location_fee.present?
  #         fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
  #         fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
  #         amount_sum = amount + fee["fee"].to_f
  #       else
  #         amount_sum = amount + fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
  #         check_fee = fee_lib.get_card_fee(params[:amount].to_f, 'echeck').to_f
  #       end
  #
  #       if  balance < amount_sum
  #       flash[:error] = "Insufficient balance You can not create check!"
  #       redirect_to debit_card_deposit_merchant_checks_path
  #     end
  #       url = URI("#{ENV['CHECKBOOK_URL_LIVE']}/check/instant")
  #       http = Net::HTTP.new(url.host, url.port)
  #       http.use_ssl = true
  #       http.verify_mode = OpenSSL::SSL::VERIFY_PEER
  #       request = Net::HTTP::Post.new(url)
  #       request["Content-Type"] = 'application/json'
  #       request["Authorization"] = @@check_book_key+":"+@@check_book_secret
  #       request.body = {name: name, recipient: recipient, amount: amount, description: description, card_number: card_no, expiration_date: exp_date, zip: zip, account_type: account_type, cvv: cvv, address_line_1: addr_1, address_line_2: addr_2, routing_number: routing_no, account_number: account_no }.to_json
  #       response = http.request(request)
  #       case response
  #       when Net::HTTPSuccess
  #         deposit_response = JSON.parse(response.body)
  #         check = TangoOrder.new
  #         check.user_id = current_user.id
  #         check.name = deposit_response["name"]
  #         check.amount = deposit_response["amount"]
  #
  #         check.recipient = deposit_response["recipient"]
  #         check.checkId = deposit_response["id"]
  #         check.number = deposit_response["number"]
  #         check.description = deposit_response["description"]
  #         check.catalog_image = deposit_response["image_uri"]
  #         check.status = deposit_response["status"]
  #         check.settled = false
  #         check.wallet_id = @wallet.id
  #         check.debit_card_deposit!
  #         if !account_no.nil?
  #           check.account_number = params[:check][:account_number].last(4)
  #           # check.routing_number= params[:check][:routing_number]
  #         end
  #         if account_no.nil?
  #           account_no = nil
  #           send_via = 'email'
  #         else
  #           account_no = account_no.last(4)
  #           send_via = 'direct_deposit'
  #         end
  #         send_check_user_info = {
  #             name: check.name,
  #             check_email: check.recipient,
  #             check_id: check.checkId,
  #             check_number: check.number
  #         }
  #         if @user.MERCHANT?
  #
  #             # qc_wallet = Wallet.escrow.first
  #             fee_class = Payment::FeeCommission.new(@user, nil, fee_object, @wallet, location,TypesEnumLib::CommissionType::SendCheck)
  #             fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::SendCheck)
  #             fee_sum = amount + fee["fee"].to_f
  #             if show_balance(@wallet.id, current_user.ledger).to_f >= fee_sum
  #               total_fee_deduct = fee["fee"].to_f
  #               # total_fee_deduct = check_fee.to_f + fee_perc.to_f
  #               if total_fee_deduct > 0
  #                 # lib= FeeLib.new
  #                 user_info = fee["splits"]
  #                 withdraw = withdraw(amount,@wallet.id, total_fee_deduct, TypesEnumLib::TransactionType::SendCheck,account_no,send_via,user_info, send_check_user_info, current_user.ledger)
  #               else
  #                 withdraw = withdraw(amount, @wallet.id, 0, TypesEnumLib::TransactionType::SendCheck,account_no,send_via,nil, send_check_user_info,current_user.ledger)
  #               end
  #             else
  #               raise StandardError 'Error Code 2041 Insuficient balance You can not create check!'
  #             end
  #
  #         else
  #
  #             withdraw(deposit_response["amount"],@wallet.id, fee_lib.get_card_fee(params[:amount].to_f, 'echeck'), 'send_check', account_number, send_via,nil,send_check_user_info, current_user.ledger)
  #
  #         end
  #         check.save!
  #         flash[:success] = "Check has been deposited successfully"
  #         redirect_to debit_card_deposit_merchant_checks_path
  #       else
  #         flash[:error] = "Something went wrong"
  #         redirect_to debit_card_deposit_merchant_checks_path
  #     end
  #     end
  #   end
  # end
  # --------------------------------GET BANK BY ROUTING NUMBER-----------------------------
  def get_bank_by_routing_number
    # Preparing Data
    parameters = [["routing_number", "#{params[:routing_number]}"]]
    uri = URI("#{ENV['CHECKBOOK_ROUTING_URL_LIVE']}get_bank_by_routing_number")
    # Sending Request To Checkboox IO
    response = Net::HTTP.post_form( uri, parameters)
    if response.is_a?(Net::HTTPSuccess)
      if response.read_body.present?
        render status: 200 , json:{:response => response.read_body}
      else
        render status: 200 , json:{:response => ""}
      end
    else
      render status: 200 , json:{:response => ""}
    end
  end
  # ----------------------------END GET BANK BY ROUTING NUMBER------------------------------
  private

  def create_single_check_from_bulk(bulk_check, wallet_id)
    #creating bulk checks for both direct and digital checks
    if bulk_check.account_number.present? && bulk_check.routing_number.present?
      dd = 1
    else
      dd = 0
    end
    amount = bulk_check.amount.to_f
    recipient = bulk_check.email
    name = bulk_check.name+" "+bulk_check.last_name
    description = "Bulk Check"

    if wallet_id.present?
      begin
        @user = current_user
        @wallet = Wallet.find(wallet_id)
        balance = show_balance(@wallet.id, @user.ledger).to_f
        if @user.present? && @wallet.present?
          location = @wallet.location
          amount_sum = 0
          location_fee = location.fees.buy_rate.first if location.present?
          redeem_fee = location_fee.redeem_fee if location.present?
          check_fee = location_fee.send_check.to_f if location_fee.present?
          fee_perc = deduct_fee(redeem_fee.to_f, amount)
          amount_sum = amount + check_fee + fee_perc.to_f
          text = "Insufficient balance" if  balance < amount_sum
          check = TangoOrder.new
          check.name = name
          check.recipient = recipient
          check.description = description

          if @user.merchant_id.present? && @user.try(:permission).admin?
            user_id = @user.merchant_id
          else
            user_id = @user.id
          end
          check.user_id = user_id
          if dd.present? && dd.to_i == 1
            card_info =  {
                :account_number => bulk_check.account_number,
                :account_type => "CHECKING"
            }
            card_info = card_info.to_json
            encrypted_card_info = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{@wallet.id}}", card_info)
            check.account_token = encrypted_card_info
            check.account_number = bulk_check.account_number.last(4)
            check.account_type = "CHECKING"
            check.routing_number = bulk_check.routing_number
            check.amount = amount
            check.actual_amount = amount
            check.send_via = "Direct Deposit"
          else
            check.amount = amount_sum
            check.actual_amount = amount
            check.check_fee = check_fee
            check.fee_perc = fee_perc
            check.send_via = "email"
          end
          check.status = "PENDING"
          check.order_type = 3
          check.settled = false
          check.wallet_id = @wallet.id
          check.approved = false
          check.save!
          bulk_check.update(tango_order_id: check.id)
          text = "PENDING"
        else
          text = "Something Wrong"
        end
      rescue => ex
        p "-----------EXCEPTION HANDLED: #{ex.message}"
        text = "Something Wrong"
      end
    else
      text = "Try again"
    end
    bulk_check.update(status: text, is_processed: true)
    # if !response.nil?
    #   bulk_check.update(status: text, response_message: response.body, is_processed: true)
    # else
    #   bulk_check.update(status: text, response_message: response, is_processed: true)
    # end
  end

  def get_location_fee_for_bulk_checks(amount, wallet, checks = nil)
    location = wallet.location
    location_fee = location.fees.buy_rate.first if location.present?
    redeem_fee = location_fee.redeem_fee.to_f if location_fee.present?
    check_fee = location_fee.send_check.to_f if location_fee.present?
    check_fee = check_fee * checks if check_fee.present?
    fee_perc = deduct_fee(redeem_fee.to_f, amount)
    amount_sum = check_fee + fee_perc.to_f
    number_with_precision(amount_sum, :precision => 2, :delimiter => ',')
  end

  def validate_checks_bulk_data?(data)
    valid_phone_number = validate_number(data[2])
    valid_email = validate_eamil(data[3])
    valid_route_number = validate_number(data[4])
    valid_account_number = validate_number(data[5])
    valid_confirm_account_number = validate_number(data[6])
    valid_amount = validate_amount(data[7])
    valid_phone_number && valid_email && valid_route_number && valid_account_number && valid_confirm_account_number && valid_amount
  end

  def validate_eamil(email_obj)
    valid_email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    !!(email_obj =~ valid_email_regex)
  end

  def validate_number(number_obj)
    number_obj !~ /\D/
  end

  def validate_amount(amount_obj)
    valid_amount_regex = /\A[+-]?\d+(\.[\d]+)?\z/
    !!(amount_obj =~ valid_amount_regex)
  end


end
