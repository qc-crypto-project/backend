class Merchant::LocationsController < Merchant::BaseController
  include ApplicationHelper
  skip_before_action :set_wallet_balance, only: [:tip_modal,:show_merchant_balance,:transfer_post,:transfer_update,:transfer_delete]
  skip_before_action :require_merchant, only: [:show_merchant_balance,:transfer_update,:transfer_delete]
  skip_before_action :set_wallet_balance, only: [:tip_modal, :previouss_transactions,:next_transactions,:show_merchant_balance,:transfer_post,:transfer_update,:transfer_delete]
  skip_before_action :verify_tos_acceptance, only: [:index, :tos_approval, :show_merchant_balance]

  def index
    @merchant_wallets=""
    @heading = "Locations"
    if current_user.merchant? && current_user.merchant_id.nil?
      locations_ids = current_user.attached_locations
      if locations_ids.present?
        @locations = []
        @locations << Location.includes(:wallets).where(id: locations_ids)
        @locations << Location.includes(:wallets).where(merchant_id: current_user.id).order(created_at: :desc)
        @locations.flatten!
        @locations = Kaminari.paginate_array(@locations).page(params[:page]).per(3)
      else
        @locations = Location.where(merchant_id: current_user.id).eager_load(:wallets).order(created_at: :desc).per_page_kaminari(params[:page]).per(3)
      end
    elsif current_user.merchant? && current_user.merchant_id.present? && current_user.try(:permission).admin?
      @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact).eager_load(:wallets).order(created_at: :desc).per_page_kaminari(params[:page]).per(3)
    else
      @locations = Location.where(id: current_user.wallets.primary.pluck(:location_id).compact).eager_load(:wallets).order(created_at: :desc).per_page_kaminari(params[:page]).per(3)
    end
    @locationss = @locations.map do |l|
      wallets = l.wallets.pluck(:id,:wallet_type,:slug)
      primary_wallet=''
      reserve_wallet=''
      tip_wallet=''
      primary_slug = ''
      tip_slug = ''
      reserve_slug = ''
      attached_location = false
      wallets.each do |w|
        primary_wallet = w.first if w.second=='primary'
        primary_slug = w.third if w.second == "primary"
        reserve_slug = w.third if w.second == "reserve"
        reserve_wallet = w.first if w.second=='reserve'
        tip_wallet = w.first if w.second=='tip'
        tip_slug = w.third if w.second=='tip'
      end
      if locations_ids.present? && locations_ids.include?(l.id)
        attached_location = true
      end
      {
          id: l.id,
          name: locations_ids.present? && locations_ids.include?(l.id) ? "(M-#{l.merchant.id}) #{l.business_name}" : l.business_name,
          location_id: l,
          wallet_primary: primary_wallet,
          primary_slug: primary_slug,
          wallet_tip: tip_wallet,
          tip_slug: tip_slug,
          wallet_reserve: reserve_wallet,
          reserve_slug: reserve_slug,
          location_secret: l.location_secure_token,
          hold_in_rear_amount: HoldInRear.calculate_pending(primary_wallet),
          secondary_location: LocationsUsers.where(location_id: l.id, user_id: current_user.id, relation_type: "secondary").try(:first) || false,
          is_ecommerce: l.ecommerce?,
          attached_location: attached_location
      }
    end
    if current_user.merchant?
      @merchant_wallets= current_user.wallets.primary if current_user.merchant_id==nil
      if @merchant_wallets.blank? && current_user.merchant_id!=nil
        # user=User.find_by(id:current_user.merchant_id)
        # @merchant_wallets= user.wallets.primary
        @merchant_wallets= current_user.wallets.primary.reject{|v| v.location_id.nil?}
      end
      # @merchant_wallets = @locationss.pluck(:wallet_primary).flatten.uniq
      @transfer = Transfer.new(amount: "")
    end
    render :index
  end

  def tos_approval
    user = current_user
    if user.merchant_id.present? && user.try(:permission).admin?
      user = User.find(user.merchant_id)
    end
    user.update(tos_checking: true,tos_acceptance_date: Time.now.utc)
    redirect_back(fallback_location: root_path)
  end

  def show
    @location = Location.find params[:id]
    @wallets = @location.wallets
    if (@location.iso_allowed  || @location.agent_allowed) && @location.users.present?
      agent = nil
      iso = nil
      @location.users.each do |t|
        if t.iso?
          iso = t
          if @location.iso_allowed
            @wallets = @wallets + iso.wallets
          end
        elsif t.agent?
          agent = t
          if @location.agent_allowed
            @wallets = @wallets + agent.wallets
          end
        elsif t.merchant? && !t.merchant_id.nil?
          @wallets = @wallets
        end
      end
    end
    render :show
  end

  def tip_modal
    @location = Location.find params[:location_id]
    @wallet= Wallet.find params[:wallet_id]
    if current_user.merchant_id.nil?
      @sub_merchants = @user.sub_merchants.where(archived: false)
    else
      parent=User.find(current_user.merchant_id)
      @sub_merchants = parent.sub_merchants.where(archived: false)
    end
    respond_to :js
  end

  def transactions
    per_page = params[:tx_filter] || 10
    transaction_main_types
    @wallet = Wallet.friendly.find params[:wallet_id]
    allowed_wallets = []
    if current_user.merchant_id.present?
      Location.includes(:wallets).where(id: current_user.wallets.primary.pluck(:location_id).compact).each do |l|
        allowed_wallets.push(l.wallets.pluck(:id))
      end
    else
      locations_ids = current_user.attached_locations
      Location.includes(:wallets).where("locations.merchant_id = ? OR locations.id IN (?)",current_user.id, locations_ids).each do |l|
        allowed_wallets.push(l.wallets.pluck(:id))
      end
    end
    unless allowed_wallets.flatten.include?(@wallet.id)
      flash[:notice] = "Wallet Not Found!"
      return redirect_back(fallback_location: root_path)
    end

    if @wallet.primary?
      @heading= "#{@wallet.wallet_type.capitalize} Wallet Transactions"
      @time_option = [{value:"00:00",view:"12:00 am"},{value:"01:00",view:"01:00 am"},{value:"02:00",view:"02:00 am"},{value:"03:00",view:"03:00 am"}, {value:"04:00",view:"04:00 am"},{value:"05:00",view:"05:00 am"}, {value:"06:00",view:"06:00 am"},{value:"07:00",view:"07:00 am"}, {value:"08:00",view:"08:00 am"},{value:"09:00",view:"09:00 am"}, {value:"10:00",view:"10:00 am"},{value:"11:00",view:"11:00 am"}, {value:"12:00",view:"12:00 pm"},{value:"13:00",view:"01:00 pm"}, {value:"14:00",view:"02:00 pm"},{value:"15:00",view:"03:00 pm"}, {value:"16:00",view:"04:00 pm"},{value:"17:00",view:"05:00 pm"}, {value:"18:00",view:"06:00 pm"},{value:"19:00",view:"07:00 pm"}, {value:"20:00",view:"08:00 pm"},{value:"21:00",view:"09:00 pm"}, {value:"22:00",view:"10:00 pm"},{value:"23:00",view:"11:00 pm"}]
      if params[:query].present?
        @transactions = searching_merchant_local_transaction(params[:query], @wallet.id,params[:trans],nil,nil,nil,nil,nil,nil,per_page, params[:page])
      else
        if params[:trans] == "success"
          @transactions = Transaction.where('sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string', wallets: @wallet.id, string: "#{@wallet.id}").where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee", "CBK Lost"]).where.not(main_type: "Sale Issue").order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
          # @transactions = Transaction.where(sender_wallet_id: @wallet.id, status: ["approved", "refunded", "complete"]).where.not(main_type: "Sale Issue").or(Transaction.where(receiver_wallet_id: @wallet.id,status: ["approved", "refunded", "complete"])).where.not(main_type: "Sale Issue").or(Transaction.where(from: @wallet.id,status: ["CBK Won", "Chargeback", "cbk_fee"])).or(Transaction.where(to: @wallet.id,status: ["CBK Won", "Chargeback", "cbk_fee"])).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
        elsif params[:trans] == "refund"
          @transactions = Transaction.where(sender_wallet_id: @wallet.id, status: ["approved"],main_type:'refund').order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
        elsif params[:trans] == "decline"
          location_id=Wallet.find_by(id: @wallet.id).try(:location_id)
          # @transactions = Transaction.where(sender_wallet_id: @wallet.id,status: "pending").or(Transaction.where(receiver_wallet_id: @wallet.id,status: "pending")).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
          @transactions = Transaction.where("(sender_wallet_id = :wallet OR receiver_wallet_id = :wallet) AND status = 'pending'", wallet: @wallet.id).where.not(main_type: "Sale Issue").or(Transaction.where("tags->'location'->>'id' = ? AND status LIKE ?","#{location_id}", 'pending')).distinct.order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
        else
          @transactions = Transaction.where('sender_wallet_id = :wallets OR receiver_wallet_id = :wallets OR transactions.to LIKE :string OR transactions.from LIKE :string', wallets: @wallet.id, string: "#{@wallet.id}").where(status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee", "CBK Lost"]).where.not(main_type: "Sale Issue").order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
          # @transactions = Transaction.where(sender_wallet_id: @wallet.id, status: ["approved", "refunded", "complete","CBK Won", "Chargeback", "cbk_fee"]).or(Transaction.where(receiver_wallet_id: @wallet.id, status: ["approved", "refunded", "complete"])).or(Transaction.where(from: @wallet.id,status: ["CBK Won", "Chargeback", "cbk_fee"])).or(Transaction.where(to: @wallet.id,status: ["CBK Won", "Chargeback", "cbk_fee"])).order(created_at: :desc).per_page_kaminari(params[:page]).per(per_page)
        end
      end
      @filters = [10,25,50,100]
      render template: 'merchant/locations/location_transaction'
    else
      amount = nil
      @heading = "#{@wallet.wallet_type.capitalize} Wallet Transactions"
      @location = @wallet.location
      ledger = @location.ledger if @location.present?
      @primary_wallet=@location.wallets.primary.first
      #transactions = getting_wallet_transaction_from_seq(params,@wallet, ledger)
      #@transactions = transactions[:transactions]
      if params[:query].present?
        @transactions = searching_merchant_local_transaction(params[:query], @wallet.id).per_page_kaminari(params[:page]).per(per_page)
      else
        @transactions = BlockTransaction.where('(sender_wallet_id = ? OR receiver_wallet_id = ?) AND block_transactions.main_type IN (?)', @wallet.id, @wallet.id, ["Reserve Money Return", "Reserve Money Deposit","Payed Tip","Account Transfer","Sale Tip", "Tip transfer"]).order(timestamp: :desc).per_page_kaminari(params[:page]).per(per_page)
      end
      #@transactions = Kaminari.paginate_array(@transactions).page(params[:page]).per(per_page)
     # @cursor = transactions[:cursor]
      #@last_page = transactions[:last_page]
      #@page_records = 0

      #@seq_transaction = []
      #@seq_transaction << {transactions: @transactions.present? ? @transactions : "", cursor: @cursor, last_page: @last_page, transactions_count: @transactions.present? ? @transactions.count : 0, page_records: @page_records, wallet_id: @wallet.id}
      @transfer=Transfer.new
      @filters = [10,25,50,100]
      render template: 'merchant/locations/wallet_transactions'
    end

  end

  def searching_local_transaction(params, wallets = nil, trans_type)
    name_wallets = []
    amount = nil
    search_wallets = []
    conditions = []
    parameters = []
    if params[:name].present?
      name_users = User.where("name ILIKE ?","%#{params[:name].strip}%")
      if name_users.present?
        name_users.each do |u|
          if u.wallets.present?
            if u.wallets.primary.first.present?
              # name_wallets << u.wallets.primary.pluck(:id)
              name_wallets << u.wallets.primary.first.id
            end
          end
        end
      end
    end
    if params[:chargeback_transaction_id].present?
      conditions << "id IN (?)"
      parameters << params[:chargeback_transaction_id]
    end
    if trans_type == "decline"
      location_id=Wallet.find_by(id:wallets).try(:location_id) if wallets.present?
    end
    name_wallets = name_wallets.uniq
    search_wallets << name_wallets.flatten
    if params[:email].present?
      email_user = User.where(email: params[:email].strip).first
      email_wallets = email_user.wallets.primary.pluck(:id) if email_user.present?
      # email_wallet = email_wallets.first if email_wallets.present?
      search_wallets << email_wallets.flatten.uniq if email_wallets.present?
    end

    if params[:phone_number].present?
      phone_user = User.where(phone_number: params[:phone_number].split.join).first
      phone_wallets = phone_user.wallets.primary.pluck(:id) if phone_user.present?
      search_wallets << phone_wallets.flatten.uniq if phone_wallets.present?
    end
    search_wallets = search_wallets.flatten.uniq
    if search_wallets.present?
      conditions << "( sender_wallet_id IN (?) OR receiver_wallet_id IN (?) )"
      parameters << search_wallets
      parameters << search_wallets
    end

    # if search_wallets.present?
    #   conditions << "sender_wallet_id IN (?)"
    #   parameters << search_wallets
    #   conditions << "OR receiver_wallet_id IN (?)"
    #   parameters << search_wallets
    # end
    if params[:amount].present?
      conditions << "total_amount::VARCHAR LIKE ?"
      parameters << "#{params[:amount].split.join.to_s}%"
    end
    date = params[:date].present? ? parse_daterange(params[:date]) : nil
    if date.present?
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], 00,00,00, params[:offset]).utc,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], 23,59,59, params[:offset]).utc
      }
      if date[:first_date].present?
        conditions << "created_at >= ?"
        parameters << date[:first_date]
      end
      if date[:second_date].present?
        conditions << "created_at <= ?"
        parameters << date[:second_date]
      end
    end
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if type.present?
      conditions << "main_type In (?)"
      parameters << type
    end
    last4 = params[:last4].present? ? params[:last4].split.join : nil
    if last4.present?
      conditions << "last4 LIKE ?"
      parameters << "#{last4}%"
    end
    unless conditions.empty?
      conditions = [conditions.join(" AND "), *parameters]
      #= removing AND from search wallets query
      # if search_wallets.present?
      #   conditions.first.slice!(24)
      #   conditions.first.slice!(24)
      #   conditions.first.slice!(24)
      # end
    end
    if trans_type == "decline"
      t = Transaction.where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where(status: "pending").where.not(main_type: "Sale Issue").where(conditions).or(Transaction.where("tags->'location'->>'id' = ? AND status LIKE ?","#{location_id}", 'pending').where(conditions))
    elsif params[:chargeback_transaction_id].present?
      t=Transaction.where(conditions)
    else
      t = Transaction.where("receiver_wallet_id IN (?) OR sender_wallet_id IN (?)",wallets, wallets).where.not(main_type: "Sale Issue", status: "pending").where(conditions)
    end
    return t
  end


  def next_transactions
    @seq_transaction = JSON.parse(params[:seq_transaction])
    list = []
    @wallet = Wallet.find params["wallet_id"].to_i
    transactions = SequenceLib.next_page(params[:cursor])
    if transactions.present? && @wallet.wallet_type == "primary"
      list.push(parse_merchants_transactions(transactions, nil)) # parsing transactions for showing
    else
      list.push(parse_company_transactions(@wallet.id,transactions[:items])) # parsing transactions for showing
    end
    @transactions = list.flatten.compact.sort_by{|i| i[:timestamp]}.reverse
    @cursor = transactions[:cursor]
    @last_page = transactions[:last_page]
    @page_records= params[:page_size].to_i
    @transactions_count = @page_records + @transactions.count
    @seq_transaction << {transactions: @transactions, cursor: @cursor, last_page: @last_page, transactions_count: @transactions_count, page_records: @page_records, wallet_id: @wallet.id}
    respond_to :js
  end

  def previouss_transactions
    @seq_transaction = JSON.parse(params[:seq_transaction])
    @seq_transaction.pop()
    @transactions = @seq_transaction.last["transactions"]
    @cursor = @seq_transaction.last["cursor"]
    @last_page = @seq_transaction.last["last_page"]
    @page_records= @seq_transaction.last["page_records"].to_i
    @transactions_count = @seq_transaction.last["transactions_count"].to_i
    @wallet_id = @seq_transaction.last["wallet_id"].to_i
    @wallet = Wallet.find_by(id: @wallet_id)
    respond_to :js
  end

  def getting_wallet_transaction_from_seq(params,wallet,ledger=nil)
    cursor = nil
    transactions = nil
    last_page = nil
    if wallet.reserve? || wallet.tip?
      if params[:query].present?
        result = searching_transaction(params[:query], wallet.id)
        transactions = []
        unless result.nil?
          transactions.push(parse_company_transactions(wallet.id,result[:items]))  if @wallet.present? # parsing transactions for showing
        end
        transactions = transactions.flatten.compact
        transactions = transactions.sort_by{|i| i[:timestamp]}.reverse
        cursor = result[:cursor] if result.present?
        last_page = result[:last_page] if result.present?
      else
        transactions = all_company_transactions(wallet.id, wallet.id, 0, ledger) if wallet.present?
        cursor = transactions.second
        last_page = transactions.third
        transactions = transactions.first.present? ? transactions.first.sort_by{|i| i[:timestamp]}.reverse : []
      end
    else
      if params[:query].present?
        result = searching_transaction(params[:query], wallet.id)
        transactions = []
        unless result.nil?
          transactions.push(parse_merchants_transactions(result, nil)) # parsing transactions for showing
        end
        transactions = transactions.flatten.compact
        transactions = transactions.sort_by{|i| i[:timestamp]}.reverse
        cursor = result[:cursor] if result.present?
        last_page = result[:last_page] if result.present?
      else
        transactions = all_merchant_transactions([wallet.id], '', ledger).to_a
        if transactions.present?
          cursor = transactions.second
          last_page = transactions.third
          transactions = transactions.first.present? ? transactions.first.sort_by{|i| i[:timestamp]}.reverse : []
        end
      end
    end
    return {:transactions => transactions.present? ? transactions : "", :cursor => cursor.present? ? cursor : "" ,:last_page => last_page}
  end

  def searching_transaction(params, wallets)
    name_wallets = []
    amount = nil
    if params[:name].present?
      name_users = User.where("name ILIKE :name",  name: "%#{params[:name]}%")
      if name_users.present?
        name_users.each do |u|
          if u.wallets.present?
            if u.wallets.primary.present?
              name_wallets << u.wallets.primary.pluck(:id)
            end
          end
        end
      end
    end
    name_wallets = name_wallets.uniq
    if params[:email].present?
      email_user = User.where(email: params[:email]).first
      email_wallets = email_user.wallets.primary.pluck(:id) if email_user.present?
      email_wallet = email_wallets.first if email_wallets.present?
    end

    if params[:phone_number].present?
      phone_user = User.where(email: params[:phone_number]).first
      phone_wallets = phone_user.wallets.primary.pluck(:id) if phone_user.present?
      phone_wallet = phone_wallets.first if phone_wallets.present?
    end
    if params[:amount].present?
      amount = SequenceLib.cents(params[:amount])
    end
    date = params[:date].present? ? parse_daterange(params[:date]) : nil
    if date.present?
      time_params={time1:params[:time1],time2:params[:time2]}
      time = parse_time(time_params)
      unless params[:offset].include?(':')
        params[:offset].insert(3,':')
      end
      puts "Offset is ======" , params[:offset]
      date = {
          first_date: Time.new(date.first[:year], date.first[:month], date.first[:day], time.first.first(2),00,00, params[:offset]).to_datetime.rfc3339,
          second_date: Time.new(date.second[:year], date.second[:month], date.second[:day], time.second.first(2),59,59, params[:offset]).to_datetime.rfc3339
      }
    end
    if params[:type].present?
      if params[:type].kind_of?(Array)
        type = params[:type]
      else
        type = JSON.parse(params[:type])
      end
    end
    if params[:block_id].present?
      block_id=params[:block_id]
      block_id  = Transaction.where("seq_transaction_id ILIKE :block_id", block_id: "#{params[:block_id]}%").try(:first).try(:seq_transaction_id) if params[:block_id].present?
    end
    last4 = params[:last4].present? ? params[:last4] : nil
    transactions = SequenceLib.transactions_merchant_search([wallets].flatten,name_wallets,email_wallet,phone_wallet,amount,date,last4,nil,type,block_id)
    return transactions
  end

#   --------------------------Merchant Balance----------------------------------
  def show_merchant_balance
    source = params[:source] if params[:source].present?
    if source.present?
      balance = show_balance(source)
      if balance.present?
        balance = balance - HoldInRear.calculate_pending(source)
        render status: 200 , json:{:balance => number_with_precision(balance,precision:2, delimiter: ',')}
      else
        render status: 200 , json:{:balance => ""}
      end
    else
      render status: 200 , json:{:balance => ""}
    end
  end
#   --------------------------Transfers ----------------------------------------------
  def transfers
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @filters = [10,25,50,100]
    @heading="Transfer Money"
    user_ids=[]
    user_ids.push(current_user.id)
    if current_user.merchant_id.present?
      user = User.find(current_user.merchant_id)
      user.sub_merchants.each do |sub_merchant|
        user_ids.push(sub_merchant.id)
      end
    else
      current_user.sub_merchants.each do |sub_merchant|
        user_ids.push(sub_merchant.id)
      end
    end
    if current_user.merchant_id.present?
      wallet_ids = current_user.wallets.primary.where.not(location_id: nil).pluck(:id)
      @transfers=Transfer.where("user_id IN (:user_id) OR user_id = :merchant_id ", user_id: user_ids, merchant_id: current_user.merchant_id).where("from_wallet IN (?) OR to_wallet IN (?)",wallet_ids,wallet_ids).order("id DESC").paginate(:page => params[:page], :per_page => page_size)
    else
      @transfers=Transfer.where("user_id IN (:user_id) OR user_id = :merchant_id ", user_id: user_ids, merchant_id: current_user.merchant_id).order("id DESC").paginate(:page => params[:page], :per_page => page_size)
    end
    @merchant_wallets=""
    if current_user.MERCHANT?
      wallets = []
      locations_ids = current_user.attached_other_location
      locations = Location.where(id: locations_ids).eager_load(:wallets)
      locations.try(:each) do |location|
        wallets << location.wallets.primary
      end
      if current_user.MERCHANT? && current_user.merchant_id.nil?
        @merchant_wallets = current_user.wallets.primary + wallets
        @merchant_wallets = @merchant_wallets.flatten
      else
        @merchant_wallets = current_user.wallets.primary.where.not(location_id: nil) + wallets
        @merchant_wallets = @merchant_wallets.flatten
      end
      @transfer = Transfer.new(amount: "")
    end
  end
  
#   --------------------------Merchant Transfer Add----------------------------------
  def transfer_post
    if new_transfer_params[:amount].to_f <= 0
      flash[:error] = I18n.t 'errors.withdrawl.amount_must_greater_than_zero'
      return redirect_back(fallback_location: root_path)
    end
    if new_transfer_params[:from_wallet] == new_transfer_params[:to_wallet]
      flash[:error] = I18n.t 'merchant.controller.cant_same_walt'
      return redirect_back(fallback_location: root_path)
    end
    balance=show_balance(new_transfer_params[:from_wallet])
    balance= balance - HoldInRear.calculate_pending(new_transfer_params[:from_wallet])
    if balance < new_transfer_params[:amount].to_f
      flash[:notification_msg] = I18n.t('merchant.controller.insufficient_balance')
      return redirect_back(fallback_location: root_path)
    end
    timezone= TIMEZONE
    if cookies[:timezone].present?
      timezone=cookies[:timezone]
      today_date=Time.now.in_time_zone(cookies[:timezone]).to_date.strftime("%m/%d/%Y")
    else
      today_date=Date.today.strftime("%m/%d/%Y")
    end
    params[:transfer]["transfer_timezone"]=timezone
    transfer_date=new_transfer_params[:transfer_date] if new_transfer_params[:transfer_date].present?
    params[:transfer][:transfer_date]=Date.strptime(transfer_date,"%m/%d/%Y") if params[:transfer][:transfer_date].present?
    transfer=current_user.transfers.new(new_transfer_params)
    if today_date==transfer_date
      transfer_details = {
          amount: new_transfer_params[:amount],
          memo: new_transfer_params[:memo].present? ? new_transfer_params[:memo] : '',
          date: transfer_date,
          transfer_timezone: timezone
      }
      sender_dba=get_business_name(Wallet.find_by(id:new_transfer_params[:from_wallet]))
      receiver_dba=get_business_name(Wallet.find_by(id:new_transfer_params[:to_wallet]))
      db_transaction = @user.transactions.create(
          to: new_transfer_params[:to_wallet],
          from: new_transfer_params[:from_wallet],
          status: "pending",
          action: 'transfer',
          sender: @user,
          receiver_id: @user.try(:id),
          sender_name: sender_dba,
          receiver_name: receiver_dba,
          sender_wallet_id: new_transfer_params[:from_wallet],
          receiver_wallet_id: new_transfer_params[:to_wallet],
          amount: new_transfer_params[:amount].to_f,
          net_amount: new_transfer_params[:amount].to_f,
          total_amount:  new_transfer_params[:amount].to_f,
          ip: get_ip,
          main_type: "account_transfer",
          tags: transfer_details,
          sender_balance: balance,
          receiver_balance: SequenceLib.balance(new_transfer_params[:to_wallet].to_i)
      )
      transaction=SequenceLib.transfer(number_with_precision(new_transfer_params[:amount],precision:2),new_transfer_params[:from_wallet],new_transfer_params[:to_wallet],"account_transfer",0,nil,TypesEnumLib::GatewayType::Quickcard,nil,current_user,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)

      if transaction.present? && transaction.id.present?
        #= creating block transaction
        parsed_transactions = parse_block_transactions(transaction.actions, transaction.timestamp)
        save_block_trans(parsed_transactions) if parsed_transactions.present?
        db_transaction.update(seq_transaction_id: transaction.id, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
        transfer.sequence_id =transaction.id
        transfer.status="completed"
        status=transfer.save
      else
        flash[:notification_msg] = I18n.t 'merchant.controller.notification_cannot_create_transfer'
        return redirect_back(fallback_location: root_path)
      end
    else
      transfer.status="pending"
      status=transfer.save
    end
    if status.present?
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_transfer'
      return redirect_back(fallback_location: root_path)
    else
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_cannot_create_transfer'
      return redirect_back(fallback_location: root_path)
    end
  end
  # ---------------------------------Merchant Transfer Update-------------------------
  def transfer_update
    if new_transfer_params[:amount].to_f <= 0
      flash[:error] = I18n.t 'errors.withdrawl.amount_must_greater_than_zero'
      return redirect_back(fallback_location: root_path)
    end
    if new_transfer_params[:from_wallet] == new_transfer_params[:to_wallet]
      flash[:error] = I18n.t 'merchant.controller.cant_same_walt'
      return redirect_back(fallback_location: root_path)
    end
    balance=show_balance(new_transfer_params[:from_wallet])
    balance= balance - HoldInRear.calculate_pending(new_transfer_params[:from_wallet])
    if balance < new_transfer_params[:amount].to_f
      flash[:notification_msg] = I18n.t('merchant.controller.insufficient_balance')
      return redirect_back(fallback_location: root_path)
    end
    @transfer=Transfer.find_by_id(params[:transfer_id])
    if  @transfer.nil?
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_cant_find_trans'
      return redirect_back(fallback_location: root_path)
    end
    timezone= TIMEZONE
    timezone=cookies[:timezone] if cookies[:timezone].present?
    today_date=Time.now.in_time_zone(timezone).to_date.strftime("%m/%d/%Y")
    params[:transfer]["transfer_timezone"]=timezone
    transfer_date=new_transfer_params[:transfer_date] if new_transfer_params[:transfer_date].present?
    params[:transfer][:transfer_date]=Date.strptime(transfer_date,"%m/%d/%Y") if params[:transfer][:transfer_date].present?
    if today_date==transfer_date
      transfer_details = {
          amount: new_transfer_params[:amount],
          memo: new_transfer_params[:memo].present? ? new_transfer_params[:memo] : '',
          date: transfer_date,
          transfer_timezone: timezone
      }
      db_transaction = current_user.transactions.create(
          to: new_transfer_params[:to_wallet],
          from: new_transfer_params[:from_wallet],
          status: "pending",
          action: 'transfer',
          sender: current_user,
          receiver_id: current_user.try(:id),
          sender_wallet_id: new_transfer_params[:from_wallet],
          receiver_wallet_id: new_transfer_params[:to_wallet],
          amount: new_transfer_params[:amount].to_f,
          net_amount: new_transfer_params[:amount].to_f,
          total_amount:  new_transfer_params[:amount].to_f,
          ip: get_ip,
          main_type: "account_transfer",
          tags: transfer_details,
          sender_balance: balance,
          receiver_balance: SequenceLib.balance(new_transfer_params[:to_wallet].to_i)
      )
      transaction=SequenceLib.transfer(number_with_precision(new_transfer_params[:amount],precision:2),new_transfer_params[:from_wallet],new_transfer_params[:to_wallet],"account_transfer",0,nil,TypesEnumLib::GatewayType::Quickcard,nil,current_user,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,transfer_details)
      if transaction.present? && transaction.id.present?
        @transfer.sequence_id =transaction.id
        db_transaction.update(seq_transaction_id: transaction.id, tags: transaction.actions.first.tags, status: "approved", timestamp: transaction.timestamp)
        @transfer.status="completed"
        status = @transfer.update(new_transfer_params)
      else
        flash[:notification_msg] = I18n.t 'merchant.controller.notification_cannot_create_transfer'
        return redirect_back(fallback_location: root_path)
      end
    else
      status = @transfer.update(new_transfer_params)
    end
    if status.present?
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_transfer'
      return redirect_back(fallback_location: root_path)
    else
      flash[:notification_msg] = I18n.t 'merchant.controller.notification_cannot_create_transfer'
      return redirect_back(fallback_location: root_path)
    end
  end
  # ----------------------------Merchant Transfer Delete-----------------------
  def transfer_delete
    transfer=Transfer.find_by_id(params[:id])
    if  transfer.nil?
      render status: 200 , json:{:status => "failed",:message => "Transfer Not Found"}
    end
    if transfer.destroy
      render status: 200 , json:{:status => "success",:message => "Transfer Deleted Successfully"}
    else
      render status: 200 , json:{:status => "failed",:message => "Cannot Delete Transfer"}
    end
  end

  private

  def new_transfer_params
    # params.require(:user).permit(:id ,:email, :password, :password_confirmation, :name ,:postal_address  , :phone_number , :status, :first_name, :last_name,)
    params.require(:transfer).permit(:from_wallet ,:to_wallet, :amount, :transfer_date, :memo,:transfer_timezone)
  end
end
