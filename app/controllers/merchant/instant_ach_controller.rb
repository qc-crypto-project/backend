class Merchant::InstantAchController < Merchant::BaseController
  skip_before_action  :set_wallet_balance, only: [:create_instant_ach, :new, :selected_locations, :location_data]
  skip_before_action :permitted_routes, only: [:location_data]

  class CheckError < StandardError; end

  def index
    page_size = params[:filter].present? ? params[:filter].to_i : 10
    @achIssueEnabled = true
    @filters = [10,25,50,100]
    if current_user.present? && !current_user.merchant_id.nil?
      parent_user = User.find(current_user.merchant_id)
      user_ids=[parent_user.id,current_user.id]
      wallet_ids = current_user.wallets.pluck(:id)
      if parent_user.oauth_apps.present?
        oauth_app = parent_user.oauth_apps.first
        @achIssueEnabled = false if oauth_app.is_block && parent_user.get_locations.uniq.map {|loc| loc.block_ach}.all?
      end
      @achs = TangoOrder.where(order_type: [5,6] ,user_id: user_ids, wallet_id: wallet_ids).order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
    else
      if current_user.sub_merchants.present?
        sub_id = current_user.sub_merchants.pluck(:id)
        user_ids=[current_user.id,sub_id]
        user_ids= user_ids.flatten if user_ids.present?
      else
        user_ids=current_user.id
      end
      if @user.oauth_apps.present?
        oauth_app = @user.oauth_apps.first
        @achIssueEnabled = false if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_ach}.all?
      end
      # @achs = current_user.tango_orders.instant_ach.order(id: :desc).per_page_kaminari(params[:page]).per(10)
      @achs = TangoOrder.where(order_type: [5,6] ,user_id: user_ids).order(id: :desc).per_page_kaminari(params[:page]).per(page_size)
    end
  end

  def request_instant_ach
    @data = []
    date_time = DateTime.now
    if params[:query].present? && params[:image].present?
      @data = params[:query].values
      if @user.merchant?
        if @user.merchant_id.present?
          @user.update(send_ach_bank_request: true)
          main_user = User.find_by(id: @user.merchant_id)
        else
          main_user = @user
          main_user.update(send_ach_bank_request: true)
        end
        users = User.where(id: main_user.id).or(User.where(merchant_id: main_user.id))
        users.update_all(merchant_ach_status: "pending", ach_bank_request_date: date_time)
      else
        @user.update(merchant_ach_status: "pending",send_ach_bank_request: true, ach_bank_request_date: date_time)
      end
      if @user.merchant?
        bank_details = {}
        @data.each do|d|
          location = Location.find_by(id: d["location_id"])
          account_no = AESCrypt.encrypt("#{location.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", d['bank_account'])
          bank = location.bank_details.create(bank_account_type: d["type"], bank_name: d["bank_name"], routing_no: d["bank_routing"], account_no: account_no)
          bank_details.merge!({"#{location.id}"=>"#{bank.id}"})
          location.bank_account=d['bank_account']
          location.bank_routing = d["bank_routing"]
          location.bank_account_type = d["type"]
          location.bank_account_name = d["bank_name"]
          location.save
        end
      else
        @data.each do|d|
          @user['system_fee']['bank_account']= AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", d["bank_account"])
          @user['system_fee']['bank_routing']=d["bank_routing"]
          @user['system_fee']['bank_account_type']=d["type"]
          @user['system_fee']['bank_account_name']=d["bank_name"]
        end
        @user.save
      end
      if @user.merchant?
        params[:image].each do|k,v|
          v["image"]["bank_detail_id"] = bank_details["#{v["image"]["location_id"]}"]
          save_image_with_params(v) if v.present?
        end
      else
        params[:image].each do|k,v|
          save_image_with_params(v) if v.present?
        end
      end

      # UserMailer.ach_request(@data.to_json,@user.id).deliver_later
      flash[:successss] = I18n.t 'merchant.controller.successful_ach_approve'
    else
      flash[:error] = I18n.t 'merchant.controller.missng_input'
    end
    redirect_back(fallback_location: root_path)
  end

  def new
    if @user.merchant?
      if @user.merchant_id.present?
        wallets = @user.wallets.primary.where.not(location_id: nil).pluck(:location_id)
        @locations = Location.where(id: wallets)
      else
        @locations = current_user.get_locations
      end
    else
      @locations = @user.wallets.primary
    end
    render partial: "new"
  end

  def selected_locations
    if params[:merchant_id].present?
      user = User.find(params[:merchant_id])
      @locations = user.get_locations
    else
      @locations = []
    end
    respond_to :js
  end

  def location_data
    if @user.merchant?
      if params[:location_id].present?
        @location = Location.find(params[:location_id])
      else
        @location = Location.new
      end
      wallets = @location.wallets.primary.last
      if wallets.present?
        @balance = show_balance(wallets.id) - HoldInRear.calculate_pending(wallets.id)
      else
        @balance = 0
      end
      if params[:type] == 'check'
        if @user.merchant?
          if @location.fees.present?
            fee =  @location.fees.try(:buy_rate).try(:first)
            if fee.present?
              @fee_dollar = fee.send_check
              @fee_perc = fee.redeem_fee
            end
          end
        end
        if @location.check_limit_type == "Transaction"
          @check_limit = @location.check_limit.to_f
          @check_limit_type = @location.check_limit_type
        elsif @location.check_limit_type == "Day"
          date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          instant_check = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status!=? and tango_orders.order_type IN (?)",@location.id,date,"VOID",[0,5]).where(order_type: 'check',void_transaction_id: nil)
          @check_limit = @location.check_limit.to_f
          @total_count = instant_check.sum(:actual_amount)
          @check_limit_type = @location.check_limit_type
        end
      elsif params[:type] == 'push_to_card'
        if @user.merchant?
          if @location.fees.present?
            fee =  @location.fees.try(:buy_rate).try(:first)
            if fee.present?
              @fee_dollar = fee.dc_deposit_fee_dollar
              @fee_perc = fee.dc_deposit_fee
            end
          end
        end
        if @location.push_to_card_limit_type == "Transaction"
          @push_to_card_limit = @location.push_to_card_limit.to_f
          @push_to_card_limit_type = @location.push_to_card_limit_type
        elsif @location.push_to_card_limit_type == "Day"
          # date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          startdate = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc
          enddate = DateTime.now.in_time_zone("Pacific Time (US & Canada)").end_of_day.utc
          date=startdate..enddate
          if current_user.merchant_id.present? && current_user.submerchant_type == 'admin_user'
            instant_achs = current_user.parent_merchant.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.status !=?",@location.id,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil).where(created_at: date)
          else
            instant_achs = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.status !=?",@location.id,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil).where(created_at: date)
          end
          @push_to_card_limit = @location.push_to_card_limit.to_f

          @total_count = instant_achs.sum(:actual_amount)
          @push_to_card_limit_type = @location.push_to_card_limit_type
        else
          @push_to_card_limit = @location.push_to_card_limit.to_f
          @push_to_card_limit_type = @location.push_to_card_limit_type
        end
      else
        if @user.merchant?
          if @location.fees.present?
            fee =  @location.fees.try(:buy_rate).try(:first)
            if fee.present?
              @fee_dollar = fee.ach_fee_dollar
              @fee_perc = fee.ach_fee
            end
          end
        end
        @banks = @location.bank_details
        if @location.ach_limit_type == "Transaction"
          @ach_limit = @location.ach_limit.to_f
          @ach_limit_type = @location.ach_limit_type
        elsif @location.ach_limit_type == "Day"
          date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
          instant_achs = current_user.tango_orders.joins(wallet: :location).where("wallets.location_id=? and tango_orders.created_at >=? and tango_orders.status !=?",@location.id,date,"VOID").instant_ach
          @ach_limit = @location.ach_limit.to_f
          @total_count = instant_achs.sum(:actual_amount)
          @ach_limit_type = @location.ach_limit_type
        end
        begin
          @bank_account = AESCrypt.decrypt("#{@location.id}-#{ENV['ACCOUNT-ENCRYPTOR']}",@location.bank_account)
        rescue
          @bank_account = @location.bank_account
        end
      end
      # =+=+=+=+=+=+=+=+=+ merchant end
    else
      if params[:location_id].present? && params[:location_id].to_i != 0
        @balance = show_balance(params[:location_id]) - HoldInRear.calculate_pending(params[:location_id])
        if params[:type] == 'check'
          @fee_dollar = @user.system_fee.try(:[],'send_check')
          @fee_perc = @user.system_fee.try(:[],'redeem_fee')
          if @user.system_fee.try(:[],'check_limit_type') == "Transaction"
            @check_limit = @user.system_fee.try(:[],'check_limit').to_f
            @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
          elsif @user.system_fee.try(:[],'check_limit_type') == "Day"
            date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
            instant_check = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=? and tango_orders.order_type IN (?)",params[:location_id],date,"VOID",[0,5])
            @check_limit = @user.system_fee.try(:[],'check_limit').to_f
            @total_count = instant_check.sum(:actual_amount)
            @check_limit_type = @user.system_fee.try(:[],'check_limit_type')
          end
        elsif params[:type] == 'push_to_card'
            @fee_dollar = @user.system_fee.try(:[],'push_to_card_dollar')
            @fee_perc = @user.system_fee.try(:[],'push_to_card_percent')
          if @user.system_fee.try(:[],'push_to_card_limit_type') == "Transaction"
            @push_to_card_limit = @user.system_fee.try(:[],'push_to_card_limit').to_f
            @push_to_card_limit_type = @user.system_fee.try(:[],'push_to_card_limit_type')
          elsif @user.system_fee.try(:[],'push_to_card_limit_type') == "Day"
            date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
            instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").where(order_type: 'instant_pay',void_transaction_id: nil)
            @push_to_card_limit = @user.system_fee.try(:[],'push_to_card_limit').to_f
            @total_count = instant_achs.sum(:actual_amount)
            @push_to_card_limit_type = @user.system_fee.try(:[],'push_to_card_limit_type')
          end
        elsif params[:type] == 'instant_ach'
          @fee_dollar = @user.system_fee.try(:[],'ach_dollar')
          @fee_perc = @user.system_fee.try(:[],'ach_percent')
          if @user.system_fee.try(:[],'ach_limit_type') == "Transaction"
             @ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
             @ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
          elsif @user.system_fee.try(:[],'ach_limit_type') == "Day"
              date = DateTime.now.in_time_zone("Pacific Time (US & Canada)").beginning_of_day.utc - 2.hours
              instant_achs = current_user.tango_orders.where("wallet_id=? and tango_orders.created_at >=? and tango_orders.status !=?",params[:location_id],date,"VOID").instant_ach
              @ach_limit = @user.system_fee.try(:[],'ach_limit').to_f
              @total_count = instant_achs.sum(:actual_amount)
              @ach_limit_type = @user.system_fee.try(:[],'ach_limit_type')
          end
        end
        #adding limits for push to card
      else
        @balance = 0
      end
    end
    respond_to :js
  end

  def create_instant_ach
    begin
      if params[:bank_routing].blank?
        result = setup_params
        raise "#{result.second}" if result.first
      end
      validate_parameters(
          {
              location_id: params[:location_id],
              bank_name: params[:bank_name],
              amount: params[:amount],
              bank_account_type: params[:bank_account_type],
              bank_routing: params[:bank_routing],
              bank_account: params[:bank_account],
              confirm_bank_account: params[:confirm_bank_account],
              email_address: params[:email_address],
              description: params[:description]
          }
      )
      raise I18n.t("errors.withdrawl.unmatch_routing_number") if params[:bank_account] != params[:confirm_bank_account]
      raise I18n.t("errors.withdrawl.amount_must_greater_than_zero") if params[:amount].to_f <= 0
      if @user.merchant?
        location = Location.find(params[:location_id])
        raise I18n.t('errors.withdrawl.blocked_ach') if location.block_ach
        raise I18n.t('errors.withdrawl.account_not_exist') if location.blank?
        # raise "Error Code 7009 Sorry but it looks like this function is currently not available. Please contact your administrator for details." if location.try(:block_withdrawal)
        merchant = current_user
        if current_user.present? && !current_user.merchant_id.nil?
          merchant = User.find(current_user.merchant_id)
          # merchant_id=merchant.id
        else
          merchant = current_user
          # merchant_id=merchant.id
        end
        if merchant.oauth_apps.present?
          oauth_app = merchant.oauth_apps.first
          raise I18n.t('merchant.controller.blocked_by_admin') if oauth_app.is_block && merchant.get_locations.uniq.map {|loc| loc.block_ach}.all?
        end
        # raise "Merchant doesn't exist." if merchant.blank?
        wallet = location.wallets.primary.first
        raise I18n.t('errors.withdrawl.wallet_not_exist') if wallet.blank?

        amount = params[:amount].to_f

        bank_name = params[:bank_name]
        recipient = params[:email_address]
        account_number = params[:bank_account]
        routing_number = params[:bank_routing]
        bank_account_type = params[:bank_account_type]
        description = params[:description]
        send_via = "ACH"
        type = 8

        # raise "Location first name or last name is missing." if location.first_name.blank? || location.last_name.blank?

        #= calculate fee start
        location_fee = location.fees if location.present?
        fee_object = location_fee.buy_rate.first if location_fee.present?
        fee_class = Payment::FeeCommission.new(merchant, nil, fee_object, wallet, location,TypesEnumLib::CommissionType::ACH)
        fee = fee_class.apply(amount.to_f, TypesEnumLib::CommissionType::ACH)
        splits = fee["splits"]
        if splits.present?
          agent_fee = splits["agent"]["amount"]
          iso_fee = splits["iso"]["amount"]
          iso_balance = show_balance(location.iso.wallets.primary.first.id)
          if splits["iso"]["iso_buyrate"].present? && splits["iso"]["iso_buyrate"] == true
            if iso_balance < iso_fee
              raise CheckError.new(I18n.t("errors.iso.iso_0005"))
            end
          else
            if iso_balance + iso_fee < agent_fee.to_f
              raise CheckError.new(I18n.t("errors.iso.iso_0006"))
            end
          end
        end
        #= calculate fee end
        amount_sum = amount + fee.try(:[],"fee").to_f
        balance = show_balance(wallet.id).try(:to_f)
        pending_amount = HoldInRear.calculate_pending(wallet.id)

        raise I18n.t("merchant.controller.insufficient_balance") if balance - pending_amount < amount_sum

        card_info = {
            :account_number => account_number,
            :account_type => bank_account_type
        }
        card_info = card_info.to_json
        account_number = account_number.last(4)
        encrypted_account_info = AESCrypt.encrypt("#{merchant.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{wallet.id}", card_info)
        rand = "#{rand(100..999)}#{Time.now.to_i.to_s}"
        ach_international = location.ach_international
        check = TangoOrder.new(user_id: merchant.id,
                               name: bank_name,
                               status: "PENDING",
                               amount: amount_sum,
                               actual_amount: amount,
                               check_fee: fee_object.try(:ach_fee_dollar),
                               fee_perc: deduct_fee(fee_object.try(:ach_fee).to_f, amount),
                               recipient: recipient,
                               checkId: rand,
                               description: description,
                               order_type: :instant_ach,
                               batch_date:Time.zone.now + 1.day,
                               approved: false,
                               account_type: bank_account_type,
                               wallet_id: wallet.id,
                               settled: :false,
                               account_token: encrypted_account_info,
                               send_via: send_via,
                               account_number: account_number,
                               routing_number: routing_number,
                               location_id: location.id,
                               amount_escrow: true,
                               ach_international: ach_international)
        location1 = location_to_parse(location)
        merchant1 = merchant_to_parse(merchant)
        tags = {
            "fee_perc" => splits,
            "location" => location1,
            "merchant" => merchant1,
            "send_check_user_info" => nil,
            "send_via" => send_via
        }
        escrow_wallet = Wallet.check_escrow.first
        escrow_user = escrow_wallet.try(:users).try(:first)
        gbox_fee = save_gbox_fee(splits, fee["fee"].to_f)
        total_fee = save_total_fee(splits, fee["fee"].to_f)
        sender_dba=get_business_name(wallet)
        transaction = merchant.transactions.create(
            to: recipient,
            status: "pending",
            amount: amount,
            sender: merchant,
            sender_name: sender_dba,
            sender_wallet_id: wallet.id,
            receiver_id: escrow_user.try(:id),
            receiver_wallet_id: escrow_wallet.try(:id),
            receiver_name: escrow_user.try(:name),
            sender_balance: balance,
            receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
            action: 'transfer',
            fee: total_fee.to_f,
            net_amount: amount.to_f,
            net_fee: total_fee.to_f,
            total_amount: amount_sum.to_f,
            gbox_fee: gbox_fee.to_f,
            iso_fee: save_iso_fee(splits),
            agent_fee: splits["agent"]["amount"].to_f,
            affiliate_fee: splits["affiliate"]["amount"].to_f,
            ip: get_ip,
            main_type: "ACH",
            tags: tags
        )
        user_info = fee.try(:[], "splits") || {}
        withdraw = withdraw(amount,wallet.id, total_fee || 0, TypesEnumLib::GatewayType::ACH,account_number,send_via,user_info, nil, nil, nil,TypesEnumLib::GatewayType::ACH,nil,true)

        if withdraw.present? && withdraw["actions"].present?
          parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
          save_block_trans(parsed_transactions,"add") if parsed_transactions.present?
          check.transaction_id = transaction.id
          check.save
          transaction.update(seq_transaction_id: withdraw["actions"].first["id"],
                             status: "approved",
                             privacy_fee: 0,
                             tags: withdraw["actions"].first.tags,
                             sub_type: withdraw["actions"].first.tags["sub_type"],
                             timestamp: withdraw["timestamp"]
          )
          flash[:success] = "Successfully sent a ACH to #{ recipient }"
        else
          flash[:error] = I18n.t 'merchant.controller.unaval_feature'
        end
      else
        iso_agent_ach
      end
    rescue => exe
      flash[:error] = exe.message
    ensure
      if flash[:error].blank? && flash[:success].blank?
        flash[:error] = I18n.t 'merchant.controller.except'
      end
      return redirect_back(fallback_location: root_path)
    end
  end

  def iso_agent_ach
    wallet = Wallet.find_by(id: params[:location_id])
    raise I18n.t('errors.withdrawl.wallet_not_exist') if wallet.blank?

    amount = params[:amount].to_f

    bank_name = params[:bank_name]
    recipient = params[:email_address]
    account_number = params[:bank_account]
    routing_number = params[:bank_routing]
    bank_account_type = params[:bank_account_type]
    description = params[:description]
    send_via = "ACH"

    fee_object = @user.system_fee if @user.system_fee.present? && @user.system_fee != "{}"
    if fee_object.present?
      send_check_fee = fee_object["ach_dollar"].present? ? fee_object["ach_dollar"].to_f : 0
      fee_perc_fee = fee_object["ach_percent"].present? ? deduct_fee(fee_object["ach_percent"].to_f, amount) : 0
    else
      send_check_fee = 0
      fee_perc_fee = 0
    end

    fee = send_check_fee + fee_perc_fee
    amount_sum = amount + fee
    balance = show_balance(params[:location_id])
    raise 'Error Code 2039 Insuficient balance!' if balance < amount_sum
    if @user.agent? || @user.affiliate?
      raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: @user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0)
    elsif @user.iso?
      raise CheckError.new(I18n.t('errors.withdrawl.keep_min_balance', min_balance: @user.system_fee.try(:[],'check_withdraw_limit').try(:to_i))) if balance - amount_sum < (@user.system_fee.try(:[],'check_withdraw_limit').present? ? @user.system_fee.try(:[],'check_withdraw_limit').try(:to_f) : 0) && @user.iso?
    end
    rand = "#{rand(100..999)}#{Time.now.to_i.to_s}"
    card_info = {
        :account_number => account_number,
        :account_type => bank_account_type
    }
    card_info = card_info.to_json
    account_number = account_number.last(4)
    encrypted_account_info = AESCrypt.encrypt("#{@user.id}-#{ENV['ACCOUNT-ENCRYPTOR']}-#{wallet.id}", card_info)
    escrow_wallet = Wallet.check_escrow.first
    escrow_user = escrow_wallet.try(:users).try(:first)
    ach_international = @user.system_fee["ach_international"]
    ach_international = false if ach_international.nil?
    check = TangoOrder.new(user_id: @user.id,
                           name: bank_name,
                           status: "PENDING",
                           amount: amount_sum,
                           actual_amount: amount,
                           check_fee: send_check_fee,
                           fee_perc: fee_perc_fee,
                           recipient: recipient,
                           checkId: rand,
                           description: description,
                           order_type: :instant_ach,
                           batch_date:Time.zone.now + 1.day,
                           approved: false,
                           account_type: bank_account_type,
                           wallet_id: wallet.id,
                           settled: :false,
                           account_token: encrypted_account_info,
                           send_via: send_via,
                           account_number: account_number,
                           routing_number: routing_number,
                           amount_escrow: true,
                           ach_international: ach_international)

    sender_dba=get_business_name(wallet)
    transaction = @user.transactions.create(
        to: recipient,
        from: nil,
        status: "pending",
        charge_id: nil,
        amount: amount,
        sender: @user,
        sender_name: sender_dba,
        sender_wallet_id: wallet.id,
        receiver_id: escrow_user.try(:id),
        receiver_wallet_id: escrow_wallet.try(:id),
        receiver_name: escrow_user.try(:name),
        sender_balance: balance,
        receiver_balance: SequenceLib.balance(escrow_wallet.try(:id)),
        action: 'transfer',
        fee: fee.to_f,
        net_amount: amount_sum.to_f - fee.to_f,
        net_fee: fee.to_f,
        total_amount: amount_sum.to_f,
        ip: get_ip,
        main_type: "ACH"
    )
    withdraw = withdraw(amount,wallet.id, fee.to_f, TypesEnumLib::GatewayType::ACH,account_number,send_via,nil, nil, nil, nil,TypesEnumLib::GatewayType::ACH, "other",true)
    puts "============WITHDRAW==================", withdraw
    if withdraw.present? && withdraw["actions"].present?
      parsed_transactions = parse_block_transactions(withdraw.actions, withdraw.timestamp)
      save_block_trans(parsed_transactions) if parsed_transactions.present?
      check.transaction_id = transaction.id
      check.save
      transaction.update(seq_transaction_id: withdraw["actions"].first["id"],
                         status: "approved",
                         privacy_fee: 0,
                         sub_type: withdraw["actions"].first.tags["sub_type"],
                         timestamp: withdraw["timestamp"],
                         tags: withdraw["actions"].first.tags
      )
      flash[:success] = "Successfully sent ACH request to Account ****#{ account_number.try(:last, 4) }"
    else
      flash[:error] = I18n.t 'merchant.controller.unaval_feature'
    end
  end
  def setup_params
    if @user.merchant?
      location_bank = BankDetail.find(params[:bank_account_name])
      return [true,"Bank not added.\nPlease email support@greenboxpos.com with ACH Bank Information."] if location_bank.try(:bank_name).blank? || location_bank.try(:account_no).blank? || location_bank.try(:routing_no).blank? || location_bank.try(:bank_account_type).blank?
      return [true, I18n.t('api.errors.wallet_not_found')] if location_bank.blank?
      params[:bank_name] = location_bank.bank_name
      params[:bank_account_type] = location_bank.bank_account_type
      params[:bank_routing] = location_bank.routing_no
      params[:bank_account] = AESCrypt.decrypt("#{params[:location_id]}-#{ENV['ACCOUNT_ENCRYPTOR']}",location_bank.account_no)
      params[:confirm_bank_account] = AESCrypt.decrypt("#{params[:location_id]}-#{ENV['ACCOUNT_ENCRYPTOR']}",location_bank.account_no)
      params[:email_address] = @user.email
      return [false,nil]
    else
      params[:bank_name] = @user['system_fee']['bank_account_name']
      params[:bank_account_type] =  @user['system_fee']['bank_account_type']
      params[:bank_routing] = @user['system_fee']['bank_routing']
      params[:bank_account] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])
      params[:confirm_bank_account] = AESCrypt.decrypt("#{@user.id}-#{ENV['ACCOUNT_ENCRYPTOR']}", @user['system_fee']['bank_account'])
      params[:email_address] = @user.email
      return [false,nil]
    end
  end
end
