class Merchant::CustomerDisputesController < Merchant::BaseController

  def index
    @customer_disputes = @user.disputes_as_merchant
  end

  def show
    @customer_dispute = CustomerDispute.find(params[:id])
    @transaction = @customer_dispute.dispute_transaction
    @tracker = @transaction.tracker
    @customer = @customer_dispute.customer
    @dispute_request_first = @customer_dispute.dispute_requests.first
    @dispute_requests_last = @customer_dispute.dispute_requests.last
    @merchant_offer = @customer_dispute.dispute_requests.where(request_type: "merchant_offer").first
    @final_merchant_offer = @customer_dispute.dispute_requests.where(request_type: "merchant_final_offer").last
    @pending_counter_offer = @customer_dispute.dispute_requests.where(user_id: @customer_dispute.customer_id,status: "pending",title: "Counteroffer").last
    @customer_offer =  @customer_dispute.dispute_requests.where(request_type: "counter_offer").last
    @return_to_sender = @customer_dispute.dispute_requests.where(request_type: "return_to_sender").last
    # TODO there should be only 1 pending dispute_requests
    render partial: 'show'
  end

end
