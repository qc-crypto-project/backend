class Merchant::GiftcardsController < Merchant::BaseController
  require 'raas'
  include Merchant::GiftcardsHelper
  include UsersHelper
  include ApplicationHelper
  before_action :validate_location, only: [:buy_card]
  skip_before_action :set_wallet_balance, only: [:show, :buy_card]

  # for getting giftcards list
  def giftcard_catalogues
    @heading = 'Buy GiftCards'
    begin
      client = TangoClient.instance
      catalog = client.catalog
      @result = catalog.get_catalog()
      @giftcards = get_all_giftcards(@result)
      # @giftcards = @giftcards
    rescue Exception => exc
      # p "Tango Exception Handled: ", exc
      # message = exc.message ? exc.message : 'Something went wrong!'
      # if exc.class != Raas::RaasGenericException && exc.errors.present? && exc.errors.count > 0
      #   message = exc.errors.first.message
      # end
      # return render_json_response({success: false, message: "#{message}"}, :ok)
    end
  end

  #  for showing giftcards of merchant
  def merchant_orders
    @heading = 'GiftCards'
    begin
      client = TangoClient.instance
      # orders = client.orders
      customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
      account_identifier = ENV["ACCOUNT_IDENTIFIER"]
      collect = Hash.new
      collect['account_identifier'] = account_identifier
      collect['customer_identifier'] = customer_identifier
      elements_per_block = 100
      collect['elements_per_block'] = elements_per_block
      page = 0
      collect['page'] = page

      orders = client.orders
      tangoIntialOrders = @user.tango_orders.order(created_at: :desc).select{|v| v.utid != nil}
      tangoIntialOrders = User.find(@user.merchant_id).tango_orders.order(created_at: :desc).select{|v| v.utid != nil} if @user.merchant_id.present? && @user.try(:permission).admin?
      @tango_orders = tangoIntialOrders.map do |order|
        object = orders.get_order(order.order_id)
        object.to_hash.merge(:catalog_image => order.catalog_image)
      end
      # return render_json_response({success: true, message: "Tango Orders List!", orders: tango_orders}, :ok)
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.try(:errors).present? && exc.try(:errors).try(:count).to_i > 0
        message = exc.errors.first.message
      end
      flash[:danger] = message
    end
  end

  def show
    @list=[]
    client = TangoClient.instance
    catalog = client.catalog
    @giftcards = catalog.get_catalog()

    if @user.merchant_id.nil? || @user.ISO? || @user.AGENT? || @user.PARTNER? || @user.AFFILIATE?
      @user.wallets.primary.each do |w|
        # @wallets << w
        @location = w.location
        if @location.present?
        location_status = @location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: location_status,
                  block_giftcard: @location.block_giftcard
              }
        else
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: false,
                  block_giftcard: @user.block_giftcard
              }
        end

      end
      if @user.wallets.qc_support.present?
        @wallets << @user.wallets.qc_support.first
      end
    else
      @main_user = User.find(@user.merchant_id)
      @main_user.wallets.primary.each do |w|
         location = w.location
          # @wallets <<
          location_status = location.is_block
          @list <<
              {
                  wallet_id: w.id,
                  wallet_name: w.name,
                  balance: show_balance(w.id),
                  location_status: location_status,
                  block_giftcard: location.block_giftcard
              }
      end
    end
    @giftcard={}
    @giftcards.brands.each do |b|
      br=b.items.select{|v|v.utid== params[:id]}
      if !br.empty?
        @giftcard = {gift_card: br.first,brand_name:b.brand_name,description:b.description,disclaimer:b.disclaimer, image_url: b.image_urls.map{|s| s.second}.third }
      end
    end
    respond_to :js
  end

  def buy_card
    fee = 0
    unless params[:email].present? &&  params[:image_url].present? && params[:wallet_id].present? &&  params[:utid].present? && params[:amount].present?
          flash[:danger] = I18n.t('merchant.controller.missng_parms')
          return redirect_back(fallback_location: root_path)
    end
    @user= current_user
    @user= User.find(current_user.merchant_id) if current_user.merchant_id.present? && current_user.try(:permission).admin?
    if @user.oauth_apps.present?
      oauth_app = @user.oauth_apps.first
      if oauth_app.is_block && @user.get_locations.uniq.map {|loc| loc.block_giftcard}.all?
        flash[:error] = I18n.t 'merchant.controller.blocked_by_admin'
        redirect_back(fallback_location: root_path)
      end
    end
    begin
      validate_parameters({wallet_id: params[:wallet_id], utid:  params[:utid], amount: params[:amount] })
      balance = show_balance(params[:wallet_id])
      client = TangoClient.instance
      if params[:amount].present? && params[:utid].present?
        fee_lib = FeeLib.new
        w = Wallet.find(params[:wallet_id])
        location = w.location
        if location.present?
          if location.is_block
            flash[:danger ] = I18n.t("errors.withdrawl.blocked_location")
            return redirect_back(fallback_location: root_path)
          end
          if location.block_giftcard
            flash[:danger ] = I18n.t 'api.registrations.unavailable'
            return redirect_back(fallback_location: root_path)
          end
        end
        amount = params[:amount].to_f
        actual_amount = params[:amount].to_f
        if @user.MERCHANT?
          location_fee = location.fees.buy_rate.first
          fee_class = Payment::FeeCommission.new(@user, nil, location_fee, w, location,TypesEnumLib::CommissionType::Giftcard)
          fee_value = fee_class.apply(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard)
          amount = amount + fee_value["fee"].to_f
          fee = fee_value["fee"].to_f
        elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
            data = deduct_giftcard_fee(@user,amount)
            amount = data[0]
            fee = data[1]
        else
          location_fee = fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
          amount = params[:amount].to_f + fee_lib.get_card_fee(params[:amount].to_f, TypesEnumLib::CommissionType::Giftcard).to_f
          fee = location_fee
        end
        if balance.to_i < amount.to_i
          flash[:danger] = I18n.t("merchant.controller.insufficient_balance")
        else
          account = client.accounts
          account_info = account.get_account('quickcard')
          if account_info.present? && account_info.current_balance.present?
            tango_balance = account_info.current_balance
          end
          if tango_balance.present? && tango_balance < amount.to_f
            flash[:danger] = I18n.t 'merchant.controller.danger_excetption'
          else
            if @user.MERCHANT?
              if balance.to_f - HoldInRear.calculate_pending(w.id) < amount.to_f
                flash[:danger ] = I18n.t("merchant.controller.insufficient_balance")
                return redirect_back(fallback_location: root_path)
              end
            end
              orders = client.orders
              customer_identifier = ENV["CUSTOMER_IDENTIFIER"]
              account_identifier = ENV["ACCOUNT_IDENTIFIER"]

              body = Raas::CreateOrderRequestModel.new
              body.account_identifier = account_identifier
              body.amount = params[:amount].to_f || 0
              body.customer_identifier = customer_identifier
              body.send_email = true
              body.sender = {
                  "email": 'admin@quickcard.me',
                  "firstName": "QuickCard",
                  "lastName": ""
              }
              body.recipient = {
                  "email": params[:email],
                  "firstName": @user.name || 'No Name',
                  "lastName": ""
              }
              body.utid = params[:utid]
              result = orders.create_order(body)
            if result
              p "TANGO ORDER: ", result
              tango_order =  @user.tango_orders.create!(
                  :utid => params[:utid],
                  :account_identifier => account_identifier,
                  :amount => body.amount,
                  :name => result.reward_name,
                  :recipient => params[:email],
                  :order_id => result.reference_order_id,
                  :catalog_image => params[:image_url] || nil,
                  :wallet_id => params[:wallet_id],
                  :gift_card_fee => fee
              )
              if @user.MERCHANT?
                user_info = fee_value["splits"]
                user_info["utid"] = params[:utid].present? ? params[:utid] : ''
                user_info["recipient_email"] = params[:email].present? ? params[:email] : ''
                user_info["brand_name"] = result.reward_name.present? ? result.reward_name : ''
                # admin_wallet = Wallet.where(wallet_type: 3).first
                tags = {
                    "fee_perc" => user_info
                }
                gbox_fee = save_gbox_fee(fee_value["splits"], fee.to_f)
                total_fee = save_total_fee(fee_value["splits"], fee.to_f)
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: total_fee.to_f,
                    net_amount: actual_amount.to_f,
                    net_fee: total_fee.to_f,
                    total_amount: amount.to_f,
                    gbox_fee: gbox_fee.to_f,
                    iso_fee: save_iso_fee(fee_value["splits"]),
                    agent_fee: fee_value["splits"]["agent"]["amount"].to_f,
                    affiliate_fee: fee_value["splits"]["affiliate"]["amount"].to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
                    tags: tags
                )
                transact = withdraw(params[:amount].to_f,params[:wallet_id], location_fee.giftcard_fee.to_f, TypesEnumLib::TransactionType::GiftCardPurchase, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango)
              elsif @user.ISO? || @user.AGENT? || @user.AFFILIATE?
                utid = params[:utid].present? ? params[:utid] : ''
                recipient_email = params[:email].present? ? params[:email] : ''
                brand_name = result.reward_name.present? ? result.reward_name : ''
                user_info = {utid: utid, recipient_email: recipient_email, brand_name:brand_name}
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: fee.to_f,
                    net_amount: actual_amount.to_f,
                    net_fee: fee.to_f,
                    total_amount: amount.to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase),
                    tags: user_info
                )
                transact = withdraw(amount,params[:wallet_id], fee, TypesEnumLib::TransactionType::GiftCard, nil,nil, user_info,nil,nil,nil,TypesEnumLib::GatewayType::Tango,'other')
              else
                # user_info = fee_lib.giftcard_fee_divsion(location,location_fee, nil, nil)
                transaction = @user.transactions.create(
                    to: nil,
                    from: params[:wallet_id].to_s,
                    status: "pending",
                    amount: actual_amount,
                    sender: @user,
                    sender_name: @user.try(:name),
                    sender_wallet_id: w.id,
                    sender_balance: balance,
                    action: 'retire',
                    fee: location_fee.to_f,
                    net_amount: actual_amount,
                    net_fee: location_fee.to_f,
                    total_amount: amount.to_f,
                    ip: get_ip,
                    main_type: updated_type(TypesEnumLib::TransactionType::GiftCardPurchase)
                )
                transact = withdraw(params[:amount].to_f,params[:wallet_id], location_fee.to_f, TypesEnumLib::TransactionType::GiftCardPurchase, nil,nil, nil,nil,nil,nil,TypesEnumLib::GatewayType::Tango)
              end
              if transact.present?
                tango_order.update(transaction_id: transaction.id)
                transaction.update(seq_transaction_id: transact.id, tags: transact.actions.first.tags, status: "approved", timestamp: transact.timestamp)
                #= creating block transaction
                parsed_transactions = parse_block_transactions(transact.actions, transact.timestamp)
                save_block_trans(parsed_transactions) if parsed_transactions.present?
              end
              flash[:success] = I18n.t 'merchant.controller.successful_order'
            end

            # transact = transaction_between(admin_wallet.id, @user.wallet_id, amount, "Tango Transfer", 0, "")
          end
        end
      else
        flash[:error] = I18n.t 'merchant.controller.missng_parms'
      end
    rescue Exception => exc
      p "Tango Exception Handled: ", exc
      message = exc.message ? exc.message : 'Something went wrong!'
      if exc.class != Raas::RaasGenericException && exc.message.present?
        message = exc.message
      end
      flash[:warning] = exc.message
    end
    redirect_back(fallback_location: root_path)
  end
end
