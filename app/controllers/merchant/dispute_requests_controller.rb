class Merchant::DisputeRequestsController < Merchant::BaseController
  before_action :set_dispute_request , only: [:update]
  EasyPost.api_key = ENV['EASY_POST_API_KEY']

  def new
    @customer_dispute = CustomerDispute.find(params[:customer_dispute_id])
    last_request = @customer_dispute.dispute_requests.last
    new_request = @customer_dispute.dispute_requests.create(user_id: @user.id,amount: params[:amount],message: params[:message],status: "pending",title: "Merchant's Offer",request_type: params[:status])
    last_request.status = "declined"
    last_request.save if new_request
    #Merchant Offer Notification and Email
    text = I18n.t('notifications.counter_offer',merchant_name: @customer_dispute.merchant.name, refund_id: @customer_dispute.id)
    Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Partial Refund Offer",nil,text)
    UserMailer.partial_refund_request_merchant(@customer_dispute.id,new_request.amount).deliver_later
    redirect_to merchant_customer_disputes_path
  end

  def return_product

    tracker = Tracker.find_by_id(params["tracker_id"])
    if tracker
      begin
        to_address = EasyPost::Address.retrieve(tracker.try(:easypost_id).try(:[],"to_address_id"))
        from_address = EasyPost::Address.retrieve(tracker.try(:easypost_id).try(:[],"from_address_id"))
        parcel = EasyPost::Parcel.retrieve(tracker.try(:easypost_id).try(:[],"parcel_id"))
        shipment = EasyPost::Shipment.create(  to_address: to_address,from_address: from_address,parcel: parcel,  is_return: true)
        @rates = shipment["rates"]
        @customer_dispute = CustomerDispute.find_by_id(params["customer_dispute_id"])
        render 'merchant/customer_disputes/return_product'
      rescue => e
        flash[:notice] = e.message
        redirect_to merchant_customer_disputes_path
      end
    end

  end

  def generate_label
    tracker = Tracker.find_by_id(params["tracker_id"])
    easypost = Payment::EasyPostGateway.new()
    generate_label = easypost.generate_label(params[:shipment_id],params[:rate_id])
    if generate_label[:success]
      tracker.is_return = true
      tracker.return_tracking_id = generate_label[:response]["tracker"]["id"]
      tracker.return_tracking_code = generate_label[:response]["tracking_code"]
      tracker.return_label_url = generate_label[:response]["postage_label"]["label_url"]
      tracker.return_tracking_details = generate_label[:response]["tracker"]["tracking_details"]
      tracker.return_easypost_ids = {
          from_address: generate_label[:response]["to_address"]["id"],
          to_address: generate_label[:response]["from_address"]["id"],
          lable_id: generate_label[:response]["postage_label"]["id"],
          parcel_id: generate_label[:response]["parcel"]["id"],
          shipment_id: params[:shipment_id],
          rate_id: params[:rate_id]}
      tracker.save
      @customer_dispute = CustomerDispute.find(params[:customer_dispute_id])
      last_request = @customer_dispute.dispute_requests.last
      new_request = @customer_dispute.dispute_requests.create(user_id: @user.id,amount: params[:amount],message: params[:message],status: "pending",title: "Return to sender",request_type: "return_to_sender")
      last_request.status = "declined"
      last_request.save if new_request
      location = Location.find(@customer_dispute.location_id)
      website = JSON.parse(location.web_site).last
      text = I18n.t('notifications.return_order',website: website ,order_id: @customer_dispute.dispute_transaction.order_id)
      Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"gernrate_label",nil,text)
      UserMailer.order_return_email(@customer_dispute.customer_id,@customer_dispute.id,@customer_dispute.location_id,@customer_dispute.dispute_transaction.order_id).deliver_later#Task 10e
      redirect_to merchant_customer_disputes_path
    else
      flash[:notice] = generate_label[:message]
      redirect_to merchant_customer_disputes_path
    end

  end

  def update
    partial_amount = nil
    change_tracker_status = nil
    check = "do_refund"
    # cancel cases and their response -- 1- full_refund -2- partial_cancel_order  -3- partial_fulfill_order  -4- dismissed_mutual_agreement

    # Refund cases and their response -- 1  refunded -2- decline_request

    if params[:status] == "refunded" # --- Refund cases
      main_status = "approved"
      status = "refunded"
      title = "Refunded (By Merchant)"
    elsif params[:status] == "decline_request"
      main_status = "declined"
      status = "declined"
      title = @dispute_request.title #"Waiting for Merchant's response" and message is not showing
      message = params[:message] if params[:message].present?
      check = "change_status_only" # no refund only status change
    elsif params[:status] == "offer_accept"
      main_status = "approved"
      status = "approved"
      title = @dispute_request.title
      partial_amount = @dispute_request.amount
    elsif params[:status] == "save refund"
      main_status = "approved"
      status = "declined"
      title = "Declined"
    elsif params[:status] == "full_refund" # -- Cancel cases
      # Run Refund Transaction (Complete Amount)
      main_status = "full_refund"
      status = "full_refund"
      title = I18n.t('customers.request.full_refund')
      change_tracker_status = "cancel"
    elsif params[:status] == "partial_fulfill_order"

      partial_amount = params[:amount].to_f
      main_status = "partial_send_order"
      status = "partial_send_order"
      title = I18n.t('customers.request.partial_refund_ship')

    elsif params[:status] == "partial_cancel_order"

      partial_amount = params[:amount].to_f
      main_status = "partial_cancel_order"
      status = "partial_cancel_order"
      title = I18n.t('customers.request.partial_refund_cancel')
      change_tracker_status = "cancel"
    elsif params[:status] == "dismissed_mutual_agreement"
      main_status = "mutual_agreement"
      status = "mutual_agreement"
      title = I18n.t('customers.request.mutual_agreement')
      check = "change_status_only" #in this case not any transaction or refund made so we only update the status of the request
    else
      check = "unknown"
    end

    ##Refunding transactions start
    if check == "do_refund"
      transaction = Transaction.find(@customer_dispute.transaction_id)
      merch       = transaction.receiver
      refund      = RefundHelper::RefundTransaction.new(transaction.seq_transaction_id, transaction,merch,partial_amount,title, @user)
      response    = refund.process
      ##Refunding transactions end
      #
      if response[:success] == true
        @customer_dispute.update(status: main_status, close_date: DateTime.now, refund_amount: partial_amount)

        @dispute_request.update(status: status, title: title, refund_amount: partial_amount)

        # Update Tracker to be canceled
        if change_tracker_status
          Tracker.where(transaction_id: transaction.id).last.update(status: change_tracker_status)
        end
        #Notification and Mail to customer for Cancel Orders
        if @customer_dispute.status == "full_refund" || @customer_dispute.status == "partial_send_order" || @customer_dispute.status == "partial_cancel_order" || @customer_dispute.status == "mutual_agreement"
        text = I18n.t('notifications.cancelled_confirmation', order_id: @customer_dispute.dispute_transaction.order_id)
        Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Order cancelled",nil,text)
        UserMailer.order_cancel_email_to_customer(@customer_dispute.id).deliver_later
        elsif @customer_dispute.status == "approved" && params[:status] == "refunded"
          txn = Transaction.find(@customer_dispute.transaction_id)
          txn_id = txn.try(:seq_transaction_id)[0..5]
          text = I18n.t('notifications.approved_refund',dba_name: txn.receiver_name, amount: @customer_dispute.amount, trans_id: txn_id)
          Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Approved Refund",nil,text)
          UserMailer.approved_refund_request(@customer_dispute.id).deliver_later
        elsif @customer_dispute.status == "approved" && params[:status] == "offer_accept"
          text = I18n.t('notifications.refunded_counter_offer',merchant_name: @customer_dispute.merchant.name,refund_amount: partial_amount, refund_id: @customer_dispute.id)
          Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Merchant Approved Customer Offer",nil,text)
          UserMailer.approved_refund_request(@customer_dispute.id).deliver_later
        elsif @customer_dispute.status == "declined" && @customer_dispute.dispute_requests.count > 1
          text = I18n.t('notifications.declined_counter_offer',merchant_name: @customer_dispute.merchant.name,refund_amount: @customer_dispute.dispute_requests.last.amount, refund_id: @customer_dispute.id)
          Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Merchant Declined Customer Offer",nil,text)
          UserMailer.merchant_declined_customer_offer(@customer_dispute.id,@customer_dispute.dispute_requests.last.amount).deliver_later

        end
        # send flash
        flash[:success] = "Refunded Successfully"
      else
        flash[:error] = response[:message]
      end
    elsif check == "change_status_only"
      @customer_dispute.update(status: main_status, close_date: DateTime.now, refund_amount: partial_amount)
      @dispute_request.update(status: status, message: message,title: title, refund_amount: partial_amount)
      flash[:success] = "Request close Successfully"
    else
      flash[:error] = "Status not known"
    end

    return redirect_to merchant_customer_disputes_path
  end

  private

  def set_dispute_request
    @customer_dispute = CustomerDispute.find(params[:customer_dispute_id])
    @dispute_request = @customer_dispute.dispute_requests.find(params[:id])
  end
end
