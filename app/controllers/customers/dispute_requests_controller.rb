class Customers::DisputeRequestsController < Customers::BaseController
	def create
		customer_dispute = CustomerDispute.find(params["dispute_id"])
		customer_dispute.dispute_requests.update_all(status: "declined")
		customer_dispute_request = customer_dispute.dispute_requests.build(request_type: "counter_offer" , status: "pending" , user_id: @user.id , title: "Counteroffer" , amount: params["amount"]) 
		if customer_dispute_request.save
			text = I18n.t('notifications.customer_counter_offer',customer_name:  customer_dispute.customer.name,refund_id: customer_dispute.id)
			Notification.notify_user(customer_dispute.merchant,customer_dispute.merchant,customer_dispute,"Customer Counter Offer",nil,text)
			UserMailer.partial_refund_request_customer_offer(customer_dispute.id).deliver_later
			flash[:notice] = "Counter offer submitted successfully!"
			return redirect_to customers_dispute_path(customer_dispute.id)
		end
	end
	def update
		@customer_dispute = CustomerDispute.find(params["dispute_id"])
		@dispute_request = DisputeRequest.find(params["id"])

		if params["status"] == "approved" || params["status"] == "declined"
			@dispute_request.update(status: params["status"])
			flash[:notice] = "Offer #{params["status"]} successfully!"
		end
		return redirect_to customers_dispute_path(@customer_dispute.id)
	end
end