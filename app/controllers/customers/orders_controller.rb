class Customers::OrdersController < Customers::BaseController
	def index
		@pending_orders =  Transaction.joins(:tracker).where(trackers: {user: current_user,status: ['new_order','pre_transit','in_transit','out_for_delivery'] ,id: Tracker.by_order_id(params[:order_id]) & Tracker.by_transaction_id(params[:transaction_id]) & Tracker.by_status(params[:order_status]) & Tracker.by_merchant_name(params[:merchant_name])  & Tracker.by_refund_case(params[:refund_no]) & Tracker.by_cancel_case(params[:cancel_no]) & Tracker.by_order_date(params[:order_date]) & Tracker.by_delivered_date(params[:deliver_date]) & Tracker.order_search(params[:order_search])}).order('created_at DESC')
		@orders = Transaction.joins(:tracker).where(:trackers => {user: current_user,status: ['delivered','return_to_sender','cancel'] ,id: Tracker.by_order_id(params[:order_id]) & Tracker.by_transaction_id(params[:transaction_id]) & Tracker.by_status(params[:order_status]) & Tracker.by_merchant_name(params[:merchant_name]) & Tracker.by_refund_case(params[:refund_no]) & Tracker.by_cancel_case(params[:cancel_no]) & Tracker.by_order_date(params[:order_date]) & Tracker.by_delivered_date(params[:deliver_date]) & Tracker.order_search(params[:order_search])}) .order('created_at DESC')		
	end
	def show
		@order = Transaction.find(params[:id])
		@tracker = @order.tracker
		@merchant = @order.receiver
	end
end 