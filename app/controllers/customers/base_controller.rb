class Customers::BaseController < ActionController::Base
	before_action :check_customer?
	layout 'customer'
	def check_customer?
		 gon.cable_url = "ws://#{ENV['CABLE_URL']}"
	   @user = current_user
		 @chat_notification=Message.where(recipient_id:current_user.id).where(read_at:nil).count if @user.present?
		 return redirect_to new_user_session_path(role: "customer"), notice: I18n.t('merchant.controller.unauthorize_access') unless current_user&.user?
	end
end