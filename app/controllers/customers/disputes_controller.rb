class Customers::DisputesController < Customers::BaseController

  def new
  	@order = Transaction.find(params[:order_id])
  	@tracker = @order.tracker
  	@customer_dispute = CustomerDispute.new
    @customer_dispute.evidences.build
    @status = @tracker.status == "delivered" ? "refund" : "cancel"
  end

  def index
    @cancel_orders = CustomerDispute.where(dispute_type: "cancel",customer_id: current_user.id , id: CustomerDispute.all_disputes(params[:all_disputes],params[:open_disputes],params[:close_disputes]) | CustomerDispute.open_disputes(params[:open_disputes]) | CustomerDispute.close_disputes(params[:close_disputes])).order('created_at DESC')
    @refund_orders = CustomerDispute.where(dispute_type: "refund",customer_id: current_user.id , id: CustomerDispute.all_disputes(params[:all_disputes],params[:open_disputes],params[:close_disputes]) | CustomerDispute.open_disputes(params[:open_disputes]) | CustomerDispute.close_disputes(params[:close_disputes])).order('created_at DESC')
  end

  def show
    @dispute = CustomerDispute.find(params[:id])
    @transaction=@dispute.dispute_transaction
    @tracker = @transaction.tracker
    @dispute_request = @dispute.dispute_requests.where(request_type: "initial_request").last
    @merchant_offer = @dispute.dispute_requests.where(user_id: @dispute.merchant_id,request_type: "merchant_offer").first
    @final_merchant_offer = @dispute.dispute_requests.where(user_id: @dispute.merchant_id,request_type: "merchant_final_offer").first
    @pending_merchant_offer = @dispute.dispute_requests.where(user_id: @dispute.merchant_id,status: "pending").last
    @customer_offer =  @dispute.dispute_requests.where(user_id: @dispute.customer_id,request_type: "counter_offer").last
    @return_to_sender = @dispute.dispute_requests.where(request_type: "return_to_sender").last
    @status = @dispute.dispute_type == "refund" ? "refund" : "cancel"
  end

  def create
  	@customer_dispute = CustomerDispute.new(dispute_params)
  	@order = Transaction.find(params["customer_dispute"][:transaction_id])
    @tracker = @order.tracker
    @status = @tracker.status == "delivered" ? "refund" : "cancel"
    params["products"] = params["products"].select{|v| v[:checked]} if params["products"].present?
    @total_amount = params[:products].present? ? 0 :  @order.total_amount
    if params["products"].present?
      params["products"].each do |prod|
        product = Product.find(prod["id"])
        @total_amount += product.amount * prod["refund_count"].to_i
      end
    end
  	@customer_dispute.assign_attributes(dispute_type: @status,location_id: @order.receiver_wallet.location_id ,customer_id: @user.id,amount: @total_amount,status: "pending",merchant_id: @order.receiver_id)
	  @customer_dispute.dispute_requests.build(request_type: "initial_request" ,user_id: @user.id,amount: @total_amount,status: "pending",title: I18n.t('customers.request.merchant_response'))
    
    if @customer_dispute.save
      if params["evidences_attributes"].present? && params["evidences_attributes"]["document"].present?
        params["evidences_attributes"]["document"].each do |evidence|
          @customer_dispute.evidences.create!(:document => evidence)
        end
      end
      if params["products"].present?
    		params["products"].each do |prod|
    			product = Product.find(prod["id"])
    			product.update(customer_dispute_id: @customer_dispute.id,refund_count: prod["refund_count"])
    		end
      end
      if @customer_dispute.dispute_type == "cancel"
        #Notification and Mail to Merchant
        text = I18n.t('notifications.cancelled_customer', order_id: @customer_dispute.dispute_transaction.order_id)
        Notification.notify_user(@customer_dispute.merchant,@customer_dispute.merchant,@customer_dispute,"Cancelled by customer",nil,text)
        UserMailer.customer_cancel_order(@customer_dispute.id).deliver_later
        #Notification and Mail to Customer
        text = I18n.t('notifications.cancelled_update', order_id: @customer_dispute.dispute_transaction.order_id)
        Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Cancelled by customer",nil,text)
        UserMailer.order_cancel_update(@customer_dispute.id).deliver_later
      else
        #Notification and Mail to Customer
        text = I18n.t('notifications.refund_open',refund_id: @customer_dispute.id, order_id: @customer_dispute.dispute_transaction.order_id)
        Notification.notify_user(@customer_dispute.customer,@customer_dispute.customer,@customer_dispute,"Refund opened",nil,text)
        UserMailer.request_refund_confirmation(@customer_dispute.id).deliver_later
        #Notification and Mail to Merchant
        text = I18n.t('notifications.refund_open_merchant',refund_id: @customer_dispute.id,refund_amount: @customer_dispute.amount, order_id: @customer_dispute.dispute_transaction.order_id)
        Notification.notify_user(@customer_dispute.merchant,@customer_dispute.merchant,@customer_dispute,"Refund opened",nil,text)
        UserMailer.request_refund_confirmation_merchant(@customer_dispute.id).deliver_later
      end
      render "successfully" , locals: {dispute_id: @customer_dispute.id,status: @status,merchant_name: @tracker.merchant.name}
  	else
  		redirect_to customers_orders_path , alert: @customer_dispute.try(:errors).try(:full_messages)
  	end
  end

  def update_notification
    notification =  Notification.find_by_id(params[:id])
    if notification && notification.read_at.nil? && notification.update!(read_at: DateTime.now.utc)
      render json: {success: true} , status: 200
    end
  end

  def update_tracker
    return_to_sender = DisputeRequest.find_by_id(params["request_id"])
    if return_to_sender.status == "pending"
      return_to_sender.status =  "approved"
      if return_to_sender.save
        tracker = Tracker.find_by_id(params["tracker_id"])
        tracker.return_status = "pre_transit"
        tracker.save
      end
    end
  end

  def mail_generate_label
    tracker = Tracker.find_by_id(params["tracker_id"])
    if UserMailer.return_label(@user,tracker.return_label_url).deliver_later
      flash[:notice] = "Successfully sent to your email!"
      redirect_to customers_dispute_path(params["id"])
    end
  end

  private

  def dispute_params
  	params.require(:customer_dispute).permit(:transaction_id,:dispute_type,:reason,:reason_detail,:status,:description,:amount,:merchant_id,:customer_id,:close_date,evidences_attributes: [:id,:document,:_destroy])
  end
end