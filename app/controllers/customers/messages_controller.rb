class Customers::MessagesController < Customers::BaseController

  def index
    @chats = Message.where("actor_id = ? OR recipient_id = ? " ,current_user.id,current_user.id).limit(10).group_by(&:transaction_id)
    @messages = []
    @chat = {}
    @count=0
    if params[:transaction_id].present?
      @chat = Message.where(transaction_id: params[:transaction_id]).limit(10)
      @chat= {params[:transaction_id] => @chat}
      @messages = @chat.values.flatten
      @messages = @messages.sort_by &:created_at
    end

    if @chats.present? && !params[:transaction_id].present?

      @msgs=Message.where(transaction_id:@chats.keys.first,read_at:nil,recipient_id:current_user.id).limit(10)
      if @msgs.present?
        @count=@msgs.count
        @msgs.update_all(read_at:DateTime.now)
      end
      temp = @chats.first
      first_messages = Message.where(transaction_id: @chats.keys.first).order(id: :desc).limit(10)
      @chat = {temp[0] => first_messages}
      @messages = first_messages
      @messages = @messages.sort_by &:created_at
      @chats[temp[0]] = first_messages
    end
  end

  def show
    if params[:last_msg_id]#for json response code
      @chat = Message.where(transaction_id: params[:id]).order(id: :desc).where('id < ?', params[:last_msg_id]).limit(10)

      @chat= {params[:id] => @chat}
      @messages = @chat.values.flatten
      #for attachments
      next_item = -1
      @messages.each_with_index do |msg,index|
        if index <= next_item
          next
        end
        if msg.try(:attachments).present?
          msg.attachments.each_with_index do |attach,i|
            doc = attach
            doc.document_content_type = attach.document.url
            doc.document_file_size = msg.recipient_id
            @messages.insert(index,doc)
            next_item = index + i + 1
          end
        end
      end
    else
        @chat = Message.where(transaction_id: params[:id]).order(id: :desc).limit(10)
        @chat= {params[:id] => @chat}
        @messages = @chat.values.flatten
        @messages = @messages.sort_by &:created_at
        Message.where(id:@messages.pluck(:id),read_at:nil,recipient_id:current_user.id).update_all(read_at:DateTime.now) if @messages.present?
    end

    respond_to do |format|
      format.html { render partial: "show" }
      format.json { render json: @messages }
    end
    # render partial: 'show'
  end
  # def show
  #   @chat = Message.where(transaction_id: params[:id])
  #   @chat= {params[:id] => @chat}
  #   @messages = @chat.values.flatten
  #   @messages = @messages.sort_by &:created_at
  #   Message.where(id:@messages.pluck(:id),read_at:nil,recipient_id:current_user.id).update_all(read_at:DateTime.now) if @messages.present?
  #   render partial: 'show'
  # end

  def new
    existing_chat = Message.where(transaction_id: params[:transaction_id])
    if existing_chat.present?
      redirect_to customers_messages_path
    else
      @message = Message.new(actor_id: params[:actor_id],recipient_id: params[:recipient_id],transaction_id: params[:transaction_id])
      if @message.save
        redirect_to customers_messages_path
      end
    end


  end

  def save_chat
    @message=Message.new(actor_id: current_user.id,transaction_id: params[:transaction_id],text: params[:message],recipient_id: params[:recipient_id])
    if @message.save
      if params["attachments"].present? && params["attachments"]["document"].present?
        params["attachments"]["document"].each do |attachment|
          @message.attachments.create!(:document => attachment)
        end
      end
      created_at=@message.created_at.in_time_zone(cookies[:timezone]).strftime("%A %I:%M %p")
      attach_ary = []
      if @message.attachments.present?
        @message.attachments.each do |attach|
          if ["jpg", "jpeg", "png"].include?(attach.document_file_name.split(".").last) || ["image/jpg", "image/jpeg", "image/png"].include?(attach.document_content_type)
            image = true
          else
            image = false
          end
          attach_ary.push({"name"=>"#{attach.document_file_name}","url" => "https:#{attach.document.url}", "is_image"=>image})
        end
      end
      ActionCable.server.broadcast "chat_channel_#{params[:recipient_id]}",action:'new_message', channel_name: "chat_channel", message: @message,created_at: created_at,attachments: attach_ary
      redirect_to customers_messages_path(transaction_id: params[:transaction_id])

    end

  end

end
