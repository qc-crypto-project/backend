class Customers::ProfilesController < Customers::BaseController
	def edit
	end
	def update
		if  user_edit_params["password"].present?
			ex = user_edit_params
		else
			ex = user_edit_params.except(:password_confirmation)
			ex = ex.except(:password)
		end
		if  params["user"]["profile_image_attributes"].present? &&  params["user"]["profile_image_attributes"]["_destroy"] == "1"
			@user.profile_image.destroy  if @user.profile_image.present?
			ex =  ex.except(:profile_image_attributes)
		elsif params["user"]["profile_image_attributes"].present? &&  params["user"]["profile_image_attributes"]["_destroy"] == "0" && params["user"]["profile_image_attributes"]["document"].blank?
			ex =  ex.except(:profile_image_attributes)
		end
		if @user.update(ex)
			flash[:notice] = "Profile Successfully Updated" 
			redirect_to edit_customers_profile_path(@user.id)
		else
			flash[:error] = @user.errors.full_messages.first 
			redirect_to edit_customers_profile_path(@user.id)
		end
	end

	def form_validate
  		phone_number = params[:phone_number] || params.try(:[], :user).try(:[], :phone_number) || params[:company].try(:[],:phone_number)
    	email = params[:email] || params.try(:[], :user).try(:[], :email) || params[:company].try(:[],:company_email)
    	if email.present?
	       	user = User.where(email: email).first
	      	if user.present? and user != current_user
	        	render json: false
	      	else
	        	render json: true
	      	end
	    end
	end

	def user_edit_params
		params.require(:user).permit(:phone_number,:email,:password,:password_confirmation,profile_image_attributes: [:id,:document,:_detsroy])
	end
end