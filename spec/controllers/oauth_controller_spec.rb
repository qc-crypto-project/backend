require 'rails_helper'

RSpec.describe OauthController, type: :controller do

  before :each do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe "API token generator" do

    it "generate token when client id and client secret key is given" do
      user = FactoryBot.create(:user, :role => 'merchant')
      oauth = OauthApp.where(user_id: user.id).first
      post :token, :params => { client_id: oauth.key, client_secret: oauth.secret }
      expect(response.status).to eq(200)
    end

    it "does not generate token when client id is not given" do
      user = FactoryBot.create(:user, :role => 'merchant')
      oauth = OauthApp.where(user_id: user.id).first
      post :token, :params => { client_secret: oauth.secret }
      expect(response.status).to eq(400)
    end

    it "does not generate token when secret id is not given" do
      user = FactoryBot.create(:user, :role => 'merchant')
      oauth = OauthApp.where(user_id: user.id).first
      post :token, :params => { client_id: oauth.key }
      expect(response.status).to eq(401)
    end

  end
end
