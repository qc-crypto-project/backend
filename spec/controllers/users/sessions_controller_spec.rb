require 'rails_helper'

RSpec.describe Users::SessionsController, type: :controller do

  before :each do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe "User sign_in" do
    subject { post :create, :params => { user: {email: "mibrahim@techcreatix.com", password: "Quickcard-2019"} } }

    it "sign in @user role admin" do
      user = FactoryBot.create(:user, :role => 'admin')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(transactions_admins_path(trans: "success"))
      expect(flash[:success]).to eq("Signed in successfully.")
      # bool = true
      # return result && bool
    end

    it "sign in @user role support" do
      user = FactoryBot.create(:user, :role => 'support')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(transactions_admins_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role iso" do
      user = FactoryBot.create(:user, :role => 'iso')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_transactions_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role agent" do
      user = FactoryBot.create(:user, :role => 'agent')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_transactions_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role qc" do
      user = FactoryBot.create(:user, :role => 'qc')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_transactions_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role partner" do
      user = FactoryBot.create(:user, :role => 'partner')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_transactions_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role affiliate" do
      user = FactoryBot.create(:user, :role => 'affiliate')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_transactions_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role merchant" do
      user = FactoryBot.create(:user, :role => 'merchant')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_locations_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role merchant and sub merchant type is admin" do
      user = FactoryBot.create(:user, :role => 'merchant', :merchant_id => 1, :submerchant_type => 'admin_user')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(merchant_locations_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "sign in @user role merchant and sub merchant type is regular" do
      user = FactoryBot.create(:user, :role => 'merchant', :merchant_id => 1, :submerchant_type => 'regular_user')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to redirect_to(virtual_terminal_merchant_sales_path)
      expect(flash[:success]).to eq("Signed in successfully.")
    end

    it "does not able to sign in @user with role user" do
      user = FactoryBot.create(:user, :role => 'user')
      post :create, :params => { user: {email: user.email, password: user.password} }
      expect(response).to have_http_status(200)
      expect(flash[:alert]).to eq("Invalid Email or password.")
    end

    it "@user does not able to sign in with wrong email" do
      user = FactoryBot.create(:user, :role => 'admin')
      post :create, :params => { user: {email: "email@testing.com", password: user.password} }
      expect(response).to have_http_status(200)
      expect(flash[:alert]).to eq("Invalid Email or password.")
    end

    it "@user does not able to sign in with wrong password" do
      user = FactoryBot.create(:user, :role => 'admin')
      post :create, :params => { user: {email: user.email, password: "testing"} }
      expect(response).to have_http_status(200)
      expect(flash[:alert]).to eq("Invalid Email or password.")
    end

    it "sign out @user" do
      user = FactoryBot.create(:user, :role => 'admin')
      sign_in user
      delete :destroy
      expect(response).to redirect_to(root_path)
      expect(flash[:notice]).to eq("Signed out successfully.")
    end
  end
end
