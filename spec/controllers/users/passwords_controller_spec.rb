require 'rails_helper'

RSpec.describe Users::PasswordsController, type: :controller do

  before :each do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe "User password reset" do

    it "reset @user password" do
      user = FactoryBot.create(:user, :role => 'admin')
      post :create, :params => { user: {email: user.email }}
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:notice]).to eq("If your email is in our system, a password reset link has been sent.")
    end

    it "show error during forget password if user does not exist" do
      post :create, :params => { user: {email: "user@testing.com" }}
      expect(response).to have_http_status(200)
    end

  end

end
