# == Schema Information
#
# Table name: giftcards
#
#  id                :bigint(8)        not null, primary key
#  token             :string
#  exp_year          :string
#  exp_month         :string
#  wallet_id         :bigint(8)
#  is_valid          :boolean          default(TRUE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  balance           :string
#  category          :string
#  remaining_balance :string
#  pincode           :string
#  company_id        :integer
#  slug              :string
#


