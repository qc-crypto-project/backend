source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end
ruby "2.7.1"
# Use Authy for sending token
# gem 'authy'
# gem 'jquery-ui-rails'
gem 'by_star', github: "radar/by_star"
gem 'two_factor_authentication'
gem 'zipline'
gem 'zip-codes'
gem 'phonelib'
gem 'jquery-form-rails', '~> 1.0', '>= 1.0.1'
gem "env_vars"
gem 'figaro', '~> 1.1.1'
gem 'bootstrap-datepicker-rails'
gem 'sendgrid-ruby'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'
gem 'jwt', '~> 2.2', '>= 2.2.1'
gem 'net-sftp'
# Use Wicked for Wizerd Form
# gem 'activerecord-session_store'
gem 'smarter_csv'
# Use Twilio to send confirmation message
gem 'twilio-ruby', '~> 5.29.1'
# gem 'sequence-sdk' ,'>= 3.1', require: 'sequence', github: 'techcreatix/blockchain-sdk'
gem 's3'
gem 'aws-sdk', '~> 3.0', '>= 3.0.1'
# gem 'aws-sdk-core', '~> 1'
# gem 'aws-sdk-s3', '~> 1'
# gem 'aws-sdk-ec2', '~> 1'
# gem 'aws-sdk-ssm', '~> 1'
# gem 'aws-sdk-kms', '~> 1'
gem 'active_model_serializers', '~> 0.10.6', require: true
gem 'chunky_png'
# gem 'obfuscate_id'
gem 'clipboard-rails'
gem 'barby',  '~> 0.6.2'
gem 'rqrcode','~> 1.1.2'
gem "paperclip", '5.1.0'
gem 'kaminari', '~> 1.1.1'
gem 'bootstrap-kaminari-views'
gem 'stripe', '~> 5.11.0'
gem 'devise', '~> 4.4.1'
gem 'json', '2.3.1'
gem 'tether-rails', '~> 1.4.0'
gem 'will_paginate', '~> 3.2.1'
gem 'will_paginate-bootstrap'
gem 'jquery-datatables-rails', github: 'techcreatix/jquery-datatables-rails'
gem 'bcrypt', '~> 3.1.11'
gem 'jquery-rails', '~> 4.3.3'
gem 'rpush', '~> 3.1.1'
gem 'sidekiq', '< 6'
gem 'sidekiq-status'
gem 'hiredis', '~> 0.6.1'
gem 'autoscaler', '~> 1.0.0'
gem 'domain_name', '~> 0.5.20190701'
gem 'mime-types', '>= 1.17.2'
gem 'net-http-digest_auth', '~> 1.4', '>= 1.4.1'
gem 'net-http-persistent', '~> 3.1'
gem 'nokogiri' ,'~> 1.10'
gem 'ntlm-http', '~> 0.1.1'
gem 'awesome_print', '~> 1.8'
gem 'url_safe_base64', '~> 0.2.2'
gem 'openssl', '~> 2.1', '>= 2.1.1'
gem 'friendly_id', '~> 5.3.0'
# -------------------UI GEM--------------------
gem 'jquery-ui-rails'
gem 'rails-jquery-autocomplete'
gem 'best_in_place', '~> 3.1.1'
# ------------------END UI GEM-----------------
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.2.4'
gem 'activerecord-import', '~> 1.0.3'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1.4'
gem "plaid", '~> 8.3.0'
gem "recaptcha", '5.2.1', require: "recaptcha/rails"
gem 'jquery-validation-rails'
gem 'email_validator'
gem "select2-rails"
gem 'rack-cors', '~> 1.1.0'
gem 'slack-ruby-client', require: false
# Use Puma as the app server
gem 'puma', '~> 4.3'
gem 'ahoy_email'
gem 'lockbox'
gem 'blind_index'
gem 'easypost', '~> 3.0', '>= 3.0.1'

gem 'minfraud', '1.0.4'
#payment gateway integrations
# gem 'activemerchant', '~> 1.80'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
gem 'business_time'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 5.0'

# file download gem for csv
gem 'jquery_file_download-rails'

#country code with flags
gem 'intl-tel-input-rails'
gem 'country_select', '~> 4.0'
gem 'countries'
gem 'city-state'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
#db indexes
# gem 'active_record_doctor', '~> 1.5', group: :development
#log all changes in models
gem 'audited', '~> 4.7', '>= 4.7.1'
gem "chartkick"
gem 'groupdate'
#generating excel files
gem 'rubyzip', '~> 1.0.0'
gem 'write_xlsx'
gem 'axlsx', '~> 2.0.1'
gem 'axlsx_rails'
gem "nested_form"
gem 'lightbox-bootstrap-rails', '5.1.0.1'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.2.1'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'dotenv-rails'
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 3.29'
  gem 'selenium-webdriver'
  gem "factory_bot_rails"
end


group :test do
  gem 'rspec-rails', '~> 3.9'
  gem 'shoulda-matchers', '~> 4.1'
  gem 'database_cleaner'
  gem 'faker'
  gem 'rails-controller-testing'
end

group :development do
  # db indexes
  gem 'active_record_doctor', '~> 1.5'
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.3'
  gem 'rb-readline'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rails-erd', require: false
  gem 'capistrano', '3.13.0'
  gem 'capistrano3-puma'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-sidekiq'
  gem 'capistrano-rails-console'
  gem 'capistrano-rvm'
  gem 'capistrano-figaro'
  gem "capistrano-db-tasks", require: false
  gem 'traceroute', '~> 0.8.0'
  gem "better_errors"
  gem "binding_of_caller"
  gem 'pry-rails'
  gem 'capistrano-rake', require: false
  gem 'capistrano-deploytags', '~> 1.0.0', require: false
  gem 'highline'
  gem 'letter_opener'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'simple_form'
gem 'client_side_validations'
gem 'client_side_validations-simple_form'

gem 'httparty'

gem 'tangocard-raas', github: 'techcreatix/raas-v2-sdk-ruby', :branch => 'master'
gem 'rest-client'

# model schema comments
gem 'annotate', '~> 3.0'

gem 'curb', '~> 0.9.6'

gem 'redis-rails'
gem 'wicked', '~> 1.3.4'
gem 'wicked_pdf', '~> 1.1'

# gem 'wkhtmltopdf-binary'
gem 'wkhtmltopdf-binary-edge', '~> 0.12.5.1'
gem 'wkhtmltopdf-heroku'

# searching gem for tables
gem 'ransack', '~> 2.3'
gem 'turnout'
gem 'query_diet'
gem 'devise-security'
gem "sidekiq-cron", "~> 1.1"

gem 'rails-i18n' # Internationalization
gem "sentry-raven" # Sentry Logging for System Exceptions
gem "delayed_paperclip"
gem "secure_headers"
gem 'aes', github: 'techcreatix/aes', :branch => 'master'
gem 'gon'
gem 'dotiw'

# Static Page Caching
gem 'actionpack-page_caching'
gem 'sequence-sdk' ,'>= 2.2', require: 'sequence'

